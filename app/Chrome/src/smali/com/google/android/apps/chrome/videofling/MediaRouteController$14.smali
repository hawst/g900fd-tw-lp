.class Lcom/google/android/apps/chrome/videofling/MediaRouteController$14;
.super Landroid/support/v7/media/i;
.source "MediaRouteController.java"


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

.field final synthetic val$bundleHandler:Lcom/google/android/apps/chrome/videofling/MediaRouteController$ResultBundleHandler;

.field final synthetic val$intent:Landroid/content/Intent;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/videofling/MediaRouteController;Lcom/google/android/apps/chrome/videofling/MediaRouteController$ResultBundleHandler;Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 1201
    iput-object p1, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController$14;->this$0:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    iput-object p2, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController$14;->val$bundleHandler:Lcom/google/android/apps/chrome/videofling/MediaRouteController$ResultBundleHandler;

    iput-object p3, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController$14;->val$intent:Landroid/content/Intent;

    invoke-direct {p0}, Landroid/support/v7/media/i;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1209
    iget-object v1, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController$14;->this$0:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    # getter for: Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mDebug:Z
    invoke-static {v1}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->access$400(Lcom/google/android/apps/chrome/videofling/MediaRouteController;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1212
    const-string/jumbo v1, "MediaRouteController"

    const-string/jumbo v2, "Error sending control request %s %s. Data: %s Error: %s"

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController$14;->val$intent:Landroid/content/Intent;

    aput-object v4, v3, v0

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController$14;->val$intent:Landroid/content/Intent;

    invoke-virtual {v5}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v5

    # invokes: Lcom/google/android/apps/chrome/videofling/MediaRouteController;->bundleToString(Landroid/os/Bundle;)Ljava/lang/String;
    invoke-static {v5}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->access$1600(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    # invokes: Lcom/google/android/apps/chrome/videofling/MediaRouteController;->bundleToString(Landroid/os/Bundle;)Ljava/lang/String;
    invoke-static {p2}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->access$1600(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x3

    aput-object p1, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1218
    :cond_0
    if-eqz p2, :cond_1

    .line 1219
    const-string/jumbo v0, "com.google.android.gms.cast.EXTRA_ERROR_CODE"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 1222
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController$14;->this$0:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    # invokes: Lcom/google/android/apps/chrome/videofling/MediaRouteController;->sendErrorToListeners(I)V
    invoke-static {v1, v0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->access$1700(Lcom/google/android/apps/chrome/videofling/MediaRouteController;I)V

    .line 1224
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController$14;->val$bundleHandler:Lcom/google/android/apps/chrome/videofling/MediaRouteController$ResultBundleHandler;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController$14;->val$bundleHandler:Lcom/google/android/apps/chrome/videofling/MediaRouteController$ResultBundleHandler;

    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/chrome/videofling/MediaRouteController$ResultBundleHandler;->onError(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 1225
    :cond_2
    return-void
.end method

.method public onResult(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 1204
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController$14;->val$bundleHandler:Lcom/google/android/apps/chrome/videofling/MediaRouteController$ResultBundleHandler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController$14;->val$bundleHandler:Lcom/google/android/apps/chrome/videofling/MediaRouteController$ResultBundleHandler;

    invoke-interface {v0, p1}, Lcom/google/android/apps/chrome/videofling/MediaRouteController$ResultBundleHandler;->onResult(Landroid/os/Bundle;)V

    .line 1205
    :cond_0
    return-void
.end method
