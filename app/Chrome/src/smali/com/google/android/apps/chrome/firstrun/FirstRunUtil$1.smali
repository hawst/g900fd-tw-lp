.class final Lcom/google/android/apps/chrome/firstrun/FirstRunUtil$1;
.super Ljava/lang/Object;
.source "FirstRunUtil.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic val$activity:Landroid/app/Activity;

.field final synthetic val$showSignInNotification:Z


# direct methods
.method constructor <init>(Landroid/app/Activity;Z)V
    .locals 0

    .prologue
    .line 84
    iput-object p1, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunUtil$1;->val$activity:Landroid/app/Activity;

    iput-boolean p2, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunUtil$1;->val$showSignInNotification:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunUtil$1;->val$activity:Landroid/app/Activity;

    invoke-static {v0}, Lorg/chromium/chrome/browser/signin/SigninManager;->get(Landroid/content/Context;)Lorg/chromium/chrome/browser/signin/SigninManager;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/signin/SigninManager;->logInSignedInUser()V

    .line 91
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunUtil$1;->val$showSignInNotification:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunUtil$1;->val$activity:Landroid/app/Activity;

    invoke-static {v0}, Lcom/google/android/apps/chrome/firstrun/FirstRunUtil;->showSyncSignInNotification(Landroid/content/Context;)V

    .line 92
    :cond_0
    return-void
.end method
