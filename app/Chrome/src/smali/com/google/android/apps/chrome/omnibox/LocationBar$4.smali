.class Lcom/google/android/apps/chrome/omnibox/LocationBar$4;
.super Lcom/google/android/apps/chrome/CustomSelectionActionModeCallback;
.source "LocationBar.java"


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/omnibox/LocationBar;)V
    .locals 0

    .prologue
    .line 949
    iput-object p1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$4;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/CustomSelectionActionModeCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onActionItemClicked(Landroid/view/ActionMode;Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    .line 959
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sget v1, Lcom/google/android/apps/chrome/R$id;->copy_url:I

    if-ne v0, v1, :cond_0

    .line 960
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$4;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v1, "clipboard"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ClipboardManager;

    .line 963
    const-string/jumbo v1, "url"

    iget-object v2, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$4;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    # getter for: Lcom/google/android/apps/chrome/omnibox/LocationBar;->mOriginalUrl:Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->access$3100(Lcom/google/android/apps/chrome/omnibox/LocationBar;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/content/ClipData;->newPlainText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/content/ClipData;

    move-result-object v1

    .line 964
    invoke-virtual {v0, v1}, Landroid/content/ClipboardManager;->setPrimaryClip(Landroid/content/ClipData;)V

    .line 965
    invoke-virtual {p1}, Landroid/view/ActionMode;->finish()V

    .line 966
    const/4 v0, 0x1

    .line 968
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/chrome/CustomSelectionActionModeCallback;->onActionItemClicked(Landroid/view/ActionMode;Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public onPrepareActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 2

    .prologue
    .line 952
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/chrome/CustomSelectionActionModeCallback;->onPrepareActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z

    .line 953
    invoke-virtual {p1}, Landroid/view/ActionMode;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$menu;->textselectionmenu:I

    invoke-virtual {v0, v1, p2}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 954
    const/4 v0, 0x1

    return v0
.end method
