.class public Lcom/google/android/apps/chrome/signin/AccountManagementFragment;
.super Lcom/google/android/apps/chrome/preferences/ChromeBaseFragment;
.source "AccountManagementFragment.java"

# interfaces
.implements Lcom/google/android/apps/chrome/services/ui/SignOutDialogFragment$Listener;
.implements Lorg/chromium/base/BaseChromiumApplication$WindowFocusChangedListener;
.implements Lorg/chromium/chrome/browser/profiles/ProfileDownloader$Observer;
.implements Lorg/chromium/chrome/browser/sync/ProfileSyncService$SyncStateChangedListener;


# static fields
.field private static final sToNamePicture:Ljava/util/HashMap;


# instance fields
.field private mAccountNotYouView:Landroid/widget/TextView;

.field private mAccountsListLayout:Landroid/widget/LinearLayout;

.field private mAccountsWhenSignedIn:Landroid/view/View;

.field private mAccountsWhenSignedOut:Landroid/view/View;

.field private mAddAccountBitmap:Landroid/graphics/Bitmap;

.field private mDefaultAccountBitmap:Landroid/graphics/Bitmap;

.field private mGaiaServiceType:I

.field private mGoIncognitoButtonDisabled:Landroid/view/View;

.field private mGoIncognitoButtonEnabled:Landroid/view/View;

.field private mParentInformation:Landroid/widget/LinearLayout;

.field private mProfileSyncService:Lorg/chromium/chrome/browser/sync/ProfileSyncService;

.field private mSavedTitle:Ljava/lang/CharSequence;

.field private mSignInOutSwitch:Landroid/widget/Switch;

.field private mSignedOut:Z

.field private mSwitchEnabler:Lcom/google/android/apps/chrome/signin/AccountManagementFragment$SignedInSwitchEnabler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 104
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->sToNamePicture:Ljava/util/HashMap;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 83
    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBaseFragment;-><init>()V

    .line 671
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/signin/AccountManagementFragment;)I
    .locals 1

    .prologue
    .line 83
    iget v0, p0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->mGaiaServiceType:I

    return v0
.end method

.method static synthetic access$102(Lcom/google/android/apps/chrome/signin/AccountManagementFragment;Z)Z
    .locals 0

    .prologue
    .line 83
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->mSignedOut:Z

    return p1
.end method

.method static synthetic access$200(Landroid/content/Context;Lorg/chromium/chrome/browser/profiles/Profile;I)V
    .locals 0

    .prologue
    .line 83
    invoke-static {p0, p1, p2}, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->openAccountManagementScreen(Landroid/content/Context;Lorg/chromium/chrome/browser/profiles/Profile;I)V

    return-void
.end method

.method static synthetic access$300(Landroid/content/Context;Lorg/chromium/chrome/browser/profiles/Profile;)V
    .locals 0

    .prologue
    .line 83
    invoke-static {p0, p1}, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->startFetchingAccountsInformation(Landroid/content/Context;Lorg/chromium/chrome/browser/profiles/Profile;)V

    return-void
.end method

.method static synthetic access$400(Landroid/app/Activity;Landroid/widget/LinearLayout;Landroid/graphics/Bitmap;Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 83
    invoke-static {p0, p1, p2, p3, p4}, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->fillAccountsList(Landroid/app/Activity;Landroid/widget/LinearLayout;Landroid/graphics/Bitmap;Ljava/lang/String;I)V

    return-void
.end method

.method static synthetic access$500(Landroid/app/Activity;Landroid/content/DialogInterface$OnDismissListener;)V
    .locals 0

    .prologue
    .line 83
    invoke-static {p0, p1}, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->showUpgradeScreen(Landroid/app/Activity;Landroid/content/DialogInterface$OnDismissListener;)V

    return-void
.end method

.method static synthetic access$600()Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 83
    sget-object v0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->sToNamePicture:Ljava/util/HashMap;

    return-object v0
.end method

.method private static fillAccountsList(Landroid/app/Activity;Landroid/widget/LinearLayout;Landroid/graphics/Bitmap;Ljava/lang/String;I)V
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 449
    invoke-static {p0}, Lorg/chromium/sync/signin/AccountManagerHelper;->get(Landroid/content/Context;)Lorg/chromium/sync/signin/AccountManagerHelper;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/sync/signin/AccountManagerHelper;->getGoogleAccounts()[Landroid/accounts/Account;

    move-result-object v4

    move v1, v2

    .line 450
    :goto_0
    array-length v0, v4

    if-ge v1, v0, :cond_1

    .line 451
    aget-object v5, v4, v1

    .line 452
    iget-object v0, v5, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->getCachedUserPicture(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 453
    if-nez v0, :cond_2

    move-object v3, p2

    .line 454
    :goto_1
    iget-object v0, v5, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v0, p3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v6

    .line 456
    sget v0, Lcom/google/android/apps/chrome/R$layout;->account_management_list_item:I

    const/4 v7, 0x0

    invoke-static {p0, v0, v7}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v7

    .line 457
    sget v0, Lcom/google/android/apps/chrome/R$id;->account_name:I

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 458
    iget-object v8, v5, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 459
    sget v0, Lcom/google/android/apps/chrome/R$id;->display_image:I

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 460
    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 462
    sget v0, Lcom/google/android/apps/chrome/R$id;->account_summary:I

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {p0}, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->getSyncStatusSummary(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 465
    if-eqz v6, :cond_0

    .line 466
    const/4 v0, 0x1

    invoke-virtual {v7, v0}, Landroid/view/View;->setClickable(Z)V

    .line 467
    new-instance v0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment$5;

    invoke-direct {v0, p4, p0, v5}, Lcom/google/android/apps/chrome/signin/AccountManagementFragment$5;-><init>(ILandroid/app/Activity;Landroid/accounts/Account;)V

    invoke-virtual {v7, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 496
    :goto_2
    invoke-virtual {p1, v7}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 497
    sget v0, Lcom/google/android/apps/chrome/R$layout;->account_management_list_separator:I

    invoke-static {p0, v0, p1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 450
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 491
    :cond_0
    sget v0, Lcom/google/android/apps/chrome/R$id;->account_summary:I

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 492
    invoke-virtual {v7, v2}, Landroid/view/View;->setClickable(Z)V

    goto :goto_2

    .line 499
    :cond_1
    return-void

    :cond_2
    move-object v3, v0

    goto :goto_1
.end method

.method public static getCachedUserName(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 848
    sget-object v0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->sToNamePicture:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .line 849
    if-eqz v0, :cond_0

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getCachedUserPicture(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 857
    sget-object v0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->sToNamePicture:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .line 858
    if-eqz v0, :cond_0

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Landroid/graphics/Bitmap;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static getSyncStatusSummary(Landroid/app/Activity;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 565
    invoke-static {p0}, Lorg/chromium/sync/signin/ChromeSigninController;->get(Landroid/content/Context;)Lorg/chromium/sync/signin/ChromeSigninController;

    move-result-object v0

    .line 566
    invoke-virtual {v0}, Lorg/chromium/sync/signin/ChromeSigninController;->isSignedIn()Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v0, ""

    .line 602
    :goto_0
    return-object v0

    .line 568
    :cond_0
    invoke-static {p0}, Lorg/chromium/sync/notifier/SyncStatusHelper;->get(Landroid/content/Context;)Lorg/chromium/sync/notifier/SyncStatusHelper;

    move-result-object v1

    .line 569
    invoke-static {p0}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->get(Landroid/content/Context;)Lorg/chromium/chrome/browser/sync/ProfileSyncService;

    move-result-object v2

    .line 570
    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 572
    invoke-static {p0}, Lcom/google/android/apps/chrome/services/ChildAccountManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/services/ChildAccountManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/services/ChildAccountManager;->hasChildAccount()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 573
    sget v0, Lcom/google/android/apps/chrome/R$string;->uca_account:I

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 576
    :cond_1
    invoke-virtual {v1}, Lorg/chromium/sync/notifier/SyncStatusHelper;->isMasterSyncAutomaticallyEnabled()Z

    move-result v4

    if-nez v4, :cond_2

    .line 577
    sget v0, Lcom/google/android/apps/chrome/R$string;->sync_android_master_sync_disabled:I

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 580
    :cond_2
    invoke-virtual {v2}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->getAuthError()Lorg/chromium/chrome/browser/sync/GoogleServiceAuthError$State;

    move-result-object v4

    sget-object v5, Lorg/chromium/chrome/browser/sync/GoogleServiceAuthError$State;->NONE:Lorg/chromium/chrome/browser/sync/GoogleServiceAuthError$State;

    if-eq v4, v5, :cond_3

    .line 581
    invoke-virtual {v2}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->getAuthError()Lorg/chromium/chrome/browser/sync/GoogleServiceAuthError$State;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/sync/GoogleServiceAuthError$State;->getMessage()I

    move-result v0

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 584
    :cond_3
    invoke-virtual {v1}, Lorg/chromium/sync/notifier/SyncStatusHelper;->isSyncEnabled()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 585
    invoke-virtual {v2}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->isSyncInitialized()Z

    move-result v4

    if-nez v4, :cond_4

    .line 586
    sget v0, Lcom/google/android/apps/chrome/R$string;->sync_setup_progress:I

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 589
    :cond_4
    sget-object v4, Lcom/google/android/apps/chrome/signin/AccountManagementFragment$13;->$SwitchMap$org$chromium$sync$internal_api$pub$SyncDecryptionPassphraseType:[I

    invoke-virtual {v2}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->getSyncDecryptionPassphraseTypeIfRequired()Lorg/chromium/sync/internal_api/pub/SyncDecryptionPassphraseType;

    move-result-object v2

    invoke-virtual {v2}, Lorg/chromium/sync/internal_api/pub/SyncDecryptionPassphraseType;->ordinal()I

    move-result v2

    aget v2, v4, v2

    packed-switch v2, :pswitch_data_0

    .line 602
    :cond_5
    invoke-virtual {v0}, Lorg/chromium/sync/signin/ChromeSigninController;->getSignedInUser()Landroid/accounts/Account;

    move-result-object v0

    invoke-virtual {v1, v0}, Lorg/chromium/sync/notifier/SyncStatusHelper;->isSyncEnabled(Landroid/accounts/Account;)Z

    move-result v0

    if-eqz v0, :cond_6

    sget v0, Lcom/google/android/apps/chrome/R$string;->sync_is_enabled:I

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 591
    :pswitch_0
    sget v0, Lcom/google/android/apps/chrome/R$string;->sync_need_password:I

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 594
    :pswitch_1
    sget v0, Lcom/google/android/apps/chrome/R$string;->sync_need_passphrase:I

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 602
    :cond_6
    sget v0, Lcom/google/android/apps/chrome/R$string;->sync_is_disabled:I

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 589
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public static makeRoundUserPicture(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 8

    .prologue
    const/high16 v7, 0x3f000000    # 0.5f

    const/4 v6, 0x0

    .line 652
    if-nez p0, :cond_0

    const/4 v0, 0x0

    .line 668
    :goto_0
    return-object v0

    .line 654
    :cond_0
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 656
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 658
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    .line 659
    new-instance v3, Landroid/graphics/Rect;

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    invoke-direct {v3, v6, v6, v4, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 661
    invoke-virtual {v1, v6, v6, v6, v6}, Landroid/graphics/Canvas;->drawARGB(IIII)V

    .line 662
    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 663
    const/4 v4, -0x1

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 664
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v4, v7

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v5, v7

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    int-to-float v6, v6

    mul-float/2addr v6, v7

    invoke-virtual {v1, v4, v5, v6, v2}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 666
    new-instance v4, Landroid/graphics/PorterDuffXfermode;

    sget-object v5, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v4, v5}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 667
    invoke-virtual {v1, p0, v3, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto :goto_0
.end method

.method private static openAccountManagementScreen(Landroid/content/Context;Lorg/chromium/chrome/browser/profiles/Profile;I)V
    .locals 3

    .prologue
    .line 615
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 616
    const-class v1, Lcom/google/android/apps/chrome/preferences/Preferences;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    .line 617
    const/high16 v1, 0x30020000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 620
    const-string/jumbo v1, ":android:show_fragment"

    const-class v2, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 622
    const-string/jumbo v1, "display_home_as_up"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 623
    const-string/jumbo v1, ":android:no_headers"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 624
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 625
    const-string/jumbo v2, "ShowGAIAServiceType"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 626
    const-string/jumbo v2, ":android:show_fragment_args"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 628
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 629
    return-void
.end method

.method public static prefetchUserNamePicture(Landroid/content/Context;Lorg/chromium/chrome/browser/profiles/Profile;)V
    .locals 2

    .prologue
    .line 884
    invoke-static {p0}, Lorg/chromium/sync/signin/ChromeSigninController;->get(Landroid/content/Context;)Lorg/chromium/sync/signin/ChromeSigninController;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/sync/signin/ChromeSigninController;->getSignedInAccountName()Ljava/lang/String;

    move-result-object v0

    .line 885
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 899
    :cond_0
    :goto_0
    return-void

    .line 886
    :cond_1
    sget-object v1, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->sToNamePicture:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    .line 888
    new-instance v1, Lcom/google/android/apps/chrome/signin/AccountManagementFragment$12;

    invoke-direct {v1, v0}, Lcom/google/android/apps/chrome/signin/AccountManagementFragment$12;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Lorg/chromium/chrome/browser/profiles/ProfileDownloader;->addObserver(Lorg/chromium/chrome/browser/profiles/ProfileDownloader$Observer;)V

    .line 898
    invoke-static {p0, p1, v0}, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->startFetchingAccountInformation(Landroid/content/Context;Lorg/chromium/chrome/browser/profiles/Profile;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static registerAccountManagementScreenManager()V
    .locals 1

    .prologue
    .line 635
    new-instance v0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment$7;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/signin/AccountManagementFragment$7;-><init>()V

    invoke-static {v0}, Lorg/chromium/chrome/browser/signin/AccountManagementScreenHelper;->setManager(Lorg/chromium/chrome/browser/signin/AccountManagementScreenHelper$AccountManagementScreenManager;)V

    .line 644
    return-void
.end method

.method public static shouldAlwaysShowUpgradeScreen()Z
    .locals 2

    .prologue
    .line 764
    invoke-static {}, Lorg/chromium/base/CommandLine;->getInstance()Lorg/chromium/base/CommandLine;

    move-result-object v0

    const-string/jumbo v1, "always-show-new-profile-management-upgrade-screen"

    invoke-virtual {v0, v1}, Lorg/chromium/base/CommandLine;->hasSwitch(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private static showUpgradeScreen(Landroid/app/Activity;Landroid/content/DialogInterface$OnDismissListener;)V
    .locals 5

    .prologue
    .line 716
    const-string/jumbo v0, "layout_inflater"

    invoke-virtual {p0, v0}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 718
    sget v1, Lcom/google/android/apps/chrome/R$layout;->account_management_upgrade_screen:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 719
    sget v0, Lcom/google/android/apps/chrome/R$id;->accounts_list:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 722
    new-instance v2, Lcom/google/android/apps/chrome/signin/AccountManagementFragment$UpgradeScreenAccountsListHelper;

    invoke-static {}, Lorg/chromium/chrome/browser/profiles/Profile;->getLastUsedProfile()Lorg/chromium/chrome/browser/profiles/Profile;

    move-result-object v3

    invoke-direct {v2, p0, v3, v0}, Lcom/google/android/apps/chrome/signin/AccountManagementFragment$UpgradeScreenAccountsListHelper;-><init>(Landroid/app/Activity;Lorg/chromium/chrome/browser/profiles/Profile;Landroid/widget/LinearLayout;)V

    .line 725
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 726
    sget v3, Lcom/google/android/apps/chrome/R$string;->account_management_upgrade_screen_title:I

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    sget v3, Lcom/google/android/apps/chrome/R$string;->account_management_upgrade_screen_ok_title:I

    new-instance v4, Lcom/google/android/apps/chrome/signin/AccountManagementFragment$9;

    invoke-direct {v4}, Lcom/google/android/apps/chrome/signin/AccountManagementFragment$9;-><init>()V

    invoke-virtual {v1, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    sget v3, Lcom/google/android/apps/chrome/R$string;->account_management_upgrade_screen_settings_title:I

    new-instance v4, Lcom/google/android/apps/chrome/signin/AccountManagementFragment$8;

    invoke-direct {v4, p0}, Lcom/google/android/apps/chrome/signin/AccountManagementFragment$8;-><init>(Landroid/app/Activity;)V

    invoke-virtual {v1, v3, v4}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 746
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 747
    new-instance v1, Lcom/google/android/apps/chrome/signin/AccountManagementFragment$10;

    invoke-direct {v1, v2, p1}, Lcom/google/android/apps/chrome/signin/AccountManagementFragment$10;-><init>(Lcom/google/android/apps/chrome/signin/AccountManagementFragment$UpgradeScreenAccountsListHelper;Landroid/content/DialogInterface$OnDismissListener;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 755
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->recordAccountConsistencyActionShown()V

    .line 756
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 757
    return-void
.end method

.method public static startFetchingAccountInformation(Landroid/content/Context;Lorg/chromium/chrome/browser/profiles/Profile;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 870
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 876
    :cond_0
    :goto_0
    return-void

    .line 871
    :cond_1
    sget-object v0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->sToNamePicture:Ljava/util/HashMap;

    invoke-virtual {v0, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 873
    invoke-static {p0}, Lorg/chromium/ui/gfx/DeviceDisplayInfo;->create(Landroid/content/Context;)Lorg/chromium/ui/gfx/DeviceDisplayInfo;

    move-result-object v0

    .line 874
    const-wide/high16 v2, 0x4050000000000000L    # 64.0

    invoke-virtual {v0}, Lorg/chromium/ui/gfx/DeviceDisplayInfo;->getDIPScale()D

    move-result-wide v0

    mul-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    .line 875
    invoke-static {p1, p2, v0}, Lorg/chromium/chrome/browser/profiles/ProfileDownloader;->startFetchingAccountInfoFor(Lorg/chromium/chrome/browser/profiles/Profile;Ljava/lang/String;I)V

    goto :goto_0
.end method

.method private static startFetchingAccountsInformation(Landroid/content/Context;Lorg/chromium/chrome/browser/profiles/Profile;)V
    .locals 5

    .prologue
    .line 227
    invoke-static {p0}, Lorg/chromium/ui/gfx/DeviceDisplayInfo;->create(Landroid/content/Context;)Lorg/chromium/ui/gfx/DeviceDisplayInfo;

    move-result-object v0

    .line 228
    const-wide/high16 v2, 0x4050000000000000L    # 64.0

    invoke-virtual {v0}, Lorg/chromium/ui/gfx/DeviceDisplayInfo;->getDIPScale()D

    move-result-wide v0

    mul-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v1, v0

    .line 229
    invoke-static {p0}, Lorg/chromium/sync/signin/AccountManagerHelper;->get(Landroid/content/Context;)Lorg/chromium/sync/signin/AccountManagerHelper;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/sync/signin/AccountManagerHelper;->getGoogleAccounts()[Landroid/accounts/Account;

    move-result-object v2

    .line 230
    const/4 v0, 0x0

    :goto_0
    array-length v3, v2

    if-ge v0, v3, :cond_1

    .line 231
    sget-object v3, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->sToNamePicture:Ljava/util/HashMap;

    aget-object v4, v2, v0

    iget-object v4, v4, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    if-nez v3, :cond_0

    .line 232
    aget-object v3, v2, v0

    iget-object v3, v3, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {p1, v3, v1}, Lorg/chromium/chrome/browser/profiles/ProfileDownloader;->startFetchingAccountInfoFor(Lorg/chromium/chrome/browser/profiles/Profile;Ljava/lang/String;I)V

    .line 230
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 236
    :cond_1
    return-void
.end method

.method public static updateUserNamePictureCache(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 3

    .prologue
    .line 839
    sget-object v0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->sToNamePicture:Ljava/util/HashMap;

    new-instance v1, Landroid/util/Pair;

    invoke-static {p2}, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->makeRoundUserPicture(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-direct {v1, p1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 841
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 169
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/preferences/ChromeBaseFragment;->onCreate(Landroid/os/Bundle;)V

    .line 170
    iput v3, p0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->mGaiaServiceType:I

    .line 171
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 172
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "ShowGAIAServiceType"

    iget v2, p0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->mGaiaServiceType:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->mGaiaServiceType:I

    .line 176
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getTitle()Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->mSavedTitle:Ljava/lang/CharSequence;

    .line 178
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->get(Landroid/content/Context;)Lorg/chromium/chrome/browser/sync/ProfileSyncService;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->mProfileSyncService:Lorg/chromium/chrome/browser/sync/ProfileSyncService;

    .line 179
    invoke-static {p0}, Lorg/chromium/chrome/browser/profiles/ProfileDownloader;->addObserver(Lorg/chromium/chrome/browser/profiles/ProfileDownloader$Observer;)V

    .line 181
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$drawable;->account_management_plus:I

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->makeRoundUserPicture(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->mAddAccountBitmap:Landroid/graphics/Bitmap;

    .line 183
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$drawable;->account_management_no_picture:I

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->makeRoundUserPicture(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->mDefaultAccountBitmap:Landroid/graphics/Bitmap;

    .line 186
    iget v0, p0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->mGaiaServiceType:I

    invoke-static {v3, v0}, Lorg/chromium/chrome/browser/signin/AccountManagementScreenHelper;->logEvent(II)V

    .line 190
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {}, Lorg/chromium/chrome/browser/profiles/Profile;->getLastUsedProfile()Lorg/chromium/chrome/browser/profiles/Profile;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->startFetchingAccountsInformation(Landroid/content/Context;Lorg/chromium/chrome/browser/profiles/Profile;)V

    .line 191
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 246
    sget v0, Lcom/google/android/apps/chrome/R$layout;->account_management_screen:I

    invoke-virtual {p1, v0, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 248
    sget v0, Lcom/google/android/apps/chrome/R$id;->sign_in_out_switch:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Switch;

    iput-object v0, p0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->mSignInOutSwitch:Landroid/widget/Switch;

    .line 249
    new-instance v0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment$SignedInSwitchEnabler;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->mSignInOutSwitch:Landroid/widget/Switch;

    invoke-direct {v0, p0, v2, v3}, Lcom/google/android/apps/chrome/signin/AccountManagementFragment$SignedInSwitchEnabler;-><init>(Lcom/google/android/apps/chrome/signin/AccountManagementFragment;Landroid/content/Context;Landroid/widget/Switch;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->mSwitchEnabler:Lcom/google/android/apps/chrome/signin/AccountManagementFragment$SignedInSwitchEnabler;

    .line 250
    iget-object v0, p0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->mSwitchEnabler:Lcom/google/android/apps/chrome/signin/AccountManagementFragment$SignedInSwitchEnabler;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/signin/AccountManagementFragment$SignedInSwitchEnabler;->attach()V

    .line 252
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/services/ChildAccountManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/services/ChildAccountManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/services/ChildAccountManager;->hasChildAccount()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 253
    iget-object v0, p0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->mSignInOutSwitch:Landroid/widget/Switch;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/Switch;->setVisibility(I)V

    .line 254
    sget v0, Lcom/google/android/apps/chrome/R$id;->sign_in_child_message:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 257
    :cond_0
    sget v0, Lcom/google/android/apps/chrome/R$id;->accounts_when_signed_in:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->mAccountsWhenSignedIn:Landroid/view/View;

    .line 258
    sget v0, Lcom/google/android/apps/chrome/R$id;->accounts_when_signed_out:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->mAccountsWhenSignedOut:Landroid/view/View;

    .line 259
    iget-object v0, p0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->mAccountsWhenSignedOut:Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lorg/chromium/chrome/browser/signin/SigninManager;->get(Landroid/content/Context;)Lorg/chromium/chrome/browser/signin/SigninManager;

    move-result-object v2

    invoke-virtual {v2}, Lorg/chromium/chrome/browser/signin/SigninManager;->isSignInAllowed()Z

    move-result v2

    invoke-virtual {v0, v2}, Landroid/view/View;->setEnabled(Z)V

    .line 261
    iget-object v0, p0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->mAccountsWhenSignedOut:Landroid/view/View;

    new-instance v2, Lcom/google/android/apps/chrome/signin/AccountManagementFragment$1;

    invoke-direct {v2, p0}, Lcom/google/android/apps/chrome/signin/AccountManagementFragment$1;-><init>(Lcom/google/android/apps/chrome/signin/AccountManagementFragment;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 268
    sget v0, Lcom/google/android/apps/chrome/R$id;->account_not_you:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->mAccountNotYouView:Landroid/widget/TextView;

    .line 269
    sget v0, Lcom/google/android/apps/chrome/R$id;->accounts_list_ll:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->mAccountsListLayout:Landroid/widget/LinearLayout;

    .line 270
    sget v0, Lcom/google/android/apps/chrome/R$id;->account_custodian_information:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->mParentInformation:Landroid/widget/LinearLayout;

    .line 273
    sget v0, Lcom/google/android/apps/chrome/R$id;->go_incognito_button:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->mGoIncognitoButtonEnabled:Landroid/view/View;

    .line 274
    iget-object v0, p0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->mGoIncognitoButtonEnabled:Landroid/view/View;

    new-instance v2, Lcom/google/android/apps/chrome/signin/AccountManagementFragment$2;

    invoke-direct {v2, p0}, Lcom/google/android/apps/chrome/signin/AccountManagementFragment$2;-><init>(Lcom/google/android/apps/chrome/signin/AccountManagementFragment;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 296
    sget v0, Lcom/google/android/apps/chrome/R$id;->go_incognito_disabled_button:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->mGoIncognitoButtonDisabled:Landroid/view/View;

    .line 297
    iget-object v0, p0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->mGoIncognitoButtonDisabled:Landroid/view/View;

    new-instance v2, Lcom/google/android/apps/chrome/signin/AccountManagementFragment$3;

    invoke-direct {v2, p0}, Lcom/google/android/apps/chrome/signin/AccountManagementFragment$3;-><init>(Lcom/google/android/apps/chrome/signin/AccountManagementFragment;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 304
    return-object v1
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 195
    invoke-super {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBaseFragment;->onDestroy()V

    .line 196
    invoke-static {p0}, Lorg/chromium/chrome/browser/profiles/ProfileDownloader;->removeObserver(Lorg/chromium/chrome/browser/profiles/ProfileDownloader$Observer;)V

    .line 197
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/preferences/Preferences;->isTabletPreferencesUi(Landroid/app/Activity;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->mSavedTitle:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setTitle(Ljava/lang/CharSequence;)V

    .line 198
    :cond_0
    return-void
.end method

.method public onDismissedSignOut()V
    .locals 2

    .prologue
    .line 548
    iget-object v0, p0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->mSignInOutSwitch:Landroid/widget/Switch;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lorg/chromium/sync/signin/ChromeSigninController;->get(Landroid/content/Context;)Lorg/chromium/sync/signin/ChromeSigninController;

    move-result-object v1

    invoke-virtual {v1}, Lorg/chromium/sync/signin/ChromeSigninController;->isSignedIn()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Switch;->setChecked(Z)V

    .line 549
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->mSignedOut:Z

    if-nez v0, :cond_0

    .line 550
    const/4 v0, 0x7

    iget v1, p0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->mGaiaServiceType:I

    invoke-static {v0, v1}, Lorg/chromium/chrome/browser/signin/AccountManagementScreenHelper;->logEvent(II)V

    .line 554
    :cond_0
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 215
    invoke-super {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBaseFragment;->onPause()V

    .line 216
    iget-object v0, p0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->mProfileSyncService:Lorg/chromium/chrome/browser/sync/ProfileSyncService;

    invoke-virtual {v0, p0}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->removeSyncStateChangedListener(Lorg/chromium/chrome/browser/sync/ProfileSyncService$SyncStateChangedListener;)V

    .line 217
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lorg/chromium/base/BaseChromiumApplication;

    invoke-virtual {v0, p0}, Lorg/chromium/base/BaseChromiumApplication;->unregisterWindowFocusChangedListener(Lorg/chromium/base/BaseChromiumApplication$WindowFocusChangedListener;)V

    .line 219
    return-void
.end method

.method public onProfileDownloaded(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 0

    .prologue
    .line 506
    invoke-static {p1, p2, p3}, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->updateUserNamePictureCache(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    .line 507
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->updateAccountsList()V

    .line 508
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 202
    invoke-super {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBaseFragment;->onResume()V

    .line 203
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lorg/chromium/base/BaseChromiumApplication;

    invoke-virtual {v0, p0}, Lorg/chromium/base/BaseChromiumApplication;->registerWindowFocusChangedListener(Lorg/chromium/base/BaseChromiumApplication$WindowFocusChangedListener;)V

    .line 206
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getSignOutAllowed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 207
    iget-object v0, p0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->mSignInOutSwitch:Landroid/widget/Switch;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Switch;->setEnabled(Z)V

    .line 209
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->mProfileSyncService:Lorg/chromium/chrome/browser/sync/ProfileSyncService;

    invoke-virtual {v0, p0}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->addSyncStateChangedListener(Lorg/chromium/chrome/browser/sync/ProfileSyncService$SyncStateChangedListener;)V

    .line 210
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->updateAccountsList()V

    .line 211
    return-void
.end method

.method public onSignedOut()V
    .locals 3

    .prologue
    .line 515
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->mSignedOut:Z

    .line 517
    new-instance v0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment$6;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/signin/AccountManagementFragment$6;-><init>(Lcom/google/android/apps/chrome/signin/AccountManagementFragment;)V

    .line 536
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lorg/chromium/sync/signin/ChromeSigninController;->get(Landroid/content/Context;)Lorg/chromium/sync/signin/ChromeSigninController;

    move-result-object v1

    invoke-virtual {v1}, Lorg/chromium/sync/signin/ChromeSigninController;->isSignedIn()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 537
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->get(Landroid/content/Context;)Lcom/google/android/apps/chrome/services/GoogleServicesManager;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->signOut(Landroid/app/Activity;Ljava/lang/Runnable;)V

    .line 538
    const/4 v0, 0x6

    iget v1, p0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->mGaiaServiceType:I

    invoke-static {v0, v1}, Lorg/chromium/chrome/browser/signin/AccountManagementScreenHelper;->logEvent(II)V

    .line 544
    :goto_0
    return-void

    .line 542
    :cond_0
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-virtual {v1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public onWindowFocusChanged(Landroid/app/Activity;Z)V
    .locals 1

    .prologue
    .line 240
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-ne p1, v0, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->updateAccountsList()V

    .line 241
    :cond_0
    return-void
.end method

.method public syncStateChanged()V
    .locals 0

    .prologue
    .line 561
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->updateAccountsList()V

    .line 562
    return-void
.end method

.method public updateAccountsList()V
    .locals 12

    .prologue
    const/4 v4, 0x4

    const/4 v11, 0x2

    const/16 v10, 0x8

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 311
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/services/ChildAccountManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/services/ChildAccountManager;

    move-result-object v5

    .line 312
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/preferences/Preferences;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/Preferences;->updatePreferencesHeadersAndMenu()V

    .line 314
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/sync/signin/ChromeSigninController;->get(Landroid/content/Context;)Lorg/chromium/sync/signin/ChromeSigninController;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/sync/signin/ChromeSigninController;->isSignedIn()Z

    move-result v0

    if-nez v0, :cond_0

    .line 315
    iget-object v0, p0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->mAccountsWhenSignedIn:Landroid/view/View;

    invoke-virtual {v0, v10}, Landroid/view/View;->setVisibility(I)V

    .line 316
    iget-object v0, p0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->mAccountsWhenSignedOut:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 317
    iget-object v0, p0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->mSignInOutSwitch:Landroid/widget/Switch;

    invoke-virtual {v0, v3}, Landroid/widget/Switch;->setChecked(Z)V

    .line 323
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->isAdded()Z

    move-result v0

    if-nez v0, :cond_1

    .line 443
    :goto_1
    return-void

    .line 319
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->mAccountsWhenSignedIn:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 320
    iget-object v0, p0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->mAccountsWhenSignedOut:Landroid/view/View;

    invoke-virtual {v0, v10}, Landroid/view/View;->setVisibility(I)V

    .line 321
    iget-object v0, p0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->mSignInOutSwitch:Landroid/widget/Switch;

    invoke-virtual {v0, v2}, Landroid/widget/Switch;->setChecked(Z)V

    goto :goto_0

    .line 325
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/sync/signin/ChromeSigninController;->get(Landroid/content/Context;)Lorg/chromium/sync/signin/ChromeSigninController;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/sync/signin/ChromeSigninController;->getSignedInAccountName()Ljava/lang/String;

    move-result-object v1

    .line 327
    invoke-static {}, Lorg/chromium/chrome/browser/profiles/Profile;->getLastUsedProfile()Lorg/chromium/chrome/browser/profiles/Profile;

    move-result-object v6

    .line 330
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/sync/signin/ChromeSigninController;->get(Landroid/content/Context;)Lorg/chromium/sync/signin/ChromeSigninController;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/sync/signin/ChromeSigninController;->isSignedIn()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 331
    invoke-static {v1}, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->getCachedUserName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 332
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-static {v6}, Lorg/chromium/chrome/browser/profiles/ProfileDownloader;->getCachedName(Lorg/chromium/chrome/browser/profiles/Profile;)Ljava/lang/String;

    move-result-object v0

    .line 333
    :cond_2
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_3

    move-object v0, v1

    .line 334
    :cond_3
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 335
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v6, Lcom/google/android/apps/chrome/R$string;->account_management_title:I

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 341
    :cond_4
    :goto_2
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/apps/chrome/preferences/Preferences;->isTabletPreferencesUi(Landroid/app/Activity;)Z

    move-result v6

    if-nez v6, :cond_5

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-virtual {v6, v0}, Landroid/app/Activity;->setTitle(Ljava/lang/CharSequence;)V

    .line 343
    :cond_5
    invoke-virtual {v5}, Lcom/google/android/apps/chrome/services/ChildAccountManager;->hasChildAccount()Z

    move-result v6

    if-eqz v6, :cond_8

    .line 344
    iget-object v6, p0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->mAccountNotYouView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    sget v8, Lcom/google/android/apps/chrome/R$string;->account_management_custodian_settings:I

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 352
    :goto_3
    iget-object v6, p0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->mAccountsListLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v6}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 353
    iget-object v6, p0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->mAccountsListLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v6, v3}, Landroid/widget/LinearLayout;->setClickable(Z)V

    .line 354
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->mAccountsListLayout:Landroid/widget/LinearLayout;

    iget-object v8, p0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->mDefaultAccountBitmap:Landroid/graphics/Bitmap;

    iget v9, p0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->mGaiaServiceType:I

    invoke-static {v6, v7, v8, v1, v9}, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->fillAccountsList(Landroid/app/Activity;Landroid/widget/LinearLayout;Landroid/graphics/Bitmap;Ljava/lang/String;I)V

    .line 358
    invoke-virtual {v5}, Lcom/google/android/apps/chrome/services/ChildAccountManager;->hasChildAccount()Z

    move-result v1

    if-nez v1, :cond_6

    .line 360
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v6, Lcom/google/android/apps/chrome/R$string;->account_management_add_account_title:I

    new-array v7, v2, [Ljava/lang/Object;

    aput-object v0, v7, v3

    invoke-virtual {v1, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    .line 362
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    sget v6, Lcom/google/android/apps/chrome/R$layout;->account_management_list_item:I

    const/4 v7, 0x0

    invoke-static {v0, v6, v7}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    .line 364
    sget v0, Lcom/google/android/apps/chrome/R$id;->account_name:I

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 365
    sget v0, Lcom/google/android/apps/chrome/R$id;->display_image:I

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->mAddAccountBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 367
    sget v0, Lcom/google/android/apps/chrome/R$id;->account_summary:I

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v10}, Landroid/view/View;->setVisibility(I)V

    .line 368
    invoke-virtual {v6, v2}, Landroid/view/View;->setClickable(Z)V

    .line 369
    new-instance v0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment$4;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/signin/AccountManagementFragment$4;-><init>(Lcom/google/android/apps/chrome/signin/AccountManagementFragment;)V

    invoke-virtual {v6, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 386
    iget-object v0, p0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->mAccountsListLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 389
    :cond_6
    invoke-virtual {v5}, Lcom/google/android/apps/chrome/services/ChildAccountManager;->hasChildAccount()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 390
    iget-object v0, p0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->mGoIncognitoButtonEnabled:Landroid/view/View;

    invoke-virtual {v0, v10}, Landroid/view/View;->setVisibility(I)V

    .line 391
    iget-object v0, p0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->mGoIncognitoButtonDisabled:Landroid/view/View;

    invoke-virtual {v0, v10}, Landroid/view/View;->setVisibility(I)V

    .line 392
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 394
    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getSupervisedUserCustodianEmail()Ljava/lang/String;

    move-result-object v1

    .line 396
    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getSupervisedUserSecondCustodianEmail()Ljava/lang/String;

    move-result-object v4

    .line 401
    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_9

    .line 402
    sget v5, Lcom/google/android/apps/chrome/R$string;->account_management_two_custodian_names:I

    new-array v6, v11, [Ljava/lang/Object;

    aput-object v1, v6, v3

    aput-object v4, v6, v2

    invoke-virtual {v0, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 412
    :goto_4
    iget-object v0, p0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->mParentInformation:Landroid/widget/LinearLayout;

    sget v4, Lcom/google/android/apps/chrome/R$id;->account_custodian_accounts:I

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 415
    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getDefaultSupervisedUserFilteringBehavior()I

    move-result v0

    if-ne v0, v11, :cond_b

    move v0, v2

    .line 419
    :goto_5
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    if-eqz v0, :cond_c

    sget v0, Lcom/google/android/apps/chrome/R$string;->account_management_uca_content_approved:I

    :goto_6
    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 423
    iget-object v0, p0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->mParentInformation:Landroid/widget/LinearLayout;

    sget v2, Lcom/google/android/apps/chrome/R$id;->account_management_uca_content_text:I

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 426
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->isForceSafeSearch()Z

    move-result v0

    if-eqz v0, :cond_d

    sget v0, Lcom/google/android/apps/chrome/R$string;->text_on:I

    :goto_7
    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 430
    iget-object v0, p0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->mParentInformation:Landroid/widget/LinearLayout;

    sget v2, Lcom/google/android/apps/chrome/R$id;->account_management_uca_safe_search_text:I

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 433
    iget-object v0, p0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->mParentInformation:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_1

    .line 338
    :cond_7
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v6, Lcom/google/android/apps/chrome/R$string;->sign_in_to_chrome:I

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 347
    :cond_8
    iget-object v6, p0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->mAccountNotYouView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    sget v8, Lcom/google/android/apps/chrome/R$string;->account_management_not_you_text:I

    new-array v9, v2, [Ljava/lang/Object;

    aput-object v0, v9, v3

    invoke-virtual {v7, v8, v9}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    .line 404
    :cond_9
    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_a

    .line 405
    sget v4, Lcom/google/android/apps/chrome/R$string;->account_management_one_custodian_name:I

    new-array v5, v2, [Ljava/lang/Object;

    aput-object v1, v5, v3

    invoke-virtual {v0, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto/16 :goto_4

    .line 408
    :cond_a
    sget v1, Lcom/google/android/apps/chrome/R$string;->account_management_no_custodian_data:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto/16 :goto_4

    :cond_b
    move v0, v3

    .line 415
    goto :goto_5

    .line 419
    :cond_c
    sget v0, Lcom/google/android/apps/chrome/R$string;->account_management_uca_content_all:I

    goto :goto_6

    .line 426
    :cond_d
    sget v0, Lcom/google/android/apps/chrome/R$string;->text_off:I

    goto :goto_7

    .line 436
    :cond_e
    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->isIncognitoModeEnabled()Z

    move-result v1

    .line 438
    iget-object v2, p0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->mGoIncognitoButtonEnabled:Landroid/view/View;

    if-eqz v1, :cond_f

    move v0, v3

    :goto_8
    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    .line 440
    iget-object v0, p0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->mGoIncognitoButtonDisabled:Landroid/view/View;

    if-eqz v1, :cond_10

    :goto_9
    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_1

    :cond_f
    move v0, v4

    .line 438
    goto :goto_8

    :cond_10
    move v4, v3

    .line 440
    goto :goto_9
.end method
