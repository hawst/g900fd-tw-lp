.class public Lcom/google/android/apps/chrome/infobar/SnapshotDownloadingInfobar;
.super Lorg/chromium/chrome/browser/infobar/MessageInfoBar;
.source "SnapshotDownloadingInfobar.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private mDotCount:I

.field private mMessage:Ljava/lang/String;

.field private mTransparencySpan:Landroid/text/style/ForegroundColorSpan;

.field private final mUIHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Lorg/chromium/chrome/browser/infobar/InfoBarListeners$Dismiss;)V
    .locals 2

    .prologue
    .line 32
    sget v0, Lcom/google/android/apps/chrome/R$drawable;->infobar_downloading:I

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lorg/chromium/chrome/browser/infobar/MessageInfoBar;-><init>(Lorg/chromium/chrome/browser/infobar/InfoBarListeners$Dismiss;ILjava/lang/CharSequence;)V

    .line 33
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/infobar/SnapshotDownloadingInfobar;->setExpireOnNavigation(Z)V

    .line 34
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/infobar/SnapshotDownloadingInfobar;->mUIHandler:Landroid/os/Handler;

    .line 35
    return-void
.end method


# virtual methods
.method public createContent(Lorg/chromium/chrome/browser/infobar/InfoBarLayout;)V
    .locals 3

    .prologue
    .line 39
    invoke-super {p0, p1}, Lorg/chromium/chrome/browser/infobar/MessageInfoBar;->createContent(Lorg/chromium/chrome/browser/infobar/InfoBarLayout;)V

    .line 40
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/google/android/apps/chrome/R$string;->snapshot_downloading_infobar_text:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "..."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/infobar/SnapshotDownloadingInfobar;->mMessage:Ljava/lang/String;

    .line 42
    iget-object v0, p0, Lcom/google/android/apps/chrome/infobar/SnapshotDownloadingInfobar;->mMessage:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lorg/chromium/chrome/browser/infobar/InfoBarLayout;->setMessage(Ljava/lang/CharSequence;)V

    .line 43
    new-instance v0, Landroid/text/style/ForegroundColorSpan;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/infobar/SnapshotDownloadingInfobar;->mTransparencySpan:Landroid/text/style/ForegroundColorSpan;

    .line 44
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/infobar/SnapshotDownloadingInfobar;->run()V

    .line 45
    return-void
.end method

.method public run()V
    .locals 4

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/apps/chrome/infobar/SnapshotDownloadingInfobar;->mTransparencySpan:Landroid/text/style/ForegroundColorSpan;

    iget-object v1, p0, Lcom/google/android/apps/chrome/infobar/SnapshotDownloadingInfobar;->mMessage:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x3

    iget v2, p0, Lcom/google/android/apps/chrome/infobar/SnapshotDownloadingInfobar;->mDotCount:I

    add-int/2addr v1, v2

    iget-object v2, p0, Lcom/google/android/apps/chrome/infobar/SnapshotDownloadingInfobar;->mMessage:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v3, 0x0

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/google/android/apps/chrome/infobar/SnapshotDownloadingInfobar;->setSpan(Ljava/lang/Object;III)V

    .line 63
    iget v0, p0, Lcom/google/android/apps/chrome/infobar/SnapshotDownloadingInfobar;->mDotCount:I

    add-int/lit8 v0, v0, 0x1

    rem-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/android/apps/chrome/infobar/SnapshotDownloadingInfobar;->mDotCount:I

    .line 65
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/infobar/SnapshotDownloadingInfobar;->isDismissed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 66
    iget-object v0, p0, Lcom/google/android/apps/chrome/infobar/SnapshotDownloadingInfobar;->mUIHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, p0, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 68
    :cond_0
    return-void
.end method

.method protected setSpan(Ljava/lang/Object;III)V
    .locals 2

    .prologue
    .line 48
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/infobar/SnapshotDownloadingInfobar;->getContentWrapper(Z)Lorg/chromium/chrome/browser/infobar/ContentWrapperView;

    move-result-object v0

    .line 49
    if-eqz v0, :cond_0

    .line 50
    sget v1, Lcom/google/android/apps/chrome/R$id;->infobar_message:I

    invoke-virtual {v0, v1}, Lorg/chromium/chrome/browser/infobar/ContentWrapperView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 51
    if-eqz v0, :cond_0

    .line 52
    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Landroid/text/Spannable;

    invoke-interface {v0, p1, p2, p3, p4}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 55
    :cond_0
    return-void
.end method
