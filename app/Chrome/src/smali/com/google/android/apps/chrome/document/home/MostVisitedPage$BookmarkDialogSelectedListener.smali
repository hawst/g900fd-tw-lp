.class Lcom/google/android/apps/chrome/document/home/MostVisitedPage$BookmarkDialogSelectedListener;
.super Ljava/lang/Object;
.source "MostVisitedPage.java"

# interfaces
.implements Lcom/google/android/apps/chrome/ntp/BookmarksPage$BookmarkSelectedListener;


# instance fields
.field private final mActivity:Lcom/google/android/apps/chrome/document/DocumentActivity;

.field private mDialog:Landroid/app/Dialog;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/chrome/document/DocumentActivity;)V
    .locals 0

    .prologue
    .line 277
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 278
    iput-object p1, p0, Lcom/google/android/apps/chrome/document/home/MostVisitedPage$BookmarkDialogSelectedListener;->mActivity:Lcom/google/android/apps/chrome/document/DocumentActivity;

    .line 279
    return-void
.end method


# virtual methods
.method public onBookmarkSelected(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 2

    .prologue
    .line 288
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/home/MostVisitedPage$BookmarkDialogSelectedListener;->mDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/document/home/MostVisitedPage$BookmarkDialogSelectedListener;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 289
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/home/MostVisitedPage$BookmarkDialogSelectedListener;->mActivity:Lcom/google/android/apps/chrome/document/DocumentActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->getCurrentTab()Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    new-instance v1, Lorg/chromium/content_public/browser/LoadUrlParams;

    invoke-direct {v1, p1}, Lorg/chromium/content_public/browser/LoadUrlParams;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->loadUrl(Lorg/chromium/content_public/browser/LoadUrlParams;)I

    .line 290
    return-void
.end method

.method public onNewTabOpened()V
    .locals 1

    .prologue
    .line 283
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/home/MostVisitedPage$BookmarkDialogSelectedListener;->mDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/document/home/MostVisitedPage$BookmarkDialogSelectedListener;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 284
    :cond_0
    return-void
.end method

.method public setDialog(Landroid/app/Dialog;)V
    .locals 0

    .prologue
    .line 293
    iput-object p1, p0, Lcom/google/android/apps/chrome/document/home/MostVisitedPage$BookmarkDialogSelectedListener;->mDialog:Landroid/app/Dialog;

    .line 294
    return-void
.end method
