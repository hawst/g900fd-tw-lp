.class Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;
.super Ljava/lang/Object;
.source "TabStrip.java"

# interfaces
.implements Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabViewHandler;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)V
    .locals 0

    .prologue
    .line 1172
    iput-object p1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$1;)V
    .locals 0

    .prologue
    .line 1172
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;-><init>(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)V

    return-void
.end method


# virtual methods
.method public handleTabClosePress(Lcom/google/android/apps/chrome/widget/tabstrip/TabView;)V
    .locals 2

    .prologue
    .line 1184
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    # getter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScrollOffset:F
    invoke-static {v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$900(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)F

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    # getter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mMinScrollOffset:F
    invoke-static {v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$1000(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)F

    move-result v1

    cmpg-float v0, v0, v1

    if-ltz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    # getter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScrollOffset:F
    invoke-static {v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$900(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_1

    .line 1217
    :cond_0
    :goto_0
    return-void

    .line 1186
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    # getter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mKeyboardHider:Lcom/google/android/apps/chrome/KeyboardHider;
    invoke-static {v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$300(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)Lcom/google/android/apps/chrome/KeyboardHider;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl$1;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl$1;-><init>(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;Lcom/google/android/apps/chrome/widget/tabstrip/TabView;)V

    invoke-interface {v0, v1}, Lcom/google/android/apps/chrome/KeyboardHider;->hideKeyboard(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public handleTabPressDown(Lcom/google/android/apps/chrome/widget/tabstrip/TabView;)V
    .locals 2

    .prologue
    .line 1177
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    # setter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mPressedTabView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;
    invoke-static {v0, p1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$602(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;Lcom/google/android/apps/chrome/widget/tabstrip/TabView;)Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    .line 1178
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    # setter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mPotentialDragTabView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;
    invoke-static {v0, p1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$702(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;Lcom/google/android/apps/chrome/widget/tabstrip/TabView;)Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    .line 1179
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentlyScrollingForFastExpand:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$802(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;Z)Z

    .line 1180
    return-void
.end method

.method public handleTabSelectPress(Lcom/google/android/apps/chrome/widget/tabstrip/TabView;)V
    .locals 7

    .prologue
    const/high16 v6, 0x40000000    # 2.0f

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 1221
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    # getter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScrollOffset:F
    invoke-static {v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$900(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)F

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    # getter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mMinScrollOffset:F
    invoke-static {v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$1000(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)F

    move-result v1

    cmpg-float v0, v0, v1

    if-ltz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    # getter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScrollOffset:F
    invoke-static {v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$900(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)F

    move-result v0

    cmpl-float v0, v0, v5

    if-lez v0, :cond_1

    .line 1255
    :cond_0
    :goto_0
    return-void

    .line 1223
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->getTabVisibleAmount()F

    move-result v0

    const v1, 0x3e99999a    # 0.3f

    cmpl-float v0, v0, v1

    if-lez v0, :cond_2

    .line 1225
    const-string/jumbo v0, "TabStrip:TabSelected"

    invoke-static {v0}, Lorg/chromium/base/PerfTraceEvent;->begin(Ljava/lang/String;)V

    .line 1227
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->getTabId()I

    move-result v1

    # invokes: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->tabSelected(IZ)V
    invoke-static {v0, v1, v4}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$1400(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;IZ)V

    .line 1229
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    # getter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;
    invoke-static {v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$1200(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    # getter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;
    invoke-static {v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$1200(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    # getter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;
    invoke-static {v2}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$1200(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->getTabId()I

    move-result v3

    invoke-static {v2, v3}, Lorg/chromium/chrome/browser/tabmodel/TabModelUtils;->getTabById(Lorg/chromium/chrome/browser/tabmodel/TabList;I)Lorg/chromium/chrome/browser/Tab;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->indexOf(Lorg/chromium/chrome/browser/Tab;)I

    move-result v1

    invoke-static {v0, v1}, Lorg/chromium/chrome/browser/tabmodel/TabModelUtils;->setIndex(Lorg/chromium/chrome/browser/tabmodel/TabModel;I)V

    goto :goto_0

    .line 1232
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    # getter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;
    invoke-static {v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$1200(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    # getter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;
    invoke-static {v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$1200(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->getTabId()I

    move-result v2

    invoke-static {v1, v2}, Lorg/chromium/chrome/browser/tabmodel/TabModelUtils;->getTabById(Lorg/chromium/chrome/browser/tabmodel/TabList;I)Lorg/chromium/chrome/browser/Tab;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->indexOf(Lorg/chromium/chrome/browser/Tab;)I

    move-result v0

    .line 1234
    int-to-float v0, v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    # getter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentTabWidth:F
    invoke-static {v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$1700(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)F

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    # getter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabOverlap:F
    invoke-static {v2}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$1800(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)F

    move-result v2

    sub-float/2addr v1, v2

    mul-float/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    # getter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentTabWidth:F
    invoke-static {v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$1700(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)F

    move-result v1

    div-float/2addr v1, v6

    add-float/2addr v1, v0

    .line 1237
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->getWidth()I

    move-result v0

    int-to-float v0, v0

    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    # getter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mNewTabButtonSpace:F
    invoke-static {v2}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$1900(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)F

    move-result v2

    sub-float/2addr v0, v2

    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    # getter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentTabWidth:F
    invoke-static {v2}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$1700(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)F

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    # getter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabOverlap:F
    invoke-static {v3}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$1800(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)F

    move-result v3

    sub-float/2addr v2, v3

    div-float/2addr v0, v2

    float-to-int v0, v0

    .line 1240
    int-to-float v0, v0

    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    # getter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentTabWidth:F
    invoke-static {v2}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$1700(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)F

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    # getter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabOverlap:F
    invoke-static {v3}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$1800(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)F

    move-result v3

    sub-float/2addr v2, v3

    mul-float/2addr v0, v2

    .line 1242
    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    # getter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScrollOffset:F
    invoke-static {v2}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$900(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)F

    move-result v2

    add-float/2addr v1, v2

    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->getWidth()I

    move-result v2

    int-to-float v2, v2

    iget-object v3, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    # getter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mNewTabButtonSpace:F
    invoke-static {v3}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$1900(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)F

    move-result v3

    sub-float/2addr v2, v3

    div-float/2addr v2, v6

    cmpl-float v1, v1, v2

    if-lez v1, :cond_3

    .line 1244
    neg-float v0, v0

    .line 1249
    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    # getter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mMinScrollOffset:F
    invoke-static {v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$1000(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)F

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    # getter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScrollOffset:F
    invoke-static {v2}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$900(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)F

    move-result v2

    sub-float/2addr v1, v2

    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    # getter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScrollOffset:F
    invoke-static {v2}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$900(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)F

    move-result v2

    sub-float v2, v5, v2

    invoke-static {v0, v1, v2}, Lorg/chromium/chrome/browser/util/MathUtils;->clamp(FFF)F

    move-result v0

    .line 1252
    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    # getter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScroller:Lcom/google/android/apps/chrome/third_party/OverScroller;
    invoke-static {v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$2000(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)Lcom/google/android/apps/chrome/third_party/OverScroller;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    # getter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScrollOffset:F
    invoke-static {v2}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$900(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)F

    move-result v2

    float-to-int v2, v2

    float-to-int v0, v0

    invoke-virtual {v1, v2, v4, v0, v4}, Lcom/google/android/apps/chrome/third_party/OverScroller;->startScroll(IIII)V

    .line 1253
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    invoke-static {v0}, Lorg/chromium/base/ApiCompatibilityUtils;->postInvalidateOnAnimation(Landroid/view/View;)V

    goto/16 :goto_0
.end method
