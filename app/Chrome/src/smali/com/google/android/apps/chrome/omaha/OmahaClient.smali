.class public Lcom/google/android/apps/chrome/omaha/OmahaClient;
.super Landroid/app/IntentService;
.source "OmahaClient.java"


# static fields
.field static final synthetic $assertionsDisabled:Z

.field static final INSTALL_SOURCE_ORGANIC:Ljava/lang/String; = "organic"

.field static final INSTALL_SOURCE_SYSTEM:Ljava/lang/String; = "system_image"

.field static final INVALID_REQUEST_ID:Ljava/lang/String; = "invalid"

.field static final PREF_INSTALL_SOURCE:Ljava/lang/String; = "installSource"

.field static final PREF_LATEST_VERSION:Ljava/lang/String; = "latestVersion"

.field static final PREF_MARKET_URL:Ljava/lang/String; = "marketURL"

.field static final PREF_PACKAGE:Ljava/lang/String; = "com.google.android.apps.chrome.omaha"

.field static final PREF_PERSISTED_REQUEST_ID:Ljava/lang/String; = "persistedRequestID"

.field static final PREF_TIMESTAMP_OF_REQUEST:Ljava/lang/String; = "timestampOfRequest"

.field static final URL_OMAHA_SERVER:Ljava/lang/String; = "https://tools.google.com/service/update2"

.field private static sEnableCommunication:Z

.field private static sEnableUpdateDetection:Z

.field private static final sIsFreshInstallLock:Ljava/lang/Object;

.field private static sIsFreshInstallOrDataCleared:Ljava/lang/Boolean;

.field private static sMarketURLGetter:Lcom/google/android/apps/chrome/omaha/MarketURLGetter;

.field private static sVersionNumberGetter:Lcom/google/android/apps/chrome/omaha/VersionNumberGetter;


# instance fields
.field private mApplicationContext:Landroid/content/Context;

.field private mBackoffScheduler:Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;

.field private mCurrentRequest:Lcom/google/android/apps/chrome/omaha/RequestData;

.field private mGenerator:Lcom/google/android/apps/chrome/omaha/RequestGenerator;

.field private mInstallSource:Ljava/lang/String;

.field private mLatestVersion:Ljava/lang/String;

.field private mMarketURL:Ljava/lang/String;

.field protected mSendInstallEvent:Z

.field private mStateHasBeenRestored:Z

.field private mTimestampForNewRequest:J

.field private mTimestampForNextPostAttempt:J

.field private mTimestampOfInstall:J


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 61
    const-class v0, Lcom/google/android/apps/chrome/omaha/OmahaClient;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->$assertionsDisabled:Z

    .line 103
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->sIsFreshInstallLock:Ljava/lang/Object;

    .line 119
    sput-boolean v1, Lcom/google/android/apps/chrome/omaha/OmahaClient;->sEnableCommunication:Z

    .line 120
    sput-boolean v1, Lcom/google/android/apps/chrome/omaha/OmahaClient;->sEnableUpdateDetection:Z

    .line 121
    sput-object v2, Lcom/google/android/apps/chrome/omaha/OmahaClient;->sVersionNumberGetter:Lcom/google/android/apps/chrome/omaha/VersionNumberGetter;

    .line 122
    sput-object v2, Lcom/google/android/apps/chrome/omaha/OmahaClient;->sMarketURLGetter:Lcom/google/android/apps/chrome/omaha/MarketURLGetter;

    .line 123
    sput-object v2, Lcom/google/android/apps/chrome/omaha/OmahaClient;->sIsFreshInstallOrDataCleared:Ljava/lang/Boolean;

    return-void

    .line 61
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 142
    const-string/jumbo v0, "OmahaClient"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 143
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->setIntentRedelivery(Z)V

    .line 144
    return-void
.end method

.method private cancelRepeatingAlarm()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 398
    iget-object v0, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mApplicationContext:Landroid/content/Context;

    invoke-static {v0, v3}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->createRegisterRequestIntent(Landroid/content/Context;Z)Landroid/content/Intent;

    move-result-object v0

    .line 399
    iget-object v1, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mApplicationContext:Landroid/content/Context;

    const/high16 v2, 0x20000000

    invoke-static {v1, v3, v0, v2}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 404
    if-eqz v1, :cond_0

    .line 405
    iget-object v0, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mApplicationContext:Landroid/content/Context;

    const-string/jumbo v2, "alarm"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 407
    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 408
    invoke-virtual {v1}, Landroid/app/PendingIntent;->cancel()V

    .line 410
    :cond_0
    return-void
.end method

.method private checkServerResponseCode(Ljava/net/HttpURLConnection;)V
    .locals 3

    .prologue
    .line 589
    :try_start_0
    invoke-virtual {p1}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v0

    const/16 v1, 0xc8

    if-eq v0, v1, :cond_0

    .line 590
    new-instance v0, Lorg/chromium/chrome/browser/omaha/RequestFailureException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Received "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " code instead of 200 (OK) from the server.  Aborting."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/chromium/chrome/browser/omaha/RequestFailureException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 594
    :catch_0
    move-exception v0

    .line 595
    new-instance v1, Lorg/chromium/chrome/browser/omaha/RequestFailureException;

    const-string/jumbo v2, "Failed to read response code from server: "

    invoke-direct {v1, v2, v0}, Lorg/chromium/chrome/browser/omaha/RequestFailureException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 596
    :cond_0
    return-void
.end method

.method public static createInitializeIntent(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 235
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/chrome/omaha/OmahaClient;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 236
    const-string/jumbo v1, "com.google.android.apps.chrome.omaha.ACTION_INITIALIZE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 237
    return-object v0
.end method

.method public static createPostRequestIntent(Landroid/content/Context;Z)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 289
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/chrome/omaha/OmahaClient;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 290
    const-string/jumbo v1, "com.google.android.apps.chrome.omaha.ACTION_POST_REQUEST"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 291
    const-string/jumbo v1, "forceAction"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 292
    return-object v0
.end method

.method public static createRegisterRequestIntent(Landroid/content/Context;Z)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 254
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/chrome/omaha/OmahaClient;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 255
    const-string/jumbo v1, "com.google.android.apps.chrome.omaha.ACTION_REGISTER_REQUEST"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 256
    const-string/jumbo v1, "forceAction"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 257
    return-object v0
.end method

.method private createRequestData(JLjava/lang/String;)Lcom/google/android/apps/chrome/omaha/RequestData;
    .locals 7

    .prologue
    .line 448
    if-eqz p3, :cond_0

    const-string/jumbo v0, "invalid"

    invoke-virtual {v0, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 449
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->generateRandomUUID()Ljava/lang/String;

    move-result-object v4

    .line 453
    :goto_0
    new-instance v0, Lcom/google/android/apps/chrome/omaha/RequestData;

    iget-boolean v1, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mSendInstallEvent:Z

    iget-object v5, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mInstallSource:Ljava/lang/String;

    move-wide v2, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/chrome/omaha/RequestData;-><init>(ZJLjava/lang/String;Ljava/lang/String;)V

    return-object v0

    :cond_1
    move-object v4, p3

    .line 451
    goto :goto_0
.end method

.method private generateAndPostRequest(JLjava/lang/String;)Z
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 343
    :try_start_0
    iget-wide v2, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mTimestampOfInstall:J

    iget-object v1, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mCurrentRequest:Lcom/google/android/apps/chrome/omaha/RequestData;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/omaha/RequestData;->isSendInstallEvent()Z

    move-result v1

    invoke-static {p1, p2, v2, v3, v1}, Lcom/google/android/apps/chrome/omaha/RequestGenerator;->installAge(JJZ)J

    move-result-wide v4

    .line 345
    invoke-static {}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->getVersionNumberGetter()Lcom/google/android/apps/chrome/omaha/VersionNumberGetter;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mApplicationContext:Landroid/content/Context;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/chrome/omaha/VersionNumberGetter;->getCurrentlyUsedVersion(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    .line 347
    iget-object v1, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mGenerator:Lcom/google/android/apps/chrome/omaha/RequestGenerator;

    iget-object v6, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mCurrentRequest:Lcom/google/android/apps/chrome/omaha/RequestData;

    move-object v2, p3

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/apps/chrome/omaha/RequestGenerator;->generateXML(Ljava/lang/String;Ljava/lang/String;JLcom/google/android/apps/chrome/omaha/RequestData;)Ljava/lang/String;

    move-result-object v1

    .line 351
    invoke-virtual {p0, p1, p2, v1}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->postRequest(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 352
    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->parseServerResponse(Ljava/lang/String;)V

    .line 355
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mCurrentRequest:Lcom/google/android/apps/chrome/omaha/RequestData;

    .line 356
    const-wide/32 v2, 0x36ee80

    add-long/2addr v2, p1

    iput-wide v2, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mTimestampForNextPostAttempt:J

    .line 357
    iget-object v1, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mBackoffScheduler:Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;->resetFailedAttempts()V
    :try_end_0
    .catch Lorg/chromium/chrome/browser/omaha/RequestFailureException; {:try_start_0 .. :try_end_0} :catch_0

    .line 359
    const/4 v0, 0x1

    .line 366
    :goto_0
    return v0

    .line 360
    :catch_0
    move-exception v1

    .line 362
    const-string/jumbo v2, "OmahaClient"

    const-string/jumbo v3, "Failed to contact server: "

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 363
    iget-object v1, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mApplicationContext:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->createPostRequestIntent(Landroid/content/Context;Z)Landroid/content/Intent;

    move-result-object v1

    .line 364
    iget-object v2, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mBackoffScheduler:Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;

    invoke-virtual {v2, v1}, Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;->createAlarm(Landroid/content/Intent;)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mTimestampForNextPostAttempt:J

    .line 365
    iget-object v1, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mBackoffScheduler:Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;->increaseFailedAttempts()V

    goto :goto_0
.end method

.method private static getBooleanFromMap(Ljava/util/Map;Ljava/lang/String;Z)Z
    .locals 1

    .prologue
    .line 812
    invoke-interface {p0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 813
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    :cond_0
    return p2
.end method

.method private static getLongFromMap(Ljava/util/Map;Ljava/lang/String;J)J
    .locals 2

    .prologue
    .line 794
    invoke-interface {p0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 795
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide p2

    :cond_0
    return-wide p2
.end method

.method public static getMarketURLGetter()Lcom/google/android/apps/chrome/omaha/MarketURLGetter;
    .locals 1

    .prologue
    .line 784
    sget-object v0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->sMarketURLGetter:Lcom/google/android/apps/chrome/omaha/MarketURLGetter;

    if-nez v0, :cond_0

    .line 785
    new-instance v0, Lcom/google/android/apps/chrome/omaha/MarketURLGetter;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/omaha/MarketURLGetter;-><init>()V

    sput-object v0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->sMarketURLGetter:Lcom/google/android/apps/chrome/omaha/MarketURLGetter;

    .line 787
    :cond_0
    sget-object v0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->sMarketURLGetter:Lcom/google/android/apps/chrome/omaha/MarketURLGetter;

    return-object v0
.end method

.method private static getStringFromMap(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 803
    invoke-interface {p0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 804
    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    move-object v0, p2

    goto :goto_0
.end method

.method static getVersionNumberGetter()Lcom/google/android/apps/chrome/omaha/VersionNumberGetter;
    .locals 1

    .prologue
    .line 765
    sget-object v0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->sVersionNumberGetter:Lcom/google/android/apps/chrome/omaha/VersionNumberGetter;

    if-nez v0, :cond_0

    .line 766
    new-instance v0, Lcom/google/android/apps/chrome/omaha/VersionNumberGetter;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/omaha/VersionNumberGetter;-><init>()V

    sput-object v0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->sVersionNumberGetter:Lcom/google/android/apps/chrome/omaha/VersionNumberGetter;

    .line 768
    :cond_0
    sget-object v0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->sVersionNumberGetter:Lcom/google/android/apps/chrome/omaha/VersionNumberGetter;

    return-object v0
.end method

.method private handleInitialize()V
    .locals 2

    .prologue
    .line 244
    invoke-direct {p0}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->scheduleRepeatingAlarm()V

    .line 247
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->hasRequest()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 248
    iget-object v0, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mApplicationContext:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->createPostRequestIntent(Landroid/content/Context;Z)Landroid/content/Intent;

    move-result-object v0

    .line 249
    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 251
    :cond_0
    return-void
.end method

.method private handlePostRequestIntent(Landroid/content/Intent;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 300
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->hasRequest()Z

    move-result v0

    if-nez v0, :cond_0

    .line 338
    :goto_0
    return-void

    .line 304
    :cond_0
    const-string/jumbo v0, "forceAction"

    invoke-virtual {p1, v0, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 307
    iget-object v0, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mBackoffScheduler:Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;->getCurrentTime()J

    move-result-wide v2

    .line 308
    iget-wide v4, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mTimestampForNextPostAttempt:J

    cmp-long v0, v2, v4

    if-gez v0, :cond_1

    if-eqz v1, :cond_5

    .line 310
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->generateRandomUUID()Ljava/lang/String;

    move-result-object v4

    .line 311
    iget-boolean v5, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mSendInstallEvent:Z

    .line 312
    invoke-direct {p0, v2, v3, v4}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->generateAndPostRequest(JLjava/lang/String;)Z

    move-result v0

    .line 314
    if-eqz v0, :cond_2

    if-eqz v5, :cond_2

    .line 316
    iput-boolean v6, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mSendInstallEvent:Z

    .line 319
    invoke-virtual {p0, v2, v3}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->registerNewRequest(J)V

    .line 320
    invoke-direct {p0, v2, v3, v4}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->generateAndPostRequest(JLjava/lang/String;)Z

    move-result v0

    .line 323
    :cond_2
    if-eqz v1, :cond_3

    .line 324
    if-eqz v0, :cond_4

    .line 325
    const-string/jumbo v0, "OmahaClient"

    const-string/jumbo v1, "Requests successfully sent to Omaha server."

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 337
    :cond_3
    :goto_1
    invoke-direct {p0}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->saveState()V

    goto :goto_0

    .line 327
    :cond_4
    const-string/jumbo v0, "OmahaClient"

    const-string/jumbo v1, "Requests failed to reach Omaha server."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 332
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mApplicationContext:Landroid/content/Context;

    invoke-static {v0, v6}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->createPostRequestIntent(Landroid/content/Context;Z)Landroid/content/Intent;

    move-result-object v0

    .line 333
    iget-object v1, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mBackoffScheduler:Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;

    iget-wide v2, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mTimestampForNextPostAttempt:J

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;->createAlarm(Landroid/content/Intent;J)J

    goto :goto_1
.end method

.method private handleRegisterRequest(Landroid/content/Intent;)V
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 265
    const-string/jumbo v0, "forceAction"

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    .line 266
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->isChromeBeingUsed()Z

    move-result v0

    if-nez v0, :cond_1

    if-nez v3, :cond_1

    .line 267
    invoke-direct {p0}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->cancelRepeatingAlarm()V

    .line 286
    :cond_0
    :goto_0
    return-void

    .line 272
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mBackoffScheduler:Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;->getCurrentTime()J

    move-result-wide v4

    .line 273
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->hasRequest()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mCurrentRequest:Lcom/google/android/apps/chrome/omaha/RequestData;

    invoke-virtual {v0, v4, v5}, Lcom/google/android/apps/chrome/omaha/RequestData;->getAgeInMilliseconds(J)J

    move-result-wide v6

    const-wide/32 v8, 0x112a880

    cmp-long v0, v6, v8

    if-ltz v0, :cond_4

    move v0, v1

    .line 275
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->hasRequest()Z

    move-result v6

    if-nez v6, :cond_5

    iget-wide v6, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mTimestampForNewRequest:J

    cmp-long v6, v4, v6

    if-ltz v6, :cond_5

    .line 276
    :goto_2
    if-nez v0, :cond_2

    if-nez v1, :cond_2

    if-eqz v3, :cond_3

    .line 277
    :cond_2
    invoke-virtual {p0, v4, v5}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->registerNewRequest(J)V

    .line 282
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->hasRequest()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 283
    iget-object v0, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mApplicationContext:Landroid/content/Context;

    invoke-static {v0, v3}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->createPostRequestIntent(Landroid/content/Context;Z)Landroid/content/Intent;

    move-result-object v0

    .line 284
    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0

    :cond_4
    move v0, v2

    .line 273
    goto :goto_1

    :cond_5
    move v1, v2

    .line 275
    goto :goto_2
.end method

.method public static isFreshInstallOrDataHasBeenCleared(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 825
    invoke-static {p0}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->setIsFreshInstallOrDataHasBeenCleared(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method public static isNewerVersionAvailable(Landroid/content/Context;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 607
    sget-boolean v1, Lcom/google/android/apps/chrome/omaha/OmahaClient;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    if-ne v1, v2, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 610
    :cond_0
    sget-boolean v1, Lcom/google/android/apps/chrome/omaha/OmahaClient;->sEnableUpdateDetection:Z

    if-nez v1, :cond_2

    .line 632
    :cond_1
    :goto_0
    return v0

    .line 616
    :cond_2
    const-string/jumbo v1, ""

    invoke-static {}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->getMarketURLGetter()Lcom/google/android/apps/chrome/omaha/MarketURLGetter;

    move-result-object v2

    invoke-virtual {v2, p0}, Lcom/google/android/apps/chrome/omaha/MarketURLGetter;->getMarketURL(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 621
    invoke-static {}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->getVersionNumberGetter()Lcom/google/android/apps/chrome/omaha/VersionNumberGetter;

    move-result-object v1

    .line 622
    invoke-virtual {v1, p0}, Lcom/google/android/apps/chrome/omaha/VersionNumberGetter;->getCurrentlyUsedVersion(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 623
    invoke-virtual {v1, p0}, Lcom/google/android/apps/chrome/omaha/VersionNumberGetter;->getLatestKnownVersion(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 625
    invoke-static {v2}, Lcom/google/android/apps/chrome/omaha/VersionNumber;->fromString(Ljava/lang/String;)Lcom/google/android/apps/chrome/omaha/VersionNumber;

    move-result-object v2

    .line 626
    invoke-static {v1}, Lcom/google/android/apps/chrome/omaha/VersionNumber;->fromString(Ljava/lang/String;)Lcom/google/android/apps/chrome/omaha/VersionNumber;

    move-result-object v1

    .line 628
    if-eqz v2, :cond_1

    if-eqz v1, :cond_1

    .line 632
    invoke-virtual {v2, v1}, Lcom/google/android/apps/chrome/omaha/VersionNumber;->isSmallerThan(Lcom/google/android/apps/chrome/omaha/VersionNumber;)Z

    move-result v0

    goto :goto_0
.end method

.method private parseServerResponse(Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 489
    iget-object v0, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mGenerator:Lcom/google/android/apps/chrome/omaha/RequestGenerator;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omaha/RequestGenerator;->getAppId()Ljava/lang/String;

    move-result-object v1

    .line 490
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mSendInstallEvent:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 491
    :goto_0
    new-instance v2, Lorg/chromium/chrome/browser/omaha/ResponseParser;

    iget-boolean v3, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mSendInstallEvent:Z

    invoke-direct {v2, v1, v3, v0, v0}, Lorg/chromium/chrome/browser/omaha/ResponseParser;-><init>(Ljava/lang/String;ZZZ)V

    .line 493
    invoke-virtual {v2, p1}, Lorg/chromium/chrome/browser/omaha/ResponseParser;->parseResponse(Ljava/lang/String;)V

    .line 494
    iget-object v0, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mBackoffScheduler:Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;->getCurrentTime()J

    move-result-wide v0

    const-wide/32 v4, 0x112a880

    add-long/2addr v0, v4

    iput-wide v0, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mTimestampForNewRequest:J

    .line 495
    invoke-virtual {v2}, Lorg/chromium/chrome/browser/omaha/ResponseParser;->getNewVersion()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mLatestVersion:Ljava/lang/String;

    .line 496
    invoke-virtual {v2}, Lorg/chromium/chrome/browser/omaha/ResponseParser;->getURL()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mMarketURL:Ljava/lang/String;

    .line 497
    invoke-direct {p0}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->scheduleRepeatingAlarm()V

    .line 498
    return-void

    .line 490
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private readResponseFromServer(Ljava/net/HttpURLConnection;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 560
    const/4 v2, 0x0

    .line 562
    :try_start_0
    new-instance v0, Ljava/io/InputStreamReader;

    invoke-virtual {p1}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    .line 563
    new-instance v1, Ljava/io/BufferedReader;

    invoke-direct {v1, v0}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 564
    :try_start_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 565
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_0

    .line 566
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 565
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 568
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->checkServerResponseCode(Ljava/net/HttpURLConnection;)V

    .line 569
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 573
    :try_start_2
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 577
    return-object v0

    .line 576
    :catch_0
    move-exception v0

    .line 577
    new-instance v1, Lorg/chromium/chrome/browser/omaha/RequestFailureException;

    const-string/jumbo v2, "Failed to close stream: "

    invoke-direct {v1, v2, v0}, Lorg/chromium/chrome/browser/omaha/RequestFailureException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 570
    :catch_1
    move-exception v0

    move-object v1, v2

    .line 571
    :goto_1
    :try_start_3
    new-instance v2, Lorg/chromium/chrome/browser/omaha/RequestFailureException;

    const-string/jumbo v3, "Failed to read response from server: "

    invoke-direct {v2, v3, v0}, Lorg/chromium/chrome/browser/omaha/RequestFailureException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 573
    :catchall_0
    move-exception v0

    :goto_2
    if-eqz v1, :cond_1

    .line 575
    :try_start_4
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    .line 577
    :cond_1
    throw v0

    .line 576
    :catch_2
    move-exception v0

    .line 577
    new-instance v1, Lorg/chromium/chrome/browser/omaha/RequestFailureException;

    const-string/jumbo v2, "Failed to close stream: "

    invoke-direct {v1, v2, v0}, Lorg/chromium/chrome/browser/omaha/RequestFailureException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 573
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_2

    .line 570
    :catch_3
    move-exception v0

    goto :goto_1
.end method

.method private saveState()V
    .locals 6

    .prologue
    .line 726
    iget-object v0, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mApplicationContext:Landroid/content/Context;

    const-string/jumbo v1, "com.google.android.apps.chrome.omaha"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 728
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 729
    const-string/jumbo v0, "sendInstallEvent"

    iget-boolean v1, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mSendInstallEvent:Z

    invoke-interface {v2, v0, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 730
    iget-object v0, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mApplicationContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->setIsFreshInstallOrDataHasBeenCleared(Landroid/content/Context;)Z

    .line 731
    const-string/jumbo v0, "timestampOfInstall"

    iget-wide v4, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mTimestampOfInstall:J

    invoke-interface {v2, v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 732
    const-string/jumbo v0, "timestampForNextPostAttempt"

    iget-wide v4, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mTimestampForNextPostAttempt:J

    invoke-interface {v2, v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 733
    const-string/jumbo v0, "timestampForNewRequest"

    iget-wide v4, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mTimestampForNewRequest:J

    invoke-interface {v2, v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 734
    const-string/jumbo v3, "timestampOfRequest"

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->hasRequest()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mCurrentRequest:Lcom/google/android/apps/chrome/omaha/RequestData;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omaha/RequestData;->getCreationTimestamp()J

    move-result-wide v0

    :goto_0
    invoke-interface {v2, v3, v0, v1}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 736
    const-string/jumbo v1, "persistedRequestID"

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->hasRequest()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mCurrentRequest:Lcom/google/android/apps/chrome/omaha/RequestData;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omaha/RequestData;->getRequestID()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-interface {v2, v1, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 738
    const-string/jumbo v1, "latestVersion"

    iget-object v0, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mLatestVersion:Ljava/lang/String;

    if-nez v0, :cond_3

    const-string/jumbo v0, ""

    :goto_2
    invoke-interface {v2, v1, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 739
    const-string/jumbo v1, "marketURL"

    iget-object v0, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mMarketURL:Ljava/lang/String;

    if-nez v0, :cond_4

    const-string/jumbo v0, ""

    :goto_3
    invoke-interface {v2, v1, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 741
    iget-object v0, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mInstallSource:Ljava/lang/String;

    if-eqz v0, :cond_0

    const-string/jumbo v0, "installSource"

    iget-object v1, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mInstallSource:Ljava/lang/String;

    invoke-interface {v2, v0, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 743
    :cond_0
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 744
    return-void

    .line 734
    :cond_1
    const-wide/16 v0, -0x1

    goto :goto_0

    .line 736
    :cond_2
    const-string/jumbo v0, "invalid"

    goto :goto_1

    .line 738
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mLatestVersion:Ljava/lang/String;

    goto :goto_2

    .line 739
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mMarketURL:Ljava/lang/String;

    goto :goto_3
.end method

.method private scheduleRepeatingAlarm()V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 376
    iget-object v0, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mApplicationContext:Landroid/content/Context;

    invoke-static {v0, v2}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->createRegisterRequestIntent(Landroid/content/Context;Z)Landroid/content/Intent;

    move-result-object v0

    .line 377
    iget-object v1, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mApplicationContext:Landroid/content/Context;

    invoke-static {v1, v2, v0, v2}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 379
    iget-object v0, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mApplicationContext:Landroid/content/Context;

    const-string/jumbo v1, "alarm"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/AlarmManager;

    .line 381
    const/4 v3, 0x1

    iget-wide v4, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mTimestampForNewRequest:J

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->setAlarm(Landroid/app/AlarmManager;Landroid/app/PendingIntent;IJ)V

    .line 382
    return-void
.end method

.method private sendRequestToServer(Ljava/net/HttpURLConnection;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 545
    :try_start_0
    new-instance v0, Ljava/io/BufferedOutputStream;

    invoke-virtual {p1}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 546
    new-instance v1, Ljava/io/OutputStreamWriter;

    invoke-direct {v1, v0}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;)V

    .line 547
    const/4 v0, 0x0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v1, p2, v0, v2}, Ljava/io/OutputStreamWriter;->write(Ljava/lang/String;II)V

    .line 548
    invoke-virtual {v1}, Ljava/io/OutputStreamWriter;->close()V

    .line 549
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->checkServerResponseCode(Ljava/net/HttpURLConnection;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 552
    return-void

    .line 550
    :catch_0
    move-exception v0

    .line 551
    new-instance v1, Lorg/chromium/chrome/browser/omaha/RequestFailureException;

    const-string/jumbo v2, "Failed to write request to server: "

    invoke-direct {v1, v2, v0}, Lorg/chromium/chrome/browser/omaha/RequestFailureException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public static setEnableCommunication(Z)V
    .locals 0

    .prologue
    .line 163
    sput-boolean p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->sEnableCommunication:Z

    .line 164
    return-void
.end method

.method public static setEnableUpdateDetection(Z)V
    .locals 0

    .prologue
    .line 171
    sput-boolean p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->sEnableUpdateDetection:Z

    .line 172
    return-void
.end method

.method private static setIsFreshInstallOrDataHasBeenCleared(Landroid/content/Context;)Z
    .locals 8

    .prologue
    const-wide/16 v6, -0x1

    const/4 v0, 0x0

    .line 829
    sget-object v1, Lcom/google/android/apps/chrome/omaha/OmahaClient;->sIsFreshInstallLock:Ljava/lang/Object;

    monitor-enter v1

    .line 830
    :try_start_0
    sget-object v2, Lcom/google/android/apps/chrome/omaha/OmahaClient;->sIsFreshInstallOrDataCleared:Ljava/lang/Boolean;

    if-nez v2, :cond_1

    .line 831
    const-string/jumbo v2, "com.google.android.apps.chrome.omaha"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 833
    const-string/jumbo v3, "timestampOfInstall"

    const-wide/16 v4, -0x1

    invoke-interface {v2, v3, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    cmp-long v2, v2, v6

    if-nez v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->sIsFreshInstallOrDataCleared:Ljava/lang/Boolean;

    .line 835
    :cond_1
    sget-object v0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->sIsFreshInstallOrDataCleared:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    .line 836
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static setMarketURLGetterForTests(Lcom/google/android/apps/chrome/omaha/MarketURLGetter;)V
    .locals 0

    .prologue
    .line 777
    sput-object p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->sMarketURLGetter:Lcom/google/android/apps/chrome/omaha/MarketURLGetter;

    .line 778
    return-void
.end method

.method private setUpPostRequest(JLjava/net/HttpURLConnection;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 524
    const/4 v0, 0x1

    :try_start_0
    invoke-virtual {p3, v0}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V

    .line 525
    invoke-virtual {p4}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    array-length v0, v0

    invoke-virtual {p3, v0}, Ljava/net/HttpURLConnection;->setFixedLengthStreamingMode(I)V

    .line 526
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mSendInstallEvent:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->getCumulativeFailedAttempts()I

    move-result v0

    if-lez v0, :cond_0

    .line 527
    iget-object v0, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mCurrentRequest:Lcom/google/android/apps/chrome/omaha/RequestData;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/chrome/omaha/RequestData;->getAgeInSeconds(J)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    .line 528
    const-string/jumbo v1, "X-RequestAge"

    invoke-virtual {p3, v1, v0}, Ljava/net/HttpURLConnection;->addRequestProperty(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalAccessError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_2

    .line 536
    :cond_0
    return-void

    .line 530
    :catch_0
    move-exception v0

    .line 531
    new-instance v1, Lorg/chromium/chrome/browser/omaha/RequestFailureException;

    const-string/jumbo v2, "Caught an IllegalAccessError:"

    invoke-direct {v1, v2, v0}, Lorg/chromium/chrome/browser/omaha/RequestFailureException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 532
    :catch_1
    move-exception v0

    .line 533
    new-instance v1, Lorg/chromium/chrome/browser/omaha/RequestFailureException;

    const-string/jumbo v2, "Caught an IllegalArgumentException:"

    invoke-direct {v1, v2, v0}, Lorg/chromium/chrome/browser/omaha/RequestFailureException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 534
    :catch_2
    move-exception v0

    .line 535
    new-instance v1, Lorg/chromium/chrome/browser/omaha/RequestFailureException;

    const-string/jumbo v2, "Caught an IllegalStateException:"

    invoke-direct {v1, v2, v0}, Lorg/chromium/chrome/browser/omaha/RequestFailureException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method static setVersionNumberGetterForTests(Lcom/google/android/apps/chrome/omaha/VersionNumberGetter;)V
    .locals 0

    .prologue
    .line 760
    sput-object p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->sVersionNumberGetter:Lcom/google/android/apps/chrome/omaha/VersionNumberGetter;

    .line 761
    return-void
.end method


# virtual methods
.method createBackoffScheduler(Ljava/lang/String;Landroid/content/Context;JJ)Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;
    .locals 9

    .prologue
    .line 195
    new-instance v1, Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    move-wide v6, p5

    invoke-direct/range {v1 .. v7}, Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;-><init>(Ljava/lang/String;Landroid/content/Context;JJ)V

    return-object v1
.end method

.method protected createConnection()Ljava/net/HttpURLConnection;
    .locals 3

    .prologue
    .line 506
    :try_start_0
    new-instance v0, Ljava/net/URL;

    const-string/jumbo v1, "https://tools.google.com/service/update2"

    invoke-direct {v0, v1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 507
    invoke-virtual {v0}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    check-cast v0, Ljava/net/HttpURLConnection;

    .line 508
    const v1, 0xea60

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 509
    const v1, 0xea60

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 510
    return-object v0

    .line 511
    :catch_0
    move-exception v0

    .line 512
    new-instance v1, Lorg/chromium/chrome/browser/omaha/RequestFailureException;

    const-string/jumbo v2, "Caught a malformed URL exception."

    invoke-direct {v1, v2, v0}, Lorg/chromium/chrome/browser/omaha/RequestFailureException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 513
    :catch_1
    move-exception v0

    .line 514
    new-instance v1, Lorg/chromium/chrome/browser/omaha/RequestFailureException;

    const-string/jumbo v2, "Failed to open connection to URL"

    invoke-direct {v1, v2, v0}, Lorg/chromium/chrome/browser/omaha/RequestFailureException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method createRequestGenerator(Landroid/content/Context;)Lcom/google/android/apps/chrome/omaha/RequestGenerator;
    .locals 1

    .prologue
    .line 203
    new-instance v0, Lcom/google/android/apps/chrome/omaha/RequestGenerator;

    invoke-direct {v0, p1}, Lcom/google/android/apps/chrome/omaha/RequestGenerator;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method determineInstallSource(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 641
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->getApplicationFlags()I

    move-result v0

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 642
    :goto_0
    if-eqz v0, :cond_1

    const-string/jumbo v0, "system_image"

    :goto_1
    return-object v0

    .line 641
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 642
    :cond_1
    const-string/jumbo v0, "organic"

    goto :goto_1
.end method

.method protected generateRandomUUID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 751
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getApplicationFlags()I
    .locals 1

    .prologue
    .line 651
    iget-object v0, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mApplicationContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    iget v0, v0, Landroid/content/pm/ApplicationInfo;->flags:I

    return v0
.end method

.method getCumulativeFailedAttempts()I
    .locals 1

    .prologue
    .line 186
    iget-object v0, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mBackoffScheduler:Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;->getNumFailedAttempts()I

    move-result v0

    return v0
.end method

.method getTimestampForNewRequest()J
    .locals 2

    .prologue
    .line 181
    iget-wide v0, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mTimestampForNewRequest:J

    return-wide v0
.end method

.method getTimestampForNextPostAttempt()J
    .locals 2

    .prologue
    .line 176
    iget-wide v0, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mTimestampForNextPostAttempt:J

    return-wide v0
.end method

.method hasRequest()Z
    .locals 1

    .prologue
    .line 458
    iget-object v0, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mCurrentRequest:Lcom/google/android/apps/chrome/omaha/RequestData;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected isChromeBeingUsed()Z
    .locals 3

    .prologue
    .line 417
    invoke-static {}, Lorg/chromium/base/ApplicationStatus;->hasVisibleActivities()Z

    move-result v1

    .line 419
    iget-object v0, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mApplicationContext:Landroid/content/Context;

    const-string/jumbo v2, "power"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 421
    invoke-static {v0}, Lcom/google/android/apps/chrome/utilities/UnreleasedApiCompatibilityUtils;->isInteractive(Landroid/os/PowerManager;)Z

    move-result v0

    .line 423
    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCreate()V
    .locals 8

    .prologue
    .line 148
    invoke-super {p0}, Landroid/app/IntentService;->onCreate()V

    .line 149
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mApplicationContext:Landroid/content/Context;

    .line 150
    const-string/jumbo v2, "com.google.android.apps.chrome.omaha"

    iget-object v3, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mApplicationContext:Landroid/content/Context;

    const-wide/32 v4, 0x36ee80

    const-wide/32 v6, 0x112a880

    move-object v1, p0

    invoke-virtual/range {v1 .. v7}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->createBackoffScheduler(Ljava/lang/String;Landroid/content/Context;JJ)Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mBackoffScheduler:Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;

    .line 152
    iget-object v0, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mApplicationContext:Landroid/content/Context;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->createRequestGenerator(Landroid/content/Context;)Lcom/google/android/apps/chrome/omaha/RequestGenerator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mGenerator:Lcom/google/android/apps/chrome/omaha/RequestGenerator;

    .line 153
    return-void
.end method

.method public onHandleIntent(Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 212
    sget-boolean v0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne v0, v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 214
    :cond_0
    sget-boolean v0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->sEnableCommunication:Z

    if-nez v0, :cond_1

    .line 215
    const-string/jumbo v0, "OmahaClient"

    const-string/jumbo v1, "Disabled.  Ignoring intent."

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 232
    :goto_0
    return-void

    .line 219
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mStateHasBeenRestored:Z

    if-nez v0, :cond_2

    .line 220
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->restoreState()V

    .line 223
    :cond_2
    const-string/jumbo v0, "com.google.android.apps.chrome.omaha.ACTION_INITIALIZE"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 224
    invoke-direct {p0}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->handleInitialize()V

    goto :goto_0

    .line 225
    :cond_3
    const-string/jumbo v0, "com.google.android.apps.chrome.omaha.ACTION_REGISTER_REQUEST"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 226
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->handleRegisterRequest(Landroid/content/Intent;)V

    goto :goto_0

    .line 227
    :cond_4
    const-string/jumbo v0, "com.google.android.apps.chrome.omaha.ACTION_POST_REQUEST"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 228
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->handlePostRequestIntent(Landroid/content/Intent;)V

    goto :goto_0

    .line 230
    :cond_5
    const-string/jumbo v0, "OmahaClient"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Got unknown action from intent: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method postRequest(JLjava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 468
    const/4 v1, 0x0

    .line 472
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->createConnection()Ljava/net/HttpURLConnection;

    move-result-object v1

    .line 473
    invoke-direct {p0, p1, p2, v1, p3}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->setUpPostRequest(JLjava/net/HttpURLConnection;Ljava/lang/String;)V

    .line 474
    invoke-direct {p0, v1, p3}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->sendRequestToServer(Ljava/net/HttpURLConnection;Ljava/lang/String;)V

    .line 475
    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->readResponseFromServer(Ljava/net/HttpURLConnection;)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 477
    if-eqz v1, :cond_0

    .line 478
    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 482
    :cond_0
    return-object v0

    .line 477
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_1

    .line 478
    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_1
    throw v0
.end method

.method registerNewRequest(J)V
    .locals 3

    .prologue
    .line 433
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->createRequestData(JLjava/lang/String;)Lcom/google/android/apps/chrome/omaha/RequestData;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mCurrentRequest:Lcom/google/android/apps/chrome/omaha/RequestData;

    .line 434
    iget-object v0, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mBackoffScheduler:Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;->resetFailedAttempts()V

    .line 435
    iput-wide p1, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mTimestampForNextPostAttempt:J

    .line 439
    const-wide/32 v0, 0x112a880

    add-long/2addr v0, p1

    iput-wide v0, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mTimestampForNewRequest:J

    .line 440
    invoke-direct {p0}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->scheduleRepeatingAlarm()V

    .line 442
    invoke-direct {p0}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->saveState()V

    .line 443
    return-void
.end method

.method restoreState()V
    .locals 12

    .prologue
    const-wide/16 v10, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 660
    .line 661
    iget-object v0, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mApplicationContext:Landroid/content/Context;

    const-string/jumbo v3, "com.google.android.apps.chrome.omaha"

    invoke-virtual {v0, v3, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 663
    invoke-interface {v0}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v3

    .line 666
    iget-object v0, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mBackoffScheduler:Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;->getCurrentTime()J

    move-result-wide v4

    .line 667
    const-string/jumbo v0, "timestampForNewRequest"

    invoke-static {v3, v0, v4, v5}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->getLongFromMap(Ljava/util/Map;Ljava/lang/String;J)J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mTimestampForNewRequest:J

    .line 669
    const-string/jumbo v0, "timestampForNextPostAttempt"

    invoke-static {v3, v0, v4, v5}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->getLongFromMap(Ljava/util/Map;Ljava/lang/String;J)J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mTimestampForNextPostAttempt:J

    .line 672
    const-string/jumbo v0, "timestampOfRequest"

    invoke-static {v3, v0, v10, v11}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->getLongFromMap(Ljava/util/Map;Ljava/lang/String;J)J

    move-result-wide v6

    .line 675
    const-string/jumbo v0, "sendInstallEvent"

    invoke-static {v3, v0, v1}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->getBooleanFromMap(Ljava/util/Map;Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mSendInstallEvent:Z

    .line 678
    iget-object v0, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mApplicationContext:Landroid/content/Context;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->determineInstallSource(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 679
    const-string/jumbo v8, "installSource"

    invoke-static {v3, v8, v0}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->getStringFromMap(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mInstallSource:Ljava/lang/String;

    .line 683
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mSendInstallEvent:Z

    if-eqz v0, :cond_2

    const-string/jumbo v0, "persistedRequestID"

    const-string/jumbo v8, "invalid"

    invoke-static {v3, v0, v8}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->getStringFromMap(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 687
    :goto_0
    cmp-long v8, v6, v10

    if-nez v8, :cond_3

    const/4 v0, 0x0

    :goto_1
    iput-object v0, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mCurrentRequest:Lcom/google/android/apps/chrome/omaha/RequestData;

    .line 690
    const-string/jumbo v0, "latestVersion"

    const-string/jumbo v6, ""

    invoke-static {v3, v0, v6}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->getStringFromMap(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mLatestVersion:Ljava/lang/String;

    .line 691
    const-string/jumbo v0, "marketURL"

    const-string/jumbo v6, ""

    invoke-static {v3, v0, v6}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->getStringFromMap(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mMarketURL:Ljava/lang/String;

    .line 694
    const-string/jumbo v0, "timestampOfInstall"

    invoke-static {v3, v0, v4, v5}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->getLongFromMap(Ljava/util/Map;Ljava/lang/String;J)J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mTimestampOfInstall:J

    .line 697
    iget-wide v6, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mTimestampForNewRequest:J

    sub-long/2addr v6, v4

    .line 698
    const-wide/32 v8, 0x112a880

    cmp-long v0, v6, v8

    if-lez v0, :cond_4

    .line 699
    const-string/jumbo v0, "OmahaClient"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Delay to next request ("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ") is longer than expected.  Resetting to now."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 701
    iput-wide v4, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mTimestampForNewRequest:J

    move v0, v1

    .line 706
    :goto_2
    iget-wide v2, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mTimestampForNextPostAttempt:J

    sub-long/2addr v2, v4

    .line 707
    iget-object v6, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mBackoffScheduler:Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;

    invoke-virtual {v6}, Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;->getGeneratedDelay()J

    move-result-wide v6

    cmp-long v6, v2, v6

    if-lez v6, :cond_0

    .line 708
    const-string/jumbo v0, "OmahaClient"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "Delay to next post attempt ("

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ") is greater than expected ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mBackoffScheduler:Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/utilities/ExponentialBackoffScheduler;->getGeneratedDelay()J

    move-result-wide v6

    invoke-virtual {v2, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ").  Resetting to now."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 711
    iput-wide v4, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mTimestampForNextPostAttempt:J

    move v0, v1

    .line 715
    :cond_0
    if-eqz v0, :cond_1

    .line 716
    invoke-direct {p0}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->saveState()V

    .line 719
    :cond_1
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/omaha/OmahaClient;->mStateHasBeenRestored:Z

    .line 720
    return-void

    .line 683
    :cond_2
    const-string/jumbo v0, "invalid"

    goto/16 :goto_0

    .line 687
    :cond_3
    invoke-direct {p0, v6, v7, v0}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->createRequestData(JLjava/lang/String;)Lcom/google/android/apps/chrome/omaha/RequestData;

    move-result-object v0

    goto/16 :goto_1

    :cond_4
    move v0, v2

    goto :goto_2
.end method

.method protected setAlarm(Landroid/app/AlarmManager;Landroid/app/PendingIntent;IJ)V
    .locals 8

    .prologue
    .line 391
    const/4 v1, 0x1

    const-wide/32 v4, 0x112a880

    move-object v0, p1

    move-wide v2, p4

    move-object v6, p2

    invoke-virtual/range {v0 .. v6}, Landroid/app/AlarmManager;->setRepeating(IJJLandroid/app/PendingIntent;)V

    .line 392
    return-void
.end method
