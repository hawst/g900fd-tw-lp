.class public Lcom/google/android/apps/chrome/utilities/HttpClientWrapperImpl;
.super Ljava/lang/Object;
.source "HttpClientWrapperImpl.java"

# interfaces
.implements Lcom/google/android/apps/chrome/utilities/HttpClientWrapper;


# instance fields
.field private final mHttpClient:Landroid/net/http/AndroidHttpClient;


# direct methods
.method public constructor <init>(Ljava/lang/String;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    invoke-static {p1, p2}, Landroid/net/http/AndroidHttpClient;->newInstance(Ljava/lang/String;Landroid/content/Context;)Landroid/net/http/AndroidHttpClient;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/utilities/HttpClientWrapperImpl;->mHttpClient:Landroid/net/http/AndroidHttpClient;

    .line 21
    return-void
.end method

.method public static newInstance(Ljava/lang/String;Landroid/content/Context;)Lcom/google/android/apps/chrome/utilities/HttpClientWrapperImpl;
    .locals 1

    .prologue
    .line 24
    new-instance v0, Lcom/google/android/apps/chrome/utilities/HttpClientWrapperImpl;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/chrome/utilities/HttpClientWrapperImpl;-><init>(Ljava/lang/String;Landroid/content/Context;)V

    return-object v0
.end method


# virtual methods
.method public close()V
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/apps/chrome/utilities/HttpClientWrapperImpl;->mHttpClient:Landroid/net/http/AndroidHttpClient;

    invoke-virtual {v0}, Landroid/net/http/AndroidHttpClient;->close()V

    .line 35
    return-void
.end method

.method public execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/apps/chrome/utilities/HttpClientWrapperImpl;->mHttpClient:Landroid/net/http/AndroidHttpClient;

    invoke-virtual {v0, p1}, Landroid/net/http/AndroidHttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    return-object v0
.end method
