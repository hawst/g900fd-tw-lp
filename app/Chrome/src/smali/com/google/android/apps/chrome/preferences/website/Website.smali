.class public Lcom/google/android/apps/chrome/preferences/website/Website;
.super Ljava/lang/Object;
.source "Website.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field static final CAMERA_ACCESS_ALLOWED:I = 0x1

.field static final CAMERA_ACCESS_DENIED:I = 0x4

.field static final INVALID_CAMERA_OR_MICROPHONE_ACCESS:I = 0x0

.field static final MICROPHONE_ACCESS_ALLOWED:I = 0x3

.field static final MICROPHONE_ACCESS_DENIED:I = 0x6

.field static final MICROPHONE_AND_CAMERA_ACCESS_ALLOWED:I = 0x2

.field static final MICROPHONE_AND_CAMERA_ACCESS_DENIED:I = 0x5


# instance fields
.field private final mAddress:Lcom/google/android/apps/chrome/preferences/website/WebsiteAddress;

.field private mGeolocationInfo:Lcom/google/android/apps/chrome/preferences/website/GeolocationInfo;

.field private mLocalStorageInfo:Lcom/google/android/apps/chrome/preferences/website/LocalStorageInfo;

.field private mMidiInfo:Lcom/google/android/apps/chrome/preferences/website/MidiInfo;

.field private mPopupExceptionInfo:Lcom/google/android/apps/chrome/preferences/website/PopupExceptionInfo;

.field private mProtectedMediaIdentifierInfo:Lcom/google/android/apps/chrome/preferences/website/ProtectedMediaIdentifierInfo;

.field private final mStorageInfo:Ljava/util/List;

.field private mStorageInfoCallbacksLeft:I

.field private mSummary:Ljava/lang/String;

.field private final mTitle:Ljava/lang/String;

.field private mVoiceAndVideoCaptureInfo:Lcom/google/android/apps/chrome/preferences/website/VoiceAndVideoCaptureInfo;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/chrome/preferences/website/WebsiteAddress;)V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/Website;->mStorageInfo:Ljava/util/List;

    .line 37
    iput-object p1, p0, Lcom/google/android/apps/chrome/preferences/website/Website;->mAddress:Lcom/google/android/apps/chrome/preferences/website/WebsiteAddress;

    .line 38
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/preferences/website/WebsiteAddress;->getTitle()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/Website;->mTitle:Ljava/lang/String;

    .line 39
    return-void
.end method

.method static synthetic access$006(Lcom/google/android/apps/chrome/preferences/website/Website;)I
    .locals 1

    .prologue
    .line 14
    iget v0, p0, Lcom/google/android/apps/chrome/preferences/website/Website;->mStorageInfoCallbacksLeft:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/apps/chrome/preferences/website/Website;->mStorageInfoCallbacksLeft:I

    return v0
.end method


# virtual methods
.method public addStorageInfo(Lcom/google/android/apps/chrome/preferences/website/StorageInfo;)V
    .locals 1

    .prologue
    .line 218
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/Website;->mStorageInfo:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 219
    return-void
.end method

.method public clearAllStoredData(Lcom/google/android/apps/chrome/preferences/website/Website$StoredDataClearedCallback;)V
    .locals 3

    .prologue
    .line 222
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/Website;->mLocalStorageInfo:Lcom/google/android/apps/chrome/preferences/website/LocalStorageInfo;

    if-eqz v0, :cond_0

    .line 223
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/Website;->mLocalStorageInfo:Lcom/google/android/apps/chrome/preferences/website/LocalStorageInfo;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/website/LocalStorageInfo;->clear()V

    .line 224
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/Website;->mLocalStorageInfo:Lcom/google/android/apps/chrome/preferences/website/LocalStorageInfo;

    .line 226
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/Website;->mStorageInfo:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/preferences/website/Website;->mStorageInfoCallbacksLeft:I

    .line 227
    iget v0, p0, Lcom/google/android/apps/chrome/preferences/website/Website;->mStorageInfoCallbacksLeft:I

    if-lez v0, :cond_2

    .line 228
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/Website;->mStorageInfo:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/preferences/website/StorageInfo;

    .line 229
    new-instance v2, Lcom/google/android/apps/chrome/preferences/website/Website$1;

    invoke-direct {v2, p0, p1}, Lcom/google/android/apps/chrome/preferences/website/Website$1;-><init>(Lcom/google/android/apps/chrome/preferences/website/Website;Lcom/google/android/apps/chrome/preferences/website/Website$StoredDataClearedCallback;)V

    invoke-virtual {v0, v2}, Lcom/google/android/apps/chrome/preferences/website/StorageInfo;->clear(Lcom/google/android/apps/chrome/preferences/website/WebsitePreferenceBridge$StorageInfoClearedCallback;)V

    goto :goto_0

    .line 238
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/Website;->mStorageInfo:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 241
    :goto_1
    return-void

    .line 240
    :cond_2
    invoke-interface {p1}, Lcom/google/android/apps/chrome/preferences/website/Website$StoredDataClearedCallback;->onStoredDataCleared()V

    goto :goto_1
.end method

.method public compareByAddressTo(Lcom/google/android/apps/chrome/preferences/website/Website;)I
    .locals 2

    .prologue
    .line 50
    if-ne p0, p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/Website;->mAddress:Lcom/google/android/apps/chrome/preferences/website/WebsiteAddress;

    iget-object v1, p1, Lcom/google/android/apps/chrome/preferences/website/Website;->mAddress:Lcom/google/android/apps/chrome/preferences/website/WebsiteAddress;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/preferences/website/WebsiteAddress;->compareTo(Lcom/google/android/apps/chrome/preferences/website/WebsiteAddress;)I

    move-result v0

    goto :goto_0
.end method

.method public getMediaAccessType()I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 183
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/website/Website;->isVoiceCaptureAllowed()Ljava/lang/Boolean;

    move-result-object v1

    .line 184
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/website/Website;->isVideoCaptureAllowed()Ljava/lang/Boolean;

    move-result-object v2

    .line 185
    if-eqz v2, :cond_4

    .line 186
    if-nez v1, :cond_2

    .line 187
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v0, v2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 188
    const/4 v0, 0x1

    .line 208
    :cond_0
    :goto_0
    return v0

    .line 190
    :cond_1
    const/4 v0, 0x4

    goto :goto_0

    .line 193
    :cond_2
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-ne v3, v4, :cond_0

    .line 196
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v0, v2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 198
    const/4 v0, 0x2

    goto :goto_0

    .line 200
    :cond_3
    const/4 v0, 0x5

    goto :goto_0

    .line 204
    :cond_4
    if-eqz v1, :cond_0

    .line 205
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 206
    const/4 v0, 0x3

    goto :goto_0

    .line 208
    :cond_5
    const/4 v0, 0x6

    goto :goto_0
.end method

.method public getSummary()Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/Website;->mSummary:Ljava/lang/String;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/Website;->mTitle:Ljava/lang/String;

    return-object v0
.end method

.method public getTotalUsage()J
    .locals 5

    .prologue
    const-wide/16 v0, 0x0

    .line 251
    .line 252
    iget-object v2, p0, Lcom/google/android/apps/chrome/preferences/website/Website;->mLocalStorageInfo:Lcom/google/android/apps/chrome/preferences/website/LocalStorageInfo;

    if-eqz v2, :cond_0

    .line 253
    iget-object v2, p0, Lcom/google/android/apps/chrome/preferences/website/Website;->mLocalStorageInfo:Lcom/google/android/apps/chrome/preferences/website/LocalStorageInfo;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/preferences/website/LocalStorageInfo;->getSize()J

    move-result-wide v2

    add-long/2addr v0, v2

    .line 255
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/chrome/preferences/website/Website;->mStorageInfo:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-wide v2, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/preferences/website/StorageInfo;

    .line 256
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/website/StorageInfo;->getSize()J

    move-result-wide v0

    add-long/2addr v0, v2

    move-wide v2, v0

    .line 257
    goto :goto_0

    .line 258
    :cond_1
    return-wide v2
.end method

.method public isGeolocationAccessAllowed()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/Website;->mGeolocationInfo:Lcom/google/android/apps/chrome/preferences/website/GeolocationInfo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/Website;->mGeolocationInfo:Lcom/google/android/apps/chrome/preferences/website/GeolocationInfo;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/website/GeolocationInfo;->getAllowed()Ljava/lang/Boolean;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isMidiAccessAllowed()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/Website;->mMidiInfo:Lcom/google/android/apps/chrome/preferences/website/MidiInfo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/Website;->mMidiInfo:Lcom/google/android/apps/chrome/preferences/website/MidiInfo;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/website/MidiInfo;->getAllowed()Ljava/lang/Boolean;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPopupExceptionAllowed()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/Website;->mPopupExceptionInfo:Lcom/google/android/apps/chrome/preferences/website/PopupExceptionInfo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/Website;->mPopupExceptionInfo:Lcom/google/android/apps/chrome/preferences/website/PopupExceptionInfo;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/website/PopupExceptionInfo;->getAllowed()Ljava/lang/Boolean;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isProtectedMediaIdentifierAccessAllowed()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/Website;->mProtectedMediaIdentifierInfo:Lcom/google/android/apps/chrome/preferences/website/ProtectedMediaIdentifierInfo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/Website;->mProtectedMediaIdentifierInfo:Lcom/google/android/apps/chrome/preferences/website/ProtectedMediaIdentifierInfo;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/website/ProtectedMediaIdentifierInfo;->getAllowed()Ljava/lang/Boolean;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isVideoCaptureAllowed()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/Website;->mVoiceAndVideoCaptureInfo:Lcom/google/android/apps/chrome/preferences/website/VoiceAndVideoCaptureInfo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/Website;->mVoiceAndVideoCaptureInfo:Lcom/google/android/apps/chrome/preferences/website/VoiceAndVideoCaptureInfo;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/website/VoiceAndVideoCaptureInfo;->isVideoCaptureAllowed()Ljava/lang/Boolean;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isVoiceCaptureAllowed()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/Website;->mVoiceAndVideoCaptureInfo:Lcom/google/android/apps/chrome/preferences/website/VoiceAndVideoCaptureInfo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/Website;->mVoiceAndVideoCaptureInfo:Lcom/google/android/apps/chrome/preferences/website/VoiceAndVideoCaptureInfo;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/website/VoiceAndVideoCaptureInfo;->isVoiceCaptureAllowed()Ljava/lang/Boolean;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setGeolocationInfo(Lcom/google/android/apps/chrome/preferences/website/GeolocationInfo;)V
    .locals 1

    .prologue
    .line 58
    iput-object p1, p0, Lcom/google/android/apps/chrome/preferences/website/Website;->mGeolocationInfo:Lcom/google/android/apps/chrome/preferences/website/GeolocationInfo;

    .line 59
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/preferences/website/GeolocationInfo;->getEmbedder()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/preferences/website/WebsiteAddress;->create(Ljava/lang/String;)Lcom/google/android/apps/chrome/preferences/website/WebsiteAddress;

    move-result-object v0

    .line 60
    if-eqz v0, :cond_0

    .line 61
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/website/WebsiteAddress;->getTitle()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/Website;->mSummary:Ljava/lang/String;

    .line 63
    :cond_0
    return-void
.end method

.method public setGeolocationInfoAllowed(Ljava/lang/Boolean;)V
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/Website;->mGeolocationInfo:Lcom/google/android/apps/chrome/preferences/website/GeolocationInfo;

    if-eqz v0, :cond_0

    .line 67
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/Website;->mGeolocationInfo:Lcom/google/android/apps/chrome/preferences/website/GeolocationInfo;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/preferences/website/GeolocationInfo;->setAllowed(Ljava/lang/Boolean;)V

    .line 69
    :cond_0
    return-void
.end method

.method public setLocalStorageInfo(Lcom/google/android/apps/chrome/preferences/website/LocalStorageInfo;)V
    .locals 0

    .prologue
    .line 214
    iput-object p1, p0, Lcom/google/android/apps/chrome/preferences/website/Website;->mLocalStorageInfo:Lcom/google/android/apps/chrome/preferences/website/LocalStorageInfo;

    .line 215
    return-void
.end method

.method public setMidiInfo(Lcom/google/android/apps/chrome/preferences/website/MidiInfo;)V
    .locals 1

    .prologue
    .line 76
    iput-object p1, p0, Lcom/google/android/apps/chrome/preferences/website/Website;->mMidiInfo:Lcom/google/android/apps/chrome/preferences/website/MidiInfo;

    .line 77
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/preferences/website/MidiInfo;->getEmbedder()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/preferences/website/WebsiteAddress;->create(Ljava/lang/String;)Lcom/google/android/apps/chrome/preferences/website/WebsiteAddress;

    move-result-object v0

    .line 78
    if-eqz v0, :cond_0

    .line 79
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/website/WebsiteAddress;->getTitle()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/Website;->mSummary:Ljava/lang/String;

    .line 81
    :cond_0
    return-void
.end method

.method public setMidiInfoAllowed(Ljava/lang/Boolean;)V
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/Website;->mMidiInfo:Lcom/google/android/apps/chrome/preferences/website/MidiInfo;

    if-eqz v0, :cond_0

    .line 85
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/Website;->mMidiInfo:Lcom/google/android/apps/chrome/preferences/website/MidiInfo;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/preferences/website/MidiInfo;->setAllowed(Ljava/lang/Boolean;)V

    .line 87
    :cond_0
    return-void
.end method

.method public setPopupExceptionInfo(Lcom/google/android/apps/chrome/preferences/website/PopupExceptionInfo;)V
    .locals 0

    .prologue
    .line 94
    iput-object p1, p0, Lcom/google/android/apps/chrome/preferences/website/Website;->mPopupExceptionInfo:Lcom/google/android/apps/chrome/preferences/website/PopupExceptionInfo;

    .line 95
    return-void
.end method

.method public setPopupExceptionInfoAllowed(Ljava/lang/Boolean;)V
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/Website;->mPopupExceptionInfo:Lcom/google/android/apps/chrome/preferences/website/PopupExceptionInfo;

    if-eqz v0, :cond_0

    .line 99
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/Website;->mPopupExceptionInfo:Lcom/google/android/apps/chrome/preferences/website/PopupExceptionInfo;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/preferences/website/PopupExceptionInfo;->setAllowed(Ljava/lang/Boolean;)V

    .line 101
    :cond_0
    return-void
.end method

.method public setProtectedMediaIdentifierInfo(Lcom/google/android/apps/chrome/preferences/website/ProtectedMediaIdentifierInfo;)V
    .locals 1

    .prologue
    .line 115
    iput-object p1, p0, Lcom/google/android/apps/chrome/preferences/website/Website;->mProtectedMediaIdentifierInfo:Lcom/google/android/apps/chrome/preferences/website/ProtectedMediaIdentifierInfo;

    .line 116
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/preferences/website/ProtectedMediaIdentifierInfo;->getEmbedder()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/preferences/website/WebsiteAddress;->create(Ljava/lang/String;)Lcom/google/android/apps/chrome/preferences/website/WebsiteAddress;

    move-result-object v0

    .line 117
    if-eqz v0, :cond_0

    .line 118
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/website/WebsiteAddress;->getTitle()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/Website;->mSummary:Ljava/lang/String;

    .line 120
    :cond_0
    return-void
.end method

.method public setProtectedMediaIdentifierInfoAllowed(Ljava/lang/Boolean;)V
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/Website;->mProtectedMediaIdentifierInfo:Lcom/google/android/apps/chrome/preferences/website/ProtectedMediaIdentifierInfo;

    if-eqz v0, :cond_0

    .line 128
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/Website;->mProtectedMediaIdentifierInfo:Lcom/google/android/apps/chrome/preferences/website/ProtectedMediaIdentifierInfo;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/preferences/website/ProtectedMediaIdentifierInfo;->setAllowed(Ljava/lang/Boolean;)V

    .line 130
    :cond_0
    return-void
.end method

.method public setVideoCaptureInfoAllowed(Ljava/lang/Boolean;)V
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/Website;->mVoiceAndVideoCaptureInfo:Lcom/google/android/apps/chrome/preferences/website/VoiceAndVideoCaptureInfo;

    if-eqz v0, :cond_0

    .line 175
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/Website;->mVoiceAndVideoCaptureInfo:Lcom/google/android/apps/chrome/preferences/website/VoiceAndVideoCaptureInfo;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/preferences/website/VoiceAndVideoCaptureInfo;->setVideoCaptureAllowed(Ljava/lang/Boolean;)V

    .line 177
    :cond_0
    return-void
.end method

.method public setVoiceAndVideoCaptureInfo(Lcom/google/android/apps/chrome/preferences/website/VoiceAndVideoCaptureInfo;)V
    .locals 1

    .prologue
    .line 152
    iput-object p1, p0, Lcom/google/android/apps/chrome/preferences/website/Website;->mVoiceAndVideoCaptureInfo:Lcom/google/android/apps/chrome/preferences/website/VoiceAndVideoCaptureInfo;

    .line 153
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/preferences/website/VoiceAndVideoCaptureInfo;->getEmbedder()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/preferences/website/WebsiteAddress;->create(Ljava/lang/String;)Lcom/google/android/apps/chrome/preferences/website/WebsiteAddress;

    move-result-object v0

    .line 154
    if-eqz v0, :cond_0

    .line 155
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/website/WebsiteAddress;->getTitle()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/Website;->mSummary:Ljava/lang/String;

    .line 157
    :cond_0
    return-void
.end method

.method public setVoiceCaptureInfoAllowed(Ljava/lang/Boolean;)V
    .locals 1

    .prologue
    .line 164
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/Website;->mVoiceAndVideoCaptureInfo:Lcom/google/android/apps/chrome/preferences/website/VoiceAndVideoCaptureInfo;

    if-eqz v0, :cond_0

    .line 165
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/Website;->mVoiceAndVideoCaptureInfo:Lcom/google/android/apps/chrome/preferences/website/VoiceAndVideoCaptureInfo;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/preferences/website/VoiceAndVideoCaptureInfo;->setVoiceCaptureAllowed(Ljava/lang/Boolean;)V

    .line 167
    :cond_0
    return-void
.end method
