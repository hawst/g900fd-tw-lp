.class public Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;
.super Landroid/support/v4/app/k;
.source "FirstRunActivity.java"

# interfaces
.implements Lcom/google/android/apps/chrome/firstrun/FirstRunPageDelegate;


# static fields
.field public static final COMING_FROM_CHROME_ICON:Ljava/lang/String; = "ComingFromChromeIcon"

.field public static final FIRE_ORIGINAL_INTENT:Ljava/lang/String; = "FireOriginalIntent"

.field public static final ORIGINAL_INTENT:Ljava/lang/String; = "OriginalIntent"

.field public static final RESULT_CLOSE_APP:Ljava/lang/String; = "Close App"

.field public static final RESULT_SHOW_SYNC_SETTINGS:Ljava/lang/String; = "ResultShowSyncSettings"

.field public static final RESULT_SIGNIN_ACCOUNT_NAME:Ljava/lang/String; = "ResultSignInTo"

.field public static final SHOW_INTRO_BITMAP:Ljava/lang/String; = "ShowIntroBitmap"

.field public static final SHOW_SIGNIN_PAGE:Ljava/lang/String; = "ShowSignIn"

.field public static final SHOW_WELCOME_PAGE:Ljava/lang/String; = "ShowWelcome"

.field public static final SKIP_WELCOME_PAGE_IF_ACCEPTED_TOS:Ljava/lang/String; = "SkipWelcomePageIfAcceptedToS"

.field public static final USE_FRE_FLOW_SEQUENCER:Ljava/lang/String; = "UseFreFlowSequencer"

.field static sGlue:Lcom/google/android/apps/chrome/firstrun/FirstRunGlue;


# instance fields
.field private mFreProperties:Landroid/os/Bundle;

.field private mNativeSideIsInitialized:Z

.field private mPager:Landroid/support/v4/view/ViewPager;

.field private mPagerAdapter:Lcom/google/android/apps/chrome/firstrun/FirstRunPagerAdapter;

.field private mPages:Ljava/util/List;

.field private mProfileDataCache:Lcom/google/android/apps/chrome/firstrun/ProfileDataCache;

.field private mResultShowSyncSettings:Z

.field private mResultSignInAccountName:Ljava/lang/String;

.field private mShowWelcomePage:Z

.field private mSkipIntroPageNumber:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 60
    new-instance v0, Lcom/google/android/apps/chrome/firstrun/FirstRunGlueImpl;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/firstrun/FirstRunGlueImpl;-><init>()V

    sput-object v0, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->sGlue:Lcom/google/android/apps/chrome/firstrun/FirstRunGlue;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Landroid/support/v4/app/k;-><init>()V

    .line 62
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->mShowWelcomePage:Z

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;)Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->mFreProperties:Landroid/os/Bundle;

    return-object v0
.end method

.method static synthetic access$002(Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 0

    .prologue
    .line 40
    iput-object p1, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->mFreProperties:Landroid/os/Bundle;

    return-object p1
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;)Z
    .locals 1

    .prologue
    .line 40
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->mShowWelcomePage:Z

    return v0
.end method

.method static synthetic access$102(Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;Z)Z
    .locals 0

    .prologue
    .line 40
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->mShowWelcomePage:Z

    return p1
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->createPageSequence()V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->mResultSignInAccountName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 40
    iput-object p1, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->mResultSignInAccountName:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$400(Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;)Ljava/util/List;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->mPages:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;)Lcom/google/android/apps/chrome/firstrun/FirstRunPagerAdapter;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->mPagerAdapter:Lcom/google/android/apps/chrome/firstrun/FirstRunPagerAdapter;

    return-object v0
.end method

.method static synthetic access$502(Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;Lcom/google/android/apps/chrome/firstrun/FirstRunPagerAdapter;)Lcom/google/android/apps/chrome/firstrun/FirstRunPagerAdapter;
    .locals 0

    .prologue
    .line 40
    iput-object p1, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->mPagerAdapter:Lcom/google/android/apps/chrome/firstrun/FirstRunPagerAdapter;

    return-object p1
.end method

.method static synthetic access$600(Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->stopProgressionIfNotAcceptedTermsOfService()V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;)Landroid/support/v4/view/ViewPager;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->mPager:Landroid/support/v4/view/ViewPager;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->skipPagesIfNecessary()V

    return-void
.end method

.method private createPageSequence()V
    .locals 4

    .prologue
    .line 86
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->mPages:Ljava/util/List;

    .line 89
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->mShowWelcomePage:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->mPages:Ljava/util/List;

    const-class v1, Lcom/google/android/apps/chrome/firstrun/ToSAndUMAFirstRunFragment;

    invoke-static {v1}, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->pageOf(Ljava/lang/Class;)Ljava/util/concurrent/Callable;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 92
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->mFreProperties:Landroid/os/Bundle;

    const-string/jumbo v1, "ShowIntroBitmap"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    const-wide/16 v2, 0x2

    and-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 93
    sget-object v0, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->sGlue:Lcom/google/android/apps/chrome/firstrun/FirstRunGlue;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/chrome/firstrun/FirstRunGlue;->isDocumentModeEligible(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 94
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->mPages:Ljava/util/List;

    const-class v1, Lcom/google/android/apps/chrome/firstrun/FirstRunIntroRecentsPage;

    invoke-static {v1}, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->pageOf(Ljava/lang/Class;)Ljava/util/concurrent/Callable;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 99
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->mPages:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->mSkipIntroPageNumber:I

    .line 102
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->mFreProperties:Landroid/os/Bundle;

    const-string/jumbo v1, "ShowSignIn"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 103
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->mPages:Ljava/util/List;

    const-class v1, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunFragment;

    invoke-static {v1}, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->pageOf(Ljava/lang/Class;)Ljava/util/concurrent/Callable;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 105
    :cond_2
    return-void
.end method

.method private static finishAllFREActivities(ILandroid/content/Intent;)V
    .locals 3

    .prologue
    .line 347
    invoke-static {}, Lorg/chromium/base/ApplicationStatus;->getRunningActivities()Ljava/util/List;

    move-result-object v0

    .line 348
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 349
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 350
    instance-of v2, v0, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;

    if-eqz v2, :cond_0

    .line 351
    invoke-virtual {v0, p0, p1}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 352
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    .line 355
    :cond_1
    return-void
.end method

.method private flushPersistentData()V
    .locals 1

    .prologue
    .line 343
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->mNativeSideIsInitialized:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/android/apps/chrome/ChromeMobileApplication;->flushPersistentData()V

    .line 344
    :cond_0
    return-void
.end method

.method private jumpToPage(IZ)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 364
    iget-boolean v2, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->mShowWelcomePage:Z

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->didAcceptTermsOfService()Z

    move-result v2

    if-nez v2, :cond_1

    .line 365
    if-nez p1, :cond_0

    .line 372
    :goto_0
    return v0

    :cond_0
    move v0, v1

    .line 365
    goto :goto_0

    .line 367
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->mPagerAdapter:Lcom/google/android/apps/chrome/firstrun/FirstRunPagerAdapter;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/firstrun/FirstRunPagerAdapter;->getCount()I

    move-result v2

    if-lt p1, v2, :cond_2

    .line 368
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->completeFirstRunExperience()V

    move v0, v1

    .line 369
    goto :goto_0

    .line 371
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->mPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1, p1, p2}, Landroid/support/v4/view/ViewPager;->a(IZ)V

    goto :goto_0
.end method

.method public static pageOf(Ljava/lang/Class;)Ljava/util/concurrent/Callable;
    .locals 1

    .prologue
    .line 398
    new-instance v0, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity$2;-><init>(Ljava/lang/Class;)V

    return-object v0
.end method

.method private skipPagesIfNecessary()V
    .locals 3

    .prologue
    .line 381
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->mPagerAdapter:Lcom/google/android/apps/chrome/firstrun/FirstRunPagerAdapter;

    if-nez v0, :cond_1

    .line 390
    :cond_0
    return-void

    .line 383
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->mPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->a()I

    move-result v0

    move v1, v0

    .line 384
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->mPagerAdapter:Lcom/google/android/apps/chrome/firstrun/FirstRunPagerAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/firstrun/FirstRunPagerAdapter;->getCount()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 385
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->mPagerAdapter:Lcom/google/android/apps/chrome/firstrun/FirstRunPagerAdapter;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/firstrun/FirstRunPagerAdapter;->getItem(I)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/firstrun/FirstRunPage;

    .line 386
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/chrome/firstrun/FirstRunPage;->shouldSkipPageOnResume(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 387
    add-int/lit8 v0, v1, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->jumpToPage(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 388
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->mPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->a()I

    move-result v0

    move v1, v0

    .line 389
    goto :goto_0
.end method

.method private stopProgressionIfNotAcceptedTermsOfService()V
    .locals 2

    .prologue
    .line 376
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->mPagerAdapter:Lcom/google/android/apps/chrome/firstrun/FirstRunPagerAdapter;

    if-nez v0, :cond_0

    .line 378
    :goto_0
    return-void

    .line 377
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->mPagerAdapter:Lcom/google/android/apps/chrome/firstrun/FirstRunPagerAdapter;

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->mShowWelcomePage:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->didAcceptTermsOfService()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/firstrun/FirstRunPagerAdapter;->setStopAtTheFirstPage(Z)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public abortFirstRunExperience()V
    .locals 3

    .prologue
    .line 262
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 263
    iget-object v1, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->mFreProperties:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 264
    const-string/jumbo v1, "Close App"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 265
    const/4 v1, 0x0

    invoke-static {v1, v0}, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->finishAllFREActivities(ILandroid/content/Intent;)V

    .line 266
    return-void
.end method

.method public acceptSignIn(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 305
    iput-object p1, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->mResultSignInAccountName:Ljava/lang/String;

    .line 306
    return-void
.end method

.method public acceptTermsOfService(Z)V
    .locals 2

    .prologue
    .line 325
    sget-object v0, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->sGlue:Lcom/google/android/apps/chrome/firstrun/FirstRunGlue;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Lcom/google/android/apps/chrome/firstrun/FirstRunGlue;->acceptTermsOfService(Landroid/content/Context;Z)V

    .line 326
    invoke-direct {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->flushPersistentData()V

    .line 327
    invoke-direct {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->stopProgressionIfNotAcceptedTermsOfService()V

    .line 328
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->mPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->a()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->jumpToPage(IZ)Z

    .line 329
    return-void
.end method

.method public advanceToNextPage()V
    .locals 2

    .prologue
    .line 247
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->mPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->a()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->jumpToPage(IZ)Z

    .line 248
    return-void
.end method

.method public askToOpenSyncSettings()V
    .locals 1

    .prologue
    .line 310
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->mResultShowSyncSettings:Z

    .line 311
    return-void
.end method

.method public completeFirstRunExperience()V
    .locals 4

    .prologue
    .line 270
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->mResultSignInAccountName:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 271
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->mResultShowSyncSettings:Z

    sget-object v1, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->sGlue:Lcom/google/android/apps/chrome/firstrun/FirstRunGlue;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->mResultSignInAccountName:Ljava/lang/String;

    invoke-interface {v1, v2, v3}, Lcom/google/android/apps/chrome/firstrun/FirstRunGlue;->isDefaultAccountName(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->freSignInAccepted(ZZ)V

    .line 276
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->mFreProperties:Landroid/os/Bundle;

    const-string/jumbo v1, "ResultSignInTo"

    iget-object v2, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->mResultSignInAccountName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 277
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->mFreProperties:Landroid/os/Bundle;

    const-string/jumbo v1, "ResultShowSyncSettings"

    iget-boolean v2, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->mResultShowSyncSettings:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 278
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->mFreProperties:Landroid/os/Bundle;

    invoke-static {p0, v0}, Lcom/google/android/apps/chrome/firstrun/FirstRunFlowSequencer;->markFlowAsCompleted(Landroid/app/Activity;Landroid/os/Bundle;)V

    .line 280
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->mFreProperties:Landroid/os/Bundle;

    const-string/jumbo v1, "FireOriginalIntent"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 281
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->mFreProperties:Landroid/os/Bundle;

    const-string/jumbo v1, "OriginalIntent"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    .line 282
    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->startActivity(Landroid/content/Intent;)V

    .line 285
    :cond_1
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 286
    iget-object v1, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->mFreProperties:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 287
    const/4 v1, -0x1

    invoke-static {v1, v0}, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->finishAllFREActivities(ILandroid/content/Intent;)V

    .line 288
    return-void
.end method

.method public didAcceptTermsOfService()Z
    .locals 2

    .prologue
    .line 315
    sget-object v0, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->sGlue:Lcom/google/android/apps/chrome/firstrun/FirstRunGlue;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/chrome/firstrun/FirstRunGlue;->didAcceptTermsOfService(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method public getProfileDataCache()Lcom/google/android/apps/chrome/firstrun/ProfileDataCache;
    .locals 1

    .prologue
    .line 242
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->mProfileDataCache:Lcom/google/android/apps/chrome/firstrun/ProfileDataCache;

    return-object v0
.end method

.method public isNeverUploadCrashDump()Z
    .locals 2

    .prologue
    .line 320
    sget-object v0, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->sGlue:Lcom/google/android/apps/chrome/firstrun/FirstRunGlue;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/chrome/firstrun/FirstRunGlue;->isNeverUploadCrashDump(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method public onBackPressed()V
    .locals 3

    .prologue
    .line 220
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->mPagerAdapter:Lcom/google/android/apps/chrome/firstrun/FirstRunPagerAdapter;

    if-nez v0, :cond_1

    .line 221
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->abortFirstRunExperience()V

    .line 236
    :cond_0
    :goto_0
    return-void

    .line 225
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->mPagerAdapter:Lcom/google/android/apps/chrome/firstrun/FirstRunPagerAdapter;

    iget-object v1, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->mPager:Landroid/support/v4/view/ViewPager;

    iget-object v2, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->mPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v2}, Landroid/support/v4/view/ViewPager;->a()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/firstrun/FirstRunPagerAdapter;->instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;

    move-result-object v0

    .line 226
    instance-of v1, v0, Lcom/google/android/apps/chrome/firstrun/FirstRunPage;

    if-eqz v1, :cond_2

    .line 227
    check-cast v0, Lcom/google/android/apps/chrome/firstrun/FirstRunPage;

    .line 228
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/firstrun/FirstRunPage;->interceptBackPressed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 231
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->mPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->a()I

    move-result v0

    if-nez v0, :cond_3

    .line 232
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->abortFirstRunExperience()V

    goto :goto_0

    .line 234
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->mPager:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->mPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->a()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->a(I)V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 117
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/ChromeMobileApplication;

    const/4 v1, 0x1

    invoke-virtual {v0, p0, v1}, Lcom/google/android/apps/chrome/ChromeMobileApplication;->startBrowserProcessesAndLoadLibrariesSync(Landroid/content/Context;Z)V

    .line 119
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->mNativeSideIsInitialized:Z
    :try_end_0
    .catch Lorg/chromium/base/library_loader/ProcessInitException; {:try_start_0 .. :try_end_0} :catch_0

    .line 126
    invoke-super {p0, p1}, Landroid/support/v4/app/k;->onCreate(Landroid/os/Bundle;)V

    .line 127
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->setFinishOnTouchOutside(Z)V

    .line 129
    if-eqz p1, :cond_0

    .line 130
    iput-object p1, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->mFreProperties:Landroid/os/Bundle;

    .line 137
    :goto_0
    new-instance v0, Landroid/support/v4/view/ViewPager;

    invoke-direct {v0, p0}, Landroid/support/v4/view/ViewPager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->mPager:Landroid/support/v4/view/ViewPager;

    .line 138
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->mPager:Landroid/support/v4/view/ViewPager;

    sget v1, Lcom/google/android/apps/chrome/R$id;->fre_pager:I

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setId(I)V

    .line 139
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->mPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->setContentView(Landroid/view/View;)V

    .line 141
    new-instance v0, Lcom/google/android/apps/chrome/firstrun/ProfileDataCache;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/chrome/firstrun/ProfileDataCache;-><init>(Landroid/app/Activity;Lorg/chromium/chrome/browser/profiles/Profile;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->mProfileDataCache:Lcom/google/android/apps/chrome/firstrun/ProfileDataCache;

    .line 142
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->mProfileDataCache:Lcom/google/android/apps/chrome/firstrun/ProfileDataCache;

    invoke-static {}, Lorg/chromium/chrome/browser/profiles/Profile;->getLastUsedProfile()Lorg/chromium/chrome/browser/profiles/Profile;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/firstrun/ProfileDataCache;->setProfile(Lorg/chromium/chrome/browser/profiles/Profile;)V

    .line 143
    new-instance v0, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity$1;

    iget-object v1, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->mFreProperties:Landroid/os/Bundle;

    invoke-direct {v0, p0, p0, v1}, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity$1;-><init>(Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;Landroid/app/Activity;Landroid/os/Bundle;)V

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity$1;->start()V

    .line 177
    :goto_1
    return-void

    .line 120
    :catch_0
    move-exception v0

    .line 121
    const-string/jumbo v1, "FirstRunActivity"

    const-string/jumbo v2, "Unable to load native library."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 122
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->abortFirstRunExperience()V

    goto :goto_1

    .line 131
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 132
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->mFreProperties:Landroid/os/Bundle;

    goto :goto_0

    .line 134
    :cond_1
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->mFreProperties:Landroid/os/Bundle;

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 198
    invoke-super {p0}, Landroid/support/v4/app/k;->onDestroy()V

    .line 199
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->mProfileDataCache:Lcom/google/android/apps/chrome/firstrun/ProfileDataCache;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/firstrun/ProfileDataCache;->onDestroy()V

    .line 200
    return-void
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 187
    invoke-super {p0}, Landroid/support/v4/app/k;->onPause()V

    .line 188
    invoke-direct {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->flushPersistentData()V

    .line 189
    return-void
.end method

.method protected onResumeFragments()V
    .locals 0

    .prologue
    .line 193
    invoke-direct {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->skipPagesIfNecessary()V

    .line 194
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 181
    invoke-super {p0, p1}, Landroid/support/v4/app/k;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 182
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->mFreProperties:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 183
    return-void
.end method

.method public onSigninDialogShown()V
    .locals 0

    .prologue
    .line 292
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->freSignInShown()V

    .line 293
    return-void
.end method

.method protected onStart()V
    .locals 2

    .prologue
    .line 204
    invoke-super {p0}, Landroid/support/v4/app/k;->onStart()V

    .line 205
    invoke-direct {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->stopProgressionIfNotAcceptedTermsOfService()V

    .line 206
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->mFreProperties:Landroid/os/Bundle;

    const-string/jumbo v1, "UseFreFlowSequencer"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 207
    invoke-static {p0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v0

    .line 208
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getFirstRunFlowComplete()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunIntroPage;->wereAllNecessaryPagesShown(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 212
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->completeFirstRunExperience()V

    .line 215
    :cond_0
    return-void
.end method

.method public openAccountAdder(Landroid/app/Fragment;)V
    .locals 1

    .prologue
    .line 333
    sget-object v0, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->sGlue:Lcom/google/android/apps/chrome/firstrun/FirstRunGlue;

    invoke-interface {v0, p1}, Lcom/google/android/apps/chrome/firstrun/FirstRunGlue;->openAccountAdder(Landroid/app/Fragment;)V

    .line 334
    return-void
.end method

.method public openChromePreferencesPage(Landroid/app/Fragment;Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 338
    invoke-direct {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->flushPersistentData()V

    .line 339
    sget-object v0, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->sGlue:Lcom/google/android/apps/chrome/firstrun/FirstRunGlue;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/apps/chrome/firstrun/FirstRunGlue;->openChromePreferencesPage(Landroid/app/Fragment;Ljava/lang/String;I)V

    .line 340
    return-void
.end method

.method public recreateCurrentPage()V
    .locals 1

    .prologue
    .line 257
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->mPagerAdapter:Lcom/google/android/apps/chrome/firstrun/FirstRunPagerAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/firstrun/FirstRunPagerAdapter;->notifyDataSetChanged()V

    .line 258
    return-void
.end method

.method public refuseSignIn()V
    .locals 1

    .prologue
    .line 297
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->freSkipSignIn()V

    .line 298
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->freSignInDeclined()V

    .line 299
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->mResultSignInAccountName:Ljava/lang/String;

    .line 300
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->mResultShowSyncSettings:Z

    .line 301
    return-void
.end method

.method public skipIntroPages()V
    .locals 2

    .prologue
    .line 252
    iget v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->mSkipIntroPageNumber:I

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->jumpToPage(IZ)Z

    .line 253
    return-void
.end method
