.class public abstract Lcom/google/android/apps/chrome/videofling/LockScreenTransportControl;
.super Lcom/google/android/apps/chrome/videofling/TransportControl;
.source "LockScreenTransportControl.java"

# interfaces
.implements Lcom/google/android/apps/chrome/videofling/MediaRouteController$Listener;


# static fields
.field private static final LOCK:Ljava/lang/Object;

.field private static sDebug:Z

.field private static sInstance:Lcom/google/android/apps/chrome/videofling/LockScreenTransportControl;


# instance fields
.field private mMediaRouteController:Lcom/google/android/apps/chrome/videofling/MediaRouteController;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControl;->LOCK:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/google/android/apps/chrome/videofling/TransportControl;-><init>()V

    .line 35
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControl;->mMediaRouteController:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    .line 47
    invoke-static {}, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControl;->setSDebug()V

    .line 48
    return-void
.end method

.method static synthetic access$000()Z
    .locals 1

    .prologue
    .line 28
    sget-boolean v0, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControl;->sDebug:Z

    return v0
.end method

.method private static enabled()Z
    .locals 2

    .prologue
    .line 138
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static getIfExists()Lcom/google/android/apps/chrome/videofling/LockScreenTransportControl;
    .locals 1

    .prologue
    .line 149
    sget-object v0, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControl;->sInstance:Lcom/google/android/apps/chrome/videofling/LockScreenTransportControl;

    return-object v0
.end method

.method public static getOrCreate(Landroid/content/Context;Lcom/google/android/apps/chrome/videofling/MediaRouteController;)Lcom/google/android/apps/chrome/videofling/LockScreenTransportControl;
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 101
    const-string/jumbo v1, "LockScreenTransportControl"

    const-string/jumbo v2, "getOrCreate called"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    sget-object v6, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControl;->LOCK:Ljava/lang/Object;

    monitor-enter v6

    .line 103
    :try_start_0
    sget-object v1, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControl;->sInstance:Lcom/google/android/apps/chrome/videofling/LockScreenTransportControl;

    if-nez v1, :cond_1

    .line 108
    invoke-static {}, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControl;->enabled()Z

    move-result v1

    if-nez v1, :cond_0

    .line 109
    monitor-exit v6

    .line 122
    :goto_0
    return-object v0

    .line 110
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-ge v0, v1, :cond_2

    .line 111
    new-instance v0, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV14;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV14;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControl;->sInstance:Lcom/google/android/apps/chrome/videofling/LockScreenTransportControl;

    .line 118
    :cond_1
    :goto_1
    sget-object v7, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControl;->sInstance:Lcom/google/android/apps/chrome/videofling/LockScreenTransportControl;

    new-instance v0, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;

    const/4 v1, 0x0

    const/4 v2, 0x0

    sget-object v3, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;->STOPPED:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;-><init>(Ljava/lang/String;ILcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;ILjava/lang/String;)V

    invoke-virtual {v7, v0}, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControl;->setVideoInfo(Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;)V

    .line 121
    sget-object v0, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControl;->sInstance:Lcom/google/android/apps/chrome/videofling/LockScreenTransportControl;

    iput-object p1, v0, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControl;->mMediaRouteController:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    .line 122
    sget-object v0, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControl;->sInstance:Lcom/google/android/apps/chrome/videofling/LockScreenTransportControl;

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 123
    :catchall_0
    move-exception v0

    monitor-exit v6

    throw v0

    .line 112
    :cond_2
    :try_start_1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-ge v0, v1, :cond_3

    .line 113
    new-instance v0, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV16;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV16;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControl;->sInstance:Lcom/google/android/apps/chrome/videofling/LockScreenTransportControl;

    goto :goto_1

    .line 115
    :cond_3
    new-instance v0, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV18;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV18;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControl;->sInstance:Lcom/google/android/apps/chrome/videofling/LockScreenTransportControl;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method

.method private static setSDebug()V
    .locals 2

    .prologue
    .line 43
    invoke-static {}, Lorg/chromium/base/CommandLine;->getInstance()Lorg/chromium/base/CommandLine;

    move-result-object v0

    const-string/jumbo v1, "enable-cast-debug"

    invoke-virtual {v0, v1}, Lorg/chromium/base/CommandLine;->hasSwitch(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControl;->sDebug:Z

    .line 44
    return-void
.end method


# virtual methods
.method public hide()V
    .locals 2

    .prologue
    .line 154
    const/4 v0, 0x0

    sget-object v1, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;->STOPPED:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControl;->onLockScreenPlaybackStateChanged(Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;)V

    .line 155
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControl;->mMediaRouteController:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->removeListener(Lcom/google/android/apps/chrome/videofling/MediaRouteController$Listener;)V

    .line 156
    return-void
.end method

.method protected abstract isPlaying()Z
.end method

.method protected abstract onLockScreenPlaybackStateChanged(Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;)V
.end method

.method public onPlaybackStateChanged(Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;Lorg/chromium/chrome/browser/Tab;I)V
    .locals 0

    .prologue
    .line 174
    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControl;->onLockScreenPlaybackStateChanged(Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;)V

    .line 175
    return-void
.end method

.method public setRouteController(Lcom/google/android/apps/chrome/videofling/MediaRouteController;)V
    .locals 2

    .prologue
    .line 166
    sget-object v1, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControl;->LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 167
    :try_start_0
    sget-object v0, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControl;->sInstance:Lcom/google/android/apps/chrome/videofling/LockScreenTransportControl;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControl;->sInstance:Lcom/google/android/apps/chrome/videofling/LockScreenTransportControl;

    iput-object p1, v0, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControl;->mMediaRouteController:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    .line 168
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public show(Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;)V
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControl;->mMediaRouteController:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->addListener(Lcom/google/android/apps/chrome/videofling/MediaRouteController$Listener;)V

    .line 161
    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1}, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControl;->onLockScreenPlaybackStateChanged(Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;)V

    .line 162
    return-void
.end method
