.class Lcom/google/android/apps/chrome/ChromeTabbedActivity$8;
.super Ljava/lang/Object;
.source "ChromeTabbedActivity.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnPreDrawListener;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/ChromeTabbedActivity;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/ChromeTabbedActivity;)V
    .locals 0

    .prologue
    .line 825
    iput-object p1, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity$8;->this$0:Lcom/google/android/apps/chrome/ChromeTabbedActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreDraw()Z
    .locals 2

    .prologue
    .line 828
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity$8;->this$0:Lcom/google/android/apps/chrome/ChromeTabbedActivity;

    # getter for: Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mControlContainer:Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->access$900(Lcom/google/android/apps/chrome/ChromeTabbedActivity;)Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity$8;->this$0:Lcom/google/android/apps/chrome/ChromeTabbedActivity;

    # getter for: Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mFirstDrawListener:Landroid/view/ViewTreeObserver$OnPreDrawListener;
    invoke-static {v1}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->access$800(Lcom/google/android/apps/chrome/ChromeTabbedActivity;)Landroid/view/ViewTreeObserver$OnPreDrawListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->removeOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 829
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity$8;->this$0:Lcom/google/android/apps/chrome/ChromeTabbedActivity;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mFirstDrawListener:Landroid/view/ViewTreeObserver$OnPreDrawListener;
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->access$802(Lcom/google/android/apps/chrome/ChromeTabbedActivity;Landroid/view/ViewTreeObserver$OnPreDrawListener;)Landroid/view/ViewTreeObserver$OnPreDrawListener;

    .line 831
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity$8;->this$0:Lcom/google/android/apps/chrome/ChromeTabbedActivity;

    iget-object v0, v0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/chrome/ChromeTabbedActivity$8$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity$8$1;-><init>(Lcom/google/android/apps/chrome/ChromeTabbedActivity$8;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 837
    const/4 v0, 0x1

    return v0
.end method
