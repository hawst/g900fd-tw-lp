.class public Lcom/google/android/apps/chrome/compositor/resources/ResourceManager;
.super Ljava/lang/Object;
.source "ResourceManager.java"

# interfaces
.implements Lcom/google/android/apps/chrome/compositor/resources/ResourceLoaderCallback;


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final mDynamicResourceLoader:Lcom/google/android/apps/chrome/compositor/resources/dynamics/DynamicResourceLoader;

.field private final mLoadedResources:Landroid/util/SparseArray;

.field private mNativeResourceManagerPtr:J

.field private mPreloadedStaticResources:Z

.field private final mPxToDp:F

.field private final mResources:Landroid/content/res/Resources;

.field private final mStaticResourceLoader:Lcom/google/android/apps/chrome/compositor/resources/statics/StaticResourceLoader;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const-class v0, Lcom/google/android/apps/chrome/compositor/resources/ResourceManager;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/compositor/resources/ResourceManager;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Landroid/content/Context;J)V
    .locals 2

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/compositor/resources/ResourceManager;->mLoadedResources:Landroid/util/SparseArray;

    .line 42
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/compositor/resources/ResourceManager;->mResources:Landroid/content/res/Resources;

    .line 43
    new-instance v0, Lcom/google/android/apps/chrome/compositor/resources/statics/StaticResourceLoader;

    iget-object v1, p0, Lcom/google/android/apps/chrome/compositor/resources/ResourceManager;->mResources:Landroid/content/res/Resources;

    invoke-direct {v0, v1, p0}, Lcom/google/android/apps/chrome/compositor/resources/statics/StaticResourceLoader;-><init>(Landroid/content/res/Resources;Lcom/google/android/apps/chrome/compositor/resources/ResourceLoaderCallback;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/compositor/resources/ResourceManager;->mStaticResourceLoader:Lcom/google/android/apps/chrome/compositor/resources/statics/StaticResourceLoader;

    .line 44
    new-instance v0, Lcom/google/android/apps/chrome/compositor/resources/dynamics/DynamicResourceLoader;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/compositor/resources/dynamics/DynamicResourceLoader;-><init>(Lcom/google/android/apps/chrome/compositor/resources/ResourceLoaderCallback;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/compositor/resources/ResourceManager;->mDynamicResourceLoader:Lcom/google/android/apps/chrome/compositor/resources/dynamics/DynamicResourceLoader;

    .line 45
    const/high16 v0, 0x3f800000    # 1.0f

    iget-object v1, p0, Lcom/google/android/apps/chrome/compositor/resources/ResourceManager;->mResources:Landroid/content/res/Resources;

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    div-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/chrome/compositor/resources/ResourceManager;->mPxToDp:F

    .line 47
    iput-wide p2, p0, Lcom/google/android/apps/chrome/compositor/resources/ResourceManager;->mNativeResourceManagerPtr:J

    .line 48
    return-void
.end method

.method private static create(Landroid/content/Context;J)Lcom/google/android/apps/chrome/compositor/resources/ResourceManager;
    .locals 1

    .prologue
    .line 58
    new-instance v0, Lcom/google/android/apps/chrome/compositor/resources/ResourceManager;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/apps/chrome/compositor/resources/ResourceManager;-><init>(Landroid/content/Context;J)V

    return-object v0
.end method

.method private destroy()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 110
    sget-boolean v0, Lcom/google/android/apps/chrome/compositor/resources/ResourceManager;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/apps/chrome/compositor/resources/ResourceManager;->mNativeResourceManagerPtr:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 111
    :cond_0
    iput-wide v2, p0, Lcom/google/android/apps/chrome/compositor/resources/ResourceManager;->mNativeResourceManagerPtr:J

    .line 112
    return-void
.end method

.method private getNativePtr()J
    .locals 2

    .prologue
    .line 122
    iget-wide v0, p0, Lcom/google/android/apps/chrome/compositor/resources/ResourceManager;->mNativeResourceManagerPtr:J

    return-wide v0
.end method

.method private native nativeOnResourceReady(JILandroid/graphics/Bitmap;IIIIIIII)V
.end method

.method private resourceRequested(I)V
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/resources/ResourceManager;->mDynamicResourceLoader:Lcom/google/android/apps/chrome/compositor/resources/dynamics/DynamicResourceLoader;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/compositor/resources/dynamics/DynamicResourceLoader;->loadResource(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 118
    :goto_0
    return-void

    .line 117
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/resources/ResourceManager;->mStaticResourceLoader:Lcom/google/android/apps/chrome/compositor/resources/statics/StaticResourceLoader;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/compositor/resources/statics/StaticResourceLoader;->loadResourceSync(I)V

    goto :goto_0
.end method


# virtual methods
.method public getDynamicResourceLoader()Lcom/google/android/apps/chrome/compositor/resources/dynamics/DynamicResourceLoader;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/resources/ResourceManager;->mDynamicResourceLoader:Lcom/google/android/apps/chrome/compositor/resources/dynamics/DynamicResourceLoader;

    return-object v0
.end method

.method public getResource(I)Lcom/google/android/apps/chrome/compositor/resources/LayoutResource;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/resources/ResourceManager;->mLoadedResources:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/compositor/resources/LayoutResource;

    return-object v0
.end method

.method public onResourceLoaded(ILcom/google/android/apps/chrome/compositor/resources/Resource;)V
    .locals 16

    .prologue
    .line 95
    if-nez p2, :cond_1

    .line 106
    :cond_0
    :goto_0
    return-void

    .line 97
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/chrome/compositor/resources/ResourceManager;->mLoadedResources:Landroid/util/SparseArray;

    new-instance v3, Lcom/google/android/apps/chrome/compositor/resources/LayoutResource;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/apps/chrome/compositor/resources/ResourceManager;->mPxToDp:F

    move-object/from16 v0, p2

    invoke-direct {v3, v4, v0}, Lcom/google/android/apps/chrome/compositor/resources/LayoutResource;-><init>(FLcom/google/android/apps/chrome/compositor/resources/Resource;)V

    move/from16 v0, p1

    invoke-virtual {v2, v0, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 99
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/google/android/apps/chrome/compositor/resources/ResourceManager;->mNativeResourceManagerPtr:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    .line 100
    invoke-interface/range {p2 .. p2}, Lcom/google/android/apps/chrome/compositor/resources/Resource;->getPadding()Landroid/graphics/Rect;

    move-result-object v2

    .line 101
    invoke-interface/range {p2 .. p2}, Lcom/google/android/apps/chrome/compositor/resources/Resource;->getAperture()Landroid/graphics/Rect;

    move-result-object v3

    .line 103
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/google/android/apps/chrome/compositor/resources/ResourceManager;->mNativeResourceManagerPtr:J

    invoke-interface/range {p2 .. p2}, Lcom/google/android/apps/chrome/compositor/resources/Resource;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v7

    iget v8, v2, Landroid/graphics/Rect;->left:I

    iget v9, v2, Landroid/graphics/Rect;->top:I

    iget v10, v2, Landroid/graphics/Rect;->right:I

    iget v11, v2, Landroid/graphics/Rect;->bottom:I

    iget v12, v3, Landroid/graphics/Rect;->left:I

    iget v13, v3, Landroid/graphics/Rect;->top:I

    iget v14, v3, Landroid/graphics/Rect;->right:I

    iget v15, v3, Landroid/graphics/Rect;->bottom:I

    move-object/from16 v3, p0

    move/from16 v6, p1

    invoke-direct/range {v3 .. v15}, Lcom/google/android/apps/chrome/compositor/resources/ResourceManager;->nativeOnResourceReady(JILandroid/graphics/Bitmap;IIIIIIII)V

    goto :goto_0
.end method

.method public preloadStaticResources(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 77
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/compositor/resources/ResourceManager;->mPreloadedStaticResources:Z

    if-eqz v0, :cond_0

    .line 83
    :goto_0
    return-void

    .line 78
    :cond_0
    new-instance v0, Lcom/google/android/apps/chrome/compositor/resources/statics/StaticResourcePreloads;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/compositor/resources/statics/StaticResourcePreloads;-><init>()V

    .line 80
    iget-object v1, p0, Lcom/google/android/apps/chrome/compositor/resources/ResourceManager;->mStaticResourceLoader:Lcom/google/android/apps/chrome/compositor/resources/statics/StaticResourceLoader;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/compositor/resources/statics/StaticResourcePreloads;->getAsynchronousResources()[I

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/chrome/compositor/resources/statics/StaticResourceLoader;->loadResourcesAsync([I)V

    .line 81
    iget-object v1, p0, Lcom/google/android/apps/chrome/compositor/resources/ResourceManager;->mStaticResourceLoader:Lcom/google/android/apps/chrome/compositor/resources/statics/StaticResourceLoader;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/compositor/resources/statics/StaticResourcePreloads;->getSynchronousResources(Landroid/content/Context;)[I

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/compositor/resources/statics/StaticResourceLoader;->loadResourcesSync([I)V

    .line 82
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/compositor/resources/ResourceManager;->mPreloadedStaticResources:Z

    goto :goto_0
.end method
