.class Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment$1;
.super Ljava/lang/Object;
.source "GoogleServicesFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 303
    const-class v0, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment$1;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;)V
    .locals 0

    .prologue
    .line 303
    iput-object p1, p0, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment$1;->this$0:Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 307
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment$1;->this$0:Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/preferences/Preferences;

    .line 308
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/Preferences;->updatePreferencesHeadersAndMenu()V

    .line 309
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/Preferences;->getCurrentFragment()Landroid/app/Fragment;

    move-result-object v0

    .line 310
    if-eqz v0, :cond_1

    .line 311
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-class v2, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 312
    sget-boolean v1, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment$1;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    invoke-static {}, Lorg/chromium/chrome/browser/signin/SigninManager;->isNewProfileManagementEnabled()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 313
    :cond_0
    check-cast v0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->updateAccountsList()V

    .line 320
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment$1;->this$0:Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment$1;->this$0:Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->closeEditor()V

    .line 321
    :cond_2
    return-void

    .line 314
    :cond_3
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-class v2, Lcom/google/android/apps/chrome/preferences/BasicsPreferences;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 315
    sget-boolean v1, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment$1;->$assertionsDisabled:Z

    if-nez v1, :cond_4

    invoke-static {}, Lorg/chromium/chrome/browser/signin/SigninManager;->isNewProfileManagementEnabled()Z

    move-result v1

    if-eqz v1, :cond_4

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 316
    :cond_4
    check-cast v0, Lcom/google/android/apps/chrome/preferences/BasicsPreferences;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/BasicsPreferences;->updateAccountHeader()V

    goto :goto_0
.end method
