.class public Lcom/google/android/apps/chrome/sync/ChromeBrowserSyncAdapter;
.super Lorg/chromium/chrome/browser/sync/ChromiumSyncAdapter;
.source "ChromeBrowserSyncAdapter.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/app/Application;)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0, p1, p2}, Lorg/chromium/chrome/browser/sync/ChromiumSyncAdapter;-><init>(Landroid/content/Context;Landroid/app/Application;)V

    .line 20
    return-void
.end method


# virtual methods
.method protected initCommandLine()V
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/ChromeBrowserSyncAdapter;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/ChromeMobileApplication;->initCommandLine(Landroid/content/Context;)V

    .line 30
    return-void
.end method

.method protected useAsyncStartup()Z
    .locals 1

    .prologue
    .line 24
    const/4 v0, 0x0

    return v0
.end method
