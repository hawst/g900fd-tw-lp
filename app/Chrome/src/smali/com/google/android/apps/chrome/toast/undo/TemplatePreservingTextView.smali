.class public Lcom/google/android/apps/chrome/toast/undo/TemplatePreservingTextView;
.super Landroid/widget/TextView;
.source "TemplatePreservingTextView.java"


# instance fields
.field private mContent:Ljava/lang/CharSequence;

.field private mTemplate:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0, p1, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 29
    return-void
.end method

.method private setClippedText(Ljava/lang/CharSequence;I)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 60
    if-eqz p1, :cond_0

    :goto_0
    iput-object p1, p0, Lcom/google/android/apps/chrome/toast/undo/TemplatePreservingTextView;->mContent:Ljava/lang/CharSequence;

    .line 62
    iget-object v0, p0, Lcom/google/android/apps/chrome/toast/undo/TemplatePreservingTextView;->mTemplate:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 63
    iget-object v0, p0, Lcom/google/android/apps/chrome/toast/undo/TemplatePreservingTextView;->mContent:Ljava/lang/CharSequence;

    sget-object v1, Landroid/widget/TextView$BufferType;->NORMAL:Landroid/widget/TextView$BufferType;

    invoke-super {p0, v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    .line 83
    :goto_1
    return-void

    .line 60
    :cond_0
    const-string/jumbo p1, ""

    goto :goto_0

    .line 67
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/toast/undo/TemplatePreservingTextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v0

    .line 70
    iget-object v1, p0, Lcom/google/android/apps/chrome/toast/undo/TemplatePreservingTextView;->mTemplate:Ljava/lang/String;

    new-array v2, v5, [Ljava/lang/Object;

    const-string/jumbo v3, ""

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 71
    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v1

    .line 74
    int-to-float v2, p2

    sub-float v1, v2, v1

    const/4 v2, 0x0

    invoke-static {v1, v2}, Ljava/lang/Math;->max(FF)F

    move-result v1

    .line 77
    iget-object v2, p0, Lcom/google/android/apps/chrome/toast/undo/TemplatePreservingTextView;->mContent:Ljava/lang/CharSequence;

    sget-object v3, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-static {v2, v0, v1, v3}, Landroid/text/TextUtils;->ellipsize(Ljava/lang/CharSequence;Landroid/text/TextPaint;FLandroid/text/TextUtils$TruncateAt;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 80
    iget-object v1, p0, Lcom/google/android/apps/chrome/toast/undo/TemplatePreservingTextView;->mTemplate:Ljava/lang/String;

    new-array v2, v5, [Ljava/lang/Object;

    aput-object v0, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 82
    sget-object v1, Landroid/widget/TextView$BufferType;->NORMAL:Landroid/widget/TextView$BufferType;

    invoke-super {p0, v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    goto :goto_1
.end method


# virtual methods
.method protected onMeasure(II)V
    .locals 2

    .prologue
    .line 52
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/toast/undo/TemplatePreservingTextView;->getPaddingLeft()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/toast/undo/TemplatePreservingTextView;->getPaddingRight()I

    move-result v1

    sub-int/2addr v0, v1

    .line 54
    iget-object v1, p0, Lcom/google/android/apps/chrome/toast/undo/TemplatePreservingTextView;->mContent:Ljava/lang/CharSequence;

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/chrome/toast/undo/TemplatePreservingTextView;->setClippedText(Ljava/lang/CharSequence;I)V

    .line 56
    invoke-super {p0, p1, p2}, Landroid/widget/TextView;->onMeasure(II)V

    .line 57
    return-void
.end method

.method public setTemplate(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 36
    iput-object p1, p0, Lcom/google/android/apps/chrome/toast/undo/TemplatePreservingTextView;->mTemplate:Ljava/lang/String;

    .line 37
    return-void
.end method

.method public setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V
    .locals 2

    .prologue
    .line 46
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/toast/undo/TemplatePreservingTextView;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/toast/undo/TemplatePreservingTextView;->getPaddingLeft()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/toast/undo/TemplatePreservingTextView;->getPaddingRight()I

    move-result v1

    sub-int/2addr v0, v1

    .line 47
    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/chrome/toast/undo/TemplatePreservingTextView;->setClippedText(Ljava/lang/CharSequence;I)V

    .line 48
    return-void
.end method
