.class public Lcom/google/android/apps/chrome/firstrun/FirstRunIntroRecentsPage;
.super Lcom/google/android/apps/chrome/firstrun/FirstRunIntroPage;
.source "FirstRunIntroRecentsPage.java"


# instance fields
.field private mLoupeAnimation:Landroid/animation/AnimatorSet;

.field private mLoupeImageView:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunIntroPage;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/firstrun/FirstRunIntroRecentsPage;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunIntroRecentsPage;->openSettings()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/firstrun/FirstRunIntroRecentsPage;)Landroid/animation/AnimatorSet;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunIntroRecentsPage;->mLoupeAnimation:Landroid/animation/AnimatorSet;

    return-object v0
.end method

.method static synthetic access$102(Lcom/google/android/apps/chrome/firstrun/FirstRunIntroRecentsPage;Landroid/animation/AnimatorSet;)Landroid/animation/AnimatorSet;
    .locals 0

    .prologue
    .line 31
    iput-object p1, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunIntroRecentsPage;->mLoupeAnimation:Landroid/animation/AnimatorSet;

    return-object p1
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/firstrun/FirstRunIntroRecentsPage;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunIntroRecentsPage;->mLoupeImageView:Landroid/widget/ImageView;

    return-object v0
.end method

.method private openSettings()V
    .locals 3

    .prologue
    .line 126
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunIntroRecentsPage;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->savePreviousOptOutState()V

    .line 128
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunIntroRecentsPage;->getPageDelegate()Lcom/google/android/apps/chrome/firstrun/FirstRunPageDelegate;

    move-result-object v0

    const-class v1, Lcom/google/android/apps/chrome/preferences/DocumentModePreference;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    sget v2, Lcom/google/android/apps/chrome/R$string;->tabs_and_apps_title:I

    invoke-interface {v0, p0, v1, v2}, Lcom/google/android/apps/chrome/firstrun/FirstRunPageDelegate;->openChromePreferencesPage(Landroid/app/Fragment;Ljava/lang/String;I)V

    .line 132
    return-void
.end method


# virtual methods
.method protected getIntroDrawableId()I
    .locals 1

    .prologue
    .line 49
    sget v0, Lcom/google/android/apps/chrome/R$drawable;->fre_new_recents_menu:I

    return v0
.end method

.method protected getIntroTextId()I
    .locals 1

    .prologue
    .line 59
    sget v0, Lcom/google/android/apps/chrome/R$string;->fre_new_recents_menu_text:I

    return v0
.end method

.method protected getIntroTitleId()I
    .locals 1

    .prologue
    .line 54
    sget v0, Lcom/google/android/apps/chrome/R$string;->fre_new_recents_menu_title:I

    return v0
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 9

    .prologue
    const/4 v8, -0x2

    const/4 v7, 0x0

    .line 70
    sget v0, Lcom/google/android/apps/chrome/R$id;->text:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 71
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 72
    sget v1, Lcom/google/android/apps/chrome/R$string;->fre_new_recents_menu_text:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/firstrun/FirstRunIntroRecentsPage;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;

    new-instance v3, Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;

    const-string/jumbo v4, "<link>"

    const-string/jumbo v5, "</link>"

    new-instance v6, Lcom/google/android/apps/chrome/firstrun/FirstRunIntroRecentsPage$1;

    invoke-direct {v6, p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunIntroRecentsPage$1;-><init>(Lcom/google/android/apps/chrome/firstrun/FirstRunIntroRecentsPage;)V

    invoke-direct {v3, v4, v5, v6}, Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    aput-object v3, v2, v7

    invoke-static {v1, v2}, Lcom/google/android/apps/chrome/utilities/StringLinkifier;->linkify(Ljava/lang/String;[Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;)Landroid/text/SpannableString;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 81
    sget v0, Lcom/google/android/apps/chrome/R$id;->negative_button:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 82
    sget v1, Lcom/google/android/apps/chrome/R$string;->fre_settings:I

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 83
    new-instance v1, Lcom/google/android/apps/chrome/firstrun/FirstRunIntroRecentsPage$2;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunIntroRecentsPage$2;-><init>(Lcom/google/android/apps/chrome/firstrun/FirstRunIntroRecentsPage;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 91
    new-instance v0, Landroid/widget/ImageView;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunIntroRecentsPage;->mLoupeImageView:Landroid/widget/ImageView;

    .line 92
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunIntroRecentsPage;->mLoupeImageView:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunIntroRecentsPage;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/chrome/R$drawable;->fre_new_recents_menu_loupe:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 94
    sget v0, Lcom/google/android/apps/chrome/R$id;->image_view_wrapper:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 95
    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v8, v8}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 97
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunIntroRecentsPage;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/chrome/R$dimen;->fre_recents_loupe_margin_left:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunIntroRecentsPage;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/google/android/apps/chrome/R$dimen;->fre_recents_loupe_margin_top:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-virtual {v1, v2, v3, v7, v7}, Landroid/widget/FrameLayout$LayoutParams;->setMargins(IIII)V

    .line 101
    iget-object v2, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunIntroRecentsPage;->mLoupeImageView:Landroid/widget/ImageView;

    invoke-virtual {v0, v2, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 102
    return-void
.end method

.method public setUserVisibleHint(Z)V
    .locals 4

    .prologue
    .line 106
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/firstrun/FirstRunIntroPage;->setUserVisibleHint(Z)V

    .line 107
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunIntroRecentsPage;->getView()Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_1

    .line 123
    :cond_0
    :goto_0
    return-void

    .line 109
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunIntroRecentsPage;->getView()Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/chrome/firstrun/FirstRunIntroRecentsPage$3;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunIntroRecentsPage$3;-><init>(Lcom/google/android/apps/chrome/firstrun/FirstRunIntroRecentsPage;)V

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method public shouldSkipPageOnResume(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 64
    invoke-static {p1}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->didOptOutStateChange()Z

    move-result v0

    return v0
.end method
