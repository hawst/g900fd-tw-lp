.class public Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences$PopupException;
.super Ljava/lang/Object;
.source "ChromeNativePreferences.java"


# instance fields
.field private final mPattern:Ljava/lang/String;

.field private final mSetting:Ljava/lang/String;

.field private final mSource:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 144
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 145
    iput-object p1, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences$PopupException;->mPattern:Ljava/lang/String;

    .line 146
    iput-object p2, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences$PopupException;->mSetting:Ljava/lang/String;

    .line 147
    iput-object p3, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences$PopupException;->mSource:Ljava/lang/String;

    .line 148
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences$1;)V
    .locals 0

    .prologue
    .line 139
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences$PopupException;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public getPattern()Ljava/lang/String;
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences$PopupException;->mPattern:Ljava/lang/String;

    return-object v0
.end method

.method public getSetting()Ljava/lang/String;
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences$PopupException;->mSetting:Ljava/lang/String;

    return-object v0
.end method
