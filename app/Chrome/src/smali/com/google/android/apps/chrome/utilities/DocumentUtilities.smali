.class public Lcom/google/android/apps/chrome/utilities/DocumentUtilities;
.super Lcom/google/android/apps/chrome/utilities/DocumentApiDelegate;
.source "DocumentUtilities.java"


# static fields
.field public static final TAG:Ljava/lang/String; = "DocumentUtilities"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/google/android/apps/chrome/utilities/DocumentApiDelegate;-><init>()V

    return-void
.end method

.method private computeStatusBarColor(I)I
    .locals 4

    .prologue
    .line 82
    const/4 v0, 0x3

    new-array v0, v0, [F

    .line 83
    invoke-static {p1, v0}, Landroid/graphics/Color;->colorToHSV(I[F)V

    .line 84
    const/4 v1, 0x2

    aget v2, v0, v1

    const v3, 0x3f19999a    # 0.6f

    mul-float/2addr v2, v3

    aput v2, v0, v1

    .line 85
    invoke-static {v0}, Landroid/graphics/Color;->HSVToColor([F)I

    move-result v0

    return v0
.end method

.method public static logTaskInfo(Landroid/app/ActivityManager$AppTask;)V
    .locals 3

    .prologue
    .line 93
    const-string/jumbo v0, "DocumentUtilities"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "TASK: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/app/ActivityManager$AppTask;->getTaskInfo()Landroid/app/ActivityManager$RecentTaskInfo;

    move-result-object v2

    iget v2, v2, Landroid/app/ActivityManager$RecentTaskInfo;->id:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Landroid/app/ActivityManager$AppTask;->getTaskInfo()Landroid/app/ActivityManager$RecentTaskInfo;

    move-result-object v2

    iget v2, v2, Landroid/app/ActivityManager$RecentTaskInfo;->persistentId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Landroid/app/ActivityManager$AppTask;->getTaskInfo()Landroid/app/ActivityManager$RecentTaskInfo;

    move-result-object v2

    iget-object v2, v2, Landroid/app/ActivityManager$RecentTaskInfo;->baseIntent:Landroid/content/Intent;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 95
    return-void
.end method


# virtual methods
.method public finishAndRemoveTask(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 110
    invoke-virtual {p1}, Landroid/app/Activity;->finishAndRemoveTask()V

    .line 111
    return-void
.end method

.method public finishOtherTasksWithData(Landroid/net/Uri;I)Landroid/content/Intent;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 47
    if-nez p1, :cond_0

    .line 73
    :goto_0
    return-object v1

    .line 49
    :cond_0
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    .line 50
    invoke-static {}, Lorg/chromium/base/ApplicationStatus;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 53
    const-string/jumbo v3, "activity"

    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 55
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 56
    invoke-virtual {v0}, Landroid/app/ActivityManager;->getAppTasks()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager$AppTask;

    .line 57
    invoke-virtual {v0}, Landroid/app/ActivityManager$AppTask;->getTaskInfo()Landroid/app/ActivityManager$RecentTaskInfo;

    move-result-object v5

    iget v5, v5, Landroid/app/ActivityManager$RecentTaskInfo;->id:I

    .line 58
    invoke-virtual {v0}, Landroid/app/ActivityManager$AppTask;->getTaskInfo()Landroid/app/ActivityManager$RecentTaskInfo;

    move-result-object v6

    iget-object v6, v6, Landroid/app/ActivityManager$RecentTaskInfo;->baseIntent:Landroid/content/Intent;

    invoke-virtual {v6}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v6

    .line 60
    invoke-static {v2, v6}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 61
    const/4 v6, -0x1

    if-eq v5, v6, :cond_2

    if-eq v5, p2, :cond_1

    .line 62
    :cond_2
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 66
    :cond_3
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move-object v0, v1

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager$AppTask;

    .line 67
    invoke-virtual {v0}, Landroid/app/ActivityManager$AppTask;->getTaskInfo()Landroid/app/ActivityManager$RecentTaskInfo;

    move-result-object v1

    iget-object v1, v1, Landroid/app/ActivityManager$RecentTaskInfo;->baseIntent:Landroid/content/Intent;

    .line 68
    const-string/jumbo v3, "DocumentUtilities"

    const-string/jumbo v4, "Removing duplicated task:"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 69
    invoke-static {v0}, Lcom/google/android/apps/chrome/utilities/DocumentUtilities;->logTaskInfo(Landroid/app/ActivityManager$AppTask;)V

    .line 70
    invoke-virtual {v0}, Landroid/app/ActivityManager$AppTask;->finishAndRemoveTask()V

    move-object v0, v1

    .line 71
    goto :goto_2

    :cond_4
    move-object v1, v0

    .line 73
    goto :goto_0
.end method

.method public migrateTabs(ZLandroid/app/Activity;Z)V
    .locals 1

    .prologue
    .line 99
    if-eqz p3, :cond_0

    const/4 v0, 0x2

    .line 101
    :goto_0
    if-eqz p1, :cond_1

    .line 102
    invoke-static {p2, v0}, Lcom/google/android/apps/chrome/document/DocumentMigrationHelper;->migrateTabsFromClassicToDocument(Landroid/app/Activity;I)V

    .line 106
    :goto_1
    return-void

    .line 99
    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    .line 104
    :cond_1
    invoke-static {p2, v0}, Lcom/google/android/apps/chrome/document/DocumentMigrationHelper;->migrateTabsFromDocumentToClassic(Landroid/app/Activity;I)V

    goto :goto_1
.end method

.method public updateTaskDescription(Landroid/app/Activity;Ljava/lang/String;Landroid/graphics/Bitmap;IZ)V
    .locals 2

    .prologue
    .line 38
    new-instance v0, Landroid/app/ActivityManager$TaskDescription;

    invoke-direct {v0, p2, p3, p4}, Landroid/app/ActivityManager$TaskDescription;-><init>(Ljava/lang/String;Landroid/graphics/Bitmap;I)V

    .line 40
    invoke-virtual {p1, v0}, Landroid/app/Activity;->setTaskDescription(Landroid/app/ActivityManager$TaskDescription;)V

    .line 41
    if-eqz p5, :cond_0

    const/high16 v0, -0x1000000

    .line 42
    :goto_0
    invoke-virtual {p1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Window;->setStatusBarColor(I)V

    .line 43
    return-void

    .line 41
    :cond_0
    invoke-direct {p0, p4}, Lcom/google/android/apps/chrome/utilities/DocumentUtilities;->computeStatusBarColor(I)I

    move-result v0

    goto :goto_0
.end method
