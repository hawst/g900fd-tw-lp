.class Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimationLandscape;
.super Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation;
.source "StackAnimationLandscape.java"


# direct methods
.method public constructor <init>(FFFFFF)V
    .locals 0

    .prologue
    .line 36
    invoke-direct/range {p0 .. p6}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation;-><init>(FFFFFF)V

    .line 38
    return-void
.end method


# virtual methods
.method protected addTiltScrollAnimation(Lcom/google/android/apps/chrome/ChromeAnimation;Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;FII)V
    .locals 10

    .prologue
    .line 218
    sget-object v3, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;->TILTY:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

    invoke-virtual {p2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getTiltY()F

    move-result v4

    int-to-long v6, p4

    int-to-long v8, p5

    move-object v1, p1

    move-object v2, p2

    move v5, p3

    invoke-static/range {v1 .. v9}, Lcom/google/android/apps/chrome/ChromeAnimation$AnimatableAnimation;->addAnimation(Lcom/google/android/apps/chrome/ChromeAnimation;Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJ)V

    .line 219
    return-void
.end method

.method protected createEnterStackAnimatorSet([Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;IIF)Lcom/google/android/apps/chrome/ChromeAnimation;
    .locals 15

    .prologue
    .line 43
    new-instance v3, Lcom/google/android/apps/chrome/ChromeAnimation;

    invoke-direct {v3}, Lcom/google/android/apps/chrome/ChromeAnimation;-><init>()V

    .line 44
    const/4 v2, 0x0

    move/from16 v0, p4

    invoke-static {v2, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->screenToScroll(FF)F

    move-result v12

    .line 46
    const/4 v2, 0x0

    :goto_0
    move-object/from16 v0, p1

    array-length v4, v0

    if-ge v2, v4, :cond_3

    .line 47
    aget-object v13, p1, v2

    .line 49
    invoke-virtual {v13}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->resetOffset()V

    .line 50
    const v4, 0x3f666666    # 0.9f

    invoke-virtual {v13, v4}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->setScale(F)V

    .line 51
    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {v13, v4}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->setAlpha(F)V

    .line 52
    invoke-virtual {v13}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getLayoutTab()Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setToolbarAlpha(F)V

    .line 53
    invoke-virtual {v13}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getLayoutTab()Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v4

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-virtual {v4, v5}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setBorderScale(F)V

    .line 55
    mul-int v4, v2, p3

    int-to-float v4, v4

    move/from16 v0, p4

    invoke-static {v4, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->screenToScroll(FF)F

    move-result v14

    .line 57
    invoke-virtual {v13}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getLayoutTab()Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v4

    sget-object v5, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;->MAX_CONTENT_HEIGHT:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

    invoke-virtual {v13}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getLayoutTab()Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getUnclampedOriginalContentHeight()F

    move-result v6

    iget v7, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimationLandscape;->mHeightMinusTopControls:F

    const-wide/16 v8, 0x12c

    const-wide/16 v10, 0x0

    invoke-static/range {v3 .. v11}, Lcom/google/android/apps/chrome/ChromeAnimation$AnimatableAnimation;->addAnimation(Lcom/google/android/apps/chrome/ChromeAnimation;Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJ)V

    .line 61
    move/from16 v0, p2

    if-ge v2, v0, :cond_0

    .line 62
    sget-object v5, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab$Property;->SCROLL_OFFSET:Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab$Property;

    const-wide/16 v8, 0x12c

    const-wide/16 v10, 0x0

    move-object v4, v13

    move v6, v12

    move v7, v14

    invoke-static/range {v3 .. v11}, Lcom/google/android/apps/chrome/ChromeAnimation$AnimatableAnimation;->addAnimation(Lcom/google/android/apps/chrome/ChromeAnimation;Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJ)V

    .line 46
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 64
    :cond_0
    move/from16 v0, p2

    if-le v2, v0, :cond_2

    .line 65
    invoke-virtual {v13, v14}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->setScrollOffset(F)V

    .line 66
    sget-object v5, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab$Property;->X_IN_STACK_OFFSET:Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab$Property;

    iget v4, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimationLandscape;->mWidth:F

    iget v6, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimationLandscape;->mHeight:F

    cmpl-float v4, v4, v6

    if-lez v4, :cond_1

    invoke-static {}, Lorg/chromium/ui/base/LocalizationUtils;->isLayoutRtl()Z

    move-result v4

    if-eqz v4, :cond_1

    iget v4, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimationLandscape;->mWidth:F

    neg-float v6, v4

    :goto_2
    const/4 v7, 0x0

    const-wide/16 v8, 0x12c

    const-wide/16 v10, 0x0

    move-object v4, v13

    invoke-static/range {v3 .. v11}, Lcom/google/android/apps/chrome/ChromeAnimation$AnimatableAnimation;->addAnimation(Lcom/google/android/apps/chrome/ChromeAnimation;Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJ)V

    goto :goto_1

    :cond_1
    iget v6, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimationLandscape;->mWidth:F

    goto :goto_2

    .line 74
    :cond_2
    invoke-virtual {v13, v14}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->setScrollOffset(F)V

    .line 75
    sget-object v5, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab$Property;->X_IN_STACK_INFLUENCE:Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab$Property;

    const/4 v6, 0x0

    const/high16 v7, 0x3f800000    # 1.0f

    const-wide/16 v8, 0xc8

    const-wide/16 v10, 0x0

    move-object v4, v13

    invoke-static/range {v3 .. v11}, Lcom/google/android/apps/chrome/ChromeAnimation$AnimatableAnimation;->addAnimation(Lcom/google/android/apps/chrome/ChromeAnimation;Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJ)V

    .line 77
    sget-object v5, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab$Property;->SCALE:Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab$Property;

    const/high16 v6, 0x3f800000    # 1.0f

    const v7, 0x3f666666    # 0.9f

    const-wide/16 v8, 0xc8

    const-wide/16 v10, 0x0

    move-object v4, v13

    invoke-static/range {v3 .. v11}, Lcom/google/android/apps/chrome/ChromeAnimation$AnimatableAnimation;->addAnimation(Lcom/google/android/apps/chrome/ChromeAnimation;Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJ)V

    .line 79
    invoke-virtual {v13}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getLayoutTab()Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v4

    sget-object v5, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;->TOOLBAR_ALPHA:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v7, 0x0

    const-wide/16 v8, 0x64

    const-wide/16 v10, 0x64

    invoke-static/range {v3 .. v11}, Lcom/google/android/apps/chrome/ChromeAnimation$AnimatableAnimation;->addAnimation(Lcom/google/android/apps/chrome/ChromeAnimation;Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJ)V

    .line 81
    invoke-virtual {v13}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getLayoutTab()Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v4

    sget-object v5, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;->TOOLBAR_Y_OFFSET:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

    const/4 v6, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimationLandscape;->getToolbarOffsetToLineUpWithBorder()F

    move-result v7

    const-wide/16 v8, 0xc8

    const-wide/16 v10, 0x0

    invoke-static/range {v3 .. v11}, Lcom/google/android/apps/chrome/ChromeAnimation$AnimatableAnimation;->addAnimation(Lcom/google/android/apps/chrome/ChromeAnimation;Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJ)V

    .line 84
    invoke-virtual {v13}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getLayoutTab()Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v4

    sget-object v5, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;->SIDE_BORDER_SCALE:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

    const/4 v6, 0x0

    const/high16 v7, 0x3f800000    # 1.0f

    const-wide/16 v8, 0xc8

    const-wide/16 v10, 0x0

    invoke-static/range {v3 .. v11}, Lcom/google/android/apps/chrome/ChromeAnimation$AnimatableAnimation;->addAnimation(Lcom/google/android/apps/chrome/ChromeAnimation;Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJ)V

    goto :goto_1

    .line 89
    :cond_3
    return-object v3
.end method

.method protected createReachTopAnimatorSet([Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;F)Lcom/google/android/apps/chrome/ChromeAnimation;
    .locals 11

    .prologue
    .line 189
    new-instance v1, Lcom/google/android/apps/chrome/ChromeAnimation;

    invoke-direct {v1}, Lcom/google/android/apps/chrome/ChromeAnimation;-><init>()V

    .line 191
    const/4 v2, 0x0

    .line 192
    const/4 v0, 0x0

    move v10, v2

    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_0

    .line 193
    aget-object v2, p1, v0

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getLayoutTab()Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getX()F

    move-result v2

    cmpl-float v2, v10, v2

    if-gez v2, :cond_0

    .line 194
    aget-object v2, p1, v0

    sget-object v3, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab$Property;->SCROLL_OFFSET:Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab$Property;

    aget-object v4, p1, v0

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getScrollOffset()F

    move-result v4

    invoke-static {v10, p2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->screenToScroll(FF)F

    move-result v5

    const-wide/16 v6, 0x190

    const-wide/16 v8, 0x0

    invoke-static/range {v1 .. v9}, Lcom/google/android/apps/chrome/ChromeAnimation$AnimatableAnimation;->addAnimation(Lcom/google/android/apps/chrome/ChromeAnimation;Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJ)V

    .line 199
    aget-object v2, p1, v0

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getLayoutTab()Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getScaledContentWidth()F

    move-result v2

    add-float/2addr v2, v10

    .line 192
    add-int/lit8 v0, v0, 0x1

    move v10, v2

    goto :goto_0

    .line 202
    :cond_0
    return-object v1
.end method

.method protected createTabFocusedAnimatorSet([Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;IIF)Lcom/google/android/apps/chrome/ChromeAnimation;
    .locals 16

    .prologue
    .line 95
    new-instance v3, Lcom/google/android/apps/chrome/ChromeAnimation;

    invoke-direct {v3}, Lcom/google/android/apps/chrome/ChromeAnimation;-><init>()V

    .line 96
    const/4 v2, 0x0

    move v14, v2

    :goto_0
    move-object/from16 v0, p1

    array-length v2, v0

    if-ge v14, v2, :cond_5

    .line 97
    aget-object v15, p1, v14

    .line 98
    invoke-virtual {v15}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getLayoutTab()Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v4

    .line 100
    const/4 v5, 0x0

    const/16 v6, 0x190

    const/4 v7, 0x0

    move-object/from16 v2, p0

    invoke-virtual/range {v2 .. v7}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimationLandscape;->addTiltScrollAnimation(Lcom/google/android/apps/chrome/ChromeAnimation;Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;FII)V

    .line 102
    sget-object v7, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab$Property;->DISCARD_AMOUNT:Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab$Property;

    invoke-virtual {v15}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getDiscardAmount()F

    move-result v8

    const/4 v9, 0x0

    const-wide/16 v10, 0x190

    const-wide/16 v12, 0x0

    move-object v5, v3

    move-object v6, v15

    invoke-static/range {v5 .. v13}, Lcom/google/android/apps/chrome/ChromeAnimation$AnimatableAnimation;->addAnimation(Lcom/google/android/apps/chrome/ChromeAnimation;Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJ)V

    .line 105
    move/from16 v0, p2

    if-ge v14, v0, :cond_0

    .line 107
    sget-object v5, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab$Property;->SCROLL_OFFSET:Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab$Property;

    invoke-virtual {v15}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getScrollOffset()F

    move-result v6

    const/4 v2, 0x0

    invoke-virtual {v15}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getScrollOffset()F

    move-result v4

    move-object/from16 v0, p0

    iget v7, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimationLandscape;->mWidth:F

    sub-float/2addr v4, v7

    move/from16 v0, p3

    int-to-float v7, v0

    sub-float/2addr v4, v7

    invoke-static {v2, v4}, Ljava/lang/Math;->max(FF)F

    move-result v7

    const-wide/16 v8, 0x190

    const-wide/16 v10, 0x0

    move-object v4, v15

    invoke-static/range {v3 .. v11}, Lcom/google/android/apps/chrome/ChromeAnimation$AnimatableAnimation;->addAnimation(Lcom/google/android/apps/chrome/ChromeAnimation;Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJ)V

    .line 96
    :goto_1
    add-int/lit8 v2, v14, 0x1

    move v14, v2

    goto :goto_0

    .line 110
    :cond_0
    move/from16 v0, p2

    if-le v14, v0, :cond_3

    .line 113
    invoke-virtual {v4}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getX()F

    move-result v2

    .line 114
    invoke-static {}, Lorg/chromium/ui/base/LocalizationUtils;->isLayoutRtl()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getScaledContentWidth()F

    move-result v4

    add-float/2addr v2, v4

    .line 117
    :goto_2
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimationLandscape;->mWidth:F

    invoke-static {v2, v4, v5}, Lorg/chromium/chrome/browser/util/MathUtils;->clamp(FFF)F

    move-result v2

    .line 118
    const/high16 v4, 0x42c80000    # 100.0f

    mul-float/2addr v2, v4

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimationLandscape;->mWidth:F

    div-float v4, v2, v4

    .line 119
    sget-object v5, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab$Property;->X_IN_STACK_OFFSET:Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab$Property;

    invoke-virtual {v15}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getXInStackOffset()F

    move-result v6

    invoke-virtual {v15}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getXInStackOffset()F

    move-result v7

    invoke-static {}, Lorg/chromium/ui/base/LocalizationUtils;->isLayoutRtl()Z

    move-result v2

    if-eqz v2, :cond_2

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimationLandscape;->mWidth:F

    neg-float v2, v2

    :goto_3
    add-float/2addr v7, v2

    const-wide/16 v8, 0x190

    float-to-long v10, v4

    sub-long/2addr v8, v10

    float-to-long v10, v4

    move-object v4, v15

    invoke-static/range {v3 .. v11}, Lcom/google/android/apps/chrome/ChromeAnimation$AnimatableAnimation;->addAnimation(Lcom/google/android/apps/chrome/ChromeAnimation;Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJ)V

    goto :goto_1

    .line 114
    :cond_1
    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimationLandscape;->mWidth:F

    sub-float v2, v4, v2

    goto :goto_2

    .line 119
    :cond_2
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimationLandscape;->mWidth:F

    goto :goto_3

    .line 130
    :cond_3
    const/4 v2, 0x0

    invoke-virtual {v15, v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->setXOutOfStack(F)V

    .line 131
    const/4 v2, 0x0

    invoke-virtual {v15, v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->setYOutOfStack(F)V

    .line 132
    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v4, v2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setBorderScale(F)V

    .line 134
    sget-object v7, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab$Property;->SCROLL_OFFSET:Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab$Property;

    invoke-virtual {v15}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getScrollOffset()F

    move-result v8

    const/4 v2, 0x0

    move/from16 v0, p4

    invoke-static {v2, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->screenToScroll(FF)F

    move-result v9

    const-wide/16 v10, 0x190

    const-wide/16 v12, 0x0

    move-object v5, v3

    move-object v6, v15

    invoke-static/range {v5 .. v13}, Lcom/google/android/apps/chrome/ChromeAnimation$AnimatableAnimation;->addAnimation(Lcom/google/android/apps/chrome/ChromeAnimation;Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJ)V

    .line 137
    sget-object v7, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab$Property;->SCALE:Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab$Property;

    invoke-virtual {v15}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getScale()F

    move-result v8

    const/high16 v9, 0x3f800000    # 1.0f

    const-wide/16 v10, 0x190

    const-wide/16 v12, 0x0

    move-object v5, v3

    move-object v6, v15

    invoke-static/range {v5 .. v13}, Lcom/google/android/apps/chrome/ChromeAnimation$AnimatableAnimation;->addAnimation(Lcom/google/android/apps/chrome/ChromeAnimation;Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJ)V

    .line 139
    sget-object v7, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab$Property;->X_IN_STACK_INFLUENCE:Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab$Property;

    invoke-virtual {v15}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getXInStackInfluence()F

    move-result v8

    const/4 v9, 0x0

    const-wide/16 v10, 0x190

    const-wide/16 v12, 0x0

    move-object v5, v3

    move-object v6, v15

    invoke-static/range {v5 .. v13}, Lcom/google/android/apps/chrome/ChromeAnimation$AnimatableAnimation;->addAnimation(Lcom/google/android/apps/chrome/ChromeAnimation;Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJ)V

    .line 141
    sget-object v7, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab$Property;->Y_IN_STACK_INFLUENCE:Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab$Property;

    invoke-virtual {v15}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getYInStackInfluence()F

    move-result v8

    const/4 v9, 0x0

    const-wide/16 v10, 0xc8

    const-wide/16 v12, 0x0

    move-object v5, v3

    move-object v6, v15

    invoke-static/range {v5 .. v13}, Lcom/google/android/apps/chrome/ChromeAnimation$AnimatableAnimation;->addAnimation(Lcom/google/android/apps/chrome/ChromeAnimation;Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJ)V

    .line 144
    invoke-virtual {v15}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getLayoutTab()Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v6

    sget-object v7, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;->MAX_CONTENT_HEIGHT:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

    invoke-virtual {v15}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getLayoutTab()Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getMaxContentHeight()F

    move-result v8

    invoke-virtual {v15}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getLayoutTab()Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getUnclampedOriginalContentHeight()F

    move-result v9

    const-wide/16 v10, 0x190

    const-wide/16 v12, 0x0

    move-object v5, v3

    invoke-static/range {v5 .. v13}, Lcom/google/android/apps/chrome/ChromeAnimation$AnimatableAnimation;->addAnimation(Lcom/google/android/apps/chrome/ChromeAnimation;Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJ)V

    .line 148
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimationLandscape;->mHeight:F

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimationLandscape;->mHeightMinusTopControls:F

    sub-float/2addr v2, v5

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimationLandscape;->mBorderTopHeight:F

    sub-float/2addr v2, v5

    invoke-virtual {v15, v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->setYOutOfStack(F)V

    .line 150
    invoke-virtual {v4}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->shouldStall()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 151
    sget-object v5, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;->SATURATION:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v7, 0x0

    const-wide/16 v8, 0xc8

    const-wide/16 v10, 0x0

    invoke-static/range {v3 .. v11}, Lcom/google/android/apps/chrome/ChromeAnimation$AnimatableAnimation;->addAnimation(Lcom/google/android/apps/chrome/ChromeAnimation;Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJ)V

    .line 154
    :cond_4
    invoke-virtual {v15}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getLayoutTab()Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v2

    sget-object v5, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;->TOOLBAR_ALPHA:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getToolbarAlpha()F

    move-result v6

    const/high16 v7, 0x3f800000    # 1.0f

    const-wide/16 v8, 0xfa

    const-wide/16 v10, 0x0

    move-object v4, v2

    invoke-static/range {v3 .. v11}, Lcom/google/android/apps/chrome/ChromeAnimation$AnimatableAnimation;->addAnimation(Lcom/google/android/apps/chrome/ChromeAnimation;Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJ)V

    .line 156
    invoke-virtual {v15}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getLayoutTab()Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v4

    sget-object v5, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;->TOOLBAR_Y_OFFSET:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimationLandscape;->getToolbarOffsetToLineUpWithBorder()F

    move-result v6

    const/4 v7, 0x0

    const-wide/16 v8, 0xfa

    const-wide/16 v10, 0x0

    invoke-static/range {v3 .. v11}, Lcom/google/android/apps/chrome/ChromeAnimation$AnimatableAnimation;->addAnimation(Lcom/google/android/apps/chrome/ChromeAnimation;Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJ)V

    .line 159
    invoke-virtual {v15}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getLayoutTab()Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v4

    sget-object v5, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;->SIDE_BORDER_SCALE:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v7, 0x0

    const-wide/16 v8, 0xfa

    const-wide/16 v10, 0x0

    invoke-static/range {v3 .. v11}, Lcom/google/android/apps/chrome/ChromeAnimation$AnimatableAnimation;->addAnimation(Lcom/google/android/apps/chrome/ChromeAnimation;Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJ)V

    goto/16 :goto_1

    .line 164
    :cond_5
    return-object v3
.end method

.method protected createViewMoreAnimatorSet([Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;I)Lcom/google/android/apps/chrome/ChromeAnimation;
    .locals 11

    .prologue
    .line 170
    new-instance v1, Lcom/google/android/apps/chrome/ChromeAnimation;

    invoke-direct {v1}, Lcom/google/android/apps/chrome/ChromeAnimation;-><init>()V

    .line 172
    add-int/lit8 v0, p2, 0x1

    array-length v2, p1

    if-lt v0, v2, :cond_1

    .line 184
    :cond_0
    return-object v1

    .line 174
    :cond_1
    aget-object v0, p1, p2

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getScrollOffset()F

    move-result v0

    add-int/lit8 v2, p2, 0x1

    aget-object v2, p1, v2

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getScrollOffset()F

    move-result v2

    sub-float/2addr v0, v2

    aget-object v2, p1, p2

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getLayoutTab()Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getScaledContentWidth()F

    move-result v2

    const/high16 v3, 0x3f400000    # 0.75f

    mul-float/2addr v2, v3

    add-float/2addr v0, v2

    .line 177
    const/high16 v2, 0x43480000    # 200.0f

    invoke-static {v2, v0}, Ljava/lang/Math;->max(FF)F

    move-result v10

    .line 178
    add-int/lit8 v0, p2, 0x1

    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_0

    .line 179
    aget-object v2, p1, v0

    sget-object v3, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab$Property;->SCROLL_OFFSET:Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab$Property;

    aget-object v4, p1, v0

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getScrollOffset()F

    move-result v4

    aget-object v5, p1, v0

    invoke-virtual {v5}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getScrollOffset()F

    move-result v5

    add-float/2addr v5, v10

    const-wide/16 v6, 0x190

    const-wide/16 v8, 0x0

    invoke-static/range {v1 .. v9}, Lcom/google/android/apps/chrome/ChromeAnimation$AnimatableAnimation;->addAnimation(Lcom/google/android/apps/chrome/ChromeAnimation;Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJ)V

    .line 178
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method protected getScreenPositionInScrollDirection(Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;)F
    .locals 1

    .prologue
    .line 212
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getLayoutTab()Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getX()F

    move-result v0

    return v0
.end method

.method protected getScreenSizeInScrollDirection()F
    .locals 1

    .prologue
    .line 223
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimationLandscape;->mWidth:F

    return v0
.end method

.method protected getTabCreationDirection()I
    .locals 1

    .prologue
    .line 228
    const/4 v0, 0x1

    return v0
.end method

.method protected isDefaultDiscardDirectionPositive()Z
    .locals 1

    .prologue
    .line 207
    const/4 v0, 0x1

    return v0
.end method
