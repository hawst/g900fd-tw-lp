.class Lcom/google/android/apps/chrome/enhancedbookmark/ChromeEnhancedBookmarkBridge$1;
.super Landroid/util/LruCache;
.source "ChromeEnhancedBookmarkBridge.java"


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/enhancedbookmark/ChromeEnhancedBookmarkBridge;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/enhancedbookmark/ChromeEnhancedBookmarkBridge;I)V
    .locals 0

    .prologue
    .line 44
    iput-object p1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/ChromeEnhancedBookmarkBridge$1;->this$0:Lcom/google/android/apps/chrome/enhancedbookmark/ChromeEnhancedBookmarkBridge;

    invoke-direct {p0, p2}, Landroid/util/LruCache;-><init>(I)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic sizeOf(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 44
    check-cast p1, Ljava/lang/String;

    check-cast p2, Landroid/util/Pair;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/chrome/enhancedbookmark/ChromeEnhancedBookmarkBridge$1;->sizeOf(Ljava/lang/String;Landroid/util/Pair;)I

    move-result v0

    return v0
.end method

.method protected sizeOf(Ljava/lang/String;Landroid/util/Pair;)I
    .locals 2

    .prologue
    .line 47
    iget-object v0, p2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    iget-object v0, p2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getByteCount()I

    move-result v0

    add-int/2addr v0, v1

    return v0
.end method
