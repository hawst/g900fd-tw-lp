.class public Lcom/google/android/apps/chrome/ntp/NewTabPageUma;
.super Ljava/lang/Object;
.source "NewTabPageUma.java"


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final ACTION_OPENED_BOOKMARK:I = 0x5

.field public static final ACTION_OPENED_FOREIGN_SESSION:I = 0x6

.field public static final ACTION_OPENED_MOST_VISITED_ENTRY:I = 0x3

.field public static final ACTION_OPENED_RECENTLY_CLOSED_ENTRY:I = 0x4


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const-class v0, Lcom/google/android/apps/chrome/ntp/NewTabPageUma;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/ntp/NewTabPageUma;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static recordAction(I)V
    .locals 2

    .prologue
    const/4 v1, 0x7

    .line 42
    sget-boolean v0, Lcom/google/android/apps/chrome/ntp/NewTabPageUma;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-gez p0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 43
    :cond_0
    sget-boolean v0, Lcom/google/android/apps/chrome/ntp/NewTabPageUma;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    if-lt p0, v1, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 44
    :cond_1
    packed-switch p0, :pswitch_data_0

    .line 56
    :goto_0
    invoke-static {p0, v1}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->ntpAction(II)V

    .line 62
    return-void

    .line 46
    :pswitch_0
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->ntpMostVisited()V

    goto :goto_0

    .line 49
    :pswitch_1
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->ntpRecentlyClosed()V

    goto :goto_0

    .line 52
    :pswitch_2
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->ntpBookmark()V

    goto :goto_0

    .line 55
    :pswitch_3
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->ntpForeignSession()V

    goto :goto_0

    .line 44
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static recordOmniboxNavigation(Ljava/lang/String;I)V
    .locals 2

    .prologue
    .line 70
    and-int/lit16 v0, p1, 0xff

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    .line 72
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/NewTabPageUma;->recordAction(I)V

    .line 78
    :goto_0
    return-void

    .line 73
    :cond_0
    invoke-static {p0}, Lorg/chromium/chrome/browser/UrlUtilities;->nativeIsGoogleHomePageUrl(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 74
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/NewTabPageUma;->recordAction(I)V

    goto :goto_0

    .line 76
    :cond_1
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/NewTabPageUma;->recordAction(I)V

    goto :goto_0
.end method
