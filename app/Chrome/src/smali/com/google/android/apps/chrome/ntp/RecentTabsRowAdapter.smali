.class public Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;
.super Landroid/widget/BaseExpandableListAdapter;
.source "RecentTabsRowAdapter.java"


# instance fields
.field private final mActivity:Landroid/app/Activity;

.field private final mDefaultFavicon:Landroid/graphics/drawable/Drawable;

.field private final mFaviconCache:Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$FaviconCache;

.field private final mFaviconSize:I

.field private final mGroups:Ljava/util/ArrayList;

.field private final mInitializationTimestamp:J

.field private final mInvisibleSeparatorGroup:Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$SeparatorGroup;

.field private final mRecentTabsManager:Lcom/google/android/apps/chrome/ntp/RecentTabsManager;

.field private final mRecentlyClosedTabsGroup:Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$RecentlyClosedTabsGroup;

.field private final mVisibleSeparatorGroup:Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$SeparatorGroup;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/apps/chrome/ntp/RecentTabsManager;)V
    .locals 4

    .prologue
    .line 719
    invoke-direct {p0}, Landroid/widget/BaseExpandableListAdapter;-><init>()V

    .line 59
    new-instance v0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$RecentlyClosedTabsGroup;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$RecentlyClosedTabsGroup;-><init>(Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->mRecentlyClosedTabsGroup:Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$RecentlyClosedTabsGroup;

    .line 60
    new-instance v0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$SeparatorGroup;

    const/4 v1, 0x1

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$SeparatorGroup;-><init>(Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;Z)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->mVisibleSeparatorGroup:Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$SeparatorGroup;

    .line 61
    new-instance v0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$SeparatorGroup;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$SeparatorGroup;-><init>(Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;Z)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->mInvisibleSeparatorGroup:Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$SeparatorGroup;

    .line 720
    iput-object p1, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->mActivity:Landroid/app/Activity;

    .line 721
    iput-object p2, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->mRecentTabsManager:Lcom/google/android/apps/chrome/ntp/RecentTabsManager;

    .line 722
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->mGroups:Ljava/util/ArrayList;

    .line 723
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->mInitializationTimestamp:J

    .line 725
    const/16 v0, 0x100

    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->buildFaviconCache(I)Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$FaviconCache;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->mFaviconCache:Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$FaviconCache;

    .line 727
    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 728
    sget v1, Lcom/google/android/apps/chrome/R$drawable;->default_favicon:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->mDefaultFavicon:Landroid/graphics/drawable/Drawable;

    .line 729
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->mDefaultFavicon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->mFaviconSize:I

    .line 730
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->mActivity:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;)J
    .locals 2

    .prologue
    .line 44
    iget-wide v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->mInitializationTimestamp:J

    return-wide v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;)Lcom/google/android/apps/chrome/ntp/RecentTabsManager;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->mRecentTabsManager:Lcom/google/android/apps/chrome/ntp/RecentTabsManager;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$ViewHolder;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->loadLocalFavicon(Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$ViewHolder;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$ViewHolder;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->loadSyncedFavicon(Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$ViewHolder;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;Landroid/graphics/Bitmap;)Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->faviconDrawable(Landroid/graphics/Bitmap;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;)Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->mDefaultFavicon:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;)Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$FaviconCache;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->mFaviconCache:Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$FaviconCache;

    return-object v0
.end method

.method private addGroup(Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$Group;)V
    .locals 2

    .prologue
    .line 858
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->mActivity:Landroid/app/Activity;

    invoke-static {v0}, Lorg/chromium/ui/base/DeviceFormFactor;->isTablet(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 859
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->mGroups:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 867
    :goto_0
    return-void

    .line 861
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->mGroups:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 862
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->mGroups:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->mInvisibleSeparatorGroup:Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$SeparatorGroup;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 864
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->mGroups:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 865
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->mGroups:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->mInvisibleSeparatorGroup:Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$SeparatorGroup;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private static buildFaviconCache(I)Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$FaviconCache;
    .locals 1

    .prologue
    .line 733
    new-instance v0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$FaviconCache;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$FaviconCache;-><init>(I)V

    return-object v0
.end method

.method private faviconDrawable(Landroid/graphics/Bitmap;)Landroid/graphics/drawable/Drawable;
    .locals 5

    .prologue
    .line 747
    if-nez p1, :cond_0

    const/4 v0, 0x0

    .line 748
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget v2, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->mFaviconSize:I

    iget v3, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->mFaviconSize:I

    const/4 v4, 0x1

    invoke-static {p1, v2, v3, v4}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method private loadLocalFavicon(Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$ViewHolder;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 765
    if-nez p2, :cond_1

    .line 767
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->mDefaultFavicon:Landroid/graphics/drawable/Drawable;

    .line 787
    :cond_0
    :goto_0
    iget-object v1, p1, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$ViewHolder;->textView:Landroid/widget/TextView;

    invoke-static {v1, v0, v3, v3, v3}, Lorg/chromium/base/ApiCompatibilityUtils;->setCompoundDrawablesRelativeWithIntrinsicBounds(Landroid/widget/TextView;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 789
    return-void

    .line 769
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->mFaviconCache:Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$FaviconCache;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$FaviconCache;->getLocalFaviconImage(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 770
    if-nez v0, :cond_0

    .line 771
    new-instance v0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$1;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$1;-><init>(Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$ViewHolder;Ljava/lang/String;)V

    .line 782
    iput-object v0, p1, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$ViewHolder;->imageCallback:Lorg/chromium/chrome/browser/favicon/FaviconHelper$FaviconImageCallback;

    .line 783
    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->mRecentTabsManager:Lcom/google/android/apps/chrome/ntp/RecentTabsManager;

    iget v2, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->mFaviconSize:I

    invoke-virtual {v1, p2, v2, v0}, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->getLocalFaviconForUrl(Ljava/lang/String;ILorg/chromium/chrome/browser/favicon/FaviconHelper$FaviconImageCallback;)Z

    .line 784
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->mDefaultFavicon:Landroid/graphics/drawable/Drawable;

    goto :goto_0
.end method

.method private loadSyncedFavicon(Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$ViewHolder;Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 753
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->mFaviconCache:Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$FaviconCache;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$FaviconCache;->getSyncedFaviconImage(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 754
    if-nez v0, :cond_1

    .line 755
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->mRecentTabsManager:Lcom/google/android/apps/chrome/ntp/RecentTabsManager;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->getSyncedFaviconImageForURL(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->faviconDrawable(Landroid/graphics/Bitmap;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 756
    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->mDefaultFavicon:Landroid/graphics/drawable/Drawable;

    .line 757
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->mFaviconCache:Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$FaviconCache;

    invoke-virtual {v1, p2, v0}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$FaviconCache;->putSycnedFaviconImage(Ljava/lang/String;Landroid/graphics/drawable/Drawable;)V

    .line 759
    :cond_1
    iget-object v1, p1, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$ViewHolder;->textView:Landroid/widget/TextView;

    invoke-static {v1, v0, v2, v2, v2}, Lorg/chromium/base/ApiCompatibilityUtils;->setCompoundDrawablesRelativeWithIntrinsicBounds(Landroid/widget/TextView;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 761
    return-void
.end method


# virtual methods
.method public getChild(II)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 833
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->getGroup(I)Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$Group;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$Group;->getChild(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getChildId(II)J
    .locals 2

    .prologue
    .line 828
    int-to-long v0, p2

    return-wide v0
.end method

.method public getChildType(II)I
    .locals 1

    .prologue
    .line 902
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->mGroups:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$Group;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$Group;->getChildType()Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$ChildType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$ChildType;->ordinal()I

    move-result v0

    return v0
.end method

.method public getChildTypeCount()I
    .locals 1

    .prologue
    .line 907
    invoke-static {}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$ChildType;->values()[Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$ChildType;

    move-result-object v0

    array-length v0, v0

    return v0
.end method

.method public getChildView(IIZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 794
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->getGroup(I)Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$Group;

    move-result-object v0

    invoke-virtual {v0, p2, p3, p4, p5}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$Group;->getChildView(IZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public getChildrenCount(I)I
    .locals 1

    .prologue
    .line 823
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->getGroup(I)Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$Group;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$Group;->getChildrenCount()I

    move-result v0

    return v0
.end method

.method public getGroup(I)Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$Group;
    .locals 1

    .prologue
    .line 811
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->mGroups:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$Group;

    return-object v0
.end method

.method public bridge synthetic getGroup(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 44
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->getGroup(I)Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$Group;

    move-result-object v0

    return-object v0
.end method

.method public getGroupCount()I
    .locals 1

    .prologue
    .line 801
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->mGroups:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getGroupId(I)J
    .locals 2

    .prologue
    .line 806
    int-to-long v0, p1

    return-wide v0
.end method

.method public getGroupType(I)I
    .locals 1

    .prologue
    .line 849
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->getGroup(I)Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$Group;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$Group;->getGroupType()Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$GroupType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$GroupType;->ordinal()I

    move-result v0

    return v0
.end method

.method public getGroupTypeCount()I
    .locals 1

    .prologue
    .line 854
    invoke-static {}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$GroupType;->values()[Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$GroupType;

    move-result-object v0

    array-length v0, v0

    return v0
.end method

.method public getGroupView(IZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 817
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->getGroup(I)Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$Group;

    move-result-object v0

    invoke-virtual {v0, p2, p3, p4}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$Group;->getGroupView(ZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public hasStableIds()Z
    .locals 1

    .prologue
    .line 844
    const/4 v0, 0x0

    return v0
.end method

.method public isChildSelectable(II)Z
    .locals 1

    .prologue
    .line 838
    const/4 v0, 0x1

    return v0
.end method

.method public notifyDataSetChanged()V
    .locals 3

    .prologue
    .line 871
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->mGroups:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 872
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->mRecentTabsManager:Lcom/google/android/apps/chrome/ntp/RecentTabsManager;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->getCurrentlyOpenTabs()Ljava/util/List;

    move-result-object v0

    .line 873
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 874
    new-instance v1, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$CurrentlyOpenTabsGroup;

    invoke-direct {v1, p0, v0}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$CurrentlyOpenTabsGroup;-><init>(Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;Ljava/util/List;)V

    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->addGroup(Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$Group;)V

    .line 876
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->mRecentlyClosedTabsGroup:Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$RecentlyClosedTabsGroup;

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->addGroup(Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$Group;)V

    .line 877
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->mRecentTabsManager:Lcom/google/android/apps/chrome/ntp/RecentTabsManager;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->getForeignSessions()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSession;

    .line 878
    new-instance v2, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$ForeignSessionGroup;

    invoke-direct {v2, p0, v0}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$ForeignSessionGroup;-><init>(Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSession;)V

    invoke-direct {p0, v2}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->addGroup(Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$Group;)V

    goto :goto_0

    .line 880
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->mRecentTabsManager:Lcom/google/android/apps/chrome/ntp/RecentTabsManager;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->getSnapshotDocuments()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 881
    new-instance v0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$SnapshotDocumentsGroup;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$SnapshotDocumentsGroup;-><init>(Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->addGroup(Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$Group;)V

    .line 883
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->mRecentTabsManager:Lcom/google/android/apps/chrome/ntp/RecentTabsManager;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->shouldDisplaySyncPromo()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 884
    new-instance v0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$SyncPromoGroup;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$SyncPromoGroup;-><init>(Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->addGroup(Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$Group;)V

    .line 888
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->mGroups:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->mRecentlyClosedTabsGroup:Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$RecentlyClosedTabsGroup;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 889
    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->mActivity:Landroid/app/Activity;

    invoke-static {v1}, Lorg/chromium/ui/base/DeviceFormFactor;->isTablet(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 890
    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->mGroups:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x2

    if-eq v0, v1, :cond_4

    .line 891
    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->mGroups:Ljava/util/ArrayList;

    add-int/lit8 v0, v0, 0x1

    iget-object v2, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->mVisibleSeparatorGroup:Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$SeparatorGroup;

    invoke-virtual {v1, v0, v2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 897
    :cond_4
    :goto_1
    invoke-super {p0}, Landroid/widget/BaseExpandableListAdapter;->notifyDataSetChanged()V

    .line 898
    return-void

    .line 893
    :cond_5
    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->mGroups:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-eq v0, v1, :cond_4

    .line 894
    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->mGroups:Ljava/util/ArrayList;

    add-int/lit8 v0, v0, 0x1

    iget-object v2, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->mVisibleSeparatorGroup:Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$SeparatorGroup;

    invoke-virtual {v1, v0, v2}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    goto :goto_1
.end method
