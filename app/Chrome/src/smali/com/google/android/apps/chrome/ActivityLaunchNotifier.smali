.class public Lcom/google/android/apps/chrome/ActivityLaunchNotifier;
.super Ljava/lang/Object;
.source "ActivityLaunchNotifier.java"


# static fields
.field private static sActivityLaunchNotifier:Lcom/google/android/apps/chrome/ActivityLaunchNotifier;


# instance fields
.field private mListeners:Ljava/util/List;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/ActivityLaunchNotifier;->mListeners:Ljava/util/List;

    .line 63
    return-void
.end method

.method public static getInstance()Lcom/google/android/apps/chrome/ActivityLaunchNotifier;
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lcom/google/android/apps/chrome/ActivityLaunchNotifier;->sActivityLaunchNotifier:Lcom/google/android/apps/chrome/ActivityLaunchNotifier;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/chrome/ActivityLaunchNotifier;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/ActivityLaunchNotifier;-><init>()V

    sput-object v0, Lcom/google/android/apps/chrome/ActivityLaunchNotifier;->sActivityLaunchNotifier:Lcom/google/android/apps/chrome/ActivityLaunchNotifier;

    .line 28
    :cond_0
    sget-object v0, Lcom/google/android/apps/chrome/ActivityLaunchNotifier;->sActivityLaunchNotifier:Lcom/google/android/apps/chrome/ActivityLaunchNotifier;

    return-object v0
.end method


# virtual methods
.method public addListener(Lcom/google/android/apps/chrome/ActivityLaunchNotifier$ActivityLaunchListener;)V
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/apps/chrome/ActivityLaunchNotifier;->mListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 50
    return-void
.end method

.method public onLaunchActivity(Landroid/content/Intent;Landroid/view/Window;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 72
    move v1, v0

    move v2, v0

    .line 73
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/ActivityLaunchNotifier;->mListeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 74
    iget-object v0, p0, Lcom/google/android/apps/chrome/ActivityLaunchNotifier;->mListeners:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/ActivityLaunchNotifier$ActivityLaunchListener;

    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/chrome/ActivityLaunchNotifier$ActivityLaunchListener;->onLaunchActivity(Landroid/content/Intent;Landroid/view/Window;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 75
    const/4 v2, 0x1

    .line 73
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 78
    :cond_1
    return v2
.end method

.method public removeListener(Lcom/google/android/apps/chrome/ActivityLaunchNotifier$ActivityLaunchListener;)V
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/apps/chrome/ActivityLaunchNotifier;->mListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 58
    return-void
.end method
