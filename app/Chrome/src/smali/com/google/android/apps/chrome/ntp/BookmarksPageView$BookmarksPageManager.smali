.class public interface abstract Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarksPageManager;
.super Ljava/lang/Object;
.source "BookmarksPageView.java"


# virtual methods
.method public abstract delete(Lcom/google/android/apps/chrome/ntp/BookmarkItemView;)V
.end method

.method public abstract edit(Lcom/google/android/apps/chrome/ntp/BookmarkItemView;)V
.end method

.method public abstract getFaviconImageForUrl(Ljava/lang/String;ILorg/chromium/chrome/browser/favicon/FaviconHelper$FaviconImageCallback;)V
.end method

.method public abstract isContextMenuEnabled()Z
.end method

.method public abstract isDestroyed()Z
.end method

.method public abstract isIncognito()Z
.end method

.method public abstract open(Lcom/google/android/apps/chrome/ntp/BookmarkItemView;)V
.end method

.method public abstract openFolder(Lcom/google/android/apps/chrome/ntp/BookmarkFolderHierarchyItem;)V
.end method

.method public abstract openInNewIncognitoTab(Lcom/google/android/apps/chrome/ntp/BookmarkItemView;)V
.end method

.method public abstract openInNewTab(Lcom/google/android/apps/chrome/ntp/BookmarkItemView;)V
.end method

.method public abstract shouldShowOpenInNewIncognitoTab()Z
.end method

.method public abstract shouldShowOpenInNewTab()Z
.end method
