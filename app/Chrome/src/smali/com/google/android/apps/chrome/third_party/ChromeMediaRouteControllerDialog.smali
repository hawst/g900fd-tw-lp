.class public Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;
.super Landroid/app/Dialog;
.source "ChromeMediaRouteControllerDialog.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final mCallback:Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog$MediaRouterCallback;

.field private mControlView:Landroid/view/View;

.field private mCreated:Z

.field private mCurrentIconDrawable:Landroid/graphics/drawable/Drawable;

.field private mDisconnectButton:Landroid/widget/Button;

.field private mDisconnectListener:Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog$DisconnectListener;

.field private mMediaRouteConnectingDrawable:Landroid/graphics/drawable/Drawable;

.field private mMediaRouteOnDrawable:Landroid/graphics/drawable/Drawable;

.field private final mRoute:Landroid/support/v7/media/MediaRouter$RouteInfo;

.field private final mRouter:Landroid/support/v7/media/MediaRouter;

.field private mVolumeControlEnabled:Z

.field private mVolumeLayout:Landroid/widget/LinearLayout;

.field private mVolumeSlider:Landroid/widget/SeekBar;

.field private mVolumeSliderTouched:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 46
    const-class v0, Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Landroid/content/Context;ILcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog$DisconnectListener;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 113
    invoke-static {p1, v1}, Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog$MediaRouterThemeHelper;->createThemedContext(Landroid/content/Context;Z)Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 58
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;->mVolumeControlEnabled:Z

    .line 114
    sget-boolean v0, Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p3, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 116
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 118
    invoke-static {v0}, Landroid/support/v7/media/MediaRouter;->a(Landroid/content/Context;)Landroid/support/v7/media/MediaRouter;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;->mRouter:Landroid/support/v7/media/MediaRouter;

    .line 119
    new-instance v0, Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog$MediaRouterCallback;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog$MediaRouterCallback;-><init>(Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog$1;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;->mCallback:Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog$MediaRouterCallback;

    .line 120
    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;->mRouter:Landroid/support/v7/media/MediaRouter;

    invoke-static {}, Landroid/support/v7/media/MediaRouter;->c()Landroid/support/v7/media/MediaRouter$RouteInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;->mRoute:Landroid/support/v7/media/MediaRouter$RouteInfo;

    .line 121
    iput-object p3, p0, Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;->mDisconnectListener:Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog$DisconnectListener;

    .line 122
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog$DisconnectListener;)V
    .locals 1

    .prologue
    .line 108
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;-><init>(Landroid/content/Context;ILcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog$DisconnectListener;)V

    .line 109
    return-void
.end method

.method static synthetic access$102(Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;Z)Z
    .locals 0

    .prologue
    .line 46
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;->mVolumeSliderTouched:Z

    return p1
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;)V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;->updateVolume()V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;)Landroid/support/v7/media/MediaRouter$RouteInfo;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;->mRoute:Landroid/support/v7/media/MediaRouter$RouteInfo;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;)Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog$DisconnectListener;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;->mDisconnectListener:Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog$DisconnectListener;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;)Z
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;->update()Z

    move-result v0

    return v0
.end method

.method private getIconDrawable()Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 296
    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;->mRoute:Landroid/support/v7/media/MediaRouter$RouteInfo;

    invoke-virtual {v0}, Landroid/support/v7/media/MediaRouter$RouteInfo;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 297
    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;->mMediaRouteConnectingDrawable:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    .line 298
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Landroid/support/v7/mediarouter/R$attr;->mediaRouteConnectingDrawable:I

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog$MediaRouterThemeHelper;->getThemeDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;->mMediaRouteConnectingDrawable:Landroid/graphics/drawable/Drawable;

    .line 301
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;->mMediaRouteConnectingDrawable:Landroid/graphics/drawable/Drawable;

    .line 307
    :goto_0
    return-object v0

    .line 303
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;->mMediaRouteOnDrawable:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_2

    .line 304
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Landroid/support/v7/mediarouter/R$attr;->mediaRouteOnDrawable:I

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog$MediaRouterThemeHelper;->getThemeDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;->mMediaRouteOnDrawable:Landroid/graphics/drawable/Drawable;

    .line 307
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;->mMediaRouteOnDrawable:Landroid/graphics/drawable/Drawable;

    goto :goto_0
.end method

.method private isVolumeControlAvailable()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 324
    iget-boolean v1, p0, Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;->mVolumeControlEnabled:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;->mRoute:Landroid/support/v7/media/MediaRouter$RouteInfo;

    invoke-virtual {v1}, Landroid/support/v7/media/MediaRouter$RouteInfo;->j()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private update()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 273
    iget-object v2, p0, Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;->mRoute:Landroid/support/v7/media/MediaRouter$RouteInfo;

    invoke-virtual {v2}, Landroid/support/v7/media/MediaRouter$RouteInfo;->f()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;->mRoute:Landroid/support/v7/media/MediaRouter$RouteInfo;

    invoke-virtual {v2}, Landroid/support/v7/media/MediaRouter$RouteInfo;->g()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 274
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;->dismiss()V

    move v0, v1

    .line 292
    :cond_1
    :goto_0
    return v0

    .line 278
    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;->mRoute:Landroid/support/v7/media/MediaRouter$RouteInfo;

    invoke-virtual {v2}, Landroid/support/v7/media/MediaRouter$RouteInfo;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 279
    invoke-direct {p0}, Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;->updateVolume()V

    .line 281
    invoke-direct {p0}, Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;->getIconDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 282
    iget-object v3, p0, Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;->mCurrentIconDrawable:Landroid/graphics/drawable/Drawable;

    if-eq v2, v3, :cond_1

    .line 283
    iput-object v2, p0, Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;->mCurrentIconDrawable:Landroid/graphics/drawable/Drawable;

    .line 289
    invoke-virtual {v2, v1, v0}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    .line 290
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/4 v3, 0x3

    invoke-virtual {v1, v3, v2}, Landroid/view/Window;->setFeatureDrawable(ILandroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method private updateVolume()V
    .locals 2

    .prologue
    .line 312
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;->mVolumeSliderTouched:Z

    if-nez v0, :cond_0

    .line 313
    invoke-direct {p0}, Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;->isVolumeControlAvailable()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 314
    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;->mVolumeLayout:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 315
    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;->mVolumeSlider:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;->mRoute:Landroid/support/v7/media/MediaRouter$RouteInfo;

    invoke-virtual {v1}, Landroid/support/v7/media/MediaRouter$RouteInfo;->l()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setMax(I)V

    .line 316
    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;->mVolumeSlider:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;->mRoute:Landroid/support/v7/media/MediaRouter$RouteInfo;

    invoke-virtual {v1}, Landroid/support/v7/media/MediaRouter$RouteInfo;->k()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 321
    :cond_0
    :goto_0
    return-void

    .line 318
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;->mVolumeLayout:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public onAttachedToWindow()V
    .locals 4

    .prologue
    .line 233
    invoke-super {p0}, Landroid/app/Dialog;->onAttachedToWindow()V

    .line 235
    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;->mRouter:Landroid/support/v7/media/MediaRouter;

    sget-object v1, Landroid/support/v7/media/e;->a:Landroid/support/v7/media/e;

    iget-object v2, p0, Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;->mCallback:Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog$MediaRouterCallback;

    const/4 v3, 0x2

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v7/media/MediaRouter;->a(Landroid/support/v7/media/e;Landroid/support/v7/media/g;I)V

    .line 237
    invoke-direct {p0}, Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;->update()Z

    .line 238
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 178
    invoke-super {p0, p1}, Landroid/app/Dialog;->onCreate(Landroid/os/Bundle;)V

    .line 180
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/view/Window;->requestFeature(I)Z

    .line 182
    sget v0, Landroid/support/v7/mediarouter/R$layout;->mr_media_route_controller_dialog:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;->setContentView(I)V

    .line 184
    sget v0, Landroid/support/v7/mediarouter/R$id;->media_route_volume_layout:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;->mVolumeLayout:Landroid/widget/LinearLayout;

    .line 185
    sget v0, Landroid/support/v7/mediarouter/R$id;->media_route_volume_slider:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;->mVolumeSlider:Landroid/widget/SeekBar;

    .line 186
    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;->mVolumeSlider:Landroid/widget/SeekBar;

    new-instance v1, Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog$1;-><init>(Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;)V

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 206
    sget v0, Landroid/support/v7/mediarouter/R$id;->media_route_disconnect_button:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;->mDisconnectButton:Landroid/widget/Button;

    .line 207
    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;->mDisconnectButton:Landroid/widget/Button;

    new-instance v1, Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog$2;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog$2;-><init>(Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 217
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;->mCreated:Z

    .line 218
    invoke-direct {p0}, Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;->update()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 219
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;->onCreateMediaControlView(Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;->mControlView:Landroid/view/View;

    .line 220
    sget v0, Landroid/support/v7/mediarouter/R$id;->media_route_control_frame:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 222
    iget-object v1, p0, Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;->mControlView:Landroid/view/View;

    if-eqz v1, :cond_1

    .line 223
    iget-object v1, p0, Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;->mControlView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 224
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 229
    :cond_0
    :goto_0
    return-void

    .line 226
    :cond_1
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method public onCreateMediaControlView(Landroid/os/Bundle;)Landroid/view/View;
    .locals 1

    .prologue
    .line 140
    const/4 v0, 0x0

    return-object v0
.end method

.method public onDetachedFromWindow()V
    .locals 2

    .prologue
    .line 242
    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;->mRouter:Landroid/support/v7/media/MediaRouter;

    iget-object v1, p0, Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;->mCallback:Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog$MediaRouterCallback;

    invoke-virtual {v0, v1}, Landroid/support/v7/media/MediaRouter;->a(Landroid/support/v7/media/g;)V

    .line 244
    invoke-super {p0}, Landroid/app/Dialog;->onDetachedFromWindow()V

    .line 245
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 249
    invoke-direct {p0}, Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;->isVolumeControlAvailable()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 250
    const/16 v1, 0x19

    if-ne p1, v1, :cond_0

    .line 251
    iget-object v1, p0, Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;->mRoute:Landroid/support/v7/media/MediaRouter$RouteInfo;

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Landroid/support/v7/media/MediaRouter$RouteInfo;->b(I)V

    .line 258
    :goto_0
    return v0

    .line 253
    :cond_0
    const/16 v1, 0x18

    if-ne p1, v1, :cond_1

    .line 254
    iget-object v1, p0, Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;->mRoute:Landroid/support/v7/media/MediaRouter$RouteInfo;

    invoke-virtual {v1, v0}, Landroid/support/v7/media/MediaRouter$RouteInfo;->b(I)V

    goto :goto_0

    .line 258
    :cond_1
    invoke-super {p0, p1, p2}, Landroid/app/Dialog;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 263
    invoke-direct {p0}, Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;->isVolumeControlAvailable()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 264
    const/16 v0, 0x19

    if-eq p1, v0, :cond_0

    const/16 v0, 0x18

    if-ne p1, v0, :cond_1

    .line 266
    :cond_0
    const/4 v0, 0x1

    .line 269
    :goto_0
    return v0

    :cond_1
    invoke-super {p0, p1, p2}, Landroid/app/Dialog;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method
