.class Lcom/google/android/apps/chrome/webapps/SlidingLayout$1;
.super Ljava/lang/Object;
.source "SlidingLayout.java"

# interfaces
.implements Landroid/view/View$OnLayoutChangeListener;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/webapps/SlidingLayout;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/webapps/SlidingLayout;)V
    .locals 0

    .prologue
    .line 204
    iput-object p1, p0, Lcom/google/android/apps/chrome/webapps/SlidingLayout$1;->this$0:Lcom/google/android/apps/chrome/webapps/SlidingLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLayoutChange(Landroid/view/View;IIIIIIII)V
    .locals 2

    .prologue
    .line 208
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/SlidingLayout$1;->this$0:Lcom/google/android/apps/chrome/webapps/SlidingLayout;

    # getter for: Lcom/google/android/apps/chrome/webapps/SlidingLayout;->mAnimationState:I
    invoke-static {v0}, Lcom/google/android/apps/chrome/webapps/SlidingLayout;->access$000(Lcom/google/android/apps/chrome/webapps/SlidingLayout;)I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 212
    :goto_0
    return-void

    .line 209
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/SlidingLayout$1;->this$0:Lcom/google/android/apps/chrome/webapps/SlidingLayout;

    # getter for: Lcom/google/android/apps/chrome/webapps/SlidingLayout;->mContentView:Landroid/view/View;
    invoke-static {v0}, Lcom/google/android/apps/chrome/webapps/SlidingLayout;->access$100(Lcom/google/android/apps/chrome/webapps/SlidingLayout;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->removeOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 210
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/SlidingLayout$1;->this$0:Lcom/google/android/apps/chrome/webapps/SlidingLayout;

    const/4 v1, 0x2

    # invokes: Lcom/google/android/apps/chrome/webapps/SlidingLayout;->updateAnimationState(I)V
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/webapps/SlidingLayout;->access$200(Lcom/google/android/apps/chrome/webapps/SlidingLayout;I)V

    .line 211
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/SlidingLayout$1;->this$0:Lcom/google/android/apps/chrome/webapps/SlidingLayout;

    # invokes: Lcom/google/android/apps/chrome/webapps/SlidingLayout;->startAnimation()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/webapps/SlidingLayout;->access$300(Lcom/google/android/apps/chrome/webapps/SlidingLayout;)V

    goto :goto_0
.end method
