.class public interface abstract Lcom/google/android/apps/chrome/firstrun/FirstRunGlue;
.super Ljava/lang/Object;
.source "FirstRunGlue.java"


# virtual methods
.method public abstract acceptTermsOfService(Landroid/content/Context;Z)V
.end method

.method public abstract didAcceptTermsOfService(Landroid/content/Context;)Z
.end method

.method public abstract isDefaultAccountName(Landroid/content/Context;Ljava/lang/String;)Z
.end method

.method public abstract isDocumentModeEligible(Landroid/content/Context;)Z
.end method

.method public abstract isNeverUploadCrashDump(Landroid/content/Context;)Z
.end method

.method public abstract openAccountAdder(Landroid/app/Fragment;)V
.end method

.method public abstract openChromePreferencesPage(Landroid/app/Fragment;Ljava/lang/String;I)V
.end method
