.class public Lcom/google/android/apps/chrome/ntp/RecentTabsPage;
.super Ljava/lang/Object;
.source "RecentTabsPage.java"

# interfaces
.implements Landroid/view/View$OnCreateContextMenuListener;
.implements Landroid/widget/ExpandableListView$OnChildClickListener;
.implements Landroid/widget/ExpandableListView$OnGroupCollapseListener;
.implements Landroid/widget/ExpandableListView$OnGroupExpandListener;
.implements Lcom/google/android/apps/chrome/compositor/InvalidationAwareThumbnailProvider;
.implements Lcom/google/android/apps/chrome/ntp/RecentTabsManager$UpdatedCallback;
.implements Lorg/chromium/chrome/browser/NativePage;


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final mActivity:Landroid/app/Activity;

.field private mAdapter:Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;

.field private final mListView:Landroid/widget/ExpandableListView;

.field private mRecentTabsManager:Lcom/google/android/apps/chrome/ntp/RecentTabsManager;

.field private mSnapshotContentChanged:Z

.field private mSnapshotHeight:I

.field private mSnapshotListPosition:I

.field private mSnapshotListTop:I

.field private mSnapshotWidth:I

.field private final mTitle:Ljava/lang/String;

.field private final mView:Landroid/view/ViewGroup;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-class v0, Lcom/google/android/apps/chrome/ntp/RecentTabsPage;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/ntp/RecentTabsPage;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/apps/chrome/ntp/RecentTabsManager;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    iput-object p1, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsPage;->mActivity:Landroid/app/Activity;

    .line 55
    iput-object p2, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsPage;->mRecentTabsManager:Lcom/google/android/apps/chrome/ntp/RecentTabsManager;

    .line 57
    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$string;->recent_tabs:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsPage;->mTitle:Ljava/lang/String;

    .line 58
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsPage;->mRecentTabsManager:Lcom/google/android/apps/chrome/ntp/RecentTabsManager;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->setUpdatedCallback(Lcom/google/android/apps/chrome/ntp/RecentTabsManager$UpdatedCallback;)V

    .line 59
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 60
    sget v1, Lcom/google/android/apps/chrome/R$layout;->recent_tabs_page:I

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsPage;->mView:Landroid/view/ViewGroup;

    .line 61
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsPage;->mView:Landroid/view/ViewGroup;

    sget v1, Lcom/google/android/apps/chrome/R$id;->odp_listview:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ExpandableListView;

    iput-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsPage;->mListView:Landroid/widget/ExpandableListView;

    .line 62
    invoke-static {p1, p2}, Lcom/google/android/apps/chrome/ntp/RecentTabsPage;->buildAdapter(Landroid/app/Activity;Lcom/google/android/apps/chrome/ntp/RecentTabsManager;)Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsPage;->mAdapter:Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;

    .line 63
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsPage;->mListView:Landroid/widget/ExpandableListView;

    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsPage;->mAdapter:Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ExpandableListView;->setAdapter(Landroid/widget/ExpandableListAdapter;)V

    .line 64
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsPage;->mListView:Landroid/widget/ExpandableListView;

    invoke-virtual {v0, p0}, Landroid/widget/ExpandableListView;->setOnChildClickListener(Landroid/widget/ExpandableListView$OnChildClickListener;)V

    .line 65
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsPage;->mListView:Landroid/widget/ExpandableListView;

    invoke-virtual {v0, v2}, Landroid/widget/ExpandableListView;->setGroupIndicator(Landroid/graphics/drawable/Drawable;)V

    .line 66
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsPage;->mListView:Landroid/widget/ExpandableListView;

    invoke-virtual {v0, p0}, Landroid/widget/ExpandableListView;->setOnGroupCollapseListener(Landroid/widget/ExpandableListView$OnGroupCollapseListener;)V

    .line 67
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsPage;->mListView:Landroid/widget/ExpandableListView;

    invoke-virtual {v0, p0}, Landroid/widget/ExpandableListView;->setOnGroupExpandListener(Landroid/widget/ExpandableListView$OnGroupExpandListener;)V

    .line 68
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsPage;->mListView:Landroid/widget/ExpandableListView;

    invoke-virtual {v0, p0}, Landroid/widget/ExpandableListView;->setOnCreateContextMenuListener(Landroid/view/View$OnCreateContextMenuListener;)V

    .line 70
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ntp/RecentTabsPage;->onUpdated()V

    .line 71
    return-void
.end method

.method private static buildAdapter(Landroid/app/Activity;Lcom/google/android/apps/chrome/ntp/RecentTabsManager;)Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;
    .locals 1

    .prologue
    .line 75
    new-instance v0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;-><init>(Landroid/app/Activity;Lcom/google/android/apps/chrome/ntp/RecentTabsManager;)V

    return-object v0
.end method


# virtual methods
.method public captureThumbnail(Landroid/graphics/Canvas;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 190
    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsPage;->mView:Landroid/view/ViewGroup;

    invoke-static {v1, p1}, Lcom/google/android/apps/chrome/utilities/ViewUtils;->captureBitmap(Landroid/view/View;Landroid/graphics/Canvas;)V

    .line 191
    iput-boolean v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsPage;->mSnapshotContentChanged:Z

    .line 192
    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsPage;->mListView:Landroid/widget/ExpandableListView;

    invoke-virtual {v1}, Landroid/widget/ExpandableListView;->getFirstVisiblePosition()I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsPage;->mSnapshotListPosition:I

    .line 193
    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsPage;->mListView:Landroid/widget/ExpandableListView;

    invoke-virtual {v1, v0}, Landroid/widget/ExpandableListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 194
    if-nez v1, :cond_0

    :goto_0
    iput v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsPage;->mSnapshotListTop:I

    .line 195
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsPage;->mView:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsPage;->mSnapshotWidth:I

    .line 196
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsPage;->mView:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsPage;->mSnapshotHeight:I

    .line 197
    return-void

    .line 194
    :cond_0
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v0

    goto :goto_0
.end method

.method public destroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 107
    sget-boolean v0, Lcom/google/android/apps/chrome/ntp/RecentTabsPage;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ntp/RecentTabsPage;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    const-string/jumbo v1, "Destroy called before removed from window"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 108
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsPage;->mRecentTabsManager:Lcom/google/android/apps/chrome/ntp/RecentTabsManager;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->destroy()V

    .line 109
    iput-object v1, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsPage;->mRecentTabsManager:Lcom/google/android/apps/chrome/ntp/RecentTabsManager;

    .line 110
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsPage;->mAdapter:Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->notifyDataSetInvalidated()V

    .line 111
    iput-object v1, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsPage;->mAdapter:Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;

    .line 112
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsPage;->mListView:Landroid/widget/ExpandableListView;

    invoke-virtual {v0, v1}, Landroid/widget/ExpandableListView;->setAdapter(Landroid/widget/ExpandableListAdapter;)V

    .line 113
    return-void
.end method

.method public getBackgroundColor()I
    .locals 1

    .prologue
    .line 92
    const/4 v0, -0x1

    return v0
.end method

.method public getHost()Ljava/lang/String;
    .locals 1

    .prologue
    .line 102
    const-string/jumbo v0, "recent-tabs"

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsPage;->mTitle:Ljava/lang/String;

    return-object v0
.end method

.method public getUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 82
    const-string/jumbo v0, "chrome-native://recent-tabs/"

    return-object v0
.end method

.method public getView()Landroid/view/View;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsPage;->mView:Landroid/view/ViewGroup;

    return-object v0
.end method

.method public onChildClick(Landroid/widget/ExpandableListView;Landroid/view/View;IIJ)Z
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsPage;->mAdapter:Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;

    invoke-virtual {v0, p3}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->getGroup(I)Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$Group;

    move-result-object v0

    invoke-virtual {v0, p4}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$Group;->onChildClick(I)Z

    move-result v0

    return v0
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 4

    .prologue
    .line 159
    check-cast p3, Landroid/widget/ExpandableListView$ExpandableListContextMenuInfo;

    .line 162
    iget-wide v0, p3, Landroid/widget/ExpandableListView$ExpandableListContextMenuInfo;->packedPosition:J

    invoke-static {v0, v1}, Landroid/widget/ExpandableListView;->getPackedPositionType(J)I

    move-result v0

    .line 163
    iget-wide v2, p3, Landroid/widget/ExpandableListView$ExpandableListContextMenuInfo;->packedPosition:J

    invoke-static {v2, v3}, Landroid/widget/ExpandableListView;->getPackedPositionGroup(J)I

    move-result v1

    .line 165
    if-nez v0, :cond_1

    .line 166
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsPage;->mAdapter:Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->getGroup(I)Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$Group;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsPage;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0, p1, v1}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$Group;->onCreateContextMenuForGroup(Landroid/view/ContextMenu;Landroid/app/Activity;)V

    .line 172
    :cond_0
    :goto_0
    return-void

    .line 167
    :cond_1
    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    .line 168
    iget-wide v2, p3, Landroid/widget/ExpandableListView$ExpandableListContextMenuInfo;->packedPosition:J

    invoke-static {v2, v3}, Landroid/widget/ExpandableListView;->getPackedPositionChild(J)I

    move-result v0

    .line 169
    iget-object v2, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsPage;->mAdapter:Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;

    invoke-virtual {v2, v1}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->getGroup(I)Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$Group;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsPage;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1, v0, p1, v2}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$Group;->onCreateContextMenuForChild(ILandroid/view/ContextMenu;Landroid/app/Activity;)V

    goto :goto_0
.end method

.method public onGroupCollapse(I)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 136
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsPage;->mAdapter:Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->getGroup(I)Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$Group;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$Group;->setCollapsed(Z)V

    .line 137
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsPage;->mSnapshotContentChanged:Z

    .line 138
    return-void
.end method

.method public onGroupExpand(I)V
    .locals 2

    .prologue
    .line 129
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsPage;->mAdapter:Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->getGroup(I)Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$Group;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$Group;->setCollapsed(Z)V

    .line 130
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsPage;->mSnapshotContentChanged:Z

    .line 131
    return-void
.end method

.method public onUpdated()V
    .locals 2

    .prologue
    .line 143
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsPage;->mAdapter:Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->notifyDataSetChanged()V

    .line 144
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsPage;->mAdapter:Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->getGroupCount()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 145
    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsPage;->mAdapter:Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->getGroup(I)Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$Group;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$Group;->isCollapsed()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 146
    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsPage;->mListView:Landroid/widget/ExpandableListView;

    invoke-virtual {v1, v0}, Landroid/widget/ExpandableListView;->collapseGroup(I)Z

    .line 144
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 148
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsPage;->mListView:Landroid/widget/ExpandableListView;

    invoke-virtual {v1, v0}, Landroid/widget/ExpandableListView;->expandGroup(I)Z

    goto :goto_1

    .line 151
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsPage;->mSnapshotContentChanged:Z

    .line 152
    return-void
.end method

.method public shouldCaptureThumbnail()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 178
    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsPage;->mView:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getWidth()I

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsPage;->mView:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getHeight()I

    move-result v1

    if-nez v1, :cond_1

    .line 181
    :cond_0
    :goto_0
    return v0

    .line 180
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsPage;->mListView:Landroid/widget/ExpandableListView;

    invoke-virtual {v1, v0}, Landroid/widget/ExpandableListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 181
    iget-boolean v2, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsPage;->mSnapshotContentChanged:Z

    if-nez v2, :cond_2

    iget v2, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsPage;->mSnapshotListPosition:I

    iget-object v3, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsPage;->mListView:Landroid/widget/ExpandableListView;

    invoke-virtual {v3}, Landroid/widget/ExpandableListView;->getFirstVisiblePosition()I

    move-result v3

    if-ne v2, v3, :cond_2

    iget v2, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsPage;->mSnapshotListTop:I

    if-nez v1, :cond_3

    move v1, v0

    :goto_1
    if-ne v2, v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsPage;->mView:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getWidth()I

    move-result v1

    iget v2, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsPage;->mSnapshotWidth:I

    if-ne v1, v2, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsPage;->mView:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getHeight()I

    move-result v1

    iget v2, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsPage;->mSnapshotHeight:I

    if-eq v1, v2, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v1

    goto :goto_1
.end method

.method public updateForUrl(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 117
    return-void
.end method
