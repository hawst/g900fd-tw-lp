.class Lcom/google/android/apps/chrome/widget/SmoothProgressBar$1;
.super Ljava/lang/Object;
.source "SmoothProgressBar.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/widget/SmoothProgressBar;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/widget/SmoothProgressBar;)V
    .locals 0

    .prologue
    .line 34
    iput-object p1, p0, Lcom/google/android/apps/chrome/widget/SmoothProgressBar$1;->this$0:Lcom/google/android/apps/chrome/widget/SmoothProgressBar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/SmoothProgressBar$1;->this$0:Lcom/google/android/apps/chrome/widget/SmoothProgressBar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/SmoothProgressBar;->getProgress()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/SmoothProgressBar$1;->this$0:Lcom/google/android/apps/chrome/widget/SmoothProgressBar;

    # getter for: Lcom/google/android/apps/chrome/widget/SmoothProgressBar;->mTargetProgress:I
    invoke-static {v1}, Lcom/google/android/apps/chrome/widget/SmoothProgressBar;->access$000(Lcom/google/android/apps/chrome/widget/SmoothProgressBar;)I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 47
    :goto_0
    return-void

    .line 38
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/SmoothProgressBar$1;->this$0:Lcom/google/android/apps/chrome/widget/SmoothProgressBar;

    # getter for: Lcom/google/android/apps/chrome/widget/SmoothProgressBar;->mIsAnimated:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/widget/SmoothProgressBar;->access$100(Lcom/google/android/apps/chrome/widget/SmoothProgressBar;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 39
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/SmoothProgressBar$1;->this$0:Lcom/google/android/apps/chrome/widget/SmoothProgressBar;

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/SmoothProgressBar$1;->this$0:Lcom/google/android/apps/chrome/widget/SmoothProgressBar;

    # getter for: Lcom/google/android/apps/chrome/widget/SmoothProgressBar;->mTargetProgress:I
    invoke-static {v1}, Lcom/google/android/apps/chrome/widget/SmoothProgressBar;->access$000(Lcom/google/android/apps/chrome/widget/SmoothProgressBar;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/widget/SmoothProgressBar;->setProgressInternal(I)V

    goto :goto_0

    .line 44
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/SmoothProgressBar$1;->this$0:Lcom/google/android/apps/chrome/widget/SmoothProgressBar;

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/SmoothProgressBar$1;->this$0:Lcom/google/android/apps/chrome/widget/SmoothProgressBar;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/widget/SmoothProgressBar;->getProgress()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/SmoothProgressBar$1;->this$0:Lcom/google/android/apps/chrome/widget/SmoothProgressBar;

    # getter for: Lcom/google/android/apps/chrome/widget/SmoothProgressBar;->mTargetProgress:I
    invoke-static {v2}, Lcom/google/android/apps/chrome/widget/SmoothProgressBar;->access$000(Lcom/google/android/apps/chrome/widget/SmoothProgressBar;)I

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/chrome/widget/SmoothProgressBar$1;->this$0:Lcom/google/android/apps/chrome/widget/SmoothProgressBar;

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/widget/SmoothProgressBar;->getProgress()I

    move-result v3

    sub-int/2addr v2, v3

    add-int/lit8 v2, v2, 0x3

    div-int/lit8 v2, v2, 0x4

    add-int/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/widget/SmoothProgressBar;->setProgressInternal(I)V

    .line 45
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/SmoothProgressBar$1;->this$0:Lcom/google/android/apps/chrome/widget/SmoothProgressBar;

    const-wide/16 v2, 0x10

    invoke-static {v0, p0, v2, v3}, Lorg/chromium/base/ApiCompatibilityUtils;->postOnAnimationDelayed(Landroid/view/View;Ljava/lang/Runnable;J)V

    goto :goto_0
.end method
