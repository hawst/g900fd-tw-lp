.class Lcom/google/android/apps/chrome/widget/bubble/TextBubble$BubbleBackgroundDrawable;
.super Landroid/graphics/drawable/Drawable;
.source "TextBubble.java"


# instance fields
.field private final mBubbleArrowDrawable:Landroid/graphics/drawable/BitmapDrawable;

.field private mBubbleArrowXOffset:I

.field private final mBubbleContentsDrawable:Landroid/graphics/drawable/Drawable;

.field private final mTooltipBorderWidth:I

.field private final mTooltipContentPadding:Landroid/graphics/Rect;

.field private mUp:Z


# direct methods
.method constructor <init>(Landroid/content/Context;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 272
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 269
    iput-boolean v2, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble$BubbleBackgroundDrawable;->mUp:Z

    .line 273
    const-string/jumbo v0, "Up_Down"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "Up_Down"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble$BubbleBackgroundDrawable;->mUp:Z

    .line 274
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$drawable;->bubble_white:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble$BubbleBackgroundDrawable;->mBubbleContentsDrawable:Landroid/graphics/drawable/Drawable;

    .line 276
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$drawable;->bubble_point_white:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble$BubbleBackgroundDrawable;->mBubbleArrowDrawable:Landroid/graphics/drawable/BitmapDrawable;

    .line 278
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$dimen;->tooltip_border_width:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble$BubbleBackgroundDrawable;->mTooltipBorderWidth:I

    .line 281
    const-string/jumbo v0, "Background_Intrinsic_Padding"

    invoke-virtual {p2, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 282
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble$BubbleBackgroundDrawable;->mTooltipContentPadding:Landroid/graphics/Rect;

    .line 283
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble$BubbleBackgroundDrawable;->mBubbleContentsDrawable:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble$BubbleBackgroundDrawable;->mTooltipContentPadding:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    .line 289
    :goto_1
    return-void

    .line 273
    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    .line 285
    :cond_1
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$dimen;->tooltip_content_padding:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 287
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1, v0, v0, v0, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v1, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble$BubbleBackgroundDrawable;->mTooltipContentPadding:Landroid/graphics/Rect;

    goto :goto_1
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 293
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble$BubbleBackgroundDrawable;->mBubbleContentsDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 294
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble$BubbleBackgroundDrawable;->mBubbleArrowDrawable:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/BitmapDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 295
    return-void
.end method

.method public getBubbleArrowOffset()I
    .locals 1

    .prologue
    .line 378
    iget v0, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble$BubbleBackgroundDrawable;->mBubbleArrowXOffset:I

    return v0
.end method

.method public getOpacity()I
    .locals 1

    .prologue
    .line 340
    const/4 v0, -0x3

    return v0
.end method

.method public getPadding(Landroid/graphics/Rect;)Z
    .locals 5

    .prologue
    .line 345
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble$BubbleBackgroundDrawable;->mTooltipContentPadding:Landroid/graphics/Rect;

    invoke-virtual {p1, v0}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 346
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble$BubbleBackgroundDrawable;->mUp:Z

    if-eqz v0, :cond_0

    .line 347
    iget v0, p1, Landroid/graphics/Rect;->left:I

    iget v1, p1, Landroid/graphics/Rect;->top:I

    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble$BubbleBackgroundDrawable;->mBubbleArrowDrawable:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/BitmapDrawable;->getIntrinsicHeight()I

    move-result v2

    add-int/2addr v1, v2

    iget v2, p1, Landroid/graphics/Rect;->right:I

    iget v3, p1, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 360
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 353
    :cond_0
    iget v0, p1, Landroid/graphics/Rect;->left:I

    iget v1, p1, Landroid/graphics/Rect;->top:I

    iget v2, p1, Landroid/graphics/Rect;->right:I

    iget v3, p1, Landroid/graphics/Rect;->bottom:I

    iget-object v4, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble$BubbleBackgroundDrawable;->mBubbleArrowDrawable:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v4}, Landroid/graphics/drawable/BitmapDrawable;->getIntrinsicHeight()I

    move-result v4

    add-int/2addr v3, v4

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    goto :goto_0
.end method

.method protected onBoundsChange(Landroid/graphics/Rect;)V
    .locals 7

    .prologue
    .line 299
    if-nez p1, :cond_0

    .line 325
    :goto_0
    return-void

    .line 301
    :cond_0
    invoke-super {p0, p1}, Landroid/graphics/drawable/Drawable;->onBoundsChange(Landroid/graphics/Rect;)V

    .line 302
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble$BubbleBackgroundDrawable;->mBubbleArrowDrawable:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getIntrinsicWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    .line 303
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    .line 304
    iget-boolean v2, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble$BubbleBackgroundDrawable;->mUp:Z

    if-eqz v2, :cond_1

    .line 305
    iget v2, p1, Landroid/graphics/Rect;->top:I

    iget-object v3, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble$BubbleBackgroundDrawable;->mBubbleArrowDrawable:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v3}, Landroid/graphics/drawable/BitmapDrawable;->getIntrinsicHeight()I

    move-result v3

    add-int/2addr v2, v3

    iget v3, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble$BubbleBackgroundDrawable;->mTooltipBorderWidth:I

    sub-int/2addr v2, v3

    .line 307
    iget-object v3, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble$BubbleBackgroundDrawable;->mBubbleContentsDrawable:Landroid/graphics/drawable/Drawable;

    iget v4, p1, Landroid/graphics/Rect;->left:I

    iget v5, p1, Landroid/graphics/Rect;->right:I

    iget v6, p1, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v3, v4, v2, v5, v6}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 309
    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble$BubbleBackgroundDrawable;->mBubbleArrowDrawable:Landroid/graphics/drawable/BitmapDrawable;

    iget v3, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble$BubbleBackgroundDrawable;->mBubbleArrowXOffset:I

    add-int/2addr v3, v1

    sub-int/2addr v3, v0

    iget v4, p1, Landroid/graphics/Rect;->top:I

    iget v5, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble$BubbleBackgroundDrawable;->mBubbleArrowXOffset:I

    add-int/2addr v1, v5

    add-int/2addr v0, v1

    iget v1, p1, Landroid/graphics/Rect;->top:I

    iget-object v5, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble$BubbleBackgroundDrawable;->mBubbleArrowDrawable:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v5}, Landroid/graphics/drawable/BitmapDrawable;->getIntrinsicHeight()I

    move-result v5

    add-int/2addr v1, v5

    invoke-virtual {v2, v3, v4, v0, v1}, Landroid/graphics/drawable/BitmapDrawable;->setBounds(IIII)V

    goto :goto_0

    .line 315
    :cond_1
    iget v2, p1, Landroid/graphics/Rect;->bottom:I

    iget-object v3, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble$BubbleBackgroundDrawable;->mBubbleArrowDrawable:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v3}, Landroid/graphics/drawable/BitmapDrawable;->getIntrinsicHeight()I

    move-result v3

    sub-int/2addr v2, v3

    .line 316
    iget-object v3, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble$BubbleBackgroundDrawable;->mBubbleContentsDrawable:Landroid/graphics/drawable/Drawable;

    iget v4, p1, Landroid/graphics/Rect;->left:I

    iget v5, p1, Landroid/graphics/Rect;->left:I

    iget v6, p1, Landroid/graphics/Rect;->right:I

    invoke-virtual {v3, v4, v5, v6, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 318
    iget-object v3, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble$BubbleBackgroundDrawable;->mBubbleArrowDrawable:Landroid/graphics/drawable/BitmapDrawable;

    iget v4, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble$BubbleBackgroundDrawable;->mBubbleArrowXOffset:I

    add-int/2addr v4, v1

    sub-int/2addr v4, v0

    iget v5, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble$BubbleBackgroundDrawable;->mTooltipBorderWidth:I

    sub-int v5, v2, v5

    iget v6, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble$BubbleBackgroundDrawable;->mBubbleArrowXOffset:I

    add-int/2addr v1, v6

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble$BubbleBackgroundDrawable;->mBubbleArrowDrawable:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/BitmapDrawable;->getIntrinsicHeight()I

    move-result v1

    add-int/2addr v1, v2

    iget v2, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble$BubbleBackgroundDrawable;->mTooltipBorderWidth:I

    sub-int/2addr v1, v2

    invoke-virtual {v3, v4, v5, v0, v1}, Landroid/graphics/drawable/BitmapDrawable;->setBounds(IIII)V

    goto :goto_0
.end method

.method public setAlpha(I)V
    .locals 1

    .prologue
    .line 329
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble$BubbleBackgroundDrawable;->mBubbleContentsDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 330
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble$BubbleBackgroundDrawable;->mBubbleArrowDrawable:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/BitmapDrawable;->setAlpha(I)V

    .line 331
    return-void
.end method

.method public setBubbleArrowXOffset(I)V
    .locals 1

    .prologue
    .line 370
    iput p1, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble$BubbleBackgroundDrawable;->mBubbleArrowXOffset:I

    .line 371
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/bubble/TextBubble$BubbleBackgroundDrawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/bubble/TextBubble$BubbleBackgroundDrawable;->onBoundsChange(Landroid/graphics/Rect;)V

    .line 372
    return-void
.end method

.method public setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 0

    .prologue
    .line 336
    return-void
.end method
