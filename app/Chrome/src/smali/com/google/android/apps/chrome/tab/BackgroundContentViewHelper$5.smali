.class Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper$5;
.super Lorg/chromium/content/browser/WebContentsObserverAndroid;
.source "BackgroundContentViewHelper.java"


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;Lorg/chromium/content_public/browser/WebContents;)V
    .locals 0

    .prologue
    .line 413
    iput-object p1, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper$5;->this$0:Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;

    invoke-direct {p0, p2}, Lorg/chromium/content/browser/WebContentsObserverAndroid;-><init>(Lorg/chromium/content_public/browser/WebContents;)V

    return-void
.end method


# virtual methods
.method public didFailLoad(ZZILjava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 437
    if-eqz p2, :cond_0

    .line 438
    const/4 v0, 0x4

    const/4 v1, 0x6

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->recordInstantSearchClicksSwapReason(II)V

    .line 440
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper$5;->this$0:Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mDidFinishLoad:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->access$602(Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;Z)Z

    .line 441
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper$5;->this$0:Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;

    # invokes: Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->trySwappingBackgroundView()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->access$300(Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;)V

    .line 443
    :cond_0
    return-void
.end method

.method public didFinishLoad(JLjava/lang/String;Z)V
    .locals 2

    .prologue
    .line 428
    if-eqz p4, :cond_0

    .line 429
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper$5;->this$0:Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mDidFinishLoad:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->access$602(Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;Z)Z

    .line 430
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper$5;->this$0:Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;

    # invokes: Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->trySwappingBackgroundView()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->access$300(Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;)V

    .line 432
    :cond_0
    return-void
.end method

.method public didFirstVisuallyNonEmptyPaint()V
    .locals 2

    .prologue
    .line 416
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper$5;->this$0:Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mPaintedNonEmpty:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->access$402(Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;Z)Z

    .line 417
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper$5;->this$0:Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;

    # invokes: Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->trySwappingBackgroundView()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->access$300(Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;)V

    .line 418
    return-void
.end method

.method public didStartProvisionalLoadForFrame(JJZLjava/lang/String;ZZ)V
    .locals 2

    .prologue
    .line 423
    if-eqz p5, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper$5;->this$0:Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mDidStartLoad:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->access$502(Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;Z)Z

    .line 424
    :cond_0
    return-void
.end method
