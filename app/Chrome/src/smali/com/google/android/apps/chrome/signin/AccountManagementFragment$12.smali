.class final Lcom/google/android/apps/chrome/signin/AccountManagementFragment$12;
.super Ljava/lang/Object;
.source "AccountManagementFragment.java"

# interfaces
.implements Lorg/chromium/chrome/browser/profiles/ProfileDownloader$Observer;


# instance fields
.field final synthetic val$accountName:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 888
    iput-object p1, p0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment$12;->val$accountName:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onProfileDownloaded(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 3

    .prologue
    .line 891
    iget-object v0, p0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment$12;->val$accountName:Ljava/lang/String;

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 892
    # getter for: Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->sToNamePicture:Ljava/util/HashMap;
    invoke-static {}, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->access$600()Ljava/util/HashMap;

    move-result-object v0

    new-instance v1, Landroid/util/Pair;

    invoke-static {p3}, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->makeRoundUserPicture(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-direct {v1, p2, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 894
    invoke-static {p0}, Lorg/chromium/chrome/browser/profiles/ProfileDownloader;->removeObserver(Lorg/chromium/chrome/browser/profiles/ProfileDownloader$Observer;)V

    .line 896
    :cond_0
    return-void
.end method
