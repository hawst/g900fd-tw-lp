.class Lcom/google/android/apps/chrome/document/DocumentActivity$TitleBarDelegate;
.super Ljava/lang/Object;
.source "DocumentActivity.java"

# interfaces
.implements Lcom/google/android/apps/chrome/document/TitleBar$DocumentDelegate;
.implements Lcom/google/android/apps/chrome/document/TitleBar$TitleBarListener;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/document/DocumentActivity;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/document/DocumentActivity;)V
    .locals 0

    .prologue
    .line 146
    iput-object p1, p0, Lcom/google/android/apps/chrome/document/DocumentActivity$TitleBarDelegate;->this$0:Lcom/google/android/apps/chrome/document/DocumentActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getSecurityLevel()I
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity$TitleBarDelegate;->this$0:Lcom/google/android/apps/chrome/document/DocumentActivity;

    iget-object v0, v0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mDocumentTab:Lcom/google/android/apps/chrome/document/DocumentTab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/document/DocumentTab;->getSecurityLevel()I

    move-result v0

    return v0
.end method

.method public isIncognito()Z
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity$TitleBarDelegate;->this$0:Lcom/google/android/apps/chrome/document/DocumentActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->isIncognito()Z

    move-result v0

    return v0
.end method

.method public onMicClicked()V
    .locals 1

    .prologue
    .line 175
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity$TitleBarDelegate;->this$0:Lcom/google/android/apps/chrome/document/DocumentActivity;

    invoke-static {v0}, Lcom/google/android/apps/chrome/document/ssb/SearchLauncher;->fireVoiceIntent(Landroid/content/Context;)V

    .line 176
    return-void
.end method

.method public onTitleClicked()V
    .locals 2

    .prologue
    .line 159
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity$TitleBarDelegate;->this$0:Lcom/google/android/apps/chrome/document/DocumentActivity;

    # invokes: Lcom/google/android/apps/chrome/document/DocumentActivity;->isNewTabPage()Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->access$000(Lcom/google/android/apps/chrome/document/DocumentActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 160
    const/4 v0, 0x5

    invoke-static {v0}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->recordDocumentActivityHomeExitAction(I)V

    .line 165
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity$TitleBarDelegate;->this$0:Lcom/google/android/apps/chrome/document/DocumentActivity;

    iget-object v1, p0, Lcom/google/android/apps/chrome/document/DocumentActivity$TitleBarDelegate;->this$0:Lcom/google/android/apps/chrome/document/DocumentActivity;

    iget-object v1, v1, Lcom/google/android/apps/chrome/document/DocumentActivity;->mDocumentTab:Lcom/google/android/apps/chrome/document/DocumentTab;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/document/DocumentTab;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/document/ssb/SearchLauncher;->fireIntent(Landroid/content/Context;Ljava/lang/String;)V

    .line 166
    return-void

    .line 163
    :cond_0
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/google/android/apps/chrome/document/DocumentUma;->recordClickAction(I)V

    goto :goto_0
.end method

.method public onTitleLongClicked()V
    .locals 1

    .prologue
    .line 170
    const/4 v0, 0x4

    invoke-static {v0}, Lcom/google/android/apps/chrome/document/DocumentUma;->recordClickAction(I)V

    .line 171
    return-void
.end method
