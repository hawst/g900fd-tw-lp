.class final Lcom/google/android/apps/chrome/services/GoogleServicesManager$6;
.super Landroid/os/AsyncTask;
.source "GoogleServicesManager.java"


# instance fields
.field final synthetic val$appContext:Landroid/content/Context;

.field final synthetic val$checker:Lcom/google/android/apps/chrome/services/GoogleServicesManager$AccountChangeEventChecker;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$intent:Landroid/content/Intent;

.field final synthetic val$receiver:Landroid/content/BroadcastReceiver;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/google/android/apps/chrome/services/GoogleServicesManager$AccountChangeEventChecker;Landroid/content/BroadcastReceiver;Landroid/content/Context;Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 616
    iput-object p1, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager$6;->val$appContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager$6;->val$checker:Lcom/google/android/apps/chrome/services/GoogleServicesManager$AccountChangeEventChecker;

    iput-object p3, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager$6;->val$receiver:Landroid/content/BroadcastReceiver;

    iput-object p4, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager$6;->val$context:Landroid/content/Context;

    iput-object p5, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager$6;->val$intent:Landroid/content/Intent;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected final bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 616
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/services/GoogleServicesManager$6;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected final varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 2

    .prologue
    .line 619
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager$6;->val$appContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager$6;->val$checker:Lcom/google/android/apps/chrome/services/GoogleServicesManager$AccountChangeEventChecker;

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->updateAccountRenameData(Landroid/content/Context;Lcom/google/android/apps/chrome/services/GoogleServicesManager$AccountChangeEventChecker;)V

    .line 620
    const/4 v0, 0x0

    return-object v0
.end method

.method protected final bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 616
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/services/GoogleServicesManager$6;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected final onPostExecute(Ljava/lang/Void;)V
    .locals 3

    .prologue
    .line 625
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager$6;->val$receiver:Landroid/content/BroadcastReceiver;

    iget-object v1, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager$6;->val$context:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager$6;->val$intent:Landroid/content/Intent;

    invoke-virtual {v0, v1, v2}, Landroid/content/BroadcastReceiver;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V

    .line 626
    return-void
.end method
