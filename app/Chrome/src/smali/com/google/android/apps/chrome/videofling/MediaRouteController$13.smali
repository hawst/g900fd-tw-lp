.class Lcom/google/android/apps/chrome/videofling/MediaRouteController$13;
.super Ljava/lang/Object;
.source "MediaRouteController.java"

# interfaces
.implements Lcom/google/android/apps/chrome/videofling/MediaRouteController$ResultBundleHandler;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/videofling/MediaRouteController;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/videofling/MediaRouteController;)V
    .locals 0

    .prologue
    .line 1064
    iput-object p1, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController$13;->this$0:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 1076
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController$13;->this$0:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->release()V

    .line 1077
    return-void
.end method

.method public onResult(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 1067
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController$13;->this$0:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->processMediaStatusBundle(Landroid/os/Bundle;)V

    .line 1068
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController$13;->this$0:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    # getter for: Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mVideoUriToStart:Landroid/net/Uri;
    invoke-static {v0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->access$500(Lcom/google/android/apps/chrome/videofling/MediaRouteController;)Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1069
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController$13;->this$0:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    iget-object v1, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController$13;->this$0:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    # getter for: Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mVideoUriToStart:Landroid/net/Uri;
    invoke-static {v1}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->access$500(Lcom/google/android/apps/chrome/videofling/MediaRouteController;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController$13;->this$0:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    # getter for: Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mPreferredTitle:Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->access$600(Lcom/google/android/apps/chrome/videofling/MediaRouteController;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController$13;->this$0:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    # getter for: Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mStartPositionMillis:J
    invoke-static {v3}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->access$700(Lcom/google/android/apps/chrome/videofling/MediaRouteController;)J

    move-result-wide v4

    # invokes: Lcom/google/android/apps/chrome/videofling/MediaRouteController;->startPlayback(Landroid/net/Uri;Ljava/lang/String;J)V
    invoke-static {v0, v1, v2, v4, v5}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->access$1500(Lcom/google/android/apps/chrome/videofling/MediaRouteController;Landroid/net/Uri;Ljava/lang/String;J)V

    .line 1070
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController$13;->this$0:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mVideoUriToStart:Landroid/net/Uri;
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->access$502(Lcom/google/android/apps/chrome/videofling/MediaRouteController;Landroid/net/Uri;)Landroid/net/Uri;

    .line 1072
    :cond_0
    return-void
.end method
