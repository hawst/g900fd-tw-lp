.class public Lcom/google/android/apps/chrome/third_party/datausagechart/ChartGridView;
.super Landroid/view/View;
.source "ChartGridView.java"


# instance fields
.field private mBorder:Landroid/graphics/drawable/Drawable;

.field private mHoriz:Lcom/google/android/apps/chrome/third_party/datausagechart/ChartAxis;

.field private mPrimary:Landroid/graphics/drawable/Drawable;

.field private mSecondary:Landroid/graphics/drawable/Drawable;

.field private mVert:Lcom/google/android/apps/chrome/third_party/datausagechart/ChartAxis;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 57
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartGridView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 58
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 61
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartGridView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 62
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 65
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 67
    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartGridView;->setWillNotDraw(Z)V

    .line 69
    sget-object v0, Lcom/google/android/apps/chrome/R$styleable;->ChartGridView:[I

    invoke-virtual {p1, p2, v0, p3, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 72
    sget v1, Lcom/google/android/apps/chrome/R$styleable;->ChartGridView_primaryDrawable:I

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartGridView;->mPrimary:Landroid/graphics/drawable/Drawable;

    .line 73
    sget v1, Lcom/google/android/apps/chrome/R$styleable;->ChartGridView_secondaryDrawable:I

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartGridView;->mSecondary:Landroid/graphics/drawable/Drawable;

    .line 74
    sget v1, Lcom/google/android/apps/chrome/R$styleable;->ChartGridView_borderDrawable:I

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartGridView;->mBorder:Landroid/graphics/drawable/Drawable;

    .line 75
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 76
    return-void
.end method


# virtual methods
.method init(Lcom/google/android/apps/chrome/third_party/datausagechart/ChartAxis;Lcom/google/android/apps/chrome/third_party/datausagechart/ChartAxis;)V
    .locals 2

    .prologue
    .line 79
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string/jumbo v1, "missing horiz"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 80
    :cond_0
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    const-string/jumbo v1, "missing vert"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 81
    :cond_1
    iput-object p1, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartGridView;->mHoriz:Lcom/google/android/apps/chrome/third_party/datausagechart/ChartAxis;

    .line 82
    iput-object p2, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartGridView;->mVert:Lcom/google/android/apps/chrome/third_party/datausagechart/ChartAxis;

    .line 83
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 11

    .prologue
    const/4 v1, 0x0

    .line 87
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartGridView;->getWidth()I

    move-result v2

    .line 88
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartGridView;->getHeight()I

    move-result v3

    .line 90
    iget-object v4, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartGridView;->mSecondary:Landroid/graphics/drawable/Drawable;

    .line 91
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartGridView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    .line 92
    const/4 v5, 0x1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    invoke-static {v5, v0}, Ljava/lang/Math;->max(II)I

    move-result v5

    .line 94
    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartGridView;->mVert:Lcom/google/android/apps/chrome/third_party/datausagechart/ChartAxis;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartAxis;->getTickPoints()[F

    move-result-object v6

    .line 95
    array-length v7, v6

    move v0, v1

    :goto_0
    if-ge v0, v7, :cond_0

    aget v8, v6, v0

    .line 96
    int-to-float v9, v5

    add-float/2addr v9, v8

    int-to-float v10, v3

    invoke-static {v9, v10}, Ljava/lang/Math;->min(FF)F

    move-result v9

    float-to-int v9, v9

    .line 97
    float-to-int v8, v8

    invoke-virtual {v4, v1, v8, v2, v9}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 98
    invoke-virtual {v4, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 95
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 101
    :cond_0
    iget-object v4, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartGridView;->mPrimary:Landroid/graphics/drawable/Drawable;

    .line 102
    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartGridView;->mHoriz:Lcom/google/android/apps/chrome/third_party/datausagechart/ChartAxis;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartAxis;->getTickPoints()[F

    move-result-object v6

    .line 105
    array-length v7, v6

    move v0, v1

    :goto_1
    if-ge v0, v7, :cond_1

    aget v8, v6, v0

    .line 106
    int-to-float v9, v5

    add-float/2addr v9, v8

    int-to-float v10, v2

    invoke-static {v9, v10}, Ljava/lang/Math;->min(FF)F

    move-result v9

    float-to-int v9, v9

    .line 107
    float-to-int v8, v8

    invoke-virtual {v4, v8, v1, v9, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 108
    invoke-virtual {v4, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 105
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 111
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartGridView;->mBorder:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1, v1, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 112
    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartGridView;->mBorder:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 113
    return-void
.end method
