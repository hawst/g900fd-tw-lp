.class public final enum Lcom/google/android/apps/chrome/snapshot/SnapshotViewableState;
.super Ljava/lang/Enum;
.source "SnapshotViewableState.java"


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/chrome/snapshot/SnapshotViewableState;

.field public static final enum DOWNLOADING:Lcom/google/android/apps/chrome/snapshot/SnapshotViewableState;

.field public static final enum ERROR:Lcom/google/android/apps/chrome/snapshot/SnapshotViewableState;

.field public static final enum READY:Lcom/google/android/apps/chrome/snapshot/SnapshotViewableState;

.field public static final enum UNKNOWN:Lcom/google/android/apps/chrome/snapshot/SnapshotViewableState;


# instance fields
.field private final mValue:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 11
    new-instance v0, Lcom/google/android/apps/chrome/snapshot/SnapshotViewableState;

    const-string/jumbo v1, "UNKNOWN"

    const-string/jumbo v2, "UNKNOWN"

    invoke-direct {v0, v1, v3, v2}, Lcom/google/android/apps/chrome/snapshot/SnapshotViewableState;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/chrome/snapshot/SnapshotViewableState;->UNKNOWN:Lcom/google/android/apps/chrome/snapshot/SnapshotViewableState;

    .line 12
    new-instance v0, Lcom/google/android/apps/chrome/snapshot/SnapshotViewableState;

    const-string/jumbo v1, "READY"

    const-string/jumbo v2, "READY"

    invoke-direct {v0, v1, v4, v2}, Lcom/google/android/apps/chrome/snapshot/SnapshotViewableState;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/chrome/snapshot/SnapshotViewableState;->READY:Lcom/google/android/apps/chrome/snapshot/SnapshotViewableState;

    .line 13
    new-instance v0, Lcom/google/android/apps/chrome/snapshot/SnapshotViewableState;

    const-string/jumbo v1, "DOWNLOADING"

    const-string/jumbo v2, "DOWNLOADING"

    invoke-direct {v0, v1, v5, v2}, Lcom/google/android/apps/chrome/snapshot/SnapshotViewableState;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/chrome/snapshot/SnapshotViewableState;->DOWNLOADING:Lcom/google/android/apps/chrome/snapshot/SnapshotViewableState;

    .line 14
    new-instance v0, Lcom/google/android/apps/chrome/snapshot/SnapshotViewableState;

    const-string/jumbo v1, "ERROR"

    const-string/jumbo v2, "ERROR"

    invoke-direct {v0, v1, v6, v2}, Lcom/google/android/apps/chrome/snapshot/SnapshotViewableState;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/chrome/snapshot/SnapshotViewableState;->ERROR:Lcom/google/android/apps/chrome/snapshot/SnapshotViewableState;

    .line 10
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/android/apps/chrome/snapshot/SnapshotViewableState;

    sget-object v1, Lcom/google/android/apps/chrome/snapshot/SnapshotViewableState;->UNKNOWN:Lcom/google/android/apps/chrome/snapshot/SnapshotViewableState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/chrome/snapshot/SnapshotViewableState;->READY:Lcom/google/android/apps/chrome/snapshot/SnapshotViewableState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/chrome/snapshot/SnapshotViewableState;->DOWNLOADING:Lcom/google/android/apps/chrome/snapshot/SnapshotViewableState;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/chrome/snapshot/SnapshotViewableState;->ERROR:Lcom/google/android/apps/chrome/snapshot/SnapshotViewableState;

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/android/apps/chrome/snapshot/SnapshotViewableState;->$VALUES:[Lcom/google/android/apps/chrome/snapshot/SnapshotViewableState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 19
    iput-object p3, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotViewableState;->mValue:Ljava/lang/String;

    .line 20
    return-void
.end method

.method public static fromValue(Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/SnapshotViewableState;
    .locals 5

    .prologue
    .line 23
    if-eqz p0, :cond_1

    .line 24
    invoke-static {}, Lcom/google/android/apps/chrome/snapshot/SnapshotViewableState;->values()[Lcom/google/android/apps/chrome/snapshot/SnapshotViewableState;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 25
    iget-object v4, v3, Lcom/google/android/apps/chrome/snapshot/SnapshotViewableState;->mValue:Ljava/lang/String;

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 26
    return-object v3

    .line 24
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 30
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Invalid state: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/SnapshotViewableState;
    .locals 1

    .prologue
    .line 10
    const-class v0, Lcom/google/android/apps/chrome/snapshot/SnapshotViewableState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/snapshot/SnapshotViewableState;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/chrome/snapshot/SnapshotViewableState;
    .locals 1

    .prologue
    .line 10
    sget-object v0, Lcom/google/android/apps/chrome/snapshot/SnapshotViewableState;->$VALUES:[Lcom/google/android/apps/chrome/snapshot/SnapshotViewableState;

    invoke-virtual {v0}, [Lcom/google/android/apps/chrome/snapshot/SnapshotViewableState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/chrome/snapshot/SnapshotViewableState;

    return-object v0
.end method


# virtual methods
.method public final toValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotViewableState;->mValue:Ljava/lang/String;

    return-object v0
.end method
