.class Lcom/google/android/apps/chrome/omnibox/SuggestionView$PerformSelectSuggestion;
.super Ljava/lang/Object;
.source "SuggestionView.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/omnibox/SuggestionView;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/chrome/omnibox/SuggestionView;)V
    .locals 0

    .prologue
    .line 548
    iput-object p1, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$PerformSelectSuggestion;->this$0:Lcom/google/android/apps/chrome/omnibox/SuggestionView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/chrome/omnibox/SuggestionView;Lcom/google/android/apps/chrome/omnibox/SuggestionView$1;)V
    .locals 0

    .prologue
    .line 548
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/omnibox/SuggestionView$PerformSelectSuggestion;-><init>(Lcom/google/android/apps/chrome/omnibox/SuggestionView;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 551
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$PerformSelectSuggestion;->this$0:Lcom/google/android/apps/chrome/omnibox/SuggestionView;

    # getter for: Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mSuggestionDelegate:Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter$OmniboxSuggestionDelegate;
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->access$1300(Lcom/google/android/apps/chrome/omnibox/SuggestionView;)Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter$OmniboxSuggestionDelegate;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$PerformSelectSuggestion;->this$0:Lcom/google/android/apps/chrome/omnibox/SuggestionView;

    # getter for: Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mSuggestion:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;
    invoke-static {v1}, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->access$1100(Lcom/google/android/apps/chrome/omnibox/SuggestionView;)Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$PerformSelectSuggestion;->this$0:Lcom/google/android/apps/chrome/omnibox/SuggestionView;

    # getter for: Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mPosition:I
    invoke-static {v2}, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->access$1200(Lcom/google/android/apps/chrome/omnibox/SuggestionView;)I

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter$OmniboxSuggestionDelegate;->onSelection(Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;I)V

    .line 552
    return-void
.end method
