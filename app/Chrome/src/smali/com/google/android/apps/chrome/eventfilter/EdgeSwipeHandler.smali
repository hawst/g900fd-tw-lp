.class public interface abstract Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;
.super Ljava/lang/Object;
.source "EdgeSwipeHandler.java"


# virtual methods
.method public abstract isSwipeEnabled(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;)Z
.end method

.method public abstract swipeFinished()V
.end method

.method public abstract swipeFlingOccurred(FFFFFF)V
.end method

.method public abstract swipeStarted(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;FF)V
.end method

.method public abstract swipeUpdated(FFFFFF)V
.end method
