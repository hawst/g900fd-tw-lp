.class public Lcom/google/android/apps/chrome/bookmark/EditBookmarkHelper;
.super Ljava/lang/Object;
.source "EditBookmarkHelper.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lorg/chromium/chrome/browser/profiles/Profile;JLjava/lang/String;)V
    .locals 1

    .prologue
    .line 27
    invoke-static {p0, p1, p2, p3}, Lcom/google/android/apps/chrome/bookmark/EditBookmarkHelper;->nativeSetPartnerBookmarkTitle(Lorg/chromium/chrome/browser/profiles/Profile;JLjava/lang/String;)V

    return-void
.end method

.method public static editBookmark(Landroid/content/Context;JZ)V
    .locals 3

    .prologue
    .line 34
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/chrome/bookmark/ManageBookmarkActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 35
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 36
    const-string/jumbo v1, "folder"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 37
    const-string/jumbo v1, "_id"

    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 38
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 39
    return-void
.end method

.method public static editPartnerBookmark(Landroid/content/Context;Lorg/chromium/chrome/browser/profiles/Profile;JLjava/lang/String;Z)V
    .locals 6

    .prologue
    .line 50
    const-string/jumbo v0, "layout_inflater"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    sget v1, Lcom/google/android/apps/chrome/R$layout;->single_line_edit_dialog:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 52
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    if-eqz p5, :cond_0

    sget v0, Lcom/google/android/apps/chrome/R$string;->edit_folder:I

    :goto_0
    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$string;->cancel:I

    new-instance v3, Lcom/google/android/apps/chrome/bookmark/EditBookmarkHelper$1;

    invoke-direct {v3}, Lcom/google/android/apps/chrome/bookmark/EditBookmarkHelper$1;-><init>()V

    invoke-virtual {v0, v1, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 61
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    .line 66
    sget v0, Lcom/google/android/apps/chrome/R$id;->title:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 67
    sget v1, Lcom/google/android/apps/chrome/R$id;->text:I

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    .line 68
    sget v4, Lcom/google/android/apps/chrome/R$string;->bookmark_name:I

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(I)V

    .line 69
    invoke-virtual {v1, p4}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 70
    const/4 v0, 0x0

    invoke-virtual {p4}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v1, v0, v4}, Landroid/widget/EditText;->setSelection(II)V

    .line 71
    invoke-virtual {v1}, Landroid/widget/EditText;->requestFocus()Z

    .line 72
    new-instance v0, Lcom/google/android/apps/chrome/bookmark/EditBookmarkHelper$2;

    invoke-direct {v0, v1}, Lcom/google/android/apps/chrome/bookmark/EditBookmarkHelper$2;-><init>(Landroid/widget/EditText;)V

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->post(Ljava/lang/Runnable;)Z

    .line 78
    new-instance v0, Lcom/google/android/apps/chrome/bookmark/EditBookmarkHelper$3;

    invoke-direct {v0, v3}, Lcom/google/android/apps/chrome/bookmark/EditBookmarkHelper$3;-><init>(Landroid/app/AlertDialog;)V

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 96
    invoke-virtual {v3, v2}, Landroid/app/AlertDialog;->setView(Landroid/view/View;)V

    .line 97
    const/4 v0, -0x1

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v4, Lcom/google/android/apps/chrome/R$string;->save:I

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v4, Lcom/google/android/apps/chrome/bookmark/EditBookmarkHelper$4;

    invoke-direct {v4, v1, p1, p2, p3}, Lcom/google/android/apps/chrome/bookmark/EditBookmarkHelper$4;-><init>(Landroid/widget/EditText;Lorg/chromium/chrome/browser/profiles/Profile;J)V

    invoke-virtual {v3, v0, v2, v4}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 106
    invoke-virtual {v3}, Landroid/app/AlertDialog;->show()V

    .line 107
    return-void

    .line 52
    :cond_0
    sget v0, Lcom/google/android/apps/chrome/R$string;->edit_bookmark:I

    goto :goto_0
.end method

.method private static native nativeSetPartnerBookmarkTitle(Lorg/chromium/chrome/browser/profiles/Profile;JLjava/lang/String;)V
.end method
