.class public Lcom/google/android/apps/chrome/document/HomeToolbarHelper;
.super Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;
.source "HomeToolbarHelper.java"


# virtual methods
.method inflateAndAttachControls(Landroid/view/ViewGroup;Z)V
    .locals 2

    .prologue
    .line 34
    if-eqz p2, :cond_0

    sget v0, Lcom/google/android/apps/chrome/R$layout;->document_omnibox:I

    .line 36
    :goto_0
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    invoke-virtual {v1, v0, p1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 37
    return-void

    .line 34
    :cond_0
    sget v0, Lcom/google/android/apps/chrome/R$layout;->home_control_container:I

    goto :goto_0
.end method

.method initializeControls(Lorg/chromium/chrome/browser/appmenu/AppMenuHandler;Lcom/google/android/apps/chrome/document/TitleBar$DocumentDelegate;Lcom/google/android/apps/chrome/document/TitleBar$TitleBarListener;)V
    .locals 4

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/HomeToolbarHelper;->mControls:Lcom/google/android/apps/chrome/document/DocumentToolbarHelper$ToolbarControls;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/document/HomeToolbarHelper;->mControls:Lcom/google/android/apps/chrome/document/DocumentToolbarHelper$ToolbarControls;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/document/DocumentToolbarHelper$ToolbarControls;->destroy()V

    .line 44
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/HomeToolbarHelper;->isUsingChromeOmnibox()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 45
    new-instance v0, Lcom/google/android/apps/chrome/toolbar/OmniboxToolbarControls;

    iget-object v1, p0, Lcom/google/android/apps/chrome/document/HomeToolbarHelper;->mParentView:Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/google/android/apps/chrome/document/HomeToolbarHelper;->mActivity:Lcom/google/android/apps/chrome/document/DocumentActivity;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/HomeToolbarHelper;->getContextualMenuBar()Lcom/google/android/apps/chrome/ContextualMenuBar;

    move-result-object v3

    invoke-direct {v0, v1, v2, p1, v3}, Lcom/google/android/apps/chrome/toolbar/OmniboxToolbarControls;-><init>(Landroid/view/View;Lcom/google/android/apps/chrome/ChromeActivity;Lorg/chromium/chrome/browser/appmenu/AppMenuHandler;Lcom/google/android/apps/chrome/ContextualMenuBar;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/document/HomeToolbarHelper;->mControls:Lcom/google/android/apps/chrome/document/DocumentToolbarHelper$ToolbarControls;

    .line 51
    :goto_0
    return-void

    .line 48
    :cond_1
    new-instance v0, Lcom/google/android/apps/chrome/document/HomeToolbarControls;

    iget-object v1, p0, Lcom/google/android/apps/chrome/document/HomeToolbarHelper;->mParentView:Landroid/view/ViewGroup;

    invoke-direct {v0, v1, p2, p1, p3}, Lcom/google/android/apps/chrome/document/HomeToolbarControls;-><init>(Landroid/view/View;Lcom/google/android/apps/chrome/document/TitleBar$DocumentDelegate;Lorg/chromium/chrome/browser/appmenu/AppMenuHandler;Lcom/google/android/apps/chrome/document/TitleBar$TitleBarListener;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/document/HomeToolbarHelper;->mControls:Lcom/google/android/apps/chrome/document/DocumentToolbarHelper$ToolbarControls;

    goto :goto_0
.end method
