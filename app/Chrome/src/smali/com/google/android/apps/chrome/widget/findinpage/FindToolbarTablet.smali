.class public Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;
.super Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;
.source "FindToolbarTablet.java"


# instance fields
.field private mActivationText:Ljava/lang/String;

.field private mAnimationEnter:Lcom/google/android/apps/chrome/ChromeAnimation;

.field private mAnimationLeave:Lcom/google/android/apps/chrome/ChromeAnimation;

.field private mCurrentAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

.field private final mYInsetPx:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 44
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 34
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;->mActivationText:Ljava/lang/String;

    .line 46
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    const/high16 v1, 0x41000000    # 8.0f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;->mYInsetPx:I

    .line 47
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;->mActivationText:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 23
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->activate(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;)Lcom/google/android/apps/chrome/ChromeAnimation;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;->mCurrentAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    return-object v0
.end method

.method static synthetic access$202(Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;Lcom/google/android/apps/chrome/ChromeAnimation;)Lcom/google/android/apps/chrome/ChromeAnimation;
    .locals 0

    .prologue
    .line 23
    iput-object p1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;->mCurrentAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    return-object p1
.end method

.method private setMakeRoomForResults(Z)V
    .locals 9

    .prologue
    .line 152
    if-eqz p1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;->getHeight()I

    move-result v0

    iget v1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;->mYInsetPx:I

    sub-int/2addr v0, v1

    neg-int v0, v0

    int-to-float v3, v0

    .line 154
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;->getTranslationY()F

    move-result v0

    cmpl-float v0, v3, v0

    if-eqz v0, :cond_0

    .line 155
    new-instance v0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet$4;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet$4;-><init>(Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;->mCurrentAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    .line 168
    iget-object v8, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;->mCurrentAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    new-instance v0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet$ChromeAnimationTranslateY;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;->getTranslationY()F

    move-result v2

    const-wide/16 v4, 0xc8

    const-wide/16 v6, 0x0

    move-object v1, p0

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet$ChromeAnimationTranslateY;-><init>(Landroid/view/View;FFJJ)V

    invoke-virtual {v8, v0}, Lcom/google/android/apps/chrome/ChromeAnimation;->add(Lcom/google/android/apps/chrome/ChromeAnimation$Animation;)V

    .line 170
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;->mCurrentAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeAnimation;->start()V

    .line 172
    :cond_0
    return-void

    .line 152
    :cond_1
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private setShowState(Z)V
    .locals 3

    .prologue
    .line 175
    const/4 v0, 0x0

    .line 177
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;->getVisibility()I

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;->mCurrentAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;->mAnimationEnter:Lcom/google/android/apps/chrome/ChromeAnimation;

    if-eq v1, v2, :cond_2

    .line 179
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;->getRootView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$id;->toolbar:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 180
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 181
    invoke-virtual {v1}, Landroid/view/View;->getBottom()I

    move-result v1

    iget v2, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;->mYInsetPx:I

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 182
    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 183
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;->mAnimationEnter:Lcom/google/android/apps/chrome/ChromeAnimation;

    .line 190
    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    .line 191
    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;->mCurrentAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    .line 192
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeAnimation;->start()V

    .line 193
    invoke-static {p0}, Lorg/chromium/base/ApiCompatibilityUtils;->postInvalidateOnAnimation(Landroid/view/View;)V

    .line 195
    :cond_1
    return-void

    .line 184
    :cond_2
    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;->getVisibility()I

    move-result v1

    const/16 v2, 0x8

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;->mCurrentAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;->mAnimationLeave:Lcom/google/android/apps/chrome/ChromeAnimation;

    if-eq v1, v2, :cond_0

    .line 186
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;->mAnimationLeave:Lcom/google/android/apps/chrome/ChromeAnimation;

    .line 187
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;->onHideAnimationStart()V

    goto :goto_0
.end method


# virtual methods
.method public activate(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 99
    iput-object p1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;->mActivationText:Ljava/lang/String;

    .line 101
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;->mCurrentAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;->mAnimationEnter:Lcom/google/android/apps/chrome/ChromeAnimation;

    if-ne v0, v1, :cond_1

    .line 104
    :cond_0
    :goto_0
    return-void

    .line 103
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;->isViewAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;->setShowState(Z)V

    goto :goto_0
.end method

.method protected clearResults()V
    .locals 1

    .prologue
    .line 147
    invoke-super {p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->clearResults()V

    .line 148
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;->setMakeRoomForResults(Z)V

    .line 149
    return-void
.end method

.method public deactivate()V
    .locals 2

    .prologue
    .line 108
    invoke-super {p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->deactivate()V

    .line 110
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;->mCurrentAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;->mAnimationLeave:Lcom/google/android/apps/chrome/ChromeAnimation;

    if-ne v0, v1, :cond_0

    .line 113
    :goto_0
    return-void

    .line 112
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;->setShowState(Z)V

    goto :goto_0
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 132
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->draw(Landroid/graphics/Canvas;)V

    .line 134
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;->mCurrentAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;->mCurrentAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeAnimation;->finished()Z

    move-result v0

    if-nez v0, :cond_0

    .line 135
    new-instance v0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet$3;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet$3;-><init>(Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;->post(Ljava/lang/Runnable;)Z

    .line 143
    :cond_0
    return-void
.end method

.method public findResultSelected(Landroid/graphics/Rect;)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 117
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->findResultSelected(Landroid/graphics/Rect;)V

    .line 120
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    .line 122
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;->getLeft()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v1

    float-to-int v2, v2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;->getRight()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v3, v1

    float-to-int v3, v3

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;->getHeight()I

    move-result v4

    int-to-float v4, v4

    div-float v1, v4, v1

    float-to-int v1, v1

    invoke-virtual {p1, v2, v0, v3, v1}, Landroid/graphics/Rect;->intersects(IIII)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 124
    const/4 v0, 0x1

    .line 127
    :cond_0
    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;->setMakeRoomForResults(Z)V

    .line 128
    return-void
.end method

.method public onFinishInflate()V
    .locals 10

    .prologue
    .line 51
    invoke-super {p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->onFinishInflate()V

    .line 53
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;->setVisibility(I)V

    .line 55
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 56
    sget v1, Lcom/google/android/apps/chrome/R$dimen;->find_in_page_popup_width:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 57
    sget v2, Lcom/google/android/apps/chrome/R$dimen;->find_in_page_popup_margin_end:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    .line 58
    add-int v8, v1, v0

    .line 60
    new-instance v0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet$1;-><init>(Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;->mAnimationEnter:Lcom/google/android/apps/chrome/ChromeAnimation;

    .line 75
    iget-object v9, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;->mAnimationEnter:Lcom/google/android/apps/chrome/ChromeAnimation;

    new-instance v0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet$ChromeAnimationTranslateX;

    int-to-float v2, v8

    const/4 v3, 0x0

    const-wide/16 v4, 0xc8

    const-wide/16 v6, 0x0

    move-object v1, p0

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet$ChromeAnimationTranslateX;-><init>(Landroid/view/View;FFJJ)V

    invoke-virtual {v9, v0}, Lcom/google/android/apps/chrome/ChromeAnimation;->add(Lcom/google/android/apps/chrome/ChromeAnimation$Animation;)V

    .line 78
    new-instance v0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet$2;-><init>(Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;->mAnimationLeave:Lcom/google/android/apps/chrome/ChromeAnimation;

    .line 93
    iget-object v9, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet;->mAnimationLeave:Lcom/google/android/apps/chrome/ChromeAnimation;

    new-instance v0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet$ChromeAnimationTranslateX;

    const/4 v2, 0x0

    int-to-float v3, v8

    const-wide/16 v4, 0xc8

    const-wide/16 v6, 0x0

    move-object v1, p0

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarTablet$ChromeAnimationTranslateX;-><init>(Landroid/view/View;FFJJ)V

    invoke-virtual {v9, v0}, Lcom/google/android/apps/chrome/ChromeAnimation;->add(Lcom/google/android/apps/chrome/ChromeAnimation$Animation;)V

    .line 95
    return-void
.end method
