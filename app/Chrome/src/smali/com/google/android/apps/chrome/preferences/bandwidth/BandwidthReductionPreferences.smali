.class public Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthReductionPreferences;
.super Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;
.source "BandwidthReductionPreferences.java"


# static fields
.field public static final DATA_REDUCTION_INDEX_BOUNDARY:I = 0x4

.field public static final DATA_REDUCTION_OFF_TO_OFF:I = 0x0

.field public static final DATA_REDUCTION_OFF_TO_ON:I = 0x1

.field public static final DATA_REDUCTION_ON_TO_OFF:I = 0x2

.field public static final DATA_REDUCTION_ON_TO_ON:I = 0x3

.field private static sProxyLock:Ljava/lang/Object;

.field private static sProxyTurnedOffThisSession:Z

.field private static sProxyTurnedOnThisSession:Z


# instance fields
.field private mActionBarSwitch:Landroid/widget/Switch;

.field private mEnabler:Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthReductionEnabler;

.field private mFromPromo:Z

.field private mIsEnabled:Z

.field private mWasEnabledAtCreation:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 61
    sput-boolean v0, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthReductionPreferences;->sProxyTurnedOnThisSession:Z

    .line 62
    sput-boolean v0, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthReductionPreferences;->sProxyTurnedOffThisSession:Z

    .line 63
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthReductionPreferences;->sProxyLock:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;-><init>()V

    return-void
.end method

.method public static generateSummary(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 203
    invoke-static {}, Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings;->getInstance()Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings;->isDataReductionProxyEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 204
    invoke-static {}, Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings;->getInstance()Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings;->getContentLengthPercentSavings()Ljava/lang/String;

    move-result-object v0

    .line 206
    sget v1, Lcom/google/android/apps/chrome/R$string;->data_reduction_menu_item_summary:I

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-virtual {p0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 209
    :goto_0
    return-object v0

    :cond_0
    sget v0, Lcom/google/android/apps/chrome/R$string;->text_off:I

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method private static getNetworkStatsHistory([JI)Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;
    .locals 12

    .prologue
    const-wide/32 v10, 0x5265c00

    .line 214
    array-length v0, p0

    if-le p1, v0, :cond_0

    array-length p1, p0

    .line 215
    :cond_0
    new-instance v1, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;

    const/4 v0, 0x2

    invoke-direct {v1, v10, v11, p1, v0}, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;-><init>(JII)V

    .line 219
    invoke-static {}, Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings;->getInstance()Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings;

    move-result-object v0

    .line 220
    invoke-virtual {v0}, Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings;->getDataReductionLastUpdateTime()J

    move-result-wide v2

    int-to-long v4, p1

    mul-long/2addr v4, v10

    sub-long v8, v2, v4

    .line 221
    array-length v0, p0

    sub-int v2, v0, p1

    const/4 v0, 0x0

    move v7, v2

    :goto_0
    array-length v2, p0

    if-ge v7, v2, :cond_1

    .line 222
    new-instance v6, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStats$Entry;

    invoke-direct {v6}, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStats$Entry;-><init>()V

    .line 223
    aget-wide v2, p0, v7

    iput-wide v2, v6, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStats$Entry;->rxBytes:J

    .line 224
    int-to-long v2, v0

    mul-long/2addr v2, v10

    add-long/2addr v2, v8

    .line 226
    const-wide/32 v4, 0x36ee80

    add-long/2addr v4, v2

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->recordData(JJLcom/google/android/apps/chrome/third_party/datausagechart/NetworkStats$Entry;)V

    .line 221
    add-int/lit8 v2, v7, 0x1

    add-int/lit8 v0, v0, 0x1

    move v7, v2

    goto :goto_0

    .line 229
    :cond_1
    return-object v1
.end method

.method public static launchDataReductionSSLInfoBar(Landroid/content/Context;Lorg/chromium/content_public/browser/WebContents;)V
    .locals 7

    .prologue
    .line 69
    invoke-static {}, Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings;->getInstance()Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings;->isIncludedInAltFieldTrial()Z

    move-result v0

    if-nez v0, :cond_1

    .line 82
    :cond_0
    :goto_0
    return-void

    .line 70
    :cond_1
    invoke-static {}, Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings;->getInstance()Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings;->isDataReductionProxyEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 71
    invoke-static {}, Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings;->getInstance()Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings;->isDataReductionProxyManaged()Z

    move-result v0

    if-nez v0, :cond_0

    .line 72
    invoke-static {p0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v6

    .line 73
    invoke-virtual {v6}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getFirstRunFlowComplete()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 74
    invoke-virtual {v6}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getDisplayedDataReductionInfoBar()Z

    move-result v0

    if-nez v0, :cond_0

    .line 75
    const-class v0, Lcom/google/android/apps/chrome/preferences/Preferences;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const-class v0, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthReductionPreferences;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    sget v0, Lcom/google/android/apps/chrome/R$string;->data_reduction_infobar_text:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    sget v0, Lcom/google/android/apps/chrome/R$string;->learn_more:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    sget v0, Lcom/google/android/apps/chrome/R$string;->data_reduction_experiment_link_url:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object v0, p1

    invoke-static/range {v0 .. v5}, Lorg/chromium/chrome/browser/infobar/DataReductionProxyInfoBar;->launch(Lorg/chromium/content_public/browser/WebContents;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    const/4 v0, 0x1

    invoke-virtual {v6, v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->setDisplayedDataReductionInfoBar(Z)V

    goto :goto_0
.end method

.method private toggleProxyTurnOff(Z)V
    .locals 2

    .prologue
    .line 91
    sget-object v1, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthReductionPreferences;->sProxyLock:Ljava/lang/Object;

    monitor-enter v1

    .line 92
    :try_start_0
    sput-boolean p1, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthReductionPreferences;->sProxyTurnedOffThisSession:Z

    .line 93
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private toggleProxyTurnOn(Z)V
    .locals 2

    .prologue
    .line 85
    sget-object v1, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthReductionPreferences;->sProxyLock:Ljava/lang/Object;

    monitor-enter v1

    .line 86
    :try_start_0
    sput-boolean p1, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthReductionPreferences;->sProxyTurnedOnThisSession:Z

    .line 87
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 98
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    .line 100
    sget v0, Lcom/google/android/apps/chrome/R$xml;->bandwidth_reduction_preferences:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthReductionPreferences;->addPreferencesFromResource(I)V

    .line 101
    invoke-static {}, Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings;->getInstance()Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings;->isDataReductionProxyEnabled()Z

    move-result v3

    .line 103
    if-nez v3, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthReductionPreferences;->mIsEnabled:Z

    .line 104
    iput-boolean v3, p0, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthReductionPreferences;->mWasEnabledAtCreation:Z

    .line 105
    invoke-virtual {p0, v3}, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthReductionPreferences;->updatePreferences(Z)V

    .line 106
    new-instance v0, Lcom/google/android/apps/chrome/preferences/ManagedSwitch;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthReductionPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-static {}, Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings;->getInstance()Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings;

    move-result-object v4

    invoke-virtual {v4}, Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings;->isDataReductionProxyManaged()Z

    move-result v4

    invoke-direct {v0, v3, v4}, Lcom/google/android/apps/chrome/preferences/ManagedSwitch;-><init>(Landroid/content/Context;Z)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthReductionPreferences;->mActionBarSwitch:Landroid/widget/Switch;

    .line 109
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthReductionPreferences;->mActionBarSwitch:Landroid/widget/Switch;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthReductionPreferences;->addSwitchToActionBar(Landroid/widget/Switch;)V

    .line 110
    new-instance v0, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthReductionEnabler;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthReductionPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthReductionPreferences;->mActionBarSwitch:Landroid/widget/Switch;

    invoke-direct {v0, v3, v4}, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthReductionEnabler;-><init>(Landroid/content/Context;Landroid/widget/Switch;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthReductionPreferences;->mEnabler:Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthReductionEnabler;

    .line 111
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthReductionPreferences;->mEnabler:Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthReductionEnabler;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthReductionEnabler;->setBandwidthReductionPreferences(Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthReductionPreferences;)V

    .line 112
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthReductionPreferences;->mEnabler:Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthReductionEnabler;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthReductionEnabler;->attach()V

    .line 114
    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthReductionPreferences;->setHasOptionsMenu(Z)V

    .line 116
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthReductionPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 117
    if-eqz v0, :cond_1

    const-string/jumbo v3, "FromPromo"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :goto_1
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthReductionPreferences;->mFromPromo:Z

    .line 118
    return-void

    :cond_0
    move v0, v2

    .line 103
    goto :goto_0

    :cond_1
    move v1, v2

    .line 117
    goto :goto_1
.end method

.method public onDestroy()V
    .locals 4

    .prologue
    const/4 v1, 0x2

    const/4 v2, 0x1

    .line 122
    invoke-super {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;->onDestroy()V

    .line 123
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthReductionPreferences;->mEnabler:Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthReductionEnabler;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthReductionEnabler;->destroy()V

    .line 124
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthReductionPreferences;->mActionBarSwitch:Landroid/widget/Switch;

    const/4 v3, 0x4

    invoke-virtual {v0, v3}, Landroid/widget/Switch;->setVisibility(I)V

    .line 126
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthReductionPreferences;->mWasEnabledAtCreation:Z

    if-eqz v0, :cond_4

    .line 128
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthReductionPreferences;->mIsEnabled:Z

    if-eqz v0, :cond_3

    const/4 v0, 0x3

    .line 132
    :goto_0
    invoke-static {v0}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->dataReductionProxySettings(I)V

    .line 133
    iget-boolean v3, p0, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthReductionPreferences;->mFromPromo:Z

    if-eqz v3, :cond_0

    if-ne v0, v2, :cond_0

    .line 134
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->dataReductionProxyTurnedOnFromPromo()V

    .line 136
    :cond_0
    if-ne v0, v2, :cond_1

    sget-boolean v3, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthReductionPreferences;->sProxyTurnedOnThisSession:Z

    if-nez v3, :cond_1

    .line 137
    invoke-direct {p0, v2}, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthReductionPreferences;->toggleProxyTurnOn(Z)V

    .line 138
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->dataReductionProxyTurnedOn()V

    .line 140
    :cond_1
    if-ne v0, v1, :cond_2

    sget-boolean v0, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthReductionPreferences;->sProxyTurnedOffThisSession:Z

    if-nez v0, :cond_2

    .line 141
    invoke-direct {p0, v2}, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthReductionPreferences;->toggleProxyTurnOff(Z)V

    .line 142
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->dataReductionProxyTurnedOff()V

    .line 144
    :cond_2
    return-void

    :cond_3
    move v0, v1

    .line 128
    goto :goto_0

    .line 130
    :cond_4
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthReductionPreferences;->mIsEnabled:Z

    if-eqz v0, :cond_5

    move v0, v2

    goto :goto_0

    :cond_5
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public updatePreferences(Z)V
    .locals 6

    .prologue
    .line 151
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthReductionPreferences;->mIsEnabled:Z

    if-ne v0, p1, :cond_0

    .line 180
    :goto_0
    return-void

    .line 152
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthReductionPreferences;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0}, Landroid/preference/PreferenceScreen;->removeAll()V

    .line 153
    if-eqz p1, :cond_1

    .line 154
    sget v0, Lcom/google/android/apps/chrome/R$xml;->bandwidth_reduction_preferences:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthReductionPreferences;->addPreferencesFromResource(I)V

    .line 155
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthReductionPreferences;->updateReductionStatistics()V

    .line 179
    :goto_1
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthReductionPreferences;->mIsEnabled:Z

    goto :goto_0

    .line 157
    :cond_1
    sget v0, Lcom/google/android/apps/chrome/R$xml;->bandwidth_reduction_preferences_off:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthReductionPreferences;->addPreferencesFromResource(I)V

    .line 158
    invoke-static {}, Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings;->getInstance()Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings;->isIncludedInAltFieldTrial()Z

    move-result v0

    if-nez v0, :cond_2

    .line 159
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthReductionPreferences;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    const-string/jumbo v1, "data_reduction_experiment_text"

    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthReductionPreferences;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 161
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthReductionPreferences;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    const-string/jumbo v1, "data_reduction_experiment_link"

    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthReductionPreferences;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 165
    :cond_2
    const-string/jumbo v0, "data_reduction_learn_more"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthReductionPreferences;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 166
    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-virtual {v0}, Landroid/preference/Preference;->getTitle()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 168
    new-instance v2, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthReductionPreferences$1;

    invoke-direct {v2, p0}, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthReductionPreferences$1;-><init>(Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthReductionPreferences;)V

    .line 175
    const/4 v3, 0x0

    invoke-virtual {v1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v4

    const/16 v5, 0x21

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 177
    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method public updateReductionStatistics()V
    .locals 7

    .prologue
    const/16 v6, 0x1e

    .line 186
    invoke-static {}, Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings;->getInstance()Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings;

    move-result-object v1

    .line 188
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthReductionPreferences;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    const-string/jumbo v2, "data_usage_statistics_category"

    invoke-virtual {v0, v2}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthStatisticsPreferenceCategory;

    .line 191
    invoke-virtual {v1}, Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings;->getOriginalNetworkStatsHistory()[J

    move-result-object v2

    .line 192
    invoke-virtual {v1}, Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings;->getReceivedNetworkStatsHistory()[J

    move-result-object v3

    .line 193
    invoke-virtual {v1}, Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings;->getDataReductionLastUpdateTime()J

    move-result-wide v4

    invoke-static {v2, v6}, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthReductionPreferences;->getNetworkStatsHistory([JI)Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;

    move-result-object v1

    invoke-static {v3, v6}, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthReductionPreferences;->getNetworkStatsHistory([JI)Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;

    move-result-object v2

    invoke-virtual {v0, v4, v5, v1, v2}, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthStatisticsPreferenceCategory;->setReductionStats(JLcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;)V

    .line 197
    return-void
.end method
