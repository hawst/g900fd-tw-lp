.class public Lcom/google/android/apps/chrome/tab/ChromeSelectActionModeCallback;
.super Lorg/chromium/content/browser/SelectActionModeCallback;
.source "ChromeSelectActionModeCallback.java"


# direct methods
.method constructor <init>(Landroid/content/Context;Lorg/chromium/content/browser/SelectActionModeCallback$ActionHandler;Z)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0, p1, p2, p3}, Lorg/chromium/content/browser/SelectActionModeCallback;-><init>(Landroid/content/Context;Lorg/chromium/content/browser/SelectActionModeCallback$ActionHandler;Z)V

    .line 27
    return-void
.end method


# virtual methods
.method public onCreateActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 2

    .prologue
    .line 31
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeSelectActionModeCallback;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/ui/base/DeviceFormFactor;->isTablet(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeSelectActionModeCallback;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$string;->actionbar_textselection_title:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {p1, v0}, Landroid/view/ActionMode;->setTitle(Ljava/lang/CharSequence;)V

    .line 33
    invoke-super {p0, p1, p2}, Lorg/chromium/content/browser/SelectActionModeCallback;->onCreateActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z

    move-result v0

    return v0

    .line 31
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
