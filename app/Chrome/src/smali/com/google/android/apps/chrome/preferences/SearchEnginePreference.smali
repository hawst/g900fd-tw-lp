.class public Lcom/google/android/apps/chrome/preferences/SearchEnginePreference;
.super Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;
.source "SearchEnginePreference.java"

# interfaces
.implements Lorg/chromium/chrome/browser/search_engines/TemplateUrlService$LoadListener;


# instance fields
.field private mNewValue:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;-><init>()V

    return-void
.end method

.method public static getSearchEngineNameAndDomain(Landroid/content/Context;Lorg/chromium/chrome/browser/search_engines/TemplateUrlService$TemplateUrl;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 33
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 34
    invoke-virtual {p1}, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService$TemplateUrl;->getShortName()Ljava/lang/String;

    move-result-object v0

    .line 35
    invoke-virtual {p1}, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService$TemplateUrl;->getKeyword()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 36
    sget v2, Lcom/google/android/apps/chrome/R$string;->search_engine_name_and_domain:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    const/4 v0, 0x1

    invoke-virtual {p1}, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService$TemplateUrl;->getKeyword()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 39
    :cond_0
    return-object v0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 44
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    .line 45
    sget v0, Lcom/google/android/apps/chrome/R$xml;->search_engine_preferences:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/SearchEnginePreference;->addPreferencesFromResource(I)V

    .line 47
    invoke-static {}, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;->getInstance()Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;->getLocalizedSearchEngines()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService$TemplateUrl;

    .line 48
    new-instance v2, Landroid/preference/Preference;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/SearchEnginePreference;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 49
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/SearchEnginePreference;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-static {v3, v0}, Lcom/google/android/apps/chrome/preferences/SearchEnginePreference;->getSearchEngineNameAndDomain(Landroid/content/Context;Lorg/chromium/chrome/browser/search_engines/TemplateUrlService$TemplateUrl;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 50
    invoke-virtual {v0}, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService$TemplateUrl;->getIndex()I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/preference/Preference;->setKey(Ljava/lang/String;)V

    .line 51
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/SearchEnginePreference;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 52
    invoke-virtual {v0}, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService$TemplateUrl;->getIndex()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/SearchEnginePreference;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getPrefSearchEnginePreference()I

    move-result v3

    if-ne v0, v3, :cond_0

    .line 55
    sget v0, Lcom/google/android/apps/chrome/R$string;->current_search_engine:I

    invoke-virtual {v2, v0}, Landroid/preference/Preference;->setSummary(I)V

    goto :goto_0

    .line 58
    :cond_1
    return-void
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .locals 3

    .prologue
    .line 62
    invoke-virtual {p2}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/preferences/SearchEnginePreference;->mNewValue:I

    .line 63
    invoke-static {}, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;->getInstance()Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;->isLoaded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 64
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/SearchEnginePreference;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/chrome/preferences/SearchEnginePreference;->mNewValue:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->setSearchEngine(I)V

    .line 68
    :goto_0
    const/4 v0, 0x0

    :goto_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/SearchEnginePreference;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    invoke-virtual {v1}, Landroid/preference/PreferenceScreen;->getPreferenceCount()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 69
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/SearchEnginePreference;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->getPreference(I)Landroid/preference/Preference;

    move-result-object v1

    .line 70
    const-string/jumbo v2, ""

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 68
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 66
    :cond_0
    invoke-static {}, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;->getInstance()Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;

    move-result-object v0

    invoke-virtual {v0, p0}, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;->registerLoadListener(Lorg/chromium/chrome/browser/search_engines/TemplateUrlService$LoadListener;)V

    goto :goto_0

    .line 72
    :cond_1
    sget v0, Lcom/google/android/apps/chrome/R$string;->current_search_engine:I

    invoke-virtual {p2, v0}, Landroid/preference/Preference;->setSummary(I)V

    .line 73
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;->onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z

    move-result v0

    return v0
.end method

.method public onTemplateUrlServiceLoaded()V
    .locals 2

    .prologue
    .line 78
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/SearchEnginePreference;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/chrome/preferences/SearchEnginePreference;->mNewValue:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->setSearchEngine(I)V

    .line 79
    invoke-static {}, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;->getInstance()Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;

    move-result-object v0

    invoke-virtual {v0, p0}, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;->unregisterLoadListener(Lorg/chromium/chrome/browser/search_engines/TemplateUrlService$LoadListener;)V

    .line 80
    return-void
.end method
