.class public Lcom/google/android/apps/chrome/preferences/Preferences;
.super Landroid/preference/PreferenceActivity;
.source "Preferences.java"

# interfaces
.implements Lcom/google/android/apps/chrome/ActivityLaunchNotifier$ActivityLaunchListener;
.implements Lcom/google/android/apps/chrome/sync/ui/SyncChooseAccountFragment$Listener;
.implements Lorg/chromium/chrome/browser/profiles/ProfileDownloader$Observer;
.implements Lorg/chromium/chrome/browser/signin/SigninManager$SignInAllowedObserver;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final ENTRY_FRAGMENTS:[Ljava/lang/String;

.field public static final EXTRA_DISPLAY_HOME_AS_UP:Ljava/lang/String; = "display_home_as_up"

.field public static final FRAGMENT:Ljava/lang/String; = "fragment"

.field public static final RESULT_ACCOUNT:I = 0x1

.field public static final RESULT_DELETE_PASSWORD:I = 0x2


# instance fields
.field private final mFragments:Ljava/util/List;

.field private mHeaders:Ljava/util/List;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 87
    const-class v0, Lcom/google/android/apps/chrome/preferences/Preferences;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/preferences/Preferences;->$assertionsDisabled:Z

    .line 90
    const/16 v0, 0x1a

    new-array v0, v0, [Ljava/lang/String;

    const-class v3, Lcom/google/android/apps/chrome/preferences/AboutChromePreferences;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v2

    const-class v2, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-class v2, Lcom/google/android/apps/chrome/preferences/AccessibilityPreferences;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-class v2, Lcom/google/android/apps/chrome/preferences/AdvancedPreferences;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-class v2, Lcom/google/android/apps/chrome/preferences/autofill/AutofillCreditCardEditor;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-class v2, Lcom/google/android/apps/chrome/preferences/autofill/AutofillPreferences;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-class v2, Lcom/google/android/apps/chrome/preferences/autofill/AutofillProfileEditor;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-class v2, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthReductionPreferences;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-class v2, Lcom/google/android/apps/chrome/preferences/BasicsPreferences;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-class v2, Lcom/google/android/apps/chrome/preferences/ContentPreferences;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-class v2, Lcom/google/android/apps/chrome/preferences/ContextualSearchPreferenceFragment;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-class v2, Lcom/google/android/apps/chrome/preferences/DoNotTrackPreference;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-class v2, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-class v2, Lcom/google/android/apps/chrome/preferences/HomepagePreferences;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-class v2, Lcom/google/android/apps/chrome/preferences/LegalInformationPreferences;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-class v2, Lcom/google/android/apps/chrome/preferences/password/ManageSavedPasswordsPreferences;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-class v2, Lcom/google/android/apps/chrome/preferences/password/PasswordEntryEditor;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-class v2, Lcom/google/android/apps/chrome/preferences/PrivacyPreferences;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-class v2, Lcom/google/android/apps/chrome/preferences/ProtectedContentPreferences;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-class v2, Lcom/google/android/apps/chrome/preferences/SearchEnginePreference;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-class v2, Lcom/google/android/apps/chrome/preferences/website/SingleWebsitePreferences;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-class v2, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-class v2, Lcom/google/android/apps/chrome/preferences/DocumentModePreference;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-class v2, Lcom/google/android/apps/chrome/preferences/TranslatePreferences;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-class v2, Lcom/google/android/apps/chrome/preferences/UsersPreferences;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-class v2, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/chrome/preferences/Preferences;->ENTRY_FRAGMENTS:[Ljava/lang/String;

    return-void

    :cond_0
    move v0, v2

    .line 87
    goto/16 :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 87
    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    .line 126
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/preferences/Preferences;->mFragments:Ljava/util/List;

    .line 465
    return-void
.end method

.method private addAccountHeader(Ljava/util/List;)V
    .locals 2

    .prologue
    .line 377
    sget-boolean v0, Lcom/google/android/apps/chrome/preferences/Preferences;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-static {}, Lorg/chromium/chrome/browser/signin/SigninManager;->isNewProfileManagementEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 379
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/Preferences;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/sync/signin/ChromeSigninController;->get(Landroid/content/Context;)Lorg/chromium/sync/signin/ChromeSigninController;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/sync/signin/ChromeSigninController;->getSignedInUser()Landroid/accounts/Account;

    move-result-object v0

    .line 380
    if-eqz v0, :cond_2

    .line 381
    invoke-static {p0}, Lcom/google/android/apps/chrome/preferences/Preferences;->isTabletPreferencesUi(Landroid/app/Activity;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 382
    new-instance v1, Landroid/preference/PreferenceActivity$Header;

    invoke-direct {v1}, Landroid/preference/PreferenceActivity$Header;-><init>()V

    .line 383
    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    iput-object v0, v1, Landroid/preference/PreferenceActivity$Header;->title:Ljava/lang/CharSequence;

    .line 384
    const-class v0, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Landroid/preference/PreferenceActivity$Header;->fragment:Ljava/lang/String;

    .line 385
    const/4 v0, 0x1

    invoke-interface {p1, v0, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 387
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/Preferences;->invalidateOptionsMenu()V

    .line 389
    :cond_2
    return-void
.end method

.method public static isTabletPreferencesUi(Landroid/app/Activity;)Z
    .locals 1

    .prologue
    .line 879
    instance-of v0, p0, Landroid/preference/PreferenceActivity;

    if-eqz v0, :cond_0

    check-cast p0, Landroid/preference/PreferenceActivity;

    invoke-virtual {p0}, Landroid/preference/PreferenceActivity;->onIsMultiPane()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setDefaultSearchEngineSummary(Ljava/util/List;)V
    .locals 8

    .prologue
    .line 320
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/Preferences;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 321
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceActivity$Header;

    .line 322
    iget-wide v4, v0, Landroid/preference/PreferenceActivity$Header;->id:J

    sget v1, Lcom/google/android/apps/chrome/R$id;->search_engine:I

    int-to-long v6, v1

    cmp-long v1, v4, v6

    if-nez v1, :cond_0

    .line 323
    invoke-static {p0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getPrefSearchEnginePreference()I

    move-result v4

    .line 326
    invoke-static {}, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;->getInstance()Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;

    move-result-object v1

    invoke-virtual {v1}, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;->getLocalizedSearchEngines()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService$TemplateUrl;

    .line 327
    invoke-virtual {v1}, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService$TemplateUrl;->getIndex()I

    move-result v6

    if-ne v4, v6, :cond_1

    .line 328
    invoke-static {v2, v1}, Lcom/google/android/apps/chrome/preferences/SearchEnginePreference;->getSearchEngineNameAndDomain(Landroid/content/Context;Lorg/chromium/chrome/browser/search_engines/TemplateUrlService$TemplateUrl;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Landroid/preference/PreferenceActivity$Header;->summary:Ljava/lang/CharSequence;

    .line 335
    :cond_2
    return-void
.end method

.method private setMenuVisibility(Landroid/view/Menu;I)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 790
    const/16 v0, 0xa

    new-array v2, v0, [I

    sget v0, Lcom/google/android/apps/chrome/R$id;->PRIVACY_MENU:I

    aput v0, v2, v1

    sget v0, Lcom/google/android/apps/chrome/R$id;->PROTECTED_CONTENT_MENU:I

    aput v0, v2, v5

    const/4 v0, 0x2

    sget v3, Lcom/google/android/apps/chrome/R$id;->TRANSLATE_MENU:I

    aput v3, v2, v0

    const/4 v0, 0x3

    sget v3, Lcom/google/android/apps/chrome/R$id;->SYNC_ACCOUNT_MENU:I

    aput v3, v2, v0

    const/4 v0, 0x4

    sget v3, Lcom/google/android/apps/chrome/R$id;->ACCOUNT_MANAGEMENT_MENU:I

    aput v3, v2, v0

    const/4 v0, 0x5

    sget v3, Lcom/google/android/apps/chrome/R$id;->SYNC_MENU:I

    aput v3, v2, v0

    const/4 v0, 0x6

    sget v3, Lcom/google/android/apps/chrome/R$id;->DO_NOT_TRACK_MENU:I

    aput v3, v2, v0

    const/4 v0, 0x7

    sget v3, Lcom/google/android/apps/chrome/R$id;->CONTEXTUAL_SEARCH_MENU:I

    aput v3, v2, v0

    const/16 v0, 0x8

    sget v3, Lcom/google/android/apps/chrome/R$id;->DEFAULT_MENU:I

    aput v3, v2, v0

    const/16 v0, 0x9

    sget v3, Lcom/google/android/apps/chrome/R$id;->TOP_LEVEL_MENU:I

    aput v3, v2, v0

    .line 794
    array-length v3, v2

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_1

    aget v4, v2, v0

    .line 795
    if-ne v4, p2, :cond_0

    .line 796
    invoke-interface {p1, v4, v5}, Landroid/view/Menu;->setGroupVisible(IZ)V

    .line 794
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 798
    :cond_0
    invoke-interface {p1, v4, v1}, Landroid/view/Menu;->setGroupVisible(IZ)V

    goto :goto_1

    .line 801
    :cond_1
    return-void
.end method

.method private setUsersSectionSummary(Ljava/util/List;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 267
    sget-boolean v0, Lcom/google/android/apps/chrome/preferences/Preferences;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-static {}, Lorg/chromium/chrome/browser/signin/SigninManager;->isNewProfileManagementEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 269
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceActivity$Header;

    .line 270
    iget-wide v2, v0, Landroid/preference/PreferenceActivity$Header;->id:J

    sget v4, Lcom/google/android/apps/chrome/R$id;->sync_account:I

    int-to-long v4, v4

    cmp-long v2, v2, v4

    if-nez v2, :cond_1

    .line 271
    invoke-static {p0}, Lorg/chromium/sync/signin/ChromeSigninController;->get(Landroid/content/Context;)Lorg/chromium/sync/signin/ChromeSigninController;

    move-result-object v1

    invoke-virtual {v1}, Lorg/chromium/sync/signin/ChromeSigninController;->getSignedInUser()Landroid/accounts/Account;

    move-result-object v2

    .line 272
    if-eqz v2, :cond_7

    .line 273
    invoke-static {p0}, Lorg/chromium/sync/signin/AccountManagerHelper;->get(Landroid/content/Context;)Lorg/chromium/sync/signin/AccountManagerHelper;

    move-result-object v1

    invoke-virtual {v1}, Lorg/chromium/sync/signin/AccountManagerHelper;->getGoogleAccountNames()Ljava/util/List;

    move-result-object v1

    .line 274
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    if-ne v3, v7, :cond_5

    .line 275
    invoke-interface {v1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    iput-object v1, v0, Landroid/preference/PreferenceActivity$Header;->summary:Ljava/lang/CharSequence;

    .line 282
    :goto_0
    const-class v1, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Landroid/preference/PreferenceActivity$Header;->fragment:Ljava/lang/String;

    .line 283
    iget-object v1, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->getCachedUserName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Landroid/preference/PreferenceActivity$Header;->title:Ljava/lang/CharSequence;

    .line 284
    iget-object v1, v0, Landroid/preference/PreferenceActivity$Header;->title:Ljava/lang/CharSequence;

    if-nez v1, :cond_4

    .line 285
    invoke-static {}, Lorg/chromium/chrome/browser/profiles/Profile;->getLastUsedProfile()Lorg/chromium/chrome/browser/profiles/Profile;

    move-result-object v3

    .line 286
    invoke-static {v3}, Lorg/chromium/chrome/browser/profiles/ProfileDownloader;->getCachedName(Lorg/chromium/chrome/browser/profiles/Profile;)Ljava/lang/String;

    move-result-object v1

    .line 287
    invoke-static {v3}, Lorg/chromium/chrome/browser/profiles/ProfileDownloader;->getCachedAvatar(Lorg/chromium/chrome/browser/profiles/Profile;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 288
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    if-nez v4, :cond_6

    .line 289
    :cond_2
    iget-object v4, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {p0, v3, v4}, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->startFetchingAccountInformation(Landroid/content/Context;Lorg/chromium/chrome/browser/profiles/Profile;Ljava/lang/String;)V

    .line 295
    :goto_1
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v1, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    .line 296
    :cond_3
    iput-object v1, v0, Landroid/preference/PreferenceActivity$Header;->title:Ljava/lang/CharSequence;

    .line 312
    :cond_4
    :goto_2
    return-void

    .line 277
    :cond_5
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-static {v3}, Ljava/text/NumberFormat;->getIntegerInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v3

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    int-to-long v4, v1

    invoke-virtual {v3, v4, v5}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v1

    .line 279
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/Preferences;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/google/android/apps/chrome/R$string;->number_of_signed_in_accounts:I

    new-array v5, v7, [Ljava/lang/Object;

    aput-object v1, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Landroid/preference/PreferenceActivity$Header;->summary:Ljava/lang/CharSequence;

    goto :goto_0

    .line 292
    :cond_6
    iget-object v3, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v3, v1, v4}, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->updateUserNamePictureCache(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    goto :goto_1

    .line 299
    :cond_7
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/Preferences;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/chrome/R$string;->sign_in_to_chrome:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Landroid/preference/PreferenceActivity$Header;->title:Ljava/lang/CharSequence;

    .line 300
    invoke-static {p0}, Lcom/google/android/apps/chrome/preferences/Preferences;->isTabletPreferencesUi(Landroid/app/Activity;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 301
    const-class v1, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Landroid/preference/PreferenceActivity$Header;->fragment:Ljava/lang/String;

    .line 302
    const-string/jumbo v1, ""

    iput-object v1, v0, Landroid/preference/PreferenceActivity$Header;->summary:Ljava/lang/CharSequence;

    goto :goto_2

    .line 304
    :cond_8
    const-class v1, Lcom/google/android/apps/chrome/sync/ui/SyncChooseAccountFragment;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Landroid/preference/PreferenceActivity$Header;->fragment:Ljava/lang/String;

    .line 305
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/Preferences;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/chrome/R$string;->sign_in_to_chrome_summary:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Landroid/preference/PreferenceActivity$Header;->summary:Ljava/lang/CharSequence;

    goto :goto_2
.end method

.method private updateBandwidthReductionPreferencesSummary(Ljava/util/List;)V
    .locals 6

    .prologue
    .line 343
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceActivity$Header;

    .line 344
    iget-wide v2, v0, Landroid/preference/PreferenceActivity$Header;->id:J

    sget v4, Lcom/google/android/apps/chrome/R$id;->reduce_data_usage_settings:I

    int-to-long v4, v4

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    .line 345
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/Preferences;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthReductionPreferences;->generateSummary(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Landroid/preference/PreferenceActivity$Header;->summary:Ljava/lang/CharSequence;

    .line 349
    :cond_1
    return-void
.end method


# virtual methods
.method public displayAccountPicker()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 833
    invoke-static {p0}, Lorg/chromium/chrome/browser/signin/SigninManager;->get(Landroid/content/Context;)Lorg/chromium/chrome/browser/signin/SigninManager;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/signin/SigninManager;->isSignInAllowed()Z

    move-result v0

    if-nez v0, :cond_1

    .line 834
    invoke-static {p0}, Lorg/chromium/chrome/browser/signin/SigninManager;->get(Landroid/content/Context;)Lorg/chromium/chrome/browser/signin/SigninManager;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/signin/SigninManager;->isSigninDisabledByPolicy()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 835
    invoke-static {p0}, Lcom/google/android/apps/chrome/preferences/ManagedPreferencesUtils;->showManagedByAdministratorToast(Landroid/content/Context;)V

    .line 848
    :cond_0
    :goto_0
    return-void

    .line 840
    :cond_1
    invoke-static {p0}, Lorg/chromium/sync/signin/AccountManagerHelper;->get(Landroid/content/Context;)Lorg/chromium/sync/signin/AccountManagerHelper;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/sync/signin/AccountManagerHelper;->hasGoogleAccounts()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 841
    new-instance v0, Lcom/google/android/apps/chrome/sync/ui/SyncChooseAccountFragment;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/sync/ui/SyncChooseAccountFragment;-><init>()V

    .line 842
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/Preferences;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/sync/ui/SyncChooseAccountFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    .line 844
    :cond_2
    new-instance v0, Lcom/google/android/apps/chrome/services/ui/AddGoogleAccountDialogFragment;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/services/ui/AddGoogleAccountDialogFragment;-><init>()V

    .line 845
    invoke-static {}, Lcom/google/android/apps/chrome/services/SyncSignInAccountListener;->get()Lcom/google/android/apps/chrome/services/SyncSignInAccountListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/services/ui/AddGoogleAccountDialogFragment;->setListener(Lcom/google/android/apps/chrome/services/ui/AddGoogleAccountDialogFragment$Listener;)V

    .line 846
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/Preferences;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/services/ui/AddGoogleAccountDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getCurrentFragment()Landroid/app/Fragment;
    .locals 2

    .prologue
    .line 399
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/Preferences;->mFragments:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/Preferences;->mFragments:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 400
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/Preferences;->mFragments:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/Preferences;->mFragments:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Fragment;

    .line 402
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getFragmentForTest(Ljava/lang/Class;)Landroid/app/Fragment;
    .locals 3

    .prologue
    .line 430
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/Preferences;->mFragments:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 431
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/Fragment;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 432
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Fragment;

    .line 435
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getHeaders()Ljava/util/List;
    .locals 1

    .prologue
    .line 258
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/Preferences;->mHeaders:Ljava/util/List;

    return-object v0
.end method

.method public isValidFragment(Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 923
    move v0, v1

    :goto_0
    sget-object v2, Lcom/google/android/apps/chrome/preferences/Preferences;->ENTRY_FRAGMENTS:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 924
    sget-object v2, Lcom/google/android/apps/chrome/preferences/Preferences;->ENTRY_FRAGMENTS:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v1, 0x1

    .line 926
    :cond_0
    return v1

    .line 923
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public onAccountSelected(Landroid/app/Activity;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 871
    invoke-static {}, Lcom/google/android/apps/chrome/services/SyncSignInAccountListener;->get()Lcom/google/android/apps/chrome/services/SyncSignInAccountListener;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/chrome/services/SyncSignInAccountListener;->onAccountSelected(Landroid/app/Activity;Ljava/lang/String;)V

    .line 872
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 852
    if-ne p1, v2, :cond_1

    .line 853
    invoke-static {p0}, Lorg/chromium/sync/signin/AccountManagerHelper;->get(Landroid/content/Context;)Lorg/chromium/sync/signin/AccountManagerHelper;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/sync/signin/AccountManagerHelper;->getGoogleAccountNames()Ljava/util/List;

    move-result-object v0

    .line 855
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-ne v1, v2, :cond_0

    .line 856
    invoke-static {}, Lcom/google/android/apps/chrome/services/SyncSignInAccountListener;->get()Lcom/google/android/apps/chrome/services/SyncSignInAccountListener;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, p0, v0}, Lcom/google/android/apps/chrome/services/SyncSignInAccountListener;->onAccountSelected(Landroid/app/Activity;Ljava/lang/String;)V

    .line 867
    :cond_0
    :goto_0
    return-void

    .line 858
    :cond_1
    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    if-eqz p3, :cond_0

    .line 859
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/Preferences;->getCurrentFragment()Landroid/app/Fragment;

    move-result-object v0

    .line 860
    if-eqz v0, :cond_0

    instance-of v1, v0, Lcom/google/android/apps/chrome/preferences/password/ManageSavedPasswordsPreferences;

    if-eqz v1, :cond_0

    .line 862
    check-cast v0, Lcom/google/android/apps/chrome/preferences/password/ManageSavedPasswordsPreferences;

    .line 864
    invoke-virtual {v0, p3}, Lcom/google/android/apps/chrome/preferences/password/ManageSavedPasswordsPreferences;->onPasswordDeleted(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public onAttachFragment(Landroid/app/Fragment;)V
    .locals 2

    .prologue
    .line 393
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onAttachFragment(Landroid/app/Fragment;)V

    .line 394
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/Preferences;->mFragments:Ljava/util/List;

    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 395
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/Preferences;->invalidateOptionsMenu()V

    .line 396
    return-void
.end method

.method public onBuildHeaders(Ljava/util/List;)V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x0

    .line 184
    invoke-static {p0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->setSharedPreferenceValueFromNative()V

    .line 185
    invoke-static {p0}, Lcom/google/android/apps/chrome/preferences/Preferences;->isTabletPreferencesUi(Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 186
    sget v0, Lcom/google/android/apps/chrome/R$xml;->preference_headers_tablet:I

    invoke-virtual {p0, v0, p1}, Lcom/google/android/apps/chrome/preferences/Preferences;->loadHeadersFromResource(ILjava/util/List;)V

    .line 195
    :cond_0
    :goto_0
    invoke-static {}, Lorg/chromium/chrome/browser/signin/SigninManager;->isNewProfileManagementEnabled()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 202
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/preferences/Preferences;->setUsersSectionSummary(Ljava/util/List;)V

    .line 212
    :goto_1
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/preferences/Preferences;->setDefaultSearchEngineSummary(Ljava/util/List;)V

    .line 213
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/preferences/Preferences;->updateBandwidthReductionPreferencesSummary(Ljava/util/List;)V

    .line 215
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/Preferences;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "display_home_as_up"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 216
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/Preferences;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v7, v7}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    .line 221
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 222
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceActivity$Header;

    iget-wide v2, v0, Landroid/preference/PreferenceActivity$Header;->id:J

    .line 223
    sget v0, Lcom/google/android/apps/chrome/R$id;->homepage:I

    int-to-long v4, v0

    cmp-long v0, v2, v4

    if-nez v0, :cond_8

    invoke-static {}, Lcom/google/android/apps/chrome/partnercustomizations/HomepageManager;->shouldShowHomepageSetting()Z

    move-result v0

    if-nez v0, :cond_8

    .line 224
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_2

    .line 188
    :cond_3
    sget v0, Lcom/google/android/apps/chrome/R$xml;->preference_headers_phone:I

    invoke-virtual {p0, v0, p1}, Lcom/google/android/apps/chrome/preferences/Preferences;->loadHeadersFromResource(ILjava/util/List;)V

    .line 189
    invoke-static {}, Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings;->getInstance()Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings;->isDataReductionProxyAllowed()Z

    move-result v0

    .line 191
    if-nez v0, :cond_0

    .line 192
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 193
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceActivity$Header;

    iget-wide v2, v0, Landroid/preference/PreferenceActivity$Header;->id:J

    sget v0, Lcom/google/android/apps/chrome/R$id;->reduce_data_usage_settings:I

    int-to-long v4, v0

    cmp-long v0, v2, v4

    if-nez v0, :cond_4

    .line 194
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 205
    :cond_5
    sget-boolean v0, Lcom/google/android/apps/chrome/preferences/Preferences;->$assertionsDisabled:Z

    if-nez v0, :cond_6

    invoke-interface {p1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceActivity$Header;

    iget-wide v0, v0, Landroid/preference/PreferenceActivity$Header;->id:J

    sget v2, Lcom/google/android/apps/chrome/R$id;->sync_account:I

    int-to-long v2, v2

    cmp-long v0, v0, v2

    if-eqz v0, :cond_6

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 206
    :cond_6
    invoke-interface {p1, v6}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 208
    sget-boolean v0, Lcom/google/android/apps/chrome/preferences/Preferences;->$assertionsDisabled:Z

    if-nez v0, :cond_7

    invoke-interface {p1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceActivity$Header;

    iget v0, v0, Landroid/preference/PreferenceActivity$Header;->titleRes:I

    sget v1, Lcom/google/android/apps/chrome/R$string;->prefs_section_basics:I

    if-eq v0, v1, :cond_7

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 209
    :cond_7
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/preferences/Preferences;->addAccountHeader(Ljava/util/List;)V

    goto/16 :goto_1

    .line 225
    :cond_8
    sget v0, Lcom/google/android/apps/chrome/R$id;->document_mode:I

    int-to-long v4, v0

    cmp-long v0, v2, v4

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/Preferences;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/utilities/FeatureUtilitiesInternal;->isDocumentModeEligible(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 227
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto/16 :goto_2

    .line 231
    :cond_9
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/Preferences;->mHeaders:Ljava/util/List;

    if-eqz v0, :cond_a

    .line 232
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/Preferences;->mHeaders:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 233
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/Preferences;->mHeaders:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 235
    :cond_a
    return-void
.end method

.method public onContentChanged()V
    .locals 4

    .prologue
    .line 239
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onContentChanged()V

    .line 241
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 243
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/Preferences;->getListView()Landroid/widget/ListView;

    move-result-object v0

    .line 244
    if-nez v0, :cond_1

    .line 251
    :cond_0
    :goto_0
    return-void

    .line 245
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/Preferences;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/chrome/R$dimen;->preference_fragment_padding_side:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    .line 247
    invoke-virtual {v0}, Landroid/widget/ListView;->getPaddingTop()I

    move-result v2

    .line 248
    invoke-virtual {v0}, Landroid/widget/ListView;->getPaddingBottom()I

    move-result v3

    .line 249
    invoke-virtual {v0, v1, v2, v1, v3}, Landroid/widget/ListView;->setPadding(IIII)V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 140
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/Preferences;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/ChromeMobileApplication;

    const/4 v1, 0x1

    invoke-virtual {v0, p0, v1}, Lcom/google/android/apps/chrome/ChromeMobileApplication;->startBrowserProcessesAndLoadLibrariesSync(Landroid/content/Context;Z)V
    :try_end_0
    .catch Lorg/chromium/base/library_loader/ProcessInitException; {:try_start_0 .. :try_end_0} :catch_0

    .line 151
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 153
    invoke-static {}, Lcom/google/android/apps/chrome/ActivityLaunchNotifier;->getInstance()Lcom/google/android/apps/chrome/ActivityLaunchNotifier;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/apps/chrome/ActivityLaunchNotifier;->addListener(Lcom/google/android/apps/chrome/ActivityLaunchNotifier$ActivityLaunchListener;)V

    .line 154
    invoke-static {p0}, Lcom/google/android/apps/chrome/preferences/Preferences;->isTabletPreferencesUi(Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 155
    sget v0, Lcom/google/android/apps/chrome/R$string;->preferences:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/Preferences;->setTitle(I)V

    .line 158
    :cond_0
    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->setPathValuesForAboutChrome()V

    .line 162
    invoke-static {p0}, Landroid/nfc/NfcAdapter;->getDefaultAdapter(Landroid/content/Context;)Landroid/nfc/NfcAdapter;

    move-result-object v0

    .line 163
    if-eqz v0, :cond_1

    const/4 v1, 0x0

    const/4 v2, 0x0

    new-array v2, v2, [Landroid/app/Activity;

    invoke-virtual {v0, v1, p0, v2}, Landroid/nfc/NfcAdapter;->setNdefPushMessage(Landroid/nfc/NdefMessage;Landroid/app/Activity;[Landroid/app/Activity;)V

    .line 165
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/Preferences;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/chrome/browser/signin/SigninManager;->get(Landroid/content/Context;)Lorg/chromium/chrome/browser/signin/SigninManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lorg/chromium/chrome/browser/signin/SigninManager;->addSignInAllowedObserver(Lorg/chromium/chrome/browser/signin/SigninManager$SignInAllowedObserver;)V

    .line 166
    invoke-static {p0}, Lorg/chromium/chrome/browser/profiles/ProfileDownloader;->addObserver(Lorg/chromium/chrome/browser/profiles/ProfileDownloader$Observer;)V

    .line 168
    invoke-static {}, Lcom/google/android/apps/chrome/utilities/DocumentApiDelegate;->create()Lcom/google/android/apps/chrome/utilities/DocumentApiDelegate;

    move-result-object v0

    .line 169
    if-eqz v0, :cond_2

    .line 170
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/Preferences;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/chrome/R$string;->app_name:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/Preferences;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v3, Lcom/google/android/apps/chrome/R$mipmap;->app_icon:I

    invoke-static {v1, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/Preferences;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v4, Lcom/google/android/apps/chrome/R$color;->default_primary_color:I

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    move-object v1, p0

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/chrome/utilities/DocumentApiDelegate;->updateTaskDescription(Landroid/app/Activity;Ljava/lang/String;Landroid/graphics/Bitmap;IZ)V

    .line 174
    :cond_2
    :goto_0
    return-void

    .line 142
    :catch_0
    move-exception v0

    .line 143
    const-string/jumbo v1, "Preferences"

    const-string/jumbo v2, "Failed to start browser process."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 148
    const/4 v0, -0x1

    invoke-static {v0}, Ljava/lang/System;->exit(I)V

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .prologue
    .line 710
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    .line 711
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/Preferences;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 712
    sget v1, Lcom/google/android/apps/chrome/R$menu;->preferences_menu:I

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 713
    const/4 v0, 0x1

    return v0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 701
    invoke-static {p0}, Lorg/chromium/chrome/browser/profiles/ProfileDownloader;->removeObserver(Lorg/chromium/chrome/browser/profiles/ProfileDownloader$Observer;)V

    .line 702
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/Preferences;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/chrome/browser/signin/SigninManager;->get(Landroid/content/Context;)Lorg/chromium/chrome/browser/signin/SigninManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lorg/chromium/chrome/browser/signin/SigninManager;->removeSignInAllowedObserver(Lorg/chromium/chrome/browser/signin/SigninManager$SignInAllowedObserver;)V

    .line 703
    invoke-static {}, Lcom/google/android/apps/chrome/ActivityLaunchNotifier;->getInstance()Lcom/google/android/apps/chrome/ActivityLaunchNotifier;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/apps/chrome/ActivityLaunchNotifier;->removeListener(Lcom/google/android/apps/chrome/ActivityLaunchNotifier$ActivityLaunchListener;)V

    .line 704
    invoke-static {}, Lcom/google/android/apps/chrome/feedback/GoogleFeedback;->cleanup()V

    .line 705
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onDestroy()V

    .line 706
    return-void
.end method

.method public onHeaderClick(Landroid/preference/PreferenceActivity$Header;I)V
    .locals 4

    .prologue
    .line 356
    iget-wide v0, p1, Landroid/preference/PreferenceActivity$Header;->id:J

    invoke-static {p0, v0, v1}, Lcom/google/android/apps/chrome/preferences/ManagedPreferencesUtils;->isIdDisabledByPolicy(Landroid/content/Context;J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 357
    invoke-static {p0}, Lcom/google/android/apps/chrome/preferences/ManagedPreferencesUtils;->showManagedByAdministratorToast(Landroid/content/Context;)V

    .line 369
    :goto_0
    return-void

    .line 361
    :cond_0
    invoke-static {}, Lorg/chromium/chrome/browser/signin/SigninManager;->isNewProfileManagementEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-wide v0, p1, Landroid/preference/PreferenceActivity$Header;->id:J

    sget v2, Lcom/google/android/apps/chrome/R$id;->sync_account:I

    int-to-long v2, v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    invoke-static {p0}, Lorg/chromium/sync/signin/ChromeSigninController;->get(Landroid/content/Context;)Lorg/chromium/sync/signin/ChromeSigninController;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/sync/signin/ChromeSigninController;->isSignedIn()Z

    move-result v0

    if-nez v0, :cond_1

    .line 364
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/Preferences;->displayAccountPicker()V

    goto :goto_0

    .line 368
    :cond_1
    invoke-super {p0, p1, p2}, Landroid/preference/PreferenceActivity;->onHeaderClick(Landroid/preference/PreferenceActivity$Header;I)V

    goto :goto_0
.end method

.method public onLaunchActivity(Landroid/content/Intent;Landroid/view/Window;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 912
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/Preferences;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v1

    iget-object v1, v1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    .line 913
    iget-object v1, v1, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    if-eqz p2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/Preferences;->getWindow()Landroid/view/Window;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/Preferences;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 915
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/Preferences;->finish()V

    .line 916
    const/4 v0, 0x1

    .line 918
    :cond_1
    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v0, 0x1

    .line 805
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    .line 806
    const v2, 0x102002c

    if-ne v1, v2, :cond_1

    .line 807
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/Preferences;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentManager;->getBackStackEntryCount()I

    move-result v1

    if-lez v1, :cond_0

    .line 808
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/Preferences;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentManager;->popBackStack()V

    .line 826
    :goto_0
    return v0

    .line 810
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/Preferences;->finish()V

    goto :goto_0

    .line 813
    :cond_1
    sget v2, Lcom/google/android/apps/chrome/R$id;->menu_id_sign_in_to_chrome:I

    if-eq v1, v2, :cond_2

    sget v2, Lcom/google/android/apps/chrome/R$id;->menu_id_sign_in_to_chrome_new:I

    if-ne v1, v2, :cond_3

    .line 815
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/Preferences;->displayAccountPicker()V

    goto :goto_0

    .line 817
    :cond_3
    sget v2, Lcom/google/android/apps/chrome/R$id;->menu_id_help_default:I

    if-ne v1, v2, :cond_4

    .line 818
    const-string/jumbo v1, "mobile_settings"

    invoke-static {p0, v1, v3}, Lcom/google/android/apps/chrome/help/GoogleHelpUtils;->show(Landroid/app/Activity;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 820
    :cond_4
    sget v2, Lcom/google/android/apps/chrome/R$id;->menu_id_import_bookmarks:I

    if-ne v1, v2, :cond_5

    .line 821
    new-instance v1, Lcom/google/android/apps/chrome/bookmark/ImportBookmarksAlertDialog;

    invoke-direct {v1}, Lcom/google/android/apps/chrome/bookmark/ImportBookmarksAlertDialog;-><init>()V

    .line 823
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/Preferences;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/chrome/bookmark/ImportBookmarksAlertDialog;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    .line 826
    :cond_5
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 178
    invoke-static {}, Lcom/google/android/apps/chrome/ChromeMobileApplication;->flushPersistentData()V

    .line 179
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onPause()V

    .line 180
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 718
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/Preferences;->getCurrentFragment()Landroid/app/Fragment;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/Preferences;->getCurrentFragment()Landroid/app/Fragment;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/google/android/apps/chrome/preferences/PrivacyPreferences;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 720
    sget v0, Lcom/google/android/apps/chrome/R$id;->PRIVACY_MENU:I

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/chrome/preferences/Preferences;->setMenuVisibility(Landroid/view/Menu;I)V

    .line 752
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/Preferences;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/chrome/browser/signin/SigninManager;->get(Landroid/content/Context;)Lorg/chromium/chrome/browser/signin/SigninManager;

    move-result-object v0

    .line 753
    invoke-virtual {v0}, Lorg/chromium/chrome/browser/signin/SigninManager;->isSignInAllowed()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p0}, Lorg/chromium/chrome/browser/util/FeatureUtilities;->canAllowSync(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 754
    :cond_1
    sget v0, Lcom/google/android/apps/chrome/R$id;->menu_id_sign_in_to_chrome:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 755
    sget v0, Lcom/google/android/apps/chrome/R$id;->menu_id_sign_in_to_chrome_new:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 758
    :cond_2
    new-instance v0, Lcom/google/android/apps/chrome/bookmark/AndroidBrowserImporter;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/bookmark/AndroidBrowserImporter;-><init>(Landroid/content/Context;)V

    .line 759
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/bookmark/AndroidBrowserImporter;->areBookmarksAccessible()Z

    move-result v0

    if-nez v0, :cond_3

    .line 760
    sget v0, Lcom/google/android/apps/chrome/R$id;->menu_id_import_bookmarks:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 764
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/Preferences;->getCurrentFragment()Landroid/app/Fragment;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/Preferences;->getCurrentFragment()Landroid/app/Fragment;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/google/android/apps/chrome/preferences/BasicsPreferences;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/Preferences;->getCurrentFragment()Landroid/app/Fragment;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/google/android/apps/chrome/preferences/AdvancedPreferences;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 767
    sget v0, Lcom/google/android/apps/chrome/R$id;->menu_id_import_bookmarks:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 771
    :cond_4
    invoke-static {p0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getSignOutAllowed()Z

    move-result v0

    .line 773
    if-nez v0, :cond_5

    .line 774
    sget v0, Lcom/google/android/apps/chrome/R$id;->menu_id_disconnect_sync_account:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 778
    :cond_5
    invoke-static {}, Lorg/chromium/chrome/browser/signin/SigninManager;->isNewProfileManagementEnabled()Z

    move-result v0

    if-nez v0, :cond_11

    .line 779
    sget v0, Lcom/google/android/apps/chrome/R$id;->menu_id_sign_in_to_chrome_new:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 780
    sget v0, Lcom/google/android/apps/chrome/R$id;->menu_id_disconnect_sync_account:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$string;->signout_title:I

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    .line 785
    :goto_1
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    .line 786
    const/4 v0, 0x1

    return v0

    .line 721
    :cond_6
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/Preferences;->getCurrentFragment()Landroid/app/Fragment;

    move-result-object v0

    if-eqz v0, :cond_7

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/Preferences;->getCurrentFragment()Landroid/app/Fragment;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/google/android/apps/chrome/preferences/ProtectedContentPreferences;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 723
    sget v0, Lcom/google/android/apps/chrome/R$id;->PROTECTED_CONTENT_MENU:I

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/chrome/preferences/Preferences;->setMenuVisibility(Landroid/view/Menu;I)V

    goto/16 :goto_0

    .line 724
    :cond_7
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/Preferences;->getCurrentFragment()Landroid/app/Fragment;

    move-result-object v0

    if-eqz v0, :cond_8

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/Preferences;->getCurrentFragment()Landroid/app/Fragment;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/google/android/apps/chrome/preferences/TranslatePreferences;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 726
    sget v0, Lcom/google/android/apps/chrome/R$id;->TRANSLATE_MENU:I

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/chrome/preferences/Preferences;->setMenuVisibility(Landroid/view/Menu;I)V

    goto/16 :goto_0

    .line 727
    :cond_8
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/Preferences;->getCurrentFragment()Landroid/app/Fragment;

    move-result-object v0

    if-eqz v0, :cond_9

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/Preferences;->getCurrentFragment()Landroid/app/Fragment;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 729
    sget v0, Lcom/google/android/apps/chrome/R$id;->SYNC_ACCOUNT_MENU:I

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/chrome/preferences/Preferences;->setMenuVisibility(Landroid/view/Menu;I)V

    goto/16 :goto_0

    .line 730
    :cond_9
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/Preferences;->getCurrentFragment()Landroid/app/Fragment;

    move-result-object v0

    if-eqz v0, :cond_a

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/Preferences;->getCurrentFragment()Landroid/app/Fragment;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 732
    sget v0, Lcom/google/android/apps/chrome/R$id;->ACCOUNT_MANAGEMENT_MENU:I

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/chrome/preferences/Preferences;->setMenuVisibility(Landroid/view/Menu;I)V

    goto/16 :goto_0

    .line 733
    :cond_a
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/Preferences;->getCurrentFragment()Landroid/app/Fragment;

    move-result-object v0

    if-eqz v0, :cond_b

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/Preferences;->getCurrentFragment()Landroid/app/Fragment;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 735
    sget v0, Lcom/google/android/apps/chrome/R$id;->SYNC_MENU:I

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/chrome/preferences/Preferences;->setMenuVisibility(Landroid/view/Menu;I)V

    goto/16 :goto_0

    .line 736
    :cond_b
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/Preferences;->getCurrentFragment()Landroid/app/Fragment;

    move-result-object v0

    if-eqz v0, :cond_c

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/Preferences;->getCurrentFragment()Landroid/app/Fragment;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/google/android/apps/chrome/preferences/DoNotTrackPreference;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 738
    sget v0, Lcom/google/android/apps/chrome/R$id;->DO_NOT_TRACK_MENU:I

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/chrome/preferences/Preferences;->setMenuVisibility(Landroid/view/Menu;I)V

    goto/16 :goto_0

    .line 739
    :cond_c
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/Preferences;->getCurrentFragment()Landroid/app/Fragment;

    move-result-object v0

    if-eqz v0, :cond_d

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/Preferences;->getCurrentFragment()Landroid/app/Fragment;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/google/android/apps/chrome/preferences/ContextualSearchPreferenceFragment;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 741
    sget v0, Lcom/google/android/apps/chrome/R$id;->CONTEXTUAL_SEARCH_MENU:I

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/chrome/preferences/Preferences;->setMenuVisibility(Landroid/view/Menu;I)V

    goto/16 :goto_0

    .line 742
    :cond_d
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/Preferences;->getCurrentFragment()Landroid/app/Fragment;

    move-result-object v0

    if-eqz v0, :cond_f

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/Preferences;->getCurrentFragment()Landroid/app/Fragment;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/google/android/apps/chrome/preferences/AdvancedPreferences;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_e

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/Preferences;->getCurrentFragment()Landroid/app/Fragment;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/google/android/apps/chrome/preferences/BasicsPreferences;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 745
    :cond_e
    sget v0, Lcom/google/android/apps/chrome/R$id;->TOP_LEVEL_MENU:I

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/chrome/preferences/Preferences;->setMenuVisibility(Landroid/view/Menu;I)V

    goto/16 :goto_0

    .line 746
    :cond_f
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/Preferences;->getCurrentFragment()Landroid/app/Fragment;

    move-result-object v0

    if-nez v0, :cond_10

    .line 747
    sget v0, Lcom/google/android/apps/chrome/R$id;->TOP_LEVEL_MENU:I

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/chrome/preferences/Preferences;->setMenuVisibility(Landroid/view/Menu;I)V

    goto/16 :goto_0

    .line 748
    :cond_10
    invoke-static {p0}, Lcom/google/android/apps/chrome/preferences/Preferences;->isTabletPreferencesUi(Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 749
    sget v0, Lcom/google/android/apps/chrome/R$id;->DEFAULT_MENU:I

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/chrome/preferences/Preferences;->setMenuVisibility(Landroid/view/Menu;I)V

    goto/16 :goto_0

    .line 782
    :cond_11
    sget v0, Lcom/google/android/apps/chrome/R$id;->menu_id_sign_in_to_chrome:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto/16 :goto_1
.end method

.method public onProfileDownloaded(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 0

    .prologue
    .line 890
    invoke-static {p1, p2, p3}, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->updateUserNamePictureCache(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    .line 891
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/Preferences;->updatePreferencesHeadersAndMenu()V

    .line 892
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 449
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onResume()V

    .line 450
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/Preferences;->updatePreferencesHeadersAndMenu()V

    .line 451
    return-void
.end method

.method public onSignInAllowedChanged()V
    .locals 0

    .prologue
    .line 885
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/Preferences;->updatePreferencesHeadersAndMenu()V

    .line 886
    return-void
.end method

.method public popFragmentList(Landroid/app/Fragment;)V
    .locals 3

    .prologue
    .line 412
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/Preferences;->mFragments:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 413
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/Preferences;->mFragments:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_0

    .line 414
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/Preferences;->mFragments:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 415
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    if-ne v2, p1, :cond_1

    .line 416
    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/Preferences;->mFragments:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 421
    :cond_0
    return-void

    .line 413
    :cond_1
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0
.end method

.method public setListAdapter(Landroid/widget/ListAdapter;)V
    .locals 3

    .prologue
    .line 456
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/preferences/Preferences;->mHeaders:Ljava/util/List;

    .line 459
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {p1}, Landroid/widget/ListAdapter;->getCount()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 460
    iget-object v2, p0, Lcom/google/android/apps/chrome/preferences/Preferences;->mHeaders:Ljava/util/List;

    invoke-interface {p1, v1}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceActivity$Header;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 459
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 462
    :cond_0
    new-instance v0, Lcom/google/android/apps/chrome/preferences/Preferences$HeaderAdapter;

    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/Preferences;->mHeaders:Ljava/util/List;

    invoke-direct {v0, p0, p0, v1}, Lcom/google/android/apps/chrome/preferences/Preferences$HeaderAdapter;-><init>(Lcom/google/android/apps/chrome/preferences/Preferences;Landroid/content/Context;Ljava/util/List;)V

    invoke-super {p0, v0}, Landroid/preference/PreferenceActivity;->setListAdapter(Landroid/widget/ListAdapter;)V

    .line 463
    return-void
.end method

.method public startActivityForResult(Landroid/content/Intent;I)V
    .locals 2

    .prologue
    .line 900
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/Preferences;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v0

    .line 901
    if-eqz v0, :cond_0

    .line 902
    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    .line 903
    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 904
    invoke-static {}, Lcom/google/android/apps/chrome/ActivityLaunchNotifier;->getInstance()Lcom/google/android/apps/chrome/ActivityLaunchNotifier;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/Preferences;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/android/apps/chrome/ActivityLaunchNotifier;->onLaunchActivity(Landroid/content/Intent;Landroid/view/Window;)Z

    .line 907
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/preference/PreferenceActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 908
    return-void
.end method

.method public updatePreferencesHeadersAndMenu()V
    .locals 0

    .prologue
    .line 443
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/Preferences;->invalidateHeaders()V

    .line 444
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/Preferences;->invalidateOptionsMenu()V

    .line 445
    return-void
.end method
