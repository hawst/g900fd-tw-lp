.class public Lcom/google/android/apps/chrome/toolbar/ChromeToolbarProgressBar;
.super Lcom/google/android/apps/chrome/toolbar/ToolbarProgressBar;
.source "ChromeToolbarProgressBar.java"


# instance fields
.field private mHideAnimator:Landroid/animation/Animator;

.field private mShowAnimator:Landroid/animation/Animator;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/toolbar/ToolbarProgressBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 34
    return-void
.end method

.method private buildAnimators()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x64

    const/4 v2, 0x2

    .line 58
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ChromeToolbarProgressBar;->mShowAnimator:Landroid/animation/Animator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ChromeToolbarProgressBar;->mShowAnimator:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ChromeToolbarProgressBar;->mShowAnimator:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->end()V

    .line 59
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ChromeToolbarProgressBar;->mHideAnimator:Landroid/animation/Animator;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ChromeToolbarProgressBar;->mHideAnimator:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ChromeToolbarProgressBar;->mHideAnimator:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->end()V

    .line 61
    :cond_1
    sget-object v0, Landroid/view/View;->SCALE_Y:Landroid/util/Property;

    new-array v1, v2, [F

    fill-array-data v1, :array_0

    invoke-static {p0, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ChromeToolbarProgressBar;->mShowAnimator:Landroid/animation/Animator;

    .line 62
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ChromeToolbarProgressBar;->mShowAnimator:Landroid/animation/Animator;

    invoke-virtual {v0, v4, v5}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 63
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ChromeToolbarProgressBar;->mShowAnimator:Landroid/animation/Animator;

    sget-object v1, Lorg/chromium/ui/interpolators/BakedBezierInterpolator;->FADE_IN_CURVE:Lorg/chromium/ui/interpolators/BakedBezierInterpolator;

    invoke-virtual {v0, v1}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 64
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ChromeToolbarProgressBar;->mShowAnimator:Landroid/animation/Animator;

    new-instance v1, Lcom/google/android/apps/chrome/toolbar/ChromeToolbarProgressBar$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/toolbar/ChromeToolbarProgressBar$1;-><init>(Lcom/google/android/apps/chrome/toolbar/ChromeToolbarProgressBar;)V

    invoke-virtual {v0, v1}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 71
    sget-object v0, Landroid/view/View;->SCALE_Y:Landroid/util/Property;

    new-array v1, v2, [F

    fill-array-data v1, :array_1

    invoke-static {p0, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ChromeToolbarProgressBar;->mHideAnimator:Landroid/animation/Animator;

    .line 72
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ChromeToolbarProgressBar;->mHideAnimator:Landroid/animation/Animator;

    invoke-virtual {v0, v4, v5}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 73
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ChromeToolbarProgressBar;->mHideAnimator:Landroid/animation/Animator;

    sget-object v1, Lorg/chromium/ui/interpolators/BakedBezierInterpolator;->FADE_OUT_CURVE:Lorg/chromium/ui/interpolators/BakedBezierInterpolator;

    invoke-virtual {v0, v1}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 74
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ChromeToolbarProgressBar;->mHideAnimator:Landroid/animation/Animator;

    new-instance v1, Lcom/google/android/apps/chrome/toolbar/ChromeToolbarProgressBar$2;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/toolbar/ChromeToolbarProgressBar$2;-><init>(Lcom/google/android/apps/chrome/toolbar/ChromeToolbarProgressBar;)V

    invoke-virtual {v0, v1}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 80
    return-void

    .line 61
    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data

    .line 71
    :array_1
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method


# virtual methods
.method protected onLayout(ZIIII)V
    .locals 1

    .prologue
    .line 38
    invoke-super/range {p0 .. p5}, Lcom/google/android/apps/chrome/toolbar/ToolbarProgressBar;->onLayout(ZIIII)V

    .line 39
    invoke-direct {p0}, Lcom/google/android/apps/chrome/toolbar/ChromeToolbarProgressBar;->buildAnimators()V

    .line 40
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/toolbar/ChromeToolbarProgressBar;->getHeight()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/toolbar/ChromeToolbarProgressBar;->setPivotY(F)V

    .line 41
    return-void
.end method

.method public declared-synchronized setProgress(I)V
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 84
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/toolbar/ChromeToolbarProgressBar;->getProgress()I

    move-result v2

    if-lez v2, :cond_5

    move v2, v0

    .line 85
    :goto_0
    if-lez p1, :cond_6

    .line 87
    :goto_1
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/toolbar/ToolbarProgressBar;->setProgress(I)V

    .line 89
    if-eq v2, v0, :cond_4

    .line 90
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ChromeToolbarProgressBar;->mShowAnimator:Landroid/animation/Animator;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ChromeToolbarProgressBar;->mHideAnimator:Landroid/animation/Animator;

    if-nez v1, :cond_1

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/toolbar/ChromeToolbarProgressBar;->buildAnimators()V

    .line 92
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ChromeToolbarProgressBar;->mShowAnimator:Landroid/animation/Animator;

    invoke-virtual {v1}, Landroid/animation/Animator;->isRunning()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ChromeToolbarProgressBar;->mShowAnimator:Landroid/animation/Animator;

    invoke-virtual {v1}, Landroid/animation/Animator;->end()V

    .line 93
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ChromeToolbarProgressBar;->mHideAnimator:Landroid/animation/Animator;

    invoke-virtual {v1}, Landroid/animation/Animator;->isRunning()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ChromeToolbarProgressBar;->mHideAnimator:Landroid/animation/Animator;

    invoke-virtual {v1}, Landroid/animation/Animator;->end()V

    .line 95
    :cond_3
    if-eqz v0, :cond_7

    .line 96
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ChromeToolbarProgressBar;->mShowAnimator:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->start()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 101
    :cond_4
    :goto_2
    monitor-exit p0

    return-void

    :cond_5
    move v2, v1

    .line 84
    goto :goto_0

    :cond_6
    move v0, v1

    .line 85
    goto :goto_1

    .line 98
    :cond_7
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ChromeToolbarProgressBar;->mHideAnimator:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->start()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 84
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setProgressDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 4

    .prologue
    .line 45
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/toolbar/ChromeToolbarProgressBar;->getProgressDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 47
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/toolbar/ToolbarProgressBar;->setProgressDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 49
    if-eqz v1, :cond_0

    instance-of v0, p1, Landroid/graphics/drawable/LayerDrawable;

    if-eqz v0, :cond_0

    .line 50
    check-cast p1, Landroid/graphics/drawable/LayerDrawable;

    .line 51
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1}, Landroid/graphics/drawable/LayerDrawable;->getNumberOfLayers()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 52
    invoke-virtual {p1, v0}, Landroid/graphics/drawable/LayerDrawable;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 51
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 55
    :cond_0
    return-void
.end method
