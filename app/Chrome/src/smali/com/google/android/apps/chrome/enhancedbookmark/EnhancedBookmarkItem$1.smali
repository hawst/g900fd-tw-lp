.class Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem$1;
.super Ljava/lang/Object;
.source "EnhancedBookmarkItem.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;)V
    .locals 0

    .prologue
    .line 206
    iput-object p1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem$1;->this$0:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3

    .prologue
    .line 210
    if-nez p3, :cond_1

    .line 211
    new-instance v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;

    iget-object v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem$1;->this$0:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;

    # getter for: Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;->mDelegate:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;
    invoke-static {v1}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;->access$000(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;)Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;->getModel()Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem$1;->this$0:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;

    # getter for: Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;->mBookmarkId:Lorg/chromium/components/bookmarks/BookmarkId;
    invoke-static {v2}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;->access$100(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;)Lorg/chromium/components/bookmarks/BookmarkId;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;-><init>(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;Lorg/chromium/components/bookmarks/BookmarkId;)V

    iget-object v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem$1;->this$0:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;

    # getter for: Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;->mDelegate:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;
    invoke-static {v1}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;->access$000(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;)Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->show(Landroid/app/FragmentManager;)V

    .line 219
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem$1;->this$0:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;

    # getter for: Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;->mPopupMenu:Landroid/widget/ListPopupWindow;
    invoke-static {v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;->access$200(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;)Landroid/widget/ListPopupWindow;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListPopupWindow;->dismiss()V

    .line 220
    return-void

    .line 213
    :cond_1
    const/4 v0, 0x1

    if-ne p3, v0, :cond_2

    .line 214
    new-instance v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog;

    iget-object v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem$1;->this$0:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;

    # getter for: Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;->mDelegate:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;
    invoke-static {v1}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;->access$000(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;)Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;->getModel()Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem$1;->this$0:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;

    # getter for: Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;->mBookmarkId:Lorg/chromium/components/bookmarks/BookmarkId;
    invoke-static {v2}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;->access$100(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;)Lorg/chromium/components/bookmarks/BookmarkId;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog;-><init>(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;Lorg/chromium/components/bookmarks/BookmarkId;)V

    iget-object v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem$1;->this$0:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;

    # getter for: Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;->mDelegate:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;
    invoke-static {v1}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;->access$000(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;)Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog;->show(Landroid/app/FragmentManager;)V

    goto :goto_0

    .line 216
    :cond_2
    const/4 v0, 0x2

    if-ne p3, v0, :cond_0

    .line 217
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem$1;->this$0:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;

    # getter for: Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;->mDelegate:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;
    invoke-static {v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;->access$000(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;)Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;->getModel()Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem$1;->this$0:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;

    # getter for: Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;->mBookmarkId:Lorg/chromium/components/bookmarks/BookmarkId;
    invoke-static {v1}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;->access$100(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;)Lorg/chromium/components/bookmarks/BookmarkId;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;->deleteBookmarkItem(Lorg/chromium/components/bookmarks/BookmarkId;)V

    goto :goto_0
.end method
