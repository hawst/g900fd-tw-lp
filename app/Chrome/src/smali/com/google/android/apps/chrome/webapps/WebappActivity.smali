.class public Lcom/google/android/apps/chrome/webapps/WebappActivity;
.super Lcom/google/android/apps/chrome/webapps/FullScreenActivity;
.source "WebappActivity.java"


# instance fields
.field private mBrandColor:Ljava/lang/Integer;

.field private final mCleanupTask:Landroid/os/AsyncTask;

.field private final mDelegate:Lcom/google/android/apps/chrome/utilities/DocumentApiDelegate;

.field private mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

.field private mIsInitialized:Z

.field private mObserver:Lorg/chromium/content/browser/WebContentsObserverAndroid;

.field private mSlidingLayout:Lcom/google/android/apps/chrome/webapps/SlidingLayout;

.field private mUrlBar:Lcom/google/android/apps/chrome/webapps/WebappUrlBar;

.field private final mWebappInfo:Lcom/google/android/apps/chrome/webapps/WebappInfo;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/google/android/apps/chrome/webapps/FullScreenActivity;-><init>()V

    .line 63
    invoke-static {}, Lcom/google/android/apps/chrome/webapps/WebappInfo;->createEmpty()Lcom/google/android/apps/chrome/webapps/WebappInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/webapps/WebappActivity;->mWebappInfo:Lcom/google/android/apps/chrome/webapps/WebappInfo;

    .line 64
    invoke-direct {p0}, Lcom/google/android/apps/chrome/webapps/WebappActivity;->createCleanupTask()Landroid/os/AsyncTask;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/webapps/WebappActivity;->mCleanupTask:Landroid/os/AsyncTask;

    .line 65
    invoke-static {}, Lcom/google/android/apps/chrome/utilities/DocumentApiDelegate;->create()Lcom/google/android/apps/chrome/utilities/DocumentApiDelegate;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/webapps/WebappActivity;->mDelegate:Lcom/google/android/apps/chrome/utilities/DocumentApiDelegate;

    .line 66
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/webapps/WebappActivity;)Z
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/google/android/apps/chrome/webapps/WebappActivity;->updateUrlBar()Z

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/webapps/WebappActivity;)Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/WebappActivity;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/webapps/WebappActivity;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/WebappActivity;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/chrome/webapps/WebappActivity;)Z
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/google/android/apps/chrome/webapps/WebappActivity;->isWebappDomain()Z

    move-result v0

    return v0
.end method

.method static synthetic access$402(Lcom/google/android/apps/chrome/webapps/WebappActivity;Ljava/lang/Integer;)Ljava/lang/Integer;
    .locals 0

    .prologue
    .line 41
    iput-object p1, p0, Lcom/google/android/apps/chrome/webapps/WebappActivity;->mBrandColor:Ljava/lang/Integer;

    return-object p1
.end method

.method static synthetic access$500(Lcom/google/android/apps/chrome/webapps/WebappActivity;)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/google/android/apps/chrome/webapps/WebappActivity;->updateTaskDescription()V

    return-void
.end method

.method private createCleanupTask()Landroid/os/AsyncTask;
    .locals 1

    .prologue
    .line 326
    new-instance v0, Lcom/google/android/apps/chrome/webapps/WebappActivity$5;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/webapps/WebappActivity$5;-><init>(Lcom/google/android/apps/chrome/webapps/WebappActivity;)V

    return-object v0
.end method

.method private createWebContentsObserver()Lorg/chromium/content/browser/WebContentsObserverAndroid;
    .locals 2

    .prologue
    .line 242
    new-instance v0, Lcom/google/android/apps/chrome/webapps/WebappActivity$3;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/webapps/WebappActivity;->getTab()Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab;->getWebContents()Lorg/chromium/content_public/browser/WebContents;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/chrome/webapps/WebappActivity$3;-><init>(Lcom/google/android/apps/chrome/webapps/WebappActivity;Lorg/chromium/content_public/browser/WebContents;)V

    return-object v0
.end method

.method private initializeFullscreenManager()V
    .locals 6

    .prologue
    .line 103
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/WebappActivity;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    if-nez v0, :cond_0

    .line 104
    new-instance v1, Lcom/google/android/apps/chrome/webapps/WebappActivity$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/webapps/WebappActivity$1;-><init>(Lcom/google/android/apps/chrome/webapps/WebappActivity;)V

    .line 115
    new-instance v0, Lcom/google/android/apps/chrome/webapps/WebappFullscreenManager;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/webapps/WebappActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/webapps/WebappActivity;->getTabModelSelector()Lcom/google/android/apps/chrome/tabmodel/SingleTabModelSelector;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/chrome/webapps/WebappActivity;->mSlidingLayout:Lcom/google/android/apps/chrome/webapps/SlidingLayout;

    iget-object v5, p0, Lcom/google/android/apps/chrome/webapps/WebappActivity;->mUrlBar:Lcom/google/android/apps/chrome/webapps/WebappUrlBar;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/chrome/webapps/WebappFullscreenManager;-><init>(Lcom/google/android/apps/chrome/webapps/WebappFullscreenManager$WebappFullscreenDelegate;Landroid/view/Window;Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;Lcom/google/android/apps/chrome/webapps/SlidingLayout;Landroid/view/View;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/webapps/WebappActivity;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    .line 120
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/webapps/WebappActivity;->getTab()Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/webapps/WebappActivity;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab;->setFullscreenManager(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;)V

    .line 121
    return-void
.end method

.method private isWebappDomain()Z
    .locals 3

    .prologue
    .line 283
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/webapps/WebappActivity;->getTab()Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab;->getUrl()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/webapps/WebappActivity;->getWebappInfo()Lcom/google/android/apps/chrome/webapps/WebappInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/webapps/WebappInfo;->uri()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lorg/chromium/chrome/browser/UrlUtilities;->sameDomainOrHost(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method private updateTaskDescription()V
    .locals 6

    .prologue
    .line 357
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/WebappActivity;->mDelegate:Lcom/google/android/apps/chrome/utilities/DocumentApiDelegate;

    if-nez v0, :cond_0

    .line 365
    :goto_0
    return-void

    .line 358
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/WebappActivity;->mWebappInfo:Lcom/google/android/apps/chrome/webapps/WebappInfo;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/webapps/WebappInfo;->title()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/webapps/WebappActivity;->getTab()Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab;->getTitle()Ljava/lang/String;

    move-result-object v2

    .line 359
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/WebappActivity;->mWebappInfo:Lcom/google/android/apps/chrome/webapps/WebappInfo;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/webapps/WebappInfo;->icon()Landroid/graphics/Bitmap;

    move-result-object v0

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/webapps/WebappActivity;->getTab()Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab;->getFavicon()Landroid/graphics/Bitmap;

    move-result-object v3

    .line 360
    :goto_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/WebappActivity;->mBrandColor:Ljava/lang/Integer;

    if-nez v0, :cond_3

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/webapps/WebappActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$color;->default_primary_color:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    .line 364
    :goto_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/WebappActivity;->mDelegate:Lcom/google/android/apps/chrome/utilities/DocumentApiDelegate;

    iget-object v1, p0, Lcom/google/android/apps/chrome/webapps/WebappActivity;->mBrandColor:Ljava/lang/Integer;

    if-nez v1, :cond_4

    const/4 v5, 0x1

    :goto_4
    move-object v1, p0

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/chrome/utilities/DocumentApiDelegate;->updateTaskDescription(Landroid/app/Activity;Ljava/lang/String;Landroid/graphics/Bitmap;IZ)V

    goto :goto_0

    .line 358
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/WebappActivity;->mWebappInfo:Lcom/google/android/apps/chrome/webapps/WebappInfo;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/webapps/WebappInfo;->title()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 359
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/WebappActivity;->mWebappInfo:Lcom/google/android/apps/chrome/webapps/WebappInfo;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/webapps/WebappInfo;->icon()Landroid/graphics/Bitmap;

    move-result-object v3

    goto :goto_2

    .line 360
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/WebappActivity;->mBrandColor:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    goto :goto_3

    .line 364
    :cond_4
    const/4 v5, 0x0

    goto :goto_4
.end method

.method private updateUrlBar()Z
    .locals 2

    .prologue
    .line 237
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/webapps/WebappActivity;->getTab()Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab;->getUrl()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/webapps/WebappActivity;->getTab()Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab;->getSecurityLevel()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/chrome/webapps/WebappActivity;->updateUrlBar(Ljava/lang/String;I)Z

    move-result v0

    return v0
.end method


# virtual methods
.method protected createPageLayout()Landroid/widget/FrameLayout;
    .locals 3

    .prologue
    .line 188
    new-instance v0, Lcom/google/android/apps/chrome/webapps/WebappActivity$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/webapps/WebappActivity$2;-><init>(Lcom/google/android/apps/chrome/webapps/WebappActivity;)V

    .line 212
    new-instance v1, Lcom/google/android/apps/chrome/webapps/FullscreenContentViewContainer;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/webapps/WebappActivity;->getTabModelSelector()Lcom/google/android/apps/chrome/tabmodel/SingleTabModelSelector;

    move-result-object v2

    invoke-direct {v1, p0, v0, v2}, Lcom/google/android/apps/chrome/webapps/FullscreenContentViewContainer;-><init>(Landroid/content/Context;Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;)V

    return-object v1
.end method

.method protected createTabObserver()Lorg/chromium/chrome/browser/TabObserver;
    .locals 1

    .prologue
    .line 288
    new-instance v0, Lcom/google/android/apps/chrome/webapps/WebappActivity$4;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/webapps/WebappActivity$4;-><init>(Lcom/google/android/apps/chrome/webapps/WebappActivity;)V

    return-object v0
.end method

.method protected createUI(Landroid/widget/FrameLayout;)V
    .locals 2

    .prologue
    .line 181
    new-instance v0, Lcom/google/android/apps/chrome/webapps/WebappUrlBar;

    iget-object v1, p0, Lcom/google/android/apps/chrome/webapps/WebappActivity;->mWebappInfo:Lcom/google/android/apps/chrome/webapps/WebappInfo;

    invoke-direct {v0, v1, p0}, Lcom/google/android/apps/chrome/webapps/WebappUrlBar;-><init>(Lcom/google/android/apps/chrome/webapps/WebappInfo;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/webapps/WebappActivity;->mUrlBar:Lcom/google/android/apps/chrome/webapps/WebappUrlBar;

    .line 182
    new-instance v0, Lcom/google/android/apps/chrome/webapps/SlidingLayout;

    iget-object v1, p0, Lcom/google/android/apps/chrome/webapps/WebappActivity;->mUrlBar:Lcom/google/android/apps/chrome/webapps/WebappUrlBar;

    invoke-direct {v0, p0, v1, p1}, Lcom/google/android/apps/chrome/webapps/SlidingLayout;-><init>(Landroid/content/Context;Lcom/google/android/apps/chrome/webapps/WebappUrlBar;Landroid/view/View;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/webapps/WebappActivity;->mSlidingLayout:Lcom/google/android/apps/chrome/webapps/SlidingLayout;

    .line 183
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/WebappActivity;->mSlidingLayout:Lcom/google/android/apps/chrome/webapps/SlidingLayout;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/webapps/WebappActivity;->setContentView(Landroid/view/View;)V

    .line 184
    return-void
.end method

.method public finishNativeInitialization()V
    .locals 1

    .prologue
    .line 134
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/webapps/WebappActivity;->mIsInitialized:Z

    .line 136
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/WebappActivity;->mWebappInfo:Lcom/google/android/apps/chrome/webapps/WebappInfo;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/webapps/WebappInfo;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/webapps/WebappActivity;->finish()V

    .line 137
    :cond_0
    invoke-super {p0}, Lcom/google/android/apps/chrome/webapps/FullScreenActivity;->finishNativeInitialization()V

    .line 138
    return-void
.end method

.method protected getActivityDirectory()Ljava/io/File;
    .locals 2

    .prologue
    .line 369
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/webapps/WebappActivity;->mWebappInfo:Lcom/google/android/apps/chrome/webapps/WebappInfo;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/webapps/WebappInfo;->id()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/chrome/webapps/WebappActivity;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method getSlidingLayoutForTests()Lcom/google/android/apps/chrome/webapps/SlidingLayout;
    .locals 1

    .prologue
    .line 379
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/WebappActivity;->mSlidingLayout:Lcom/google/android/apps/chrome/webapps/SlidingLayout;

    return-object v0
.end method

.method getUrlBarForTests()Lcom/google/android/apps/chrome/webapps/WebappUrlBar;
    .locals 1

    .prologue
    .line 384
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/WebappActivity;->mUrlBar:Lcom/google/android/apps/chrome/webapps/WebappUrlBar;

    return-object v0
.end method

.method getWebappInfo()Lcom/google/android/apps/chrome/webapps/WebappInfo;
    .locals 1

    .prologue
    .line 219
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/WebappActivity;->mWebappInfo:Lcom/google/android/apps/chrome/webapps/WebappInfo;

    return-object v0
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 70
    if-nez p1, :cond_1

    .line 83
    :cond_0
    :goto_0
    return-void

    .line 71
    :cond_1
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/webapps/FullScreenActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 72
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/webapps/WebappActivity;->setIntent(Landroid/content/Intent;)V

    .line 74
    invoke-static {p1}, Lcom/google/android/apps/chrome/webapps/WebappInfo;->create(Landroid/content/Intent;)Lcom/google/android/apps/chrome/webapps/WebappInfo;

    move-result-object v0

    .line 75
    if-nez v0, :cond_2

    .line 76
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/webapps/WebappActivity;->getLoggingTag()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Failed to parse new Intent: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 77
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/webapps/WebappActivity;->finish()V

    goto :goto_0

    .line 78
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/chrome/webapps/WebappActivity;->mWebappInfo:Lcom/google/android/apps/chrome/webapps/WebappInfo;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/webapps/WebappInfo;->id()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/webapps/WebappInfo;->id()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 79
    iget-object v1, p0, Lcom/google/android/apps/chrome/webapps/WebappActivity;->mWebappInfo:Lcom/google/android/apps/chrome/webapps/WebappInfo;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/webapps/WebappInfo;->copy(Lcom/google/android/apps/chrome/webapps/WebappInfo;)V

    .line 80
    iput-object v3, p0, Lcom/google/android/apps/chrome/webapps/WebappActivity;->mSavedInstanceState:Landroid/os/Bundle;

    .line 81
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/webapps/WebappActivity;->mIsInitialized:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0, v3}, Lcom/google/android/apps/chrome/webapps/WebappActivity;->showNewTab(Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 163
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/webapps/WebappActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/WebappActivity;->mDelegate:Lcom/google/android/apps/chrome/utilities/DocumentApiDelegate;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/webapps/WebappActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 165
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/WebappActivity;->mDelegate:Lcom/google/android/apps/chrome/utilities/DocumentApiDelegate;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/webapps/WebappActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/webapps/WebappActivity;->getTaskId()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/utilities/DocumentApiDelegate;->finishOtherTasksWithData(Landroid/net/Uri;I)Landroid/content/Intent;

    .line 167
    :cond_0
    invoke-super {p0}, Lcom/google/android/apps/chrome/webapps/FullScreenActivity;->onResume()V

    .line 168
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 142
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/webapps/FullScreenActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 143
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/WebappActivity;->mWebappInfo:Lcom/google/android/apps/chrome/webapps/WebappInfo;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/webapps/WebappInfo;->writeToBundle(Landroid/os/Bundle;)V

    .line 144
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/webapps/WebappActivity;->getTab()Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/webapps/WebappActivity;->getTab()Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab;->saveInstanceState(Landroid/os/Bundle;)V

    .line 145
    :cond_0
    return-void
.end method

.method public onStartWithNative()V
    .locals 2

    .prologue
    .line 149
    invoke-super {p0}, Lcom/google/android/apps/chrome/webapps/FullScreenActivity;->onStartWithNative()V

    .line 150
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/WebappActivity;->mCleanupTask:Landroid/os/AsyncTask;

    invoke-virtual {v0}, Landroid/os/AsyncTask;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->PENDING:Landroid/os/AsyncTask$Status;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/WebappActivity;->mCleanupTask:Landroid/os/AsyncTask;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 151
    :cond_0
    return-void
.end method

.method public onStopWithNative()V
    .locals 2

    .prologue
    .line 155
    invoke-super {p0}, Lcom/google/android/apps/chrome/webapps/FullScreenActivity;->onStopWithNative()V

    .line 156
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/WebappActivity;->mCleanupTask:Landroid/os/AsyncTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->cancel(Z)Z

    .line 157
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/webapps/WebappActivity;->getTab()Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/webapps/WebappActivity;->getTab()Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/webapps/WebappActivity;->getActivityDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab;->saveState(Ljava/io/File;)V

    .line 158
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/WebappActivity;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/WebappActivity;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->setPersistentFullscreenMode(Z)V

    .line 159
    :cond_1
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 1

    .prologue
    .line 172
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/webapps/FullScreenActivity;->onWindowFocusChanged(Z)V

    .line 173
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/WebappActivity;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/WebappActivity;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->onWindowFocusChanged(Z)V

    .line 174
    :cond_0
    return-void
.end method

.method public preInflationStartup()V
    .locals 2

    .prologue
    .line 125
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/webapps/WebappActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/webapps/WebappInfo;->create(Landroid/content/Intent;)Lcom/google/android/apps/chrome/webapps/WebappInfo;

    move-result-object v0

    .line 126
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/webapps/WebappActivity;->mWebappInfo:Lcom/google/android/apps/chrome/webapps/WebappInfo;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/webapps/WebappInfo;->copy(Lcom/google/android/apps/chrome/webapps/WebappInfo;)V

    .line 127
    :cond_0
    invoke-super {p0}, Lcom/google/android/apps/chrome/webapps/FullScreenActivity;->preInflationStartup()V

    .line 129
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/WebappActivity;->mWebappInfo:Lcom/google/android/apps/chrome/webapps/WebappInfo;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/webapps/WebappInfo;->orientation()I

    move-result v0

    int-to-byte v0, v0

    invoke-static {v0, p0}, Lorg/chromium/content/browser/ScreenOrientationProvider;->lockOrientation(BLandroid/app/Activity;)V

    .line 130
    return-void
.end method

.method protected showNewTab(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 87
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/webapps/FullScreenActivity;->showNewTab(Landroid/os/Bundle;)V

    .line 88
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/WebappActivity;->mSlidingLayout:Lcom/google/android/apps/chrome/webapps/SlidingLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/webapps/SlidingLayout;->setUrlBarVisibility(Z)Z

    .line 90
    if-nez p1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/WebappActivity;->mWebappInfo:Lcom/google/android/apps/chrome/webapps/WebappInfo;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/webapps/WebappInfo;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 91
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/webapps/WebappActivity;->getTab()Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab;->getUrl()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/WebappActivity;->mWebappInfo:Lcom/google/android/apps/chrome/webapps/WebappInfo;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/webapps/WebappInfo;->uri()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/webapps/WebappActivity;->loadUrl(Ljava/lang/String;)V

    .line 96
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/webapps/WebappActivity;->createWebContentsObserver()Lorg/chromium/content/browser/WebContentsObserverAndroid;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/webapps/WebappActivity;->mObserver:Lorg/chromium/content/browser/WebContentsObserverAndroid;

    .line 97
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/webapps/WebappActivity;->getTab()Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/webapps/WebappActivity;->createTabObserver()Lorg/chromium/chrome/browser/TabObserver;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab;->addObserver(Lorg/chromium/chrome/browser/TabObserver;)V

    .line 98
    invoke-direct {p0}, Lcom/google/android/apps/chrome/webapps/WebappActivity;->initializeFullscreenManager()V

    .line 99
    invoke-direct {p0}, Lcom/google/android/apps/chrome/webapps/WebappActivity;->updateTaskDescription()V

    .line 100
    return-void

    .line 93
    :cond_1
    invoke-static {}, Lorg/chromium/net/NetworkChangeNotifier;->isOnline()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/webapps/WebappActivity;->getTab()Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab;->reloadIgnoringCache()V

    goto :goto_0
.end method

.method protected updateTabViewOnLayout(Landroid/view/View;Z)V
    .locals 1

    .prologue
    .line 374
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/WebappActivity;->mPageLayout:Landroid/widget/FrameLayout;

    check-cast v0, Lcom/google/android/apps/chrome/webapps/FullscreenContentViewContainer;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/chrome/webapps/FullscreenContentViewContainer;->updateTabView(Landroid/view/View;Z)V

    .line 375
    return-void
.end method

.method updateUrlBar(Ljava/lang/String;I)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 230
    iget-object v1, p0, Lcom/google/android/apps/chrome/webapps/WebappActivity;->mUrlBar:Lcom/google/android/apps/chrome/webapps/WebappUrlBar;

    if-nez v1, :cond_0

    .line 232
    :goto_0
    return v0

    .line 231
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/webapps/WebappActivity;->mUrlBar:Lcom/google/android/apps/chrome/webapps/WebappUrlBar;

    invoke-virtual {v1, p1, p2}, Lcom/google/android/apps/chrome/webapps/WebappUrlBar;->update(Ljava/lang/String;I)Z

    move-result v1

    .line 232
    iget-object v2, p0, Lcom/google/android/apps/chrome/webapps/WebappActivity;->mSlidingLayout:Lcom/google/android/apps/chrome/webapps/SlidingLayout;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/chrome/webapps/WebappActivity;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->getPersistentFullscreenMode()Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v0, 0x1

    :cond_1
    invoke-virtual {v2, v0}, Lcom/google/android/apps/chrome/webapps/SlidingLayout;->setUrlBarVisibility(Z)Z

    move-result v0

    goto :goto_0
.end method
