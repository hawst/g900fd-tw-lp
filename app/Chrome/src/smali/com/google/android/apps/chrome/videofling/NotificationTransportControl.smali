.class public Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;
.super Lcom/google/android/apps/chrome/videofling/TransportControl;
.source "NotificationTransportControl.java"

# interfaces
.implements Lcom/google/android/apps/chrome/videofling/MediaRouteController$Listener;


# static fields
.field private static final LOCK:Ljava/lang/Object;

.field private static sInstance:Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mHandler:Landroid/os/Handler;

.field private mIcon:Landroid/graphics/Bitmap;

.field private mMediaRouteController:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

.field private mNotification:Landroid/app/Notification;

.field private final mPauseDescription:Ljava/lang/String;

.field private final mPlayDescription:Ljava/lang/String;

.field private mProgressUpdateInterval:I

.field private mService:Lcom/google/android/apps/chrome/videofling/NotificationTransportControl$ListenerService;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 167
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->sInstance:Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;

    .line 168
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->LOCK:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 267
    invoke-direct {p0}, Lcom/google/android/apps/chrome/videofling/TransportControl;-><init>()V

    .line 265
    const/16 v0, 0x3e8

    iput v0, p0, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->mProgressUpdateInterval:I

    .line 268
    iput-object p1, p0, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->mContext:Landroid/content/Context;

    .line 269
    new-instance v0, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl$1;

    invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl$1;-><init>(Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->mHandler:Landroid/os/Handler;

    .line 279
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$string;->accessibility_play:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->mPlayDescription:Ljava/lang/String;

    .line 280
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$string;->accessibility_pause:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->mPauseDescription:Ljava/lang/String;

    .line 281
    return-void
.end method

.method static synthetic access$000()Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;
    .locals 1

    .prologue
    .line 43
    sget-object v0, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->sInstance:Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/videofling/NotificationTransportControl$ListenerService;)V
    .locals 0

    .prologue
    .line 43
    invoke-static {p0}, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->onServiceStarted(Lcom/google/android/apps/chrome/videofling/NotificationTransportControl$ListenerService;)V

    return-void
.end method

.method static synthetic access$200()V
    .locals 0

    .prologue
    .line 43
    invoke-static {}, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->onServiceDestroyed()V

    return-void
.end method

.method static synthetic access$300(Z)V
    .locals 0

    .prologue
    .line 43
    invoke-static {p0}, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->checkState(Z)V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;)V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->updateNotificationInternal()V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;)Lcom/google/android/apps/chrome/videofling/MediaRouteController;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->mMediaRouteController:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;)I
    .locals 1

    .prologue
    .line 43
    iget v0, p0, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->mProgressUpdateInterval:I

    return v0
.end method

.method private buildNotification(Landroid/app/Notification$Builder;)V
    .locals 2

    .prologue
    .line 478
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    .line 479
    invoke-virtual {p1}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->mNotification:Landroid/app/Notification;

    .line 483
    :goto_0
    return-void

    .line 481
    :cond_0
    invoke-virtual {p1}, Landroid/app/Notification$Builder;->getNotification()Landroid/app/Notification;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->mNotification:Landroid/app/Notification;

    goto :goto_0
.end method

.method private static checkState(Z)V
    .locals 1

    .prologue
    .line 208
    if-nez p0, :cond_0

    .line 209
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 211
    :cond_0
    return-void
.end method

.method private createContentView()Landroid/widget/RemoteViews;
    .locals 4

    .prologue
    .line 450
    new-instance v0, Landroid/widget/RemoteViews;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    sget v2, Lcom/google/android/apps/chrome/R$layout;->remote_notification_bar:I

    invoke-direct {v0, v1, v2}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 452
    sget v1, Lcom/google/android/apps/chrome/R$id;->stop:I

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->getService()Lcom/google/android/apps/chrome/videofling/NotificationTransportControl$ListenerService;

    move-result-object v2

    const/4 v3, 0x5

    invoke-virtual {v2, v3}, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl$ListenerService;->getPendingIntent(I)Landroid/app/PendingIntent;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 455
    return-object v0
.end method

.method private createNotification()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 459
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->mNotification:Landroid/app/Notification;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->checkState(Z)V

    .line 461
    new-instance v0, Landroid/app/Notification$Builder;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setDefaults(I)Landroid/app/Notification$Builder;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$drawable;->ic_notification_media_route:I

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->createContentView()Landroid/widget/RemoteViews;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setContent(Landroid/widget/RemoteViews;)Landroid/app/Notification$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->getService()Lcom/google/android/apps/chrome/videofling/NotificationTransportControl$ListenerService;

    move-result-object v1

    const/4 v2, 0x6

    invoke-virtual {v1, v2}, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl$ListenerService;->getPendingIntent(I)Landroid/app/PendingIntent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->getService()Lcom/google/android/apps/chrome/videofling/NotificationTransportControl$ListenerService;

    move-result-object v1

    const/4 v2, 0x5

    invoke-virtual {v1, v2}, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl$ListenerService;->getPendingIntent(I)Landroid/app/PendingIntent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setDeleteIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v0

    .line 468
    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->setNotificationVisibility(Landroid/app/Notification$Builder;)V

    .line 469
    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->buildNotification(Landroid/app/Notification$Builder;)V

    .line 470
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->mNotification:Landroid/app/Notification;

    iget v1, v0, Landroid/app/Notification;->flags:I

    or-int/lit8 v1, v1, 0xa

    iput v1, v0, Landroid/app/Notification;->flags:I

    .line 471
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->mNotification:Landroid/app/Notification;

    iget v1, v0, Landroid/app/Notification;->flags:I

    iget-object v2, p0, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->mNotification:Landroid/app/Notification;

    iget v2, v2, Landroid/app/Notification;->flags:I

    invoke-static {v2}, Lcom/google/android/apps/chrome/utilities/UnreleasedApiCompatibilityUtils;->setLocalOnly(I)I

    move-result v2

    or-int/2addr v1, v2

    iput v1, v0, Landroid/app/Notification;->flags:I

    .line 472
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->updateNotification()V

    .line 473
    return-void

    :cond_0
    move v0, v1

    .line 459
    goto :goto_0
.end method

.method private destroyNotification()V
    .locals 2

    .prologue
    .line 505
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->mNotification:Landroid/app/Notification;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->checkState(Z)V

    .line 508
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 510
    invoke-direct {p0}, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v1, "notification"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 512
    sget v1, Lcom/google/android/apps/chrome/R$id;->remote_notification:I

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 513
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->mNotification:Landroid/app/Notification;

    .line 514
    return-void

    .line 505
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private final getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 517
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static getIfExists()Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;
    .locals 1

    .prologue
    .line 197
    sget-object v0, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->sInstance:Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;

    return-object v0
.end method

.method public static getOrCreate(Landroid/content/Context;Lcom/google/android/apps/chrome/videofling/MediaRouteController;)Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;
    .locals 8

    .prologue
    .line 183
    sget-object v6, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->LOCK:Ljava/lang/Object;

    monitor-enter v6

    .line 184
    :try_start_0
    sget-object v0, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->sInstance:Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;

    if-nez v0, :cond_0

    .line 185
    new-instance v7, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;

    invoke-direct {v7, p0}, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;-><init>(Landroid/content/Context;)V

    .line 186
    sput-object v7, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->sInstance:Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;

    new-instance v0, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;

    const/4 v1, 0x0

    const/4 v2, 0x0

    sget-object v3, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;->STOPPED:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;-><init>(Ljava/lang/String;ILcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;ILjava/lang/String;)V

    invoke-virtual {v7, v0}, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->setVideoInfo(Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;)V

    .line 190
    :cond_0
    sget-object v0, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->sInstance:Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;

    invoke-direct {v0, p1}, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->setMediaRouteController(Lcom/google/android/apps/chrome/videofling/MediaRouteController;)V

    .line 191
    sget-object v0, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->sInstance:Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 192
    :catchall_0
    move-exception v0

    monitor-exit v6

    throw v0
.end method

.method private getStatus()Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 521
    invoke-direct {p0}, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 522
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->getVideoInfo()Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;

    move-result-object v2

    .line 523
    if-eqz v2, :cond_0

    iget-object v0, v2, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;->title:Ljava/lang/String;

    .line 524
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->hasError()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 525
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->getError()Ljava/lang/String;

    move-result-object v0

    .line 552
    :goto_1
    return-object v0

    .line 523
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 526
    :cond_1
    if-eqz v2, :cond_6

    .line 527
    sget-object v3, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl$3;->$SwitchMap$com$google$android$apps$chrome$videofling$RemoteVideoInfo$PlayerState:[I

    iget-object v4, v2, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;->state:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 549
    iget-object v0, v2, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;->errorMessage:Ljava/lang/String;

    goto :goto_1

    .line 529
    :pswitch_0
    if-eqz v0, :cond_2

    sget v2, Lcom/google/android/apps/chrome/R$string;->athome_notification_playing_for_video:I

    new-array v3, v6, [Ljava/lang/Object;

    aput-object v0, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_2
    sget v0, Lcom/google/android/apps/chrome/R$string;->athome_notification_playing:I

    invoke-virtual {v1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 533
    :pswitch_1
    if-eqz v0, :cond_3

    sget v2, Lcom/google/android/apps/chrome/R$string;->athome_notification_loading_for_video:I

    new-array v3, v6, [Ljava/lang/Object;

    aput-object v0, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_3
    sget v0, Lcom/google/android/apps/chrome/R$string;->athome_notification_loading:I

    invoke-virtual {v1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 537
    :pswitch_2
    if-eqz v0, :cond_4

    sget v2, Lcom/google/android/apps/chrome/R$string;->athome_notification_paused_for_video:I

    new-array v3, v6, [Ljava/lang/Object;

    aput-object v0, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_4
    sget v0, Lcom/google/android/apps/chrome/R$string;->athome_notification_paused:I

    invoke-virtual {v1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 541
    :pswitch_3
    sget v0, Lcom/google/android/apps/chrome/R$string;->athome_notification_stopped:I

    invoke-virtual {v1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 544
    :pswitch_4
    if-eqz v0, :cond_5

    sget v2, Lcom/google/android/apps/chrome/R$string;->athome_notification_finished_for_video:I

    new-array v3, v6, [Ljava/lang/Object;

    aput-object v0, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_5
    sget v0, Lcom/google/android/apps/chrome/R$string;->athome_notification_finished:I

    invoke-virtual {v1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 552
    :cond_6
    const-string/jumbo v0, ""

    goto :goto_1

    .line 527
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_4
    .end packed-switch
.end method

.method private getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 557
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->getScreenName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private monitorProgress()V
    .locals 4

    .prologue
    .line 561
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl$2;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl$2;-><init>(Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;)V

    iget v2, p0, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->mProgressUpdateInterval:I

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 571
    return-void
.end method

.method private static onServiceDestroyed()V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 214
    sget-object v0, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->sInstance:Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->checkState(Z)V

    .line 215
    sget-object v0, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->sInstance:Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;

    iget-object v0, v0, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->mService:Lcom/google/android/apps/chrome/videofling/NotificationTransportControl$ListenerService;

    if-eqz v0, :cond_1

    :goto_1
    invoke-static {v1}, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->checkState(Z)V

    .line 216
    sget-object v0, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->sInstance:Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->destroyNotification()V

    .line 217
    sget-object v0, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->sInstance:Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->mService:Lcom/google/android/apps/chrome/videofling/NotificationTransportControl$ListenerService;

    .line 218
    return-void

    :cond_0
    move v0, v2

    .line 214
    goto :goto_0

    :cond_1
    move v1, v2

    .line 215
    goto :goto_1
.end method

.method private static onServiceStarted(Lcom/google/android/apps/chrome/videofling/NotificationTransportControl$ListenerService;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 221
    sget-object v0, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->sInstance:Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->checkState(Z)V

    .line 222
    sget-object v0, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->sInstance:Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;

    iget-object v0, v0, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->mService:Lcom/google/android/apps/chrome/videofling/NotificationTransportControl$ListenerService;

    if-nez v0, :cond_1

    :goto_1
    invoke-static {v1}, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->checkState(Z)V

    .line 223
    sget-object v0, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->sInstance:Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;

    iput-object p0, v0, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->mService:Lcom/google/android/apps/chrome/videofling/NotificationTransportControl$ListenerService;

    .line 224
    sget-object v0, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->sInstance:Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->createNotification()V

    .line 225
    return-void

    :cond_0
    move v0, v2

    .line 221
    goto :goto_0

    :cond_1
    move v1, v2

    .line 222
    goto :goto_1
.end method

.method private static scaleBitmap(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;
    .locals 3

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 231
    if-nez p0, :cond_0

    .line 232
    const/4 v0, 0x0

    .line 246
    :goto_0
    return-object v0

    .line 237
    :cond_0
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    if-le v0, p1, :cond_2

    .line 238
    int-to-float v0, p1

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v0, v2

    .line 240
    :goto_1
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    if-le v2, p2, :cond_1

    .line 241
    int-to-float v1, p2

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    .line 243
    :cond_1
    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 244
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, v0

    float-to-int v1, v1

    .line 245
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v0, v2

    float-to-int v0, v0

    .line 246
    const/4 v2, 0x0

    invoke-static {p0, v1, v0, v2}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method private scaleBitmapForIcon(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 3

    .prologue
    .line 578
    invoke-direct {p0}, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 579
    sget v1, Lcom/google/android/apps/chrome/R$dimen;->remote_notification_logo_max_width:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    .line 580
    sget v2, Lcom/google/android/apps/chrome/R$dimen;->remote_notification_logo_max_height:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    .line 581
    float-to-int v1, v1

    float-to-int v0, v0

    invoke-static {p1, v1, v0}, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->scaleBitmap(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method private setMediaRouteController(Lcom/google/android/apps/chrome/videofling/MediaRouteController;)V
    .locals 1

    .prologue
    .line 591
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->mMediaRouteController:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    if-ne v0, p1, :cond_1

    .line 599
    :cond_0
    :goto_0
    return-void

    .line 593
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->mMediaRouteController:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    if-eqz v0, :cond_2

    .line 594
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->mMediaRouteController:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->removeListener(Lcom/google/android/apps/chrome/videofling/MediaRouteController$Listener;)V

    .line 597
    :cond_2
    iput-object p1, p0, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->mMediaRouteController:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    .line 598
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->mMediaRouteController:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->addListener(Lcom/google/android/apps/chrome/videofling/MediaRouteController$Listener;)V

    goto :goto_0
.end method

.method private setNotificationVisibility(Landroid/app/Notification$Builder;)V
    .locals 6

    .prologue
    .line 488
    :try_start_0
    const-class v0, Landroid/app/Notification;

    const-string/jumbo v1, "VISIBILITY_PUBLIC"

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v0

    .line 490
    const-class v1, Landroid/app/Notification$Builder;

    const-string/jumbo v2, "setVisibility"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Class;

    const/4 v4, 0x0

    sget-object v5, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 492
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-virtual {v1, p1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_3

    .line 502
    :goto_0
    return-void

    .line 493
    :catch_0
    move-exception v0

    .line 494
    const-string/jumbo v1, "NotificationTransportControl"

    const-string/jumbo v2, "Cannot find setVisibility() method: "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 495
    :catch_1
    move-exception v0

    .line 496
    const-string/jumbo v1, "NotificationTransportControl"

    const-string/jumbo v2, "Cannot invoke Notification.Builder.setVisibility() method: "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 497
    :catch_2
    move-exception v0

    .line 498
    const-string/jumbo v1, "NotificationTransportControl"

    const-string/jumbo v2, "Cannot access field or method in Notification: "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 499
    :catch_3
    move-exception v0

    .line 500
    const-string/jumbo v1, "NotificationTransportControl"

    const-string/jumbo v2, "Cannot find VISIBILITY_PUBLIC field in Notification class: "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private updateNotificationInternal()V
    .locals 8

    .prologue
    const/4 v4, 0x4

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 602
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->mNotification:Landroid/app/Notification;

    if-eqz v0, :cond_2

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->checkState(Z)V

    .line 604
    invoke-direct {p0}, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->createContentView()Landroid/widget/RemoteViews;

    move-result-object v5

    .line 606
    sget v0, Lcom/google/android/apps/chrome/R$id;->title:I

    invoke-direct {p0}, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v0, v3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 607
    sget v0, Lcom/google/android/apps/chrome/R$id;->status:I

    invoke-direct {p0}, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->getStatus()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v0, v3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 608
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->mIcon:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_3

    .line 609
    sget v0, Lcom/google/android/apps/chrome/R$id;->icon:I

    iget-object v3, p0, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->mIcon:Landroid/graphics/Bitmap;

    invoke-virtual {v5, v0, v3}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    .line 614
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->getVideoInfo()Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;

    move-result-object v6

    .line 615
    if-eqz v6, :cond_1

    .line 618
    sget-object v0, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl$3;->$SwitchMap$com$google$android$apps$chrome$videofling$RemoteVideoInfo$PlayerState:[I

    iget-object v3, v6, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;->state:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;->ordinal()I

    move-result v3

    aget v0, v0, v3

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    move v1, v2

    move v0, v2

    .line 648
    :goto_2
    sget v7, Lcom/google/android/apps/chrome/R$id;->playpause:I

    if-eqz v0, :cond_4

    move v3, v2

    :goto_3
    invoke-virtual {v5, v7, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 658
    sget v3, Lcom/google/android/apps/chrome/R$id;->progress:I

    if-eqz v1, :cond_5

    iget v1, v6, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;->durationMillis:I

    if-lez v1, :cond_5

    move v1, v2

    :goto_4
    invoke-virtual {v5, v3, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 660
    sget v1, Lcom/google/android/apps/chrome/R$id;->stop:I

    if-eqz v0, :cond_6

    :goto_5
    invoke-virtual {v5, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 662
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->mNotification:Landroid/app/Notification;

    iput-object v5, v0, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    .line 664
    invoke-direct {p0}, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v1, "notification"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 666
    sget v1, Lcom/google/android/apps/chrome/R$id;->remote_notification:I

    iget-object v2, p0, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->mNotification:Landroid/app/Notification;

    invoke-virtual {v0, v1, v2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 668
    iget-object v0, v6, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;->state:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    sget-object v1, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;->STOPPED:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    if-eq v0, v1, :cond_0

    iget-object v0, v6, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;->state:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    sget-object v1, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;->FINISHED:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    if-ne v0, v1, :cond_7

    .line 669
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->getService()Lcom/google/android/apps/chrome/videofling/NotificationTransportControl$ListenerService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl$ListenerService;->stopSelf()V

    .line 674
    :cond_1
    :goto_6
    return-void

    :cond_2
    move v0, v2

    .line 602
    goto/16 :goto_0

    .line 611
    :cond_3
    sget v0, Lcom/google/android/apps/chrome/R$id;->icon:I

    sget v3, Lcom/google/android/apps/chrome/R$drawable;->ic_notification_media_route:I

    invoke-virtual {v5, v0, v3}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    goto :goto_1

    .line 622
    :pswitch_1
    sget v0, Lcom/google/android/apps/chrome/R$id;->progress:I

    iget v3, v6, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;->durationMillis:I

    iget v7, v6, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;->currentTimeMillis:I

    invoke-virtual {v5, v0, v3, v7, v2}, Landroid/widget/RemoteViews;->setProgressBar(IIIZ)V

    .line 624
    sget v0, Lcom/google/android/apps/chrome/R$id;->playpause:I

    sget v3, Lcom/google/android/apps/chrome/R$drawable;->ic_vidcontrol_pause:I

    invoke-virtual {v5, v0, v3}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 626
    sget v0, Lcom/google/android/apps/chrome/R$id;->playpause:I

    iget-object v3, p0, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->mPauseDescription:Ljava/lang/String;

    invoke-static {v5, v0, v3}, Lorg/chromium/base/ApiCompatibilityUtils;->setContentDescriptionForRemoteView(Landroid/widget/RemoteViews;ILjava/lang/CharSequence;)V

    .line 628
    sget v0, Lcom/google/android/apps/chrome/R$id;->playpause:I

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->getService()Lcom/google/android/apps/chrome/videofling/NotificationTransportControl$ListenerService;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl$ListenerService;->getPendingIntent(I)Landroid/app/PendingIntent;

    move-result-object v3

    invoke-virtual {v5, v0, v3}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    move v0, v1

    .line 630
    goto :goto_2

    .line 634
    :pswitch_2
    sget v0, Lcom/google/android/apps/chrome/R$id;->progress:I

    iget v3, v6, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;->durationMillis:I

    iget v7, v6, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;->currentTimeMillis:I

    invoke-virtual {v5, v0, v3, v7, v2}, Landroid/widget/RemoteViews;->setProgressBar(IIIZ)V

    .line 636
    sget v0, Lcom/google/android/apps/chrome/R$id;->playpause:I

    sget v3, Lcom/google/android/apps/chrome/R$drawable;->ic_vidcontrol_play:I

    invoke-virtual {v5, v0, v3}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 637
    sget v0, Lcom/google/android/apps/chrome/R$id;->playpause:I

    iget-object v3, p0, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->mPlayDescription:Ljava/lang/String;

    invoke-static {v5, v0, v3}, Lorg/chromium/base/ApiCompatibilityUtils;->setContentDescriptionForRemoteView(Landroid/widget/RemoteViews;ILjava/lang/CharSequence;)V

    .line 639
    sget v0, Lcom/google/android/apps/chrome/R$id;->playpause:I

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->getService()Lcom/google/android/apps/chrome/videofling/NotificationTransportControl$ListenerService;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl$ListenerService;->getPendingIntent(I)Landroid/app/PendingIntent;

    move-result-object v3

    invoke-virtual {v5, v0, v3}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    move v0, v1

    .line 641
    goto/16 :goto_2

    .line 644
    :pswitch_3
    sget v0, Lcom/google/android/apps/chrome/R$id;->progress:I

    invoke-virtual {v5, v0, v2, v2, v1}, Landroid/widget/RemoteViews;->setProgressBar(IIIZ)V

    move v0, v2

    .line 645
    goto/16 :goto_2

    :pswitch_4
    move v0, v2

    .line 647
    goto/16 :goto_2

    :cond_4
    move v3, v4

    .line 648
    goto/16 :goto_3

    .line 658
    :cond_5
    const/16 v1, 0x8

    goto/16 :goto_4

    :cond_6
    move v2, v4

    .line 660
    goto/16 :goto_5

    .line 671
    :cond_7
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->getService()Lcom/google/android/apps/chrome/videofling/NotificationTransportControl$ListenerService;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$id;->remote_notification:I

    iget-object v2, p0, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->mNotification:Landroid/app/Notification;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl$ListenerService;->startForeground(ILandroid/app/Notification;)V

    goto :goto_6

    .line 618
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method


# virtual methods
.method getNotification()Landroid/app/Notification;
    .locals 1

    .prologue
    .line 413
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->mNotification:Landroid/app/Notification;

    return-object v0
.end method

.method final getService()Lcom/google/android/apps/chrome/videofling/NotificationTransportControl$ListenerService;
    .locals 1

    .prologue
    .line 418
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->mService:Lcom/google/android/apps/chrome/videofling/NotificationTransportControl$ListenerService;

    return-object v0
.end method

.method public hide()V
    .locals 4

    .prologue
    .line 288
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->mContext:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->mContext:Landroid/content/Context;

    const-class v3, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl$ListenerService;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    .line 289
    return-void
.end method

.method public isShowing()Z
    .locals 1

    .prologue
    .line 295
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->mService:Lcom/google/android/apps/chrome/videofling/NotificationTransportControl$ListenerService;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onDurationUpdated(I)V
    .locals 3

    .prologue
    .line 300
    new-instance v0, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->getVideoInfo()Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;-><init>(Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;)V

    .line 301
    iput p1, v0, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;->durationMillis:I

    .line 302
    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->setVideoInfo(Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;)V

    .line 308
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 309
    iget v1, v0, Landroid/util/DisplayMetrics;->density:F

    .line 310
    iget v2, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-float v2, v2

    div-float/2addr v2, v1

    .line 311
    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v0, v0

    div-float/2addr v0, v1

    .line 313
    invoke-static {v2, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 315
    const/16 v1, 0x3e8

    int-to-float v2, p1

    div-float v0, v2, v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->mProgressUpdateInterval:I

    .line 317
    return-void
.end method

.method public onError(ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 322
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->hide()V

    .line 323
    return-void
.end method

.method protected onErrorChanged()V
    .locals 1

    .prologue
    .line 423
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 424
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->updateNotification()V

    .line 426
    :cond_0
    return-void
.end method

.method public onPlaybackStateChanged(Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;Lorg/chromium/chrome/browser/Tab;I)V
    .locals 2

    .prologue
    .line 328
    new-instance v0, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->getVideoInfo()Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;-><init>(Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;)V

    .line 329
    iput-object p2, v0, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;->state:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    .line 330
    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->setVideoInfo(Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;)V

    .line 332
    if-ne p2, p1, :cond_1

    .line 346
    :cond_0
    :goto_0
    return-void

    .line 334
    :cond_1
    sget-object v0, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;->PLAYING:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    if-eq p2, v0, :cond_2

    sget-object v0, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;->LOADING:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    if-eq p2, v0, :cond_2

    sget-object v0, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;->PAUSED:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    if-ne p2, v0, :cond_3

    .line 336
    :cond_2
    invoke-virtual {p0, p2}, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->show(Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;)V

    .line 337
    sget-object v0, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;->PLAYING:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    if-ne p2, v0, :cond_0

    .line 339
    invoke-direct {p0}, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->monitorProgress()V

    goto :goto_0

    .line 341
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;->FINISHED:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    if-eq p2, v0, :cond_4

    sget-object v0, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;->INVALIDATED:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    if-ne p2, v0, :cond_0

    .line 344
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->hide()V

    goto :goto_0
.end method

.method public onPositionChanged(I)V
    .locals 2

    .prologue
    .line 350
    new-instance v0, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->getVideoInfo()Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;-><init>(Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;)V

    .line 351
    iput p1, v0, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;->currentTimeMillis:I

    .line 352
    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->setVideoInfo(Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;)V

    .line 353
    return-void
.end method

.method protected onPosterBitmapChanged()V
    .locals 1

    .prologue
    .line 430
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->getPosterBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 431
    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->scaleBitmapForIcon(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->mIcon:Landroid/graphics/Bitmap;

    .line 432
    invoke-super {p0}, Lcom/google/android/apps/chrome/videofling/TransportControl;->onPosterBitmapChanged()V

    .line 433
    return-void
.end method

.method public onPrepared(Lcom/google/android/apps/chrome/videofling/MediaRouteController;)V
    .locals 1

    .prologue
    .line 357
    sget-object v0, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;->PLAYING:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->show(Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;)V

    .line 358
    return-void
.end method

.method public onRouteSelected(Ljava/lang/String;Lorg/chromium/chrome/browser/Tab;ILcom/google/android/apps/chrome/videofling/MediaRouteController;)V
    .locals 0

    .prologue
    .line 363
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->setScreenName(Ljava/lang/String;)V

    .line 364
    return-void
.end method

.method public onRouteUnselected(Lorg/chromium/chrome/browser/Tab;ILcom/google/android/apps/chrome/videofling/MediaRouteController;)V
    .locals 0

    .prologue
    .line 369
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->hide()V

    .line 370
    return-void
.end method

.method protected onScreenNameChanged()V
    .locals 1

    .prologue
    .line 437
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 438
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->updateNotification()V

    .line 440
    :cond_0
    return-void
.end method

.method public onSeekCompleted()V
    .locals 0

    .prologue
    .line 373
    return-void
.end method

.method public onTitleChanged(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 377
    new-instance v0, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->getVideoInfo()Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;-><init>(Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;)V

    .line 378
    iput-object p1, v0, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;->title:Ljava/lang/String;

    .line 379
    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->setVideoInfo(Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;)V

    .line 380
    return-void
.end method

.method protected onVideoInfoChanged()V
    .locals 1

    .prologue
    .line 444
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 445
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->updateNotification()V

    .line 447
    :cond_0
    return-void
.end method

.method public setRouteController(Lcom/google/android/apps/chrome/videofling/MediaRouteController;)V
    .locals 0

    .prologue
    .line 384
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->setMediaRouteController(Lcom/google/android/apps/chrome/videofling/MediaRouteController;)V

    .line 385
    return-void
.end method

.method public show(Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;)V
    .locals 4

    .prologue
    .line 392
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->mMediaRouteController:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->addListener(Lcom/google/android/apps/chrome/videofling/MediaRouteController$Listener;)V

    .line 393
    new-instance v0, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;

    iget-object v1, p0, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->mVideoInfo:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;

    invoke-direct {v0, v1}, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;-><init>(Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;)V

    .line 394
    iput-object p1, v0, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;->state:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    .line 395
    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->setVideoInfo(Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;)V

    .line 396
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->mContext:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->mContext:Landroid/content/Context;

    const-class v3, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl$ListenerService;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 398
    sget-object v0, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;->PLAYING:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    if-ne p1, v0, :cond_0

    .line 399
    invoke-direct {p0}, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->monitorProgress()V

    .line 401
    :cond_0
    return-void
.end method

.method public updateNotification()V
    .locals 2

    .prologue
    .line 404
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->mNotification:Landroid/app/Notification;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->checkState(Z)V

    .line 408
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 409
    return-void

    .line 404
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
