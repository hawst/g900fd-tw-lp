.class Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer$3$1;
.super Ljava/lang/Object;
.source "SuggestionView.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic this$2:Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer$3;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer$3;)V
    .locals 0

    .prologue
    .line 653
    iput-object p1, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer$3$1;->this$2:Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer$3;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2

    .prologue
    .line 656
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->omniboxDeleteRequested()V

    .line 657
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer$3$1;->this$2:Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer$3;

    iget-object v0, v0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer$3;->this$1:Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;

    iget-object v0, v0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->this$0:Lcom/google/android/apps/chrome/omnibox/SuggestionView;

    # getter for: Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mSuggestionDelegate:Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter$OmniboxSuggestionDelegate;
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->access$1300(Lcom/google/android/apps/chrome/omnibox/SuggestionView;)Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter$OmniboxSuggestionDelegate;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer$3$1;->this$2:Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer$3;

    iget-object v1, v1, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer$3;->this$1:Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;

    iget-object v1, v1, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->this$0:Lcom/google/android/apps/chrome/omnibox/SuggestionView;

    # getter for: Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mPosition:I
    invoke-static {v1}, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->access$1200(Lcom/google/android/apps/chrome/omnibox/SuggestionView;)I

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter$OmniboxSuggestionDelegate;->onDeleteSuggestion(I)V

    .line 658
    return-void
.end method
