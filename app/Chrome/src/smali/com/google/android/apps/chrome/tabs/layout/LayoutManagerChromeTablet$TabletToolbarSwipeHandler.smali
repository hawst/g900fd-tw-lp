.class Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet$TabletToolbarSwipeHandler;
.super Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome$ToolbarSwipeHandler;
.source "LayoutManagerChromeTablet.java"


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet;


# virtual methods
.method public isSwipeEnabled(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;)Z
    .locals 2

    .prologue
    .line 234
    sget-object v0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;->LEFT:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    if-eq p1, v0, :cond_0

    sget-object v0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;->RIGHT:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    if-ne p1, v0, :cond_2

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet$TabletToolbarSwipeHandler;->this$0:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet;->getTabModelSelector()Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet$TabletToolbarSwipeHandler;->this$0:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet;->getTabModelSelector()Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    move-result-object v0

    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->getCurrentModel()Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v0

    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->getCount()I

    move-result v0

    const/4 v1, 0x1

    if-gt v0, v1, :cond_2

    .line 237
    :cond_1
    const/4 v0, 0x0

    .line 240
    :goto_0
    return v0

    :cond_2
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome$ToolbarSwipeHandler;->isSwipeEnabled(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;)Z

    move-result v0

    goto :goto_0
.end method
