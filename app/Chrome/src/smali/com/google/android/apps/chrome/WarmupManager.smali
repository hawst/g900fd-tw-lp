.class public Lcom/google/android/apps/chrome/WarmupManager;
.super Ljava/lang/Object;
.source "WarmupManager.java"


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static sWarmupManager:Lcom/google/android/apps/chrome/WarmupManager;


# instance fields
.field private mExternalPrerenderHandler:Lorg/chromium/chrome/browser/prerender/ExternalPrerenderHandler;

.field private mMainView:Landroid/view/ViewGroup;

.field private mPrerendered:Z

.field private mPrerenderedWebContents:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const-class v0, Lcom/google/android/apps/chrome/WarmupManager;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/WarmupManager;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    return-void
.end method

.method public static getInstance()Lcom/google/android/apps/chrome/WarmupManager;
    .locals 1

    .prologue
    .line 34
    sget-object v0, Lcom/google/android/apps/chrome/WarmupManager;->sWarmupManager:Lcom/google/android/apps/chrome/WarmupManager;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/chrome/WarmupManager;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/WarmupManager;-><init>()V

    sput-object v0, Lcom/google/android/apps/chrome/WarmupManager;->sWarmupManager:Lcom/google/android/apps/chrome/WarmupManager;

    .line 35
    :cond_0
    sget-object v0, Lcom/google/android/apps/chrome/WarmupManager;->sWarmupManager:Lcom/google/android/apps/chrome/WarmupManager;

    return-object v0
.end method

.method private takeMainView()Landroid/view/ViewGroup;
    .locals 2

    .prologue
    .line 147
    iget-object v0, p0, Lcom/google/android/apps/chrome/WarmupManager;->mMainView:Landroid/view/ViewGroup;

    .line 148
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/chrome/WarmupManager;->mMainView:Landroid/view/ViewGroup;

    .line 149
    return-object v0
.end method


# virtual methods
.method public cancelCurrentPrerender()V
    .locals 1

    .prologue
    .line 130
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/WarmupManager;->clearWebContentsIfNecessary()V

    .line 131
    iget-object v0, p0, Lcom/google/android/apps/chrome/WarmupManager;->mExternalPrerenderHandler:Lorg/chromium/chrome/browser/prerender/ExternalPrerenderHandler;

    if-nez v0, :cond_0

    .line 134
    :goto_0
    return-void

    .line 133
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/WarmupManager;->mExternalPrerenderHandler:Lorg/chromium/chrome/browser/prerender/ExternalPrerenderHandler;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/prerender/ExternalPrerenderHandler;->cancelCurrentPrerender()V

    goto :goto_0
.end method

.method public clearWebContentsIfNecessary()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 119
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/WarmupManager;->mPrerendered:Z

    .line 120
    iget-wide v0, p0, Lcom/google/android/apps/chrome/WarmupManager;->mPrerenderedWebContents:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 124
    :goto_0
    return-void

    .line 122
    :cond_0
    iget-wide v0, p0, Lcom/google/android/apps/chrome/WarmupManager;->mPrerenderedWebContents:J

    invoke-static {v0, v1}, Lorg/chromium/chrome/browser/ContentViewUtil;->destroyNativeWebContents(J)V

    .line 123
    iput-wide v2, p0, Lcom/google/android/apps/chrome/WarmupManager;->mPrerenderedWebContents:J

    goto :goto_0
.end method

.method public hasAnyPrerenderedUrl()Z
    .locals 1

    .prologue
    .line 56
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/WarmupManager;->mPrerendered:Z

    return v0
.end method

.method public hasBuiltViewHierarchy()Z
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lcom/google/android/apps/chrome/WarmupManager;->mMainView:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasPrerenderedUrl(Ljava/lang/String;)Z
    .locals 4

    .prologue
    .line 48
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/WarmupManager;->hasAnyPrerenderedUrl()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lorg/chromium/chrome/browser/profiles/Profile;->getLastUsedProfile()Lorg/chromium/chrome/browser/profiles/Profile;

    move-result-object v0

    iget-wide v2, p0, Lcom/google/android/apps/chrome/WarmupManager;->mPrerenderedWebContents:J

    invoke-static {v0, p1, v2, v3}, Lorg/chromium/chrome/browser/prerender/ExternalPrerenderHandler;->hasPrerenderedUrl(Lorg/chromium/chrome/browser/profiles/Profile;Ljava/lang/String;J)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public prerenderUrl(Ljava/lang/String;Ljava/lang/String;II)V
    .locals 6

    .prologue
    .line 79
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/WarmupManager;->clearWebContentsIfNecessary()V

    .line 80
    iget-object v0, p0, Lcom/google/android/apps/chrome/WarmupManager;->mExternalPrerenderHandler:Lorg/chromium/chrome/browser/prerender/ExternalPrerenderHandler;

    if-nez v0, :cond_0

    .line 81
    new-instance v0, Lorg/chromium/chrome/browser/prerender/ExternalPrerenderHandler;

    invoke-direct {v0}, Lorg/chromium/chrome/browser/prerender/ExternalPrerenderHandler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/WarmupManager;->mExternalPrerenderHandler:Lorg/chromium/chrome/browser/prerender/ExternalPrerenderHandler;

    .line 84
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/WarmupManager;->mExternalPrerenderHandler:Lorg/chromium/chrome/browser/prerender/ExternalPrerenderHandler;

    invoke-static {}, Lorg/chromium/chrome/browser/profiles/Profile;->getLastUsedProfile()Lorg/chromium/chrome/browser/profiles/Profile;

    move-result-object v1

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    invoke-virtual/range {v0 .. v5}, Lorg/chromium/chrome/browser/prerender/ExternalPrerenderHandler;->addPrerender(Lorg/chromium/chrome/browser/profiles/Profile;Ljava/lang/String;Ljava/lang/String;II)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/chrome/WarmupManager;->mPrerenderedWebContents:J

    .line 86
    iget-wide v0, p0, Lcom/google/android/apps/chrome/WarmupManager;->mPrerenderedWebContents:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 87
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/WarmupManager;->mPrerendered:Z

    .line 89
    :cond_1
    return-void
.end method

.method public takePrerenderedNativeWebContents()J
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 64
    iget-wide v0, p0, Lcom/google/android/apps/chrome/WarmupManager;->mPrerenderedWebContents:J

    .line 65
    sget-boolean v2, Lcom/google/android/apps/chrome/WarmupManager;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    iget-wide v2, p0, Lcom/google/android/apps/chrome/WarmupManager;->mPrerenderedWebContents:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 66
    :cond_0
    iput-wide v4, p0, Lcom/google/android/apps/chrome/WarmupManager;->mPrerenderedWebContents:J

    .line 67
    return-wide v0
.end method

.method public transferViewHierarchyTo(Landroid/view/ViewGroup;)V
    .locals 2

    .prologue
    .line 106
    invoke-direct {p0}, Lcom/google/android/apps/chrome/WarmupManager;->takeMainView()Landroid/view/ViewGroup;

    move-result-object v0

    .line 107
    if-nez v0, :cond_1

    .line 113
    :cond_0
    return-void

    .line 108
    :cond_1
    :goto_0
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    if-lez v1, :cond_0

    .line 109
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 110
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 111
    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_0
.end method
