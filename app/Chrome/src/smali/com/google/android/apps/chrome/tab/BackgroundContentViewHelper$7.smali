.class Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper$7;
.super Lorg/chromium/content/browser/WebContentsObserverAndroid;
.source "BackgroundContentViewHelper.java"


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;Lorg/chromium/content_public/browser/WebContents;)V
    .locals 0

    .prologue
    .line 808
    iput-object p1, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper$7;->this$0:Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;

    invoke-direct {p0, p2}, Lorg/chromium/content/browser/WebContentsObserverAndroid;-><init>(Lorg/chromium/content_public/browser/WebContents;)V

    return-void
.end method


# virtual methods
.method public didCommitProvisionalLoadForFrame(JZLjava/lang/String;I)V
    .locals 1

    .prologue
    .line 838
    if-eqz p3, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper$7;->this$0:Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->hasPendingBackgroundPage()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper$7;->this$0:Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;

    # invokes: Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->getOriginalUrlForPreviewUrl(Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v0, p4}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->access$1100(Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 844
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper$7;->this$0:Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;

    # invokes: Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->destroyContentViewCore()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->access$1200(Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;)V

    .line 846
    :cond_0
    return-void
.end method

.method public didFailLoad(ZZILjava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 812
    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper$7;->this$0:Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->hasPendingBackgroundPage()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 813
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper$7;->this$0:Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;

    # getter for: Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->access$1000(Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;)Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->getWebContents()Lorg/chromium/content_public/browser/WebContents;

    move-result-object v0

    invoke-interface {v0}, Lorg/chromium/content_public/browser/WebContents;->getUrl()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper$7;->this$0:Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;

    # invokes: Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->getOriginalUrlForPreviewUrl(Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v1, p5}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->access$1100(Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 815
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper$7;->this$0:Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->forceSwappingContentViews()V

    .line 816
    const/4 v0, 0x3

    const/4 v1, 0x6

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->recordInstantSearchClicksSwapReason(II)V

    .line 820
    :cond_0
    return-void
.end method

.method public didStartProvisionalLoadForFrame(JJZLjava/lang/String;ZZ)V
    .locals 1

    .prologue
    .line 826
    if-eqz p5, :cond_0

    .line 827
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper$7;->this$0:Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;

    # getter for: Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mTab:Lorg/chromium/chrome/browser/Tab;
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->access$700(Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;)Lorg/chromium/chrome/browser/Tab;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/Tab;->getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 828
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper$7;->this$0:Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;

    invoke-virtual {v0, p6}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->loadOriginalUrlIfPreview(Ljava/lang/String;)V

    .line 833
    :cond_0
    :goto_0
    return-void

    .line 830
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper$7;->this$0:Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;

    # invokes: Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->destroyContentViewCore()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->access$1200(Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;)V

    goto :goto_0
.end method
