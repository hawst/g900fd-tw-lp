.class Lcom/google/android/apps/chrome/ntp/BookmarksPage$1;
.super Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkModelObserver;
.source "BookmarksPage.java"


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/ntp/BookmarksPage;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/ntp/BookmarksPage;)V
    .locals 0

    .prologue
    .line 323
    iput-object p1, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPage$1;->this$0:Lcom/google/android/apps/chrome/ntp/BookmarksPage;

    invoke-direct {p0}, Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkModelObserver;-><init>()V

    return-void
.end method

.method private updateIfEitherNodeIsCurrentFolder(Lorg/chromium/components/bookmarks/BookmarkId;Lorg/chromium/components/bookmarks/BookmarkId;)V
    .locals 3

    .prologue
    .line 330
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPage$1;->this$0:Lcom/google/android/apps/chrome/ntp/BookmarksPage;

    # getter for: Lcom/google/android/apps/chrome/ntp/BookmarksPage;->mCurrentFolderId:Lorg/chromium/components/bookmarks/BookmarkId;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/BookmarksPage;->access$500(Lcom/google/android/apps/chrome/ntp/BookmarksPage;)Lorg/chromium/components/bookmarks/BookmarkId;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/chromium/components/bookmarks/BookmarkId;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPage$1;->this$0:Lcom/google/android/apps/chrome/ntp/BookmarksPage;

    # getter for: Lcom/google/android/apps/chrome/ntp/BookmarksPage;->mCurrentFolderId:Lorg/chromium/components/bookmarks/BookmarkId;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/BookmarksPage;->access$500(Lcom/google/android/apps/chrome/ntp/BookmarksPage;)Lorg/chromium/components/bookmarks/BookmarkId;

    move-result-object v0

    invoke-virtual {v0, p2}, Lorg/chromium/components/bookmarks/BookmarkId;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 332
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPage$1;->this$0:Lcom/google/android/apps/chrome/ntp/BookmarksPage;

    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPage$1;->this$0:Lcom/google/android/apps/chrome/ntp/BookmarksPage;

    # getter for: Lcom/google/android/apps/chrome/ntp/BookmarksPage;->mCurrentFolderId:Lorg/chromium/components/bookmarks/BookmarkId;
    invoke-static {v1}, Lcom/google/android/apps/chrome/ntp/BookmarksPage;->access$500(Lcom/google/android/apps/chrome/ntp/BookmarksPage;)Lorg/chromium/components/bookmarks/BookmarkId;

    move-result-object v1

    const/4 v2, 0x1

    # invokes: Lcom/google/android/apps/chrome/ntp/BookmarksPage;->updateBookmarksPageContents(Lorg/chromium/components/bookmarks/BookmarkId;Z)V
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/ntp/BookmarksPage;->access$400(Lcom/google/android/apps/chrome/ntp/BookmarksPage;Lorg/chromium/components/bookmarks/BookmarkId;Z)V

    .line 334
    :cond_1
    return-void
.end method


# virtual methods
.method public bookmarkModelChanged()V
    .locals 3

    .prologue
    .line 373
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPage$1;->this$0:Lcom/google/android/apps/chrome/ntp/BookmarksPage;

    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPage$1;->this$0:Lcom/google/android/apps/chrome/ntp/BookmarksPage;

    # getter for: Lcom/google/android/apps/chrome/ntp/BookmarksPage;->mCurrentFolderId:Lorg/chromium/components/bookmarks/BookmarkId;
    invoke-static {v1}, Lcom/google/android/apps/chrome/ntp/BookmarksPage;->access$500(Lcom/google/android/apps/chrome/ntp/BookmarksPage;)Lorg/chromium/components/bookmarks/BookmarkId;

    move-result-object v1

    const/4 v2, 0x1

    # invokes: Lcom/google/android/apps/chrome/ntp/BookmarksPage;->updateBookmarksPageContents(Lorg/chromium/components/bookmarks/BookmarkId;Z)V
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/ntp/BookmarksPage;->access$400(Lcom/google/android/apps/chrome/ntp/BookmarksPage;Lorg/chromium/components/bookmarks/BookmarkId;Z)V

    .line 374
    return-void
.end method

.method public bookmarkModelLoaded()V
    .locals 0

    .prologue
    .line 369
    return-void
.end method

.method public bookmarkNodeAdded(Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;I)V
    .locals 1

    .prologue
    .line 344
    invoke-virtual {p1}, Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;->getId()Lorg/chromium/components/bookmarks/BookmarkId;

    move-result-object v0

    invoke-direct {p0, v0, v0}, Lcom/google/android/apps/chrome/ntp/BookmarksPage$1;->updateIfEitherNodeIsCurrentFolder(Lorg/chromium/components/bookmarks/BookmarkId;Lorg/chromium/components/bookmarks/BookmarkId;)V

    .line 345
    return-void
.end method

.method public bookmarkNodeChanged(Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;)V
    .locals 2

    .prologue
    .line 358
    invoke-virtual {p1}, Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;->getId()Lorg/chromium/components/bookmarks/BookmarkId;

    move-result-object v0

    invoke-virtual {p1}, Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;->getParentId()Lorg/chromium/components/bookmarks/BookmarkId;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/ntp/BookmarksPage$1;->updateIfEitherNodeIsCurrentFolder(Lorg/chromium/components/bookmarks/BookmarkId;Lorg/chromium/components/bookmarks/BookmarkId;)V

    .line 359
    return-void
.end method

.method public bookmarkNodeChildrenReordered(Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;)V
    .locals 1

    .prologue
    .line 363
    invoke-virtual {p1}, Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;->getId()Lorg/chromium/components/bookmarks/BookmarkId;

    move-result-object v0

    invoke-direct {p0, v0, v0}, Lcom/google/android/apps/chrome/ntp/BookmarksPage$1;->updateIfEitherNodeIsCurrentFolder(Lorg/chromium/components/bookmarks/BookmarkId;Lorg/chromium/components/bookmarks/BookmarkId;)V

    .line 364
    return-void
.end method

.method public bookmarkNodeMoved(Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;ILorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;I)V
    .locals 2

    .prologue
    .line 339
    invoke-virtual {p1}, Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;->getId()Lorg/chromium/components/bookmarks/BookmarkId;

    move-result-object v0

    invoke-virtual {p3}, Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;->getId()Lorg/chromium/components/bookmarks/BookmarkId;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/ntp/BookmarksPage$1;->updateIfEitherNodeIsCurrentFolder(Lorg/chromium/components/bookmarks/BookmarkId;Lorg/chromium/components/bookmarks/BookmarkId;)V

    .line 340
    return-void
.end method

.method public bookmarkNodeRemoved(Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;ILorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;Z)V
    .locals 2

    .prologue
    .line 353
    invoke-virtual {p3}, Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;->getId()Lorg/chromium/components/bookmarks/BookmarkId;

    move-result-object v0

    invoke-virtual {p1}, Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;->getId()Lorg/chromium/components/bookmarks/BookmarkId;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/ntp/BookmarksPage$1;->updateIfEitherNodeIsCurrentFolder(Lorg/chromium/components/bookmarks/BookmarkId;Lorg/chromium/components/bookmarks/BookmarkId;)V

    .line 354
    return-void
.end method
