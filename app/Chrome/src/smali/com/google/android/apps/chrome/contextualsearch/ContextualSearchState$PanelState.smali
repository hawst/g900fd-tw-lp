.class public final enum Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;
.super Ljava/lang/Enum;
.source "ContextualSearchState.java"


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

.field public static final enum CLOSED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

.field public static final enum EXPANDED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

.field public static final enum MAXIMIZED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

.field public static final enum PEEKED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

.field public static final enum PROMO:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

.field public static final enum UNDEFINED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 21
    new-instance v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    const-string/jumbo v1, "UNDEFINED"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->UNDEFINED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    .line 22
    new-instance v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    const-string/jumbo v1, "CLOSED"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->CLOSED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    .line 23
    new-instance v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    const-string/jumbo v1, "PEEKED"

    invoke-direct {v0, v1, v5}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->PEEKED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    .line 24
    new-instance v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    const-string/jumbo v1, "PROMO"

    invoke-direct {v0, v1, v6}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->PROMO:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    .line 25
    new-instance v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    const-string/jumbo v1, "EXPANDED"

    invoke-direct {v0, v1, v7}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->EXPANDED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    .line 26
    new-instance v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    const-string/jumbo v1, "MAXIMIZED"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->MAXIMIZED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    .line 20
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    sget-object v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->UNDEFINED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->CLOSED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->PEEKED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->PROMO:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->EXPANDED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->MAXIMIZED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->$VALUES:[Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;
    .locals 1

    .prologue
    .line 20
    const-class v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->$VALUES:[Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    invoke-virtual {v0}, [Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    return-object v0
.end method
