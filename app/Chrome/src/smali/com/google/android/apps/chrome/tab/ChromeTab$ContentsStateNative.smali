.class Lcom/google/android/apps/chrome/tab/ChromeTab$ContentsStateNative;
.super Lcom/google/android/apps/chrome/tab/ChromeTab$ContentsState;
.source "ChromeTab.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final mHandler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 249
    const-class v0, Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/tab/ChromeTab$ContentsStateNative;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljava/nio/ByteBuffer;)V
    .locals 1

    .prologue
    .line 253
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/tab/ChromeTab$ContentsState;-><init>(Ljava/nio/ByteBuffer;)V

    .line 254
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$ContentsStateNative;->mHandler:Landroid/os/Handler;

    .line 255
    return-void
.end method


# virtual methods
.method protected finalize()V
    .locals 2

    .prologue
    .line 259
    sget-boolean v0, Lcom/google/android/apps/chrome/tab/ChromeTab$ContentsStateNative;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$ContentsStateNative;->mHandler:Landroid/os/Handler;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 260
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$ContentsStateNative;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/chrome/tab/ChromeTab$ContentsStateNative$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/tab/ChromeTab$ContentsStateNative$1;-><init>(Lcom/google/android/apps/chrome/tab/ChromeTab$ContentsStateNative;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 266
    return-void
.end method
