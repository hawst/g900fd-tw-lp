.class Lcom/google/android/apps/chrome/ntp/NewTabPageView$2;
.super Ljava/lang/Object;
.source "NewTabPageView.java"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/ntp/NewTabPageView;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/ntp/NewTabPageView;)V
    .locals 0

    .prologue
    .line 225
    iput-object p1, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView$2;->this$0:Lcom/google/android/apps/chrome/ntp/NewTabPageView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 3

    .prologue
    .line 236
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v0

    if-nez v0, :cond_0

    .line 239
    :goto_0
    return-void

    .line 237
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView$2;->this$0:Lcom/google/android/apps/chrome/ntp/NewTabPageView;

    # getter for: Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mManager:Lcom/google/android/apps/chrome/ntp/NewTabPageView$NewTabPageManager;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->access$000(Lcom/google/android/apps/chrome/ntp/NewTabPageView;)Lcom/google/android/apps/chrome/ntp/NewTabPageView$NewTabPageManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/chrome/ntp/NewTabPageView$NewTabPageManager;->focusSearchBox(ZLjava/lang/String;)V

    .line 238
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView$2;->this$0:Lcom/google/android/apps/chrome/ntp/NewTabPageView;

    # getter for: Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mSearchBoxTextView:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->access$100(Lcom/google/android/apps/chrome/ntp/NewTabPageView;)Landroid/widget/TextView;

    move-result-object v0

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 228
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 232
    return-void
.end method
