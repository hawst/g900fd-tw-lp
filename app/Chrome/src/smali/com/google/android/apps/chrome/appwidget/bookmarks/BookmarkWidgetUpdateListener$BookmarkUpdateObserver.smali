.class Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener$BookmarkUpdateObserver;
.super Landroid/database/ContentObserver;
.source "BookmarkWidgetUpdateListener.java"


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener;)V
    .locals 1

    .prologue
    .line 52
    iput-object p1, p0, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener$BookmarkUpdateObserver;->this$0:Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener;

    .line 53
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    invoke-direct {p0, v0}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 54
    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener$BookmarkUpdateObserver;->this$0:Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener;

    # getter for: Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener;->mListener:Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener$UpdateListener;
    invoke-static {v0}, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener;->access$000(Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener;)Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener$UpdateListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener$BookmarkUpdateObserver;->this$0:Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener;

    # getter for: Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener;->mListener:Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener$UpdateListener;
    invoke-static {v0}, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener;->access$000(Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener;)Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener$UpdateListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener$UpdateListener;->onBookmarkModelUpdated()V

    .line 59
    :cond_0
    return-void
.end method
