.class public Lcom/google/android/apps/chrome/preferences/DocumentModePreference;
.super Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;
.source "DocumentModePreference.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 19
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    .line 20
    sget v0, Lcom/google/android/apps/chrome/R$xml;->document_mode_preferences:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/DocumentModePreference;->addPreferencesFromResource(I)V

    .line 22
    new-instance v0, Landroid/widget/Switch;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/DocumentModePreference;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/Switch;-><init>(Landroid/content/Context;)V

    .line 23
    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/DocumentModePreference;->addSwitchToActionBar(Landroid/widget/Switch;)V

    .line 24
    new-instance v1, Lcom/google/android/apps/chrome/preferences/DocumentModeEnabler;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/DocumentModePreference;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/google/android/apps/chrome/preferences/DocumentModeEnabler;-><init>(Landroid/app/Activity;Landroid/widget/Switch;)V

    .line 25
    invoke-virtual {v1}, Lcom/google/android/apps/chrome/preferences/DocumentModeEnabler;->attach()V

    .line 26
    return-void
.end method
