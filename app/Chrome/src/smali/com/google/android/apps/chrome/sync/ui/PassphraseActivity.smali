.class public Lcom/google/android/apps/chrome/sync/ui/PassphraseActivity;
.super Landroid/support/v4/app/k;
.source "PassphraseActivity.java"

# interfaces
.implements Lcom/google/android/apps/chrome/sync/ui/PassphraseDialogFragment$Listener;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final FRAGMENT_PASSWORD:Ljava/lang/String; = "password_fragment"

.field public static final FRAGMENT_SPINNER:Ljava/lang/String; = "spinner_fragment"

.field private static sSyncStateChangedListener:Lorg/chromium/chrome/browser/sync/ProfileSyncService$SyncStateChangedListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const-class v0, Lcom/google/android/apps/chrome/sync/ui/PassphraseActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/sync/ui/PassphraseActivity;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Landroid/support/v4/app/k;-><init>()V

    .line 135
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/sync/ui/PassphraseActivity;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/ui/PassphraseActivity;->removeSyncStateChangedListener()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/sync/ui/PassphraseActivity;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/ui/PassphraseActivity;->displayPasswordDialog()V

    return-void
.end method

.method private addSyncStateChangedListener()V
    .locals 2

    .prologue
    .line 74
    sget-object v0, Lcom/google/android/apps/chrome/sync/ui/PassphraseActivity;->sSyncStateChangedListener:Lorg/chromium/chrome/browser/sync/ProfileSyncService$SyncStateChangedListener;

    if-eqz v0, :cond_0

    .line 87
    :goto_0
    return-void

    .line 77
    :cond_0
    new-instance v0, Lcom/google/android/apps/chrome/sync/ui/PassphraseActivity$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/sync/ui/PassphraseActivity$1;-><init>(Lcom/google/android/apps/chrome/sync/ui/PassphraseActivity;)V

    sput-object v0, Lcom/google/android/apps/chrome/sync/ui/PassphraseActivity;->sSyncStateChangedListener:Lorg/chromium/chrome/browser/sync/ProfileSyncService$SyncStateChangedListener;

    .line 86
    invoke-static {p0}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->get(Landroid/content/Context;)Lorg/chromium/chrome/browser/sync/ProfileSyncService;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/chrome/sync/ui/PassphraseActivity;->sSyncStateChangedListener:Lorg/chromium/chrome/browser/sync/ProfileSyncService$SyncStateChangedListener;

    invoke-virtual {v0, v1}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->addSyncStateChangedListener(Lorg/chromium/chrome/browser/sync/ProfileSyncService$SyncStateChangedListener;)V

    goto :goto_0
.end method

.method private displayPasswordDialog()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 99
    sget-boolean v0, Lcom/google/android/apps/chrome/sync/ui/PassphraseActivity;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-static {p0}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->get(Landroid/content/Context;)Lorg/chromium/chrome/browser/sync/ProfileSyncService;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->isSyncInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 100
    :cond_0
    invoke-static {p0}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->get(Landroid/content/Context;)Lorg/chromium/chrome/browser/sync/ProfileSyncService;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->isUsingSecondaryPassphrase()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 101
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/ui/PassphraseActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v2

    .line 102
    const/4 v3, 0x0

    invoke-static {v3, v0, v1}, Lcom/google/android/apps/chrome/sync/ui/PassphraseDialogFragment;->newInstance(Landroid/app/Fragment;ZZ)Lcom/google/android/apps/chrome/sync/ui/PassphraseDialogFragment;

    move-result-object v0

    const-string/jumbo v1, "password_fragment"

    invoke-virtual {v0, v2, v1}, Lcom/google/android/apps/chrome/sync/ui/PassphraseDialogFragment;->show(Landroid/app/FragmentTransaction;Ljava/lang/String;)I

    .line 103
    return-void

    :cond_1
    move v0, v1

    .line 100
    goto :goto_0
.end method

.method private displaySpinnerDialog()V
    .locals 3

    .prologue
    .line 106
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/ui/PassphraseActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 107
    new-instance v1, Lcom/google/android/apps/chrome/sync/ui/PassphraseActivity$SpinnerDialogFragment;

    invoke-direct {v1}, Lcom/google/android/apps/chrome/sync/ui/PassphraseActivity$SpinnerDialogFragment;-><init>()V

    .line 108
    const-string/jumbo v2, "spinner_fragment"

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/chrome/sync/ui/PassphraseActivity$SpinnerDialogFragment;->show(Landroid/app/FragmentTransaction;Ljava/lang/String;)I

    .line 109
    return-void
.end method

.method private isShowingDialog(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 95
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/ui/PassphraseActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private removeSyncStateChangedListener()V
    .locals 2

    .prologue
    .line 90
    invoke-static {p0}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->get(Landroid/content/Context;)Lorg/chromium/chrome/browser/sync/ProfileSyncService;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/chrome/sync/ui/PassphraseActivity;->sSyncStateChangedListener:Lorg/chromium/chrome/browser/sync/ProfileSyncService$SyncStateChangedListener;

    invoke-virtual {v0, v1}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->removeSyncStateChangedListener(Lorg/chromium/chrome/browser/sync/ProfileSyncService$SyncStateChangedListener;)V

    .line 91
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/apps/chrome/sync/ui/PassphraseActivity;->sSyncStateChangedListener:Lorg/chromium/chrome/browser/sync/ProfileSyncService$SyncStateChangedListener;

    .line 92
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 39
    invoke-super {p0, p1}, Landroid/support/v4/app/k;->onCreate(Landroid/os/Bundle;)V

    .line 45
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/ui/PassphraseActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/ChromeMobileApplication;

    const/4 v1, 0x1

    invoke-virtual {v0, p0, v1}, Lcom/google/android/apps/chrome/ChromeMobileApplication;->startBrowserProcessesAndLoadLibrariesSync(Landroid/content/Context;Z)V
    :try_end_0
    .catch Lorg/chromium/base/library_loader/ProcessInitException; {:try_start_0 .. :try_end_0} :catch_0

    .line 51
    :goto_0
    sget v0, Lcom/google/android/apps/chrome/R$layout;->sync_passphrase_activity:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/sync/ui/PassphraseActivity;->setContentView(I)V

    .line 52
    return-void

    .line 47
    :catch_0
    move-exception v0

    .line 48
    const-string/jumbo v1, "PassphraseActivity"

    const-string/jumbo v2, "Failed to start browser process."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 49
    invoke-static {v0}, Lcom/google/android/apps/chrome/ChromeMobileApplication;->reportStartupErrorAndExit(Lorg/chromium/base/library_loader/ProcessInitException;)V

    goto :goto_0
.end method

.method public onPassphraseCanceled(ZZ)V
    .locals 1

    .prologue
    .line 128
    invoke-static {}, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->getInstance()Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->syncStateChanged()V

    .line 129
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/ui/PassphraseActivity;->finish()V

    .line 130
    return-void
.end method

.method public onPassphraseEntered(Ljava/lang/String;ZZ)V
    .locals 2

    .prologue
    .line 113
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {p0}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->get(Landroid/content/Context;)Lorg/chromium/chrome/browser/sync/ProfileSyncService;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->setDecryptionPassphrase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 115
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/ui/PassphraseActivity;->finish()V

    .line 123
    :cond_0
    :goto_0
    return-void

    .line 118
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/ui/PassphraseActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const-string/jumbo v1, "password_fragment"

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    .line 119
    instance-of v1, v0, Lcom/google/android/apps/chrome/sync/ui/PassphraseDialogFragment;

    if-eqz v1, :cond_0

    .line 120
    check-cast v0, Lcom/google/android/apps/chrome/sync/ui/PassphraseDialogFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/sync/ui/PassphraseDialogFragment;->invalidPassphrase()V

    goto :goto_0
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 56
    invoke-super {p0}, Landroid/support/v4/app/k;->onResume()V

    .line 57
    invoke-static {p0}, Lorg/chromium/sync/signin/ChromeSigninController;->get(Landroid/content/Context;)Lorg/chromium/sync/signin/ChromeSigninController;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/sync/signin/ChromeSigninController;->getSignedInUser()Landroid/accounts/Account;

    move-result-object v0

    .line 58
    if-nez v0, :cond_1

    .line 59
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/ui/PassphraseActivity;->finish()V

    .line 71
    :cond_0
    :goto_0
    return-void

    .line 63
    :cond_1
    const-string/jumbo v0, "password_fragment"

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/sync/ui/PassphraseActivity;->isShowingDialog(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 64
    invoke-static {p0}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->get(Landroid/content/Context;)Lorg/chromium/chrome/browser/sync/ProfileSyncService;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->isSyncInitialized()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 65
    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/ui/PassphraseActivity;->displayPasswordDialog()V

    goto :goto_0

    .line 67
    :cond_2
    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/ui/PassphraseActivity;->addSyncStateChangedListener()V

    .line 68
    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/ui/PassphraseActivity;->displaySpinnerDialog()V

    goto :goto_0
.end method
