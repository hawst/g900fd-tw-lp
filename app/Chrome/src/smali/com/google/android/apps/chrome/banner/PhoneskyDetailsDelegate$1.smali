.class Lcom/google/android/apps/chrome/banner/PhoneskyDetailsDelegate$1;
.super Landroid/os/AsyncTask;
.source "PhoneskyDetailsDelegate.java"


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/banner/PhoneskyDetailsDelegate;

.field final synthetic val$iconSize:I

.field final synthetic val$observer:Lorg/chromium/chrome/browser/banners/AppDetailsDelegate$Observer;

.field final synthetic val$packageName:Ljava/lang/String;

.field final synthetic val$url:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/banner/PhoneskyDetailsDelegate;Ljava/lang/String;Ljava/lang/String;ILorg/chromium/chrome/browser/banners/AppDetailsDelegate$Observer;)V
    .locals 0

    .prologue
    .line 83
    iput-object p1, p0, Lcom/google/android/apps/chrome/banner/PhoneskyDetailsDelegate$1;->this$0:Lcom/google/android/apps/chrome/banner/PhoneskyDetailsDelegate;

    iput-object p2, p0, Lcom/google/android/apps/chrome/banner/PhoneskyDetailsDelegate$1;->val$packageName:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/apps/chrome/banner/PhoneskyDetailsDelegate$1;->val$url:Ljava/lang/String;

    iput p4, p0, Lcom/google/android/apps/chrome/banner/PhoneskyDetailsDelegate$1;->val$iconSize:I

    iput-object p5, p0, Lcom/google/android/apps/chrome/banner/PhoneskyDetailsDelegate$1;->val$observer:Lorg/chromium/chrome/browser/banners/AppDetailsDelegate$Observer;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 83
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/banner/PhoneskyDetailsDelegate$1;->doInBackground([Ljava/lang/Void;)Lorg/chromium/chrome/browser/banners/AppData;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Lorg/chromium/chrome/browser/banners/AppData;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 86
    iget-object v1, p0, Lcom/google/android/apps/chrome/banner/PhoneskyDetailsDelegate$1;->this$0:Lcom/google/android/apps/chrome/banner/PhoneskyDetailsDelegate;

    # getter for: Lcom/google/android/apps/chrome/banner/PhoneskyDetailsDelegate;->mDetailsService:Lcom/a/b/a/a;
    invoke-static {v1}, Lcom/google/android/apps/chrome/banner/PhoneskyDetailsDelegate;->access$000(Lcom/google/android/apps/chrome/banner/PhoneskyDetailsDelegate;)Lcom/a/b/a/a;

    move-result-object v1

    if-nez v1, :cond_0

    .line 96
    :goto_0
    return-object v0

    .line 89
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/banner/PhoneskyDetailsDelegate$1;->this$0:Lcom/google/android/apps/chrome/banner/PhoneskyDetailsDelegate;

    # getter for: Lcom/google/android/apps/chrome/banner/PhoneskyDetailsDelegate;->mDetailsService:Lcom/a/b/a/a;
    invoke-static {v1}, Lcom/google/android/apps/chrome/banner/PhoneskyDetailsDelegate;->access$000(Lcom/google/android/apps/chrome/banner/PhoneskyDetailsDelegate;)Lcom/a/b/a/a;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/banner/PhoneskyDetailsDelegate$1;->val$packageName:Ljava/lang/String;

    invoke-interface {v1, v2}, Lcom/a/b/a/a;->a(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    .line 90
    iget-object v2, p0, Lcom/google/android/apps/chrome/banner/PhoneskyDetailsDelegate$1;->this$0:Lcom/google/android/apps/chrome/banner/PhoneskyDetailsDelegate;

    iget-object v3, p0, Lcom/google/android/apps/chrome/banner/PhoneskyDetailsDelegate$1;->val$url:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/chrome/banner/PhoneskyDetailsDelegate$1;->val$packageName:Ljava/lang/String;

    iget v5, p0, Lcom/google/android/apps/chrome/banner/PhoneskyDetailsDelegate$1;->val$iconSize:I

    # invokes: Lcom/google/android/apps/chrome/banner/PhoneskyDetailsDelegate;->parseAndVerifyAppBundle(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;I)Lorg/chromium/chrome/browser/banners/AppData;
    invoke-static {v2, v3, v4, v1, v5}, Lcom/google/android/apps/chrome/banner/PhoneskyDetailsDelegate;->access$100(Lcom/google/android/apps/chrome/banner/PhoneskyDetailsDelegate;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;I)Lorg/chromium/chrome/browser/banners/AppData;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    goto :goto_0

    .line 91
    :catch_0
    move-exception v1

    .line 92
    const-string/jumbo v2, "PhoneskyDetailsDelegate"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Crash in the remote Service: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 94
    :catch_1
    move-exception v1

    .line 95
    const-string/jumbo v2, "PhoneskyDetailsDelegate"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Remote exception: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 83
    check-cast p1, Lorg/chromium/chrome/browser/banners/AppData;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/banner/PhoneskyDetailsDelegate$1;->onPostExecute(Lorg/chromium/chrome/browser/banners/AppData;)V

    return-void
.end method

.method protected onPostExecute(Lorg/chromium/chrome/browser/banners/AppData;)V
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/google/android/apps/chrome/banner/PhoneskyDetailsDelegate$1;->val$observer:Lorg/chromium/chrome/browser/banners/AppDetailsDelegate$Observer;

    invoke-interface {v0, p1}, Lorg/chromium/chrome/browser/banners/AppDetailsDelegate$Observer;->onAppDetailsRetrieved(Lorg/chromium/chrome/browser/banners/AppData;)V

    .line 103
    iget-object v0, p0, Lcom/google/android/apps/chrome/banner/PhoneskyDetailsDelegate$1;->this$0:Lcom/google/android/apps/chrome/banner/PhoneskyDetailsDelegate;

    # invokes: Lcom/google/android/apps/chrome/banner/PhoneskyDetailsDelegate;->performNextRequest()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/banner/PhoneskyDetailsDelegate;->access$200(Lcom/google/android/apps/chrome/banner/PhoneskyDetailsDelegate;)V

    .line 104
    return-void
.end method
