.class Lcom/google/android/apps/chrome/webapps/WebappActivity$3;
.super Lorg/chromium/content/browser/WebContentsObserverAndroid;
.source "WebappActivity.java"


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/webapps/WebappActivity;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/webapps/WebappActivity;Lorg/chromium/content_public/browser/WebContents;)V
    .locals 0

    .prologue
    .line 242
    iput-object p1, p0, Lcom/google/android/apps/chrome/webapps/WebappActivity$3;->this$0:Lcom/google/android/apps/chrome/webapps/WebappActivity;

    invoke-direct {p0, p2}, Lorg/chromium/content/browser/WebContentsObserverAndroid;-><init>(Lorg/chromium/content_public/browser/WebContents;)V

    return-void
.end method


# virtual methods
.method public didAttachInterstitialPage()V
    .locals 4

    .prologue
    .line 251
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/WebappActivity$3;->this$0:Lcom/google/android/apps/chrome/webapps/WebappActivity;

    # invokes: Lcom/google/android/apps/chrome/webapps/WebappActivity;->updateUrlBar()Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/webapps/WebappActivity;->access$000(Lcom/google/android/apps/chrome/webapps/WebappActivity;)Z

    .line 253
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/WebappActivity$3;->this$0:Lcom/google/android/apps/chrome/webapps/WebappActivity;

    invoke-static {v0}, Lorg/chromium/base/ApplicationStatus;->getStateForActivity(Landroid/app/Activity;)I

    move-result v0

    .line 254
    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    const/4 v1, 0x5

    if-eq v0, v1, :cond_0

    const/4 v1, 0x6

    if-ne v0, v1, :cond_1

    .line 273
    :cond_0
    :goto_0
    return-void

    .line 260
    :cond_1
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.action.VIEW"

    iget-object v2, p0, Lcom/google/android/apps/chrome/webapps/WebappActivity$3;->this$0:Lcom/google/android/apps/chrome/webapps/WebappActivity;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/webapps/WebappActivity;->getTab()Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab;->getUrl()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 261
    iget-object v1, p0, Lcom/google/android/apps/chrome/webapps/WebappActivity$3;->this$0:Lcom/google/android/apps/chrome/webapps/WebappActivity;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/webapps/WebappActivity;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 262
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 263
    iget-object v1, p0, Lcom/google/android/apps/chrome/webapps/WebappActivity$3;->this$0:Lcom/google/android/apps/chrome/webapps/WebappActivity;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/webapps/WebappActivity;->startActivity(Landroid/content/Intent;)V

    .line 267
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/WebappActivity$3;->this$0:Lcom/google/android/apps/chrome/webapps/WebappActivity;

    # getter for: Lcom/google/android/apps/chrome/webapps/WebappActivity;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/google/android/apps/chrome/webapps/WebappActivity;->access$200(Lcom/google/android/apps/chrome/webapps/WebappActivity;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/chrome/webapps/WebappActivity$3$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/webapps/WebappActivity$3$1;-><init>(Lcom/google/android/apps/chrome/webapps/WebappActivity$3;)V

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method public didDetachInterstitialPage()V
    .locals 1

    .prologue
    .line 277
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/WebappActivity$3;->this$0:Lcom/google/android/apps/chrome/webapps/WebappActivity;

    # invokes: Lcom/google/android/apps/chrome/webapps/WebappActivity;->updateUrlBar()Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/webapps/WebappActivity;->access$000(Lcom/google/android/apps/chrome/webapps/WebappActivity;)Z

    .line 278
    return-void
.end method

.method public didNavigateMainFrame(Ljava/lang/String;Ljava/lang/String;ZZ)V
    .locals 1

    .prologue
    .line 246
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/WebappActivity$3;->this$0:Lcom/google/android/apps/chrome/webapps/WebappActivity;

    # invokes: Lcom/google/android/apps/chrome/webapps/WebappActivity;->updateUrlBar()Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/webapps/WebappActivity;->access$000(Lcom/google/android/apps/chrome/webapps/WebappActivity;)Z

    .line 247
    return-void
.end method
