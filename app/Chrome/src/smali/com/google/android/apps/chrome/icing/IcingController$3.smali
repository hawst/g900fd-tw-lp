.class Lcom/google/android/apps/chrome/icing/IcingController$3;
.super Lcom/google/android/apps/chrome/icing/ConnectedTask;
.source "IcingController.java"


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/icing/IcingController;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/icing/IcingController;Lcom/google/android/apps/chrome/icing/GoogleApiIcingClient;Ljava/util/concurrent/ScheduledThreadPoolExecutor;)V
    .locals 0

    .prologue
    .line 115
    iput-object p1, p0, Lcom/google/android/apps/chrome/icing/IcingController$3;->this$0:Lcom/google/android/apps/chrome/icing/IcingController;

    invoke-direct {p0, p2, p3}, Lcom/google/android/apps/chrome/icing/ConnectedTask;-><init>(Lcom/google/android/apps/chrome/icing/GoogleApiIcingClient;Ljava/util/concurrent/ScheduledThreadPoolExecutor;)V

    return-void
.end method


# virtual methods
.method protected doWhenConnected(Lcom/google/android/apps/chrome/icing/GoogleApiIcingClient;)V
    .locals 2

    .prologue
    .line 123
    iget-object v0, p0, Lcom/google/android/apps/chrome/icing/IcingController$3;->this$0:Lcom/google/android/apps/chrome/icing/IcingController;

    # getter for: Lcom/google/android/apps/chrome/icing/IcingController;->mJniBridge:Lcom/google/android/apps/chrome/icing/SearchJniBridge;
    invoke-static {v0}, Lcom/google/android/apps/chrome/icing/IcingController;->access$100(Lcom/google/android/apps/chrome/icing/IcingController;)Lcom/google/android/apps/chrome/icing/SearchJniBridge;

    move-result-object v0

    const/16 v1, 0x64

    invoke-interface {v0, v1}, Lcom/google/android/apps/chrome/icing/SearchJniBridge;->getUsageReportsBatch(I)[Lcom/google/android/apps/chrome/icing/UsageReport;

    move-result-object v0

    .line 124
    array-length v1, v0

    if-lez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/chrome/icing/IcingController$3;->this$0:Lcom/google/android/apps/chrome/icing/IcingController;

    # getter for: Lcom/google/android/apps/chrome/icing/IcingController;->mUsageReportingTaskActive:Ljava/util/concurrent/atomic/AtomicBoolean;
    invoke-static {v1}, Lcom/google/android/apps/chrome/icing/IcingController;->access$300(Lcom/google/android/apps/chrome/icing/IcingController;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 125
    invoke-interface {p1, v0}, Lcom/google/android/apps/chrome/icing/GoogleApiIcingClient;->reportPageVisits([Lcom/google/android/apps/chrome/icing/UsageReport;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 126
    iget-object v1, p0, Lcom/google/android/apps/chrome/icing/IcingController$3;->this$0:Lcom/google/android/apps/chrome/icing/IcingController;

    # getter for: Lcom/google/android/apps/chrome/icing/IcingController;->mJniBridge:Lcom/google/android/apps/chrome/icing/SearchJniBridge;
    invoke-static {v1}, Lcom/google/android/apps/chrome/icing/IcingController;->access$100(Lcom/google/android/apps/chrome/icing/IcingController;)Lcom/google/android/apps/chrome/icing/SearchJniBridge;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/android/apps/chrome/icing/SearchJniBridge;->removeUsageReports([Lcom/google/android/apps/chrome/icing/UsageReport;)V

    .line 128
    :cond_0
    sget-wide v0, Lcom/google/android/apps/chrome/icing/IcingController;->USAGE_REPORTS_DELAY_MS:J

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/chrome/icing/IcingController$3;->reschedule(J)V

    .line 132
    :goto_0
    return-void

    .line 130
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/icing/IcingController$3;->this$0:Lcom/google/android/apps/chrome/icing/IcingController;

    # getter for: Lcom/google/android/apps/chrome/icing/IcingController;->mUsageReportingTaskActive:Ljava/util/concurrent/atomic/AtomicBoolean;
    invoke-static {v0}, Lcom/google/android/apps/chrome/icing/IcingController;->access$300(Lcom/google/android/apps/chrome/icing/IcingController;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    goto :goto_0
.end method

.method protected getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 118
    const-string/jumbo v0, "ReportUsage"

    return-object v0
.end method
