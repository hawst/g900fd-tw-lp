.class Lcom/google/android/apps/chrome/sync/AutoSigninSyncDelay$WelcomeTabObserver;
.super Lorg/chromium/chrome/browser/EmptyTabObserver;
.source "AutoSigninSyncDelay.java"


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/sync/AutoSigninSyncDelay;


# virtual methods
.method public onDestroyed(Lorg/chromium/chrome/browser/Tab;)V
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/AutoSigninSyncDelay$WelcomeTabObserver;->this$0:Lcom/google/android/apps/chrome/sync/AutoSigninSyncDelay;

    # invokes: Lcom/google/android/apps/chrome/sync/AutoSigninSyncDelay;->startSyncAndUnregisterListeners()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/sync/AutoSigninSyncDelay;->access$000(Lcom/google/android/apps/chrome/sync/AutoSigninSyncDelay;)V

    .line 39
    return-void
.end method

.method public onUpdateUrl(Lorg/chromium/chrome/browser/Tab;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 30
    const-string/jumbo v0, "chrome://welcome/"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 34
    :goto_0
    return-void

    .line 33
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/AutoSigninSyncDelay$WelcomeTabObserver;->this$0:Lcom/google/android/apps/chrome/sync/AutoSigninSyncDelay;

    # invokes: Lcom/google/android/apps/chrome/sync/AutoSigninSyncDelay;->startSyncAndUnregisterListeners()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/sync/AutoSigninSyncDelay;->access$000(Lcom/google/android/apps/chrome/sync/AutoSigninSyncDelay;)V

    goto :goto_0
.end method
