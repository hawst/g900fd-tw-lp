.class Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$FileMigrationTask;
.super Landroid/os/AsyncTask;
.source "TabPersistentStore.java"


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;)V
    .locals 0

    .prologue
    .line 809
    iput-object p1, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$FileMigrationTask;->this$0:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$1;)V
    .locals 0

    .prologue
    .line 809
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$FileMigrationTask;-><init>(Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 809
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$FileMigrationTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 812
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$FileMigrationTask;->this$0:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;

    # getter for: Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->access$1000(Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    .line 813
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$FileMigrationTask;->this$0:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;

    # getter for: Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->access$1000(Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;)Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$FileMigrationTask;->this$0:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;

    # getter for: Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mSelectorIndex:I
    invoke-static {v2}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->access$1100(Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;)I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->getStateFolder(Landroid/content/Context;I)Ljava/io/File;

    move-result-object v1

    .line 815
    invoke-virtual {v1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    .line 816
    if-eqz v2, :cond_1

    array-length v2, v2

    if-lez v2, :cond_1

    .line 832
    :cond_0
    return-object v7

    .line 818
    :cond_1
    new-instance v2, Ljava/io/File;

    const-string/jumbo v3, "tab_state"

    invoke-direct {v2, v0, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 819
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 820
    new-instance v3, Ljava/io/File;

    const-string/jumbo v4, "tab_state"

    invoke-direct {v3, v1, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    .line 823
    :cond_2
    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    .line 824
    if-eqz v2, :cond_0

    .line 825
    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 826
    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->parseStateDataFromFilename(Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v5

    if-eqz v5, :cond_3

    .line 827
    new-instance v5, Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v1, v6}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v4, v5}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    .line 825
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
