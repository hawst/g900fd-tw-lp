.class public Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog;
.super Landroid/app/DialogFragment;
.source "EnhancedBookmarkFolderSelectDialog.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private mBackButton:Landroid/widget/ImageButton;

.field private mBookmarkId:Lorg/chromium/components/bookmarks/BookmarkId;

.field private mBookmarkIdsAdapter:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog$FolderListAdapter;

.field private mBookmarkIdsList:Landroid/widget/ListView;

.field private final mEnhancedBookmarksModel:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;

.field private mFolderSelectedListener:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog$FolderSelectedListener;

.field private mIsCreatingFolder:Z

.field private mParentId:Lorg/chromium/components/bookmarks/BookmarkId;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const-class v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog$FolderSelectedListener;)V
    .locals 1

    .prologue
    .line 73
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 74
    iput-object p1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog;->mEnhancedBookmarksModel:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;

    .line 75
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog;->mIsCreatingFolder:Z

    .line 76
    iput-object p2, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog;->mFolderSelectedListener:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog$FolderSelectedListener;

    .line 77
    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;Lorg/chromium/components/bookmarks/BookmarkId;)V
    .locals 2

    .prologue
    .line 62
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 63
    iput-object p1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog;->mEnhancedBookmarksModel:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;

    .line 64
    iput-object p2, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog;->mBookmarkId:Lorg/chromium/components/bookmarks/BookmarkId;

    .line 65
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog;->mEnhancedBookmarksModel:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;

    iget-object v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog;->mBookmarkId:Lorg/chromium/components/bookmarks/BookmarkId;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;->getBookmarkById(Lorg/chromium/components/bookmarks/BookmarkId;)Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;->getParentId()Lorg/chromium/components/bookmarks/BookmarkId;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog;->mParentId:Lorg/chromium/components/bookmarks/BookmarkId;

    .line 66
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 130
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 131
    const/4 v0, 0x2

    sget v1, Lcom/google/android/apps/chrome/R$style;->EnhancedBookmarkDialog:I

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog;->setStyle(II)V

    .line 132
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 13

    .prologue
    .line 85
    sget v0, Lcom/google/android/apps/chrome/R$layout;->eb_folder_select_dialog:I

    invoke-virtual {p1, v0, p2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v9

    .line 87
    sget v0, Lcom/google/android/apps/chrome/R$id;->eb_folder_list:I

    invoke-virtual {v9, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog;->mBookmarkIdsList:Landroid/widget/ListView;

    .line 88
    sget v0, Lcom/google/android/apps/chrome/R$id;->back:I

    invoke-virtual {v9, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog;->mBackButton:Landroid/widget/ImageButton;

    .line 89
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog;->mBackButton:Landroid/widget/ImageButton;

    new-instance v1, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog$1;-><init>(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 96
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 97
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 98
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog;->mEnhancedBookmarksModel:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;

    invoke-virtual {v0, v10, v11}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;->getAllFoldersWithDepths(Ljava/util/List;Ljava/util/List;)V

    .line 99
    new-instance v12, Ljava/util/ArrayList;

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x3

    invoke-direct {v12, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 100
    new-instance v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog$FolderListEntry;

    const/4 v1, 0x0

    const/4 v2, 0x0

    sget v3, Lcom/google/android/apps/chrome/R$string;->enhanced_bookmark_none_folder:I

    invoke-virtual {p0, v3}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog$FolderListEntry;-><init>(Lorg/chromium/components/bookmarks/BookmarkId;ILjava/lang/String;ZI)V

    .line 103
    invoke-interface {v12, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 104
    iget-boolean v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog;->mIsCreatingFolder:Z

    if-nez v1, :cond_0

    .line 105
    new-instance v1, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog$FolderListEntry;

    const/4 v2, 0x0

    const/4 v3, 0x0

    sget v4, Lcom/google/android/apps/chrome/R$string;->enhanced_bookmark_add_folder:I

    invoke-virtual {p0, v4}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x1

    invoke-direct/range {v1 .. v6}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog$FolderListEntry;-><init>(Lorg/chromium/components/bookmarks/BookmarkId;ILjava/lang/String;ZI)V

    invoke-interface {v12, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 109
    :cond_0
    new-instance v1, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog$FolderListEntry;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const-string/jumbo v4, ""

    const/4 v5, 0x0

    const/4 v6, 0x2

    invoke-direct/range {v1 .. v6}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog$FolderListEntry;-><init>(Lorg/chromium/components/bookmarks/BookmarkId;ILjava/lang/String;ZI)V

    invoke-interface {v12, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 111
    const/4 v2, 0x1

    .line 112
    const/4 v1, 0x0

    move v7, v1

    move v1, v2

    :goto_0
    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v2

    if-ge v7, v2, :cond_1

    .line 113
    invoke-interface {v10, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/chromium/components/bookmarks/BookmarkId;

    .line 114
    iget-object v3, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog;->mEnhancedBookmarksModel:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;

    invoke-virtual {v3, v2}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;->getBookmarkById(Lorg/chromium/components/bookmarks/BookmarkId;)Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;

    move-result-object v3

    invoke-virtual {v3}, Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;->getTitle()Ljava/lang/String;

    move-result-object v4

    .line 115
    iget-object v3, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog;->mParentId:Lorg/chromium/components/bookmarks/BookmarkId;

    invoke-virtual {v2, v3}, Lorg/chromium/components/bookmarks/BookmarkId;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v8, 0x0

    .line 116
    :goto_1
    new-instance v1, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog$FolderListEntry;

    invoke-interface {v11, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    iget-object v5, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog;->mParentId:Lorg/chromium/components/bookmarks/BookmarkId;

    invoke-virtual {v2, v5}, Lorg/chromium/components/bookmarks/BookmarkId;->equals(Ljava/lang/Object;)Z

    move-result v5

    const/4 v6, 0x3

    invoke-direct/range {v1 .. v6}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog$FolderListEntry;-><init>(Lorg/chromium/components/bookmarks/BookmarkId;ILjava/lang/String;ZI)V

    invoke-interface {v12, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 112
    add-int/lit8 v1, v7, 0x1

    move v7, v1

    move v1, v8

    goto :goto_0

    .line 119
    :cond_1
    iput-boolean v1, v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog$FolderListEntry;->mIsSelected:Z

    .line 121
    new-instance v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog$FolderListAdapter;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v12, v1}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog$FolderListAdapter;-><init>(Ljava/util/List;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog;->mBookmarkIdsAdapter:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog$FolderListAdapter;

    .line 122
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog;->mBookmarkIdsList:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog;->mBookmarkIdsAdapter:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog$FolderListAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 123
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog;->mBookmarkIdsList:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 125
    return-object v9

    :cond_2
    move v8, v1

    goto :goto_1
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 5

    .prologue
    const/4 v4, 0x3

    const/4 v3, 0x0

    .line 147
    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog$FolderListEntry;

    .line 148
    iget-boolean v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog;->mIsCreatingFolder:Z

    if-eqz v1, :cond_3

    .line 149
    const/4 v1, 0x0

    .line 150
    iget v2, v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog$FolderListEntry;->mType:I

    if-nez v2, :cond_1

    .line 151
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog;->mEnhancedBookmarksModel:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;->getMobileFolderId()Lorg/chromium/components/bookmarks/BookmarkId;

    move-result-object v0

    .line 158
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog;->mFolderSelectedListener:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog$FolderSelectedListener;

    invoke-interface {v1, v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog$FolderSelectedListener;->onFolderSelected(Lorg/chromium/components/bookmarks/BookmarkId;)V

    .line 159
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog;->dismiss()V

    .line 175
    :cond_0
    :goto_1
    return-void

    .line 152
    :cond_1
    iget v2, v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog$FolderListEntry;->mType:I

    if-ne v2, v4, :cond_2

    .line 153
    iget-object v0, v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog$FolderListEntry;->mId:Lorg/chromium/components/bookmarks/BookmarkId;

    goto :goto_0

    .line 156
    :cond_2
    sget-boolean v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog;->$assertionsDisabled:Z

    if-nez v0, :cond_6

    new-instance v0, Ljava/lang/AssertionError;

    const-string/jumbo v1, "Divider and new folder items should not be clickable in creating mode"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 163
    :cond_3
    iget v1, v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog$FolderListEntry;->mType:I

    if-nez v1, :cond_4

    .line 164
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog;->mEnhancedBookmarksModel:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;

    iget-object v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog;->mBookmarkId:Lorg/chromium/components/bookmarks/BookmarkId;

    iget-object v2, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog;->mEnhancedBookmarksModel:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;->getMobileFolderId()Lorg/chromium/components/bookmarks/BookmarkId;

    move-result-object v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;->moveBookmark(Lorg/chromium/components/bookmarks/BookmarkId;Lorg/chromium/components/bookmarks/BookmarkId;I)V

    .line 166
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog;->dismiss()V

    goto :goto_1

    .line 167
    :cond_4
    iget v1, v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog$FolderListEntry;->mType:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_5

    .line 168
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog;->mEnhancedBookmarksModel:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;

    iget-object v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog;->mBookmarkId:Lorg/chromium/components/bookmarks/BookmarkId;

    invoke-static {v0, v1, p0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkAddEditFolderDialog;->createAddFolderDialog(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;Lorg/chromium/components/bookmarks/BookmarkId;Landroid/app/DialogFragment;)Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkAddEditFolderDialog;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkAddEditFolderDialog;->show(Landroid/app/FragmentManager;)V

    goto :goto_1

    .line 170
    :cond_5
    iget v1, v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog$FolderListEntry;->mType:I

    if-ne v1, v4, :cond_0

    .line 172
    iget-object v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog;->mEnhancedBookmarksModel:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;

    iget-object v2, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog;->mBookmarkId:Lorg/chromium/components/bookmarks/BookmarkId;

    iget-object v0, v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog$FolderListEntry;->mId:Lorg/chromium/components/bookmarks/BookmarkId;

    invoke-virtual {v1, v2, v0, v3}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;->moveBookmark(Lorg/chromium/components/bookmarks/BookmarkId;Lorg/chromium/components/bookmarks/BookmarkId;I)V

    .line 173
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog;->dismiss()V

    goto :goto_1

    :cond_6
    move-object v0, v1

    goto :goto_0
.end method

.method public show(Landroid/app/FragmentManager;)V
    .locals 1

    .prologue
    .line 138
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-super {p0, p1, v0}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 139
    return-void
.end method
