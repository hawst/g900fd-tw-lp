.class Lcom/google/android/apps/chrome/ChromeMobileApplication$3;
.super Ljava/lang/Object;
.source "ChromeMobileApplication.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/ChromeMobileApplication;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$initGoogleServicesManager:Z


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/ChromeMobileApplication;Landroid/content/Context;Z)V
    .locals 0

    .prologue
    .line 317
    iput-object p1, p0, Lcom/google/android/apps/chrome/ChromeMobileApplication$3;->this$0:Lcom/google/android/apps/chrome/ChromeMobileApplication;

    iput-object p2, p0, Lcom/google/android/apps/chrome/ChromeMobileApplication$3;->val$context:Landroid/content/Context;

    iput-boolean p3, p0, Lcom/google/android/apps/chrome/ChromeMobileApplication$3;->val$initGoogleServicesManager:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()Ljava/lang/Integer;
    .locals 3

    .prologue
    .line 321
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeMobileApplication$3;->val$context:Landroid/content/Context;

    invoke-static {v0}, Lorg/chromium/content/browser/BrowserStartupController;->get(Landroid/content/Context;)Lorg/chromium/content/browser/BrowserStartupController;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lorg/chromium/content/browser/BrowserStartupController;->startBrowserProcessesSync(Z)V

    .line 322
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/ChromeMobileApplication$3;->val$initGoogleServicesManager:Z

    if-eqz v0, :cond_0

    .line 323
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeMobileApplication$3;->val$context:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->get(Landroid/content/Context;)Lcom/google/android/apps/chrome/services/GoogleServicesManager;

    .line 325
    :cond_0
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_0
    .catch Lorg/chromium/base/library_loader/ProcessInitException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 328
    :goto_0
    return-object v0

    .line 326
    :catch_0
    move-exception v0

    .line 327
    const-string/jumbo v1, "ChromeMobileApplication"

    const-string/jumbo v2, "Unable to load native library."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 328
    invoke-virtual {v0}, Lorg/chromium/base/library_loader/ProcessInitException;->getErrorCode()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0
.end method

.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 317
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeMobileApplication$3;->call()Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method
