.class public Lcom/google/android/apps/chrome/tabs/layout/phone/StackViewAnimation;
.super Ljava/lang/Object;
.source "StackViewAnimation.java"


# instance fields
.field private final mDpToPx:F

.field private final mWidthDp:F


# direct methods
.method public constructor <init>(FF)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput p1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackViewAnimation;->mDpToPx:F

    .line 40
    iput p2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackViewAnimation;->mWidthDp:F

    .line 41
    return-void
.end method

.method private createNewTabOpenedAnimatorSet([Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;Landroid/view/ViewGroup;Lorg/chromium/chrome/browser/tabmodel/TabModel;I)Landroid/animation/AnimatorSet;
    .locals 9

    .prologue
    const/4 v0, 0x0

    const/4 v4, -0x1

    const/high16 v8, 0x41c00000    # 24.0f

    const/4 v7, 0x2

    .line 73
    invoke-interface {p3, p4}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->getTabAt(I)Lorg/chromium/chrome/browser/Tab;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->fromTab(Lorg/chromium/chrome/browser/Tab;)Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v1

    .line 74
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->isNativePage()Z

    move-result v2

    if-nez v2, :cond_1

    .line 108
    :cond_0
    :goto_0
    return-object v0

    .line 76
    :cond_1
    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getView()Landroid/view/View;

    move-result-object v2

    .line 77
    if-eqz v2, :cond_0

    .line 80
    invoke-virtual {v2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 81
    :cond_2
    new-instance v3, Landroid/widget/FrameLayout;

    invoke-virtual {v2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v3, v0}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 82
    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getBackgroundColor()I

    move-result v0

    invoke-virtual {v3, v0}, Landroid/view/ViewGroup;->setBackgroundColor(I)V

    .line 83
    invoke-virtual {v3, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 84
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v0, v4, v4}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {p2, v3, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 88
    if-eqz p1, :cond_3

    if-ltz p4, :cond_3

    array-length v0, p1

    if-ge p4, v0, :cond_3

    .line 89
    aget-object v0, p1, p4

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->setAlpha(F)V

    .line 93
    :cond_3
    sget-object v0, Landroid/view/View;->SCALE_X:Landroid/util/Property;

    new-array v1, v7, [F

    fill-array-data v1, :array_0

    invoke-static {v3, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 94
    sget-object v1, Landroid/view/View;->SCALE_Y:Landroid/util/Property;

    new-array v2, v7, [F

    fill-array-data v2, :array_1

    invoke-static {v3, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    .line 95
    sget-object v1, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v4, v7, [F

    fill-array-data v4, :array_2

    invoke-static {v3, v1, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v4

    .line 97
    new-instance v1, Landroid/animation/AnimatorSet;

    invoke-direct {v1}, Landroid/animation/AnimatorSet;-><init>()V

    .line 98
    const/4 v5, 0x3

    new-array v5, v5, [Landroid/animation/Animator;

    const/4 v6, 0x0

    aput-object v0, v5, v6

    const/4 v0, 0x1

    aput-object v2, v5, v0

    aput-object v4, v5, v7

    invoke-virtual {v1, v5}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 100
    const-wide/16 v4, 0x12c

    invoke-virtual {v1, v4, v5}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 101
    sget-object v0, Lorg/chromium/ui/interpolators/BakedBezierInterpolator;->TRANSFORM_FOLLOW_THROUGH_CURVE:Lorg/chromium/ui/interpolators/BakedBezierInterpolator;

    invoke-virtual {v1, v0}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 104
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackViewAnimation;->mDpToPx:F

    mul-float/2addr v0, v8

    .line 106
    invoke-virtual {v3, v8}, Landroid/view/ViewGroup;->setPivotY(F)V

    .line 107
    invoke-static {}, Lorg/chromium/ui/base/LocalizationUtils;->isLayoutRtl()Z

    move-result v2

    if-eqz v2, :cond_4

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackViewAnimation;->mWidthDp:F

    iget v4, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackViewAnimation;->mDpToPx:F

    mul-float/2addr v2, v4

    sub-float v0, v2, v0

    :cond_4
    invoke-virtual {v3, v0}, Landroid/view/ViewGroup;->setPivotX(F)V

    move-object v0, v1

    .line 108
    goto/16 :goto_0

    .line 93
    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data

    .line 94
    :array_1
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data

    .line 95
    :array_2
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method


# virtual methods
.method public createAnimatorSetForType(Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;Landroid/view/ViewGroup;Lorg/chromium/chrome/browser/tabmodel/TabModel;I)Landroid/animation/AnimatorSet;
    .locals 3

    .prologue
    .line 56
    const/4 v0, 0x0

    .line 58
    if-eqz p4, :cond_0

    .line 59
    sget-object v1, Lcom/google/android/apps/chrome/tabs/layout/phone/StackViewAnimation$1;->$SwitchMap$com$google$android$apps$chrome$tabs$layout$phone$StackAnimation$OverviewAnimationType:[I

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation$OverviewAnimationType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 62
    :cond_0
    :goto_0
    return-object v0

    .line 61
    :pswitch_0
    invoke-direct {p0, p2, p3, p4, p5}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackViewAnimation;->createNewTabOpenedAnimatorSet([Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;Landroid/view/ViewGroup;Lorg/chromium/chrome/browser/tabmodel/TabModel;I)Landroid/animation/AnimatorSet;

    move-result-object v0

    goto :goto_0

    .line 59
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
