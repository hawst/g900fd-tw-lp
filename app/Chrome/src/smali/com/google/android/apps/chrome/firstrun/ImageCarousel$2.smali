.class final Lcom/google/android/apps/chrome/firstrun/ImageCarousel$2;
.super Landroid/util/Property;
.source "ImageCarousel.java"


# direct methods
.method constructor <init>(Ljava/lang/Class;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 117
    invoke-direct {p0, p1, p2}, Landroid/util/Property;-><init>(Ljava/lang/Class;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final get(Lcom/google/android/apps/chrome/firstrun/ImageCarousel;)Ljava/lang/Float;
    .locals 3

    .prologue
    .line 120
    # getter for: Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->mViews:[Landroid/widget/ImageView;
    invoke-static {p1}, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->access$200(Lcom/google/android/apps/chrome/firstrun/ImageCarousel;)[Landroid/widget/ImageView;

    move-result-object v0

    const/4 v1, 0x4

    const/4 v2, 0x1

    invoke-virtual {p1, v1, v2}, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->getChildDrawingOrder(II)I

    move-result v1

    aget-object v0, v0, v1

    invoke-virtual {v0}, Landroid/widget/ImageView;->getAlpha()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 117
    check-cast p1, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/firstrun/ImageCarousel$2;->get(Lcom/google/android/apps/chrome/firstrun/ImageCarousel;)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public final set(Lcom/google/android/apps/chrome/firstrun/ImageCarousel;Ljava/lang/Float;)V
    .locals 3

    .prologue
    const/4 v2, 0x4

    .line 125
    # getter for: Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->mViews:[Landroid/widget/ImageView;
    invoke-static {p1}, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->access$200(Lcom/google/android/apps/chrome/firstrun/ImageCarousel;)[Landroid/widget/ImageView;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p1, v2, v1}, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->getChildDrawingOrder(II)I

    move-result v1

    aget-object v0, v0, v1

    invoke-virtual {p2}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 126
    # getter for: Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->mViews:[Landroid/widget/ImageView;
    invoke-static {p1}, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->access$200(Lcom/google/android/apps/chrome/firstrun/ImageCarousel;)[Landroid/widget/ImageView;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {p1, v2, v1}, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->getChildDrawingOrder(II)I

    move-result v1

    aget-object v0, v0, v1

    invoke-virtual {p2}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 127
    return-void
.end method

.method public final bridge synthetic set(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 117
    check-cast p1, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;

    check-cast p2, Ljava/lang/Float;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/chrome/firstrun/ImageCarousel$2;->set(Lcom/google/android/apps/chrome/firstrun/ImageCarousel;Ljava/lang/Float;)V

    return-void
.end method
