.class Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog$AnimatorRunner;
.super Landroid/animation/AnimatorListenerAdapter;
.source "EnhancedBookmarkDetailDialog.java"


# instance fields
.field private mLastAnimator:Landroid/animation/Animator;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 388
    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog$1;)V
    .locals 0

    .prologue
    .line 388
    invoke-direct {p0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog$AnimatorRunner;-><init>()V

    return-void
.end method


# virtual methods
.method public endOldAnimation()V
    .locals 1

    .prologue
    .line 409
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog$AnimatorRunner;->mLastAnimator:Landroid/animation/Animator;

    if-eqz v0, :cond_0

    .line 410
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog$AnimatorRunner;->mLastAnimator:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->end()V

    .line 412
    :cond_0
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 1

    .prologue
    .line 393
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog$AnimatorRunner;->mLastAnimator:Landroid/animation/Animator;

    .line 394
    return-void
.end method

.method public run(Ljava/util/List;ILandroid/animation/Animator$AnimatorListener;)V
    .locals 4

    .prologue
    .line 397
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog$AnimatorRunner;->endOldAnimation()V

    .line 398
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    .line 399
    int-to-long v2, p2

    invoke-virtual {v0, v2, v3}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 400
    sget-object v1, Lorg/chromium/ui/interpolators/BakedBezierInterpolator;->TRANSFORM_CURVE:Lorg/chromium/ui/interpolators/BakedBezierInterpolator;

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 401
    invoke-virtual {v0, p1}, Landroid/animation/AnimatorSet;->playTogether(Ljava/util/Collection;)V

    .line 402
    invoke-virtual {v0, p3}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 403
    invoke-virtual {v0, p0}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 404
    iput-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog$AnimatorRunner;->mLastAnimator:Landroid/animation/Animator;

    .line 405
    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    .line 406
    return-void
.end method
