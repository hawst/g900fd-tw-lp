.class Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$1;
.super Landroid/util/Property;
.source "ToolbarPhone.java"


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;Ljava/lang/Class;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 167
    iput-object p1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$1;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    invoke-direct {p0, p2, p3}, Landroid/util/Property;-><init>(Ljava/lang/Class;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public get(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Ljava/lang/Float;
    .locals 1

    .prologue
    .line 170
    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUrlFocusChangePercent:F
    invoke-static {p1}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$000(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 167
    check-cast p1, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$1;->get(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public set(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;Ljava/lang/Float;)V
    .locals 2

    .prologue
    .line 175
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$1;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    invoke-virtual {p2}, Ljava/lang/Float;->floatValue()F

    move-result v1

    # invokes: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->setUrlFocusChangePercent(F)V
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$100(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;F)V

    .line 176
    return-void
.end method

.method public bridge synthetic set(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 167
    check-cast p1, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    check-cast p2, Ljava/lang/Float;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$1;->set(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;Ljava/lang/Float;)V

    return-void
.end method
