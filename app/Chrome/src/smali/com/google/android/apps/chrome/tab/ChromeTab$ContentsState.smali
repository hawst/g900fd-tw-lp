.class Lcom/google/android/apps/chrome/tab/ChromeTab$ContentsState;
.super Ljava/lang/Object;
.source "ChromeTab.java"


# instance fields
.field public final mBuffer:Ljava/nio/ByteBuffer;

.field private mVersion:I


# direct methods
.method public constructor <init>(Ljava/nio/ByteBuffer;)V
    .locals 0

    .prologue
    .line 232
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 233
    iput-object p1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$ContentsState;->mBuffer:Ljava/nio/ByteBuffer;

    .line 234
    return-void
.end method


# virtual methods
.method public buffer()Ljava/nio/ByteBuffer;
    .locals 1

    .prologue
    .line 237
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$ContentsState;->mBuffer:Ljava/nio/ByteBuffer;

    return-object v0
.end method

.method public setVersion(I)V
    .locals 0

    .prologue
    .line 245
    iput p1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$ContentsState;->mVersion:I

    .line 246
    return-void
.end method

.method public version()I
    .locals 1

    .prologue
    .line 241
    iget v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$ContentsState;->mVersion:I

    return v0
.end method
