.class public Lcom/google/android/apps/chrome/document/ChromeLauncherActivity;
.super Landroid/app/Activity;
.source "ChromeLauncherActivity.java"

# interfaces
.implements Lcom/google/android/apps/chrome/IntentHandler$IntentHandlerDelegate;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field static final ACTION_CLOSE_ALL_INCOGNITO:Ljava/lang/String; = "com.google.android.apps.chrome.document.CLOSE_ALL_INCOGNITO"

.field public static final LAUNCH_MODE_AFFILIATED:I = 0x1

.field public static final LAUNCH_MODE_FOREGROUND:I = 0x0

.field public static final LAUNCH_MODE_RETARGET:I = 0x2


# instance fields
.field private mIntentHandler:Lcom/google/android/apps/chrome/IntentHandler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 52
    const-class v0, Lcom/google/android/apps/chrome/document/ChromeLauncherActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/document/ChromeLauncherActivity;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method static synthetic access$000(ZLjava/lang/String;)Z
    .locals 1

    .prologue
    .line 52
    invoke-static {p0, p1}, Lcom/google/android/apps/chrome/document/ChromeLauncherActivity;->relaunchTask(ZLjava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private cleanUpChromeRecents(Z)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 511
    const-string/jumbo v0, "activity"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/document/ChromeLauncherActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 512
    invoke-virtual {v0}, Landroid/app/ActivityManager;->getAppTasks()Ljava/util/List;

    move-result-object v4

    .line 513
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/ChromeLauncherActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    move v1, v2

    .line 514
    :goto_0
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 515
    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager$AppTask;

    .line 516
    invoke-static {v0, v5}, Lcom/google/android/apps/chrome/document/ChromeLauncherActivity;->getTaskClassName(Landroid/app/ActivityManager$AppTask;Landroid/content/pm/PackageManager;)Ljava/lang/String;

    move-result-object v3

    .line 517
    if-eqz v3, :cond_0

    .line 520
    if-nez p1, :cond_1

    invoke-static {v3}, Lcom/google/android/apps/chrome/document/ChromeLauncherActivity;->isADocumentActivity(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v3, 0x1

    .line 521
    :goto_1
    if-nez v3, :cond_0

    invoke-virtual {v0}, Landroid/app/ActivityManager$AppTask;->getTaskInfo()Landroid/app/ActivityManager$RecentTaskInfo;

    move-result-object v0

    iget v0, v0, Landroid/app/ActivityManager$RecentTaskInfo;->id:I

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/ChromeLauncherActivity;->getTaskId()I

    move-result v3

    if-eq v0, v3, :cond_0

    .line 522
    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager$AppTask;

    invoke-virtual {v0}, Landroid/app/ActivityManager$AppTask;->finishAndRemoveTask()V

    .line 514
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    move v3, v2

    .line 520
    goto :goto_1

    .line 525
    :cond_2
    return-void
.end method

.method static closeAllIncognitoTabs(Landroid/content/Context;)V
    .locals 5

    .prologue
    .line 270
    const-string/jumbo v0, "activity"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 271
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 272
    invoke-virtual {v0}, Landroid/app/ActivityManager;->getAppTasks()Ljava/util/List;

    move-result-object v3

    .line 274
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 275
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager$AppTask;

    invoke-static {v0, v2}, Lcom/google/android/apps/chrome/document/ChromeLauncherActivity;->getTaskClassName(Landroid/app/ActivityManager$AppTask;Landroid/content/pm/PackageManager;)Ljava/lang/String;

    move-result-object v0

    .line 276
    if-eqz v0, :cond_0

    .line 277
    const-class v4, Lcom/google/android/apps/chrome/document/IncognitoDocumentActivity;

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 278
    if-eqz v0, :cond_0

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager$AppTask;

    invoke-virtual {v0}, Landroid/app/ActivityManager$AppTask;->finishAndRemoveTask()V

    .line 274
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 280
    :cond_1
    invoke-static {p0}, Lcom/google/android/apps/chrome/document/IncognitoDocumentActivity;->dismissIncognitoNotification(Landroid/content/Context;)V

    .line 281
    return-void
.end method

.method private static createLaunchIntent(Landroid/content/Context;Landroid/content/Intent;Ljava/lang/String;ZI)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 412
    invoke-static {}, Lcom/google/android/apps/chrome/document/DocumentActivityDatabase;->generateValidTabId()I

    move-result v1

    .line 415
    if-nez p1, :cond_1

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 416
    :goto_0
    const-string/jumbo v2, "android.intent.action.VIEW"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 417
    invoke-static {p3}, Lcom/google/android/apps/chrome/document/ChromeLauncherActivity;->getDocumentClassName(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, p0, v2}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    .line 418
    invoke-static {v1, p2}, Lcom/google/android/apps/chrome/document/DocumentActivityDatabase;->createDocumentDataString(ILjava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 419
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 420
    const-string/jumbo v1, "com.android.chrome.parent_tab_id"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 421
    if-eqz p1, :cond_0

    const-string/jumbo v1, "android.intent.action.VIEW"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 422
    const-string/jumbo v1, "com.android.chrome.original_intent"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 425
    :cond_0
    invoke-static {v0}, Lcom/google/android/apps/chrome/IntentHandler;->getTabIdFromIntent(Landroid/content/Intent;)I

    move-result v1

    .line 426
    invoke-static {p3}, Lcom/google/android/apps/chrome/document/DocumentActivityDatabase;->getTabList(Z)Lcom/google/android/apps/chrome/document/DocumentTabList;

    move-result-object v2

    invoke-virtual {v2, v1, p4}, Lcom/google/android/apps/chrome/document/DocumentTabList;->recordDocumentLaunching(II)V

    .line 428
    return-object v0

    .line 415
    :cond_1
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method static getDocumentClassName(Z)Ljava/lang/String;
    .locals 1

    .prologue
    .line 405
    if-eqz p0, :cond_0

    const-class v0, Lcom/google/android/apps/chrome/document/IncognitoDocumentActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-class v0, Lcom/google/android/apps/chrome/document/DocumentActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static getRemoveAllIncognitoTabsIntent(Landroid/content/Context;)Landroid/app/PendingIntent;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 399
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "com.google.android.apps.chrome.document.CLOSE_ALL_INCOGNITO"

    const/4 v2, 0x0

    const-class v3, Lcom/google/android/apps/chrome/document/ChromeLauncherActivity;

    invoke-direct {v0, v1, v2, p0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 401
    invoke-static {p0, v4, v0, v4}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method public static getTaskClassName(Landroid/app/ActivityManager$AppTask;Landroid/content/pm/PackageManager;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 234
    invoke-virtual {p0}, Landroid/app/ActivityManager$AppTask;->getTaskInfo()Landroid/app/ActivityManager$RecentTaskInfo;

    move-result-object v0

    iget-object v0, v0, Landroid/app/ActivityManager$RecentTaskInfo;->baseIntent:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 235
    invoke-virtual {p0}, Landroid/app/ActivityManager$AppTask;->getTaskInfo()Landroid/app/ActivityManager$RecentTaskInfo;

    move-result-object v0

    iget-object v0, v0, Landroid/app/ActivityManager$RecentTaskInfo;->baseIntent:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v0

    .line 239
    :goto_0
    return-object v0

    .line 237
    :cond_0
    invoke-virtual {p0}, Landroid/app/ActivityManager$AppTask;->getTaskInfo()Landroid/app/ActivityManager$RecentTaskInfo;

    move-result-object v0

    iget-object v0, v0, Landroid/app/ActivityManager$RecentTaskInfo;->baseIntent:Landroid/content/Intent;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v0

    .line 238
    if-nez v0, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 239
    :cond_1
    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    goto :goto_0
.end method

.method private handleIntent()V
    .locals 7

    .prologue
    const/4 v6, -0x1

    const/4 v1, 0x0

    .line 163
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/ChromeLauncherActivity;->getTaskId()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Lorg/chromium/chrome/browser/Tab;->incrementIdCounterTo(I)V

    .line 165
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/ChromeLauncherActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v2, "com.google.android.apps.chrome.EXTRA_OPEN_NEW_INCOGNITO_TAB"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    .line 167
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/ChromeLauncherActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/IntentHandler;->getUrlFromIntent(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v0

    .line 169
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/ChromeLauncherActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    if-nez v3, :cond_1

    .line 223
    :cond_0
    :goto_0
    return-void

    .line 171
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/ChromeLauncherActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "com.google.android.apps.chrome.document.CLOSE_ALL_INCOGNITO"

    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 172
    invoke-static {p0}, Lcom/google/android/apps/chrome/document/ChromeLauncherActivity;->closeAllIncognitoTabs(Landroid/content/Context;)V

    goto :goto_0

    .line 176
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/ChromeLauncherActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "android.intent.action.MAIN"

    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 177
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/ChromeLauncherActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/chrome/IntentHandler$TabOpenType;->BRING_TAB_TO_FRONT:Lcom/google/android/apps/chrome/IntentHandler$TabOpenType;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/IntentHandler$TabOpenType;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 179
    if-eq v0, v6, :cond_3

    invoke-static {v0}, Lcom/google/android/apps/chrome/document/ChromeLauncherActivity;->relaunchTask(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 180
    :cond_3
    invoke-direct {p0}, Lcom/google/android/apps/chrome/document/ChromeLauncherActivity;->launchLastViewedActivity()Z

    move-result v0

    if-nez v0, :cond_0

    .line 181
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/google/android/apps/chrome/document/ChromeLauncherActivity;->launchDefaultPage(Landroid/app/Activity;I)V

    goto :goto_0

    .line 185
    :cond_4
    iget-object v3, p0, Lcom/google/android/apps/chrome/document/ChromeLauncherActivity;->mIntentHandler:Lcom/google/android/apps/chrome/IntentHandler;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/ChromeLauncherActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {v3, p0, v4}, Lcom/google/android/apps/chrome/IntentHandler;->shouldIgnoreIntent(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 186
    const-string/jumbo v0, "ChromeLauncherActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Ignoring intent: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/ChromeLauncherActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 188
    :cond_5
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/ChromeLauncherActivity;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/ChromeLauncherActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string/jumbo v5, "com.android.browser.application_id"

    invoke-virtual {v4, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 191
    new-instance v3, Lcom/google/android/apps/chrome/document/DocumentActivityDatabase$PendingDocumentData;

    invoke-direct {v3}, Lcom/google/android/apps/chrome/document/DocumentActivityDatabase$PendingDocumentData;-><init>()V

    .line 192
    iput-object v0, v3, Lcom/google/android/apps/chrome/document/DocumentActivityDatabase$PendingDocumentData;->url:Ljava/lang/String;

    .line 193
    new-instance v4, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/ChromeLauncherActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    invoke-direct {v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    iput-object v4, v3, Lcom/google/android/apps/chrome/document/DocumentActivityDatabase$PendingDocumentData;->originalIntent:Landroid/content/Intent;

    .line 194
    sget v4, Lcom/google/android/apps/chrome/document/DocumentActivity;->sLastUsedTabId:I

    .line 195
    if-eq v4, v6, :cond_6

    .line 196
    invoke-static {v4, v3}, Lcom/google/android/apps/chrome/document/DocumentActivityDatabase;->addPendingDocumentData(ILcom/google/android/apps/chrome/document/DocumentActivityDatabase$PendingDocumentData;)V

    .line 197
    invoke-static {v4}, Lcom/google/android/apps/chrome/document/ChromeLauncherActivity;->relaunchTask(I)Z

    move-result v3

    if-nez v3, :cond_0

    .line 199
    invoke-static {v4}, Lcom/google/android/apps/chrome/document/DocumentActivityDatabase;->removePendingDocumentData(I)Lcom/google/android/apps/chrome/document/DocumentActivityDatabase$PendingDocumentData;

    .line 204
    :cond_6
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/ChromeLauncherActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string/jumbo v4, "com.android.chrome.append_task"

    invoke-virtual {v3, v4, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    .line 205
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/ChromeLauncherActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string/jumbo v5, "REUSE_URL_MATCHING_TAB_ELSE_NEW_TAB"

    invoke-virtual {v4, v5, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    .line 209
    if-eqz v4, :cond_7

    if-nez v3, :cond_7

    invoke-static {v2, v0}, Lcom/google/android/apps/chrome/document/ChromeLauncherActivity;->relaunchTask(ZLjava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 213
    :cond_7
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/ChromeLauncherActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/ChromeLauncherActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    invoke-static {v4, v5, v0, v2, v6}, Lcom/google/android/apps/chrome/document/ChromeLauncherActivity;->createLaunchIntent(Landroid/content/Context;Landroid/content/Intent;Ljava/lang/String;ZI)Landroid/content/Intent;

    move-result-object v4

    .line 215
    if-eqz v3, :cond_9

    move v0, v1

    :goto_1
    invoke-static {v4, v0, v2}, Lcom/google/android/apps/chrome/document/ChromeLauncherActivity;->setRecentsFlagsOnIntent(Landroid/content/Intent;IZ)V

    .line 218
    const/4 v0, 0x0

    .line 219
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/ChromeLauncherActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v3, "com.android.chrome.open_with_affiliation"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 220
    invoke-static {}, Landroid/app/ActivityOptions;->makeTaskLaunchBehind()Landroid/app/ActivityOptions;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/base/ApiCompatibilityUtils;->toBundle(Landroid/app/ActivityOptions;)Landroid/os/Bundle;

    move-result-object v0

    .line 222
    :cond_8
    invoke-static {p0, v4, v0}, Lorg/chromium/base/ApiCompatibilityUtils;->startActivity(Landroid/content/Context;Landroid/content/Intent;Landroid/os/Bundle;)V

    goto/16 :goto_0

    .line 215
    :cond_9
    const/high16 v0, 0x80000

    goto :goto_1
.end method

.method public static isADocumentActivity(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 501
    const-class v0, Lcom/google/android/apps/chrome/document/IncognitoDocumentActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-class v0, Lcom/google/android/apps/chrome/document/DocumentActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static launchDefaultPage(Landroid/app/Activity;I)V
    .locals 4

    .prologue
    .line 354
    new-instance v0, Lcom/google/android/apps/chrome/document/ChromeLauncherActivity$1;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/chrome/document/ChromeLauncherActivity$1;-><init>(Landroid/app/Activity;I)V

    const-wide/16 v2, 0x1f4

    invoke-static {v0, v2, v3}, Lcom/google/android/apps/chrome/partnercustomizations/PartnerBrowserCustomizations;->setOnInitializeAsyncFinished(Ljava/lang/Runnable;J)V

    .line 364
    return-void
.end method

.method public static launchInstance(Landroid/app/Activity;ZILjava/lang/String;IIZLcom/google/android/apps/chrome/document/DocumentActivityDatabase$PendingDocumentData;)V
    .locals 8

    .prologue
    .line 305
    const/4 v0, 0x2

    if-ne p2, v0, :cond_0

    invoke-static {p1, p3}, Lcom/google/android/apps/chrome/document/ChromeLauncherActivity;->relaunchTask(ZLjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 342
    :goto_0
    return-void

    .line 310
    :cond_0
    if-eqz p0, :cond_3

    const/4 v0, 0x1

    if-eq p2, v0, :cond_1

    const/16 v0, 0xc8

    if-eq p4, v0, :cond_1

    const/16 v0, 0x1f4

    if-ne p4, v0, :cond_3

    :cond_1
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/IntentHandler;->getTabIdFromIntent(Landroid/content/Intent;)I

    move-result v0

    .line 317
    :goto_1
    invoke-static {}, Lorg/chromium/base/ApplicationStatus;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 318
    const/4 v2, 0x0

    invoke-static {v1, v2, p3, p1, v0}, Lcom/google/android/apps/chrome/document/ChromeLauncherActivity;->createLaunchIntent(Landroid/content/Context;Landroid/content/Intent;Ljava/lang/String;ZI)Landroid/content/Intent;

    move-result-object v2

    .line 319
    const/high16 v0, 0x80000

    invoke-static {v2, v0, p1}, Lcom/google/android/apps/chrome/document/ChromeLauncherActivity;->setRecentsFlagsOnIntent(Landroid/content/Intent;IZ)V

    .line 320
    const-string/jumbo v0, "com.google.android.apps.chrome.EXTRA_OPEN_NEW_INCOGNITO_TAB"

    invoke-virtual {v2, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 321
    const-string/jumbo v0, "com.google.chrome.transition_type"

    invoke-virtual {v2, v0, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 322
    const-string/jumbo v0, "com.android.chrome.started_by"

    invoke-virtual {v2, v0, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 323
    const-string/jumbo v0, "com.android.chrome.use_desktop_user_agent"

    invoke-virtual {v2, v0, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 324
    invoke-static {v2, v1}, Lcom/google/android/apps/chrome/IntentHandler;->addTrustedIntentExtras(Landroid/content/Intent;Landroid/content/Context;)V

    .line 326
    const/4 v0, 0x0

    .line 328
    if-eqz p7, :cond_2

    .line 329
    invoke-static {v2}, Lcom/google/android/apps/chrome/IntentHandler;->getTabIdFromIntent(Landroid/content/Intent;)I

    move-result v0

    .line 330
    invoke-static {v0, p7}, Lcom/google/android/apps/chrome/document/DocumentActivityDatabase;->addPendingDocumentData(ILcom/google/android/apps/chrome/document/DocumentActivityDatabase$PendingDocumentData;)V

    .line 331
    iget-wide v4, p7, Lcom/google/android/apps/chrome/document/DocumentActivityDatabase$PendingDocumentData;->nativeWebContents:J

    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    .line 334
    :cond_2
    :goto_2
    const/4 v3, 0x1

    if-ne p2, v3, :cond_5

    if-nez v0, :cond_5

    invoke-static {}, Landroid/app/ActivityOptions;->makeTaskLaunchBehind()Landroid/app/ActivityOptions;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/base/ApiCompatibilityUtils;->toBundle(Landroid/app/ActivityOptions;)Landroid/os/Bundle;

    move-result-object v0

    .line 336
    :goto_3
    if-nez p0, :cond_6

    .line 337
    const/high16 v3, 0x10000000

    invoke-virtual {v2, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 338
    invoke-static {v1, v2, v0}, Lorg/chromium/base/ApiCompatibilityUtils;->startActivity(Landroid/content/Context;Landroid/content/Intent;Landroid/os/Bundle;)V

    goto :goto_0

    .line 310
    :cond_3
    const/4 v0, -0x1

    goto :goto_1

    .line 331
    :cond_4
    const/4 v0, 0x0

    goto :goto_2

    .line 334
    :cond_5
    const/4 v0, 0x0

    goto :goto_3

    .line 340
    :cond_6
    invoke-static {p0, v2, v0}, Lorg/chromium/base/ApiCompatibilityUtils;->startActivity(Landroid/content/Context;Landroid/content/Intent;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method private launchLastViewedActivity()Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v3, -0x1

    .line 246
    sget v0, Lcom/google/android/apps/chrome/document/DocumentActivity;->sLastUsedTabId:I

    if-eq v0, v3, :cond_0

    .line 248
    sget v0, Lcom/google/android/apps/chrome/document/DocumentActivity;->sLastUsedTabId:I

    .line 254
    :goto_0
    if-eq v0, v3, :cond_1

    invoke-static {v0}, Lcom/google/android/apps/chrome/document/ChromeLauncherActivity;->relaunchTask(I)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    .line 265
    :goto_1
    return v0

    .line 251
    :cond_0
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 252
    const-string/jumbo v2, "pref_last_used_tab_id"

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    goto :goto_0

    .line 257
    :cond_1
    const-string/jumbo v0, "activity"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/document/ChromeLauncherActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 258
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/ChromeLauncherActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 259
    invoke-virtual {v0}, Landroid/app/ActivityManager;->getAppTasks()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager$AppTask;

    .line 260
    invoke-static {v0, v2}, Lcom/google/android/apps/chrome/document/ChromeLauncherActivity;->getTaskClassName(Landroid/app/ActivityManager$AppTask;Landroid/content/pm/PackageManager;)Ljava/lang/String;

    move-result-object v4

    .line 261
    if-eqz v4, :cond_2

    invoke-static {v4}, Lcom/google/android/apps/chrome/document/ChromeLauncherActivity;->isADocumentActivity(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 262
    invoke-virtual {v0}, Landroid/app/ActivityManager$AppTask;->moveToFront()V

    move v0, v1

    .line 263
    goto :goto_1

    .line 265
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method static launchNtp(Landroid/app/Activity;ZI)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 375
    if-eqz p1, :cond_1

    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->isIncognitoModeEnabled()Z

    move-result v0

    if-nez v0, :cond_1

    .line 379
    :cond_0
    :goto_0
    return-void

    .line 376
    :cond_1
    const-string/jumbo v0, "chrome-native://newtab/"

    invoke-static {p1, v0}, Lcom/google/android/apps/chrome/document/ChromeLauncherActivity;->relaunchTask(ZLjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 377
    const-string/jumbo v3, "chrome-native://newtab/"

    const/4 v5, 0x6

    const/4 v7, 0x0

    move-object v0, p0

    move v1, p1

    move v4, p2

    move v6, v2

    invoke-static/range {v0 .. v7}, Lcom/google/android/apps/chrome/document/ChromeLauncherActivity;->launchInstance(Landroid/app/Activity;ZILjava/lang/String;IIZLcom/google/android/apps/chrome/document/DocumentActivityDatabase$PendingDocumentData;)V

    goto :goto_0
.end method

.method static launchPreferences(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 386
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/chrome/preferences/Preferences;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 387
    const/high16 v1, 0x20020000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 389
    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 390
    return-void
.end method

.method private launchTabbedMode()V
    .locals 3

    .prologue
    .line 432
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/ChromeLauncherActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    .line 433
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/ChromeLauncherActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const-class v2, Lcom/google/android/apps/chrome/ChromeTabbedActivity;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 435
    const/high16 v1, 0x14000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 436
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x15

    if-lt v1, v2, :cond_0

    .line 437
    const/16 v1, 0x2000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 439
    :cond_0
    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/document/ChromeLauncherActivity;->startActivity(Landroid/content/Intent;)V

    .line 440
    return-void
.end method

.method private maybePerformMigrationTasks()V
    .locals 2

    .prologue
    .line 139
    invoke-static {p0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->isOptOutCleanUpPending()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 140
    invoke-static {p0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->isOptedOutOfDocumentMode()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/document/ChromeLauncherActivity;->cleanUpChromeRecents(Z)V

    .line 142
    invoke-static {p0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->setOptOutCleanUpPending(Z)V

    .line 144
    :cond_0
    return-void
.end method

.method private static relaunchTask(I)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 449
    const/4 v0, -0x1

    if-ne p0, v0, :cond_0

    move v0, v1

    .line 462
    :goto_0
    return v0

    .line 451
    :cond_0
    invoke-static {}, Lorg/chromium/base/ApplicationStatus;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 452
    const-string/jumbo v2, "activity"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 454
    invoke-virtual {v0}, Landroid/app/ActivityManager;->getAppTasks()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager$AppTask;

    .line 455
    invoke-virtual {v0}, Landroid/app/ActivityManager$AppTask;->getTaskInfo()Landroid/app/ActivityManager$RecentTaskInfo;

    move-result-object v3

    iget-object v3, v3, Landroid/app/ActivityManager$RecentTaskInfo;->baseIntent:Landroid/content/Intent;

    invoke-static {v3}, Lcom/google/android/apps/chrome/IntentHandler;->getTabIdFromIntent(Landroid/content/Intent;)I

    move-result v3

    .line 456
    if-ne v3, p0, :cond_1

    .line 457
    invoke-static {v3}, Lcom/google/android/apps/chrome/document/DocumentActivityDatabase;->setPrioritizedTabId(I)V

    .line 458
    invoke-virtual {v0}, Landroid/app/ActivityManager$AppTask;->moveToFront()V

    .line 459
    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    .line 462
    goto :goto_0
.end method

.method private static relaunchTask(ZLjava/lang/String;)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 473
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 491
    :goto_0
    return v0

    .line 475
    :cond_0
    invoke-static {}, Lorg/chromium/base/ApplicationStatus;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 476
    const-string/jumbo v2, "activity"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 478
    invoke-virtual {v0}, Landroid/app/ActivityManager;->getAppTasks()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager$AppTask;

    .line 479
    invoke-virtual {v0}, Landroid/app/ActivityManager$AppTask;->getTaskInfo()Landroid/app/ActivityManager$RecentTaskInfo;

    move-result-object v3

    iget-object v3, v3, Landroid/app/ActivityManager$RecentTaskInfo;->baseIntent:Landroid/content/Intent;

    .line 480
    invoke-static {v3}, Lcom/google/android/apps/chrome/IntentHandler;->getUrlForDocument(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v4

    .line 481
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    invoke-static {v4, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 483
    invoke-static {v3}, Lcom/google/android/apps/chrome/IntentHandler;->getTabIdFromIntent(Landroid/content/Intent;)I

    move-result v3

    .line 484
    invoke-static {v3}, Lcom/google/android/apps/chrome/document/DocumentActivityDatabase;->setPrioritizedTabId(I)V

    .line 485
    invoke-static {p0}, Lcom/google/android/apps/chrome/document/DocumentActivityDatabase;->getTabList(Z)Lcom/google/android/apps/chrome/document/DocumentTabList;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/google/android/apps/chrome/document/DocumentTabList;->isRetargetable(I)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 487
    invoke-virtual {v0}, Landroid/app/ActivityManager$AppTask;->moveToFront()V

    .line 488
    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    .line 491
    goto :goto_0
.end method

.method private static setRecentsFlagsOnIntent(Landroid/content/Intent;IZ)V
    .locals 2

    .prologue
    .line 535
    invoke-virtual {p0}, Landroid/content/Intent;->getFlags()I

    move-result v0

    const v1, -0x800001

    and-int/2addr v0, v1

    invoke-virtual {p0, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 536
    if-nez p2, :cond_0

    const/16 v0, 0x2000

    invoke-virtual {p0, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 537
    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p0, p1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 538
    :cond_1
    return-void
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 115
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    .line 116
    const/16 v0, 0x65

    if-ne p1, v0, :cond_0

    .line 117
    const/4 v0, -0x1

    if-ne p2, v0, :cond_2

    .line 119
    invoke-static {p0}, Lcom/google/android/apps/chrome/utilities/FeatureUtilitiesInternal;->isDocumentMode(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 120
    invoke-direct {p0}, Lcom/google/android/apps/chrome/document/ChromeLauncherActivity;->handleIntent()V

    .line 121
    invoke-static {p0}, Lcom/google/android/apps/chrome/utilities/UnreleasedApiCompatibilityUtils;->finishAndRemoveTask(Landroid/app/Activity;)V

    .line 132
    :cond_0
    :goto_0
    return-void

    .line 123
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/chrome/document/ChromeLauncherActivity;->launchTabbedMode()V

    .line 124
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/ChromeLauncherActivity;->finish()V

    goto :goto_0

    .line 130
    :cond_2
    invoke-static {p0}, Lcom/google/android/apps/chrome/utilities/UnreleasedApiCompatibilityUtils;->finishAndRemoveTask(Landroid/app/Activity;)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 85
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 90
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/ChromeLauncherActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-wide/16 v2, 0x2710

    invoke-static {v0, v2, v3}, Lcom/google/android/apps/chrome/partnercustomizations/PartnerBrowserCustomizations;->initializeAsync(Landroid/content/Context;J)V

    .line 93
    new-instance v0, Lcom/google/android/apps/chrome/IntentHandler;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/ChromeLauncherActivity;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/chrome/IntentHandler;-><init>(Lcom/google/android/apps/chrome/IntentHandler$IntentHandlerDelegate;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/document/ChromeLauncherActivity;->mIntentHandler:Lcom/google/android/apps/chrome/IntentHandler;

    .line 94
    invoke-direct {p0}, Lcom/google/android/apps/chrome/document/ChromeLauncherActivity;->maybePerformMigrationTasks()V

    .line 96
    invoke-static {p0}, Lcom/google/android/apps/chrome/utilities/FeatureUtilitiesInternal;->isDocumentMode(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 97
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/ChromeLauncherActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/ChromeLauncherActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "android.intent.action.MAIN"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 99
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/ChromeLauncherActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-static {p0, v1, v0}, Lcom/google/android/apps/chrome/firstrun/FirstRunFlowSequencer;->checkIfFirstRunIsNecessary(Landroid/app/Activity;Landroid/content/Intent;Z)Landroid/content/Intent;

    move-result-object v0

    .line 101
    if-eqz v0, :cond_1

    .line 102
    const/16 v1, 0x65

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/chrome/document/ChromeLauncherActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 111
    :goto_1
    return-void

    .line 97
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 104
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/chrome/document/ChromeLauncherActivity;->handleIntent()V

    .line 105
    invoke-static {p0}, Lcom/google/android/apps/chrome/utilities/UnreleasedApiCompatibilityUtils;->finishAndRemoveTask(Landroid/app/Activity;)V

    goto :goto_1

    .line 108
    :cond_2
    invoke-direct {p0}, Lcom/google/android/apps/chrome/document/ChromeLauncherActivity;->launchTabbedMode()V

    .line 109
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/ChromeLauncherActivity;->finish()V

    goto :goto_1
.end method

.method public processUrlViewIntent(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/chrome/IntentHandler$TabOpenType;Ljava/lang/String;Ljava/lang/String;ILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 155
    sget-boolean v0, Lcom/google/android/apps/chrome/document/ChromeLauncherActivity;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 156
    :cond_0
    return-void
.end method

.method public processWebSearchIntent(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 148
    sget-boolean v0, Lcom/google/android/apps/chrome/document/ChromeLauncherActivity;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 149
    :cond_0
    return-void
.end method
