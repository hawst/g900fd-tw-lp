.class Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;
.super Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;
.source "ToolbarPhone.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private mIsOverlayTabStackDrawableLight:Z

.field private mOverlayDrawablesVisualState:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$VisualState;

.field private mPreTextureCaptureAlpha:F

.field private mToolbarBrandColor:I

.field private mUseLightToolbarDrawables:Z

.field private mVisibleNewTabPage:Lcom/google/android/apps/chrome/ntp/NewTabPage;

.field private mVisualState:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$VisualState;

.field final synthetic this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1103
    const-class v0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)V
    .locals 1

    .prologue
    .line 1114
    iput-object p1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    .line 1115
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;-><init>(Landroid/view/View;)V

    .line 1105
    sget-object v0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$VisualState;->NORMAL:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$VisualState;

    iput-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->mVisualState:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$VisualState;

    .line 1111
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->mPreTextureCaptureAlpha:F

    .line 1116
    return-void
.end method

.method static synthetic access$1600(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;Z)V
    .locals 0

    .prologue
    .line 1103
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->updateViewsForOverviewMode(Z)V

    return-void
.end method

.method static synthetic access$1700(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;Z)V
    .locals 0

    .prologue
    .line 1103
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->updateShadowVisibility(Z)V

    return-void
.end method

.method static synthetic access$4400(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;Z)V
    .locals 0

    .prologue
    .line 1103
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->updateVisualsForToolbarState(Z)V

    return-void
.end method

.method static synthetic access$4500(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;)V
    .locals 0

    .prologue
    .line 1103
    invoke-direct {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->updateNtpAnimationState()V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;)Z
    .locals 1

    .prologue
    .line 1103
    invoke-direct {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->isLocationBarShownInNTP()Z

    move-result v0

    return v0
.end method

.method private buildUrlScrollProperty(Landroid/view/View;Z)Landroid/util/Property;
    .locals 6

    .prologue
    .line 1428
    new-instance v0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone$4;

    const-class v2, Ljava/lang/Integer;

    const-string/jumbo v3, "scrollX"

    move-object v1, p0

    move-object v4, p1

    move v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone$4;-><init>(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;Ljava/lang/Class;Ljava/lang/String;Landroid/view/View;Z)V

    return-object v0
.end method

.method private createEnterOverviewModeAnimation()Landroid/animation/ObjectAnimator;
    .locals 5

    .prologue
    .line 1225
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mOverviewModePercentProperty:Landroid/util/Property;
    invoke-static {v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$1500(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Landroid/util/Property;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [F

    const/4 v3, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    aput v4, v2, v3

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 1227
    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 1228
    invoke-static {}, Lcom/google/android/apps/chrome/ChromeAnimation;->getLinearInterpolator()Landroid/view/animation/LinearInterpolator;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 1229
    new-instance v1, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone$1;-><init>(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 1239
    return-object v0
.end method

.method private createExitOverviewAnimation(Z)Landroid/animation/ObjectAnimator;
    .locals 5

    .prologue
    .line 1244
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mOverviewModePercentProperty:Landroid/util/Property;
    invoke-static {v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$1500(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Landroid/util/Property;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [F

    const/4 v3, 0x0

    const/4 v4, 0x0

    aput v4, v2, v3

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    .line 1246
    if-eqz p1, :cond_0

    const-wide/16 v0, 0xc8

    :goto_0
    invoke-virtual {v2, v0, v1}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 1249
    invoke-static {}, Lcom/google/android/apps/chrome/ChromeAnimation;->getLinearInterpolator()Landroid/view/animation/LinearInterpolator;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 1250
    new-instance v0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone$2;-><init>(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;)V

    invoke-virtual {v2, v0}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 1257
    return-object v2

    .line 1246
    :cond_0
    const-wide/16 v0, 0x64

    goto :goto_0
.end method

.method private createPostExitOverviewAnimation()Landroid/animation/ObjectAnimator;
    .locals 5

    .prologue
    .line 1261
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    sget-object v1, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    const/4 v2, 0x2

    new-array v2, v2, [F

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->getHeight()I

    move-result v4

    neg-int v4, v4

    int-to-float v4, v4

    aput v4, v2, v3

    const/4 v3, 0x1

    const/4 v4, 0x0

    aput v4, v2, v3

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 1263
    const-wide/16 v2, 0x64

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 1264
    sget-object v1, Lorg/chromium/ui/interpolators/BakedBezierInterpolator;->TRANSFORM_CURVE:Lorg/chromium/ui/interpolators/BakedBezierInterpolator;

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 1265
    new-instance v1, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone$3;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone$3;-><init>(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 1284
    return-object v0
.end method

.method private isLocationBarShownInNTP()Z
    .locals 1

    .prologue
    .line 1761
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->getToolbarDataProvider()Lcom/google/android/apps/chrome/toolbar/ToolbarDataProvider;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarDataProvider;->getNewTabPageForCurrentTab()Lcom/google/android/apps/chrome/ntp/NewTabPage;

    move-result-object v0

    .line 1762
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ntp/NewTabPage;->isLocationBarShownInNTP()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isOverviewAnimationRunning()Z
    .locals 1

    .prologue
    .line 1301
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUIAnimatingOverviewTransition:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$1900(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mOverviewModeAnimation:Landroid/animation/ObjectAnimator;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$1000(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mOverviewModeAnimation:Landroid/animation/ObjectAnimator;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$1000(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->isRunning()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mDelayedOverviewModeAnimation:Landroid/animation/ObjectAnimator;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$1300(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mDelayedOverviewModeAnimation:Landroid/animation/ObjectAnimator;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$1300(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private populateUrlClearFocusingAnimatorSet(Ljava/util/List;)V
    .locals 12

    .prologue
    const-wide/16 v10, 0xfa

    const/4 v7, 0x0

    const-wide/16 v8, 0x64

    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 1509
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    iget-object v2, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUrlFocusChangePercentProperty:Landroid/util/Property;
    invoke-static {v2}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$3200(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Landroid/util/Property;

    move-result-object v2

    new-array v3, v6, [F

    aput v7, v3, v1

    invoke-static {v0, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 1511
    invoke-virtual {v0, v10, v11}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 1512
    sget-object v2, Lorg/chromium/ui/interpolators/BakedBezierInterpolator;->TRANSFORM_CURVE:Lorg/chromium/ui/interpolators/BakedBezierInterpolator;

    invoke-virtual {v0, v2}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 1513
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1515
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->mMenuButton:Landroid/widget/ImageButton;

    sget-object v2, Landroid/view/View;->TRANSLATION_X:Landroid/util/Property;

    new-array v3, v6, [F

    aput v7, v3, v1

    invoke-static {v0, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 1516
    invoke-virtual {v0, v8, v9}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 1517
    invoke-virtual {v0, v10, v11}, Landroid/animation/Animator;->setStartDelay(J)V

    .line 1518
    sget-object v2, Lorg/chromium/ui/interpolators/BakedBezierInterpolator;->TRANSFORM_CURVE:Lorg/chromium/ui/interpolators/BakedBezierInterpolator;

    invoke-virtual {v0, v2}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 1519
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1521
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->mMenuButton:Landroid/widget/ImageButton;

    sget-object v2, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v3, v6, [F

    const/high16 v4, 0x3f800000    # 1.0f

    aput v4, v3, v1

    invoke-static {v0, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 1522
    invoke-virtual {v0, v8, v9}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 1523
    invoke-virtual {v0, v10, v11}, Landroid/animation/Animator;->setStartDelay(J)V

    .line 1524
    sget-object v2, Lorg/chromium/ui/interpolators/BakedBezierInterpolator;->TRANSFORM_CURVE:Lorg/chromium/ui/interpolators/BakedBezierInterpolator;

    invoke-virtual {v0, v2}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 1525
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1527
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mToggleTabStackButton:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$500(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Landroid/widget/ImageView;

    move-result-object v0

    sget-object v2, Landroid/view/View;->TRANSLATION_X:Landroid/util/Property;

    new-array v3, v6, [F

    aput v7, v3, v1

    invoke-static {v0, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 1528
    invoke-virtual {v0, v8, v9}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 1529
    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, Landroid/animation/Animator;->setStartDelay(J)V

    .line 1530
    sget-object v2, Lorg/chromium/ui/interpolators/BakedBezierInterpolator;->TRANSFORM_CURVE:Lorg/chromium/ui/interpolators/BakedBezierInterpolator;

    invoke-virtual {v0, v2}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 1531
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1533
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mToggleTabStackButton:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$500(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Landroid/widget/ImageView;

    move-result-object v0

    sget-object v2, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v3, v6, [F

    const/high16 v4, 0x3f800000    # 1.0f

    aput v4, v3, v1

    invoke-static {v0, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 1534
    invoke-virtual {v0, v8, v9}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 1535
    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, Landroid/animation/Animator;->setStartDelay(J)V

    .line 1536
    sget-object v2, Lorg/chromium/ui/interpolators/BakedBezierInterpolator;->TRANSFORM_CURVE:Lorg/chromium/ui/interpolators/BakedBezierInterpolator;

    invoke-virtual {v0, v2}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 1537
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1539
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mReaderModeButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$1400(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Landroid/widget/ImageButton;

    move-result-object v0

    sget-object v2, Landroid/view/View;->TRANSLATION_X:Landroid/util/Property;

    new-array v3, v6, [F

    aput v7, v3, v1

    invoke-static {v0, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 1540
    invoke-virtual {v0, v8, v9}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 1541
    const-wide/16 v2, 0x96

    invoke-virtual {v0, v2, v3}, Landroid/animation/Animator;->setStartDelay(J)V

    .line 1542
    sget-object v2, Lorg/chromium/ui/interpolators/BakedBezierInterpolator;->TRANSFORM_CURVE:Lorg/chromium/ui/interpolators/BakedBezierInterpolator;

    invoke-virtual {v0, v2}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 1543
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1545
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mReaderModeButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$1400(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Landroid/widget/ImageButton;

    move-result-object v0

    sget-object v2, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v3, v6, [F

    const/high16 v4, 0x3f800000    # 1.0f

    aput v4, v3, v1

    invoke-static {v0, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 1546
    invoke-virtual {v0, v8, v9}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 1547
    const-wide/16 v2, 0x96

    invoke-virtual {v0, v2, v3}, Landroid/animation/Animator;->setStartDelay(J)V

    .line 1548
    sget-object v2, Lorg/chromium/ui/interpolators/BakedBezierInterpolator;->TRANSFORM_CURVE:Lorg/chromium/ui/interpolators/BakedBezierInterpolator;

    invoke-virtual {v0, v2}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 1549
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v0, v1

    .line 1551
    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mPhoneLocationBar:Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;
    invoke-static {v2}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$3300(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->getChildCount()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 1552
    iget-object v2, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mPhoneLocationBar:Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;
    invoke-static {v2}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$3300(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 1553
    iget-object v3, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mPhoneLocationBar:Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;
    invoke-static {v3}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$3300(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->getFirstViewVisibleWhenFocused()Landroid/view/View;

    move-result-object v3

    if-eq v2, v3, :cond_0

    .line 1554
    sget-object v3, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v4, v6, [F

    const/high16 v5, 0x3f800000    # 1.0f

    aput v5, v4, v1

    invoke-static {v2, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    .line 1555
    invoke-virtual {v2, v8, v9}, Landroid/animation/Animator;->setStartDelay(J)V

    .line 1556
    invoke-virtual {v2, v10, v11}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 1557
    sget-object v3, Lorg/chromium/ui/interpolators/BakedBezierInterpolator;->TRANSFORM_CURVE:Lorg/chromium/ui/interpolators/BakedBezierInterpolator;

    invoke-virtual {v2, v3}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 1558
    invoke-interface {p1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1551
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1561
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->isLocationBarShownInNTP()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mNtpSearchBoxScrollPercent:F
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$3400(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)F

    move-result v0

    cmpl-float v0, v0, v7

    if-nez v0, :cond_2

    .line 1591
    :cond_1
    :goto_1
    return-void

    .line 1563
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/utilities/FeatureUtilitiesInternal;->isDocumentMode(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->getLocationBar()Lcom/google/android/apps/chrome/omnibox/LocationBar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->showingQueryInTheOmnibox()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1568
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mPhoneLocationBar:Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$3300(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/base/ApiCompatibilityUtils;->isLayoutRtl(Landroid/view/View;)Z

    move-result v2

    .line 1569
    if-eqz v2, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUrlBar:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$3100(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1571
    :cond_4
    if-eqz v2, :cond_5

    .line 1572
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUrlBar:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$3100(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/text/Layout;->getPrimaryHorizontal(I)F

    move-result v0

    float-to-int v0, v0

    .line 1573
    iget-object v3, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUrlBar:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$3100(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/TextView;->getWidth()I

    move-result v3

    sub-int/2addr v0, v3

    .line 1580
    :goto_2
    iget-object v3, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUrlBar:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$3100(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/TextView;->getScrollX()I

    move-result v3

    if-eq v3, v0, :cond_1

    .line 1581
    iget-object v3, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUrlBar:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$3100(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Landroid/widget/TextView;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mPhoneLocationBar:Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;
    invoke-static {v4}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$3300(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;

    move-result-object v4

    invoke-direct {p0, v4, v2}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->buildUrlScrollProperty(Landroid/view/View;Z)Landroid/util/Property;

    move-result-object v2

    new-array v4, v6, [I

    aput v0, v4, v1

    invoke-static {v3, v2, v4}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Landroid/util/Property;[I)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 1585
    invoke-virtual {v0, v10, v11}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 1586
    sget-object v1, Lorg/chromium/ui/interpolators/BakedBezierInterpolator;->TRANSFORM_CURVE:Lorg/chromium/ui/interpolators/BakedBezierInterpolator;

    invoke-virtual {v0, v1}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 1587
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_5
    move v0, v1

    goto :goto_2
.end method

.method private populateUrlFocusingAnimatorSet(Ljava/util/List;)V
    .locals 12

    .prologue
    const/16 v11, 0xa

    const/4 v10, 0x0

    const-wide/16 v8, 0x64

    const/4 v7, 0x1

    const/4 v1, 0x0

    .line 1456
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    iget-object v2, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUrlFocusChangePercentProperty:Landroid/util/Property;
    invoke-static {v2}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$3200(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Landroid/util/Property;

    move-result-object v2

    new-array v3, v7, [F

    const/high16 v4, 0x3f800000    # 1.0f

    aput v4, v3, v1

    invoke-static {v0, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 1458
    const-wide/16 v2, 0xfa

    invoke-virtual {v0, v2, v3}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 1459
    sget-object v2, Lorg/chromium/ui/interpolators/BakedBezierInterpolator;->TRANSFORM_CURVE:Lorg/chromium/ui/interpolators/BakedBezierInterpolator;

    invoke-virtual {v0, v2}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 1460
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v0, v1

    .line 1462
    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mPhoneLocationBar:Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;
    invoke-static {v2}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$3300(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->getChildCount()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 1463
    iget-object v2, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mPhoneLocationBar:Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;
    invoke-static {v2}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$3300(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 1464
    iget-object v3, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mPhoneLocationBar:Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;
    invoke-static {v3}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$3300(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->getFirstViewVisibleWhenFocused()Landroid/view/View;

    move-result-object v3

    if-eq v2, v3, :cond_0

    .line 1465
    sget-object v3, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v4, v7, [F

    aput v10, v4, v1

    invoke-static {v2, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    .line 1466
    const-wide/16 v4, 0xfa

    invoke-virtual {v2, v4, v5}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 1467
    sget-object v3, Lorg/chromium/ui/interpolators/BakedBezierInterpolator;->TRANSFORM_CURVE:Lorg/chromium/ui/interpolators/BakedBezierInterpolator;

    invoke-virtual {v2, v3}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 1468
    invoke-interface {p1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1462
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1471
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    .line 1472
    iget-object v2, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    invoke-static {v2}, Lorg/chromium/base/ApiCompatibilityUtils;->isLayoutRtl(Landroid/view/View;)Z

    move-result v2

    .line 1474
    iget-object v3, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->mMenuButton:Landroid/widget/ImageButton;

    sget-object v4, Landroid/view/View;->TRANSLATION_X:Landroid/util/Property;

    new-array v5, v7, [F

    invoke-static {v11, v2}, Lcom/google/android/apps/chrome/utilities/ChromeMathUtils;->flipSignIf(IZ)I

    move-result v6

    int-to-float v6, v6

    mul-float/2addr v6, v0

    aput v6, v5, v1

    invoke-static {v3, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    .line 1476
    invoke-virtual {v3, v8, v9}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 1477
    sget-object v4, Lorg/chromium/ui/interpolators/BakedBezierInterpolator;->FADE_OUT_CURVE:Lorg/chromium/ui/interpolators/BakedBezierInterpolator;

    invoke-virtual {v3, v4}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 1478
    invoke-interface {p1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1480
    iget-object v3, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->mMenuButton:Landroid/widget/ImageButton;

    sget-object v4, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v5, v7, [F

    aput v10, v5, v1

    invoke-static {v3, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    .line 1481
    invoke-virtual {v3, v8, v9}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 1482
    sget-object v4, Lorg/chromium/ui/interpolators/BakedBezierInterpolator;->FADE_OUT_CURVE:Lorg/chromium/ui/interpolators/BakedBezierInterpolator;

    invoke-virtual {v3, v4}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 1483
    invoke-interface {p1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1485
    iget-object v3, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mToggleTabStackButton:Landroid/widget/ImageView;
    invoke-static {v3}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$500(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Landroid/widget/ImageView;

    move-result-object v3

    sget-object v4, Landroid/view/View;->TRANSLATION_X:Landroid/util/Property;

    new-array v5, v7, [F

    invoke-static {v11, v2}, Lcom/google/android/apps/chrome/utilities/ChromeMathUtils;->flipSignIf(IZ)I

    move-result v6

    int-to-float v6, v6

    mul-float/2addr v6, v0

    aput v6, v5, v1

    invoke-static {v3, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    .line 1487
    invoke-virtual {v3, v8, v9}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 1488
    sget-object v4, Lorg/chromium/ui/interpolators/BakedBezierInterpolator;->FADE_OUT_CURVE:Lorg/chromium/ui/interpolators/BakedBezierInterpolator;

    invoke-virtual {v3, v4}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 1489
    invoke-interface {p1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1491
    iget-object v3, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mToggleTabStackButton:Landroid/widget/ImageView;
    invoke-static {v3}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$500(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Landroid/widget/ImageView;

    move-result-object v3

    sget-object v4, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v5, v7, [F

    aput v10, v5, v1

    invoke-static {v3, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    .line 1492
    invoke-virtual {v3, v8, v9}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 1493
    sget-object v4, Lorg/chromium/ui/interpolators/BakedBezierInterpolator;->FADE_OUT_CURVE:Lorg/chromium/ui/interpolators/BakedBezierInterpolator;

    invoke-virtual {v3, v4}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 1494
    invoke-interface {p1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1496
    iget-object v3, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mReaderModeButton:Landroid/widget/ImageButton;
    invoke-static {v3}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$1400(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Landroid/widget/ImageButton;

    move-result-object v3

    sget-object v4, Landroid/view/View;->TRANSLATION_X:Landroid/util/Property;

    new-array v5, v7, [F

    invoke-static {v11, v2}, Lcom/google/android/apps/chrome/utilities/ChromeMathUtils;->flipSignIf(IZ)I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v0, v2

    aput v0, v5, v1

    invoke-static {v3, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 1498
    invoke-virtual {v0, v8, v9}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 1499
    sget-object v2, Lorg/chromium/ui/interpolators/BakedBezierInterpolator;->FADE_OUT_CURVE:Lorg/chromium/ui/interpolators/BakedBezierInterpolator;

    invoke-virtual {v0, v2}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 1500
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1502
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mReaderModeButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$1400(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Landroid/widget/ImageButton;

    move-result-object v0

    sget-object v2, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v3, v7, [F

    aput v10, v3, v1

    invoke-static {v0, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 1503
    invoke-virtual {v0, v8, v9}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 1504
    sget-object v1, Lorg/chromium/ui/interpolators/BakedBezierInterpolator;->FADE_OUT_CURVE:Lorg/chromium/ui/interpolators/BakedBezierInterpolator;

    invoke-virtual {v0, v1}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 1505
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1506
    return-void
.end method

.method private updateNtpAnimationState()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1720
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # invokes: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->resetNtpAnimationValues()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$4300(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)V

    .line 1721
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->mVisibleNewTabPage:Lcom/google/android/apps/chrome/ntp/NewTabPage;

    if-eqz v0, :cond_0

    .line 1722
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->mVisibleNewTabPage:Lcom/google/android/apps/chrome/ntp/NewTabPage;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/ntp/NewTabPage;->setSearchBoxScrollListener(Lcom/google/android/apps/chrome/ntp/NewTabPage$OnSearchBoxScrollListener;)V

    .line 1723
    iput-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->mVisibleNewTabPage:Lcom/google/android/apps/chrome/ntp/NewTabPage;

    .line 1725
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->getToolbarDataProvider()Lcom/google/android/apps/chrome/toolbar/ToolbarDataProvider;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarDataProvider;->getNewTabPageForCurrentTab()Lcom/google/android/apps/chrome/ntp/NewTabPage;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->mVisibleNewTabPage:Lcom/google/android/apps/chrome/ntp/NewTabPage;

    .line 1726
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->mVisibleNewTabPage:Lcom/google/android/apps/chrome/ntp/NewTabPage;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->mVisibleNewTabPage:Lcom/google/android/apps/chrome/ntp/NewTabPage;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ntp/NewTabPage;->isLocationBarShownInNTP()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1727
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->mVisibleNewTabPage:Lcom/google/android/apps/chrome/ntp/NewTabPage;

    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/ntp/NewTabPage;->setSearchBoxScrollListener(Lcom/google/android/apps/chrome/ntp/NewTabPage$OnSearchBoxScrollListener;)V

    .line 1729
    :cond_1
    return-void
.end method

.method private updateOverlayDrawables()V
    .locals 4

    .prologue
    .line 1384
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->isNativeLibraryReady()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1412
    :cond_0
    :goto_0
    return-void

    .line 1386
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # invokes: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->isIncognitoMode()Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$2600(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Z

    move-result v2

    .line 1390
    sget-object v1, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$VisualState;->NORMAL:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$VisualState;

    .line 1391
    sget v0, Lcom/google/android/apps/chrome/R$color;->default_primary_color:I

    .line 1392
    if-eqz v2, :cond_3

    .line 1393
    sget-object v1, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$VisualState;->INCOGNITO:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$VisualState;

    .line 1394
    sget v0, Lcom/google/android/apps/chrome/R$color;->incognito_primary_color:I

    .line 1400
    :cond_2
    :goto_1
    iget-object v3, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->mOverlayDrawablesVisualState:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$VisualState;

    if-eq v3, v1, :cond_0

    .line 1401
    iput-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->mOverlayDrawablesVisualState:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$VisualState;

    .line 1403
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 1404
    iget-object v3, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    # setter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mOverviewAnimationBgOverlay:Landroid/graphics/drawable/Drawable;
    invoke-static {v3, v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$2702(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    .line 1406
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->shouldShowMenuButton()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1407
    iget-object v3, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    if-eqz v2, :cond_4

    sget v0, Lcom/google/android/apps/chrome/R$drawable;->btn_menu_white_normal:I

    :goto_2
    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    # setter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mOverviewAnimationMenuDrawable:Landroid/graphics/drawable/Drawable;
    invoke-static {v3, v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$2802(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    .line 1410
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mOverviewAnimationMenuDrawable:Landroid/graphics/drawable/Drawable;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$2800(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/BitmapDrawable;->setGravity(I)V

    goto :goto_0

    .line 1395
    :cond_3
    invoke-direct {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->isLocationBarShownInNTP()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1396
    sget-object v1, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$VisualState;->NEW_TAB_NORMAL:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$VisualState;

    .line 1397
    sget v0, Lcom/google/android/apps/chrome/R$drawable;->toolbar_normal:I

    goto :goto_1

    .line 1407
    :cond_4
    sget v0, Lcom/google/android/apps/chrome/R$drawable;->btn_menu_normal:I

    goto :goto_2
.end method

.method private updateShadowVisibility(Z)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1766
    if-nez p1, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->isOverviewAnimationRunning()Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x1

    .line 1767
    :goto_0
    if-eqz v1, :cond_2

    .line 1769
    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mToolbarShadow:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$3900(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ImageView;->getVisibility()I

    move-result v1

    if-eq v1, v0, :cond_0

    .line 1770
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mToolbarShadow:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$3900(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1772
    :cond_0
    return-void

    :cond_1
    move v1, v0

    .line 1766
    goto :goto_0

    .line 1767
    :cond_2
    const/4 v0, 0x4

    goto :goto_1
.end method

.method private updateViewsForOverviewMode(Z)V
    .locals 6

    .prologue
    const/4 v2, 0x4

    const/4 v3, 0x0

    .line 1308
    if-eqz p1, :cond_0

    move v4, v3

    .line 1309
    :goto_0
    if-eqz p1, :cond_1

    move v1, v2

    .line 1311
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mOverviewModeViews:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$2000(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 1312
    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    :cond_0
    move v4, v2

    .line 1308
    goto :goto_0

    :cond_1
    move v1, v3

    .line 1309
    goto :goto_1

    .line 1314
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mBrowsingModeViews:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$800(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 1315
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_3

    .line 1317
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mProgressBar:Lcom/google/android/apps/chrome/widget/SmoothProgressBar;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$1200(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Lcom/google/android/apps/chrome/widget/SmoothProgressBar;

    move-result-object v0

    if-nez p1, :cond_4

    invoke-direct {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->isOverviewAnimationRunning()Z

    move-result v1

    if-eqz v1, :cond_5

    :cond_4
    move v3, v2

    :cond_5
    invoke-virtual {v0, v3}, Lcom/google/android/apps/chrome/widget/SmoothProgressBar;->setVisibility(I)V

    .line 1319
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->updateVisualsForToolbarState(Z)V

    .line 1321
    return-void
.end method

.method private updateVisualsForToolbarState(Z)V
    .locals 11

    .prologue
    const/16 v5, 0x33

    const/16 v6, 0xff

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1775
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # invokes: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->isIncognitoMode()Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$2600(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Z

    move-result v7

    .line 1779
    if-eqz p1, :cond_4

    .line 1780
    if-eqz v7, :cond_3

    sget-object v0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$VisualState;->OVERVIEW_INCOGNITO:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$VisualState;

    :goto_0
    move-object v1, v0

    .line 1794
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->mVisualState:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$VisualState;

    if-eq v0, v1, :cond_8

    move v0, v2

    .line 1795
    :goto_2
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->getToolbarDataProvider()Lcom/google/android/apps/chrome/toolbar/ToolbarDataProvider;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/apps/chrome/toolbar/ToolbarDataProvider;->getPrimaryColor()I

    move-result v8

    .line 1796
    iget-object v4, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->mVisualState:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$VisualState;

    sget-object v9, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$VisualState;->BRAND_COLOR:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$VisualState;

    if-ne v4, v9, :cond_1

    if-nez v0, :cond_1

    iget v4, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->mToolbarBrandColor:I

    if-eq v8, v4, :cond_1

    .line 1798
    invoke-static {v8}, Lcom/google/android/apps/chrome/document/BrandColorUtils;->getLightnessForColor(I)F

    move-result v4

    .line 1799
    invoke-static {v4}, Lcom/google/android/apps/chrome/document/BrandColorUtils;->shouldUseLightDrawablesForToolbar(F)Z

    move-result v9

    .line 1801
    invoke-static {v4}, Lcom/google/android/apps/chrome/document/BrandColorUtils;->shouldUseOpaqueTextboxBackground(F)Z

    move-result v4

    if-nez v4, :cond_9

    move v4, v2

    .line 1803
    :goto_3
    iget-boolean v10, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->mUseLightToolbarDrawables:Z

    if-ne v9, v10, :cond_0

    iget-object v9, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUnfocusedLocationBarUsesTransparentBg:Z
    invoke-static {v9}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$4600(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Z

    move-result v9

    if-eq v4, v9, :cond_a

    :cond_0
    move v0, v2

    .line 1813
    :cond_1
    :goto_4
    iput-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->mVisualState:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$VisualState;

    .line 1815
    invoke-direct {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->updateOverlayDrawables()V

    .line 1816
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->updateShadowVisibility(Z)V

    .line 1817
    if-nez v0, :cond_b

    .line 1818
    sget-object v0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$VisualState;->NEW_TAB_NORMAL:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$VisualState;

    if-ne v1, v0, :cond_2

    .line 1819
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->getToolbarDataProvider()Lcom/google/android/apps/chrome/toolbar/ToolbarDataProvider;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarDataProvider;->getNewTabPageForCurrentTab()Lcom/google/android/apps/chrome/ntp/NewTabPage;

    move-result-object v1

    # invokes: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->updateNtpTransitionAnimation(Lcom/google/android/apps/chrome/ntp/NewTabPage;)V
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$4800(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;Lcom/google/android/apps/chrome/ntp/NewTabPage;)V

    .line 1902
    :cond_2
    :goto_5
    return-void

    .line 1780
    :cond_3
    sget-object v0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$VisualState;->OVERVIEW_NORMAL:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$VisualState;

    goto :goto_0

    .line 1783
    :cond_4
    invoke-direct {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->isLocationBarShownInNTP()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1784
    sget-object v0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$VisualState;->NEW_TAB_NORMAL:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$VisualState;

    move-object v1, v0

    goto :goto_1

    .line 1785
    :cond_5
    if-eqz v7, :cond_6

    .line 1786
    sget-object v0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$VisualState;->INCOGNITO:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$VisualState;

    move-object v1, v0

    goto :goto_1

    .line 1787
    :cond_6
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->getToolbarDataProvider()Lcom/google/android/apps/chrome/toolbar/ToolbarDataProvider;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarDataProvider;->isUsingBrandColor()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1788
    sget-object v0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$VisualState;->BRAND_COLOR:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$VisualState;

    move-object v1, v0

    goto :goto_1

    .line 1790
    :cond_7
    sget-object v0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$VisualState;->NORMAL:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$VisualState;

    move-object v1, v0

    goto :goto_1

    :cond_8
    move v0, v3

    .line 1794
    goto :goto_2

    :cond_9
    move v4, v3

    .line 1801
    goto :goto_3

    .line 1808
    :cond_a
    iput v8, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->mToolbarBrandColor:I

    .line 1809
    iget-object v4, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    new-instance v8, Landroid/graphics/drawable/ColorDrawable;

    iget v9, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->mToolbarBrandColor:I

    invoke-direct {v8, v9}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    # invokes: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->updateToolbarBackground(Landroid/graphics/drawable/Drawable;)V
    invoke-static {v4, v8}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$4700(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;Landroid/graphics/drawable/Drawable;)V

    goto :goto_4

    .line 1825
    :cond_b
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    .line 1826
    iput-boolean v3, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->mUseLightToolbarDrawables:Z

    .line 1827
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # setter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUnfocusedLocationBarUsesTransparentBg:Z
    invoke-static {v0, v3}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$4602(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;Z)Z

    .line 1828
    sget v0, Lcom/google/android/apps/chrome/R$drawable;->btn_menu:I

    .line 1829
    sget v0, Lcom/google/android/apps/chrome/R$drawable;->btn_toolbar_home:I

    .line 1830
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # setter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUrlBackgroundAlpha:I
    invoke-static {v0, v6}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$4902(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;I)I

    .line 1831
    sget v0, Lcom/google/android/apps/chrome/R$drawable;->progress_bar:I

    .line 1832
    if-eqz p1, :cond_10

    .line 1833
    iput-boolean v2, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->mUseLightToolbarDrawables:Z

    .line 1834
    sget v0, Lcom/google/android/apps/chrome/R$drawable;->btn_menu_white:I

    .line 1835
    sget v1, Lcom/google/android/apps/chrome/R$drawable;->btn_toolbar_home_white:I

    .line 1836
    iget-object v2, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # setter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUrlBackgroundAlpha:I
    invoke-static {v2, v5}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$4902(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;I)I

    .line 1837
    sget v2, Lcom/google/android/apps/chrome/R$drawable;->progress_bar_white:I

    .line 1838
    iget-object v3, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    sget v4, Lcom/google/android/apps/chrome/R$color;->tab_switcher_background:I

    invoke-virtual {v8, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    # invokes: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->updateToolbarBackground(Landroid/graphics/drawable/Drawable;)V
    invoke-static {v3, v4}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$4700(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;Landroid/graphics/drawable/Drawable;)V

    .line 1879
    :goto_6
    iget-object v3, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mProgressBar:Lcom/google/android/apps/chrome/widget/SmoothProgressBar;
    invoke-static {v3}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$1200(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Lcom/google/android/apps/chrome/widget/SmoothProgressBar;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/google/android/apps/chrome/widget/SmoothProgressBar;->setProgressDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1881
    iget-object v2, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mToggleTabStackButton:Landroid/widget/ImageView;
    invoke-static {v2}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$500(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Landroid/widget/ImageView;

    move-result-object v3

    iget-boolean v2, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->mUseLightToolbarDrawables:Z

    if-eqz v2, :cond_19

    iget-object v2, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mTabSwitcherButtonDrawableLight:Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;
    invoke-static {v2}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$4000(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;

    move-result-object v2

    :goto_7
    invoke-virtual {v3, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1883
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->shouldShowMenuButton()Z

    move-result v2

    if-eqz v2, :cond_c

    iget-object v2, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->mMenuButton:Landroid/widget/ImageButton;

    invoke-virtual {v2, v0}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 1884
    :cond_c
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mHomeButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$700(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getVisibility()I

    move-result v0

    const/16 v2, 0x8

    if-eq v0, v2, :cond_d

    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mHomeButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$700(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 1885
    :cond_d
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->mLocationBar:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->updateVisualsForState()V

    .line 1889
    invoke-direct {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->isLocationBarShownInNTP()Z

    move-result v0

    if-eqz v0, :cond_e

    if-nez p1, :cond_e

    .line 1890
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->getToolbarDataProvider()Lcom/google/android/apps/chrome/toolbar/ToolbarDataProvider;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarDataProvider;->getNewTabPageForCurrentTab()Lcom/google/android/apps/chrome/ntp/NewTabPage;

    move-result-object v1

    # invokes: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->updateNtpTransitionAnimation(Lcom/google/android/apps/chrome/ntp/NewTabPage;)V
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$4800(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;Lcom/google/android/apps/chrome/ntp/NewTabPage;)V

    .line 1894
    :cond_e
    if-eqz p1, :cond_f

    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mNewTabButton:Lcom/google/android/apps/chrome/widget/newtab/NewTabButton;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$2300(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Lcom/google/android/apps/chrome/widget/newtab/NewTabButton;

    move-result-object v0

    invoke-virtual {v0, v7}, Lcom/google/android/apps/chrome/widget/newtab/NewTabButton;->setIsIncognito(Z)V

    .line 1896
    :cond_f
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    if-eqz v7, :cond_1a

    sget v0, Lcom/google/android/apps/chrome/R$string;->accessibility_toolbar_btn_new_incognito_tab:I

    :goto_8
    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    .line 1899
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mNewTabButton:Lcom/google/android/apps/chrome/widget/newtab/NewTabButton;
    invoke-static {v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$2300(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Lcom/google/android/apps/chrome/widget/newtab/NewTabButton;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/widget/newtab/NewTabButton;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1900
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mNewTabButton:Lcom/google/android/apps/chrome/widget/newtab/NewTabButton;
    invoke-static {v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$2300(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Lcom/google/android/apps/chrome/widget/newtab/NewTabButton;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/widget/newtab/NewTabButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_5

    .line 1839
    :cond_10
    if-eqz v7, :cond_11

    .line 1840
    iput-boolean v2, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->mUseLightToolbarDrawables:Z

    .line 1841
    sget v0, Lcom/google/android/apps/chrome/R$drawable;->btn_menu_white:I

    .line 1842
    sget v1, Lcom/google/android/apps/chrome/R$drawable;->btn_toolbar_home_white:I

    .line 1843
    iget-object v2, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # setter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUrlBackgroundAlpha:I
    invoke-static {v2, v5}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$4902(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;I)I

    .line 1844
    sget v2, Lcom/google/android/apps/chrome/R$drawable;->progress_bar_white:I

    .line 1845
    iget-object v3, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    sget v4, Lcom/google/android/apps/chrome/R$color;->incognito_primary_color:I

    invoke-virtual {v8, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    # invokes: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->updateToolbarBackground(Landroid/graphics/drawable/Drawable;)V
    invoke-static {v3, v4}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$4700(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_6

    .line 1846
    :cond_11
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->mVisualState:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$VisualState;

    sget-object v4, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$VisualState;->BRAND_COLOR:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$VisualState;

    if-ne v0, v4, :cond_17

    .line 1847
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->getToolbarDataProvider()Lcom/google/android/apps/chrome/toolbar/ToolbarDataProvider;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarDataProvider;->getPrimaryColor()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->mToolbarBrandColor:I

    .line 1848
    iget v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->mToolbarBrandColor:I

    invoke-static {v0}, Lcom/google/android/apps/chrome/document/BrandColorUtils;->getLightnessForColor(I)F

    move-result v0

    .line 1849
    invoke-static {v0}, Lcom/google/android/apps/chrome/document/BrandColorUtils;->shouldUseLightDrawablesForToolbar(F)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->mUseLightToolbarDrawables:Z

    .line 1851
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    invoke-static {v0}, Lcom/google/android/apps/chrome/document/BrandColorUtils;->shouldUseOpaqueTextboxBackground(F)Z

    move-result v0

    if-nez v0, :cond_12

    :goto_9
    # setter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUnfocusedLocationBarUsesTransparentBg:Z
    invoke-static {v1, v2}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$4602(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;Z)Z

    .line 1853
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->mUseLightToolbarDrawables:Z

    if-eqz v0, :cond_13

    sget v0, Lcom/google/android/apps/chrome/R$drawable;->btn_menu_white:I

    .line 1855
    :goto_a
    iget-boolean v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->mUseLightToolbarDrawables:Z

    if-eqz v1, :cond_14

    sget v1, Lcom/google/android/apps/chrome/R$drawable;->btn_toolbar_home_white:I

    .line 1857
    :goto_b
    iget-object v3, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    iget-object v2, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUnfocusedLocationBarUsesTransparentBg:Z
    invoke-static {v2}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$4600(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Z

    move-result v2

    if-eqz v2, :cond_15

    move v2, v5

    :goto_c
    # setter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUrlBackgroundAlpha:I
    invoke-static {v3, v2}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$4902(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;I)I

    .line 1859
    iget-boolean v2, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->mUseLightToolbarDrawables:Z

    if-eqz v2, :cond_16

    sget v2, Lcom/google/android/apps/chrome/R$drawable;->progress_bar_white:I

    .line 1861
    :goto_d
    iget-object v3, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    new-instance v4, Landroid/graphics/drawable/ColorDrawable;

    iget v5, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->mToolbarBrandColor:I

    invoke-direct {v4, v5}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    # invokes: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->updateToolbarBackground(Landroid/graphics/drawable/Drawable;)V
    invoke-static {v3, v4}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$4700(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_6

    :cond_12
    move v2, v3

    .line 1851
    goto :goto_9

    .line 1853
    :cond_13
    sget v0, Lcom/google/android/apps/chrome/R$drawable;->btn_menu:I

    goto :goto_a

    .line 1855
    :cond_14
    sget v1, Lcom/google/android/apps/chrome/R$drawable;->btn_toolbar_home:I

    goto :goto_b

    :cond_15
    move v2, v6

    .line 1857
    goto :goto_c

    .line 1859
    :cond_16
    sget v2, Lcom/google/android/apps/chrome/R$drawable;->progress_bar:I

    goto :goto_d

    .line 1863
    :cond_17
    iput-boolean v3, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->mUseLightToolbarDrawables:Z

    .line 1864
    sget v4, Lcom/google/android/apps/chrome/R$drawable;->btn_menu:I

    .line 1865
    sget v2, Lcom/google/android/apps/chrome/R$drawable;->btn_toolbar_home:I

    .line 1866
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # setter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUrlBackgroundAlpha:I
    invoke-static {v0, v6}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$4902(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;I)I

    .line 1867
    sget v5, Lcom/google/android/apps/chrome/R$drawable;->progress_bar:I

    .line 1868
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    sget v9, Lcom/google/android/apps/chrome/R$drawable;->toolbar_normal:I

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v8

    # invokes: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->updateToolbarBackground(Landroid/graphics/drawable/Drawable;)V
    invoke-static {v0, v8}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$4700(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;Landroid/graphics/drawable/Drawable;)V

    .line 1869
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mToolbarBackground:Landroid/graphics/drawable/Drawable;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$5000(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/LayerDrawable;

    .line 1870
    sget v8, Lcom/google/android/apps/chrome/R$id;->toolbar_bg:I

    invoke-virtual {v0, v8}, Landroid/graphics/drawable/LayerDrawable;->findDrawableByLayerId(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1871
    sget-object v8, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$VisualState;->NEW_TAB_NORMAL:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$VisualState;

    if-ne v1, v8, :cond_18

    .line 1872
    invoke-virtual {v0, v3}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 1873
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # setter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUrlBackgroundAlpha:I
    invoke-static {v0, v3}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$4902(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;I)I

    move v1, v2

    move v0, v4

    move v2, v5

    goto/16 :goto_6

    .line 1875
    :cond_18
    invoke-virtual {v0, v6}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    move v1, v2

    move v0, v4

    move v2, v5

    goto/16 :goto_6

    .line 1881
    :cond_19
    iget-object v2, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mTabSwitcherButtonDrawable:Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;
    invoke-static {v2}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$4100(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;

    move-result-object v2

    goto/16 :goto_7

    .line 1896
    :cond_1a
    sget v0, Lcom/google/android/apps/chrome/R$string;->accessibility_toolbar_btn_new_tab:I

    goto/16 :goto_8
.end method


# virtual methods
.method public finishAnimations()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1163
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mOverviewModeAnimation:Landroid/animation/ObjectAnimator;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$1000(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1164
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mOverviewModeAnimation:Landroid/animation/ObjectAnimator;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$1000(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->end()V

    .line 1165
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # setter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mOverviewModeAnimation:Landroid/animation/ObjectAnimator;
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$1002(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;Landroid/animation/ObjectAnimator;)Landroid/animation/ObjectAnimator;

    .line 1167
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mDelayedOverviewModeAnimation:Landroid/animation/ObjectAnimator;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$1300(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1168
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mDelayedOverviewModeAnimation:Landroid/animation/ObjectAnimator;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$1300(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->end()V

    .line 1169
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # setter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mDelayedOverviewModeAnimation:Landroid/animation/ObjectAnimator;
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$1302(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;Landroid/animation/ObjectAnimator;)Landroid/animation/ObjectAnimator;

    .line 1171
    :cond_1
    return-void
.end method

.method public getLocationBarContentRect(Landroid/graphics/Rect;)V
    .locals 1

    .prologue
    .line 1175
    invoke-direct {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->isLocationBarShownInNTP()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->isFocused()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1176
    invoke-virtual {p1}, Landroid/graphics/Rect;->setEmpty()V

    .line 1181
    :goto_0
    return-void

    .line 1180
    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->getLocationBarContentRect(Landroid/graphics/Rect;)V

    goto :goto_0
.end method

.method protected handleFindToolbarStateChange(Z)V
    .locals 3

    .prologue
    const/16 v2, 0xfa

    .line 1751
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->getView()Landroid/view/View;

    move-result-object v1

    if-eqz p1, :cond_0

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1752
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mToolbarShadow:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$3900(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/TransitionDrawable;

    .line 1753
    if-eqz p1, :cond_1

    .line 1754
    invoke-virtual {v0, v2}, Landroid/graphics/drawable/TransitionDrawable;->startTransition(I)V

    .line 1758
    :goto_1
    return-void

    .line 1751
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1756
    :cond_1
    invoke-virtual {v0, v2}, Landroid/graphics/drawable/TransitionDrawable;->reverseTransition(I)V

    goto :goto_1
.end method

.method public isAnimatingForOverview()Z
    .locals 1

    .prologue
    .line 1142
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mOverviewModeAnimation:Landroid/animation/ObjectAnimator;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$1000(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isOverviewMode()Z
    .locals 1

    .prologue
    .line 1137
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mOverviewMode:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$900(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Z

    move-result v0

    return v0
.end method

.method protected onDefaultSearchEngineChanged()V
    .locals 2

    .prologue
    .line 1733
    invoke-super {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->onDefaultSearchEngineChanged()V

    .line 1740
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    new-instance v1, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone$6;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone$6;-><init>(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->post(Ljava/lang/Runnable;)Z

    .line 1747
    return-void
.end method

.method protected onHomeButtonUpdate(Z)V
    .locals 2

    .prologue
    .line 1185
    if-eqz p1, :cond_3

    .line 1186
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mHomeButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$700(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Landroid/widget/ImageButton;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->urlHasFocus()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mOverviewMode:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$900(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    const/4 v0, 0x4

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1187
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mBrowsingModeViews:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$800(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mHomeButton:Landroid/widget/ImageButton;
    invoke-static {v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$700(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Landroid/widget/ImageButton;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1188
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mBrowsingModeViews:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$800(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mHomeButton:Landroid/widget/ImageButton;
    invoke-static {v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$700(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Landroid/widget/ImageButton;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1194
    :cond_1
    :goto_1
    return-void

    .line 1186
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 1191
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mHomeButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$700(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Landroid/widget/ImageButton;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1192
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mBrowsingModeViews:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$800(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mHomeButton:Landroid/widget/ImageButton;
    invoke-static {v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$700(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Landroid/widget/ImageButton;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public onNativeLibraryReady()V
    .locals 2

    .prologue
    .line 1120
    invoke-super {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->onNativeLibraryReady()V

    .line 1122
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/partnercustomizations/HomepageManager;->isHomepageEnabled(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1123
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mHomeButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$700(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Landroid/widget/ImageButton;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->urlHasFocus()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x4

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1124
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mBrowsingModeViews:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$800(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mHomeButton:Landroid/widget/ImageButton;
    invoke-static {v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$700(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Landroid/widget/ImageButton;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1127
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mOverviewMode:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$900(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->updateVisualsForToolbarState(Z)V

    .line 1128
    return-void

    .line 1123
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onNavigatedToDifferentPage()V
    .locals 2

    .prologue
    .line 1147
    invoke-super {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->onNavigatedToDifferentPage()V

    .line 1148
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/utilities/FeatureUtilitiesInternal;->isDocumentMode(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1149
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUrlContainer:Lcom/google/android/apps/chrome/omnibox/UrlContainer;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$1100(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Lcom/google/android/apps/chrome/omnibox/UrlContainer;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/omnibox/UrlContainer;->setTrailingTextVisible(Z)V

    .line 1151
    :cond_0
    return-void
.end method

.method protected onOverviewTransitionFinished()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1366
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->setAlpha(F)V

    .line 1367
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mClipRect:Landroid/graphics/Rect;
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$2502(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;Landroid/graphics/Rect;)Landroid/graphics/Rect;

    .line 1368
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # setter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUIAnimatingOverviewTransition:Z
    invoke-static {v0, v2}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$1902(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;Z)Z

    .line 1369
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mAnimateNormalToolbar:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$2400(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1370
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->finishAnimations()V

    .line 1371
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mOverviewMode:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$900(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->updateVisualsForToolbarState(Z)V

    .line 1374
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mDelayingOverviewAnimation:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$2100(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1375
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # setter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mDelayingOverviewAnimation:Z
    invoke-static {v0, v2}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$2102(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;Z)Z

    .line 1376
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->createPostExitOverviewAnimation()Landroid/animation/ObjectAnimator;

    move-result-object v1

    # setter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mDelayedOverviewModeAnimation:Landroid/animation/ObjectAnimator;
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$1302(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;Landroid/animation/ObjectAnimator;)Landroid/animation/ObjectAnimator;

    .line 1377
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mDelayedOverviewModeAnimation:Landroid/animation/ObjectAnimator;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$1300(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 1381
    :goto_0
    return-void

    .line 1379
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mOverviewMode:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$900(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->updateViewsForOverviewMode(Z)V

    goto :goto_0
.end method

.method protected onPrimaryColorChanged()V
    .locals 1

    .prologue
    .line 1714
    invoke-super {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->onPrimaryColorChanged()V

    .line 1716
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mOverviewMode:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$900(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->updateVisualsForToolbarState(Z)V

    .line 1717
    return-void
.end method

.method public onStateRestored()V
    .locals 2

    .prologue
    .line 1132
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mToggleTabStackButton:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$500(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Landroid/widget/ImageView;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setClickable(Z)V

    .line 1133
    return-void
.end method

.method protected onTabContentViewChanged()V
    .locals 1

    .prologue
    .line 1700
    invoke-super {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->onTabContentViewChanged()V

    .line 1701
    invoke-direct {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->updateNtpAnimationState()V

    .line 1702
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mOverviewMode:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$900(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->updateVisualsForToolbarState(Z)V

    .line 1703
    return-void
.end method

.method protected onTabOrModelChanged()V
    .locals 1

    .prologue
    .line 1707
    invoke-super {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->onTabOrModelChanged()V

    .line 1708
    invoke-direct {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->updateNtpAnimationState()V

    .line 1709
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mOverviewMode:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$900(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->updateVisualsForToolbarState(Z)V

    .line 1710
    return-void
.end method

.method public onUrlFocusChange(Z)V
    .locals 4

    .prologue
    const/16 v3, 0xfa

    .line 1595
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->onUrlFocusChange(Z)V

    .line 1596
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUrlFocusLayoutAnimator:Landroid/animation/AnimatorSet;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$2200(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Landroid/animation/AnimatorSet;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUrlFocusLayoutAnimator:Landroid/animation/AnimatorSet;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$2200(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Landroid/animation/AnimatorSet;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1597
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUrlFocusLayoutAnimator:Landroid/animation/AnimatorSet;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$2200(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Landroid/animation/AnimatorSet;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->cancel()V

    .line 1598
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUrlFocusLayoutAnimator:Landroid/animation/AnimatorSet;
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$2202(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;Landroid/animation/AnimatorSet;)Landroid/animation/AnimatorSet;

    .line 1601
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1602
    if-eqz p1, :cond_2

    .line 1603
    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->populateUrlFocusingAnimatorSet(Ljava/util/List;)V

    .line 1607
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    new-instance v2, Landroid/animation/AnimatorSet;

    invoke-direct {v2}, Landroid/animation/AnimatorSet;-><init>()V

    # setter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUrlFocusLayoutAnimator:Landroid/animation/AnimatorSet;
    invoke-static {v1, v2}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$2202(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;Landroid/animation/AnimatorSet;)Landroid/animation/AnimatorSet;

    .line 1608
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUrlFocusLayoutAnimator:Landroid/animation/AnimatorSet;
    invoke-static {v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$2200(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Landroid/animation/AnimatorSet;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/animation/AnimatorSet;->playTogether(Ljava/util/Collection;)V

    .line 1610
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUrlFocusChangeInProgress:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$3502(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;Z)Z

    .line 1611
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUrlFocusLayoutAnimator:Landroid/animation/AnimatorSet;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$2200(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Landroid/animation/AnimatorSet;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone$5;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone$5;-><init>(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;Z)V

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 1658
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUrlFocusLayoutAnimator:Landroid/animation/AnimatorSet;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$2200(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Landroid/animation/AnimatorSet;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    .line 1659
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->getLocationBar()Lcom/google/android/apps/chrome/omnibox/LocationBar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->shouldSkipAnimation()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUrlFocusLayoutAnimator:Landroid/animation/AnimatorSet;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$2200(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Landroid/animation/AnimatorSet;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->end()V

    .line 1661
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mToolbarShadow:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$3900(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/TransitionDrawable;

    .line 1662
    if-eqz p1, :cond_3

    .line 1663
    invoke-virtual {v0, v3}, Landroid/graphics/drawable/TransitionDrawable;->startTransition(I)V

    .line 1667
    :goto_1
    return-void

    .line 1605
    :cond_2
    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->populateUrlClearFocusingAnimatorSet(Ljava/util/List;)V

    goto :goto_0

    .line 1665
    :cond_3
    invoke-virtual {v0, v3}, Landroid/graphics/drawable/TransitionDrawable;->reverseTransition(I)V

    goto :goto_1
.end method

.method public setLoadProgress(I)V
    .locals 2

    .prologue
    .line 1155
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mProgressBar:Lcom/google/android/apps/chrome/widget/SmoothProgressBar;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$1200(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Lcom/google/android/apps/chrome/widget/SmoothProgressBar;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/widget/SmoothProgressBar;->setProgress(I)V

    .line 1156
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/utilities/FeatureUtilitiesInternal;->isDocumentMode(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x64

    if-ne p1, v0, :cond_0

    .line 1157
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUrlContainer:Lcom/google/android/apps/chrome/omnibox/UrlContainer;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$1100(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Lcom/google/android/apps/chrome/omnibox/UrlContainer;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/omnibox/UrlContainer;->setTrailingTextVisible(Z)V

    .line 1159
    :cond_0
    return-void
.end method

.method public setOnNewTabClickHandler(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 1421
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # setter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mNewTabListener:Landroid/view/View$OnClickListener;
    invoke-static {v0, p1}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$3002(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;Landroid/view/View$OnClickListener;)Landroid/view/View$OnClickListener;

    .line 1422
    return-void
.end method

.method public setOnOverviewClickHandler(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 1416
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # setter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mOverviewListener:Landroid/view/View$OnClickListener;
    invoke-static {v0, p1}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$2902(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;Landroid/view/View$OnClickListener;)Landroid/view/View$OnClickListener;

    .line 1417
    return-void
.end method

.method protected setOverviewMode(ZLandroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1325
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mOverviewMode:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$900(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Z

    move-result v0

    if-ne v0, p1, :cond_0

    .line 1362
    :goto_0
    return-void

    .line 1327
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->finishAnimations()V

    .line 1331
    if-nez p1, :cond_4

    move v0, v1

    .line 1332
    :goto_1
    iget-object v3, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/chrome/device/DeviceClassManager;->enableAnimations(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_5

    move v0, v1

    .line 1338
    :goto_2
    iget-object v3, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    const-string/jumbo v4, "creating_ntp"

    invoke-virtual {p2, v4, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    # setter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mDelayingOverviewAnimation:Z
    invoke-static {v3, v2}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$2102(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;Z)Z

    .line 1340
    if-eqz p1, :cond_6

    .line 1341
    iget-object v2, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUrlFocusLayoutAnimator:Landroid/animation/AnimatorSet;
    invoke-static {v2}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$2200(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Landroid/animation/AnimatorSet;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUrlFocusLayoutAnimator:Landroid/animation/AnimatorSet;
    invoke-static {v2}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$2200(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Landroid/animation/AnimatorSet;

    move-result-object v2

    invoke-virtual {v2}, Landroid/animation/AnimatorSet;->isRunning()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1342
    iget-object v2, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUrlFocusLayoutAnimator:Landroid/animation/AnimatorSet;
    invoke-static {v2}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$2200(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Landroid/animation/AnimatorSet;

    move-result-object v2

    invoke-virtual {v2}, Landroid/animation/AnimatorSet;->end()V

    .line 1343
    iget-object v2, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    const/4 v3, 0x0

    # setter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUrlFocusLayoutAnimator:Landroid/animation/AnimatorSet;
    invoke-static {v2, v3}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$2202(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;Landroid/animation/AnimatorSet;)Landroid/animation/AnimatorSet;

    .line 1345
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mNewTabButton:Lcom/google/android/apps/chrome/widget/newtab/NewTabButton;
    invoke-static {v2}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$2300(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Lcom/google/android/apps/chrome/widget/newtab/NewTabButton;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/android/apps/chrome/widget/newtab/NewTabButton;->setEnabled(Z)V

    .line 1346
    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->updateViewsForOverviewMode(Z)V

    .line 1347
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->createEnterOverviewModeAnimation()Landroid/animation/ObjectAnimator;

    move-result-object v2

    # setter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mOverviewModeAnimation:Landroid/animation/ObjectAnimator;
    invoke-static {v1, v2}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$1002(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;Landroid/animation/ObjectAnimator;)Landroid/animation/ObjectAnimator;

    .line 1355
    :goto_3
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # setter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mAnimateNormalToolbar:Z
    invoke-static {v1, v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$2402(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;Z)Z

    .line 1356
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # setter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mOverviewMode:Z
    invoke-static {v0, p1}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$902(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;Z)Z

    .line 1357
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mOverviewModeAnimation:Landroid/animation/ObjectAnimator;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$1000(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mOverviewModeAnimation:Landroid/animation/ObjectAnimator;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$1000(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 1359
    :cond_2
    invoke-static {}, Lcom/google/android/apps/chrome/device/DeviceClassManager;->isLowEndDevice()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->finishAnimations()V

    .line 1361
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    invoke-static {v0}, Lorg/chromium/base/ApiCompatibilityUtils;->postInvalidateOnAnimation(Landroid/view/View;)V

    goto/16 :goto_0

    :cond_4
    move v0, v2

    .line 1331
    goto :goto_1

    .line 1335
    :cond_5
    const-string/jumbo v3, "show_toolbar"

    invoke-virtual {p2, v3, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    goto :goto_2

    .line 1349
    :cond_6
    iget-object v2, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mDelayingOverviewAnimation:Z
    invoke-static {v2}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$2100(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Z

    move-result v2

    if-nez v2, :cond_7

    .line 1350
    iget-object v2, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->createExitOverviewAnimation(Z)Landroid/animation/ObjectAnimator;

    move-result-object v3

    # setter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mOverviewModeAnimation:Landroid/animation/ObjectAnimator;
    invoke-static {v2, v3}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$1002(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;Landroid/animation/ObjectAnimator;)Landroid/animation/ObjectAnimator;

    .line 1352
    :cond_7
    iget-object v2, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # setter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUIAnimatingOverviewTransition:Z
    invoke-static {v2, v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$1902(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;Z)Z

    goto :goto_3
.end method

.method public setTextureCaptureMode(Z)V
    .locals 3

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    .line 1289
    sget-boolean v0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mTextureCaptureMode:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$1800(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Z

    move-result v0

    if-ne v0, p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1290
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # setter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mTextureCaptureMode:Z
    invoke-static {v0, p1}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$1802(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;Z)Z

    .line 1291
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mTextureCaptureMode:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$1800(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1292
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->getAlpha()F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->mPreTextureCaptureAlpha:F

    .line 1293
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->setAlpha(F)V

    .line 1298
    :goto_0
    return-void

    .line 1295
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    iget v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->mPreTextureCaptureAlpha:F

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->setAlpha(F)V

    .line 1296
    iput v2, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->mPreTextureCaptureAlpha:F

    goto :goto_0
.end method

.method public updateReaderModeButton(ZZ)V
    .locals 3

    .prologue
    const/16 v1, 0x8

    .line 1198
    .line 1199
    if-eqz p1, :cond_5

    .line 1200
    const/4 v0, 0x0

    .line 1201
    iget-object v2, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mToolbarDelegate:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;
    invoke-static {v2}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$300(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->urlHasFocus()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mOverviewMode:Z
    invoke-static {v2}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$900(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mOverviewModeAnimation:Landroid/animation/ObjectAnimator;
    invoke-static {v2}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$1000(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Landroid/animation/ObjectAnimator;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 1203
    :cond_0
    const/4 v0, 0x4

    .line 1207
    :cond_1
    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mReaderModeButton:Landroid/widget/ImageButton;
    invoke-static {v2}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$1400(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Landroid/widget/ImageButton;

    move-result-object v2

    invoke-virtual {v2, p2}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 1209
    iget-object v2, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mReaderModeButton:Landroid/widget/ImageButton;
    invoke-static {v2}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$1400(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Landroid/widget/ImageButton;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/ImageButton;->getVisibility()I

    move-result v2

    if-ne v0, v2, :cond_2

    .line 1222
    :goto_1
    return-void

    .line 1211
    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mReaderModeButton:Landroid/widget/ImageButton;
    invoke-static {v2}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$1400(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Landroid/widget/ImageButton;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1212
    if-ne v0, v1, :cond_4

    .line 1213
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mBrowsingModeViews:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$800(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mReaderModeButton:Landroid/widget/ImageButton;
    invoke-static {v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$1400(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Landroid/widget/ImageButton;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1221
    :cond_3
    :goto_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->mLocationBar:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->requestLayout()V

    goto :goto_1

    .line 1215
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mBrowsingModeViews:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$800(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mReaderModeButton:Landroid/widget/ImageButton;
    invoke-static {v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$1400(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Landroid/widget/ImageButton;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1216
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mBrowsingModeViews:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$800(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mReaderModeButton:Landroid/widget/ImageButton;
    invoke-static {v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$1400(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Landroid/widget/ImageButton;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method protected updateTabCountVisuals(I)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1671
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mToggleTabStackButton:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$500(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Landroid/widget/ImageView;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1696
    :cond_0
    :goto_0
    return-void

    .line 1672
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mHomeButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$700(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 1674
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mToggleTabStackButton:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$500(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Landroid/widget/ImageView;

    move-result-object v3

    if-lez p1, :cond_4

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 1675
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mToggleTabStackButton:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$500(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Landroid/widget/ImageView;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/google/android/apps/chrome/R$string;->accessibility_toolbar_btn_tabswitcher_toggle:I

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v1, v2

    invoke-virtual {v3, v4, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1678
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mTabSwitcherButtonDrawableLight:Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$4000(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # invokes: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->isIncognitoMode()Z
    invoke-static {v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$2600(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Z

    move-result v1

    invoke-virtual {v0, p1, v1}, Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;->updateForTabCount(IZ)V

    .line 1679
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mTabSwitcherButtonDrawable:Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$4100(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # invokes: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->isIncognitoMode()Z
    invoke-static {v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$2600(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Z

    move-result v1

    invoke-virtual {v0, p1, v1}, Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;->updateForTabCount(IZ)V

    .line 1681
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # invokes: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->isIncognitoMode()Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$2600(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Z

    move-result v0

    .line 1682
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mOverviewAnimationTabStackDrawable:Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;
    invoke-static {v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$4200(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-boolean v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->mIsOverlayTabStackDrawableLight:Z

    if-eq v1, v0, :cond_3

    .line 1684
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    iget-object v2, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-static {v2, v0}, Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;->createTabSwitcherDrawableForTexture(Landroid/content/res/Resources;Z)Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;

    move-result-object v2

    # setter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mOverviewAnimationTabStackDrawable:Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;
    invoke-static {v1, v2}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$4202(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;)Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;

    .line 1687
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mOverviewAnimationTabStackDrawable:Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;
    invoke-static {v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$4200(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mToggleTabStackButton:Landroid/widget/ImageView;
    invoke-static {v2}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$500(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Landroid/widget/ImageView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;->setBounds(Landroid/graphics/Rect;)V

    .line 1689
    iput-boolean v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->mIsOverlayTabStackDrawableLight:Z

    .line 1692
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mOverviewAnimationTabStackDrawable:Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$4200(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1693
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mOverviewAnimationTabStackDrawable:Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$4200(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # invokes: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->isIncognitoMode()Z
    invoke-static {v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$2600(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Z

    move-result v1

    invoke-virtual {v0, p1, v1}, Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;->updateForTabCount(IZ)V

    goto/16 :goto_0

    :cond_4
    move v0, v2

    .line 1674
    goto/16 :goto_1
.end method
