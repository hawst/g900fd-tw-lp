.class public Lcom/google/android/apps/chrome/eventfilter/AreaGestureEventFilter;
.super Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;
.source "AreaGestureEventFilter.java"


# instance fields
.field private mTriggerRect:Landroid/graphics/RectF;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/chrome/eventfilter/EventFilterHost;Lcom/google/android/apps/chrome/eventfilter/GestureHandler;Landroid/graphics/RectF;ZZ)V
    .locals 6

    .prologue
    .line 55
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p5

    move v5, p6

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;-><init>(Landroid/content/Context;Lcom/google/android/apps/chrome/eventfilter/EventFilterHost;Lcom/google/android/apps/chrome/eventfilter/GestureHandler;ZZ)V

    .line 56
    invoke-virtual {p0, p4}, Lcom/google/android/apps/chrome/eventfilter/AreaGestureEventFilter;->setEventArea(Landroid/graphics/RectF;)V

    .line 57
    return-void
.end method


# virtual methods
.method public onInterceptTouchEventInternal(Landroid/view/MotionEvent;Z)Z
    .locals 4

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/AreaGestureEventFilter;->mTriggerRect:Landroid/graphics/RectF;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    iget v2, p0, Lcom/google/android/apps/chrome/eventfilter/AreaGestureEventFilter;->mPxToDp:F

    mul-float/2addr v1, v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    iget v3, p0, Lcom/google/android/apps/chrome/eventfilter/AreaGestureEventFilter;->mPxToDp:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 69
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->onInterceptTouchEventInternal(Landroid/view/MotionEvent;Z)Z

    move-result v0

    .line 71
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setEventArea(Landroid/graphics/RectF;)V
    .locals 0

    .prologue
    .line 63
    iput-object p1, p0, Lcom/google/android/apps/chrome/eventfilter/AreaGestureEventFilter;->mTriggerRect:Landroid/graphics/RectF;

    .line 64
    return-void
.end method
