.class public Lcom/google/android/apps/chrome/videofling/FullscreenMediaRouteButton;
.super Landroid/support/v7/app/o;
.source "FullscreenMediaRouteButton.java"


# instance fields
.field private mRecordedInitialVisibility:Z

.field private mVisibilityRequested:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 30
    invoke-direct {p0, p1, p2}, Landroid/support/v7/app/o;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 31
    iput-boolean v0, p0, Lcom/google/android/apps/chrome/videofling/FullscreenMediaRouteButton;->mRecordedInitialVisibility:Z

    .line 32
    iput-boolean v0, p0, Lcom/google/android/apps/chrome/videofling/FullscreenMediaRouteButton;->mVisibilityRequested:Z

    .line 33
    return-void
.end method

.method private setButtonVisibility(I)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 75
    if-nez p1, :cond_2

    .line 76
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/FullscreenMediaRouteButton;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 77
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/videofling/FullscreenMediaRouteButton;->setVisibility(I)V

    .line 78
    invoke-static {v1}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->castButtonShown(Z)V

    .line 80
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/videofling/FullscreenMediaRouteButton;->mRecordedInitialVisibility:Z

    if-nez v0, :cond_0

    .line 81
    invoke-static {v1}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->castButtonShownOnInitialFullscreen(Z)V

    .line 82
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/videofling/FullscreenMediaRouteButton;->mRecordedInitialVisibility:Z

    .line 90
    :cond_0
    :goto_0
    return-void

    .line 85
    :cond_1
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/videofling/FullscreenMediaRouteButton;->setVisibility(I)V

    goto :goto_0

    .line 88
    :cond_2
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/videofling/FullscreenMediaRouteButton;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public initialize(Lcom/google/android/apps/chrome/videofling/MediaRouteController;)V
    .locals 1

    .prologue
    .line 39
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->buildMediaRouteSelector()Landroid/support/v7/media/e;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/videofling/FullscreenMediaRouteButton;->setRouteSelector(Landroid/support/v7/media/e;)V

    .line 40
    new-instance v0, Lcom/google/android/apps/chrome/videofling/ChromeMediaRouteDialogFactory;

    invoke-direct {v0, p1}, Lcom/google/android/apps/chrome/videofling/ChromeMediaRouteDialogFactory;-><init>(Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog$DisconnectListener;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/videofling/FullscreenMediaRouteButton;->setDialogFactory(Landroid/support/v7/app/B;)V

    .line 41
    return-void
.end method

.method public setEnabled(Z)V
    .locals 1

    .prologue
    .line 45
    invoke-static {}, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->isRemotePlaybackEnabled()Z

    move-result v0

    if-nez v0, :cond_1

    .line 59
    :cond_0
    :goto_0
    return-void

    .line 49
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/FullscreenMediaRouteButton;->isEnabled()Z

    move-result v0

    .line 50
    invoke-super {p0, p1}, Landroid/support/v7/app/o;->setEnabled(Z)V

    .line 52
    if-eq v0, p1, :cond_0

    .line 54
    if-eqz p1, :cond_2

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/videofling/FullscreenMediaRouteButton;->mVisibilityRequested:Z

    if-eqz v0, :cond_2

    .line 55
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/videofling/FullscreenMediaRouteButton;->setButtonVisibility(I)V

    goto :goto_0

    .line 57
    :cond_2
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/videofling/FullscreenMediaRouteButton;->setVisibility(I)V

    goto :goto_0
.end method
