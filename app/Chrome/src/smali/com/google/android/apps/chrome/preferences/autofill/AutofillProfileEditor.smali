.class public Lcom/google/android/apps/chrome/preferences/autofill/AutofillProfileEditor;
.super Lcom/google/android/apps/chrome/preferences/ChromeBaseFragment;
.source "AutofillProfileEditor.java"

# interfaces
.implements Landroid/text/TextWatcher;
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# instance fields
.field private mAddressWidget:Lcom/a/a/a/l;

.field private mEmailText:Landroid/widget/EditText;

.field private mGUID:Ljava/lang/String;

.field private mLanguageCodeString:Ljava/lang/String;

.field private mNoCountryItemIsSelected:Z

.field private mPhoneText:Landroid/widget/EditText;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBaseFragment;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/preferences/autofill/AutofillProfileEditor;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/autofill/AutofillProfileEditor;->deleteProfile()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/preferences/autofill/AutofillProfileEditor;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/autofill/AutofillProfileEditor;->saveProfile()V

    return-void
.end method

.method private addProfileDataToEditFields(Landroid/view/View;)V
    .locals 9

    .prologue
    .line 102
    new-instance v5, Lcom/a/a/a/c;

    invoke-direct {v5}, Lcom/a/a/a/c;-><init>()V

    .line 103
    invoke-static {}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager;->getInstance()Lorg/chromium/chrome/browser/autofill/PersonalDataManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/autofill/AutofillProfileEditor;->mGUID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager;->getProfile(Ljava/lang/String;)Lorg/chromium/chrome/browser/autofill/PersonalDataManager$AutofillProfile;

    move-result-object v7

    .line 105
    if-eqz v7, :cond_0

    .line 106
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/autofill/AutofillProfileEditor;->mPhoneText:Landroid/widget/EditText;

    invoke-virtual {v7}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager$AutofillProfile;->getPhoneNumber()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 107
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/autofill/AutofillProfileEditor;->mEmailText:Landroid/widget/EditText;

    invoke-virtual {v7}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager$AutofillProfile;->getEmailAddress()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 108
    invoke-virtual {v7}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager$AutofillProfile;->getLanguageCode()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/preferences/autofill/AutofillProfileEditor;->mLanguageCodeString:Ljava/lang/String;

    .line 110
    sget-object v0, Lcom/a/a/a/e;->a:Lcom/a/a/a/e;

    invoke-virtual {v7}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager$AutofillProfile;->getRegion()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v0, v1}, Lcom/a/a/a/c;->a(Lcom/a/a/a/e;Ljava/lang/String;)Lcom/a/a/a/c;

    .line 111
    sget-object v0, Lcom/a/a/a/e;->b:Lcom/a/a/a/e;

    invoke-virtual {v7}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager$AutofillProfile;->getLocality()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v0, v1}, Lcom/a/a/a/c;->a(Lcom/a/a/a/e;Ljava/lang/String;)Lcom/a/a/a/c;

    .line 112
    sget-object v0, Lcom/a/a/a/e;->c:Lcom/a/a/a/e;

    invoke-virtual {v7}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager$AutofillProfile;->getFullName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v0, v1}, Lcom/a/a/a/c;->a(Lcom/a/a/a/e;Ljava/lang/String;)Lcom/a/a/a/c;

    .line 113
    sget-object v0, Lcom/a/a/a/e;->d:Lcom/a/a/a/e;

    invoke-virtual {v7}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager$AutofillProfile;->getCompanyName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v0, v1}, Lcom/a/a/a/c;->a(Lcom/a/a/a/e;Ljava/lang/String;)Lcom/a/a/a/c;

    .line 114
    sget-object v0, Lcom/a/a/a/e;->g:Lcom/a/a/a/e;

    invoke-virtual {v7}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager$AutofillProfile;->getDependentLocality()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v0, v1}, Lcom/a/a/a/c;->a(Lcom/a/a/a/e;Ljava/lang/String;)Lcom/a/a/a/c;

    .line 115
    sget-object v0, Lcom/a/a/a/e;->h:Lcom/a/a/a/e;

    invoke-virtual {v7}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager$AutofillProfile;->getPostalCode()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v0, v1}, Lcom/a/a/a/c;->a(Lcom/a/a/a/e;Ljava/lang/String;)Lcom/a/a/a/c;

    .line 116
    sget-object v0, Lcom/a/a/a/e;->i:Lcom/a/a/a/e;

    invoke-virtual {v7}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager$AutofillProfile;->getSortingCode()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v0, v1}, Lcom/a/a/a/c;->a(Lcom/a/a/a/e;Ljava/lang/String;)Lcom/a/a/a/c;

    .line 117
    sget-object v0, Lcom/a/a/a/e;->e:Lcom/a/a/a/e;

    invoke-virtual {v7}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager$AutofillProfile;->getStreetAddress()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v0, v1}, Lcom/a/a/a/c;->a(Lcom/a/a/a/e;Ljava/lang/String;)Lcom/a/a/a/c;

    .line 118
    sget-object v0, Lcom/a/a/a/e;->k:Lcom/a/a/a/e;

    invoke-virtual {v7}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager$AutofillProfile;->getCountryCode()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v0, v1}, Lcom/a/a/a/c;->a(Lcom/a/a/a/e;Ljava/lang/String;)Lcom/a/a/a/c;

    .line 121
    :cond_0
    sget v0, Lcom/google/android/apps/chrome/R$id;->autofill_profile_widget_root:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    .line 122
    new-instance v0, Lcom/a/a/a/l;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/autofill/AutofillProfileEditor;->getActivity()Landroid/app/Activity;

    move-result-object v1

    new-instance v3, Lcom/a/a/a/E;

    invoke-direct {v3}, Lcom/a/a/a/E;-><init>()V

    invoke-virtual {v3}, Lcom/a/a/a/E;->a()Lcom/a/a/a/D;

    move-result-object v3

    new-instance v4, Lcom/a/a/a/w;

    invoke-direct {v4}, Lcom/a/a/a/w;-><init>()V

    invoke-virtual {v5}, Lcom/a/a/a/c;->a()Lcom/a/a/a/a;

    move-result-object v5

    new-instance v6, Lcom/google/android/apps/chrome/preferences/autofill/ChromeAddressWidgetUiComponentProvider;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/autofill/AutofillProfileEditor;->getActivity()Landroid/app/Activity;

    move-result-object v8

    invoke-direct {v6, v8}, Lcom/google/android/apps/chrome/preferences/autofill/ChromeAddressWidgetUiComponentProvider;-><init>(Landroid/content/Context;)V

    invoke-direct/range {v0 .. v6}, Lcom/a/a/a/l;-><init>(Landroid/content/Context;Landroid/view/ViewGroup;Lcom/a/a/a/D;Lcom/a/a/a/w;Lcom/a/a/a/a;Lcom/a/a/a/s;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/preferences/autofill/AutofillProfileEditor;->mAddressWidget:Lcom/a/a/a/l;

    .line 128
    if-nez v7, :cond_1

    .line 129
    invoke-virtual {v2}, Landroid/view/ViewGroup;->requestFocus()Z

    .line 131
    :cond_1
    return-void
.end method

.method private deleteProfile()V
    .locals 2

    .prologue
    .line 161
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/autofill/AutofillProfileEditor;->mGUID:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 162
    invoke-static {}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager;->getInstance()Lorg/chromium/chrome/browser/autofill/PersonalDataManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/autofill/AutofillProfileEditor;->mGUID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager;->deleteProfile(Ljava/lang/String;)V

    .line 164
    :cond_0
    return-void
.end method

.method private enableSaveButton()V
    .locals 2

    .prologue
    .line 213
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/autofill/AutofillProfileEditor;->getView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$id;->autofill_profile_save:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 214
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 215
    return-void
.end method

.method private hookupSaveCancelDeleteButtons(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 167
    sget v0, Lcom/google/android/apps/chrome/R$id;->autofill_profile_delete:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 168
    iget-object v2, p0, Lcom/google/android/apps/chrome/preferences/autofill/AutofillProfileEditor;->mGUID:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/chrome/preferences/autofill/AutofillProfileEditor;->mGUID:Ljava/lang/String;

    const-string/jumbo v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_2

    .line 170
    :cond_0
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 180
    :goto_0
    sget v0, Lcom/google/android/apps/chrome/R$id;->autofill_profile_cancel:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 181
    new-instance v2, Lcom/google/android/apps/chrome/preferences/autofill/AutofillProfileEditor$2;

    invoke-direct {v2, p0}, Lcom/google/android/apps/chrome/preferences/autofill/AutofillProfileEditor$2;-><init>(Lcom/google/android/apps/chrome/preferences/autofill/AutofillProfileEditor;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 187
    sget v0, Lcom/google/android/apps/chrome/R$id;->autofill_profile_save:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 188
    new-instance v2, Lcom/google/android/apps/chrome/preferences/autofill/AutofillProfileEditor$3;

    invoke-direct {v2, p0}, Lcom/google/android/apps/chrome/preferences/autofill/AutofillProfileEditor$3;-><init>(Lcom/google/android/apps/chrome/preferences/autofill/AutofillProfileEditor;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 195
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 198
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/autofill/AutofillProfileEditor;->mPhoneText:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 199
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/autofill/AutofillProfileEditor;->mEmailText:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 200
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/preferences/autofill/AutofillProfileEditor;->mNoCountryItemIsSelected:Z

    .line 202
    invoke-static {}, Lcom/a/a/a/e;->values()[Lcom/a/a/a/e;

    move-result-object v2

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_4

    aget-object v0, v2, v1

    .line 203
    iget-object v4, p0, Lcom/google/android/apps/chrome/preferences/autofill/AutofillProfileEditor;->mAddressWidget:Lcom/a/a/a/l;

    invoke-virtual {v4, v0}, Lcom/a/a/a/l;->a(Lcom/a/a/a/e;)Landroid/view/View;

    move-result-object v0

    .line 204
    instance-of v4, v0, Landroid/widget/EditText;

    if-eqz v4, :cond_3

    .line 205
    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 202
    :cond_1
    :goto_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 172
    :cond_2
    new-instance v2, Lcom/google/android/apps/chrome/preferences/autofill/AutofillProfileEditor$1;

    invoke-direct {v2, p0}, Lcom/google/android/apps/chrome/preferences/autofill/AutofillProfileEditor$1;-><init>(Lcom/google/android/apps/chrome/preferences/autofill/AutofillProfileEditor;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 206
    :cond_3
    instance-of v4, v0, Landroid/widget/Spinner;

    if-eqz v4, :cond_1

    .line 207
    check-cast v0, Landroid/widget/Spinner;

    invoke-virtual {v0, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    goto :goto_2

    .line 210
    :cond_4
    return-void
.end method

.method private saveProfile()V
    .locals 15

    .prologue
    .line 136
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/autofill/AutofillProfileEditor;->mAddressWidget:Lcom/a/a/a/l;

    invoke-virtual {v0}, Lcom/a/a/a/l;->a()Lcom/a/a/a/a;

    move-result-object v11

    .line 137
    invoke-virtual {v11}, Lcom/a/a/a/a;->b()Ljava/lang/String;

    move-result-object v5

    .line 138
    invoke-virtual {v11}, Lcom/a/a/a/a;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 139
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v11}, Lcom/a/a/a/a;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 142
    :cond_0
    new-instance v0, Lorg/chromium/chrome/browser/autofill/PersonalDataManager$AutofillProfile;

    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/autofill/AutofillProfileEditor;->mGUID:Ljava/lang/String;

    const-string/jumbo v2, "Chrome settings"

    invoke-virtual {v11}, Lcom/a/a/a/a;->h()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v11}, Lcom/a/a/a/a;->g()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v11}, Lcom/a/a/a/a;->d()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v11}, Lcom/a/a/a/a;->e()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v11}, Lcom/a/a/a/a;->f()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v11}, Lcom/a/a/a/a;->i()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v11}, Lcom/a/a/a/a;->j()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v11}, Lcom/a/a/a/a;->a()Ljava/lang/String;

    move-result-object v11

    iget-object v12, p0, Lcom/google/android/apps/chrome/preferences/autofill/AutofillProfileEditor;->mPhoneText:Landroid/widget/EditText;

    invoke-virtual {v12}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v12

    iget-object v13, p0, Lcom/google/android/apps/chrome/preferences/autofill/AutofillProfileEditor;->mEmailText:Landroid/widget/EditText;

    invoke-virtual {v13}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v13

    iget-object v14, p0, Lcom/google/android/apps/chrome/preferences/autofill/AutofillProfileEditor;->mLanguageCodeString:Ljava/lang/String;

    invoke-direct/range {v0 .. v14}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager$AutofillProfile;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 157
    invoke-static {}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager;->getInstance()Lorg/chromium/chrome/browser/autofill/PersonalDataManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager;->setProfile(Lorg/chromium/chrome/browser/autofill/PersonalDataManager$AutofillProfile;)Ljava/lang/String;

    .line 158
    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 78
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 81
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 49
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/preferences/ChromeBaseFragment;->onCreate(Landroid/os/Bundle;)V

    .line 50
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    .line 55
    invoke-super {p0, p3}, Lcom/google/android/apps/chrome/preferences/ChromeBaseFragment;->onCreate(Landroid/os/Bundle;)V

    .line 56
    sget v0, Lcom/google/android/apps/chrome/R$layout;->autofill_profile_editor:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 57
    sget v0, Lcom/google/android/apps/chrome/R$string;->autofill_create_or_edit_profile:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 59
    sget v0, Lcom/google/android/apps/chrome/R$id;->autofill_profile_editor_phone_number_edit:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/google/android/apps/chrome/preferences/autofill/AutofillProfileEditor;->mPhoneText:Landroid/widget/EditText;

    .line 60
    sget v0, Lcom/google/android/apps/chrome/R$id;->autofill_profile_editor_email_address_edit:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/google/android/apps/chrome/preferences/autofill/AutofillProfileEditor;->mEmailText:Landroid/widget/EditText;

    .line 64
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/autofill/AutofillProfileEditor;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 65
    if-eqz v0, :cond_0

    .line 66
    const-string/jumbo v2, "guid"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/preferences/autofill/AutofillProfileEditor;->mGUID:Ljava/lang/String;

    .line 68
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/autofill/AutofillProfileEditor;->mGUID:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 69
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/google/android/apps/chrome/preferences/autofill/AutofillProfileEditor;->mGUID:Ljava/lang/String;

    .line 72
    :cond_1
    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/preferences/autofill/AutofillProfileEditor;->addProfileDataToEditFields(Landroid/view/View;)V

    .line 73
    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/preferences/autofill/AutofillProfileEditor;->hookupSaveCancelDeleteButtons(Landroid/view/View;)V

    .line 74
    return-object v1
.end method

.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 1

    .prologue
    .line 90
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/preferences/autofill/AutofillProfileEditor;->mNoCountryItemIsSelected:Z

    if-eqz v0, :cond_0

    .line 91
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/preferences/autofill/AutofillProfileEditor;->mNoCountryItemIsSelected:Z

    .line 96
    :goto_0
    return-void

    .line 95
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/autofill/AutofillProfileEditor;->enableSaveButton()V

    goto :goto_0
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0

    .prologue
    .line 99
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 85
    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/autofill/AutofillProfileEditor;->enableSaveButton()V

    .line 86
    return-void
.end method
