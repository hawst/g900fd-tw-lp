.class Lcom/google/android/apps/chrome/NativeInitializationController$1;
.super Ljava/lang/Thread;
.source "NativeInitializationController.java"


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/NativeInitializationController;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/NativeInitializationController;)V
    .locals 0

    .prologue
    .line 78
    iput-object p1, p0, Lcom/google/android/apps/chrome/NativeInitializationController$1;->this$0:Lcom/google/android/apps/chrome/NativeInitializationController;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 82
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/NativeInitializationController$1;->this$0:Lcom/google/android/apps/chrome/NativeInitializationController;

    # getter for: Lcom/google/android/apps/chrome/NativeInitializationController;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/google/android/apps/chrome/NativeInitializationController;->access$000(Lcom/google/android/apps/chrome/NativeInitializationController;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lorg/chromium/base/library_loader/LibraryLoader;->ensureInitialized(Landroid/content/Context;Z)V
    :try_end_0
    .catch Lorg/chromium/base/library_loader/ProcessInitException; {:try_start_0 .. :try_end_0} :catch_0

    .line 90
    iget-object v0, p0, Lcom/google/android/apps/chrome/NativeInitializationController$1;->this$0:Lcom/google/android/apps/chrome/NativeInitializationController;

    # getter for: Lcom/google/android/apps/chrome/NativeInitializationController;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/google/android/apps/chrome/NativeInitializationController;->access$000(Lcom/google/android/apps/chrome/NativeInitializationController;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/content/browser/ChildProcessLauncher;->warmUp(Landroid/content/Context;)V

    .line 91
    new-instance v0, Lcom/google/android/apps/chrome/NativeInitializationController$1$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/NativeInitializationController$1$1;-><init>(Lcom/google/android/apps/chrome/NativeInitializationController$1;)V

    invoke-static {v0}, Lorg/chromium/base/ThreadUtils;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 97
    :goto_0
    return-void

    .line 85
    :catch_0
    move-exception v0

    .line 86
    const-string/jumbo v1, "NativeInitializationController"

    const-string/jumbo v2, "Unable to load native library."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 87
    invoke-static {v0}, Lcom/google/android/apps/chrome/ChromeMobileApplication;->reportStartupErrorAndExit(Lorg/chromium/base/library_loader/ProcessInitException;)V

    goto :goto_0
.end method
