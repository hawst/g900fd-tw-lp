.class Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarkListAdapter$1;
.super Ljava/lang/Object;
.source "BookmarksPageView.java"

# interfaces
.implements Lorg/chromium/chrome/browser/favicon/FaviconHelper$FaviconImageCallback;


# instance fields
.field final synthetic this$1:Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarkListAdapter;

.field final synthetic val$bookmark:Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;

.field final synthetic val$item:Lcom/google/android/apps/chrome/ntp/BookmarkItemView;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarkListAdapter;Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;Lcom/google/android/apps/chrome/ntp/BookmarkItemView;)V
    .locals 0

    .prologue
    .line 357
    iput-object p1, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarkListAdapter$1;->this$1:Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarkListAdapter;

    iput-object p2, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarkListAdapter$1;->val$bookmark:Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;

    iput-object p3, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarkListAdapter$1;->val$item:Lcom/google/android/apps/chrome/ntp/BookmarkItemView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFaviconAvailable(Landroid/graphics/Bitmap;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 360
    if-nez p1, :cond_1

    .line 361
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarkListAdapter$1;->this$1:Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarkListAdapter;

    iget-object v0, v0, Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarkListAdapter;->this$0:Lcom/google/android/apps/chrome/ntp/BookmarksPageView;

    # getter for: Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->mDefaultFavicon:Landroid/graphics/Bitmap;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->access$700(Lcom/google/android/apps/chrome/ntp/BookmarksPageView;)Landroid/graphics/Bitmap;

    move-result-object v0

    if-nez v0, :cond_0

    .line 362
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarkListAdapter$1;->this$1:Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarkListAdapter;

    iget-object v0, v0, Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarkListAdapter;->this$0:Lcom/google/android/apps/chrome/ntp/BookmarksPageView;

    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarkListAdapter$1;->this$1:Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarkListAdapter;

    iget-object v1, v1, Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarkListAdapter;->this$0:Lcom/google/android/apps/chrome/ntp/BookmarksPageView;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/chrome/R$drawable;->default_favicon:I

    invoke-static {v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    # setter for: Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->mDefaultFavicon:Landroid/graphics/Bitmap;
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->access$702(Lcom/google/android/apps/chrome/ntp/BookmarksPageView;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 365
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarkListAdapter$1;->this$1:Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarkListAdapter;

    iget-object v0, v0, Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarkListAdapter;->this$0:Lcom/google/android/apps/chrome/ntp/BookmarksPageView;

    # getter for: Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->mDefaultFavicon:Landroid/graphics/Bitmap;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->access$700(Lcom/google/android/apps/chrome/ntp/BookmarksPageView;)Landroid/graphics/Bitmap;

    move-result-object p1

    .line 367
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarkListAdapter$1;->this$1:Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarkListAdapter;

    iget-object v0, v0, Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarkListAdapter;->this$0:Lcom/google/android/apps/chrome/ntp/BookmarksPageView;

    # getter for: Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->mFaviconCache:Landroid/util/LruCache;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->access$600(Lcom/google/android/apps/chrome/ntp/BookmarksPageView;)Landroid/util/LruCache;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarkListAdapter$1;->val$bookmark:Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;

    invoke-virtual {v1}, Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 371
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarkListAdapter$1;->val$bookmark:Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;->getUrl()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarkListAdapter$1;->val$item:Lcom/google/android/apps/chrome/ntp/BookmarkItemView;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/ntp/BookmarkItemView;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 372
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarkListAdapter$1;->val$item:Lcom/google/android/apps/chrome/ntp/BookmarkItemView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/ntp/BookmarkItemView;->setFavicon(Landroid/graphics/Bitmap;)V

    .line 373
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarkListAdapter$1;->this$1:Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarkListAdapter;

    iget-object v0, v0, Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarkListAdapter;->this$0:Lcom/google/android/apps/chrome/ntp/BookmarksPageView;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->mSnapshotBookmarksChanged:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->access$802(Lcom/google/android/apps/chrome/ntp/BookmarksPageView;Z)Z

    .line 375
    :cond_2
    return-void
.end method
