.class Lcom/google/android/apps/chrome/omnibox/SuggestionView$1;
.super Landroid/view/View;
.source "SuggestionView.java"


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/omnibox/SuggestionView;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/omnibox/SuggestionView;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 115
    iput-object p1, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$1;->this$0:Lcom/google/android/apps/chrome/omnibox/SuggestionView;

    invoke-direct {p0, p2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method protected drawableStateChanged()V
    .locals 2

    .prologue
    .line 144
    invoke-super {p0}, Landroid/view/View;->drawableStateChanged()V

    .line 146
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$1;->this$0:Lcom/google/android/apps/chrome/omnibox/SuggestionView;

    # getter for: Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mRefineIcon:Landroid/graphics/drawable/Drawable;
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->access$000(Lcom/google/android/apps/chrome/omnibox/SuggestionView;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$1;->this$0:Lcom/google/android/apps/chrome/omnibox/SuggestionView;

    # getter for: Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mRefineIcon:Landroid/graphics/drawable/Drawable;
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->access$000(Lcom/google/android/apps/chrome/omnibox/SuggestionView;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 147
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$1;->this$0:Lcom/google/android/apps/chrome/omnibox/SuggestionView;

    # getter for: Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mRefineIcon:Landroid/graphics/drawable/Drawable;
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->access$000(Lcom/google/android/apps/chrome/omnibox/SuggestionView;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView$1;->getDrawableState()[I

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 149
    :cond_0
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    .line 118
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 120
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$1;->this$0:Lcom/google/android/apps/chrome/omnibox/SuggestionView;

    # getter for: Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mRefineIcon:Landroid/graphics/drawable/Drawable;
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->access$000(Lcom/google/android/apps/chrome/omnibox/SuggestionView;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-nez v0, :cond_0

    .line 127
    :goto_0
    return-void

    .line 121
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 122
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView$1;->getMeasuredWidth()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$1;->this$0:Lcom/google/android/apps/chrome/omnibox/SuggestionView;

    # getter for: Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mRefineIcon:Landroid/graphics/drawable/Drawable;
    invoke-static {v1}, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->access$000(Lcom/google/android/apps/chrome/omnibox/SuggestionView;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    sub-int/2addr v0, v1

    int-to-float v0, v0

    div-float/2addr v0, v3

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView$1;->getMeasuredHeight()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$1;->this$0:Lcom/google/android/apps/chrome/omnibox/SuggestionView;

    # getter for: Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mRefineIcon:Landroid/graphics/drawable/Drawable;
    invoke-static {v2}, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->access$000(Lcom/google/android/apps/chrome/omnibox/SuggestionView;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    sub-int/2addr v1, v2

    int-to-float v1, v1

    div-float/2addr v1, v3

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 125
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$1;->this$0:Lcom/google/android/apps/chrome/omnibox/SuggestionView;

    # getter for: Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mRefineIcon:Landroid/graphics/drawable/Drawable;
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->access$000(Lcom/google/android/apps/chrome/omnibox/SuggestionView;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 126
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    goto :goto_0
.end method

.method public setVisibility(I)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 131
    invoke-super {p0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 133
    if-nez p1, :cond_0

    .line 134
    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/omnibox/SuggestionView$1;->setClickable(Z)V

    .line 135
    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/omnibox/SuggestionView$1;->setFocusable(Z)V

    .line 140
    :goto_0
    return-void

    .line 137
    :cond_0
    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView$1;->setClickable(Z)V

    .line 138
    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView$1;->setFocusable(Z)V

    goto :goto_0
.end method
