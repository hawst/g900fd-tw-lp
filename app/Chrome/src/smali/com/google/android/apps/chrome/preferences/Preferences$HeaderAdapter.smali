.class Lcom/google/android/apps/chrome/preferences/Preferences$HeaderAdapter;
.super Landroid/widget/ArrayAdapter;
.source "Preferences.java"


# static fields
.field static final synthetic $assertionsDisabled:Z

.field static final HEADER_TYPE_BITMAP:I = 0x3

.field static final HEADER_TYPE_CATEGORY:I = 0x0

.field static final HEADER_TYPE_CHILD_BITMAP:I = 0x4

.field static final HEADER_TYPE_NORMAL:I = 0x1

.field static final HEADER_TYPE_SWITCH:I = 0x2


# instance fields
.field private final mInflater:Landroid/view/LayoutInflater;

.field final synthetic this$0:Lcom/google/android/apps/chrome/preferences/Preferences;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 465
    const-class v0, Lcom/google/android/apps/chrome/preferences/Preferences;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/preferences/Preferences$HeaderAdapter;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/google/android/apps/chrome/preferences/Preferences;Landroid/content/Context;Ljava/util/List;)V
    .locals 1

    .prologue
    .line 537
    iput-object p1, p0, Lcom/google/android/apps/chrome/preferences/Preferences$HeaderAdapter;->this$0:Lcom/google/android/apps/chrome/preferences/Preferences;

    .line 538
    const/4 v0, 0x0

    invoke-direct {p0, p2, v0, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 539
    const-string/jumbo v0, "layout_inflater"

    invoke-virtual {p2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/google/android/apps/chrome/preferences/Preferences$HeaderAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 540
    return-void
.end method

.method private disableEnableView(Landroid/view/View;Ljava/lang/Boolean;)V
    .locals 2

    .prologue
    .line 506
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setEnabled(Z)V

    .line 507
    sget v0, Lcom/google/android/apps/chrome/R$id;->header_title:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 508
    sget v0, Lcom/google/android/apps/chrome/R$id;->header_summary:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 509
    return-void
.end method


# virtual methods
.method public areAllItemsEnabled()Z
    .locals 1

    .prologue
    .line 519
    const/4 v0, 0x0

    return v0
.end method

.method getHeaderType(Landroid/preference/PreferenceActivity$Header;)I
    .locals 6

    .prologue
    const/4 v0, 0x3

    .line 488
    iget-object v1, p1, Landroid/preference/PreferenceActivity$Header;->fragment:Ljava/lang/String;

    if-nez v1, :cond_1

    iget-object v1, p1, Landroid/preference/PreferenceActivity$Header;->intent:Landroid/content/Intent;

    if-nez v1, :cond_1

    .line 489
    const/4 v0, 0x0

    .line 501
    :cond_0
    :goto_0
    return v0

    .line 490
    :cond_1
    iget-wide v2, p1, Landroid/preference/PreferenceActivity$Header;->id:J

    sget v1, Lcom/google/android/apps/chrome/R$id;->sync_account:I

    int-to-long v4, v1

    cmp-long v1, v2, v4

    if-nez v1, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/Preferences$HeaderAdapter;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lorg/chromium/sync/signin/ChromeSigninController;->get(Landroid/content/Context;)Lorg/chromium/sync/signin/ChromeSigninController;

    move-result-object v1

    invoke-virtual {v1}, Lorg/chromium/sync/signin/ChromeSigninController;->isSignedIn()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 492
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/Preferences$HeaderAdapter;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/chrome/services/ChildAccountManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/services/ChildAccountManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/services/ChildAccountManager;->hasChildAccount()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 493
    const/4 v0, 0x4

    goto :goto_0

    .line 497
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/Preferences$HeaderAdapter;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-wide v2, p1, Landroid/preference/PreferenceActivity$Header;->id:J

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/chrome/preferences/ManagedPreferencesUtils;->isIdControlledByPolicy(Landroid/content/Context;J)Z

    move-result v1

    if-nez v1, :cond_0

    .line 501
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getItemViewType(I)I
    .locals 1

    .prologue
    .line 513
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/preferences/Preferences$HeaderAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceActivity$Header;

    .line 514
    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/Preferences$HeaderAdapter;->getHeaderType(Landroid/preference/PreferenceActivity$Header;)I

    move-result v0

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10

    .prologue
    .line 545
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/preferences/Preferences$HeaderAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceActivity$Header;

    .line 546
    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/Preferences$HeaderAdapter;->getHeaderType(Landroid/preference/PreferenceActivity$Header;)I

    move-result v5

    .line 547
    if-nez p2, :cond_1

    .line 550
    new-instance v3, Lcom/google/android/apps/chrome/preferences/Preferences$HeaderAdapter$HeaderViewHolder;

    const/4 v1, 0x0

    invoke-direct {v3, p0, v1}, Lcom/google/android/apps/chrome/preferences/Preferences$HeaderAdapter$HeaderViewHolder;-><init>(Lcom/google/android/apps/chrome/preferences/Preferences$HeaderAdapter;Lcom/google/android/apps/chrome/preferences/Preferences$1;)V

    .line 551
    packed-switch v5, :pswitch_data_0

    .line 590
    sget-boolean v0, Lcom/google/android/apps/chrome/preferences/Preferences$HeaderAdapter;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 553
    :pswitch_0
    new-instance v2, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/Preferences$HeaderAdapter;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v4, 0x0

    const v6, 0x1010208

    invoke-direct {v2, v1, v4, v6}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    move-object v1, v2

    .line 555
    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v3, Lcom/google/android/apps/chrome/preferences/Preferences$HeaderAdapter$HeaderViewHolder;->mTitle:Landroid/widget/TextView;

    .line 593
    :goto_0
    invoke-virtual {v2, v3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v4, v3

    move-object v3, v2

    .line 602
    :goto_1
    packed-switch v5, :pswitch_data_1

    .line 691
    sget-boolean v0, Lcom/google/android/apps/chrome/preferences/Preferences$HeaderAdapter;->$assertionsDisabled:Z

    if-nez v0, :cond_13

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 559
    :pswitch_1
    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/Preferences$HeaderAdapter;->mInflater:Landroid/view/LayoutInflater;

    sget v2, Lcom/google/android/apps/chrome/R$layout;->preference_header_switch_item_layout:I

    const/4 v4, 0x0

    invoke-virtual {v1, v2, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 561
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/Preferences$HeaderAdapter;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->changeViewStyle(Landroid/view/View;Landroid/content/Context;)V

    .line 562
    sget v1, Lcom/google/android/apps/chrome/R$id;->header_title:I

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v3, Lcom/google/android/apps/chrome/preferences/Preferences$HeaderAdapter$HeaderViewHolder;->mTitle:Landroid/widget/TextView;

    .line 563
    sget v1, Lcom/google/android/apps/chrome/R$id;->header_summary:I

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v3, Lcom/google/android/apps/chrome/preferences/Preferences$HeaderAdapter$HeaderViewHolder;->mSummary:Landroid/widget/TextView;

    goto :goto_0

    .line 567
    :pswitch_2
    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/Preferences$HeaderAdapter;->mInflater:Landroid/view/LayoutInflater;

    sget v2, Lcom/google/android/apps/chrome/R$layout;->preference_header_item_layout:I

    const/4 v4, 0x0

    invoke-virtual {v1, v2, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 569
    sget v1, Lcom/google/android/apps/chrome/R$id;->header_title:I

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v3, Lcom/google/android/apps/chrome/preferences/Preferences$HeaderAdapter$HeaderViewHolder;->mTitle:Landroid/widget/TextView;

    .line 570
    sget v1, Lcom/google/android/apps/chrome/R$id;->header_summary:I

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v3, Lcom/google/android/apps/chrome/preferences/Preferences$HeaderAdapter$HeaderViewHolder;->mSummary:Landroid/widget/TextView;

    goto :goto_0

    .line 574
    :pswitch_3
    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/Preferences$HeaderAdapter;->mInflater:Landroid/view/LayoutInflater;

    sget v2, Lcom/google/android/apps/chrome/R$layout;->preference_header_bitmap_item_layout:I

    const/4 v4, 0x0

    invoke-virtual {v1, v2, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 576
    sget v1, Lcom/google/android/apps/chrome/R$id;->header_title:I

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v3, Lcom/google/android/apps/chrome/preferences/Preferences$HeaderAdapter$HeaderViewHolder;->mTitle:Landroid/widget/TextView;

    .line 577
    sget v1, Lcom/google/android/apps/chrome/R$id;->header_summary:I

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v3, Lcom/google/android/apps/chrome/preferences/Preferences$HeaderAdapter$HeaderViewHolder;->mSummary:Landroid/widget/TextView;

    .line 578
    sget v1, Lcom/google/android/apps/chrome/R$id;->header_image:I

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, v3, Lcom/google/android/apps/chrome/preferences/Preferences$HeaderAdapter$HeaderViewHolder;->mImage:Landroid/widget/ImageView;

    goto :goto_0

    .line 582
    :pswitch_4
    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/Preferences$HeaderAdapter;->mInflater:Landroid/view/LayoutInflater;

    sget v2, Lcom/google/android/apps/chrome/R$layout;->preference_header_uca_bitmap_item_layout:I

    const/4 v4, 0x0

    invoke-virtual {v1, v2, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 584
    sget v1, Lcom/google/android/apps/chrome/R$id;->header_title:I

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v3, Lcom/google/android/apps/chrome/preferences/Preferences$HeaderAdapter$HeaderViewHolder;->mTitle:Landroid/widget/TextView;

    .line 585
    sget v1, Lcom/google/android/apps/chrome/R$id;->header_summary:I

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v3, Lcom/google/android/apps/chrome/preferences/Preferences$HeaderAdapter$HeaderViewHolder;->mSummary:Landroid/widget/TextView;

    .line 586
    sget v1, Lcom/google/android/apps/chrome/R$id;->header_image:I

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, v3, Lcom/google/android/apps/chrome/preferences/Preferences$HeaderAdapter$HeaderViewHolder;->mImage:Landroid/widget/ImageView;

    goto/16 :goto_0

    .line 591
    :cond_0
    const/4 v0, 0x0

    .line 695
    :goto_2
    return-object v0

    .line 596
    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/chrome/preferences/Preferences$HeaderAdapter$HeaderViewHolder;

    .line 597
    invoke-virtual {p2}, Landroid/view/View;->isEnabled()Z

    move-result v2

    if-nez v2, :cond_2

    .line 598
    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-direct {p0, p2, v2}, Lcom/google/android/apps/chrome/preferences/Preferences$HeaderAdapter;->disableEnableView(Landroid/view/View;Ljava/lang/Boolean;)V

    :cond_2
    move-object v3, p2

    move-object v4, v1

    goto/16 :goto_1

    .line 604
    :pswitch_5
    iget-object v1, v4, Lcom/google/android/apps/chrome/preferences/Preferences$HeaderAdapter$HeaderViewHolder;->mTitle:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/Preferences$HeaderAdapter;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/preference/PreferenceActivity$Header;->getTitle(Landroid/content/res/Resources;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_3
    :goto_3
    move-object v0, v3

    .line 695
    goto :goto_2

    .line 617
    :pswitch_6
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/Preferences$HeaderAdapter;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceActivity$Header;->getTitle(Landroid/content/res/Resources;)Ljava/lang/CharSequence;

    move-result-object v5

    .line 618
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/Preferences$HeaderAdapter;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceActivity$Header;->getSummary(Landroid/content/res/Resources;)Ljava/lang/CharSequence;

    move-result-object v2

    .line 619
    const/4 v1, 0x0

    .line 620
    iget-wide v6, v0, Landroid/preference/PreferenceActivity$Header;->id:J

    sget v8, Lcom/google/android/apps/chrome/R$id;->sync_account:I

    int-to-long v8, v8

    cmp-long v6, v6, v8

    if-nez v6, :cond_8

    .line 621
    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/Preferences$HeaderAdapter;->this$0:Lcom/google/android/apps/chrome/preferences/Preferences;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/preferences/Preferences;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lorg/chromium/sync/signin/ChromeSigninController;->get(Landroid/content/Context;)Lorg/chromium/sync/signin/ChromeSigninController;

    move-result-object v1

    invoke-virtual {v1}, Lorg/chromium/sync/signin/ChromeSigninController;->isSignedIn()Z

    move-result v1

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/Preferences$HeaderAdapter;->this$0:Lcom/google/android/apps/chrome/preferences/Preferences;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/preferences/Preferences;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lorg/chromium/chrome/browser/signin/SigninManager;->get(Landroid/content/Context;)Lorg/chromium/chrome/browser/signin/SigninManager;

    move-result-object v1

    invoke-virtual {v1}, Lorg/chromium/chrome/browser/signin/SigninManager;->isSignInAllowed()Z

    move-result v1

    if-nez v1, :cond_4

    .line 623
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-direct {p0, v3, v1}, Lcom/google/android/apps/chrome/preferences/Preferences$HeaderAdapter;->disableEnableView(Landroid/view/View;Ljava/lang/Boolean;)V

    .line 625
    :cond_4
    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/Preferences$HeaderAdapter;->this$0:Lcom/google/android/apps/chrome/preferences/Preferences;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/preferences/Preferences;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lorg/chromium/sync/signin/ChromeSigninController;->get(Landroid/content/Context;)Lorg/chromium/sync/signin/ChromeSigninController;

    move-result-object v1

    invoke-virtual {v1}, Lorg/chromium/sync/signin/ChromeSigninController;->getSignedInAccountName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->getCachedUserPicture(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 628
    if-nez v1, :cond_5

    .line 629
    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/Preferences$HeaderAdapter;->this$0:Lcom/google/android/apps/chrome/preferences/Preferences;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/preferences/Preferences;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v6, Lcom/google/android/apps/chrome/R$drawable;->account_management_no_picture:I

    invoke-static {v1, v6}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->makeRoundUserPicture(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 667
    :cond_5
    :goto_4
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/Preferences$HeaderAdapter;->getContext()Landroid/content/Context;

    move-result-object v6

    iget-wide v8, v0, Landroid/preference/PreferenceActivity$Header;->id:J

    invoke-static {v6, v8, v9}, Lcom/google/android/apps/chrome/preferences/ManagedPreferencesUtils;->isIdDisabledByPolicy(Landroid/content/Context;J)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 668
    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-direct {p0, v3, v6}, Lcom/google/android/apps/chrome/preferences/Preferences$HeaderAdapter;->disableEnableView(Landroid/view/View;Ljava/lang/Boolean;)V

    .line 671
    :cond_6
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/Preferences$HeaderAdapter;->getContext()Landroid/content/Context;

    move-result-object v6

    iget-wide v8, v0, Landroid/preference/PreferenceActivity$Header;->id:J

    invoke-static {v6, v8, v9}, Lcom/google/android/apps/chrome/preferences/ManagedPreferencesUtils;->isIdControlledByPolicy(Landroid/content/Context;J)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 672
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/Preferences$HeaderAdapter;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/preferences/ManagedPreferencesUtils;->getManagedByEnterpriseIcon(Landroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 675
    :cond_7
    iget-object v0, v4, Lcom/google/android/apps/chrome/preferences/Preferences$HeaderAdapter$HeaderViewHolder;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 676
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_11

    .line 677
    iget-object v0, v4, Lcom/google/android/apps/chrome/preferences/Preferences$HeaderAdapter$HeaderViewHolder;->mSummary:Landroid/widget/TextView;

    const/4 v5, 0x0

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 678
    iget-object v0, v4, Lcom/google/android/apps/chrome/preferences/Preferences$HeaderAdapter$HeaderViewHolder;->mSummary:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 682
    :goto_5
    if-eqz v1, :cond_12

    iget-object v0, v4, Lcom/google/android/apps/chrome/preferences/Preferences$HeaderAdapter$HeaderViewHolder;->mImage:Landroid/widget/ImageView;

    if-eqz v0, :cond_12

    .line 683
    iget-object v0, v4, Lcom/google/android/apps/chrome/preferences/Preferences$HeaderAdapter$HeaderViewHolder;->mImage:Landroid/widget/ImageView;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 684
    iget-object v0, v4, Lcom/google/android/apps/chrome/preferences/Preferences$HeaderAdapter$HeaderViewHolder;->mImage:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto/16 :goto_3

    .line 633
    :cond_8
    iget-wide v6, v0, Landroid/preference/PreferenceActivity$Header;->id:J

    sget v8, Lcom/google/android/apps/chrome/R$id;->document_mode:I

    int-to-long v8, v8

    cmp-long v6, v6, v8

    if-nez v6, :cond_9

    .line 634
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/Preferences$HeaderAdapter;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->isOptedOutOfDocumentMode()Z

    move-result v2

    if-nez v2, :cond_10

    .line 636
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/Preferences$HeaderAdapter;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v6, Lcom/google/android/apps/chrome/R$string;->text_on:I

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    goto :goto_4

    .line 640
    :cond_9
    iget-wide v6, v0, Landroid/preference/PreferenceActivity$Header;->id:J

    sget v8, Lcom/google/android/apps/chrome/R$id;->autofill_settings:I

    int-to-long v8, v8

    cmp-long v6, v6, v8

    if-nez v6, :cond_b

    .line 641
    invoke-static {}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager;->isAutofillEnabled()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 642
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/Preferences$HeaderAdapter;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v6, Lcom/google/android/apps/chrome/R$string;->text_on:I

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    goto/16 :goto_4

    .line 644
    :cond_a
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/Preferences$HeaderAdapter;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v6, Lcom/google/android/apps/chrome/R$string;->text_off:I

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    goto/16 :goto_4

    .line 646
    :cond_b
    iget-wide v6, v0, Landroid/preference/PreferenceActivity$Header;->id:J

    sget v8, Lcom/google/android/apps/chrome/R$id;->manage_saved_passwords:I

    int-to-long v8, v8

    cmp-long v6, v6, v8

    if-nez v6, :cond_d

    .line 647
    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->isRememberPasswordsEnabled()Z

    move-result v2

    if-eqz v2, :cond_c

    .line 648
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/Preferences$HeaderAdapter;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v6, Lcom/google/android/apps/chrome/R$string;->text_on:I

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    goto/16 :goto_4

    .line 650
    :cond_c
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/Preferences$HeaderAdapter;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v6, Lcom/google/android/apps/chrome/R$string;->text_off:I

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    goto/16 :goto_4

    .line 652
    :cond_d
    iget-wide v6, v0, Landroid/preference/PreferenceActivity$Header;->id:J

    sget v8, Lcom/google/android/apps/chrome/R$id;->do_not_track:I

    int-to-long v8, v8

    cmp-long v6, v6, v8

    if-nez v6, :cond_f

    .line 653
    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->isDoNotTrackEnabled()Z

    move-result v2

    if-eqz v2, :cond_e

    .line 654
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/Preferences$HeaderAdapter;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v6, Lcom/google/android/apps/chrome/R$string;->text_on:I

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    goto/16 :goto_4

    .line 656
    :cond_e
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/Preferences$HeaderAdapter;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v6, Lcom/google/android/apps/chrome/R$string;->text_off:I

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    goto/16 :goto_4

    .line 658
    :cond_f
    iget-wide v6, v0, Landroid/preference/PreferenceActivity$Header;->id:J

    sget v8, Lcom/google/android/apps/chrome/R$id;->homepage:I

    int-to-long v8, v8

    cmp-long v6, v6, v8

    if-nez v6, :cond_5

    .line 659
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/Preferences$HeaderAdapter;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getPrefHomepagePreference()Z

    move-result v2

    if-eqz v2, :cond_10

    .line 661
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/Preferences$HeaderAdapter;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v6, Lcom/google/android/apps/chrome/R$string;->text_on:I

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    goto/16 :goto_4

    .line 663
    :cond_10
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/Preferences$HeaderAdapter;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v6, Lcom/google/android/apps/chrome/R$string;->text_off:I

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    goto/16 :goto_4

    .line 680
    :cond_11
    iget-object v0, v4, Lcom/google/android/apps/chrome/preferences/Preferences$HeaderAdapter$HeaderViewHolder;->mSummary:Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_5

    .line 685
    :cond_12
    iget-object v0, v4, Lcom/google/android/apps/chrome/preferences/Preferences$HeaderAdapter$HeaderViewHolder;->mImage:Landroid/widget/ImageView;

    if-eqz v0, :cond_3

    .line 686
    iget-object v0, v4, Lcom/google/android/apps/chrome/preferences/Preferences$HeaderAdapter$HeaderViewHolder;->mImage:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_3

    .line 692
    :cond_13
    const/4 v0, 0x0

    goto/16 :goto_2

    .line 551
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
        :pswitch_4
    .end packed-switch

    .line 602
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_5
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
    .end packed-switch
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 529
    const/4 v0, 0x5

    return v0
.end method

.method public hasStableIds()Z
    .locals 1

    .prologue
    .line 534
    const/4 v0, 0x1

    return v0
.end method

.method public isEnabled(I)Z
    .locals 1

    .prologue
    .line 524
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/preferences/Preferences$HeaderAdapter;->getItemViewType(I)I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
