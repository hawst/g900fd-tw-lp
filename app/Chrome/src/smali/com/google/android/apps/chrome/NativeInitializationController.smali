.class Lcom/google/android/apps/chrome/NativeInitializationController;
.super Ljava/lang/Object;
.source "NativeInitializationController.java"


# instance fields
.field private final mActivityDelegate:Lcom/google/android/apps/chrome/ChromeActivityNativeDelegate;

.field private final mContext:Landroid/content/Context;

.field private final mHandler:Landroid/os/Handler;

.field private mInitializationComplete:Z

.field private mOnResumePending:Z

.field private mOnStartPending:Z

.field private mPendingActivityResults:Ljava/util/List;

.field private mPendingNewIntents:Ljava/util/List;

.field private mWaitingForFirstDraw:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/chrome/ChromeActivityNativeDelegate;)V
    .locals 2

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/NativeInitializationController;->mContext:Landroid/content/Context;

    .line 68
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/NativeInitializationController;->mHandler:Landroid/os/Handler;

    .line 69
    iput-object p2, p0, Lcom/google/android/apps/chrome/NativeInitializationController;->mActivityDelegate:Lcom/google/android/apps/chrome/ChromeActivityNativeDelegate;

    .line 70
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/NativeInitializationController;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/android/apps/chrome/NativeInitializationController;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/NativeInitializationController;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/google/android/apps/chrome/NativeInitializationController;->onLibraryLoaded()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/NativeInitializationController;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/google/android/apps/chrome/NativeInitializationController;->onNativeLibraryLoaded()V

    return-void
.end method

.method private onLibraryLoaded()V
    .locals 2

    .prologue
    .line 102
    iget-object v0, p0, Lcom/google/android/apps/chrome/NativeInitializationController;->mActivityDelegate:Lcom/google/android/apps/chrome/ChromeActivityNativeDelegate;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/ChromeActivityNativeDelegate;->hasDoneFirstDraw()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lorg/chromium/base/CommandLine;->getInstance()Lorg/chromium/base/CommandLine;

    move-result-object v0

    const-string/jumbo v1, "take-surface"

    invoke-virtual {v0, v1}, Lorg/chromium/base/CommandLine;->hasSwitch(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 105
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/NativeInitializationController;->onNativeLibraryLoaded()V

    .line 109
    :goto_0
    return-void

    .line 107
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/NativeInitializationController;->mWaitingForFirstDraw:Z

    goto :goto_0
.end method

.method private onNativeLibraryLoaded()V
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/google/android/apps/chrome/NativeInitializationController;->mActivityDelegate:Lcom/google/android/apps/chrome/ChromeActivityNativeDelegate;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/ChromeActivityNativeDelegate;->isActivityDestroyed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 137
    :goto_0
    return-void

    .line 136
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/NativeInitializationController;->mActivityDelegate:Lcom/google/android/apps/chrome/ChromeActivityNativeDelegate;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/ChromeActivityNativeDelegate;->onCreateWithNative()V

    goto :goto_0
.end method

.method private startNowAndProcessPendingItems()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 237
    iget-object v0, p0, Lcom/google/android/apps/chrome/NativeInitializationController;->mActivityDelegate:Lcom/google/android/apps/chrome/ChromeActivityNativeDelegate;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/ChromeActivityNativeDelegate;->onStartWithNative()V

    .line 239
    iget-object v0, p0, Lcom/google/android/apps/chrome/NativeInitializationController;->mPendingNewIntents:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 240
    iget-object v0, p0, Lcom/google/android/apps/chrome/NativeInitializationController;->mPendingNewIntents:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    .line 241
    iget-object v2, p0, Lcom/google/android/apps/chrome/NativeInitializationController;->mActivityDelegate:Lcom/google/android/apps/chrome/ChromeActivityNativeDelegate;

    invoke-interface {v2, v0}, Lcom/google/android/apps/chrome/ChromeActivityNativeDelegate;->onNewIntentWithNative(Landroid/content/Intent;)V

    goto :goto_0

    .line 243
    :cond_0
    iput-object v5, p0, Lcom/google/android/apps/chrome/NativeInitializationController;->mPendingNewIntents:Ljava/util/List;

    .line 246
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/NativeInitializationController;->mPendingActivityResults:Ljava/util/List;

    if-eqz v0, :cond_3

    .line 248
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/NativeInitializationController;->mPendingActivityResults:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 249
    iget-object v0, p0, Lcom/google/android/apps/chrome/NativeInitializationController;->mPendingActivityResults:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/NativeInitializationController$ActivityResult;

    .line 250
    iget-object v2, p0, Lcom/google/android/apps/chrome/NativeInitializationController;->mActivityDelegate:Lcom/google/android/apps/chrome/ChromeActivityNativeDelegate;

    iget v3, v0, Lcom/google/android/apps/chrome/NativeInitializationController$ActivityResult;->requestCode:I

    iget v4, v0, Lcom/google/android/apps/chrome/NativeInitializationController$ActivityResult;->resultCode:I

    iget-object v0, v0, Lcom/google/android/apps/chrome/NativeInitializationController$ActivityResult;->data:Landroid/content/Intent;

    invoke-interface {v2, v3, v4, v0}, Lcom/google/android/apps/chrome/ChromeActivityNativeDelegate;->onActivityResultWithNative(IILandroid/content/Intent;)Z

    .line 248
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 253
    :cond_2
    iput-object v5, p0, Lcom/google/android/apps/chrome/NativeInitializationController;->mPendingActivityResults:Ljava/util/List;

    .line 255
    :cond_3
    return-void
.end method


# virtual methods
.method public firstDrawComplete()V
    .locals 2

    .prologue
    .line 118
    const/16 v0, 0x40

    invoke-static {v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastImmediateNotification(I)V

    .line 121
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/NativeInitializationController;->mWaitingForFirstDraw:Z

    if-eqz v0, :cond_0

    .line 122
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/NativeInitializationController;->mWaitingForFirstDraw:Z

    .line 124
    iget-object v0, p0, Lcom/google/android/apps/chrome/NativeInitializationController;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/chrome/NativeInitializationController$2;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/NativeInitializationController$2;-><init>(Lcom/google/android/apps/chrome/NativeInitializationController;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 131
    :cond_0
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    .line 219
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/NativeInitializationController;->mInitializationComplete:Z

    if-eqz v0, :cond_0

    .line 220
    iget-object v0, p0, Lcom/google/android/apps/chrome/NativeInitializationController;->mActivityDelegate:Lcom/google/android/apps/chrome/ChromeActivityNativeDelegate;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/apps/chrome/ChromeActivityNativeDelegate;->onActivityResultWithNative(IILandroid/content/Intent;)Z

    .line 227
    :goto_0
    return-void

    .line 222
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/NativeInitializationController;->mPendingActivityResults:Ljava/util/List;

    if-nez v0, :cond_1

    .line 223
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/NativeInitializationController;->mPendingActivityResults:Ljava/util/List;

    .line 225
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/NativeInitializationController;->mPendingActivityResults:Ljava/util/List;

    new-instance v1, Lcom/google/android/apps/chrome/NativeInitializationController$ActivityResult;

    invoke-direct {v1, p1, p2, p3}, Lcom/google/android/apps/chrome/NativeInitializationController$ActivityResult;-><init>(IILandroid/content/Intent;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public onNativeInitializationComplete()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 144
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/NativeInitializationController;->mInitializationComplete:Z

    .line 146
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/NativeInitializationController;->mOnStartPending:Z

    if-eqz v0, :cond_0

    .line 147
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/NativeInitializationController;->mOnStartPending:Z

    .line 148
    invoke-direct {p0}, Lcom/google/android/apps/chrome/NativeInitializationController;->startNowAndProcessPendingItems()V

    .line 151
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/NativeInitializationController;->mOnResumePending:Z

    if-eqz v0, :cond_1

    .line 152
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/NativeInitializationController;->mOnResumePending:Z

    .line 153
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/NativeInitializationController;->onResume()V

    .line 156
    :cond_1
    invoke-static {}, Lorg/chromium/base/library_loader/LibraryLoader;->onNativeInitializationComplete()V

    .line 157
    return-void
.end method

.method public onNewIntent(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 203
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/NativeInitializationController;->mInitializationComplete:Z

    if-eqz v0, :cond_0

    .line 204
    iget-object v0, p0, Lcom/google/android/apps/chrome/NativeInitializationController;->mActivityDelegate:Lcom/google/android/apps/chrome/ChromeActivityNativeDelegate;

    invoke-interface {v0, p1}, Lcom/google/android/apps/chrome/ChromeActivityNativeDelegate;->onNewIntentWithNative(Landroid/content/Intent;)V

    .line 209
    :goto_0
    return-void

    .line 206
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/NativeInitializationController;->mPendingNewIntents:Ljava/util/List;

    if-nez v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/NativeInitializationController;->mPendingNewIntents:Ljava/util/List;

    .line 207
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/NativeInitializationController;->mPendingNewIntents:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 185
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/NativeInitializationController;->mOnResumePending:Z

    .line 186
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/NativeInitializationController;->mInitializationComplete:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/NativeInitializationController;->mActivityDelegate:Lcom/google/android/apps/chrome/ChromeActivityNativeDelegate;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/ChromeActivityNativeDelegate;->onPauseWithNative()V

    .line 187
    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 174
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/NativeInitializationController;->mInitializationComplete:Z

    if-eqz v0, :cond_0

    .line 175
    iget-object v0, p0, Lcom/google/android/apps/chrome/NativeInitializationController;->mActivityDelegate:Lcom/google/android/apps/chrome/ChromeActivityNativeDelegate;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/ChromeActivityNativeDelegate;->onResumeWithNative()V

    .line 179
    :goto_0
    return-void

    .line 177
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/NativeInitializationController;->mOnResumePending:Z

    goto :goto_0
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 163
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/NativeInitializationController;->mInitializationComplete:Z

    if-eqz v0, :cond_0

    .line 164
    invoke-direct {p0}, Lcom/google/android/apps/chrome/NativeInitializationController;->startNowAndProcessPendingItems()V

    .line 168
    :goto_0
    return-void

    .line 166
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/NativeInitializationController;->mOnStartPending:Z

    goto :goto_0
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 193
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/NativeInitializationController;->mOnStartPending:Z

    .line 194
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/NativeInitializationController;->mInitializationComplete:Z

    if-nez v0, :cond_0

    .line 196
    :goto_0
    return-void

    .line 195
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/NativeInitializationController;->mActivityDelegate:Lcom/google/android/apps/chrome/ChromeActivityNativeDelegate;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/ChromeActivityNativeDelegate;->onStopWithNative()V

    goto :goto_0
.end method

.method public startBackgroundTasks()V
    .locals 1

    .prologue
    .line 78
    new-instance v0, Lcom/google/android/apps/chrome/NativeInitializationController$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/NativeInitializationController$1;-><init>(Lcom/google/android/apps/chrome/NativeInitializationController;)V

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/NativeInitializationController$1;->start()V

    .line 99
    return-void
.end method
