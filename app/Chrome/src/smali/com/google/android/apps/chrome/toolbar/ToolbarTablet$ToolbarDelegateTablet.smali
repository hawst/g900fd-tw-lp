.class Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$ToolbarDelegateTablet;
.super Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;
.source "ToolbarTablet.java"


# instance fields
.field private mUseLightColorAssets:Ljava/lang/Boolean;

.field final synthetic this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;)V
    .locals 0

    .prologue
    .line 306
    iput-object p1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$ToolbarDelegateTablet;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;

    .line 307
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;-><init>(Landroid/view/View;)V

    .line 308
    return-void
.end method


# virtual methods
.method public onAccessibilityStatusChanged(Z)V
    .locals 3

    .prologue
    .line 423
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$ToolbarDelegateTablet;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;

    if-nez p1, :cond_0

    invoke-static {}, Lorg/chromium/base/CommandLine;->getInstance()Lorg/chromium/base/CommandLine;

    move-result-object v0

    const-string/jumbo v2, "enable-tablet-tab-stack"

    invoke-virtual {v0, v2}, Lorg/chromium/base/CommandLine;->hasSwitch(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    # setter for: Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mShowTabStack:Z
    invoke-static {v1, v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->access$1002(Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;Z)Z

    .line 425
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$ToolbarDelegateTablet;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;

    # invokes: Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->updateSwitcherButtonVisibility(Z)V
    invoke-static {v0, p1}, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->access$1100(Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;Z)V

    .line 426
    return-void

    .line 423
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onBookmarkUiVisibilityChange(Z)V
    .locals 1

    .prologue
    .line 418
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$ToolbarDelegateTablet;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mBookmarkButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->access$900(Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 419
    return-void
.end method

.method protected onHomeButtonUpdate(Z)V
    .locals 2

    .prologue
    .line 440
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$ToolbarDelegateTablet;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mHomeButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->access$200(Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;)Landroid/widget/ImageButton;

    move-result-object v1

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 441
    return-void

    .line 440
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public onNativeLibraryReady()V
    .locals 2

    .prologue
    .line 312
    invoke-super {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->onNativeLibraryReady()V

    .line 314
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$ToolbarDelegateTablet;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/partnercustomizations/HomepageManager;->isHomepageEnabled(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 315
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$ToolbarDelegateTablet;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mHomeButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->access$200(Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;)Landroid/widget/ImageButton;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 317
    :cond_0
    return-void
.end method

.method public onTabOrModelChanged()V
    .locals 3

    .prologue
    .line 321
    invoke-super {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->onTabOrModelChanged()V

    .line 323
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$ToolbarDelegateTablet;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mToolbarDelegate:Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$ToolbarDelegateTablet;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->access$300(Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;)Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$ToolbarDelegateTablet;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$ToolbarDelegateTablet;->getToolbarDataProvider()Lcom/google/android/apps/chrome/toolbar/ToolbarDataProvider;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarDataProvider;->isIncognito()Z

    move-result v1

    .line 324
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$ToolbarDelegateTablet;->mUseLightColorAssets:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$ToolbarDelegateTablet;->mUseLightColorAssets:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eq v0, v1, :cond_1

    .line 325
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$ToolbarDelegateTablet;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;

    if-eqz v1, :cond_2

    sget v0, Lcom/google/android/apps/chrome/R$color;->incognito_primary_color:I

    :goto_0
    invoke-virtual {v2, v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->setBackgroundResource(I)V

    .line 327
    iget-object v2, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$ToolbarDelegateTablet;->mMenuButton:Landroid/widget/ImageButton;

    if-eqz v1, :cond_3

    sget v0, Lcom/google/android/apps/chrome/R$drawable;->btn_menu_white:I

    :goto_1
    invoke-virtual {v2, v0}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 329
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$ToolbarDelegateTablet;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mHomeButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->access$200(Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;)Landroid/widget/ImageButton;

    move-result-object v2

    if-eqz v1, :cond_4

    sget v0, Lcom/google/android/apps/chrome/R$drawable;->btn_toolbar_home_white:I

    :goto_2
    invoke-virtual {v2, v0}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 331
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$ToolbarDelegateTablet;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mBackButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->access$000(Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;)Landroid/widget/ImageButton;

    move-result-object v2

    if-eqz v1, :cond_5

    sget v0, Lcom/google/android/apps/chrome/R$drawable;->btn_back_white:I

    :goto_3
    invoke-virtual {v2, v0}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 333
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$ToolbarDelegateTablet;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mForwardButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->access$100(Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;)Landroid/widget/ImageButton;

    move-result-object v2

    if-eqz v1, :cond_6

    sget v0, Lcom/google/android/apps/chrome/R$drawable;->btn_forward_white:I

    :goto_4
    invoke-virtual {v2, v0}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 335
    if-eqz v1, :cond_7

    .line 336
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$ToolbarDelegateTablet;->mLocationBar:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    const/16 v2, 0x33

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 341
    :goto_5
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$ToolbarDelegateTablet;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mAccessibilitySwitcherButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->access$600(Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;)Landroid/widget/ImageButton;

    move-result-object v2

    if-eqz v1, :cond_8

    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$ToolbarDelegateTablet;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mTabSwitcherButtonDrawableLight:Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->access$400(Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;)Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;

    move-result-object v0

    :goto_6
    invoke-virtual {v2, v0}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 343
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$ToolbarDelegateTablet;->mLocationBar:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->updateVisualsForState()V

    .line 344
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$ToolbarDelegateTablet;->mUseLightColorAssets:Ljava/lang/Boolean;

    .line 346
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$ToolbarDelegateTablet;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;

    sget v1, Lcom/google/android/apps/chrome/R$id;->url_bar:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->clearFocus()V

    .line 347
    return-void

    .line 325
    :cond_2
    sget v0, Lcom/google/android/apps/chrome/R$color;->default_primary_color:I

    goto :goto_0

    .line 327
    :cond_3
    sget v0, Lcom/google/android/apps/chrome/R$drawable;->btn_menu:I

    goto :goto_1

    .line 329
    :cond_4
    sget v0, Lcom/google/android/apps/chrome/R$drawable;->btn_toolbar_home:I

    goto :goto_2

    .line 331
    :cond_5
    sget v0, Lcom/google/android/apps/chrome/R$drawable;->btn_back:I

    goto :goto_3

    .line 333
    :cond_6
    sget v0, Lcom/google/android/apps/chrome/R$drawable;->btn_forward:I

    goto :goto_4

    .line 339
    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$ToolbarDelegateTablet;->mLocationBar:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    const/16 v2, 0xff

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    goto :goto_5

    .line 341
    :cond_8
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$ToolbarDelegateTablet;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mTabSwitcherButtonDrawable:Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->access$500(Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;)Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;

    move-result-object v0

    goto :goto_6
.end method

.method public setBookmarkClickHandler(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 430
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$ToolbarDelegateTablet;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;

    # setter for: Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mBookmarkListener:Landroid/view/View$OnClickListener;
    invoke-static {v0, p1}, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->access$1202(Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;Landroid/view/View$OnClickListener;)Landroid/view/View$OnClickListener;

    .line 431
    return-void
.end method

.method public setOnOverviewClickHandler(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 435
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$ToolbarDelegateTablet;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;

    # setter for: Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mOverviewListener:Landroid/view/View$OnClickListener;
    invoke-static {v0, p1}, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->access$1302(Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;Landroid/view/View$OnClickListener;)Landroid/view/View$OnClickListener;

    .line 436
    return-void
.end method

.method protected setOverviewMode(ZLandroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 395
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$ToolbarDelegateTablet;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mShowTabStack:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->access$1000(Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;)Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 396
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$ToolbarDelegateTablet;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mInOverviewMode:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->access$702(Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;Z)Z

    .line 397
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$ToolbarDelegateTablet;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mBackButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->access$000(Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 398
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$ToolbarDelegateTablet;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mForwardButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->access$100(Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 399
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$ToolbarDelegateTablet;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mReloadButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->access$800(Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 400
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$ToolbarDelegateTablet;->mLocationBar:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->setVisibility(I)V

    .line 405
    :goto_0
    return-void

    .line 402
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$ToolbarDelegateTablet;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;

    # setter for: Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mInOverviewMode:Z
    invoke-static {v0, v2}, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->access$702(Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;Z)Z

    .line 403
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$ToolbarDelegateTablet;->mLocationBar:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->setVisibility(I)V

    goto :goto_0
.end method

.method protected updateBackButtonVisibility(Z)V
    .locals 2

    .prologue
    .line 351
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$ToolbarDelegateTablet;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mInOverviewMode:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->access$700(Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 352
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$ToolbarDelegateTablet;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mBackButton:Landroid/widget/ImageButton;
    invoke-static {v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->access$000(Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;)Landroid/widget/ImageButton;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 353
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$ToolbarDelegateTablet;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mBackButton:Landroid/widget/ImageButton;
    invoke-static {v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->access$000(Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;)Landroid/widget/ImageButton;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 354
    return-void

    .line 351
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected updateBookmarkButtonVisibility(Z)V
    .locals 2

    .prologue
    .line 383
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$ToolbarDelegateTablet;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mToolbarDelegate:Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$ToolbarDelegateTablet;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->access$300(Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;)Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$ToolbarDelegateTablet;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$ToolbarDelegateTablet;->getToolbarDataProvider()Lcom/google/android/apps/chrome/toolbar/ToolbarDataProvider;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarDataProvider;->isIncognito()Z

    move-result v0

    .line 384
    if-eqz p1, :cond_1

    .line 385
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$ToolbarDelegateTablet;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mBookmarkButton:Landroid/widget/ImageButton;
    invoke-static {v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->access$900(Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;)Landroid/widget/ImageButton;

    move-result-object v1

    if-eqz v0, :cond_0

    sget v0, Lcom/google/android/apps/chrome/R$drawable;->btn_star_white_filled:I

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 391
    :goto_1
    return-void

    .line 385
    :cond_0
    sget v0, Lcom/google/android/apps/chrome/R$drawable;->btn_star_filled:I

    goto :goto_0

    .line 388
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$ToolbarDelegateTablet;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mBookmarkButton:Landroid/widget/ImageButton;
    invoke-static {v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->access$900(Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;)Landroid/widget/ImageButton;

    move-result-object v1

    if-eqz v0, :cond_2

    sget v0, Lcom/google/android/apps/chrome/R$drawable;->btn_star_white:I

    :goto_2
    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setImageResource(I)V

    goto :goto_1

    :cond_2
    sget v0, Lcom/google/android/apps/chrome/R$drawable;->btn_star:I

    goto :goto_2
.end method

.method protected updateForwardButtonVisibility(Z)V
    .locals 2

    .prologue
    .line 358
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$ToolbarDelegateTablet;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mInOverviewMode:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->access$700(Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 359
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$ToolbarDelegateTablet;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mForwardButton:Landroid/widget/ImageButton;
    invoke-static {v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->access$100(Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;)Landroid/widget/ImageButton;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 360
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$ToolbarDelegateTablet;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mForwardButton:Landroid/widget/ImageButton;
    invoke-static {v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->access$100(Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;)Landroid/widget/ImageButton;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 361
    return-void

    .line 358
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected updateReloadButtonVisibility(Z)V
    .locals 3

    .prologue
    .line 365
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$ToolbarDelegateTablet;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mToolbarDelegate:Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$ToolbarDelegateTablet;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->access$300(Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;)Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$ToolbarDelegateTablet;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$ToolbarDelegateTablet;->getToolbarDataProvider()Lcom/google/android/apps/chrome/toolbar/ToolbarDataProvider;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarDataProvider;->isIncognito()Z

    move-result v0

    .line 366
    if-eqz p1, :cond_1

    .line 367
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$ToolbarDelegateTablet;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mReloadButton:Landroid/widget/ImageButton;
    invoke-static {v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->access$800(Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;)Landroid/widget/ImageButton;

    move-result-object v1

    if-eqz v0, :cond_0

    sget v0, Lcom/google/android/apps/chrome/R$drawable;->btn_toolbar_stop_loading_white:I

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 370
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$ToolbarDelegateTablet;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mReloadButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->access$800(Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;)Landroid/widget/ImageButton;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$ToolbarDelegateTablet;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/google/android/apps/chrome/R$string;->accessibility_btn_stop_loading:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 378
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$ToolbarDelegateTablet;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mReloadButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->access$800(Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;)Landroid/widget/ImageButton;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$ToolbarDelegateTablet;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mInOverviewMode:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->access$700(Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;)Z

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x1

    :goto_2
    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 379
    return-void

    .line 367
    :cond_0
    sget v0, Lcom/google/android/apps/chrome/R$drawable;->btn_toolbar_stop_loading:I

    goto :goto_0

    .line 373
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$ToolbarDelegateTablet;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mReloadButton:Landroid/widget/ImageButton;
    invoke-static {v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->access$800(Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;)Landroid/widget/ImageButton;

    move-result-object v1

    if-eqz v0, :cond_2

    sget v0, Lcom/google/android/apps/chrome/R$drawable;->btn_toolbar_reload_white:I

    :goto_3
    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 375
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$ToolbarDelegateTablet;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mReloadButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->access$800(Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;)Landroid/widget/ImageButton;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$ToolbarDelegateTablet;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/google/android/apps/chrome/R$string;->accessibility_btn_refresh:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 373
    :cond_2
    sget v0, Lcom/google/android/apps/chrome/R$drawable;->btn_toolbar_reload:I

    goto :goto_3

    .line 378
    :cond_3
    const/4 v0, 0x0

    goto :goto_2
.end method

.method protected updateTabCountVisuals(I)V
    .locals 6

    .prologue
    .line 409
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$ToolbarDelegateTablet;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mAccessibilitySwitcherButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->access$600(Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;)Landroid/widget/ImageButton;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$ToolbarDelegateTablet;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/chrome/R$string;->accessibility_toolbar_btn_tabswitcher_toggle:I

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 412
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$ToolbarDelegateTablet;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mTabSwitcherButtonDrawable:Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->access$500(Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;)Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$ToolbarDelegateTablet;->isIncognito()Z

    move-result v1

    invoke-virtual {v0, p1, v1}, Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;->updateForTabCount(IZ)V

    .line 413
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$ToolbarDelegateTablet;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mTabSwitcherButtonDrawableLight:Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->access$400(Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;)Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$ToolbarDelegateTablet;->isIncognito()Z

    move-result v1

    invoke-virtual {v0, p1, v1}, Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;->updateForTabCount(IZ)V

    .line 414
    return-void
.end method
