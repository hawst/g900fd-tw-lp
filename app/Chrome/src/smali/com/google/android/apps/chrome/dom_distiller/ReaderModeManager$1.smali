.class Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager$1;
.super Lorg/chromium/content/browser/WebContentsObserverAndroid;
.source "ReaderModeManager.java"


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;Lorg/chromium/content_public/browser/WebContents;)V
    .locals 0

    .prologue
    .line 97
    iput-object p1, p0, Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager$1;->this$0:Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;

    invoke-direct {p0, p2}, Lorg/chromium/content/browser/WebContentsObserverAndroid;-><init>(Lorg/chromium/content_public/browser/WebContents;)V

    return-void
.end method


# virtual methods
.method public didFinishLoad(JLjava/lang/String;Z)V
    .locals 2

    .prologue
    .line 100
    if-nez p4, :cond_1

    .line 103
    :cond_0
    :goto_0
    return-void

    .line 101
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager$1;->this$0:Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;

    # getter for: Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;->mTab:Lcom/google/android/apps/chrome/tab/ChromeTab;
    invoke-static {v0}, Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;->access$000(Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;)Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getUrl()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/components/dom_distiller/core/DomDistillerUrlUtils;->isDistilledPage(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 102
    iget-object v0, p0, Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager$1;->this$0:Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;

    const/4 v1, 0x1

    # invokes: Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;->updateStatusBasedOnReaderModeCriteria(Z)V
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;->access$100(Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;Z)V

    goto :goto_0
.end method

.method public didNavigateMainFrame(Ljava/lang/String;Ljava/lang/String;ZZ)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 120
    invoke-static {p1}, Lorg/chromium/components/dom_distiller/core/DomDistillerUrlUtils;->isDistilledPage(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 121
    iget-object v0, p0, Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager$1;->this$0:Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;

    # getter for: Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;->mTab:Lcom/google/android/apps/chrome/tab/ChromeTab;
    invoke-static {v0}, Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;->access$000(Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;)Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/tab/ChromeTab;->updateTopControlsState(IZ)V

    .line 135
    :goto_0
    return-void

    .line 125
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager$1;->this$0:Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;

    # setter for: Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;->mReaderModeStatus:I
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;->access$202(Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;I)I

    .line 126
    iget-object v0, p0, Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager$1;->this$0:Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;

    # getter for: Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;->mReaderModePageUrl:Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;->access$400(Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/components/dom_distiller/core/DomDistillerUrlUtils;->getOriginalUrlFromDistillerUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 129
    iget-object v0, p0, Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager$1;->this$0:Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;

    # setter for: Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;->mReaderModeStatus:I
    invoke-static {v0, v2}, Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;->access$202(Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;I)I

    .line 130
    iget-object v0, p0, Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager$1;->this$0:Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;

    # setter for: Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;->mIsUmaRecorded:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;->access$502(Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;Z)Z

    .line 131
    iget-object v0, p0, Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager$1;->this$0:Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;

    # invokes: Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;->updateStatusBasedOnReaderModeCriteria(Z)V
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;->access$100(Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;Z)V

    .line 133
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager$1;->this$0:Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;->mReaderModePageUrl:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;->access$402(Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;Ljava/lang/String;)Ljava/lang/String;

    .line 134
    iget-object v0, p0, Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager$1;->this$0:Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;

    # invokes: Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;->sendReaderModeStatusChangedNotification()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;->access$300(Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;)V

    goto :goto_0
.end method

.method public didStartProvisionalLoadForFrame(JJZLjava/lang/String;ZZ)V
    .locals 2

    .prologue
    .line 109
    if-nez p5, :cond_1

    .line 115
    :cond_0
    :goto_0
    return-void

    .line 110
    :cond_1
    invoke-static {p6}, Lorg/chromium/components/dom_distiller/core/DomDistillerUrlUtils;->isDistilledPage(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 111
    iget-object v0, p0, Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager$1;->this$0:Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;

    const/4 v1, 0x2

    # setter for: Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;->mReaderModeStatus:I
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;->access$202(Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;I)I

    .line 112
    iget-object v0, p0, Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager$1;->this$0:Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;

    # invokes: Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;->sendReaderModeStatusChangedNotification()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;->access$300(Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;)V

    .line 113
    iget-object v0, p0, Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager$1;->this$0:Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;

    # setter for: Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;->mReaderModePageUrl:Ljava/lang/String;
    invoke-static {v0, p6}, Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;->access$402(Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0
.end method
