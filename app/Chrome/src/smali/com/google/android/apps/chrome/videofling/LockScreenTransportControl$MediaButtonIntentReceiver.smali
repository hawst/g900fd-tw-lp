.class public Lcom/google/android/apps/chrome/videofling/LockScreenTransportControl$MediaButtonIntentReceiver;
.super Landroid/content/BroadcastReceiver;
.source "LockScreenTransportControl.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    .prologue
    .line 60
    # getter for: Lcom/google/android/apps/chrome/videofling/LockScreenTransportControl;->sDebug:Z
    invoke-static {}, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControl;->access$000()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "LockScreenTransportControl"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Received intent: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 61
    :cond_0
    const-string/jumbo v0, "android.intent.action.MEDIA_BUTTON"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 62
    const-string/jumbo v0, "android.intent.extra.KEY_EVENT"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/view/KeyEvent;

    .line 63
    invoke-static {}, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControl;->getIfExists()Lcom/google/android/apps/chrome/videofling/LockScreenTransportControl;

    move-result-object v1

    .line 64
    if-nez v1, :cond_2

    .line 65
    const-string/jumbo v0, "LockScreenTransportControl"

    const-string/jumbo v1, "Event received when no LockScreenTransportControl exists"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 91
    :cond_1
    :goto_0
    return-void

    .line 68
    :cond_2
    invoke-virtual {v1}, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControl;->getListeners()Ljava/util/Set;

    move-result-object v2

    .line 71
    invoke-virtual {v0}, Landroid/view/KeyEvent;->getAction()I

    move-result v3

    if-eqz v3, :cond_1

    .line 73
    invoke-virtual {v0}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 88
    const-string/jumbo v1, "LockScreenTransportControl"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Unrecognized event: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 75
    :pswitch_0
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/videofling/TransportControl$Listener;

    .line 76
    invoke-virtual {v1}, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControl;->isPlaying()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 77
    invoke-interface {v0}, Lcom/google/android/apps/chrome/videofling/TransportControl$Listener;->onPause()V

    goto :goto_1

    .line 79
    :cond_3
    invoke-interface {v0}, Lcom/google/android/apps/chrome/videofling/TransportControl$Listener;->onPlay()V

    goto :goto_1

    .line 84
    :pswitch_1
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/videofling/TransportControl$Listener;

    .line 85
    invoke-interface {v0}, Lcom/google/android/apps/chrome/videofling/TransportControl$Listener;->onStop()V

    goto :goto_2

    .line 73
    :pswitch_data_0
    .packed-switch 0x55
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
