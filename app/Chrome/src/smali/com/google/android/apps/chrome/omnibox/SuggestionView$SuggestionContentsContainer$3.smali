.class Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer$3;
.super Ljava/lang/Object;
.source "SuggestionView.java"

# interfaces
.implements Landroid/view/View$OnLongClickListener;


# instance fields
.field final synthetic this$1:Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;

.field final synthetic val$this$0:Lcom/google/android/apps/chrome/omnibox/SuggestionView;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;Lcom/google/android/apps/chrome/omnibox/SuggestionView;)V
    .locals 0

    .prologue
    .line 643
    iput-object p1, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer$3;->this$1:Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;

    iput-object p2, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer$3;->val$this$0:Lcom/google/android/apps/chrome/omnibox/SuggestionView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLongClick(Landroid/view/View;)Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 646
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->omniboxDeleteGesture()V

    .line 647
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer$3;->this$1:Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;

    iget-object v0, v0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->this$0:Lcom/google/android/apps/chrome/omnibox/SuggestionView;

    # getter for: Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mSuggestion:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->access$1100(Lcom/google/android/apps/chrome/omnibox/SuggestionView;)Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;->isDeletable()Z

    move-result v0

    if-nez v0, :cond_0

    .line 680
    :goto_0
    return v3

    .line 649
    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer$3;->this$1:Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 650
    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer$3;->this$1:Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;

    iget-object v1, v1, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->this$0:Lcom/google/android/apps/chrome/omnibox/SuggestionView;

    # getter for: Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mSuggestion:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;
    invoke-static {v1}, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->access$1100(Lcom/google/android/apps/chrome/omnibox/SuggestionView;)Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;

    move-result-object v1

    invoke-virtual {v1}, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;->getDisplayText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 651
    sget v1, Lcom/google/android/apps/chrome/R$string;->omnibox_confirm_delete:I

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 652
    new-instance v1, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer$3$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer$3$1;-><init>(Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer$3;)V

    .line 660
    const v2, 0x104000a

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 661
    new-instance v1, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer$3$2;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer$3$2;-><init>(Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer$3;)V

    .line 668
    const/high16 v2, 0x1040000

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 670
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 671
    new-instance v1, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer$3$3;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer$3$3;-><init>(Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer$3;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 678
    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer$3;->this$1:Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;

    iget-object v1, v1, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->this$0:Lcom/google/android/apps/chrome/omnibox/SuggestionView;

    # getter for: Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mSuggestionDelegate:Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter$OmniboxSuggestionDelegate;
    invoke-static {v1}, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->access$1300(Lcom/google/android/apps/chrome/omnibox/SuggestionView;)Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter$OmniboxSuggestionDelegate;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter$OmniboxSuggestionDelegate;->onShowModal()V

    .line 679
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto :goto_0
.end method
