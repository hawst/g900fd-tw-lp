.class public Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;
.super Lcom/google/android/apps/chrome/tabs/layout/Layout;
.source "SimpleAnimationLayout.java"

# interfaces
.implements Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field protected static final FOREGROUND_ANIMATION_DURATION:I = 0x12c

.field protected static final TAB_CLOSED_ANIMATION_DURATION:I = 0xfa


# instance fields
.field private mAnimatedTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

.field private mClosedTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const-class v0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;Lcom/google/android/apps/chrome/eventfilter/EventFilter;)V
    .locals 6

    .prologue
    .line 72
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/chrome/tabs/layout/Layout;-><init>(Landroid/content/Context;Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;Lcom/google/android/apps/chrome/eventfilter/EventFilter;Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripStacker;)V

    .line 73
    return-void
.end method

.method private ensureSourceTabCreated(I)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 119
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    array-length v0, v0

    if-ne v0, v2, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    aget-object v0, v0, v3

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getId()I

    move-result v0

    if-ne v0, p1, :cond_1

    .line 131
    :cond_0
    :goto_0
    return-void

    .line 123
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    invoke-interface {v0, p1}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->getModelForTabId(I)Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v0

    .line 124
    if-eqz v0, :cond_0

    .line 125
    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->isIncognito()Z

    move-result v0

    invoke-virtual {p0, p1, v0, v3, v3}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->createLayoutTab(IZZZ)Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v0

    .line 127
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setBorderAlpha(F)V

    .line 129
    new-array v1, v2, [Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    aput-object v0, v1, v3

    iput-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    .line 130
    new-instance v0, Ljava/util/LinkedList;

    new-array v1, v2, [Ljava/lang/Integer;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/LinkedList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->updateCacheVisibleIds(Ljava/util/List;)V

    goto :goto_0
.end method

.method private getDiscardRange()F
    .locals 2

    .prologue
    .line 363
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->getWidth()F

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->getHeight()F

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    const v1, 0x3f333333    # 0.7f

    mul-float/2addr v0, v1

    return v0
.end method

.method private reset()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 375
    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    .line 376
    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mAnimatedTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    .line 377
    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mClosedTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    .line 378
    return-void
.end method

.method private setDiscardAmount(F)V
    .locals 7

    .prologue
    const/high16 v6, 0x3f800000    # 1.0f

    .line 345
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mAnimatedTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    if-eqz v0, :cond_0

    .line 346
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->getDiscardRange()F

    move-result v0

    .line 347
    const/4 v1, 0x1

    invoke-static {p1, v0, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->computeDiscardScale(FFZ)F

    move-result v1

    .line 349
    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mAnimatedTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getOriginalContentWidth()F

    move-result v2

    .line 350
    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mAnimatedTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getOriginalContentHeight()F

    move-result v3

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    .line 351
    iget-object v4, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mAnimatedTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    sub-float v5, v6, v1

    mul-float/2addr v2, v5

    invoke-virtual {v4, v2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setX(F)V

    .line 352
    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mAnimatedTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    sub-float v4, v6, v1

    mul-float/2addr v3, v4

    invoke-virtual {v2, v3}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setY(F)V

    .line 353
    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mAnimatedTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    invoke-virtual {v2, v1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setScale(F)V

    .line 354
    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mAnimatedTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    invoke-virtual {v2, v1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setBorderScale(F)V

    .line 355
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mAnimatedTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    invoke-static {p1, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->computeDiscardAlpha(FF)F

    move-result v0

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setAlpha(F)V

    .line 357
    :cond_0
    return-void
.end method

.method private tabCreatedInBackground(IIZFF)V
    .locals 21

    .prologue
    .line 198
    const/4 v4, 0x0

    const/4 v5, 0x1

    move-object/from16 v0, p0

    move/from16 v1, p1

    move/from16 v2, p3

    invoke-virtual {v0, v1, v2, v4, v5}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->createLayoutTab(IZZZ)Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v20

    .line 200
    sget-boolean v4, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->$assertionsDisabled:Z

    if-nez v4, :cond_0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    array-length v4, v4

    const/4 v5, 0x1

    if-eq v4, v5, :cond_0

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 201
    :cond_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    const/4 v5, 0x0

    aget-object v6, v4, v5

    .line 202
    const/4 v4, 0x2

    new-array v4, v4, [Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    const/4 v5, 0x0

    aput-object v6, v4, v5

    const/4 v5, 0x1

    aput-object v20, v4, v5

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    .line 203
    new-instance v4, Ljava/util/LinkedList;

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Integer;

    const/4 v7, 0x0

    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v5, v7

    const/4 v7, 0x1

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v5, v7

    invoke-static {v5}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/util/LinkedList;-><init>(Ljava/util/Collection;)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->updateCacheVisibleIds(Ljava/util/List;)V

    .line 205
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->forceAnimationToFinish()V

    .line 207
    const/4 v4, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setBorderAlpha(F)V

    .line 208
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->getWidth()F

    move-result v4

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->getHeight()F

    move-result v5

    invoke-static {v4, v5}, Ljava/lang/Math;->min(FF)F

    move-result v4

    const v5, 0x3dccccd0    # 0.100000024f

    mul-float/2addr v4, v5

    const/high16 v5, 0x40000000    # 2.0f

    div-float v18, v4, v5

    .line 212
    sget-object v7, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;->SCALE:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

    const/high16 v8, 0x3f800000    # 1.0f

    const v9, 0x3f666666    # 0.9f

    const-wide/16 v10, 0x12c

    const-wide/16 v12, 0x0

    const/4 v14, 0x0

    sget-object v15, Lorg/chromium/ui/interpolators/BakedBezierInterpolator;->TRANSFORM_CURVE:Lorg/chromium/ui/interpolators/BakedBezierInterpolator;

    move-object/from16 v5, p0

    invoke-virtual/range {v5 .. v15}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->addToAnimation(Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJZLandroid/view/animation/Interpolator;)V

    .line 214
    sget-object v7, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;->X:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

    const/4 v8, 0x0

    const-wide/16 v10, 0x12c

    const-wide/16 v12, 0x0

    const/4 v14, 0x0

    sget-object v15, Lorg/chromium/ui/interpolators/BakedBezierInterpolator;->TRANSFORM_CURVE:Lorg/chromium/ui/interpolators/BakedBezierInterpolator;

    move-object/from16 v5, p0

    move/from16 v9, v18

    invoke-virtual/range {v5 .. v15}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->addToAnimation(Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJZLandroid/view/animation/Interpolator;)V

    .line 216
    sget-object v7, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;->Y:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

    const/4 v8, 0x0

    const-wide/16 v10, 0x12c

    const-wide/16 v12, 0x0

    const/4 v14, 0x0

    sget-object v15, Lorg/chromium/ui/interpolators/BakedBezierInterpolator;->TRANSFORM_CURVE:Lorg/chromium/ui/interpolators/BakedBezierInterpolator;

    move-object/from16 v5, p0

    move/from16 v9, v18

    invoke-virtual/range {v5 .. v15}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->addToAnimation(Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJZLandroid/view/animation/Interpolator;)V

    .line 218
    sget-object v7, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;->BORDER_SCALE:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

    const v8, 0x3f8e38e4

    const/high16 v9, 0x3f800000    # 1.0f

    const-wide/16 v10, 0x12c

    const-wide/16 v12, 0x0

    const/4 v14, 0x0

    sget-object v15, Lorg/chromium/ui/interpolators/BakedBezierInterpolator;->TRANSFORM_CURVE:Lorg/chromium/ui/interpolators/BakedBezierInterpolator;

    move-object/from16 v5, p0

    invoke-virtual/range {v5 .. v15}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->addToAnimation(Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJZLandroid/view/animation/Interpolator;)V

    .line 220
    sget-object v7, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;->BORDER_ALPHA:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

    const/4 v8, 0x0

    const/high16 v9, 0x3f800000    # 1.0f

    const-wide/16 v10, 0x12c

    const-wide/16 v12, 0x0

    const/4 v14, 0x0

    sget-object v15, Lorg/chromium/ui/interpolators/BakedBezierInterpolator;->TRANSFORM_CURVE:Lorg/chromium/ui/interpolators/BakedBezierInterpolator;

    move-object/from16 v5, p0

    invoke-virtual/range {v5 .. v15}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->addToAnimation(Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJZLandroid/view/animation/Interpolator;)V

    .line 225
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->getOrientation()I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_1

    .line 226
    const/high16 v4, 0x3f000000    # 0.5f

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->getHeight()F

    move-result v5

    mul-float/2addr v4, v5

    move/from16 v19, v18

    .line 231
    :goto_0
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->getHeight()F

    move-result v5

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->getHeightMinusTopControls()F

    move-result v7

    sub-float/2addr v5, v7

    .line 232
    sget-object v9, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;->ALPHA:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

    const/4 v10, 0x0

    const/high16 v11, 0x3f800000    # 1.0f

    const-wide/16 v12, 0x96

    const-wide/16 v14, 0x0

    const/16 v16, 0x0

    sget-object v17, Lorg/chromium/ui/interpolators/BakedBezierInterpolator;->FADE_IN_CURVE:Lorg/chromium/ui/interpolators/BakedBezierInterpolator;

    move-object/from16 v7, p0

    move-object/from16 v8, v20

    invoke-virtual/range {v7 .. v17}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->addToAnimation(Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJZLandroid/view/animation/Interpolator;)V

    .line 234
    sget-object v9, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;->SCALE:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

    const/4 v10, 0x0

    const v11, 0x3f666666    # 0.9f

    const-wide/16 v12, 0x12c

    const-wide/16 v14, 0x0

    const/16 v16, 0x0

    sget-object v17, Lorg/chromium/ui/interpolators/BakedBezierInterpolator;->FADE_IN_CURVE:Lorg/chromium/ui/interpolators/BakedBezierInterpolator;

    move-object/from16 v7, p0

    move-object/from16 v8, v20

    invoke-virtual/range {v7 .. v17}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->addToAnimation(Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJZLandroid/view/animation/Interpolator;)V

    .line 236
    sget-object v9, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;->X:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

    const-wide/16 v12, 0x12c

    const-wide/16 v14, 0x0

    const/16 v16, 0x0

    sget-object v17, Lorg/chromium/ui/interpolators/BakedBezierInterpolator;->FADE_IN_CURVE:Lorg/chromium/ui/interpolators/BakedBezierInterpolator;

    move-object/from16 v7, p0

    move-object/from16 v8, v20

    move/from16 v10, p4

    move/from16 v11, v19

    invoke-virtual/range {v7 .. v17}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->addToAnimation(Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJZLandroid/view/animation/Interpolator;)V

    .line 238
    sget-object v9, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;->Y:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

    sub-float v10, p5, v5

    const-wide/16 v12, 0x12c

    const-wide/16 v14, 0x0

    const/16 v16, 0x0

    sget-object v17, Lorg/chromium/ui/interpolators/BakedBezierInterpolator;->FADE_IN_CURVE:Lorg/chromium/ui/interpolators/BakedBezierInterpolator;

    move-object/from16 v7, p0

    move-object/from16 v8, v20

    move v11, v4

    invoke-virtual/range {v7 .. v17}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->addToAnimation(Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJZLandroid/view/animation/Interpolator;)V

    .line 244
    sget-object v7, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;->SCALE:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

    const v8, 0x3f666666    # 0.9f

    const/high16 v9, 0x3f800000    # 1.0f

    const-wide/16 v10, 0x12c

    const-wide/16 v12, 0x1c2

    const/4 v14, 0x1

    sget-object v15, Lorg/chromium/ui/interpolators/BakedBezierInterpolator;->TRANSFORM_CURVE:Lorg/chromium/ui/interpolators/BakedBezierInterpolator;

    move-object/from16 v5, p0

    invoke-virtual/range {v5 .. v15}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->addToAnimation(Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJZLandroid/view/animation/Interpolator;)V

    .line 247
    sget-object v7, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;->X:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

    const/4 v9, 0x0

    const-wide/16 v10, 0x12c

    const-wide/16 v12, 0x1c2

    const/4 v14, 0x1

    sget-object v15, Lorg/chromium/ui/interpolators/BakedBezierInterpolator;->TRANSFORM_CURVE:Lorg/chromium/ui/interpolators/BakedBezierInterpolator;

    move-object/from16 v5, p0

    move/from16 v8, v18

    invoke-virtual/range {v5 .. v15}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->addToAnimation(Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJZLandroid/view/animation/Interpolator;)V

    .line 250
    sget-object v7, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;->Y:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

    const/4 v9, 0x0

    const-wide/16 v10, 0x12c

    const-wide/16 v12, 0x1c2

    const/4 v14, 0x1

    sget-object v15, Lorg/chromium/ui/interpolators/BakedBezierInterpolator;->TRANSFORM_CURVE:Lorg/chromium/ui/interpolators/BakedBezierInterpolator;

    move-object/from16 v5, p0

    move/from16 v8, v18

    invoke-virtual/range {v5 .. v15}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->addToAnimation(Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJZLandroid/view/animation/Interpolator;)V

    .line 253
    sget-object v7, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;->BORDER_SCALE:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

    const/high16 v8, 0x3f800000    # 1.0f

    const v9, 0x3f8e38e4

    const-wide/16 v10, 0x12c

    const-wide/16 v12, 0x1c2

    const/4 v14, 0x1

    sget-object v15, Lorg/chromium/ui/interpolators/BakedBezierInterpolator;->TRANSFORM_CURVE:Lorg/chromium/ui/interpolators/BakedBezierInterpolator;

    move-object/from16 v5, p0

    invoke-virtual/range {v5 .. v15}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->addToAnimation(Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJZLandroid/view/animation/Interpolator;)V

    .line 256
    sget-object v7, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;->BORDER_ALPHA:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

    const/high16 v8, 0x3f800000    # 1.0f

    const/4 v9, 0x0

    const-wide/16 v10, 0x12c

    const-wide/16 v12, 0x1c2

    const/4 v14, 0x1

    sget-object v15, Lorg/chromium/ui/interpolators/BakedBezierInterpolator;->TRANSFORM_CURVE:Lorg/chromium/ui/interpolators/BakedBezierInterpolator;

    move-object/from16 v5, p0

    invoke-virtual/range {v5 .. v15}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->addToAnimation(Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJZLandroid/view/animation/Interpolator;)V

    .line 260
    sget-object v7, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;->ALPHA:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

    const/high16 v8, 0x3f800000    # 1.0f

    const/4 v9, 0x0

    const-wide/16 v10, 0x12c

    const-wide/16 v12, 0x1c2

    const/4 v14, 0x1

    sget-object v15, Lorg/chromium/ui/interpolators/BakedBezierInterpolator;->FADE_OUT_CURVE:Lorg/chromium/ui/interpolators/BakedBezierInterpolator;

    move-object/from16 v5, p0

    move-object/from16 v6, v20

    invoke-virtual/range {v5 .. v15}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->addToAnimation(Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJZLandroid/view/animation/Interpolator;)V

    .line 263
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->getOrientation()I

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_2

    .line 264
    sget-object v7, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;->Y:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->getHeight()F

    move-result v9

    const-wide/16 v10, 0x12c

    const-wide/16 v12, 0x1c2

    const/4 v14, 0x1

    sget-object v15, Lorg/chromium/ui/interpolators/BakedBezierInterpolator;->FADE_OUT_CURVE:Lorg/chromium/ui/interpolators/BakedBezierInterpolator;

    move-object/from16 v5, p0

    move-object/from16 v6, v20

    move v8, v4

    invoke-virtual/range {v5 .. v15}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->addToAnimation(Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJZLandroid/view/animation/Interpolator;)V

    .line 273
    :goto_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    move/from16 v0, p3

    invoke-interface {v4, v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->selectModel(Z)V

    .line 274
    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->startHiding(I)V

    .line 275
    return-void

    .line 228
    :cond_1
    const/high16 v4, 0x3f000000    # 0.5f

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->getWidth()F

    move-result v5

    mul-float/2addr v4, v5

    move/from16 v19, v4

    move/from16 v4, v18

    goto/16 :goto_0

    .line 268
    :cond_2
    sget-object v7, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;->X:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->getWidth()F

    move-result v9

    const-wide/16 v10, 0x12c

    const-wide/16 v12, 0x1c2

    const/4 v14, 0x1

    sget-object v15, Lorg/chromium/ui/interpolators/BakedBezierInterpolator;->FADE_OUT_CURVE:Lorg/chromium/ui/interpolators/BakedBezierInterpolator;

    move-object/from16 v5, p0

    move-object/from16 v6, v20

    move/from16 v8, v19

    invoke-virtual/range {v5 .. v15}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->addToAnimation(Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJZLandroid/view/animation/Interpolator;)V

    goto :goto_1
.end method

.method private tabCreatedInForeground(IIZFF)V
    .locals 12

    .prologue
    .line 157
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, p1, p3, v0, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->createLayoutTab(IZZZ)Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v2

    .line 158
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    array-length v0, v0

    if-nez v0, :cond_1

    .line 159
    :cond_0
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    const/4 v1, 0x0

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    .line 163
    :goto_0
    new-instance v0, Ljava/util/LinkedList;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Integer;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v3

    const/4 v3, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v3

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/LinkedList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->updateCacheVisibleIds(Ljava/util/List;)V

    .line 165
    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setBorderAlpha(F)V

    .line 166
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {v2, v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setStaticToViewBlend(F)V

    .line 168
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->forceAnimationToFinish()V

    .line 170
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->getHeight()F

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->getHeightMinusTopControls()F

    move-result v1

    sub-float/2addr v0, v1

    .line 171
    sget-object v11, Lorg/chromium/ui/interpolators/BakedBezierInterpolator;->TRANSFORM_CURVE:Lorg/chromium/ui/interpolators/BakedBezierInterpolator;

    .line 172
    sget-object v3, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;->SCALE:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

    const/4 v4, 0x0

    const/high16 v5, 0x3f800000    # 1.0f

    const-wide/16 v6, 0x12c

    const-wide/16 v8, 0x0

    const/4 v10, 0x0

    move-object v1, p0

    invoke-virtual/range {v1 .. v11}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->addToAnimation(Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJZLandroid/view/animation/Interpolator;)V

    .line 174
    sget-object v3, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;->ALPHA:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

    const/4 v4, 0x0

    const/high16 v5, 0x3f800000    # 1.0f

    const-wide/16 v6, 0x12c

    const-wide/16 v8, 0x0

    const/4 v10, 0x0

    move-object v1, p0

    invoke-virtual/range {v1 .. v11}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->addToAnimation(Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJZLandroid/view/animation/Interpolator;)V

    .line 176
    sget-object v3, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;->X:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

    const/4 v5, 0x0

    const-wide/16 v6, 0x12c

    const-wide/16 v8, 0x0

    const/4 v10, 0x0

    move-object v1, p0

    move/from16 v4, p4

    invoke-virtual/range {v1 .. v11}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->addToAnimation(Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJZLandroid/view/animation/Interpolator;)V

    .line 178
    sget-object v3, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;->Y:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

    sub-float v4, p5, v0

    const/4 v5, 0x0

    const-wide/16 v6, 0x12c

    const-wide/16 v8, 0x0

    const/4 v10, 0x0

    move-object v1, p0

    invoke-virtual/range {v1 .. v11}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->addToAnimation(Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJZLandroid/view/animation/Interpolator;)V

    .line 181
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    invoke-interface {v0, p3}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->selectModel(Z)V

    .line 182
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->startHiding(I)V

    .line 183
    return-void

    .line 161
    :cond_1
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    const/4 v1, 0x0

    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    const/4 v4, 0x0

    aget-object v3, v3, v4

    aput-object v3, v0, v1

    const/4 v1, 0x1

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    goto/16 :goto_0
.end method


# virtual methods
.method public getSizingFlags()I
    .locals 1

    .prologue
    .line 77
    const/16 v0, 0x111

    return v0
.end method

.method public handlesTabClosing()Z
    .locals 1

    .prologue
    .line 93
    const/4 v0, 0x1

    return v0
.end method

.method public handlesTabCreating()Z
    .locals 1

    .prologue
    .line 88
    const/4 v0, 0x1

    return v0
.end method

.method public onTabClosed(JIIZ)V
    .locals 15

    .prologue
    .line 307
    invoke-super/range {p0 .. p5}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->onTabClosed(JIIZ)V

    .line 309
    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mClosedTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    if-eqz v2, :cond_0

    .line 310
    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    move/from16 v0, p4

    invoke-interface {v2, v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->getModelForTabId(I)Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v2

    .line 311
    if-eqz v2, :cond_1

    .line 312
    invoke-interface {v2}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->isIncognito()Z

    move-result v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    move/from16 v0, p4

    invoke-virtual {p0, v0, v3, v4, v5}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->createLayoutTab(IZZZ)Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v3

    .line 314
    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setDrawDecoration(Z)V

    .line 316
    const/4 v4, 0x2

    new-array v4, v4, [Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    const/4 v5, 0x0

    aput-object v3, v4, v5

    const/4 v3, 0x1

    iget-object v5, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mClosedTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    aput-object v5, v4, v3

    iput-object v4, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    .line 317
    new-instance v3, Ljava/util/LinkedList;

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Integer;

    const/4 v5, 0x0

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    iget-object v6, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mClosedTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    invoke-virtual {v6}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getId()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v4}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/util/LinkedList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {p0, v3}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->updateCacheVisibleIds(Ljava/util/List;)V

    .line 323
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->forceAnimationToFinish()V

    .line 324
    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mClosedTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    iput-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mAnimatedTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    .line 325
    sget-object v5, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout$Property;->DISCARD_AMOUNT:Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout$Property;

    const/4 v6, 0x0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->getDiscardRange()F

    move-result v7

    const-wide/16 v8, 0xfa

    const-wide/16 v10, 0x0

    const/4 v12, 0x0

    sget-object v13, Lorg/chromium/ui/interpolators/BakedBezierInterpolator;->FADE_OUT_CURVE:Lorg/chromium/ui/interpolators/BakedBezierInterpolator;

    move-object v3, p0

    move-object v4, p0

    invoke-virtual/range {v3 .. v13}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->addToAnimation(Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJZLandroid/view/animation/Interpolator;)V

    .line 329
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mClosedTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    .line 330
    if-eqz v2, :cond_0

    .line 331
    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    invoke-interface {v2}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->isIncognito()Z

    move-result v2

    invoke-interface {v3, v2}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->selectModel(Z)V

    .line 334
    :cond_0
    move/from16 v0, p4

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->startHiding(I)V

    .line 335
    return-void

    .line 320
    :cond_1
    const/4 v3, 0x1

    new-array v3, v3, [Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mClosedTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    aput-object v5, v3, v4

    iput-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    goto :goto_0
.end method

.method public onTabClosing(JI)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 282
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->reset()V

    .line 285
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->forceAnimationToFinish()V

    .line 288
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    invoke-interface {v0, p3}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->getModelForTabId(I)Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v0

    .line 289
    if-eqz v0, :cond_0

    .line 290
    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->isIncognito()Z

    move-result v0

    invoke-virtual {p0, p3, v0, v3, v3}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->createLayoutTab(IZZZ)Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mClosedTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    .line 291
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mClosedTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setBorderAlpha(F)V

    .line 292
    new-array v0, v2, [Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mClosedTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    aput-object v1, v0, v3

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    .line 293
    new-instance v0, Ljava/util/LinkedList;

    new-array v1, v2, [Ljava/lang/Integer;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/LinkedList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->updateCacheVisibleIds(Ljava/util/List;)V

    .line 299
    :goto_0
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->onTabClosing(JI)V

    .line 300
    return-void

    .line 295
    :cond_0
    iput-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    .line 296
    iput-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mClosedTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    goto :goto_0
.end method

.method public onTabCreated(JIIIZZFF)V
    .locals 7

    .prologue
    .line 136
    invoke-super/range {p0 .. p9}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->onTabCreated(JIIIZZFF)V

    .line 138
    invoke-direct {p0, p5}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->ensureSourceTabCreated(I)V

    .line 139
    if-eqz p7, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    array-length v0, v0

    if-lez v0, :cond_0

    move-object v0, p0

    move v1, p3

    move v2, p5

    move v3, p6

    move v4, p8

    move/from16 v5, p9

    .line 140
    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->tabCreatedInBackground(IIZFF)V

    .line 144
    :goto_0
    return-void

    :cond_0
    move-object v0, p0

    move v1, p3

    move v2, p5

    move v3, p6

    move v4, p8

    move/from16 v5, p9

    .line 142
    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->tabCreatedInForeground(IIZFF)V

    goto :goto_0
.end method

.method public onTabCreating(I)V
    .locals 0

    .prologue
    .line 109
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->onTabCreating(I)V

    .line 110
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->reset()V

    .line 113
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->forceAnimationToFinish()V

    .line 115
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->ensureSourceTabCreated(I)V

    .line 116
    return-void
.end method

.method public onUpdateAnimation(JZ)Z
    .locals 1

    .prologue
    .line 368
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->onUpdateAnimation(JZ)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mClosedTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setProperty(Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout$Property;F)V
    .locals 2

    .prologue
    .line 387
    sget-object v0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout$1;->$SwitchMap$com$google$android$apps$chrome$tabs$layout$phone$SimpleAnimationLayout$Property:[I

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout$Property;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 390
    :goto_0
    return-void

    .line 389
    :pswitch_0
    invoke-direct {p0, p2}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->setDiscardAmount(F)V

    goto :goto_0

    .line 387
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic setProperty(Ljava/lang/Enum;F)V
    .locals 0

    .prologue
    .line 26
    check-cast p1, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout$Property;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->setProperty(Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout$Property;F)V

    return-void
.end method

.method public show(JZ)V
    .locals 1

    .prologue
    .line 82
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->show(JZ)V

    .line 83
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->reset()V

    .line 84
    return-void
.end method

.method protected updateLayout(JJ)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 98
    invoke-super {p0, p1, p2, p3, p4}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->updateLayout(JJ)V

    .line 99
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    if-nez v0, :cond_1

    .line 105
    :cond_0
    :goto_0
    return-void

    .line 101
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    move v2, v0

    move v0, v1

    :goto_1
    if-ltz v2, :cond_4

    .line 102
    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    aget-object v3, v3, v2

    invoke-virtual {v3, p3, p4}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->updateSnap(J)Z

    move-result v3

    if-nez v3, :cond_2

    if-eqz v0, :cond_3

    :cond_2
    const/4 v0, 0x1

    .line 101
    :goto_2
    add-int/lit8 v2, v2, -0x1

    goto :goto_1

    :cond_3
    move v0, v1

    .line 102
    goto :goto_2

    .line 104
    :cond_4
    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;->requestUpdate()V

    goto :goto_0
.end method
