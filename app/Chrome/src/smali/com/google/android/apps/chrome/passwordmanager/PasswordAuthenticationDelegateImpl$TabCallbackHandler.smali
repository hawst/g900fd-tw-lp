.class Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl$TabCallbackHandler;
.super Lorg/chromium/chrome/browser/EmptyTabObserver;
.source "PasswordAuthenticationDelegateImpl.java"

# interfaces
.implements Landroid/view/View$OnAttachStateChangeListener;
.implements Lcom/google/android/apps/chrome/passwordmanager/AuthenticationCallback;
.implements Lorg/chromium/ui/base/WindowAndroid$IntentCallback;


# instance fields
.field private final mCallbacks:Ljava/util/List;

.field private mIsPendingCallbackResult:Z

.field private final mTab:Lorg/chromium/chrome/browser/Tab;

.field private mTabView:Landroid/view/View;

.field final synthetic this$0:Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl;Lorg/chromium/chrome/browser/Tab;Lorg/chromium/chrome/browser/password_manager/PasswordAuthenticationManager$PasswordAuthenticationCallback;)V
    .locals 1

    .prologue
    .line 206
    iput-object p1, p0, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl$TabCallbackHandler;->this$0:Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl;

    invoke-direct {p0}, Lorg/chromium/chrome/browser/EmptyTabObserver;-><init>()V

    .line 207
    iput-object p2, p0, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl$TabCallbackHandler;->mTab:Lorg/chromium/chrome/browser/Tab;

    .line 208
    iget-object v0, p0, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl$TabCallbackHandler;->mTab:Lorg/chromium/chrome/browser/Tab;

    invoke-virtual {v0, p0}, Lorg/chromium/chrome/browser/Tab;->removeObserver(Lorg/chromium/chrome/browser/TabObserver;)V

    .line 209
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl$TabCallbackHandler;->mCallbacks:Ljava/util/List;

    .line 210
    iget-object v0, p0, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl$TabCallbackHandler;->mCallbacks:Ljava/util/List;

    invoke-interface {v0, p3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 212
    iget-object v0, p0, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl$TabCallbackHandler;->mTab:Lorg/chromium/chrome/browser/Tab;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl$TabCallbackHandler;->onContentChanged(Lorg/chromium/chrome/browser/Tab;)V

    .line 213
    return-void
.end method

.method private destroy()V
    .locals 1

    .prologue
    .line 240
    iget-object v0, p0, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl$TabCallbackHandler;->mTab:Lorg/chromium/chrome/browser/Tab;

    invoke-virtual {v0, p0}, Lorg/chromium/chrome/browser/Tab;->removeObserver(Lorg/chromium/chrome/browser/TabObserver;)V

    .line 241
    iget-object v0, p0, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl$TabCallbackHandler;->mTabView:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl$TabCallbackHandler;->mTabView:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->removeOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    .line 242
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl$TabCallbackHandler;->this$0:Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl;

    # getter for: Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl;->mPendingTabCallbacks:Lorg/chromium/base/ObserverList;
    invoke-static {v0}, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl;->access$200(Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl;)Lorg/chromium/base/ObserverList;

    move-result-object v0

    invoke-virtual {v0, p0}, Lorg/chromium/base/ObserverList;->removeObserver(Ljava/lang/Object;)Z

    .line 243
    return-void
.end method


# virtual methods
.method addCallback(Lorg/chromium/chrome/browser/password_manager/PasswordAuthenticationManager$PasswordAuthenticationCallback;)V
    .locals 1

    .prologue
    .line 227
    iget-object v0, p0, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl$TabCallbackHandler;->mCallbacks:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 228
    return-void
.end method

.method getTab()Lorg/chromium/chrome/browser/Tab;
    .locals 1

    .prologue
    .line 219
    iget-object v0, p0, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl$TabCallbackHandler;->mTab:Lorg/chromium/chrome/browser/Tab;

    return-object v0
.end method

.method isForTab(Lorg/chromium/chrome/browser/Tab;)Z
    .locals 1

    .prologue
    .line 236
    iget-object v0, p0, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl$TabCallbackHandler;->mTab:Lorg/chromium/chrome/browser/Tab;

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public onAuthenticationResult(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 300
    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl$TabCallbackHandler;->mCallbacks:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 301
    iget-object v0, p0, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl$TabCallbackHandler;->mCallbacks:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/password_manager/PasswordAuthenticationManager$PasswordAuthenticationCallback;

    invoke-virtual {v0, p1}, Lorg/chromium/chrome/browser/password_manager/PasswordAuthenticationManager$PasswordAuthenticationCallback;->onResult(Z)V

    .line 300
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 303
    :cond_0
    iput-boolean v2, p0, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl$TabCallbackHandler;->mIsPendingCallbackResult:Z

    .line 304
    iget-object v0, p0, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl$TabCallbackHandler;->mCallbacks:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 305
    invoke-direct {p0}, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl$TabCallbackHandler;->destroy()V

    .line 306
    return-void
.end method

.method public onContentChanged(Lorg/chromium/chrome/browser/Tab;)V
    .locals 1

    .prologue
    .line 279
    iget-object v0, p0, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl$TabCallbackHandler;->mTabView:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl$TabCallbackHandler;->mTabView:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->removeOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    .line 280
    :cond_0
    invoke-virtual {p1}, Lorg/chromium/chrome/browser/Tab;->getView()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl$TabCallbackHandler;->mTabView:Landroid/view/View;

    .line 281
    iget-object v0, p0, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl$TabCallbackHandler;->mTabView:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    .line 282
    return-void
.end method

.method public onDestroyed(Lorg/chromium/chrome/browser/Tab;)V
    .locals 1

    .prologue
    .line 286
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl$TabCallbackHandler;->onAuthenticationResult(Z)V

    .line 287
    return-void
.end method

.method public onIntentCompleted(Lorg/chromium/ui/base/WindowAndroid;ILandroid/content/ContentResolver;Landroid/content/Intent;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 311
    const/4 v1, -0x1

    if-ne p2, v1, :cond_0

    const-string/jumbo v1, "AUTH_STATUS"

    invoke-virtual {p4, v1, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl$TabCallbackHandler;->onAuthenticationResult(Z)V

    .line 313
    return-void
.end method

.method public onViewAttachedToWindow(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 291
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl$TabCallbackHandler;->triggerAuthenticationIfApplicable()V

    .line 292
    return-void
.end method

.method public onViewDetachedFromWindow(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 296
    return-void
.end method

.method triggerAuthenticationIfApplicable()V
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 249
    iget-object v0, p0, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl$TabCallbackHandler;->mTab:Lorg/chromium/chrome/browser/Tab;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/Tab;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_2

    move v1, v2

    .line 250
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl$TabCallbackHandler;->mTab:Lorg/chromium/chrome/browser/Tab;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/Tab;->getWindowAndroid()Lorg/chromium/ui/base/WindowAndroid;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/ui/base/WindowAndroid;->getActivity()Ljava/lang/ref/WeakReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 251
    const/4 v5, 0x6

    .line 252
    iget-object v4, p0, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl$TabCallbackHandler;->this$0:Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl;

    # getter for: Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl;->mOverviewModeShown:Z
    invoke-static {v4}, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl;->access$000(Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl;)Z

    move-result v4

    .line 253
    if-eqz v0, :cond_0

    .line 254
    invoke-static {v0}, Lorg/chromium/base/ApplicationStatus;->getStateForActivity(Landroid/app/Activity;)I

    move-result v5

    .line 257
    iget-object v6, p0, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl$TabCallbackHandler;->this$0:Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl;

    # getter for: Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl;->mOverviewModePerActivityMap:Landroid/util/SparseBooleanArray;
    invoke-static {v6}, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl;->access$100(Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl;)Landroid/util/SparseBooleanArray;

    move-result-object v6

    monitor-enter v6

    .line 258
    :try_start_0
    iget-object v7, p0, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl$TabCallbackHandler;->this$0:Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl;

    # getter for: Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl;->mOverviewModePerActivityMap:Landroid/util/SparseBooleanArray;
    invoke-static {v7}, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl;->access$100(Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl;)Landroid/util/SparseBooleanArray;

    move-result-object v7

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v8

    invoke-virtual {v7, v8, v4}, Landroid/util/SparseBooleanArray;->get(IZ)Z

    move-result v4

    .line 260
    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 262
    :cond_0
    if-eqz v1, :cond_1

    if-nez v4, :cond_1

    iget-boolean v1, p0, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl$TabCallbackHandler;->mIsPendingCallbackResult:Z

    if-nez v1, :cond_1

    const/4 v1, 0x3

    if-eq v5, v1, :cond_3

    .line 275
    :cond_1
    :goto_1
    return-void

    :cond_2
    move v1, v3

    .line 249
    goto :goto_0

    .line 260
    :catchall_0
    move-exception v0

    monitor-exit v6

    throw v0

    .line 269
    :cond_3
    iput-boolean v2, p0, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl$TabCallbackHandler;->mIsPendingCallbackResult:Z

    .line 270
    iget-object v1, p0, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl$TabCallbackHandler;->mTab:Lorg/chromium/chrome/browser/Tab;

    invoke-virtual {v1}, Lorg/chromium/chrome/browser/Tab;->getWindowAndroid()Lorg/chromium/ui/base/WindowAndroid;

    move-result-object v1

    # invokes: Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl;->buildAuthenticationIntent(Landroid/content/Context;)Landroid/content/Intent;
    invoke-static {v0}, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl;->access$300(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    sget v2, Lcom/google/android/apps/chrome/R$string;->error_unexpected_system_problem:I

    invoke-virtual {v1, v0, p0, v2}, Lorg/chromium/ui/base/WindowAndroid;->showIntent(Landroid/content/Intent;Lorg/chromium/ui/base/WindowAndroid$IntentCallback;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 273
    invoke-virtual {p0, v3}, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl$TabCallbackHandler;->onAuthenticationResult(Z)V

    goto :goto_1
.end method
