.class public Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkUndoController;
.super Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkModelObserver;
.source "EnhancedBookmarkUndoController.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private mBookmarksModel:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;

.field private mHideRunnable:Ljava/lang/Runnable;

.field private final mParent:Landroid/view/View;

.field private mPopup:Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupWindow;

.field private final mPopupBottomMargin:I


# direct methods
.method public constructor <init>(Landroid/view/View;Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;)V
    .locals 3

    .prologue
    .line 48
    invoke-direct {p0}, Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkModelObserver;-><init>()V

    .line 36
    new-instance v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkUndoController$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkUndoController$1;-><init>(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkUndoController;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkUndoController;->mHideRunnable:Ljava/lang/Runnable;

    .line 49
    iput-object p1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkUndoController;->mParent:Landroid/view/View;

    .line 50
    iput-object p2, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkUndoController;->mBookmarksModel:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;

    .line 51
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkUndoController;->mBookmarksModel:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;->addModelObserver(Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkModelObserver;)V

    .line 53
    const/4 v0, 0x1

    const/high16 v1, 0x41f00000    # 30.0f

    iget-object v2, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkUndoController;->mParent:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    invoke-static {v0, v1, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkUndoController;->mPopupBottomMargin:I

    .line 55
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkUndoController;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkUndoController;->hideUndoBar()V

    return-void
.end method

.method private hideUndoBar()V
    .locals 2

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkUndoController;->mParent:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHandler()Landroid/os/Handler;

    move-result-object v0

    .line 71
    if-eqz v0, :cond_0

    .line 72
    iget-object v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkUndoController;->mHideRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 75
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkUndoController;->mPopup:Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupWindow;

    if-eqz v0, :cond_1

    .line 76
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkUndoController;->mPopup:Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupWindow;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupWindow;->dismiss()V

    .line 77
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkUndoController;->mPopup:Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupWindow;

    .line 79
    :cond_1
    return-void
.end method

.method private showUndoBar(Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 82
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkUndoController;->mParent:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHandler()Landroid/os/Handler;

    move-result-object v0

    .line 83
    if-eqz v0, :cond_0

    .line 84
    iget-object v2, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkUndoController;->mHideRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 85
    iget-object v2, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkUndoController;->mHideRunnable:Ljava/lang/Runnable;

    const-wide/16 v4, 0x1388

    invoke-virtual {v0, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 88
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkUndoController;->mPopup:Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupWindow;

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    .line 90
    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkUndoController;->mPopup:Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupWindow;

    if-nez v2, :cond_1

    .line 91
    new-instance v2, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupWindow;

    iget-object v3, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkUndoController;->mParent:Landroid/view/View;

    iget-object v4, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkUndoController;->mParent:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    sget v5, Lcom/google/android/apps/chrome/R$string;->undo_bar_delete_message:I

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, p0, v4}, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupWindow;-><init>(Landroid/view/View;Landroid/view/View$OnClickListener;Ljava/lang/String;)V

    iput-object v2, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkUndoController;->mPopup:Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupWindow;

    .line 93
    iget-object v2, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkUndoController;->mPopup:Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupWindow;

    iget-object v3, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkUndoController;->mParent:Landroid/view/View;

    const/16 v4, 0x51

    invoke-virtual {v2, v3, v4, v1, v1}, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupWindow;->showAtLocation(Landroid/view/View;III)V

    .line 95
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkUndoController;->mPopup:Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupWindow;

    invoke-virtual {v1, p1, v0}, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupWindow;->setUndoText(Ljava/lang/String;Z)V

    .line 96
    return-void

    :cond_2
    move v0, v1

    .line 88
    goto :goto_0
.end method


# virtual methods
.method public bookmarkModelChanged()V
    .locals 0

    .prologue
    .line 66
    invoke-direct {p0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkUndoController;->hideUndoBar()V

    .line 67
    return-void
.end method

.method public bookmarkNodeRemoved(Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;ILorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;)V
    .locals 1

    .prologue
    .line 59
    if-eqz p3, :cond_0

    .line 60
    invoke-virtual {p3}, Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;->getTitle()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkUndoController;->showUndoBar(Ljava/lang/String;)V

    .line 62
    :cond_0
    return-void
.end method

.method public destroy()V
    .locals 1

    .prologue
    .line 104
    invoke-direct {p0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkUndoController;->hideUndoBar()V

    .line 105
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkUndoController;->mBookmarksModel:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;->removeModelObserver(Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkModelObserver;)V

    .line 106
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkUndoController;->mBookmarksModel:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;->undo()V

    .line 101
    return-void
.end method
