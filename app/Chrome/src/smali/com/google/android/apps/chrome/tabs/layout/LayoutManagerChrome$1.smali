.class Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome$1;
.super Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;
.source "LayoutManagerChrome.java"


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;)V
    .locals 0

    .prologue
    .line 397
    iput-object p1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome$1;->this$0:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 9

    .prologue
    const/4 v5, 0x0

    const/4 v2, 0x0

    .line 400
    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    .line 514
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome$1;->this$0:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->handleNotificationMessage(Landroid/os/Message;)V

    .line 520
    return-void

    .line 403
    :sswitch_0
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "tabId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 404
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string/jumbo v2, "lastId"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 405
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    const-string/jumbo v3, "incognito"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    .line 406
    if-eq v0, v1, :cond_0

    .line 407
    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome$1;->this$0:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;

    invoke-virtual {v3, v0, v1, v2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->tabSelected(IIZ)V

    goto :goto_0

    .line 413
    :sswitch_1
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "type"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;->valueOf(Ljava/lang/String;)Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;

    move-result-object v3

    .line 415
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "tabId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 416
    sget-object v0, Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;->FROM_INSTANT:Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;

    if-eq v3, v0, :cond_0

    sget-object v0, Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;->FROM_RESTORE:Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;

    if-eq v3, v0, :cond_0

    .line 418
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v4, "incognito"

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    .line 419
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome$1;->this$0:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->getTabModelSelector()Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    move-result-object v0

    invoke-interface {v0, v4}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->getModel(Z)Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v0

    invoke-static {v0, v1}, Lorg/chromium/chrome/browser/tabmodel/TabModelUtils;->getTabIndexById(Lorg/chromium/chrome/browser/tabmodel/TabList;I)I

    move-result v0

    .line 421
    iget-object v6, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome$1;->this$0:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;

    invoke-virtual {v6}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->getTabModelSelector()Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    move-result-object v6

    invoke-interface {v6, v4}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->getModel(Z)Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v6

    invoke-interface {v6}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->index()I

    move-result v6

    if-ne v6, v0, :cond_1

    const/4 v5, 0x1

    .line 423
    :cond_1
    invoke-static {}, Lorg/chromium/ui/base/LocalizationUtils;->isLayoutRtl()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome$1;->this$0:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;

    iget v0, v0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->mLastContentWidthDp:F

    .line 426
    :goto_1
    sget-object v6, Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;->FROM_MENU_OR_OVERVIEW:Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;

    if-eq v3, v6, :cond_6

    .line 427
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome$1;->this$0:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;

    iget-object v0, v0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->mLastFullscreenViewportDp:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v0

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome$1;->this$0:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;

    iget-object v2, v2, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->mLastVisibleViewportDp:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v2

    sub-float/2addr v0, v2

    .line 429
    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome$1;->this$0:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;

    iget v2, v2, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->mPxToDp:F

    iget-object v6, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome$1;->this$0:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;

    iget-object v6, v6, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->mHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;

    invoke-interface {v6}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;->getLastTapX()I

    move-result v6

    int-to-float v6, v6

    mul-float/2addr v6, v2

    .line 430
    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome$1;->this$0:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;

    iget v2, v2, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->mPxToDp:F

    iget-object v7, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome$1;->this$0:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;

    iget-object v7, v7, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->mHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;

    invoke-interface {v7}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;->getLastTapY()I

    move-result v7

    int-to-float v7, v7

    mul-float/2addr v2, v7

    sub-float v7, v2, v0

    .line 433
    :goto_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome$1;->this$0:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    const-string/jumbo v8, "sourceTabId"

    invoke-virtual {v2, v8}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->tabCreated(IILorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;ZZFF)V

    goto/16 :goto_0

    :cond_2
    move v0, v2

    .line 423
    goto :goto_1

    .line 446
    :sswitch_2
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "type"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;->valueOf(Ljava/lang/String;)Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;

    move-result-object v0

    .line 448
    sget-object v1, Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;->FROM_MENU_OR_OVERVIEW:Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;

    if-eq v0, v1, :cond_3

    sget-object v1, Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;->FROM_LINK:Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;

    if-eq v0, v1, :cond_3

    sget-object v1, Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;->FROM_LONGPRESS_FOREGROUND:Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;

    if-eq v0, v1, :cond_3

    sget-object v1, Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;->FROM_LONGPRESS_BACKGROUND:Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;

    if-ne v0, v1, :cond_0

    .line 452
    :cond_3
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "sourceTabId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 453
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string/jumbo v2, "url"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 454
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    const-string/jumbo v3, "incognito"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    .line 455
    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome$1;->this$0:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;

    invoke-virtual {v3, v0, v1, v2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->tabCreating(ILjava/lang/String;Z)V

    goto/16 :goto_0

    .line 460
    :sswitch_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome$1;->this$0:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->getTabModelSelector()Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome$1;->this$0:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->getTabModelSelector()Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    move-result-object v0

    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->getCurrentTab()Lorg/chromium/chrome/browser/Tab;

    move-result-object v0

    .line 462
    :goto_3
    if-eqz v0, :cond_5

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/Tab;->getId()I

    move-result v0

    .line 464
    :goto_4
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome$1;->this$0:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    const-string/jumbo v3, "tabId"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v3

    const-string/jumbo v4, "incognito"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    invoke-virtual {v1, v2, v0, v3}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->tabClosed(IIZ)V

    goto/16 :goto_0

    .line 460
    :cond_4
    const/4 v0, 0x0

    goto :goto_3

    .line 462
    :cond_5
    const/4 v0, -0x1

    goto :goto_4

    .line 468
    :sswitch_4
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome$1;->this$0:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string/jumbo v2, "incognito"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->tabModelSwitched(Z)V

    goto/16 :goto_0

    .line 471
    :sswitch_5
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome$1;->this$0:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string/jumbo v2, "tabId"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    const-string/jumbo v3, "fromPosition"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v3

    const-string/jumbo v4, "toPosition"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v4

    const-string/jumbo v5, "incognito"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    # invokes: Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->tabMoved(IIIZ)V
    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->access$000(Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;IIIZ)V

    goto/16 :goto_0

    .line 475
    :sswitch_6
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome$1;->this$0:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string/jumbo v2, "incognito"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    # invokes: Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->closeAllTabsRequest(Z)V
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->access$100(Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;Z)V

    goto/16 :goto_0

    .line 478
    :sswitch_7
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome$1;->this$0:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string/jumbo v2, "tabId"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    const-string/jumbo v3, "incognito"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    # invokes: Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->tabPageLoadStarted(IZ)V
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->access$200(Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;IZ)V

    goto/16 :goto_0

    .line 484
    :sswitch_8
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome$1;->this$0:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string/jumbo v2, "tabId"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    const-string/jumbo v3, "incognito"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    # invokes: Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->tabPageLoadFinished(IZ)V
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->access$300(Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;IZ)V

    goto/16 :goto_0

    .line 489
    :sswitch_9
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "tabId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 490
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string/jumbo v2, "incognito"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 491
    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome$1;->this$0:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->getTabById(I)Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v2

    .line 492
    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome$1;->this$0:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;

    # invokes: Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->tabPageLoadFinished(IZ)V
    invoke-static {v2, v0, v1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->access$300(Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;IZ)V

    goto/16 :goto_0

    .line 496
    :sswitch_a
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome$1;->this$0:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string/jumbo v2, "tabId"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    const-string/jumbo v3, "incognito"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    # invokes: Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->tabLoadStarted(IZ)V
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->access$400(Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;IZ)V

    goto/16 :goto_0

    .line 500
    :sswitch_b
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome$1;->this$0:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string/jumbo v2, "tabId"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    const-string/jumbo v3, "incognito"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    # invokes: Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->tabLoadFinished(IZ)V
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->access$500(Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;IZ)V

    goto/16 :goto_0

    .line 504
    :sswitch_c
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome$1;->this$0:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string/jumbo v2, "tabId"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    const-string/jumbo v3, "incognito"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    # invokes: Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->tabClosurePending(IZ)V
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->access$600(Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;IZ)V

    goto/16 :goto_0

    .line 508
    :sswitch_d
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome$1;->this$0:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string/jumbo v2, "tabId"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    const-string/jumbo v3, "incognito"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    # invokes: Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->tabClosureCancelled(IZ)V
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->access$700(Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;IZ)V

    goto/16 :goto_0

    .line 512
    :sswitch_e
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome$1;->this$0:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string/jumbo v2, "tabId"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    const-string/jumbo v3, "incognito"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->tabClosureCommitted(IZ)V

    goto/16 :goto_0

    :cond_6
    move v7, v2

    move v6, v0

    goto/16 :goto_2

    .line 400
    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_1
        0x3 -> :sswitch_0
        0x4 -> :sswitch_5
        0x5 -> :sswitch_3
        0x8 -> :sswitch_7
        0x9 -> :sswitch_9
        0xc -> :sswitch_4
        0x1a -> :sswitch_a
        0x1b -> :sswitch_b
        0x1c -> :sswitch_8
        0x20 -> :sswitch_8
        0x2b -> :sswitch_6
        0x30 -> :sswitch_2
        0x44 -> :sswitch_c
        0x45 -> :sswitch_d
        0x49 -> :sswitch_e
    .end sparse-switch
.end method
