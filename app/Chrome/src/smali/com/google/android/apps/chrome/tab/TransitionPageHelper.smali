.class public Lcom/google/android/apps/chrome/tab/TransitionPageHelper;
.super Lorg/chromium/chrome/browser/EmptyTabObserver;
.source "TransitionPageHelper.java"


# instance fields
.field private final mAnimator:Landroid/animation/ObjectAnimator;

.field private mCSSSelector:Ljava/lang/String;

.field private final mContext:Landroid/content/Context;

.field private mDestinationDOMContentLoaded:Z

.field private mDestinationMainFrameID:J

.field private mDestinationPainted:Z

.field private mDestinationReady:Z

.field private mDestinationReadyToResume:Z

.field private final mHandler:Landroid/os/Handler;

.field private mListener:Lcom/google/android/apps/chrome/tab/TransitionPageHelper$TransitionPageHelperListener;

.field private mNativeTransitionPageHelperPtr:J

.field private mOpacity:F

.field private mSourceColor:Ljava/lang/String;

.field private mSourceContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

.field private mSourceObserver:Lorg/chromium/content/browser/WebContentsObserverAndroid;

.field private mState:I

.field private final mTab:Lorg/chromium/chrome/browser/Tab;

.field private mTransitionColor:Ljava/lang/String;

.field private mTransitionContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

.field private mTransitionFinished:Z

.field private mTransitionFramesPainted:I

.field private mTransitionLoaded:Z

.field private mTransitionObserver:Lorg/chromium/content/browser/WebContentsObserverAndroid;

.field private mTransitionPainted:Z

.field private final mTransitionStylesheets:Ljava/util/ArrayList;

.field private mTransitionUnloaded:Z

.field private mVisible:Z

.field private final mWindowAndroid:Lorg/chromium/ui/base/WindowAndroid;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lorg/chromium/ui/base/WindowAndroid;Lorg/chromium/chrome/browser/Tab;)V
    .locals 4

    .prologue
    .line 171
    invoke-direct {p0}, Lorg/chromium/chrome/browser/EmptyTabObserver;-><init>()V

    .line 172
    iput-object p1, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mContext:Landroid/content/Context;

    .line 173
    iput-object p2, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mWindowAndroid:Lorg/chromium/ui/base/WindowAndroid;

    .line 174
    iput-object p3, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mTab:Lorg/chromium/chrome/browser/Tab;

    .line 175
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mTab:Lorg/chromium/chrome/browser/Tab;

    invoke-virtual {v0, p0}, Lorg/chromium/chrome/browser/Tab;->addObserver(Lorg/chromium/chrome/browser/TabObserver;)V

    .line 177
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->nativeInit()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mNativeTransitionPageHelperPtr:J

    .line 178
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mHandler:Landroid/os/Handler;

    .line 179
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mTransitionStylesheets:Ljava/util/ArrayList;

    .line 180
    const-string/jumbo v0, "transitionOpacity"

    const/4 v1, 0x2

    new-array v1, v1, [F

    fill-array-data v1, :array_0

    invoke-static {p0, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mAnimator:Landroid/animation/ObjectAnimator;

    .line 181
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mAnimator:Landroid/animation/ObjectAnimator;

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 182
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mAnimator:Landroid/animation/ObjectAnimator;

    new-instance v1, Lcom/google/android/apps/chrome/tab/TransitionPageHelper$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/tab/TransitionPageHelper$1;-><init>(Lcom/google/android/apps/chrome/tab/TransitionPageHelper;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 194
    const-string/jumbo v0, "NavigationTransitions"

    invoke-static {v0}, Lorg/chromium/base/FieldTrialList;->findFullName(Ljava/lang/String;)Ljava/lang/String;

    .line 196
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->resetTransitionState()V

    .line 197
    return-void

    .line 180
    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/tab/TransitionPageHelper;)Lorg/chromium/content/browser/ContentViewCore;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mTransitionContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/google/android/apps/chrome/tab/TransitionPageHelper;)Z
    .locals 1

    .prologue
    .line 32
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mTransitionPainted:Z

    return v0
.end method

.method static synthetic access$1002(Lcom/google/android/apps/chrome/tab/TransitionPageHelper;Z)Z
    .locals 0

    .prologue
    .line 32
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mTransitionPainted:Z

    return p1
.end method

.method static synthetic access$102(Lcom/google/android/apps/chrome/tab/TransitionPageHelper;Z)Z
    .locals 0

    .prologue
    .line 32
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mTransitionUnloaded:Z

    return p1
.end method

.method static synthetic access$1102(Lcom/google/android/apps/chrome/tab/TransitionPageHelper;Z)Z
    .locals 0

    .prologue
    .line 32
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mTransitionLoaded:Z

    return p1
.end method

.method static synthetic access$1200(Lcom/google/android/apps/chrome/tab/TransitionPageHelper;)Z
    .locals 1

    .prologue
    .line 32
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mDestinationDOMContentLoaded:Z

    return v0
.end method

.method static synthetic access$1202(Lcom/google/android/apps/chrome/tab/TransitionPageHelper;Z)Z
    .locals 0

    .prologue
    .line 32
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mDestinationDOMContentLoaded:Z

    return p1
.end method

.method static synthetic access$1300(Lcom/google/android/apps/chrome/tab/TransitionPageHelper;)Z
    .locals 1

    .prologue
    .line 32
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mDestinationPainted:Z

    return v0
.end method

.method static synthetic access$1302(Lcom/google/android/apps/chrome/tab/TransitionPageHelper;Z)Z
    .locals 0

    .prologue
    .line 32
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mDestinationPainted:Z

    return p1
.end method

.method static synthetic access$1400(Lcom/google/android/apps/chrome/tab/TransitionPageHelper;)Z
    .locals 1

    .prologue
    .line 32
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mDestinationReady:Z

    return v0
.end method

.method static synthetic access$1402(Lcom/google/android/apps/chrome/tab/TransitionPageHelper;Z)Z
    .locals 0

    .prologue
    .line 32
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mDestinationReady:Z

    return p1
.end method

.method static synthetic access$1500(Lcom/google/android/apps/chrome/tab/TransitionPageHelper;)J
    .locals 2

    .prologue
    .line 32
    iget-wide v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mDestinationMainFrameID:J

    return-wide v0
.end method

.method static synthetic access$1502(Lcom/google/android/apps/chrome/tab/TransitionPageHelper;J)J
    .locals 1

    .prologue
    .line 32
    iput-wide p1, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mDestinationMainFrameID:J

    return-wide p1
.end method

.method static synthetic access$1700(Lcom/google/android/apps/chrome/tab/TransitionPageHelper;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/google/android/apps/chrome/tab/TransitionPageHelper;)Z
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->isTransitionRunning()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1902(Lcom/google/android/apps/chrome/tab/TransitionPageHelper;Z)Z
    .locals 0

    .prologue
    .line 32
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mDestinationReadyToResume:Z

    return p1
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/tab/TransitionPageHelper;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->updateState()V

    return-void
.end method

.method static synthetic access$2000(Lcom/google/android/apps/chrome/tab/TransitionPageHelper;F)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->setTransitionOpacity(F)V

    return-void
.end method

.method static synthetic access$2100(Lcom/google/android/apps/chrome/tab/TransitionPageHelper;Z)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->setTransitionVisibility(Z)V

    return-void
.end method

.method static synthetic access$2202(Lcom/google/android/apps/chrome/tab/TransitionPageHelper;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 32
    iput-object p1, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mCSSSelector:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$2300(Lcom/google/android/apps/chrome/tab/TransitionPageHelper;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->startTransitionNavigation()V

    return-void
.end method

.method static synthetic access$2400(Lcom/google/android/apps/chrome/tab/TransitionPageHelper;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mTransitionStylesheets:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/android/apps/chrome/tab/TransitionPageHelper;I)I
    .locals 0

    .prologue
    .line 32
    iput p1, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mTransitionFramesPainted:I

    return p1
.end method

.method static synthetic access$308(Lcom/google/android/apps/chrome/tab/TransitionPageHelper;)I
    .locals 2

    .prologue
    .line 32
    iget v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mTransitionFramesPainted:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mTransitionFramesPainted:I

    return v0
.end method

.method static synthetic access$402(Lcom/google/android/apps/chrome/tab/TransitionPageHelper;Z)Z
    .locals 0

    .prologue
    .line 32
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mTransitionFinished:Z

    return p1
.end method

.method static synthetic access$500(Lcom/google/android/apps/chrome/tab/TransitionPageHelper;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mSourceColor:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$502(Lcom/google/android/apps/chrome/tab/TransitionPageHelper;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 32
    iput-object p1, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mSourceColor:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$600(Lcom/google/android/apps/chrome/tab/TransitionPageHelper;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mTransitionColor:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$602(Lcom/google/android/apps/chrome/tab/TransitionPageHelper;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 32
    iput-object p1, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mTransitionColor:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$700(Lcom/google/android/apps/chrome/tab/TransitionPageHelper;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->scheduleTransitionFramesTimeout()V

    return-void
.end method

.method static synthetic access$800(Lcom/google/android/apps/chrome/tab/TransitionPageHelper;)I
    .locals 1

    .prologue
    .line 32
    iget v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mState:I

    return v0
.end method

.method static synthetic access$900(Lcom/google/android/apps/chrome/tab/TransitionPageHelper;)Lorg/chromium/chrome/browser/Tab;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mTab:Lorg/chromium/chrome/browser/Tab;

    return-object v0
.end method

.method private applyTransitionStylesheets()V
    .locals 3

    .prologue
    .line 277
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mTransitionStylesheets:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 278
    iget-object v2, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mTransitionContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mTransitionStylesheets:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, Lorg/chromium/content/browser/ContentViewCore;->addStyleSheetByURL(Ljava/lang/String;)V

    .line 277
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 281
    :cond_0
    return-void
.end method

.method private buildContentView()V
    .locals 7

    .prologue
    .line 447
    iget-wide v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mNativeTransitionPageHelperPtr:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 481
    :goto_0
    return-void

    .line 449
    :cond_0
    new-instance v0, Lorg/chromium/content/browser/ContentViewCore;

    iget-object v1, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lorg/chromium/content/browser/ContentViewCore;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mTransitionContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    .line 450
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mTransitionContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    invoke-static {v0, v1}, Lorg/chromium/content/browser/ContentView;->newInstance(Landroid/content/Context;Lorg/chromium/content/browser/ContentViewCore;)Lorg/chromium/content/browser/ContentView;

    move-result-object v2

    .line 451
    iget-object v1, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mTransitionContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mSourceContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    invoke-static {v0}, Lorg/chromium/chrome/browser/ContentViewUtil;->createNativeWebContentsWithSharedSiteInstance(Lorg/chromium/content/browser/ContentViewCore;)J

    move-result-wide v4

    iget-object v6, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mWindowAndroid:Lorg/chromium/ui/base/WindowAndroid;

    move-object v3, v2

    invoke-virtual/range {v1 .. v6}, Lorg/chromium/content/browser/ContentViewCore;->initialize(Landroid/view/ViewGroup;Lorg/chromium/content/browser/ContentViewCore$InternalAccessDelegate;JLorg/chromium/ui/base/WindowAndroid;)V

    .line 456
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->createTransitionObserver()Lorg/chromium/content/browser/WebContentsObserverAndroid;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mTransitionObserver:Lorg/chromium/content/browser/WebContentsObserverAndroid;

    .line 458
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mListener:Lcom/google/android/apps/chrome/tab/TransitionPageHelper$TransitionPageHelperListener;

    if-eqz v0, :cond_1

    .line 459
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mListener:Lcom/google/android/apps/chrome/tab/TransitionPageHelper$TransitionPageHelperListener;

    invoke-interface {v0, p0}, Lcom/google/android/apps/chrome/tab/TransitionPageHelper$TransitionPageHelperListener;->onTransitionPageCreated(Lcom/google/android/apps/chrome/tab/TransitionPageHelper;)V

    .line 462
    :cond_1
    iget-wide v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mNativeTransitionPageHelperPtr:J

    iget-object v2, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mTransitionContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->nativeSetWebContents(JLorg/chromium/content/browser/ContentViewCore;)V

    .line 463
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->setTransitionOpacity(F)V

    .line 465
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mTransitionContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    new-instance v1, Lcom/google/android/apps/chrome/tab/TransitionPageHelper$5;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/tab/TransitionPageHelper$5;-><init>(Lcom/google/android/apps/chrome/tab/TransitionPageHelper;)V

    invoke-virtual {v0, v1}, Lorg/chromium/content/browser/ContentViewCore;->setContentViewClient(Lorg/chromium/content/browser/ContentViewClient;)V

    goto :goto_0
.end method

.method private createSourceObserver()Lorg/chromium/content/browser/WebContentsObserverAndroid;
    .locals 2

    .prologue
    .line 512
    new-instance v0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper$7;

    iget-object v1, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mSourceContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    invoke-virtual {v1}, Lorg/chromium/content/browser/ContentViewCore;->getWebContents()Lorg/chromium/content_public/browser/WebContents;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/chrome/tab/TransitionPageHelper$7;-><init>(Lcom/google/android/apps/chrome/tab/TransitionPageHelper;Lorg/chromium/content_public/browser/WebContents;)V

    return-object v0
.end method

.method private createTransitionObserver()Lorg/chromium/content/browser/WebContentsObserverAndroid;
    .locals 2

    .prologue
    .line 488
    new-instance v0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper$6;

    iget-object v1, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mTransitionContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    invoke-virtual {v1}, Lorg/chromium/content/browser/ContentViewCore;->getWebContents()Lorg/chromium/content_public/browser/WebContents;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/chrome/tab/TransitionPageHelper$6;-><init>(Lcom/google/android/apps/chrome/tab/TransitionPageHelper;Lorg/chromium/content_public/browser/WebContents;)V

    return-object v0
.end method

.method private destroy()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 210
    iget-wide v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mNativeTransitionPageHelperPtr:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 216
    :goto_0
    return-void

    .line 212
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->teardownNavigation()V

    .line 214
    iget-wide v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mNativeTransitionPageHelperPtr:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->nativeDestroy(J)V

    .line 215
    iput-wide v2, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mNativeTransitionPageHelperPtr:J

    goto :goto_0
.end method

.method private getNativePtr()J
    .locals 2

    .prologue
    .line 625
    iget-wide v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mNativeTransitionPageHelperPtr:J

    return-wide v0
.end method

.method private isTransitionRunning()Z
    .locals 1

    .prologue
    .line 222
    iget v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mState:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private native nativeDestroy(J)V
.end method

.method private native nativeInit()J
.end method

.method private native nativeReleaseWebContents(J)V
.end method

.method private native nativeSetOpacity(JLorg/chromium/content/browser/ContentViewCore;F)V
.end method

.method private native nativeSetWebContents(JLorg/chromium/content/browser/ContentViewCore;)V
.end method

.method private resetTransitionState()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 375
    iput-boolean v2, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mTransitionUnloaded:Z

    .line 376
    iput-boolean v2, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mTransitionFinished:Z

    .line 377
    iput-boolean v2, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mDestinationReady:Z

    .line 378
    iput-boolean v2, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mDestinationReadyToResume:Z

    .line 379
    iput-boolean v2, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mDestinationPainted:Z

    .line 380
    iput-boolean v2, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mDestinationDOMContentLoaded:Z

    .line 381
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mDestinationMainFrameID:J

    .line 382
    iput-boolean v2, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mTransitionPainted:Z

    .line 383
    iput v2, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mTransitionFramesPainted:I

    .line 384
    iput-boolean v2, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mTransitionLoaded:Z

    .line 385
    iput v2, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mState:I

    .line 386
    iput-boolean v2, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mVisible:Z

    .line 387
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mOpacity:F

    .line 388
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mTransitionStylesheets:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 389
    iput-object v3, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mTransitionColor:Ljava/lang/String;

    .line 390
    iput-object v3, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mCSSSelector:Ljava/lang/String;

    .line 391
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 392
    return-void
.end method

.method private scheduleCurrentToUnload()V
    .locals 4

    .prologue
    .line 244
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/chrome/tab/TransitionPageHelper$3;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/tab/TransitionPageHelper$3;-><init>(Lcom/google/android/apps/chrome/tab/TransitionPageHelper;)V

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 260
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->setTransitionVisibility(Z)V

    .line 261
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->setTransitionOpacity(F)V

    .line 263
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mTransitionContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mSourceContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    if-eqz v0, :cond_0

    .line 267
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mSourceContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    iget-object v1, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mCSSSelector:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lorg/chromium/content/browser/ContentViewCore;->beginExitTransition(Ljava/lang/String;)V

    .line 270
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->applyTransitionStylesheets()V

    .line 271
    return-void
.end method

.method private scheduleTransitionFramesTimeout()V
    .locals 4

    .prologue
    .line 229
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/chrome/tab/TransitionPageHelper$2;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/tab/TransitionPageHelper$2;-><init>(Lcom/google/android/apps/chrome/tab/TransitionPageHelper;)V

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 237
    return-void
.end method

.method private setTransitionOpacity(F)V
    .locals 3

    .prologue
    .line 341
    iput p1, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mOpacity:F

    .line 343
    iget-wide v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mNativeTransitionPageHelperPtr:J

    iget-object v2, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mTransitionContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    invoke-direct {p0, v0, v1, v2, p1}, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->nativeSetOpacity(JLorg/chromium/content/browser/ContentViewCore;F)V

    .line 344
    return-void
.end method

.method private setTransitionVisibility(Z)V
    .locals 1

    .prologue
    .line 350
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mVisible:Z

    .line 352
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mListener:Lcom/google/android/apps/chrome/tab/TransitionPageHelper$TransitionPageHelperListener;

    if-eqz v0, :cond_0

    .line 353
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mListener:Lcom/google/android/apps/chrome/tab/TransitionPageHelper$TransitionPageHelperListener;

    invoke-interface {v0, p0, p1}, Lcom/google/android/apps/chrome/tab/TransitionPageHelper$TransitionPageHelperListener;->onTransitionPageVisibility(Lcom/google/android/apps/chrome/tab/TransitionPageHelper;Z)V

    .line 355
    :cond_0
    return-void
.end method

.method private startTransitionNavigation()V
    .locals 3

    .prologue
    .line 399
    iget v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mState:I

    if-eqz v0, :cond_0

    .line 419
    :goto_0
    return-void

    .line 401
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->teardownNavigation()V

    .line 402
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->resetTransitionState()V

    .line 404
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mState:I

    .line 406
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->buildContentView()V

    .line 410
    new-instance v0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper$4;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/tab/TransitionPageHelper$4;-><init>(Lcom/google/android/apps/chrome/tab/TransitionPageHelper;)V

    .line 417
    const-string/jumbo v1, "getComputedStyle(document.body).backgroundColor"

    .line 418
    iget-object v2, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mSourceContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    invoke-virtual {v2, v1, v0}, Lorg/chromium/content/browser/ContentViewCore;->evaluateJavaScript(Ljava/lang/String;Lorg/chromium/content_public/browser/JavaScriptCallback;)V

    goto :goto_0
.end method

.method private teardownNavigation()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 425
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 426
    iget-wide v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mNativeTransitionPageHelperPtr:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 441
    :goto_0
    return-void

    .line 428
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mTransitionObserver:Lorg/chromium/content/browser/WebContentsObserverAndroid;

    if-eqz v0, :cond_1

    .line 429
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mTransitionObserver:Lorg/chromium/content/browser/WebContentsObserverAndroid;

    invoke-virtual {v0}, Lorg/chromium/content/browser/WebContentsObserverAndroid;->detachFromWebContents()V

    .line 430
    iput-object v4, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mTransitionObserver:Lorg/chromium/content/browser/WebContentsObserverAndroid;

    .line 432
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mTransitionContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    if-eqz v0, :cond_3

    .line 433
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mListener:Lcom/google/android/apps/chrome/tab/TransitionPageHelper$TransitionPageHelperListener;

    if-eqz v0, :cond_2

    .line 434
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mListener:Lcom/google/android/apps/chrome/tab/TransitionPageHelper$TransitionPageHelperListener;

    invoke-interface {v0, p0}, Lcom/google/android/apps/chrome/tab/TransitionPageHelper$TransitionPageHelperListener;->onTransitionPageDestroyed(Lcom/google/android/apps/chrome/tab/TransitionPageHelper;)V

    .line 436
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mTransitionContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->destroy()V

    .line 437
    iput-object v4, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mTransitionContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    .line 438
    iget-wide v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mNativeTransitionPageHelperPtr:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->nativeReleaseWebContents(J)V

    .line 440
    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mState:I

    goto :goto_0
.end method

.method private updateState()V
    .locals 6

    .prologue
    const/4 v5, 0x5

    const/4 v4, 0x4

    const/4 v3, 0x3

    const/4 v2, 0x2

    .line 295
    iget v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mState:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 299
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mTransitionPainted:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mTransitionLoaded:Z

    if-eqz v0, :cond_0

    .line 300
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->scheduleCurrentToUnload()V

    .line 301
    iput v2, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mState:I

    .line 335
    :cond_0
    :goto_0
    return-void

    .line 303
    :cond_1
    iget v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mState:I

    if-ne v0, v2, :cond_3

    .line 307
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mDestinationReadyToResume:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mTransitionFinished:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mTransitionFramesPainted:I

    if-lt v0, v2, :cond_0

    .line 309
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mSourceContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    if-eqz v0, :cond_2

    .line 310
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mSourceContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->resumeResponseDeferredAtStart()V

    .line 312
    :cond_2
    iput v3, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mState:I

    .line 313
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->updateState()V

    goto :goto_0

    .line 315
    :cond_3
    iget v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mState:I

    if-ne v0, v3, :cond_4

    .line 319
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mDestinationReady:Z

    if-eqz v0, :cond_0

    .line 320
    iput v4, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mState:I

    .line 321
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->updateState()V

    goto :goto_0

    .line 323
    :cond_4
    iget v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mState:I

    if-ne v0, v4, :cond_5

    .line 326
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 327
    iput v5, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mState:I

    goto :goto_0

    .line 328
    :cond_5
    iget v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mState:I

    if-ne v0, v5, :cond_0

    .line 330
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mTransitionUnloaded:Z

    if-eqz v0, :cond_0

    .line 331
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->teardownNavigation()V

    .line 332
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mState:I

    goto :goto_0
.end method


# virtual methods
.method public getTransitionContentViewCore()Lorg/chromium/content/browser/ContentViewCore;
    .locals 1

    .prologue
    .line 203
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mTransitionContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    return-object v0
.end method

.method public isTransitionVisible()Z
    .locals 1

    .prologue
    .line 361
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mVisible:Z

    return v0
.end method

.method public onContentChanged(Lorg/chromium/chrome/browser/Tab;)V
    .locals 2

    .prologue
    .line 557
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mTab:Lorg/chromium/chrome/browser/Tab;

    if-ne p1, v0, :cond_2

    .line 558
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mSourceContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mSourceContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->getWebContents()Lorg/chromium/content_public/browser/WebContents;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 559
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mSourceContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->getWebContents()Lorg/chromium/content_public/browser/WebContents;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lorg/chromium/content_public/browser/WebContents;->setNavigationTransitionDelegate(Lorg/chromium/content_public/browser/NavigationTransitionDelegate;)V

    .line 562
    :cond_0
    invoke-virtual {p1}, Lorg/chromium/chrome/browser/Tab;->getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mSourceContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    .line 564
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mSourceObserver:Lorg/chromium/content/browser/WebContentsObserverAndroid;

    if-eqz v0, :cond_1

    .line 565
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mSourceObserver:Lorg/chromium/content/browser/WebContentsObserverAndroid;

    invoke-virtual {v0}, Lorg/chromium/content/browser/WebContentsObserverAndroid;->detachFromWebContents()V

    .line 568
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mSourceContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    if-nez v0, :cond_3

    .line 614
    :cond_2
    :goto_0
    return-void

    .line 570
    :cond_3
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->createSourceObserver()Lorg/chromium/content/browser/WebContentsObserverAndroid;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mSourceObserver:Lorg/chromium/content/browser/WebContentsObserverAndroid;

    .line 571
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mSourceContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->getWebContents()Lorg/chromium/content_public/browser/WebContents;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/chrome/tab/TransitionPageHelper$8;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/tab/TransitionPageHelper$8;-><init>(Lcom/google/android/apps/chrome/tab/TransitionPageHelper;)V

    invoke-interface {v0, v1}, Lorg/chromium/content_public/browser/WebContents;->setNavigationTransitionDelegate(Lorg/chromium/content_public/browser/NavigationTransitionDelegate;)V

    goto :goto_0
.end method

.method public onDestroyed(Lorg/chromium/chrome/browser/Tab;)V
    .locals 1

    .prologue
    .line 618
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mTab:Lorg/chromium/chrome/browser/Tab;

    if-ne p1, v0, :cond_0

    .line 619
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->destroy()V

    .line 621
    :cond_0
    return-void
.end method

.method public setListener(Lcom/google/android/apps/chrome/tab/TransitionPageHelper$TransitionPageHelperListener;)V
    .locals 0

    .prologue
    .line 162
    iput-object p1, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mListener:Lcom/google/android/apps/chrome/tab/TransitionPageHelper$TransitionPageHelperListener;

    .line 163
    return-void
.end method
