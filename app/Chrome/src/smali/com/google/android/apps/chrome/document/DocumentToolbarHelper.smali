.class public Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;
.super Ljava/lang/Object;
.source "DocumentToolbarHelper.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private mActionBarDelegate:Lcom/google/android/apps/chrome/ContextualMenuBar$ActionBarDelegate;

.field protected final mActivity:Lcom/google/android/apps/chrome/document/DocumentActivity;

.field private mContextualMenuBar:Lcom/google/android/apps/chrome/ContextualMenuBar;

.field protected mControls:Lcom/google/android/apps/chrome/document/DocumentToolbarHelper$ToolbarControls;

.field private mCurrentControlContainer:Landroid/view/View;

.field private final mIsIncognito:Z

.field private mIsUsingChromeOmnibox:Z

.field private final mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

.field protected final mParentView:Landroid/view/ViewGroup;

.field private mRegisteredForNotifications:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const-class v0, Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/google/android/apps/chrome/document/DocumentActivity;Landroid/view/ViewGroup;Z)V
    .locals 2

    .prologue
    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    new-instance v0, Lcom/google/android/apps/chrome/document/DocumentToolbarHelper$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/document/DocumentToolbarHelper$1;-><init>(Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    .line 88
    iput-object p1, p0, Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;->mActivity:Lcom/google/android/apps/chrome/document/DocumentActivity;

    .line 89
    iput-object p2, p0, Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;->mParentView:Landroid/view/ViewGroup;

    .line 90
    iput-boolean p3, p0, Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;->mIsIncognito:Z

    .line 91
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;->inflateControls()Z

    .line 93
    new-instance v0, Lcom/google/android/apps/chrome/document/DocumentToolbarHelper$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/document/DocumentToolbarHelper$2;-><init>(Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;->mActionBarDelegate:Lcom/google/android/apps/chrome/ContextualMenuBar$ActionBarDelegate;

    .line 121
    new-instance v0, Lcom/google/android/apps/chrome/ContextualMenuBar;

    iget-object v1, p0, Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;->mActionBarDelegate:Lcom/google/android/apps/chrome/ContextualMenuBar$ActionBarDelegate;

    invoke-direct {v0, p1, v1}, Lcom/google/android/apps/chrome/ContextualMenuBar;-><init>(Landroid/content/Context;Lcom/google/android/apps/chrome/ContextualMenuBar$ActionBarDelegate;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;->mContextualMenuBar:Lcom/google/android/apps/chrome/ContextualMenuBar;

    .line 122
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;->mContextualMenuBar:Lcom/google/android/apps/chrome/ContextualMenuBar;

    new-instance v1, Lcom/google/android/apps/chrome/CustomSelectionActionModeCallback;

    invoke-direct {v1}, Lcom/google/android/apps/chrome/CustomSelectionActionModeCallback;-><init>()V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/ContextualMenuBar;->setCustomSelectionActionModeCallback(Lcom/google/android/apps/chrome/CustomSelectionActionModeCallback;)V

    .line 124
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;)Lcom/google/android/apps/chrome/ContextualMenuBar$ActionBarDelegate;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;->mActionBarDelegate:Lcom/google/android/apps/chrome/ContextualMenuBar$ActionBarDelegate;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;)Landroid/view/View;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;->mCurrentControlContainer:Landroid/view/View;

    return-object v0
.end method

.method private shouldUseChromeOmnibox()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 224
    iget-object v1, p0, Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;->mParentView:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string/jumbo v2, "default_search_engine_is_google"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 227
    iget-object v2, p0, Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;->mParentView:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    const-string/jumbo v3, "search_suggestions"

    invoke-interface {v2, v3, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    .line 230
    iget-boolean v3, p0, Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;->mIsIncognito:Z

    if-nez v3, :cond_0

    if-eqz v1, :cond_0

    if-eqz v2, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;->mParentView:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/chrome/document/ssb/SearchLauncher;->isGSAAboveMinVersion(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;->mParentView:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/chrome/document/ssb/SearchLauncher;->isGSAAvailable(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public destroy()V
    .locals 3

    .prologue
    .line 246
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;->mControls:Lcom/google/android/apps/chrome/document/DocumentToolbarHelper$ToolbarControls;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;->mControls:Lcom/google/android/apps/chrome/document/DocumentToolbarHelper$ToolbarControls;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/document/DocumentToolbarHelper$ToolbarControls;->destroy()V

    .line 247
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;->mRegisteredForNotifications:Z

    if-eqz v0, :cond_1

    .line 248
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;->mActivity:Lcom/google/android/apps/chrome/document/DocumentActivity;

    const/16 v1, 0x22

    iget-object v2, p0, Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->unregisterForNotification(Landroid/content/Context;ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    .line 251
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;->mRegisteredForNotifications:Z

    .line 253
    :cond_1
    return-void
.end method

.method public focusUrlBar()V
    .locals 1

    .prologue
    .line 206
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;->mControls:Lcom/google/android/apps/chrome/document/DocumentToolbarHelper$ToolbarControls;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/document/DocumentToolbarHelper$ToolbarControls;->focusUrlBar()V

    .line 207
    return-void
.end method

.method getContextualMenuBar()Lcom/google/android/apps/chrome/ContextualMenuBar;
    .locals 1

    .prologue
    .line 178
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;->mContextualMenuBar:Lcom/google/android/apps/chrome/ContextualMenuBar;

    return-object v0
.end method

.method public getMenuAnchor()Landroid/view/View;
    .locals 1

    .prologue
    .line 199
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;->mControls:Lcom/google/android/apps/chrome/document/DocumentToolbarHelper$ToolbarControls;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/document/DocumentToolbarHelper$ToolbarControls;->getMenuAnchor()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method inflateAndAttachControls(Landroid/view/ViewGroup;Z)V
    .locals 2

    .prologue
    .line 218
    if-eqz p2, :cond_0

    sget v0, Lcom/google/android/apps/chrome/R$layout;->document_omnibox:I

    .line 220
    :goto_0
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    invoke-virtual {v1, v0, p1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 221
    return-void

    .line 218
    :cond_0
    sget v0, Lcom/google/android/apps/chrome/R$layout;->document_control_container:I

    goto :goto_0
.end method

.method public inflateControls()Z
    .locals 4

    .prologue
    .line 132
    invoke-direct {p0}, Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;->shouldUseChromeOmnibox()Z

    move-result v0

    .line 133
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;->isUsingChromeOmnibox()Z

    move-result v1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;->isInitialized()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    .line 146
    :goto_0
    return v0

    .line 134
    :cond_0
    iput-boolean v0, p0, Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;->mIsUsingChromeOmnibox:Z

    .line 137
    iget-object v1, p0, Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;->mParentView:Landroid/view/ViewGroup;

    sget v2, Lcom/google/android/apps/chrome/R$id;->omnibox_results_container_stub:I

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 138
    iget-object v2, p0, Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;->mCurrentControlContainer:Landroid/view/View;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;->mParentView:Landroid/view/ViewGroup;

    iget-object v3, p0, Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;->mCurrentControlContainer:Landroid/view/View;

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 139
    :cond_1
    if-eqz v1, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;->mParentView:Landroid/view/ViewGroup;

    invoke-virtual {v2, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 142
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;->mParentView:Landroid/view/ViewGroup;

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;->inflateAndAttachControls(Landroid/view/ViewGroup;Z)V

    .line 143
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;->mParentView:Landroid/view/ViewGroup;

    sget v1, Lcom/google/android/apps/chrome/R$id;->control_container:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;->mCurrentControlContainer:Landroid/view/View;

    .line 144
    sget-boolean v0, Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;->$assertionsDisabled:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;->mCurrentControlContainer:Landroid/view/View;

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 146
    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method initializeControls(Lorg/chromium/chrome/browser/appmenu/AppMenuHandler;Lcom/google/android/apps/chrome/document/TitleBar$DocumentDelegate;Lcom/google/android/apps/chrome/document/TitleBar$TitleBarListener;)V
    .locals 6

    .prologue
    .line 157
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;->mControls:Lcom/google/android/apps/chrome/document/DocumentToolbarHelper$ToolbarControls;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;->mControls:Lcom/google/android/apps/chrome/document/DocumentToolbarHelper$ToolbarControls;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/document/DocumentToolbarHelper$ToolbarControls;->destroy()V

    .line 158
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;->mRegisteredForNotifications:Z

    if-nez v0, :cond_1

    .line 159
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;->mActivity:Lcom/google/android/apps/chrome/document/DocumentActivity;

    const/16 v1, 0x22

    iget-object v2, p0, Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->registerForNotification(Landroid/content/Context;ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    .line 162
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;->mRegisteredForNotifications:Z

    .line 165
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;->isUsingChromeOmnibox()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 166
    new-instance v0, Lcom/google/android/apps/chrome/toolbar/OmniboxToolbarControls;

    iget-object v1, p0, Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;->mParentView:Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;->mActivity:Lcom/google/android/apps/chrome/document/DocumentActivity;

    iget-object v3, p0, Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;->mContextualMenuBar:Lcom/google/android/apps/chrome/ContextualMenuBar;

    invoke-direct {v0, v1, v2, p1, v3}, Lcom/google/android/apps/chrome/toolbar/OmniboxToolbarControls;-><init>(Landroid/view/View;Lcom/google/android/apps/chrome/ChromeActivity;Lorg/chromium/chrome/browser/appmenu/AppMenuHandler;Lcom/google/android/apps/chrome/ContextualMenuBar;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;->mControls:Lcom/google/android/apps/chrome/document/DocumentToolbarHelper$ToolbarControls;

    .line 172
    :goto_0
    return-void

    .line 169
    :cond_2
    new-instance v0, Lcom/google/android/apps/chrome/document/DocumentToolbarControls;

    iget-object v1, p0, Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;->mParentView:Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;->mActivity:Lcom/google/android/apps/chrome/document/DocumentActivity;

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/chrome/document/DocumentToolbarControls;-><init>(Landroid/view/View;Lcom/google/android/apps/chrome/ChromeActivity;Lorg/chromium/chrome/browser/appmenu/AppMenuHandler;Lcom/google/android/apps/chrome/document/TitleBar$DocumentDelegate;Lcom/google/android/apps/chrome/document/TitleBar$TitleBarListener;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;->mControls:Lcom/google/android/apps/chrome/document/DocumentToolbarHelper$ToolbarControls;

    goto :goto_0
.end method

.method isInitialized()Z
    .locals 1

    .prologue
    .line 192
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;->mControls:Lcom/google/android/apps/chrome/document/DocumentToolbarHelper$ToolbarControls;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method isUsingChromeOmnibox()Z
    .locals 1

    .prologue
    .line 185
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;->mIsUsingChromeOmnibox:Z

    return v0
.end method

.method public onOrientationChange()V
    .locals 1

    .prologue
    .line 259
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;->mContextualMenuBar:Lcom/google/android/apps/chrome/ContextualMenuBar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ContextualMenuBar;->showControlsOnOrientationChange()V

    .line 260
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 238
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 240
    :goto_0
    return-void

    .line 239
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;->mControls:Lcom/google/android/apps/chrome/document/DocumentToolbarHelper$ToolbarControls;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/document/DocumentToolbarHelper$ToolbarControls;->onResume()V

    goto :goto_0
.end method

.method setThemeColor(I)V
    .locals 1

    .prologue
    .line 214
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;->mControls:Lcom/google/android/apps/chrome/document/DocumentToolbarHelper$ToolbarControls;

    invoke-interface {v0, p1}, Lcom/google/android/apps/chrome/document/DocumentToolbarHelper$ToolbarControls;->setThemeColor(I)V

    .line 215
    return-void
.end method
