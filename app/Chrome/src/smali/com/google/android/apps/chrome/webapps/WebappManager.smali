.class public Lcom/google/android/apps/chrome/webapps/WebappManager;
.super Landroid/app/Activity;
.source "WebappManager.java"


# static fields
.field public static final ACTION_START_WEBAPP:Ljava/lang/String; = "com.google.android.apps.chrome.webapps.WebappManager.ACTION_START_WEBAPP"

.field public static final WEBAPP_SCHEME:Ljava/lang/String; = "webapp://"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method static launchWebapp(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 3

    .prologue
    .line 93
    const-class v0, Lcom/google/android/apps/chrome/webapps/WebappActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    .line 94
    invoke-static {p0}, Lcom/google/android/apps/chrome/utilities/FeatureUtilitiesInternal;->isDocumentMode(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 96
    invoke-static {p0}, Lcom/google/android/apps/chrome/webapps/ActivityAssigner;->instance(Landroid/content/Context;)Lcom/google/android/apps/chrome/webapps/ActivityAssigner;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/android/apps/chrome/webapps/ActivityAssigner;->assign(Ljava/lang/String;)I

    move-result v1

    .line 97
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 101
    :cond_0
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 102
    invoke-virtual {v1, p0, v0}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    .line 103
    const-string/jumbo v0, "org.chromium.chrome.browser.webapp_icon"

    invoke-virtual {v1, v0, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 104
    const-string/jumbo v0, "org.chromium.chrome.browser.webapp_id"

    invoke-virtual {v1, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 105
    const-string/jumbo v0, "org.chromium.chrome.browser.webapp_url"

    invoke-virtual {v1, v0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 106
    const-string/jumbo v0, "org.chromium.chrome.browser.webapp_title"

    invoke-virtual {v1, v0, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 107
    const-string/jumbo v0, "org.chromium.content_public.common.orientation"

    invoke-virtual {v1, v0, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 110
    const-string/jumbo v0, "android.intent.action.VIEW"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 111
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "webapp://"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 112
    const/high16 v0, 0x80000

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 114
    invoke-virtual {p0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 115
    return-void
.end method

.method public static showDialog(Landroid/app/Activity;Lorg/chromium/chrome/browser/Tab;)V
    .locals 6

    .prologue
    .line 123
    invoke-virtual {p0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$layout;->single_line_edit_dialog:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 125
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v1, Lcom/google/android/apps/chrome/R$string;->menu_add_to_homescreen:I

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$string;->cancel:I

    new-instance v3, Lcom/google/android/apps/chrome/webapps/WebappManager$1;

    invoke-direct {v3}, Lcom/google/android/apps/chrome/webapps/WebappManager$1;-><init>()V

    invoke-virtual {v0, v1, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 134
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    .line 138
    sget v0, Lcom/google/android/apps/chrome/R$id;->title:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 139
    sget v1, Lcom/google/android/apps/chrome/R$id;->text:I

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    .line 140
    sget v4, Lcom/google/android/apps/chrome/R$string;->add_to_homescreen_title:I

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(I)V

    .line 141
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 143
    new-instance v0, Lorg/chromium/chrome/browser/ShortcutHelper;

    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v0, v4, p1}, Lorg/chromium/chrome/browser/ShortcutHelper;-><init>(Landroid/content/Context;Lorg/chromium/chrome/browser/Tab;)V

    .line 151
    new-instance v4, Lcom/google/android/apps/chrome/webapps/WebappManager$2;

    invoke-direct {v4, v3, v1}, Lcom/google/android/apps/chrome/webapps/WebappManager$2;-><init>(Landroid/app/AlertDialog;Landroid/widget/EditText;)V

    invoke-virtual {v0, v4}, Lorg/chromium/chrome/browser/ShortcutHelper;->initialize(Lorg/chromium/chrome/browser/ShortcutHelper$OnInitialized;)V

    .line 169
    new-instance v4, Lcom/google/android/apps/chrome/webapps/WebappManager$3;

    invoke-direct {v4, v3}, Lcom/google/android/apps/chrome/webapps/WebappManager$3;-><init>(Landroid/app/AlertDialog;)V

    invoke-virtual {v1, v4}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 188
    invoke-virtual {v3, v2}, Landroid/app/AlertDialog;->setView(Landroid/view/View;)V

    .line 189
    const/4 v2, -0x1

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/google/android/apps/chrome/R$string;->add:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lcom/google/android/apps/chrome/webapps/WebappManager$4;

    invoke-direct {v5, v0, v1}, Lcom/google/android/apps/chrome/webapps/WebappManager$4;-><init>(Lorg/chromium/chrome/browser/ShortcutHelper;Landroid/widget/EditText;)V

    invoke-virtual {v3, v2, v4, v5}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 214
    new-instance v1, Lcom/google/android/apps/chrome/webapps/WebappManager$5;

    invoke-direct {v1, v0}, Lcom/google/android/apps/chrome/webapps/WebappManager$5;-><init>(Lorg/chromium/chrome/browser/ShortcutHelper;)V

    invoke-virtual {v3, v1}, Landroid/app/AlertDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 223
    new-instance v1, Lcom/google/android/apps/chrome/webapps/WebappManager$6;

    invoke-direct {v1, v0, v3}, Lcom/google/android/apps/chrome/webapps/WebappManager$6;-><init>(Lorg/chromium/chrome/browser/ShortcutHelper;Landroid/app/AlertDialog;)V

    invoke-virtual {v3, v1}, Landroid/app/AlertDialog;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V

    .line 231
    invoke-virtual {v3}, Landroid/app/AlertDialog;->show()V

    .line 232
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 49
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 52
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/webapps/WebappManager;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "org.chromium.chrome.browser.webapp_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 53
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/webapps/WebappManager;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v2, "org.chromium.chrome.browser.webapp_url"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 54
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/webapps/WebappManager;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v3, "org.chromium.chrome.browser.webapp_title"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 55
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/webapps/WebappManager;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v3, "org.chromium.chrome.browser.webapp_icon"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 56
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/webapps/WebappManager;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v5, "org.chromium.content_public.common.orientation"

    invoke-virtual {v0, v5, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    .line 59
    if-eqz v1, :cond_0

    if-eqz v2, :cond_0

    .line 60
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/webapps/WebappManager;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v6, "org.chromium.chrome.browser.webapp_mac"

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 61
    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 64
    :goto_0
    if-eqz v0, :cond_2

    invoke-static {p0, v2, v0}, Lorg/chromium/chrome/browser/WebappAuthenticator;->isUrlValid(Landroid/content/Context;Ljava/lang/String;[B)Z

    move-result v0

    if-eqz v0, :cond_2

    move-object v0, p0

    .line 65
    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/chrome/webapps/WebappManager;->launchWebapp(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 79
    :cond_0
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/webapps/WebappManager;->finish()V

    .line 80
    return-void

    .line 61
    :cond_1
    invoke-static {v0, v7}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v0

    goto :goto_0

    .line 67
    :cond_2
    const-string/jumbo v0, "WebappManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Shortcut ("

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v3, ") opened in Chrome."

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 68
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.action.VIEW"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 69
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/webapps/WebappManager;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 70
    const-string/jumbo v1, "REUSE_URL_MATCHING_TAB_ELSE_NEW_TAB"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 72
    :try_start_0
    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/webapps/WebappManager;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    goto :goto_1
.end method
