.class public Lcom/google/android/apps/chrome/variations/VariationsSession;
.super Ljava/lang/Object;
.source "VariationsSession.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private native nativeStartVariationsSession()V
.end method


# virtual methods
.method public start()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/google/android/apps/chrome/variations/VariationsSession;->nativeStartVariationsSession()V

    .line 17
    return-void
.end method
