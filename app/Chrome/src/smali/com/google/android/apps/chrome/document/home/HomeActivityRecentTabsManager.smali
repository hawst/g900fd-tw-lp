.class public Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;
.super Lcom/google/android/apps/chrome/ntp/RecentTabsManager;
.source "HomeActivityRecentTabsManager.java"


# instance fields
.field private final mActivity:Landroid/app/Activity;

.field private final mCurrentlyOpenTabs:Ljava/util/List;

.field private mDialog:Landroid/app/Dialog;

.field private final mFinishActivityOnOpen:Z

.field private mShowingAllInCurrentTabs:Z

.field private final mTabList:Lcom/google/android/apps/chrome/document/DocumentTabList;

.field private final mUpdateOpenTabsObserver:Lcom/google/android/apps/chrome/document/DocumentTabList$InitializationObserver;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/chrome/tab/ChromeTab;Landroid/app/Activity;Z)V
    .locals 2

    .prologue
    .line 58
    invoke-static {}, Lcom/google/android/apps/chrome/document/DocumentManager;->getInstance()Lcom/google/android/apps/chrome/document/DocumentManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/document/DocumentManager;->getCurrentProfile()Lorg/chromium/chrome/browser/profiles/Profile;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;-><init>(Lcom/google/android/apps/chrome/tab/ChromeTab;Lorg/chromium/chrome/browser/profiles/Profile;Landroid/content/Context;)V

    .line 59
    iput-object p2, p0, Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;->mActivity:Landroid/app/Activity;

    .line 60
    iput-boolean p3, p0, Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;->mFinishActivityOnOpen:Z

    .line 61
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;->mCurrentlyOpenTabs:Ljava/util/List;

    .line 62
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/google/android/apps/chrome/document/DocumentActivityDatabase;->getTabList(Z)Lcom/google/android/apps/chrome/document/DocumentTabList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;->mTabList:Lcom/google/android/apps/chrome/document/DocumentTabList;

    .line 63
    new-instance v0, Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager$1;

    iget-object v1, p0, Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;->mTabList:Lcom/google/android/apps/chrome/document/DocumentTabList;

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager$1;-><init>(Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;Lcom/google/android/apps/chrome/document/DocumentTabList;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;->mUpdateOpenTabsObserver:Lcom/google/android/apps/chrome/document/DocumentTabList$InitializationObserver;

    .line 79
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;->updateCurrentlyOpenTabs()V

    .line 80
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;)Lcom/google/android/apps/chrome/document/DocumentTabList;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;->mTabList:Lcom/google/android/apps/chrome/document/DocumentTabList;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;->mActivity:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;->updateCurrentlyOpenTabsWhenDatabaseReady()V

    return-void
.end method

.method static synthetic access$301(Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSession;Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSessionTab;I)V
    .locals 0

    .prologue
    .line 38
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->openForeignSessionTab(Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSession;Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSessionTab;I)V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;)Landroid/app/Dialog;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;->mDialog:Landroid/app/Dialog;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;)Z
    .locals 1

    .prologue
    .line 38
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;->mFinishActivityOnOpen:Z

    return v0
.end method

.method static synthetic access$601(Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;Lorg/chromium/chrome/browser/RecentlyClosedBridge$RecentlyClosedTab;I)V
    .locals 0

    .prologue
    .line 38
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->openRecentlyClosedTab(Lorg/chromium/chrome/browser/RecentlyClosedBridge$RecentlyClosedTab;I)V

    return-void
.end method

.method private updateCurrentlyOpenTabsWhenDatabaseReady()V
    .locals 8

    .prologue
    .line 162
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/IntentHandler;->getTabIdFromIntent(Landroid/content/Intent;)I

    move-result v2

    .line 164
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;->mActivity:Landroid/app/Activity;

    const-string/jumbo v1, "activity"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 166
    invoke-virtual {v0}, Landroid/app/ActivityManager;->getAppTasks()Ljava/util/List;

    move-result-object v3

    .line 167
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 168
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager$AppTask;

    invoke-virtual {v0}, Landroid/app/ActivityManager$AppTask;->getTaskInfo()Landroid/app/ActivityManager$RecentTaskInfo;

    move-result-object v0

    iget-object v0, v0, Landroid/app/ActivityManager$RecentTaskInfo;->baseIntent:Landroid/content/Intent;

    .line 169
    invoke-static {v0}, Lcom/google/android/apps/chrome/IntentHandler;->getTabIdFromIntent(Landroid/content/Intent;)I

    move-result v4

    .line 170
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;->mTabList:Lcom/google/android/apps/chrome/document/DocumentTabList;

    invoke-virtual {v0, v4}, Lcom/google/android/apps/chrome/document/DocumentTabList;->getCurrentUrlForDocument(I)Ljava/lang/String;

    move-result-object v5

    .line 171
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "chrome-native://"

    invoke-virtual {v5, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 172
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager$AppTask;

    invoke-virtual {v0}, Landroid/app/ActivityManager$AppTask;->getTaskInfo()Landroid/app/ActivityManager$RecentTaskInfo;

    move-result-object v0

    iget-object v0, v0, Landroid/app/ActivityManager$RecentTaskInfo;->description:Ljava/lang/CharSequence;

    .line 176
    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 178
    :goto_1
    new-instance v6, Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager$4;

    invoke-direct {v6, p0, v4}, Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager$4;-><init>(Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;I)V

    .line 191
    new-instance v7, Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager$5;

    invoke-direct {v7, p0, v2, v4, v6}, Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager$5;-><init>(Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;IILjava/lang/Runnable;)V

    .line 200
    iget-object v4, p0, Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;->mCurrentlyOpenTabs:Ljava/util/List;

    new-instance v6, Lcom/google/android/apps/chrome/ntp/CurrentlyOpenTab;

    invoke-direct {v6, v5, v0, v7}, Lcom/google/android/apps/chrome/ntp/CurrentlyOpenTab;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)V

    invoke-interface {v4, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 167
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 176
    :cond_1
    const-string/jumbo v0, ""

    goto :goto_1

    .line 202
    :cond_2
    return-void
.end method


# virtual methods
.method public destroy()V
    .locals 0

    .prologue
    .line 92
    invoke-super {p0}, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->destroy()V

    .line 93
    return-void
.end method

.method public getCurrentlyOpenTabs()Ljava/util/List;
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;->mCurrentlyOpenTabs:Ljava/util/List;

    return-object v0
.end method

.method public isCurrentlyOpenTabsShowingAll()Z
    .locals 1

    .prologue
    .line 153
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;->mShowingAllInCurrentTabs:Z

    return v0
.end method

.method public openForeignSessionTab(Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSession;Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSessionTab;I)V
    .locals 4

    .prologue
    .line 100
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;->mDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->hide()V

    .line 101
    :cond_0
    new-instance v0, Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager$2;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager$2;-><init>(Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSession;Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSessionTab;)V

    const-wide/16 v2, 0x96

    invoke-static {v0, v2, v3}, Lorg/chromium/base/ThreadUtils;->postOnUiThreadDelayed(Ljava/lang/Runnable;J)V

    .line 110
    return-void
.end method

.method public openHistoryPage()V
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;->mDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 131
    :cond_0
    invoke-super {p0}, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->openHistoryPage()V

    .line 132
    return-void
.end method

.method public openRecentlyClosedTab(Lorg/chromium/chrome/browser/RecentlyClosedBridge$RecentlyClosedTab;I)V
    .locals 4

    .prologue
    .line 116
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;->mDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->hide()V

    .line 117
    :cond_0
    new-instance v0, Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager$3;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager$3;-><init>(Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;Lorg/chromium/chrome/browser/RecentlyClosedBridge$RecentlyClosedTab;)V

    const-wide/16 v2, 0x96

    invoke-static {v0, v2, v3}, Lorg/chromium/base/ThreadUtils;->postOnUiThreadDelayed(Ljava/lang/Runnable;J)V

    .line 126
    return-void
.end method

.method public openSnapshotDocument(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)V
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;->mDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 137
    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->openSnapshotDocument(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)V

    .line 138
    return-void
.end method

.method public setCurrentlyOpenTabsShowAll(Z)V
    .locals 0

    .prologue
    .line 147
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;->mShowingAllInCurrentTabs:Z

    .line 148
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;->postUpdate()V

    .line 149
    return-void
.end method

.method public setDialog(Landroid/app/Dialog;)V
    .locals 0

    .prologue
    .line 87
    iput-object p1, p0, Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;->mDialog:Landroid/app/Dialog;

    .line 88
    return-void
.end method

.method protected updateCurrentlyOpenTabs()V
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;->mUpdateOpenTabsObserver:Lcom/google/android/apps/chrome/document/DocumentTabList$InitializationObserver;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/document/DocumentTabList$InitializationObserver;->runWhenReady()V

    .line 159
    return-void
.end method
