.class public Lcom/google/android/apps/chrome/ChromeAnimation;
.super Ljava/lang/Object;
.source "ChromeAnimation.java"


# static fields
.field private static sAccelerateInterpolator:Landroid/view/animation/AccelerateInterpolator;

.field private static sDecelerateInterpolator:Landroid/view/animation/DecelerateInterpolator;

.field private static sLinearInterpolator:Landroid/view/animation/LinearInterpolator;


# instance fields
.field private final mAnimations:Ljava/util/ArrayList;

.field private mCurrentTime:J

.field private final mFinishCalled:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/ChromeAnimation;->mFinishCalled:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 39
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/ChromeAnimation;->mAnimations:Ljava/util/ArrayList;

    .line 362
    return-void
.end method

.method private finishInternal()V
    .locals 2

    .prologue
    .line 184
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeAnimation;->mFinishCalled:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 188
    :goto_0
    return-void

    .line 186
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeAnimation;->finish()V

    .line 187
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeAnimation;->mFinishCalled:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    goto :goto_0
.end method

.method public static getAccelerateInterpolator()Landroid/view/animation/AccelerateInterpolator;
    .locals 1

    .prologue
    .line 51
    sget-object v0, Lcom/google/android/apps/chrome/ChromeAnimation;->sAccelerateInterpolator:Landroid/view/animation/AccelerateInterpolator;

    if-nez v0, :cond_0

    .line 52
    new-instance v0, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    sput-object v0, Lcom/google/android/apps/chrome/ChromeAnimation;->sAccelerateInterpolator:Landroid/view/animation/AccelerateInterpolator;

    .line 54
    :cond_0
    sget-object v0, Lcom/google/android/apps/chrome/ChromeAnimation;->sAccelerateInterpolator:Landroid/view/animation/AccelerateInterpolator;

    return-object v0
.end method

.method public static getDecelerateInterpolator()Landroid/view/animation/DecelerateInterpolator;
    .locals 1

    .prologue
    .line 71
    sget-object v0, Lcom/google/android/apps/chrome/ChromeAnimation;->sDecelerateInterpolator:Landroid/view/animation/DecelerateInterpolator;

    if-nez v0, :cond_0

    .line 72
    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    sput-object v0, Lcom/google/android/apps/chrome/ChromeAnimation;->sDecelerateInterpolator:Landroid/view/animation/DecelerateInterpolator;

    .line 74
    :cond_0
    sget-object v0, Lcom/google/android/apps/chrome/ChromeAnimation;->sDecelerateInterpolator:Landroid/view/animation/DecelerateInterpolator;

    return-object v0
.end method

.method public static getLinearInterpolator()Landroid/view/animation/LinearInterpolator;
    .locals 1

    .prologue
    .line 61
    sget-object v0, Lcom/google/android/apps/chrome/ChromeAnimation;->sLinearInterpolator:Landroid/view/animation/LinearInterpolator;

    if-nez v0, :cond_0

    .line 62
    new-instance v0, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v0}, Landroid/view/animation/LinearInterpolator;-><init>()V

    sput-object v0, Lcom/google/android/apps/chrome/ChromeAnimation;->sLinearInterpolator:Landroid/view/animation/LinearInterpolator;

    .line 64
    :cond_0
    sget-object v0, Lcom/google/android/apps/chrome/ChromeAnimation;->sLinearInterpolator:Landroid/view/animation/LinearInterpolator;

    return-object v0
.end method


# virtual methods
.method public add(Lcom/google/android/apps/chrome/ChromeAnimation$Animation;)V
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeAnimation;->mAnimations:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 86
    return-void
.end method

.method public cancel(Ljava/lang/Object;Ljava/lang/Enum;)V
    .locals 3

    .prologue
    .line 109
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeAnimation;->mAnimations:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_2

    .line 110
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeAnimation;->mAnimations:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/ChromeAnimation$Animation;

    .line 111
    if-eqz p1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeAnimation$Animation;->getAnimatedObject()Ljava/lang/Object;

    move-result-object v2

    if-ne v2, p1, :cond_1

    :cond_0
    invoke-virtual {v0, p2}, Lcom/google/android/apps/chrome/ChromeAnimation$Animation;->checkProperty(Ljava/lang/Enum;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 113
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeAnimation;->mAnimations:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 109
    :cond_1
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 116
    :cond_2
    return-void
.end method

.method protected finish()V
    .locals 0

    .prologue
    .line 198
    return-void
.end method

.method public finished()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 170
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeAnimation;->mFinishCalled:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_1

    move v2, v3

    .line 180
    :cond_0
    :goto_0
    return v2

    :cond_1
    move v1, v2

    .line 174
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeAnimation;->mAnimations:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 175
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeAnimation;->mAnimations:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/ChromeAnimation$Animation;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeAnimation$Animation;->finished()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 174
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_2
    move v2, v3

    .line 180
    goto :goto_0
.end method

.method public start()V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 94
    iget-object v1, p0, Lcom/google/android/apps/chrome/ChromeAnimation;->mFinishCalled:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 95
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/google/android/apps/chrome/ChromeAnimation;->mCurrentTime:J

    move v1, v0

    .line 96
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeAnimation;->mAnimations:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 97
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeAnimation;->mAnimations:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/ChromeAnimation$Animation;

    .line 98
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeAnimation$Animation;->start()V

    .line 96
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 100
    :cond_0
    return-void
.end method

.method public update()Z
    .locals 2

    .prologue
    .line 137
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/chrome/ChromeAnimation;->update(J)Z

    move-result v0

    return v0
.end method

.method public update(J)Z
    .locals 9

    .prologue
    const/4 v0, 0x1

    const/4 v2, 0x0

    .line 148
    iget-object v1, p0, Lcom/google/android/apps/chrome/ChromeAnimation;->mFinishCalled:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 163
    :goto_0
    return v0

    .line 151
    :cond_0
    iget-wide v4, p0, Lcom/google/android/apps/chrome/ChromeAnimation;->mCurrentTime:J

    const-wide/16 v6, 0x0

    cmp-long v1, v4, v6

    if-nez v1, :cond_1

    const-wide/16 v4, 0x10

    sub-long v4, p1, v4

    iput-wide v4, p0, Lcom/google/android/apps/chrome/ChromeAnimation;->mCurrentTime:J

    .line 152
    :cond_1
    iget-wide v4, p0, Lcom/google/android/apps/chrome/ChromeAnimation;->mCurrentTime:J

    sub-long v4, p1, v4

    .line 153
    iget-wide v6, p0, Lcom/google/android/apps/chrome/ChromeAnimation;->mCurrentTime:J

    add-long/2addr v6, v4

    iput-wide v6, p0, Lcom/google/android/apps/chrome/ChromeAnimation;->mCurrentTime:J

    move v1, v2

    move v3, v0

    .line 155
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeAnimation;->mAnimations:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 156
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeAnimation;->mAnimations:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/ChromeAnimation$Animation;

    invoke-virtual {v0, v4, v5}, Lcom/google/android/apps/chrome/ChromeAnimation$Animation;->update(J)V

    .line 157
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeAnimation;->mAnimations:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/ChromeAnimation$Animation;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeAnimation$Animation;->finished()Z

    move-result v0

    and-int/2addr v3, v0

    .line 155
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 160
    :cond_2
    if-eqz v3, :cond_3

    .line 161
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeAnimation;->updateAndFinish()V

    :cond_3
    move v0, v2

    .line 163
    goto :goto_0
.end method

.method public updateAndFinish()V
    .locals 2

    .prologue
    .line 123
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeAnimation;->mAnimations:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 124
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeAnimation;->mAnimations:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/ChromeAnimation$Animation;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeAnimation$Animation;->updateAndFinish()V

    .line 123
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 126
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/ChromeAnimation;->finishInternal()V

    .line 127
    return-void
.end method
