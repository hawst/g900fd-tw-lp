.class Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListViewAdapter;
.super Landroid/widget/BaseAdapter;
.source "EnhancedBookmarkDrawerListViewAdapter.java"


# static fields
.field static final synthetic $assertionsDisabled:Z

.field static final TYPE_ALL_ITEMS:I = -0x1

.field static final TYPE_DIVIDER:I = -0x3

.field static final TYPE_FOLDER:I = 0x0

.field static final TYPE_UNCATEGORIZED:I = -0x2

.field static final VIEW_TYPE_DIVIDER:I = 0x1

.field static final VIEW_TYPE_ITEM:I


# instance fields
.field private mAllSection:Ljava/util/List;

.field private mDelegate:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;

.field private mFilterSection:Ljava/util/List;

.field private mFolderListSection:Ljava/util/List;

.field private mHasUncategorizedItem:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const-class v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListViewAdapter;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListViewAdapter;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>()V
    .locals 1

    .prologue
    .line 69
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 31
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListViewAdapter;->mHasUncategorizedItem:Z

    .line 33
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListViewAdapter;->mAllSection:Ljava/util/List;

    .line 34
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListViewAdapter;->mFolderListSection:Ljava/util/List;

    .line 35
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListViewAdapter;->mFilterSection:Ljava/util/List;

    .line 71
    invoke-direct {p0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListViewAdapter;->populateAllSection()V

    .line 72
    return-void
.end method

.method private populateAllSection()V
    .locals 3

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListViewAdapter;->mAllSection:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 76
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListViewAdapter;->mAllSection:Ljava/util/List;

    new-instance v1, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListViewAdapter$Item;

    const/4 v2, -0x1

    invoke-direct {v1, v2}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListViewAdapter$Item;-><init>(I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 77
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListViewAdapter;->mHasUncategorizedItem:Z

    if-eqz v0, :cond_0

    .line 78
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListViewAdapter;->mAllSection:Ljava/util/List;

    new-instance v1, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListViewAdapter$Item;

    const/4 v2, -0x2

    invoke-direct {v1, v2}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListViewAdapter$Item;-><init>(I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 80
    :cond_0
    return-void
.end method


# virtual methods
.method public areAllItemsEnabled()Z
    .locals 1

    .prologue
    .line 160
    const/4 v0, 0x0

    return v0
.end method

.method public getCount()I
    .locals 2

    .prologue
    .line 182
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListViewAdapter;->mAllSection:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListViewAdapter;->mFolderListSection:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListViewAdapter;->mFilterSection:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 193
    if-gez p1, :cond_1

    .line 208
    :cond_0
    :goto_0
    return-object v0

    .line 197
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListViewAdapter;->mAllSection:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge p1, v1, :cond_2

    .line 198
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListViewAdapter;->mAllSection:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 200
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListViewAdapter;->mAllSection:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    sub-int v1, p1, v1

    .line 201
    iget-object v2, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListViewAdapter;->mFolderListSection:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_3

    .line 202
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListViewAdapter;->mFolderListSection:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 204
    :cond_3
    iget-object v2, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListViewAdapter;->mFolderListSection:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    sub-int/2addr v1, v2

    .line 205
    iget-object v2, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListViewAdapter;->mFilterSection:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 206
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListViewAdapter;->mFilterSection:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 188
    int-to-long v0, p1

    return-wide v0
.end method

.method getItemPosition(ILjava/lang/Object;)I
    .locals 4

    .prologue
    const/4 v3, 0x3

    const/4 v2, 0x2

    const/4 v0, 0x1

    .line 129
    sget-boolean v1, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListViewAdapter;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    if-eq p1, v0, :cond_0

    if-eq p1, v2, :cond_0

    if-eq p1, v3, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 132
    :cond_0
    if-ne p1, v0, :cond_2

    .line 133
    const/4 v0, 0x0

    .line 153
    :cond_1
    :goto_0
    return v0

    .line 134
    :cond_2
    iget-boolean v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListViewAdapter;->mHasUncategorizedItem:Z

    if-eqz v1, :cond_3

    if-eq p1, v2, :cond_1

    .line 136
    :cond_3
    if-ne p1, v3, :cond_5

    .line 137
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListViewAdapter;->mDelegate:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;->getModel()Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;->getTopLevelFolderParentIDs()Ljava/util/List;

    move-result-object v1

    .line 141
    check-cast p2, Lorg/chromium/components/bookmarks/BookmarkId;

    .line 143
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListViewAdapter;->mDelegate:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;->getModel()Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;->getBookmarkById(Lorg/chromium/components/bookmarks/BookmarkId;)Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;->getParentId()Lorg/chromium/components/bookmarks/BookmarkId;

    move-result-object v0

    .line 145
    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move-object p2, v0

    .line 149
    goto :goto_1

    .line 150
    :cond_4
    invoke-virtual {p0, p2}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListViewAdapter;->positionOfBookmarkId(Lorg/chromium/components/bookmarks/BookmarkId;)I

    move-result v0

    goto :goto_0

    .line 153
    :cond_5
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public getItemViewType(I)I
    .locals 2

    .prologue
    .line 171
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListViewAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListViewAdapter$Item;

    .line 172
    iget v0, v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListViewAdapter$Item;->mType:I

    const/4 v1, -0x3

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 213
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListViewAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListViewAdapter$Item;

    .line 216
    if-nez p2, :cond_5

    .line 217
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListViewAdapter;->getItemViewType(I)I

    move-result v1

    .line 218
    if-nez v1, :cond_0

    .line 219
    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    sget v2, Lcom/google/android/apps/chrome/R$layout;->eb_drawer_item:I

    invoke-virtual {v1, v2, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    move-object v2, p2

    .line 229
    :goto_0
    iget v1, v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListViewAdapter$Item;->mType:I

    const/4 v6, -0x3

    if-ne v1, v6, :cond_2

    .line 263
    :goto_1
    return-object v2

    .line 221
    :cond_0
    if-ne v1, v4, :cond_1

    .line 222
    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    sget v2, Lcom/google/android/apps/chrome/R$layout;->eb_drawer_item_divider:I

    invoke-virtual {v1, v2, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    move-object v2, p2

    goto :goto_0

    .line 225
    :cond_1
    sget-boolean v1, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListViewAdapter;->$assertionsDisabled:Z

    if-nez v1, :cond_5

    new-instance v0, Ljava/lang/AssertionError;

    const-string/jumbo v1, "Invalid item view type."

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    :cond_2
    move-object v1, v2

    .line 231
    check-cast v1, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListItemView;

    .line 233
    const/4 v6, 0x0

    .line 237
    iget v7, v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListViewAdapter$Item;->mType:I

    packed-switch v7, :pswitch_data_0

    .line 251
    const-string/jumbo v0, ""

    .line 253
    sget-boolean v4, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListViewAdapter;->$assertionsDisabled:Z

    if-nez v4, :cond_4

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 239
    :pswitch_0
    sget v0, Lcom/google/android/apps/chrome/R$string;->enhanced_bookmark_drawer_all_items:I

    move v3, v4

    move-object v4, v6

    .line 256
    :goto_2
    if-nez v4, :cond_3

    .line 257
    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListItemView;->setText(I)V

    .line 261
    :goto_3
    invoke-virtual {v1, v3}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListItemView;->setIcon(I)V

    goto :goto_1

    .line 243
    :pswitch_1
    sget v0, Lcom/google/android/apps/chrome/R$string;->enhanced_bookmark_drawer_uncategorized:I

    .line 244
    const/4 v3, 0x2

    move-object v4, v6

    .line 245
    goto :goto_2

    .line 247
    :pswitch_2
    iget-object v4, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListViewAdapter;->mDelegate:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;

    invoke-interface {v4}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;->getModel()Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;

    move-result-object v4

    iget-object v0, v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListViewAdapter$Item;->mFolderId:Lorg/chromium/components/bookmarks/BookmarkId;

    invoke-virtual {v4, v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;->getBookmarkById(Lorg/chromium/components/bookmarks/BookmarkId;)Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;->getTitle()Ljava/lang/String;

    move-result-object v0

    move-object v4, v0

    move v0, v3

    move v3, v5

    .line 249
    goto :goto_2

    .line 259
    :cond_3
    invoke-virtual {v1, v4}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListItemView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3

    :cond_4
    move-object v4, v0

    move v0, v3

    move v3, v5

    goto :goto_2

    :cond_5
    move-object v2, p2

    goto :goto_0

    .line 237
    nop

    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 177
    const/4 v0, 0x2

    return v0
.end method

.method public isEnabled(I)Z
    .locals 2

    .prologue
    .line 165
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListViewAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListViewAdapter$Item;

    .line 166
    iget v0, v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListViewAdapter$Item;->mType:I

    const/4 v1, -0x3

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method positionOfBookmarkId(Lorg/chromium/components/bookmarks/BookmarkId;)I
    .locals 2

    .prologue
    .line 117
    new-instance v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListViewAdapter$Item;

    invoke-direct {v0, p1}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListViewAdapter$Item;-><init>(Lorg/chromium/components/bookmarks/BookmarkId;)V

    .line 118
    iget-object v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListViewAdapter;->mFolderListSection:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 119
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 120
    iget-object v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListViewAdapter;->mAllSection:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/2addr v0, v1

    .line 122
    :cond_0
    return v0
.end method

.method setEnhancedBookmarkUIDelegate(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;)V
    .locals 0

    .prologue
    .line 108
    iput-object p1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListViewAdapter;->mDelegate:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;

    .line 109
    return-void
.end method

.method setTopFolders(Ljava/util/List;)V
    .locals 4

    .prologue
    .line 86
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListViewAdapter;->mFolderListSection:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 88
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 90
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListViewAdapter;->mFolderListSection:Ljava/util/List;

    new-instance v1, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListViewAdapter$Item;

    const/4 v2, -0x3

    invoke-direct {v1, v2}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListViewAdapter$Item;-><init>(I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 94
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/components/bookmarks/BookmarkId;

    .line 95
    iget-object v2, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListViewAdapter;->mFolderListSection:Ljava/util/List;

    new-instance v3, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListViewAdapter$Item;

    invoke-direct {v3, v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListViewAdapter$Item;-><init>(Lorg/chromium/components/bookmarks/BookmarkId;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 97
    :cond_1
    return-void
.end method

.method showUncategorizedItem(Z)V
    .locals 0

    .prologue
    .line 103
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListViewAdapter;->mHasUncategorizedItem:Z

    .line 104
    invoke-direct {p0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListViewAdapter;->populateAllSection()V

    .line 105
    return-void
.end method
