.class Lcom/google/android/apps/chrome/ntp/NewTabPageView$7;
.super Ljava/lang/Object;
.source "NewTabPageView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/ntp/NewTabPageView;

.field final synthetic val$prefManager:Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/ntp/NewTabPageView;Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;)V
    .locals 0

    .prologue
    .line 325
    iput-object p1, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView$7;->this$0:Lcom/google/android/apps/chrome/ntp/NewTabPageView;

    iput-object p2, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView$7;->val$prefManager:Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 328
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView$7;->this$0:Lcom/google/android/apps/chrome/ntp/NewTabPageView;

    # getter for: Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mOptOutView:Landroid/view/View;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->access$200(Lcom/google/android/apps/chrome/ntp/NewTabPageView;)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 329
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->recordDocumentOptOutPromoClick(I)V

    .line 331
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView$7;->val$prefManager:Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->setOptedOutState(I)V

    .line 333
    return-void
.end method
