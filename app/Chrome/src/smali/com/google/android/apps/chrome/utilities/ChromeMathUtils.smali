.class public Lcom/google/android/apps/chrome/utilities/ChromeMathUtils;
.super Ljava/lang/Object;
.source "ChromeMathUtils.java"


# static fields
.field public static SPEED_FAST:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    const v0, 0x3da3d70a    # 0.08f

    sput v0, Lcom/google/android/apps/chrome/utilities/ChromeMathUtils;->SPEED_FAST:F

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static flipSignIf(FZ)F
    .locals 0

    .prologue
    .line 58
    if-eqz p1, :cond_0

    neg-float p0, p0

    :cond_0
    return p0
.end method

.method public static flipSignIf(IZ)I
    .locals 0

    .prologue
    .line 54
    if-eqz p1, :cond_0

    neg-int p0, p0

    :cond_0
    return p0
.end method

.method public static interpolate(FFF)F
    .locals 1

    .prologue
    .line 17
    sub-float v0, p1, p0

    mul-float/2addr v0, p2

    add-float/2addr v0, p0

    return v0
.end method

.method public static scaleToFitTargetSize([III)F
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 40
    array-length v0, p0

    const/4 v1, 0x2

    if-lt v0, v1, :cond_0

    aget v0, p0, v3

    if-lez v0, :cond_0

    aget v0, p0, v4

    if-gtz v0, :cond_1

    .line 41
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Expected dimensions to have length >= 2 && dimensions[0] > 0 && dimensions[1] > 0"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 45
    :cond_1
    int-to-float v0, p1

    aget v1, p0, v3

    int-to-float v1, v1

    div-float/2addr v0, v1

    int-to-float v1, p2

    aget v2, p0, v4

    int-to-float v2, v2

    div-float/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 48
    aget v1, p0, v3

    int-to-float v1, v1

    mul-float/2addr v1, v0

    float-to-int v1, v1

    aput v1, p0, v3

    .line 49
    aget v1, p0, v4

    int-to-float v1, v1

    mul-float/2addr v1, v0

    float-to-int v1, v1

    aput v1, p0, v4

    .line 50
    return v0
.end method
