.class Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView$SpinnerOnItemSelectedListener;
.super Ljava/lang/Object;
.source "AccountFirstRunView.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;)V
    .locals 0

    .prologue
    .line 83
    iput-object p1, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView$SpinnerOnItemSelectedListener;->this$0:Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView$1;)V
    .locals 0

    .prologue
    .line 83
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView$SpinnerOnItemSelectedListener;-><init>(Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;)V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 86
    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 87
    iget-object v2, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView$SpinnerOnItemSelectedListener;->this$0:Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;

    # getter for: Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mAddAnotherAccount:Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->access$000(Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 89
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView$SpinnerOnItemSelectedListener;->this$0:Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;

    # getter for: Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mArrayAdapter:Landroid/widget/ArrayAdapter;
    invoke-static {v0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->access$200(Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;)Landroid/widget/ArrayAdapter;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView$SpinnerOnItemSelectedListener;->this$0:Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;

    # getter for: Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mAccountName:Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->access$100(Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/ArrayAdapter;->getPosition(Ljava/lang/Object;)I

    move-result v0

    .line 90
    const/4 v2, -0x1

    if-ne v0, v2, :cond_0

    move v0, v1

    .line 91
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView$SpinnerOnItemSelectedListener;->this$0:Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;

    # getter for: Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mSpinner:Landroid/widget/Spinner;
    invoke-static {v2}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->access$300(Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;)Landroid/widget/Spinner;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Landroid/widget/Spinner;->setSelection(IZ)V

    .line 93
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView$SpinnerOnItemSelectedListener;->this$0:Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;

    # getter for: Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mListener:Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView$Listener;
    invoke-static {v0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->access$400(Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;)Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView$Listener;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView$Listener;->onNewAccount()V

    .line 99
    :goto_0
    return-void

    .line 95
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView$SpinnerOnItemSelectedListener;->this$0:Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;

    # setter for: Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mAccountName:Ljava/lang/String;
    invoke-static {v2, v0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->access$102(Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;Ljava/lang/String;)Ljava/lang/String;

    .line 96
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView$SpinnerOnItemSelectedListener;->this$0:Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;

    # getter for: Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mPositionSetProgrammatically:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->access$500(Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView$SpinnerOnItemSelectedListener;->this$0:Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;

    # getter for: Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mImageCarousel:Lcom/google/android/apps/chrome/firstrun/ImageCarousel;
    invoke-static {v0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->access$600(Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;)Lcom/google/android/apps/chrome/firstrun/ImageCarousel;

    move-result-object v0

    int-to-float v2, p3

    invoke-virtual {v0, v2, v1, v1}, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->scrollTo(FZZ)V

    .line 97
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView$SpinnerOnItemSelectedListener;->this$0:Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;

    # setter for: Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mPositionSetProgrammatically:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->access$502(Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;Z)Z

    goto :goto_0
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 2

    .prologue
    .line 102
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView$SpinnerOnItemSelectedListener;->this$0:Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;

    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mAccountName:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->access$102(Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;Ljava/lang/String;)Ljava/lang/String;

    .line 103
    return-void
.end method
