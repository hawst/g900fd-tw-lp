.class Lcom/google/android/apps/chrome/document/DocumentMigrationHelper$MigrationTabList;
.super Lcom/google/android/apps/chrome/document/DocumentTabList;
.source "DocumentMigrationHelper.java"


# instance fields
.field private mTitleList:Landroid/util/SparseArray;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/document/DocumentTabList$Delegate;)V
    .locals 2

    .prologue
    .line 78
    const/4 v0, 0x0

    const/4 v1, -0x1

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/apps/chrome/document/DocumentTabList;-><init>(Lcom/google/android/apps/chrome/document/DocumentTabList$Delegate;ZI)V

    .line 79
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentMigrationHelper$MigrationTabList;->mTitleList:Landroid/util/SparseArray;

    .line 80
    return-void
.end method


# virtual methods
.method public getTitleForDocument(I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentMigrationHelper$MigrationTabList;->mTitleList:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 89
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string/jumbo v0, ""

    :cond_0
    return-object v0
.end method

.method protected shouldStartDeserialization()Z
    .locals 2

    .prologue
    .line 94
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentMigrationHelper$MigrationTabList;->getCurrentState()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected updateEntryInfoFromTabState(Lcom/google/android/apps/chrome/document/DocumentTabList$Entry;Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;)V
    .locals 3

    .prologue
    .line 99
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/chrome/document/DocumentTabList;->updateEntryInfoFromTabState(Lcom/google/android/apps/chrome/document/DocumentTabList$Entry;Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;)V

    .line 100
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentMigrationHelper$MigrationTabList;->mTitleList:Landroid/util/SparseArray;

    iget v1, p1, Lcom/google/android/apps/chrome/document/DocumentTabList$Entry;->tabId:I

    iget-object v2, p2, Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;->contentsState:Lcom/google/android/apps/chrome/tab/ChromeTab$ContentsState;

    invoke-static {v2}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getDisplayTitleFromState(Lcom/google/android/apps/chrome/tab/ChromeTab$ContentsState;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 101
    return-void
.end method
