.class public Lcom/google/android/apps/chrome/document/ssb/Constants;
.super Ljava/lang/Object;
.source "Constants.java"


# static fields
.field public static final KEY_SSB_CONTEXT:Ljava/lang/String; = "ssb_service:ssb_context"

.field public static final KEY_SSB_PACKAGE_NAME:Ljava/lang/String; = "ssb_service:ssb_package_name"

.field public static final KEY_SSB_STATE:Ljava/lang/String; = "ssb_service:ssb_state"

.field public static final REQUEST_PREPARE_OVERLAY:I = 0x1

.field public static final REQUEST_REGISTER_CLIENT:I = 0x2

.field public static final RESPONSE_UPDATE_SSB:I = 0x3


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
