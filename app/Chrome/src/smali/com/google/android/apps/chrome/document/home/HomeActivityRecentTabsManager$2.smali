.class Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager$2;
.super Ljava/lang/Object;
.source "HomeActivityRecentTabsManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;

.field final synthetic val$session:Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSession;

.field final synthetic val$tab:Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSessionTab;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSession;Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSessionTab;)V
    .locals 0

    .prologue
    .line 101
    iput-object p1, p0, Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager$2;->this$0:Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;

    iput-object p2, p0, Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager$2;->val$session:Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSession;

    iput-object p3, p0, Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager$2;->val$tab:Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSessionTab;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 104
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager$2;->this$0:Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;

    iget-object v1, p0, Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager$2;->val$session:Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSession;

    iget-object v2, p0, Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager$2;->val$tab:Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSessionTab;

    const/4 v3, 0x4

    # invokes: Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->openForeignSessionTab(Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSession;Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSessionTab;I)V
    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;->access$301(Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSession;Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSessionTab;I)V

    .line 106
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager$2;->this$0:Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;

    # getter for: Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;->mDialog:Landroid/app/Dialog;
    invoke-static {v0}, Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;->access$400(Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;)Landroid/app/Dialog;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager$2;->this$0:Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;

    # getter for: Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;->mDialog:Landroid/app/Dialog;
    invoke-static {v0}, Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;->access$400(Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;)Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 107
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager$2;->this$0:Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;

    # getter for: Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;->mFinishActivityOnOpen:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;->access$500(Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager$2;->this$0:Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;

    # getter for: Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;->access$100(Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finishAndRemoveTask()V

    .line 108
    :cond_1
    return-void
.end method
