.class public Lcom/google/android/apps/chrome/webapps/WebappInfo;
.super Ljava/lang/Object;
.source "WebappInfo.java"


# instance fields
.field private mIcon:Landroid/graphics/Bitmap;

.field private mId:Ljava/lang/String;

.field private mIsInitialized:Z

.field private mOrientation:I

.field private mTitle:Ljava/lang/String;

.field private mUri:Landroid/net/Uri;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 83
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Landroid/net/Uri;Landroid/graphics/Bitmap;Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    iput-object p3, p0, Lcom/google/android/apps/chrome/webapps/WebappInfo;->mIcon:Landroid/graphics/Bitmap;

    .line 75
    iput-object p1, p0, Lcom/google/android/apps/chrome/webapps/WebappInfo;->mId:Ljava/lang/String;

    .line 76
    iput-object p4, p0, Lcom/google/android/apps/chrome/webapps/WebappInfo;->mTitle:Ljava/lang/String;

    .line 77
    iput-object p2, p0, Lcom/google/android/apps/chrome/webapps/WebappInfo;->mUri:Landroid/net/Uri;

    .line 78
    iput p5, p0, Lcom/google/android/apps/chrome/webapps/WebappInfo;->mOrientation:I

    .line 79
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/WebappInfo;->mUri:Landroid/net/Uri;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/chrome/webapps/WebappInfo;->mIsInitialized:Z

    .line 80
    return-void

    .line 79
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static create(Landroid/content/Intent;)Lcom/google/android/apps/chrome/webapps/WebappInfo;
    .locals 6

    .prologue
    .line 39
    const-string/jumbo v0, "org.chromium.chrome.browser.webapp_id"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 40
    const-string/jumbo v1, "org.chromium.chrome.browser.webapp_icon"

    invoke-virtual {p0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 41
    const-string/jumbo v2, "org.chromium.chrome.browser.webapp_title"

    invoke-virtual {p0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 42
    const-string/jumbo v3, "org.chromium.chrome.browser.webapp_url"

    invoke-virtual {p0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 43
    const-string/jumbo v4, "org.chromium.content_public.common.orientation"

    const/4 v5, 0x0

    invoke-virtual {p0, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    .line 45
    invoke-static {v0, v3, v1, v2, v4}, Lcom/google/android/apps/chrome/webapps/WebappInfo;->create(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Lcom/google/android/apps/chrome/webapps/WebappInfo;

    move-result-object v0

    return-object v0
.end method

.method public static create(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Lcom/google/android/apps/chrome/webapps/WebappInfo;
    .locals 6

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 58
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 59
    :cond_0
    const-string/jumbo v0, "WebappInfo"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Data passed in was incomplete: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v3

    .line 70
    :goto_0
    return-object v0

    .line 64
    :cond_1
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 65
    invoke-static {p2, v2}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v0

    .line 66
    array-length v1, v0

    invoke-static {v0, v2, v1}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 69
    :cond_2
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 70
    new-instance v0, Lcom/google/android/apps/chrome/webapps/WebappInfo;

    move-object v1, p0

    move-object v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/chrome/webapps/WebappInfo;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/graphics/Bitmap;Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public static createEmpty()Lcom/google/android/apps/chrome/webapps/WebappInfo;
    .locals 1

    .prologue
    .line 31
    new-instance v0, Lcom/google/android/apps/chrome/webapps/WebappInfo;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/webapps/WebappInfo;-><init>()V

    return-object v0
.end method


# virtual methods
.method copy(Lcom/google/android/apps/chrome/webapps/WebappInfo;)V
    .locals 1

    .prologue
    .line 104
    iget-boolean v0, p1, Lcom/google/android/apps/chrome/webapps/WebappInfo;->mIsInitialized:Z

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/webapps/WebappInfo;->mIsInitialized:Z

    .line 105
    iget-object v0, p1, Lcom/google/android/apps/chrome/webapps/WebappInfo;->mIcon:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/google/android/apps/chrome/webapps/WebappInfo;->mIcon:Landroid/graphics/Bitmap;

    .line 106
    iget-object v0, p1, Lcom/google/android/apps/chrome/webapps/WebappInfo;->mId:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/chrome/webapps/WebappInfo;->mId:Ljava/lang/String;

    .line 107
    iget-object v0, p1, Lcom/google/android/apps/chrome/webapps/WebappInfo;->mUri:Landroid/net/Uri;

    iput-object v0, p0, Lcom/google/android/apps/chrome/webapps/WebappInfo;->mUri:Landroid/net/Uri;

    .line 108
    iget-object v0, p1, Lcom/google/android/apps/chrome/webapps/WebappInfo;->mTitle:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/chrome/webapps/WebappInfo;->mTitle:Ljava/lang/String;

    .line 109
    iget v0, p1, Lcom/google/android/apps/chrome/webapps/WebappInfo;->mOrientation:I

    iput v0, p0, Lcom/google/android/apps/chrome/webapps/WebappInfo;->mOrientation:I

    .line 110
    return-void
.end method

.method public icon()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/WebappInfo;->mIcon:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public id()Ljava/lang/String;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/WebappInfo;->mId:Ljava/lang/String;

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 112
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/webapps/WebappInfo;->mIsInitialized:Z

    return v0
.end method

.method public orientation()I
    .locals 1

    .prologue
    .line 117
    iget v0, p0, Lcom/google/android/apps/chrome/webapps/WebappInfo;->mOrientation:I

    return v0
.end method

.method public title()Ljava/lang/String;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/WebappInfo;->mTitle:Ljava/lang/String;

    return-object v0
.end method

.method public uri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/WebappInfo;->mUri:Landroid/net/Uri;

    return-object v0
.end method

.method writeToBundle(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 90
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/webapps/WebappInfo;->mIsInitialized:Z

    if-nez v0, :cond_0

    .line 97
    :goto_0
    return-void

    .line 92
    :cond_0
    const-string/jumbo v0, "org.chromium.chrome.browser.webapp_id"

    iget-object v1, p0, Lcom/google/android/apps/chrome/webapps/WebappInfo;->mId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    const-string/jumbo v0, "org.chromium.chrome.browser.webapp_url"

    iget-object v1, p0, Lcom/google/android/apps/chrome/webapps/WebappInfo;->mUri:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    const-string/jumbo v0, "org.chromium.chrome.browser.webapp_icon"

    iget-object v1, p0, Lcom/google/android/apps/chrome/webapps/WebappInfo;->mIcon:Landroid/graphics/Bitmap;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 95
    const-string/jumbo v0, "org.chromium.chrome.browser.webapp_title"

    iget-object v1, p0, Lcom/google/android/apps/chrome/webapps/WebappInfo;->mTitle:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    const-string/jumbo v0, "org.chromium.content_public.common.orientation"

    iget v1, p0, Lcom/google/android/apps/chrome/webapps/WebappInfo;->mOrientation:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_0
.end method
