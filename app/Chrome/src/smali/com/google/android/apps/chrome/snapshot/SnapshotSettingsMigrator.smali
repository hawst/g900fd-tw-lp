.class public Lcom/google/android/apps/chrome/snapshot/SnapshotSettingsMigrator;
.super Ljava/lang/Object;
.source "SnapshotSettingsMigrator.java"


# static fields
.field static final INITIAL_PREFERENCE_PACKAGE:Ljava/lang/String; = "com.android.chrome.snapshot"

.field static final OLD_AUTH_TOKEN_KEY:Ljava/lang/String; = "cps_authToken"

.field static final OLD_C2DM_REGISTRATION_ID_KEY:Ljava/lang/String; = "c2dm_registrationId"

.field static final OLD_ENABLED_KEY:Ljava/lang/String; = "enabled"

.field static final OLD_PRINTER_ID_KEY:Ljava/lang/String; = "cps_printerId"

.field static final RENAMED_PREFERENCE_PACKAGE:Ljava/lang/String; = "com.google.android.apps.chrome.snapshot"


# instance fields
.field private final mContext:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    iput-object p1, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotSettingsMigrator;->mContext:Landroid/content/Context;

    .line 57
    return-void
.end method

.method private static deleteOldPreferencesFile(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 130
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ".xml"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 132
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v1

    if-nez v1, :cond_0

    .line 133
    const-string/jumbo v1, "SnapshotSettingsMigrator"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Failed to delete old preferences file: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 136
    :cond_0
    return-void
.end method

.method private deleteOldPreferencesFiles()V
    .locals 2

    .prologue
    .line 121
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettingsMigrator;->getDataDir()Ljava/lang/String;

    move-result-object v0

    .line 122
    const-string/jumbo v1, "com.android.chrome.snapshot"

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettingsMigrator;->deleteOldPreferencesFile(Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    const-string/jumbo v1, "com.google.android.apps.chrome.snapshot"

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettingsMigrator;->deleteOldPreferencesFile(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 127
    :goto_0
    return-void

    .line 124
    :catch_0
    move-exception v0

    .line 125
    const-string/jumbo v0, "SnapshotSettingsMigrator"

    const-string/jumbo v1, "Failed to get package information during migration."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static needsMigration(Landroid/content/Context;)Z
    .locals 4

    .prologue
    .line 60
    invoke-static {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->getVersion(Landroid/content/Context;)J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method getDataDir()Ljava/lang/String;
    .locals 3

    .prologue
    .line 114
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotSettingsMigrator;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 115
    iget-object v1, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotSettingsMigrator;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 116
    iget-object v0, v0, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->dataDir:Ljava/lang/String;

    return-object v0
.end method

.method public migrate()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 67
    const-string/jumbo v0, "SnapshotSettingsMigrator"

    const-string/jumbo v1, "Running one-time migration of settings for Chrome to Mobile"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 69
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotSettingsMigrator;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "com.android.chrome.snapshot"

    invoke-virtual {v0, v1, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 71
    iget-object v1, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotSettingsMigrator;->mContext:Landroid/content/Context;

    const-string/jumbo v2, "com.google.android.apps.chrome.snapshot"

    invoke-virtual {v1, v2, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 73
    iget-object v2, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotSettingsMigrator;->mContext:Landroid/content/Context;

    invoke-static {v2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 75
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 80
    const-string/jumbo v3, "enabled"

    invoke-interface {v1, v3}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 81
    const-string/jumbo v3, "chrome_to_mobile_enabled"

    const-string/jumbo v4, "enabled"

    invoke-interface {v1, v4, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 87
    :goto_0
    const-string/jumbo v3, "cps_printerId"

    invoke-interface {v1, v3, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 88
    if-eqz v3, :cond_1

    .line 89
    const-string/jumbo v4, "chrome_to_mobile_printerId"

    invoke-interface {v2, v4, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 94
    :goto_1
    const-string/jumbo v3, "cps_authToken"

    invoke-interface {v1, v3, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 95
    if-eqz v1, :cond_2

    .line 96
    const-string/jumbo v0, "chrome_to_mobile_authToken"

    invoke-interface {v2, v0, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 103
    :goto_2
    const-string/jumbo v0, "chrome_to_mobile_version"

    const-wide/16 v4, 0x3

    invoke-interface {v2, v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 106
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 109
    invoke-direct {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettingsMigrator;->deleteOldPreferencesFiles()V

    .line 110
    return-void

    .line 84
    :cond_0
    const-string/jumbo v3, "chrome_to_mobile_enabled"

    const-string/jumbo v4, "enabled"

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    goto :goto_0

    .line 91
    :cond_1
    const-string/jumbo v3, "chrome_to_mobile_printerId"

    const-string/jumbo v4, "cps_printerId"

    invoke-interface {v0, v4, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    goto :goto_1

    .line 98
    :cond_2
    const-string/jumbo v1, "chrome_to_mobile_authToken"

    const-string/jumbo v3, "cps_authToken"

    invoke-interface {v0, v3, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v1, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    goto :goto_2
.end method
