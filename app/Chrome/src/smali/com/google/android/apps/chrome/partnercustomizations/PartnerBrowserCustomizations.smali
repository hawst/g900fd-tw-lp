.class public Lcom/google/android/apps/chrome/partnercustomizations/PartnerBrowserCustomizations;
.super Ljava/lang/Object;
.source "PartnerBrowserCustomizations.java"


# static fields
.field static final PARTNER_DISABLE_BOOKMARKS_EDITING_PATH:Ljava/lang/String; = "disablebookmarksediting"

.field static final PARTNER_DISABLE_INCOGNITO_MODE_PATH:Ljava/lang/String; = "disableincognitomode"

.field static final PARTNER_HOMEPAGE_PATH:Ljava/lang/String; = "homepage"

.field private static volatile sBookmarksEditingDisabled:Z

.field private static volatile sHomepage:Ljava/lang/String;

.field private static sIgnoreBrowserProviderSystemPackageCheck:Z

.field private static volatile sIncognitoModeDisabled:Z

.field private static sInitializeAsyncCallbacks:Ljava/util/List;

.field private static sIsInitialized:Z

.field private static sProviderAuthority:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    const-string/jumbo v0, "com.android.partnerbrowsercustomizations"

    sput-object v0, Lcom/google/android/apps/chrome/partnercustomizations/PartnerBrowserCustomizations;->sProviderAuthority:Ljava/lang/String;

    .line 41
    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/apps/chrome/partnercustomizations/PartnerBrowserCustomizations;->sIgnoreBrowserProviderSystemPackageCheck:Z

    .line 46
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/google/android/apps/chrome/partnercustomizations/PartnerBrowserCustomizations;->sInitializeAsyncCallbacks:Ljava/util/List;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$002(Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 31
    sput-object p0, Lcom/google/android/apps/chrome/partnercustomizations/PartnerBrowserCustomizations;->sHomepage:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$100()Z
    .locals 1

    .prologue
    .line 31
    sget-boolean v0, Lcom/google/android/apps/chrome/partnercustomizations/PartnerBrowserCustomizations;->sIncognitoModeDisabled:Z

    return v0
.end method

.method static synthetic access$102(Z)Z
    .locals 0

    .prologue
    .line 31
    sput-boolean p0, Lcom/google/android/apps/chrome/partnercustomizations/PartnerBrowserCustomizations;->sIncognitoModeDisabled:Z

    return p0
.end method

.method static synthetic access$200()Z
    .locals 1

    .prologue
    .line 31
    sget-boolean v0, Lcom/google/android/apps/chrome/partnercustomizations/PartnerBrowserCustomizations;->sBookmarksEditingDisabled:Z

    return v0
.end method

.method static synthetic access$202(Z)Z
    .locals 0

    .prologue
    .line 31
    sput-boolean p0, Lcom/google/android/apps/chrome/partnercustomizations/PartnerBrowserCustomizations;->sBookmarksEditingDisabled:Z

    return p0
.end method

.method static synthetic access$300()Ljava/lang/String;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/google/android/apps/chrome/partnercustomizations/PartnerBrowserCustomizations;->sProviderAuthority:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400()Z
    .locals 1

    .prologue
    .line 31
    sget-boolean v0, Lcom/google/android/apps/chrome/partnercustomizations/PartnerBrowserCustomizations;->sIgnoreBrowserProviderSystemPackageCheck:Z

    return v0
.end method

.method static synthetic access$502(Z)Z
    .locals 0

    .prologue
    .line 31
    sput-boolean p0, Lcom/google/android/apps/chrome/partnercustomizations/PartnerBrowserCustomizations;->sIsInitialized:Z

    return p0
.end method

.method static synthetic access$600()Ljava/util/List;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/google/android/apps/chrome/partnercustomizations/PartnerBrowserCustomizations;->sInitializeAsyncCallbacks:Ljava/util/List;

    return-object v0
.end method

.method static buildQueryUri(Ljava/lang/String;)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 99
    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    const-string/jumbo v1, "content"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/chrome/partnercustomizations/PartnerBrowserCustomizations;->sProviderAuthority:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static destroy()V
    .locals 1

    .prologue
    .line 271
    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/apps/chrome/partnercustomizations/PartnerBrowserCustomizations;->sIsInitialized:Z

    .line 272
    sget-object v0, Lcom/google/android/apps/chrome/partnercustomizations/PartnerBrowserCustomizations;->sInitializeAsyncCallbacks:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 273
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/apps/chrome/partnercustomizations/PartnerBrowserCustomizations;->sHomepage:Ljava/lang/String;

    .line 274
    return-void
.end method

.method public static getHomePageUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 281
    sget-object v0, Lcom/google/android/apps/chrome/partnercustomizations/PartnerBrowserCustomizations;->sHomepage:Ljava/lang/String;

    return-object v0
.end method

.method static ignoreBrowserProviderSystemPackageCheckForTests(Z)V
    .locals 0

    .prologue
    .line 95
    sput-boolean p0, Lcom/google/android/apps/chrome/partnercustomizations/PartnerBrowserCustomizations;->sIgnoreBrowserProviderSystemPackageCheck:Z

    .line 96
    return-void
.end method

.method public static initializeAsync(Landroid/content/Context;J)V
    .locals 3

    .prologue
    .line 114
    new-instance v0, Lcom/google/android/apps/chrome/partnercustomizations/PartnerBrowserCustomizations$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/partnercustomizations/PartnerBrowserCustomizations$1;-><init>(Landroid/content/Context;)V

    .line 239
    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Landroid/os/AsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 242
    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v2, Lcom/google/android/apps/chrome/partnercustomizations/PartnerBrowserCustomizations$2;

    invoke-direct {v2, v0}, Lcom/google/android/apps/chrome/partnercustomizations/PartnerBrowserCustomizations$2;-><init>(Landroid/os/AsyncTask;)V

    invoke-virtual {v1, v2, p1, p2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 248
    return-void
.end method

.method static isBookmarksEditingDisabled()Z
    .locals 1

    .prologue
    .line 69
    sget-boolean v0, Lcom/google/android/apps/chrome/partnercustomizations/PartnerBrowserCustomizations;->sBookmarksEditingDisabled:Z

    return v0
.end method

.method public static isHomepageProviderAvailableAndEnabled()Z
    .locals 1

    .prologue
    .line 54
    invoke-static {}, Lcom/google/android/apps/chrome/partnercustomizations/PartnerBrowserCustomizations;->getHomePageUrl()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isIncognitoDisabled()Z
    .locals 1

    .prologue
    .line 61
    sget-boolean v0, Lcom/google/android/apps/chrome/partnercustomizations/PartnerBrowserCustomizations;->sIncognitoModeDisabled:Z

    return v0
.end method

.method static isInitialized()Z
    .locals 1

    .prologue
    .line 78
    sget-boolean v0, Lcom/google/android/apps/chrome/partnercustomizations/PartnerBrowserCustomizations;->sIsInitialized:Z

    return v0
.end method

.method public static setOnInitializeAsyncFinished(Ljava/lang/Runnable;J)V
    .locals 3

    .prologue
    .line 258
    sget-object v0, Lcom/google/android/apps/chrome/partnercustomizations/PartnerBrowserCustomizations;->sInitializeAsyncCallbacks:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 260
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v1, Lcom/google/android/apps/chrome/partnercustomizations/PartnerBrowserCustomizations$3;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/partnercustomizations/PartnerBrowserCustomizations$3;-><init>(Ljava/lang/Runnable;)V

    sget-boolean v2, Lcom/google/android/apps/chrome/partnercustomizations/PartnerBrowserCustomizations;->sIsInitialized:Z

    if-eqz v2, :cond_0

    const-wide/16 p1, 0x0

    :cond_0
    invoke-virtual {v0, v1, p1, p2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 268
    return-void
.end method

.method static setProviderAuthorityForTests(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 83
    sput-object p0, Lcom/google/android/apps/chrome/partnercustomizations/PartnerBrowserCustomizations;->sProviderAuthority:Ljava/lang/String;

    .line 84
    return-void
.end method
