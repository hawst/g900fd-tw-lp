.class Lcom/google/android/apps/chrome/document/DocumentTabList$2;
.super Landroid/os/AsyncTask;
.source "DocumentTabList.java"


# instance fields
.field private final mCachedEntries:Ljava/util/List;

.field final synthetic this$0:Lcom/google/android/apps/chrome/document/DocumentTabList;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/document/DocumentTabList;)V
    .locals 2

    .prologue
    .line 710
    iput-object p1, p0, Lcom/google/android/apps/chrome/document/DocumentTabList$2;->this$0:Lcom/google/android/apps/chrome/document/DocumentTabList;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 711
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/apps/chrome/document/DocumentTabList$2;->this$0:Lcom/google/android/apps/chrome/document/DocumentTabList;

    # getter for: Lcom/google/android/apps/chrome/document/DocumentTabList;->mEntryMap:Landroid/util/SparseArray;
    invoke-static {v1}, Lcom/google/android/apps/chrome/document/DocumentTabList;->access$400(Lcom/google/android/apps/chrome/document/DocumentTabList;)Landroid/util/SparseArray;

    move-result-object v1

    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentTabList$2;->mCachedEntries:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 710
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/document/DocumentTabList$2;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method public varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 4

    .prologue
    .line 726
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentTabList$2;->mCachedEntries:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/document/DocumentTabList$Entry;

    .line 727
    iget-object v2, v0, Lcom/google/android/apps/chrome/document/DocumentTabList$Entry;->tabState:Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;

    .line 728
    iget-object v3, p0, Lcom/google/android/apps/chrome/document/DocumentTabList$2;->this$0:Lcom/google/android/apps/chrome/document/DocumentTabList;

    invoke-virtual {v3, v0, v2}, Lcom/google/android/apps/chrome/document/DocumentTabList;->updateEntryInfoFromTabState(Lcom/google/android/apps/chrome/document/DocumentTabList$Entry;Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;)V

    goto :goto_0

    .line 730
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 710
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/document/DocumentTabList$2;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method public onPostExecute(Ljava/lang/Void;)V
    .locals 4

    .prologue
    .line 735
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentTabList$2;->mCachedEntries:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/document/DocumentTabList$Entry;

    .line 736
    iget-object v1, p0, Lcom/google/android/apps/chrome/document/DocumentTabList$2;->this$0:Lcom/google/android/apps/chrome/document/DocumentTabList;

    # getter for: Lcom/google/android/apps/chrome/document/DocumentTabList;->mEntryMap:Landroid/util/SparseArray;
    invoke-static {v1}, Lcom/google/android/apps/chrome/document/DocumentTabList;->access$400(Lcom/google/android/apps/chrome/document/DocumentTabList;)Landroid/util/SparseArray;

    move-result-object v1

    iget v3, v0, Lcom/google/android/apps/chrome/document/DocumentTabList$Entry;->tabId:I

    invoke-virtual {v1, v3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/chrome/document/DocumentTabList$Entry;

    .line 737
    if-eqz v1, :cond_0

    iget-object v3, v1, Lcom/google/android/apps/chrome/document/DocumentTabList$Entry;->currentUrl:Ljava/lang/String;

    if-nez v3, :cond_0

    .line 738
    iget-object v0, v0, Lcom/google/android/apps/chrome/document/DocumentTabList$Entry;->currentUrl:Ljava/lang/String;

    iput-object v0, v1, Lcom/google/android/apps/chrome/document/DocumentTabList$Entry;->currentUrl:Ljava/lang/String;

    goto :goto_0

    .line 741
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentTabList$2;->this$0:Lcom/google/android/apps/chrome/document/DocumentTabList;

    const/4 v1, 0x6

    # invokes: Lcom/google/android/apps/chrome/document/DocumentTabList;->setCurrentState(I)V
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/document/DocumentTabList;->access$100(Lcom/google/android/apps/chrome/document/DocumentTabList;I)V

    .line 742
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentTabList$2;->this$0:Lcom/google/android/apps/chrome/document/DocumentTabList;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/document/DocumentTabList;->isNativeCreated()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 743
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentTabList$2;->this$0:Lcom/google/android/apps/chrome/document/DocumentTabList;

    iget-object v1, p0, Lcom/google/android/apps/chrome/document/DocumentTabList$2;->this$0:Lcom/google/android/apps/chrome/document/DocumentTabList;

    # getter for: Lcom/google/android/apps/chrome/document/DocumentTabList;->mNativePointer:J
    invoke-static {v1}, Lcom/google/android/apps/chrome/document/DocumentTabList;->access$600(Lcom/google/android/apps/chrome/document/DocumentTabList;)J

    move-result-wide v2

    # invokes: Lcom/google/android/apps/chrome/document/DocumentTabList;->nativeBroadcastSessionRestoreComplete(J)V
    invoke-static {v0, v2, v3}, Lcom/google/android/apps/chrome/document/DocumentTabList;->access$700(Lcom/google/android/apps/chrome/document/DocumentTabList;J)V

    .line 744
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentTabList$2;->this$0:Lcom/google/android/apps/chrome/document/DocumentTabList;

    # invokes: Lcom/google/android/apps/chrome/document/DocumentTabList;->loadHistoricalTabsAsync()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/document/DocumentTabList;->access$800(Lcom/google/android/apps/chrome/document/DocumentTabList;)V

    .line 746
    :cond_2
    return-void
.end method

.method public onPreExecute()V
    .locals 5

    .prologue
    .line 715
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentTabList$2;->this$0:Lcom/google/android/apps/chrome/document/DocumentTabList;

    const/4 v1, 0x5

    # invokes: Lcom/google/android/apps/chrome/document/DocumentTabList;->setCurrentState(I)V
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/document/DocumentTabList;->access$100(Lcom/google/android/apps/chrome/document/DocumentTabList;I)V

    .line 717
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentTabList$2;->this$0:Lcom/google/android/apps/chrome/document/DocumentTabList;

    # getter for: Lcom/google/android/apps/chrome/document/DocumentTabList;->mEntryMap:Landroid/util/SparseArray;
    invoke-static {v0}, Lcom/google/android/apps/chrome/document/DocumentTabList;->access$400(Lcom/google/android/apps/chrome/document/DocumentTabList;)Landroid/util/SparseArray;

    move-result-object v0

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 718
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentTabList$2;->this$0:Lcom/google/android/apps/chrome/document/DocumentTabList;

    # getter for: Lcom/google/android/apps/chrome/document/DocumentTabList;->mEntryMap:Landroid/util/SparseArray;
    invoke-static {v0}, Lcom/google/android/apps/chrome/document/DocumentTabList;->access$400(Lcom/google/android/apps/chrome/document/DocumentTabList;)Landroid/util/SparseArray;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/document/DocumentTabList$Entry;

    .line 719
    iget-object v2, v0, Lcom/google/android/apps/chrome/document/DocumentTabList$Entry;->tabState:Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;

    if-eqz v2, :cond_0

    .line 720
    iget-object v2, p0, Lcom/google/android/apps/chrome/document/DocumentTabList$2;->mCachedEntries:Ljava/util/List;

    new-instance v3, Lcom/google/android/apps/chrome/document/DocumentTabList$Entry;

    iget v4, v0, Lcom/google/android/apps/chrome/document/DocumentTabList$Entry;->tabId:I

    iget-object v0, v0, Lcom/google/android/apps/chrome/document/DocumentTabList$Entry;->tabState:Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;

    invoke-direct {v3, v4, v0}, Lcom/google/android/apps/chrome/document/DocumentTabList$Entry;-><init>(ILcom/google/android/apps/chrome/tab/ChromeTab$TabState;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 717
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 722
    :cond_1
    return-void
.end method
