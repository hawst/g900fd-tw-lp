.class public abstract Lcom/google/android/apps/chrome/bookmark/BookmarkImporter;
.super Ljava/lang/Object;
.source "BookmarkImporter.java"


# static fields
.field private static final EXISTS_PROJECTION:[Ljava/lang/String;

.field static final ROOT_FOLDER_ID:J

.field private static final SELECT_IS_BOOKMARK:Ljava/lang/String;

.field private static final VALUE_IS_BOOKMARK:Ljava/lang/Integer;


# instance fields
.field protected final mContext:Landroid/content/Context;

.field private mTask:Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$ImportBookmarksTask;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 84
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter;->VALUE_IS_BOOKMARK:Ljava/lang/Integer;

    .line 85
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "bookmark="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter;->VALUE_IS_BOOKMARK:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter;->SELECT_IS_BOOKMARK:Ljava/lang/String;

    .line 88
    new-array v0, v2, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "url"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter;->EXISTS_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 94
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 95
    iput-object p1, p0, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter;->mContext:Landroid/content/Context;

    .line 96
    return-void
.end method

.method static synthetic access$000()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter;->VALUE_IS_BOOKMARK:Ljava/lang/Integer;

    return-object v0
.end method

.method static synthetic access$100()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter;->EXISTS_PROJECTION:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200()Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter;->SELECT_IS_BOOKMARK:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method protected abstract availableBookmarks()[Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$BookmarkIterator;
.end method

.method public cancel()V
    .locals 2

    .prologue
    .line 105
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter;->mTask:Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$ImportBookmarksTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$ImportBookmarksTask;->cancel(Z)Z

    .line 106
    return-void
.end method

.method public importBookmarks(Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$OnBookmarksImportedListener;)V
    .locals 2

    .prologue
    .line 100
    new-instance v0, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$ImportBookmarksTask;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$ImportBookmarksTask;-><init>(Lcom/google/android/apps/chrome/bookmark/BookmarkImporter;Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$OnBookmarksImportedListener;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter;->mTask:Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$ImportBookmarksTask;

    .line 101
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter;->mTask:Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$ImportBookmarksTask;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$ImportBookmarksTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 102
    return-void
.end method
