.class public Lcom/google/android/apps/chrome/webapps/FullscreenContentViewContainer;
.super Landroid/widget/FrameLayout;
.source "FullscreenContentViewContainer.java"

# interfaces
.implements Lcom/google/android/apps/chrome/eventfilter/EventFilterHost;


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private mIsKeyboardShowing:Z

.field private final mSwipeEventFilter:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;

.field protected mTabView:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const-class v0, Lcom/google/android/apps/chrome/webapps/FullscreenContentViewContainer;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/webapps/FullscreenContentViewContainer;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;)V
    .locals 2

    .prologue
    .line 43
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 45
    sget-boolean v0, Lcom/google/android/apps/chrome/webapps/FullscreenContentViewContainer;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p3, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 46
    :cond_0
    sget-boolean v0, Lcom/google/android/apps/chrome/webapps/FullscreenContentViewContainer;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 48
    :cond_1
    new-instance v0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/webapps/FullscreenContentViewContainer;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p0, p2}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;-><init>(Landroid/content/Context;Lcom/google/android/apps/chrome/eventfilter/EventFilterHost;Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/webapps/FullscreenContentViewContainer;->mSwipeEventFilter:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;

    .line 49
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/FullscreenContentViewContainer;->mSwipeEventFilter:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;

    invoke-virtual {v0, p3}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->setTabModelSelector(Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;)V

    .line 50
    return-void
.end method


# virtual methods
.method public getView()Landroid/view/View;
    .locals 0

    .prologue
    .line 84
    return-object p0
.end method

.method public onEndGesture()V
    .locals 0

    .prologue
    .line 91
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/FullscreenContentViewContainer;->mTabView:Landroid/view/View;

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 68
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/FullscreenContentViewContainer;->mSwipeEventFilter:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;

    iget-boolean v1, p0, Lcom/google/android/apps/chrome/webapps/FullscreenContentViewContainer;->mIsKeyboardShowing:Z

    invoke-virtual {v0, p1, v1}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->onInterceptTouchEvent(Landroid/view/MotionEvent;Z)Z

    move-result v0

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 1

    .prologue
    .line 61
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onMeasure(II)V

    .line 62
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/webapps/FullscreenContentViewContainer;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p0}, Lorg/chromium/ui/UiUtils;->isKeyboardShowing(Landroid/content/Context;Landroid/view/View;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/webapps/FullscreenContentViewContainer;->mIsKeyboardShowing:Z

    .line 63
    return-void
.end method

.method public onStartGesture()V
    .locals 0

    .prologue
    .line 88
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/FullscreenContentViewContainer;->mTabView:Landroid/view/View;

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 74
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/FullscreenContentViewContainer;->mSwipeEventFilter:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public propagateEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/FullscreenContentViewContainer;->mTabView:Landroid/view/View;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/FullscreenContentViewContainer;->mTabView:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public updateTabView(Landroid/view/View;Z)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 100
    if-eqz p2, :cond_0

    .line 101
    iput-object p1, p0, Lcom/google/android/apps/chrome/webapps/FullscreenContentViewContainer;->mTabView:Landroid/view/View;

    .line 102
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 104
    iget-object v1, p0, Lcom/google/android/apps/chrome/webapps/FullscreenContentViewContainer;->mTabView:Landroid/view/View;

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/chrome/webapps/FullscreenContentViewContainer;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 109
    :goto_0
    return-void

    .line 106
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/FullscreenContentViewContainer;->mTabView:Landroid/view/View;

    if-ne v0, p1, :cond_1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/webapps/FullscreenContentViewContainer;->mTabView:Landroid/view/View;

    .line 107
    :cond_1
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/webapps/FullscreenContentViewContainer;->removeView(Landroid/view/View;)V

    goto :goto_0
.end method
