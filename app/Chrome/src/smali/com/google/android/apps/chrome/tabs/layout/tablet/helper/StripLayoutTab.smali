.class public Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;
.super Ljava/lang/Object;
.source "StripLayoutTab.java"

# interfaces
.implements Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;


# instance fields
.field private mCanShowCloseButton:Z

.field private final mCloseButton:Lcom/google/android/apps/chrome/tabs/CompositorButton;

.field private final mClosePlacement:Landroid/graphics/RectF;

.field private mContentAnimations:Lcom/google/android/apps/chrome/ChromeAnimation;

.field private mContentOffsetX:F

.field private mDrawX:F

.field private mDrawY:F

.field private mHeight:F

.field private mId:I

.field private mIdealX:F

.field private mIncognito:Z

.field private mIsDying:Z

.field private final mLoadTracker:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/TabLoadTracker;

.field private mLoadingSpinnerRotationDegrees:F

.field private final mRenderHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;

.field private mShowingCloseButton:Z

.field private mTabOffsetX:F

.field private mTabOffsetY:F

.field private mVisible:Z

.field private mVisiblePercentage:F

.field private mWidth:F


# direct methods
.method public constructor <init>(Landroid/content/Context;ILcom/google/android/apps/chrome/tabs/layout/tablet/helper/TabLoadTracker$TabLoadTrackerCallback;Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;Z)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v5, 0x0

    .line 99
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->mId:I

    .line 57
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->mVisible:Z

    .line 58
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->mIsDying:Z

    .line 59
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->mCanShowCloseButton:Z

    .line 62
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->mVisiblePercentage:F

    .line 75
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->mShowingCloseButton:Z

    .line 82
    iput v5, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->mLoadingSpinnerRotationDegrees:F

    .line 85
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->mClosePlacement:Landroid/graphics/RectF;

    .line 100
    iput p2, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->mId:I

    .line 101
    new-instance v0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/TabLoadTracker;

    invoke-direct {v0, p2, p3}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/TabLoadTracker;-><init>(ILcom/google/android/apps/chrome/tabs/layout/tablet/helper/TabLoadTracker$TabLoadTrackerCallback;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->mLoadTracker:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/TabLoadTracker;

    .line 102
    iput-object p4, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->mRenderHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;

    .line 103
    iput-boolean p5, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->mIncognito:Z

    .line 104
    new-instance v0, Lcom/google/android/apps/chrome/tabs/CompositorButton;

    invoke-direct {v0, p1, v5, v5}, Lcom/google/android/apps/chrome/tabs/CompositorButton;-><init>(Landroid/content/Context;FF)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->mCloseButton:Lcom/google/android/apps/chrome/tabs/CompositorButton;

    .line 105
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->mCloseButton:Lcom/google/android/apps/chrome/tabs/CompositorButton;

    sget v1, Lcom/google/android/apps/chrome/R$drawable;->btn_tab_close_normal:I

    sget v2, Lcom/google/android/apps/chrome/R$drawable;->btn_tab_close_pressed:I

    sget v3, Lcom/google/android/apps/chrome/R$drawable;->btn_tab_close_white_normal:I

    sget v4, Lcom/google/android/apps/chrome/R$drawable;->btn_tab_close_white_pressed:I

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/apps/chrome/tabs/CompositorButton;->setResources(IIII)V

    .line 109
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->mCloseButton:Lcom/google/android/apps/chrome/tabs/CompositorButton;

    iget-boolean v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->mIncognito:Z

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/tabs/CompositorButton;->setIncognito(Z)V

    .line 110
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->mCloseButton:Lcom/google/android/apps/chrome/tabs/CompositorButton;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->getCloseRect()Landroid/graphics/RectF;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/tabs/CompositorButton;->setBounds(Landroid/graphics/RectF;)V

    .line 111
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->mCloseButton:Lcom/google/android/apps/chrome/tabs/CompositorButton;

    invoke-virtual {v0, v5}, Lcom/google/android/apps/chrome/tabs/CompositorButton;->setClickSlop(F)V

    .line 112
    return-void
.end method

.method private buildCloseButtonOpacityAnimation(F)Lcom/google/android/apps/chrome/ChromeAnimation$Animation;
    .locals 10

    .prologue
    .line 519
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->mCloseButton:Lcom/google/android/apps/chrome/tabs/CompositorButton;

    sget-object v1, Lcom/google/android/apps/chrome/tabs/CompositorButton$Property;->OPACITY:Lcom/google/android/apps/chrome/tabs/CompositorButton$Property;

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->mCloseButton:Lcom/google/android/apps/chrome/tabs/CompositorButton;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tabs/CompositorButton;->getOpacity()F

    move-result v2

    const-wide/16 v4, 0x96

    const-wide/16 v6, 0x0

    const/4 v8, 0x0

    invoke-static {}, Lcom/google/android/apps/chrome/ChromeAnimation;->getLinearInterpolator()Landroid/view/animation/LinearInterpolator;

    move-result-object v9

    move v3, p1

    invoke-static/range {v0 .. v9}, Lcom/google/android/apps/chrome/ChromeAnimation$AnimatableAnimation;->createAnimation(Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJZLandroid/view/animation/Interpolator;)Lcom/google/android/apps/chrome/ChromeAnimation$Animation;

    move-result-object v0

    return-object v0
.end method

.method private checkCloseButtonVisibility(Z)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 502
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->mCanShowCloseButton:Z

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->mVisiblePercentage:F

    const v3, 0x3f7d70a4    # 0.99f

    cmpl-float v0, v0, v3

    if-lez v0, :cond_1

    move v0, v1

    .line 505
    :goto_0
    iget-boolean v3, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->mShowingCloseButton:Z

    if-eq v0, v3, :cond_0

    .line 506
    if-eqz v0, :cond_2

    const/high16 v3, 0x3f800000    # 1.0f

    .line 507
    :goto_1
    if-eqz p1, :cond_3

    .line 508
    invoke-direct {p0, v3}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->buildCloseButtonOpacityAnimation(F)Lcom/google/android/apps/chrome/ChromeAnimation$Animation;

    move-result-object v3

    invoke-direct {p0, v3, v1}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->startAnimation(Lcom/google/android/apps/chrome/ChromeAnimation$Animation;Z)V

    .line 512
    :goto_2
    iput-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->mShowingCloseButton:Z

    .line 513
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->mShowingCloseButton:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->mCloseButton:Lcom/google/android/apps/chrome/tabs/CompositorButton;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/chrome/tabs/CompositorButton;->setPressed(Z)V

    .line 515
    :cond_0
    return-void

    :cond_1
    move v0, v2

    .line 502
    goto :goto_0

    .line 506
    :cond_2
    const/4 v3, 0x0

    goto :goto_1

    .line 510
    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->mCloseButton:Lcom/google/android/apps/chrome/tabs/CompositorButton;

    invoke-virtual {v1, v3}, Lcom/google/android/apps/chrome/tabs/CompositorButton;->setOpacity(F)V

    goto :goto_2
.end method

.method private getCloseRect()Landroid/graphics/RectF;
    .locals 4

    .prologue
    const/high16 v3, 0x42100000    # 36.0f

    const/4 v0, 0x0

    .line 475
    invoke-static {}, Lorg/chromium/ui/base/LocalizationUtils;->isLayoutRtl()Z

    move-result v1

    if-nez v1, :cond_1

    .line 476
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->mClosePlacement:Landroid/graphics/RectF;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->getWidth()F

    move-result v2

    sub-float/2addr v2, v3

    iput v2, v1, Landroid/graphics/RectF;->left:F

    .line 477
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->mClosePlacement:Landroid/graphics/RectF;

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->mClosePlacement:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->left:F

    add-float/2addr v2, v3

    iput v2, v1, Landroid/graphics/RectF;->right:F

    .line 483
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->mClosePlacement:Landroid/graphics/RectF;

    iput v0, v1, Landroid/graphics/RectF;->top:F

    .line 484
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->mClosePlacement:Landroid/graphics/RectF;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->getHeight()F

    move-result v2

    iput v2, v1, Landroid/graphics/RectF;->bottom:F

    .line 487
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->mRenderHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;

    invoke-interface {v1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;->getResourceManager()Lcom/google/android/apps/chrome/compositor/resources/ResourceManager;

    move-result-object v1

    .line 488
    if-eqz v1, :cond_0

    .line 489
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->getResourceId(Z)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/chrome/compositor/resources/ResourceManager;->getResource(I)Lcom/google/android/apps/chrome/compositor/resources/LayoutResource;

    move-result-object v1

    .line 490
    if-eqz v1, :cond_0

    .line 491
    invoke-static {}, Lorg/chromium/ui/base/LocalizationUtils;->isLayoutRtl()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/compositor/resources/LayoutResource;->getPadding()Landroid/graphics/RectF;

    move-result-object v0

    iget v0, v0, Landroid/graphics/RectF;->left:F

    .line 496
    :cond_0
    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->mClosePlacement:Landroid/graphics/RectF;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->getDrawX()F

    move-result v2

    add-float/2addr v0, v2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->getDrawY()F

    move-result v2

    invoke-virtual {v1, v0, v2}, Landroid/graphics/RectF;->offset(FF)V

    .line 497
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->mClosePlacement:Landroid/graphics/RectF;

    return-object v0

    .line 479
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->mClosePlacement:Landroid/graphics/RectF;

    iput v0, v1, Landroid/graphics/RectF;->left:F

    .line 480
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->mClosePlacement:Landroid/graphics/RectF;

    iput v3, v1, Landroid/graphics/RectF;->right:F

    goto :goto_0

    .line 491
    :cond_2
    invoke-virtual {v1}, Lcom/google/android/apps/chrome/compositor/resources/LayoutResource;->getBitmapSize()Landroid/graphics/RectF;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v0

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/compositor/resources/LayoutResource;->getPadding()Landroid/graphics/RectF;

    move-result-object v1

    iget v1, v1, Landroid/graphics/RectF;->right:F

    sub-float/2addr v0, v1

    neg-float v0, v0

    goto :goto_1
.end method

.method private resetCloseRect()V
    .locals 3

    .prologue
    .line 467
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->getCloseRect()Landroid/graphics/RectF;

    move-result-object v0

    .line 468
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->mCloseButton:Lcom/google/android/apps/chrome/tabs/CompositorButton;

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/chrome/tabs/CompositorButton;->setWidth(F)V

    .line 469
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->mCloseButton:Lcom/google/android/apps/chrome/tabs/CompositorButton;

    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/chrome/tabs/CompositorButton;->setHeight(F)V

    .line 470
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->mCloseButton:Lcom/google/android/apps/chrome/tabs/CompositorButton;

    iget v2, v0, Landroid/graphics/RectF;->left:F

    invoke-virtual {v1, v2}, Lcom/google/android/apps/chrome/tabs/CompositorButton;->setX(F)V

    .line 471
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->mCloseButton:Lcom/google/android/apps/chrome/tabs/CompositorButton;

    iget v0, v0, Landroid/graphics/RectF;->top:F

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/tabs/CompositorButton;->setY(F)V

    .line 472
    return-void
.end method

.method private startAnimation(Lcom/google/android/apps/chrome/ChromeAnimation$Animation;Z)V
    .locals 1

    .prologue
    .line 404
    if-eqz p2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->finishAnimation()V

    .line 406
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->mContentAnimations:Lcom/google/android/apps/chrome/ChromeAnimation;

    if-nez v0, :cond_1

    .line 407
    new-instance v0, Lcom/google/android/apps/chrome/ChromeAnimation;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/ChromeAnimation;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->mContentAnimations:Lcom/google/android/apps/chrome/ChromeAnimation;

    .line 410
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->mContentAnimations:Lcom/google/android/apps/chrome/ChromeAnimation;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/ChromeAnimation;->add(Lcom/google/android/apps/chrome/ChromeAnimation$Animation;)V

    .line 411
    return-void
.end method


# virtual methods
.method public addLoadingSpinnerRotation(F)V
    .locals 2

    .prologue
    .line 183
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->mLoadingSpinnerRotationDegrees:F

    add-float/2addr v0, p1

    const/high16 v1, 0x44870000    # 1080.0f

    rem-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->mLoadingSpinnerRotationDegrees:F

    .line 185
    return-void
.end method

.method public checkCloseHitTest(FF)Z
    .locals 1

    .prologue
    .line 351
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->mShowingCloseButton:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->mCloseButton:Lcom/google/android/apps/chrome/tabs/CompositorButton;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/chrome/tabs/CompositorButton;->checkClicked(FF)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public finishAnimation()V
    .locals 1

    .prologue
    .line 417
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->mContentAnimations:Lcom/google/android/apps/chrome/ChromeAnimation;

    if-nez v0, :cond_0

    .line 421
    :goto_0
    return-void

    .line 419
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->mContentAnimations:Lcom/google/android/apps/chrome/ChromeAnimation;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeAnimation;->updateAndFinish()V

    .line 420
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->mContentAnimations:Lcom/google/android/apps/chrome/ChromeAnimation;

    goto :goto_0
.end method

.method public getCloseButton()Lcom/google/android/apps/chrome/tabs/CompositorButton;
    .locals 1

    .prologue
    .line 331
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->mCloseButton:Lcom/google/android/apps/chrome/tabs/CompositorButton;

    return-object v0
.end method

.method public getClosePressed()Z
    .locals 1

    .prologue
    .line 324
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->mCloseButton:Lcom/google/android/apps/chrome/tabs/CompositorButton;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/CompositorButton;->isPressed()Z

    move-result v0

    return v0
.end method

.method public getContentOffsetX()F
    .locals 1

    .prologue
    .line 226
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->mContentOffsetX:F

    return v0
.end method

.method public getDrawX()F
    .locals 1

    .prologue
    .line 265
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->mDrawX:F

    return v0
.end method

.method public getDrawY()F
    .locals 1

    .prologue
    .line 280
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->mDrawY:F

    return v0
.end method

.method public getHeight()F
    .locals 1

    .prologue
    .line 310
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->mHeight:F

    return v0
.end method

.method public getId()I
    .locals 1

    .prologue
    .line 118
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->mId:I

    return v0
.end method

.method public getIdealX()F
    .locals 1

    .prologue
    .line 383
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->mIdealX:F

    return v0
.end method

.method public getLoadingSpinnerRotation()F
    .locals 1

    .prologue
    .line 175
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->mLoadingSpinnerRotationDegrees:F

    return v0
.end method

.method public getOffsetX()F
    .locals 1

    .prologue
    .line 367
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->mTabOffsetX:F

    return v0
.end method

.method public getOffsetY()F
    .locals 1

    .prologue
    .line 399
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->mTabOffsetY:F

    return v0
.end method

.method public getResourceId(Z)I
    .locals 1

    .prologue
    .line 126
    if-eqz p1, :cond_1

    .line 127
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->mIncognito:Z

    if-eqz v0, :cond_0

    sget v0, Lcom/google/android/apps/chrome/R$drawable;->bg_tabstrip_incognito_tab:I

    .line 130
    :goto_0
    return v0

    .line 127
    :cond_0
    sget v0, Lcom/google/android/apps/chrome/R$drawable;->bg_tabstrip_tab:I

    goto :goto_0

    .line 130
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->mIncognito:Z

    if-eqz v0, :cond_2

    sget v0, Lcom/google/android/apps/chrome/R$drawable;->bg_tabstrip_incognito_background_tab:I

    goto :goto_0

    :cond_2
    sget v0, Lcom/google/android/apps/chrome/R$drawable;->bg_tabstrip_background_tab:I

    goto :goto_0
.end method

.method public getVisiblePercentage()F
    .locals 1

    .prologue
    .line 242
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->mVisiblePercentage:F

    return v0
.end method

.method public getWidth()F
    .locals 1

    .prologue
    .line 295
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->mWidth:F

    return v0
.end method

.method public getWidthWeight()F
    .locals 3

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    .line 341
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->mDrawY:F

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->mHeight:F

    div-float/2addr v0, v1

    sub-float v0, v2, v0

    const/4 v1, 0x0

    invoke-static {v0, v1, v2}, Lorg/chromium/chrome/browser/util/MathUtils;->clamp(FFF)F

    move-result v0

    return v0
.end method

.method public isAnimating()Z
    .locals 1

    .prologue
    .line 427
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->mContentAnimations:Lcom/google/android/apps/chrome/ChromeAnimation;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isDying()Z
    .locals 1

    .prologue
    .line 161
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->mIsDying:Z

    return v0
.end method

.method public isLoading()Z
    .locals 1

    .prologue
    .line 168
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->mLoadTracker:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/TabLoadTracker;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/TabLoadTracker;->isLoading()Z

    move-result v0

    return v0
.end method

.method public isVisible()Z
    .locals 1

    .prologue
    .line 145
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->mVisible:Z

    return v0
.end method

.method public loadingFinished()V
    .locals 1

    .prologue
    .line 212
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->mLoadTracker:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/TabLoadTracker;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/TabLoadTracker;->loadingFinished()V

    .line 213
    return-void
.end method

.method public loadingStarted()V
    .locals 1

    .prologue
    .line 205
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->mLoadTracker:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/TabLoadTracker;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/TabLoadTracker;->loadingStarted()V

    .line 206
    return-void
.end method

.method public onUpdateAnimation(JZ)Z
    .locals 1

    .prologue
    .line 437
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->mContentAnimations:Lcom/google/android/apps/chrome/ChromeAnimation;

    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 448
    :cond_0
    :goto_0
    return v0

    .line 439
    :cond_1
    if-eqz p3, :cond_3

    .line 441
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->mContentAnimations:Lcom/google/android/apps/chrome/ChromeAnimation;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeAnimation;->finished()Z

    move-result v0

    .line 446
    :goto_1
    if-nez p3, :cond_2

    if-eqz v0, :cond_0

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->finishAnimation()V

    goto :goto_0

    .line 443
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->mContentAnimations:Lcom/google/android/apps/chrome/ChromeAnimation;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/chrome/ChromeAnimation;->update(J)Z

    move-result v0

    goto :goto_1
.end method

.method public pageLoadingFinished()V
    .locals 1

    .prologue
    .line 198
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->mLoadTracker:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/TabLoadTracker;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/TabLoadTracker;->pageLoadingFinished()V

    .line 199
    return-void
.end method

.method public pageLoadingStarted()V
    .locals 1

    .prologue
    .line 191
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->mLoadTracker:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/TabLoadTracker;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/TabLoadTracker;->pageLoadingStarted()V

    .line 192
    return-void
.end method

.method public setCanShowCloseButton(Z)V
    .locals 1

    .prologue
    .line 249
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->mCanShowCloseButton:Z

    .line 250
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->checkCloseButtonVisibility(Z)V

    .line 251
    return-void
.end method

.method public setClosePressed(Z)V
    .locals 1

    .prologue
    .line 317
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->mCloseButton:Lcom/google/android/apps/chrome/tabs/CompositorButton;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/tabs/CompositorButton;->setPressed(Z)V

    .line 318
    return-void
.end method

.method public setContentOffsetX(F)V
    .locals 2

    .prologue
    .line 219
    const/4 v0, 0x0

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->mWidth:F

    invoke-static {p1, v0, v1}, Lorg/chromium/chrome/browser/util/MathUtils;->clamp(FFF)F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->mContentOffsetX:F

    .line 220
    return-void
.end method

.method public setDrawX(F)V
    .locals 3

    .prologue
    .line 257
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->mCloseButton:Lcom/google/android/apps/chrome/tabs/CompositorButton;

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->mCloseButton:Lcom/google/android/apps/chrome/tabs/CompositorButton;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tabs/CompositorButton;->getX()F

    move-result v1

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->mDrawX:F

    sub-float v2, p1, v2

    add-float/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/tabs/CompositorButton;->setX(F)V

    .line 258
    iput p1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->mDrawX:F

    .line 259
    return-void
.end method

.method public setDrawY(F)V
    .locals 3

    .prologue
    .line 272
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->mCloseButton:Lcom/google/android/apps/chrome/tabs/CompositorButton;

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->mCloseButton:Lcom/google/android/apps/chrome/tabs/CompositorButton;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tabs/CompositorButton;->getY()F

    move-result v1

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->mDrawY:F

    sub-float v2, p1, v2

    add-float/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/tabs/CompositorButton;->setY(F)V

    .line 273
    iput p1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->mDrawY:F

    .line 274
    return-void
.end method

.method public setHeight(F)V
    .locals 0

    .prologue
    .line 302
    iput p1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->mHeight:F

    .line 303
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->resetCloseRect()V

    .line 304
    return-void
.end method

.method public setIdealX(F)V
    .locals 0

    .prologue
    .line 375
    iput p1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->mIdealX:F

    .line 376
    return-void
.end method

.method public setIsDying(Z)V
    .locals 0

    .prologue
    .line 154
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->mIsDying:Z

    .line 155
    return-void
.end method

.method public setOffsetX(F)V
    .locals 0

    .prologue
    .line 359
    iput p1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->mTabOffsetX:F

    .line 360
    return-void
.end method

.method public setOffsetY(F)V
    .locals 0

    .prologue
    .line 391
    iput p1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->mTabOffsetY:F

    .line 392
    return-void
.end method

.method public setProperty(Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab$Property;F)V
    .locals 2

    .prologue
    .line 453
    sget-object v0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab$1;->$SwitchMap$com$google$android$apps$chrome$tabs$layout$tablet$helper$StripLayoutTab$Property:[I

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab$Property;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 464
    :goto_0
    return-void

    .line 455
    :pswitch_0
    invoke-virtual {p0, p2}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->setOffsetX(F)V

    goto :goto_0

    .line 458
    :pswitch_1
    invoke-virtual {p0, p2}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->setOffsetY(F)V

    goto :goto_0

    .line 461
    :pswitch_2
    invoke-virtual {p0, p2}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->setWidth(F)V

    goto :goto_0

    .line 453
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public bridge synthetic setProperty(Ljava/lang/Enum;F)V
    .locals 0

    .prologue
    .line 32
    check-cast p1, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab$Property;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->setProperty(Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab$Property;F)V

    return-void
.end method

.method public setVisible(Z)V
    .locals 0

    .prologue
    .line 138
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->mVisible:Z

    .line 139
    return-void
.end method

.method public setVisiblePercentage(F)V
    .locals 1

    .prologue
    .line 233
    iput p1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->mVisiblePercentage:F

    .line 234
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->checkCloseButtonVisibility(Z)V

    .line 235
    return-void
.end method

.method public setWidth(F)V
    .locals 0

    .prologue
    .line 287
    iput p1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->mWidth:F

    .line 288
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->resetCloseRect()V

    .line 289
    return-void
.end method
