.class public Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;
.super Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;
.source "ChromeFullscreenManager.java"

# interfaces
.implements Lorg/chromium/base/ApplicationStatus$ActivityStateListener;
.implements Lorg/chromium/base/BaseChromiumApplication$WindowFocusChangedListener;


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final mActivity:Landroid/app/Activity;

.field private mBrowserControlOffset:F

.field private mContentViewScrolling:Z

.field private mControlAnimation:Landroid/animation/ObjectAnimator;

.field private mControlContainer:Lcom/google/android/apps/chrome/widget/ControlContainer;

.field private final mControlContainerHeight:I

.field private mControlOffset:F

.field private mCurrentAnimationIsShowing:Z

.field private mCurrentShowTime:J

.field private mDisableBrowserOverride:Z

.field private final mEnabled:Z

.field private final mHandler:Landroid/os/Handler;

.field private mInGesture:Z

.field private mIsEnteringPersistentModeState:Z

.field private final mListeners:Ljava/util/ArrayList;

.field private mMaxAnimationDurationMs:J

.field private mMinShowNotificationMs:J

.field private final mPersistentControlTokens:Ljava/util/HashSet;

.field private mPersistentControlsCurrentToken:I

.field private mPreviousContentOffset:F

.field private mPreviousControlOffset:F

.field private mRendererContentOffset:F

.field private mRendererControlOffset:F

.field private mTopControlsAndroidViewHidden:Z

.field private mTopControlsPermanentlyHidden:Z

.field private final mUpdateVisibilityRunnable:Ljava/lang/Runnable;

.field private final mWindow:Landroid/view/Window;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 46
    const-class v0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/apps/chrome/widget/ControlContainer;ZZLorg/chromium/chrome/browser/tabmodel/TabModelSelector;I)V
    .locals 3

    .prologue
    const/high16 v2, 0x7fc00000    # NaNf

    .line 161
    invoke-virtual {p1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-direct {p0, v0, p5, p3, p4}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;-><init>(Landroid/view/Window;Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;ZZ)V

    .line 60
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mPersistentControlTokens:Ljava/util/HashSet;

    .line 69
    const-wide/16 v0, 0xbb8

    iput-wide v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mMinShowNotificationMs:J

    .line 70
    const-wide/16 v0, 0x1f4

    iput-wide v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mMaxAnimationDurationMs:J

    .line 74
    iput v2, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mBrowserControlOffset:F

    .line 75
    iput v2, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mRendererControlOffset:F

    .line 96
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mListeners:Ljava/util/ArrayList;

    .line 142
    new-instance v0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager$1;-><init>(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mUpdateVisibilityRunnable:Ljava/lang/Runnable;

    .line 163
    iput-object p1, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mActivity:Landroid/app/Activity;

    .line 164
    invoke-static {p0, p1}, Lorg/chromium/base/ApplicationStatus;->registerStateListenerForActivity(Lorg/chromium/base/ApplicationStatus$ActivityStateListener;Landroid/app/Activity;)V

    .line 165
    invoke-virtual {p1}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lorg/chromium/base/BaseChromiumApplication;

    invoke-virtual {v0, p0}, Lorg/chromium/base/BaseChromiumApplication;->registerWindowFocusChangedListener(Lorg/chromium/base/BaseChromiumApplication$WindowFocusChangedListener;)V

    .line 168
    invoke-virtual {p1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mWindow:Landroid/view/Window;

    .line 169
    new-instance v0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager$2;-><init>(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mHandler:Landroid/os/Handler;

    .line 186
    invoke-virtual {p0, p2}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->setControlContainer(Lcom/google/android/apps/chrome/widget/ControlContainer;)V

    .line 187
    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mWindow:Landroid/view/Window;

    invoke-virtual {v0}, Landroid/view/Window;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 188
    invoke-virtual {v0, p6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mControlContainerHeight:I

    .line 189
    iget v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mControlContainerHeight:I

    int-to-float v0, v0

    iput v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mRendererContentOffset:F

    .line 190
    iput-boolean p3, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mEnabled:Z

    .line 191
    invoke-direct {p0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->updateControlOffset()V

    .line 192
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;)Z
    .locals 1

    .prologue
    .line 46
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mDisableBrowserOverride:Z

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;)F
    .locals 1

    .prologue
    .line 46
    iget v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mBrowserControlOffset:F

    return v0
.end method

.method static synthetic access$102(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;F)F
    .locals 0

    .prologue
    .line 46
    iput p1, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mBrowserControlOffset:F

    return p1
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;)V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->updateControlOffset()V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;)V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->updateVisuals()V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;)Z
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->shouldShowAndroidControls()Z

    move-result v0

    return v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;)Lcom/google/android/apps/chrome/widget/ControlContainer;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mControlContainer:Lcom/google/android/apps/chrome/widget/ControlContainer;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;Z)V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->update(Z)V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;)Z
    .locals 1

    .prologue
    .line 46
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mIsEnteringPersistentModeState:Z

    return v0
.end method

.method static synthetic access$702(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;Z)Z
    .locals 0

    .prologue
    .line 46
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mIsEnteringPersistentModeState:Z

    return p1
.end method

.method static synthetic access$802(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;Landroid/animation/ObjectAnimator;)Landroid/animation/ObjectAnimator;
    .locals 0

    .prologue
    .line 46
    iput-object p1, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mControlAnimation:Landroid/animation/ObjectAnimator;

    return-object p1
.end method

.method private animateIfNecessary(ZJ)V
    .locals 6

    .prologue
    .line 685
    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mControlAnimation:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_1

    .line 686
    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mControlAnimation:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mCurrentAnimationIsShowing:Z

    if-eq v0, p1, :cond_2

    .line 687
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mControlAnimation:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 688
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mControlAnimation:Landroid/animation/ObjectAnimator;

    .line 694
    :cond_1
    if-eqz p1, :cond_3

    const/4 v0, 0x0

    .line 695
    :goto_0
    iget-wide v2, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mMaxAnimationDurationMs:J

    long-to-float v1, v2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->getControlOffset()F

    move-result v2

    sub-float v2, v0, v2

    iget v3, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mControlContainerHeight:I

    int-to-float v3, v3

    div-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    mul-float/2addr v1, v2

    float-to-long v2, v1

    .line 697
    new-instance v1, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager$ControlsOffsetProperty;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager$ControlsOffsetProperty;-><init>(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;)V

    const/4 v4, 0x1

    new-array v4, v4, [F

    const/4 v5, 0x0

    aput v0, v4, v5

    invoke-static {p0, v1, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mControlAnimation:Landroid/animation/ObjectAnimator;

    .line 698
    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mControlAnimation:Landroid/animation/ObjectAnimator;

    new-instance v1, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager$4;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager$4;-><init>(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;Z)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 712
    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mControlAnimation:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0, p2, p3}, Landroid/animation/ObjectAnimator;->setStartDelay(J)V

    .line 713
    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mControlAnimation:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 714
    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mControlAnimation:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 715
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mCurrentAnimationIsShowing:Z

    .line 716
    :cond_2
    return-void

    .line 694
    :cond_3
    iget v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mControlContainerHeight:I

    neg-int v0, v0

    int-to-float v0, v0

    goto :goto_0
.end method

.method private applyMarginToFullChildViews(Landroid/view/ViewGroup;F)V
    .locals 5

    .prologue
    .line 565
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 566
    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 567
    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    instance-of v0, v0, Landroid/widget/FrameLayout$LayoutParams;

    if-eqz v0, :cond_0

    .line 568
    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 571
    iget v3, v0, Landroid/widget/FrameLayout$LayoutParams;->height:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_0

    iget v3, v0, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    float-to-int v4, p2

    if-eq v3, v4, :cond_0

    .line 573
    float-to-int v3, p2

    iput v3, v0, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 574
    invoke-virtual {v2}, Landroid/view/View;->requestLayout()V

    .line 575
    const-string/jumbo v0, "FullscreenManager:child.requestLayout()"

    invoke-static {v0}, Lorg/chromium/base/TraceEvent;->instant(Ljava/lang/String;)V

    .line 565
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 578
    :cond_1
    return-void
.end method

.method private applyTranslationToTopChildViews(Landroid/view/ViewGroup;F)V
    .locals 4

    .prologue
    .line 581
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 582
    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 583
    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    instance-of v0, v0, Landroid/widget/FrameLayout$LayoutParams;

    if-eqz v0, :cond_0

    .line 585
    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 587
    const/16 v3, 0x30

    iget v0, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    and-int/lit8 v0, v0, 0x70

    if-ne v3, v0, :cond_0

    .line 588
    invoke-virtual {v2, p2}, Landroid/view/View;->setTranslationY(F)V

    .line 589
    const-string/jumbo v0, "FullscreenManager:child.setTranslationY()"

    invoke-static {v0}, Lorg/chromium/base/TraceEvent;->instant(Ljava/lang/String;)V

    .line 581
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 592
    :cond_1
    return-void
.end method

.method private getActiveContentViewCore()Lorg/chromium/content/browser/ContentViewCore;
    .locals 1

    .prologue
    .line 595
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->getTabModelSelector()Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    move-result-object v0

    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->getCurrentTab()Lorg/chromium/chrome/browser/Tab;

    move-result-object v0

    .line 596
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/Tab;->getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private rendererContentOffset()F
    .locals 1

    .prologue
    .line 412
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mEnabled:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mControlContainerHeight:I

    int-to-float v0, v0

    .line 413
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mRendererContentOffset:F

    goto :goto_0
.end method

.method private rendererControlOffset()F
    .locals 1

    .prologue
    .line 417
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mEnabled:Z

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 418
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mRendererControlOffset:F

    goto :goto_0
.end method

.method private scheduleVisibilityUpdate()V
    .locals 2

    .prologue
    .line 476
    invoke-direct {p0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->shouldShowAndroidControls()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 477
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mControlContainer:Lcom/google/android/apps/chrome/widget/ControlContainer;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/widget/ControlContainer;->getVisibility()I

    move-result v1

    if-ne v1, v0, :cond_1

    .line 480
    :goto_1
    return-void

    .line 476
    :cond_0
    const/4 v0, 0x4

    goto :goto_0

    .line 478
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mControlContainer:Lcom/google/android/apps/chrome/widget/ControlContainer;

    iget-object v1, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mUpdateVisibilityRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/widget/ControlContainer;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 479
    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mControlContainer:Lcom/google/android/apps/chrome/widget/ControlContainer;

    iget-object v1, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mUpdateVisibilityRunnable:Ljava/lang/Runnable;

    invoke-static {v0, v1}, Lorg/chromium/base/ApiCompatibilityUtils;->postOnAnimation(Landroid/view/View;Ljava/lang/Runnable;)V

    goto :goto_1
.end method

.method private shouldShowAndroidControls()Z
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 540
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mTopControlsAndroidViewHidden:Z

    if-eqz v0, :cond_0

    .line 561
    :goto_0
    return v3

    .line 542
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->getControlOffset()F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_1

    move v1, v2

    .line 543
    :goto_1
    invoke-direct {p0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->getActiveContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    .line 544
    if-nez v0, :cond_2

    move v3, v1

    goto :goto_0

    :cond_1
    move v1, v3

    .line 542
    goto :goto_1

    .line 545
    :cond_2
    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->getContainerView()Landroid/view/ViewGroup;

    move-result-object v5

    move v4, v3

    .line 547
    :goto_2
    invoke-virtual {v5}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-ge v4, v0, :cond_3

    .line 548
    invoke-virtual {v5, v4}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 549
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    instance-of v6, v6, Landroid/widget/FrameLayout$LayoutParams;

    if-eqz v6, :cond_4

    .line 551
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 553
    const/16 v6, 0x30

    iget v0, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    and-int/lit8 v0, v0, 0x70

    if-ne v6, v0, :cond_4

    move v1, v2

    .line 559
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mPersistentControlTokens:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    :goto_3
    or-int v3, v1, v2

    .line 561
    goto :goto_0

    .line 547
    :cond_4
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_2

    :cond_5
    move v2, v3

    .line 559
    goto :goto_3
.end method

.method private update(Z)V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const-wide/16 v6, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 656
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mInGesture:Z

    .line 657
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mContentViewScrolling:Z

    .line 659
    iget-boolean v2, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mEnabled:Z

    if-nez v2, :cond_1

    .line 682
    :cond_0
    :goto_0
    return-void

    .line 661
    :cond_1
    if-eqz p1, :cond_2

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mCurrentShowTime:J

    .line 664
    :cond_2
    if-nez p1, :cond_5

    .line 665
    iget-object v2, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mControlAnimation:Landroid/animation/ObjectAnimator;

    if-eqz v2, :cond_4

    iget-boolean v2, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mCurrentAnimationIsShowing:Z

    if-eqz v2, :cond_4

    .line 676
    :cond_3
    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v8}, Landroid/os/Handler;->removeMessages(I)V

    .line 677
    if-eqz v0, :cond_0

    .line 678
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mCurrentShowTime:J

    sub-long/2addr v0, v2

    .line 679
    iget-object v2, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mHandler:Landroid/os/Handler;

    iget-wide v4, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mMinShowNotificationMs:J

    sub-long v0, v4, v0

    invoke-static {v0, v1, v6, v7}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    invoke-virtual {v2, v8, v0, v1}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 668
    :cond_4
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mCurrentShowTime:J

    sub-long/2addr v2, v4

    .line 669
    iget-wide v4, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mMinShowNotificationMs:J

    sub-long v2, v4, v2

    invoke-static {v2, v3, v6, v7}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    invoke-direct {p0, v1, v2, v3}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->animateIfNecessary(ZJ)V

    move v0, v1

    .line 670
    goto :goto_1

    .line 672
    :cond_5
    invoke-direct {p0, v0, v6, v7}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->animateIfNecessary(ZJ)V

    .line 673
    iget-object v2, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mPersistentControlTokens:Ljava/util/HashSet;

    invoke-virtual {v2}, Ljava/util/HashSet;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    goto :goto_1
.end method

.method private updateControlOffset()V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 370
    const/4 v4, 0x0

    .line 372
    iget v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mBrowserControlOffset:F

    iget v3, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mBrowserControlOffset:F

    cmpl-float v0, v0, v3

    if-eqz v0, :cond_2

    move v0, v1

    .line 373
    :goto_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->rendererControlOffset()F

    move-result v3

    .line 374
    cmpl-float v5, v3, v3

    if-eqz v5, :cond_0

    move v2, v1

    .line 375
    :cond_0
    if-eqz v0, :cond_1

    if-nez v2, :cond_5

    .line 376
    :cond_1
    if-eqz v0, :cond_3

    iget v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mControlContainerHeight:I

    neg-int v0, v0

    int-to-float v0, v0

    move v1, v0

    :goto_1
    if-eqz v2, :cond_4

    iget v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mControlContainerHeight:I

    neg-int v0, v0

    int-to-float v0, v0

    :goto_2
    invoke-static {v1, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 382
    :goto_3
    iput v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mControlOffset:F

    .line 383
    return-void

    :cond_2
    move v0, v2

    .line 372
    goto :goto_0

    .line 376
    :cond_3
    iget v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mBrowserControlOffset:F

    move v1, v0

    goto :goto_1

    :cond_4
    move v0, v3

    goto :goto_2

    :cond_5
    move v0, v4

    goto :goto_3
.end method

.method private updateVisuals()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 483
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mEnabled:Z

    if-nez v0, :cond_0

    .line 526
    :goto_0
    return-void

    .line 485
    :cond_0
    const-string/jumbo v0, "FullscreenManager:updateVisuals"

    invoke-static {v0}, Lorg/chromium/base/TraceEvent;->begin(Ljava/lang/String;)V

    .line 487
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->getControlOffset()F

    move-result v3

    .line 488
    iget v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mPreviousControlOffset:F

    invoke-static {v0, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v0

    if-eqz v0, :cond_3

    .line 489
    iput v3, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mPreviousControlOffset:F

    .line 490
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->getHtmlApiHandler()Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;->updateBubblePosition()V

    .line 492
    invoke-direct {p0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->scheduleVisibilityUpdate()V

    .line 493
    invoke-direct {p0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->shouldShowAndroidControls()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mControlContainer:Lcom/google/android/apps/chrome/widget/ControlContainer;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->getControlOffset()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/widget/ControlContainer;->setTranslationY(F)V

    .line 498
    :cond_1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-ge v0, v1, :cond_2

    .line 499
    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_2

    .line 500
    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_2
    move v1, v2

    .line 503
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 504
    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager$FullscreenListener;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->getVisibleContentOffset()F

    move-result v4

    invoke-interface {v0, v4}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager$FullscreenListener;->onVisibleContentOffsetChanged(F)V

    .line 503
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 508
    :cond_3
    invoke-direct {p0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->getActiveContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    .line 509
    if-eqz v0, :cond_4

    iget v1, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mControlContainerHeight:I

    neg-int v1, v1

    int-to-float v1, v1

    cmpl-float v1, v3, v1

    if-nez v1, :cond_4

    iget-boolean v1, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mIsEnteringPersistentModeState:Z

    if-eqz v1, :cond_4

    .line 511
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->getHtmlApiHandler()Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;->enterFullscreen(Lorg/chromium/content/browser/ContentViewCore;)V

    .line 512
    iput-boolean v2, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mIsEnteringPersistentModeState:Z

    .line 515
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->updateContentViewChildrenState()V

    .line 517
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->getContentOffset()F

    move-result v1

    .line 518
    iget v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mPreviousContentOffset:F

    invoke-static {v0, v1}, Ljava/lang/Float;->compare(FF)I

    move-result v0

    if-eqz v0, :cond_6

    .line 519
    :goto_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_5

    .line 520
    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager$FullscreenListener;

    invoke-interface {v0, v1}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager$FullscreenListener;->onContentOffsetChanged(F)V

    .line 519
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 522
    :cond_5
    iput v1, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mPreviousContentOffset:F

    .line 525
    :cond_6
    const-string/jumbo v0, "FullscreenManager:updateVisuals"

    invoke-static {v0}, Lorg/chromium/base/TraceEvent;->end(Ljava/lang/String;)V

    goto/16 :goto_0
.end method


# virtual methods
.method public addListener(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager$FullscreenListener;)V
    .locals 1

    .prologue
    .line 433
    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 434
    :cond_0
    return-void
.end method

.method public areTopControlsPermanentlyHidden()Z
    .locals 1

    .prologue
    .line 337
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mTopControlsPermanentlyHidden:Z

    return v0
.end method

.method public controlContainerHeight()F
    .locals 1

    .prologue
    .line 408
    iget v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mControlContainerHeight:I

    int-to-float v0, v0

    return v0
.end method

.method protected createApiDelegate()Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler$FullscreenHtmlApiDelegate;
    .locals 1

    .prologue
    .line 234
    new-instance v0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager$3;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager$3;-><init>(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;)V

    return-object v0
.end method

.method protected disableBrowserOverrideForTest()V
    .locals 2

    .prologue
    .line 276
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mDisableBrowserOverride:Z

    .line 277
    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mPersistentControlTokens:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    .line 278
    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 279
    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mControlAnimation:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_0

    .line 280
    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mControlAnimation:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 281
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mControlAnimation:Landroid/animation/ObjectAnimator;

    .line 283
    :cond_0
    const/high16 v0, 0x7fc00000    # NaNf

    iput v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mBrowserControlOffset:F

    .line 284
    invoke-direct {p0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->updateVisuals()V

    .line 285
    return-void
.end method

.method public drawControlsAsTexture()Z
    .locals 2

    .prologue
    .line 344
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->getControlOffset()F

    move-result v0

    iget v1, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mControlContainerHeight:I

    neg-int v1, v1

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getContentOffset()F
    .locals 1

    .prologue
    .line 356
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mEnabled:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mTopControlsPermanentlyHidden:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    .line 357
    :goto_0
    return v0

    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->rendererContentOffset()F

    move-result v0

    goto :goto_0
.end method

.method public getControlOffset()F
    .locals 1

    .prologue
    .line 364
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mEnabled:Z

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 366
    :goto_0
    return v0

    .line 365
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mTopControlsPermanentlyHidden:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->getTopControlsHeight()I

    move-result v0

    neg-int v0, v0

    int-to-float v0, v0

    goto :goto_0

    .line 366
    :cond_1
    iget v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mControlOffset:F

    goto :goto_0
.end method

.method public getTopControlsHeight()I
    .locals 1

    .prologue
    .line 351
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mEnabled:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mControlContainerHeight:I

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getVisibleContentOffset()F
    .locals 2

    .prologue
    .line 425
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mEnabled:Z

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 426
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mControlContainerHeight:I

    int-to-float v0, v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->getControlOffset()F

    move-result v1

    add-float/2addr v0, v1

    goto :goto_0
.end method

.method protected hasBrowserControlOffsetOverride()Z
    .locals 1

    .prologue
    .line 399
    iget v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mBrowserControlOffset:F

    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mControlAnimation:Landroid/animation/ObjectAnimator;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mPersistentControlTokens:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hideControlsPersistent(I)V
    .locals 2

    .prologue
    .line 319
    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mPersistentControlTokens:Ljava/util/HashSet;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mPersistentControlTokens:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 320
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->update(Z)V

    .line 322
    :cond_0
    return-void
.end method

.method public isEnabled()Z
    .locals 1

    .prologue
    .line 198
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mEnabled:Z

    return v0
.end method

.method public onActivityStateChange(Landroid/app/Activity;I)V
    .locals 1

    .prologue
    .line 213
    const/4 v0, 0x5

    if-ne p2, v0, :cond_1

    .line 217
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->setPersistentFullscreenMode(Z)V

    .line 225
    :cond_0
    :goto_0
    return-void

    .line 218
    :cond_1
    const/4 v0, 0x2

    if-ne p2, v0, :cond_2

    .line 219
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->showControlsTransient()V

    goto :goto_0

    .line 220
    :cond_2
    const/4 v0, 0x6

    if-ne p2, v0, :cond_0

    .line 221
    invoke-static {p0}, Lorg/chromium/base/ApplicationStatus;->unregisterActivityStateListener(Lorg/chromium/base/ApplicationStatus$ActivityStateListener;)V

    .line 222
    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mWindow:Landroid/view/Window;

    invoke-virtual {v0}, Landroid/view/Window;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lorg/chromium/base/BaseChromiumApplication;

    invoke-virtual {v0, p0}, Lorg/chromium/base/BaseChromiumApplication;->unregisterWindowFocusChangedListener(Lorg/chromium/base/BaseChromiumApplication$WindowFocusChangedListener;)V

    goto :goto_0
.end method

.method public onContentViewScrollingStateChanged(Z)V
    .locals 0

    .prologue
    .line 720
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mContentViewScrolling:Z

    .line 721
    if-nez p1, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->updateVisuals()V

    .line 722
    :cond_0
    return-void
.end method

.method public onInterceptMotionEvent(Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    .line 630
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mEnabled:Z

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->getControlOffset()F

    move-result v1

    iget v2, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mControlContainerHeight:I

    int-to-float v2, v2

    add-float/2addr v1, v2

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mTopControlsAndroidViewHidden:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onMotionEvent(Landroid/view/MotionEvent;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 639
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mEnabled:Z

    if-nez v0, :cond_1

    .line 650
    :cond_0
    :goto_0
    return-void

    .line 640
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    .line 641
    if-eqz v0, :cond_2

    const/4 v1, 0x5

    if-ne v0, v1, :cond_3

    .line 643
    :cond_2
    iput-boolean v2, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mInGesture:Z

    .line 644
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->getHtmlApiHandler()Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;->hideNotificationBubble()V

    goto :goto_0

    .line 645
    :cond_3
    const/4 v1, 0x3

    if-eq v0, v1, :cond_4

    if-ne v0, v2, :cond_0

    .line 647
    :cond_4
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mInGesture:Z

    .line 648
    invoke-direct {p0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->updateVisuals()V

    goto :goto_0
.end method

.method public onWindowFocusChanged(Landroid/app/Activity;Z)V
    .locals 1

    .prologue
    .line 229
    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mActivity:Landroid/app/Activity;

    if-ne v0, p1, :cond_0

    invoke-virtual {p0, p2}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->onWindowFocusChanged(Z)V

    .line 230
    :cond_0
    return-void
.end method

.method public removeListener(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager$FullscreenListener;)V
    .locals 1

    .prologue
    .line 441
    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 442
    return-void
.end method

.method protected setAnimationDurationsForTest(JJ)V
    .locals 1

    .prologue
    .line 294
    iput-wide p1, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mMinShowNotificationMs:J

    .line 295
    iput-wide p3, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mMaxAnimationDurationMs:J

    .line 296
    return-void
.end method

.method public setControlContainer(Lcom/google/android/apps/chrome/widget/ControlContainer;)V
    .locals 1

    .prologue
    .line 207
    sget-boolean v0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 208
    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mControlContainer:Lcom/google/android/apps/chrome/widget/ControlContainer;

    .line 209
    return-void
.end method

.method public setHideTopControlsAndroidView(Z)V
    .locals 1

    .prologue
    .line 534
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mTopControlsAndroidViewHidden:Z

    if-ne v0, p1, :cond_0

    .line 537
    :goto_0
    return-void

    .line 535
    :cond_0
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mTopControlsAndroidViewHidden:Z

    .line 536
    invoke-direct {p0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->scheduleVisibilityUpdate()V

    goto :goto_0
.end method

.method public setOverlayVideoMode(Z)V
    .locals 2

    .prologue
    .line 387
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->setOverlayVideoMode(Z)V

    .line 389
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 390
    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager$FullscreenListener;

    invoke-interface {v0, p1}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager$FullscreenListener;->onToggleOverlayVideoMode(Z)V

    .line 389
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 392
    :cond_0
    return-void
.end method

.method public setPositionsForTab(FF)V
    .locals 3

    .prologue
    .line 606
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mEnabled:Z

    if-nez v0, :cond_1

    .line 622
    :cond_0
    :goto_0
    return-void

    .line 607
    :cond_1
    iget v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mControlContainerHeight:I

    neg-int v0, v0

    int-to-float v0, v0

    invoke-static {p1, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    int-to-float v0, v0

    .line 609
    invoke-static {p2}, Ljava/lang/Math;->round(F)I

    move-result v1

    int-to-float v1, v1

    iget v2, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mControlContainerHeight:I

    int-to-float v2, v2

    add-float/2addr v2, v0

    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v1

    .line 612
    iget v2, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mRendererControlOffset:F

    invoke-static {v0, v2}, Ljava/lang/Float;->compare(FF)I

    move-result v2

    if-nez v2, :cond_2

    iget v2, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mRendererContentOffset:F

    invoke-static {v1, v2}, Ljava/lang/Float;->compare(FF)I

    move-result v2

    if-eqz v2, :cond_0

    .line 617
    :cond_2
    iput v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mRendererControlOffset:F

    .line 618
    iput v1, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mRendererContentOffset:F

    .line 619
    invoke-direct {p0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->updateControlOffset()V

    .line 621
    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mControlAnimation:Landroid/animation/ObjectAnimator;

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->updateVisuals()V

    goto :goto_0
.end method

.method public setPositionsForTabToNonFullscreen()V
    .locals 2

    .prologue
    .line 601
    const/4 v0, 0x0

    iget v1, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mControlContainerHeight:I

    int-to-float v1, v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->setPositionsForTab(FF)V

    .line 602
    return-void
.end method

.method public setTopControlsPermamentlyHidden(Z)V
    .locals 1

    .prologue
    .line 328
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mTopControlsPermanentlyHidden:Z

    if-ne p1, v0, :cond_0

    .line 331
    :goto_0
    return-void

    .line 329
    :cond_0
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mTopControlsPermanentlyHidden:Z

    .line 330
    invoke-direct {p0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->updateVisuals()V

    goto :goto_0
.end method

.method public showControlsPersistent()I
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 305
    iget v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mPersistentControlsCurrentToken:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mPersistentControlsCurrentToken:I

    .line 306
    iget-object v1, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mPersistentControlTokens:Ljava/util/HashSet;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 307
    iget-object v1, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mPersistentControlTokens:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/util/HashSet;->size()I

    move-result v1

    if-ne v1, v3, :cond_0

    invoke-direct {p0, v3}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->update(Z)V

    .line 308
    :cond_0
    return v0
.end method

.method public showControlsPersistentAndClearOldToken(I)I
    .locals 2

    .prologue
    .line 313
    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mPersistentControlTokens:Ljava/util/HashSet;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 314
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->showControlsPersistent()I

    move-result v0

    return v0
.end method

.method public showControlsTransient()V
    .locals 1

    .prologue
    .line 300
    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mPersistentControlTokens:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->update(Z)V

    .line 301
    :cond_0
    return-void
.end method

.method public updateContentViewChildrenState()V
    .locals 4

    .prologue
    .line 461
    invoke-direct {p0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->getActiveContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    .line 462
    if-eqz v0, :cond_0

    iget-boolean v1, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mEnabled:Z

    if-nez v1, :cond_1

    .line 469
    :cond_0
    :goto_0
    return-void

    .line 463
    :cond_1
    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->getContainerView()Landroid/view/ViewGroup;

    move-result-object v1

    .line 465
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->getControlOffset()F

    move-result v2

    iget v3, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mControlContainerHeight:I

    int-to-float v3, v3

    add-float/2addr v2, v3

    .line 466
    invoke-direct {p0, v1, v2}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->applyTranslationToTopChildViews(Landroid/view/ViewGroup;F)V

    .line 467
    invoke-direct {p0, v1, v2}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->applyMarginToFullChildViews(Landroid/view/ViewGroup;F)V

    .line 468
    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->updateContentViewViewportSize(Lorg/chromium/content/browser/ContentViewCore;)V

    goto :goto_0
.end method

.method public updateContentViewViewportSize(Lorg/chromium/content/browser/ContentViewCore;)V
    .locals 2

    .prologue
    .line 450
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mEnabled:Z

    if-eqz v0, :cond_0

    if-nez p1, :cond_1

    .line 457
    :cond_0
    :goto_0
    return-void

    .line 451
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mInGesture:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mContentViewScrolling:Z

    if-nez v0, :cond_0

    .line 454
    invoke-direct {p0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->rendererContentOffset()F

    move-result v0

    float-to-int v0, v0

    .line 455
    if-eqz v0, :cond_2

    iget v1, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mControlContainerHeight:I

    if-ne v0, v1, :cond_0

    .line 456
    :cond_2
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, Lorg/chromium/content/browser/ContentViewCore;->setViewportSizeOffset(II)V

    goto :goto_0
.end method
