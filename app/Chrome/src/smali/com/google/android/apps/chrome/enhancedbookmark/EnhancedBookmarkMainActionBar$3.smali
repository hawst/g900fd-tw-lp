.class Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkMainActionBar$3;
.super Ljava/lang/Object;
.source "EnhancedBookmarkMainActionBar.java"

# interfaces
.implements Landroid/support/v7/widget/F;


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkMainActionBar;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 60
    const-class v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkMainActionBar;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkMainActionBar$3;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkMainActionBar;)V
    .locals 0

    .prologue
    .line 60
    iput-object p1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkMainActionBar$3;->this$0:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkMainActionBar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 63
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sget v1, Lcom/google/android/apps/chrome/R$id;->edit_menu_id:I

    if-eq v0, v1, :cond_0

    .line 65
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sget v1, Lcom/google/android/apps/chrome/R$id;->search_menu_id:I

    if-eq v0, v1, :cond_0

    .line 67
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sget v1, Lcom/google/android/apps/chrome/R$id;->close_menu_id:I

    if-ne v0, v1, :cond_0

    .line 68
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkMainActionBar$3;->this$0:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkMainActionBar;

    # getter for: Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkMainActionBar;->mDelegate:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;
    invoke-static {v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkMainActionBar;->access$000(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkMainActionBar;)Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;->closeDialog()V

    .line 69
    const/4 v0, 0x1

    .line 73
    :goto_0
    return v0

    .line 72
    :cond_0
    sget-boolean v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkMainActionBar$3;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    const-string/jumbo v1, "Unhandled menu click."

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 73
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
