.class public Lcom/google/android/apps/chrome/services/AndroidEdu;
.super Ljava/lang/Object;
.source "AndroidEdu.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    return-void
.end method

.method public static checkIsAndroidEduDevice(Landroid/content/Context;Lcom/google/android/apps/chrome/services/AndroidEdu$OwnerCheckCallback;)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    .line 60
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x12

    if-ge v0, v2, :cond_1

    .line 61
    invoke-static {p1}, Lcom/google/android/apps/chrome/services/AndroidEdu;->notifyNonSchoolAsync(Lcom/google/android/apps/chrome/services/AndroidEdu$OwnerCheckCallback;)V

    .line 126
    :cond_0
    :goto_0
    return-void

    .line 65
    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 66
    const-string/jumbo v0, "device_policy"

    invoke-virtual {v2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/admin/DevicePolicyManager;

    .line 68
    if-eqz v0, :cond_2

    :try_start_0
    const-string/jumbo v3, "com.google.android.apps.enterprise.dmagent"

    invoke-virtual {v0, v3}, Landroid/app/admin/DevicePolicyManager;->isDeviceOwnerApp(Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v0

    if-nez v0, :cond_3

    :cond_2
    move v0, v1

    .line 80
    :goto_1
    if-eqz v0, :cond_4

    .line 81
    invoke-static {p1}, Lcom/google/android/apps/chrome/services/AndroidEdu;->notifyNonSchoolAsync(Lcom/google/android/apps/chrome/services/AndroidEdu$OwnerCheckCallback;)V

    goto :goto_0

    .line 68
    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    .line 72
    :catch_0
    move-exception v0

    .line 73
    const-string/jumbo v3, "AndroidEdu"

    const-string/jumbo v4, "Failure calling isDeviceOwnerApp"

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move v0, v1

    .line 78
    goto :goto_1

    .line 75
    :catch_1
    move-exception v0

    .line 76
    const-string/jumbo v3, "AndroidEdu"

    const-string/jumbo v4, "Failure calling isDeviceOwnerApp"

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move v0, v1

    .line 77
    goto :goto_1

    .line 89
    :cond_4
    new-instance v0, Lcom/google/android/apps/chrome/services/AndroidEdu$1;

    invoke-direct {v0, v2, p1}, Lcom/google/android/apps/chrome/services/AndroidEdu$1;-><init>(Landroid/content/Context;Lcom/google/android/apps/chrome/services/AndroidEdu$OwnerCheckCallback;)V

    .line 120
    new-instance v3, Landroid/content/Intent;

    const-string/jumbo v4, "com.google.android.nfcprovision.IOwnedService.BIND"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 121
    new-instance v4, Landroid/content/ComponentName;

    const-string/jumbo v5, "com.google.android.nfcprovision"

    const-string/jumbo v6, "com.google.android.nfcprovision.SchoolOwnedService"

    invoke-direct {v4, v5, v6}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 122
    invoke-virtual {v2, v3, v0, v1}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 123
    const-string/jumbo v0, "AndroidEdu"

    const-string/jumbo v1, "Could not find IOwnedService"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 124
    invoke-static {p1}, Lcom/google/android/apps/chrome/services/AndroidEdu;->notifyNonSchoolAsync(Lcom/google/android/apps/chrome/services/AndroidEdu$OwnerCheckCallback;)V

    goto :goto_0
.end method

.method private static notifyNonSchoolAsync(Lcom/google/android/apps/chrome/services/AndroidEdu$OwnerCheckCallback;)V
    .locals 2

    .prologue
    .line 134
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v1, Lcom/google/android/apps/chrome/services/AndroidEdu$2;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/services/AndroidEdu$2;-><init>(Lcom/google/android/apps/chrome/services/AndroidEdu$OwnerCheckCallback;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 140
    return-void
.end method
