.class public Lcom/google/android/apps/chrome/SystemDownloadNotifier;
.super Ljava/lang/Object;
.source "SystemDownloadNotifier.java"

# interfaces
.implements Lcom/google/android/apps/chrome/DownloadNotifier;


# instance fields
.field private final mApplicationContext:Landroid/content/Context;

.field private final mDownloadManager:Landroid/app/DownloadManager;

.field private final mNotificationManager:Landroid/app/NotificationManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/SystemDownloadNotifier;->mApplicationContext:Landroid/content/Context;

    .line 44
    iget-object v0, p0, Lcom/google/android/apps/chrome/SystemDownloadNotifier;->mApplicationContext:Landroid/content/Context;

    const-string/jumbo v1, "notification"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    iput-object v0, p0, Lcom/google/android/apps/chrome/SystemDownloadNotifier;->mNotificationManager:Landroid/app/NotificationManager;

    .line 46
    iget-object v0, p0, Lcom/google/android/apps/chrome/SystemDownloadNotifier;->mApplicationContext:Landroid/content/Context;

    const-string/jumbo v1, "download"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/DownloadManager;

    iput-object v0, p0, Lcom/google/android/apps/chrome/SystemDownloadNotifier;->mDownloadManager:Landroid/app/DownloadManager;

    .line 48
    return-void
.end method

.method private updateNotification(ILandroid/app/Notification;)V
    .locals 2

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/android/apps/chrome/SystemDownloadNotifier;->mNotificationManager:Landroid/app/NotificationManager;

    const-string/jumbo v1, "SystemDownloadNotifier"

    invoke-virtual {v0, v1, p1, p2}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    .line 57
    return-void
.end method


# virtual methods
.method public cancelNotification(I)V
    .locals 2

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/android/apps/chrome/SystemDownloadNotifier;->mNotificationManager:Landroid/app/NotificationManager;

    const-string/jumbo v1, "SystemDownloadNotifier"

    invoke-virtual {v0, v1, p1}, Landroid/app/NotificationManager;->cancel(Ljava/lang/String;I)V

    .line 66
    return-void
.end method

.method public notifyDownloadFailed(Lorg/chromium/content/browser/DownloadInfo;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 90
    new-instance v0, Landroid/support/v4/app/L;

    iget-object v1, p0, Lcom/google/android/apps/chrome/SystemDownloadNotifier;->mApplicationContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/support/v4/app/L;-><init>(Landroid/content/Context;)V

    invoke-virtual {p1}, Lorg/chromium/content/browser/DownloadInfo;->getFileName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/L;->a(Ljava/lang/CharSequence;)Landroid/support/v4/app/L;

    move-result-object v0

    const v1, 0x1080082

    invoke-virtual {v0, v1}, Landroid/support/v4/app/L;->a(I)Landroid/support/v4/app/L;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/app/L;->a(Z)Landroid/support/v4/app/L;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/support/v4/app/L;->b(Z)Landroid/support/v4/app/L;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/SystemDownloadNotifier;->mApplicationContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/chrome/R$string;->download_notification_failed:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/L;->b(Ljava/lang/CharSequence;)Landroid/support/v4/app/L;

    move-result-object v0

    .line 99
    invoke-static {v0, v3}, Lcom/google/android/apps/chrome/utilities/UnreleasedApiCompatibilityUtils;->setLocalOnly(Landroid/support/v4/app/L;Z)Landroid/support/v4/app/L;

    move-result-object v0

    .line 100
    invoke-virtual {p1}, Lorg/chromium/content/browser/DownloadInfo;->getDownloadId()I

    move-result v1

    invoke-virtual {v0}, Landroid/support/v4/app/L;->a()Landroid/app/Notification;

    move-result-object v0

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/chrome/SystemDownloadNotifier;->updateNotification(ILandroid/app/Notification;)V

    .line 101
    return-void
.end method

.method public notifyDownloadProgress(Lorg/chromium/content/browser/DownloadInfo;J)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 106
    invoke-virtual {p1}, Lorg/chromium/content/browser/DownloadInfo;->getPercentCompleted()I

    move-result v0

    const/4 v2, -0x1

    if-ne v0, v2, :cond_2

    move v0, v1

    .line 107
    :goto_0
    new-instance v2, Landroid/support/v4/app/L;

    iget-object v3, p0, Lcom/google/android/apps/chrome/SystemDownloadNotifier;->mApplicationContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Landroid/support/v4/app/L;-><init>(Landroid/content/Context;)V

    invoke-virtual {p1}, Lorg/chromium/content/browser/DownloadInfo;->getFileName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/support/v4/app/L;->a(Ljava/lang/CharSequence;)Landroid/support/v4/app/L;

    move-result-object v2

    const v3, 0x1080081

    invoke-virtual {v2, v3}, Landroid/support/v4/app/L;->a(I)Landroid/support/v4/app/L;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/support/v4/app/L;->a(Z)Landroid/support/v4/app/L;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/support/v4/app/L;->b(Z)Landroid/support/v4/app/L;

    move-result-object v2

    const/16 v3, 0x64

    invoke-virtual {p1}, Lorg/chromium/content/browser/DownloadInfo;->getPercentCompleted()I

    move-result v4

    invoke-virtual {v2, v3, v4, v0}, Landroid/support/v4/app/L;->a(IIZ)Landroid/support/v4/app/L;

    move-result-object v2

    .line 114
    invoke-static {v2, v1}, Lcom/google/android/apps/chrome/utilities/UnreleasedApiCompatibilityUtils;->setLocalOnly(Landroid/support/v4/app/L;Z)Landroid/support/v4/app/L;

    move-result-object v1

    .line 116
    if-nez v0, :cond_0

    .line 117
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-static {v0}, Ljava/text/NumberFormat;->getPercentInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v0

    .line 118
    invoke-virtual {p1}, Lorg/chromium/content/browser/DownloadInfo;->getPercentCompleted()I

    move-result v2

    int-to-double v2, v2

    const-wide/high16 v4, 0x4059000000000000L    # 100.0

    div-double/2addr v2, v4

    invoke-virtual {v0, v2, v3}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object v0

    .line 119
    invoke-virtual {p1}, Lorg/chromium/content/browser/DownloadInfo;->getTimeRemainingInMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Lorg/chromium/ui/base/LocalizationUtils;->getDurationString(J)Ljava/lang/String;

    move-result-object v2

    .line 121
    invoke-virtual {v1, v2}, Landroid/support/v4/app/L;->b(Ljava/lang/CharSequence;)Landroid/support/v4/app/L;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/support/v4/app/L;->c(Ljava/lang/CharSequence;)Landroid/support/v4/app/L;

    .line 124
    :cond_0
    const-wide/16 v2, 0x0

    cmp-long v0, p2, v2

    if-lez v0, :cond_1

    invoke-virtual {v1, p2, p3}, Landroid/support/v4/app/L;->a(J)Landroid/support/v4/app/L;

    .line 126
    :cond_1
    invoke-virtual {p1}, Lorg/chromium/content/browser/DownloadInfo;->getDownloadId()I

    move-result v0

    invoke-virtual {v1}, Landroid/support/v4/app/L;->a()Landroid/app/Notification;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/SystemDownloadNotifier;->updateNotification(ILandroid/app/Notification;)V

    .line 127
    return-void

    .line 106
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public notifyDownloadSuccessful(Lorg/chromium/content/browser/DownloadInfo;)V
    .locals 9

    .prologue
    .line 70
    invoke-virtual {p1}, Lorg/chromium/content/browser/DownloadInfo;->getMimeType()Ljava/lang/String;

    move-result-object v4

    .line 71
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v4, "application/unknown"

    .line 74
    :cond_0
    :try_start_0
    invoke-virtual {p1}, Lorg/chromium/content/browser/DownloadInfo;->getDescription()Ljava/lang/String;

    move-result-object v2

    .line 75
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lorg/chromium/content/browser/DownloadInfo;->getFileName()Ljava/lang/String;

    move-result-object v2

    .line 76
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/SystemDownloadNotifier;->mDownloadManager:Landroid/app/DownloadManager;

    invoke-virtual {p1}, Lorg/chromium/content/browser/DownloadInfo;->getFileName()Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x1

    invoke-virtual {p1}, Lorg/chromium/content/browser/DownloadInfo;->getFilePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1}, Lorg/chromium/content/browser/DownloadInfo;->getContentLength()J

    move-result-wide v6

    const/4 v8, 0x1

    invoke-virtual/range {v0 .. v8}, Landroid/app/DownloadManager;->addCompletedDownload(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;JZ)J
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 86
    :goto_0
    return-void

    .line 79
    :catch_0
    move-exception v0

    .line 82
    const-string/jumbo v1, "SystemDownloadNotifier"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Failed to add the download item to DownloadManager: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 83
    iget-object v0, p0, Lcom/google/android/apps/chrome/SystemDownloadNotifier;->mApplicationContext:Landroid/content/Context;

    sget v1, Lcom/google/android/apps/chrome/R$string;->cannot_add_downloaded_item_to_manager:I

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method
