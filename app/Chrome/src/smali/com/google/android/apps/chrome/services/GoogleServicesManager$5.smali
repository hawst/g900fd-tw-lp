.class Lcom/google/android/apps/chrome/services/GoogleServicesManager$5;
.super Ljava/lang/Object;
.source "GoogleServicesManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/services/GoogleServicesManager;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/services/GoogleServicesManager;)V
    .locals 0

    .prologue
    .line 457
    iput-object p1, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager$5;->this$0:Lcom/google/android/apps/chrome/services/GoogleServicesManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 460
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager$5;->this$0:Lcom/google/android/apps/chrome/services/GoogleServicesManager;

    # getter for: Lcom/google/android/apps/chrome/services/GoogleServicesManager;->mChromeSigninController:Lorg/chromium/sync/signin/ChromeSigninController;
    invoke-static {v0}, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->access$600(Lcom/google/android/apps/chrome/services/GoogleServicesManager;)Lorg/chromium/sync/signin/ChromeSigninController;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/sync/signin/ChromeSigninController;->getSignedInUser()Landroid/accounts/Account;

    move-result-object v0

    .line 461
    if-nez v0, :cond_0

    .line 462
    const-string/jumbo v0, "GoogleServicesManager"

    const-string/jumbo v1, "Ignoring pref change because user is not signed in to Chrome"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 473
    :goto_0
    return-void

    .line 465
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager$5;->this$0:Lcom/google/android/apps/chrome/services/GoogleServicesManager;

    # getter for: Lcom/google/android/apps/chrome/services/GoogleServicesManager;->mSyncStatusHelper:Lorg/chromium/sync/notifier/SyncStatusHelper;
    invoke-static {v1}, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->access$700(Lcom/google/android/apps/chrome/services/GoogleServicesManager;)Lorg/chromium/sync/notifier/SyncStatusHelper;

    move-result-object v1

    invoke-virtual {v1, v0}, Lorg/chromium/sync/notifier/SyncStatusHelper;->isSyncEnabledForChrome(Landroid/accounts/Account;)Z

    move-result v1

    .line 466
    iget-object v2, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager$5;->this$0:Lcom/google/android/apps/chrome/services/GoogleServicesManager;

    # getter for: Lcom/google/android/apps/chrome/services/GoogleServicesManager;->mSyncStatusHelper:Lorg/chromium/sync/notifier/SyncStatusHelper;
    invoke-static {v2}, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->access$700(Lcom/google/android/apps/chrome/services/GoogleServicesManager;)Lorg/chromium/sync/notifier/SyncStatusHelper;

    move-result-object v2

    invoke-virtual {v2}, Lorg/chromium/sync/notifier/SyncStatusHelper;->isMasterSyncAutomaticallyEnabled()Z

    move-result v2

    .line 467
    iget-object v3, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager$5;->this$0:Lcom/google/android/apps/chrome/services/GoogleServicesManager;

    # invokes: Lcom/google/android/apps/chrome/services/GoogleServicesManager;->getPreviousMasterSyncState()Z
    invoke-static {v3}, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->access$800(Lcom/google/android/apps/chrome/services/GoogleServicesManager;)Z

    move-result v3

    .line 468
    iget-object v4, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager$5;->this$0:Lcom/google/android/apps/chrome/services/GoogleServicesManager;

    # invokes: Lcom/google/android/apps/chrome/services/GoogleServicesManager;->getWantedSyncState(Landroid/accounts/Account;)Z
    invoke-static {v4, v0}, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->access$900(Lcom/google/android/apps/chrome/services/GoogleServicesManager;Landroid/accounts/Account;)Z

    move-result v4

    .line 469
    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/apps/chrome/services/SyncStateCalculator;->calculateNewSyncStates(Landroid/accounts/Account;ZZZZ)Lcom/google/android/apps/chrome/services/GoogleServicesStates;

    move-result-object v1

    .line 472
    iget-object v2, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager$5;->this$0:Lcom/google/android/apps/chrome/services/GoogleServicesManager;

    invoke-virtual {v2, v1, v0}, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->setStates(Lcom/google/android/apps/chrome/services/GoogleServicesStates;Landroid/accounts/Account;)V

    goto :goto_0
.end method
