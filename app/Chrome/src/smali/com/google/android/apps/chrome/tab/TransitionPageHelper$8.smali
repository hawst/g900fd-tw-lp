.class Lcom/google/android/apps/chrome/tab/TransitionPageHelper$8;
.super Ljava/lang/Object;
.source "TransitionPageHelper.java"

# interfaces
.implements Lorg/chromium/content_public/browser/NavigationTransitionDelegate;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/tab/TransitionPageHelper;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/tab/TransitionPageHelper;)V
    .locals 0

    .prologue
    .line 572
    iput-object p1, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper$8;->this$0:Lcom/google/android/apps/chrome/tab/TransitionPageHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public addEnteringStylesheetToTransition(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 608
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper$8;->this$0:Lcom/google/android/apps/chrome/tab/TransitionPageHelper;

    # getter for: Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mTransitionContentViewCore:Lorg/chromium/content/browser/ContentViewCore;
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->access$000(Lcom/google/android/apps/chrome/tab/TransitionPageHelper;)Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper$8;->this$0:Lcom/google/android/apps/chrome/tab/TransitionPageHelper;

    # invokes: Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->isTransitionRunning()Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->access$1800(Lcom/google/android/apps/chrome/tab/TransitionPageHelper;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 609
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper$8;->this$0:Lcom/google/android/apps/chrome/tab/TransitionPageHelper;

    # getter for: Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mTransitionStylesheets:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->access$2400(Lcom/google/android/apps/chrome/tab/TransitionPageHelper;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 611
    :cond_0
    return-void
.end method

.method public didDeferAfterResponseStarted(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 576
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper$8;->this$0:Lcom/google/android/apps/chrome/tab/TransitionPageHelper;

    # getter for: Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mTransitionContentViewCore:Lorg/chromium/content/browser/ContentViewCore;
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->access$000(Lcom/google/android/apps/chrome/tab/TransitionPageHelper;)Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper$8;->this$0:Lcom/google/android/apps/chrome/tab/TransitionPageHelper;

    # invokes: Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->isTransitionRunning()Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->access$1800(Lcom/google/android/apps/chrome/tab/TransitionPageHelper;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 577
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper$8;->this$0:Lcom/google/android/apps/chrome/tab/TransitionPageHelper;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mDestinationReadyToResume:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->access$1902(Lcom/google/android/apps/chrome/tab/TransitionPageHelper;Z)Z

    .line 579
    new-instance v0, Lorg/chromium/content_public/browser/LoadUrlParams;

    const-string/jumbo v1, "about:blank"

    invoke-direct {v0, v1}, Lorg/chromium/content_public/browser/LoadUrlParams;-><init>(Ljava/lang/String;)V

    .line 581
    iget-object v1, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper$8;->this$0:Lcom/google/android/apps/chrome/tab/TransitionPageHelper;

    # getter for: Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mTransitionContentViewCore:Lorg/chromium/content/browser/ContentViewCore;
    invoke-static {v1}, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->access$000(Lcom/google/android/apps/chrome/tab/TransitionPageHelper;)Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v1

    invoke-virtual {v1}, Lorg/chromium/content/browser/ContentViewCore;->getWebContents()Lorg/chromium/content_public/browser/WebContents;

    move-result-object v1

    invoke-interface {v1}, Lorg/chromium/content_public/browser/WebContents;->getNavigationController()Lorg/chromium/content_public/browser/NavigationController;

    move-result-object v1

    invoke-interface {v1, v0}, Lorg/chromium/content_public/browser/NavigationController;->loadUrl(Lorg/chromium/content_public/browser/LoadUrlParams;)V

    .line 583
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper$8;->this$0:Lcom/google/android/apps/chrome/tab/TransitionPageHelper;

    # getter for: Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mTransitionContentViewCore:Lorg/chromium/content/browser/ContentViewCore;
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->access$000(Lcom/google/android/apps/chrome/tab/TransitionPageHelper;)Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    invoke-virtual {v0, v2}, Lorg/chromium/content/browser/ContentViewCore;->setBackgroundOpaque(Z)V

    .line 584
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper$8;->this$0:Lcom/google/android/apps/chrome/tab/TransitionPageHelper;

    # getter for: Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mTransitionContentViewCore:Lorg/chromium/content/browser/ContentViewCore;
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->access$000(Lcom/google/android/apps/chrome/tab/TransitionPageHelper;)Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/chromium/content/browser/ContentViewCore;->setupTransitionView(Ljava/lang/String;)V

    .line 585
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper$8;->this$0:Lcom/google/android/apps/chrome/tab/TransitionPageHelper;

    # getter for: Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mTransitionContentViewCore:Lorg/chromium/content/browser/ContentViewCore;
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->access$000(Lcom/google/android/apps/chrome/tab/TransitionPageHelper;)Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->onShow()V

    .line 587
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper$8;->this$0:Lcom/google/android/apps/chrome/tab/TransitionPageHelper;

    const/4 v1, 0x0

    # invokes: Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->setTransitionOpacity(F)V
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->access$2000(Lcom/google/android/apps/chrome/tab/TransitionPageHelper;F)V

    .line 588
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper$8;->this$0:Lcom/google/android/apps/chrome/tab/TransitionPageHelper;

    # invokes: Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->setTransitionVisibility(Z)V
    invoke-static {v0, v2}, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->access$2100(Lcom/google/android/apps/chrome/tab/TransitionPageHelper;Z)V

    .line 590
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper$8;->this$0:Lcom/google/android/apps/chrome/tab/TransitionPageHelper;

    # setter for: Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mTransitionColor:Ljava/lang/String;
    invoke-static {v0, p3}, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->access$602(Lcom/google/android/apps/chrome/tab/TransitionPageHelper;Ljava/lang/String;)Ljava/lang/String;

    .line 591
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper$8;->this$0:Lcom/google/android/apps/chrome/tab/TransitionPageHelper;

    # setter for: Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mCSSSelector:Ljava/lang/String;
    invoke-static {v0, p2}, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->access$2202(Lcom/google/android/apps/chrome/tab/TransitionPageHelper;Ljava/lang/String;)Ljava/lang/String;

    .line 593
    :cond_0
    return-void
.end method

.method public didStartNavigationTransitionForFrame(J)V
    .locals 1

    .prologue
    .line 597
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper$8;->this$0:Lcom/google/android/apps/chrome/tab/TransitionPageHelper;

    # invokes: Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->startTransitionNavigation()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->access$2300(Lcom/google/android/apps/chrome/tab/TransitionPageHelper;)V

    .line 598
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper$8;->this$0:Lcom/google/android/apps/chrome/tab/TransitionPageHelper;

    # setter for: Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mDestinationMainFrameID:J
    invoke-static {v0, p1, p2}, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->access$1502(Lcom/google/android/apps/chrome/tab/TransitionPageHelper;J)J

    .line 599
    return-void
.end method

.method public willHandleDeferAfterResponseStarted()Z
    .locals 1

    .prologue
    .line 603
    const/4 v0, 0x1

    return v0
.end method
