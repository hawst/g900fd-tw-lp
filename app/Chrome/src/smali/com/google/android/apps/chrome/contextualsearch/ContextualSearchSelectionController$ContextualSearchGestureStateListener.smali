.class Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController$ContextualSearchGestureStateListener;
.super Lorg/chromium/content_public/browser/GestureStateListener;
.source "ContextualSearchSelectionController.java"


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController;)V
    .locals 0

    .prologue
    .line 38
    iput-object p1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController$ContextualSearchGestureStateListener;->this$0:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController;

    invoke-direct {p0}, Lorg/chromium/content_public/browser/GestureStateListener;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController$1;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController$ContextualSearchGestureStateListener;-><init>(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController;)V

    return-void
.end method


# virtual methods
.method public onScrollStarted(II)V
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController$ContextualSearchGestureStateListener;->this$0:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController;

    # getter for: Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController;->mHandler:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionHandler;
    invoke-static {v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController;->access$000(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController;)Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionHandler;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionHandler;->handleScroll()V

    .line 42
    return-void
.end method

.method public onSingleTap(ZII)V
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController$ContextualSearchGestureStateListener;->this$0:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController;

    # invokes: Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController;->handleSingleTap(ZII)V
    invoke-static {v0, p1, p2, p3}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController;->access$100(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController;ZII)V

    .line 47
    return-void
.end method
