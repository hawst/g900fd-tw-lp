.class public interface abstract Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManagementDelegate;
.super Ljava/lang/Object;
.source "ContextualSearchManagementDelegate.java"


# virtual methods
.method public abstract getSearchContentViewVerticalScroll()F
.end method

.method public abstract isRunningInCompatibilityMode()Z
.end method

.method public abstract openResolvedSearchUrlInNewTab()V
.end method

.method public abstract preserveBasePageSelectionOnNextLossOfFocus()V
.end method

.method public abstract promoteToTab()V
.end method

.method public abstract resetSearchContentViewScroll()V
.end method

.method public abstract setContextualSearchLayoutDelegate(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchLayoutDelegate;)V
.end method

.method public abstract setSearchContentViewVisibility(Z)V
.end method

.method public abstract updateTopControlsState(IZ)V
.end method
