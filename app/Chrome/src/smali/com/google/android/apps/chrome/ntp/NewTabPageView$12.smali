.class Lcom/google/android/apps/chrome/ntp/NewTabPageView$12;
.super Ljava/lang/Object;
.source "NewTabPageView.java"

# interfaces
.implements Lorg/chromium/chrome/browser/profiles/MostVisitedSites$ThumbnailCallback;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/ntp/NewTabPageView;

.field final synthetic val$isInitialLoad:Z

.field final synthetic val$item:Lcom/google/android/apps/chrome/ntp/MostVisitedItem;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/ntp/NewTabPageView;Lcom/google/android/apps/chrome/ntp/MostVisitedItem;Z)V
    .locals 0

    .prologue
    .line 680
    iput-object p1, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView$12;->this$0:Lcom/google/android/apps/chrome/ntp/NewTabPageView;

    iput-object p2, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView$12;->val$item:Lcom/google/android/apps/chrome/ntp/MostVisitedItem;

    iput-boolean p3, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView$12;->val$isInitialLoad:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMostVisitedURLsThumbnailAvailable(Landroid/graphics/Bitmap;)V
    .locals 2

    .prologue
    .line 683
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView$12;->val$item:Lcom/google/android/apps/chrome/ntp/MostVisitedItem;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/ntp/MostVisitedItem;->setThumbnail(Landroid/graphics/Bitmap;)V

    .line 684
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView$12;->this$0:Lcom/google/android/apps/chrome/ntp/NewTabPageView;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mSnapshotMostVisitedChanged:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->access$902(Lcom/google/android/apps/chrome/ntp/NewTabPageView;Z)Z

    .line 685
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView$12;->val$isInitialLoad:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView$12;->this$0:Lcom/google/android/apps/chrome/ntp/NewTabPageView;

    # invokes: Lcom/google/android/apps/chrome/ntp/NewTabPageView;->loadTaskCompleted()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->access$1000(Lcom/google/android/apps/chrome/ntp/NewTabPageView;)V

    .line 686
    :cond_0
    return-void
.end method
