.class public Lcom/google/android/apps/chrome/compositor/StaticTabLayerContainer;
.super Ljava/lang/Object;
.source "StaticTabLayerContainer.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private mNativeStaticTabLayerContainer:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const-class v0, Lcom/google/android/apps/chrome/compositor/StaticTabLayerContainer;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/compositor/StaticTabLayerContainer;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(J)V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-wide p1, p0, Lcom/google/android/apps/chrome/compositor/StaticTabLayerContainer;->mNativeStaticTabLayerContainer:J

    .line 25
    return-void
.end method

.method private static create(J)Lcom/google/android/apps/chrome/compositor/StaticTabLayerContainer;
    .locals 2

    .prologue
    .line 56
    new-instance v0, Lcom/google/android/apps/chrome/compositor/StaticTabLayerContainer;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/chrome/compositor/StaticTabLayerContainer;-><init>(J)V

    return-object v0
.end method

.method private destroy()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 61
    sget-boolean v0, Lcom/google/android/apps/chrome/compositor/StaticTabLayerContainer;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/apps/chrome/compositor/StaticTabLayerContainer;->mNativeStaticTabLayerContainer:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 62
    :cond_0
    iput-wide v2, p0, Lcom/google/android/apps/chrome/compositor/StaticTabLayerContainer;->mNativeStaticTabLayerContainer:J

    .line 63
    return-void
.end method

.method private native nativeSetStaticLayer(JIIZZIFFFFFFF)V
.end method


# virtual methods
.method public pushLayers(Landroid/content/Context;Lcom/google/android/apps/chrome/tabs/layout/Layout;F)V
    .locals 18

    .prologue
    .line 37
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/google/android/apps/chrome/compositor/StaticTabLayerContainer;->mNativeStaticTabLayerContainer:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    .line 52
    :goto_0
    return-void

    .line 39
    :cond_0
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->density:F

    .line 41
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->getLayoutTabsToRender()[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v3

    .line 42
    sget-boolean v4, Lcom/google/android/apps/chrome/compositor/StaticTabLayerContainer;->$assertionsDisabled:Z

    if-nez v4, :cond_2

    if-eqz v3, :cond_1

    array-length v4, v3

    const/4 v5, 0x1

    if-eq v4, v5, :cond_2

    :cond_1
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 43
    :cond_2
    const/4 v4, 0x0

    aget-object v3, v3, v4

    .line 46
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/google/android/apps/chrome/compositor/StaticTabLayerContainer;->mNativeStaticTabLayerContainer:J

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getId()I

    move-result v6

    sget v7, Lcom/google/android/apps/chrome/R$id;->control_container:I

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->canUseLiveTexture()Z

    move-result v8

    const/4 v9, 0x0

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getBackgroundColor()I

    move-result v10

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getRenderX()F

    move-result v11

    mul-float/2addr v11, v2

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getRenderY()F

    move-result v12

    mul-float/2addr v12, v2

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getScaledContentWidth()F

    move-result v13

    mul-float/2addr v13, v2

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getScaledContentHeight()F

    move-result v14

    mul-float/2addr v14, v2

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getStaticToViewBlend()F

    move-result v16

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getSaturation()F

    move-result v17

    move-object/from16 v3, p0

    move/from16 v15, p3

    invoke-direct/range {v3 .. v17}, Lcom/google/android/apps/chrome/compositor/StaticTabLayerContainer;->nativeSetStaticLayer(JIIZZIFFFFFFF)V

    goto :goto_0
.end method
