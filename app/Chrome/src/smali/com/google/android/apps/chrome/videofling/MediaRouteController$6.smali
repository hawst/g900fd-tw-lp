.class Lcom/google/android/apps/chrome/videofling/MediaRouteController$6;
.super Ljava/lang/Object;
.source "MediaRouteController.java"

# interfaces
.implements Lcom/google/android/apps/chrome/videofling/MediaRouteController$ResultBundleHandler;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/videofling/MediaRouteController;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/videofling/MediaRouteController;)V
    .locals 0

    .prologue
    .line 589
    iput-object p1, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController$6;->this$0:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 598
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController$6;->this$0:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->release()V

    .line 599
    invoke-static {}, Lorg/chromium/base/library_loader/LibraryLoader;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-static {v0}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->castDefaultPlayerResult(Z)V

    .line 600
    :cond_0
    return-void
.end method

.method public onResult(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 592
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController$6;->this$0:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    const-string/jumbo v1, "android.media.intent.extra.ITEM_ID"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mCurrentItemId:Ljava/lang/String;

    .line 593
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController$6;->this$0:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->processMediaStatusBundle(Landroid/os/Bundle;)V

    .line 594
    return-void
.end method
