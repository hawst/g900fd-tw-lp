.class Lcom/google/android/apps/chrome/preferences/website/Website$1;
.super Ljava/lang/Object;
.source "Website.java"

# interfaces
.implements Lcom/google/android/apps/chrome/preferences/website/WebsitePreferenceBridge$StorageInfoClearedCallback;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/preferences/website/Website;

.field final synthetic val$callback:Lcom/google/android/apps/chrome/preferences/website/Website$StoredDataClearedCallback;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/preferences/website/Website;Lcom/google/android/apps/chrome/preferences/website/Website$StoredDataClearedCallback;)V
    .locals 0

    .prologue
    .line 230
    iput-object p1, p0, Lcom/google/android/apps/chrome/preferences/website/Website$1;->this$0:Lcom/google/android/apps/chrome/preferences/website/Website;

    iput-object p2, p0, Lcom/google/android/apps/chrome/preferences/website/Website$1;->val$callback:Lcom/google/android/apps/chrome/preferences/website/Website$StoredDataClearedCallback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onStorageInfoCleared()V
    .locals 1

    .prologue
    .line 233
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/Website$1;->this$0:Lcom/google/android/apps/chrome/preferences/website/Website;

    # --operator for: Lcom/google/android/apps/chrome/preferences/website/Website;->mStorageInfoCallbacksLeft:I
    invoke-static {v0}, Lcom/google/android/apps/chrome/preferences/website/Website;->access$006(Lcom/google/android/apps/chrome/preferences/website/Website;)I

    move-result v0

    if-nez v0, :cond_0

    .line 234
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/Website$1;->val$callback:Lcom/google/android/apps/chrome/preferences/website/Website$StoredDataClearedCallback;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/preferences/website/Website$StoredDataClearedCallback;->onStoredDataCleared()V

    .line 235
    :cond_0
    return-void
.end method
