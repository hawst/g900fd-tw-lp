.class Lcom/google/android/apps/chrome/omnibox/VoiceSuggestionProvider;
.super Ljava/lang/Object;
.source "VoiceSuggestionProvider.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final mConfidenceThresholdHideAlts:F

.field private final mConfidenceThresholdShow:F

.field private final mResults:Ljava/util/List;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const-class v0, Lcom/google/android/apps/chrome/omnibox/VoiceSuggestionProvider;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/omnibox/VoiceSuggestionProvider;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/omnibox/VoiceSuggestionProvider;->mResults:Ljava/util/List;

    .line 36
    const v0, 0x3e99999a    # 0.3f

    iput v0, p0, Lcom/google/android/apps/chrome/omnibox/VoiceSuggestionProvider;->mConfidenceThresholdShow:F

    .line 37
    const v0, 0x3f4ccccd    # 0.8f

    iput v0, p0, Lcom/google/android/apps/chrome/omnibox/VoiceSuggestionProvider;->mConfidenceThresholdHideAlts:F

    .line 38
    return-void
.end method

.method protected constructor <init>(FF)V
    .locals 1

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/omnibox/VoiceSuggestionProvider;->mResults:Ljava/util/List;

    .line 52
    iput p1, p0, Lcom/google/android/apps/chrome/omnibox/VoiceSuggestionProvider;->mConfidenceThresholdShow:F

    .line 53
    iput p2, p0, Lcom/google/android/apps/chrome/omnibox/VoiceSuggestionProvider;->mConfidenceThresholdHideAlts:F

    .line 54
    return-void
.end method

.method private addVoiceResultToOmniboxSuggestions(Ljava/util/List;Lcom/google/android/apps/chrome/omnibox/VoiceSuggestionProvider$VoiceResult;F)V
    .locals 13

    .prologue
    .line 141
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/omnibox/VoiceSuggestionProvider;->doesVoiceResultHaveMatch(Ljava/util/List;Lcom/google/android/apps/chrome/omnibox/VoiceSuggestionProvider$VoiceResult;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 158
    :cond_0
    :goto_0
    return-void

    .line 142
    :cond_1
    invoke-virtual {p2}, Lcom/google/android/apps/chrome/omnibox/VoiceSuggestionProvider$VoiceResult;->getConfidence()F

    move-result v0

    cmpg-float v0, v0, p3

    if-gez v0, :cond_2

    invoke-virtual {p2}, Lcom/google/android/apps/chrome/omnibox/VoiceSuggestionProvider$VoiceResult;->getConfidence()F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-gtz v0, :cond_0

    .line 143
    :cond_2
    invoke-static {}, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;->getInstance()Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;

    move-result-object v0

    invoke-virtual {p2}, Lcom/google/android/apps/chrome/omnibox/VoiceSuggestionProvider$VoiceResult;->getMatch()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;->getUrlForVoiceSearchQuery(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 145
    new-instance v0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;

    sget-object v1, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;->VOICE_SUGGEST:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

    invoke-virtual {v1}, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;->nativeType()I

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-virtual {p2}, Lcom/google/android/apps/chrome/omnibox/VoiceSuggestionProvider$VoiceResult;->getMatch()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    move-object v10, v9

    invoke-direct/range {v0 .. v12}, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;-><init>(IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private doesVoiceResultHaveMatch(Ljava/util/List;Lcom/google/android/apps/chrome/omnibox/VoiceSuggestionProvider$VoiceResult;)Z
    .locals 3

    .prologue
    .line 162
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;

    .line 163
    invoke-virtual {v0}, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;->getDisplayText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Lcom/google/android/apps/chrome/omnibox/VoiceSuggestionProvider$VoiceResult;->getMatch()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 166
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public addVoiceSuggestions(Ljava/util/List;I)Ljava/util/List;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 117
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/VoiceSuggestionProvider;->mResults:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-eqz v0, :cond_0

    if-nez p2, :cond_1

    .line 136
    :cond_0
    :goto_0
    return-object p1

    .line 119
    :cond_1
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 120
    if-eqz p1, :cond_2

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 121
    invoke-interface {v3, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 124
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/VoiceSuggestionProvider;->mResults:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/omnibox/VoiceSuggestionProvider$VoiceResult;

    .line 125
    const/4 v2, 0x0

    invoke-direct {p0, v3, v0, v2}, Lcom/google/android/apps/chrome/omnibox/VoiceSuggestionProvider;->addVoiceResultToOmniboxSuggestions(Ljava/util/List;Lcom/google/android/apps/chrome/omnibox/VoiceSuggestionProvider$VoiceResult;F)V

    .line 127
    if-eqz p1, :cond_3

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    .line 128
    :cond_3
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omnibox/VoiceSuggestionProvider$VoiceResult;->getConfidence()F

    move-result v0

    iget v2, p0, Lcom/google/android/apps/chrome/omnibox/VoiceSuggestionProvider;->mConfidenceThresholdHideAlts:F

    cmpg-float v0, v0, v2

    if-gez v0, :cond_4

    .line 129
    const/4 v0, 0x1

    move v2, v0

    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/VoiceSuggestionProvider;->mResults:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_4

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    add-int v4, v1, p2

    if-ge v0, v4, :cond_4

    .line 131
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/VoiceSuggestionProvider;->mResults:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/omnibox/VoiceSuggestionProvider$VoiceResult;

    iget v4, p0, Lcom/google/android/apps/chrome/omnibox/VoiceSuggestionProvider;->mConfidenceThresholdShow:F

    invoke-direct {p0, v3, v0, v4}, Lcom/google/android/apps/chrome/omnibox/VoiceSuggestionProvider;->addVoiceResultToOmniboxSuggestions(Ljava/util/List;Lcom/google/android/apps/chrome/omnibox/VoiceSuggestionProvider$VoiceResult;F)V

    .line 130
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_4
    move-object p1, v3

    .line 136
    goto :goto_0
.end method

.method public clearVoiceSearchResults()V
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/VoiceSuggestionProvider;->mResults:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 61
    return-void
.end method

.method public getResults()Ljava/util/List;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/VoiceSuggestionProvider;->mResults:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public setVoiceResultsFromIntentBundle(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    .line 80
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/VoiceSuggestionProvider;->clearVoiceSearchResults()V

    .line 82
    if-nez p1, :cond_1

    .line 106
    :cond_0
    return-void

    .line 84
    :cond_1
    const-string/jumbo v0, "android.speech.extra.RESULTS"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 86
    const-string/jumbo v0, "android.speech.extra.CONFIDENCE_SCORES"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getFloatArray(Ljava/lang/String;)[F

    move-result-object v3

    .line 89
    if-eqz v2, :cond_0

    if-eqz v3, :cond_0

    .line 91
    sget-boolean v0, Lcom/google/android/apps/chrome/omnibox/VoiceSuggestionProvider;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    array-length v1, v3

    if-eq v0, v1, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 92
    :cond_2
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    array-length v1, v3

    if-ne v0, v1, :cond_0

    .line 94
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 101
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string/jumbo v4, " "

    const-string/jumbo v5, ""

    invoke-virtual {v0, v4, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 102
    invoke-static {v0}, Lorg/chromium/chrome/browser/omnibox/AutocompleteController;->nativeQualifyPartialURLQuery(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 103
    iget-object v5, p0, Lcom/google/android/apps/chrome/omnibox/VoiceSuggestionProvider;->mResults:Ljava/util/List;

    new-instance v6, Lcom/google/android/apps/chrome/omnibox/VoiceSuggestionProvider$VoiceResult;

    if-nez v4, :cond_3

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    :cond_3
    aget v4, v3, v1

    invoke-direct {v6, v0, v4}, Lcom/google/android/apps/chrome/omnibox/VoiceSuggestionProvider$VoiceResult;-><init>(Ljava/lang/String;F)V

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 94
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method
