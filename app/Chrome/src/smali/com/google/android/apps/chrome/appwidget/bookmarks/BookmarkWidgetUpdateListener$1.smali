.class Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener$1;
.super Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;
.source "BookmarkWidgetUpdateListener.java"


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener;)V
    .locals 0

    .prologue
    .line 85
    iput-object p1, p0, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener$1;->this$0:Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2

    .prologue
    .line 88
    iget v0, p1, Landroid/os/Message;->what:I

    const/16 v1, 0x3d

    if-eq v0, v1, :cond_1

    .line 89
    const-string/jumbo v0, "BookmarkWidgetUpdateListener"

    const-string/jumbo v1, "Unexpected notification received."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 96
    :cond_0
    :goto_0
    return-void

    .line 93
    :cond_1
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "url"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 94
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_0

    .line 95
    iget-object v1, p0, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener$1;->this$0:Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener;

    # getter for: Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener;->mListener:Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener$UpdateListener;
    invoke-static {v1}, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener;->access$000(Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener;)Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener$UpdateListener;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener$1;->this$0:Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener;

    # getter for: Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener;->mListener:Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener$UpdateListener;
    invoke-static {v1}, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener;->access$000(Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener;)Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener$UpdateListener;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener$UpdateListener;->onThumbnailUpdated(Ljava/lang/String;)V

    goto :goto_0
.end method
