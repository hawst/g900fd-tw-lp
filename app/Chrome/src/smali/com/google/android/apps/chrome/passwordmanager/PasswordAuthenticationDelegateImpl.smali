.class public Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl;
.super Ljava/lang/Object;
.source "PasswordAuthenticationDelegateImpl.java"

# interfaces
.implements Lorg/chromium/base/ApplicationStatus$ApplicationStateListener;
.implements Lorg/chromium/chrome/browser/password_manager/PasswordAuthenticationManager$PasswordAuthenticationDelegate;


# static fields
.field private static final NOTIFICATIONS:[I


# instance fields
.field private final mApplicationContext:Landroid/content/Context;

.field private mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

.field private mOverviewModePerActivityMap:Landroid/util/SparseBooleanArray;

.field private mOverviewModeShown:Z

.field private final mPendingTabCallbacks:Lorg/chromium/base/ObserverList;

.field private mRegisteredForNotifications:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 50
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl;->NOTIFICATIONS:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x0
        0x1
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 94
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    new-instance v0, Landroid/util/SparseBooleanArray;

    invoke-direct {v0}, Landroid/util/SparseBooleanArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl;->mOverviewModePerActivityMap:Landroid/util/SparseBooleanArray;

    .line 62
    new-instance v0, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl$1;-><init>(Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    .line 95
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl;->mApplicationContext:Landroid/content/Context;

    .line 97
    sget-object v0, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl;->NOTIFICATIONS:[I

    iget-object v1, p0, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->registerForNotifications([ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    .line 98
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl;->mRegisteredForNotifications:Z

    .line 100
    new-instance v0, Lorg/chromium/base/ObserverList;

    invoke-direct {v0}, Lorg/chromium/base/ObserverList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl;->mPendingTabCallbacks:Lorg/chromium/base/ObserverList;

    .line 101
    invoke-static {p0}, Lorg/chromium/base/ApplicationStatus;->registerApplicationStateListener(Lorg/chromium/base/ApplicationStatus$ApplicationStateListener;)V

    .line 102
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl;)Z
    .locals 1

    .prologue
    .line 42
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl;->mOverviewModeShown:Z

    return v0
.end method

.method static synthetic access$002(Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl;Z)Z
    .locals 0

    .prologue
    .line 42
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl;->mOverviewModeShown:Z

    return p1
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl;)Landroid/util/SparseBooleanArray;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl;->mOverviewModePerActivityMap:Landroid/util/SparseBooleanArray;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl;)Lorg/chromium/base/ObserverList;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl;->mPendingTabCallbacks:Lorg/chromium/base/ObserverList;

    return-object v0
.end method

.method static synthetic access$300(Landroid/content/Context;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 42
    invoke-static {p0}, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl;->buildAuthenticationIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private static final buildAuthenticationIntent(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 105
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 106
    invoke-static {p0}, Lcom/google/android/apps/chrome/deviceextras/DeviceSupportHelper;->getSupportPackageName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 107
    const-string/jumbo v1, "com.chrome.deviceextras.AUTHENTICATE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 108
    return-object v0
.end method


# virtual methods
.method public getPasswordProtectionString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl;->mApplicationContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/apps/chrome/passwordmanager/SamsungFingerprintAuthenticationHelper;->getFingerprintPasswordProtectionString(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isPasswordAuthenticationEnabled()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 113
    iget-object v1, p0, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl;->mApplicationContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/apps/chrome/deviceextras/DeviceSupportHelper;->isSupportPackageAvailable(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 122
    :cond_0
    :goto_0
    return v0

    .line 115
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl;->mApplicationContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 116
    iget-object v2, p0, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl;->mApplicationContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl;->buildAuthenticationIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v2

    const/high16 v3, 0x10000

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 122
    iget-object v0, p0, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl;->mApplicationContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/apps/chrome/passwordmanager/SamsungFingerprintAuthenticationHelper;->isEnabled(Landroid/content/Context;)Z

    move-result v0

    goto :goto_0
.end method

.method public onApplicationStateChange(I)V
    .locals 7

    .prologue
    const/4 v3, 0x6

    const/4 v6, 0x1

    .line 158
    if-ne p1, v6, :cond_0

    .line 159
    iget-object v0, p0, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl;->mPendingTabCallbacks:Lorg/chromium/base/ObserverList;

    invoke-virtual {v0}, Lorg/chromium/base/ObserverList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl$TabCallbackHandler;

    .line 160
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl$TabCallbackHandler;->triggerAuthenticationIfApplicable()V

    goto :goto_0

    .line 166
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl;->mPendingTabCallbacks:Lorg/chromium/base/ObserverList;

    invoke-virtual {v0}, Lorg/chromium/base/ObserverList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl$TabCallbackHandler;

    .line 167
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl$TabCallbackHandler;->getTab()Lorg/chromium/chrome/browser/Tab;

    move-result-object v5

    .line 168
    invoke-virtual {v5}, Lorg/chromium/chrome/browser/Tab;->getWindowAndroid()Lorg/chromium/ui/base/WindowAndroid;

    move-result-object v1

    invoke-virtual {v1}, Lorg/chromium/ui/base/WindowAndroid;->getActivity()Ljava/lang/ref/WeakReference;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/Activity;

    .line 171
    if-eqz v1, :cond_5

    invoke-static {v1}, Lorg/chromium/base/ApplicationStatus;->getStateForActivity(Landroid/app/Activity;)I

    move-result v2

    .line 173
    :goto_2
    if-ne v2, v3, :cond_1

    .line 174
    invoke-virtual {v0, v5}, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl$TabCallbackHandler;->onDestroyed(Lorg/chromium/chrome/browser/Tab;)V

    .line 175
    if-eqz v1, :cond_1

    .line 176
    iget-object v2, p0, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl;->mOverviewModePerActivityMap:Landroid/util/SparseBooleanArray;

    monitor-enter v2

    .line 177
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl;->mOverviewModePerActivityMap:Landroid/util/SparseBooleanArray;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/util/SparseBooleanArray;->delete(I)V

    .line 178
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    .line 182
    :cond_2
    const/4 v0, 0x4

    if-ne p1, v0, :cond_4

    .line 183
    sget-object v0, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl;->NOTIFICATIONS:[I

    iget-object v1, p0, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->unregisterForNotifications([ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    .line 185
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl;->mRegisteredForNotifications:Z

    .line 186
    iget-object v1, p0, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl;->mOverviewModePerActivityMap:Landroid/util/SparseBooleanArray;

    monitor-enter v1

    .line 187
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl;->mOverviewModePerActivityMap:Landroid/util/SparseBooleanArray;

    invoke-virtual {v0}, Landroid/util/SparseBooleanArray;->clear()V

    .line 188
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 193
    :cond_3
    :goto_3
    return-void

    .line 188
    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0

    .line 189
    :cond_4
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl;->mRegisteredForNotifications:Z

    if-nez v0, :cond_3

    .line 190
    sget-object v0, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl;->NOTIFICATIONS:[I

    iget-object v1, p0, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->registerForNotifications([ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    .line 191
    iput-boolean v6, p0, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl;->mRegisteredForNotifications:Z

    goto :goto_3

    :cond_5
    move v2, v3

    goto :goto_2
.end method

.method public requestAuthentication(Lorg/chromium/chrome/browser/Tab;Lorg/chromium/chrome/browser/password_manager/PasswordAuthenticationManager$PasswordAuthenticationCallback;)V
    .locals 4

    .prologue
    .line 127
    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    .line 129
    iget-object v0, p0, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl;->mApplicationContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/apps/chrome/passwordmanager/SamsungFingerprintAuthenticationHelper;->isSupported(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl;->mApplicationContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/apps/chrome/passwordmanager/SamsungFingerprintAuthenticationHelper;->isEnabled(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 131
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Lorg/chromium/chrome/browser/password_manager/PasswordAuthenticationManager$PasswordAuthenticationCallback;->onResult(Z)V

    .line 148
    :goto_0
    return-void

    .line 135
    :cond_0
    const/4 v1, 0x0

    .line 136
    iget-object v0, p0, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl;->mPendingTabCallbacks:Lorg/chromium/base/ObserverList;

    invoke-virtual {v0}, Lorg/chromium/base/ObserverList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl$TabCallbackHandler;

    .line 137
    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl$TabCallbackHandler;->isForTab(Lorg/chromium/chrome/browser/Tab;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 138
    invoke-virtual {v0, p2}, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl$TabCallbackHandler;->addCallback(Lorg/chromium/chrome/browser/password_manager/PasswordAuthenticationManager$PasswordAuthenticationCallback;)V

    .line 143
    :goto_1
    if-nez v0, :cond_2

    .line 144
    new-instance v0, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl$TabCallbackHandler;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl$TabCallbackHandler;-><init>(Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl;Lorg/chromium/chrome/browser/Tab;Lorg/chromium/chrome/browser/password_manager/PasswordAuthenticationManager$PasswordAuthenticationCallback;)V

    .line 145
    iget-object v1, p0, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl;->mPendingTabCallbacks:Lorg/chromium/base/ObserverList;

    invoke-virtual {v1, v0}, Lorg/chromium/base/ObserverList;->addObserver(Ljava/lang/Object;)Z

    .line 147
    :cond_2
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl$TabCallbackHandler;->triggerAuthenticationIfApplicable()V

    goto :goto_0

    :cond_3
    move-object v0, v1

    goto :goto_1
.end method
