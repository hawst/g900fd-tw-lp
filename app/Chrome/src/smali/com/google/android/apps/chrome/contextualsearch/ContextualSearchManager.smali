.class public Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;
.super Ljava/lang/Object;
.source "ContextualSearchManager.java"

# interfaces
.implements Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManagementDelegate;
.implements Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchNetworkCommunicator;
.implements Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionHandler;
.implements Lorg/chromium/base/ApplicationStatus$ActivityStateListener;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final HIDE_NOTIFICATIONS:[I


# instance fields
.field private final mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

.field private final mContainsWordPattern:Ljava/util/regex/Pattern;

.field private mControl:Lcom/google/android/apps/chrome/widget/ContextualSearchControl;

.field private mDidLoadResolvedSearchRequest:Z

.field private mDidLogFirstRunFlowOutcome:Z

.field private mEdgeSwipeHandler:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;

.field private final mFreSearchBarSlideOffsetDp:F

.field private mHideNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

.field private mIsContextualSearchPanelShowing:Z

.field private mIsSearchContentViewShowing:Z

.field private mIsShowingFirstRunFlow:Z

.field private mLayoutDelegate:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchLayoutDelegate;

.field private mLoadedSearchUrlTimeMs:J

.field private mNativeContextualSearchManagerPtr:J

.field private mNetworkCommunicator:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchNetworkCommunicator;

.field private mParentView:Landroid/view/ViewGroup;

.field private mResourceLoader:Lcom/google/android/apps/chrome/compositor/resources/dynamics/DynamicResourceLoader;

.field private mSearchContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

.field private mSearchContentViewDelegate:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager$ContextualSearchContentViewDelegate;

.field private mSearchRequest:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchRequest;

.field private mSearchWebContentsObserver:Lorg/chromium/content/browser/WebContentsObserverAndroid;

.field private mSelectionController:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController;

.field private final mTabPromotionDelegate:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager$ContextualSearchTabPromotionDelegate;

.field private mTabRedirectHandler:Lcom/google/android/apps/chrome/tab/TabRedirectHandler;

.field private final mWebContentsDelegate:Lorg/chromium/components/web_contents_delegate_android/WebContentsDelegateAndroid;

.field private mWereInfoBarsHidden:Z

.field private mWereSearchResultsSeen:Z

.field private final mWindowAndroid:Lorg/chromium/ui/base/WindowAndroid;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 66
    const-class v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->$assertionsDisabled:Z

    .line 73
    const/16 v0, 0xa

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->HIDE_NOTIFICATIONS:[I

    return-void

    .line 66
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 73
    nop

    :array_0
    .array-data 4
        0x0
        0x8
        0x2
        0x6
        0x24
        0x2a
        0x2c
        0x43
        0x47
        0x4c
    .end array-data
.end method

.method public constructor <init>(Lcom/google/android/apps/chrome/ChromeActivity;Lorg/chromium/ui/base/WindowAndroid;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager$ContextualSearchTabPromotionDelegate;)V
    .locals 2

    .prologue
    .line 195
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 196
    iput-object p1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    .line 197
    iput-object p2, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mWindowAndroid:Lorg/chromium/ui/base/WindowAndroid;

    .line 198
    iput-object p3, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mTabPromotionDelegate:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager$ContextualSearchTabPromotionDelegate;

    .line 199
    const-string/jumbo v0, "(\\w|\\p{L}|\\p{N})+"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mContainsWordPattern:Ljava/util/regex/Pattern;

    .line 201
    new-instance v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController;

    invoke-direct {v0, p1, p0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController;-><init>(Lcom/google/android/apps/chrome/ChromeActivity;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionHandler;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mSelectionController:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController;

    .line 203
    new-instance v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager$1;-><init>(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mWebContentsDelegate:Lorg/chromium/components/web_contents_delegate_android/WebContentsDelegateAndroid;

    .line 233
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$dimen;->contextual_search_bar_height:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mFreSearchBarSlideOffsetDp:F

    .line 235
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;)Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchLayoutDelegate;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mLayoutDelegate:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchLayoutDelegate;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 66
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->onFirstRunNavigation(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;)Lcom/google/android/apps/chrome/ChromeActivity;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;)Lcom/google/android/apps/chrome/tab/TabRedirectHandler;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mTabRedirectHandler:Lcom/google/android/apps/chrome/tab/TabRedirectHandler;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;)Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchRequest;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mSearchRequest:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchRequest;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;)Lorg/chromium/content/browser/ContentViewCore;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mSearchContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;)Z
    .locals 1

    .prologue
    .line 66
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mIsSearchContentViewShowing:Z

    return v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 66
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->loadSearchUrl(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$602(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;Z)Z
    .locals 0

    .prologue
    .line 66
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mDidLoadResolvedSearchRequest:Z

    return p1
.end method

.method static synthetic access$700(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;)V
    .locals 0

    .prologue
    .line 66
    invoke-direct {p0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->extractFirstRunFlowContentHeight()V

    return-void
.end method

.method static synthetic access$800(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;)Z
    .locals 1

    .prologue
    .line 66
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mIsShowingFirstRunFlow:Z

    return v0
.end method

.method static synthetic access$802(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;Z)Z
    .locals 0

    .prologue
    .line 66
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mIsShowingFirstRunFlow:Z

    return p1
.end method

.method private createNewSearchContentView()V
    .locals 7

    .prologue
    .line 826
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mSearchContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    if-eqz v0, :cond_0

    .line 827
    invoke-direct {p0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->destroySearchContentView()V

    .line 830
    :cond_0
    new-instance v0, Lorg/chromium/content/browser/ContentViewCore;

    iget-object v1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    invoke-direct {v0, v1}, Lorg/chromium/content/browser/ContentViewCore;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mSearchContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    .line 831
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    iget-object v1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mSearchContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    invoke-static {v0, v1}, Lorg/chromium/content/browser/ContentView;->newInstance(Landroid/content/Context;Lorg/chromium/content/browser/ContentViewCore;)Lorg/chromium/content/browser/ContentView;

    move-result-object v2

    .line 833
    iget-object v1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mSearchContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    const/4 v0, 0x0

    const/4 v3, 0x1

    invoke-static {v0, v3}, Lorg/chromium/chrome/browser/ContentViewUtil;->createNativeWebContents(ZZ)J

    move-result-wide v4

    iget-object v6, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mWindowAndroid:Lorg/chromium/ui/base/WindowAndroid;

    move-object v3, v2

    invoke-virtual/range {v1 .. v6}, Lorg/chromium/content/browser/ContentViewCore;->initialize(Landroid/view/ViewGroup;Lorg/chromium/content/browser/ContentViewCore$InternalAccessDelegate;JLorg/chromium/ui/base/WindowAndroid;)V

    .line 837
    iget-wide v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mNativeContextualSearchManagerPtr:J

    iget-object v2, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mSearchContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    iget-object v3, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mWebContentsDelegate:Lorg/chromium/components/web_contents_delegate_android/WebContentsDelegateAndroid;

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->nativeSetWebContents(JLorg/chromium/content/browser/ContentViewCore;Lorg/chromium/components/web_contents_delegate_android/WebContentsDelegateAndroid;)V

    .line 840
    new-instance v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager$3;

    iget-object v1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mSearchContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    invoke-virtual {v1}, Lorg/chromium/content/browser/ContentViewCore;->getWebContents()Lorg/chromium/content_public/browser/WebContents;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager$3;-><init>(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;Lorg/chromium/content_public/browser/WebContents;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mSearchWebContentsObserver:Lorg/chromium/content/browser/WebContentsObserverAndroid;

    .line 902
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mSearchContentViewDelegate:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager$ContextualSearchContentViewDelegate;

    iget-object v1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mSearchContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    invoke-interface {v0, v1}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager$ContextualSearchContentViewDelegate;->setContextualSearchContentViewCore(Lorg/chromium/content/browser/ContentViewCore;)V

    .line 903
    iget-wide v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mNativeContextualSearchManagerPtr:J

    new-instance v2, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager$InterceptNavigationDelegateImpl;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager$InterceptNavigationDelegateImpl;-><init>(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager$1;)V

    iget-object v3, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mSearchContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    invoke-virtual {v3}, Lorg/chromium/content/browser/ContentViewCore;->getWebContents()Lorg/chromium/content_public/browser/WebContents;

    move-result-object v3

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->nativeSetInterceptNavigationDelegate(JLorg/chromium/components/navigation_interception/InterceptNavigationDelegate;Lorg/chromium/content_public/browser/WebContents;)V

    .line 905
    return-void
.end method

.method private destroySearchContentView()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 929
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mSearchContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mSearchContentViewDelegate:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager$ContextualSearchContentViewDelegate;

    if-eqz v0, :cond_1

    .line 930
    iget-wide v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mNativeContextualSearchManagerPtr:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->nativeDestroyWebContents(J)V

    .line 931
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mSearchContentViewDelegate:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager$ContextualSearchContentViewDelegate;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager$ContextualSearchContentViewDelegate;->releaseContextualSearchContentViewCore()V

    .line 932
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mSearchContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->destroy()V

    .line 933
    iput-object v2, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mSearchContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    .line 934
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mSearchWebContentsObserver:Lorg/chromium/content/browser/WebContentsObserverAndroid;

    if-eqz v0, :cond_0

    .line 935
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mSearchWebContentsObserver:Lorg/chromium/content/browser/WebContentsObserverAndroid;

    invoke-virtual {v0}, Lorg/chromium/content/browser/WebContentsObserverAndroid;->detachFromWebContents()V

    .line 936
    iput-object v2, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mSearchWebContentsObserver:Lorg/chromium/content/browser/WebContentsObserverAndroid;

    .line 945
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->setSearchContentViewVisibility(Z)V

    .line 947
    :cond_1
    return-void
.end method

.method private extractFirstRunFlowContentHeight()V
    .locals 3

    .prologue
    .line 911
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mSearchContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->getWebContents()Lorg/chromium/content_public/browser/WebContents;

    move-result-object v0

    const-string/jumbo v1, "getContentHeight()"

    new-instance v2, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager$4;

    invoke-direct {v2, p0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager$4;-><init>(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;)V

    invoke-interface {v0, v1, v2}, Lorg/chromium/content_public/browser/WebContents;->evaluateJavaScript(Ljava/lang/String;Lorg/chromium/content_public/browser/JavaScriptCallback;)V

    .line 923
    return-void
.end method

.method private getContextualSearchControl()Lcom/google/android/apps/chrome/widget/ContextualSearchControl;
    .locals 3

    .prologue
    .line 553
    sget-boolean v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mParentView:Landroid/view/ViewGroup;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 554
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mControl:Lcom/google/android/apps/chrome/widget/ContextualSearchControl;

    if-nez v0, :cond_3

    .line 555
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$layout;->contextual_search_view:I

    iget-object v2, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mParentView:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 556
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mParentView:Landroid/view/ViewGroup;

    sget v1, Lcom/google/android/apps/chrome/R$id;->contextual_search_view:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/widget/ContextualSearchControl;

    iput-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mControl:Lcom/google/android/apps/chrome/widget/ContextualSearchControl;

    .line 558
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mControl:Lcom/google/android/apps/chrome/widget/ContextualSearchControl;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/chrome/widget/ContextualSearchControl;->setContextualSearchManagementDelegate(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManagementDelegate;)V

    .line 559
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mResourceLoader:Lcom/google/android/apps/chrome/compositor/resources/dynamics/DynamicResourceLoader;

    if-eqz v0, :cond_1

    .line 560
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mResourceLoader:Lcom/google/android/apps/chrome/compositor/resources/dynamics/DynamicResourceLoader;

    sget v1, Lcom/google/android/apps/chrome/R$id;->contextual_search_bar_text:I

    iget-object v2, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mControl:Lcom/google/android/apps/chrome/widget/ContextualSearchControl;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/widget/ContextualSearchControl;->getResourceAdapter()Lcom/google/android/apps/chrome/compositor/resources/dynamics/ViewResourceAdapter;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/compositor/resources/dynamics/DynamicResourceLoader;->registerResource(ILcom/google/android/apps/chrome/compositor/resources/dynamics/DynamicResource;)V

    .line 563
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mEdgeSwipeHandler:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mControl:Lcom/google/android/apps/chrome/widget/ContextualSearchControl;

    iget-object v1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mEdgeSwipeHandler:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/widget/ContextualSearchControl;->setSwipeHandler(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;)V

    .line 564
    :cond_2
    invoke-direct {p0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->runFadeInAnimation()V

    .line 566
    :cond_3
    sget-boolean v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->$assertionsDisabled:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mControl:Lcom/google/android/apps/chrome/widget/ContextualSearchControl;

    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 567
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mControl:Lcom/google/android/apps/chrome/widget/ContextualSearchControl;

    return-object v0
.end method

.method private getInfoBarContainer()Lorg/chromium/chrome/browser/infobar/InfoBarContainer;
    .locals 1

    .prologue
    .line 525
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeActivity;->getCurrentTab()Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->fromTab(Lorg/chromium/chrome/browser/Tab;)Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    .line 526
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getInfoBarContainer()Lorg/chromium/chrome/browser/infobar/InfoBarContainer;

    move-result-object v0

    goto :goto_0
.end method

.method private isValidSelection(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 392
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->getBaseContentView()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->isValidSelection(Ljava/lang/String;Lorg/chromium/content/browser/ContentViewCore;)Z

    move-result v0

    return v0
.end method

.method private listenForHideNotifications()V
    .locals 2

    .prologue
    .line 592
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mHideNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    if-nez v0, :cond_0

    .line 593
    new-instance v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager$2;-><init>(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mHideNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    .line 599
    sget-object v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->HIDE_NOTIFICATIONS:[I

    iget-object v1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mHideNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->registerForNotifications([ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    .line 602
    :cond_0
    return-void
.end method

.method private loadSearchUrl(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 719
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mSearchContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mSearchContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->getWebContents()Lorg/chromium/content_public/browser/WebContents;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 720
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mLoadedSearchUrlTimeMs:J

    .line 721
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mNetworkCommunicator:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchNetworkCommunicator;

    invoke-interface {v0, p1}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchNetworkCommunicator;->loadUrl(Ljava/lang/String;)V

    .line 722
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mLayoutDelegate:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchLayoutDelegate;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchLayoutDelegate;->getContextualSearchState()Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->setIsFirstRunFlowActive(Z)V

    .line 723
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mDidLoadResolvedSearchRequest:Z

    .line 731
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mIsSearchContentViewShowing:Z

    if-eqz v0, :cond_0

    .line 732
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mSearchContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->onShow()V

    .line 735
    :cond_0
    return-void
.end method

.method private logFirstRunFlowOutcome()V
    .locals 1

    .prologue
    .line 808
    invoke-static {}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma;->logFirstRunFlowOutcome()V

    .line 809
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mDidLogFirstRunFlowOutcome:Z

    .line 810
    return-void
.end method

.method private native nativeContinueSearchTermResolutionRequest(J)V
.end method

.method private native nativeDestroy(J)V
.end method

.method private native nativeDestroyWebContents(J)V
.end method

.method private native nativeDestroyWebContentsFromContentViewCore(JLorg/chromium/content/browser/ContentViewCore;)V
.end method

.method private native nativeGatherSurroundingText(JLjava/lang/String;ZLorg/chromium/content/browser/ContentViewCore;)V
.end method

.method private native nativeInit()J
.end method

.method private native nativeReleaseWebContents(J)V
.end method

.method private native nativeRemoveLastSearchVisit(JLjava/lang/String;J)V
.end method

.method private native nativeSetInterceptNavigationDelegate(JLorg/chromium/components/navigation_interception/InterceptNavigationDelegate;Lorg/chromium/content_public/browser/WebContents;)V
.end method

.method private native nativeSetWebContents(JLorg/chromium/content/browser/ContentViewCore;Lorg/chromium/components/web_contents_delegate_android/WebContentsDelegateAndroid;)V
.end method

.method private native nativeShouldHidePromoHeader(J)Z
.end method

.method private native nativeStartSearchTermResolutionRequest(JLjava/lang/String;ZLorg/chromium/content/browser/ContentViewCore;)V
.end method

.method private onFirstRunNavigation(Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 773
    const-string/jumbo v0, "chrome://contextual-search-promo/promo.html#optin"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 774
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mLayoutDelegate:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchLayoutDelegate;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchLayoutDelegate;->getContextualSearchState()Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->setIsFirstRunFlowActive(Z)V

    .line 775
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mLayoutDelegate:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchLayoutDelegate;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchLayoutDelegate;->animateAfterFirstRunSuccess()V

    .line 776
    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->setContextualSearchState(Z)V

    .line 777
    invoke-direct {p0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->logFirstRunFlowOutcome()V

    .line 778
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mSelectionController:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController;->getLastSelectedText()Ljava/lang/String;

    move-result-object v0

    .line 779
    iget-object v1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mSelectionController:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController;->getLastSelectionType()Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController$SelectionType;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController$SelectionType;->LONG_PRESS:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController$SelectionType;

    if-ne v1, v2, :cond_1

    .line 780
    new-instance v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchRequest;

    invoke-direct {v1, v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchRequest;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mSearchRequest:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchRequest;

    .line 781
    iget-object v1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mSearchRequest:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchRequest;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchRequest;->getSearchUrl()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->loadSearchUrl(Ljava/lang/String;)V

    .line 782
    invoke-direct {p0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->getContextualSearchControl()Lcom/google/android/apps/chrome/widget/ContextualSearchControl;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/widget/ContextualSearchControl;->setResolvedSearchTerm(Ljava/lang/String;)V

    .line 795
    :cond_0
    :goto_0
    return-void

    .line 784
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mNetworkCommunicator:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchNetworkCommunicator;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchNetworkCommunicator;->continueSearchTermResolutionRequest()V

    goto :goto_0

    .line 786
    :cond_2
    const-string/jumbo v0, "chrome://contextual-search-promo/promo.html#optout"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 787
    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->setContextualSearchState(Z)V

    .line 788
    invoke-direct {p0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->logFirstRunFlowOutcome()V

    .line 789
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mLayoutDelegate:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchLayoutDelegate;

    sget-object v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->OPTOUT:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    invoke-interface {v0, v1}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchLayoutDelegate;->closePanel(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;)V

    .line 790
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mLayoutDelegate:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchLayoutDelegate;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchLayoutDelegate;->getContextualSearchState()Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->setIsFirstRunFlowActive(Z)V

    goto :goto_0

    .line 791
    :cond_3
    const-string/jumbo v0, "chrome://contextual-search-promo/promo.html#learn-more"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 792
    invoke-direct {p0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->openContextualSearchLearnMore()V

    .line 793
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mLayoutDelegate:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchLayoutDelegate;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchLayoutDelegate;->getContextualSearchState()Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->setIsFirstRunFlowActive(Z)V

    goto :goto_0
.end method

.method private onSurroundingTextAvailable(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 647
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mIsContextualSearchPanelShowing:Z

    if-eqz v0, :cond_0

    .line 648
    invoke-direct {p0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->getContextualSearchControl()Lcom/google/android/apps/chrome/widget/ContextualSearchControl;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mSelectionController:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController;->getLastSelectedText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1, p2}, Lcom/google/android/apps/chrome/widget/ContextualSearchControl;->setSearchContext(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 651
    :cond_0
    return-void
.end method

.method private openContextualSearchLearnMore()V
    .locals 2

    .prologue
    .line 801
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    sget v1, Lcom/google/android/apps/chrome/R$string;->contextual_search_learn_more_url:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/ChromeActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->openUrlInNewTab(Ljava/lang/String;)V

    .line 802
    return-void
.end method

.method private openUrlInNewTab(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 960
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeActivity;->getTabModelSelector()Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    move-result-object v0

    .line 961
    new-instance v1, Lorg/chromium/content_public/browser/LoadUrlParams;

    invoke-direct {v1, p1}, Lorg/chromium/content_public/browser/LoadUrlParams;-><init>(Ljava/lang/String;)V

    sget-object v2, Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;->FROM_MENU_OR_OVERVIEW:Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;

    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->getCurrentTab()Lorg/chromium/chrome/browser/Tab;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/chrome/tab/ChromeTab;->fromTab(Lorg/chromium/chrome/browser/Tab;)Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v3

    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->isIncognitoSelected()Z

    move-result v4

    invoke-interface {v0, v1, v2, v3, v4}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->openNewTab(Lorg/chromium/content_public/browser/LoadUrlParams;Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;Lorg/chromium/chrome/browser/Tab;Z)Lorg/chromium/chrome/browser/Tab;

    .line 966
    return-void
.end method

.method private removeLastSearchVisit()V
    .locals 6

    .prologue
    .line 761
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mSearchRequest:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchRequest;

    if-eqz v0, :cond_0

    .line 762
    iget-wide v1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mNativeContextualSearchManagerPtr:J

    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mSearchRequest:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchRequest;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchRequest;->getSearchUrl()Ljava/lang/String;

    move-result-object v3

    iget-wide v4, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mLoadedSearchUrlTimeMs:J

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->nativeRemoveLastSearchVisit(JLjava/lang/String;J)V

    .line 765
    :cond_0
    return-void
.end method

.method private runFadeInAnimation()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 574
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mControl:Lcom/google/android/apps/chrome/widget/ContextualSearchControl;

    if-nez v0, :cond_0

    .line 586
    :goto_0
    return-void

    .line 576
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mControl:Lcom/google/android/apps/chrome/widget/ContextualSearchControl;

    invoke-virtual {v0, v6}, Lcom/google/android/apps/chrome/widget/ContextualSearchControl;->setAlpha(F)V

    .line 577
    iget-object v1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mControl:Lcom/google/android/apps/chrome/widget/ContextualSearchControl;

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mIsShowingFirstRunFlow:Z

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mFreSearchBarSlideOffsetDp:F

    :goto_1
    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/widget/ContextualSearchControl;->setTranslationY(F)V

    .line 579
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mControl:Lcom/google/android/apps/chrome/widget/ContextualSearchControl;

    sget-object v1, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v2, v5, [F

    const/high16 v3, 0x3f800000    # 1.0f

    aput v3, v2, v4

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 580
    iget-object v1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mControl:Lcom/google/android/apps/chrome/widget/ContextualSearchControl;

    sget-object v2, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    new-array v3, v5, [F

    aput v6, v3, v4

    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 581
    new-instance v2, Landroid/animation/AnimatorSet;

    invoke-direct {v2}, Landroid/animation/AnimatorSet;-><init>()V

    .line 582
    const/4 v3, 0x2

    new-array v3, v3, [Landroid/animation/Animator;

    aput-object v0, v3, v4

    aput-object v1, v3, v5

    invoke-virtual {v2, v3}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 583
    sget-object v0, Lorg/chromium/ui/interpolators/BakedBezierInterpolator;->FADE_IN_CURVE:Lorg/chromium/ui/interpolators/BakedBezierInterpolator;

    invoke-virtual {v2, v0}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 584
    const-wide/16 v0, 0xda

    invoke-virtual {v2, v0, v1}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 585
    invoke-virtual {v2}, Landroid/animation/AnimatorSet;->start()V

    goto :goto_0

    .line 577
    :cond_1
    const/high16 v0, 0x42200000    # 40.0f

    goto :goto_1
.end method

.method private shouldPreloadSearchResult()Z
    .locals 2

    .prologue
    .line 710
    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getNetworkPredictionOptions()Lcom/google/android/apps/chrome/preferences/NetworkPredictionOptions;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/chrome/preferences/NetworkPredictionOptions;->NETWORK_PREDICTION_NEVER:Lcom/google/android/apps/chrome/preferences/NetworkPredictionOptions;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private showContextualSearch(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 409
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mIsContextualSearchPanelShowing:Z

    if-nez v0, :cond_0

    .line 411
    invoke-direct {p0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->getInfoBarContainer()Lorg/chromium/chrome/browser/infobar/InfoBarContainer;

    move-result-object v0

    .line 412
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    .line 413
    iput-boolean v6, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mWereInfoBarsHidden:Z

    .line 414
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->setVisibility(I)V

    .line 415
    iget-object v1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/ChromeActivity;->getCurrentTab()Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v1

    invoke-virtual {v0, v6, v1}, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->setDoStayInvisible(ZLorg/chromium/chrome/browser/Tab;)V

    .line 421
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mLayoutDelegate:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchLayoutDelegate;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchLayoutDelegate;->getContextualSearchState()Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->getPanelState()Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    move-result-object v0

    .line 422
    iget-boolean v1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mWereSearchResultsSeen:Z

    if-nez v1, :cond_1

    iget-wide v2, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mLoadedSearchUrlTimeMs:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_1

    sget-object v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->UNDEFINED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    if-eq v0, v1, :cond_1

    sget-object v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->CLOSED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    if-eq v0, v1, :cond_1

    .line 424
    invoke-direct {p0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->removeLastSearchVisit()V

    .line 431
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->createNewSearchContentView()V

    .line 435
    invoke-static {}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchFieldTrial;->isFirstRunEnabled()Z

    move-result v0

    .line 436
    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->isContextualSearchUninitialized()Z

    move-result v1

    .line 438
    if-eqz v1, :cond_3

    if-eqz v0, :cond_3

    .line 439
    invoke-direct {p0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->showFirstRunFlow()V

    .line 453
    :goto_0
    invoke-virtual {p0, v6}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->setContextualSearchViewVisibility(Z)V

    .line 454
    iput-boolean v6, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mIsContextualSearchPanelShowing:Z

    .line 455
    iput-boolean v7, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mWereSearchResultsSeen:Z

    .line 458
    iget-object v2, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mLayoutDelegate:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchLayoutDelegate;

    invoke-interface {v2}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchLayoutDelegate;->getContextualSearchState()Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->PEEKED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    invoke-virtual {v2, v3, p1}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->setPanelState(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;)V

    .line 461
    if-eqz v1, :cond_2

    if-eqz v0, :cond_2

    .line 462
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mLayoutDelegate:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchLayoutDelegate;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchLayoutDelegate;->getContextualSearchState()Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->setDidSearchInvolveFirstRun()V

    .line 464
    :cond_2
    return-void

    .line 440
    :cond_3
    iget-object v2, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mSelectionController:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController;->getLastSelectionType()Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController$SelectionType;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController$SelectionType;->TAP:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController$SelectionType;

    if-ne v2, v3, :cond_4

    .line 441
    iget-object v2, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mNetworkCommunicator:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchNetworkCommunicator;

    iget-object v3, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mSelectionController:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController;

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController;->getLastSelectedText()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchNetworkCommunicator;->startSearchTermResolutionRequest(Ljava/lang/String;)V

    goto :goto_0

    .line 444
    :cond_4
    new-instance v2, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchRequest;

    iget-object v3, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mSelectionController:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController;

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController;->getLastSelectedText()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchRequest;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mSearchRequest:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchRequest;

    .line 446
    iput-boolean v7, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mDidLoadResolvedSearchRequest:Z

    .line 447
    invoke-direct {p0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->getContextualSearchControl()Lcom/google/android/apps/chrome/widget/ContextualSearchControl;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mSelectionController:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController;

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController;->getLastSelectedText()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/chrome/widget/ContextualSearchControl;->setResolvedSearchTerm(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private showFirstRunFlow()V
    .locals 7

    .prologue
    const/4 v5, 0x1

    .line 482
    iput-boolean v5, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mIsShowingFirstRunFlow:Z

    .line 483
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mDidLogFirstRunFlowOutcome:Z

    .line 484
    invoke-direct {p0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->getContextualSearchControl()Lcom/google/android/apps/chrome/widget/ContextualSearchControl;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mSelectionController:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController;->getLastSelectedText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/widget/ContextualSearchControl;->setFirstRunText(Ljava/lang/String;)V

    .line 485
    const-string/jumbo v0, "chrome://contextual-search-promo/promo.html"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->loadUrl(Ljava/lang/String;)V

    .line 486
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mLayoutDelegate:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchLayoutDelegate;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchLayoutDelegate;->getContextualSearchState()Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->setIsFirstRunFlowActive(Z)V

    .line 488
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mSelectionController:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController;->getLastSelectionType()Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController$SelectionType;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController$SelectionType;->TAP:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController$SelectionType;

    if-ne v0, v1, :cond_0

    .line 489
    iget-wide v2, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mNativeContextualSearchManagerPtr:J

    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mSelectionController:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController;->getLastSelectedText()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->getBaseContentView()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v6

    move-object v1, p0

    invoke-direct/range {v1 .. v6}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->nativeGatherSurroundingText(JLjava/lang/String;ZLorg/chromium/content/browser/ContentViewCore;)V

    .line 493
    :cond_0
    return-void
.end method


# virtual methods
.method public clearNativeManager()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 626
    sget-boolean v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mNativeContextualSearchManagerPtr:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 627
    :cond_0
    iput-wide v2, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mNativeContextualSearchManagerPtr:J

    .line 628
    return-void
.end method

.method public continueSearchTermResolutionRequest()V
    .locals 2

    .prologue
    .line 474
    iget-wide v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mNativeContextualSearchManagerPtr:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->nativeContinueSearchTermResolutionRequest(J)V

    .line 475
    return-void
.end method

.method public doesContainAWord(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 518
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mContainsWordPattern:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v0

    return v0
.end method

.method public getBaseContentView()Lorg/chromium/content/browser/ContentViewCore;
    .locals 1

    .prologue
    .line 312
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeActivity;->getCurrentTab()Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    return-object v0
.end method

.method public getContextualSearchLayoutDelegate()Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchLayoutDelegate;
    .locals 1

    .prologue
    .line 280
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mLayoutDelegate:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchLayoutDelegate;

    return-object v0
.end method

.method public getGestureStateListener()Lorg/chromium/content_public/browser/GestureStateListener;
    .locals 1

    .prologue
    .line 1090
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mSelectionController:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController;->getGestureStateListener()Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController$ContextualSearchGestureStateListener;

    move-result-object v0

    return-object v0
.end method

.method public getSearchContentViewCore()Lorg/chromium/content/browser/ContentViewCore;
    .locals 1

    .prologue
    .line 746
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mSearchContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    return-object v0
.end method

.method public getSearchContentViewVerticalScroll()F
    .locals 1

    .prologue
    .line 1007
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mSearchContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mSearchContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->computeVerticalScrollOffset()I

    move-result v0

    int-to-float v0, v0

    :goto_0
    return v0

    :cond_0
    const/high16 v0, -0x40800000    # -1.0f

    goto :goto_0
.end method

.method getSelectionController()Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController;
    .locals 1

    .prologue
    .line 288
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mSelectionController:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController;

    return-object v0
.end method

.method public handleInvalidTap()V
    .locals 1

    .prologue
    .line 1118
    sget-object v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->BASE_PAGE_TAP:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->hideContextualSearch(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;)V

    .line 1119
    return-void
.end method

.method public handleScroll()V
    .locals 1

    .prologue
    .line 1113
    sget-object v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->BASE_PAGE_SCROLL:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->hideContextualSearch(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;)V

    .line 1114
    return-void
.end method

.method public handleSearchTermResolutionResponse(ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 676
    iget-boolean v2, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mIsContextualSearchPanelShowing:Z

    if-nez v2, :cond_1

    .line 701
    :cond_0
    :goto_0
    return-void

    .line 678
    :cond_1
    if-eqz p1, :cond_5

    .line 679
    iget-object v2, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/ChromeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/chrome/R$string;->contextual_search_network_unavailable:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p4

    .line 688
    :cond_2
    :goto_1
    invoke-direct {p0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->getContextualSearchControl()Lcom/google/android/apps/chrome/widget/ContextualSearchControl;

    move-result-object v2

    invoke-virtual {v2, p4}, Lcom/google/android/apps/chrome/widget/ContextualSearchControl;->setResolvedSearchTerm(Ljava/lang/String;)V

    .line 690
    invoke-virtual {p3}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 691
    if-nez p6, :cond_6

    invoke-direct {p0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->shouldPreloadSearchResult()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 692
    :goto_2
    new-instance v2, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchRequest;

    invoke-direct {v2, p3, p5, v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    iput-object v2, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mSearchRequest:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchRequest;

    .line 693
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mDidLoadResolvedSearchRequest:Z

    .line 694
    iget-boolean v1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mIsSearchContentViewShowing:Z

    if-eqz v1, :cond_3

    .line 695
    iget-object v1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mSearchRequest:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchRequest;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchRequest;->setNormalPriority()V

    .line 697
    :cond_3
    iget-boolean v1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mIsSearchContentViewShowing:Z

    if-nez v1, :cond_4

    if-eqz v0, :cond_0

    .line 698
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mSearchRequest:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchRequest;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchRequest;->getSearchUrl()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->loadSearchUrl(Ljava/lang/String;)V

    goto :goto_0

    .line 681
    :cond_5
    const/16 v2, 0xc8

    if-eq p2, v2, :cond_2

    .line 684
    iget-object v2, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/ChromeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/chrome/R$string;->contextual_search_error:I

    new-array v4, v0, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p4

    goto :goto_1

    :cond_6
    move v0, v1

    .line 691
    goto :goto_2
.end method

.method public handleSelection(Ljava/lang/String;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController$SelectionType;FF)V
    .locals 2

    .prologue
    .line 1123
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1124
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->isValidSelection(Ljava/lang/String;)Z

    move-result v1

    .line 1126
    sget-object v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController$SelectionType;->TAP:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController$SelectionType;

    if-ne p2, v0, :cond_1

    sget-object v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->TEXT_SELECT_TAP:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    .line 1128
    :goto_0
    invoke-static {v1}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma;->logSelectionIsValid(Z)V

    .line 1130
    if-eqz v1, :cond_2

    .line 1131
    iget-object v1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mLayoutDelegate:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchLayoutDelegate;

    invoke-interface {v1, p4}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchLayoutDelegate;->updateBasePageSelectionStartYPx(F)V

    .line 1132
    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->showContextualSearch(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;)V

    .line 1137
    :cond_0
    :goto_1
    return-void

    .line 1126
    :cond_1
    sget-object v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->TEXT_SELECT_LONG_PRESS:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    goto :goto_0

    .line 1134
    :cond_2
    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->hideContextualSearch(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;)V

    goto :goto_1
.end method

.method public handleSelectionChanged(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1098
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mSelectionController:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController;->handleSelectionChanged(Ljava/lang/String;)V

    .line 1099
    return-void
.end method

.method public handleSelectionEvent(IFF)V
    .locals 1

    .prologue
    .line 1108
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mSelectionController:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController;->handleSelectionEvent(IFF)V

    .line 1109
    return-void
.end method

.method public handleSelectionModification(Ljava/lang/String;FF)V
    .locals 1

    .prologue
    .line 1141
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mIsContextualSearchPanelShowing:Z

    if-eqz v0, :cond_0

    .line 1142
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mIsShowingFirstRunFlow:Z

    if-eqz v0, :cond_1

    .line 1143
    invoke-direct {p0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->getContextualSearchControl()Lcom/google/android/apps/chrome/widget/ContextualSearchControl;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/widget/ContextualSearchControl;->setFirstRunText(Ljava/lang/String;)V

    .line 1148
    :cond_0
    :goto_0
    return-void

    .line 1145
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->getContextualSearchControl()Lcom/google/android/apps/chrome/widget/ContextualSearchControl;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/widget/ContextualSearchControl;->setResolvedSearchTerm(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public hideContextualSearch(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 320
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mSelectionController:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController;->getLastSelectionType()Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController$SelectionType;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController$SelectionType;->TAP:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController$SelectionType;

    if-ne v0, v1, :cond_0

    .line 321
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mSelectionController:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController;->clearSelection()V

    .line 324
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mIsContextualSearchPanelShowing:Z

    if-nez v0, :cond_1

    .line 366
    :goto_0
    return-void

    .line 327
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mWereInfoBarsHidden:Z

    if-eqz v0, :cond_2

    .line 328
    iput-boolean v2, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mWereInfoBarsHidden:Z

    .line 329
    invoke-direct {p0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->getInfoBarContainer()Lorg/chromium/chrome/browser/infobar/InfoBarContainer;

    move-result-object v0

    .line 330
    if-eqz v0, :cond_2

    .line 331
    invoke-virtual {v0, v2}, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->setVisibility(I)V

    .line 332
    iget-object v1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/ChromeActivity;->getCurrentTab()Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->setDoStayInvisible(ZLorg/chromium/chrome/browser/Tab;)V

    .line 336
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mLayoutDelegate:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchLayoutDelegate;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchLayoutDelegate;->hide()V

    .line 337
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mLayoutDelegate:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchLayoutDelegate;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchLayoutDelegate;->getContextualSearchState()Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->CLOSED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    invoke-virtual {v0, v1, p1}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->setPanelState(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;)V

    .line 339
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mWereSearchResultsSeen:Z

    if-nez v0, :cond_3

    iget-wide v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mLoadedSearchUrlTimeMs:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_3

    .line 340
    invoke-direct {p0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->removeLastSearchVisit()V

    .line 345
    :cond_3
    iput-wide v4, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mLoadedSearchUrlTimeMs:J

    .line 346
    iput-boolean v2, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mWereSearchResultsSeen:Z

    .line 347
    iput-boolean v2, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mIsContextualSearchPanelShowing:Z

    .line 349
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mControl:Lcom/google/android/apps/chrome/widget/ContextualSearchControl;

    if-eqz v0, :cond_4

    .line 350
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mControl:Lcom/google/android/apps/chrome/widget/ContextualSearchControl;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/ContextualSearchControl;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mControl:Lcom/google/android/apps/chrome/widget/ContextualSearchControl;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 351
    iput-object v3, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mControl:Lcom/google/android/apps/chrome/widget/ContextualSearchControl;

    .line 352
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mResourceLoader:Lcom/google/android/apps/chrome/compositor/resources/dynamics/DynamicResourceLoader;

    if-eqz v0, :cond_4

    .line 353
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mResourceLoader:Lcom/google/android/apps/chrome/compositor/resources/dynamics/DynamicResourceLoader;

    sget v1, Lcom/google/android/apps/chrome/R$id;->contextual_search_bar_text:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/compositor/resources/dynamics/DynamicResourceLoader;->unregisterResource(I)V

    .line 357
    :cond_4
    invoke-direct {p0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->destroySearchContentView()V

    .line 358
    iput-object v3, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mSearchRequest:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchRequest;

    .line 360
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mIsShowingFirstRunFlow:Z

    if-eqz v0, :cond_5

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mDidLogFirstRunFlowOutcome:Z

    if-nez v0, :cond_5

    .line 361
    invoke-direct {p0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->logFirstRunFlowOutcome()V

    .line 364
    :cond_5
    iput-boolean v2, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mIsShowingFirstRunFlow:Z

    .line 365
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mLayoutDelegate:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchLayoutDelegate;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchLayoutDelegate;->getContextualSearchState()Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->setIsFirstRunFlowActive(Z)V

    goto :goto_0
.end method

.method public initialize(Landroid/view/ViewGroup;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 242
    iput-object p1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mParentView:Landroid/view/ViewGroup;

    .line 243
    invoke-direct {p0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->nativeInit()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mNativeContextualSearchManagerPtr:J

    .line 244
    invoke-direct {p0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->listenForHideNotifications()V

    .line 245
    new-instance v0, Lcom/google/android/apps/chrome/tab/TabRedirectHandler;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/apps/chrome/tab/TabRedirectHandler;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mTabRedirectHandler:Lcom/google/android/apps/chrome/tab/TabRedirectHandler;

    .line 247
    iput-boolean v2, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mIsShowingFirstRunFlow:Z

    .line 248
    iput-boolean v2, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mDidLogFirstRunFlowOutcome:Z

    .line 249
    iput-boolean v2, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mDidLoadResolvedSearchRequest:Z

    .line 250
    iput-boolean v2, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mWereSearchResultsSeen:Z

    .line 251
    iput-object p0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mNetworkCommunicator:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchNetworkCommunicator;

    .line 252
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    invoke-static {p0, v0}, Lorg/chromium/base/ApplicationStatus;->registerStateListenerForActivity(Lorg/chromium/base/ApplicationStatus$ActivityStateListener;Landroid/app/Activity;)V

    .line 253
    return-void
.end method

.method isContextualSearchPanelShowing()Z
    .locals 1

    .prologue
    .line 293
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mIsContextualSearchPanelShowing:Z

    return v0
.end method

.method public isRunningInCompatibilityMode()Z
    .locals 1

    .prologue
    .line 970
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    invoke-static {v0}, Lcom/google/android/apps/chrome/device/DeviceClassManager;->isAccessibilityModeEnabled(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/android/apps/chrome/device/DeviceClassManager;->isLowEndDevice()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSearchBarPeeking()Z
    .locals 2

    .prologue
    .line 498
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mIsContextualSearchPanelShowing:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mLayoutDelegate:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchLayoutDelegate;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchLayoutDelegate;->getContextualSearchState()Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->getPanelState()Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->PEEKED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method isValidSelection(Ljava/lang/String;Lorg/chromium/content/browser/ContentViewCore;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 397
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    const/16 v2, 0x64

    if-gt v1, v2, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->doesContainAWord(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 400
    :cond_0
    :goto_0
    return v0

    :cond_1
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lorg/chromium/content/browser/ContentViewCore;->isFocusedNodeEditable()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public loadUrl(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 814
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mSearchContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mSearchContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->getWebContents()Lorg/chromium/content_public/browser/WebContents;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 815
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mSearchContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->getWebContents()Lorg/chromium/content_public/browser/WebContents;

    move-result-object v0

    invoke-interface {v0}, Lorg/chromium/content_public/browser/WebContents;->getNavigationController()Lorg/chromium/content_public/browser/NavigationController;

    move-result-object v0

    new-instance v1, Lorg/chromium/content_public/browser/LoadUrlParams;

    invoke-direct {v1, p1}, Lorg/chromium/content_public/browser/LoadUrlParams;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lorg/chromium/content_public/browser/NavigationController;->loadUrl(Lorg/chromium/content_public/browser/LoadUrlParams;)V

    .line 818
    :cond_0
    return-void
.end method

.method public onActivityStateChange(Landroid/app/Activity;I)V
    .locals 1

    .prologue
    .line 615
    const/4 v0, 0x4

    if-eq p2, v0, :cond_0

    const/4 v0, 0x5

    if-eq p2, v0, :cond_0

    const/4 v0, 0x6

    if-ne p2, v0, :cond_1

    .line 617
    :cond_0
    sget-object v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->UNKNOWN:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->hideContextualSearch(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;)V

    .line 619
    :cond_1
    return-void
.end method

.method public onBackPressed()Z
    .locals 1

    .prologue
    .line 372
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mIsContextualSearchPanelShowing:Z

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 374
    :goto_0
    return v0

    .line 373
    :cond_0
    sget-object v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->BACK_PRESS:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->hideContextualSearch(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;)V

    .line 374
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onSearchTermResolutionResponse(ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 7

    .prologue
    .line 668
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mNetworkCommunicator:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchNetworkCommunicator;

    move v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move v6, p6

    invoke-interface/range {v0 .. v6}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchNetworkCommunicator;->handleSearchTermResolutionResponse(ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 670
    return-void
.end method

.method public openResolvedSearchUrlInNewTab()V
    .locals 1

    .prologue
    .line 951
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mSearchRequest:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchRequest;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mSearchRequest:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchRequest;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchRequest;->getSearchUrl()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 952
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mSearchRequest:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchRequest;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchRequest;->getSearchUrl()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->openUrlInNewTab(Ljava/lang/String;)V

    .line 954
    :cond_0
    return-void
.end method

.method public preserveBasePageSelectionOnNextLossOfFocus()V
    .locals 1

    .prologue
    .line 1042
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->getBaseContentView()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    .line 1043
    if-eqz v0, :cond_0

    .line 1044
    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->preserveSelectionOnNextLossOfFocus()V

    .line 1046
    :cond_0
    return-void
.end method

.method public promoteToTab()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 976
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mLayoutDelegate:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchLayoutDelegate;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchLayoutDelegate;->getContextualSearchState()Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->CLOSED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    sget-object v2, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->TAB_PROMOTION:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->setPanelState(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;)V

    .line 978
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mSearchContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mSearchContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->getWebContents()Lorg/chromium/content_public/browser/WebContents;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mSearchContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->getWebContents()Lorg/chromium/content_public/browser/WebContents;

    move-result-object v0

    invoke-interface {v0}, Lorg/chromium/content_public/browser/WebContents;->getUrl()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "chrome://contextual-search-promo/promo.html"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 980
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mSelectionController:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController;->clearSelection()V

    .line 981
    iget-wide v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mNativeContextualSearchManagerPtr:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->nativeReleaseWebContents(J)V

    .line 982
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mSearchContentViewDelegate:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager$ContextualSearchContentViewDelegate;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager$ContextualSearchContentViewDelegate;->releaseContextualSearchContentViewCore()V

    .line 983
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mTabPromotionDelegate:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager$ContextualSearchTabPromotionDelegate;

    iget-object v1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mSearchContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    invoke-interface {v0, v1}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager$ContextualSearchTabPromotionDelegate;->createContextualSearchTab(Lorg/chromium/content/browser/ContentViewCore;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 984
    iget-wide v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mNativeContextualSearchManagerPtr:J

    iget-object v2, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mSearchContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->nativeDestroyWebContentsFromContentViewCore(JLorg/chromium/content/browser/ContentViewCore;)V

    .line 986
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mSearchContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->destroy()V

    .line 988
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mSearchWebContentsObserver:Lorg/chromium/content/browser/WebContentsObserverAndroid;

    if-eqz v0, :cond_1

    .line 989
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mSearchWebContentsObserver:Lorg/chromium/content/browser/WebContentsObserverAndroid;

    invoke-virtual {v0}, Lorg/chromium/content/browser/WebContentsObserverAndroid;->detachFromWebContents()V

    .line 990
    iput-object v3, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mSearchWebContentsObserver:Lorg/chromium/content/browser/WebContentsObserverAndroid;

    .line 992
    :cond_1
    iput-object v3, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mSearchContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    .line 993
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mIsSearchContentViewShowing:Z

    .line 994
    iput-object v3, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mSearchRequest:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchRequest;

    .line 996
    :cond_2
    return-void
.end method

.method public resetSearchContentViewScroll()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1000
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mSearchContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    if-eqz v0, :cond_0

    .line 1001
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mSearchContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    invoke-virtual {v0, v1, v1}, Lorg/chromium/content/browser/ContentViewCore;->scrollTo(II)V

    .line 1003
    :cond_0
    return-void
.end method

.method public setContextualSearchLayoutDelegate(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchLayoutDelegate;)V
    .locals 2

    .prologue
    .line 269
    iput-object p1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mLayoutDelegate:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchLayoutDelegate;

    .line 271
    iget-wide v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mNativeContextualSearchManagerPtr:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->nativeShouldHidePromoHeader(J)Z

    move-result v0

    .line 272
    iget-object v1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mLayoutDelegate:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchLayoutDelegate;

    invoke-interface {v1}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchLayoutDelegate;->getContextualSearchState()Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->setShouldHidePromoHeader(Z)V

    .line 273
    return-void
.end method

.method public setContextualSearchViewVisibility(Z)V
    .locals 2

    .prologue
    .line 535
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mControl:Lcom/google/android/apps/chrome/widget/ContextualSearchControl;

    if-eqz v0, :cond_0

    .line 536
    iget-object v1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mControl:Lcom/google/android/apps/chrome/widget/ContextualSearchControl;

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/widget/ContextualSearchControl;->setVisibility(I)V

    .line 538
    :cond_0
    return-void

    .line 536
    :cond_1
    const/4 v0, 0x4

    goto :goto_0
.end method

.method public setDynamicResourceLoader(Lcom/google/android/apps/chrome/compositor/resources/dynamics/DynamicResourceLoader;)V
    .locals 3

    .prologue
    .line 300
    iput-object p1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mResourceLoader:Lcom/google/android/apps/chrome/compositor/resources/dynamics/DynamicResourceLoader;

    .line 302
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mControl:Lcom/google/android/apps/chrome/widget/ContextualSearchControl;

    if-eqz v0, :cond_0

    .line 303
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mResourceLoader:Lcom/google/android/apps/chrome/compositor/resources/dynamics/DynamicResourceLoader;

    sget v1, Lcom/google/android/apps/chrome/R$id;->contextual_search_bar_text:I

    iget-object v2, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mControl:Lcom/google/android/apps/chrome/widget/ContextualSearchControl;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/widget/ContextualSearchControl;->getResourceAdapter()Lcom/google/android/apps/chrome/compositor/resources/dynamics/ViewResourceAdapter;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/compositor/resources/dynamics/DynamicResourceLoader;->registerResource(ILcom/google/android/apps/chrome/compositor/resources/dynamics/DynamicResource;)V

    .line 306
    :cond_0
    return-void
.end method

.method public setEdgeSwipeHandler(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;)V
    .locals 2

    .prologue
    .line 545
    iput-object p1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mEdgeSwipeHandler:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;

    .line 546
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mControl:Lcom/google/android/apps/chrome/widget/ContextualSearchControl;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mControl:Lcom/google/android/apps/chrome/widget/ContextualSearchControl;

    iget-object v1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mEdgeSwipeHandler:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/widget/ContextualSearchControl;->setSwipeHandler(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;)V

    .line 547
    :cond_0
    return-void
.end method

.method public setNativeManager(J)V
    .locals 5

    .prologue
    .line 636
    sget-boolean v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mNativeContextualSearchManagerPtr:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 637
    :cond_0
    iput-wide p1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mNativeContextualSearchManagerPtr:J

    .line 638
    return-void
.end method

.method public setNetworkCommunicator(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchNetworkCommunicator;)V
    .locals 0

    .prologue
    .line 383
    iput-object p1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mNetworkCommunicator:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchNetworkCommunicator;

    .line 384
    return-void
.end method

.method public setSearchContentViewDelegate(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager$ContextualSearchContentViewDelegate;)V
    .locals 0

    .prologue
    .line 754
    iput-object p1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mSearchContentViewDelegate:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager$ContextualSearchContentViewDelegate;

    .line 755
    return-void
.end method

.method public setSearchContentViewVisibility(Z)V
    .locals 2

    .prologue
    .line 1013
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mIsSearchContentViewShowing:Z

    if-ne v0, p1, :cond_1

    .line 1038
    :cond_0
    :goto_0
    return-void

    .line 1015
    :cond_1
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mIsSearchContentViewShowing:Z

    .line 1016
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mSearchContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    if-eqz v0, :cond_0

    .line 1017
    if-eqz p1, :cond_4

    .line 1018
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mWereSearchResultsSeen:Z

    .line 1019
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mIsShowingFirstRunFlow:Z

    if-nez v0, :cond_2

    .line 1020
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mSelectionController:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController;->getLastSelectionType()Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController$SelectionType;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController$SelectionType;->LONG_PRESS:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController$SelectionType;

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mSelectionController:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController;->getLastSelectedText()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1022
    new-instance v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchRequest;

    iget-object v1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mSelectionController:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController;->getLastSelectedText()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchRequest;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mSearchRequest:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchRequest;

    .line 1024
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mDidLoadResolvedSearchRequest:Z

    .line 1027
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mSearchRequest:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchRequest;

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mDidLoadResolvedSearchRequest:Z

    if-nez v0, :cond_3

    .line 1028
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mSearchRequest:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchRequest;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchRequest;->setNormalPriority()V

    .line 1029
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mSearchRequest:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchRequest;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchRequest;->getSearchUrl()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->loadSearchUrl(Ljava/lang/String;)V

    .line 1031
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mSearchContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->onShow()V

    .line 1033
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mLayoutDelegate:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchLayoutDelegate;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchLayoutDelegate;->getContextualSearchState()Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->setWasSearchContentViewSeen()V

    goto :goto_0

    .line 1035
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mSearchContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->onHide()V

    goto :goto_0
.end method

.method public startSearchTermResolutionRequest(Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 468
    iget-wide v2, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mNativeContextualSearchManagerPtr:J

    const/4 v5, 0x1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->getBaseContentView()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v6

    move-object v1, p0

    move-object v4, p1

    invoke-direct/range {v1 .. v6}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->nativeStartSearchTermResolutionRequest(JLjava/lang/String;ZLorg/chromium/content/browser/ContentViewCore;)V

    .line 470
    return-void
.end method

.method public updateTopControlsState(IZ)V
    .locals 1

    .prologue
    .line 504
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeActivity;->getTabModelSelector()Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    move-result-object v0

    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->getCurrentTab()Lorg/chromium/chrome/browser/Tab;

    move-result-object v0

    .line 505
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->fromTab(Lorg/chromium/chrome/browser/Tab;)Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    .line 506
    if-eqz v0, :cond_0

    .line 507
    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/chrome/tab/ChromeTab;->updateTopControlsState(IZ)V

    .line 509
    :cond_0
    return-void
.end method
