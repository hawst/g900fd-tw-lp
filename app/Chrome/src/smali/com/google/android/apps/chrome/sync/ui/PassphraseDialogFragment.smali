.class public Lcom/google/android/apps/chrome/sync/ui/PassphraseDialogFragment;
.super Lcom/google/android/apps/chrome/preferences/ChromeBaseDialogFragment;
.source "PassphraseDialogFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# static fields
.field static final ARG_IS_GAIA:Ljava/lang/String; = "is_gaia"

.field static final ARG_IS_UPDATE:Ljava/lang/String; = "is_update"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBaseDialogFragment;-><init>()V

    .line 42
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/sync/ui/PassphraseDialogFragment;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/ui/PassphraseDialogFragment;->handleOk()V

    return-void
.end method

.method private getListener()Lcom/google/android/apps/chrome/sync/ui/PassphraseDialogFragment$Listener;
    .locals 2

    .prologue
    .line 188
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/ui/PassphraseDialogFragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v0

    .line 189
    instance-of v1, v0, Lcom/google/android/apps/chrome/sync/ui/PassphraseDialogFragment$Listener;

    if-eqz v1, :cond_0

    .line 190
    check-cast v0, Lcom/google/android/apps/chrome/sync/ui/PassphraseDialogFragment$Listener;

    .line 192
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/ui/PassphraseDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/sync/ui/PassphraseDialogFragment$Listener;

    goto :goto_0
.end method

.method private handleCancel()V
    .locals 3

    .prologue
    .line 170
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/ui/PassphraseDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "is_update"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    .line 171
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/ui/PassphraseDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string/jumbo v2, "is_gaia"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 172
    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/ui/PassphraseDialogFragment;->getListener()Lcom/google/android/apps/chrome/sync/ui/PassphraseDialogFragment$Listener;

    move-result-object v2

    invoke-interface {v2, v1, v0}, Lcom/google/android/apps/chrome/sync/ui/PassphraseDialogFragment$Listener;->onPassphraseCanceled(ZZ)V

    .line 173
    return-void
.end method

.method private handleOk()V
    .locals 4

    .prologue
    .line 176
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/ui/PassphraseDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$id;->verifying:I

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 177
    sget v1, Lcom/google/android/apps/chrome/R$string;->sync_verifying:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 179
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/ui/PassphraseDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$id;->passphrase:I

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 180
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 182
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/ui/PassphraseDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string/jumbo v2, "is_update"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 183
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/ui/PassphraseDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    const-string/jumbo v3, "is_gaia"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    .line 184
    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/ui/PassphraseDialogFragment;->getListener()Lcom/google/android/apps/chrome/sync/ui/PassphraseDialogFragment$Listener;

    move-result-object v3

    invoke-interface {v3, v0, v2, v1}, Lcom/google/android/apps/chrome/sync/ui/PassphraseDialogFragment$Listener;->onPassphraseEntered(Ljava/lang/String;ZZ)V

    .line 185
    return-void
.end method

.method public static newInstance(Landroid/app/Fragment;ZZ)Lcom/google/android/apps/chrome/sync/ui/PassphraseDialogFragment;
    .locals 3

    .prologue
    .line 56
    new-instance v0, Lcom/google/android/apps/chrome/sync/ui/PassphraseDialogFragment;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/sync/ui/PassphraseDialogFragment;-><init>()V

    .line 57
    if-eqz p0, :cond_0

    .line 58
    const/4 v1, -0x1

    invoke-virtual {v0, p0, v1}, Lcom/google/android/apps/chrome/sync/ui/PassphraseDialogFragment;->setTargetFragment(Landroid/app/Fragment;I)V

    .line 60
    :cond_0
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 61
    const-string/jumbo v2, "is_gaia"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 62
    const-string/jumbo v2, "is_update"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 63
    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/sync/ui/PassphraseDialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 64
    return-object v0
.end method


# virtual methods
.method public invalidPassphrase()V
    .locals 2

    .prologue
    .line 199
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/ui/PassphraseDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$id;->verifying:I

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 200
    sget v1, Lcom/google/android/apps/chrome/R$string;->sync_passphrase_incorrect:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 201
    return-void
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 1

    .prologue
    .line 164
    const/4 v0, -0x2

    if-ne p2, v0, :cond_0

    .line 165
    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/ui/PassphraseDialogFragment;->handleCancel()V

    .line 167
    :cond_0
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 13

    .prologue
    const/4 v12, 0x0

    .line 69
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/ui/PassphraseDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    .line 70
    sget v1, Lcom/google/android/apps/chrome/R$layout;->sync_enter_passphrase:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 72
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/ui/PassphraseDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "is_gaia"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    .line 73
    sget v0, Lcom/google/android/apps/chrome/R$id;->prompt_text:I

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 74
    sget v1, Lcom/google/android/apps/chrome/R$id;->passphrase:I

    invoke-virtual {v3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    .line 75
    invoke-virtual {v1}, Landroid/widget/EditText;->getContext()Landroid/content/Context;

    move-result-object v5

    .line 76
    sget v2, Lcom/google/android/apps/chrome/R$id;->reset_text:I

    invoke-virtual {v3, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 77
    invoke-static {v5}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->get(Landroid/content/Context;)Lorg/chromium/chrome/browser/sync/ProfileSyncService;

    move-result-object v6

    .line 78
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->getCurrentSignedInAccountText()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "\n\n"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 79
    if-eqz v4, :cond_0

    .line 80
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget v4, Lcom/google/android/apps/chrome/R$string;->sync_enter_google_passphrase:I

    invoke-virtual {v5, v4}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 82
    sget v0, Lcom/google/android/apps/chrome/R$string;->sync_enter_google_passphrase_hint:I

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setHint(I)V

    .line 124
    :goto_0
    new-instance v0, Lcom/google/android/apps/chrome/sync/ui/PassphraseDialogFragment$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/sync/ui/PassphraseDialogFragment$2;-><init>(Lcom/google/android/apps/chrome/sync/ui/PassphraseDialogFragment;)V

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 134
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/ui/PassphraseDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const/4 v2, 0x3

    invoke-direct {v0, v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$string;->ok:I

    new-instance v2, Lcom/google/android/apps/chrome/sync/ui/PassphraseDialogFragment$3;

    invoke-direct {v2, p0}, Lcom/google/android/apps/chrome/sync/ui/PassphraseDialogFragment$3;-><init>(Lcom/google/android/apps/chrome/sync/ui/PassphraseDialogFragment;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$string;->cancel:I

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$string;->sign_in_google_account:I

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 147
    new-instance v1, Lcom/google/android/apps/chrome/sync/ui/PassphraseDialogFragment$4;

    invoke-direct {v1, p0, v0}, Lcom/google/android/apps/chrome/sync/ui/PassphraseDialogFragment$4;-><init>(Lcom/google/android/apps/chrome/sync/ui/PassphraseDialogFragment;Landroid/app/AlertDialog;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V

    .line 159
    return-object v0

    .line 84
    :cond_0
    invoke-virtual {v6}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->hasExplicitPassphraseTime()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 85
    sget v4, Lcom/google/android/apps/chrome/R$string;->sync_passphrase_reset_instructions:I

    invoke-virtual {v5, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    new-array v5, v5, [Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;

    new-instance v8, Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;

    const-string/jumbo v9, "<link>"

    const-string/jumbo v10, "</link>"

    new-instance v11, Lcom/google/android/apps/chrome/sync/ui/PassphraseDialogFragment$1;

    invoke-direct {v11, p0}, Lcom/google/android/apps/chrome/sync/ui/PassphraseDialogFragment$1;-><init>(Lcom/google/android/apps/chrome/sync/ui/PassphraseDialogFragment;)V

    invoke-direct {v8, v9, v10, v11}, Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    aput-object v8, v5, v12

    invoke-static {v4, v5}, Lcom/google/android/apps/chrome/utilities/StringLinkifier;->linkify(Ljava/lang/String;[Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;)Landroid/text/SpannableString;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 94
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 95
    invoke-virtual {v2, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 96
    invoke-virtual {v6}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->getSyncDecryptionPassphraseType()Lorg/chromium/sync/internal_api/pub/SyncDecryptionPassphraseType;

    move-result-object v2

    .line 98
    sget-object v4, Lcom/google/android/apps/chrome/sync/ui/PassphraseDialogFragment$5;->$SwitchMap$org$chromium$sync$internal_api$pub$SyncDecryptionPassphraseType:[I

    invoke-virtual {v2}, Lorg/chromium/sync/internal_api/pub/SyncDecryptionPassphraseType;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 112
    const-string/jumbo v4, "PassphraseDialogFragment"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "Found incorrect passphrase type "

    invoke-direct {v5, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v5, ". Falling back to default string."

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 114
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v6}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->getSyncEnterCustomPassphraseBodyText()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 122
    :goto_1
    sget v0, Lcom/google/android/apps/chrome/R$string;->sync_enter_custom_passphrase_hint:I

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setHint(I)V

    goto/16 :goto_0

    .line 100
    :pswitch_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v6}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->getSyncEnterGooglePassphraseBodyWithDateText()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 104
    :pswitch_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v6}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->getSyncEnterCustomPassphraseBodyWithDateText()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 119
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v6}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->getSyncEnterCustomPassphraseBodyText()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 98
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
