.class public Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;
.super Ljava/lang/Object;
.source "EnhancedBookmarksModel.java"


# instance fields
.field private final mBookmarksBridge:Lorg/chromium/chrome/browser/BookmarksBridge;

.field private final mChromeEnhancedBookmarkBridge:Lcom/google/android/apps/chrome/enhancedbookmark/ChromeEnhancedBookmarkBridge;

.field private final mEnhancedBookmarksBridge:Lorg/chromium/chrome/browser/EnhancedBookmarksBridge;


# direct methods
.method constructor <init>(Lorg/chromium/chrome/browser/profiles/Profile;I)V
    .locals 4

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    new-instance v0, Lorg/chromium/chrome/browser/BookmarksBridge;

    invoke-direct {v0, p1}, Lorg/chromium/chrome/browser/BookmarksBridge;-><init>(Lorg/chromium/chrome/browser/profiles/Profile;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;->mBookmarksBridge:Lorg/chromium/chrome/browser/BookmarksBridge;

    .line 29
    new-instance v0, Lorg/chromium/chrome/browser/EnhancedBookmarksBridge;

    invoke-static {p1}, Lorg/chromium/chrome/browser/BookmarksBridge;->getNativeBookmarkModel(Lorg/chromium/chrome/browser/profiles/Profile;)J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lorg/chromium/chrome/browser/EnhancedBookmarksBridge;-><init>(J)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;->mEnhancedBookmarksBridge:Lorg/chromium/chrome/browser/EnhancedBookmarksBridge;

    .line 31
    new-instance v0, Lcom/google/android/apps/chrome/enhancedbookmark/ChromeEnhancedBookmarkBridge;

    invoke-direct {v0, p1, p2}, Lcom/google/android/apps/chrome/enhancedbookmark/ChromeEnhancedBookmarkBridge;-><init>(Lorg/chromium/chrome/browser/profiles/Profile;I)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;->mChromeEnhancedBookmarkBridge:Lcom/google/android/apps/chrome/enhancedbookmark/ChromeEnhancedBookmarkBridge;

    .line 33
    return-void
.end method


# virtual methods
.method public addFolder(Lorg/chromium/components/bookmarks/BookmarkId;ILjava/lang/String;)Lorg/chromium/components/bookmarks/BookmarkId;
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;->mBookmarksBridge:Lorg/chromium/chrome/browser/BookmarksBridge;

    invoke-virtual {v0, p1, p2, p3}, Lorg/chromium/chrome/browser/BookmarksBridge;->addFolder(Lorg/chromium/components/bookmarks/BookmarkId;ILjava/lang/String;)Lorg/chromium/components/bookmarks/BookmarkId;

    move-result-object v0

    return-object v0
.end method

.method public addModelObserver(Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkModelObserver;)V
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;->mBookmarksBridge:Lorg/chromium/chrome/browser/BookmarksBridge;

    invoke-virtual {v0, p1}, Lorg/chromium/chrome/browser/BookmarksBridge;->addObserver(Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkModelObserver;)V

    .line 49
    return-void
.end method

.method public deleteBookmarkItem(Lorg/chromium/components/bookmarks/BookmarkId;)V
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;->mBookmarksBridge:Lorg/chromium/chrome/browser/BookmarksBridge;

    invoke-virtual {v0, p1}, Lorg/chromium/chrome/browser/BookmarksBridge;->deleteBookmark(Lorg/chromium/components/bookmarks/BookmarkId;)V

    .line 77
    return-void
.end method

.method public destroy()V
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;->mBookmarksBridge:Lorg/chromium/chrome/browser/BookmarksBridge;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/BookmarksBridge;->destroy()V

    .line 40
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;->mEnhancedBookmarksBridge:Lorg/chromium/chrome/browser/EnhancedBookmarksBridge;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/EnhancedBookmarksBridge;->destroy()V

    .line 41
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;->mChromeEnhancedBookmarkBridge:Lcom/google/android/apps/chrome/enhancedbookmark/ChromeEnhancedBookmarkBridge;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/enhancedbookmark/ChromeEnhancedBookmarkBridge;->destroy()V

    .line 42
    return-void
.end method

.method public doesBookmarkExist(Lorg/chromium/components/bookmarks/BookmarkId;)Z
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;->mBookmarksBridge:Lorg/chromium/chrome/browser/BookmarksBridge;

    invoke-virtual {v0, p1}, Lorg/chromium/chrome/browser/BookmarksBridge;->doesBookmarkExist(Lorg/chromium/components/bookmarks/BookmarkId;)Z

    move-result v0

    return v0
.end method

.method public getAllBookmarkIDsOrderedByCreationDate()Ljava/util/List;
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;->mBookmarksBridge:Lorg/chromium/chrome/browser/BookmarksBridge;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/BookmarksBridge;->getAllBookmarkIDsOrderedByCreationDate()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getAllFoldersWithDepths(Ljava/util/List;Ljava/util/List;)V
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;->mBookmarksBridge:Lorg/chromium/chrome/browser/BookmarksBridge;

    invoke-virtual {v0, p1, p2}, Lorg/chromium/chrome/browser/BookmarksBridge;->getAllFoldersWithDepths(Ljava/util/List;Ljava/util/List;)V

    .line 105
    return-void
.end method

.method public getBookmarkById(Lorg/chromium/components/bookmarks/BookmarkId;)Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;->mBookmarksBridge:Lorg/chromium/chrome/browser/BookmarksBridge;

    invoke-virtual {v0, p1}, Lorg/chromium/chrome/browser/BookmarksBridge;->getBookmarkById(Lorg/chromium/components/bookmarks/BookmarkId;)Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;

    move-result-object v0

    return-object v0
.end method

.method public getBookmarkDescription(Lorg/chromium/components/bookmarks/BookmarkId;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 168
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;->mEnhancedBookmarksBridge:Lorg/chromium/chrome/browser/EnhancedBookmarksBridge;

    invoke-virtual {v0, p1}, Lorg/chromium/chrome/browser/EnhancedBookmarksBridge;->getBookmarkDescription(Lorg/chromium/components/bookmarks/BookmarkId;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getChildIDs(Lorg/chromium/components/bookmarks/BookmarkId;ZZ)Ljava/util/List;
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;->mBookmarksBridge:Lorg/chromium/chrome/browser/BookmarksBridge;

    invoke-virtual {v0, p1, p2, p3}, Lorg/chromium/chrome/browser/BookmarksBridge;->getChildIDs(Lorg/chromium/components/bookmarks/BookmarkId;ZZ)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getMobileFolderId()Lorg/chromium/components/bookmarks/BookmarkId;
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;->mBookmarksBridge:Lorg/chromium/chrome/browser/BookmarksBridge;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/BookmarksBridge;->getMobileFolderId()Lorg/chromium/components/bookmarks/BookmarkId;

    move-result-object v0

    return-object v0
.end method

.method public getTopLevelFolderIDs(ZZ)Ljava/util/List;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;->mBookmarksBridge:Lorg/chromium/chrome/browser/BookmarksBridge;

    invoke-virtual {v0, p1, p2}, Lorg/chromium/chrome/browser/BookmarksBridge;->getTopLevelFolderIDs(ZZ)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getTopLevelFolderParentIDs()Ljava/util/List;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;->mBookmarksBridge:Lorg/chromium/chrome/browser/BookmarksBridge;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/BookmarksBridge;->getTopLevelFolderParentIDs()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getUncategorizedBookmarkIDs()Ljava/util/List;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;->mBookmarksBridge:Lorg/chromium/chrome/browser/BookmarksBridge;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/BookmarksBridge;->getUncategorizedBookmarkIDs()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public isBookmarkModelLoaded()Z
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;->mBookmarksBridge:Lorg/chromium/chrome/browser/BookmarksBridge;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/BookmarksBridge;->isBookmarkModelLoaded()Z

    move-result v0

    return v0
.end method

.method public moveBookmark(Lorg/chromium/components/bookmarks/BookmarkId;Lorg/chromium/components/bookmarks/BookmarkId;I)V
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;->mBookmarksBridge:Lorg/chromium/chrome/browser/BookmarksBridge;

    invoke-virtual {v0, p1, p2, p3}, Lorg/chromium/chrome/browser/BookmarksBridge;->moveBookmark(Lorg/chromium/components/bookmarks/BookmarkId;Lorg/chromium/components/bookmarks/BookmarkId;I)V

    .line 162
    return-void
.end method

.method public removeModelObserver(Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkModelObserver;)V
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;->mBookmarksBridge:Lorg/chromium/chrome/browser/BookmarksBridge;

    invoke-virtual {v0, p1}, Lorg/chromium/chrome/browser/BookmarksBridge;->removeObserver(Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkModelObserver;)V

    .line 56
    return-void
.end method

.method public salientImageForUrl(Ljava/lang/String;Lcom/google/android/apps/chrome/enhancedbookmark/ChromeEnhancedBookmarkBridge$SalientImageCallback;)V
    .locals 1

    .prologue
    .line 182
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;->mChromeEnhancedBookmarkBridge:Lcom/google/android/apps/chrome/enhancedbookmark/ChromeEnhancedBookmarkBridge;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/chrome/enhancedbookmark/ChromeEnhancedBookmarkBridge;->salientImageForUrl(Ljava/lang/String;Lcom/google/android/apps/chrome/enhancedbookmark/ChromeEnhancedBookmarkBridge$SalientImageCallback;)V

    .line 183
    return-void
.end method

.method public setBookmarkDescription(Lorg/chromium/components/bookmarks/BookmarkId;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 175
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;->mEnhancedBookmarksBridge:Lorg/chromium/chrome/browser/EnhancedBookmarksBridge;

    invoke-virtual {v0, p1, p2}, Lorg/chromium/chrome/browser/EnhancedBookmarksBridge;->setBookmarkDescription(Lorg/chromium/components/bookmarks/BookmarkId;Ljava/lang/String;)V

    .line 176
    return-void
.end method

.method public setBookmarkTitle(Lorg/chromium/components/bookmarks/BookmarkId;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;->mBookmarksBridge:Lorg/chromium/chrome/browser/BookmarksBridge;

    invoke-virtual {v0, p1, p2}, Lorg/chromium/chrome/browser/BookmarksBridge;->setBookmarkTitle(Lorg/chromium/components/bookmarks/BookmarkId;Ljava/lang/String;)V

    .line 140
    return-void
.end method

.method public setBookmarkUrl(Lorg/chromium/components/bookmarks/BookmarkId;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;->mBookmarksBridge:Lorg/chromium/chrome/browser/BookmarksBridge;

    invoke-virtual {v0, p1, p2}, Lorg/chromium/chrome/browser/BookmarksBridge;->setBookmarkUrl(Lorg/chromium/components/bookmarks/BookmarkId;Ljava/lang/String;)V

    .line 147
    return-void
.end method

.method public undo()V
    .locals 1

    .prologue
    .line 189
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;->mBookmarksBridge:Lorg/chromium/chrome/browser/BookmarksBridge;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/BookmarksBridge;->undo()V

    .line 190
    return-void
.end method
