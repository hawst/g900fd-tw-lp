.class Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$2;
.super Landroid/util/Property;
.source "ToolbarPhone.java"


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;Ljava/lang/Class;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 180
    iput-object p1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$2;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    invoke-direct {p0, p2, p3}, Landroid/util/Property;-><init>(Ljava/lang/Class;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public get(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Ljava/lang/Float;
    .locals 1

    .prologue
    .line 183
    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mOverviewModePercent:F
    invoke-static {p1}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$200(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 180
    check-cast p1, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$2;->get(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public set(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;Ljava/lang/Float;)V
    .locals 2

    .prologue
    .line 188
    invoke-virtual {p2}, Ljava/lang/Float;->floatValue()F

    move-result v0

    # setter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mOverviewModePercent:F
    invoke-static {p1, v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$202(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;F)F

    .line 189
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$2;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mToolbarDelegate:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$300(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$2;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->triggerPaintInvalidate(Lcom/google/android/apps/chrome/compositor/Invalidator$Client;)V

    .line 190
    return-void
.end method

.method public bridge synthetic set(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 180
    check-cast p1, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    check-cast p2, Ljava/lang/Float;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$2;->set(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;Ljava/lang/Float;)V

    return-void
.end method
