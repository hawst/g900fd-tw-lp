.class Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ViewScrollerGestureDetector;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source "EdgeSwipeEventFilter.java"


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;)V
    .locals 0

    .prologue
    .line 335
    iput-object p1, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ViewScrollerGestureDetector;->this$0:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$1;)V
    .locals 0

    .prologue
    .line 335
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ViewScrollerGestureDetector;-><init>(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;)V

    return-void
.end method


# virtual methods
.method public onDoubleTap(Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    .line 358
    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ViewScrollerGestureDetector;->this$0:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mInDoubleTap:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->access$102(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;Z)Z

    .line 359
    const/4 v0, 0x0

    return v0
.end method

.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 338
    iget-object v1, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ViewScrollerGestureDetector;->this$0:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;

    # getter for: Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mInDoubleTap:Z
    invoke-static {v1}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->access$100(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 341
    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ViewScrollerGestureDetector;->this$0:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;

    iget-object v0, v0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mHost:Lcom/google/android/apps/chrome/eventfilter/EventFilterHost;

    invoke-interface {v0, p1}, Lcom/google/android/apps/chrome/eventfilter/EventFilterHost;->propagateEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 345
    :goto_0
    return v0

    .line 343
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ViewScrollerGestureDetector;->this$0:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;

    # getter for: Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mAccumulatedEvents:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->access$200(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 344
    iget-object v1, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ViewScrollerGestureDetector;->this$0:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;

    # setter for: Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mScrollStarted:Z
    invoke-static {v1, v0}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->access$302(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;Z)Z

    goto :goto_0
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 7

    .prologue
    .line 439
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ViewScrollerGestureDetector;->this$0:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;

    iget v1, v1, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mPxToDp:F

    mul-float/2addr v1, v0

    .line 440
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iget-object v2, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ViewScrollerGestureDetector;->this$0:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;

    iget v2, v2, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mPxToDp:F

    mul-float/2addr v2, v0

    .line 441
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v3

    sub-float/2addr v0, v3

    iget-object v3, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ViewScrollerGestureDetector;->this$0:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;

    iget v3, v3, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mPxToDp:F

    mul-float/2addr v3, v0

    .line 442
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v4

    sub-float/2addr v0, v4

    iget-object v4, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ViewScrollerGestureDetector;->this$0:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;

    iget v4, v4, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mPxToDp:F

    mul-float/2addr v4, v0

    .line 443
    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ViewScrollerGestureDetector;->this$0:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;

    iget v0, v0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mPxToDp:F

    mul-float v5, p3, v0

    .line 444
    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ViewScrollerGestureDetector;->this$0:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;

    iget v0, v0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mPxToDp:F

    mul-float v6, p4, v0

    .line 445
    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ViewScrollerGestureDetector;->this$0:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;

    # invokes: Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->flingOccurred(FFFFFF)V
    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->access$1600(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;FFFFFF)V

    .line 446
    const/4 v0, 0x0

    return v0
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 2

    .prologue
    .line 366
    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ViewScrollerGestureDetector;->this$0:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;

    # invokes: Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->propagateAccumulatedEventsAndClear()Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->access$400(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;)Z

    .line 367
    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ViewScrollerGestureDetector;->this$0:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mInLongPress:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->access$502(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;Z)Z

    .line 368
    return-void
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 10

    .prologue
    .line 372
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ViewScrollerGestureDetector;->this$0:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;

    iget v1, v1, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mPxToDp:F

    mul-float/2addr v1, v0

    .line 373
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iget-object v2, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ViewScrollerGestureDetector;->this$0:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;

    iget v2, v2, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mPxToDp:F

    mul-float/2addr v2, v0

    .line 374
    neg-float v0, p3

    iget-object v3, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ViewScrollerGestureDetector;->this$0:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;

    iget v3, v3, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mPxToDp:F

    mul-float/2addr v3, v0

    .line 375
    neg-float v0, p4

    iget-object v4, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ViewScrollerGestureDetector;->this$0:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;

    iget v4, v4, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mPxToDp:F

    mul-float/2addr v4, v0

    .line 376
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v5

    sub-float/2addr v0, v5

    iget-object v5, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ViewScrollerGestureDetector;->this$0:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;

    iget v5, v5, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mPxToDp:F

    mul-float/2addr v5, v0

    .line 377
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v6

    sub-float/2addr v0, v6

    iget-object v6, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ViewScrollerGestureDetector;->this$0:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;

    iget v6, v6, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mPxToDp:F

    mul-float/2addr v6, v0

    .line 379
    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ViewScrollerGestureDetector;->this$0:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;

    # getter for: Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mScrollStarted:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->access$300(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 380
    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ViewScrollerGestureDetector;->this$0:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;

    # invokes: Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->scrollUpdated(FFFFFF)V
    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->access$600(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;FFFFFF)V

    .line 381
    const/4 v0, 0x1

    .line 433
    :goto_0
    return v0

    .line 384
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ViewScrollerGestureDetector;->this$0:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;

    # getter for: Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mScrollDirection:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;
    invoke-static {v0}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->access$700(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;)Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    move-result-object v0

    sget-object v3, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;->DOWN:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    if-eq v0, v3, :cond_1

    const/4 v0, 0x1

    move v3, v0

    .line 385
    :goto_1
    if-eqz v3, :cond_2

    move v0, v5

    :goto_2
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v4

    .line 386
    if-eqz v3, :cond_3

    :goto_3
    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v3

    .line 388
    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ViewScrollerGestureDetector;->this$0:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;

    # getter for: Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mAccumulatingEvents:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->access$800(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 389
    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ViewScrollerGestureDetector;->this$0:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;

    # getter for: Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mAccumulatedEvents:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->access$200(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v5, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ViewScrollerGestureDetector;->this$0:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;

    # getter for: Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mAccumulatedEvents:Ljava/util/ArrayList;
    invoke-static {v5}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->access$200(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;)Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/MotionEvent;

    invoke-virtual {v0}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v6

    .line 391
    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ViewScrollerGestureDetector;->this$0:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;

    # getter for: Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mAccumulatedEvents:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->access$200(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;)Ljava/util/ArrayList;

    move-result-object v0

    const/4 v5, 0x0

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/MotionEvent;

    invoke-virtual {v0}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v8

    .line 392
    sub-long/2addr v6, v8

    .line 396
    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ViewScrollerGestureDetector;->this$0:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;

    # getter for: Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mAccumulateThreshold:F
    invoke-static {v0}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->access$900(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;)F

    move-result v0

    cmpg-float v0, v3, v0

    if-gez v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ViewScrollerGestureDetector;->this$0:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;

    # getter for: Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mSwipeRegion:F
    invoke-static {v0}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->access$1000(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;)F

    move-result v0

    cmpg-float v0, v4, v0

    if-gez v0, :cond_5

    const-wide/16 v8, 0xc8

    cmp-long v0, v6, v8

    if-gtz v0, :cond_5

    .line 399
    const/4 v0, 0x1

    goto :goto_0

    .line 384
    :cond_1
    const/4 v0, 0x0

    move v3, v0

    goto :goto_1

    :cond_2
    move v0, v6

    .line 385
    goto :goto_2

    :cond_3
    move v6, v5

    .line 386
    goto :goto_3

    .line 403
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ViewScrollerGestureDetector;->this$0:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;

    # getter for: Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mAccumulatedEvents:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->access$200(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {p2}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 409
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ViewScrollerGestureDetector;->this$0:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;

    # getter for: Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mAccumulatingEvents:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->access$800(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;)Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ViewScrollerGestureDetector;->this$0:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;

    # getter for: Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mAccumulateThreshold:F
    invoke-static {v0}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->access$900(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;)F

    move-result v0

    cmpg-float v0, v3, v0

    if-gez v0, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ViewScrollerGestureDetector;->this$0:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;

    # getter for: Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mAccumulateThreshold:F
    invoke-static {v0}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->access$900(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;)F

    move-result v0

    cmpg-float v0, v4, v0

    if-gez v0, :cond_6

    .line 412
    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ViewScrollerGestureDetector;->this$0:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mAccumulatingEvents:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->access$802(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;Z)Z

    .line 413
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 416
    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ViewScrollerGestureDetector;->this$0:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;

    # invokes: Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->checkForFastScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;)Z
    invoke-static {v0, p1, p2}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->access$1100(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;Landroid/view/MotionEvent;Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_7

    float-to-double v6, v3

    float-to-double v4, v4

    # getter for: Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->TAN_SIDE_SWIPE_ANGLE_THRESHOLD:D
    invoke-static {}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->access$1200()D

    move-result-wide v8

    mul-double/2addr v4, v8

    cmpl-double v0, v6, v4

    if-lez v0, :cond_9

    .line 420
    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ViewScrollerGestureDetector;->this$0:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;

    # invokes: Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->propagateAccumulatedEventsAndClear()Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->access$400(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;)Z

    .line 421
    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ViewScrollerGestureDetector;->this$0:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;

    sget-object v1, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;->UNKNOWN:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    # setter for: Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mScrollDirection:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->access$702(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;)Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    .line 422
    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ViewScrollerGestureDetector;->this$0:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mPropagateEventsToHostView:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->access$1302(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;Z)Z

    .line 433
    :cond_8
    :goto_4
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 424
    :cond_9
    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ViewScrollerGestureDetector;->this$0:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;

    iget-object v3, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ViewScrollerGestureDetector;->this$0:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;

    # getter for: Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mScrollDirection:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;
    invoke-static {v3}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->access$700(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;)Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    move-result-object v3

    # invokes: Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->scrollStarted(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;FF)V
    invoke-static {v0, v3, v1, v2}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->access$1400(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;FF)V

    .line 425
    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ViewScrollerGestureDetector;->this$0:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mScrollStarted:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->access$302(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;Z)Z

    .line 426
    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ViewScrollerGestureDetector;->this$0:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;

    # getter for: Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mAccumulatedEvents:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->access$200(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 427
    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ViewScrollerGestureDetector;->this$0:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mAccumulatingEvents:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->access$802(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;Z)Z

    .line 428
    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ViewScrollerGestureDetector;->this$0:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;

    # getter for: Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mScrollDirection:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;
    invoke-static {v0}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->access$700(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;)Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;->DOWN:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    if-eq v0, v1, :cond_8

    .line 429
    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ViewScrollerGestureDetector;->this$0:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;

    # getter for: Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->access$1500(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;)Landroid/content/Context;

    move-result-object v0

    const/16 v1, 0x11

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastNotification(Landroid/content/Context;I)V

    goto :goto_4
.end method

.method public onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 350
    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ViewScrollerGestureDetector;->this$0:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;

    # invokes: Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->propagateAccumulatedEventsAndClear()Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->access$400(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;)Z

    .line 351
    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ViewScrollerGestureDetector;->this$0:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;

    iget-object v0, v0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mHost:Lcom/google/android/apps/chrome/eventfilter/EventFilterHost;

    invoke-interface {v0, p1}, Lcom/google/android/apps/chrome/eventfilter/EventFilterHost;->propagateEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method
