.class Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager$1;
.super Lorg/chromium/components/web_contents_delegate_android/WebContentsDelegateAndroid;
.source "ContextualSearchManager.java"


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;)V
    .locals 0

    .prologue
    .line 203
    iput-object p1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager$1;->this$0:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;

    invoke-direct {p0}, Lorg/chromium/components/web_contents_delegate_android/WebContentsDelegateAndroid;-><init>()V

    return-void
.end method


# virtual methods
.method public onLoadProgressChanged(I)V
    .locals 1

    .prologue
    .line 228
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager$1;->this$0:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;

    # getter for: Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mLayoutDelegate:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchLayoutDelegate;
    invoke-static {v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->access$000(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;)Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchLayoutDelegate;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchLayoutDelegate;->getContextualSearchState()Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->setProgressBarCompletion(I)V

    .line 229
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager$1;->this$0:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;

    # getter for: Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mLayoutDelegate:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchLayoutDelegate;
    invoke-static {v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->access$000(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;)Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchLayoutDelegate;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchLayoutDelegate;->refresh()V

    .line 230
    return-void
.end method

.method public onLoadStarted()V
    .locals 2

    .prologue
    .line 206
    invoke-super {p0}, Lorg/chromium/components/web_contents_delegate_android/WebContentsDelegateAndroid;->onLoadStarted()V

    .line 207
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager$1;->this$0:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;

    # getter for: Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mLayoutDelegate:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchLayoutDelegate;
    invoke-static {v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->access$000(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;)Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchLayoutDelegate;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchLayoutDelegate;->getContextualSearchState()Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->setProgressBarCompletion(I)V

    .line 208
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager$1;->this$0:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;

    # getter for: Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mLayoutDelegate:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchLayoutDelegate;
    invoke-static {v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->access$000(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;)Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchLayoutDelegate;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchLayoutDelegate;->getContextualSearchState()Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->setProgressBarVisible(Z)V

    .line 209
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager$1;->this$0:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;

    # getter for: Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mLayoutDelegate:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchLayoutDelegate;
    invoke-static {v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->access$000(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;)Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchLayoutDelegate;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchLayoutDelegate;->refresh()V

    .line 210
    return-void
.end method

.method public onLoadStopped()V
    .locals 4

    .prologue
    .line 214
    invoke-super {p0}, Lorg/chromium/components/web_contents_delegate_android/WebContentsDelegateAndroid;->onLoadStopped()V

    .line 217
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager$1$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager$1$1;-><init>(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager$1;)V

    const-wide/16 v2, 0x40

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 224
    return-void
.end method
