.class Lcom/google/android/apps/chrome/preferences/FontSizePreview$FontSizeObserver;
.super Ljava/lang/Object;
.source "FontSizePreview.java"

# interfaces
.implements Lorg/chromium/chrome/browser/accessibility/FontSizePrefs$Observer;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/preferences/FontSizePreview;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/chrome/preferences/FontSizePreview;)V
    .locals 0

    .prologue
    .line 27
    iput-object p1, p0, Lcom/google/android/apps/chrome/preferences/FontSizePreview$FontSizeObserver;->this$0:Lcom/google/android/apps/chrome/preferences/FontSizePreview;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/chrome/preferences/FontSizePreview;Lcom/google/android/apps/chrome/preferences/FontSizePreview$1;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/preferences/FontSizePreview$FontSizeObserver;-><init>(Lcom/google/android/apps/chrome/preferences/FontSizePreview;)V

    return-void
.end method


# virtual methods
.method public onChangeFontSize(F)V
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/FontSizePreview$FontSizeObserver;->this$0:Lcom/google/android/apps/chrome/preferences/FontSizePreview;

    # invokes: Lcom/google/android/apps/chrome/preferences/FontSizePreview;->updatePreview()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/preferences/FontSizePreview;->access$000(Lcom/google/android/apps/chrome/preferences/FontSizePreview;)V

    .line 31
    return-void
.end method

.method public onChangeForceEnableZoom(Z)V
    .locals 0

    .prologue
    .line 34
    return-void
.end method

.method public onChangeUserSetForceEnableZoom(Z)V
    .locals 0

    .prologue
    .line 37
    return-void
.end method
