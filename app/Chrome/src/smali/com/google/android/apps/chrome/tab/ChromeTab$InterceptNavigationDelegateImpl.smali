.class Lcom/google/android/apps/chrome/tab/ChromeTab$InterceptNavigationDelegateImpl;
.super Ljava/lang/Object;
.source "ChromeTab.java"

# interfaces
.implements Lorg/chromium/components/navigation_interception/InterceptNavigationDelegate;


# instance fields
.field final mAuthenticatorHelper:Lcom/google/android/apps/chrome/tab/AuthenticatorHelper;

.field final mUrlHandler:Lcom/google/android/apps/chrome/UrlHandler;

.field final synthetic this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/chrome/tab/ChromeTab;)V
    .locals 3

    .prologue
    .line 2830
    iput-object p1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$InterceptNavigationDelegateImpl;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2831
    new-instance v0, Lcom/google/android/apps/chrome/UrlHandler;

    iget-object v1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$InterceptNavigationDelegateImpl;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # getter for: Lcom/google/android/apps/chrome/tab/ChromeTab;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;
    invoke-static {v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$900(Lcom/google/android/apps/chrome/tab/ChromeTab;)Lcom/google/android/apps/chrome/ChromeActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/chrome/UrlHandler;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$InterceptNavigationDelegateImpl;->mUrlHandler:Lcom/google/android/apps/chrome/UrlHandler;

    .line 2832
    new-instance v0, Lcom/google/android/apps/chrome/tab/AuthenticatorHelper;

    iget-object v1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$InterceptNavigationDelegateImpl;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    iget-object v2, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$InterceptNavigationDelegateImpl;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getWindowAndroid()Lorg/chromium/ui/base/WindowAndroid;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/chrome/tab/AuthenticatorHelper;-><init>(Lcom/google/android/apps/chrome/tab/ChromeTab;Lorg/chromium/ui/base/WindowAndroid;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$InterceptNavigationDelegateImpl;->mAuthenticatorHelper:Lcom/google/android/apps/chrome/tab/AuthenticatorHelper;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/chrome/tab/ChromeTab;Lcom/google/android/apps/chrome/tab/ChromeTab$1;)V
    .locals 0

    .prologue
    .line 2830
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/tab/ChromeTab$InterceptNavigationDelegateImpl;-><init>(Lcom/google/android/apps/chrome/tab/ChromeTab;)V

    return-void
.end method

.method private getReferrerUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2865
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$InterceptNavigationDelegateImpl;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$InterceptNavigationDelegateImpl;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->getWebContents()Lorg/chromium/content_public/browser/WebContents;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2866
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$InterceptNavigationDelegateImpl;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->getWebContents()Lorg/chromium/content_public/browser/WebContents;

    move-result-object v0

    invoke-interface {v0}, Lorg/chromium/content_public/browser/WebContents;->getNavigationController()Lorg/chromium/content_public/browser/NavigationController;

    move-result-object v0

    invoke-interface {v0}, Lorg/chromium/content_public/browser/NavigationController;->getOriginalUrlForVisibleNavigationEntry()Ljava/lang/String;

    move-result-object v0

    .line 2869
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public shouldIgnoreNavigation(Lorg/chromium/components/navigation_interception/NavigationParams;)Z
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 2843
    iget-object v1, p1, Lorg/chromium/components/navigation_interception/NavigationParams;->url:Ljava/lang/String;

    .line 2845
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$InterceptNavigationDelegateImpl;->mAuthenticatorHelper:Lcom/google/android/apps/chrome/tab/AuthenticatorHelper;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/tab/AuthenticatorHelper;->handleAuthenticatorUrl(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v7

    .line 2856
    :goto_0
    return v0

    .line 2847
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$InterceptNavigationDelegateImpl;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # getter for: Lcom/google/android/apps/chrome/tab/ChromeTab;->mTabRedirectHandler:Lcom/google/android/apps/chrome/tab/TabRedirectHandler;
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$6500(Lcom/google/android/apps/chrome/tab/ChromeTab;)Lcom/google/android/apps/chrome/tab/TabRedirectHandler;

    move-result-object v0

    iget v2, p1, Lorg/chromium/components/navigation_interception/NavigationParams;->pageTransitionType:I

    iget-object v3, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$InterceptNavigationDelegateImpl;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # getter for: Lcom/google/android/apps/chrome/tab/ChromeTab;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;
    invoke-static {v3}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$900(Lcom/google/android/apps/chrome/tab/ChromeTab;)Lcom/google/android/apps/chrome/ChromeActivity;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/ChromeActivity;->getLastUserInteractionTime()J

    move-result-wide v4

    invoke-virtual {v0, v2, v4, v5}, Lcom/google/android/apps/chrome/tab/TabRedirectHandler;->applyNewUrlLoading(IJ)V

    .line 2850
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$InterceptNavigationDelegateImpl;->mUrlHandler:Lcom/google/android/apps/chrome/UrlHandler;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab$InterceptNavigationDelegateImpl;->getReferrerUrl()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$InterceptNavigationDelegateImpl;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tab/ChromeTab;->isIncognito()Z

    move-result v3

    iget v4, p1, Lorg/chromium/components/navigation_interception/NavigationParams;->pageTransitionType:I

    iget-boolean v5, p1, Lorg/chromium/components/navigation_interception/NavigationParams;->isRedirect:Z

    iget-object v6, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$InterceptNavigationDelegateImpl;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # getter for: Lcom/google/android/apps/chrome/tab/ChromeTab;->mTabRedirectHandler:Lcom/google/android/apps/chrome/tab/TabRedirectHandler;
    invoke-static {v6}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$6500(Lcom/google/android/apps/chrome/tab/ChromeTab;)Lcom/google/android/apps/chrome/tab/TabRedirectHandler;

    move-result-object v6

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/apps/chrome/UrlHandler;->shouldOverrideUrlLoading(Ljava/lang/String;Ljava/lang/String;ZIZLcom/google/android/apps/chrome/tab/TabRedirectHandler;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2853
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$InterceptNavigationDelegateImpl;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->onOverrideUrlLoading()V

    move v0, v7

    .line 2854
    goto :goto_0

    .line 2856
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public shouldIgnoreNewTab(Ljava/lang/String;Z)Z
    .locals 1

    .prologue
    .line 2836
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$InterceptNavigationDelegateImpl;->mAuthenticatorHelper:Lcom/google/android/apps/chrome/tab/AuthenticatorHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/tab/AuthenticatorHelper;->handleAuthenticatorUrl(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 2838
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$InterceptNavigationDelegateImpl;->mUrlHandler:Lcom/google/android/apps/chrome/UrlHandler;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/chrome/UrlHandler;->shouldOverrideNewTab(Ljava/lang/String;Z)Z

    move-result v0

    goto :goto_0
.end method
