.class public Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkColor;
.super Ljava/lang/Object;
.source "EnhancedBookmarkColor.java"


# static fields
.field private static final DEFAULT_BACKGROUND_COLORS:[I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/16 v5, 0xff

    .line 18
    const/4 v0, 0x5

    new-array v0, v0, [I

    const/4 v1, 0x0

    const/16 v2, 0xec

    const/16 v3, 0x40

    const/16 v4, 0x7a

    invoke-static {v5, v2, v3, v4}, Landroid/graphics/Color;->argb(IIII)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x1

    const/16 v2, 0x29

    const/16 v3, 0xb6

    const/16 v4, 0xf6

    invoke-static {v5, v2, v3, v4}, Landroid/graphics/Color;->argb(IIII)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x2

    const/16 v2, 0xf7

    const/16 v3, 0xcb

    const/16 v4, 0x4d

    invoke-static {v5, v2, v3, v4}, Landroid/graphics/Color;->argb(IIII)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x3

    const/16 v2, 0x5c

    const/16 v3, 0x6b

    const/16 v4, 0xc0

    invoke-static {v5, v2, v3, v4}, Landroid/graphics/Color;->argb(IIII)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x4

    const/16 v2, 0x26

    const/16 v3, 0xa6

    const/16 v4, 0x9a

    invoke-static {v5, v2, v3, v4}, Landroid/graphics/Color;->argb(IIII)I

    move-result v2

    aput v2, v0, v1

    sput-object v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkColor;->DEFAULT_BACKGROUND_COLORS:[I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static generateBackgroundColor(Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;)I
    .locals 2

    .prologue
    .line 33
    invoke-virtual {p0}, Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;->getUrl()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    sget-object v1, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkColor;->DEFAULT_BACKGROUND_COLORS:[I

    array-length v1, v1

    invoke-static {v0, v1}, Lorg/chromium/chrome/browser/util/MathUtils;->positiveModulo(II)I

    move-result v0

    .line 35
    sget-object v1, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkColor;->DEFAULT_BACKGROUND_COLORS:[I

    aget v0, v1, v0

    return v0
.end method
