.class public Lcom/google/android/apps/chrome/snapshot/gcm/GcmReceiver;
.super Lcom/google/ipc/invalidation/external/client/contrib/MultiplexingGcmListener$AbstractListener;
.source "GcmReceiver.java"


# static fields
.field public static final CPS_PRINTER_ID_KEY:Ljava/lang/String; = "printer_id"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 52
    const-string/jumbo v0, "GcmReceiver"

    invoke-direct {p0, v0}, Lcom/google/ipc/invalidation/external/client/contrib/MultiplexingGcmListener$AbstractListener;-><init>(Ljava/lang/String;)V

    .line 53
    return-void
.end method

.method private static parsePrintJobJobData(Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob$JobData;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 92
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 99
    :cond_0
    :goto_0
    return-object v0

    .line 96
    :cond_1
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJobParser;->parsePrintJobJobData(Lorg/json/JSONObject;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob$JobData;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 98
    :catch_0
    move-exception v1

    const-string/jumbo v1, "GcmReceiver"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Unable to parse jobdata: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method protected onDeletedMessages(I)V
    .locals 2

    .prologue
    .line 106
    const-string/jumbo v0, "GcmReceiver"

    const-string/jumbo v1, "Got deleted messages notification from GCM frontend"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 107
    invoke-static {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->createSyncStateWithServerIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/snapshot/gcm/GcmReceiver;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 108
    return-void
.end method

.method protected onMessage(Landroid/content/Intent;)V
    .locals 9

    .prologue
    const/4 v4, 0x0

    .line 57
    const-string/jumbo v0, "job_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "email"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 71
    :cond_0
    :goto_0
    return-void

    .line 61
    :cond_1
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "email"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 62
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "job_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 63
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v2, "job_data"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 64
    const-string/jumbo v0, "GcmReceiver"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Messaging request received for job_id = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 65
    new-instance v0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob;

    const-wide/16 v2, -0x1

    invoke-static {v5}, Lcom/google/android/apps/chrome/snapshot/gcm/GcmReceiver;->parsePrintJobJobData(Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob$JobData;

    move-result-object v7

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob;-><init>(Ljava/lang/String;JLjava/lang/String;Landroid/net/Uri;Ljava/lang/String;Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob$JobData;)V

    .line 67
    invoke-static {v8}, Lorg/chromium/sync/signin/AccountManagerHelper;->createAccountFromName(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v1

    .line 68
    invoke-static {p0, v1, v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->createPrintJobFromC2DMNotificationIntent(Landroid/content/Context;Landroid/accounts/Account;Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob;)Landroid/content/Intent;

    move-result-object v0

    .line 70
    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/snapshot/gcm/GcmReceiver;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0
.end method

.method public onRegistered(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 75
    const-string/jumbo v0, "GcmReceiver"

    const-string/jumbo v1, "Got successful registration from GCM frontend"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 76
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->setNeedsUpdating(Landroid/content/Context;Z)V

    .line 77
    invoke-static {p0, p1}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->createSyncStateWithServerWithC2dmRegistrationIdIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 80
    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/snapshot/gcm/GcmReceiver;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 81
    return-void
.end method

.method public onUnregistered(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 85
    const-string/jumbo v0, "GcmReceiver"

    const-string/jumbo v1, "Got successful unregistration from GCM frontend"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 86
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->setNeedsUpdating(Landroid/content/Context;Z)V

    .line 87
    invoke-static {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->createSyncStateWithServerIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 88
    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/snapshot/gcm/GcmReceiver;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 89
    return-void
.end method
