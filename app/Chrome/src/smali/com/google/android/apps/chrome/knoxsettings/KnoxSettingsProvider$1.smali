.class final Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider$1;
.super Landroid/database/ContentObserver;
.source "KnoxSettingsProvider.java"


# instance fields
.field final synthetic val$obs:Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider$Observer;


# direct methods
.method constructor <init>(Landroid/os/Handler;Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider$Observer;)V
    .locals 0

    .prologue
    .line 230
    iput-object p2, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider$1;->val$obs:Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider$Observer;

    invoke-direct {p0, p1}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public final onChange(ZLandroid/net/Uri;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 235
    invoke-virtual {p2}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v1

    .line 237
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v3, :cond_1

    .line 238
    :cond_0
    const-string/jumbo v0, "KnoxSettingsProvider"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Observed change for unknown URI "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 301
    :goto_0
    return-void

    .line 242
    :cond_1
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 243
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-ne v2, v3, :cond_2

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 249
    :goto_1
    const-string/jumbo v2, "ADMIN_REMOVED"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 250
    iget-object v0, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider$1;->val$obs:Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider$Observer;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider$Observer;->onDeviceAdminRemoved()V

    goto :goto_0

    .line 243
    :cond_2
    const-string/jumbo v1, ""

    goto :goto_1

    .line 252
    :cond_3
    const-string/jumbo v2, "BrowserPolicy"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 253
    const-string/jumbo v0, "getHttpProxy"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 254
    iget-object v0, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider$1;->val$obs:Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider$Observer;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider$Observer;->onHttpProxyHostPortChanged()V

    goto :goto_0

    .line 256
    :cond_4
    const-string/jumbo v0, "getAutoFillSetting"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 257
    iget-object v0, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider$1;->val$obs:Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider$Observer;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider$Observer;->onAutofillEnabledChanged()V

    goto :goto_0

    .line 259
    :cond_5
    const-string/jumbo v0, "getCookiesSetting"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 260
    iget-object v0, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider$1;->val$obs:Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider$Observer;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider$Observer;->onCookiesEnabledChanged()V

    goto :goto_0

    .line 262
    :cond_6
    const-string/jumbo v0, "getJavaScriptSetting"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 263
    iget-object v0, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider$1;->val$obs:Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider$Observer;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider$Observer;->onJavascriptEnabledChanged()V

    goto :goto_0

    .line 265
    :cond_7
    const-string/jumbo v0, "getPopupsSetting"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 266
    iget-object v0, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider$1;->val$obs:Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider$Observer;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider$Observer;->onPopupsEnabledChanged()V

    goto :goto_0

    .line 269
    :cond_8
    const-string/jumbo v2, "SmartCardBrowserPolicy"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 270
    const-string/jumbo v0, "isAuthenticationEnabled"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 271
    iget-object v0, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider$1;->val$obs:Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider$Observer;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider$Observer;->onSmartCardAuthenticationEnabledChanged()V

    goto/16 :goto_0

    .line 273
    :cond_9
    const-string/jumbo v0, "getClientCertificateAlias"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 274
    iget-object v0, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider$1;->val$obs:Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider$Observer;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider$Observer;->onClientCertificateAliasChanged()V

    goto/16 :goto_0

    .line 277
    :cond_a
    const-string/jumbo v2, "FirewallPolicy"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 278
    const-string/jumbo v0, "getURLFilterList"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 279
    iget-object v0, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider$1;->val$obs:Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider$Observer;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider$Observer;->onUrlBlacklistChanged()V

    goto/16 :goto_0

    .line 281
    :cond_b
    const-string/jumbo v0, "getURLFilterEnabled"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 282
    iget-object v0, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider$1;->val$obs:Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider$Observer;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider$Observer;->onUrlFilterEnabledChanged()V

    goto/16 :goto_0

    .line 284
    :cond_c
    const-string/jumbo v0, "getURLFilterReportEnabled"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 285
    iget-object v0, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider$1;->val$obs:Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider$Observer;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider$Observer;->onUrlFilterReportEnabledChanged()V

    goto/16 :goto_0

    .line 288
    :cond_d
    const-string/jumbo v2, "CertificatePolicy"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_e

    .line 289
    const-string/jumbo v0, "certificateRemoved"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 290
    iget-object v0, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider$1;->val$obs:Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider$Observer;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider$Observer;->onCertificateRemoved()V

    goto/16 :goto_0

    .line 293
    :cond_e
    const-string/jumbo v2, "AuditLog"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 294
    const-string/jumbo v0, "isAuditLogEnabled"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 295
    iget-object v0, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider$1;->val$obs:Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider$Observer;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider$Observer;->onAuditLogEnabledChanged()V

    goto/16 :goto_0

    .line 300
    :cond_f
    const-string/jumbo v0, "KnoxSettingsProvider"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Observed change for unknown URI "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method
