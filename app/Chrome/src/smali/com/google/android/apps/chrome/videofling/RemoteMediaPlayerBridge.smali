.class public Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerBridge;
.super Lorg/chromium/media/MediaPlayerBridge;
.source "RemoteMediaPlayerBridge.java"

# interfaces
.implements Lcom/google/android/apps/chrome/videofling/MediaRouteController$Listener;


# instance fields
.field private final mNativePlayerId:I

.field private final mNativeRemoteMediaPlayerBridge:J

.field private mOnCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

.field private mOnErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

.field private mOnPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

.field private mOnSeekCompleteListener:Landroid/media/MediaPlayer$OnSeekCompleteListener;

.field private final mRouteController:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

.field private final mStartPositionMillis:J

.field private final mTabId:I


# direct methods
.method private constructor <init>(JIIJ)V
    .locals 5

    .prologue
    .line 37
    invoke-direct {p0}, Lorg/chromium/media/MediaPlayerBridge;-><init>()V

    .line 38
    iput-wide p1, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerBridge;->mNativeRemoteMediaPlayerBridge:J

    .line 39
    iput-wide p5, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerBridge;->mStartPositionMillis:J

    .line 40
    iput p3, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerBridge;->mTabId:I

    .line 41
    iput p4, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerBridge;->mNativePlayerId:I

    .line 42
    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->instance(J)Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;

    move-result-object v0

    iget-wide v2, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerBridge;->mNativeRemoteMediaPlayerBridge:J

    invoke-direct {p0, v2, v3}, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerBridge;->nativeGetFrameUrl(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->getMediaRouteController(Ljava/lang/String;)Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerBridge;->mRouteController:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    .line 44
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerBridge;->mRouteController:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->addListener(Lcom/google/android/apps/chrome/videofling/MediaRouteController$Listener;)V

    .line 45
    return-void
.end method

.method private static create(JIIJ)Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerBridge;
    .locals 8

    .prologue
    .line 50
    new-instance v1, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerBridge;

    move-wide v2, p0

    move v4, p2

    move v5, p3

    move-wide v6, p4

    invoke-direct/range {v1 .. v7}, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerBridge;-><init>(JIIJ)V

    return-object v1
.end method

.method private native nativeGetFrameUrl(J)Ljava/lang/String;
.end method

.method private native nativeOnPaused(J)V
.end method

.method private native nativeOnPlaying(J)V
.end method


# virtual methods
.method protected getAllowedOperations()Lorg/chromium/media/MediaPlayerBridge$AllowedOperations;
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 157
    new-instance v0, Lorg/chromium/media/MediaPlayerBridge$AllowedOperations;

    invoke-direct {v0, v1, v1, v1}, Lorg/chromium/media/MediaPlayerBridge$AllowedOperations;-><init>(ZZZ)V

    return-object v0
.end method

.method protected getCurrentPosition()I
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerBridge;->mRouteController:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->getPosition()I

    move-result v0

    return v0
.end method

.method protected getDuration()I
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerBridge;->mRouteController:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->getDuration()I

    move-result v0

    return v0
.end method

.method protected getLocalPlayer()Landroid/media/MediaPlayer;
    .locals 1

    .prologue
    .line 56
    const/4 v0, 0x0

    return-object v0
.end method

.method protected isPlaying()Z
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerBridge;->mRouteController:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->isPlaying()Z

    move-result v0

    return v0
.end method

.method public onCompleted()V
    .locals 2

    .prologue
    .line 178
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerBridge;->mOnCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

    if-eqz v0, :cond_0

    .line 179
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerBridge;->mOnCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/media/MediaPlayer$OnCompletionListener;->onCompletion(Landroid/media/MediaPlayer;)V

    .line 181
    :cond_0
    return-void
.end method

.method public onDurationUpdated(I)V
    .locals 0

    .prologue
    .line 211
    return-void
.end method

.method public onError(ILjava/lang/String;)V
    .locals 4

    .prologue
    .line 162
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerBridge;->mOnErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    if-eqz v0, :cond_0

    .line 164
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-lt v0, v1, :cond_1

    const/16 v0, -0x6e

    .line 166
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerBridge;->mOnErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-interface {v1, v2, v3, v0}, Landroid/media/MediaPlayer$OnErrorListener;->onError(Landroid/media/MediaPlayer;II)Z

    .line 168
    :cond_0
    return-void

    .line 164
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onPlaybackStateChanged(Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;Lorg/chromium/chrome/browser/Tab;I)V
    .locals 2

    .prologue
    .line 201
    sget-object v0, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;->FINISHED:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    if-eq p2, v0, :cond_0

    sget-object v0, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;->INVALIDATED:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    if-ne p2, v0, :cond_2

    .line 202
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerBridge;->onCompleted()V

    .line 208
    :cond_1
    :goto_0
    return-void

    .line 203
    :cond_2
    sget-object v0, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;->PLAYING:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    if-ne p2, v0, :cond_3

    .line 204
    iget-wide v0, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerBridge;->mNativeRemoteMediaPlayerBridge:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerBridge;->nativeOnPlaying(J)V

    goto :goto_0

    .line 205
    :cond_3
    sget-object v0, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;->PAUSED:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    if-ne p2, v0, :cond_1

    .line 206
    iget-wide v0, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerBridge;->mNativeRemoteMediaPlayerBridge:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerBridge;->nativeOnPaused(J)V

    goto :goto_0
.end method

.method public onPositionChanged(I)V
    .locals 0

    .prologue
    .line 214
    return-void
.end method

.method public onPrepared(Lcom/google/android/apps/chrome/videofling/MediaRouteController;)V
    .locals 2

    .prologue
    .line 185
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerBridge;->mOnPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

    if-eqz v0, :cond_0

    .line 186
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerBridge;->mOnPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/media/MediaPlayer$OnPreparedListener;->onPrepared(Landroid/media/MediaPlayer;)V

    .line 188
    :cond_0
    return-void
.end method

.method public onRouteSelected(Ljava/lang/String;Lorg/chromium/chrome/browser/Tab;ILcom/google/android/apps/chrome/videofling/MediaRouteController;)V
    .locals 0

    .prologue
    .line 192
    return-void
.end method

.method public onRouteUnselected(Lorg/chromium/chrome/browser/Tab;ILcom/google/android/apps/chrome/videofling/MediaRouteController;)V
    .locals 0

    .prologue
    .line 196
    return-void
.end method

.method public onSeekCompleted()V
    .locals 2

    .prologue
    .line 172
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerBridge;->mOnSeekCompleteListener:Landroid/media/MediaPlayer$OnSeekCompleteListener;

    if-eqz v0, :cond_0

    .line 173
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerBridge;->mOnSeekCompleteListener:Landroid/media/MediaPlayer$OnSeekCompleteListener;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/media/MediaPlayer$OnSeekCompleteListener;->onSeekComplete(Landroid/media/MediaPlayer;)V

    .line 175
    :cond_0
    return-void
.end method

.method public onTitleChanged(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 217
    return-void
.end method

.method protected pause()V
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerBridge;->mRouteController:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->pause()V

    .line 112
    return-void
.end method

.method protected prepareAsync()Z
    .locals 4

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerBridge;->mRouteController:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    iget-wide v2, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerBridge;->mNativeRemoteMediaPlayerBridge:J

    invoke-direct {p0, v2, v3}, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerBridge;->nativeGetFrameUrl(J)Ljava/lang/String;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerBridge;->mStartPositionMillis:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->prepareAsync(Ljava/lang/String;J)V

    .line 68
    const/4 v0, 0x1

    return v0
.end method

.method protected release()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 91
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerBridge;->mRouteController:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->removeListener(Lcom/google/android/apps/chrome/videofling/MediaRouteController$Listener;)V

    .line 94
    iput-object v1, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerBridge;->mOnCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

    .line 95
    iput-object v1, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerBridge;->mOnSeekCompleteListener:Landroid/media/MediaPlayer$OnSeekCompleteListener;

    .line 96
    iput-object v1, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerBridge;->mOnErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    .line 97
    iput-object v1, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerBridge;->mOnPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

    .line 98
    return-void
.end method

.method protected seekTo(I)V
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerBridge;->mRouteController:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->seekTo(I)V

    .line 117
    return-void
.end method

.method protected setDataSource(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Z
    .locals 2

    .prologue
    .line 122
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerBridge;->mRouteController:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1, p3}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->setDataSource(Landroid/net/Uri;Ljava/lang/String;)V

    .line 123
    const/4 v0, 0x1

    return v0
.end method

.method protected setOnBufferingUpdateListener(Landroid/media/MediaPlayer$OnBufferingUpdateListener;)V
    .locals 0

    .prologue
    .line 128
    return-void
.end method

.method protected setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V
    .locals 0

    .prologue
    .line 132
    iput-object p1, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerBridge;->mOnCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

    .line 133
    return-void
.end method

.method protected setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V
    .locals 0

    .prologue
    .line 142
    iput-object p1, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerBridge;->mOnErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    .line 143
    return-void
.end method

.method protected setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V
    .locals 0

    .prologue
    .line 147
    iput-object p1, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerBridge;->mOnPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

    .line 148
    return-void
.end method

.method protected setOnSeekCompleteListener(Landroid/media/MediaPlayer$OnSeekCompleteListener;)V
    .locals 0

    .prologue
    .line 137
    iput-object p1, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerBridge;->mOnSeekCompleteListener:Landroid/media/MediaPlayer$OnSeekCompleteListener;

    .line 138
    return-void
.end method

.method protected setOnVideoSizeChangedListener(Landroid/media/MediaPlayer$OnVideoSizeChangedListener;)V
    .locals 0

    .prologue
    .line 152
    return-void
.end method

.method protected setSurface(Landroid/view/Surface;)V
    .locals 0

    .prologue
    .line 62
    return-void
.end method

.method protected setVolume(D)V
    .locals 0

    .prologue
    .line 102
    return-void
.end method

.method protected start()V
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerBridge;->mRouteController:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->resume()V

    .line 107
    return-void
.end method
