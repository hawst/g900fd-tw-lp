.class public Lcom/google/android/apps/chrome/ContextualMenuBar;
.super Ljava/lang/Object;
.source "ContextualMenuBar.java"


# static fields
.field public static final TOP_MARGIN_ANIM_PROPERTY:Landroid/util/Property;


# instance fields
.field private final mActionBarDelegate:Lcom/google/android/apps/chrome/ContextualMenuBar$ActionBarDelegate;

.field private final mContext:Landroid/content/Context;

.field private mCurrentAnimation:Landroid/animation/ObjectAnimator;

.field private mCustomSelectionActionModeCallback:Lcom/google/android/apps/chrome/CustomSelectionActionModeCallback;

.field private mShowingContextualActionBar:Z

.field private mTabStripHeight:F


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 32
    new-instance v0, Lcom/google/android/apps/chrome/ContextualMenuBar$1;

    const-class v1, Ljava/lang/Integer;

    const-string/jumbo v2, "controlTopMargin"

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/chrome/ContextualMenuBar$1;-><init>(Ljava/lang/Class;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/chrome/ContextualMenuBar;->TOP_MARGIN_ANIM_PROPERTY:Landroid/util/Property;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/chrome/ContextualMenuBar$ActionBarDelegate;)V
    .locals 2

    .prologue
    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/ContextualMenuBar;->mCurrentAnimation:Landroid/animation/ObjectAnimator;

    .line 26
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/ContextualMenuBar;->mShowingContextualActionBar:Z

    .line 82
    iput-object p2, p0, Lcom/google/android/apps/chrome/ContextualMenuBar;->mActionBarDelegate:Lcom/google/android/apps/chrome/ContextualMenuBar$ActionBarDelegate;

    .line 83
    iput-object p1, p0, Lcom/google/android/apps/chrome/ContextualMenuBar;->mContext:Landroid/content/Context;

    .line 84
    iget-object v0, p0, Lcom/google/android/apps/chrome/ContextualMenuBar;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$dimen;->tab_strip_height:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/ContextualMenuBar;->mTabStripHeight:F

    .line 85
    return-void
.end method

.method static synthetic access$002(Lcom/google/android/apps/chrome/ContextualMenuBar;Landroid/animation/ObjectAnimator;)Landroid/animation/ObjectAnimator;
    .locals 0

    .prologue
    .line 20
    iput-object p1, p0, Lcom/google/android/apps/chrome/ContextualMenuBar;->mCurrentAnimation:Landroid/animation/ObjectAnimator;

    return-object p1
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/ContextualMenuBar;)Lcom/google/android/apps/chrome/ContextualMenuBar$ActionBarDelegate;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/google/android/apps/chrome/ContextualMenuBar;->mActionBarDelegate:Lcom/google/android/apps/chrome/ContextualMenuBar$ActionBarDelegate;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/ContextualMenuBar;)I
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/google/android/apps/chrome/ContextualMenuBar;->queryCurrentActionBarHeight()I

    move-result v0

    return v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/chrome/ContextualMenuBar;)F
    .locals 1

    .prologue
    .line 20
    iget v0, p0, Lcom/google/android/apps/chrome/ContextualMenuBar;->mTabStripHeight:F

    return v0
.end method

.method private queryCurrentActionBarHeight()I
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 116
    iget-object v0, p0, Lcom/google/android/apps/chrome/ContextualMenuBar;->mActionBarDelegate:Lcom/google/android/apps/chrome/ContextualMenuBar$ActionBarDelegate;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/ContextualMenuBar$ActionBarDelegate;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 117
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/ActionBar;->getHeight()I

    move-result v0

    .line 123
    :goto_0
    return v0

    .line 119
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/ContextualMenuBar;->mContext:Landroid/content/Context;

    const/4 v1, 0x1

    new-array v1, v1, [I

    const v2, 0x10102eb

    aput v2, v1, v3

    invoke-virtual {v0, v1}, Landroid/content/Context;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 121
    invoke-virtual {v1, v3, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    .line 122
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    goto :goto_0
.end method


# virtual methods
.method public getActionBarDelegate()Lcom/google/android/apps/chrome/ContextualMenuBar$ActionBarDelegate;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/google/android/apps/chrome/ContextualMenuBar;->mActionBarDelegate:Lcom/google/android/apps/chrome/ContextualMenuBar$ActionBarDelegate;

    return-object v0
.end method

.method public getCustomSelectionActionModeCallback()Lcom/google/android/apps/chrome/CustomSelectionActionModeCallback;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/google/android/apps/chrome/ContextualMenuBar;->mCustomSelectionActionModeCallback:Lcom/google/android/apps/chrome/CustomSelectionActionModeCallback;

    return-object v0
.end method

.method public hideControls()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 172
    iget-object v0, p0, Lcom/google/android/apps/chrome/ContextualMenuBar;->mCurrentAnimation:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/ContextualMenuBar;->mCurrentAnimation:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 174
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/ContextualMenuBar;->mActionBarDelegate:Lcom/google/android/apps/chrome/ContextualMenuBar$ActionBarDelegate;

    sget-object v1, Lcom/google/android/apps/chrome/ContextualMenuBar;->TOP_MARGIN_ANIM_PROPERTY:Landroid/util/Property;

    const/4 v2, 0x1

    new-array v2, v2, [I

    aput v4, v2, v4

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Landroid/util/Property;[I)Landroid/animation/ObjectAnimator;

    move-result-object v0

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/ContextualMenuBar;->mCurrentAnimation:Landroid/animation/ObjectAnimator;

    .line 177
    iget-object v0, p0, Lcom/google/android/apps/chrome/ContextualMenuBar;->mCurrentAnimation:Landroid/animation/ObjectAnimator;

    new-instance v1, Lcom/google/android/apps/chrome/ContextualMenuBar$4;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/ContextualMenuBar$4;-><init>(Lcom/google/android/apps/chrome/ContextualMenuBar;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 185
    iget-object v0, p0, Lcom/google/android/apps/chrome/ContextualMenuBar;->mCurrentAnimation:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 186
    iput-boolean v4, p0, Lcom/google/android/apps/chrome/ContextualMenuBar;->mShowingContextualActionBar:Z

    .line 187
    return-void
.end method

.method public setCustomSelectionActionModeCallback(Lcom/google/android/apps/chrome/CustomSelectionActionModeCallback;)V
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/google/android/apps/chrome/ContextualMenuBar;->mCustomSelectionActionModeCallback:Lcom/google/android/apps/chrome/CustomSelectionActionModeCallback;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 103
    :goto_0
    return-void

    .line 101
    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/chrome/ContextualMenuBar;->mCustomSelectionActionModeCallback:Lcom/google/android/apps/chrome/CustomSelectionActionModeCallback;

    .line 102
    iget-object v0, p0, Lcom/google/android/apps/chrome/ContextualMenuBar;->mCustomSelectionActionModeCallback:Lcom/google/android/apps/chrome/CustomSelectionActionModeCallback;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/chrome/CustomSelectionActionModeCallback;->setContextualMenuBar(Lcom/google/android/apps/chrome/ContextualMenuBar;)V

    goto :goto_0
.end method

.method public showControls()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 139
    iget-object v0, p0, Lcom/google/android/apps/chrome/ContextualMenuBar;->mCurrentAnimation:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/ContextualMenuBar;->mCurrentAnimation:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 141
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/ContextualMenuBar;->mActionBarDelegate:Lcom/google/android/apps/chrome/ContextualMenuBar$ActionBarDelegate;

    sget-object v1, Lcom/google/android/apps/chrome/ContextualMenuBar;->TOP_MARGIN_ANIM_PROPERTY:Landroid/util/Property;

    new-array v2, v7, [I

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/ContextualMenuBar;->queryCurrentActionBarHeight()I

    move-result v5

    int-to-float v5, v5

    iget v6, p0, Lcom/google/android/apps/chrome/ContextualMenuBar;->mTabStripHeight:F

    sub-float/2addr v5, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->max(FF)F

    move-result v4

    float-to-int v4, v4

    aput v4, v2, v3

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Landroid/util/Property;[I)Landroid/animation/ObjectAnimator;

    move-result-object v0

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/ContextualMenuBar;->mCurrentAnimation:Landroid/animation/ObjectAnimator;

    .line 145
    iget-object v0, p0, Lcom/google/android/apps/chrome/ContextualMenuBar;->mCurrentAnimation:Landroid/animation/ObjectAnimator;

    new-instance v1, Lcom/google/android/apps/chrome/ContextualMenuBar$2;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/ContextualMenuBar$2;-><init>(Lcom/google/android/apps/chrome/ContextualMenuBar;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 152
    iget-object v0, p0, Lcom/google/android/apps/chrome/ContextualMenuBar;->mCurrentAnimation:Landroid/animation/ObjectAnimator;

    new-instance v1, Lcom/google/android/apps/chrome/ContextualMenuBar$3;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/ContextualMenuBar$3;-><init>(Lcom/google/android/apps/chrome/ContextualMenuBar;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 163
    iget-object v0, p0, Lcom/google/android/apps/chrome/ContextualMenuBar;->mActionBarDelegate:Lcom/google/android/apps/chrome/ContextualMenuBar$ActionBarDelegate;

    invoke-interface {v0, v7}, Lcom/google/android/apps/chrome/ContextualMenuBar$ActionBarDelegate;->setActionBarBackgroundVisibility(Z)V

    .line 164
    iget-object v0, p0, Lcom/google/android/apps/chrome/ContextualMenuBar;->mCurrentAnimation:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 165
    iput-boolean v7, p0, Lcom/google/android/apps/chrome/ContextualMenuBar;->mShowingContextualActionBar:Z

    .line 166
    return-void
.end method

.method public showControlsOnOrientationChange()V
    .locals 1

    .prologue
    .line 130
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/ContextualMenuBar;->mShowingContextualActionBar:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/ContextualMenuBar;->mCurrentAnimation:Landroid/animation/ObjectAnimator;

    if-nez v0, :cond_0

    .line 131
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ContextualMenuBar;->showControls()V

    .line 133
    :cond_0
    return-void
.end method
