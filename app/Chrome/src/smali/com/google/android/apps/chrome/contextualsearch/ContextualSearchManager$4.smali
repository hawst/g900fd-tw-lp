.class Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager$4;
.super Ljava/lang/Object;
.source "ContextualSearchManager.java"

# interfaces
.implements Lorg/chromium/content_public/browser/JavaScriptCallback;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;)V
    .locals 0

    .prologue
    .line 912
    iput-object p1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager$4;->this$0:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleJavaScriptResult(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 916
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager$4;->this$0:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;

    # getter for: Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mLayoutDelegate:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchLayoutDelegate;
    invoke-static {v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->access$000(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;)Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchLayoutDelegate;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchLayoutDelegate;->setFirstRunFlowContentHeight(F)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 921
    :goto_0
    return-void

    .line 918
    :catch_0
    move-exception v0

    const-string/jumbo v0, "ContextualSearch"

    const-string/jumbo v1, "Could not extract first-run content height, using default value."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
