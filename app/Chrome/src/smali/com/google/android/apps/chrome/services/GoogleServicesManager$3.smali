.class Lcom/google/android/apps/chrome/services/GoogleServicesManager$3;
.super Ljava/lang/Object;
.source "GoogleServicesManager.java"

# interfaces
.implements Lorg/chromium/chrome/browser/signin/SigninManager$SignInFlowObserver;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/services/GoogleServicesManager;

.field final synthetic val$account:Landroid/accounts/Account;

.field final synthetic val$dataTypes:Ljava/util/Set;

.field final synthetic val$states:Lcom/google/android/apps/chrome/services/GoogleServicesStates;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/services/GoogleServicesManager;Lcom/google/android/apps/chrome/services/GoogleServicesStates;Landroid/accounts/Account;Ljava/util/Set;)V
    .locals 0

    .prologue
    .line 339
    iput-object p1, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager$3;->this$0:Lcom/google/android/apps/chrome/services/GoogleServicesManager;

    iput-object p2, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager$3;->val$states:Lcom/google/android/apps/chrome/services/GoogleServicesStates;

    iput-object p3, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager$3;->val$account:Landroid/accounts/Account;

    iput-object p4, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager$3;->val$dataTypes:Ljava/util/Set;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSigninCancelled()V
    .locals 0

    .prologue
    .line 360
    return-void
.end method

.method public onSigninComplete()V
    .locals 3

    .prologue
    .line 342
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager$3;->this$0:Lcom/google/android/apps/chrome/services/GoogleServicesManager;

    iget-object v0, v0, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->get(Landroid/content/Context;)Lorg/chromium/chrome/browser/sync/ProfileSyncService;

    .line 343
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager$3;->this$0:Lcom/google/android/apps/chrome/services/GoogleServicesManager;

    iget-object v0, v0, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->get(Landroid/content/Context;)Lorg/chromium/chrome/browser/sync/ProfileSyncService;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->setSetupInProgress(Z)V

    .line 344
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager$3;->this$0:Lcom/google/android/apps/chrome/services/GoogleServicesManager;

    iget-object v0, v0, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->get(Landroid/content/Context;)Lcom/google/android/apps/chrome/services/GoogleServicesManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager$3;->val$states:Lcom/google/android/apps/chrome/services/GoogleServicesStates;

    iget-object v2, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager$3;->val$account:Landroid/accounts/Account;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->setStates(Lcom/google/android/apps/chrome/services/GoogleServicesStates;Landroid/accounts/Account;)V

    .line 346
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager$3;->val$states:Lcom/google/android/apps/chrome/services/GoogleServicesStates;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/services/GoogleServicesStates;->isSyncEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 347
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager$3;->this$0:Lcom/google/android/apps/chrome/services/GoogleServicesManager;

    iget-object v0, v0, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lorg/chromium/chrome/browser/invalidation/InvalidationController;->get(Landroid/content/Context;)Lorg/chromium/chrome/browser/invalidation/InvalidationController;

    move-result-object v0

    .line 348
    invoke-virtual {v0}, Lorg/chromium/chrome/browser/invalidation/InvalidationController;->start()V

    .line 349
    iget-object v1, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager$3;->val$dataTypes:Ljava/util/Set;

    invoke-virtual {v0, v1}, Lorg/chromium/chrome/browser/invalidation/InvalidationController;->refreshRegisteredTypes(Ljava/util/Set;)V

    .line 351
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager$3;->this$0:Lcom/google/android/apps/chrome/services/GoogleServicesManager;

    iget-object v0, v0, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->get(Landroid/content/Context;)Lorg/chromium/chrome/browser/sync/ProfileSyncService;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->enableSync()V

    .line 352
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager$3;->this$0:Lcom/google/android/apps/chrome/services/GoogleServicesManager;

    iget-object v0, v0, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lorg/chromium/sync/notifier/SyncStatusHelper;->get(Landroid/content/Context;)Lorg/chromium/sync/notifier/SyncStatusHelper;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager$3;->val$account:Landroid/accounts/Account;

    invoke-virtual {v0, v1}, Lorg/chromium/sync/notifier/SyncStatusHelper;->enableAndroidSync(Landroid/accounts/Account;)V

    .line 355
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager$3;->this$0:Lcom/google/android/apps/chrome/services/GoogleServicesManager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->validateAccountSettings(Z)V

    .line 356
    return-void
.end method
