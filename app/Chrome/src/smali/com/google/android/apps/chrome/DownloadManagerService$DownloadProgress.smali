.class Lcom/google/android/apps/chrome/DownloadManagerService$DownloadProgress;
.super Ljava/lang/Object;
.source "DownloadManagerService.java"


# instance fields
.field volatile mDownloadInfo:Lorg/chromium/content/browser/DownloadInfo;

.field volatile mDownloadStatus:Lcom/google/android/apps/chrome/DownloadManagerService$DownloadStatus;

.field final mStartTimeInMillis:J


# direct methods
.method constructor <init>(JLorg/chromium/content/browser/DownloadInfo;Lcom/google/android/apps/chrome/DownloadManagerService$DownloadStatus;)V
    .locals 1

    .prologue
    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    iput-wide p1, p0, Lcom/google/android/apps/chrome/DownloadManagerService$DownloadProgress;->mStartTimeInMillis:J

    .line 73
    iput-object p3, p0, Lcom/google/android/apps/chrome/DownloadManagerService$DownloadProgress;->mDownloadInfo:Lorg/chromium/content/browser/DownloadInfo;

    .line 74
    iput-object p4, p0, Lcom/google/android/apps/chrome/DownloadManagerService$DownloadProgress;->mDownloadStatus:Lcom/google/android/apps/chrome/DownloadManagerService$DownloadStatus;

    .line 75
    return-void
.end method
