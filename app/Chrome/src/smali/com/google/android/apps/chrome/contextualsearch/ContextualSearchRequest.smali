.class public Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchRequest;
.super Ljava/lang/Object;
.source "ContextualSearchRequest.java"


# instance fields
.field private mHasFailedLowPriorityLoad:Z

.field private mIsLowPriority:Z

.field private final mLowPriorityUrl:Ljava/lang/String;

.field private final mNormalPriorityUrl:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 28
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 29
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 4

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    invoke-static {}, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;->getInstance()Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;->getUrlForContextualSearchQuery(Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchRequest;->mNormalPriorityUrl:Ljava/lang/String;

    .line 42
    if-eqz p3, :cond_0

    .line 44
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchRequest;->mNormalPriorityUrl:Ljava/lang/String;

    const-string/jumbo v2, "/search"

    const-string/jumbo v3, "/s"

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "&sns=1"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchRequest;->mLowPriorityUrl:Ljava/lang/String;

    .line 45
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchRequest;->mIsLowPriority:Z

    .line 50
    :goto_0
    return-void

    .line 47
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchRequest;->mIsLowPriority:Z

    .line 48
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchRequest;->mLowPriorityUrl:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public getHasFailed()Z
    .locals 1

    .prologue
    .line 77
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchRequest;->mHasFailedLowPriorityLoad:Z

    return v0
.end method

.method public getSearchUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 85
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchRequest;->mIsLowPriority:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchRequest;->mLowPriorityUrl:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 86
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchRequest;->mLowPriorityUrl:Ljava/lang/String;

    .line 88
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchRequest;->mNormalPriorityUrl:Ljava/lang/String;

    goto :goto_0
.end method

.method public isSearchUrl(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchRequest;->mLowPriorityUrl:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchRequest;->mNormalPriorityUrl:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isUsingLowPriority()Z
    .locals 1

    .prologue
    .line 63
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchRequest;->mIsLowPriority:Z

    return v0
.end method

.method public setHasFailed()V
    .locals 1

    .prologue
    .line 70
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchRequest;->mHasFailedLowPriorityLoad:Z

    .line 71
    return-void
.end method

.method public setNormalPriority()V
    .locals 1

    .prologue
    .line 56
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchRequest;->mIsLowPriority:Z

    .line 57
    return-void
.end method
