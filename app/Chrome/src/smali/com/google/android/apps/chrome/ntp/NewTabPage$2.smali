.class Lcom/google/android/apps/chrome/ntp/NewTabPage$2;
.super Ljava/lang/Object;
.source "NewTabPage.java"

# interfaces
.implements Lcom/google/android/apps/chrome/ntp/NewTabPageView$NewTabPageManager;


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/ntp/NewTabPage;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 130
    const-class v0, Lcom/google/android/apps/chrome/ntp/NewTabPage;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/ntp/NewTabPage$2;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lcom/google/android/apps/chrome/ntp/NewTabPage;)V
    .locals 0

    .prologue
    .line 130
    iput-object p1, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage$2;->this$0:Lcom/google/android/apps/chrome/ntp/NewTabPage;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private recordOpenedMostVisitedItem(Lcom/google/android/apps/chrome/ntp/MostVisitedItem;)V
    .locals 2

    .prologue
    .line 140
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage$2;->this$0:Lcom/google/android/apps/chrome/ntp/NewTabPage;

    # getter for: Lcom/google/android/apps/chrome/ntp/NewTabPage;->mIsDestroyed:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/NewTabPage;->access$100(Lcom/google/android/apps/chrome/ntp/NewTabPage;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 145
    :goto_0
    return-void

    .line 141
    :cond_0
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/NewTabPageUma;->recordAction(I)V

    .line 142
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/ntp/MostVisitedItem;->getIndex()I

    move-result v0

    const/4 v1, 0x6

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->ntpMostVisitedIndex(II)V

    .line 144
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage$2;->this$0:Lcom/google/android/apps/chrome/ntp/NewTabPage;

    # getter for: Lcom/google/android/apps/chrome/ntp/NewTabPage;->mMostVisitedSites:Lorg/chromium/chrome/browser/profiles/MostVisitedSites;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/NewTabPage;->access$400(Lcom/google/android/apps/chrome/ntp/NewTabPage;)Lorg/chromium/chrome/browser/profiles/MostVisitedSites;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/ntp/MostVisitedItem;->getIndex()I

    move-result v1

    invoke-virtual {v0, v1}, Lorg/chromium/chrome/browser/profiles/MostVisitedSites;->recordOpenedMostVisitedItem(I)V

    goto :goto_0
.end method


# virtual methods
.method public focusSearchBox(ZLjava/lang/String;)V
    .locals 1

    .prologue
    .line 222
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage$2;->this$0:Lcom/google/android/apps/chrome/ntp/NewTabPage;

    # getter for: Lcom/google/android/apps/chrome/ntp/NewTabPage;->mIsDestroyed:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/NewTabPage;->access$100(Lcom/google/android/apps/chrome/ntp/NewTabPage;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 230
    :cond_0
    :goto_0
    return-void

    .line 223
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage$2;->this$0:Lcom/google/android/apps/chrome/ntp/NewTabPage;

    # getter for: Lcom/google/android/apps/chrome/ntp/NewTabPage;->mLocationBar:Lcom/google/android/apps/chrome/omnibox/LocationBar;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/NewTabPage;->access$900(Lcom/google/android/apps/chrome/ntp/NewTabPage;)Lcom/google/android/apps/chrome/omnibox/LocationBar;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 224
    if-eqz p1, :cond_2

    .line 225
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage$2;->this$0:Lcom/google/android/apps/chrome/ntp/NewTabPage;

    # getter for: Lcom/google/android/apps/chrome/ntp/NewTabPage;->mLocationBar:Lcom/google/android/apps/chrome/omnibox/LocationBar;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/NewTabPage;->access$900(Lcom/google/android/apps/chrome/ntp/NewTabPage;)Lcom/google/android/apps/chrome/omnibox/LocationBar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->startVoiceRecognition()V

    goto :goto_0

    .line 227
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage$2;->this$0:Lcom/google/android/apps/chrome/ntp/NewTabPage;

    # getter for: Lcom/google/android/apps/chrome/ntp/NewTabPage;->mLocationBar:Lcom/google/android/apps/chrome/omnibox/LocationBar;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/NewTabPage;->access$900(Lcom/google/android/apps/chrome/ntp/NewTabPage;)Lcom/google/android/apps/chrome/omnibox/LocationBar;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->requestUrlFocusFromFakebox(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getLocalFaviconImageForURL(Ljava/lang/String;ILorg/chromium/chrome/browser/favicon/FaviconHelper$FaviconImageCallback;)V
    .locals 6

    .prologue
    .line 247
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage$2;->this$0:Lcom/google/android/apps/chrome/ntp/NewTabPage;

    # getter for: Lcom/google/android/apps/chrome/ntp/NewTabPage;->mIsDestroyed:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/NewTabPage;->access$100(Lcom/google/android/apps/chrome/ntp/NewTabPage;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 251
    :goto_0
    return-void

    .line 248
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage$2;->this$0:Lcom/google/android/apps/chrome/ntp/NewTabPage;

    # getter for: Lcom/google/android/apps/chrome/ntp/NewTabPage;->mFaviconHelper:Lorg/chromium/chrome/browser/favicon/FaviconHelper;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/NewTabPage;->access$1000(Lcom/google/android/apps/chrome/ntp/NewTabPage;)Lorg/chromium/chrome/browser/favicon/FaviconHelper;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage$2;->this$0:Lcom/google/android/apps/chrome/ntp/NewTabPage;

    # getter for: Lcom/google/android/apps/chrome/ntp/NewTabPage;->mProfile:Lorg/chromium/chrome/browser/profiles/Profile;
    invoke-static {v1}, Lcom/google/android/apps/chrome/ntp/NewTabPage;->access$700(Lcom/google/android/apps/chrome/ntp/NewTabPage;)Lorg/chromium/chrome/browser/profiles/Profile;

    move-result-object v1

    const/4 v3, 0x7

    move-object v2, p1

    move v4, p2

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Lorg/chromium/chrome/browser/favicon/FaviconHelper;->getLocalFaviconImageForURL(Lorg/chromium/chrome/browser/profiles/Profile;Ljava/lang/String;IILorg/chromium/chrome/browser/favicon/FaviconHelper$FaviconImageCallback;)Z

    goto :goto_0
.end method

.method public getSearchProviderLogo(Lorg/chromium/chrome/browser/LogoBridge$LogoObserver;)V
    .locals 2

    .prologue
    .line 263
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage$2;->this$0:Lcom/google/android/apps/chrome/ntp/NewTabPage;

    # getter for: Lcom/google/android/apps/chrome/ntp/NewTabPage;->mIsDestroyed:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/NewTabPage;->access$100(Lcom/google/android/apps/chrome/ntp/NewTabPage;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 273
    :goto_0
    return-void

    .line 264
    :cond_0
    new-instance v0, Lcom/google/android/apps/chrome/ntp/NewTabPage$2$1;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/chrome/ntp/NewTabPage$2$1;-><init>(Lcom/google/android/apps/chrome/ntp/NewTabPage$2;Lorg/chromium/chrome/browser/LogoBridge$LogoObserver;)V

    .line 272
    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage$2;->this$0:Lcom/google/android/apps/chrome/ntp/NewTabPage;

    # getter for: Lcom/google/android/apps/chrome/ntp/NewTabPage;->mLogoBridge:Lorg/chromium/chrome/browser/LogoBridge;
    invoke-static {v1}, Lcom/google/android/apps/chrome/ntp/NewTabPage;->access$1200(Lcom/google/android/apps/chrome/ntp/NewTabPage;)Lorg/chromium/chrome/browser/LogoBridge;

    move-result-object v1

    invoke-virtual {v1, v0}, Lorg/chromium/chrome/browser/LogoBridge;->getCurrentLogo(Lorg/chromium/chrome/browser/LogoBridge$LogoObserver;)V

    goto :goto_0
.end method

.method public getURLThumbnail(Ljava/lang/String;Lorg/chromium/chrome/browser/profiles/MostVisitedSites$ThumbnailCallback;)V
    .locals 1

    .prologue
    .line 240
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage$2;->this$0:Lcom/google/android/apps/chrome/ntp/NewTabPage;

    # getter for: Lcom/google/android/apps/chrome/ntp/NewTabPage;->mIsDestroyed:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/NewTabPage;->access$100(Lcom/google/android/apps/chrome/ntp/NewTabPage;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 242
    :goto_0
    return-void

    .line 241
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage$2;->this$0:Lcom/google/android/apps/chrome/ntp/NewTabPage;

    # getter for: Lcom/google/android/apps/chrome/ntp/NewTabPage;->mMostVisitedSites:Lorg/chromium/chrome/browser/profiles/MostVisitedSites;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/NewTabPage;->access$400(Lcom/google/android/apps/chrome/ntp/NewTabPage;)Lorg/chromium/chrome/browser/profiles/MostVisitedSites;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lorg/chromium/chrome/browser/profiles/MostVisitedSites;->getURLThumbnail(Ljava/lang/String;Lorg/chromium/chrome/browser/profiles/MostVisitedSites$ThumbnailCallback;)V

    goto :goto_0
.end method

.method public isLocationBarShownInNTP()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 133
    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage$2;->this$0:Lcom/google/android/apps/chrome/ntp/NewTabPage;

    # getter for: Lcom/google/android/apps/chrome/ntp/NewTabPage;->mIsDestroyed:Z
    invoke-static {v1}, Lcom/google/android/apps/chrome/ntp/NewTabPage;->access$100(Lcom/google/android/apps/chrome/ntp/NewTabPage;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 135
    :cond_0
    :goto_0
    return v0

    .line 134
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage$2;->this$0:Lcom/google/android/apps/chrome/ntp/NewTabPage;

    # getter for: Lcom/google/android/apps/chrome/ntp/NewTabPage;->mNewTabPageView:Lcom/google/android/apps/chrome/ntp/NewTabPageView;
    invoke-static {v1}, Lcom/google/android/apps/chrome/ntp/NewTabPage;->access$200(Lcom/google/android/apps/chrome/ntp/NewTabPage;)Lcom/google/android/apps/chrome/ntp/NewTabPageView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 135
    iget-object v2, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage$2;->this$0:Lcom/google/android/apps/chrome/ntp/NewTabPage;

    # invokes: Lcom/google/android/apps/chrome/ntp/NewTabPage;->isInSingleUrlBarMode(Landroid/content/Context;)Z
    invoke-static {v2, v1}, Lcom/google/android/apps/chrome/ntp/NewTabPage;->access$300(Lcom/google/android/apps/chrome/ntp/NewTabPage;Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage$2;->this$0:Lcom/google/android/apps/chrome/ntp/NewTabPage;

    # getter for: Lcom/google/android/apps/chrome/ntp/NewTabPage;->mNewTabPageView:Lcom/google/android/apps/chrome/ntp/NewTabPageView;
    invoke-static {v1}, Lcom/google/android/apps/chrome/ntp/NewTabPage;->access$200(Lcom/google/android/apps/chrome/ntp/NewTabPage;)Lcom/google/android/apps/chrome/ntp/NewTabPageView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->urlFocusAnimationsDisabled()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public navigateToBookmarks()V
    .locals 3

    .prologue
    .line 203
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage$2;->this$0:Lcom/google/android/apps/chrome/ntp/NewTabPage;

    # getter for: Lcom/google/android/apps/chrome/ntp/NewTabPage;->mIsDestroyed:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/NewTabPage;->access$100(Lcom/google/android/apps/chrome/ntp/NewTabPage;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 211
    :goto_0
    return-void

    .line 204
    :cond_0
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->ntpSectionBookmarks()V

    .line 206
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage$2;->this$0:Lcom/google/android/apps/chrome/ntp/NewTabPage;

    # getter for: Lcom/google/android/apps/chrome/ntp/NewTabPage;->mProfile:Lorg/chromium/chrome/browser/profiles/Profile;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/NewTabPage;->access$700(Lcom/google/android/apps/chrome/ntp/NewTabPage;)Lorg/chromium/chrome/browser/profiles/Profile;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;->isEnhancedBookmarkEnabled(Lorg/chromium/chrome/browser/profiles/Profile;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 207
    new-instance v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;

    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage$2;->this$0:Lcom/google/android/apps/chrome/ntp/NewTabPage;

    # getter for: Lcom/google/android/apps/chrome/ntp/NewTabPage;->mActivity:Landroid/app/Activity;
    invoke-static {v1}, Lcom/google/android/apps/chrome/ntp/NewTabPage;->access$800(Lcom/google/android/apps/chrome/ntp/NewTabPage;)Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage$2;->this$0:Lcom/google/android/apps/chrome/ntp/NewTabPage;

    # getter for: Lcom/google/android/apps/chrome/ntp/NewTabPage;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;
    invoke-static {v2}, Lcom/google/android/apps/chrome/ntp/NewTabPage;->access$600(Lcom/google/android/apps/chrome/ntp/NewTabPage;)Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;-><init>(Landroid/app/Activity;Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;)V

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;->show()V

    goto :goto_0

    .line 209
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage$2;->this$0:Lcom/google/android/apps/chrome/ntp/NewTabPage;

    # getter for: Lcom/google/android/apps/chrome/ntp/NewTabPage;->mTab:Lcom/google/android/apps/chrome/tab/ChromeTab;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/NewTabPage;->access$500(Lcom/google/android/apps/chrome/ntp/NewTabPage;)Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    new-instance v1, Lorg/chromium/content_public/browser/LoadUrlParams;

    const-string/jumbo v2, "chrome-native://bookmarks/"

    invoke-direct {v1, v2}, Lorg/chromium/content_public/browser/LoadUrlParams;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->loadUrl(Lorg/chromium/content_public/browser/LoadUrlParams;)I

    goto :goto_0
.end method

.method public navigateToRecentTabs()V
    .locals 3

    .prologue
    .line 215
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage$2;->this$0:Lcom/google/android/apps/chrome/ntp/NewTabPage;

    # getter for: Lcom/google/android/apps/chrome/ntp/NewTabPage;->mIsDestroyed:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/NewTabPage;->access$100(Lcom/google/android/apps/chrome/ntp/NewTabPage;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 218
    :goto_0
    return-void

    .line 216
    :cond_0
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->ntpSectionOpenTabs()V

    .line 217
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage$2;->this$0:Lcom/google/android/apps/chrome/ntp/NewTabPage;

    # getter for: Lcom/google/android/apps/chrome/ntp/NewTabPage;->mTab:Lcom/google/android/apps/chrome/tab/ChromeTab;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/NewTabPage;->access$500(Lcom/google/android/apps/chrome/ntp/NewTabPage;)Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    new-instance v1, Lorg/chromium/content_public/browser/LoadUrlParams;

    const-string/jumbo v2, "chrome-native://recent-tabs/"

    invoke-direct {v1, v2}, Lorg/chromium/content_public/browser/LoadUrlParams;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->loadUrl(Lorg/chromium/content_public/browser/LoadUrlParams;)I

    goto :goto_0
.end method

.method public notifyLoadingComplete()V
    .locals 4

    .prologue
    .line 277
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage$2;->this$0:Lcom/google/android/apps/chrome/ntp/NewTabPage;

    # getter for: Lcom/google/android/apps/chrome/ntp/NewTabPage;->mConstructedTimeNs:J
    invoke-static {v2}, Lcom/google/android/apps/chrome/ntp/NewTabPage;->access$1300(Lcom/google/android/apps/chrome/ntp/NewTabPage;)J

    move-result-wide v2

    sub-long/2addr v0, v2

    const-wide/32 v2, 0xf4240

    div-long/2addr v0, v2

    .line 278
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->ntpLoadTime(J)V

    .line 280
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage$2;->this$0:Lcom/google/android/apps/chrome/ntp/NewTabPage;

    # getter for: Lcom/google/android/apps/chrome/ntp/NewTabPage;->mIsDestroyed:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/NewTabPage;->access$100(Lcom/google/android/apps/chrome/ntp/NewTabPage;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 287
    :goto_0
    return-void

    .line 281
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 282
    const-string/jumbo v1, "tabId"

    iget-object v2, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage$2;->this$0:Lcom/google/android/apps/chrome/ntp/NewTabPage;

    # getter for: Lcom/google/android/apps/chrome/ntp/NewTabPage;->mTab:Lcom/google/android/apps/chrome/tab/ChromeTab;
    invoke-static {v2}, Lcom/google/android/apps/chrome/ntp/NewTabPage;->access$500(Lcom/google/android/apps/chrome/ntp/NewTabPage;)Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getId()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 283
    const-string/jumbo v1, "incognito"

    iget-object v2, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage$2;->this$0:Lcom/google/android/apps/chrome/ntp/NewTabPage;

    # getter for: Lcom/google/android/apps/chrome/ntp/NewTabPage;->mTab:Lcom/google/android/apps/chrome/tab/ChromeTab;
    invoke-static {v2}, Lcom/google/android/apps/chrome/ntp/NewTabPage;->access$500(Lcom/google/android/apps/chrome/ntp/NewTabPage;)Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tab/ChromeTab;->isIncognito()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 284
    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage$2;->this$0:Lcom/google/android/apps/chrome/ntp/NewTabPage;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/ntp/NewTabPage;->getView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    const/16 v2, 0x20

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastNotification(Landroid/content/Context;ILandroid/os/Bundle;)V

    .line 286
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage$2;->this$0:Lcom/google/android/apps/chrome/ntp/NewTabPage;

    # getter for: Lcom/google/android/apps/chrome/ntp/NewTabPage;->mMostVisitedSites:Lorg/chromium/chrome/browser/profiles/MostVisitedSites;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/NewTabPage;->access$400(Lcom/google/android/apps/chrome/ntp/NewTabPage;)Lorg/chromium/chrome/browser/profiles/MostVisitedSites;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/profiles/MostVisitedSites;->onLoadingComplete()V

    goto :goto_0
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/MenuItem$OnMenuItemClickListener;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 166
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage$2;->this$0:Lcom/google/android/apps/chrome/ntp/NewTabPage;

    # getter for: Lcom/google/android/apps/chrome/ntp/NewTabPage;->mIsDestroyed:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/NewTabPage;->access$100(Lcom/google/android/apps/chrome/ntp/NewTabPage;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 176
    :goto_0
    return-void

    .line 167
    :cond_0
    sget v0, Lcom/google/android/apps/chrome/R$string;->contextmenu_open_in_new_tab:I

    invoke-interface {p1, v2, v2, v2, v0}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, p2}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 169
    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->isIncognitoModeEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 170
    const/4 v0, 0x1

    sget v1, Lcom/google/android/apps/chrome/R$string;->contextmenu_open_in_incognito_tab:I

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, p2}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 174
    :cond_1
    const/4 v0, 0x2

    sget v1, Lcom/google/android/apps/chrome/R$string;->remove:I

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, p2}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method public onMenuItemClick(ILcom/google/android/apps/chrome/ntp/MostVisitedItem;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 180
    iget-object v2, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage$2;->this$0:Lcom/google/android/apps/chrome/ntp/NewTabPage;

    # getter for: Lcom/google/android/apps/chrome/ntp/NewTabPage;->mIsDestroyed:Z
    invoke-static {v2}, Lcom/google/android/apps/chrome/ntp/NewTabPage;->access$100(Lcom/google/android/apps/chrome/ntp/NewTabPage;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 197
    :goto_0
    return v0

    .line 181
    :cond_0
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 183
    :pswitch_0
    invoke-direct {p0, p2}, Lcom/google/android/apps/chrome/ntp/NewTabPage$2;->recordOpenedMostVisitedItem(Lcom/google/android/apps/chrome/ntp/MostVisitedItem;)V

    .line 184
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage$2;->this$0:Lcom/google/android/apps/chrome/ntp/NewTabPage;

    # getter for: Lcom/google/android/apps/chrome/ntp/NewTabPage;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/NewTabPage;->access$600(Lcom/google/android/apps/chrome/ntp/NewTabPage;)Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    move-result-object v0

    new-instance v2, Lorg/chromium/content_public/browser/LoadUrlParams;

    invoke-virtual {p2}, Lcom/google/android/apps/chrome/ntp/MostVisitedItem;->getUrl()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lorg/chromium/content_public/browser/LoadUrlParams;-><init>(Ljava/lang/String;)V

    sget-object v3, Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;->FROM_LONGPRESS_BACKGROUND:Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;

    iget-object v4, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage$2;->this$0:Lcom/google/android/apps/chrome/ntp/NewTabPage;

    # getter for: Lcom/google/android/apps/chrome/ntp/NewTabPage;->mTab:Lcom/google/android/apps/chrome/tab/ChromeTab;
    invoke-static {v4}, Lcom/google/android/apps/chrome/ntp/NewTabPage;->access$500(Lcom/google/android/apps/chrome/ntp/NewTabPage;)Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage$2;->this$0:Lcom/google/android/apps/chrome/ntp/NewTabPage;

    # getter for: Lcom/google/android/apps/chrome/ntp/NewTabPage;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;
    invoke-static {v5}, Lcom/google/android/apps/chrome/ntp/NewTabPage;->access$600(Lcom/google/android/apps/chrome/ntp/NewTabPage;)Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    move-result-object v5

    invoke-interface {v5}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->isIncognitoSelected()Z

    move-result v5

    invoke-interface {v0, v2, v3, v4, v5}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->openNewTab(Lorg/chromium/content_public/browser/LoadUrlParams;Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;Lorg/chromium/chrome/browser/Tab;Z)Lorg/chromium/chrome/browser/Tab;

    move v0, v1

    .line 187
    goto :goto_0

    .line 189
    :pswitch_1
    invoke-direct {p0, p2}, Lcom/google/android/apps/chrome/ntp/NewTabPage$2;->recordOpenedMostVisitedItem(Lcom/google/android/apps/chrome/ntp/MostVisitedItem;)V

    .line 190
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage$2;->this$0:Lcom/google/android/apps/chrome/ntp/NewTabPage;

    # getter for: Lcom/google/android/apps/chrome/ntp/NewTabPage;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/NewTabPage;->access$600(Lcom/google/android/apps/chrome/ntp/NewTabPage;)Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    move-result-object v0

    new-instance v2, Lorg/chromium/content_public/browser/LoadUrlParams;

    invoke-virtual {p2}, Lcom/google/android/apps/chrome/ntp/MostVisitedItem;->getUrl()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lorg/chromium/content_public/browser/LoadUrlParams;-><init>(Ljava/lang/String;)V

    sget-object v3, Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;->FROM_LONGPRESS_FOREGROUND:Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;

    iget-object v4, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage$2;->this$0:Lcom/google/android/apps/chrome/ntp/NewTabPage;

    # getter for: Lcom/google/android/apps/chrome/ntp/NewTabPage;->mTab:Lcom/google/android/apps/chrome/tab/ChromeTab;
    invoke-static {v4}, Lcom/google/android/apps/chrome/ntp/NewTabPage;->access$500(Lcom/google/android/apps/chrome/ntp/NewTabPage;)Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v4

    invoke-interface {v0, v2, v3, v4, v1}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->openNewTab(Lorg/chromium/content_public/browser/LoadUrlParams;Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;Lorg/chromium/chrome/browser/Tab;Z)Lorg/chromium/chrome/browser/Tab;

    move v0, v1

    .line 192
    goto :goto_0

    .line 194
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage$2;->this$0:Lcom/google/android/apps/chrome/ntp/NewTabPage;

    # getter for: Lcom/google/android/apps/chrome/ntp/NewTabPage;->mMostVisitedSites:Lorg/chromium/chrome/browser/profiles/MostVisitedSites;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/NewTabPage;->access$400(Lcom/google/android/apps/chrome/ntp/NewTabPage;)Lorg/chromium/chrome/browser/profiles/MostVisitedSites;

    move-result-object v0

    invoke-virtual {p2}, Lcom/google/android/apps/chrome/ntp/MostVisitedItem;->getUrl()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lorg/chromium/chrome/browser/profiles/MostVisitedSites;->blacklistUrl(Ljava/lang/String;)V

    move v0, v1

    .line 195
    goto :goto_0

    .line 181
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public open(Lcom/google/android/apps/chrome/ntp/MostVisitedItem;)V
    .locals 3

    .prologue
    .line 159
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage$2;->this$0:Lcom/google/android/apps/chrome/ntp/NewTabPage;

    # getter for: Lcom/google/android/apps/chrome/ntp/NewTabPage;->mIsDestroyed:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/NewTabPage;->access$100(Lcom/google/android/apps/chrome/ntp/NewTabPage;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 162
    :goto_0
    return-void

    .line 160
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/ntp/NewTabPage$2;->recordOpenedMostVisitedItem(Lcom/google/android/apps/chrome/ntp/MostVisitedItem;)V

    .line 161
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage$2;->this$0:Lcom/google/android/apps/chrome/ntp/NewTabPage;

    # getter for: Lcom/google/android/apps/chrome/ntp/NewTabPage;->mTab:Lcom/google/android/apps/chrome/tab/ChromeTab;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/NewTabPage;->access$500(Lcom/google/android/apps/chrome/ntp/NewTabPage;)Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    new-instance v1, Lorg/chromium/content_public/browser/LoadUrlParams;

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/ntp/MostVisitedItem;->getUrl()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/chromium/content_public/browser/LoadUrlParams;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->loadUrl(Lorg/chromium/content_public/browser/LoadUrlParams;)I

    goto :goto_0
.end method

.method public openLogoLink()V
    .locals 4

    .prologue
    .line 255
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage$2;->this$0:Lcom/google/android/apps/chrome/ntp/NewTabPage;

    # getter for: Lcom/google/android/apps/chrome/ntp/NewTabPage;->mIsDestroyed:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/NewTabPage;->access$100(Lcom/google/android/apps/chrome/ntp/NewTabPage;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 259
    :cond_0
    :goto_0
    return-void

    .line 256
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage$2;->this$0:Lcom/google/android/apps/chrome/ntp/NewTabPage;

    # getter for: Lcom/google/android/apps/chrome/ntp/NewTabPage;->mOnLogoClickUrl:Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/NewTabPage;->access$1100(Lcom/google/android/apps/chrome/ntp/NewTabPage;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 257
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage$2;->this$0:Lcom/google/android/apps/chrome/ntp/NewTabPage;

    # getter for: Lcom/google/android/apps/chrome/ntp/NewTabPage;->mTab:Lcom/google/android/apps/chrome/tab/ChromeTab;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/NewTabPage;->access$500(Lcom/google/android/apps/chrome/ntp/NewTabPage;)Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    new-instance v1, Lorg/chromium/content_public/browser/LoadUrlParams;

    iget-object v2, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage$2;->this$0:Lcom/google/android/apps/chrome/ntp/NewTabPage;

    # getter for: Lcom/google/android/apps/chrome/ntp/NewTabPage;->mOnLogoClickUrl:Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/apps/chrome/ntp/NewTabPage;->access$1100(Lcom/google/android/apps/chrome/ntp/NewTabPage;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lorg/chromium/content_public/browser/LoadUrlParams;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->loadUrl(Lorg/chromium/content_public/browser/LoadUrlParams;)I

    goto :goto_0
.end method

.method public optOutPromoSettingsSelected()V
    .locals 2

    .prologue
    .line 154
    sget-boolean v0, Lcom/google/android/apps/chrome/ntp/NewTabPage$2;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    const-string/jumbo v1, "Should never be called for this page"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 155
    :cond_0
    return-void
.end method

.method public setMostVisitedURLsObserver(Lorg/chromium/chrome/browser/profiles/MostVisitedSites$MostVisitedURLsObserver;I)V
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage$2;->this$0:Lcom/google/android/apps/chrome/ntp/NewTabPage;

    # getter for: Lcom/google/android/apps/chrome/ntp/NewTabPage;->mIsDestroyed:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/NewTabPage;->access$100(Lcom/google/android/apps/chrome/ntp/NewTabPage;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 236
    :goto_0
    return-void

    .line 235
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage$2;->this$0:Lcom/google/android/apps/chrome/ntp/NewTabPage;

    # getter for: Lcom/google/android/apps/chrome/ntp/NewTabPage;->mMostVisitedSites:Lorg/chromium/chrome/browser/profiles/MostVisitedSites;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/NewTabPage;->access$400(Lcom/google/android/apps/chrome/ntp/NewTabPage;)Lorg/chromium/chrome/browser/profiles/MostVisitedSites;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lorg/chromium/chrome/browser/profiles/MostVisitedSites;->setMostVisitedURLsObserver(Lorg/chromium/chrome/browser/profiles/MostVisitedSites$MostVisitedURLsObserver;I)V

    goto :goto_0
.end method

.method public shouldShowOptOutPromo()Z
    .locals 1

    .prologue
    .line 149
    const/4 v0, 0x0

    return v0
.end method
