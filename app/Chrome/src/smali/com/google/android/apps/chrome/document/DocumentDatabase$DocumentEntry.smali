.class public final Lcom/google/android/apps/chrome/document/DocumentDatabase$DocumentEntry;
.super Lcom/google/protobuf/nano/ExtendableMessageNano;
.source "DocumentDatabase.java"


# static fields
.field private static volatile _emptyArray:[Lcom/google/android/apps/chrome/document/DocumentDatabase$DocumentEntry;


# instance fields
.field public canGoBack:Ljava/lang/Boolean;

.field public tabId:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/google/protobuf/nano/ExtendableMessageNano;-><init>()V

    .line 32
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentDatabase$DocumentEntry;->clear()Lcom/google/android/apps/chrome/document/DocumentDatabase$DocumentEntry;

    .line 33
    return-void
.end method

.method public static emptyArray()[Lcom/google/android/apps/chrome/document/DocumentDatabase$DocumentEntry;
    .locals 2

    .prologue
    .line 14
    sget-object v0, Lcom/google/android/apps/chrome/document/DocumentDatabase$DocumentEntry;->_emptyArray:[Lcom/google/android/apps/chrome/document/DocumentDatabase$DocumentEntry;

    if-nez v0, :cond_1

    .line 15
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 17
    :try_start_0
    sget-object v0, Lcom/google/android/apps/chrome/document/DocumentDatabase$DocumentEntry;->_emptyArray:[Lcom/google/android/apps/chrome/document/DocumentDatabase$DocumentEntry;

    if-nez v0, :cond_0

    .line 18
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/apps/chrome/document/DocumentDatabase$DocumentEntry;

    sput-object v0, Lcom/google/android/apps/chrome/document/DocumentDatabase$DocumentEntry;->_emptyArray:[Lcom/google/android/apps/chrome/document/DocumentDatabase$DocumentEntry;

    .line 20
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 22
    :cond_1
    sget-object v0, Lcom/google/android/apps/chrome/document/DocumentDatabase$DocumentEntry;->_emptyArray:[Lcom/google/android/apps/chrome/document/DocumentDatabase$DocumentEntry;

    return-object v0

    .line 20
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final clear()Lcom/google/android/apps/chrome/document/DocumentDatabase$DocumentEntry;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 36
    iput-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentDatabase$DocumentEntry;->canGoBack:Ljava/lang/Boolean;

    .line 37
    iput-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentDatabase$DocumentEntry;->tabId:Ljava/lang/Integer;

    .line 38
    iput-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentDatabase$DocumentEntry;->unknownFieldData:Ljava/util/List;

    .line 39
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/chrome/document/DocumentDatabase$DocumentEntry;->cachedSize:I

    .line 40
    return-object p0
.end method

.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 55
    invoke-super {p0}, Lcom/google/protobuf/nano/ExtendableMessageNano;->computeSerializedSize()I

    move-result v0

    .line 56
    iget-object v1, p0, Lcom/google/android/apps/chrome/document/DocumentDatabase$DocumentEntry;->canGoBack:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    .line 57
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/apps/chrome/document/DocumentDatabase$DocumentEntry;->canGoBack:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 60
    :cond_0
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/apps/chrome/document/DocumentDatabase$DocumentEntry;->tabId:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 62
    return v0
.end method

.method public final mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/apps/chrome/document/DocumentDatabase$DocumentEntry;
    .locals 1

    .prologue
    .line 70
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->a()I

    move-result v0

    .line 71
    sparse-switch v0, :sswitch_data_0

    .line 75
    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/chrome/document/DocumentDatabase$DocumentEntry;->storeUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 76
    :sswitch_0
    return-object p0

    .line 81
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentDatabase$DocumentEntry;->canGoBack:Ljava/lang/Boolean;

    goto :goto_0

    .line 85
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->c()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentDatabase$DocumentEntry;->tabId:Ljava/lang/Integer;

    goto :goto_0

    .line 71
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x20 -> :sswitch_1
        0x28 -> :sswitch_2
    .end sparse-switch
.end method

.method public final bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1

    .prologue
    .line 8
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/document/DocumentDatabase$DocumentEntry;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/apps/chrome/document/DocumentDatabase$DocumentEntry;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentDatabase$DocumentEntry;->canGoBack:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 47
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/apps/chrome/document/DocumentDatabase$DocumentEntry;->canGoBack:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(IZ)V

    .line 49
    :cond_0
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/android/apps/chrome/document/DocumentDatabase$DocumentEntry;->tabId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(II)V

    .line 50
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/ExtendableMessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 51
    return-void
.end method
