.class public Lcom/google/android/apps/chrome/sync/ui/CustomPassphraseDialogFragment;
.super Lcom/google/android/apps/chrome/preferences/ChromeBaseDialogFragment;
.source "CustomPassphraseDialogFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field private mConfirmPassphrase:Landroid/widget/EditText;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBaseDialogFragment;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/sync/ui/CustomPassphraseDialogFragment;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ui/CustomPassphraseDialogFragment;->mConfirmPassphrase:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/sync/ui/CustomPassphraseDialogFragment;Landroid/widget/EditText;Landroid/widget/EditText;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/sync/ui/CustomPassphraseDialogFragment;->validatePasswordText(Landroid/widget/EditText;Landroid/widget/EditText;)V

    return-void
.end method

.method private validatePasswordText(Landroid/widget/EditText;Landroid/widget/EditText;)V
    .locals 4

    .prologue
    .line 106
    invoke-virtual {p1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 107
    invoke-virtual {p2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 111
    const/4 v0, 0x0

    .line 112
    invoke-virtual {p2}, Landroid/widget/EditText;->hasFocus()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    .line 113
    :cond_0
    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 114
    sget v0, Lcom/google/android/apps/chrome/R$string;->sync_passphrase_cannot_be_blank:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/sync/ui/CustomPassphraseDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 119
    :cond_1
    :goto_0
    invoke-virtual {p2, v0}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    .line 120
    return-void

    .line 115
    :cond_2
    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 116
    sget v0, Lcom/google/android/apps/chrome/R$string;->sync_passphrases_do_not_match:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/sync/ui/CustomPassphraseDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4

    .prologue
    .line 124
    const/4 v0, -0x1

    if-ne p2, v0, :cond_1

    .line 125
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ui/CustomPassphraseDialogFragment;->mConfirmPassphrase:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getError()Ljava/lang/CharSequence;

    move-result-object v0

    if-nez v0, :cond_0

    .line 126
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/ui/CustomPassphraseDialogFragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/sync/ui/PassphraseDialogFragment$Listener;

    .line 128
    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/ui/CustomPassphraseDialogFragment;->mConfirmPassphrase:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 129
    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/apps/chrome/sync/ui/PassphraseDialogFragment$Listener;->onPassphraseEntered(Ljava/lang/String;ZZ)V

    .line 134
    :cond_0
    :goto_0
    return-void

    .line 131
    :cond_1
    const/4 v0, -0x2

    if-ne p2, v0, :cond_0

    .line 132
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/ui/CustomPassphraseDialogFragment;->dismiss()V

    goto :goto_0
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4

    .prologue
    .line 36
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/preferences/ChromeBaseDialogFragment;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;

    .line 37
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/ui/CustomPassphraseDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    .line 38
    sget v1, Lcom/google/android/apps/chrome/R$layout;->sync_custom_passphrase:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 39
    sget v0, Lcom/google/android/apps/chrome/R$id;->passphrase:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 40
    sget v1, Lcom/google/android/apps/chrome/R$id;->confirm_passphrase:I

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/google/android/apps/chrome/sync/ui/CustomPassphraseDialogFragment;->mConfirmPassphrase:Landroid/widget/EditText;

    .line 43
    new-instance v1, Lcom/google/android/apps/chrome/sync/ui/CustomPassphraseDialogFragment$1;

    invoke-direct {v1, p0, v0}, Lcom/google/android/apps/chrome/sync/ui/CustomPassphraseDialogFragment$1;-><init>(Lcom/google/android/apps/chrome/sync/ui/CustomPassphraseDialogFragment;Landroid/widget/EditText;)V

    .line 57
    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 58
    iget-object v3, p0, Lcom/google/android/apps/chrome/sync/ui/CustomPassphraseDialogFragment;->mConfirmPassphrase:Landroid/widget/EditText;

    invoke-virtual {v3, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 61
    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/ui/CustomPassphraseDialogFragment;->mConfirmPassphrase:Landroid/widget/EditText;

    new-instance v3, Lcom/google/android/apps/chrome/sync/ui/CustomPassphraseDialogFragment$2;

    invoke-direct {v3, p0, v0}, Lcom/google/android/apps/chrome/sync/ui/CustomPassphraseDialogFragment$2;-><init>(Lcom/google/android/apps/chrome/sync/ui/CustomPassphraseDialogFragment;Landroid/widget/EditText;)V

    invoke-virtual {v1, v3}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 71
    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/ui/CustomPassphraseDialogFragment;->mConfirmPassphrase:Landroid/widget/EditText;

    new-instance v3, Lcom/google/android/apps/chrome/sync/ui/CustomPassphraseDialogFragment$3;

    invoke-direct {v3, p0, v0}, Lcom/google/android/apps/chrome/sync/ui/CustomPassphraseDialogFragment$3;-><init>(Lcom/google/android/apps/chrome/sync/ui/CustomPassphraseDialogFragment;Landroid/widget/EditText;)V

    invoke-virtual {v1, v3}, Landroid/widget/EditText;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 81
    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/ui/CustomPassphraseDialogFragment;->mConfirmPassphrase:Landroid/widget/EditText;

    new-instance v3, Lcom/google/android/apps/chrome/sync/ui/CustomPassphraseDialogFragment$4;

    invoke-direct {v3, p0, v0}, Lcom/google/android/apps/chrome/sync/ui/CustomPassphraseDialogFragment$4;-><init>(Lcom/google/android/apps/chrome/sync/ui/CustomPassphraseDialogFragment;Landroid/widget/EditText;)V

    invoke-virtual {v1, v3}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 97
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/ui/CustomPassphraseDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const/4 v3, 0x3

    invoke-direct {v0, v1, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$string;->sync_passphrase_type_custom:I

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$string;->ok:I

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$string;->cancel:I

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method
