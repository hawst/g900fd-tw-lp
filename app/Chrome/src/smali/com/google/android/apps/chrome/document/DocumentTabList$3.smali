.class Lcom/google/android/apps/chrome/document/DocumentTabList$3;
.super Landroid/os/AsyncTask;
.source "DocumentTabList.java"


# instance fields
.field private mEntries:Ljava/util/List;

.field private mHistoricalTabsForBackgroundThread:Ljava/util/Set;

.field final synthetic this$0:Lcom/google/android/apps/chrome/document/DocumentTabList;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/document/DocumentTabList;)V
    .locals 0

    .prologue
    .line 768
    iput-object p1, p0, Lcom/google/android/apps/chrome/document/DocumentTabList$3;->this$0:Lcom/google/android/apps/chrome/document/DocumentTabList;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 768
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/document/DocumentTabList$3;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method public varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 7

    .prologue
    .line 781
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentTabList$3;->mHistoricalTabsForBackgroundThread:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 783
    iget-object v2, p0, Lcom/google/android/apps/chrome/document/DocumentTabList$3;->mEntries:Ljava/util/List;

    new-instance v3, Lcom/google/android/apps/chrome/document/DocumentTabList$Entry;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    iget-object v5, p0, Lcom/google/android/apps/chrome/document/DocumentTabList$3;->this$0:Lcom/google/android/apps/chrome/document/DocumentTabList;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v6

    # invokes: Lcom/google/android/apps/chrome/document/DocumentTabList;->restoreTabState(I)Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;
    invoke-static {v5, v6}, Lcom/google/android/apps/chrome/document/DocumentTabList;->access$300(Lcom/google/android/apps/chrome/document/DocumentTabList;I)Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/google/android/apps/chrome/document/DocumentTabList$Entry;-><init>(ILcom/google/android/apps/chrome/tab/ChromeTab$TabState;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 784
    iget-object v2, p0, Lcom/google/android/apps/chrome/document/DocumentTabList$3;->this$0:Lcom/google/android/apps/chrome/document/DocumentTabList;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    # invokes: Lcom/google/android/apps/chrome/document/DocumentTabList;->getTabFile(I)Ljava/io/File;
    invoke-static {v2, v3}, Lcom/google/android/apps/chrome/document/DocumentTabList;->access$900(Lcom/google/android/apps/chrome/document/DocumentTabList;I)Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    move-result v2

    .line 785
    if-nez v2, :cond_0

    const-string/jumbo v2, "DocumentTabList"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Failed to delete file for tab "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 788
    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method

.method public bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 768
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/document/DocumentTabList$3;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method public onPostExecute(Ljava/lang/Void;)V
    .locals 3

    .prologue
    .line 793
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentTabList$3;->mEntries:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/document/DocumentTabList$Entry;

    .line 794
    iget-object v2, v0, Lcom/google/android/apps/chrome/document/DocumentTabList$Entry;->tabState:Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/google/android/apps/chrome/document/DocumentTabList$Entry;->tabState:Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;

    iget-object v2, v2, Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;->contentsState:Lcom/google/android/apps/chrome/tab/ChromeTab$ContentsState;

    if-eqz v2, :cond_0

    .line 795
    iget-object v0, v0, Lcom/google/android/apps/chrome/document/DocumentTabList$Entry;->tabState:Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;

    iget-object v0, v0, Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;->contentsState:Lcom/google/android/apps/chrome/tab/ChromeTab$ContentsState;

    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->createHistoricalTabFromState(Lcom/google/android/apps/chrome/tab/ChromeTab$ContentsState;)V

    goto :goto_0

    .line 797
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentTabList$3;->this$0:Lcom/google/android/apps/chrome/document/DocumentTabList;

    # getter for: Lcom/google/android/apps/chrome/document/DocumentTabList;->mHistoricalTabs:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/apps/chrome/document/DocumentTabList;->access$1000(Lcom/google/android/apps/chrome/document/DocumentTabList;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 798
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentTabList$3;->this$0:Lcom/google/android/apps/chrome/document/DocumentTabList;

    const/16 v1, 0x8

    # invokes: Lcom/google/android/apps/chrome/document/DocumentTabList;->setCurrentState(I)V
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/document/DocumentTabList;->access$100(Lcom/google/android/apps/chrome/document/DocumentTabList;I)V

    .line 799
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentTabList$3;->this$0:Lcom/google/android/apps/chrome/document/DocumentTabList;

    # invokes: Lcom/google/android/apps/chrome/document/DocumentTabList;->cleanUpObsoleteTabStatesAsync()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/document/DocumentTabList;->access$1100(Lcom/google/android/apps/chrome/document/DocumentTabList;)V

    .line 800
    return-void
.end method

.method public onPreExecute()V
    .locals 2

    .prologue
    .line 774
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentTabList$3;->this$0:Lcom/google/android/apps/chrome/document/DocumentTabList;

    const/4 v1, 0x7

    # invokes: Lcom/google/android/apps/chrome/document/DocumentTabList;->setCurrentState(I)V
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/document/DocumentTabList;->access$100(Lcom/google/android/apps/chrome/document/DocumentTabList;I)V

    .line 775
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentTabList$3;->this$0:Lcom/google/android/apps/chrome/document/DocumentTabList;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/document/DocumentTabList;->getHistoricalTabs()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentTabList$3;->mHistoricalTabsForBackgroundThread:Ljava/util/Set;

    .line 776
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/apps/chrome/document/DocumentTabList$3;->mHistoricalTabsForBackgroundThread:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentTabList$3;->mEntries:Ljava/util/List;

    .line 777
    return-void
.end method
