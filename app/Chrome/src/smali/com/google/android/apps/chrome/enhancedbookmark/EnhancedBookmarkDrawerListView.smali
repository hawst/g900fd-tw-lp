.class Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListView;
.super Landroid/widget/ListView;
.source "EnhancedBookmarkDrawerListView.java"

# interfaces
.implements Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkUIObserver;


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final mAdapter:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListViewAdapter;

.field private mBookmarkModelObserver:Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkModelObserver;

.field private mDelegate:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-class v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListView;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListView;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0, p1, p2}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 24
    new-instance v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListView$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListView$1;-><init>(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListView;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListView;->mBookmarkModelObserver:Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkModelObserver;

    .line 31
    new-instance v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListViewAdapter;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListViewAdapter;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListView;->mAdapter:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListViewAdapter;

    .line 37
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListView;->mAdapter:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListViewAdapter;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 38
    new-instance v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListView$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListView$2;-><init>(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListView;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 61
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListView;)Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListView;->mDelegate:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListView;)Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListViewAdapter;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListView;->mAdapter:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListViewAdapter;

    return-object v0
.end method

.method private updateTopFolder()V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 68
    sget-boolean v2, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListView;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListView;->mDelegate:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;

    if-nez v2, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 69
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListView;->mAdapter:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListViewAdapter;

    iget-object v3, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListView;->mDelegate:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;

    invoke-interface {v3}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;->getModel()Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;

    move-result-object v3

    invoke-virtual {v3, v1, v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;->getTopLevelFolderIDs(ZZ)Ljava/util/List;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListViewAdapter;->setTopFolders(Ljava/util/List;)V

    .line 70
    iget-object v2, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListView;->mAdapter:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListViewAdapter;

    iget-object v3, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListView;->mDelegate:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;

    invoke-interface {v3}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;->getModel()Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;->getUncategorizedBookmarkIDs()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_1

    :goto_0
    invoke-virtual {v2, v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListViewAdapter;->showUncategorizedItem(Z)V

    .line 72
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListView;->mAdapter:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListViewAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListViewAdapter;->notifyDataSetChanged()V

    .line 73
    return-void

    :cond_1
    move v0, v1

    .line 70
    goto :goto_0
.end method


# virtual methods
.method public onAllBookmarksModeSet()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 97
    invoke-direct {p0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListView;->updateTopFolder()V

    .line 98
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListView;->mAdapter:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListViewAdapter;

    const/4 v1, 0x0

    invoke-virtual {v0, v2, v1}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListViewAdapter;->getItemPosition(ILjava/lang/Object;)I

    move-result v0

    invoke-virtual {p0, v0, v2}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListView;->setItemChecked(IZ)V

    .line 100
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 86
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListView;->mDelegate:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;->getModel()Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListView;->mBookmarkModelObserver:Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkModelObserver;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;->removeModelObserver(Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkModelObserver;)V

    .line 87
    return-void
.end method

.method public onEmptyModeSet()V
    .locals 0

    .prologue
    .line 91
    invoke-direct {p0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListView;->updateTopFolder()V

    .line 92
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListView;->clearChoices()V

    .line 93
    return-void
.end method

.method public onEnhancedBookmarkDelegateInitialized(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;)V
    .locals 2

    .prologue
    .line 79
    iput-object p1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListView;->mDelegate:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;

    .line 80
    invoke-interface {p1}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;->getModel()Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListView;->mBookmarkModelObserver:Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkModelObserver;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;->addModelObserver(Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkModelObserver;)V

    .line 81
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListView;->mAdapter:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListViewAdapter;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListViewAdapter;->setEnhancedBookmarkUIDelegate(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;)V

    .line 82
    return-void
.end method

.method public onFolderModeSet(Lorg/chromium/components/bookmarks/BookmarkId;)V
    .locals 2

    .prologue
    .line 111
    invoke-direct {p0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListView;->updateTopFolder()V

    .line 112
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListView;->mAdapter:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListViewAdapter;

    const/4 v1, 0x3

    invoke-virtual {v0, v1, p1}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListViewAdapter;->getItemPosition(ILjava/lang/Object;)I

    move-result v0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListView;->setItemChecked(IZ)V

    .line 114
    return-void
.end method

.method public onUncategorizedModeSet()V
    .locals 3

    .prologue
    .line 104
    invoke-direct {p0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListView;->updateTopFolder()V

    .line 105
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListView;->mAdapter:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListViewAdapter;

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListViewAdapter;->getItemPosition(ILjava/lang/Object;)I

    move-result v0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListView;->setItemChecked(IZ)V

    .line 107
    return-void
.end method
