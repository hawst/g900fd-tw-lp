.class public Lcom/google/android/apps/chrome/document/DocumentManager;
.super Ljava/lang/Object;
.source "DocumentManager.java"


# static fields
.field private static sDocumentManager:Lcom/google/android/apps/chrome/document/DocumentManager;


# instance fields
.field private mCurrentProfile:Lorg/chromium/chrome/browser/profiles/Profile;

.field private mCurrentUrl:Ljava/lang/String;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    invoke-static {}, Lorg/chromium/chrome/browser/profiles/Profile;->getLastUsedProfile()Lorg/chromium/chrome/browser/profiles/Profile;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/document/DocumentManager;->setProfile(Lorg/chromium/chrome/browser/profiles/Profile;)V

    .line 31
    return-void
.end method

.method public static getInstance()Lcom/google/android/apps/chrome/document/DocumentManager;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lcom/google/android/apps/chrome/document/DocumentManager;->sDocumentManager:Lcom/google/android/apps/chrome/document/DocumentManager;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/chrome/document/DocumentManager;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/document/DocumentManager;-><init>()V

    sput-object v0, Lcom/google/android/apps/chrome/document/DocumentManager;->sDocumentManager:Lcom/google/android/apps/chrome/document/DocumentManager;

    .line 26
    :cond_0
    sget-object v0, Lcom/google/android/apps/chrome/document/DocumentManager;->sDocumentManager:Lcom/google/android/apps/chrome/document/DocumentManager;

    return-object v0
.end method


# virtual methods
.method public getCurrentProfile()Lorg/chromium/chrome/browser/profiles/Profile;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentManager;->mCurrentProfile:Lorg/chromium/chrome/browser/profiles/Profile;

    return-object v0
.end method

.method setCurrentUrl(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 46
    iput-object p1, p0, Lcom/google/android/apps/chrome/document/DocumentManager;->mCurrentUrl:Ljava/lang/String;

    .line 47
    return-void
.end method

.method public setProfile(Lorg/chromium/chrome/browser/profiles/Profile;)V
    .locals 0

    .prologue
    .line 38
    iput-object p1, p0, Lcom/google/android/apps/chrome/document/DocumentManager;->mCurrentProfile:Lorg/chromium/chrome/browser/profiles/Profile;

    .line 39
    return-void
.end method
