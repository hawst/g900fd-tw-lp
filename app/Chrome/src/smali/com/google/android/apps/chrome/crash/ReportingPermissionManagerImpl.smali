.class public Lcom/google/android/apps/chrome/crash/ReportingPermissionManagerImpl;
.super Ljava/lang/Object;
.source "ReportingPermissionManagerImpl.java"

# interfaces
.implements Lcom/google/android/apps/chrome/crash/ReportingPermissionManager;


# instance fields
.field private final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/google/android/apps/chrome/crash/ReportingPermissionManagerImpl;->mContext:Landroid/content/Context;

    .line 19
    return-void
.end method


# virtual methods
.method public isUploadPermitted()Z
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/apps/chrome/crash/ReportingPermissionManagerImpl;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->allowUploadCrashDumpNow()Z

    move-result v0

    return v0
.end method
