.class Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsContainer$1;
.super Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkModelObserver;
.source "EnhancedBookmarkItemsContainer.java"


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsContainer;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsContainer;)V
    .locals 0

    .prologue
    .line 39
    iput-object p1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsContainer$1;->this$0:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsContainer;

    invoke-direct {p0}, Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkModelObserver;-><init>()V

    return-void
.end method


# virtual methods
.method public bookmarkModelChanged()V
    .locals 2

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsContainer$1;->this$0:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsContainer;

    # getter for: Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsContainer;->mDelegate:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;
    invoke-static {v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsContainer;->access$000(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsContainer;)Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsContainer$1;->this$0:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsContainer;

    invoke-interface {v0, v1}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;->notifyCurrentModeSet(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkUIObserver;)V

    .line 61
    return-void
.end method

.method public bookmarkNodeChanged(Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;)V
    .locals 4

    .prologue
    .line 42
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsContainer$1;->this$0:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsContainer;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsContainer;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 43
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsContainer$1;->this$0:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsContainer;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsContainer;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 44
    instance-of v2, v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolder;

    if-eqz v2, :cond_1

    .line 45
    check-cast v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolder;

    .line 46
    invoke-virtual {p1}, Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;->getId()Lorg/chromium/components/bookmarks/BookmarkId;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolder;->getBookmarkId()Lorg/chromium/components/bookmarks/BookmarkId;

    move-result-object v3

    invoke-virtual {v2, v3}, Lorg/chromium/components/bookmarks/BookmarkId;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 47
    invoke-virtual {p1}, Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolder;->setText(Ljava/lang/CharSequence;)V

    .line 42
    :cond_0
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 49
    :cond_1
    instance-of v2, v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;

    if-eqz v2, :cond_0

    .line 50
    check-cast v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;

    .line 51
    invoke-virtual {p1}, Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;->getId()Lorg/chromium/components/bookmarks/BookmarkId;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;->getBookmarkId()Lorg/chromium/components/bookmarks/BookmarkId;

    move-result-object v3

    invoke-virtual {v2, v3}, Lorg/chromium/components/bookmarks/BookmarkId;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 52
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;->updateBookmarkInfo()V

    goto :goto_1

    .line 56
    :cond_2
    return-void
.end method
