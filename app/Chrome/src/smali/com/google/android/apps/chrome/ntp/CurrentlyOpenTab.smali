.class public Lcom/google/android/apps/chrome/ntp/CurrentlyOpenTab;
.super Ljava/lang/Object;
.source "CurrentlyOpenTab.java"


# instance fields
.field private final mRunnable:Ljava/lang/Runnable;

.field private final mTitle:Ljava/lang/String;

.field private final mUrl:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/google/android/apps/chrome/ntp/CurrentlyOpenTab;->mUrl:Ljava/lang/String;

    .line 24
    iput-object p2, p0, Lcom/google/android/apps/chrome/ntp/CurrentlyOpenTab;->mTitle:Ljava/lang/String;

    .line 25
    iput-object p3, p0, Lcom/google/android/apps/chrome/ntp/CurrentlyOpenTab;->mRunnable:Ljava/lang/Runnable;

    .line 26
    return-void
.end method


# virtual methods
.method public getRunnable()Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/CurrentlyOpenTab;->mRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/CurrentlyOpenTab;->mTitle:Ljava/lang/String;

    return-object v0
.end method

.method public getUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/CurrentlyOpenTab;->mUrl:Ljava/lang/String;

    return-object v0
.end method
