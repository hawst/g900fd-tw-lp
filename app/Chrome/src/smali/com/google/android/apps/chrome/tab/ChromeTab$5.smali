.class Lcom/google/android/apps/chrome/tab/ChromeTab$5;
.super Ljava/lang/Object;
.source "ChromeTab.java"

# interfaces
.implements Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper$BackgroundContentViewDelegate;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/tab/ChromeTab;)V
    .locals 0

    .prologue
    .line 724
    iput-object p1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$5;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onBackgroundViewReady(Lorg/chromium/content/browser/ContentViewCore;ZZI)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 729
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$5;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getWebContents()Lorg/chromium/content_public/browser/WebContents;

    move-result-object v0

    .line 730
    iget-object v1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$5;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    const/4 v2, 0x0

    # invokes: Lcom/google/android/apps/chrome/tab/ChromeTab;->swapContentViewCore(Lorg/chromium/content/browser/ContentViewCore;ZZZ)V
    invoke-static {v1, p1, v2, p2, p3}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$1200(Lcom/google/android/apps/chrome/tab/ChromeTab;Lorg/chromium/content/browser/ContentViewCore;ZZZ)V

    .line 731
    iget-object v1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$5;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # setter for: Lcom/google/android/apps/chrome/tab/ChromeTab;->mLoadProgressAtViewSwapInTime:I
    invoke-static {v1, p4}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$1302(Lcom/google/android/apps/chrome/tab/ChromeTab;I)I

    .line 732
    iget-object v1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$5;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # getter for: Lcom/google/android/apps/chrome/tab/ChromeTab;->mBackgroundContentViewHelper:Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;
    invoke-static {v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$1400(Lcom/google/android/apps/chrome/tab/ChromeTab;)Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->unloadAndDeleteWebContents(Lorg/chromium/content_public/browser/WebContents;)V

    .line 735
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$5;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # getter for: Lcom/google/android/apps/chrome/tab/ChromeTab;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$1500(Lcom/google/android/apps/chrome/tab/ChromeTab;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 736
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$5;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # getter for: Lcom/google/android/apps/chrome/tab/ChromeTab;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$1500(Lcom/google/android/apps/chrome/tab/ChromeTab;)Landroid/os/Handler;

    move-result-object v0

    const-wide/16 v2, 0xbb8

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 738
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$5;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->updateFullscreenEnabledState()V

    .line 739
    return-void
.end method

.method public onLoadProgressChanged(I)V
    .locals 1

    .prologue
    .line 743
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$5;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # invokes: Lcom/google/android/apps/chrome/tab/ChromeTab;->notifyLoadProgress()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$1600(Lcom/google/android/apps/chrome/tab/ChromeTab;)V

    .line 744
    return-void
.end method
