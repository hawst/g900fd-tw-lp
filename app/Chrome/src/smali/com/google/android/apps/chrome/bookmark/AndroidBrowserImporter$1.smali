.class Lcom/google/android/apps/chrome/bookmark/AndroidBrowserImporter$1;
.super Ljava/lang/Object;
.source "AndroidBrowserImporter.java"

# interfaces
.implements Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$OnBookmarksImportedListener;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/bookmark/AndroidBrowserImporter;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/bookmark/AndroidBrowserImporter;)V
    .locals 0

    .prologue
    .line 73
    iput-object p1, p0, Lcom/google/android/apps/chrome/bookmark/AndroidBrowserImporter$1;->this$0:Lcom/google/android/apps/chrome/bookmark/AndroidBrowserImporter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onBookmarksImported(Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$ImportResults;)V
    .locals 2

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AndroidBrowserImporter$1;->this$0:Lcom/google/android/apps/chrome/bookmark/AndroidBrowserImporter;

    # getter for: Lcom/google/android/apps/chrome/bookmark/AndroidBrowserImporter;->mDatabaseFile:Ljava/io/File;
    invoke-static {v0}, Lcom/google/android/apps/chrome/bookmark/AndroidBrowserImporter;->access$000(Lcom/google/android/apps/chrome/bookmark/AndroidBrowserImporter;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v0

    if-nez v0, :cond_0

    .line 78
    const-string/jumbo v0, "AndroidBrowserImporter"

    const-string/jumbo v1, "Database file deletion failed."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 79
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AndroidBrowserImporter$1;->this$0:Lcom/google/android/apps/chrome/bookmark/AndroidBrowserImporter;

    iget-object v0, v0, Lcom/google/android/apps/chrome/bookmark/AndroidBrowserImporter;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "android_browser_temp.db-shm"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getDatabasePath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v0

    if-nez v0, :cond_1

    .line 80
    const-string/jumbo v0, "AndroidBrowserImporter"

    const-string/jumbo v1, "Temporary database files deletion failed (shm)."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 81
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AndroidBrowserImporter$1;->this$0:Lcom/google/android/apps/chrome/bookmark/AndroidBrowserImporter;

    iget-object v0, v0, Lcom/google/android/apps/chrome/bookmark/AndroidBrowserImporter;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "android_browser_temp.db-wal"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getDatabasePath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v0

    if-nez v0, :cond_2

    .line 82
    const-string/jumbo v0, "AndroidBrowserImporter"

    const-string/jumbo v1, "Temporary database files deletion failed (wal)."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 83
    :cond_2
    const-string/jumbo v0, "AndroidBrowserImporter"

    const-string/jumbo v1, "Automatic bookmark import finished."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 84
    return-void
.end method
