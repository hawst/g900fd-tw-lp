.class interface abstract Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkUIObserver;
.super Ljava/lang/Object;
.source "EnhancedBookmarkUIObserver.java"


# virtual methods
.method public abstract onAllBookmarksModeSet()V
.end method

.method public abstract onDestroy()V
.end method

.method public abstract onEmptyModeSet()V
.end method

.method public abstract onEnhancedBookmarkDelegateInitialized(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;)V
.end method

.method public abstract onFolderModeSet(Lorg/chromium/components/bookmarks/BookmarkId;)V
.end method

.method public abstract onUncategorizedModeSet()V
.end method
