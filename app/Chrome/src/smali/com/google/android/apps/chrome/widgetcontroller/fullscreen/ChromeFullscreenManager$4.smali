.class Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager$4;
.super Landroid/animation/AnimatorListenerAdapter;
.source "ChromeFullscreenManager.java"


# instance fields
.field private mCanceled:Z

.field final synthetic this$0:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;

.field final synthetic val$show:Z


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;Z)V
    .locals 1

    .prologue
    .line 698
    iput-object p1, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager$4;->this$0:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;

    iput-boolean p2, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager$4;->val$show:Z

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    .line 699
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager$4;->mCanceled:Z

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 1

    .prologue
    .line 703
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager$4;->mCanceled:Z

    .line 704
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2

    .prologue
    .line 708
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager$4;->val$show:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager$4;->mCanceled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager$4;->this$0:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;

    const/high16 v1, 0x7fc00000    # NaNf

    # setter for: Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mBrowserControlOffset:F
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->access$102(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;F)F

    .line 709
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager$4;->this$0:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mControlAnimation:Landroid/animation/ObjectAnimator;
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->access$802(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;Landroid/animation/ObjectAnimator;)Landroid/animation/ObjectAnimator;

    .line 710
    return-void
.end method
