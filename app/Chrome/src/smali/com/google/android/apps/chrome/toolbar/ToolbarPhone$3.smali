.class Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$3;
.super Lcom/google/android/apps/chrome/toolbar/KeyboardNavigationListener;
.source "ToolbarPhone.java"


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)V
    .locals 0

    .prologue
    .line 221
    iput-object p1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$3;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/toolbar/KeyboardNavigationListener;-><init>()V

    return-void
.end method


# virtual methods
.method public getNextFocusBackward()Landroid/view/View;
    .locals 2

    .prologue
    .line 233
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$3;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    sget v1, Lcom/google/android/apps/chrome/R$id;->url_bar:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->findViewById(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public getNextFocusForward()Landroid/view/View;
    .locals 1

    .prologue
    .line 224
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$3;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mMenuButton:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$400(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Landroid/widget/ImageView;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$3;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mMenuButton:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$400(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ImageView;->isShown()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 225
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$3;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mMenuButton:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$400(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Landroid/widget/ImageView;

    move-result-object v0

    .line 227
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$3;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->getDelegate()Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->getCurrentView()Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method
