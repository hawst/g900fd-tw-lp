.class public Lcom/google/android/apps/chrome/ntp/RecentTabsGroupView;
.super Landroid/widget/RelativeLayout;
.source "RecentTabsGroupView.java"


# instance fields
.field private mDeviceIcon:Landroid/widget/ImageView;

.field private mDeviceLabel:Landroid/widget/TextView;

.field private mDeviceLabelCollapsedColor:I

.field private mDeviceLabelExpandedColor:I

.field private mExpandCollapseIcon:Landroid/widget/ImageView;

.field private mInitializationTimestamp:J

.field private mTimeLabel:Landroid/widget/TextView;

.field private mTimeLabelCollapsedColor:I

.field private mTimeLabelExpandedColor:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 49
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 50
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ntp/RecentTabsGroupView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 51
    sget v1, Lcom/google/android/apps/chrome/R$color;->active_control_color:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsGroupView;->mDeviceLabelExpandedColor:I

    .line 52
    sget v1, Lcom/google/android/apps/chrome/R$color;->ntp_list_header_text:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsGroupView;->mDeviceLabelCollapsedColor:I

    .line 53
    sget v1, Lcom/google/android/apps/chrome/R$color;->ntp_list_header_subtext_active:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsGroupView;->mTimeLabelExpandedColor:I

    .line 54
    sget v1, Lcom/google/android/apps/chrome/R$color;->ntp_list_header_subtext:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsGroupView;->mTimeLabelCollapsedColor:I

    .line 55
    return-void
.end method

.method private configureExpandedCollapsed(Z)V
    .locals 2

    .prologue
    .line 155
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ntp/RecentTabsGroupView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    if-eqz p1, :cond_0

    sget v0, Lcom/google/android/apps/chrome/R$string;->ntp_recent_tabs_accessibility_expanded_group:I

    :goto_0
    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 158
    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsGroupView;->mExpandCollapseIcon:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 160
    if-eqz p1, :cond_1

    const/4 v0, 0x1

    .line 161
    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsGroupView;->mExpandCollapseIcon:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->setLevel(I)Z

    .line 162
    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsGroupView;->mDeviceIcon:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->setLevel(I)Z

    .line 164
    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsGroupView;->mDeviceLabel:Landroid/widget/TextView;

    if-eqz p1, :cond_2

    iget v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsGroupView;->mDeviceLabelExpandedColor:I

    :goto_2
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 167
    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsGroupView;->mTimeLabel:Landroid/widget/TextView;

    if-eqz p1, :cond_3

    iget v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsGroupView;->mTimeLabelExpandedColor:I

    :goto_3
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 168
    return-void

    .line 155
    :cond_0
    sget v0, Lcom/google/android/apps/chrome/R$string;->ntp_recent_tabs_accessibility_collapsed_group:I

    goto :goto_0

    .line 160
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 164
    :cond_2
    iget v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsGroupView;->mDeviceLabelCollapsedColor:I

    goto :goto_2

    .line 167
    :cond_3
    iget v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsGroupView;->mTimeLabelCollapsedColor:I

    goto :goto_3
.end method

.method private getTimeString(Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSession;)Ljava/lang/String;
    .locals 12

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    const-wide/16 v2, 0x0

    .line 171
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget-wide v4, p1, Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSession;->modifiedTime:J

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v4, v5, v1}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v0

    .line 173
    iget-wide v4, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsGroupView;->mInitializationTimestamp:J

    sub-long v0, v4, v0

    .line 174
    cmp-long v4, v0, v2

    if-lez v4, :cond_0

    .line 176
    :goto_0
    const-wide/32 v4, 0x15180

    div-long v4, v0, v4

    .line 177
    const-wide/16 v6, 0xe10

    div-long v6, v0, v6

    .line 178
    const-wide/16 v8, 0x3c

    div-long/2addr v0, v8

    .line 180
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ntp/RecentTabsGroupView;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    .line 181
    cmp-long v9, v4, v2

    if-lez v9, :cond_1

    .line 182
    sget v0, Lcom/google/android/apps/chrome/R$string;->ntp_recent_tabs_last_synced_days:I

    new-array v1, v11, [Ljava/lang/Object;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v10

    invoke-virtual {v8, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 189
    :goto_1
    return-object v0

    :cond_0
    move-wide v0, v2

    .line 174
    goto :goto_0

    .line 183
    :cond_1
    cmp-long v4, v6, v2

    if-lez v4, :cond_2

    .line 184
    sget v0, Lcom/google/android/apps/chrome/R$string;->ntp_recent_tabs_last_synced_hours:I

    new-array v1, v11, [Ljava/lang/Object;

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v10

    invoke-virtual {v8, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 185
    :cond_2
    cmp-long v2, v0, v2

    if-lez v2, :cond_3

    .line 186
    sget v2, Lcom/google/android/apps/chrome/R$string;->ntp_recent_tabs_last_synced_minutes:I

    new-array v3, v11, [Ljava/lang/Object;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v3, v10

    invoke-virtual {v8, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 189
    :cond_3
    sget v0, Lcom/google/android/apps/chrome/R$string;->ntp_recent_tabs_last_synced_just_now:I

    invoke-virtual {v8, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method private static replaceRule(Landroid/view/View;II)V
    .locals 2

    .prologue
    .line 211
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 212
    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 213
    invoke-virtual {v0, p2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 214
    return-void
.end method

.method private setTimeLabelVisibility(I)V
    .locals 3

    .prologue
    const/16 v2, 0xf

    const/16 v1, 0xa

    .line 199
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsGroupView;->mTimeLabel:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    if-ne v0, p1, :cond_0

    .line 208
    :goto_0
    return-void

    .line 200
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsGroupView;->mTimeLabel:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 201
    const/16 v0, 0x8

    if-ne p1, v0, :cond_1

    .line 202
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsGroupView;->mDeviceIcon:Landroid/widget/ImageView;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/ntp/RecentTabsGroupView;->replaceRule(Landroid/view/View;II)V

    .line 203
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsGroupView;->mDeviceLabel:Landroid/widget/TextView;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/ntp/RecentTabsGroupView;->replaceRule(Landroid/view/View;II)V

    goto :goto_0

    .line 205
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsGroupView;->mDeviceIcon:Landroid/widget/ImageView;

    invoke-static {v0, v2, v1}, Lcom/google/android/apps/chrome/ntp/RecentTabsGroupView;->replaceRule(Landroid/view/View;II)V

    .line 206
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsGroupView;->mDeviceLabel:Landroid/widget/TextView;

    invoke-static {v0, v2, v1}, Lcom/google/android/apps/chrome/ntp/RecentTabsGroupView;->replaceRule(Landroid/view/View;II)V

    goto :goto_0
.end method


# virtual methods
.method public configureForCurrentlyOpenTabs(Z)V
    .locals 2

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsGroupView;->mDeviceIcon:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 82
    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsGroupView;->mDeviceIcon:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ntp/RecentTabsGroupView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/ui/base/DeviceFormFactor;->isTablet(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget v0, Lcom/google/android/apps/chrome/R$drawable;->recent_tablet:I

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 84
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ntp/RecentTabsGroupView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$string;->recent_tabs_this_device:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 85
    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsGroupView;->mDeviceLabel:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 86
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/ntp/RecentTabsGroupView;->setTimeLabelVisibility(I)V

    .line 87
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/ntp/RecentTabsGroupView;->configureExpandedCollapsed(Z)V

    .line 88
    return-void

    .line 82
    :cond_0
    sget v0, Lcom/google/android/apps/chrome/R$drawable;->recent_phone:I

    goto :goto_0
.end method

.method public configureForForeignSession(Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSession;Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 97
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsGroupView;->mDeviceIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 98
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsGroupView;->mDeviceLabel:Landroid/widget/TextView;

    iget-object v1, p1, Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSession;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 99
    invoke-direct {p0, v2}, Lcom/google/android/apps/chrome/ntp/RecentTabsGroupView;->setTimeLabelVisibility(I)V

    .line 100
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsGroupView;->mTimeLabel:Landroid/widget/TextView;

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/ntp/RecentTabsGroupView;->getTimeString(Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSession;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 101
    iget v0, p1, Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSession;->deviceType:I

    packed-switch v0, :pswitch_data_0

    .line 109
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsGroupView;->mDeviceIcon:Landroid/widget/ImageView;

    sget v1, Lcom/google/android/apps/chrome/R$drawable;->recent_laptop:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 112
    :goto_0
    invoke-direct {p0, p2}, Lcom/google/android/apps/chrome/ntp/RecentTabsGroupView;->configureExpandedCollapsed(Z)V

    .line 113
    return-void

    .line 103
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsGroupView;->mDeviceIcon:Landroid/widget/ImageView;

    sget v1, Lcom/google/android/apps/chrome/R$drawable;->recent_phone:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 106
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsGroupView;->mDeviceIcon:Landroid/widget/ImageView;

    sget v1, Lcom/google/android/apps/chrome/R$drawable;->recent_tablet:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 101
    nop

    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public configureForRecentlyClosedTabs(Z)V
    .locals 2

    .prologue
    .line 134
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsGroupView;->mDeviceIcon:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 135
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsGroupView;->mDeviceIcon:Landroid/widget/ImageView;

    sget v1, Lcom/google/android/apps/chrome/R$drawable;->recent_recently_closed:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 136
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsGroupView;->mDeviceLabel:Landroid/widget/TextView;

    sget v1, Lcom/google/android/apps/chrome/R$string;->recently_closed:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 137
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/ntp/RecentTabsGroupView;->setTimeLabelVisibility(I)V

    .line 138
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/ntp/RecentTabsGroupView;->configureExpandedCollapsed(Z)V

    .line 139
    return-void
.end method

.method public configureForSnapshotDocument(Z)V
    .locals 2

    .prologue
    .line 121
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsGroupView;->mDeviceIcon:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 122
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsGroupView;->mDeviceIcon:Landroid/widget/ImageView;

    sget v1, Lcom/google/android/apps/chrome/R$drawable;->ntp_chrome_to_mobile_group_icon:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 123
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsGroupView;->mDeviceLabel:Landroid/widget/TextView;

    sget v1, Lcom/google/android/apps/chrome/R$string;->ntp_recent_tabs_chrome_to_mobile_group:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 124
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/ntp/RecentTabsGroupView;->setTimeLabelVisibility(I)V

    .line 125
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/ntp/RecentTabsGroupView;->configureExpandedCollapsed(Z)V

    .line 126
    return-void
.end method

.method public configureForSyncPromo(Z)V
    .locals 2

    .prologue
    .line 147
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsGroupView;->mDeviceIcon:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 148
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsGroupView;->mDeviceIcon:Landroid/widget/ImageView;

    sget v1, Lcom/google/android/apps/chrome/R$drawable;->recent_laptop:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 149
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsGroupView;->mDeviceLabel:Landroid/widget/TextView;

    sget v1, Lcom/google/android/apps/chrome/R$string;->ntp_recent_tabs_sync_promo_title:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 150
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/ntp/RecentTabsGroupView;->setTimeLabelVisibility(I)V

    .line 151
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/ntp/RecentTabsGroupView;->configureExpandedCollapsed(Z)V

    .line 152
    return-void
.end method

.method public initialize(J)V
    .locals 1

    .prologue
    .line 72
    iput-wide p1, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsGroupView;->mInitializationTimestamp:J

    .line 73
    return-void
.end method

.method public onFinishInflate()V
    .locals 1

    .prologue
    .line 59
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 60
    sget v0, Lcom/google/android/apps/chrome/R$id;->device_icon:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ntp/RecentTabsGroupView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsGroupView;->mDeviceIcon:Landroid/widget/ImageView;

    .line 61
    sget v0, Lcom/google/android/apps/chrome/R$id;->time_label:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ntp/RecentTabsGroupView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsGroupView;->mTimeLabel:Landroid/widget/TextView;

    .line 62
    sget v0, Lcom/google/android/apps/chrome/R$id;->device_label:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ntp/RecentTabsGroupView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsGroupView;->mDeviceLabel:Landroid/widget/TextView;

    .line 63
    sget v0, Lcom/google/android/apps/chrome/R$id;->expand_collapse_icon:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ntp/RecentTabsGroupView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsGroupView;->mExpandCollapseIcon:Landroid/widget/ImageView;

    .line 64
    return-void
.end method
