.class public Lcom/google/android/apps/chrome/playlog/PlayLogTracker;
.super Ljava/lang/Object;
.source "PlayLogTracker.java"


# static fields
.field private static final sLock:Ljava/lang/Object;

.field private static sPlayLogger:Lcom/google/android/gms/b/a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/apps/chrome/playlog/PlayLogTracker;->sLock:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000()Lcom/google/android/gms/b/a;
    .locals 1

    .prologue
    .line 18
    sget-object v0, Lcom/google/android/apps/chrome/playlog/PlayLogTracker;->sPlayLogger:Lcom/google/android/gms/b/a;

    return-object v0
.end method

.method public static createPlayLogger(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 31
    sget-object v1, Lcom/google/android/apps/chrome/playlog/PlayLogTracker;->sLock:Ljava/lang/Object;

    monitor-enter v1

    .line 32
    :try_start_0
    sget-object v0, Lcom/google/android/apps/chrome/playlog/PlayLogTracker;->sPlayLogger:Lcom/google/android/gms/b/a;

    if-eqz v0, :cond_0

    monitor-exit v1

    .line 56
    :goto_0
    return-void

    .line 33
    :cond_0
    invoke-static {p0}, Lcom/google/android/apps/chrome/externalauth/ExternalAuthUtils;->canUseFirstPartyGooglePlayServices(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 56
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 35
    :cond_1
    :try_start_1
    new-instance v0, Lcom/google/android/gms/b/a;

    const/16 v2, 0xb

    new-instance v3, Lcom/google/android/apps/chrome/playlog/PlayLogTracker$1;

    invoke-direct {v3, p0}, Lcom/google/android/apps/chrome/playlog/PlayLogTracker$1;-><init>(Landroid/content/Context;)V

    invoke-direct {v0, p0, v2, v3}, Lcom/google/android/gms/b/a;-><init>(Landroid/content/Context;ILcom/google/android/gms/b/b;)V

    .line 55
    sput-object v0, Lcom/google/android/apps/chrome/playlog/PlayLogTracker;->sPlayLogger:Lcom/google/android/gms/b/a;

    invoke-virtual {v0}, Lcom/google/android/gms/b/a;->a()V

    .line 56
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public static stopPlayLogger()V
    .locals 2

    .prologue
    .line 63
    sget-object v1, Lcom/google/android/apps/chrome/playlog/PlayLogTracker;->sLock:Ljava/lang/Object;

    monitor-enter v1

    .line 64
    :try_start_0
    sget-object v0, Lcom/google/android/apps/chrome/playlog/PlayLogTracker;->sPlayLogger:Lcom/google/android/gms/b/a;

    if-nez v0, :cond_0

    monitor-exit v1

    .line 68
    :goto_0
    return-void

    .line 66
    :cond_0
    sget-object v0, Lcom/google/android/apps/chrome/playlog/PlayLogTracker;->sPlayLogger:Lcom/google/android/gms/b/a;

    invoke-virtual {v0}, Lcom/google/android/gms/b/a;->b()V

    .line 67
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/apps/chrome/playlog/PlayLogTracker;->sPlayLogger:Lcom/google/android/gms/b/a;

    .line 68
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
