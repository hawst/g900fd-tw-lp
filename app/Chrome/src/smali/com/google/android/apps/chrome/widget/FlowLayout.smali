.class public Lcom/google/android/apps/chrome/widget/FlowLayout;
.super Landroid/view/ViewGroup;
.source "FlowLayout.java"


# instance fields
.field private mHorizontalSpacing:I

.field private mVerticalSpacing:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 26
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/FlowLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    const/high16 v1, 0x40400000    # 3.0f

    mul-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 28
    iput v0, p0, Lcom/google/android/apps/chrome/widget/FlowLayout;->mHorizontalSpacing:I

    .line 29
    iput v0, p0, Lcom/google/android/apps/chrome/widget/FlowLayout;->mVerticalSpacing:I

    .line 30
    return-void
.end method


# virtual methods
.method protected checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .locals 1

    .prologue
    .line 103
    instance-of v0, p1, Lcom/google/android/apps/chrome/widget/FlowLayout$LayoutParams;

    return v0
.end method

.method protected bridge synthetic generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 18
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/FlowLayout;->generateDefaultLayoutParams()Lcom/google/android/apps/chrome/widget/FlowLayout$LayoutParams;

    move-result-object v0

    return-object v0
.end method

.method protected generateDefaultLayoutParams()Lcom/google/android/apps/chrome/widget/FlowLayout$LayoutParams;
    .locals 2

    .prologue
    const/4 v1, -0x2

    .line 108
    new-instance v0, Lcom/google/android/apps/chrome/widget/FlowLayout$LayoutParams;

    invoke-direct {v0, v1, v1}, Lcom/google/android/apps/chrome/widget/FlowLayout$LayoutParams;-><init>(II)V

    return-object v0
.end method

.method public bridge synthetic generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 18
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/widget/FlowLayout;->generateLayoutParams(Landroid/util/AttributeSet;)Lcom/google/android/apps/chrome/widget/FlowLayout$LayoutParams;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 18
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/widget/FlowLayout;->generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Lcom/google/android/apps/chrome/widget/FlowLayout$LayoutParams;

    move-result-object v0

    return-object v0
.end method

.method public generateLayoutParams(Landroid/util/AttributeSet;)Lcom/google/android/apps/chrome/widget/FlowLayout$LayoutParams;
    .locals 2

    .prologue
    .line 113
    new-instance v0, Lcom/google/android/apps/chrome/widget/FlowLayout$LayoutParams;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/FlowLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/google/android/apps/chrome/widget/FlowLayout$LayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method protected generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Lcom/google/android/apps/chrome/widget/FlowLayout$LayoutParams;
    .locals 3

    .prologue
    .line 118
    new-instance v0, Lcom/google/android/apps/chrome/widget/FlowLayout$LayoutParams;

    iget v1, p1, Landroid/view/ViewGroup$LayoutParams;->width:I

    iget v2, p1, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/chrome/widget/FlowLayout$LayoutParams;-><init>(II)V

    return-object v0
.end method

.method protected onLayout(ZIIII)V
    .locals 9

    .prologue
    .line 90
    sub-int v3, p4, p2

    .line 91
    invoke-static {p0}, Lorg/chromium/base/ApiCompatibilityUtils;->isLayoutRtl(Landroid/view/View;)Z

    move-result v4

    .line 92
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/FlowLayout;->getChildCount()I

    move-result v0

    if-ge v2, v0, :cond_1

    .line 93
    invoke-virtual {p0, v2}, Lcom/google/android/apps/chrome/widget/FlowLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 94
    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/widget/FlowLayout$LayoutParams;

    .line 95
    if-eqz v4, :cond_0

    iget v1, v0, Lcom/google/android/apps/chrome/widget/FlowLayout$LayoutParams;->start:I

    sub-int v1, v3, v1

    invoke-virtual {v5}, Landroid/view/View;->getMeasuredWidth()I

    move-result v6

    sub-int/2addr v1, v6

    .line 96
    :goto_1
    iget v6, v0, Lcom/google/android/apps/chrome/widget/FlowLayout$LayoutParams;->top:I

    invoke-virtual {v5}, Landroid/view/View;->getMeasuredWidth()I

    move-result v7

    add-int/2addr v7, v1

    iget v0, v0, Lcom/google/android/apps/chrome/widget/FlowLayout$LayoutParams;->top:I

    invoke-virtual {v5}, Landroid/view/View;->getMeasuredHeight()I

    move-result v8

    add-int/2addr v0, v8

    invoke-virtual {v5, v1, v6, v7, v0}, Landroid/view/View;->layout(IIII)V

    .line 92
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 95
    :cond_0
    iget v1, v0, Lcom/google/android/apps/chrome/widget/FlowLayout$LayoutParams;->start:I

    goto :goto_1

    .line 99
    :cond_1
    return-void
.end method

.method protected onMeasure(II)V
    .locals 13

    .prologue
    .line 34
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    invoke-static {p0}, Lorg/chromium/base/ApiCompatibilityUtils;->getPaddingEnd(Landroid/view/View;)I

    move-result v1

    sub-int v9, v0, v1

    .line 36
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    .line 38
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    move v1, v0

    .line 40
    :goto_0
    const/4 v7, 0x0

    .line 41
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/FlowLayout;->getPaddingTop()I

    move-result v6

    .line 43
    invoke-static {p0}, Lorg/chromium/base/ApiCompatibilityUtils;->getPaddingStart(Landroid/view/View;)I

    move-result v5

    .line 44
    const/4 v4, 0x0

    .line 46
    const/4 v3, 0x0

    .line 47
    const/4 v2, 0x0

    .line 49
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/FlowLayout;->getChildCount()I

    move-result v10

    .line 50
    const/4 v0, 0x0

    move v12, v0

    move v0, v2

    move v2, v3

    move v3, v4

    move v4, v5

    move v5, v6

    move v6, v7

    move v7, v12

    :goto_1
    if-ge v7, v10, :cond_2

    .line 51
    invoke-virtual {p0, v7}, Lcom/google/android/apps/chrome/widget/FlowLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v11

    .line 52
    invoke-virtual {p0, v11, p1, p2}, Lcom/google/android/apps/chrome/widget/FlowLayout;->measureChild(Landroid/view/View;II)V

    .line 54
    invoke-virtual {v11}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/widget/FlowLayout$LayoutParams;

    .line 55
    iget v8, p0, Lcom/google/android/apps/chrome/widget/FlowLayout;->mHorizontalSpacing:I

    .line 57
    if-eqz v1, :cond_1

    invoke-virtual {v11}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    add-int/2addr v2, v4

    if-le v2, v9, :cond_1

    .line 58
    iget v2, p0, Lcom/google/android/apps/chrome/widget/FlowLayout;->mVerticalSpacing:I

    add-int/2addr v2, v3

    add-int/2addr v5, v2

    .line 59
    const/4 v3, 0x0

    .line 60
    sub-int v2, v4, v8

    invoke-static {v6, v2}, Ljava/lang/Math;->max(II)I

    move-result v6

    .line 61
    invoke-static {p0}, Lorg/chromium/base/ApiCompatibilityUtils;->getPaddingStart(Landroid/view/View;)I

    move-result v4

    .line 62
    const/4 v2, 0x1

    .line 67
    :goto_2
    iput v4, v0, Lcom/google/android/apps/chrome/widget/FlowLayout$LayoutParams;->start:I

    .line 68
    iput v5, v0, Lcom/google/android/apps/chrome/widget/FlowLayout$LayoutParams;->top:I

    .line 70
    invoke-virtual {v11}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    add-int/2addr v0, v8

    add-int/2addr v4, v0

    .line 71
    invoke-virtual {v11}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 50
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    move v0, v8

    goto :goto_1

    .line 38
    :cond_0
    const/4 v0, 0x0

    move v1, v0

    goto :goto_0

    .line 64
    :cond_1
    const/4 v2, 0x0

    goto :goto_2

    .line 75
    :cond_2
    if-nez v2, :cond_3

    .line 76
    add-int/2addr v5, v3

    .line 77
    sub-int v0, v4, v0

    invoke-static {v6, v0}, Ljava/lang/Math;->max(II)I

    move-result v6

    .line 80
    :cond_3
    invoke-static {p0}, Lorg/chromium/base/ApiCompatibilityUtils;->getPaddingEnd(Landroid/view/View;)I

    move-result v0

    add-int/2addr v0, v6

    .line 81
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/FlowLayout;->getPaddingBottom()I

    move-result v1

    add-int/2addr v1, v5

    .line 83
    invoke-static {v0, p1}, Lcom/google/android/apps/chrome/widget/FlowLayout;->resolveSize(II)I

    move-result v0

    invoke-static {v1, p2}, Lcom/google/android/apps/chrome/widget/FlowLayout;->resolveSize(II)I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/chrome/widget/FlowLayout;->setMeasuredDimension(II)V

    .line 85
    return-void
.end method
