.class Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$LocalStorageInfoFetcher$1;
.super Ljava/lang/Object;
.source "WebsitePreferences.java"

# interfaces
.implements Lcom/google/android/apps/chrome/preferences/website/WebsitePreferenceBridge$LocalStorageInfoReadyCallback;


# instance fields
.field final synthetic this$1:Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$LocalStorageInfoFetcher;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$LocalStorageInfoFetcher;)V
    .locals 0

    .prologue
    .line 126
    iput-object p1, p0, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$LocalStorageInfoFetcher$1;->this$1:Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$LocalStorageInfoFetcher;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLocalStorageInfoReady(Ljava/util/HashMap;)V
    .locals 5

    .prologue
    .line 130
    invoke-virtual {p1}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 131
    check-cast v0, Ljava/util/Map$Entry;

    .line 133
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/apps/chrome/preferences/website/WebsiteAddress;->create(Ljava/lang/String;)Lcom/google/android/apps/chrome/preferences/website/WebsiteAddress;

    move-result-object v1

    .line 134
    if-eqz v1, :cond_0

    .line 135
    iget-object v2, p0, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$LocalStorageInfoFetcher$1;->this$1:Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$LocalStorageInfoFetcher;

    iget-object v2, v2, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$LocalStorageInfoFetcher;->this$0:Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences;

    # invokes: Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences;->findOrCreateSitesByOrigin(Lcom/google/android/apps/chrome/preferences/website/WebsiteAddress;)Ljava/util/Set;
    invoke-static {v2, v1}, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences;->access$1100(Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences;Lcom/google/android/apps/chrome/preferences/website/WebsiteAddress;)Ljava/util/Set;

    move-result-object v1

    .line 136
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/chrome/preferences/website/Website;

    .line 137
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/chrome/preferences/website/LocalStorageInfo;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/chrome/preferences/website/Website;->setLocalStorageInfo(Lcom/google/android/apps/chrome/preferences/website/LocalStorageInfo;)V

    goto :goto_0

    .line 140
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$LocalStorageInfoFetcher$1;->this$1:Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$LocalStorageInfoFetcher;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$LocalStorageInfoFetcher;->next()V

    .line 141
    return-void
.end method
