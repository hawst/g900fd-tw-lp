.class public Lcom/google/android/apps/chrome/compositor/SingleTabRenderView;
.super Lorg/chromium/content/browser/ContentViewRenderView;
.source "SingleTabRenderView.java"

# interfaces
.implements Lcom/google/android/apps/chrome/compositor/ContentOffsetProvider;
.implements Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager$FullscreenListener;


# instance fields
.field private mBuildHelper:Lcom/google/android/apps/chrome/compositor/ToolbarLayerTreeBuildHelper;

.field private mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;

.field private mToolbarResource:Lcom/google/android/apps/chrome/compositor/resources/dynamics/ViewResourceAdapter;


# virtual methods
.method public getOverlayTranslateY()I
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/SingleTabRenderView;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 98
    :goto_0
    return v0

    .line 95
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/SingleTabRenderView;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->areTopControlsPermanentlyHidden()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/SingleTabRenderView;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->getTopControlsHeight()I

    move-result v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/SingleTabRenderView;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->getVisibleContentOffset()F

    move-result v0

    float-to-int v0, v0

    goto :goto_0
.end method

.method protected onCompositorLayout()V
    .locals 4

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/SingleTabRenderView;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;

    if-eqz v0, :cond_2

    .line 67
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/SingleTabRenderView;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->controlContainerHeight()F

    move-result v1

    .line 68
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/SingleTabRenderView;->mToolbarResource:Lcom/google/android/apps/chrome/compositor/resources/dynamics/ViewResourceAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/compositor/resources/dynamics/ViewResourceAdapter;->isDirty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 69
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/SingleTabRenderView;->mBuildHelper:Lcom/google/android/apps/chrome/compositor/ToolbarLayerTreeBuildHelper;

    iget-object v2, p0, Lcom/google/android/apps/chrome/compositor/SingleTabRenderView;->mToolbarResource:Lcom/google/android/apps/chrome/compositor/resources/dynamics/ViewResourceAdapter;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/compositor/resources/dynamics/ViewResourceAdapter;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, Lcom/google/android/apps/chrome/compositor/ToolbarLayerTreeBuildHelper;->updateToolbarTexture(Landroid/graphics/Bitmap;F)V

    .line 72
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/SingleTabRenderView;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->getControlOffset()F

    move-result v2

    .line 73
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/SingleTabRenderView;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->drawControlsAsTexture()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    cmpl-float v0, v2, v0

    if-nez v0, :cond_3

    :cond_1
    const/4 v0, 0x1

    .line 74
    :goto_0
    iget-object v3, p0, Lcom/google/android/apps/chrome/compositor/SingleTabRenderView;->mBuildHelper:Lcom/google/android/apps/chrome/compositor/ToolbarLayerTreeBuildHelper;

    invoke-virtual {v3, v2, v1, v0}, Lcom/google/android/apps/chrome/compositor/ToolbarLayerTreeBuildHelper;->updateToolbarLayer(FFZ)V

    .line 76
    :cond_2
    invoke-super {p0}, Lorg/chromium/content/browser/ContentViewRenderView;->onCompositorLayout()V

    .line 77
    return-void

    .line 73
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onContentOffsetChanged(F)V
    .locals 0

    .prologue
    .line 81
    return-void
.end method

.method public onNativeLibraryLoaded(Lorg/chromium/ui/base/WindowAndroid;)V
    .locals 2

    .prologue
    .line 39
    invoke-super {p0, p1}, Lorg/chromium/content/browser/ContentViewRenderView;->onNativeLibraryLoaded(Lorg/chromium/ui/base/WindowAndroid;)V

    .line 40
    new-instance v0, Lcom/google/android/apps/chrome/compositor/ToolbarLayerTreeBuildHelper;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/compositor/ToolbarLayerTreeBuildHelper;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/compositor/SingleTabRenderView;->mBuildHelper:Lcom/google/android/apps/chrome/compositor/ToolbarLayerTreeBuildHelper;

    .line 41
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/SingleTabRenderView;->mBuildHelper:Lcom/google/android/apps/chrome/compositor/ToolbarLayerTreeBuildHelper;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/compositor/ToolbarLayerTreeBuildHelper;->getNativePointer()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/chrome/compositor/SingleTabRenderView;->setLayerTreeBuildHelper(J)V

    .line 42
    return-void
.end method

.method public onToggleOverlayVideoMode(Z)V
    .locals 0

    .prologue
    .line 90
    return-void
.end method

.method public onVisibleContentOffsetChanged(F)V
    .locals 0

    .prologue
    .line 85
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/compositor/SingleTabRenderView;->setNeedsComposite()V

    .line 86
    return-void
.end method
