.class Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog$5;
.super Landroid/animation/AnimatorListenerAdapter;
.source "EnhancedBookmarkDetailDialog.java"


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;)V
    .locals 0

    .prologue
    .line 301
    iput-object p1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog$5;->this$0:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 3

    .prologue
    .line 304
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog$5;->this$0:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;

    # getter for: Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mScrollView:Landroid/widget/ScrollView;
    invoke-static {v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->access$600(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;)Landroid/widget/ScrollView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->setTranslationY(F)V

    .line 305
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog$5;->this$0:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;

    # getter for: Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mMaskView:Landroid/view/View;
    invoke-static {v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->access$800(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 307
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog$5;->this$0:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;

    # getter for: Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mTitleEditText:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->access$1100(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->clearFocus()V

    .line 308
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog$5;->this$0:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;

    # getter for: Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mUrlEditText:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->access$1200(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->clearFocus()V

    .line 309
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog$5;->this$0:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string/jumbo v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 311
    iget-object v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog$5;->this$0:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;

    # getter for: Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mTitleEditText:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->access$1100(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 312
    return-void
.end method
