.class Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;
.super Ljava/lang/Object;
.source "OverScroller.java"


# static fields
.field private static final DECELERATION_RATE:F

.field private static final SPLINE_POSITION:[F

.field private static final SPLINE_TIME:[F

.field private static sPhysicalCoef:F


# instance fields
.field private mCurrVelocity:F

.field private mCurrentPosition:I

.field private mDeceleration:F

.field private mDuration:I

.field private mFinal:I

.field private mFinished:Z

.field private mFlingFriction:F

.field private mOver:I

.field private mSplineDistance:I

.field private mSplineDuration:I

.field private mStart:I

.field private mStartTime:J

.field private mState:I

.field private mTempScrollState:Lcom/google/android/apps/chrome/third_party/OverScroller$ScrollState;

.field private mVelocity:I


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .prologue
    const v13, 0x3e333333    # 0.175f

    const/4 v4, 0x0

    const-wide v14, 0x3ee4f8b588e368f1L    # 1.0E-5

    const/16 v12, 0x64

    const/high16 v1, 0x3f800000    # 1.0f

    .line 589
    const-wide v2, 0x3fe8f5c28f5c28f6L    # 0.78

    invoke-static {v2, v3}, Ljava/lang/Math;->log(D)D

    move-result-wide v2

    const-wide v6, 0x3feccccccccccccdL    # 0.9

    invoke-static {v6, v7}, Ljava/lang/Math;->log(D)D

    move-result-wide v6

    div-double/2addr v2, v6

    double-to-float v0, v2

    sput v0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->DECELERATION_RATE:F

    .line 597
    const/16 v0, 0x65

    new-array v0, v0, [F

    sput-object v0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->SPLINE_POSITION:[F

    .line 598
    const/16 v0, 0x65

    new-array v0, v0, [F

    sput-object v0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->SPLINE_TIME:[F

    .line 612
    const/4 v0, 0x0

    move v5, v0

    move v2, v4

    :goto_0
    if-ge v5, v12, :cond_4

    .line 613
    int-to-float v0, v5

    const/high16 v3, 0x42c80000    # 100.0f

    div-float v6, v0, v3

    move v0, v1

    move v3, v2

    .line 618
    :goto_1
    sub-float v2, v0, v3

    const/high16 v7, 0x40000000    # 2.0f

    div-float/2addr v2, v7

    add-float/2addr v2, v3

    .line 619
    const/high16 v7, 0x40400000    # 3.0f

    mul-float/2addr v7, v2

    sub-float v8, v1, v2

    mul-float/2addr v7, v8

    .line 620
    sub-float v8, v1, v2

    mul-float/2addr v8, v13

    const v9, 0x3eb33334    # 0.35000002f

    mul-float/2addr v9, v2

    add-float/2addr v8, v9

    mul-float/2addr v8, v7

    mul-float v9, v2, v2

    mul-float/2addr v9, v2

    add-float/2addr v8, v9

    .line 621
    sub-float v9, v8, v6

    invoke-static {v9}, Ljava/lang/Math;->abs(F)F

    move-result v9

    float-to-double v10, v9

    cmpg-double v9, v10, v14

    if-ltz v9, :cond_1

    .line 622
    cmpl-float v7, v8, v6

    if-lez v7, :cond_0

    move v0, v2

    goto :goto_1

    :cond_0
    move v3, v2

    .line 623
    goto :goto_1

    .line 625
    :cond_1
    sget-object v0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->SPLINE_POSITION:[F

    sub-float v8, v1, v2

    const/high16 v9, 0x3f000000    # 0.5f

    mul-float/2addr v8, v9

    add-float/2addr v8, v2

    mul-float/2addr v7, v8

    mul-float v8, v2, v2

    mul-float/2addr v2, v8

    add-float/2addr v2, v7

    aput v2, v0, v5

    move v0, v1

    .line 630
    :goto_2
    sub-float v2, v0, v4

    const/high16 v7, 0x40000000    # 2.0f

    div-float/2addr v2, v7

    add-float/2addr v2, v4

    .line 631
    const/high16 v7, 0x40400000    # 3.0f

    mul-float/2addr v7, v2

    sub-float v8, v1, v2

    mul-float/2addr v7, v8

    .line 632
    sub-float v8, v1, v2

    const/high16 v9, 0x3f000000    # 0.5f

    mul-float/2addr v8, v9

    add-float/2addr v8, v2

    mul-float/2addr v8, v7

    mul-float v9, v2, v2

    mul-float/2addr v9, v2

    add-float/2addr v8, v9

    .line 633
    sub-float v9, v8, v6

    invoke-static {v9}, Ljava/lang/Math;->abs(F)F

    move-result v9

    float-to-double v10, v9

    cmpg-double v9, v10, v14

    if-ltz v9, :cond_3

    .line 634
    cmpl-float v7, v8, v6

    if-lez v7, :cond_2

    move v0, v2

    goto :goto_2

    :cond_2
    move v4, v2

    .line 635
    goto :goto_2

    .line 637
    :cond_3
    sget-object v0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->SPLINE_TIME:[F

    sub-float v6, v1, v2

    mul-float/2addr v6, v13

    const v8, 0x3eb33334    # 0.35000002f

    mul-float/2addr v8, v2

    add-float/2addr v6, v8

    mul-float/2addr v6, v7

    mul-float v7, v2, v2

    mul-float/2addr v2, v7

    add-float/2addr v2, v6

    aput v2, v0, v5

    .line 612
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    move v2, v3

    goto/16 :goto_0

    .line 639
    :cond_4
    sget-object v0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->SPLINE_POSITION:[F

    sget-object v2, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->SPLINE_TIME:[F

    aput v1, v2, v12

    aput v1, v0, v12

    .line 640
    return-void
.end method

.method constructor <init>()V
    .locals 2

    .prologue
    .line 654
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 578
    invoke-static {}, Landroid/view/ViewConfiguration;->getScrollFriction()F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mFlingFriction:F

    .line 581
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mState:I

    .line 607
    new-instance v0, Lcom/google/android/apps/chrome/third_party/OverScroller$ScrollState;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/apps/chrome/third_party/OverScroller$ScrollState;-><init>(Lcom/google/android/apps/chrome/third_party/OverScroller$1;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mTempScrollState:Lcom/google/android/apps/chrome/third_party/OverScroller$ScrollState;

    .line 655
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mFinished:Z

    .line 656
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;)Z
    .locals 1

    .prologue
    .line 540
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mFinished:Z

    return v0
.end method

.method static synthetic access$002(Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;Z)Z
    .locals 0

    .prologue
    .line 540
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mFinished:Z

    return p1
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;)I
    .locals 1

    .prologue
    .line 540
    iget v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mCurrentPosition:I

    return v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;)F
    .locals 1

    .prologue
    .line 540
    iget v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mCurrVelocity:F

    return v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;)I
    .locals 1

    .prologue
    .line 540
    iget v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mFinal:I

    return v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;)I
    .locals 1

    .prologue
    .line 540
    iget v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mDuration:I

    return v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;)J
    .locals 2

    .prologue
    .line 540
    iget-wide v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mStartTime:J

    return-wide v0
.end method

.method private adjustDuration(III)V
    .locals 6

    .prologue
    const/high16 v4, 0x42c80000    # 100.0f

    .line 674
    sub-int v0, p2, p1

    .line 675
    sub-int v1, p3, p1

    .line 676
    int-to-float v1, v1

    int-to-float v0, v0

    div-float v0, v1, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    .line 677
    mul-float v1, v4, v0

    float-to-int v1, v1

    .line 678
    const/16 v2, 0x64

    if-ge v1, v2, :cond_0

    .line 679
    int-to-float v2, v1

    div-float/2addr v2, v4

    .line 680
    add-int/lit8 v3, v1, 0x1

    int-to-float v3, v3

    div-float/2addr v3, v4

    .line 681
    sget-object v4, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->SPLINE_TIME:[F

    aget v4, v4, v1

    .line 682
    sget-object v5, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->SPLINE_TIME:[F

    add-int/lit8 v1, v1, 0x1

    aget v1, v5, v1

    .line 683
    sub-float/2addr v0, v2

    sub-float v2, v3, v2

    div-float/2addr v0, v2

    sub-float/2addr v1, v4

    mul-float/2addr v0, v1

    add-float/2addr v0, v4

    .line 684
    iget v1, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mDuration:I

    int-to-float v1, v1

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mDuration:I

    .line 686
    :cond_0
    return-void
.end method

.method private computeStateAtTime(J)Lcom/google/android/apps/chrome/third_party/OverScroller$ScrollState;
    .locals 9

    .prologue
    const/high16 v7, 0x447a0000    # 1000.0f

    const/high16 v6, 0x40000000    # 2.0f

    const/high16 v5, 0x42c80000    # 100.0f

    .line 948
    iget v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mDuration:I

    int-to-long v0, v0

    cmp-long v0, p1, v0

    if-lez v0, :cond_0

    .line 949
    const/4 v0, 0x0

    .line 992
    :goto_0
    return-object v0

    .line 952
    :cond_0
    const-wide/16 v0, 0x0

    .line 953
    iget v2, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mState:I

    packed-switch v2, :pswitch_data_0

    .line 987
    :goto_1
    iget-object v2, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mTempScrollState:Lcom/google/android/apps/chrome/third_party/OverScroller$ScrollState;

    iget v3, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mStart:I

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    long-to-int v0, v0

    add-int/2addr v0, v3

    iput v0, v2, Lcom/google/android/apps/chrome/third_party/OverScroller$ScrollState;->mPosition:I

    .line 992
    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mTempScrollState:Lcom/google/android/apps/chrome/third_party/OverScroller$ScrollState;

    goto :goto_0

    .line 955
    :pswitch_0
    long-to-float v0, p1

    iget v1, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mSplineDuration:I

    int-to-float v1, v1

    div-float v2, v0, v1

    .line 956
    mul-float v0, v5, v2

    float-to-int v3, v0

    .line 957
    const/high16 v1, 0x3f800000    # 1.0f

    .line 958
    const/4 v0, 0x0

    .line 959
    const/16 v4, 0x64

    if-ge v3, v4, :cond_1

    .line 960
    int-to-float v0, v3

    div-float v1, v0, v5

    .line 961
    add-int/lit8 v0, v3, 0x1

    int-to-float v0, v0

    div-float/2addr v0, v5

    .line 962
    sget-object v4, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->SPLINE_POSITION:[F

    aget v4, v4, v3

    .line 963
    sget-object v5, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->SPLINE_POSITION:[F

    add-int/lit8 v3, v3, 0x1

    aget v3, v5, v3

    .line 964
    sub-float/2addr v3, v4

    sub-float/2addr v0, v1

    div-float v0, v3, v0

    .line 965
    sub-float v1, v2, v1

    mul-float/2addr v1, v0

    add-float/2addr v1, v4

    .line 968
    :cond_1
    iget v2, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mSplineDistance:I

    int-to-float v2, v2

    mul-float/2addr v1, v2

    float-to-double v2, v1

    .line 969
    iget-object v1, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mTempScrollState:Lcom/google/android/apps/chrome/third_party/OverScroller$ScrollState;

    iget v4, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mSplineDistance:I

    int-to-float v4, v4

    mul-float/2addr v0, v4

    iget v4, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mSplineDuration:I

    int-to-float v4, v4

    div-float/2addr v0, v4

    mul-float/2addr v0, v7

    iput v0, v1, Lcom/google/android/apps/chrome/third_party/OverScroller$ScrollState;->mVelocity:F

    move-wide v0, v2

    .line 971
    goto :goto_1

    .line 975
    :pswitch_1
    long-to-float v0, p1

    div-float/2addr v0, v7

    .line 976
    iget-object v1, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mTempScrollState:Lcom/google/android/apps/chrome/third_party/OverScroller$ScrollState;

    iget v2, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mVelocity:I

    int-to-float v2, v2

    iget v3, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mDeceleration:F

    mul-float/2addr v3, v0

    add-float/2addr v2, v3

    iput v2, v1, Lcom/google/android/apps/chrome/third_party/OverScroller$ScrollState;->mVelocity:F

    .line 977
    iget v1, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mVelocity:I

    int-to-float v1, v1

    mul-float/2addr v1, v0

    iget v2, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mDeceleration:F

    mul-float/2addr v2, v0

    mul-float/2addr v0, v2

    div-float/2addr v0, v6

    add-float/2addr v0, v1

    float-to-double v0, v0

    .line 978
    goto :goto_1

    .line 982
    :pswitch_2
    long-to-float v0, p1

    iget v1, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mDuration:I

    int-to-float v1, v1

    div-float v2, v0, v1

    .line 983
    mul-float v3, v2, v2

    .line 984
    iget v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mVelocity:I

    int-to-float v0, v0

    invoke-static {v0}, Ljava/lang/Math;->signum(F)F

    move-result v4

    .line 985
    iget v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mOver:I

    int-to-float v0, v0

    mul-float/2addr v0, v4

    const/high16 v1, 0x40400000    # 3.0f

    mul-float/2addr v1, v3

    mul-float v5, v2, v6

    mul-float/2addr v5, v3

    sub-float/2addr v1, v5

    mul-float/2addr v0, v1

    float-to-double v0, v0

    .line 986
    iget-object v5, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mTempScrollState:Lcom/google/android/apps/chrome/third_party/OverScroller$ScrollState;

    iget v6, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mOver:I

    int-to-float v6, v6

    mul-float/2addr v4, v6

    const/high16 v6, 0x40c00000    # 6.0f

    mul-float/2addr v4, v6

    neg-float v2, v2

    add-float/2addr v2, v3

    mul-float/2addr v2, v4

    iput v2, v5, Lcom/google/android/apps/chrome/third_party/OverScroller$ScrollState;->mVelocity:F

    goto/16 :goto_1

    .line 953
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method private fitOnBounceCurve(III)V
    .locals 6

    .prologue
    const/high16 v4, 0x40000000    # 2.0f

    .line 816
    mul-int v0, p3, p3

    int-to-float v0, v0

    div-float/2addr v0, v4

    iget v1, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mDeceleration:F

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    div-float/2addr v0, v1

    .line 817
    sub-int v1, p2, p1

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    int-to-float v1, v1

    .line 818
    add-float v2, v1, v0

    iget v3, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mOver:I

    int-to-float v3, v3

    cmpl-float v2, v2, v3

    if-lez v2, :cond_0

    .line 819
    iget v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mOver:I

    int-to-float v0, v0

    sub-float/2addr v0, v1

    .line 820
    iget v2, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mVelocity:I

    int-to-float v2, v2

    invoke-static {v2}, Ljava/lang/Math;->signum(F)F

    move-result v2

    .line 821
    neg-float v2, v2

    int-to-float v3, p3

    mul-float/2addr v2, v3

    int-to-float v3, p3

    mul-float/2addr v2, v3

    div-float/2addr v2, v4

    div-float/2addr v2, v0

    iput v2, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mDeceleration:F

    .line 824
    :cond_0
    neg-int v2, p3

    int-to-float v2, v2

    iget v3, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mDeceleration:F

    div-float/2addr v2, v3

    .line 825
    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    add-float/2addr v0, v1

    float-to-double v0, v0

    mul-double/2addr v0, v4

    iget v3, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mDeceleration:F

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    float-to-double v4, v3

    div-double/2addr v0, v4

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v0, v0

    .line 827
    iget-wide v4, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mStartTime:J

    const/high16 v1, 0x447a0000    # 1000.0f

    sub-float v2, v0, v2

    mul-float/2addr v1, v2

    float-to-int v1, v1

    int-to-long v2, v1

    sub-long v2, v4, v2

    iput-wide v2, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mStartTime:J

    .line 828
    iput p2, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mStart:I

    .line 829
    iget v1, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mDeceleration:F

    neg-float v1, v1

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mVelocity:I

    .line 830
    return-void
.end method

.method private static getDeceleration(I)F
    .locals 1

    .prologue
    .line 666
    if-lez p0, :cond_0

    const/high16 v0, -0x3b060000    # -2000.0f

    :goto_0
    return v0

    :cond_0
    const/high16 v0, 0x44fa0000    # 2000.0f

    goto :goto_0
.end method

.method private getSplineDeceleration(I)D
    .locals 3

    .prologue
    .line 791
    const v0, 0x3eb33333    # 0.35f

    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mFlingFriction:F

    sget v2, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->sPhysicalCoef:F

    mul-float/2addr v1, v2

    div-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->log(D)D

    move-result-wide v0

    return-wide v0
.end method

.method private getSplineFlingDistance(I)D
    .locals 8

    .prologue
    .line 795
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->getSplineDeceleration(I)D

    move-result-wide v0

    .line 796
    sget v2, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->DECELERATION_RATE:F

    float-to-double v2, v2

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    sub-double/2addr v2, v4

    .line 797
    iget v4, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mFlingFriction:F

    sget v5, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->sPhysicalCoef:F

    mul-float/2addr v4, v5

    float-to-double v4, v4

    sget v6, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->DECELERATION_RATE:F

    float-to-double v6, v6

    div-double v2, v6, v2

    mul-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->exp(D)D

    move-result-wide v0

    mul-double/2addr v0, v4

    return-wide v0
.end method

.method private getSplineFlingDuration(I)I
    .locals 6

    .prologue
    .line 802
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->getSplineDeceleration(I)D

    move-result-wide v0

    .line 803
    sget v2, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->DECELERATION_RATE:F

    float-to-double v2, v2

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    sub-double/2addr v2, v4

    .line 804
    const-wide v4, 0x408f400000000000L    # 1000.0

    div-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->exp(D)D

    move-result-wide v0

    mul-double/2addr v0, v4

    double-to-int v0, v0

    return v0
.end method

.method static initFromContext(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 643
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    const/high16 v1, 0x43200000    # 160.0f

    mul-float/2addr v0, v1

    .line 644
    const v1, 0x43c10b3d

    mul-float/2addr v0, v1

    const v1, 0x3f570a3d    # 0.84f

    mul-float/2addr v0, v1

    sput v0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->sPhysicalCoef:F

    .line 648
    return-void
.end method

.method private onEdgeReached()V
    .locals 4

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    .line 874
    iget v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mVelocity:I

    iget v1, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mVelocity:I

    mul-int/2addr v0, v1

    int-to-float v0, v0

    iget v1, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mDeceleration:F

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    mul-float/2addr v1, v3

    div-float/2addr v0, v1

    .line 875
    iget v1, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mVelocity:I

    int-to-float v1, v1

    invoke-static {v1}, Ljava/lang/Math;->signum(F)F

    move-result v1

    .line 877
    iget v2, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mOver:I

    int-to-float v2, v2

    cmpl-float v2, v0, v2

    if-lez v2, :cond_0

    .line 879
    neg-float v0, v1

    iget v1, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mVelocity:I

    int-to-float v1, v1

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mVelocity:I

    int-to-float v1, v1

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mOver:I

    int-to-float v1, v1

    mul-float/2addr v1, v3

    div-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mDeceleration:F

    .line 880
    iget v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mOver:I

    int-to-float v0, v0

    .line 883
    :cond_0
    float-to-int v1, v0

    iput v1, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mOver:I

    .line 884
    const/4 v1, 0x2

    iput v1, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mState:I

    .line 885
    iget v1, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mStart:I

    iget v2, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mVelocity:I

    if-lez v2, :cond_1

    :goto_0
    float-to-int v0, v0

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mFinal:I

    .line 886
    const/high16 v0, 0x447a0000    # 1000.0f

    iget v1, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mVelocity:I

    int-to-float v1, v1

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mDeceleration:F

    div-float/2addr v0, v1

    float-to-int v0, v0

    neg-int v0, v0

    iput v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mDuration:I

    .line 887
    return-void

    .line 885
    :cond_1
    neg-float v0, v0

    goto :goto_0
.end method

.method private startAfterEdge(IIII)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 839
    if-le p1, p2, :cond_0

    if-ge p1, p3, :cond_0

    .line 840
    const-string/jumbo v1, "OverScroller"

    const-string/jumbo v2, "startAfterEdge called from a valid position"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 841
    iput-boolean v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mFinished:Z

    .line 859
    :goto_0
    return-void

    .line 844
    :cond_0
    if-le p1, p3, :cond_1

    move v4, v0

    .line 845
    :goto_1
    if-eqz v4, :cond_2

    move v2, p3

    .line 846
    :goto_2
    sub-int v3, p1, v2

    .line 847
    mul-int v5, v3, p4

    if-ltz v5, :cond_3

    .line 848
    :goto_3
    if-eqz v0, :cond_4

    .line 850
    invoke-direct {p0, p1, v2, p4}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->startBounceAfterEdge(III)V

    goto :goto_0

    :cond_1
    move v4, v1

    .line 844
    goto :goto_1

    :cond_2
    move v2, p2

    .line 845
    goto :goto_2

    :cond_3
    move v0, v1

    .line 847
    goto :goto_3

    .line 852
    :cond_4
    invoke-direct {p0, p4}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->getSplineFlingDistance(I)D

    move-result-wide v0

    .line 853
    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v3

    int-to-double v6, v3

    cmpl-double v0, v0, v6

    if-lez v0, :cond_7

    .line 854
    if-eqz v4, :cond_5

    move v3, p2

    :goto_4
    if-eqz v4, :cond_6

    move v4, p1

    :goto_5
    iget v5, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mOver:I

    move-object v0, p0

    move v1, p1

    move v2, p4

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->fling(IIIII)V

    goto :goto_0

    :cond_5
    move v3, p1

    goto :goto_4

    :cond_6
    move v4, p3

    goto :goto_5

    .line 856
    :cond_7
    invoke-direct {p0, p1, v2, p4}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->startSpringback(III)V

    goto :goto_0
.end method

.method private startBounceAfterEdge(III)V
    .locals 1

    .prologue
    .line 833
    if-nez p3, :cond_0

    sub-int v0, p1, p2

    :goto_0
    invoke-static {v0}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->getDeceleration(I)F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mDeceleration:F

    .line 834
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->fitOnBounceCurve(III)V

    .line 835
    invoke-direct {p0}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->onEdgeReached()V

    .line 836
    return-void

    :cond_0
    move v0, p3

    .line 833
    goto :goto_0
.end method

.method private startSpringback(III)V
    .locals 6

    .prologue
    .line 742
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mFinished:Z

    .line 743
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mState:I

    .line 744
    iput p1, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mStart:I

    .line 745
    iput p2, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mFinal:I

    .line 746
    sub-int v0, p1, p2

    .line 747
    const/high16 v1, 0x40800000    # 4.0f

    invoke-static {v0}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->getDeceleration(I)F

    move-result v2

    mul-float/2addr v1, v2

    iput v1, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mDeceleration:F

    .line 749
    neg-int v1, v0

    iput v1, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mVelocity:I

    .line 750
    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mOver:I

    .line 751
    const-wide v2, 0x408f400000000000L    # 1000.0

    const-wide/high16 v4, -0x4000000000000000L    # -2.0

    int-to-double v0, v0

    mul-double/2addr v0, v4

    iget v4, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mDeceleration:F

    float-to-double v4, v4

    div-double/2addr v0, v4

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    mul-double/2addr v0, v2

    double-to-int v0, v0

    iput v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mDuration:I

    .line 752
    return-void
.end method


# virtual methods
.method computeInterpolatedPosition(F)I
    .locals 3

    .prologue
    .line 996
    iget v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mStart:I

    iget v1, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mFinal:I

    iget v2, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mStart:I

    sub-int/2addr v1, v2

    int-to-float v1, v1

    mul-float/2addr v1, p1

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method continueWhenFinished()Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 890
    iget v1, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mState:I

    packed-switch v1, :pswitch_data_0

    .line 914
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->update()Z

    .line 915
    const/4 v0, 0x1

    :cond_0
    :pswitch_0
    return v0

    .line 893
    :pswitch_1
    iget v1, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mDuration:I

    iget v2, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mSplineDuration:I

    if-ge v1, v2, :cond_0

    .line 895
    iget v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mFinal:I

    iput v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mStart:I

    .line 897
    iget v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mCurrVelocity:F

    float-to-int v0, v0

    iput v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mVelocity:I

    .line 898
    iget v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mVelocity:I

    invoke-static {v0}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->getDeceleration(I)F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mDeceleration:F

    .line 899
    iget-wide v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mStartTime:J

    iget v2, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mDuration:I

    int-to-long v2, v2

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mStartTime:J

    .line 900
    invoke-direct {p0}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->onEdgeReached()V

    goto :goto_0

    .line 907
    :pswitch_2
    iget-wide v2, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mStartTime:J

    iget v1, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mDuration:I

    int-to-long v4, v1

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mStartTime:J

    .line 908
    iget v1, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mFinal:I

    iget v2, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mStart:I

    invoke-direct {p0, v1, v2, v0}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->startSpringback(III)V

    goto :goto_0

    .line 890
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method finish()V
    .locals 1

    .prologue
    .line 703
    iget v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mFinal:I

    iput v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mCurrentPosition:I

    .line 707
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mFinished:Z

    .line 708
    return-void
.end method

.method fling(IIIII)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 755
    iput p5, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mOver:I

    .line 756
    iput-boolean v2, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mFinished:Z

    .line 757
    iput p2, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mVelocity:I

    int-to-float v0, p2

    iput v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mCurrVelocity:F

    .line 758
    iput v2, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mSplineDuration:I

    iput v2, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mDuration:I

    .line 759
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mStartTime:J

    .line 760
    iput p1, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mStart:I

    iput p1, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mCurrentPosition:I

    .line 762
    if-gt p1, p4, :cond_0

    if-ge p1, p3, :cond_2

    .line 763
    :cond_0
    invoke-direct {p0, p1, p3, p4, p2}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->startAfterEdge(IIII)V

    .line 788
    :cond_1
    :goto_0
    return-void

    .line 767
    :cond_2
    iput v2, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mState:I

    .line 768
    const-wide/16 v0, 0x0

    .line 770
    if-eqz p2, :cond_3

    .line 771
    invoke-direct {p0, p2}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->getSplineFlingDuration(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mSplineDuration:I

    iput v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mDuration:I

    .line 772
    invoke-direct {p0, p2}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->getSplineFlingDistance(I)D

    move-result-wide v0

    .line 775
    :cond_3
    int-to-float v2, p2

    invoke-static {v2}, Ljava/lang/Math;->signum(F)F

    move-result v2

    float-to-double v2, v2

    mul-double/2addr v0, v2

    double-to-int v0, v0

    iput v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mSplineDistance:I

    .line 776
    iget v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mSplineDistance:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mFinal:I

    .line 779
    iget v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mFinal:I

    if-ge v0, p3, :cond_4

    .line 780
    iget v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mStart:I

    iget v1, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mFinal:I

    invoke-direct {p0, v0, v1, p3}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->adjustDuration(III)V

    .line 781
    iput p3, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mFinal:I

    .line 784
    :cond_4
    iget v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mFinal:I

    if-le v0, p4, :cond_1

    .line 785
    iget v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mStart:I

    iget v1, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mFinal:I

    invoke-direct {p0, v0, v1, p4}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->adjustDuration(III)V

    .line 786
    iput p4, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mFinal:I

    goto :goto_0
.end method

.method setFinalPosition(I)V
    .locals 1

    .prologue
    .line 711
    iput p1, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mFinal:I

    .line 712
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mFinished:Z

    .line 713
    return-void
.end method

.method springback(III)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 723
    iput-boolean v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mFinished:Z

    .line 725
    iput p1, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mFinal:I

    iput p1, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mStart:I

    .line 726
    iput v1, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mVelocity:I

    .line 728
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mStartTime:J

    .line 729
    iput v1, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mDuration:I

    .line 731
    if-ge p1, p2, :cond_1

    .line 732
    invoke-direct {p0, p1, p2, v1}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->startSpringback(III)V

    .line 737
    :cond_0
    :goto_0
    iget-boolean v2, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mFinished:Z

    if-nez v2, :cond_2

    :goto_1
    return v0

    .line 733
    :cond_1
    if-le p1, p3, :cond_0

    .line 734
    invoke-direct {p0, p1, p3, v1}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->startSpringback(III)V

    goto :goto_0

    :cond_2
    move v0, v1

    .line 737
    goto :goto_1
.end method

.method startScroll(III)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 689
    iput-boolean v2, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mFinished:Z

    .line 691
    iput p1, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mStart:I

    .line 692
    add-int v0, p1, p2

    iput v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mFinal:I

    .line 694
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mStartTime:J

    .line 695
    iput p3, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mDuration:I

    .line 698
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mDeceleration:F

    .line 699
    iput v2, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mVelocity:I

    .line 700
    return-void
.end method

.method update()Z
    .locals 4

    .prologue
    .line 924
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v0

    .line 925
    iget-wide v2, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mStartTime:J

    sub-long/2addr v0, v2

    .line 926
    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->computeStateAtTime(J)Lcom/google/android/apps/chrome/third_party/OverScroller$ScrollState;

    move-result-object v0

    .line 927
    if-nez v0, :cond_0

    .line 928
    const/4 v0, 0x0

    .line 932
    :goto_0
    return v0

    .line 930
    :cond_0
    iget v1, v0, Lcom/google/android/apps/chrome/third_party/OverScroller$ScrollState;->mVelocity:F

    iput v1, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mCurrVelocity:F

    .line 931
    iget v0, v0, Lcom/google/android/apps/chrome/third_party/OverScroller$ScrollState;->mPosition:I

    iput v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mCurrentPosition:I

    .line 932
    const/4 v0, 0x1

    goto :goto_0
.end method

.method updateScroll(F)V
    .locals 1

    .prologue
    .line 659
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->computeInterpolatedPosition(F)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mCurrentPosition:I

    .line 660
    return-void
.end method
