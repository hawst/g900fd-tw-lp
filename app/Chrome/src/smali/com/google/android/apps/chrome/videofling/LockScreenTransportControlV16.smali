.class Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV16;
.super Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV14;
.source "LockScreenTransportControlV16.java"


# instance fields
.field private final mMediaRouter:Landroid/support/v7/media/MediaRouter;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV14;-><init>(Landroid/content/Context;)V

    .line 22
    invoke-static {p1}, Landroid/support/v7/media/MediaRouter;->a(Landroid/content/Context;)Landroid/support/v7/media/MediaRouter;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV16;->mMediaRouter:Landroid/support/v7/media/MediaRouter;

    .line 23
    return-void
.end method


# virtual methods
.method protected register()V
    .locals 1

    .prologue
    .line 27
    invoke-super {p0}, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV14;->register()V

    .line 28
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV16;->mMediaRouter:Landroid/support/v7/media/MediaRouter;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV16;->getRemoteControlClient()Landroid/media/RemoteControlClient;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v7/media/MediaRouter;->a(Ljava/lang/Object;)V

    .line 29
    return-void
.end method

.method protected unregister()V
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV16;->mMediaRouter:Landroid/support/v7/media/MediaRouter;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV16;->getRemoteControlClient()Landroid/media/RemoteControlClient;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v7/media/MediaRouter;->b(Ljava/lang/Object;)V

    .line 34
    invoke-super {p0}, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV14;->unregister()V

    .line 35
    return-void
.end method
