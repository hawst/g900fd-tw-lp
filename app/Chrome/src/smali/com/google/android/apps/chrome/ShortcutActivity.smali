.class public Lcom/google/android/apps/chrome/ShortcutActivity;
.super Lcom/google/android/apps/chrome/ChromeActivity;
.source "ShortcutActivity.java"

# interfaces
.implements Lcom/google/android/apps/chrome/ntp/BookmarksPage$BookmarkSelectedListener;


# instance fields
.field private mBookmarksPage:Lcom/google/android/apps/chrome/ntp/BookmarksPage;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/google/android/apps/chrome/ChromeActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public initializeState()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 39
    invoke-super {p0}, Lcom/google/android/apps/chrome/ChromeActivity;->initializeState()V

    .line 42
    new-instance v0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksShim;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksShim;-><init>()V

    .line 43
    invoke-virtual {v0, p0}, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksShim;->initialize(Landroid/content/Context;)V

    .line 44
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksShim;->kickOffReading()V

    .line 46
    invoke-static {}, Lorg/chromium/chrome/browser/profiles/Profile;->getLastUsedProfile()Lorg/chromium/chrome/browser/profiles/Profile;

    move-result-object v0

    .line 47
    invoke-static {p0, v0, p0}, Lcom/google/android/apps/chrome/ntp/BookmarksPage;->buildPageInSelectBookmarkMode(Landroid/content/Context;Lorg/chromium/chrome/browser/profiles/Profile;Lcom/google/android/apps/chrome/ntp/BookmarksPage$BookmarkSelectedListener;)Lcom/google/android/apps/chrome/ntp/BookmarksPage;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/ShortcutActivity;->mBookmarksPage:Lcom/google/android/apps/chrome/ntp/BookmarksPage;

    .line 48
    iget-object v0, p0, Lcom/google/android/apps/chrome/ShortcutActivity;->mBookmarksPage:Lcom/google/android/apps/chrome/ntp/BookmarksPage;

    const-string/jumbo v1, "chrome-native://bookmarks/"

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/ntp/BookmarksPage;->updateForUrl(Ljava/lang/String;)V

    .line 50
    iget-object v0, p0, Lcom/google/android/apps/chrome/ShortcutActivity;->mBookmarksPage:Lcom/google/android/apps/chrome/ntp/BookmarksPage;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ntp/BookmarksPage;->getView()Landroid/view/View;

    move-result-object v0

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/chrome/ShortcutActivity;->setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 53
    return-void
.end method

.method public onBookmarkSelected(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 7

    .prologue
    .line 72
    invoke-static {p3}, Lorg/chromium/chrome/browser/favicon/FaviconHelper;->getDominantColorForBitmap(Landroid/graphics/Bitmap;)I

    move-result v0

    .line 73
    invoke-static {v0}, Landroid/graphics/Color;->red(I)I

    move-result v4

    invoke-static {v0}, Landroid/graphics/Color;->green(I)I

    move-result v5

    invoke-static {v0}, Landroid/graphics/Color;->blue(I)I

    move-result v6

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-static/range {v0 .. v6}, Lorg/chromium/chrome/browser/BookmarkUtils;->createAddToHomeIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;III)Landroid/content/Intent;

    move-result-object v0

    .line 75
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/chrome/ShortcutActivity;->setResult(ILandroid/content/Intent;)V

    .line 76
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ShortcutActivity;->finish()V

    .line 77
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 57
    invoke-super {p0}, Lcom/google/android/apps/chrome/ChromeActivity;->onDestroy()V

    .line 58
    iget-object v0, p0, Lcom/google/android/apps/chrome/ShortcutActivity;->mBookmarksPage:Lcom/google/android/apps/chrome/ntp/BookmarksPage;

    if-eqz v0, :cond_0

    .line 59
    iget-object v0, p0, Lcom/google/android/apps/chrome/ShortcutActivity;->mBookmarksPage:Lcom/google/android/apps/chrome/ntp/BookmarksPage;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ntp/BookmarksPage;->destroy()V

    .line 60
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/ShortcutActivity;->mBookmarksPage:Lcom/google/android/apps/chrome/ntp/BookmarksPage;

    .line 62
    :cond_0
    return-void
.end method

.method public onNewTabOpened()V
    .locals 0

    .prologue
    .line 68
    return-void
.end method

.method public postInflationStartup()V
    .locals 2

    .prologue
    .line 34
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ShortcutActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$string;->bookmark_shortcut_choose_bookmark:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ShortcutActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 35
    return-void
.end method

.method protected setContentView()V
    .locals 0

    .prologue
    .line 30
    return-void
.end method
