.class Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager$3;
.super Ljava/lang/Object;
.source "ChromeFullscreenManager.java"

# interfaces
.implements Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler$FullscreenHtmlApiDelegate;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;)V
    .locals 0

    .prologue
    .line 234
    iput-object p1, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager$3;->this$0:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public cancelPendingEnterFullscreen()Z
    .locals 3

    .prologue
    .line 252
    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager$3;->this$0:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;

    # getter for: Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mIsEnteringPersistentModeState:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->access$700(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;)Z

    move-result v0

    .line 253
    iget-object v1, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager$3;->this$0:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;

    const/4 v2, 0x0

    # setter for: Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mIsEnteringPersistentModeState:Z
    invoke-static {v1, v2}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->access$702(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;Z)Z

    .line 254
    return v0
.end method

.method public getNotificationAnchorView()Landroid/view/View;
    .locals 1

    .prologue
    .line 237
    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager$3;->this$0:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;

    # getter for: Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mControlContainer:Lcom/google/android/apps/chrome/widget/ControlContainer;
    invoke-static {v0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->access$500(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;)Lcom/google/android/apps/chrome/widget/ControlContainer;

    move-result-object v0

    return-object v0
.end method

.method public getNotificationOffsetY()I
    .locals 1

    .prologue
    .line 242
    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager$3;->this$0:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->getControlOffset()F

    move-result v0

    float-to-int v0, v0

    return v0
.end method

.method public onEnterFullscreen()V
    .locals 2

    .prologue
    .line 247
    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager$3;->this$0:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mIsEnteringPersistentModeState:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->access$702(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;Z)Z

    .line 248
    return-void
.end method

.method public onFullscreenExited(Lorg/chromium/content/browser/ContentViewCore;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 259
    invoke-virtual {p1, v1, v0, v0}, Lorg/chromium/content/browser/ContentViewCore;->updateTopControlsState(ZZZ)V

    .line 260
    invoke-virtual {p1, v0, v0, v1}, Lorg/chromium/content/browser/ContentViewCore;->updateTopControlsState(ZZZ)V

    .line 261
    return-void
.end method

.method public shouldShowNotificationBubble()Z
    .locals 1

    .prologue
    .line 265
    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager$3;->this$0:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->isOverlayVideoMode()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
