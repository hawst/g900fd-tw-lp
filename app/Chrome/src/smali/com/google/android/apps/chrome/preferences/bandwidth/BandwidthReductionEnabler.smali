.class public Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthReductionEnabler;
.super Lcom/google/android/apps/chrome/preferences/ChromeBaseSwitchEnabler;
.source "BandwidthReductionEnabler.java"


# instance fields
.field private mBandwidthReductionPreferences:Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthReductionPreferences;

.field private final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/widget/Switch;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/preferences/ChromeBaseSwitchEnabler;-><init>(Landroid/content/Context;Landroid/widget/Switch;)V

    .line 26
    iput-object p1, p0, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthReductionEnabler;->mContext:Landroid/content/Context;

    .line 27
    return-void
.end method


# virtual methods
.method protected isSwitchEnabled()Z
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthReductionEnabler;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getPrefEnableWebAccelerationPreference()Z

    move-result v0

    return v0
.end method

.method public onValueChanged(Z)V
    .locals 2

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthReductionEnabler;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->setWebAccelerationEnabled(Ljava/lang/Boolean;)V

    .line 41
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthReductionEnabler;->mBandwidthReductionPreferences:Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthReductionPreferences;

    if-eqz v0, :cond_0

    .line 42
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthReductionEnabler;->mBandwidthReductionPreferences:Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthReductionPreferences;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthReductionPreferences;->updatePreferences(Z)V

    .line 44
    :cond_0
    return-void
.end method

.method public setBandwidthReductionPreferences(Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthReductionPreferences;)V
    .locals 0

    .prologue
    .line 35
    iput-object p1, p0, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthReductionEnabler;->mBandwidthReductionPreferences:Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthReductionPreferences;

    .line 36
    return-void
.end method
