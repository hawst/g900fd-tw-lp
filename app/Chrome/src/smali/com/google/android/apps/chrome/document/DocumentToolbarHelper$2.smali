.class Lcom/google/android/apps/chrome/document/DocumentToolbarHelper$2;
.super Ljava/lang/Object;
.source "DocumentToolbarHelper.java"

# interfaces
.implements Lcom/google/android/apps/chrome/ContextualMenuBar$ActionBarDelegate;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;)V
    .locals 0

    .prologue
    .line 93
    iput-object p1, p0, Lcom/google/android/apps/chrome/document/DocumentToolbarHelper$2;->this$0:Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getActionBar()Landroid/app/ActionBar;
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentToolbarHelper$2;->this$0:Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;

    iget-object v0, v0, Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;->mActivity:Lcom/google/android/apps/chrome/document/DocumentActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    return-object v0
.end method

.method public getControlTopMargin()I
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentToolbarHelper$2;->this$0:Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;

    # getter for: Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;->mCurrentControlContainer:Landroid/view/View;
    invoke-static {v0}, Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;->access$100(Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 106
    iget v0, v0, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    return v0
.end method

.method public setActionBarBackgroundVisibility(Z)V
    .locals 0

    .prologue
    .line 119
    return-void
.end method

.method public setControlTopMargin(I)V
    .locals 2

    .prologue
    .line 96
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentToolbarHelper$2;->this$0:Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;

    # getter for: Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;->mCurrentControlContainer:Landroid/view/View;
    invoke-static {v0}, Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;->access$100(Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 98
    iput p1, v0, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 99
    iget-object v1, p0, Lcom/google/android/apps/chrome/document/DocumentToolbarHelper$2;->this$0:Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;

    # getter for: Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;->mCurrentControlContainer:Landroid/view/View;
    invoke-static {v1}, Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;->access$100(Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 100
    return-void
.end method
