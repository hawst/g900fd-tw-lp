.class Lcom/google/android/apps/chrome/document/DocumentActivity$2;
.super Lcom/google/android/apps/chrome/document/DocumentTabList$InitializationObserver;
.source "DocumentActivity.java"


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/document/DocumentActivity;

.field final synthetic val$tabId:I


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/document/DocumentActivity;Lcom/google/android/apps/chrome/document/DocumentTabList;I)V
    .locals 0

    .prologue
    .line 229
    iput-object p1, p0, Lcom/google/android/apps/chrome/document/DocumentActivity$2;->this$0:Lcom/google/android/apps/chrome/document/DocumentActivity;

    iput p3, p0, Lcom/google/android/apps/chrome/document/DocumentActivity$2;->val$tabId:I

    invoke-direct {p0, p2}, Lcom/google/android/apps/chrome/document/DocumentTabList$InitializationObserver;-><init>(Lcom/google/android/apps/chrome/document/DocumentTabList;)V

    return-void
.end method


# virtual methods
.method public isCanceled()Z
    .locals 1

    .prologue
    .line 238
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity$2;->this$0:Lcom/google/android/apps/chrome/document/DocumentActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->isDestroyed()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity$2;->this$0:Lcom/google/android/apps/chrome/document/DocumentActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSatisfied()Z
    .locals 2

    .prologue
    .line 232
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity$2;->this$0:Lcom/google/android/apps/chrome/document/DocumentActivity;

    # getter for: Lcom/google/android/apps/chrome/document/DocumentActivity;->mTabList:Lcom/google/android/apps/chrome/document/DocumentTabList;
    invoke-static {v0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->access$100(Lcom/google/android/apps/chrome/document/DocumentActivity;)Lcom/google/android/apps/chrome/document/DocumentTabList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/document/DocumentTabList;->getCurrentState()I

    move-result v0

    const/4 v1, 0x4

    if-ge v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity$2;->this$0:Lcom/google/android/apps/chrome/document/DocumentActivity;

    # getter for: Lcom/google/android/apps/chrome/document/DocumentActivity;->mTabList:Lcom/google/android/apps/chrome/document/DocumentTabList;
    invoke-static {v0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->access$100(Lcom/google/android/apps/chrome/document/DocumentActivity;)Lcom/google/android/apps/chrome/document/DocumentTabList;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/chrome/document/DocumentActivity$2;->val$tabId:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/document/DocumentTabList;->isTabStateReady(I)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public runImmediately()V
    .locals 1

    .prologue
    .line 243
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity$2;->this$0:Lcom/google/android/apps/chrome/document/DocumentActivity;

    # invokes: Lcom/google/android/apps/chrome/document/DocumentActivity;->initializeUI()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->access$200(Lcom/google/android/apps/chrome/document/DocumentActivity;)V

    .line 244
    return-void
.end method
