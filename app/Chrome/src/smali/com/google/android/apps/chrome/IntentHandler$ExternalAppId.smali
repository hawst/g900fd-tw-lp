.class public final enum Lcom/google/android/apps/chrome/IntentHandler$ExternalAppId;
.super Ljava/lang/Enum;
.source "IntentHandler.java"


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/chrome/IntentHandler$ExternalAppId;

.field public static final enum CHROME:Lcom/google/android/apps/chrome/IntentHandler$ExternalAppId;

.field public static final enum FACEBOOK:Lcom/google/android/apps/chrome/IntentHandler$ExternalAppId;

.field public static final enum GMAIL:Lcom/google/android/apps/chrome/IntentHandler$ExternalAppId;

.field public static final enum INDEX_BOUNDARY:Lcom/google/android/apps/chrome/IntentHandler$ExternalAppId;

.field public static final enum OTHER:Lcom/google/android/apps/chrome/IntentHandler$ExternalAppId;

.field public static final enum PLUS:Lcom/google/android/apps/chrome/IntentHandler$ExternalAppId;

.field public static final enum TWITTER:Lcom/google/android/apps/chrome/IntentHandler$ExternalAppId;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 126
    new-instance v0, Lcom/google/android/apps/chrome/IntentHandler$ExternalAppId;

    const-string/jumbo v1, "OTHER"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/chrome/IntentHandler$ExternalAppId;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/IntentHandler$ExternalAppId;->OTHER:Lcom/google/android/apps/chrome/IntentHandler$ExternalAppId;

    .line 127
    new-instance v0, Lcom/google/android/apps/chrome/IntentHandler$ExternalAppId;

    const-string/jumbo v1, "GMAIL"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/chrome/IntentHandler$ExternalAppId;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/IntentHandler$ExternalAppId;->GMAIL:Lcom/google/android/apps/chrome/IntentHandler$ExternalAppId;

    .line 128
    new-instance v0, Lcom/google/android/apps/chrome/IntentHandler$ExternalAppId;

    const-string/jumbo v1, "FACEBOOK"

    invoke-direct {v0, v1, v5}, Lcom/google/android/apps/chrome/IntentHandler$ExternalAppId;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/IntentHandler$ExternalAppId;->FACEBOOK:Lcom/google/android/apps/chrome/IntentHandler$ExternalAppId;

    .line 129
    new-instance v0, Lcom/google/android/apps/chrome/IntentHandler$ExternalAppId;

    const-string/jumbo v1, "PLUS"

    invoke-direct {v0, v1, v6}, Lcom/google/android/apps/chrome/IntentHandler$ExternalAppId;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/IntentHandler$ExternalAppId;->PLUS:Lcom/google/android/apps/chrome/IntentHandler$ExternalAppId;

    .line 130
    new-instance v0, Lcom/google/android/apps/chrome/IntentHandler$ExternalAppId;

    const-string/jumbo v1, "TWITTER"

    invoke-direct {v0, v1, v7}, Lcom/google/android/apps/chrome/IntentHandler$ExternalAppId;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/IntentHandler$ExternalAppId;->TWITTER:Lcom/google/android/apps/chrome/IntentHandler$ExternalAppId;

    .line 131
    new-instance v0, Lcom/google/android/apps/chrome/IntentHandler$ExternalAppId;

    const-string/jumbo v1, "CHROME"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/chrome/IntentHandler$ExternalAppId;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/IntentHandler$ExternalAppId;->CHROME:Lcom/google/android/apps/chrome/IntentHandler$ExternalAppId;

    .line 132
    new-instance v0, Lcom/google/android/apps/chrome/IntentHandler$ExternalAppId;

    const-string/jumbo v1, "INDEX_BOUNDARY"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/chrome/IntentHandler$ExternalAppId;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/IntentHandler$ExternalAppId;->INDEX_BOUNDARY:Lcom/google/android/apps/chrome/IntentHandler$ExternalAppId;

    .line 125
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/google/android/apps/chrome/IntentHandler$ExternalAppId;

    sget-object v1, Lcom/google/android/apps/chrome/IntentHandler$ExternalAppId;->OTHER:Lcom/google/android/apps/chrome/IntentHandler$ExternalAppId;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/chrome/IntentHandler$ExternalAppId;->GMAIL:Lcom/google/android/apps/chrome/IntentHandler$ExternalAppId;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/chrome/IntentHandler$ExternalAppId;->FACEBOOK:Lcom/google/android/apps/chrome/IntentHandler$ExternalAppId;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/chrome/IntentHandler$ExternalAppId;->PLUS:Lcom/google/android/apps/chrome/IntentHandler$ExternalAppId;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/apps/chrome/IntentHandler$ExternalAppId;->TWITTER:Lcom/google/android/apps/chrome/IntentHandler$ExternalAppId;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/apps/chrome/IntentHandler$ExternalAppId;->CHROME:Lcom/google/android/apps/chrome/IntentHandler$ExternalAppId;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/apps/chrome/IntentHandler$ExternalAppId;->INDEX_BOUNDARY:Lcom/google/android/apps/chrome/IntentHandler$ExternalAppId;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/chrome/IntentHandler$ExternalAppId;->$VALUES:[Lcom/google/android/apps/chrome/IntentHandler$ExternalAppId;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 125
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/chrome/IntentHandler$ExternalAppId;
    .locals 1

    .prologue
    .line 125
    const-class v0, Lcom/google/android/apps/chrome/IntentHandler$ExternalAppId;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/IntentHandler$ExternalAppId;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/chrome/IntentHandler$ExternalAppId;
    .locals 1

    .prologue
    .line 125
    sget-object v0, Lcom/google/android/apps/chrome/IntentHandler$ExternalAppId;->$VALUES:[Lcom/google/android/apps/chrome/IntentHandler$ExternalAppId;

    invoke-virtual {v0}, [Lcom/google/android/apps/chrome/IntentHandler$ExternalAppId;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/chrome/IntentHandler$ExternalAppId;

    return-object v0
.end method
