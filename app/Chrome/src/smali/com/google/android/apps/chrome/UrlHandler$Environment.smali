.class public interface abstract Lcom/google/android/apps/chrome/UrlHandler$Environment;
.super Ljava/lang/Object;
.source "UrlHandler.java"


# virtual methods
.method public abstract canResolveActivity(Landroid/content/Intent;)Z
.end method

.method public abstract getPackageName()Ljava/lang/String;
.end method

.method public abstract isSpecializedHandlerAvailable(Landroid/content/Intent;)Z
.end method

.method public abstract queryIntentActivities(Landroid/content/Intent;)Ljava/util/List;
.end method

.method public abstract startActivity(Landroid/content/Intent;)V
.end method

.method public abstract startActivityIfNeeded(Landroid/content/Intent;)Z
.end method

.method public abstract startIncognitoIntent(Landroid/content/Intent;)V
.end method

.method public abstract willChromeHandleIntent(Landroid/content/Intent;)Z
.end method
