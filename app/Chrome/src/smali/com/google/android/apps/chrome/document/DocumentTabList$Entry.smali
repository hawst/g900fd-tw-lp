.class final Lcom/google/android/apps/chrome/document/DocumentTabList$Entry;
.super Ljava/lang/Object;
.source "DocumentTabList.java"


# instance fields
.field public canGoBack:Z

.field public currentUrl:Ljava/lang/String;

.field public frozenTab:Lcom/google/android/apps/chrome/tab/ChromeTab;

.field public initialUrl:Ljava/lang/String;

.field public isDirty:Z

.field public isTabStateReady:Z

.field public final tabId:I

.field public tabState:Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;


# direct methods
.method public constructor <init>(I)V
    .locals 0

    .prologue
    .line 148
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 149
    iput p1, p0, Lcom/google/android/apps/chrome/document/DocumentTabList$Entry;->tabId:I

    .line 150
    return-void
.end method

.method public constructor <init>(ILcom/google/android/apps/chrome/tab/ChromeTab$TabState;)V
    .locals 1

    .prologue
    .line 157
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 158
    iput p1, p0, Lcom/google/android/apps/chrome/document/DocumentTabList$Entry;->tabId:I

    .line 159
    iput-object p2, p0, Lcom/google/android/apps/chrome/document/DocumentTabList$Entry;->tabState:Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;

    .line 160
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/document/DocumentTabList$Entry;->isTabStateReady:Z

    .line 161
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 152
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 153
    iput p1, p0, Lcom/google/android/apps/chrome/document/DocumentTabList$Entry;->tabId:I

    .line 154
    iput-object p2, p0, Lcom/google/android/apps/chrome/document/DocumentTabList$Entry;->initialUrl:Ljava/lang/String;

    .line 155
    return-void
.end method
