.class Lcom/google/android/apps/chrome/omaha/UpdateInfoBarHelper$1;
.super Landroid/os/AsyncTask;
.source "UpdateInfoBarHelper.java"


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/omaha/UpdateInfoBarHelper;

.field final synthetic val$activity:Lcom/google/android/apps/chrome/ChromeActivity;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/omaha/UpdateInfoBarHelper;Lcom/google/android/apps/chrome/ChromeActivity;)V
    .locals 0

    .prologue
    .line 37
    iput-object p1, p0, Lcom/google/android/apps/chrome/omaha/UpdateInfoBarHelper$1;->this$0:Lcom/google/android/apps/chrome/omaha/UpdateInfoBarHelper;

    iput-object p2, p0, Lcom/google/android/apps/chrome/omaha/UpdateInfoBarHelper$1;->val$activity:Lcom/google/android/apps/chrome/ChromeActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 37
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/omaha/UpdateInfoBarHelper$1;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 3

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/apps/chrome/omaha/UpdateInfoBarHelper$1;->val$activity:Lcom/google/android/apps/chrome/ChromeActivity;

    invoke-static {v0}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->isNewerVersionAvailable(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 41
    iget-object v0, p0, Lcom/google/android/apps/chrome/omaha/UpdateInfoBarHelper$1;->this$0:Lcom/google/android/apps/chrome/omaha/UpdateInfoBarHelper;

    invoke-static {}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->getMarketURLGetter()Lcom/google/android/apps/chrome/omaha/MarketURLGetter;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/omaha/UpdateInfoBarHelper$1;->val$activity:Lcom/google/android/apps/chrome/ChromeActivity;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/chrome/omaha/MarketURLGetter;->getMarketURL(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/google/android/apps/chrome/omaha/UpdateInfoBarHelper;->mUpdateURL:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/omaha/UpdateInfoBarHelper;->access$002(Lcom/google/android/apps/chrome/omaha/UpdateInfoBarHelper;Ljava/lang/String;)Ljava/lang/String;

    .line 42
    iget-object v0, p0, Lcom/google/android/apps/chrome/omaha/UpdateInfoBarHelper$1;->this$0:Lcom/google/android/apps/chrome/omaha/UpdateInfoBarHelper;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/apps/chrome/omaha/UpdateInfoBarHelper;->mMustShowInfoBar:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/omaha/UpdateInfoBarHelper;->access$102(Lcom/google/android/apps/chrome/omaha/UpdateInfoBarHelper;Z)Z

    .line 46
    :goto_0
    const/4 v0, 0x0

    return-object v0

    .line 44
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/omaha/UpdateInfoBarHelper$1;->this$0:Lcom/google/android/apps/chrome/omaha/UpdateInfoBarHelper;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/apps/chrome/omaha/UpdateInfoBarHelper;->mMustShowInfoBar:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/omaha/UpdateInfoBarHelper;->access$102(Lcom/google/android/apps/chrome/omaha/UpdateInfoBarHelper;Z)Z

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 37
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/omaha/UpdateInfoBarHelper$1;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 2

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/apps/chrome/omaha/UpdateInfoBarHelper$1;->val$activity:Lcom/google/android/apps/chrome/ChromeActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeActivity;->isActivityDestroyed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 53
    :goto_0
    return-void

    .line 52
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/omaha/UpdateInfoBarHelper$1;->this$0:Lcom/google/android/apps/chrome/omaha/UpdateInfoBarHelper;

    iget-object v1, p0, Lcom/google/android/apps/chrome/omaha/UpdateInfoBarHelper$1;->val$activity:Lcom/google/android/apps/chrome/ChromeActivity;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/omaha/UpdateInfoBarHelper;->showUpdateInfobarIfNecessary(Lcom/google/android/apps/chrome/ChromeActivity;)V

    goto :goto_0
.end method
