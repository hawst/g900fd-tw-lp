.class public Lcom/google/android/apps/chrome/services/DefaultNotificationManagerWrapper;
.super Ljava/lang/Object;
.source "DefaultNotificationManagerWrapper.java"

# interfaces
.implements Lcom/google/android/apps/chrome/services/NotificationManagerWrapper;


# instance fields
.field private mNotificationManager:Landroid/app/NotificationManager;


# direct methods
.method public constructor <init>(Landroid/app/NotificationManager;)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/google/android/apps/chrome/services/DefaultNotificationManagerWrapper;->mNotificationManager:Landroid/app/NotificationManager;

    .line 20
    return-void
.end method


# virtual methods
.method public cancel(I)V
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/DefaultNotificationManagerWrapper;->mNotificationManager:Landroid/app/NotificationManager;

    invoke-virtual {v0, p1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 30
    return-void
.end method

.method public notify(ILandroid/app/Notification;)V
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/DefaultNotificationManagerWrapper;->mNotificationManager:Landroid/app/NotificationManager;

    invoke-virtual {v0, p1, p2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 25
    return-void
.end method
