.class public Lcom/google/android/apps/chrome/compositor/TabContentManager;
.super Ljava/lang/Object;
.source "TabContentManager.java"


# static fields
.field private static final NOTIFICATIONS:[I


# instance fields
.field private final mContentOffsetProvider:Lcom/google/android/apps/chrome/compositor/ContentOffsetProvider;

.field private final mContext:Landroid/content/Context;

.field private final mDecompressRequests:Landroid/util/SparseArray;

.field private final mFullResThumbnailsMaxSize:I

.field private final mListeners:Ljava/util/ArrayList;

.field private mNativeTabContentManager:J

.field private final mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

.field private mPriorityTabIds:[I

.field private final mThumbnailScale:F

.field private final mThumbnailsDir:Ljava/io/File;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 363
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/apps/chrome/compositor/TabContentManager;->NOTIFICATIONS:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x6
        0x8
        0x27
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/chrome/compositor/ContentOffsetProvider;)V
    .locals 10

    .prologue
    const/4 v6, 0x0

    const/high16 v9, 0x3fc00000    # 1.5f

    const/high16 v0, 0x3f800000    # 1.0f

    .line 94
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/chrome/compositor/TabContentManager;->mListeners:Ljava/util/ArrayList;

    .line 59
    new-instance v1, Landroid/util/SparseArray;

    invoke-direct {v1}, Landroid/util/SparseArray;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/chrome/compositor/TabContentManager;->mDecompressRequests:Landroid/util/SparseArray;

    .line 379
    new-instance v1, Lcom/google/android/apps/chrome/compositor/TabContentManager$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/compositor/TabContentManager$1;-><init>(Lcom/google/android/apps/chrome/compositor/TabContentManager;)V

    iput-object v1, p0, Lcom/google/android/apps/chrome/compositor/TabContentManager;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    .line 95
    iput-object p1, p0, Lcom/google/android/apps/chrome/compositor/TabContentManager;->mContext:Landroid/content/Context;

    .line 96
    iput-object p2, p0, Lcom/google/android/apps/chrome/compositor/TabContentManager;->mContentOffsetProvider:Lcom/google/android/apps/chrome/compositor/ContentOffsetProvider;

    .line 98
    iget-object v1, p0, Lcom/google/android/apps/chrome/compositor/TabContentManager;->mContext:Landroid/content/Context;

    const-string/jumbo v2, "textures"

    invoke-virtual {v1, v2, v6}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/chrome/compositor/TabContentManager;->mThumbnailsDir:Ljava/io/File;

    .line 99
    iget-object v1, p0, Lcom/google/android/apps/chrome/compositor/TabContentManager;->mThumbnailsDir:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    .line 102
    iget-object v2, p0, Lcom/google/android/apps/chrome/compositor/TabContentManager;->mContext:Landroid/content/Context;

    sget v3, Lcom/google/android/apps/chrome/R$integer;->default_thumbnail_cache_size:I

    const-string/jumbo v4, "thumbnails"

    invoke-static {v2, v3, v4}, Lcom/google/android/apps/chrome/compositor/TabContentManager;->getIntegerResourceWithOverride(Landroid/content/Context;ILjava/lang/String;)I

    move-result v2

    .line 105
    iput v2, p0, Lcom/google/android/apps/chrome/compositor/TabContentManager;->mFullResThumbnailsMaxSize:I

    .line 107
    iget-object v3, p0, Lcom/google/android/apps/chrome/compositor/TabContentManager;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/google/android/apps/chrome/R$integer;->default_compression_queue_size:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v4

    .line 109
    iget-object v3, p0, Lcom/google/android/apps/chrome/compositor/TabContentManager;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v5, Lcom/google/android/apps/chrome/R$integer;->default_write_queue_size:I

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v5

    .line 114
    iget-object v3, p0, Lcom/google/android/apps/chrome/compositor/TabContentManager;->mContext:Landroid/content/Context;

    sget v7, Lcom/google/android/apps/chrome/R$integer;->default_approximation_thumbnail_cache_size:I

    const-string/jumbo v8, "approximation-thumbnails"

    invoke-static {v3, v7, v8}, Lcom/google/android/apps/chrome/compositor/TabContentManager;->getIntegerResourceWithOverride(Landroid/content/Context;ILjava/lang/String;)I

    move-result v3

    .line 120
    iget-object v7, p0, Lcom/google/android/apps/chrome/compositor/TabContentManager;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v7

    iget v7, v7, Landroid/util/DisplayMetrics;->density:F

    .line 121
    iget-object v8, p0, Lcom/google/android/apps/chrome/compositor/TabContentManager;->mContext:Landroid/content/Context;

    invoke-static {v8}, Lorg/chromium/ui/base/DeviceFormFactor;->isTablet(Landroid/content/Context;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 123
    div-float/2addr v0, v7

    .line 133
    :goto_0
    iput v0, p0, Lcom/google/android/apps/chrome/compositor/TabContentManager;->mThumbnailScale:F

    .line 135
    iget v0, p0, Lcom/google/android/apps/chrome/compositor/TabContentManager;->mFullResThumbnailsMaxSize:I

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/android/apps/chrome/compositor/TabContentManager;->mPriorityTabIds:[I

    move-object v0, p0

    .line 137
    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/chrome/compositor/TabContentManager;->nativeInit(Ljava/lang/String;IIIIZ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/chrome/compositor/TabContentManager;->mNativeTabContentManager:J

    .line 140
    invoke-direct {p0}, Lcom/google/android/apps/chrome/compositor/TabContentManager;->registerNotifications()V

    .line 141
    return-void

    .line 128
    :cond_0
    cmpl-float v6, v7, v9

    if-lez v6, :cond_1

    .line 129
    div-float v0, v9, v7

    .line 131
    :cond_1
    const/4 v6, 0x1

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/compositor/TabContentManager;)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/google/android/apps/chrome/compositor/TabContentManager;->unregisterNotifications()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/compositor/TabContentManager;I)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/compositor/TabContentManager;->removeTabThumbnail(I)V

    return-void
.end method

.method private static getIntegerResourceWithOverride(Landroid/content/Context;ILjava/lang/String;)I
    .locals 2

    .prologue
    .line 81
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    .line 82
    invoke-static {}, Lorg/chromium/base/CommandLine;->getInstance()Lorg/chromium/base/CommandLine;

    move-result-object v1

    invoke-virtual {v1, p2}, Lorg/chromium/base/CommandLine;->getSwitchValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 83
    if-eqz v1, :cond_0

    .line 84
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 87
    :cond_0
    return v0
.end method

.method private getNativePtr()J
    .locals 2

    .prologue
    .line 155
    iget-wide v0, p0, Lcom/google/android/apps/chrome/compositor/TabContentManager;->mNativeTabContentManager:J

    return-wide v0
.end method

.method private native nativeCacheTab(JLjava/lang/Object;Ljava/lang/Object;F)V
.end method

.method private native nativeCacheTabWithBitmap(JLjava/lang/Object;Ljava/lang/Object;F)V
.end method

.method private static native nativeDestroy(J)V
.end method

.method private native nativeGetDecompressedThumbnail(JI)V
.end method

.method private native nativeHasFullCachedThumbnail(JI)Z
.end method

.method private native nativeInit(Ljava/lang/String;IIIIZ)J
.end method

.method private native nativeInvalidateIfChanged(JILjava/lang/String;)V
.end method

.method private native nativeRemoveTabThumbnail(JI)V
.end method

.method private native nativeRemoveTabThumbnailFromDiskAtAndAboveId(JI)V
.end method

.method private native nativeSetUIResourceProvider(JJ)V
.end method

.method private native nativeUpdateVisibleIds(J[I)V
.end method

.method private notifyDecompressBitmapFinished(ILandroid/graphics/Bitmap;)V
    .locals 2

    .prologue
    .line 263
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/TabContentManager;->mDecompressRequests:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/compositor/TabContentManager$DecompressThumbnailCallback;

    .line 264
    iget-object v1, p0, Lcom/google/android/apps/chrome/compositor/TabContentManager;->mDecompressRequests:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->remove(I)V

    .line 265
    if-nez v0, :cond_0

    .line 267
    :goto_0
    return-void

    .line 266
    :cond_0
    invoke-interface {v0, p2}, Lcom/google/android/apps/chrome/compositor/TabContentManager$DecompressThumbnailCallback;->onFinishGetBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method private readbackNativePage(Lorg/chromium/chrome/browser/Tab;F)Landroid/graphics/Bitmap;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 177
    invoke-virtual {p1}, Lorg/chromium/chrome/browser/Tab;->getNativePage()Lorg/chromium/chrome/browser/NativePage;

    move-result-object v1

    .line 179
    if-nez v1, :cond_0

    move-object v0, v2

    .line 216
    :goto_0
    return-object v0

    .line 183
    :cond_0
    invoke-interface {v1}, Lorg/chromium/chrome/browser/NativePage;->getView()Landroid/view/View;

    move-result-object v3

    .line 184
    if-eqz v3, :cond_1

    invoke-virtual {v3}, Landroid/view/View;->getWidth()I

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    move-object v0, v2

    .line 185
    goto :goto_0

    .line 188
    :cond_2
    instance-of v0, v1, Lcom/google/android/apps/chrome/compositor/InvalidationAwareThumbnailProvider;

    if-eqz v0, :cond_3

    move-object v0, v1

    .line 189
    check-cast v0, Lcom/google/android/apps/chrome/compositor/InvalidationAwareThumbnailProvider;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/compositor/InvalidationAwareThumbnailProvider;->shouldCaptureThumbnail()Z

    move-result v0

    if-nez v0, :cond_3

    move-object v0, v2

    .line 190
    goto :goto_0

    .line 194
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/TabContentManager;->mContentOffsetProvider:Lcom/google/android/apps/chrome/compositor/ContentOffsetProvider;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/compositor/ContentOffsetProvider;->getOverlayTranslateY()I

    move-result v4

    .line 197
    :try_start_0
    invoke-virtual {v3}, Landroid/view/View;->getWidth()I

    move-result v0

    int-to-float v0, v0

    iget v5, p0, Lcom/google/android/apps/chrome/compositor/TabContentManager;->mThumbnailScale:F

    mul-float/2addr v0, v5

    float-to-int v0, v0

    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v5

    sub-int/2addr v5, v4

    int-to-float v5, v5

    iget v6, p0, Lcom/google/android/apps/chrome/compositor/TabContentManager;->mThumbnailScale:F

    mul-float/2addr v5, v6

    float-to-int v5, v5

    sget-object v6, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v5, v6}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 207
    new-instance v2, Landroid/graphics/Canvas;

    invoke-direct {v2, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 208
    invoke-virtual {v2, p2, p2}, Landroid/graphics/Canvas;->scale(FF)V

    .line 209
    const/4 v5, 0x0

    neg-int v4, v4

    int-to-float v4, v4

    invoke-virtual {v2, v5, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 210
    instance-of v4, v1, Lcom/google/android/apps/chrome/compositor/InvalidationAwareThumbnailProvider;

    if-eqz v4, :cond_4

    .line 211
    check-cast v1, Lcom/google/android/apps/chrome/compositor/InvalidationAwareThumbnailProvider;

    invoke-interface {v1, v2}, Lcom/google/android/apps/chrome/compositor/InvalidationAwareThumbnailProvider;->captureThumbnail(Landroid/graphics/Canvas;)V

    goto :goto_0

    .line 202
    :catch_0
    move-exception v0

    const/16 v0, 0x31

    invoke-static {v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastImmediateNotification(I)V

    move-object v0, v2

    .line 204
    goto :goto_0

    .line 213
    :cond_4
    invoke-virtual {v3, v2}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    goto :goto_0
.end method

.method private registerNotifications()V
    .locals 3

    .prologue
    .line 370
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/TabContentManager;->mContext:Landroid/content/Context;

    sget-object v1, Lcom/google/android/apps/chrome/compositor/TabContentManager;->NOTIFICATIONS:[I

    iget-object v2, p0, Lcom/google/android/apps/chrome/compositor/TabContentManager;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->registerForNotifications(Landroid/content/Context;[ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    .line 372
    return-void
.end method

.method private removeTabThumbnail(I)V
    .locals 4

    .prologue
    .line 309
    iget-wide v0, p0, Lcom/google/android/apps/chrome/compositor/TabContentManager;->mNativeTabContentManager:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 310
    iget-wide v0, p0, Lcom/google/android/apps/chrome/compositor/TabContentManager;->mNativeTabContentManager:J

    invoke-direct {p0, v0, v1, p1}, Lcom/google/android/apps/chrome/compositor/TabContentManager;->nativeRemoveTabThumbnail(JI)V

    .line 312
    :cond_0
    return-void
.end method

.method private unregisterNotifications()V
    .locals 3

    .prologue
    .line 375
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/TabContentManager;->mContext:Landroid/content/Context;

    sget-object v1, Lcom/google/android/apps/chrome/compositor/TabContentManager;->NOTIFICATIONS:[I

    iget-object v2, p0, Lcom/google/android/apps/chrome/compositor/TabContentManager;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->unregisterForNotifications(Landroid/content/Context;[ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    .line 377
    return-void
.end method


# virtual methods
.method public addThumbnailChangeListener(Lcom/google/android/apps/chrome/compositor/TabContentManager$ThumbnailChangeListener;)V
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/TabContentManager;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 164
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/TabContentManager;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 166
    :cond_0
    return-void
.end method

.method public cacheTabThumbnail(Lorg/chromium/chrome/browser/Tab;)V
    .locals 7

    .prologue
    .line 234
    iget-wide v0, p0, Lcom/google/android/apps/chrome/compositor/TabContentManager;->mNativeTabContentManager:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 235
    invoke-virtual {p1}, Lorg/chromium/chrome/browser/Tab;->getNativePage()Lorg/chromium/chrome/browser/NativePage;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 236
    iget v0, p0, Lcom/google/android/apps/chrome/compositor/TabContentManager;->mThumbnailScale:F

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/chrome/compositor/TabContentManager;->readbackNativePage(Lorg/chromium/chrome/browser/Tab;F)Landroid/graphics/Bitmap;

    move-result-object v5

    .line 237
    if-nez v5, :cond_1

    .line 246
    :cond_0
    :goto_0
    return-void

    .line 238
    :cond_1
    iget-wide v2, p0, Lcom/google/android/apps/chrome/compositor/TabContentManager;->mNativeTabContentManager:J

    iget v6, p0, Lcom/google/android/apps/chrome/compositor/TabContentManager;->mThumbnailScale:F

    move-object v1, p0

    move-object v4, p1

    invoke-direct/range {v1 .. v6}, Lcom/google/android/apps/chrome/compositor/TabContentManager;->nativeCacheTabWithBitmap(JLjava/lang/Object;Ljava/lang/Object;F)V

    .line 240
    invoke-virtual {v5}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_0

    .line 242
    :cond_2
    iget-wide v2, p0, Lcom/google/android/apps/chrome/compositor/TabContentManager;->mNativeTabContentManager:J

    invoke-virtual {p1}, Lorg/chromium/chrome/browser/Tab;->getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v5

    iget v6, p0, Lcom/google/android/apps/chrome/compositor/TabContentManager;->mThumbnailScale:F

    move-object v1, p0

    move-object v4, p1

    invoke-direct/range {v1 .. v6}, Lcom/google/android/apps/chrome/compositor/TabContentManager;->nativeCacheTab(JLjava/lang/Object;Ljava/lang/Object;F)V

    goto :goto_0
.end method

.method public cleanupPersistentData(Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 330
    iget-object v1, p0, Lcom/google/android/apps/chrome/compositor/TabContentManager;->mThumbnailsDir:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    .line 331
    if-eqz v1, :cond_0

    iget-wide v2, p0, Lcom/google/android/apps/chrome/compositor/TabContentManager;->mNativeTabContentManager:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-nez v2, :cond_1

    .line 344
    :cond_0
    return-void

    .line 333
    :cond_1
    array-length v2, v1

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 335
    :try_start_0
    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 336
    const/4 v4, 0x0

    invoke-interface {p1, v4}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->getModel(Z)Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v4

    invoke-static {v4, v3}, Lorg/chromium/chrome/browser/tabmodel/TabModelUtils;->getTabById(Lorg/chromium/chrome/browser/tabmodel/TabList;I)Lorg/chromium/chrome/browser/Tab;

    move-result-object v4

    if-nez v4, :cond_2

    const/4 v4, 0x1

    invoke-interface {p1, v4}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->getModel(Z)Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v4

    invoke-static {v4, v3}, Lorg/chromium/chrome/browser/tabmodel/TabModelUtils;->getTabById(Lorg/chromium/chrome/browser/tabmodel/TabList;I)Lorg/chromium/chrome/browser/Tab;

    move-result-object v4

    if-nez v4, :cond_2

    .line 338
    iget-wide v4, p0, Lcom/google/android/apps/chrome/compositor/TabContentManager;->mNativeTabContentManager:J

    invoke-direct {p0, v4, v5, v3}, Lcom/google/android/apps/chrome/compositor/TabContentManager;->nativeRemoveTabThumbnail(JI)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 333
    :cond_2
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :catch_0
    move-exception v3

    goto :goto_1
.end method

.method public cleanupPersistentDataAtAndAboveId(I)V
    .locals 4

    .prologue
    .line 320
    iget-wide v0, p0, Lcom/google/android/apps/chrome/compositor/TabContentManager;->mNativeTabContentManager:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 321
    iget-wide v0, p0, Lcom/google/android/apps/chrome/compositor/TabContentManager;->mNativeTabContentManager:J

    invoke-direct {p0, v0, v1, p1}, Lcom/google/android/apps/chrome/compositor/TabContentManager;->nativeRemoveTabThumbnailFromDiskAtAndAboveId(JI)V

    .line 323
    :cond_0
    return-void
.end method

.method public closeTab(I)V
    .locals 0

    .prologue
    .line 305
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/compositor/TabContentManager;->removeTabThumbnail(I)V

    .line 306
    return-void
.end method

.method public destroy()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 147
    iget-wide v0, p0, Lcom/google/android/apps/chrome/compositor/TabContentManager;->mNativeTabContentManager:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 148
    iget-wide v0, p0, Lcom/google/android/apps/chrome/compositor/TabContentManager;->mNativeTabContentManager:J

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/compositor/TabContentManager;->nativeDestroy(J)V

    .line 149
    iput-wide v2, p0, Lcom/google/android/apps/chrome/compositor/TabContentManager;->mNativeTabContentManager:J

    .line 151
    :cond_0
    return-void
.end method

.method public getThumbnailForId(ILcom/google/android/apps/chrome/compositor/TabContentManager$DecompressThumbnailCallback;)V
    .locals 4

    .prologue
    .line 254
    iget-wide v0, p0, Lcom/google/android/apps/chrome/compositor/TabContentManager;->mNativeTabContentManager:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 259
    :cond_0
    :goto_0
    return-void

    .line 255
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/TabContentManager;->mDecompressRequests:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 257
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/TabContentManager;->mDecompressRequests:Landroid/util/SparseArray;

    invoke-virtual {v0, p1, p2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 258
    iget-wide v0, p0, Lcom/google/android/apps/chrome/compositor/TabContentManager;->mNativeTabContentManager:J

    invoke-direct {p0, v0, v1, p1}, Lcom/google/android/apps/chrome/compositor/TabContentManager;->nativeGetDecompressedThumbnail(JI)V

    goto :goto_0
.end method

.method public hasFullCachedThumbnail(I)Z
    .locals 4

    .prologue
    .line 225
    iget-wide v0, p0, Lcom/google/android/apps/chrome/compositor/TabContentManager;->mNativeTabContentManager:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 226
    :goto_0
    return v0

    :cond_0
    iget-wide v0, p0, Lcom/google/android/apps/chrome/compositor/TabContentManager;->mNativeTabContentManager:J

    invoke-direct {p0, v0, v1, p1}, Lcom/google/android/apps/chrome/compositor/TabContentManager;->nativeHasFullCachedThumbnail(JI)Z

    move-result v0

    goto :goto_0
.end method

.method public invalidateIfChanged(ILjava/lang/String;)V
    .locals 4

    .prologue
    .line 275
    iget-wide v0, p0, Lcom/google/android/apps/chrome/compositor/TabContentManager;->mNativeTabContentManager:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 276
    iget-wide v0, p0, Lcom/google/android/apps/chrome/compositor/TabContentManager;->mNativeTabContentManager:J

    invoke-direct {p0, v0, v1, p1, p2}, Lcom/google/android/apps/chrome/compositor/TabContentManager;->nativeInvalidateIfChanged(JILjava/lang/String;)V

    .line 278
    :cond_0
    return-void
.end method

.method protected notifyListenersOfThumbnailChange(I)V
    .locals 2

    .prologue
    .line 358
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/TabContentManager;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/compositor/TabContentManager$ThumbnailChangeListener;

    .line 359
    invoke-interface {v0, p1}, Lcom/google/android/apps/chrome/compositor/TabContentManager$ThumbnailChangeListener;->onThumbnailChange(I)V

    goto :goto_0

    .line 361
    :cond_0
    return-void
.end method

.method public removeThumbnailChangeListener(Lcom/google/android/apps/chrome/compositor/TabContentManager$ThumbnailChangeListener;)V
    .locals 1

    .prologue
    .line 173
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/TabContentManager;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 174
    return-void
.end method

.method public updateVisibleIds(Ljava/util/List;)V
    .locals 4

    .prologue
    .line 285
    iget-wide v0, p0, Lcom/google/android/apps/chrome/compositor/TabContentManager;->mNativeTabContentManager:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_2

    .line 286
    iget v0, p0, Lcom/google/android/apps/chrome/compositor/TabContentManager;->mFullResThumbnailsMaxSize:I

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 288
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/TabContentManager;->mPriorityTabIds:[I

    array-length v0, v0

    if-eq v2, v0, :cond_0

    .line 289
    new-array v0, v2, [I

    iput-object v0, p0, Lcom/google/android/apps/chrome/compositor/TabContentManager;->mPriorityTabIds:[I

    .line 292
    :cond_0
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 293
    iget-object v3, p0, Lcom/google/android/apps/chrome/compositor/TabContentManager;->mPriorityTabIds:[I

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    aput v0, v3, v1

    .line 292
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 295
    :cond_1
    iget-wide v0, p0, Lcom/google/android/apps/chrome/compositor/TabContentManager;->mNativeTabContentManager:J

    iget-object v2, p0, Lcom/google/android/apps/chrome/compositor/TabContentManager;->mPriorityTabIds:[I

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/apps/chrome/compositor/TabContentManager;->nativeUpdateVisibleIds(J[I)V

    .line 297
    :cond_2
    return-void
.end method
