.class public Lcom/google/android/apps/chrome/firstrun/ProfileDataCache;
.super Ljava/lang/Object;
.source "ProfileDataCache.java"

# interfaces
.implements Lorg/chromium/chrome/browser/profiles/ProfileDownloader$Observer;


# instance fields
.field private final mActivity:Landroid/app/Activity;

.field private final mFullNames:Ljava/util/HashMap;

.field private final mImageSizePx:I

.field private final mImageStrokeColor:I

.field private final mImageStrokePx:I

.field private mObserver:Lorg/chromium/chrome/browser/profiles/ProfileDownloader$Observer;

.field private final mPictures:Ljava/util/HashMap;

.field private final mPlaceholderImage:Landroid/graphics/Bitmap;

.field private mProfile:Lorg/chromium/chrome/browser/profiles/Profile;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lorg/chromium/chrome/browser/profiles/Profile;)V
    .locals 6

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/firstrun/ProfileDataCache;->mPictures:Ljava/util/HashMap;

    .line 38
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/firstrun/ProfileDataCache;->mFullNames:Ljava/util/HashMap;

    .line 50
    invoke-static {p0}, Lorg/chromium/chrome/browser/profiles/ProfileDownloader;->addObserver(Lorg/chromium/chrome/browser/profiles/ProfileDownloader$Observer;)V

    .line 52
    iput-object p1, p0, Lcom/google/android/apps/chrome/firstrun/ProfileDataCache;->mActivity:Landroid/app/Activity;

    .line 53
    iput-object p2, p0, Lcom/google/android/apps/chrome/firstrun/ProfileDataCache;->mProfile:Lorg/chromium/chrome/browser/profiles/Profile;

    .line 55
    invoke-static {p1}, Lorg/chromium/ui/gfx/DeviceDisplayInfo;->create(Landroid/content/Context;)Lorg/chromium/ui/gfx/DeviceDisplayInfo;

    move-result-object v0

    .line 56
    const-wide/high16 v2, 0x4061000000000000L    # 136.0

    invoke-virtual {v0}, Lorg/chromium/ui/gfx/DeviceDisplayInfo;->getDIPScale()D

    move-result-wide v4

    mul-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v1, v2

    iput v1, p0, Lcom/google/android/apps/chrome/firstrun/ProfileDataCache;->mImageSizePx:I

    .line 57
    const-wide/high16 v2, 0x4008000000000000L    # 3.0

    invoke-virtual {v0}, Lorg/chromium/ui/gfx/DeviceDisplayInfo;->getDIPScale()D

    move-result-wide v0

    mul-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    iput v0, p0, Lcom/google/android/apps/chrome/firstrun/ProfileDataCache;->mImageStrokePx:I

    .line 58
    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$color;->fre_background_color:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/firstrun/ProfileDataCache;->mImageStrokeColor:I

    .line 60
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/ProfileDataCache;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$drawable;->fre_placeholder:I

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 62
    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/firstrun/ProfileDataCache;->getCroppedBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/firstrun/ProfileDataCache;->mPlaceholderImage:Landroid/graphics/Bitmap;

    .line 64
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/ProfileDataCache;->update()V

    .line 65
    return-void
.end method

.method private getCroppedBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 8

    .prologue
    const/high16 v7, 0x40000000    # 2.0f

    const/4 v6, 0x0

    .line 126
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 128
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 130
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    .line 131
    new-instance v3, Landroid/graphics/Rect;

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    invoke-direct {v3, v6, v6, v4, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 133
    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 134
    invoke-virtual {v1, v6, v6, v6, v6}, Landroid/graphics/Canvas;->drawARGB(IIII)V

    .line 135
    const/4 v4, -0x1

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 137
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    iget v5, p0, Lcom/google/android/apps/chrome/firstrun/ProfileDataCache;->mImageStrokePx:I

    sub-int/2addr v4, v5

    int-to-float v4, v4

    div-float/2addr v4, v7

    .line 138
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    int-to-float v5, v5

    div-float/2addr v5, v7

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    int-to-float v6, v6

    div-float/2addr v6, v7

    invoke-virtual {v1, v5, v6, v4, v2}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 139
    new-instance v5, Landroid/graphics/PorterDuffXfermode;

    sget-object v6, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v5, v6}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v2, v5}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 140
    invoke-virtual {v1, p1, v3, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 142
    iget v3, p0, Lcom/google/android/apps/chrome/firstrun/ProfileDataCache;->mImageStrokeColor:I

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 143
    sget-object v3, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 144
    new-instance v3, Landroid/graphics/PorterDuffXfermode;

    sget-object v5, Landroid/graphics/PorterDuff$Mode;->SRC:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v3, v5}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 145
    iget v3, p0, Lcom/google/android/apps/chrome/firstrun/ProfileDataCache;->mImageStrokePx:I

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 146
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v3, v7

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    int-to-float v5, v5

    div-float/2addr v5, v7

    invoke-virtual {v1, v3, v5, v4, v2}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 148
    return-object v0
.end method


# virtual methods
.method public getFullName(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/ProfileDataCache;->mFullNames:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getImage(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/ProfileDataCache;->mPictures:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 99
    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/ProfileDataCache;->mPlaceholderImage:Landroid/graphics/Bitmap;

    .line 100
    :cond_0
    return-object v0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 113
    invoke-static {p0}, Lorg/chromium/chrome/browser/profiles/ProfileDownloader;->removeObserver(Lorg/chromium/chrome/browser/profiles/ProfileDownloader$Observer;)V

    .line 114
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/firstrun/ProfileDataCache;->mObserver:Lorg/chromium/chrome/browser/profiles/ProfileDownloader$Observer;

    .line 115
    return-void
.end method

.method public onProfileDownloaded(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 2

    .prologue
    .line 119
    invoke-direct {p0, p3}, Lcom/google/android/apps/chrome/firstrun/ProfileDataCache;->getCroppedBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 120
    iget-object v1, p0, Lcom/google/android/apps/chrome/firstrun/ProfileDataCache;->mPictures:Ljava/util/HashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 121
    iget-object v1, p0, Lcom/google/android/apps/chrome/firstrun/ProfileDataCache;->mFullNames:Ljava/util/HashMap;

    invoke-virtual {v1, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 122
    iget-object v1, p0, Lcom/google/android/apps/chrome/firstrun/ProfileDataCache;->mObserver:Lorg/chromium/chrome/browser/profiles/ProfileDownloader$Observer;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/firstrun/ProfileDataCache;->mObserver:Lorg/chromium/chrome/browser/profiles/ProfileDownloader$Observer;

    invoke-interface {v1, p1, p2, v0}, Lorg/chromium/chrome/browser/profiles/ProfileDownloader$Observer;->onProfileDownloaded(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    .line 123
    :cond_0
    return-void
.end method

.method public setObserver(Lorg/chromium/chrome/browser/profiles/ProfileDownloader$Observer;)V
    .locals 0

    .prologue
    .line 155
    iput-object p1, p0, Lcom/google/android/apps/chrome/firstrun/ProfileDataCache;->mObserver:Lorg/chromium/chrome/browser/profiles/ProfileDownloader$Observer;

    .line 156
    return-void
.end method

.method public setProfile(Lorg/chromium/chrome/browser/profiles/Profile;)V
    .locals 0

    .prologue
    .line 72
    iput-object p1, p0, Lcom/google/android/apps/chrome/firstrun/ProfileDataCache;->mProfile:Lorg/chromium/chrome/browser/profiles/Profile;

    .line 73
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/ProfileDataCache;->update()V

    .line 74
    return-void
.end method

.method public update()V
    .locals 5

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/ProfileDataCache;->mProfile:Lorg/chromium/chrome/browser/profiles/Profile;

    if-nez v0, :cond_1

    .line 90
    :cond_0
    return-void

    .line 83
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/ProfileDataCache;->mActivity:Landroid/app/Activity;

    invoke-static {v0}, Lorg/chromium/sync/signin/AccountManagerHelper;->get(Landroid/content/Context;)Lorg/chromium/sync/signin/AccountManagerHelper;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/sync/signin/AccountManagerHelper;->getGoogleAccounts()[Landroid/accounts/Account;

    move-result-object v1

    .line 84
    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_0

    .line 85
    iget-object v2, p0, Lcom/google/android/apps/chrome/firstrun/ProfileDataCache;->mPictures:Ljava/util/HashMap;

    aget-object v3, v1, v0

    iget-object v3, v3, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_2

    .line 86
    iget-object v2, p0, Lcom/google/android/apps/chrome/firstrun/ProfileDataCache;->mProfile:Lorg/chromium/chrome/browser/profiles/Profile;

    aget-object v3, v1, v0

    iget-object v3, v3, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget v4, p0, Lcom/google/android/apps/chrome/firstrun/ProfileDataCache;->mImageSizePx:I

    invoke-static {v2, v3, v4}, Lorg/chromium/chrome/browser/profiles/ProfileDownloader;->startFetchingAccountInfoFor(Lorg/chromium/chrome/browser/profiles/Profile;Ljava/lang/String;I)V

    .line 84
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
