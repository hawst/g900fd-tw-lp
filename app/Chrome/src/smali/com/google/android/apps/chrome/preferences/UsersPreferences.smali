.class public Lcom/google/android/apps/chrome/preferences/UsersPreferences;
.super Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;
.source "UsersPreferences.java"


# static fields
.field public static final PREF_CATEGORY_SYNC_ACCOUNT:Ljava/lang/String; = "sync_account"


# instance fields
.field private mSignInHeader:Landroid/preference/Preference;

.field private mSyncAccountPreferenceScreen:Landroid/preference/PreferenceScreen;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;-><init>()V

    return-void
.end method

.method private displayAccountPicker()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 58
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/UsersPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/chrome/browser/signin/SigninManager;->get(Landroid/content/Context;)Lorg/chromium/chrome/browser/signin/SigninManager;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/signin/SigninManager;->isSignInAllowed()Z

    move-result v0

    if-nez v0, :cond_1

    .line 59
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/UsersPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/chrome/browser/signin/SigninManager;->get(Landroid/content/Context;)Lorg/chromium/chrome/browser/signin/SigninManager;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/signin/SigninManager;->isSigninDisabledByPolicy()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 60
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/UsersPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/preferences/ManagedPreferencesUtils;->showManagedByAdministratorToast(Landroid/content/Context;)V

    .line 73
    :cond_0
    :goto_0
    return-void

    .line 65
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/UsersPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/sync/signin/AccountManagerHelper;->get(Landroid/content/Context;)Lorg/chromium/sync/signin/AccountManagerHelper;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/sync/signin/AccountManagerHelper;->hasGoogleAccounts()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 66
    new-instance v0, Lcom/google/android/apps/chrome/sync/ui/SyncChooseAccountFragment;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/sync/ui/SyncChooseAccountFragment;-><init>()V

    .line 67
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/UsersPreferences;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/sync/ui/SyncChooseAccountFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    .line 69
    :cond_2
    new-instance v0, Lcom/google/android/apps/chrome/services/ui/AddGoogleAccountDialogFragment;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/services/ui/AddGoogleAccountDialogFragment;-><init>()V

    .line 70
    invoke-static {}, Lcom/google/android/apps/chrome/services/SyncSignInAccountListener;->get()Lcom/google/android/apps/chrome/services/SyncSignInAccountListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/services/ui/AddGoogleAccountDialogFragment;->setListener(Lcom/google/android/apps/chrome/services/ui/AddGoogleAccountDialogFragment$Listener;)V

    .line 71
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/UsersPreferences;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/services/ui/AddGoogleAccountDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private updatePreferences()V
    .locals 1

    .prologue
    .line 39
    sget v0, Lcom/google/android/apps/chrome/R$xml;->users_preferences:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/UsersPreferences;->addPreferencesFromResource(I)V

    .line 40
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 34
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    .line 35
    sget v0, Lcom/google/android/apps/chrome/R$xml;->users_preferences:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/UsersPreferences;->addPreferencesFromResource(I)V

    .line 36
    return-void
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/UsersPreferences;->mSignInHeader:Landroid/preference/Preference;

    if-ne p2, v0, :cond_0

    .line 51
    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/UsersPreferences;->displayAccountPicker()V

    .line 52
    const/4 v0, 0x1

    .line 54
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;->onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z

    move-result v0

    goto :goto_0
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 44
    invoke-super {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;->onResume()V

    .line 45
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/UsersPreferences;->updateAccountHeader()V

    .line 46
    return-void
.end method

.method public updateAccountHeader()V
    .locals 4

    .prologue
    .line 79
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/UsersPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/sync/signin/ChromeSigninController;->get(Landroid/content/Context;)Lorg/chromium/sync/signin/ChromeSigninController;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/sync/signin/ChromeSigninController;->getSignedInUser()Landroid/accounts/Account;

    move-result-object v0

    .line 80
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/UsersPreferences;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    .line 81
    if-eqz v0, :cond_0

    .line 82
    invoke-virtual {v1}, Landroid/preference/PreferenceScreen;->removeAll()V

    .line 83
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/UsersPreferences;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/UsersPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/preference/PreferenceManager;->createPreferenceScreen(Landroid/content/Context;)Landroid/preference/PreferenceScreen;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/chrome/preferences/UsersPreferences;->mSyncAccountPreferenceScreen:Landroid/preference/PreferenceScreen;

    .line 85
    iget-object v2, p0, Lcom/google/android/apps/chrome/preferences/UsersPreferences;->mSyncAccountPreferenceScreen:Landroid/preference/PreferenceScreen;

    const-string/jumbo v3, "sync_account"

    invoke-virtual {v2, v3}, Landroid/preference/PreferenceScreen;->setKey(Ljava/lang/String;)V

    .line 86
    iget-object v2, p0, Lcom/google/android/apps/chrome/preferences/UsersPreferences;->mSyncAccountPreferenceScreen:Landroid/preference/PreferenceScreen;

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v2, v0}, Landroid/preference/PreferenceScreen;->setTitle(Ljava/lang/CharSequence;)V

    .line 87
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/UsersPreferences;->mSyncAccountPreferenceScreen:Landroid/preference/PreferenceScreen;

    const-class v2, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/preference/PreferenceScreen;->setFragment(Ljava/lang/String;)V

    .line 88
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/UsersPreferences;->mSyncAccountPreferenceScreen:Landroid/preference/PreferenceScreen;

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 89
    sget v0, Lcom/google/android/apps/chrome/R$xml;->users_preferences:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/UsersPreferences;->addPreferencesFromResource(I)V

    .line 99
    :goto_0
    return-void

    .line 91
    :cond_0
    invoke-virtual {v1}, Landroid/preference/PreferenceScreen;->removeAll()V

    .line 92
    new-instance v0, Landroid/preference/Preference;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/UsersPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/preferences/UsersPreferences;->mSignInHeader:Landroid/preference/Preference;

    .line 93
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/UsersPreferences;->mSignInHeader:Landroid/preference/Preference;

    const-class v2, Lcom/google/android/apps/chrome/sync/ui/SyncChooseAccountFragment;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/preference/Preference;->setFragment(Ljava/lang/String;)V

    .line 94
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/UsersPreferences;->mSignInHeader:Landroid/preference/Preference;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/UsersPreferences;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/chrome/R$string;->sign_in_to_chrome:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 95
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/UsersPreferences;->mSignInHeader:Landroid/preference/Preference;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/UsersPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lorg/chromium/chrome/browser/signin/SigninManager;->get(Landroid/content/Context;)Lorg/chromium/chrome/browser/signin/SigninManager;

    move-result-object v2

    invoke-virtual {v2}, Lorg/chromium/chrome/browser/signin/SigninManager;->isSignInAllowed()Z

    move-result v2

    invoke-virtual {v0, v2}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 96
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/UsersPreferences;->mSignInHeader:Landroid/preference/Preference;

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 97
    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/UsersPreferences;->updatePreferences()V

    goto :goto_0
.end method
