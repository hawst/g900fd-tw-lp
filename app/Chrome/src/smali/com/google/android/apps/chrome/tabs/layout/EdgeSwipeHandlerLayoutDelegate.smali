.class Lcom/google/android/apps/chrome/tabs/layout/EdgeSwipeHandlerLayoutDelegate;
.super Lcom/google/android/apps/chrome/eventfilter/EmptyEdgeSwipeHandler;
.source "EdgeSwipeHandlerLayoutDelegate.java"


# instance fields
.field private final mLayoutProvider:Lcom/google/android/apps/chrome/tabs/layout/LayoutProvider;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/chrome/tabs/layout/LayoutProvider;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/google/android/apps/chrome/eventfilter/EmptyEdgeSwipeHandler;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/google/android/apps/chrome/tabs/layout/EdgeSwipeHandlerLayoutDelegate;->mLayoutProvider:Lcom/google/android/apps/chrome/tabs/layout/LayoutProvider;

    .line 24
    return-void
.end method


# virtual methods
.method public swipeFinished()V
    .locals 4

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/EdgeSwipeHandlerLayoutDelegate;->mLayoutProvider:Lcom/google/android/apps/chrome/tabs/layout/LayoutProvider;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutProvider;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    if-nez v0, :cond_0

    .line 42
    :goto_0
    return-void

    .line 41
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/EdgeSwipeHandlerLayoutDelegate;->mLayoutProvider:Lcom/google/android/apps/chrome/tabs/layout/LayoutProvider;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutProvider;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    invoke-static {}, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->time()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->swipeFinished(J)V

    goto :goto_0
.end method

.method public swipeFlingOccurred(FFFFFF)V
    .locals 10

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/EdgeSwipeHandlerLayoutDelegate;->mLayoutProvider:Lcom/google/android/apps/chrome/tabs/layout/LayoutProvider;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutProvider;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    if-nez v0, :cond_0

    .line 49
    :goto_0
    return-void

    .line 47
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/EdgeSwipeHandlerLayoutDelegate;->mLayoutProvider:Lcom/google/android/apps/chrome/tabs/layout/LayoutProvider;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutProvider;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v1

    invoke-static {}, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->time()J

    move-result-wide v2

    move v4, p1

    move v5, p2

    move v6, p3

    move v7, p4

    move v8, p5

    move/from16 v9, p6

    invoke-virtual/range {v1 .. v9}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->swipeFlingOccurred(JFFFFFF)V

    goto :goto_0
.end method

.method public swipeStarted(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;FF)V
    .locals 7

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/EdgeSwipeHandlerLayoutDelegate;->mLayoutProvider:Lcom/google/android/apps/chrome/tabs/layout/LayoutProvider;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutProvider;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    if-nez v0, :cond_0

    .line 30
    :goto_0
    return-void

    .line 29
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/EdgeSwipeHandlerLayoutDelegate;->mLayoutProvider:Lcom/google/android/apps/chrome/tabs/layout/LayoutProvider;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutProvider;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v1

    invoke-static {}, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->time()J

    move-result-wide v2

    move-object v4, p1

    move v5, p2

    move v6, p3

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->swipeStarted(JLcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;FF)V

    goto :goto_0
.end method

.method public swipeUpdated(FFFFFF)V
    .locals 10

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/EdgeSwipeHandlerLayoutDelegate;->mLayoutProvider:Lcom/google/android/apps/chrome/tabs/layout/LayoutProvider;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutProvider;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    if-nez v0, :cond_0

    .line 36
    :goto_0
    return-void

    .line 35
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/EdgeSwipeHandlerLayoutDelegate;->mLayoutProvider:Lcom/google/android/apps/chrome/tabs/layout/LayoutProvider;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutProvider;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v1

    invoke-static {}, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->time()J

    move-result-wide v2

    move v4, p1

    move v5, p2

    move v6, p3

    move v7, p4

    move v8, p5

    move/from16 v9, p6

    invoke-virtual/range {v1 .. v9}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->swipeUpdated(JFFFFFF)V

    goto :goto_0
.end method
