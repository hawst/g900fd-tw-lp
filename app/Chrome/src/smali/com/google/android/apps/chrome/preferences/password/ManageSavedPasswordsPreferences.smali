.class public Lcom/google/android/apps/chrome/preferences/password/ManageSavedPasswordsPreferences;
.super Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;
.source "ManageSavedPasswordsPreferences.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;
.implements Landroid/preference/Preference$OnPreferenceClickListener;
.implements Lorg/chromium/chrome/browser/PasswordUIView$PasswordListObserver;


# static fields
.field public static final DELETED_ITEM_IS_EXCEPTION:Ljava/lang/String; = "is_exception"

.field public static final PASSWORD_LIST_DELETED_ID:Ljava/lang/String; = "deleted_id"

.field public static final PASSWORD_LIST_ID:Ljava/lang/String; = "id"

.field public static final PASSWORD_LIST_NAME:Ljava/lang/String; = "name"

.field public static final PASSWORD_LIST_URL:Ljava/lang/String; = "url"


# instance fields
.field private mActionBarSwitch:Landroid/widget/Switch;

.field private mEmptyView:Landroid/widget/TextView;

.field private mLinkPref:Landroid/preference/Preference;

.field private mNoPasswordExceptions:Z

.field private mNoPasswords:Z

.field private mPasswordEnabler:Lcom/google/android/apps/chrome/preferences/WidgetEnabler;

.field private final mPasswordManagerHandler:Lorg/chromium/chrome/browser/PasswordUIView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;-><init>()V

    .line 59
    new-instance v0, Lorg/chromium/chrome/browser/PasswordUIView;

    invoke-direct {v0}, Lorg/chromium/chrome/browser/PasswordUIView;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/preferences/password/ManageSavedPasswordsPreferences;->mPasswordManagerHandler:Lorg/chromium/chrome/browser/PasswordUIView;

    return-void
.end method

.method private displayEmptyScreenMessage()V
    .locals 2

    .prologue
    .line 92
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/password/ManageSavedPasswordsPreferences;->mEmptyView:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 93
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/password/ManageSavedPasswordsPreferences;->mEmptyView:Landroid/widget/TextView;

    sget v1, Lcom/google/android/apps/chrome/R$string;->saved_passwords_none_text:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 95
    :cond_0
    return-void
.end method

.method private getManageAccountTextSize()I
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 239
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 240
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/password/ManageSavedPasswordsPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    const v2, 0x1010041

    invoke-virtual {v1, v2, v0, v4}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 242
    new-array v1, v4, [I

    const v2, 0x1010095

    aput v2, v1, v3

    .line 243
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/password/ManageSavedPasswordsPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v2

    iget v0, v0, Landroid/util/TypedValue;->data:I

    invoke-virtual {v2, v0, v1}, Landroid/app/Activity;->obtainStyledAttributes(I[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 244
    const/4 v1, -0x1

    invoke-virtual {v0, v3, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    .line 245
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 246
    return v1
.end method


# virtual methods
.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 82
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 83
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/password/ManageSavedPasswordsPreferences;->getView()Landroid/view/View;

    move-result-object v0

    const v1, 0x102000a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 84
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/password/ManageSavedPasswordsPreferences;->getView()Landroid/view/View;

    move-result-object v1

    const v2, 0x1020004

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/apps/chrome/preferences/password/ManageSavedPasswordsPreferences;->mEmptyView:Landroid/widget/TextView;

    .line 85
    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/password/ManageSavedPasswordsPreferences;->mEmptyView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    .line 86
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 69
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    .line 70
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/password/ManageSavedPasswordsPreferences;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/password/ManageSavedPasswordsPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceManager;->createPreferenceScreen(Landroid/content/Context;)Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/password/ManageSavedPasswordsPreferences;->setPreferenceScreen(Landroid/preference/PreferenceScreen;)V

    .line 71
    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->isRememberPasswordsManaged()Z

    move-result v0

    .line 73
    new-instance v1, Lcom/google/android/apps/chrome/preferences/ManagedSwitch;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/password/ManageSavedPasswordsPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/google/android/apps/chrome/preferences/ManagedSwitch;-><init>(Landroid/content/Context;Z)V

    iput-object v1, p0, Lcom/google/android/apps/chrome/preferences/password/ManageSavedPasswordsPreferences;->mActionBarSwitch:Landroid/widget/Switch;

    .line 74
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/password/ManageSavedPasswordsPreferences;->mActionBarSwitch:Landroid/widget/Switch;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/password/ManageSavedPasswordsPreferences;->addSwitchToActionBar(Landroid/widget/Switch;)V

    .line 75
    new-instance v0, Lcom/google/android/apps/chrome/preferences/password/PasswordEnabler;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/password/ManageSavedPasswordsPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/preferences/password/ManageSavedPasswordsPreferences;->mActionBarSwitch:Landroid/widget/Switch;

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/chrome/preferences/password/PasswordEnabler;-><init>(Landroid/content/Context;Landroid/widget/Switch;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/preferences/password/ManageSavedPasswordsPreferences;->mPasswordEnabler:Lcom/google/android/apps/chrome/preferences/WidgetEnabler;

    .line 76
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/password/ManageSavedPasswordsPreferences;->mPasswordEnabler:Lcom/google/android/apps/chrome/preferences/WidgetEnabler;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/preferences/WidgetEnabler;->attach()V

    .line 77
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/password/ManageSavedPasswordsPreferences;->mPasswordManagerHandler:Lorg/chromium/chrome/browser/PasswordUIView;

    invoke-virtual {v0, p0}, Lorg/chromium/chrome/browser/PasswordUIView;->addObserver(Lorg/chromium/chrome/browser/PasswordUIView$PasswordListObserver;)V

    .line 78
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 206
    invoke-super {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;->onDestroy()V

    .line 207
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/password/ManageSavedPasswordsPreferences;->mPasswordEnabler:Lcom/google/android/apps/chrome/preferences/WidgetEnabler;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/preferences/WidgetEnabler;->destroy()V

    .line 208
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/password/ManageSavedPasswordsPreferences;->mPasswordManagerHandler:Lorg/chromium/chrome/browser/PasswordUIView;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/PasswordUIView;->destroy()V

    .line 209
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/password/ManageSavedPasswordsPreferences;->mActionBarSwitch:Landroid/widget/Switch;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/Switch;->setVisibility(I)V

    .line 210
    return-void
.end method

.method public onPasswordDeleted(Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 193
    if-eqz p1, :cond_0

    const-string/jumbo v0, "deleted_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 194
    const-string/jumbo v0, "deleted_id"

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 195
    const-string/jumbo v1, "is_exception"

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 196
    if-eqz v1, :cond_1

    .line 197
    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/password/ManageSavedPasswordsPreferences;->mPasswordManagerHandler:Lorg/chromium/chrome/browser/PasswordUIView;

    invoke-virtual {v1, v0}, Lorg/chromium/chrome/browser/PasswordUIView;->removeSavedPasswordException(I)V

    .line 202
    :cond_0
    :goto_0
    return-void

    .line 199
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/password/ManageSavedPasswordsPreferences;->mPasswordManagerHandler:Lorg/chromium/chrome/browser/PasswordUIView;

    invoke-virtual {v1, v0}, Lorg/chromium/chrome/browser/PasswordUIView;->removeSavedPasswordEntry(I)V

    goto :goto_0
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 99
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/password/ManageSavedPasswordsPreferences;->rebuildPasswordLists()V

    .line 100
    const/4 v0, 0x1

    return v0
.end method

.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 3

    .prologue
    .line 218
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/password/ManageSavedPasswordsPreferences;->mLinkPref:Landroid/preference/Preference;

    if-ne p1, v0, :cond_0

    .line 219
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.action.VIEW"

    invoke-static {}, Lorg/chromium/chrome/browser/PasswordUIView;->getAccountDashboardURL()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 222
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/password/ManageSavedPasswordsPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-class v2, Lcom/google/android/apps/chrome/ChromeTabbedActivity;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    .line 223
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/password/ManageSavedPasswordsPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 235
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 227
    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 228
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/password/ManageSavedPasswordsPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-class v2, Lcom/google/android/apps/chrome/preferences/Preferences;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    .line 229
    const-string/jumbo v1, ":android:show_fragment"

    const-class v2, Lcom/google/android/apps/chrome/preferences/password/PasswordEntryEditor;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 231
    const-string/jumbo v1, ":android:show_fragment_args"

    invoke-virtual {p1}, Landroid/preference/Preference;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 233
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/password/ManageSavedPasswordsPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 184
    invoke-super {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;->onResume()V

    .line 185
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/password/ManageSavedPasswordsPreferences;->rebuildPasswordLists()V

    .line 186
    return-void
.end method

.method public passwordExceptionListAvailable(I)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 160
    if-nez p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/chrome/preferences/password/ManageSavedPasswordsPreferences;->mNoPasswordExceptions:Z

    .line 161
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/preferences/password/ManageSavedPasswordsPreferences;->mNoPasswordExceptions:Z

    if-eqz v0, :cond_2

    .line 162
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/preferences/password/ManageSavedPasswordsPreferences;->mNoPasswords:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/password/ManageSavedPasswordsPreferences;->displayEmptyScreenMessage()V

    .line 180
    :cond_0
    return-void

    :cond_1
    move v0, v1

    .line 160
    goto :goto_0

    .line 166
    :cond_2
    new-instance v0, Landroid/preference/PreferenceCategory;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/password/ManageSavedPasswordsPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 167
    const-string/jumbo v2, "exceptions"

    invoke-virtual {v0, v2}, Landroid/preference/PreferenceCategory;->setKey(Ljava/lang/String;)V

    .line 168
    sget v2, Lcom/google/android/apps/chrome/R$string;->section_saved_passwords_exceptions:I

    invoke-virtual {v0, v2}, Landroid/preference/PreferenceCategory;->setTitle(I)V

    .line 169
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/password/ManageSavedPasswordsPreferences;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 170
    :goto_1
    if-ge v1, p1, :cond_0

    .line 171
    iget-object v2, p0, Lcom/google/android/apps/chrome/preferences/password/ManageSavedPasswordsPreferences;->mPasswordManagerHandler:Lorg/chromium/chrome/browser/PasswordUIView;

    invoke-virtual {v2, v1}, Lorg/chromium/chrome/browser/PasswordUIView;->getSavedPasswordException(I)Ljava/lang/String;

    move-result-object v2

    .line 172
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/password/ManageSavedPasswordsPreferences;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/password/ManageSavedPasswordsPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/preference/PreferenceManager;->createPreferenceScreen(Landroid/content/Context;)Landroid/preference/PreferenceScreen;

    move-result-object v3

    .line 173
    invoke-virtual {v3, v2}, Landroid/preference/PreferenceScreen;->setTitle(Ljava/lang/CharSequence;)V

    .line 174
    invoke-virtual {v3, p0}, Landroid/preference/PreferenceScreen;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 175
    invoke-virtual {v3}, Landroid/preference/PreferenceScreen;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    .line 176
    const-string/jumbo v5, "url"

    invoke-virtual {v4, v5, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 177
    const-string/jumbo v2, "id"

    invoke-virtual {v4, v2, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 178
    invoke-virtual {v0, v3}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 170
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public passwordListAvailable(I)V
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 112
    if-nez p1, :cond_1

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/chrome/preferences/password/ManageSavedPasswordsPreferences;->mNoPasswords:Z

    .line 113
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/preferences/password/ManageSavedPasswordsPreferences;->mNoPasswords:Z

    if-eqz v0, :cond_2

    .line 114
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/preferences/password/ManageSavedPasswordsPreferences;->mNoPasswordExceptions:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/password/ManageSavedPasswordsPreferences;->displayEmptyScreenMessage()V

    .line 156
    :cond_0
    return-void

    :cond_1
    move v0, v2

    .line 112
    goto :goto_0

    .line 118
    :cond_2
    invoke-static {}, Lorg/chromium/chrome/browser/PasswordUIView;->shouldDisplayManageAccountLink()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 119
    new-instance v0, Landroid/preference/Preference;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/password/ManageSavedPasswordsPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-direct {v0, v3}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/preferences/password/ManageSavedPasswordsPreferences;->mLinkPref:Landroid/preference/Preference;

    .line 125
    new-instance v0, Landroid/text/SpannableString;

    sget v3, Lcom/google/android/apps/chrome/R$string;->manage_passwords_text:I

    invoke-virtual {p0, v3}, Lcom/google/android/apps/chrome/preferences/password/ManageSavedPasswordsPreferences;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v1, v1, [Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;

    new-instance v4, Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;

    const-string/jumbo v5, "<link>"

    const-string/jumbo v6, "</link>"

    new-instance v7, Landroid/text/style/ForegroundColorSpan;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/password/ManageSavedPasswordsPreferences;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    sget v9, Lcom/google/android/apps/chrome/R$color;->active_control_color:I

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v8

    invoke-direct {v7, v8}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-direct {v4, v5, v6, v7}, Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    aput-object v4, v1, v2

    invoke-static {v3, v1}, Lcom/google/android/apps/chrome/utilities/StringLinkifier;->linkify(Ljava/lang/String;[Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;)Landroid/text/SpannableString;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 130
    new-instance v1, Landroid/text/style/AbsoluteSizeSpan;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/password/ManageSavedPasswordsPreferences;->getManageAccountTextSize()I

    move-result v3

    invoke-direct {v1, v3}, Landroid/text/style/AbsoluteSizeSpan;-><init>(I)V

    invoke-interface {v0}, Landroid/text/Spannable;->length()I

    move-result v3

    const/16 v4, 0x12

    invoke-interface {v0, v1, v2, v3, v4}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 132
    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/password/ManageSavedPasswordsPreferences;->mLinkPref:Landroid/preference/Preference;

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 133
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/password/ManageSavedPasswordsPreferences;->mLinkPref:Landroid/preference/Preference;

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 134
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/password/ManageSavedPasswordsPreferences;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/password/ManageSavedPasswordsPreferences;->mLinkPref:Landroid/preference/Preference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 137
    :cond_3
    new-instance v0, Landroid/preference/PreferenceCategory;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/password/ManageSavedPasswordsPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 138
    const-string/jumbo v1, "saved_passwords"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->setKey(Ljava/lang/String;)V

    .line 139
    sget v1, Lcom/google/android/apps/chrome/R$string;->section_saved_passwords:I

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->setTitle(I)V

    .line 140
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/password/ManageSavedPasswordsPreferences;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 141
    :goto_1
    if-ge v2, p1, :cond_0

    .line 142
    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/password/ManageSavedPasswordsPreferences;->mPasswordManagerHandler:Lorg/chromium/chrome/browser/PasswordUIView;

    invoke-virtual {v1, v2}, Lorg/chromium/chrome/browser/PasswordUIView;->getSavedPasswordEntry(I)Lorg/chromium/chrome/browser/PasswordUIView$SavedPasswordEntry;

    move-result-object v1

    .line 144
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/password/ManageSavedPasswordsPreferences;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/password/ManageSavedPasswordsPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/preference/PreferenceManager;->createPreferenceScreen(Landroid/content/Context;)Landroid/preference/PreferenceScreen;

    move-result-object v3

    .line 145
    invoke-virtual {v1}, Lorg/chromium/chrome/browser/PasswordUIView$SavedPasswordEntry;->getUrl()Ljava/lang/String;

    move-result-object v4

    .line 146
    invoke-virtual {v1}, Lorg/chromium/chrome/browser/PasswordUIView$SavedPasswordEntry;->getUserName()Ljava/lang/String;

    move-result-object v1

    .line 147
    invoke-virtual {v3, v4}, Landroid/preference/PreferenceScreen;->setTitle(Ljava/lang/CharSequence;)V

    .line 148
    invoke-virtual {v3, p0}, Landroid/preference/PreferenceScreen;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 149
    invoke-virtual {v3, v1}, Landroid/preference/PreferenceScreen;->setSummary(Ljava/lang/CharSequence;)V

    .line 150
    invoke-virtual {v3}, Landroid/preference/PreferenceScreen;->getExtras()Landroid/os/Bundle;

    move-result-object v5

    .line 151
    const-string/jumbo v6, "name"

    invoke-virtual {v5, v6, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 152
    const-string/jumbo v1, "url"

    invoke-virtual {v5, v1, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    const-string/jumbo v1, "id"

    invoke-virtual {v5, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 154
    invoke-virtual {v0, v3}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 141
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method rebuildPasswordLists()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 104
    iput-boolean v0, p0, Lcom/google/android/apps/chrome/preferences/password/ManageSavedPasswordsPreferences;->mNoPasswords:Z

    .line 105
    iput-boolean v0, p0, Lcom/google/android/apps/chrome/preferences/password/ManageSavedPasswordsPreferences;->mNoPasswordExceptions:Z

    .line 106
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/password/ManageSavedPasswordsPreferences;->mPasswordManagerHandler:Lorg/chromium/chrome/browser/PasswordUIView;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/PasswordUIView;->updatePasswordLists()V

    .line 107
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/password/ManageSavedPasswordsPreferences;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0}, Landroid/preference/PreferenceScreen;->removeAll()V

    .line 108
    return-void
.end method
