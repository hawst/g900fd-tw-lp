.class public Lcom/google/android/apps/chrome/document/DocumentTab;
.super Lcom/google/android/apps/chrome/tab/ChromeTab;
.source "DocumentTab.java"

# interfaces
.implements Lorg/chromium/chrome/browser/favicon/FaviconHelper$FaviconImageCallback;


# static fields
.field private static final ICON_TYPES:[I

.field private static final sNativePageFactory:Lcom/google/android/apps/chrome/tab/NativePageFactory;


# instance fields
.field private final mActivity:Lcom/google/android/apps/chrome/document/DocumentActivity;

.field private mCreatedFromWebContents:Z

.field private mDesiredIconSizePx:I

.field private mDidRestoreState:Z

.field private mFaviconHelper:Lorg/chromium/chrome/browser/favicon/FaviconHelper;

.field private mWebContentsObserver:Lorg/chromium/content/browser/WebContentsObserverAndroid;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 53
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/apps/chrome/document/DocumentTab;->ICON_TYPES:[I

    .line 57
    new-instance v0, Lcom/google/android/apps/chrome/tab/NativePageFactory;

    new-instance v1, Lcom/google/android/apps/chrome/document/DocumentTab$1;

    invoke-direct {v1}, Lcom/google/android/apps/chrome/document/DocumentTab$1;-><init>()V

    invoke-direct {v0, v1}, Lcom/google/android/apps/chrome/tab/NativePageFactory;-><init>(Lcom/google/android/apps/chrome/tab/NativePageFactory$NativePageBuilder;)V

    sput-object v0, Lcom/google/android/apps/chrome/document/DocumentTab;->sNativePageFactory:Lcom/google/android/apps/chrome/tab/NativePageFactory;

    return-void

    .line 53
    nop

    :array_0
    .array-data 4
        0x1
        0x6
    .end array-data
.end method

.method private constructor <init>(Lcom/google/android/apps/chrome/document/DocumentActivity;ZLorg/chromium/ui/base/WindowAndroid;Ljava/lang/String;I)V
    .locals 8

    .prologue
    .line 121
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/document/DocumentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/IntentHandler;->getTabIdFromIntent(Landroid/content/Intent;)I

    move-result v1

    sget-object v5, Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;->FROM_EXTERNAL_APP:Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;

    const/4 v7, 0x0

    move-object v0, p0

    move-object v2, p1

    move v3, p2

    move-object v4, p3

    move v6, p5

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/chrome/tab/ChromeTab;-><init>(ILcom/google/android/apps/chrome/ChromeActivity;ZLorg/chromium/ui/base/WindowAndroid;Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;ILcom/google/android/apps/chrome/tab/TabUma$TabCreationState;)V

    .line 123
    iput-object p1, p0, Lcom/google/android/apps/chrome/document/DocumentTab;->mActivity:Lcom/google/android/apps/chrome/document/DocumentActivity;

    .line 124
    const-wide/16 v2, 0x0

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/document/DocumentActivity;->getTabContentManager()Lcom/google/android/apps/chrome/compositor/TabContentManager;

    move-result-object v4

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/chrome/document/DocumentTab;->initialize(Ljava/lang/String;JLcom/google/android/apps/chrome/compositor/TabContentManager;Z)V

    .line 125
    return-void
.end method

.method private constructor <init>(Lcom/google/android/apps/chrome/document/DocumentActivity;ZLorg/chromium/ui/base/WindowAndroid;Ljava/lang/String;IJ)V
    .locals 8

    .prologue
    .line 154
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/document/DocumentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/IntentHandler;->getTabIdFromIntent(Landroid/content/Intent;)I

    move-result v1

    sget-object v5, Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;->FROM_LONGPRESS_FOREGROUND:Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;

    const/4 v7, 0x0

    move-object v0, p0

    move-object v2, p1

    move v3, p2

    move-object v4, p3

    move v6, p5

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/chrome/tab/ChromeTab;-><init>(ILcom/google/android/apps/chrome/ChromeActivity;ZLorg/chromium/ui/base/WindowAndroid;Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;ILcom/google/android/apps/chrome/tab/TabUma$TabCreationState;)V

    .line 157
    iput-object p1, p0, Lcom/google/android/apps/chrome/document/DocumentTab;->mActivity:Lcom/google/android/apps/chrome/document/DocumentActivity;

    .line 158
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/document/DocumentActivity;->getTabContentManager()Lcom/google/android/apps/chrome/compositor/TabContentManager;

    move-result-object v4

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p4

    move-wide v2, p6

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/chrome/document/DocumentTab;->initialize(Ljava/lang/String;JLcom/google/android/apps/chrome/compositor/TabContentManager;Z)V

    .line 159
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/document/DocumentTab;->mCreatedFromWebContents:Z

    .line 160
    return-void
.end method

.method private constructor <init>(Lcom/google/android/apps/chrome/document/DocumentActivity;ZLorg/chromium/ui/base/WindowAndroid;Ljava/lang/String;Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;I)V
    .locals 8

    .prologue
    .line 138
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/document/DocumentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/IntentHandler;->getTabIdFromIntent(Landroid/content/Intent;)I

    move-result v1

    const/4 v7, 0x0

    move-object v0, p0

    move-object v2, p1

    move v3, p2

    move-object v4, p3

    move v5, p6

    move-object v6, p5

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/chrome/tab/ChromeTab;-><init>(ILcom/google/android/apps/chrome/ChromeActivity;ZLorg/chromium/ui/base/WindowAndroid;ILcom/google/android/apps/chrome/tab/ChromeTab$TabState;Lcom/google/android/apps/chrome/tab/TabUma$TabCreationState;)V

    .line 140
    iput-object p1, p0, Lcom/google/android/apps/chrome/document/DocumentTab;->mActivity:Lcom/google/android/apps/chrome/document/DocumentActivity;

    .line 141
    const-wide/16 v2, 0x0

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/document/DocumentActivity;->getTabContentManager()Lcom/google/android/apps/chrome/compositor/TabContentManager;

    move-result-object v4

    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/chrome/document/DocumentTab;->initialize(Ljava/lang/String;JLcom/google/android/apps/chrome/compositor/TabContentManager;Z)V

    .line 142
    return-void
.end method

.method static synthetic access$000()[I
    .locals 1

    .prologue
    .line 52
    sget-object v0, Lcom/google/android/apps/chrome/document/DocumentTab;->ICON_TYPES:[I

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/document/DocumentTab;)I
    .locals 1

    .prologue
    .line 52
    iget v0, p0, Lcom/google/android/apps/chrome/document/DocumentTab;->mDesiredIconSizePx:I

    return v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/document/DocumentTab;)Lorg/chromium/chrome/browser/favicon/FaviconHelper;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentTab;->mFaviconHelper:Lorg/chromium/chrome/browser/favicon/FaviconHelper;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/chrome/document/DocumentTab;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 52
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/document/DocumentTab;->shouldInterceptContextMenuDownload(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/chrome/document/DocumentTab;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 52
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/document/DocumentTab;->isSpdyProxyEnabledForUrl(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/chrome/document/DocumentTab;)Lcom/google/android/apps/chrome/document/DocumentActivity;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentTab;->mActivity:Lcom/google/android/apps/chrome/document/DocumentActivity;

    return-object v0
.end method

.method static create(Lcom/google/android/apps/chrome/document/DocumentActivity;ZLorg/chromium/ui/base/WindowAndroid;Ljava/lang/String;J)Lcom/google/android/apps/chrome/document/DocumentTab;
    .locals 14

    .prologue
    .line 388
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "com.android.chrome.parent_tab_id"

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    .line 390
    const-wide/16 v0, 0x0

    cmp-long v0, p4, v0

    if-eqz v0, :cond_0

    .line 391
    new-instance v0, Lcom/google/android/apps/chrome/document/DocumentTab;

    move-object v1, p0

    move v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-wide/from16 v6, p4

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/chrome/document/DocumentTab;-><init>(Lcom/google/android/apps/chrome/document/DocumentActivity;ZLorg/chromium/ui/base/WindowAndroid;Ljava/lang/String;IJ)V

    .line 401
    :goto_0
    return-object v0

    .line 395
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/IntentHandler;->getTabIdFromIntent(Landroid/content/Intent;)I

    move-result v0

    .line 396
    invoke-static {p1}, Lcom/google/android/apps/chrome/document/DocumentActivityDatabase;->getTabList(Z)Lcom/google/android/apps/chrome/document/DocumentTabList;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/document/DocumentTabList;->getTabStateForDocument(I)Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;

    move-result-object v11

    .line 398
    if-nez v11, :cond_1

    .line 399
    new-instance v0, Lcom/google/android/apps/chrome/document/DocumentTab;

    move-object v1, p0

    move v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/chrome/document/DocumentTab;-><init>(Lcom/google/android/apps/chrome/document/DocumentActivity;ZLorg/chromium/ui/base/WindowAndroid;Ljava/lang/String;I)V

    goto :goto_0

    .line 401
    :cond_1
    new-instance v6, Lcom/google/android/apps/chrome/document/DocumentTab;

    const-string/jumbo v10, ""

    move-object v7, p0

    move v8, p1

    move-object/from16 v9, p2

    move v12, v5

    invoke-direct/range {v6 .. v12}, Lcom/google/android/apps/chrome/document/DocumentTab;-><init>(Lcom/google/android/apps/chrome/document/DocumentActivity;ZLorg/chromium/ui/base/WindowAndroid;Ljava/lang/String;Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;I)V

    move-object v0, v6

    goto :goto_0
.end method

.method private initialize(Ljava/lang/String;JLcom/google/android/apps/chrome/compositor/TabContentManager;Z)V
    .locals 2

    .prologue
    .line 194
    const/high16 v0, 0x42000000    # 32.0f

    iget-object v1, p0, Lcom/google/android/apps/chrome/document/DocumentTab;->mActivity:Lcom/google/android/apps/chrome/document/DocumentActivity;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/document/DocumentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/google/android/apps/chrome/document/DocumentTab;->mDesiredIconSizePx:I

    .line 197
    if-nez p5, :cond_0

    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-nez v0, :cond_0

    .line 198
    invoke-static {}, Lcom/google/android/apps/chrome/WarmupManager;->getInstance()Lcom/google/android/apps/chrome/WarmupManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/WarmupManager;->hasPrerenderedUrl(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/google/android/apps/chrome/WarmupManager;->getInstance()Lcom/google/android/apps/chrome/WarmupManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/WarmupManager;->takePrerenderedNativeWebContents()J

    move-result-wide v0

    :goto_0
    move-wide p2, v0

    .line 202
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, p2, p3, p4, v0}, Lcom/google/android/apps/chrome/document/DocumentTab;->initialize(JLcom/google/android/apps/chrome/compositor/TabContentManager;Z)V

    .line 203
    if-eqz p5, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentTab;->unfreezeContents()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/document/DocumentTab;->mDidRestoreState:Z

    .line 205
    :cond_1
    new-instance v0, Lorg/chromium/chrome/browser/favicon/FaviconHelper;

    invoke-direct {v0}, Lorg/chromium/chrome/browser/favicon/FaviconHelper;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentTab;->mFaviconHelper:Lorg/chromium/chrome/browser/favicon/FaviconHelper;

    .line 206
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentTab;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 207
    return-void

    .line 198
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentTab;->isIncognito()Z

    move-result v0

    invoke-static {v0}, Lorg/chromium/chrome/browser/ContentViewUtil;->createNativeWebContents(Z)J

    move-result-wide v0

    goto :goto_0
.end method


# virtual methods
.method protected createContextMenuPopulator()Lorg/chromium/chrome/browser/contextmenu/ContextMenuPopulator;
    .locals 3

    .prologue
    .line 316
    new-instance v0, Lorg/chromium/chrome/browser/contextmenu/ChromeContextMenuPopulator;

    new-instance v1, Lcom/google/android/apps/chrome/document/DocumentTab$DocumentContextMenuItemDelegate;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/android/apps/chrome/document/DocumentTab$DocumentContextMenuItemDelegate;-><init>(Lcom/google/android/apps/chrome/document/DocumentTab;Lcom/google/android/apps/chrome/document/DocumentTab$1;)V

    invoke-direct {v0, v1}, Lorg/chromium/chrome/browser/contextmenu/ChromeContextMenuPopulator;-><init>(Lorg/chromium/chrome/browser/contextmenu/ChromeContextMenuItemDelegate;)V

    return-object v0
.end method

.method protected createWebContentsDelegate()Lorg/chromium/chrome/browser/Tab$TabChromeWebContentsDelegateAndroid;
    .locals 1

    .prologue
    .line 356
    new-instance v0, Lcom/google/android/apps/chrome/document/DocumentTab$DocumentTabChromeWebContentsDelegateAndroidImpl;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/document/DocumentTab$DocumentTabChromeWebContentsDelegateAndroidImpl;-><init>(Lcom/google/android/apps/chrome/document/DocumentTab;)V

    return-object v0
.end method

.method public destroy()V
    .locals 1

    .prologue
    .line 165
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentTab;->mFaviconHelper:Lorg/chromium/chrome/browser/favicon/FaviconHelper;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentTab;->mFaviconHelper:Lorg/chromium/chrome/browser/favicon/FaviconHelper;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/favicon/FaviconHelper;->destroy()V

    .line 166
    :cond_0
    invoke-super {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->destroy()V

    .line 167
    return-void
.end method

.method protected destroyContentViewCoreInternal(Lorg/chromium/content/browser/ContentViewCore;)V
    .locals 1

    .prologue
    .line 211
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->destroyContentViewCoreInternal(Lorg/chromium/content/browser/ContentViewCore;)V

    .line 212
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentTab;->mWebContentsObserver:Lorg/chromium/content/browser/WebContentsObserverAndroid;

    .line 213
    return-void
.end method

.method public didRestoreState()Z
    .locals 1

    .prologue
    .line 368
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/document/DocumentTab;->mDidRestoreState:Z

    return v0
.end method

.method protected getNativePageFactory()Lcom/google/android/apps/chrome/tab/NativePageFactory;
    .locals 1

    .prologue
    .line 361
    sget-object v0, Lcom/google/android/apps/chrome/document/DocumentTab;->sNativePageFactory:Lcom/google/android/apps/chrome/tab/NativePageFactory;

    return-object v0
.end method

.method protected initContentViewCore(J)V
    .locals 3

    .prologue
    .line 171
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/chrome/tab/ChromeTab;->initContentViewCore(J)V

    .line 172
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentTab;->getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lorg/chromium/content/browser/ContentViewCore;->setFullscreenRequiredForOrientationLock(Z)V

    .line 173
    new-instance v0, Lcom/google/android/apps/chrome/document/DocumentTab$2;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentTab;->getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v1

    invoke-virtual {v1}, Lorg/chromium/content/browser/ContentViewCore;->getWebContents()Lorg/chromium/content_public/browser/WebContents;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/chrome/document/DocumentTab$2;-><init>(Lcom/google/android/apps/chrome/document/DocumentTab;Lorg/chromium/content_public/browser/WebContents;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentTab;->mWebContentsObserver:Lorg/chromium/content/browser/WebContentsObserverAndroid;

    .line 184
    return-void
.end method

.method public isCreatedWithWebContents()Z
    .locals 1

    .prologue
    .line 375
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/document/DocumentTab;->mCreatedFromWebContents:Z

    return v0
.end method

.method protected notifyTabCrashed(Z)V
    .locals 3

    .prologue
    .line 239
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->notifyTabCrashed(Z)V

    .line 240
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentTab;->getTabObservers()Lorg/chromium/base/ObserverList$RewindableIterator;

    move-result-object v1

    .line 241
    :cond_0
    :goto_0
    invoke-interface {v1}, Lorg/chromium/base/ObserverList$RewindableIterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 242
    invoke-interface {v1}, Lorg/chromium/base/ObserverList$RewindableIterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/TabObserver;

    .line 243
    instance-of v2, v0, Lcom/google/android/apps/chrome/document/DocumentTab$DocumentTabObserver;

    if-eqz v2, :cond_0

    .line 244
    check-cast v0, Lcom/google/android/apps/chrome/document/DocumentTab$DocumentTabObserver;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/document/DocumentTab$DocumentTabObserver;->onTabCrashed()V

    goto :goto_0

    .line 247
    :cond_1
    return-void
.end method

.method public onFaviconAvailable(Landroid/graphics/Bitmap;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 224
    if-nez p1, :cond_1

    .line 235
    :cond_0
    return-void

    .line 225
    :cond_1
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iget v1, p0, Lcom/google/android/apps/chrome/document/DocumentTab;->mDesiredIconSizePx:I

    if-lt v0, v1, :cond_0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    iget v1, p0, Lcom/google/android/apps/chrome/document/DocumentTab;->mDesiredIconSizePx:I

    if-lt v0, v1, :cond_0

    .line 228
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentTab;->getTabObservers()Lorg/chromium/base/ObserverList$RewindableIterator;

    move-result-object v1

    .line 229
    :cond_2
    :goto_0
    invoke-interface {v1}, Lorg/chromium/base/ObserverList$RewindableIterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 230
    invoke-interface {v1}, Lorg/chromium/base/ObserverList$RewindableIterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/TabObserver;

    .line 231
    instance-of v2, v0, Lcom/google/android/apps/chrome/document/DocumentTab$DocumentTabObserver;

    if-eqz v2, :cond_2

    .line 232
    check-cast v0, Lcom/google/android/apps/chrome/document/DocumentTab$DocumentTabObserver;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/document/DocumentTab$DocumentTabObserver;->onFaviconReceived(Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method public onFaviconUpdated()V
    .locals 6

    .prologue
    .line 217
    invoke-super {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->onFaviconUpdated()V

    .line 218
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentTab;->mFaviconHelper:Lorg/chromium/chrome/browser/favicon/FaviconHelper;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentTab;->getProfile()Lorg/chromium/chrome/browser/profiles/Profile;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentTab;->getUrl()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/chrome/document/DocumentTab;->ICON_TYPES:[I

    iget v4, p0, Lcom/google/android/apps/chrome/document/DocumentTab;->mDesiredIconSizePx:I

    move-object v5, p0

    invoke-virtual/range {v0 .. v5}, Lorg/chromium/chrome/browser/favicon/FaviconHelper;->getLargestRawFaviconForUrl(Lorg/chromium/chrome/browser/profiles/Profile;Ljava/lang/String;[IILorg/chromium/chrome/browser/favicon/FaviconHelper$FaviconImageCallback;)V

    .line 220
    return-void
.end method
