.class public Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;
.super Ljava/lang/Object;
.source "StripLayoutHelperManager.java"


# instance fields
.field private final mHeight:F

.field private final mIncognitoHelper:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;

.field private mIsIncognito:Z

.field private final mModelSelectorButton:Lcom/google/android/apps/chrome/tabs/CompositorButton;

.field private final mNormalHelper:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;

.field private final mRenderHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;

.field private mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

.field private final mUpdateHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;

.field private mWidth:F


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;)V
    .locals 5

    .prologue
    const/high16 v3, 0x41c00000    # 24.0f

    const/4 v2, 0x0

    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    const/high16 v0, 0x42200000    # 40.0f

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->mHeight:F

    .line 64
    iput-object p2, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->mUpdateHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;

    .line 65
    iput-object p3, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->mRenderHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;

    .line 67
    new-instance v0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;

    invoke-direct {v0, p1, p2, p3, v2}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;-><init>(Landroid/content/Context;Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;Z)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->mNormalHelper:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;

    .line 68
    new-instance v0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;

    const/4 v1, 0x1

    invoke-direct {v0, p1, p2, p3, v1}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;-><init>(Landroid/content/Context;Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;Z)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->mIncognitoHelper:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;

    .line 71
    new-instance v0, Lcom/google/android/apps/chrome/tabs/CompositorButton;

    invoke-direct {v0, p1, v3, v3}, Lcom/google/android/apps/chrome/tabs/CompositorButton;-><init>(Landroid/content/Context;FF)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->mModelSelectorButton:Lcom/google/android/apps/chrome/tabs/CompositorButton;

    .line 73
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->mModelSelectorButton:Lcom/google/android/apps/chrome/tabs/CompositorButton;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/chrome/tabs/CompositorButton;->setIncognito(Z)V

    .line 74
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->mModelSelectorButton:Lcom/google/android/apps/chrome/tabs/CompositorButton;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/chrome/tabs/CompositorButton;->setVisible(Z)V

    .line 76
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->mModelSelectorButton:Lcom/google/android/apps/chrome/tabs/CompositorButton;

    sget v1, Lcom/google/android/apps/chrome/R$drawable;->btn_tabstrip_switch_normal:I

    sget v2, Lcom/google/android/apps/chrome/R$drawable;->btn_tabstrip_switch_normal:I

    sget v3, Lcom/google/android/apps/chrome/R$drawable;->btn_tabstrip_switch_incognito:I

    sget v4, Lcom/google/android/apps/chrome/R$drawable;->btn_tabstrip_switch_incognito:I

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/apps/chrome/tabs/CompositorButton;->setResources(IIII)V

    .line 80
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->mModelSelectorButton:Lcom/google/android/apps/chrome/tabs/CompositorButton;

    const/high16 v1, 0x41200000    # 10.0f

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/tabs/CompositorButton;->setY(F)V

    .line 82
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->onContextChanged(Landroid/content/Context;)V

    .line 83
    return-void
.end method

.method private getInactiveStripLayoutHelper()Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;
    .locals 1

    .prologue
    .line 413
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->mIsIncognito:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->mNormalHelper:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->mIncognitoHelper:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;

    goto :goto_0
.end method

.method private updateModelSwitcherButton()V
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 213
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->mModelSelectorButton:Lcom/google/android/apps/chrome/tabs/CompositorButton;

    iget-boolean v2, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->mIsIncognito:Z

    invoke-virtual {v1, v2}, Lcom/google/android/apps/chrome/tabs/CompositorButton;->setIncognito(Z)V

    .line 214
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    if-eqz v1, :cond_0

    .line 215
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    invoke-interface {v1, v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->getModel(Z)Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v1

    invoke-interface {v1}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->getCount()I

    move-result v1

    if-eqz v1, :cond_1

    .line 216
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->mModelSelectorButton:Lcom/google/android/apps/chrome/tabs/CompositorButton;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/tabs/CompositorButton;->setVisible(Z)V

    .line 217
    if-eqz v0, :cond_2

    const/high16 v0, 0x42040000    # 33.0f

    .line 221
    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->mNormalHelper:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->setRightMargin(F)V

    .line 222
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->mIncognitoHelper:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->setRightMargin(F)V

    .line 224
    :cond_0
    return-void

    .line 215
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 217
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public click(JFF)V
    .locals 3

    .prologue
    .line 376
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->mModelSelectorButton:Lcom/google/android/apps/chrome/tabs/CompositorButton;

    invoke-virtual {v0, p3, p4}, Lcom/google/android/apps/chrome/tabs/CompositorButton;->click(FF)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    if-eqz v0, :cond_1

    .line 377
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->isIncognitoSelected()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-interface {v1, v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->selectModel(Z)V

    .line 381
    :goto_1
    return-void

    .line 377
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 380
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->getActiveStripLayoutHelper()Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->click(JFF)V

    goto :goto_1
.end method

.method public drag(JFFFFFF)V
    .locals 11

    .prologue
    .line 332
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->mModelSelectorButton:Lcom/google/android/apps/chrome/tabs/CompositorButton;

    invoke-virtual {v0, p3, p4}, Lcom/google/android/apps/chrome/tabs/CompositorButton;->drag(FF)Z

    .line 333
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->getActiveStripLayoutHelper()Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;

    move-result-object v1

    move-wide v2, p1

    move v4, p3

    move v5, p4

    move/from16 v6, p5

    move/from16 v7, p6

    move/from16 v8, p7

    move/from16 v9, p8

    invoke-virtual/range {v1 .. v9}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->drag(JFFFFFF)V

    .line 334
    return-void
.end method

.method public fling(JFFFF)V
    .locals 9

    .prologue
    .line 345
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->getActiveStripLayoutHelper()Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;

    move-result-object v1

    move-wide v2, p1

    move v4, p3

    move v5, p4

    move v6, p5

    move v7, p6

    invoke-virtual/range {v1 .. v7}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->fling(JFFFF)V

    .line 346
    return-void
.end method

.method public getActiveStripLayoutHelper()Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;
    .locals 1

    .prologue
    .line 409
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->mIsIncognito:Z

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->getStripLayoutHelper(Z)Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;

    move-result-object v0

    return-object v0
.end method

.method public getBorderOpacity()F
    .locals 1

    .prologue
    .line 176
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->mIsIncognito:Z

    if-eqz v0, :cond_0

    const v0, 0x3ecccccd    # 0.4f

    :goto_0
    return v0

    :cond_0
    const v0, 0x3e4ccccd    # 0.2f

    goto :goto_0
.end method

.method public getHeight()F
    .locals 1

    .prologue
    .line 162
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->mHeight:F

    return v0
.end method

.method public getModelSelectorButton()Lcom/google/android/apps/chrome/tabs/CompositorButton;
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->mModelSelectorButton:Lcom/google/android/apps/chrome/tabs/CompositorButton;

    return-object v0
.end method

.method public getNewTabButton()Lcom/google/android/apps/chrome/tabs/CompositorButton;
    .locals 1

    .prologue
    .line 97
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->getActiveStripLayoutHelper()Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->getNewTabButton()Lcom/google/android/apps/chrome/tabs/CompositorButton;

    move-result-object v0

    return-object v0
.end method

.method public getStripBrightness()F
    .locals 1

    .prologue
    .line 112
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->getActiveStripLayoutHelper()Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->getStripBrightness()F

    move-result v0

    return v0
.end method

.method public getStripLayoutHelper(Z)Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;
    .locals 1

    .prologue
    .line 401
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->mIncognitoHelper:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->mNormalHelper:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;

    goto :goto_0
.end method

.method public getStripLayoutTabsToRender()[Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;
    .locals 1

    .prologue
    .line 90
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->getActiveStripLayoutHelper()Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->getStripLayoutTabsToRender()[Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    move-result-object v0

    return-object v0
.end method

.method public getWidth()F
    .locals 1

    .prologue
    .line 169
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->mWidth:F

    return v0
.end method

.method public onContextChanged(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->mNormalHelper:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->onContextChanged(Landroid/content/Context;)V

    .line 185
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->mIncognitoHelper:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->onContextChanged(Landroid/content/Context;)V

    .line 186
    return-void
.end method

.method public onDown(JFF)V
    .locals 1

    .prologue
    .line 355
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->mModelSelectorButton:Lcom/google/android/apps/chrome/tabs/CompositorButton;

    invoke-virtual {v0, p3, p4}, Lcom/google/android/apps/chrome/tabs/CompositorButton;->onDown(FF)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 357
    :goto_0
    return-void

    .line 356
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->getActiveStripLayoutHelper()Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->onDown(JFF)V

    goto :goto_0
.end method

.method public onLongPress(JFF)V
    .locals 1

    .prologue
    .line 366
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->getActiveStripLayoutHelper()Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->onLongPress(JFF)V

    .line 367
    return-void
.end method

.method public onSizeChanged(F)V
    .locals 3

    .prologue
    .line 150
    iput p1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->mWidth:F

    .line 151
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->mModelSelectorButton:Lcom/google/android/apps/chrome/tabs/CompositorButton;

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->mWidth:F

    const/high16 v2, 0x41c00000    # 24.0f

    sub-float/2addr v1, v2

    const/high16 v2, 0x40c00000    # 6.0f

    sub-float/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/tabs/CompositorButton;->setX(F)V

    .line 154
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->mNormalHelper:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->mWidth:F

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->mHeight:F

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->onSizeChanged(FF)V

    .line 155
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->mIncognitoHelper:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->mWidth:F

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->mHeight:F

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->onSizeChanged(FF)V

    .line 156
    return-void
.end method

.method public onUpOrCancel(J)V
    .locals 3

    .prologue
    .line 388
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->mModelSelectorButton:Lcom/google/android/apps/chrome/tabs/CompositorButton;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/CompositorButton;->onUpOrCancel()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    if-eqz v0, :cond_1

    .line 389
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->isIncognitoSelected()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-interface {v1, v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->selectModel(Z)V

    .line 393
    :goto_1
    return-void

    .line 389
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 392
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->getActiveStripLayoutHelper()Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->onUpOrCancel(J)V

    goto :goto_1
.end method

.method public setTabModelSelector(Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreatorManager;Lcom/google/android/apps/chrome/compositor/TabContentManager;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 125
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    if-ne v0, p1, :cond_0

    .line 132
    :goto_0
    return-void

    .line 126
    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    .line 127
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->mNormalHelper:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    invoke-interface {v1, v2}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->getModel(Z)Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v1

    invoke-interface {p2, v2}, Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreatorManager;->getTabCreator(Z)Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;

    move-result-object v2

    invoke-virtual {v0, v1, p3, v2}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->setTabModel(Lorg/chromium/chrome/browser/tabmodel/TabModel;Lcom/google/android/apps/chrome/compositor/TabContentManager;Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;)V

    .line 129
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->mIncognitoHelper:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    invoke-interface {v1, v3}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->getModel(Z)Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v1

    invoke-interface {p2, v3}, Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreatorManager;->getTabCreator(Z)Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;

    move-result-object v2

    invoke-virtual {v0, v1, p3, v2}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->setTabModel(Lorg/chromium/chrome/browser/tabmodel/TabModel;Lcom/google/android/apps/chrome/compositor/TabContentManager;Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;)V

    .line 131
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->isIncognitoSelected()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->tabModelSwitched(Z)V

    goto :goto_0
.end method

.method public setTabStacker(Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripStacker;)V
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->mNormalHelper:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->setTabStacker(Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripStacker;)V

    .line 142
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->mIncognitoHelper:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->setTabStacker(Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripStacker;)V

    .line 143
    return-void
.end method

.method public tabClosed(JZI)V
    .locals 1

    .prologue
    .line 257
    invoke-virtual {p0, p3}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->getStripLayoutHelper(Z)Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p4}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->tabClosed(JI)V

    .line 258
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->updateModelSwitcherButton()V

    .line 259
    return-void
.end method

.method public tabClosureCancelled(JZI)V
    .locals 1

    .prologue
    .line 268
    invoke-virtual {p0, p3}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->getStripLayoutHelper(Z)Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p4}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->tabClosureCancelled(JI)V

    .line 269
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->updateModelSwitcherButton()V

    .line 270
    return-void
.end method

.method public tabCreated(JZIIZ)V
    .locals 7

    .prologue
    .line 281
    invoke-virtual {p0, p3}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->getStripLayoutHelper(Z)Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;

    move-result-object v1

    move-wide v2, p1

    move v4, p4

    move v5, p5

    move v6, p6

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->tabCreated(JIIZ)V

    .line 282
    return-void
.end method

.method public tabLoadFinished(IZ)V
    .locals 1

    .prologue
    .line 317
    invoke-virtual {p0, p2}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->getStripLayoutHelper(Z)Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->tabLoadFinished(I)V

    .line 318
    return-void
.end method

.method public tabLoadStarted(IZ)V
    .locals 1

    .prologue
    .line 308
    invoke-virtual {p0, p2}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->getStripLayoutHelper(Z)Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->tabLoadStarted(I)V

    .line 309
    return-void
.end method

.method public tabModelSwitched(Z)V
    .locals 1

    .prologue
    .line 204
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->mIsIncognito:Z

    if-ne p1, v0, :cond_0

    .line 210
    :goto_0
    return-void

    .line 205
    :cond_0
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->mIsIncognito:Z

    .line 207
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->updateModelSwitcherButton()V

    .line 209
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->mUpdateHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;->requestUpdate()V

    goto :goto_0
.end method

.method public tabMoved(JZIII)V
    .locals 7

    .prologue
    .line 246
    invoke-virtual {p0, p3}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->getStripLayoutHelper(Z)Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;

    move-result-object v1

    move-wide v2, p1

    move v4, p4

    move v5, p5

    move v6, p6

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->tabMoved(JIII)V

    .line 247
    return-void
.end method

.method public tabPageLoadFinished(IZ)V
    .locals 1

    .prologue
    .line 299
    invoke-virtual {p0, p2}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->getStripLayoutHelper(Z)Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->tabPageLoadFinished(I)V

    .line 300
    return-void
.end method

.method public tabPageLoadStarted(IZ)V
    .locals 1

    .prologue
    .line 290
    invoke-virtual {p0, p2}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->getStripLayoutHelper(Z)Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->tabPageLoadStarted(I)V

    .line 291
    return-void
.end method

.method public tabSelected(JZII)V
    .locals 1

    .prologue
    .line 234
    invoke-virtual {p0, p3}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->getStripLayoutHelper(Z)Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p4, p5}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->tabSelected(JII)V

    .line 235
    return-void
.end method

.method public updateLayout(JJ)Z
    .locals 1

    .prologue
    .line 195
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->getInactiveStripLayoutHelper()Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->finishAnimation()V

    .line 196
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->getActiveStripLayoutHelper()Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->updateLayout(JJ)Z

    move-result v0

    return v0
.end method
