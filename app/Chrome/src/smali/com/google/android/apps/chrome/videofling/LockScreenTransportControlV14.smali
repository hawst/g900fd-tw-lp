.class public Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV14;
.super Lcom/google/android/apps/chrome/videofling/LockScreenTransportControl;
.source "LockScreenTransportControlV14.java"


# static fields
.field private static sDebug:Z


# instance fields
.field private final mAudioFocusListener:Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV14$AudioFocusListener;

.field private final mAudioManager:Landroid/media/AudioManager;

.field private mIsPlaying:Z

.field private final mMediaEventReceiver:Landroid/content/ComponentName;

.field private final mMediaPendingIntent:Landroid/app/PendingIntent;

.field private mRemoteControlClient:Landroid/media/RemoteControlClient;


# direct methods
.method protected constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 46
    invoke-direct {p0}, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControl;-><init>()V

    .line 47
    invoke-static {}, Lorg/chromium/base/CommandLine;->getInstance()Lorg/chromium/base/CommandLine;

    move-result-object v0

    const-string/jumbo v1, "enable-cast-debug"

    invoke-virtual {v0, v1}, Lorg/chromium/base/CommandLine;->hasSwitch(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV14;->sDebug:Z

    .line 49
    const-string/jumbo v0, "audio"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV14;->mAudioManager:Landroid/media/AudioManager;

    .line 50
    new-instance v0, Landroid/content/ComponentName;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const-class v2, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControl$MediaButtonIntentReceiver;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV14;->mMediaEventReceiver:Landroid/content/ComponentName;

    .line 52
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.action.MEDIA_BUTTON"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 53
    iget-object v1, p0, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV14;->mMediaEventReceiver:Landroid/content/ComponentName;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 54
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v3, v0, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV14;->mMediaPendingIntent:Landroid/app/PendingIntent;

    .line 56
    new-instance v0, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV14$AudioFocusListener;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV14$AudioFocusListener;-><init>(Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV14$1;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV14;->mAudioFocusListener:Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV14$AudioFocusListener;

    .line 57
    return-void
.end method

.method private updateBitmap(Landroid/media/RemoteControlClient$MetadataEditor;)V
    .locals 3

    .prologue
    .line 142
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV14;->getPosterBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 143
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 144
    :goto_0
    const/16 v1, 0x64

    invoke-virtual {p1, v1, v0}, Landroid/media/RemoteControlClient$MetadataEditor;->putBitmap(ILandroid/graphics/Bitmap;)Landroid/media/RemoteControlClient$MetadataEditor;

    .line 145
    return-void

    .line 143
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected final getRemoteControlClient()Landroid/media/RemoteControlClient;
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV14;->mRemoteControlClient:Landroid/media/RemoteControlClient;

    return-object v0
.end method

.method protected getTransportControlFlags()I
    .locals 1

    .prologue
    .line 176
    const/16 v0, 0x28

    return v0
.end method

.method protected isPlaying()Z
    .locals 1

    .prologue
    .line 238
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV14;->mIsPlaying:Z

    return v0
.end method

.method public onDurationUpdated(I)V
    .locals 2

    .prologue
    .line 217
    new-instance v0, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV14;->getVideoInfo()Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;-><init>(Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;)V

    .line 218
    iput p1, v0, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;->durationMillis:I

    .line 219
    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV14;->setVideoInfo(Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;)V

    .line 220
    return-void
.end method

.method public onError(ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 212
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV14;->hide()V

    .line 213
    return-void
.end method

.method public onErrorChanged()V
    .locals 1

    .prologue
    .line 61
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV14;->hasError()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x9

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV14;->updatePlaybackState(I)V

    .line 62
    :cond_0
    return-void
.end method

.method public onLockScreenPlaybackStateChanged(Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 66
    sget-boolean v0, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV14;->sDebug:Z

    if-eqz v0, :cond_0

    const-string/jumbo v0, "LockScreenTransportControlV14"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "onLockScreenPlaybackStateChanged - new state: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 69
    :cond_0
    if-eqz p2, :cond_4

    .line 70
    iput-boolean v2, p0, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV14;->mIsPlaying:Z

    .line 72
    sget-object v0, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV14$1;->$SwitchMap$com$google$android$apps$chrome$videofling$RemoteVideoInfo$PlayerState:[I

    invoke-virtual {p2}, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;->ordinal()I

    move-result v3

    aget v0, v0, v3

    packed-switch v0, :pswitch_data_0

    move v0, v2

    move v3, v1

    .line 92
    :goto_0
    iget-object v4, p0, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV14;->mRemoteControlClient:Landroid/media/RemoteControlClient;

    if-eqz v4, :cond_2

    .line 93
    :goto_1
    if-eq v1, v0, :cond_1

    .line 94
    if-eqz v0, :cond_3

    .line 95
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV14;->register()V

    .line 96
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV14;->onVideoInfoChanged()V

    .line 97
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV14;->onPosterBitmapChanged()V

    .line 103
    :cond_1
    :goto_2
    invoke-virtual {p0, v3}, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV14;->updatePlaybackState(I)V

    .line 104
    return-void

    .line 74
    :pswitch_0
    const/4 v0, 0x2

    move v3, v0

    move v0, v1

    .line 75
    goto :goto_0

    .line 77
    :pswitch_1
    const/16 v0, 0x9

    move v3, v0

    move v0, v1

    .line 78
    goto :goto_0

    .line 80
    :pswitch_2
    const/4 v0, 0x3

    .line 81
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV14;->mIsPlaying:Z

    move v3, v0

    move v0, v1

    .line 82
    goto :goto_0

    .line 84
    :pswitch_3
    const/16 v0, 0x8

    move v3, v0

    move v0, v1

    .line 85
    goto :goto_0

    :cond_2
    move v1, v2

    .line 92
    goto :goto_1

    .line 99
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV14;->unregister()V

    goto :goto_2

    :cond_4
    move v0, v2

    move v3, v1

    goto :goto_0

    .line 72
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onPositionChanged(I)V
    .locals 2

    .prologue
    .line 224
    new-instance v0, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV14;->getVideoInfo()Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;-><init>(Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;)V

    .line 225
    iput p1, v0, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;->currentTimeMillis:I

    .line 226
    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV14;->setVideoInfo(Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;)V

    .line 227
    return-void
.end method

.method public onPosterBitmapChanged()V
    .locals 2

    .prologue
    .line 129
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV14;->mRemoteControlClient:Landroid/media/RemoteControlClient;

    if-nez v0, :cond_0

    .line 135
    :goto_0
    return-void

    .line 131
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV14;->mRemoteControlClient:Landroid/media/RemoteControlClient;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/media/RemoteControlClient;->editMetadata(Z)Landroid/media/RemoteControlClient$MetadataEditor;

    move-result-object v0

    .line 133
    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV14;->updateBitmap(Landroid/media/RemoteControlClient$MetadataEditor;)V

    .line 134
    invoke-virtual {v0}, Landroid/media/RemoteControlClient$MetadataEditor;->apply()V

    goto :goto_0
.end method

.method public onPrepared(Lcom/google/android/apps/chrome/videofling/MediaRouteController;)V
    .locals 0

    .prologue
    .line 203
    return-void
.end method

.method public onRouteSelected(Ljava/lang/String;Lorg/chromium/chrome/browser/Tab;ILcom/google/android/apps/chrome/videofling/MediaRouteController;)V
    .locals 0

    .prologue
    .line 192
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV14;->setScreenName(Ljava/lang/String;)V

    .line 193
    return-void
.end method

.method public onRouteUnselected(Lorg/chromium/chrome/browser/Tab;ILcom/google/android/apps/chrome/videofling/MediaRouteController;)V
    .locals 0

    .prologue
    .line 198
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV14;->hide()V

    .line 199
    return-void
.end method

.method public onSeekCompleted()V
    .locals 0

    .prologue
    .line 207
    return-void
.end method

.method public onTitleChanged(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 231
    new-instance v0, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV14;->getVideoInfo()Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;-><init>(Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;)V

    .line 232
    iput-object p1, v0, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;->title:Ljava/lang/String;

    .line 233
    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV14;->setVideoInfo(Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;)V

    .line 234
    return-void
.end method

.method public onVideoInfoChanged()V
    .locals 5

    .prologue
    .line 108
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV14;->mRemoteControlClient:Landroid/media/RemoteControlClient;

    if-nez v0, :cond_0

    .line 125
    :goto_0
    return-void

    .line 110
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV14;->getVideoInfo()Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;

    move-result-object v3

    .line 112
    const/4 v2, 0x0

    .line 113
    const-wide/16 v0, 0x0

    .line 114
    if-eqz v3, :cond_1

    .line 115
    iget-object v2, v3, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;->title:Ljava/lang/String;

    .line 116
    iget v0, v3, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;->durationMillis:I

    int-to-long v0, v0

    .line 119
    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV14;->mRemoteControlClient:Landroid/media/RemoteControlClient;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/media/RemoteControlClient;->editMetadata(Z)Landroid/media/RemoteControlClient$MetadataEditor;

    move-result-object v3

    .line 121
    const/4 v4, 0x7

    invoke-virtual {v3, v4, v2}, Landroid/media/RemoteControlClient$MetadataEditor;->putString(ILjava/lang/String;)Landroid/media/RemoteControlClient$MetadataEditor;

    .line 122
    const/16 v2, 0x9

    invoke-virtual {v3, v2, v0, v1}, Landroid/media/RemoteControlClient$MetadataEditor;->putLong(IJ)Landroid/media/RemoteControlClient$MetadataEditor;

    .line 123
    invoke-direct {p0, v3}, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV14;->updateBitmap(Landroid/media/RemoteControlClient$MetadataEditor;)V

    .line 124
    invoke-virtual {v3}, Landroid/media/RemoteControlClient$MetadataEditor;->apply()V

    goto :goto_0
.end method

.method protected register()V
    .locals 4

    .prologue
    .line 152
    sget-boolean v0, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV14;->sDebug:Z

    if-eqz v0, :cond_0

    const-string/jumbo v0, "LockScreenTransportControlV14"

    const-string/jumbo v1, "register called"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 153
    :cond_0
    new-instance v0, Landroid/media/RemoteControlClient;

    iget-object v1, p0, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV14;->mMediaPendingIntent:Landroid/app/PendingIntent;

    invoke-direct {v0, v1}, Landroid/media/RemoteControlClient;-><init>(Landroid/app/PendingIntent;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV14;->mRemoteControlClient:Landroid/media/RemoteControlClient;

    .line 154
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV14;->mAudioManager:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV14;->mAudioFocusListener:Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV14$AudioFocusListener;

    const/high16 v2, -0x80000000

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    .line 156
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV14;->mAudioManager:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV14;->mMediaEventReceiver:Landroid/content/ComponentName;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->registerMediaButtonEventReceiver(Landroid/content/ComponentName;)V

    .line 157
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV14;->mAudioManager:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV14;->mRemoteControlClient:Landroid/media/RemoteControlClient;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->registerRemoteControlClient(Landroid/media/RemoteControlClient;)V

    .line 158
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV14;->mRemoteControlClient:Landroid/media/RemoteControlClient;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV14;->getTransportControlFlags()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/media/RemoteControlClient;->setTransportControlFlags(I)V

    .line 159
    return-void
.end method

.method protected unregister()V
    .locals 2

    .prologue
    .line 162
    sget-boolean v0, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV14;->sDebug:Z

    if-eqz v0, :cond_0

    const-string/jumbo v0, "LockScreenTransportControlV14"

    const-string/jumbo v1, "unregister called"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 163
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV14;->mRemoteControlClient:Landroid/media/RemoteControlClient;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/media/RemoteControlClient;->editMetadata(Z)Landroid/media/RemoteControlClient$MetadataEditor;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/RemoteControlClient$MetadataEditor;->apply()V

    .line 164
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV14;->mRemoteControlClient:Landroid/media/RemoteControlClient;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/media/RemoteControlClient;->setTransportControlFlags(I)V

    .line 165
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV14;->mAudioManager:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV14;->mAudioFocusListener:Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV14$AudioFocusListener;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 166
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV14;->mAudioManager:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV14;->mMediaEventReceiver:Landroid/content/ComponentName;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->unregisterMediaButtonEventReceiver(Landroid/content/ComponentName;)V

    .line 167
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV14;->mAudioManager:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV14;->mRemoteControlClient:Landroid/media/RemoteControlClient;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->unregisterRemoteControlClient(Landroid/media/RemoteControlClient;)V

    .line 168
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV14;->mRemoteControlClient:Landroid/media/RemoteControlClient;

    .line 169
    return-void
.end method

.method protected updatePlaybackState(I)V
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV14;->mRemoteControlClient:Landroid/media/RemoteControlClient;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV14;->mRemoteControlClient:Landroid/media/RemoteControlClient;

    invoke-virtual {v0, p1}, Landroid/media/RemoteControlClient;->setPlaybackState(I)V

    .line 173
    :cond_0
    return-void
.end method
