.class Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$SyncPromoGroup;
.super Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$Group;
.source "RecentTabsRowAdapter.java"


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;)V
    .locals 0

    .prologue
    .line 626
    iput-object p1, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$SyncPromoGroup;->this$0:Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$Group;-><init>(Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;)V

    return-void
.end method


# virtual methods
.method public configureChildView(ILcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$ViewHolder;)V
    .locals 0

    .prologue
    .line 662
    return-void
.end method

.method public configureGroupView(Lcom/google/android/apps/chrome/ntp/RecentTabsGroupView;Z)V
    .locals 0

    .prologue
    .line 666
    invoke-virtual {p1, p2}, Lcom/google/android/apps/chrome/ntp/RecentTabsGroupView;->configureForSyncPromo(Z)V

    .line 667
    return-void
.end method

.method public getChild(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 644
    const/4 v0, 0x0

    return-object v0
.end method

.method public getChildType()Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$ChildType;
    .locals 1

    .prologue
    .line 634
    sget-object v0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$ChildType;->SYNC_PROMO:Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$ChildType;

    return-object v0
.end method

.method getChildView(IZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 650
    if-nez p3, :cond_0

    .line 651
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$SyncPromoGroup;->this$0:Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;

    # getter for: Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->access$000(Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;)Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 652
    sget v1, Lcom/google/android/apps/chrome/R$layout;->recent_tabs_promo_view_controller:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p4, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/ntp/SyncPromoView;

    .line 654
    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$SyncPromoGroup;->this$0:Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;

    # getter for: Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->mRecentTabsManager:Lcom/google/android/apps/chrome/ntp/RecentTabsManager;
    invoke-static {v1}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->access$300(Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;)Lcom/google/android/apps/chrome/ntp/RecentTabsManager;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$SyncPromoGroup;->this$0:Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;

    # getter for: Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->mActivity:Landroid/app/Activity;
    invoke-static {v2}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->access$000(Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;)Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/ntp/SyncPromoView;->initialize(Lcom/google/android/apps/chrome/ntp/SyncPromoView$SyncPromoModel;Landroid/app/Activity;)V

    .line 657
    :goto_0
    return-object v0

    :cond_0
    move-object v0, p3

    goto :goto_0
.end method

.method public getChildrenCount()I
    .locals 1

    .prologue
    .line 639
    const/4 v0, 0x1

    return v0
.end method

.method public getGroupType()Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$GroupType;
    .locals 1

    .prologue
    .line 629
    sget-object v0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$GroupType;->CONTENT:Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$GroupType;

    return-object v0
.end method

.method public isCollapsed()Z
    .locals 1

    .prologue
    .line 676
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$SyncPromoGroup;->this$0:Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;

    # getter for: Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->mRecentTabsManager:Lcom/google/android/apps/chrome/ntp/RecentTabsManager;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->access$300(Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;)Lcom/google/android/apps/chrome/ntp/RecentTabsManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->isSyncPromoCollapsed()Z

    move-result v0

    return v0
.end method

.method public onChildClick(I)Z
    .locals 1

    .prologue
    .line 681
    const/4 v0, 0x0

    return v0
.end method

.method public setCollapsed(Z)V
    .locals 1

    .prologue
    .line 671
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$SyncPromoGroup;->this$0:Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;

    # getter for: Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->mRecentTabsManager:Lcom/google/android/apps/chrome/ntp/RecentTabsManager;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->access$300(Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;)Lcom/google/android/apps/chrome/ntp/RecentTabsManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->setSyncPromoCollapsed(Z)V

    .line 672
    return-void
.end method
