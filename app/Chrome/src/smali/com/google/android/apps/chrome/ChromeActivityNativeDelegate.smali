.class public interface abstract Lcom/google/android/apps/chrome/ChromeActivityNativeDelegate;
.super Ljava/lang/Object;
.source "ChromeActivityNativeDelegate.java"


# virtual methods
.method public abstract hasDoneFirstDraw()Z
.end method

.method public abstract isActivityDestroyed()Z
.end method

.method public abstract onActivityResultWithNative(IILandroid/content/Intent;)Z
.end method

.method public abstract onCreateWithNative()V
.end method

.method public abstract onNewIntentWithNative(Landroid/content/Intent;)V
.end method

.method public abstract onPauseWithNative()V
.end method

.method public abstract onResumeWithNative()V
.end method

.method public abstract onStartWithNative()V
.end method

.method public abstract onStopWithNative()V
.end method
