.class Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager$5;
.super Ljava/lang/Object;
.source "HomeActivityRecentTabsManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;

.field final synthetic val$currentTabId:I

.field final synthetic val$startNewDocument:Ljava/lang/Runnable;

.field final synthetic val$tabId:I


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;IILjava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 191
    iput-object p1, p0, Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager$5;->this$0:Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;

    iput p2, p0, Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager$5;->val$currentTabId:I

    iput p3, p0, Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager$5;->val$tabId:I

    iput-object p4, p0, Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager$5;->val$startNewDocument:Ljava/lang/Runnable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 194
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager$5;->this$0:Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;

    # getter for: Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;->mDialog:Landroid/app/Dialog;
    invoke-static {v0}, Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;->access$400(Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;)Landroid/app/Dialog;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager$5;->this$0:Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;

    # getter for: Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;->mDialog:Landroid/app/Dialog;
    invoke-static {v0}, Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;->access$400(Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;)Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 195
    :cond_0
    iget v0, p0, Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager$5;->val$currentTabId:I

    iget v1, p0, Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager$5;->val$tabId:I

    if-eq v0, v1, :cond_1

    .line 196
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager$5;->val$startNewDocument:Ljava/lang/Runnable;

    const-wide/16 v2, 0x96

    invoke-static {v0, v2, v3}, Lorg/chromium/base/ThreadUtils;->postOnUiThreadDelayed(Ljava/lang/Runnable;J)V

    .line 198
    :cond_1
    return-void
.end method
