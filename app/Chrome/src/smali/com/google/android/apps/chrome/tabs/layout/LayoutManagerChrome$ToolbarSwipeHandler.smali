.class public Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome$ToolbarSwipeHandler;
.super Lcom/google/android/apps/chrome/tabs/layout/EdgeSwipeHandlerLayoutDelegate;
.source "LayoutManagerChrome.java"


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;Lcom/google/android/apps/chrome/tabs/layout/LayoutProvider;)V
    .locals 0

    .prologue
    .line 662
    iput-object p1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome$ToolbarSwipeHandler;->this$0:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;

    .line 663
    invoke-direct {p0, p2}, Lcom/google/android/apps/chrome/tabs/layout/EdgeSwipeHandlerLayoutDelegate;-><init>(Lcom/google/android/apps/chrome/tabs/layout/LayoutProvider;)V

    .line 664
    return-void
.end method


# virtual methods
.method public isSwipeEnabled(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 679
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome$ToolbarSwipeHandler;->this$0:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;

    iget-object v1, v1, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->mHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;

    invoke-interface {v1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;->getFullscreenManager()Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;

    move-result-object v1

    .line 680
    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome$ToolbarSwipeHandler;->this$0:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome$ToolbarSwipeHandler;->this$0:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;

    iget-object v3, v3, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->mStaticLayout:Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;

    if-ne v2, v3, :cond_0

    invoke-static {}, Lcom/google/android/apps/chrome/device/DeviceClassManager;->enableToolbarSwipe()Z

    move-result v2

    if-eqz v2, :cond_0

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->getPersistentFullscreenMode()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 688
    :cond_0
    :goto_0
    return v0

    .line 686
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome$ToolbarSwipeHandler;->this$0:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;

    iget-object v1, v1, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->mHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;

    invoke-interface {v1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/chrome/device/DeviceClassManager;->isAccessibilityModeEnabled(Landroid/content/Context;)Z

    move-result v1

    .line 688
    sget-object v2, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;->LEFT:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    if-eq p1, v2, :cond_2

    sget-object v2, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;->RIGHT:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    if-eq p1, v2, :cond_2

    sget-object v2, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;->DOWN:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    if-ne p1, v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome$ToolbarSwipeHandler;->this$0:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;

    iget-object v2, v2, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->mStackLayout:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;

    if-eqz v2, :cond_0

    if-nez v1, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public swipeStarted(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;FF)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 668
    sget-object v0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;->DOWN:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    if-ne p1, v0, :cond_1

    .line 669
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome$ToolbarSwipeHandler;->this$0:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome$ToolbarSwipeHandler;->this$0:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;

    iget-object v1, v1, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->mStackLayout:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->startShowing(Lcom/google/android/apps/chrome/tabs/layout/Layout;Z)V

    .line 670
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/chrome/tabs/layout/EdgeSwipeHandlerLayoutDelegate;->swipeStarted(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;FF)V

    .line 675
    :cond_0
    :goto_0
    return-void

    .line 671
    :cond_1
    sget-object v0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;->LEFT:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    if-eq p1, v0, :cond_2

    sget-object v0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;->RIGHT:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    if-ne p1, v0, :cond_0

    .line 672
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome$ToolbarSwipeHandler;->this$0:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome$ToolbarSwipeHandler;->this$0:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;

    iget-object v1, v1, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->mToolbarSwipeLayout:Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->startShowing(Lcom/google/android/apps/chrome/tabs/layout/Layout;Z)V

    .line 673
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/chrome/tabs/layout/EdgeSwipeHandlerLayoutDelegate;->swipeStarted(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;FF)V

    goto :goto_0
.end method
