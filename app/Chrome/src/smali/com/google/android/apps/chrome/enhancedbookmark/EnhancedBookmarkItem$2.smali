.class Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem$2;
.super Ljava/lang/Object;
.source "EnhancedBookmarkItem.java"

# interfaces
.implements Lcom/google/android/apps/chrome/enhancedbookmark/ChromeEnhancedBookmarkBridge$SalientImageCallback;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;)V
    .locals 0

    .prologue
    .line 256
    iput-object p1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem$2;->this$0:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSalientImageReady(Landroid/graphics/Bitmap;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 260
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem$2;->this$0:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;

    # getter for: Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;->mSalientImageCallback:Lcom/google/android/apps/chrome/enhancedbookmark/ChromeEnhancedBookmarkBridge$SalientImageCallback;
    invoke-static {v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;->access$300(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;)Lcom/google/android/apps/chrome/enhancedbookmark/ChromeEnhancedBookmarkBridge$SalientImageCallback;

    move-result-object v0

    if-eq p0, v0, :cond_1

    .line 295
    :cond_0
    :goto_0
    return-void

    .line 262
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem$2;->this$0:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;->mSalientImageCallback:Lcom/google/android/apps/chrome/enhancedbookmark/ChromeEnhancedBookmarkBridge$SalientImageCallback;
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;->access$302(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;Lcom/google/android/apps/chrome/enhancedbookmark/ChromeEnhancedBookmarkBridge$SalientImageCallback;)Lcom/google/android/apps/chrome/enhancedbookmark/ChromeEnhancedBookmarkBridge$SalientImageCallback;

    .line 263
    if-eqz p1, :cond_0

    .line 265
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem$2;->this$0:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;

    # getter for: Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;->mBackgroundSalientImage:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;->access$400(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;)Landroid/widget/ImageView;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem$RoundedTopCornerDrawable;

    iget-object v2, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem$2;->this$0:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;

    invoke-direct {v1, v2, p1}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem$RoundedTopCornerDrawable;-><init>(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;Landroid/graphics/Bitmap;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 266
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem$2;->this$0:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;

    # getter for: Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;->mBackgroundSalientImage:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;->access$400(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 268
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem$2;->this$0:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;

    # getter for: Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;->mBackgroundSolidColor:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;->access$500(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ImageView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 271
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem$2;->this$0:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;

    # getter for: Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;->mBackgroundSalientImage:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;->access$400(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 273
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/4 v1, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 274
    sget-object v1, Lorg/chromium/ui/interpolators/BakedBezierInterpolator;->FADE_IN_CURVE:Lorg/chromium/ui/interpolators/BakedBezierInterpolator;

    invoke-virtual {v0, v1}, Landroid/view/animation/AlphaAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 275
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/animation/AlphaAnimation;->setFillAfter(Z)V

    .line 276
    const-wide/16 v2, 0x190

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 277
    new-instance v1, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem$2$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem$2$1;-><init>(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem$2;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/AlphaAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 293
    iget-object v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem$2;->this$0:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;

    # getter for: Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;->mBackgroundSalientImage:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;->access$400(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;)Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method
