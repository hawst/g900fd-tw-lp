.class Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment$PassphraseTypeUiElementContainer;
.super Ljava/lang/Object;
.source "PassphraseTypeDialogFragment.java"


# instance fields
.field private final mElements:Ljava/util/ArrayList;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment$PassphraseTypeUiElementContainer;->mElements:Ljava/util/ArrayList;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment$1;)V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment$PassphraseTypeUiElementContainer;-><init>()V

    return-void
.end method


# virtual methods
.method public add(Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment$PassphraseTypeUiElement;)V
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment$PassphraseTypeUiElementContainer;->mElements:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    # setter for: Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment$PassphraseTypeUiElement;->mPosition:I
    invoke-static {p1, v0}, Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment$PassphraseTypeUiElement;->access$002(Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment$PassphraseTypeUiElement;I)I

    .line 65
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment$PassphraseTypeUiElementContainer;->mElements:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 66
    return-void
.end method

.method public getDisplayNames()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment$PassphraseTypeUiElementContainer;->mElements:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v2, v0, [Ljava/lang/String;

    .line 74
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment$PassphraseTypeUiElementContainer;->mElements:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 75
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment$PassphraseTypeUiElementContainer;->mElements:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment$PassphraseTypeUiElement;

    # getter for: Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment$PassphraseTypeUiElement;->mDisplayName:Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment$PassphraseTypeUiElement;->access$100(Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment$PassphraseTypeUiElement;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v1

    .line 74
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 77
    :cond_0
    return-object v2
.end method

.method public getElements()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment$PassphraseTypeUiElementContainer;->mElements:Ljava/util/ArrayList;

    return-object v0
.end method
