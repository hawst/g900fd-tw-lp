.class final Lcom/google/android/apps/chrome/ContextualMenuBar$1;
.super Landroid/util/Property;
.source "ContextualMenuBar.java"


# direct methods
.method constructor <init>(Ljava/lang/Class;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0, p1, p2}, Landroid/util/Property;-><init>(Ljava/lang/Class;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final get(Lcom/google/android/apps/chrome/ContextualMenuBar$ActionBarDelegate;)Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 36
    invoke-interface {p1}, Lcom/google/android/apps/chrome/ContextualMenuBar$ActionBarDelegate;->getControlTopMargin()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 33
    check-cast p1, Lcom/google/android/apps/chrome/ContextualMenuBar$ActionBarDelegate;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/ContextualMenuBar$1;->get(Lcom/google/android/apps/chrome/ContextualMenuBar$ActionBarDelegate;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final set(Lcom/google/android/apps/chrome/ContextualMenuBar$ActionBarDelegate;Ljava/lang/Integer;)V
    .locals 1

    .prologue
    .line 40
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {p1, v0}, Lcom/google/android/apps/chrome/ContextualMenuBar$ActionBarDelegate;->setControlTopMargin(I)V

    .line 41
    return-void
.end method

.method public final bridge synthetic set(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 33
    check-cast p1, Lcom/google/android/apps/chrome/ContextualMenuBar$ActionBarDelegate;

    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/chrome/ContextualMenuBar$1;->set(Lcom/google/android/apps/chrome/ContextualMenuBar$ActionBarDelegate;Ljava/lang/Integer;)V

    return-void
.end method
