.class Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager$3;
.super Ljava/lang/Object;
.source "DelegatedPKCS11Manager.java"

# interfaces
.implements Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Observer;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;)V
    .locals 0

    .prologue
    .line 193
    iput-object p1, p0, Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager$3;->this$0:Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSettingsAvailable(Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;)V
    .locals 2

    .prologue
    .line 196
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->getSmartCardAuthenticationEnabled()Z

    move-result v0

    .line 197
    iget-object v1, p0, Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager$3;->this$0:Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;

    # getter for: Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;->mIsEnabled:Z
    invoke-static {v1}, Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;->access$700(Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;)Z

    move-result v1

    if-nez v1, :cond_1

    if-eqz v0, :cond_1

    .line 198
    iget-object v0, p0, Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager$3;->this$0:Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;->mIsEnabled:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;->access$702(Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;Z)Z

    .line 199
    iget-object v0, p0, Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager$3;->this$0:Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;

    # invokes: Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;->bindToService()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;->access$800(Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;)V

    .line 204
    :cond_0
    :goto_0
    return-void

    .line 200
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager$3;->this$0:Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;

    # getter for: Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;->mIsEnabled:Z
    invoke-static {v1}, Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;->access$700(Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;)Z

    move-result v1

    if-eqz v1, :cond_0

    if-nez v0, :cond_0

    .line 201
    iget-object v0, p0, Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager$3;->this$0:Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;->mIsEnabled:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;->access$702(Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;Z)Z

    .line 202
    iget-object v0, p0, Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager$3;->this$0:Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;

    # invokes: Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;->disconnectFromRemote()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;->access$000(Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;)V

    goto :goto_0
.end method
