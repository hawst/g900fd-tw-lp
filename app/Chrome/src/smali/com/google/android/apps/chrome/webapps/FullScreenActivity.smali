.class public abstract Lcom/google/android/apps/chrome/webapps/FullScreenActivity;
.super Lcom/google/android/apps/chrome/ChromeActivity;
.source "FullScreenActivity.java"


# instance fields
.field private mContentViewRenderView:Lorg/chromium/content/browser/ContentViewRenderView;

.field protected mPageLayout:Landroid/widget/FrameLayout;

.field private mTab:Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab;

.field private mWindowAndroid:Lorg/chromium/ui/base/ActivityWindowAndroid;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/google/android/apps/chrome/ChromeActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected createPageLayout()Landroid/widget/FrameLayout;
    .locals 1

    .prologue
    .line 132
    new-instance v0, Landroid/widget/FrameLayout;

    invoke-direct {v0, p0}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method protected createUI(Landroid/widget/FrameLayout;)V
    .locals 0

    .prologue
    .line 125
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/webapps/FullScreenActivity;->setContentView(Landroid/view/View;)V

    .line 126
    return-void
.end method

.method public finishNativeInitialization()V
    .locals 2

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/FullScreenActivity;->mPageLayout:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/google/android/apps/chrome/webapps/FullScreenActivity;->mContentViewRenderView:Lorg/chromium/content/browser/ContentViewRenderView;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 79
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/FullScreenActivity;->mSavedInstanceState:Landroid/os/Bundle;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/webapps/FullScreenActivity;->showNewTab(Landroid/os/Bundle;)V

    .line 80
    invoke-static {p0}, Lcom/google/android/apps/chrome/ChromeBrowserInitializer;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/ChromeBrowserInitializer;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/ChromeBrowserInitializer;->onDeferredStartup(Z)V

    .line 81
    invoke-super {p0}, Lcom/google/android/apps/chrome/ChromeActivity;->finishNativeInitialization()V

    .line 82
    return-void
.end method

.method protected getActivityDirectory()Ljava/io/File;
    .locals 2

    .prologue
    .line 139
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    .line 140
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/chrome/webapps/FullScreenActivity;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method protected final getLoggingTag()Ljava/lang/String;
    .locals 1

    .prologue
    .line 147
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected final getTab()Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab;
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/FullScreenActivity;->mTab:Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab;

    return-object v0
.end method

.method public getTabModelSelector()Lcom/google/android/apps/chrome/tabmodel/SingleTabModelSelector;
    .locals 1

    .prologue
    .line 86
    invoke-super {p0}, Lcom/google/android/apps/chrome/ChromeActivity;->getTabModelSelector()Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/tabmodel/SingleTabModelSelector;

    return-object v0
.end method

.method public bridge synthetic getTabModelSelector()Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;
    .locals 1

    .prologue
    .line 36
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/webapps/FullScreenActivity;->getTabModelSelector()Lcom/google/android/apps/chrome/tabmodel/SingleTabModelSelector;

    move-result-object v0

    return-object v0
.end method

.method public initializeCompositor()V
    .locals 2

    .prologue
    .line 64
    new-instance v0, Lorg/chromium/ui/base/ActivityWindowAndroid;

    invoke-direct {v0, p0}, Lorg/chromium/ui/base/ActivityWindowAndroid;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/webapps/FullScreenActivity;->mWindowAndroid:Lorg/chromium/ui/base/ActivityWindowAndroid;

    .line 65
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/FullScreenActivity;->mWindowAndroid:Lorg/chromium/ui/base/ActivityWindowAndroid;

    iget-object v1, p0, Lcom/google/android/apps/chrome/webapps/FullScreenActivity;->mSavedInstanceState:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Lorg/chromium/ui/base/ActivityWindowAndroid;->restoreInstanceState(Landroid/os/Bundle;)V

    .line 66
    new-instance v0, Lorg/chromium/content/browser/ContentViewRenderView;

    invoke-direct {v0, p0}, Lorg/chromium/content/browser/ContentViewRenderView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/webapps/FullScreenActivity;->mContentViewRenderView:Lorg/chromium/content/browser/ContentViewRenderView;

    .line 67
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/FullScreenActivity;->mContentViewRenderView:Lorg/chromium/content/browser/ContentViewRenderView;

    iget-object v1, p0, Lcom/google/android/apps/chrome/webapps/FullScreenActivity;->mWindowAndroid:Lorg/chromium/ui/base/ActivityWindowAndroid;

    invoke-virtual {v0, v1}, Lorg/chromium/content/browser/ContentViewRenderView;->onNativeLibraryLoaded(Lorg/chromium/ui/base/WindowAndroid;)V

    .line 68
    return-void
.end method

.method protected final loadUrl(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 186
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/webapps/FullScreenActivity;->getTab()Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab;->loadUrl(Ljava/lang/String;)V

    .line 187
    return-void
.end method

.method public onActivityResultWithNative(IILandroid/content/Intent;)Z
    .locals 1

    .prologue
    .line 46
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/chrome/ChromeActivity;->onActivityResultWithNative(IILandroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 47
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/FullScreenActivity;->mWindowAndroid:Lorg/chromium/ui/base/ActivityWindowAndroid;

    invoke-virtual {v0, p1, p2, p3}, Lorg/chromium/ui/base/ActivityWindowAndroid;->onActivityResult(IILandroid/content/Intent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 103
    invoke-static {}, Lorg/chromium/content/browser/ContentVideoView;->getContentVideoView()Lorg/chromium/content/browser/ContentVideoView;

    move-result-object v0

    .line 104
    if-eqz v0, :cond_0

    .line 105
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lorg/chromium/content/browser/ContentVideoView;->exitFullscreen(Z)V

    .line 115
    :goto_0
    return-void

    .line 109
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/webapps/FullScreenActivity;->getTab()Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/webapps/FullScreenActivity;->getTab()Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab;->canGoBack()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 110
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/webapps/FullScreenActivity;->getTab()Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab;->goBack()V

    goto :goto_0

    .line 114
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/webapps/FullScreenActivity;->finish()V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 97
    invoke-super {p0}, Lcom/google/android/apps/chrome/ChromeActivity;->onDestroy()V

    .line 98
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/FullScreenActivity;->mTab:Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/FullScreenActivity;->mTab:Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab;->destroy()V

    .line 99
    :cond_0
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 52
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/ChromeActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 53
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/webapps/FullScreenActivity;->setIntent(Landroid/content/Intent;)V

    .line 54
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 91
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/ChromeActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 92
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/FullScreenActivity;->mWindowAndroid:Lorg/chromium/ui/base/ActivityWindowAndroid;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/FullScreenActivity;->mWindowAndroid:Lorg/chromium/ui/base/ActivityWindowAndroid;

    invoke-virtual {v0, p1}, Lorg/chromium/ui/base/ActivityWindowAndroid;->saveInstanceState(Landroid/os/Bundle;)V

    .line 93
    :cond_0
    return-void
.end method

.method public preInflationStartup()V
    .locals 3

    .prologue
    .line 58
    invoke-super {p0}, Lcom/google/android/apps/chrome/ChromeActivity;->preInflationStartup()V

    .line 59
    new-instance v0, Lcom/google/android/apps/chrome/tabmodel/SingleTabModelSelector;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/apps/chrome/tabmodel/SingleTabModelSelector;-><init>(Landroid/app/Activity;ZZ)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/webapps/FullScreenActivity;->setTabModelSelector(Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;)V

    .line 60
    return-void
.end method

.method protected setContentView()V
    .locals 1

    .prologue
    .line 72
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/webapps/FullScreenActivity;->createPageLayout()Landroid/widget/FrameLayout;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/webapps/FullScreenActivity;->mPageLayout:Landroid/widget/FrameLayout;

    .line 73
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/FullScreenActivity;->mPageLayout:Landroid/widget/FrameLayout;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/webapps/FullScreenActivity;->createUI(Landroid/widget/FrameLayout;)V

    .line 74
    return-void
.end method

.method protected final setOverlayVideoMode(Z)V
    .locals 1

    .prologue
    .line 194
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/FullScreenActivity;->mContentViewRenderView:Lorg/chromium/content/browser/ContentViewRenderView;

    invoke-virtual {v0, p1}, Lorg/chromium/content/browser/ContentViewRenderView;->setOverlayVideoMode(Z)V

    .line 195
    return-void
.end method

.method protected showNewTab(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 156
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/FullScreenActivity;->mTab:Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/FullScreenActivity;->mTab:Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab;->getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 157
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/FullScreenActivity;->mTab:Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab;->getView()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/chrome/webapps/FullScreenActivity;->updateTabViewOnLayout(Landroid/view/View;Z)V

    .line 160
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/FullScreenActivity;->mWindowAndroid:Lorg/chromium/ui/base/ActivityWindowAndroid;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/webapps/FullScreenActivity;->getActivityDirectory()Ljava/io/File;

    move-result-object v1

    invoke-static {p0, v0, v1, p1}, Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab;->create(Lcom/google/android/apps/chrome/ChromeActivity;Lorg/chromium/ui/base/WindowAndroid;Ljava/io/File;Landroid/os/Bundle;)Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/webapps/FullScreenActivity;->mTab:Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab;

    .line 162
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/webapps/FullScreenActivity;->getTabModelSelector()Lcom/google/android/apps/chrome/tabmodel/SingleTabModelSelector;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/webapps/FullScreenActivity;->mTab:Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/tabmodel/SingleTabModelSelector;->setTab(Lorg/chromium/chrome/browser/Tab;)V

    .line 163
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/FullScreenActivity;->mTab:Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab;

    sget-object v1, Lorg/chromium/chrome/browser/tabmodel/TabModel$TabSelectionType;->FROM_NEW:Lorg/chromium/chrome/browser/tabmodel/TabModel$TabSelectionType;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab;->show(Lorg/chromium/chrome/browser/tabmodel/TabModel$TabSelectionType;)V

    .line 164
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/FullScreenActivity;->mTab:Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab;->getView()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/chrome/webapps/FullScreenActivity;->updateTabViewOnLayout(Landroid/view/View;Z)V

    .line 165
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/FullScreenActivity;->mContentViewRenderView:Lorg/chromium/content/browser/ContentViewRenderView;

    iget-object v1, p0, Lcom/google/android/apps/chrome/webapps/FullScreenActivity;->mTab:Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab;->getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/chromium/content/browser/ContentViewRenderView;->setCurrentContentViewCore(Lorg/chromium/content/browser/ContentViewCore;)V

    .line 166
    return-void
.end method

.method protected updateTabViewOnLayout(Landroid/view/View;Z)V
    .locals 1

    .prologue
    .line 174
    if-eqz p2, :cond_0

    .line 175
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/FullScreenActivity;->mPageLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v0, p1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 179
    :goto_0
    return-void

    .line 177
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/FullScreenActivity;->mPageLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v0, p1}, Landroid/widget/FrameLayout;->removeView(Landroid/view/View;)V

    goto :goto_0
.end method
