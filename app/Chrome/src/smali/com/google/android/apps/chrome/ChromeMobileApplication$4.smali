.class Lcom/google/android/apps/chrome/ChromeMobileApplication$4;
.super Ljava/lang/Object;
.source "ChromeMobileApplication.java"

# interfaces
.implements Lorg/chromium/base/ApplicationStatus$ActivityStateListener;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/ChromeMobileApplication;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/ChromeMobileApplication;)V
    .locals 0

    .prologue
    .line 668
    iput-object p1, p0, Lcom/google/android/apps/chrome/ChromeMobileApplication$4;->this$0:Lcom/google/android/apps/chrome/ChromeMobileApplication;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onActivityStateChange(Landroid/app/Activity;I)V
    .locals 1

    .prologue
    .line 671
    const/4 v0, 0x1

    if-ne p2, v0, :cond_1

    .line 672
    invoke-static {p1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->enable(Landroid/content/Context;)V

    .line 676
    :cond_0
    :goto_0
    return-void

    .line 673
    :cond_1
    const/4 v0, 0x6

    if-ne p2, v0, :cond_0

    .line 674
    invoke-static {p1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->disable(Landroid/content/Context;)V

    goto :goto_0
.end method
