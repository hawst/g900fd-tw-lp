.class public Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;
.super Lcom/google/android/apps/chrome/eventfilter/EventFilter;
.source "EdgeSwipeEventFilter.java"


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final TAN_SIDE_SWIPE_ANGLE_THRESHOLD:D


# instance fields
.field private final mAccumulateThreshold:F

.field private final mAccumulatedEvents:Ljava/util/ArrayList;

.field private mAccumulatingEvents:Z

.field private final mContext:Landroid/content/Context;

.field private final mEdgeSwipeHandler:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;

.field private mEdgeSwipeStarted:Z

.field private mEnableTabSwiping:Z

.field private final mGestureDetector:Landroid/view/GestureDetector;

.field private final mGutterDistance:F

.field private mInDoubleTap:Z

.field private mInLongPress:Z

.field private mPropagateEventsToHostView:Z

.field private mScrollDirection:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

.field private mScrollStarted:Z

.field private final mSwipeRegion:F

.field private final mSwipeTimeConstant:D

.field private mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 24
    const-class v0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->$assertionsDisabled:Z

    .line 29
    const-wide v0, 0x4046800000000000L    # 45.0

    invoke-static {v0, v1}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Math;->tan(D)D

    move-result-wide v0

    sput-wide v0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->TAN_SIDE_SWIPE_ANGLE_THRESHOLD:D

    return-void

    .line 24
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/chrome/eventfilter/EventFilterHost;Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 73
    invoke-direct {p0, p2, v0}, Lcom/google/android/apps/chrome/eventfilter/EventFilter;-><init>(Lcom/google/android/apps/chrome/eventfilter/EventFilterHost;Z)V

    .line 54
    iput-boolean v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mInLongPress:Z

    .line 55
    iput-boolean v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mInDoubleTap:Z

    .line 60
    iput-boolean v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mAccumulatingEvents:Z

    .line 61
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mAccumulatedEvents:Ljava/util/ArrayList;

    .line 74
    iput-object p1, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mContext:Landroid/content/Context;

    .line 75
    iput-boolean v3, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mEnableTabSwiping:Z

    .line 76
    sget-object v0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;->UNKNOWN:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    iput-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mScrollDirection:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    .line 78
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 79
    sget v1, Lcom/google/android/apps/chrome/R$dimen;->gutter_distance:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    iget v2, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mPxToDp:F

    mul-float/2addr v1, v2

    iput v1, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mGutterDistance:F

    .line 80
    sget v1, Lcom/google/android/apps/chrome/R$dimen;->swipe_region:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    iget v2, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mPxToDp:F

    mul-float/2addr v1, v2

    iput v1, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mSwipeRegion:F

    .line 81
    sget v1, Lcom/google/android/apps/chrome/R$dimen;->side_swipe_accumulate_threshold:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    iget v2, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mPxToDp:F

    mul-float/2addr v1, v2

    iput v1, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mAccumulateThreshold:F

    .line 83
    sget v1, Lcom/google/android/apps/chrome/R$dimen;->side_swipe_constant:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-double v0, v0

    iput-wide v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mSwipeTimeConstant:D

    .line 85
    new-instance v0, Landroid/view/GestureDetector;

    new-instance v1, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ViewScrollerGestureDetector;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ViewScrollerGestureDetector;-><init>(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$1;)V

    invoke-direct {v0, p1, v1}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mGestureDetector:Landroid/view/GestureDetector;

    .line 86
    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mGestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v0, v3}, Landroid/view/GestureDetector;->setIsLongpressEnabled(Z)V

    .line 88
    iput-object p3, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mEdgeSwipeHandler:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;

    .line 89
    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;)Z
    .locals 1

    .prologue
    .line 24
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mInDoubleTap:Z

    return v0
.end method

.method static synthetic access$1000(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;)F
    .locals 1

    .prologue
    .line 24
    iget v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mSwipeRegion:F

    return v0
.end method

.method static synthetic access$102(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;Z)Z
    .locals 0

    .prologue
    .line 24
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mInDoubleTap:Z

    return p1
.end method

.method static synthetic access$1100(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;Landroid/view/MotionEvent;Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->checkForFastScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$1200()D
    .locals 2

    .prologue
    .line 24
    sget-wide v0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->TAN_SIDE_SWIPE_ANGLE_THRESHOLD:D

    return-wide v0
.end method

.method static synthetic access$1302(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;Z)Z
    .locals 0

    .prologue
    .line 24
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mPropagateEventsToHostView:Z

    return p1
.end method

.method static synthetic access$1400(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;FF)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->scrollStarted(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;FF)V

    return-void
.end method

.method static synthetic access$1500(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;FFFFFF)V
    .locals 0

    .prologue
    .line 24
    invoke-direct/range {p0 .. p6}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->flingOccurred(FFFFFF)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mAccumulatedEvents:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;)Z
    .locals 1

    .prologue
    .line 24
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mScrollStarted:Z

    return v0
.end method

.method static synthetic access$302(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;Z)Z
    .locals 0

    .prologue
    .line 24
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mScrollStarted:Z

    return p1
.end method

.method static synthetic access$400(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;)Z
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->propagateAccumulatedEventsAndClear()Z

    move-result v0

    return v0
.end method

.method static synthetic access$502(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;Z)Z
    .locals 0

    .prologue
    .line 24
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mInLongPress:Z

    return p1
.end method

.method static synthetic access$600(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;FFFFFF)V
    .locals 0

    .prologue
    .line 24
    invoke-direct/range {p0 .. p6}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->scrollUpdated(FFFFFF)V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;)Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mScrollDirection:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    return-object v0
.end method

.method static synthetic access$702(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;)Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;
    .locals 0

    .prologue
    .line 24
    iput-object p1, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mScrollDirection:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    return-object p1
.end method

.method static synthetic access$800(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;)Z
    .locals 1

    .prologue
    .line 24
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mAccumulatingEvents:Z

    return v0
.end method

.method static synthetic access$802(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;Z)Z
    .locals 0

    .prologue
    .line 24
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mAccumulatingEvents:Z

    return p1
.end method

.method static synthetic access$900(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;)F
    .locals 1

    .prologue
    .line 24
    iget v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mAccumulateThreshold:F

    return v0
.end method

.method private calculateBiasedPosition(FFF)F
    .locals 8

    .prologue
    .line 217
    sget-boolean v0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    const/4 v0, 0x0

    cmpl-float v0, p3, v0

    if-gtz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 218
    :cond_0
    sub-float v0, p2, p1

    iget v1, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mPxToDp:F

    mul-float/2addr v0, v1

    div-float/2addr v0, p3

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    .line 219
    sub-float v1, p2, p1

    invoke-static {v1}, Ljava/lang/Math;->signum(F)F

    move-result v1

    float-to-double v2, v1

    iget-wide v4, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mSwipeTimeConstant:D

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->exp(D)D

    move-result-wide v0

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    sub-double/2addr v0, v6

    mul-double/2addr v0, v4

    mul-double/2addr v0, v2

    double-to-float v0, v0

    .line 221
    iget v1, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mPxToDp:F

    mul-float/2addr v1, p1

    sub-float v0, v1, v0

    return v0
.end method

.method private checkForFastScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 187
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v4

    sub-long/2addr v2, v4

    long-to-float v0, v2

    .line 188
    const/4 v2, 0x0

    cmpg-float v2, v0, v2

    if-gtz v2, :cond_0

    move v0, v1

    .line 213
    :goto_0
    return v0

    .line 191
    :cond_0
    sget-object v2, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$1;->$SwitchMap$com$google$android$apps$chrome$eventfilter$EdgeSwipeEventFilter$ScrollDirection:[I

    iget-object v3, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mScrollDirection:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 209
    iget v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mGutterDistance:F

    .line 213
    :goto_1
    iget v2, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mGutterDistance:F

    cmpg-float v0, v0, v2

    if-gez v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    .line 193
    :pswitch_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    iget v3, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mCurrentTouchOffsetX:F

    add-float/2addr v2, v3

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    iget v4, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mCurrentTouchOffsetX:F

    add-float/2addr v3, v4

    invoke-direct {p0, v2, v3, v0}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->calculateBiasedPosition(FFF)F

    move-result v0

    goto :goto_1

    .line 198
    :pswitch_1
    iget-object v2, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mHost:Lcom/google/android/apps/chrome/eventfilter/EventFilterHost;

    invoke-interface {v2}, Lcom/google/android/apps/chrome/eventfilter/EventFilterHost;->getView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v2

    int-to-float v2, v2

    iget v3, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mPxToDp:F

    mul-float/2addr v2, v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    iget v4, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mCurrentTouchOffsetX:F

    add-float/2addr v3, v4

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    iget v5, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mCurrentTouchOffsetX:F

    add-float/2addr v4, v5

    invoke-direct {p0, v3, v4, v0}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->calculateBiasedPosition(FFF)F

    move-result v0

    sub-float v0, v2, v0

    .line 202
    goto :goto_1

    .line 204
    :pswitch_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    iget v3, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mCurrentTouchOffsetY:F

    add-float/2addr v2, v3

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    iget v4, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mCurrentTouchOffsetY:F

    add-float/2addr v3, v4

    invoke-direct {p0, v2, v3, v0}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->calculateBiasedPosition(FFF)F

    move-result v0

    goto :goto_1

    :cond_1
    move v0, v1

    .line 213
    goto :goto_0

    .line 191
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private flingOccurred(FFFFFF)V
    .locals 7

    .prologue
    .line 158
    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mEdgeSwipeHandler:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mEdgeSwipeStarted:Z

    if-eqz v0, :cond_0

    .line 159
    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mEdgeSwipeHandler:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    invoke-interface/range {v0 .. v6}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;->swipeFlingOccurred(FFFFFF)V

    .line 161
    :cond_0
    return-void
.end method

.method private propagateAccumulatedEventsAndClear()Z
    .locals 6

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 230
    move v1, v2

    move v3, v4

    .line 231
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mAccumulatedEvents:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 232
    iget-object v5, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mHost:Lcom/google/android/apps/chrome/eventfilter/EventFilterHost;

    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mAccumulatedEvents:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/MotionEvent;

    invoke-interface {v5, v0}, Lcom/google/android/apps/chrome/eventfilter/EventFilterHost;->propagateEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    move v3, v4

    .line 231
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    move v3, v2

    .line 232
    goto :goto_1

    .line 234
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mAccumulatedEvents:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 235
    iput-boolean v2, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mAccumulatingEvents:Z

    .line 236
    return v3
.end method

.method private scrollFinished()V
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mEdgeSwipeHandler:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mEdgeSwipeStarted:Z

    if-eqz v0, :cond_0

    .line 127
    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mEdgeSwipeHandler:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;->swipeFinished()V

    .line 129
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mEdgeSwipeStarted:Z

    .line 130
    return-void
.end method

.method private scrollStarted(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;FF)V
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mEdgeSwipeHandler:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;

    if-eqz v0, :cond_0

    .line 116
    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mEdgeSwipeHandler:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;->swipeStarted(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;FF)V

    .line 117
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mEdgeSwipeStarted:Z

    .line 119
    :cond_0
    return-void
.end method

.method private scrollUpdated(FFFFFF)V
    .locals 7

    .prologue
    .line 143
    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mEdgeSwipeHandler:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mEdgeSwipeStarted:Z

    if-eqz v0, :cond_0

    .line 144
    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mEdgeSwipeHandler:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    invoke-interface/range {v0 .. v6}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;->swipeUpdated(FFFFFF)V

    .line 146
    :cond_0
    return-void
.end method


# virtual methods
.method public onInterceptTouchEventInternal(Landroid/view/MotionEvent;Z)Z
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 241
    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    if-nez v0, :cond_0

    move v0, v1

    .line 272
    :goto_0
    return v0

    .line 243
    :cond_0
    iput-boolean v2, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mPropagateEventsToHostView:Z

    .line 245
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mInLongPress:Z

    .line 246
    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->getCurrentModel()Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v0

    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->getCount()I

    move-result v0

    .line 247
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v3

    .line 248
    iget-boolean v4, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mEnableTabSwiping:Z

    if-eqz v4, :cond_4

    if-nez p2, :cond_4

    if-nez v3, :cond_4

    if-lez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mScrollDirection:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    sget-object v3, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;->UNKNOWN:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    if-ne v0, v3, :cond_4

    .line 253
    sget-object v0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;->UNKNOWN:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    .line 254
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    iget v4, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mCurrentTouchOffsetX:F

    add-float/2addr v3, v4

    iget v4, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mPxToDp:F

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mSwipeRegion:F

    cmpg-float v3, v3, v4

    if-gez v3, :cond_5

    .line 255
    sget-object v0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;->RIGHT:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    .line 264
    :cond_1
    :goto_1
    sget-object v3, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;->UNKNOWN:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    if-eq v0, v3, :cond_3

    iget-object v3, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mEdgeSwipeHandler:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mEdgeSwipeHandler:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;

    invoke-interface {v3, v0}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;->isSwipeEnabled(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 266
    :cond_2
    iput-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mScrollDirection:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    .line 269
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mScrollDirection:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    sget-object v3, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;->UNKNOWN:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    if-eq v0, v3, :cond_4

    iput-boolean v1, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mPropagateEventsToHostView:Z

    :cond_4
    move v0, v2

    .line 272
    goto :goto_0

    .line 256
    :cond_5
    iget-object v3, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mHost:Lcom/google/android/apps/chrome/eventfilter/EventFilterHost;

    invoke-interface {v3}, Lcom/google/android/apps/chrome/eventfilter/EventFilterHost;->getView()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getWidth()I

    move-result v3

    int-to-float v3, v3

    iget v4, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mPxToDp:F

    mul-float/2addr v3, v4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    iget v5, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mCurrentTouchOffsetX:F

    add-float/2addr v4, v5

    iget v5, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mPxToDp:F

    mul-float/2addr v4, v5

    sub-float/2addr v3, v4

    iget v4, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mSwipeRegion:F

    cmpg-float v3, v3, v4

    if-gez v3, :cond_6

    .line 258
    sget-object v0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;->LEFT:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    goto :goto_1

    .line 259
    :cond_6
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    iget v4, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mCurrentTouchOffsetY:F

    add-float/2addr v3, v4

    iget v4, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mPxToDp:F

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mSwipeRegion:F

    cmpg-float v3, v3, v4

    if-gez v3, :cond_1

    .line 260
    sget-object v0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;->DOWN:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    goto :goto_1
.end method

.method public onTouchEventInternal(Landroid/view/MotionEvent;)Z
    .locals 9

    .prologue
    const/4 v4, 0x3

    const/4 v5, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x1

    .line 277
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mPropagateEventsToHostView:Z

    if-eqz v0, :cond_1

    .line 278
    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mHost:Lcom/google/android/apps/chrome/eventfilter/EventFilterHost;

    invoke-interface {v0, p1}, Lcom/google/android/apps/chrome/eventfilter/EventFilterHost;->propagateEvent(Landroid/view/MotionEvent;)Z

    .line 328
    :cond_0
    :goto_0
    return v8

    .line 284
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    .line 285
    const/4 v1, 0x5

    if-ne v0, v1, :cond_2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    iget-boolean v1, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mScrollStarted:Z

    if-nez v1, :cond_2

    .line 288
    iput-boolean v8, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mPropagateEventsToHostView:Z

    .line 290
    sget-object v0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;->UNKNOWN:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    iput-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mScrollDirection:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    .line 291
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getDownTime()J

    move-result-wide v0

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    move v6, v5

    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v0

    .line 296
    iget-object v1, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mGestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v1, v0}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 297
    invoke-direct {p0}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->propagateAccumulatedEventsAndClear()Z

    .line 298
    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mHost:Lcom/google/android/apps/chrome/eventfilter/EventFilterHost;

    invoke-interface {v0, p1}, Lcom/google/android/apps/chrome/eventfilter/EventFilterHost;->propagateEvent(Landroid/view/MotionEvent;)Z

    goto :goto_0

    .line 302
    :cond_2
    if-ne v0, v8, :cond_4

    .line 303
    iget-boolean v1, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mInLongPress:Z

    if-nez v1, :cond_3

    iget-boolean v1, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mInDoubleTap:Z

    if-eqz v1, :cond_4

    .line 304
    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mHost:Lcom/google/android/apps/chrome/eventfilter/EventFilterHost;

    invoke-interface {v1, p1}, Lcom/google/android/apps/chrome/eventfilter/EventFilterHost;->propagateEvent(Landroid/view/MotionEvent;)Z

    .line 305
    iput-boolean v7, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mInLongPress:Z

    .line 306
    iput-boolean v7, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mInDoubleTap:Z

    .line 310
    :cond_4
    iget-object v1, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mScrollDirection:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    sget-object v2, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;->UNKNOWN:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    if-eq v1, v2, :cond_0

    .line 311
    iget-boolean v1, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mAccumulatingEvents:Z

    if-eqz v1, :cond_5

    .line 312
    iget-object v1, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mAccumulatedEvents:Ljava/util/ArrayList;

    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 314
    :cond_5
    iget-object v1, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mGestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v1, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 316
    if-eq v0, v8, :cond_6

    if-ne v0, v4, :cond_0

    .line 317
    :cond_6
    invoke-direct {p0}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->scrollFinished()V

    .line 318
    invoke-direct {p0}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->propagateAccumulatedEventsAndClear()Z

    .line 319
    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mScrollDirection:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    sget-object v1, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;->DOWN:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    if-eq v0, v1, :cond_7

    .line 320
    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mContext:Landroid/content/Context;

    const/16 v1, 0x12

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastNotification(Landroid/content/Context;I)V

    .line 324
    :cond_7
    sget-object v0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;->UNKNOWN:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    iput-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mScrollDirection:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    goto :goto_0
.end method

.method public setTabModelSelector(Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;)V
    .locals 0

    .prologue
    .line 104
    iput-object p1, p0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    .line 105
    return-void
.end method
