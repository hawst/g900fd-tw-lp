.class public Lcom/google/android/apps/chrome/snapshot/DatabaseChangeCalculator;
.super Ljava/lang/Object;
.source "DatabaseChangeCalculator.java"


# static fields
.field public static final TYPE_SNAPSHOT:Ljava/lang/String; = "snapshot"

.field public static final TYPE_SNAPSHOT_DELAYED:Ljava/lang/String; = "url_with_delayed_snapshot"

.field public static final TYPE_URL:Ljava/lang/String; = "url"


# instance fields
.field private mJobIdMapMaker:Lcom/google/android/apps/chrome/snapshot/DatabaseChangeCalculator$SnapshotMapMaker;

.field private mPageUrlJobIdMapMaker:Lcom/google/android/apps/chrome/snapshot/DatabaseChangeCalculator$SnapshotMapMaker;

.field private mSnapshotIdMapMaker:Lcom/google/android/apps/chrome/snapshot/DatabaseChangeCalculator$SnapshotMapMaker;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 217
    return-void
.end method

.method private static copyJobData(Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob;Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;Lcom/google/android/apps/chrome/snapshot/SnapshotState;)Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;
    .locals 4

    .prologue
    .line 142
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob;->getJobId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->withJobId(Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob;->getCreateTime()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->withCreateTimeIfNewer(J)Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->withTitle(Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob;->getDownloadUri()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->withDownloadUri(Landroid/net/Uri;)Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob;->getMimeType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->withMimeType(Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob;->getMimeType()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;->DOWNLOAD_PENDING:Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;

    :goto_0
    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->withStateIfNotSet(Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;)Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;

    move-result-object v0

    .line 150
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob;->getJobData()Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob$JobData;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/snapshot/DatabaseChangeCalculator;->handleData(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob$JobData;)Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;

    move-result-object v0

    return-object v0

    .line 142
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static copyPageUrlData(Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob;Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;Lcom/google/android/apps/chrome/snapshot/SnapshotState;)Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;
    .locals 4

    .prologue
    .line 155
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob;->getJobId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->withPageUrlJobId(Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob;->getCreateTime()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->withCreateTimeIfNewer(J)Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->withTitle(Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob;->getDownloadUri()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->withPageUrlDownloadUri(Landroid/net/Uri;)Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob;->getMimeType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->withPageUrlMimeType(Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/chrome/snapshot/PageUrlJobState;->READY:Lcom/google/android/apps/chrome/snapshot/PageUrlJobState;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->withPageUrlStateIfNotSet(Lcom/google/android/apps/chrome/snapshot/PageUrlJobState;)Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;

    move-result-object v0

    .line 162
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob;->getJobData()Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob$JobData;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/snapshot/DatabaseChangeCalculator;->handleData(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob$JobData;)Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;

    move-result-object v0

    return-object v0
.end method

.method private createJobIdMap(Ljava/util/Collection;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 189
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/DatabaseChangeCalculator;->mJobIdMapMaker:Lcom/google/android/apps/chrome/snapshot/DatabaseChangeCalculator$SnapshotMapMaker;

    if-nez v0, :cond_0

    .line 190
    new-instance v0, Lcom/google/android/apps/chrome/snapshot/DatabaseChangeCalculator$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/snapshot/DatabaseChangeCalculator$2;-><init>(Lcom/google/android/apps/chrome/snapshot/DatabaseChangeCalculator;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/snapshot/DatabaseChangeCalculator;->mJobIdMapMaker:Lcom/google/android/apps/chrome/snapshot/DatabaseChangeCalculator$SnapshotMapMaker;

    .line 199
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/DatabaseChangeCalculator;->mJobIdMapMaker:Lcom/google/android/apps/chrome/snapshot/DatabaseChangeCalculator$SnapshotMapMaker;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/snapshot/DatabaseChangeCalculator$SnapshotMapMaker;->createMap(Ljava/util/Collection;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method private createPageUrlJobIdMap(Ljava/util/Collection;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 175
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/DatabaseChangeCalculator;->mPageUrlJobIdMapMaker:Lcom/google/android/apps/chrome/snapshot/DatabaseChangeCalculator$SnapshotMapMaker;

    if-nez v0, :cond_0

    .line 176
    new-instance v0, Lcom/google/android/apps/chrome/snapshot/DatabaseChangeCalculator$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/snapshot/DatabaseChangeCalculator$1;-><init>(Lcom/google/android/apps/chrome/snapshot/DatabaseChangeCalculator;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/snapshot/DatabaseChangeCalculator;->mPageUrlJobIdMapMaker:Lcom/google/android/apps/chrome/snapshot/DatabaseChangeCalculator$SnapshotMapMaker;

    .line 185
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/DatabaseChangeCalculator;->mPageUrlJobIdMapMaker:Lcom/google/android/apps/chrome/snapshot/DatabaseChangeCalculator$SnapshotMapMaker;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/snapshot/DatabaseChangeCalculator$SnapshotMapMaker;->createMap(Ljava/util/Collection;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method private createSnapshotIdMap(Ljava/util/Collection;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 204
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/DatabaseChangeCalculator;->mSnapshotIdMapMaker:Lcom/google/android/apps/chrome/snapshot/DatabaseChangeCalculator$SnapshotMapMaker;

    if-nez v0, :cond_0

    .line 205
    new-instance v0, Lcom/google/android/apps/chrome/snapshot/DatabaseChangeCalculator$3;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/snapshot/DatabaseChangeCalculator$3;-><init>(Lcom/google/android/apps/chrome/snapshot/DatabaseChangeCalculator;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/snapshot/DatabaseChangeCalculator;->mSnapshotIdMapMaker:Lcom/google/android/apps/chrome/snapshot/DatabaseChangeCalculator$SnapshotMapMaker;

    .line 214
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/DatabaseChangeCalculator;->mSnapshotIdMapMaker:Lcom/google/android/apps/chrome/snapshot/DatabaseChangeCalculator$SnapshotMapMaker;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/snapshot/DatabaseChangeCalculator$SnapshotMapMaker;->createMap(Ljava/util/Collection;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method private static findDocumentFromSnapshotId(Ljava/util/Map;Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 129
    if-nez p1, :cond_0

    move-object v0, v1

    .line 137
    :goto_0
    return-object v0

    .line 132
    :cond_0
    invoke-interface {p0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;

    .line 133
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->getSnapshotId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 137
    goto :goto_0
.end method

.method private static handleData(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob$JobData;)Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;
    .locals 2

    .prologue
    .line 167
    if-nez p1, :cond_0

    .line 168
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->build()Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;

    move-result-object v0

    .line 170
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob$JobData;->getUri()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->withUri(Landroid/net/Uri;)Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob$JobData;->getSnapshotId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->withSnapshotId(Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->build()Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;

    move-result-object v0

    goto :goto_0
.end method

.method private static handleEventFromChromeDesktop(Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob;)V
    .locals 3

    .prologue
    .line 55
    invoke-virtual {p3}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob;->getJobData()Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob$JobData;

    move-result-object v0

    .line 56
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob$JobData;->getType()Ljava/lang/String;

    move-result-object v1

    .line 57
    const-string/jumbo v2, "url"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 58
    invoke-virtual {p3}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob;->getJobId()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 59
    sget-object v1, Lcom/google/android/apps/chrome/snapshot/SnapshotState;->UPDATED:Lcom/google/android/apps/chrome/snapshot/SnapshotState;

    invoke-virtual {p3}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob;->getJobId()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->builder()Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;

    move-result-object v0

    invoke-static {p0, p3, v1, v0}, Lcom/google/android/apps/chrome/snapshot/DatabaseChangeCalculator;->updatePageUrlChanges(Ljava/util/Map;Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob;Lcom/google/android/apps/chrome/snapshot/SnapshotState;Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;)V

    .line 84
    :goto_0
    return-void

    .line 62
    :cond_0
    sget-object v0, Lcom/google/android/apps/chrome/snapshot/SnapshotState;->CREATED:Lcom/google/android/apps/chrome/snapshot/SnapshotState;

    invoke-static {}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->createBuilder()Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;

    move-result-object v1

    invoke-static {p0, p3, v0, v1}, Lcom/google/android/apps/chrome/snapshot/DatabaseChangeCalculator;->updatePageUrlChanges(Ljava/util/Map;Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob;Lcom/google/android/apps/chrome/snapshot/SnapshotState;Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;)V

    goto :goto_0

    .line 65
    :cond_1
    const-string/jumbo v2, "url_with_delayed_snapshot"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 66
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob$JobData;->getSnapshotId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 67
    sget-object v1, Lcom/google/android/apps/chrome/snapshot/SnapshotState;->UPDATED:Lcom/google/android/apps/chrome/snapshot/SnapshotState;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob$JobData;->getSnapshotId()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->builder()Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;

    move-result-object v0

    invoke-static {p0, p3, v1, v0}, Lcom/google/android/apps/chrome/snapshot/DatabaseChangeCalculator;->updatePageUrlChanges(Ljava/util/Map;Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob;Lcom/google/android/apps/chrome/snapshot/SnapshotState;Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;)V

    goto :goto_0

    .line 70
    :cond_2
    sget-object v0, Lcom/google/android/apps/chrome/snapshot/SnapshotState;->CREATED:Lcom/google/android/apps/chrome/snapshot/SnapshotState;

    invoke-static {}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->createBuilder()Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;

    move-result-object v1

    invoke-static {p0, p3, v0, v1}, Lcom/google/android/apps/chrome/snapshot/DatabaseChangeCalculator;->updatePageUrlChanges(Ljava/util/Map;Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob;Lcom/google/android/apps/chrome/snapshot/SnapshotState;Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;)V

    goto :goto_0

    .line 73
    :cond_3
    const-string/jumbo v2, "snapshot"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 74
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob$JobData;->getSnapshotId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 75
    sget-object v1, Lcom/google/android/apps/chrome/snapshot/SnapshotState;->UPDATED:Lcom/google/android/apps/chrome/snapshot/SnapshotState;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob$JobData;->getSnapshotId()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->builder()Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;

    move-result-object v0

    invoke-static {p0, p3, v1, v0}, Lcom/google/android/apps/chrome/snapshot/DatabaseChangeCalculator;->updateChanges(Ljava/util/Map;Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob;Lcom/google/android/apps/chrome/snapshot/SnapshotState;Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;)V

    goto :goto_0

    .line 78
    :cond_4
    sget-object v0, Lcom/google/android/apps/chrome/snapshot/SnapshotState;->CREATED:Lcom/google/android/apps/chrome/snapshot/SnapshotState;

    invoke-static {}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->createBuilder()Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;

    move-result-object v1

    invoke-static {p0, p3, v0, v1}, Lcom/google/android/apps/chrome/snapshot/DatabaseChangeCalculator;->updateChanges(Ljava/util/Map;Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob;Lcom/google/android/apps/chrome/snapshot/SnapshotState;Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;)V

    goto :goto_0

    .line 82
    :cond_5
    const-string/jumbo v0, "DatabaseChangeCalculator"

    const-string/jumbo v1, "Unable to parse type"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private static handleNormalPrintJob(Ljava/util/Map;Ljava/util/Map;Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob;)V
    .locals 2

    .prologue
    .line 88
    invoke-virtual {p2}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob;->getJobId()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 89
    sget-object v1, Lcom/google/android/apps/chrome/snapshot/SnapshotState;->UPDATED:Lcom/google/android/apps/chrome/snapshot/SnapshotState;

    invoke-virtual {p2}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob;->getJobId()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->builder()Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;

    move-result-object v0

    invoke-static {p0, p2, v1, v0}, Lcom/google/android/apps/chrome/snapshot/DatabaseChangeCalculator;->updateChanges(Ljava/util/Map;Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob;Lcom/google/android/apps/chrome/snapshot/SnapshotState;Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;)V

    .line 94
    :goto_0
    return-void

    .line 92
    :cond_0
    sget-object v0, Lcom/google/android/apps/chrome/snapshot/SnapshotState;->CREATED:Lcom/google/android/apps/chrome/snapshot/SnapshotState;

    invoke-static {}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->createBuilder()Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;

    move-result-object v1

    invoke-static {p0, p2, v0, v1}, Lcom/google/android/apps/chrome/snapshot/DatabaseChangeCalculator;->updateChanges(Ljava/util/Map;Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob;Lcom/google/android/apps/chrome/snapshot/SnapshotState;Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;)V

    goto :goto_0
.end method

.method private static updateChanges(Ljava/util/Map;Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob;Lcom/google/android/apps/chrome/snapshot/SnapshotState;Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;)V
    .locals 2

    .prologue
    .line 98
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob;->getJobData()Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob$JobData;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    move-object v1, v0

    .line 100
    :goto_0
    if-nez v1, :cond_1

    .line 101
    invoke-static {p1, p3, p2}, Lcom/google/android/apps/chrome/snapshot/DatabaseChangeCalculator;->copyJobData(Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob;Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;Lcom/google/android/apps/chrome/snapshot/SnapshotState;)Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;

    move-result-object v0

    .line 102
    invoke-interface {p0, v0, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 109
    :goto_1
    return-void

    .line 98
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob;->getJobData()Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob$JobData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob$JobData;->getSnapshotId()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/apps/chrome/snapshot/DatabaseChangeCalculator;->findDocumentFromSnapshotId(Ljava/util/Map;Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    .line 104
    :cond_1
    invoke-interface {p0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/snapshot/SnapshotState;

    .line 105
    invoke-virtual {v1}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->builder()Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;

    move-result-object v1

    invoke-static {p1, v1, v0}, Lcom/google/android/apps/chrome/snapshot/DatabaseChangeCalculator;->copyJobData(Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob;Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;Lcom/google/android/apps/chrome/snapshot/SnapshotState;)Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;

    move-result-object v1

    .line 107
    invoke-interface {p0, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1
.end method

.method private static updatePageUrlChanges(Ljava/util/Map;Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob;Lcom/google/android/apps/chrome/snapshot/SnapshotState;Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;)V
    .locals 2

    .prologue
    .line 113
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob;->getJobData()Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob$JobData;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    move-object v1, v0

    .line 115
    :goto_0
    if-nez v1, :cond_1

    .line 116
    invoke-static {p1, p3, p2}, Lcom/google/android/apps/chrome/snapshot/DatabaseChangeCalculator;->copyPageUrlData(Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob;Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;Lcom/google/android/apps/chrome/snapshot/SnapshotState;)Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;

    move-result-object v0

    .line 117
    invoke-interface {p0, v0, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 124
    :goto_1
    return-void

    .line 113
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob;->getJobData()Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob$JobData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob$JobData;->getSnapshotId()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/apps/chrome/snapshot/DatabaseChangeCalculator;->findDocumentFromSnapshotId(Ljava/util/Map;Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    .line 119
    :cond_1
    invoke-interface {p0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/snapshot/SnapshotState;

    .line 120
    invoke-virtual {v1}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->builder()Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;

    move-result-object v1

    invoke-static {p1, v1, v0}, Lcom/google/android/apps/chrome/snapshot/DatabaseChangeCalculator;->copyPageUrlData(Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob;Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;Lcom/google/android/apps/chrome/snapshot/SnapshotState;)Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;

    move-result-object v1

    .line 122
    invoke-interface {p0, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1
.end method


# virtual methods
.method public calculateChanges(Ljava/util/Collection;Ljava/util/Collection;)Ljava/util/Map;
    .locals 7

    .prologue
    .line 37
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 39
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/snapshot/DatabaseChangeCalculator;->createSnapshotIdMap(Ljava/util/Collection;)Ljava/util/Map;

    move-result-object v2

    .line 40
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/snapshot/DatabaseChangeCalculator;->createJobIdMap(Ljava/util/Collection;)Ljava/util/Map;

    move-result-object v3

    .line 41
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/snapshot/DatabaseChangeCalculator;->createPageUrlJobIdMap(Ljava/util/Collection;)Ljava/util/Map;

    move-result-object v4

    .line 42
    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob;

    .line 43
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob;->hasJobData()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 44
    invoke-static {v1, v2, v4, v0}, Lcom/google/android/apps/chrome/snapshot/DatabaseChangeCalculator;->handleEventFromChromeDesktop(Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob;)V

    goto :goto_0

    .line 46
    :cond_0
    invoke-static {v1, v3, v0}, Lcom/google/android/apps/chrome/snapshot/DatabaseChangeCalculator;->handleNormalPrintJob(Ljava/util/Map;Ljava/util/Map;Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob;)V

    goto :goto_0

    .line 49
    :cond_1
    return-object v1
.end method
