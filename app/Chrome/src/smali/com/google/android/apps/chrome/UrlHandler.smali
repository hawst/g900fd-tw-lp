.class public Lcom/google/android/apps/chrome/UrlHandler;
.super Ljava/lang/Object;
.source "UrlHandler.java"


# instance fields
.field private final mEnvironment:Lcom/google/android/apps/chrome/UrlHandler$Environment;


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 101
    new-instance v0, Lcom/google/android/apps/chrome/UrlHandler$ActivityEnvironment;

    invoke-direct {v0, p1}, Lcom/google/android/apps/chrome/UrlHandler$ActivityEnvironment;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/UrlHandler;->mEnvironment:Lcom/google/android/apps/chrome/UrlHandler$Environment;

    .line 102
    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/chrome/UrlHandler$Environment;)V
    .locals 0

    .prologue
    .line 105
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 106
    iput-object p1, p0, Lcom/google/android/apps/chrome/UrlHandler;->mEnvironment:Lcom/google/android/apps/chrome/UrlHandler$Environment;

    .line 107
    return-void
.end method

.method private shouldOverrideInternal(Ljava/lang/String;ZLcom/google/android/apps/chrome/tab/TabRedirectHandler;)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 189
    const-string/jumbo v2, "wtai://wp/"

    invoke-virtual {p1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 192
    const-string/jumbo v2, "wtai://wp/mc;"

    invoke-virtual {p1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 193
    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v2, "android.intent.action.VIEW"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "tel:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v4, 0xd

    invoke-virtual {p1, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 196
    iget-object v2, p0, Lcom/google/android/apps/chrome/UrlHandler;->mEnvironment:Lcom/google/android/apps/chrome/UrlHandler$Environment;

    invoke-interface {v2, v1}, Lcom/google/android/apps/chrome/UrlHandler$Environment;->startActivity(Landroid/content/Intent;)V

    .line 241
    :cond_0
    :goto_0
    return v0

    .line 201
    :cond_1
    const-string/jumbo v2, "wtai://wp/sd;"

    invoke-virtual {p1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    move v0, v1

    .line 203
    goto :goto_0

    .line 208
    :cond_2
    const-string/jumbo v2, "wtai://wp/ap;"

    invoke-virtual {p1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    move v0, v1

    .line 210
    goto :goto_0

    .line 218
    :cond_3
    const-string/jumbo v2, "about:"

    invoke-virtual {p1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    move v0, v1

    .line 219
    goto :goto_0

    .line 223
    :cond_4
    const-string/jumbo v2, "content:"

    invoke-virtual {p1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    move v0, v1

    .line 224
    goto :goto_0

    .line 231
    :cond_5
    const-string/jumbo v2, ".*youtube\\.com.*[?&]pairingCode=.*"

    invoke-virtual {p1, v2}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    move v0, v1

    .line 232
    goto :goto_0

    .line 235
    :cond_6
    invoke-static {}, Lorg/chromium/base/CommandLine;->getInstance()Lorg/chromium/base/CommandLine;

    move-result-object v2

    const-string/jumbo v3, "disable-external-intent-requests"

    invoke-virtual {v2, v3}, Lorg/chromium/base/CommandLine;->hasSwitch(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_7

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/chrome/UrlHandler;->startActivityForUrl(Ljava/lang/String;ZLcom/google/android/apps/chrome/tab/TabRedirectHandler;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_7
    move v0, v1

    .line 241
    goto :goto_0
.end method

.method private startActivityForUrl(Ljava/lang/String;ZLcom/google/android/apps/chrome/tab/TabRedirectHandler;)Z
    .locals 7

    .prologue
    const/4 v5, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 250
    const/4 v2, 0x1

    :try_start_0
    invoke-static {p1, v2}, Landroid/content/Intent;->parseUri(Ljava/lang/String;I)Landroid/content/Intent;
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 258
    iget-object v3, p0, Lcom/google/android/apps/chrome/UrlHandler;->mEnvironment:Lcom/google/android/apps/chrome/UrlHandler$Environment;

    invoke-interface {v3, v2}, Lcom/google/android/apps/chrome/UrlHandler$Environment;->canResolveActivity(Landroid/content/Intent;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 259
    invoke-virtual {v2}, Landroid/content/Intent;->getPackage()Ljava/lang/String;

    move-result-object v2

    .line 260
    if-eqz v2, :cond_0

    .line 262
    :try_start_1
    new-instance v3, Landroid/content/Intent;

    const-string/jumbo v4, "android.intent.action.VIEW"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "market://details?id="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v5, "&referrer="

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v5, p0, Lcom/google/android/apps/chrome/UrlHandler;->mEnvironment:Lcom/google/android/apps/chrome/UrlHandler$Environment;

    invoke-interface {v5}, Lcom/google/android/apps/chrome/UrlHandler$Environment;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v3, v4, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 265
    const-string/jumbo v2, "android.intent.category.BROWSABLE"

    invoke-virtual {v3, v2}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 266
    const-string/jumbo v2, "com.android.vending"

    invoke-virtual {v3, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 267
    iget-object v2, p0, Lcom/google/android/apps/chrome/UrlHandler;->mEnvironment:Lcom/google/android/apps/chrome/UrlHandler$Environment;

    invoke-interface {v2, v3}, Lcom/google/android/apps/chrome/UrlHandler$Environment;->startActivity(Landroid/content/Intent;)V
    :try_end_1
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    .line 322
    :goto_0
    return v0

    .line 251
    :catch_0
    move-exception v0

    .line 252
    const-string/jumbo v2, "Chrome"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Bad URI "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ": "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/net/URISyntaxException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    .line 253
    goto :goto_0

    .line 272
    :catch_1
    move-exception v0

    move v0, v1

    goto :goto_0

    :cond_0
    move v0, v1

    .line 275
    goto :goto_0

    .line 281
    :cond_1
    const-string/jumbo v3, "android.intent.category.BROWSABLE"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 282
    invoke-virtual {v2, v5}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 283
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0xf

    if-lt v3, v4, :cond_2

    .line 284
    invoke-virtual {v2}, Landroid/content/Intent;->getSelector()Landroid/content/Intent;

    move-result-object v3

    .line 285
    if-eqz v3, :cond_2

    .line 286
    const-string/jumbo v4, "android.intent.category.BROWSABLE"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 287
    invoke-virtual {v3, v5}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 293
    :cond_2
    const-string/jumbo v3, "com.android.browser.application_id"

    iget-object v4, p0, Lcom/google/android/apps/chrome/UrlHandler;->mEnvironment:Lcom/google/android/apps/chrome/UrlHandler$Environment;

    invoke-interface {v4}, Lcom/google/android/apps/chrome/UrlHandler$Environment;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 294
    const/high16 v3, 0x10000000

    invoke-virtual {v2, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 299
    invoke-static {p1}, Lorg/chromium/chrome/browser/UrlUtilities;->isAcceptedScheme(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/android/apps/chrome/UrlHandler;->mEnvironment:Lcom/google/android/apps/chrome/UrlHandler$Environment;

    invoke-interface {v3, v2}, Lcom/google/android/apps/chrome/UrlHandler$Environment;->isSpecializedHandlerAvailable(Landroid/content/Intent;)Z

    move-result v3

    if-nez v3, :cond_3

    move v0, v1

    .line 301
    goto :goto_0

    .line 304
    :cond_3
    if-eqz p2, :cond_4

    :try_start_2
    iget-object v3, p0, Lcom/google/android/apps/chrome/UrlHandler;->mEnvironment:Lcom/google/android/apps/chrome/UrlHandler$Environment;

    invoke-interface {v3, v2}, Lcom/google/android/apps/chrome/UrlHandler$Environment;->willChromeHandleIntent(Landroid/content/Intent;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 307
    iget-object v3, p0, Lcom/google/android/apps/chrome/UrlHandler;->mEnvironment:Lcom/google/android/apps/chrome/UrlHandler$Environment;

    invoke-interface {v3, v2}, Lcom/google/android/apps/chrome/UrlHandler$Environment;->startIncognitoIntent(Landroid/content/Intent;)V

    goto :goto_0

    .line 322
    :catch_2
    move-exception v0

    move v0, v1

    goto :goto_0

    .line 310
    :cond_4
    if-eqz p3, :cond_5

    .line 311
    iget-object v0, p0, Lcom/google/android/apps/chrome/UrlHandler;->mEnvironment:Lcom/google/android/apps/chrome/UrlHandler$Environment;

    invoke-virtual {p3, v0, v2}, Lcom/google/android/apps/chrome/tab/TabRedirectHandler;->hasNewResolver(Lcom/google/android/apps/chrome/UrlHandler$Environment;Landroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_5

    move v0, v1

    .line 312
    goto/16 :goto_0

    .line 315
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/chrome/UrlHandler;->mEnvironment:Lcom/google/android/apps/chrome/UrlHandler$Environment;

    invoke-interface {v0, v2}, Lcom/google/android/apps/chrome/UrlHandler$Environment;->startActivityIfNeeded(Landroid/content/Intent;)Z
    :try_end_2
    .catch Landroid/content/ActivityNotFoundException; {:try_start_2 .. :try_end_2} :catch_2

    move-result v0

    goto/16 :goto_0
.end method


# virtual methods
.method public shouldOverrideNewTab(Ljava/lang/String;Z)Z
    .locals 1

    .prologue
    .line 181
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/chrome/UrlHandler;->shouldOverrideInternal(Ljava/lang/String;ZLcom/google/android/apps/chrome/tab/TabRedirectHandler;)Z

    move-result v0

    return v0
.end method

.method public shouldOverrideUrlLoading(Ljava/lang/String;Ljava/lang/String;ZIZLcom/google/android/apps/chrome/tab/TabRedirectHandler;)Z
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 112
    and-int/lit16 v0, p4, 0xff

    .line 113
    if-nez v0, :cond_1

    move v8, v1

    .line 114
    :goto_0
    if-ne v0, v1, :cond_2

    move v7, v1

    .line 115
    :goto_1
    const/4 v3, 0x7

    if-ne v0, v3, :cond_3

    move v6, v1

    .line 118
    :goto_2
    const/high16 v0, 0x8000000

    and-int/2addr v0, p4

    if-eqz v0, :cond_4

    move v3, v1

    .line 120
    :goto_3
    const/high16 v0, 0x1000000

    and-int/2addr v0, p4

    if-eqz v0, :cond_5

    move v0, v1

    .line 122
    :goto_4
    invoke-static {p1}, Lorg/chromium/chrome/browser/UrlUtilities;->isAcceptedScheme(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_6

    move v5, v1

    .line 135
    :goto_5
    if-eqz v0, :cond_7

    .line 176
    :cond_0
    :goto_6
    return v2

    :cond_1
    move v8, v2

    .line 113
    goto :goto_0

    :cond_2
    move v7, v2

    .line 114
    goto :goto_1

    :cond_3
    move v6, v2

    .line 115
    goto :goto_2

    :cond_4
    move v3, v2

    .line 118
    goto :goto_3

    :cond_5
    move v0, v2

    .line 120
    goto :goto_4

    :cond_6
    move v5, v2

    .line 122
    goto :goto_5

    .line 142
    :cond_7
    if-eqz v8, :cond_c

    if-nez v3, :cond_c

    move v4, v1

    .line 144
    :goto_7
    if-nez p6, :cond_d

    move v0, v2

    .line 149
    :goto_8
    if-eqz v8, :cond_8

    if-eqz v3, :cond_8

    if-nez p5, :cond_9

    :cond_8
    if-eqz v0, :cond_e

    :cond_9
    move v3, v1

    .line 154
    :goto_9
    if-eqz v6, :cond_f

    if-eqz p5, :cond_f

    move v0, v1

    .line 158
    :goto_a
    if-eqz v7, :cond_10

    if-eqz p5, :cond_10

    if-eqz v5, :cond_10

    .line 160
    :goto_b
    if-nez v4, :cond_a

    if-nez v1, :cond_a

    if-nez v3, :cond_a

    if-eqz v0, :cond_0

    .line 170
    :cond_a
    if-eqz p2, :cond_b

    const-string/jumbo v0, "chrome://"

    invoke-virtual {p2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    const-string/jumbo v0, "http://"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "https://"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 176
    :cond_b
    if-eqz v3, :cond_11

    :goto_c
    invoke-direct {p0, p1, p3, p6}, Lcom/google/android/apps/chrome/UrlHandler;->shouldOverrideInternal(Ljava/lang/String;ZLcom/google/android/apps/chrome/tab/TabRedirectHandler;)Z

    move-result v2

    goto :goto_6

    :cond_c
    move v4, v2

    .line 142
    goto :goto_7

    .line 144
    :cond_d
    invoke-virtual {p6}, Lcom/google/android/apps/chrome/tab/TabRedirectHandler;->isOnEffectiveIntentRedirectChain()Z

    move-result v0

    goto :goto_8

    :cond_e
    move v3, v2

    .line 149
    goto :goto_9

    :cond_f
    move v0, v2

    .line 154
    goto :goto_a

    :cond_10
    move v1, v2

    .line 158
    goto :goto_b

    .line 176
    :cond_11
    const/4 p6, 0x0

    goto :goto_c
.end method
