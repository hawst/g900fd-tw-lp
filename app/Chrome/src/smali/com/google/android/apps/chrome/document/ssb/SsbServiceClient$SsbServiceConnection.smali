.class Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient$SsbServiceConnection;
.super Ljava/lang/Object;
.source "SsbServiceClient.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient;)V
    .locals 0

    .prologue
    .line 134
    iput-object p1, p0, Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient$SsbServiceConnection;->this$0:Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient;Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient$1;)V
    .locals 0

    .prologue
    .line 134
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient$SsbServiceConnection;-><init>(Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient;)V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 3

    .prologue
    .line 145
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient$SsbServiceConnection;->this$0:Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient;

    # getter for: Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient;->mCallback:Lcom/google/android/apps/chrome/document/ssb/SsbServiceCallback;
    invoke-static {v0}, Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient;->access$100(Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient;)Lcom/google/android/apps/chrome/document/ssb/SsbServiceCallback;

    move-result-object v0

    if-nez v0, :cond_0

    .line 158
    :goto_0
    return-void

    .line 147
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient$SsbServiceConnection;->this$0:Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient;

    new-instance v1, Landroid/os/Messenger;

    invoke-direct {v1, p2}, Landroid/os/Messenger;-><init>(Landroid/os/IBinder;)V

    # setter for: Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient;->mService:Landroid/os/Messenger;
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient;->access$302(Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient;Landroid/os/Messenger;)Landroid/os/Messenger;

    .line 149
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient$SsbServiceConnection;->this$0:Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient;

    # invokes: Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient;->prepareOverlayInternal()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient;->access$400(Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient;)V

    .line 151
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    .line 153
    iget-object v1, p0, Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient$SsbServiceConnection;->this$0:Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient;

    # getter for: Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient;->mMessenger:Landroid/os/Messenger;
    invoke-static {v1}, Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient;->access$500(Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient;)Landroid/os/Messenger;

    move-result-object v1

    iput-object v1, v0, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    .line 154
    iget-object v1, p0, Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient$SsbServiceConnection;->this$0:Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient;

    # invokes: Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient;->sendMessage(Landroid/os/Message;)V
    invoke-static {v1, v0}, Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient;->access$600(Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient;Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 155
    :catch_0
    move-exception v0

    .line 156
    const-string/jumbo v1, "SsbServiceConnection"

    const-string/jumbo v2, "SsbServiceConnection - remote call failed"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2

    .prologue
    .line 139
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient$SsbServiceConnection;->this$0:Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient;->mService:Landroid/os/Messenger;
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient;->access$302(Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient;Landroid/os/Messenger;)Landroid/os/Messenger;

    .line 140
    return-void
.end method
