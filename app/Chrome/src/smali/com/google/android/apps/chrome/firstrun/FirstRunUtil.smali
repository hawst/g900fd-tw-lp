.class public Lcom/google/android/apps/chrome/firstrun/FirstRunUtil;
.super Ljava/lang/Object;
.source "FirstRunUtil.java"


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final SIGNIN_SYNC_IMMEDIATELY:I = 0x1

.field public static final SIGNIN_SYNC_SETUP_IN_PROGRESS:I = 0x0

.field public static final SIGNIN_TYPE_FORCED_CHILD_ACCOUNT:I = 0x2

.field public static final SIGNIN_TYPE_FORCED_EDU:I = 0x1

.field public static final SIGNIN_TYPE_INTERACTIVE:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    const-class v0, Lcom/google/android/apps/chrome/firstrun/FirstRunUtil;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/firstrun/FirstRunUtil;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static setCrashUploadPreference(Landroid/content/Context;Z)V
    .locals 3

    .prologue
    .line 160
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 162
    invoke-static {p0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->isMobileNetworkCapable()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 163
    if-eqz p1, :cond_0

    .line 164
    const-string/jumbo v1, "crash_dump_upload"

    sget v2, Lcom/google/android/apps/chrome/R$string;->crash_dump_only_with_wifi_value:I

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 175
    :goto_0
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 176
    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->setCrashReporting(Z)V

    .line 177
    return-void

    .line 167
    :cond_0
    const-string/jumbo v1, "crash_dump_upload"

    sget v2, Lcom/google/android/apps/chrome/R$string;->crash_dump_never_upload_value:I

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    goto :goto_0

    .line 171
    :cond_1
    const-string/jumbo v1, "crash_dump_upload"

    sget v2, Lcom/google/android/apps/chrome/R$string;->crash_dump_never_upload_value:I

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 173
    const-string/jumbo v1, "crash_dump_upload_no_cellular"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    goto :goto_0
.end method

.method public static setupCloudPrint(Landroid/app/Activity;Landroid/accounts/Account;Ljava/lang/Runnable;)V
    .locals 2

    .prologue
    .line 139
    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 140
    new-instance v1, Lcom/google/android/apps/chrome/firstrun/FirstRunUtil$3;

    invoke-direct {v1, v0, p2}, Lcom/google/android/apps/chrome/firstrun/FirstRunUtil$3;-><init>(Landroid/content/Context;Ljava/lang/Runnable;)V

    invoke-static {p0, p1, v1}, Lcom/google/android/apps/chrome/snapshot/cloudprint/AuthTokenHelper;->getCloudPrintAuthToken(Landroid/app/Activity;Landroid/accounts/Account;Ljava/lang/Runnable;)V

    .line 151
    return-void
.end method

.method public static showSyncSignInNotification(Landroid/content/Context;)V
    .locals 6

    .prologue
    .line 185
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 186
    const-class v1, Lcom/google/android/apps/chrome/preferences/Preferences;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    .line 187
    invoke-static {}, Lorg/chromium/chrome/browser/signin/SigninManager;->isNewProfileManagementEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 188
    const-string/jumbo v1, ":android:show_fragment"

    const-class v2, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;

    invoke-virtual {v2}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 194
    :goto_0
    const-string/jumbo v1, "display_home_as_up"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 195
    const/high16 v1, 0x20020000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 199
    invoke-static {}, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->getInstance()Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;

    move-result-object v1

    .line 201
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lorg/chromium/chrome/R$string;->firstrun_signed_in_title:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 203
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lorg/chromium/chrome/R$string;->firstrun_signed_in_description:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 205
    const/4 v4, 0x3

    invoke-virtual {v1, v4, v2, v3, v0}, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->showOneOffNotification(ILjava/lang/String;Ljava/lang/String;Landroid/content/Intent;)V

    .line 208
    return-void

    .line 191
    :cond_0
    const-string/jumbo v1, ":android:show_fragment"

    const-class v2, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;

    invoke-virtual {v2}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0
.end method

.method public static showToSDialog(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 215
    invoke-static {}, Lorg/chromium/chrome/browser/BrowserVersion;->getTermsOfServiceHtml()Ljava/lang/String;

    move-result-object v0

    .line 216
    new-instance v1, Ljava/util/Scanner;

    invoke-direct {v1, v0}, Ljava/util/Scanner;-><init>(Ljava/lang/String;)V

    .line 217
    const-string/jumbo v2, "\\A"

    invoke-virtual {v1, v2}, Ljava/util/Scanner;->useDelimiter(Ljava/lang/String;)Ljava/util/Scanner;

    .line 218
    sget-boolean v2, Lcom/google/android/apps/chrome/firstrun/FirstRunUtil;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    invoke-virtual {v1}, Ljava/util/Scanner;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 219
    :cond_0
    invoke-virtual {v1}, Ljava/util/Scanner;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 220
    invoke-virtual {v1}, Ljava/util/Scanner;->next()Ljava/lang/String;

    move-result-object v0

    .line 222
    :cond_1
    invoke-virtual {v1}, Ljava/util/Scanner;->close()V

    .line 223
    const-string/jumbo v1, "</head>"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    aget-object v0, v0, v1

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    .line 224
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 225
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 226
    return-void
.end method

.method public static signInToSelectedAccount(Landroid/app/Activity;Landroid/accounts/Account;IIZLorg/chromium/chrome/browser/signin/SigninManager$SignInFlowObserver;)V
    .locals 9

    .prologue
    .line 82
    invoke-static {p0}, Lorg/chromium/chrome/browser/signin/SigninManager;->get(Landroid/content/Context;)Lorg/chromium/chrome/browser/signin/SigninManager;

    move-result-object v8

    .line 83
    if-eqz p2, :cond_0

    const/4 v0, 0x1

    move v7, v0

    .line 84
    :goto_0
    new-instance v6, Lcom/google/android/apps/chrome/firstrun/FirstRunUtil$1;

    invoke-direct {v6, p0, p4}, Lcom/google/android/apps/chrome/firstrun/FirstRunUtil$1;-><init>(Landroid/app/Activity;Z)V

    .line 95
    new-instance v0, Lcom/google/android/apps/chrome/firstrun/FirstRunUtil$2;

    move-object v1, p0

    move v2, p3

    move-object v3, p1

    move-object v4, p5

    move v5, p2

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/chrome/firstrun/FirstRunUtil$2;-><init>(Landroid/app/Activity;ILandroid/accounts/Account;Lorg/chromium/chrome/browser/signin/SigninManager$SignInFlowObserver;ILjava/lang/Runnable;)V

    invoke-virtual {v8, p0, p1, v7, v0}, Lorg/chromium/chrome/browser/signin/SigninManager;->startSignIn(Landroid/app/Activity;Landroid/accounts/Account;ZLorg/chromium/chrome/browser/signin/SigninManager$SignInFlowObserver;)V

    .line 126
    return-void

    .line 83
    :cond_0
    const/4 v0, 0x0

    move v7, v0

    goto :goto_0
.end method
