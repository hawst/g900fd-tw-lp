.class Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog$1;
.super Ljava/lang/Object;
.source "ChromeMediaRouteControllerDialog.java"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;)V
    .locals 0

    .prologue
    .line 186
    iput-object p1, p0, Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog$1;->this$0:Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 1

    .prologue
    .line 200
    if-eqz p3, :cond_0

    .line 201
    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog$1;->this$0:Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;

    # getter for: Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;->mRoute:Landroid/support/v7/media/MediaRouter$RouteInfo;
    invoke-static {v0}, Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;->access$300(Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;)Landroid/support/v7/media/MediaRouter$RouteInfo;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/support/v7/media/MediaRouter$RouteInfo;->a(I)V

    .line 203
    :cond_0
    return-void
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 2

    .prologue
    .line 189
    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog$1;->this$0:Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;->mVolumeSliderTouched:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;->access$102(Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;Z)Z

    .line 190
    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 2

    .prologue
    .line 194
    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog$1;->this$0:Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;->mVolumeSliderTouched:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;->access$102(Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;Z)Z

    .line 195
    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog$1;->this$0:Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;

    # invokes: Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;->updateVolume()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;->access$200(Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;)V

    .line 196
    return-void
.end method
