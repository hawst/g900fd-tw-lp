.class Lcom/google/android/apps/chrome/omnibox/UrlBar$AutocompleteSpan;
.super Ljava/lang/Object;
.source "UrlBar.java"


# instance fields
.field private mAutocompleteText:Ljava/lang/CharSequence;

.field private mUserText:Ljava/lang/CharSequence;

.field final synthetic this$0:Lcom/google/android/apps/chrome/omnibox/UrlBar;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/chrome/omnibox/UrlBar;)V
    .locals 0

    .prologue
    .line 1019
    iput-object p1, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar$AutocompleteSpan;->this$0:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/chrome/omnibox/UrlBar;Lcom/google/android/apps/chrome/omnibox/UrlBar$1;)V
    .locals 0

    .prologue
    .line 1019
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/omnibox/UrlBar$AutocompleteSpan;-><init>(Lcom/google/android/apps/chrome/omnibox/UrlBar;)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/omnibox/UrlBar$AutocompleteSpan;)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 1019
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar$AutocompleteSpan;->mAutocompleteText:Ljava/lang/CharSequence;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/omnibox/UrlBar$AutocompleteSpan;)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 1019
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar$AutocompleteSpan;->mUserText:Ljava/lang/CharSequence;

    return-object v0
.end method


# virtual methods
.method public clearSpan()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1042
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar$AutocompleteSpan;->this$0:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0, p0}, Landroid/text/Editable;->removeSpan(Ljava/lang/Object;)V

    .line 1043
    iput-object v1, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar$AutocompleteSpan;->mAutocompleteText:Ljava/lang/CharSequence;

    .line 1044
    iput-object v1, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar$AutocompleteSpan;->mUserText:Ljava/lang/CharSequence;

    .line 1045
    return-void
.end method

.method public setSpan(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 4

    .prologue
    .line 1029
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar$AutocompleteSpan;->this$0:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->getText()Landroid/text/Editable;

    move-result-object v0

    .line 1030
    invoke-interface {v0, p0}, Landroid/text/Editable;->removeSpan(Ljava/lang/Object;)V

    .line 1031
    iput-object p2, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar$AutocompleteSpan;->mAutocompleteText:Ljava/lang/CharSequence;

    .line 1032
    iput-object p1, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar$AutocompleteSpan;->mUserText:Ljava/lang/CharSequence;

    .line 1033
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar$AutocompleteSpan;->this$0:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->getQueryTextOffset()I

    move-result v2

    add-int/2addr v1, v2

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v2

    const/16 v3, 0x21

    invoke-interface {v0, p0, v1, v2, v3}, Landroid/text/Editable;->setSpan(Ljava/lang/Object;III)V

    .line 1038
    return-void
.end method
