.class public Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;
.super Ljava/lang/Object;
.source "ContextualSearchState.java"


# static fields
.field private static final PREVIOUS_STATES_FIRST_RUN:Ljava/util/Map;

.field private static final PREVIOUS_STATES_NORMAL:Ljava/util/Map;


# instance fields
.field private mBasePageSelectionStartYPx:F

.field private mDidSearchInvolveFirstRun:Z

.field private mFirstRunFlowContentHeight:F

.field private mHasExitedExpanded:Z

.field private mHasExitedMaximized:Z

.field private mHasExitedPeeking:Z

.field private mHasExpanded:Z

.field private mHasMaximized:Z

.field private mHeight:F

.field private mIsFirstRunFlowActive:Z

.field private mIsMaximized:Z

.field private mIsProgressBarVisible:Z

.field private mIsSearchBarBorderVisible:Z

.field private mIsSerpNavigation:Z

.field private mOffsetY:F

.field private mPanelState:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

.field private mProgressBarCompletion:I

.field private mProgressBarHeight:F

.field private mProgressBarOpacity:F

.field private mProgressBarY:F

.field private mSearchBarBorderHeight:F

.field private mSearchBarBorderY:F

.field private mSearchBarHeight:F

.field private mSearchBarMarginTop:F

.field private mSearchBarTextOpacity:F

.field private mSearchIconOpacity:F

.field private mSearchIconPaddingLeft:F

.field private mSearchProviderIconOpacity:F

.field private mSearchStartTimeNs:J

.field private mShouldHidePromoHeader:Z

.field private mWasSearchContentViewSeen:Z

.field private mWidth:F


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 54
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 56
    sget-object v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->PEEKED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    sget-object v2, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->CLOSED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 57
    sget-object v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->PROMO:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    sget-object v2, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->PEEKED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 58
    sget-object v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->EXPANDED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    sget-object v2, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->PROMO:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 59
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->PREVIOUS_STATES_FIRST_RUN:Ljava/util/Map;

    .line 65
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 67
    sget-object v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->PEEKED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    sget-object v2, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->CLOSED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 68
    sget-object v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->EXPANDED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    sget-object v2, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->PEEKED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 69
    sget-object v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->MAXIMIZED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    sget-object v2, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->EXPANDED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 70
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->PREVIOUS_STATES_NORMAL:Ljava/util/Map;

    .line 71
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    sget-object v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->UNDEFINED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    iput-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->mPanelState:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    return-void
.end method

.method private isEndingContextualSearch(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;Z)Z
    .locals 1

    .prologue
    .line 342
    invoke-direct {p0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->isOngoingContextualSearch()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->CLOSED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    if-eq p1, v0, :cond_0

    if-eqz p2, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isOngoingContextualSearch()Z
    .locals 2

    .prologue
    .line 349
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->mPanelState:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    sget-object v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->UNDEFINED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->mPanelState:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    sget-object v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->CLOSED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isStartingNewContextualSearch(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;)Z
    .locals 1

    .prologue
    .line 330
    sget-object v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->PEEKED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    if-ne p1, v0, :cond_1

    sget-object v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->TEXT_SELECT_TAP:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    if-eq p2, v0, :cond_0

    sget-object v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->TEXT_SELECT_LONG_PRESS:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    if-ne p2, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getFirstRunFlowContentHeight()F
    .locals 2

    .prologue
    .line 659
    iget v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->mFirstRunFlowContentHeight:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->mFirstRunFlowContentHeight:F

    :goto_0
    return v0

    :cond_0
    const/high16 v0, 0x437a0000    # 250.0f

    goto :goto_0
.end method

.method public getIntermediaryState()Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;
    .locals 1

    .prologue
    .line 188
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->mIsFirstRunFlowActive:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->PROMO:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->EXPANDED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    goto :goto_0
.end method

.method public getMaximumState()Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;
    .locals 1

    .prologue
    .line 180
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->mIsFirstRunFlowActive:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->PROMO:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->MAXIMIZED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    goto :goto_0
.end method

.method public getOffsetY()F
    .locals 1

    .prologue
    .line 356
    iget v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->mOffsetY:F

    return v0
.end method

.method public getPanelState()Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->mPanelState:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    return-object v0
.end method

.method public getPreviousPanelState(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;)Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;
    .locals 1

    .prologue
    .line 169
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->mIsFirstRunFlowActive:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->PREVIOUS_STATES_FIRST_RUN:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    .line 172
    :goto_0
    if-eqz v0, :cond_1

    :goto_1
    return-object v0

    .line 169
    :cond_0
    sget-object v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->PREVIOUS_STATES_NORMAL:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    goto :goto_0

    .line 172
    :cond_1
    sget-object v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->UNDEFINED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    goto :goto_1
.end method

.method public getProgressBarCompletion()I
    .locals 1

    .prologue
    .line 634
    iget v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->mProgressBarCompletion:I

    return v0
.end method

.method public getProgressBarHeight()F
    .locals 1

    .prologue
    .line 606
    iget v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->mProgressBarHeight:F

    return v0
.end method

.method public getProgressBarOpacity()F
    .locals 1

    .prologue
    .line 620
    iget v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->mProgressBarOpacity:F

    return v0
.end method

.method public getProgressBarY()F
    .locals 1

    .prologue
    .line 592
    iget v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->mProgressBarY:F

    return v0
.end method

.method public getSearchBarBorderHeight()F
    .locals 1

    .prologue
    .line 486
    iget v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->mSearchBarBorderHeight:F

    return v0
.end method

.method public getSearchBarBorderY()F
    .locals 1

    .prologue
    .line 472
    iget v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->mSearchBarBorderY:F

    return v0
.end method

.method public getSearchBarHeight()F
    .locals 1

    .prologue
    .line 430
    iget v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->mSearchBarHeight:F

    return v0
.end method

.method public getSearchBarMarginTop()F
    .locals 1

    .prologue
    .line 416
    iget v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->mSearchBarMarginTop:F

    return v0
.end method

.method public getSearchBarTextOpacity()F
    .locals 1

    .prologue
    .line 444
    iget v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->mSearchBarTextOpacity:F

    return v0
.end method

.method public getSearchContentViewOffsetY()F
    .locals 2

    .prologue
    .line 151
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->getOffsetY()F

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->getSearchBarHeight()F

    move-result v1

    add-float/2addr v0, v1

    return v0
.end method

.method public getSearchIconOpacity()F
    .locals 1

    .prologue
    .line 528
    iget v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->mSearchIconOpacity:F

    return v0
.end method

.method public getSearchIconPaddingLeft()F
    .locals 1

    .prologue
    .line 514
    iget v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->mSearchIconPaddingLeft:F

    return v0
.end method

.method public getSearchProviderIconOpacity()F
    .locals 1

    .prologue
    .line 500
    iget v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->mSearchProviderIconOpacity:F

    return v0
.end method

.method public getWidth()F
    .locals 1

    .prologue
    .line 384
    iget v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->mWidth:F

    return v0
.end method

.method public isMaximized()Z
    .locals 1

    .prologue
    .line 398
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->mIsMaximized:Z

    return v0
.end method

.method public isProgressBarVisible()Z
    .locals 1

    .prologue
    .line 578
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->mIsProgressBarVisible:Z

    return v0
.end method

.method public isSearchBarBorderVisible()Z
    .locals 1

    .prologue
    .line 458
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->mIsSearchBarBorderVisible:Z

    return v0
.end method

.method public isValidState(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;)Z
    .locals 2

    .prologue
    .line 290
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->mIsFirstRunFlowActive:Z

    if-eqz v0, :cond_0

    .line 291
    new-instance v0, Ljava/util/ArrayList;

    sget-object v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->PREVIOUS_STATES_FIRST_RUN:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 298
    :goto_0
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0

    .line 293
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    sget-object v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->PREVIOUS_STATES_NORMAL:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 295
    sget-object v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->MAXIMIZED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public isYCoordinateInsideBasePage(F)Z
    .locals 1

    .prologue
    .line 128
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->getOffsetY()F

    move-result v0

    cmpg-float v0, p1, v0

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isYCoordinateInsideSearchBar(F)Z
    .locals 1

    .prologue
    .line 136
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->isYCoordinateInsideBasePage(F)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->isYCoordinateInsideSearchContentView(F)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isYCoordinateInsideSearchContentView(F)Z
    .locals 1

    .prologue
    .line 144
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->getSearchContentViewOffsetY()F

    move-result v0

    cmpl-float v0, p1, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setBasePageSelectionStartYPx(F)V
    .locals 0

    .prologue
    .line 567
    iput p1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->mBasePageSelectionStartYPx:F

    .line 568
    return-void
.end method

.method public setDidSearchInvolveFirstRun()V
    .locals 1

    .prologue
    .line 313
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->mDidSearchInvolveFirstRun:Z

    .line 314
    return-void
.end method

.method public setFirstRunFlowContentHeight(F)V
    .locals 0

    .prologue
    .line 652
    iput p1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->mFirstRunFlowContentHeight:F

    .line 653
    return-void
.end method

.method public setHeight(F)V
    .locals 0

    .prologue
    .line 377
    iput p1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->mHeight:F

    .line 378
    return-void
.end method

.method public setIsFirstRunFlowActive(Z)V
    .locals 0

    .prologue
    .line 667
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->mIsFirstRunFlowActive:Z

    .line 668
    return-void
.end method

.method public setMaximized(Z)V
    .locals 0

    .prologue
    .line 405
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->mIsMaximized:Z

    .line 406
    return-void
.end method

.method public setOffsetY(F)V
    .locals 0

    .prologue
    .line 363
    iput p1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->mOffsetY:F

    .line 364
    return-void
.end method

.method public setPanelState(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;)V
    .locals 12

    .prologue
    .line 199
    iget-object v5, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->mPanelState:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    .line 200
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->isStartingNewContextualSearch(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;)Z

    move-result v6

    .line 201
    invoke-direct {p0, p1, v6}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->isEndingContextualSearch(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;Z)Z

    move-result v7

    .line 202
    if-eqz v6, :cond_e

    invoke-direct {p0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->isOngoingContextualSearch()Z

    move-result v0

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    move v4, v0

    .line 203
    :goto_0
    if-ne v5, p1, :cond_f

    const/4 v0, 0x1

    .line 204
    :goto_1
    sget-object v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->PEEKED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    if-ne v5, v1, :cond_10

    iget-boolean v1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->mHasExitedPeeking:Z

    if-nez v1, :cond_10

    if-eqz v0, :cond_0

    if-eqz v6, :cond_10

    :cond_0
    const/4 v1, 0x1

    .line 206
    :goto_2
    sget-object v2, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->EXPANDED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    if-ne v5, v2, :cond_11

    iget-boolean v2, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->mHasExitedExpanded:Z

    if-nez v2, :cond_11

    if-nez v0, :cond_11

    const/4 v2, 0x1

    .line 208
    :goto_3
    sget-object v3, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->MAXIMIZED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    if-ne v5, v3, :cond_12

    iget-boolean v3, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->mHasExitedMaximized:Z

    if-nez v3, :cond_12

    if-nez v0, :cond_12

    const/4 v0, 0x1

    move v3, v0

    .line 211
    :goto_4
    if-eqz v7, :cond_2

    .line 212
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->mDidSearchInvolveFirstRun:Z

    if-nez v0, :cond_1

    .line 214
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v8

    iget-wide v10, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->mSearchStartTimeNs:J

    sub-long/2addr v8, v10

    const-wide/32 v10, 0xf4240

    div-long/2addr v8, v10

    .line 215
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->mWasSearchContentViewSeen:Z

    invoke-static {v0, v4, v8, v9}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma;->logDuration(ZZJ)V

    .line 217
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->mIsFirstRunFlowActive:Z

    if-eqz v0, :cond_13

    .line 219
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->mWasSearchContentViewSeen:Z

    invoke-static {v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma;->logFirstRunPanelSeen(Z)V

    .line 224
    :cond_2
    :goto_5
    if-eqz v6, :cond_3

    .line 225
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v8

    iput-wide v8, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->mSearchStartTimeNs:J

    .line 234
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->mIsSerpNavigation:Z

    if-eqz v0, :cond_14

    sget-object v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->SERP_NAVIGATION:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    .line 236
    :goto_6
    if-nez v6, :cond_5

    if-nez v7, :cond_5

    iget-boolean v8, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->mHasExpanded:Z

    if-nez v8, :cond_4

    sget-object v8, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->EXPANDED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    if-eq p1, v8, :cond_5

    :cond_4
    iget-boolean v8, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->mHasMaximized:Z

    if-nez v8, :cond_6

    sget-object v8, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->MAXIMIZED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    if-ne p1, v8, :cond_6

    .line 239
    :cond_5
    invoke-static {v5, p1, v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma;->logFirstStateEntry(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;)V

    .line 243
    :cond_6
    if-eqz v6, :cond_7

    if-eqz v4, :cond_8

    :cond_7
    if-nez v1, :cond_8

    if-nez v2, :cond_8

    if-eqz v3, :cond_9

    .line 245
    :cond_8
    invoke-static {v5, p1, v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma;->logFirstStateExit(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;)V

    .line 249
    :cond_9
    if-eqz v1, :cond_15

    .line 250
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->mHasExitedPeeking:Z

    .line 257
    :cond_a
    :goto_7
    iput-object p1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->mPanelState:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    .line 259
    sget-object v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->EXPANDED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    if-ne p1, v0, :cond_17

    .line 260
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->mHasExpanded:Z

    .line 264
    :cond_b
    :goto_8
    sget-object v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->SERP_NAVIGATION:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    if-ne p2, v0, :cond_c

    .line 265
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->mIsSerpNavigation:Z

    .line 268
    :cond_c
    if-eqz v7, :cond_d

    .line 269
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->mDidSearchInvolveFirstRun:Z

    .line 270
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->mWasSearchContentViewSeen:Z

    .line 271
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->mHasExpanded:Z

    .line 272
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->mHasMaximized:Z

    .line 273
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->mHasExitedPeeking:Z

    .line 274
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->mHasExitedExpanded:Z

    .line 275
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->mHasExitedMaximized:Z

    .line 276
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->mIsSerpNavigation:Z

    .line 281
    :cond_d
    return-void

    .line 202
    :cond_e
    const/4 v0, 0x0

    move v4, v0

    goto/16 :goto_0

    .line 203
    :cond_f
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 204
    :cond_10
    const/4 v1, 0x0

    goto/16 :goto_2

    .line 206
    :cond_11
    const/4 v2, 0x0

    goto/16 :goto_3

    .line 208
    :cond_12
    const/4 v0, 0x0

    move v3, v0

    goto/16 :goto_4

    .line 221
    :cond_13
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->mWasSearchContentViewSeen:Z

    invoke-static {v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma;->logResultsSeen(Z)V

    goto :goto_5

    :cond_14
    move-object v0, p2

    .line 234
    goto :goto_6

    .line 251
    :cond_15
    if-eqz v2, :cond_16

    .line 252
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->mHasExitedExpanded:Z

    goto :goto_7

    .line 253
    :cond_16
    if-eqz v3, :cond_a

    .line 254
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->mHasExitedMaximized:Z

    goto :goto_7

    .line 261
    :cond_17
    sget-object v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->MAXIMIZED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    if-ne p1, v0, :cond_b

    .line 262
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->mHasMaximized:Z

    goto :goto_8
.end method

.method public setProgressBarCompletion(I)V
    .locals 0

    .prologue
    .line 641
    iput p1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->mProgressBarCompletion:I

    .line 642
    return-void
.end method

.method public setProgressBarHeight(F)V
    .locals 0

    .prologue
    .line 613
    iput p1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->mProgressBarHeight:F

    .line 614
    return-void
.end method

.method public setProgressBarOpacity(F)V
    .locals 0

    .prologue
    .line 627
    iput p1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->mProgressBarOpacity:F

    .line 628
    return-void
.end method

.method public setProgressBarVisible(Z)V
    .locals 0

    .prologue
    .line 585
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->mIsProgressBarVisible:Z

    .line 586
    return-void
.end method

.method public setProgressBarY(F)V
    .locals 0

    .prologue
    .line 599
    iput p1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->mProgressBarY:F

    .line 600
    return-void
.end method

.method public setSearchBarBorderHeight(F)V
    .locals 0

    .prologue
    .line 493
    iput p1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->mSearchBarBorderHeight:F

    .line 494
    return-void
.end method

.method public setSearchBarBorderVisible(Z)V
    .locals 0

    .prologue
    .line 465
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->mIsSearchBarBorderVisible:Z

    .line 466
    return-void
.end method

.method public setSearchBarBorderY(F)V
    .locals 0

    .prologue
    .line 479
    iput p1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->mSearchBarBorderY:F

    .line 480
    return-void
.end method

.method public setSearchBarHeight(F)V
    .locals 0

    .prologue
    .line 437
    iput p1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->mSearchBarHeight:F

    .line 438
    return-void
.end method

.method public setSearchBarMarginTop(F)V
    .locals 0

    .prologue
    .line 423
    iput p1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->mSearchBarMarginTop:F

    .line 424
    return-void
.end method

.method public setSearchBarTextOpacity(F)V
    .locals 0

    .prologue
    .line 451
    iput p1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->mSearchBarTextOpacity:F

    .line 452
    return-void
.end method

.method public setSearchIconOpacity(F)V
    .locals 0

    .prologue
    .line 535
    iput p1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->mSearchIconOpacity:F

    .line 536
    return-void
.end method

.method public setSearchIconPaddingLeft(F)V
    .locals 0

    .prologue
    .line 521
    iput p1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->mSearchIconPaddingLeft:F

    .line 522
    return-void
.end method

.method public setSearchProviderIconOpacity(F)V
    .locals 0

    .prologue
    .line 507
    iput p1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->mSearchProviderIconOpacity:F

    .line 508
    return-void
.end method

.method public setShouldHidePromoHeader(Z)V
    .locals 0

    .prologue
    .line 681
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->mShouldHidePromoHeader:Z

    .line 682
    return-void
.end method

.method public setWasSearchContentViewSeen()V
    .locals 1

    .prologue
    .line 320
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->mWasSearchContentViewSeen:Z

    .line 321
    return-void
.end method

.method public setWidth(F)V
    .locals 0

    .prologue
    .line 391
    iput p1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->mWidth:F

    .line 392
    return-void
.end method

.method public shouldHidePromoHeader()Z
    .locals 1

    .prologue
    .line 688
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->mShouldHidePromoHeader:Z

    return v0
.end method
