.class public Lcom/google/android/apps/chrome/CustomSelectionActionModeCallback;
.super Ljava/lang/Object;
.source "CustomSelectionActionModeCallback.java"

# interfaces
.implements Landroid/view/ActionMode$Callback;


# instance fields
.field private mContextualMenuBar:Lcom/google/android/apps/chrome/ContextualMenuBar;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onActionItemClicked(Landroid/view/ActionMode;Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 43
    const/4 v0, 0x0

    return v0
.end method

.method public onCreateActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/apps/chrome/CustomSelectionActionModeCallback;->mContextualMenuBar:Lcom/google/android/apps/chrome/ContextualMenuBar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ContextualMenuBar;->showControls()V

    .line 38
    const/4 v0, 0x1

    return v0
.end method

.method public onDestroyActionMode(Landroid/view/ActionMode;)V
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/android/apps/chrome/CustomSelectionActionModeCallback;->mContextualMenuBar:Lcom/google/android/apps/chrome/ContextualMenuBar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ContextualMenuBar;->hideControls()V

    .line 33
    return-void
.end method

.method public onPrepareActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 27
    const/4 v0, 0x1

    return v0
.end method

.method public setContextualMenuBar(Lcom/google/android/apps/chrome/ContextualMenuBar;)V
    .locals 0

    .prologue
    .line 22
    iput-object p1, p0, Lcom/google/android/apps/chrome/CustomSelectionActionModeCallback;->mContextualMenuBar:Lcom/google/android/apps/chrome/ContextualMenuBar;

    .line 23
    return-void
.end method
