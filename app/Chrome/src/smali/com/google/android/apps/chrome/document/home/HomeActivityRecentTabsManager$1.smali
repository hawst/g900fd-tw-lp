.class Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager$1;
.super Lcom/google/android/apps/chrome/document/DocumentTabList$InitializationObserver;
.source "HomeActivityRecentTabsManager.java"


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;Lcom/google/android/apps/chrome/document/DocumentTabList;)V
    .locals 0

    .prologue
    .line 63
    iput-object p1, p0, Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager$1;->this$0:Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;

    invoke-direct {p0, p2}, Lcom/google/android/apps/chrome/document/DocumentTabList$InitializationObserver;-><init>(Lcom/google/android/apps/chrome/document/DocumentTabList;)V

    return-void
.end method


# virtual methods
.method public isCanceled()Z
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager$1;->this$0:Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;

    # getter for: Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;->access$100(Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->isDestroyed()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager$1;->this$0:Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;

    # getter for: Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;->access$100(Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSatisfied()Z
    .locals 2

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager$1;->this$0:Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;

    # getter for: Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;->mTabList:Lcom/google/android/apps/chrome/document/DocumentTabList;
    invoke-static {v0}, Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;->access$000(Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;)Lcom/google/android/apps/chrome/document/DocumentTabList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/document/DocumentTabList;->getCurrentState()I

    move-result v0

    const/16 v1, 0xa

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected runImmediately()V
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager$1;->this$0:Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;

    # invokes: Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;->updateCurrentlyOpenTabsWhenDatabaseReady()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;->access$200(Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;)V

    .line 77
    return-void
.end method
