.class Lcom/google/android/apps/chrome/tab/ChromeTab$ChromeTabChromeContextMenuPopulator;
.super Lorg/chromium/chrome/browser/contextmenu/ChromeContextMenuPopulator;
.source "ChromeTab.java"


# instance fields
.field private final mDelegate:Lcom/google/android/apps/chrome/tab/ChromeTab$ChromeTabChromeContextMenuItemDelegate;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/chrome/tab/ChromeTab$ChromeTabChromeContextMenuItemDelegate;)V
    .locals 0

    .prologue
    .line 1132
    invoke-direct {p0, p1}, Lorg/chromium/chrome/browser/contextmenu/ChromeContextMenuPopulator;-><init>(Lorg/chromium/chrome/browser/contextmenu/ChromeContextMenuItemDelegate;)V

    .line 1134
    iput-object p1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$ChromeTabChromeContextMenuPopulator;->mDelegate:Lcom/google/android/apps/chrome/tab/ChromeTab$ChromeTabChromeContextMenuItemDelegate;

    .line 1135
    return-void
.end method


# virtual methods
.method public buildContextMenu(Landroid/view/ContextMenu;Landroid/content/Context;Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;)V
    .locals 4

    .prologue
    .line 1140
    invoke-virtual {p3}, Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;->isImage()Z

    move-result v0

    invoke-virtual {p3}, Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;->isAnchor()Z

    move-result v1

    invoke-virtual {p3}, Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;->isSelectedText()Z

    move-result v2

    invoke-virtual {p3}, Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;->isVideo()Z

    move-result v3

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->recordShowContextMenu(ZZZZ)V

    .line 1143
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$ChromeTabChromeContextMenuPopulator;->mDelegate:Lcom/google/android/apps/chrome/tab/ChromeTab$ChromeTabChromeContextMenuItemDelegate;

    invoke-virtual {p3}, Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;->isImage()Z

    move-result v1

    invoke-virtual {p3}, Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;->isVideo()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/tab/ChromeTab$ChromeTabChromeContextMenuItemDelegate;->setParamsInfo(ZZ)V

    .line 1144
    invoke-super {p0, p1, p2, p3}, Lorg/chromium/chrome/browser/contextmenu/ChromeContextMenuPopulator;->buildContextMenu(Landroid/view/ContextMenu;Landroid/content/Context;Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;)V

    .line 1145
    return-void
.end method
