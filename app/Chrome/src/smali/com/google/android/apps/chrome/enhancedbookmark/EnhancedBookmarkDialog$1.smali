.class Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog$1;
.super Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkModelObserver;
.source "EnhancedBookmarkDialog.java"


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;)V
    .locals 0

    .prologue
    .line 72
    iput-object p1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog$1;->this$0:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;

    invoke-direct {p0}, Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkModelObserver;-><init>()V

    return-void
.end method


# virtual methods
.method public bookmarkModelChanged()V
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog$1;->this$0:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;

    # invokes: Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;->bookmarkModelRefreshed()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;->access$500(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;)V

    .line 97
    return-void
.end method

.method public bookmarkModelLoaded()V
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog$1;->this$0:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;

    # getter for: Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;->mIsInitializedWithBookmarksBridge:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;->access$300(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 90
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog$1;->this$0:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;

    # invokes: Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;->initializeWithBookmarksBridge()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;->access$400(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;)V

    .line 92
    :cond_0
    return-void
.end method

.method public bookmarkNodeRemoved(Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;ILorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 77
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog$1;->this$0:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;

    # getter for: Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;->mMode:I
    invoke-static {v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;->access$000(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;)I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    invoke-virtual {p3}, Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;->getId()Lorg/chromium/components/bookmarks/BookmarkId;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog$1;->this$0:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;

    # getter for: Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;->mModeDetail:Ljava/lang/Object;
    invoke-static {v1}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;->access$100(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/chromium/components/bookmarks/BookmarkId;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 78
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog$1;->this$0:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;

    # getter for: Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;->mEnhancedBookmarksModel:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;
    invoke-static {v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;->access$200(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;)Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;

    move-result-object v0

    invoke-virtual {v0, v2, v2}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;->getTopLevelFolderIDs(ZZ)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1}, Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;->getId()Lorg/chromium/components/bookmarks/BookmarkId;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 80
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog$1;->this$0:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;->setAllBookmarksMode()V

    .line 85
    :cond_0
    :goto_0
    return-void

    .line 82
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog$1;->this$0:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;

    invoke-virtual {p1}, Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;->getId()Lorg/chromium/components/bookmarks/BookmarkId;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;->setFolderMode(Lorg/chromium/components/bookmarks/BookmarkId;)V

    goto :goto_0
.end method
