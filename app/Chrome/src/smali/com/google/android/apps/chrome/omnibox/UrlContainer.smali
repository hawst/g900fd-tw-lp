.class public Lcom/google/android/apps/chrome/omnibox/UrlContainer;
.super Landroid/view/ViewGroup;
.source "UrlContainer.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private mLastShowRequestTime:J

.field private mShowTrailingText:Z

.field private mTrailingTextAnimator:Landroid/animation/Animator;

.field private mTrailingTextView:Landroid/widget/TextView;

.field private final mTriggerHideAnimationRunnable:Ljava/lang/Runnable;

.field private final mTriggerHideRunnable:Ljava/lang/Runnable;

.field private mUrlBarTextWidth:I

.field private mUrlBarView:Lcom/google/android/apps/chrome/omnibox/UrlBar;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const-class v0, Lcom/google/android/apps/chrome/omnibox/UrlContainer;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/omnibox/UrlContainer;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 53
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 55
    new-instance v0, Lcom/google/android/apps/chrome/omnibox/UrlContainer$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/omnibox/UrlContainer$1;-><init>(Lcom/google/android/apps/chrome/omnibox/UrlContainer;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/omnibox/UrlContainer;->mTriggerHideRunnable:Ljava/lang/Runnable;

    .line 61
    new-instance v0, Lcom/google/android/apps/chrome/omnibox/UrlContainer$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/omnibox/UrlContainer$2;-><init>(Lcom/google/android/apps/chrome/omnibox/UrlContainer;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/omnibox/UrlContainer;->mTriggerHideAnimationRunnable:Ljava/lang/Runnable;

    .line 67
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/omnibox/UrlContainer;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/google/android/apps/chrome/omnibox/UrlContainer;->hideTrailingText()V

    return-void
.end method

.method static synthetic access$102(Lcom/google/android/apps/chrome/omnibox/UrlContainer;Landroid/animation/Animator;)Landroid/animation/Animator;
    .locals 0

    .prologue
    .line 32
    iput-object p1, p0, Lcom/google/android/apps/chrome/omnibox/UrlContainer;->mTrailingTextAnimator:Landroid/animation/Animator;

    return-object p1
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/omnibox/UrlContainer;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/UrlContainer;->mTrailingTextView:Landroid/widget/TextView;

    return-object v0
.end method

.method private hideTrailingText()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 228
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/UrlContainer;->mTrailingTextAnimator:Landroid/animation/Animator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/UrlContainer;->mTrailingTextAnimator:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 229
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/UrlContainer;->mTrailingTextAnimator:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->cancel()V

    .line 232
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/UrlContainer;->mUrlBarView:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->setAccessibilityTextOverride(Ljava/lang/String;)V

    .line 234
    const/high16 v0, 0x42480000    # 50.0f

    invoke-static {p0}, Lorg/chromium/base/ApiCompatibilityUtils;->isLayoutRtl(Landroid/view/View;)Z

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/utilities/ChromeMathUtils;->flipSignIf(FZ)F

    move-result v0

    .line 237
    new-instance v1, Landroid/animation/AnimatorSet;

    invoke-direct {v1}, Landroid/animation/AnimatorSet;-><init>()V

    .line 238
    const/4 v2, 0x2

    new-array v2, v2, [Landroid/animation/Animator;

    iget-object v3, p0, Lcom/google/android/apps/chrome/omnibox/UrlContainer;->mTrailingTextView:Landroid/widget/TextView;

    sget-object v4, Lcom/google/android/apps/chrome/omnibox/UrlContainer;->ALPHA:Landroid/util/Property;

    new-array v5, v8, [F

    const/4 v6, 0x0

    aput v6, v5, v7

    invoke-static {v3, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    aput-object v3, v2, v7

    iget-object v3, p0, Lcom/google/android/apps/chrome/omnibox/UrlContainer;->mTrailingTextView:Landroid/widget/TextView;

    sget-object v4, Lcom/google/android/apps/chrome/omnibox/UrlContainer;->TRANSLATION_X:Landroid/util/Property;

    new-array v5, v8, [F

    aput v0, v5, v7

    invoke-static {v3, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    aput-object v0, v2, v8

    invoke-virtual {v1, v2}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 241
    const-wide/16 v2, 0xc8

    invoke-virtual {v1, v2, v3}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 242
    sget-object v0, Lorg/chromium/ui/interpolators/BakedBezierInterpolator;->FADE_OUT_CURVE:Lorg/chromium/ui/interpolators/BakedBezierInterpolator;

    invoke-virtual {v1, v0}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 243
    new-instance v0, Lcom/google/android/apps/chrome/omnibox/UrlContainer$4;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/omnibox/UrlContainer$4;-><init>(Lcom/google/android/apps/chrome/omnibox/UrlContainer;)V

    invoke-virtual {v1, v0}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 251
    invoke-virtual {v1}, Landroid/animation/AnimatorSet;->start()V

    .line 252
    iput-object v1, p0, Lcom/google/android/apps/chrome/omnibox/UrlContainer;->mTrailingTextAnimator:Landroid/animation/Animator;

    .line 253
    return-void
.end method

.method private static layoutChild(Landroid/view/View;II)V
    .locals 3

    .prologue
    .line 140
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    sub-int v0, p1, v0

    div-int/lit8 v0, v0, 0x2

    .line 141
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    add-int/2addr v1, p2

    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    add-int/2addr v2, v0

    invoke-virtual {p0, p2, v0, v1, v2}, Landroid/view/View;->layout(IIII)V

    .line 146
    return-void
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 71
    invoke-super {p0}, Landroid/view/ViewGroup;->onFinishInflate()V

    .line 73
    sget v0, Lcom/google/android/apps/chrome/R$id;->url_bar:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/omnibox/UrlContainer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/omnibox/UrlBar;

    iput-object v0, p0, Lcom/google/android/apps/chrome/omnibox/UrlContainer;->mUrlBarView:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    .line 74
    sget-boolean v0, Lcom/google/android/apps/chrome/omnibox/UrlContainer;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/UrlContainer;->mUrlBarView:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    const-string/jumbo v1, "url_bar is not defined as a child."

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 76
    :cond_0
    sget v0, Lcom/google/android/apps/chrome/R$id;->trailing_text:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/omnibox/UrlContainer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/chrome/omnibox/UrlContainer;->mTrailingTextView:Landroid/widget/TextView;

    .line 77
    sget-boolean v0, Lcom/google/android/apps/chrome/omnibox/UrlContainer;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/UrlContainer;->mTrailingTextView:Landroid/widget/TextView;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    const-string/jumbo v1, "trailing_text is not defined as a child."

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 79
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/omnibox/UrlContainer;->setClickable(Z)V

    .line 80
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 129
    sub-int v0, p5, p3

    .line 131
    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/UrlContainer;->mUrlBarView:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    invoke-static {v1, v0, v2}, Lcom/google/android/apps/chrome/omnibox/UrlContainer;->layoutChild(Landroid/view/View;II)V

    .line 132
    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/UrlContainer;->mUrlBarView:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    invoke-static {v1}, Lorg/chromium/base/ApiCompatibilityUtils;->isLayoutRtl(Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 133
    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/UrlContainer;->mTrailingTextView:Landroid/widget/TextView;

    invoke-static {v1, v0, v2}, Lcom/google/android/apps/chrome/omnibox/UrlContainer;->layoutChild(Landroid/view/View;II)V

    .line 137
    :goto_0
    return-void

    .line 135
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/UrlContainer;->mTrailingTextView:Landroid/widget/TextView;

    iget v2, p0, Lcom/google/android/apps/chrome/omnibox/UrlContainer;->mUrlBarTextWidth:I

    invoke-static {v1, v0, v2}, Lcom/google/android/apps/chrome/omnibox/UrlContainer;->layoutChild(Landroid/view/View;II)V

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 5

    .prologue
    const/high16 v4, 0x40000000    # 2.0f

    .line 84
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->onMeasure(II)V

    .line 86
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 87
    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/UrlContainer;->mUrlBarView:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    invoke-static {v0, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-virtual {v1, v2, p2}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->measure(II)V

    .line 90
    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/UrlContainer;->mUrlBarView:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 91
    iput v0, p0, Lcom/google/android/apps/chrome/omnibox/UrlContainer;->mUrlBarTextWidth:I

    .line 98
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/UrlContainer;->mTrailingTextView:Landroid/widget/TextView;

    iget v2, p0, Lcom/google/android/apps/chrome/omnibox/UrlContainer;->mUrlBarTextWidth:I

    sub-int/2addr v0, v2

    invoke-static {v0, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-virtual {v1, v0, p2}, Landroid/widget/TextView;->measure(II)V

    .line 101
    return-void

    .line 92
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/UrlContainer;->mUrlBarView:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->hasFocus()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 93
    iput v0, p0, Lcom/google/android/apps/chrome/omnibox/UrlContainer;->mUrlBarTextWidth:I

    goto :goto_0

    .line 95
    :cond_1
    int-to-float v1, v0

    iget-object v2, p0, Lcom/google/android/apps/chrome/omnibox/UrlContainer;->mUrlBarView:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->getText()Landroid/text/Editable;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/chrome/omnibox/UrlContainer;->mUrlBarView:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->getPaint()Landroid/text/TextPaint;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/text/Layout;->getDesiredWidth(Ljava/lang/CharSequence;Landroid/text/TextPaint;)F

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v1

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v1, v2

    iput v1, p0, Lcom/google/android/apps/chrome/omnibox/UrlContainer;->mUrlBarTextWidth:I

    goto :goto_0
.end method

.method public onRtlPropertiesChanged(I)V
    .locals 2

    .prologue
    .line 106
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onRtlPropertiesChanged(I)V

    .line 110
    packed-switch p1, :pswitch_data_0

    .line 122
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/UrlContainer;->mTrailingTextView:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lorg/chromium/base/ApiCompatibilityUtils;->setTextDirection(Landroid/view/View;I)V

    .line 125
    :goto_0
    return-void

    .line 112
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/UrlContainer;->mTrailingTextView:Landroid/widget/TextView;

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lorg/chromium/base/ApiCompatibilityUtils;->setTextDirection(Landroid/view/View;I)V

    goto :goto_0

    .line 115
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/UrlContainer;->mTrailingTextView:Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lorg/chromium/base/ApiCompatibilityUtils;->setTextDirection(Landroid/view/View;I)V

    goto :goto_0

    .line 118
    :pswitch_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/UrlContainer;->mTrailingTextView:Landroid/widget/TextView;

    const/4 v1, 0x5

    invoke-static {v0, v1}, Lorg/chromium/base/ApiCompatibilityUtils;->setTextDirection(Landroid/view/View;I)V

    goto :goto_0

    .line 110
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public setTrailingTextVisible(Z)V
    .locals 9

    .prologue
    const-wide/16 v4, 0xbb8

    const/4 v3, 0x2

    const/4 v8, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 172
    if-eqz p1, :cond_0

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/chrome/omnibox/UrlContainer;->mLastShowRequestTime:J

    .line 174
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/omnibox/UrlContainer;->mShowTrailingText:Z

    if-ne p1, v0, :cond_1

    .line 225
    :goto_0
    return-void

    .line 175
    :cond_1
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/omnibox/UrlContainer;->mShowTrailingText:Z

    .line 177
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/UrlContainer;->mTriggerHideRunnable:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/omnibox/UrlContainer;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 178
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/UrlContainer;->mTriggerHideAnimationRunnable:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/omnibox/UrlContainer;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 180
    if-eqz p1, :cond_4

    .line 181
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/UrlContainer;->mTrailingTextAnimator:Landroid/animation/Animator;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/UrlContainer;->mTrailingTextAnimator:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 182
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/UrlContainer;->mTrailingTextAnimator:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->cancel()V

    .line 191
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/UrlContainer;->mTrailingTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 193
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/UrlContainer;->mTrailingTextView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 194
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/UrlContainer;->mUrlBarView:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    new-array v1, v3, [Ljava/lang/CharSequence;

    iget-object v2, p0, Lcom/google/android/apps/chrome/omnibox/UrlContainer;->mUrlBarView:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->getText()Landroid/text/Editable;

    move-result-object v2

    aput-object v2, v1, v6

    iget-object v2, p0, Lcom/google/android/apps/chrome/omnibox/UrlContainer;->mTrailingTextView:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    aput-object v2, v1, v7

    invoke-static {v1}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->setAccessibilityTextOverride(Ljava/lang/String;)V

    .line 199
    :cond_2
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    .line 200
    new-array v1, v3, [Landroid/animation/Animator;

    iget-object v2, p0, Lcom/google/android/apps/chrome/omnibox/UrlContainer;->mTrailingTextView:Landroid/widget/TextView;

    sget-object v3, Lcom/google/android/apps/chrome/omnibox/UrlContainer;->ALPHA:Landroid/util/Property;

    new-array v4, v7, [F

    const/high16 v5, 0x3f800000    # 1.0f

    aput v5, v4, v6

    invoke-static {v2, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    aput-object v2, v1, v6

    iget-object v2, p0, Lcom/google/android/apps/chrome/omnibox/UrlContainer;->mTrailingTextView:Landroid/widget/TextView;

    sget-object v3, Lcom/google/android/apps/chrome/omnibox/UrlContainer;->TRANSLATION_X:Landroid/util/Property;

    new-array v4, v7, [F

    aput v8, v4, v6

    invoke-static {v2, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    aput-object v2, v1, v7

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 203
    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 204
    sget-object v1, Lorg/chromium/ui/interpolators/BakedBezierInterpolator;->FADE_IN_CURVE:Lorg/chromium/ui/interpolators/BakedBezierInterpolator;

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 205
    new-instance v1, Lcom/google/android/apps/chrome/omnibox/UrlContainer$3;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/omnibox/UrlContainer$3;-><init>(Lcom/google/android/apps/chrome/omnibox/UrlContainer;)V

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 212
    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    .line 213
    iput-object v0, p0, Lcom/google/android/apps/chrome/omnibox/UrlContainer;->mTrailingTextAnimator:Landroid/animation/Animator;

    .line 215
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/UrlContainer;->mTriggerHideRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1770

    invoke-virtual {p0, v0, v2, v3}, Lcom/google/android/apps/chrome/omnibox/UrlContainer;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 184
    :cond_3
    const/high16 v0, 0x42480000    # 50.0f

    invoke-static {p0}, Lorg/chromium/base/ApiCompatibilityUtils;->isLayoutRtl(Landroid/view/View;)Z

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/utilities/ChromeMathUtils;->flipSignIf(FZ)F

    move-result v0

    .line 186
    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/UrlContainer;->mTrailingTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v8}, Landroid/widget/TextView;->setAlpha(F)V

    .line 187
    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/UrlContainer;->mTrailingTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTranslationX(F)V

    goto/16 :goto_1

    .line 217
    :cond_4
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/apps/chrome/omnibox/UrlContainer;->mLastShowRequestTime:J

    sub-long/2addr v0, v2

    .line 218
    cmp-long v2, v0, v4

    if-ltz v2, :cond_5

    .line 219
    invoke-direct {p0}, Lcom/google/android/apps/chrome/omnibox/UrlContainer;->hideTrailingText()V

    goto/16 :goto_0

    .line 221
    :cond_5
    iget-object v2, p0, Lcom/google/android/apps/chrome/omnibox/UrlContainer;->mTriggerHideAnimationRunnable:Ljava/lang/Runnable;

    sub-long v0, v4, v0

    invoke-virtual {p0, v2, v0, v1}, Lcom/google/android/apps/chrome/omnibox/UrlContainer;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0
.end method

.method public setUrlText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 158
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/UrlContainer;->mUrlBarView:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    invoke-virtual {v0, p3, p1}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->setUrl(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    .line 159
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/UrlContainer;->mTrailingTextView:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/UrlContainer;->requestLayout()V

    .line 160
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/UrlContainer;->mTrailingTextView:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v1, p2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 161
    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/UrlContainer;->mTrailingTextView:Landroid/widget/TextView;

    invoke-virtual {v1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 162
    or-int/lit8 v0, v0, 0x1

    .line 164
    :cond_1
    return v0
.end method
