.class Lcom/google/android/apps/chrome/promoscreen/SigninPromoScreen$1;
.super Ljava/lang/Object;
.source "SigninPromoScreen.java"

# interfaces
.implements Lorg/chromium/chrome/browser/signin/SigninManager$SignInFlowObserver;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/promoscreen/SigninPromoScreen;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/promoscreen/SigninPromoScreen;)V
    .locals 0

    .prologue
    .line 101
    iput-object p1, p0, Lcom/google/android/apps/chrome/promoscreen/SigninPromoScreen$1;->this$0:Lcom/google/android/apps/chrome/promoscreen/SigninPromoScreen;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSigninCancelled()V
    .locals 1

    .prologue
    .line 111
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->recordAndroidSigninPromoDeclined()V

    .line 112
    iget-object v0, p0, Lcom/google/android/apps/chrome/promoscreen/SigninPromoScreen$1;->this$0:Lcom/google/android/apps/chrome/promoscreen/SigninPromoScreen;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/promoscreen/SigninPromoScreen;->dismiss()V

    .line 113
    return-void
.end method

.method public onSigninComplete()V
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/google/android/apps/chrome/promoscreen/SigninPromoScreen$1;->this$0:Lcom/google/android/apps/chrome/promoscreen/SigninPromoScreen;

    # getter for: Lcom/google/android/apps/chrome/promoscreen/SigninPromoScreen;->mAccountFirstRunView:Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;
    invoke-static {v0}, Lcom/google/android/apps/chrome/promoscreen/SigninPromoScreen;->access$000(Lcom/google/android/apps/chrome/promoscreen/SigninPromoScreen;)Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->switchToSignedMode()V

    .line 105
    iget-object v0, p0, Lcom/google/android/apps/chrome/promoscreen/SigninPromoScreen$1;->this$0:Lcom/google/android/apps/chrome/promoscreen/SigninPromoScreen;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/promoscreen/SigninPromoScreen;->getOwnerActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/chrome/browser/signin/SigninManager;->get(Landroid/content/Context;)Lorg/chromium/chrome/browser/signin/SigninManager;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/signin/SigninManager;->logInSignedInUser()V

    .line 106
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->recordAndroidSigninPromoAccepted()V

    .line 107
    return-void
.end method
