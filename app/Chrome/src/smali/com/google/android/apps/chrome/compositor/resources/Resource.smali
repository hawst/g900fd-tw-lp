.class public interface abstract Lcom/google/android/apps/chrome/compositor/resources/Resource;
.super Ljava/lang/Object;
.source "Resource.java"


# virtual methods
.method public abstract getAperture()Landroid/graphics/Rect;
.end method

.method public abstract getBitmap()Landroid/graphics/Bitmap;
.end method

.method public abstract getBitmapSize()Landroid/graphics/Rect;
.end method

.method public abstract getPadding()Landroid/graphics/Rect;
.end method
