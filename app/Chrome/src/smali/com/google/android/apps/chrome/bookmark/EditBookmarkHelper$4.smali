.class final Lcom/google/android/apps/chrome/bookmark/EditBookmarkHelper$4;
.super Ljava/lang/Object;
.source "EditBookmarkHelper.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic val$bookmarkId:J

.field final synthetic val$input:Landroid/widget/EditText;

.field final synthetic val$profile:Lorg/chromium/chrome/browser/profiles/Profile;


# direct methods
.method constructor <init>(Landroid/widget/EditText;Lorg/chromium/chrome/browser/profiles/Profile;J)V
    .locals 1

    .prologue
    .line 99
    iput-object p1, p0, Lcom/google/android/apps/chrome/bookmark/EditBookmarkHelper$4;->val$input:Landroid/widget/EditText;

    iput-object p2, p0, Lcom/google/android/apps/chrome/bookmark/EditBookmarkHelper$4;->val$profile:Lorg/chromium/chrome/browser/profiles/Profile;

    iput-wide p3, p0, Lcom/google/android/apps/chrome/bookmark/EditBookmarkHelper$4;->val$bookmarkId:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 4

    .prologue
    .line 102
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/EditBookmarkHelper$4;->val$input:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 103
    iget-object v1, p0, Lcom/google/android/apps/chrome/bookmark/EditBookmarkHelper$4;->val$profile:Lorg/chromium/chrome/browser/profiles/Profile;

    iget-wide v2, p0, Lcom/google/android/apps/chrome/bookmark/EditBookmarkHelper$4;->val$bookmarkId:J

    # invokes: Lcom/google/android/apps/chrome/bookmark/EditBookmarkHelper;->nativeSetPartnerBookmarkTitle(Lorg/chromium/chrome/browser/profiles/Profile;JLjava/lang/String;)V
    invoke-static {v1, v2, v3, v0}, Lcom/google/android/apps/chrome/bookmark/EditBookmarkHelper;->access$000(Lorg/chromium/chrome/browser/profiles/Profile;JLjava/lang/String;)V

    .line 104
    return-void
.end method
