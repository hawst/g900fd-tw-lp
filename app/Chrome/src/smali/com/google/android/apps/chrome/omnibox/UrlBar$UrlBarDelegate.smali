.class public interface abstract Lcom/google/android/apps/chrome/omnibox/UrlBar$UrlBarDelegate;
.super Ljava/lang/Object;
.source "UrlBar.java"


# virtual methods
.method public abstract backKeyPressed()V
.end method

.method public abstract getCurrentTab()Lcom/google/android/apps/chrome/tab/ChromeTab;
.end method

.method public abstract onFinishSetUrl()V
.end method

.method public abstract onUrlPreFocusChanged(Z)V
.end method

.method public abstract setIgnoreURLBarModification(Z)V
.end method

.method public abstract showingOriginalUrlForPreview()Z
.end method
