.class Lcom/google/android/apps/chrome/toolbar/ToolbarManager$7;
.super Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;
.source "ToolbarManager.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 572
    const-class v0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$7;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lcom/google/android/apps/chrome/toolbar/ToolbarManager;)V
    .locals 0

    .prologue
    .line 572
    iput-object p1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$7;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarManager;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;-><init>()V

    return-void
.end method

.method private handleControlMessage(Landroid/os/Message;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v3, -0x1

    const/4 v0, 0x1

    .line 574
    iget v2, p1, Landroid/os/Message;->what:I

    sparse-switch v2, :sswitch_data_0

    move v0, v1

    .line 637
    :cond_0
    :goto_0
    return v0

    .line 576
    :sswitch_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$7;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarManager;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mToolbarDelegate:Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;
    invoke-static {v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->access$300(Lcom/google/android/apps/chrome/toolbar/ToolbarManager;)Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;

    move-result-object v1

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->setOverviewMode(ZLandroid/os/Bundle;)V

    .line 577
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$7;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarManager;

    # invokes: Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->updateButtonStatus()V
    invoke-static {v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->access$1400(Lcom/google/android/apps/chrome/toolbar/ToolbarManager;)V

    goto :goto_0

    .line 580
    :sswitch_1
    iget-object v2, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$7;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarManager;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mToolbarDelegate:Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;
    invoke-static {v2}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->access$300(Lcom/google/android/apps/chrome/toolbar/ToolbarManager;)Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;

    move-result-object v2

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->setOverviewMode(ZLandroid/os/Bundle;)V

    .line 581
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$7;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarManager;

    # invokes: Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->updateButtonStatus()V
    invoke-static {v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->access$1400(Lcom/google/android/apps/chrome/toolbar/ToolbarManager;)V

    goto :goto_0

    .line 584
    :sswitch_2
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$7;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarManager;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mToolbarDelegate:Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;
    invoke-static {v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->access$300(Lcom/google/android/apps/chrome/toolbar/ToolbarManager;)Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->onOverviewTransitionFinished()V

    goto :goto_0

    .line 587
    :sswitch_3
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$7;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarManager;

    # invokes: Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->updateTabCount()V
    invoke-static {v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->access$1500(Lcom/google/android/apps/chrome/toolbar/ToolbarManager;)V

    goto :goto_0

    .line 590
    :sswitch_4
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$7;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarManager;

    # invokes: Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->updateTabCount()V
    invoke-static {v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->access$1500(Lcom/google/android/apps/chrome/toolbar/ToolbarManager;)V

    .line 591
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$7;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarManager;

    # invokes: Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->refreshSelectedTab()V
    invoke-static {v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->access$1100(Lcom/google/android/apps/chrome/toolbar/ToolbarManager;)V

    goto :goto_0

    .line 594
    :sswitch_5
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$7;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarManager;

    # setter for: Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mPreselectedTabId:I
    invoke-static {v1, v3}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->access$1602(Lcom/google/android/apps/chrome/toolbar/ToolbarManager;I)I

    .line 595
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$7;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarManager;

    # invokes: Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->refreshSelectedTab()V
    invoke-static {v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->access$1100(Lcom/google/android/apps/chrome/toolbar/ToolbarManager;)V

    goto :goto_0

    .line 598
    :sswitch_6
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$7;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarManager;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    const-string/jumbo v3, "tabId"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    # setter for: Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mPreselectedTabId:I
    invoke-static {v1, v2}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->access$1602(Lcom/google/android/apps/chrome/toolbar/ToolbarManager;I)I

    .line 599
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$7;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarManager;

    # invokes: Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->refreshSelectedTab()V
    invoke-static {v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->access$1100(Lcom/google/android/apps/chrome/toolbar/ToolbarManager;)V

    goto :goto_0

    .line 602
    :sswitch_7
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$7;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarManager;

    # setter for: Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mPreselectedTabId:I
    invoke-static {v1, v3}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->access$1602(Lcom/google/android/apps/chrome/toolbar/ToolbarManager;I)I

    .line 603
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$7;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarManager;

    # invokes: Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->refreshSelectedTab()V
    invoke-static {v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->access$1100(Lcom/google/android/apps/chrome/toolbar/ToolbarManager;)V

    goto :goto_0

    .line 606
    :sswitch_8
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$7;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarManager;

    # invokes: Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->updateTabCount()V
    invoke-static {v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->access$1500(Lcom/google/android/apps/chrome/toolbar/ToolbarManager;)V

    .line 607
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$7;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarManager;

    # invokes: Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->refreshSelectedTab()V
    invoke-static {v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->access$1100(Lcom/google/android/apps/chrome/toolbar/ToolbarManager;)V

    goto :goto_0

    .line 610
    :sswitch_9
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$7;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarManager;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mToolbarDelegate:Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;
    invoke-static {v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->access$300(Lcom/google/android/apps/chrome/toolbar/ToolbarManager;)Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->onDefaultSearchEngineChanged()V

    goto/16 :goto_0

    .line 613
    :sswitch_a
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$7;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarManager;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mToolbarDelegate:Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;
    invoke-static {v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->access$300(Lcom/google/android/apps/chrome/toolbar/ToolbarManager;)Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->handleFindToolbarStateChange(Z)V

    .line 614
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$7;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarManager;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;
    invoke-static {v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->access$1200(Lcom/google/android/apps/chrome/toolbar/ToolbarManager;)Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 615
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$7;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarManager;

    iget-object v2, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$7;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarManager;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;
    invoke-static {v2}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->access$1200(Lcom/google/android/apps/chrome/toolbar/ToolbarManager;)Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$7;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarManager;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mFullscreenFindInPageToken:I
    invoke-static {v3}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->access$1700(Lcom/google/android/apps/chrome/toolbar/ToolbarManager;)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->showControlsPersistentAndClearOldToken(I)I

    move-result v2

    # setter for: Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mFullscreenFindInPageToken:I
    invoke-static {v1, v2}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->access$1702(Lcom/google/android/apps/chrome/toolbar/ToolbarManager;I)I

    goto/16 :goto_0

    .line 621
    :sswitch_b
    iget-object v2, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$7;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarManager;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mToolbarDelegate:Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;
    invoke-static {v2}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->access$300(Lcom/google/android/apps/chrome/toolbar/ToolbarManager;)Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->handleFindToolbarStateChange(Z)V

    .line 622
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$7;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarManager;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;
    invoke-static {v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->access$1200(Lcom/google/android/apps/chrome/toolbar/ToolbarManager;)Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 623
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$7;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarManager;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;
    invoke-static {v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->access$1200(Lcom/google/android/apps/chrome/toolbar/ToolbarManager;)Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$7;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarManager;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mFullscreenFindInPageToken:I
    invoke-static {v2}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->access$1700(Lcom/google/android/apps/chrome/toolbar/ToolbarManager;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->hideControlsPersistent(I)V

    .line 624
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$7;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarManager;

    # setter for: Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mFullscreenFindInPageToken:I
    invoke-static {v1, v3}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->access$1702(Lcom/google/android/apps/chrome/toolbar/ToolbarManager;I)I

    goto/16 :goto_0

    .line 629
    :sswitch_c
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$7;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarManager;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mToolbarDelegate:Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;
    invoke-static {v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->access$300(Lcom/google/android/apps/chrome/toolbar/ToolbarManager;)Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$7;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarManager;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mToolbarView:Landroid/view/View;
    invoke-static {v2}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->access$600(Lcom/google/android/apps/chrome/toolbar/ToolbarManager;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/chrome/partnercustomizations/HomepageManager;->isHomepageEnabled(Landroid/content/Context;)Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->onHomeButtonUpdate(Z)V

    goto/16 :goto_0

    .line 633
    :sswitch_d
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$7;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarManager;

    # invokes: Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->updateTabCount()V
    invoke-static {v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->access$1500(Lcom/google/android/apps/chrome/toolbar/ToolbarManager;)V

    .line 634
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$7;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarManager;

    # invokes: Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->refreshSelectedTab()V
    invoke-static {v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->access$1100(Lcom/google/android/apps/chrome/toolbar/ToolbarManager;)V

    goto/16 :goto_0

    .line 574
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_2
        0x2 -> :sswitch_3
        0x3 -> :sswitch_5
        0x5 -> :sswitch_8
        0xc -> :sswitch_4
        0xf -> :sswitch_1
        0x12 -> :sswitch_7
        0x15 -> :sswitch_6
        0x2a -> :sswitch_9
        0x2d -> :sswitch_b
        0x3b -> :sswitch_a
        0x45 -> :sswitch_d
        0x46 -> :sswitch_c
        0x48 -> :sswitch_c
    .end sparse-switch
.end method

.method private handleTabSpecificMessage(Landroid/os/Message;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 642
    iget-object v2, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$7;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarManager;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mToolbarModel:Lcom/google/android/apps/chrome/toolbar/ToolbarModelImpl;
    invoke-static {v2}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->access$100(Lcom/google/android/apps/chrome/toolbar/ToolbarManager;)Lcom/google/android/apps/chrome/toolbar/ToolbarModelImpl;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/toolbar/ToolbarModelImpl;->getTab()Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    const-string/jumbo v3, "tabId"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$7;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarManager;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mToolbarModel:Lcom/google/android/apps/chrome/toolbar/ToolbarModelImpl;
    invoke-static {v3}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->access$100(Lcom/google/android/apps/chrome/toolbar/ToolbarManager;)Lcom/google/android/apps/chrome/toolbar/ToolbarModelImpl;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/toolbar/ToolbarModelImpl;->getTab()Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getId()I

    move-result v3

    if-eq v2, v3, :cond_1

    .line 680
    :cond_0
    :goto_0
    return v0

    .line 646
    :cond_1
    iget v2, p1, Landroid/os/Message;->what:I

    sparse-switch v2, :sswitch_data_0

    move v0, v1

    .line 680
    goto :goto_0

    .line 648
    :sswitch_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$7;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarManager;

    # invokes: Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->onTabCrash()V
    invoke-static {v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->access$1800(Lcom/google/android/apps/chrome/toolbar/ToolbarManager;)V

    goto :goto_0

    .line 651
    :sswitch_1
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$7;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarManager;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mToolbarDelegate:Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;
    invoke-static {v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->access$300(Lcom/google/android/apps/chrome/toolbar/ToolbarManager;)Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->onTabContentViewChanged()V

    goto :goto_0

    .line 654
    :sswitch_2
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$7;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarManager;

    # invokes: Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->updateButtonStatus()V
    invoke-static {v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->access$1400(Lcom/google/android/apps/chrome/toolbar/ToolbarManager;)V

    .line 655
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$7;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarManager;

    # invokes: Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->updateTabLoadingState(ZZ)V
    invoke-static {v1, v0, v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->access$1900(Lcom/google/android/apps/chrome/toolbar/ToolbarManager;ZZ)V

    goto :goto_0

    .line 658
    :sswitch_3
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$7;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarManager;

    # invokes: Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->onPageLoadFinished()V
    invoke-static {v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->access$2000(Lcom/google/android/apps/chrome/toolbar/ToolbarManager;)V

    goto :goto_0

    .line 661
    :sswitch_4
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$7;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarManager;

    # invokes: Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->onPageLoadFailed()V
    invoke-static {v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->access$2100(Lcom/google/android/apps/chrome/toolbar/ToolbarManager;)V

    goto :goto_0

    .line 664
    :sswitch_5
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$7;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarManager;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    const-string/jumbo v3, "progress"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    # invokes: Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->updateLoadProgress(I)V
    invoke-static {v1, v2}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->access$2200(Lcom/google/android/apps/chrome/toolbar/ToolbarManager;I)V

    goto :goto_0

    .line 669
    :sswitch_6
    iget-object v2, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$7;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarManager;

    # invokes: Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->updateTabLoadingState(ZZ)V
    invoke-static {v2, v1, v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->access$1900(Lcom/google/android/apps/chrome/toolbar/ToolbarManager;ZZ)V

    goto :goto_0

    .line 672
    :sswitch_7
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    const-string/jumbo v3, "fullyPrerendered"

    invoke-virtual {v2, v3, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 673
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$7;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarManager;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mLoadProgressSimulator:Lcom/google/android/apps/chrome/toolbar/ToolbarManager$LoadProgressSimulator;
    invoke-static {v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->access$2300(Lcom/google/android/apps/chrome/toolbar/ToolbarManager;)Lcom/google/android/apps/chrome/toolbar/ToolbarManager$LoadProgressSimulator;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$LoadProgressSimulator;->start()V

    goto :goto_0

    .line 677
    :sswitch_8
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$7;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarManager;

    # invokes: Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->onReaderModeStatusChanged()V
    invoke-static {v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->access$2400(Lcom/google/android/apps/chrome/toolbar/ToolbarManager;)V

    goto :goto_0

    .line 646
    :sswitch_data_0
    .sparse-switch
        0x6 -> :sswitch_0
        0x8 -> :sswitch_2
        0x9 -> :sswitch_3
        0xb -> :sswitch_5
        0x16 -> :sswitch_1
        0x19 -> :sswitch_6
        0x1c -> :sswitch_4
        0x23 -> :sswitch_7
        0x4f -> :sswitch_8
    .end sparse-switch
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3

    .prologue
    .line 686
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$7;->handleControlMessage(Landroid/os/Message;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 689
    :cond_0
    return-void

    .line 687
    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$7;->handleTabSpecificMessage(Landroid/os/Message;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 688
    sget-boolean v0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$7;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Unhandled message for ID: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method
