.class public Lcom/google/android/apps/chrome/precache/PrecacheServiceLauncher;
.super Landroid/content/BroadcastReceiver;
.source "PrecacheServiceLauncher.java"


# static fields
.field static final ACTION_ALARM:Ljava/lang/String; = "com.google.android.apps.chrome.precache.PrecacheServiceLauncher.ALARM"

.field static final PREF_IS_PRECACHING_ENABLED:Ljava/lang/String; = "precache.is_precaching_enabled"

.field static final PREF_PRECACHE_LAST_TIME:Ljava/lang/String; = "precache.last_time"

.field static final WAIT_UNTIL_NEXT_PRECACHE_MS:I = 0xdbba00

.field private static sWakeLock:Landroid/os/PowerManager$WakeLock;


# instance fields
.field private mDeviceState:Lcom/google/android/apps/chrome/precache/DeviceState;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/apps/chrome/precache/PrecacheServiceLauncher;->sWakeLock:Landroid/os/PowerManager$WakeLock;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 46
    invoke-static {}, Lcom/google/android/apps/chrome/precache/DeviceState;->getInstance()Lcom/google/android/apps/chrome/precache/DeviceState;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/precache/PrecacheServiceLauncher;->mDeviceState:Lcom/google/android/apps/chrome/precache/DeviceState;

    return-void
.end method

.method private acquireWakeLockAndStartService(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 146
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/precache/PrecacheServiceLauncher;->acquireWakeLock(Landroid/content/Context;)V

    .line 147
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/precache/PrecacheServiceLauncher;->startPrecacheService(Landroid/content/Context;)V

    .line 148
    return-void
.end method

.method private cancelAlarm(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 207
    invoke-static {p1}, Lcom/google/android/apps/chrome/precache/PrecacheServiceLauncher;->getPendingAlarmIntent(Landroid/content/Context;)Landroid/app/PendingIntent;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/chrome/precache/PrecacheServiceLauncher;->cancelAlarmOnSystem(Landroid/content/Context;Landroid/app/PendingIntent;)V

    .line 208
    return-void
.end method

.method private static getPendingAlarmIntent(Landroid/content/Context;)Landroid/app/PendingIntent;
    .locals 5

    .prologue
    .line 174
    const/4 v0, 0x0

    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v2, "com.google.android.apps.chrome.precache.PrecacheServiceLauncher.ALARM"

    const/4 v3, 0x0

    const-class v4, Lcom/google/android/apps/chrome/precache/PrecacheServiceLauncher;

    invoke-direct {v1, v2, v3, p0, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v2, 0x8000000

    invoke-static {p0, v0, v1, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method protected static releaseWakeLock()V
    .locals 1

    .prologue
    .line 136
    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    .line 137
    sget-object v0, Lcom/google/android/apps/chrome/precache/PrecacheServiceLauncher;->sWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/chrome/precache/PrecacheServiceLauncher;->sWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/chrome/precache/PrecacheServiceLauncher;->sWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 138
    :cond_0
    return-void
.end method

.method private setAlarm(Landroid/content/Context;J)V
    .locals 8

    .prologue
    .line 186
    const/4 v3, 0x2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/precache/PrecacheServiceLauncher;->getElapsedRealtimeOnSystem()J

    move-result-wide v0

    add-long v4, v0, p2

    invoke-static {p1}, Lcom/google/android/apps/chrome/precache/PrecacheServiceLauncher;->getPendingAlarmIntent(Landroid/content/Context;)Landroid/app/PendingIntent;

    move-result-object v6

    move-object v1, p0

    move-object v2, p1

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/apps/chrome/precache/PrecacheServiceLauncher;->setAlarmOnSystem(Landroid/content/Context;IJLandroid/app/PendingIntent;)V

    .line 188
    return-void
.end method

.method public static setIsPrecachingEnabled(Landroid/content/Context;Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 63
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 64
    const-string/jumbo v1, "precache.is_precaching_enabled"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 65
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 67
    if-nez p1, :cond_0

    .line 70
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/chrome/precache/PrecacheService;

    invoke-direct {v0, v2, v2, p0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 71
    invoke-virtual {p0, v0}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    .line 73
    :cond_0
    return-void
.end method


# virtual methods
.method protected acquireWakeLock(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 159
    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    .line 160
    sget-object v0, Lcom/google/android/apps/chrome/precache/PrecacheServiceLauncher;->sWakeLock:Landroid/os/PowerManager$WakeLock;

    if-nez v0, :cond_0

    .line 161
    const-string/jumbo v0, "power"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 162
    const/4 v1, 0x1

    const-string/jumbo v2, "PrecacheServiceLauncher"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/chrome/precache/PrecacheServiceLauncher;->sWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 164
    :cond_0
    sget-object v0, Lcom/google/android/apps/chrome/precache/PrecacheServiceLauncher;->sWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 165
    return-void
.end method

.method protected cancelAlarmOnSystem(Landroid/content/Context;Landroid/app/PendingIntent;)V
    .locals 1

    .prologue
    .line 213
    const-string/jumbo v0, "alarm"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 214
    invoke-virtual {v0, p2}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 215
    return-void
.end method

.method protected getElapsedRealtimeOnSystem()J
    .locals 2

    .prologue
    .line 219
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    return-wide v0
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 12

    .prologue
    const-wide/32 v10, 0xdbba00

    const-wide/16 v4, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 78
    invoke-static {p1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 79
    const-string/jumbo v0, "precache.is_precaching_enabled"

    invoke-interface {v3, v0, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 80
    const-string/jumbo v6, "precache.last_time"

    invoke-interface {v3, v6, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v6

    .line 81
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/precache/PrecacheServiceLauncher;->getElapsedRealtimeOnSystem()J

    move-result-wide v8

    cmp-long v8, v6, v8

    if-lez v8, :cond_5

    .line 88
    :goto_0
    if-nez v0, :cond_0

    .line 131
    :goto_1
    return-void

    .line 90
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/precache/PrecacheServiceLauncher;->mDeviceState:Lcom/google/android/apps/chrome/precache/DeviceState;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/precache/DeviceState;->isPowerConnected(Landroid/content/Context;)Z

    move-result v6

    .line 91
    iget-object v0, p0, Lcom/google/android/apps/chrome/precache/PrecacheServiceLauncher;->mDeviceState:Lcom/google/android/apps/chrome/precache/DeviceState;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/precache/DeviceState;->isWifiAvailable(Landroid/content/Context;)Z

    move-result v7

    .line 92
    iget-object v0, p0, Lcom/google/android/apps/chrome/precache/PrecacheServiceLauncher;->mDeviceState:Lcom/google/android/apps/chrome/precache/DeviceState;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/precache/DeviceState;->isInteractive(Landroid/content/Context;)Z

    move-result v0

    .line 93
    if-eqz v6, :cond_1

    if-eqz v7, :cond_1

    if-nez v0, :cond_1

    move v0, v1

    .line 95
    :goto_2
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/precache/PrecacheServiceLauncher;->getElapsedRealtimeOnSystem()J

    move-result-wide v8

    sub-long v4, v8, v4

    .line 96
    cmp-long v8, v4, v10

    if-ltz v8, :cond_2

    .line 102
    :goto_3
    const-string/jumbo v2, "com.google.android.apps.chrome.precache.PrecacheServiceLauncher.ALARM"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    if-eqz v0, :cond_3

    if-eqz v1, :cond_3

    .line 106
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 107
    const-string/jumbo v1, "precache.last_time"

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/precache/PrecacheServiceLauncher;->getElapsedRealtimeOnSystem()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 108
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 110
    const v0, 0xdbba0

    const v1, 0xdbba00

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    int-to-long v0, v0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/apps/chrome/precache/PrecacheServiceLauncher;->setAlarm(Landroid/content/Context;J)V

    .line 113
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/precache/PrecacheServiceLauncher;->acquireWakeLockAndStartService(Landroid/content/Context;)V

    goto :goto_1

    :cond_1
    move v0, v2

    .line 93
    goto :goto_2

    :cond_2
    move v1, v2

    .line 96
    goto :goto_3

    .line 115
    :cond_3
    if-eqz v6, :cond_4

    if-eqz v7, :cond_4

    .line 123
    const-wide/32 v0, 0xdbba0

    sub-long v2, v10, v4

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/apps/chrome/precache/PrecacheServiceLauncher;->setAlarm(Landroid/content/Context;J)V

    goto :goto_1

    .line 128
    :cond_4
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/precache/PrecacheServiceLauncher;->cancelAlarm(Landroid/content/Context;)V

    goto :goto_1

    :cond_5
    move-wide v4, v6

    goto :goto_0
.end method

.method protected setAlarmOnSystem(Landroid/content/Context;IJLandroid/app/PendingIntent;)V
    .locals 1

    .prologue
    .line 197
    const-string/jumbo v0, "alarm"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 198
    invoke-virtual {v0, p2, p3, p4, p5}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 199
    return-void
.end method

.method setDeviceState(Lcom/google/android/apps/chrome/precache/DeviceState;)V
    .locals 0

    .prologue
    .line 50
    iput-object p1, p0, Lcom/google/android/apps/chrome/precache/PrecacheServiceLauncher;->mDeviceState:Lcom/google/android/apps/chrome/precache/DeviceState;

    .line 51
    return-void
.end method

.method protected startPrecacheService(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 152
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "com.google.android.apps.chrome.precache.PrecacheService.START_PRECACHE"

    const/4 v2, 0x0

    const-class v3, Lcom/google/android/apps/chrome/precache/PrecacheService;

    invoke-direct {v0, v1, v2, p1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 154
    invoke-virtual {p1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 155
    return-void
.end method
