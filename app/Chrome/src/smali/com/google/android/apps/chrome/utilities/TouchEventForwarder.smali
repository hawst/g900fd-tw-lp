.class public Lcom/google/android/apps/chrome/utilities/TouchEventForwarder;
.super Ljava/lang/Object;
.source "TouchEventForwarder.java"


# instance fields
.field private final mCachedTargetBounds:Landroid/graphics/Rect;

.field private final mSource:Landroid/view/ViewGroup;

.field private final mTargets:Ljava/util/List;


# direct methods
.method public varargs constructor <init>(Landroid/view/ViewGroup;[Landroid/view/View;)V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/utilities/TouchEventForwarder;->mCachedTargetBounds:Landroid/graphics/Rect;

    .line 31
    iput-object p1, p0, Lcom/google/android/apps/chrome/utilities/TouchEventForwarder;->mSource:Landroid/view/ViewGroup;

    .line 32
    invoke-static {p2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/utilities/TouchEventForwarder;->mTargets:Ljava/util/List;

    .line 33
    return-void
.end method

.method private static dxToRect(Landroid/graphics/Rect;F)F
    .locals 1

    .prologue
    .line 69
    iget v0, p0, Landroid/graphics/Rect;->left:I

    int-to-float v0, v0

    cmpg-float v0, p1, v0

    if-gez v0, :cond_0

    .line 70
    iget v0, p0, Landroid/graphics/Rect;->left:I

    int-to-float v0, v0

    sub-float/2addr v0, p1

    .line 74
    :goto_0
    return v0

    .line 71
    :cond_0
    iget v0, p0, Landroid/graphics/Rect;->right:I

    int-to-float v0, v0

    cmpl-float v0, p1, v0

    if-lez v0, :cond_1

    .line 72
    iget v0, p0, Landroid/graphics/Rect;->right:I

    int-to-float v0, v0

    sub-float/2addr v0, p1

    goto :goto_0

    .line 74
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static dyToRect(Landroid/graphics/Rect;F)F
    .locals 1

    .prologue
    .line 79
    iget v0, p0, Landroid/graphics/Rect;->top:I

    int-to-float v0, v0

    cmpg-float v0, p1, v0

    if-gez v0, :cond_0

    .line 80
    iget v0, p0, Landroid/graphics/Rect;->top:I

    int-to-float v0, v0

    sub-float/2addr v0, p1

    .line 84
    :goto_0
    return v0

    .line 81
    :cond_0
    iget v0, p0, Landroid/graphics/Rect;->bottom:I

    int-to-float v0, v0

    cmpl-float v0, p1, v0

    if-lez v0, :cond_1

    .line 82
    iget v0, p0, Landroid/graphics/Rect;->bottom:I

    int-to-float v0, v0

    sub-float/2addr v0, p1

    goto :goto_0

    .line 84
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 13

    .prologue
    const/4 v6, 0x0

    const/4 v1, 0x0

    .line 36
    const/4 v5, 0x0

    .line 41
    iget-object v0, p0, Lcom/google/android/apps/chrome/utilities/TouchEventForwarder;->mTargets:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move v2, v1

    move v3, v1

    :cond_0
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 42
    invoke-virtual {v0}, Landroid/view/View;->isShown()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 43
    iget-object v4, p0, Lcom/google/android/apps/chrome/utilities/TouchEventForwarder;->mCachedTargetBounds:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v8

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v9

    invoke-virtual {v4, v6, v6, v8, v9}, Landroid/graphics/Rect;->set(IIII)V

    .line 46
    iget-object v4, p0, Lcom/google/android/apps/chrome/utilities/TouchEventForwarder;->mSource:Landroid/view/ViewGroup;

    iget-object v8, p0, Lcom/google/android/apps/chrome/utilities/TouchEventForwarder;->mCachedTargetBounds:Landroid/graphics/Rect;

    invoke-virtual {v4, v0, v8}, Landroid/view/ViewGroup;->offsetDescendantRectToMyCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 47
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v8

    .line 48
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v9

    .line 49
    iget-object v4, p0, Lcom/google/android/apps/chrome/utilities/TouchEventForwarder;->mCachedTargetBounds:Landroid/graphics/Rect;

    invoke-static {v4, v8}, Lcom/google/android/apps/chrome/utilities/TouchEventForwarder;->dxToRect(Landroid/graphics/Rect;F)F

    move-result v10

    .line 50
    iget-object v4, p0, Lcom/google/android/apps/chrome/utilities/TouchEventForwarder;->mCachedTargetBounds:Landroid/graphics/Rect;

    invoke-static {v4, v9}, Lcom/google/android/apps/chrome/utilities/TouchEventForwarder;->dyToRect(Landroid/graphics/Rect;F)F

    move-result v11

    .line 51
    invoke-static {v10}, Ljava/lang/Math;->abs(F)F

    move-result v4

    invoke-static {v11}, Ljava/lang/Math;->abs(F)F

    move-result v12

    add-float/2addr v4, v12

    .line 52
    if-eqz v5, :cond_1

    cmpg-float v12, v4, v3

    if-gez v12, :cond_4

    .line 55
    :cond_1
    add-float v2, v8, v10

    .line 56
    add-float v1, v9, v11

    move-object v3, v0

    move v0, v1

    move v1, v2

    move v2, v4

    :goto_1
    move-object v5, v3

    move v3, v2

    move v2, v1

    move v1, v0

    .line 58
    goto :goto_0

    .line 60
    :cond_2
    if-nez v5, :cond_3

    move v0, v6

    .line 64
    :goto_2
    return v0

    .line 63
    :cond_3
    invoke-virtual {p1, v2, v1}, Landroid/view/MotionEvent;->setLocation(FF)V

    .line 64
    invoke-virtual {v5, p1}, Landroid/view/View;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_2

    :cond_4
    move v0, v1

    move v1, v2

    move v2, v3

    move-object v3, v5

    goto :goto_1
.end method
