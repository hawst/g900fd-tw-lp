.class final Lcom/google/android/apps/chrome/ntp/BookmarkItemView$DrawingData;
.super Ljava/lang/Object;
.source "BookmarkItemView.java"


# instance fields
.field private final mFaviconContainerSize:I

.field private final mFaviconSize:I

.field private final mMinHeight:I

.field private final mPadding:I

.field private final mTextColor:I

.field private final mTextSize:I


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 63
    sget v1, Lcom/google/android/apps/chrome/R$dimen;->ntp_list_item_padding:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/chrome/ntp/BookmarkItemView$DrawingData;->mPadding:I

    .line 64
    sget v1, Lcom/google/android/apps/chrome/R$dimen;->ntp_list_item_min_height:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/chrome/ntp/BookmarkItemView$DrawingData;->mMinHeight:I

    .line 65
    sget v1, Lcom/google/android/apps/chrome/R$dimen;->ntp_list_item_favicon_size:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/chrome/ntp/BookmarkItemView$DrawingData;->mFaviconSize:I

    .line 66
    sget v1, Lcom/google/android/apps/chrome/R$dimen;->ntp_list_item_favicon_container_size:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/chrome/ntp/BookmarkItemView$DrawingData;->mFaviconContainerSize:I

    .line 68
    sget v1, Lcom/google/android/apps/chrome/R$dimen;->ntp_list_item_text_size:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/chrome/ntp/BookmarkItemView$DrawingData;->mTextSize:I

    .line 69
    sget v1, Lcom/google/android/apps/chrome/R$color;->ntp_list_item_text:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarkItemView$DrawingData;->mTextColor:I

    .line 70
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/ntp/BookmarkItemView$DrawingData;)I
    .locals 1

    .prologue
    .line 48
    iget v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarkItemView$DrawingData;->mTextColor:I

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/ntp/BookmarkItemView$DrawingData;)I
    .locals 1

    .prologue
    .line 48
    iget v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarkItemView$DrawingData;->mTextSize:I

    return v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/ntp/BookmarkItemView$DrawingData;)I
    .locals 1

    .prologue
    .line 48
    iget v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarkItemView$DrawingData;->mMinHeight:I

    return v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/chrome/ntp/BookmarkItemView$DrawingData;)I
    .locals 1

    .prologue
    .line 48
    iget v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarkItemView$DrawingData;->mPadding:I

    return v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/chrome/ntp/BookmarkItemView$DrawingData;)I
    .locals 1

    .prologue
    .line 48
    iget v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarkItemView$DrawingData;->mFaviconContainerSize:I

    return v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/chrome/ntp/BookmarkItemView$DrawingData;)I
    .locals 1

    .prologue
    .line 48
    iget v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarkItemView$DrawingData;->mFaviconSize:I

    return v0
.end method
