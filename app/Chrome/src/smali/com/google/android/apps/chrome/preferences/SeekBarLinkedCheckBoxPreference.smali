.class public Lcom/google/android/apps/chrome/preferences/SeekBarLinkedCheckBoxPreference;
.super Lcom/google/android/apps/chrome/preferences/ChromeBaseCheckBoxPreference;
.source "SeekBarLinkedCheckBoxPreference.java"


# instance fields
.field private mCheckable:Landroid/widget/Checkable;

.field private mLinkedSeekBarPreference:Lcom/google/android/apps/chrome/preferences/SeekBarPreference;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/preferences/ChromeBaseCheckBoxPreference;-><init>(Landroid/content/Context;)V

    .line 32
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/preferences/ChromeBaseCheckBoxPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 28
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/chrome/preferences/ChromeBaseCheckBoxPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 24
    return-void
.end method


# virtual methods
.method protected notifyChanged()V
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/SeekBarLinkedCheckBoxPreference;->mLinkedSeekBarPreference:Lcom/google/android/apps/chrome/preferences/SeekBarPreference;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/SeekBarLinkedCheckBoxPreference;->mLinkedSeekBarPreference:Lcom/google/android/apps/chrome/preferences/SeekBarPreference;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->isTrackingTouch()Z

    move-result v0

    if-nez v0, :cond_1

    .line 56
    :cond_0
    invoke-super {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBaseCheckBoxPreference;->notifyChanged()V

    .line 58
    :cond_1
    return-void
.end method

.method protected onBindView(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 36
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/preferences/ChromeBaseCheckBoxPreference;->onBindView(Landroid/view/View;)V

    .line 37
    const v0, 0x1020001

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 38
    if-eqz v0, :cond_0

    instance-of v1, v0, Landroid/widget/Checkable;

    if-eqz v1, :cond_0

    .line 39
    check-cast v0, Landroid/widget/Checkable;

    iput-object v0, p0, Lcom/google/android/apps/chrome/preferences/SeekBarLinkedCheckBoxPreference;->mCheckable:Landroid/widget/Checkable;

    .line 41
    :cond_0
    return-void
.end method

.method public setChecked(Z)V
    .locals 1

    .prologue
    .line 45
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/preferences/ChromeBaseCheckBoxPreference;->setChecked(Z)V

    .line 46
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/SeekBarLinkedCheckBoxPreference;->mCheckable:Landroid/widget/Checkable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/SeekBarLinkedCheckBoxPreference;->mCheckable:Landroid/widget/Checkable;

    invoke-interface {v0, p1}, Landroid/widget/Checkable;->setChecked(Z)V

    .line 47
    :cond_0
    return-void
.end method

.method public setLinkedSeekBarPreference(Lcom/google/android/apps/chrome/preferences/SeekBarPreference;)V
    .locals 0

    .prologue
    .line 50
    iput-object p1, p0, Lcom/google/android/apps/chrome/preferences/SeekBarLinkedCheckBoxPreference;->mLinkedSeekBarPreference:Lcom/google/android/apps/chrome/preferences/SeekBarPreference;

    .line 51
    return-void
.end method
