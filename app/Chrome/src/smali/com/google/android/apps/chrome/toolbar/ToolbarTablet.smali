.class public Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;
.super Landroid/widget/LinearLayout;
.source "ToolbarTablet.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnLongClickListener;
.implements Lcom/google/android/apps/chrome/toolbar/Toolbar;


# instance fields
.field private mAccessibilitySwitcherButton:Landroid/widget/ImageButton;

.field private mBackButton:Landroid/widget/ImageButton;

.field private mBookmarkButton:Landroid/widget/ImageButton;

.field private mBookmarkListener:Landroid/view/View$OnClickListener;

.field private mForwardButton:Landroid/widget/ImageButton;

.field private mHomeButton:Landroid/widget/ImageButton;

.field private mInOverviewMode:Z

.field private mMenuButton:Landroid/widget/ImageButton;

.field private mNavigationPopup:Lorg/chromium/chrome/browser/NavigationPopup;

.field private mOverviewListener:Landroid/view/View$OnClickListener;

.field private mReloadButton:Landroid/widget/ImageButton;

.field private mShowTabStack:Z

.field private mTabSwitcherButtonDrawable:Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;

.field private mTabSwitcherButtonDrawableLight:Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;

.field private mToolbarDelegate:Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$ToolbarDelegateTablet;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 73
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 58
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mInOverviewMode:Z

    .line 74
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;)Landroid/widget/ImageButton;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mBackButton:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;)Landroid/widget/ImageButton;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mForwardButton:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;)Z
    .locals 1

    .prologue
    .line 34
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mShowTabStack:Z

    return v0
.end method

.method static synthetic access$1002(Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;Z)Z
    .locals 0

    .prologue
    .line 34
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mShowTabStack:Z

    return p1
.end method

.method static synthetic access$1100(Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;Z)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->updateSwitcherButtonVisibility(Z)V

    return-void
.end method

.method static synthetic access$1202(Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;Landroid/view/View$OnClickListener;)Landroid/view/View$OnClickListener;
    .locals 0

    .prologue
    .line 34
    iput-object p1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mBookmarkListener:Landroid/view/View$OnClickListener;

    return-object p1
.end method

.method static synthetic access$1302(Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;Landroid/view/View$OnClickListener;)Landroid/view/View$OnClickListener;
    .locals 0

    .prologue
    .line 34
    iput-object p1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mOverviewListener:Landroid/view/View$OnClickListener;

    return-object p1
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;)Landroid/widget/ImageButton;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mHomeButton:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;)Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$ToolbarDelegateTablet;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mToolbarDelegate:Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$ToolbarDelegateTablet;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;)Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mTabSwitcherButtonDrawableLight:Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;)Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mTabSwitcherButtonDrawable:Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;)Landroid/widget/ImageButton;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mAccessibilitySwitcherButton:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;)Z
    .locals 1

    .prologue
    .line 34
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mInOverviewMode:Z

    return v0
.end method

.method static synthetic access$702(Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;Z)Z
    .locals 0

    .prologue
    .line 34
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mInOverviewMode:Z

    return p1
.end method

.method static synthetic access$800(Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;)Landroid/widget/ImageButton;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mReloadButton:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;)Landroid/widget/ImageButton;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mBookmarkButton:Landroid/widget/ImageButton;

    return-object v0
.end method

.method private displayNavigationPopup(ZLandroid/view/View;)V
    .locals 3

    .prologue
    .line 253
    new-instance v0, Lorg/chromium/chrome/browser/NavigationPopup;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mToolbarDelegate:Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$ToolbarDelegateTablet;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$ToolbarDelegateTablet;->getToolbarDataProvider()Lcom/google/android/apps/chrome/toolbar/ToolbarDataProvider;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/chrome/toolbar/ToolbarDataProvider;->getTab()Lorg/chromium/chrome/browser/Tab;

    move-result-object v2

    invoke-direct {v0, v1, v2, p1}, Lorg/chromium/chrome/browser/NavigationPopup;-><init>(Landroid/content/Context;Lorg/chromium/content/browser/NavigationClient;Z)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mNavigationPopup:Lorg/chromium/chrome/browser/NavigationPopup;

    .line 256
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mNavigationPopup:Lorg/chromium/chrome/browser/NavigationPopup;

    invoke-virtual {v0, p2}, Lorg/chromium/chrome/browser/NavigationPopup;->setAnchorView(Landroid/view/View;)V

    .line 258
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$dimen;->menu_width:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 259
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mNavigationPopup:Lorg/chromium/chrome/browser/NavigationPopup;

    invoke-virtual {v1, v0}, Lorg/chromium/chrome/browser/NavigationPopup;->setWidth(I)V

    .line 261
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mNavigationPopup:Lorg/chromium/chrome/browser/NavigationPopup;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/NavigationPopup;->shouldBeShown()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mNavigationPopup:Lorg/chromium/chrome/browser/NavigationPopup;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/NavigationPopup;->show()V

    .line 262
    :cond_0
    return-void
.end method

.method private updateSwitcherButtonVisibility(Z)V
    .locals 2

    .prologue
    .line 299
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mAccessibilitySwitcherButton:Landroid/widget/ImageButton;

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mShowTabStack:Z

    if-nez v0, :cond_0

    if-eqz p1, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 301
    return-void

    .line 299
    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method


# virtual methods
.method public getDelegate()Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;
    .locals 1

    .prologue
    .line 213
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mToolbarDelegate:Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$ToolbarDelegateTablet;

    return-object v0
.end method

.method public getView()Landroid/view/View;
    .locals 0

    .prologue
    .line 218
    return-object p0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 266
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mHomeButton:Landroid/widget/ImageButton;

    if-ne v0, p1, :cond_1

    .line 267
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mToolbarDelegate:Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$ToolbarDelegateTablet;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$ToolbarDelegateTablet;->openHomepage()V

    .line 296
    :cond_0
    :goto_0
    return-void

    .line 268
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mBackButton:Landroid/widget/ImageButton;

    if-ne v0, p1, :cond_2

    .line 269
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mToolbarDelegate:Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$ToolbarDelegateTablet;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$ToolbarDelegateTablet;->back()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 280
    invoke-static {v1}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->back(Z)V

    goto :goto_0

    .line 281
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mForwardButton:Landroid/widget/ImageButton;

    if-ne v0, p1, :cond_3

    .line 282
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mToolbarDelegate:Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$ToolbarDelegateTablet;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$ToolbarDelegateTablet;->forward()Z

    .line 283
    invoke-static {v1}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->forward(Z)V

    goto :goto_0

    .line 284
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mReloadButton:Landroid/widget/ImageButton;

    if-ne v0, p1, :cond_4

    .line 285
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mToolbarDelegate:Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$ToolbarDelegateTablet;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$ToolbarDelegateTablet;->stopOrReloadCurrentTab()V

    goto :goto_0

    .line 286
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mBookmarkButton:Landroid/widget/ImageButton;

    if-ne v0, p1, :cond_5

    .line 287
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mBookmarkListener:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_0

    .line 288
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mBookmarkListener:Landroid/view/View$OnClickListener;

    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mBookmarkButton:Landroid/widget/ImageButton;

    invoke-interface {v0, v1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 289
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->toolbarToggleBookmark()V

    goto :goto_0

    .line 291
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mAccessibilitySwitcherButton:Landroid/widget/ImageButton;

    if-ne v0, p1, :cond_0

    .line 292
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mOverviewListener:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_0

    .line 293
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mOverviewListener:Landroid/view/View$OnClickListener;

    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mAccessibilitySwitcherButton:Landroid/widget/ImageButton;

    invoke-interface {v0, v1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    goto :goto_0
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 223
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onDraw(Landroid/graphics/Canvas;)V

    .line 224
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mToolbarDelegate:Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$ToolbarDelegateTablet;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$ToolbarDelegateTablet;->recordFirstDrawTime()V

    .line 225
    return-void
.end method

.method public onFinishInflate()V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/16 v3, 0x8

    const/4 v1, 0x0

    .line 78
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 80
    new-instance v0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$ToolbarDelegateTablet;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$ToolbarDelegateTablet;-><init>(Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mToolbarDelegate:Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$ToolbarDelegateTablet;

    .line 82
    sget v0, Lcom/google/android/apps/chrome/R$id;->home_button:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mHomeButton:Landroid/widget/ImageButton;

    .line 83
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mHomeButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 84
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mHomeButton:Landroid/widget/ImageButton;

    new-instance v4, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$1;

    invoke-direct {v4, p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$1;-><init>(Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;)V

    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 102
    sget v0, Lcom/google/android/apps/chrome/R$id;->back_button:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mBackButton:Landroid/widget/ImageButton;

    .line 103
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mBackButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 104
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mBackButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 105
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mBackButton:Landroid/widget/ImageButton;

    new-instance v4, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$2;

    invoke-direct {v4, p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$2;-><init>(Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;)V

    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 125
    sget v0, Lcom/google/android/apps/chrome/R$id;->forward_button:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mForwardButton:Landroid/widget/ImageButton;

    .line 126
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mForwardButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 127
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mForwardButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 128
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mForwardButton:Landroid/widget/ImageButton;

    new-instance v4, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$3;

    invoke-direct {v4, p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$3;-><init>(Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;)V

    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 146
    sget v0, Lcom/google/android/apps/chrome/R$id;->refresh_button:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mReloadButton:Landroid/widget/ImageButton;

    .line 147
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mReloadButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 148
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mReloadButton:Landroid/widget/ImageButton;

    new-instance v4, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$4;

    invoke-direct {v4, p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$4;-><init>(Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;)V

    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 168
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/device/DeviceClassManager;->isAccessibilityModeEnabled(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lorg/chromium/base/CommandLine;->getInstance()Lorg/chromium/base/CommandLine;

    move-result-object v0

    const-string/jumbo v4, "enable-tablet-tab-stack"

    invoke-virtual {v0, v4}, Lorg/chromium/base/CommandLine;->hasSwitch(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    move v0, v2

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mShowTabStack:Z

    .line 171
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;->createTabSwitcherDrawable(Landroid/content/res/Resources;Z)Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mTabSwitcherButtonDrawable:Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;

    .line 173
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0, v2}, Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;->createTabSwitcherDrawable(Landroid/content/res/Resources;Z)Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mTabSwitcherButtonDrawableLight:Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;

    .line 176
    sget v0, Lcom/google/android/apps/chrome/R$id;->tab_switcher_button:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mAccessibilitySwitcherButton:Landroid/widget/ImageButton;

    .line 177
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mAccessibilitySwitcherButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 178
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mAccessibilitySwitcherButton:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mTabSwitcherButtonDrawable:Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 179
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mShowTabStack:Z

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->updateSwitcherButtonVisibility(Z)V

    .line 181
    sget v0, Lcom/google/android/apps/chrome/R$id;->bookmark_button:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mBookmarkButton:Landroid/widget/ImageButton;

    .line 182
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mBookmarkButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 184
    sget v0, Lcom/google/android/apps/chrome/R$id;->menu_button:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mMenuButton:Landroid/widget/ImageButton;

    .line 185
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mMenuButton:Landroid/widget/ImageButton;

    new-instance v2, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$5;

    invoke-direct {v2, p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$5;-><init>(Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;)V

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 201
    iget-object v2, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mMenuButton:Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mToolbarDelegate:Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$ToolbarDelegateTablet;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$ToolbarDelegateTablet;->shouldShowMenuButton()Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    :goto_1
    invoke-virtual {v2, v0}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 204
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mAccessibilitySwitcherButton:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getVisibility()I

    move-result v0

    if-ne v0, v3, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mMenuButton:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getVisibility()I

    move-result v0

    if-ne v0, v3, :cond_1

    .line 206
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mMenuButton:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/chrome/R$dimen;->tablet_toolbar_end_padding:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-static {v0, v1, v1, v2, v1}, Lorg/chromium/base/ApiCompatibilityUtils;->setPaddingRelative(Landroid/view/View;IIII)V

    .line 209
    :cond_1
    return-void

    :cond_2
    move v0, v1

    .line 168
    goto/16 :goto_0

    :cond_3
    move v0, v3

    .line 201
    goto :goto_1
.end method

.method public onLongClick(Landroid/view/View;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 229
    iget-object v2, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mBackButton:Landroid/widget/ImageButton;

    if-ne v2, p1, :cond_0

    .line 231
    iget-object v2, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mBackButton:Landroid/widget/ImageButton;

    invoke-direct {p0, v1, v2}, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->displayNavigationPopup(ZLandroid/view/View;)V

    .line 239
    :goto_0
    return v0

    .line 233
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mForwardButton:Landroid/widget/ImageButton;

    if-ne v2, p1, :cond_1

    .line 235
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mForwardButton:Landroid/widget/ImageButton;

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->displayNavigationPopup(ZLandroid/view/View;)V

    goto :goto_0

    :cond_1
    move v0, v1

    .line 239
    goto :goto_0
.end method

.method public onWindowFocusChanged(Z)V
    .locals 1

    .prologue
    .line 245
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mNavigationPopup:Lorg/chromium/chrome/browser/NavigationPopup;

    if-eqz v0, :cond_0

    .line 246
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mNavigationPopup:Lorg/chromium/chrome/browser/NavigationPopup;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/NavigationPopup;->dismiss()V

    .line 247
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mNavigationPopup:Lorg/chromium/chrome/browser/NavigationPopup;

    .line 249
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onWindowFocusChanged(Z)V

    .line 250
    return-void
.end method
