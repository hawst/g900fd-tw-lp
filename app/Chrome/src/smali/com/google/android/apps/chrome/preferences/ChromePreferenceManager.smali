.class public Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;
.super Ljava/lang/Object;
.source "ChromePreferenceManager.java"


# static fields
.field public static final ALLOW_PRERENDER:Ljava/lang/String; = "allow_prefetch"

.field public static final BREAKPAD_UPLOAD_FAIL:Ljava/lang/String; = "breakpad_upload_fail"

.field public static final BREAKPAD_UPLOAD_SUCCESS:Ljava/lang/String; = "breakpad_upload_success"

.field public static final DEFAULT_SEARCH_ENGINE_IS_GOOGLE:Ljava/lang/String; = "default_search_engine_is_google"

.field public static final DELAY_SYNC_SETUP:Ljava/lang/String; = "delay_sync_setup"

.field public static final DISPLAYED_ACCOUNT_MANAGEMENT_UPGRADE_SCREEN:Ljava/lang/String; = "displayed_account_management_upgrade_screen"

.field public static final DISPLAYED_DATA_REDUCTION_INFOBAR:Ljava/lang/String; = "displayed_data_reduction_infobar"

.field public static final DISPLAYED_DATA_REDUCTION_PROMO:Ljava/lang/String; = "displayed_data_reduction_promo"

.field public static final FIRST_RUN_EULA_ACCEPTED:Ljava/lang/String; = "first_run_eula_accepted"

.field public static final FIRST_RUN_FLOW_COMPLETE:Ljava/lang/String; = "first_run_flow"

.field public static final FIRST_RUN_FLOW_SIGNIN_ACCOUNT_NAME:Ljava/lang/String; = "first_run_signin_account_name"

.field public static final FIRST_RUN_FLOW_SIGNIN_COMPLETE:Ljava/lang/String; = "first_run_signin_complete"

.field public static final FIRST_RUN_FLOW_SIGNIN_SETUP_SYNC:Ljava/lang/String; = "first_run_signin_setup_sync"

.field public static final FIRST_RUN_INTRO_PAGE_VISITED_FLAGS:Ljava/lang/String; = "first_run_intro_pages_bitmap"

.field public static final MIGRATION_ON_UPGRADE_ATTEMPTED:Ljava/lang/String; = "migration_on_upgrade_attempted"

.field public static final OPTED_OUT_OF_DOCUMENT_MODE:I = 0x2

.field public static final OPT_OUT_CLEAN_UP_PENDING:Ljava/lang/String; = "opt_out_clean_up_pending"

.field public static final OPT_OUT_PREVIOUS_STATE:Ljava/lang/String; = "opt_out_previous_state"

.field public static final OPT_OUT_PROMO_DISMISSED:I = 0x1

.field public static final OPT_OUT_SHOWN_COUNT:Ljava/lang/String; = "opt_out_shown_count"

.field public static final OPT_OUT_STATE:Ljava/lang/String; = "opt_out_state"

.field public static final PREF_ACCEPT_COOKIES:Ljava/lang/String; = "accept_cookies"

.field public static final PREF_APPLICATION_VERSION:Ljava/lang/String; = "application_version"

.field public static final PREF_AUTOFILL:Ljava/lang/String; = "autofill"

.field public static final PREF_BANDWIDTH:Ljava/lang/String; = "prefetch_bandwidth"

.field public static final PREF_BANDWIDTH_NO_CELLULAR:Ljava/lang/String; = "prefetch_bandwidth_no_cellular"

.field public static final PREF_BLOCK_POPUPS:Ljava/lang/String; = "block_popups"

.field public static final PREF_CRASH_DUMP_UPLOAD:Ljava/lang/String; = "crash_dump_upload"

.field public static final PREF_CRASH_DUMP_UPLOAD_NO_CELLULAR:Ljava/lang/String; = "crash_dump_upload_no_cellular"

.field public static final PREF_DO_NOT_TRACK:Ljava/lang/String; = "do_not_track"

.field public static final PREF_ENABLE_JAVASCRIPT:Ljava/lang/String; = "enable_javascript"

.field public static final PREF_ENABLE_LOCATION:Ljava/lang/String; = "enable_location"

.field public static final PREF_ENABLE_PROTECTED_CONTENT:Ljava/lang/String; = "enable_protected_content"

.field public static final PREF_ENABLE_TRANSLATE:Ljava/lang/String; = "enable_translate"

.field public static final PREF_ENABLE_WEB_ACCELERATION:Ljava/lang/String; = "enable_web_acceleration"

.field public static final PREF_EXECUTABLE_PATH:Ljava/lang/String; = "executable_path"

.field public static final PREF_GOOGLE_LOCATION_SETTINGS:Ljava/lang/String; = "google_location_settings"

.field public static final PREF_HARDWARE_ACCELERATION:Ljava/lang/String; = "hardware_acceleration"

.field public static final PREF_HOMEPAGE:Ljava/lang/String; = "homepage"

.field public static final PREF_HOMEPAGE_CUSTOM_URI:Ljava/lang/String; = "homepage_custom_uri"

.field public static final PREF_HOMEPAGE_PARTNER_ENABLED:Ljava/lang/String; = "homepage_partner_enabled"

.field public static final PREF_JAVASCRIPT_VERSION:Ljava/lang/String; = "javascript_version"

.field public static final PREF_LEGAL_INFORMATION:Ljava/lang/String; = "legal_information"

.field public static final PREF_LOCALE:Ljava/lang/String; = "locale"

.field public static final PREF_NAVIGATION_ERROR:Ljava/lang/String; = "navigation_error"

.field public static final PREF_NETWORK_PREDICTIONS:Ljava/lang/String; = "network_predictions"

.field public static final PREF_NETWORK_PREDICTIONS_NO_CELLULAR:Ljava/lang/String; = "network_predictions_no_cellular"

.field public static final PREF_OMAHA_FORCE_UPDATED:Ljava/lang/String; = "omaha_force_updated"

.field public static final PREF_OS_VERSION:Ljava/lang/String; = "os_version"

.field public static final PREF_PROFILE_PATH:Ljava/lang/String; = "profile_path"

.field public static final PREF_RLZ_NOTIFIED:Ljava/lang/String; = "rlz_first_search_notified"

.field public static final PREF_SAVE_PASSWORDS:Ljava/lang/String; = "save_passwords"

.field public static final PREF_SEARCH_ENGINE:Ljava/lang/String; = "search_engine"

.field public static final PREF_SEARCH_SUGGESTIONS:Ljava/lang/String; = "search_suggestions"

.field public static final PREF_WEBKIT_VERSION:Ljava/lang/String; = "webkit_version"

.field public static final PROMOS_SKIPPED_ON_FIRST_START:Ljava/lang/String; = "promos_skipped_on_first_start"

.field public static final SHOW_SIGNIN_PROMO:Ljava/lang/String; = "show_signin_promo"

.field public static final SIGNIN_PROMO_LAST_SHOWN:Ljava/lang/String; = "signin_promo_last_timestamp_key"

.field public static final SIGN_OUT_ALLOWED:Ljava/lang/String; = "auto_signed_in_school_account"

.field public static final WELCOME_SCREEN_SHOWN:Ljava/lang/String; = "welcome_screen_shown"

.field private static sPrefs:Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mCrashDumpAlwaysUpload:Ljava/lang/String;

.field private final mCrashDumpNeverUpload:Ljava/lang/String;

.field private final mCrashDumpWifiOnlyUpload:Ljava/lang/String;

.field private mCrashUploadingEnabled:Z

.field private final mSharedPreferences:Landroid/content/SharedPreferences;


# direct methods
.method protected constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 126
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 127
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mContext:Landroid/content/Context;

    .line 128
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    .line 129
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mCrashUploadingEnabled:Z

    .line 130
    sget v0, Lcom/google/android/apps/chrome/R$string;->crash_dump_never_upload_value:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mCrashDumpNeverUpload:Ljava/lang/String;

    .line 131
    sget v0, Lcom/google/android/apps/chrome/R$string;->crash_dump_only_with_wifi_value:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mCrashDumpWifiOnlyUpload:Ljava/lang/String;

    .line 132
    sget v0, Lcom/google/android/apps/chrome/R$string;->crash_dump_always_upload_value:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mCrashDumpAlwaysUpload:Ljava/lang/String;

    .line 133
    return-void
.end method

.method private allowUploadCrashDump()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 917
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->isMobileNetworkCapable()Z

    move-result v1

    if-nez v1, :cond_1

    .line 918
    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string/jumbo v2, "crash_dump_upload_no_cellular"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 922
    :cond_0
    :goto_0
    return v0

    .line 920
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string/jumbo v2, "crash_dump_upload"

    iget-object v3, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mCrashDumpNeverUpload:Ljava/lang/String;

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 922
    iget-object v2, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mCrashDumpAlwaysUpload:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mCrashDumpWifiOnlyUpload:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->isWiFiOrEthernetNetwork()Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static changeViewStyle(Landroid/view/View;Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 970
    instance-of v1, p0, Landroid/view/ViewGroup;

    if-eqz v1, :cond_0

    .line 971
    check-cast p0, Landroid/view/ViewGroup;

    .line 972
    :goto_0
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 973
    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->changeViewStyle(Landroid/view/View;Landroid/content/Context;)V

    .line 972
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 975
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-class v2, Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 979
    check-cast p0, Landroid/widget/TextView;

    .line 980
    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 981
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 989
    :cond_1
    :goto_1
    return-void

    .line 982
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Landroid/widget/Switch;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 986
    check-cast p0, Landroid/widget/Switch;

    .line 987
    sget v0, Lcom/google/android/apps/chrome/R$style;->PreferenceHeaderSwitchText:I

    invoke-virtual {p0, p1, v0}, Landroid/widget/Switch;->setSwitchTextAppearance(Landroid/content/Context;I)V

    goto :goto_1
.end method

.method private getActiveNetworkInfo()Landroid/net/NetworkInfo;
    .locals 2

    .prologue
    .line 878
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "connectivity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 880
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    return-object v0
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;
    .locals 2

    .prologue
    .line 141
    const-class v1, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->sPrefs:Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    if-nez v0, :cond_0

    .line 142
    new-instance v0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->sPrefs:Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    .line 144
    :cond_0
    sget-object v0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->sPrefs:Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 141
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public allowUploadCrashDumpNow()Z
    .locals 2

    .prologue
    .line 943
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mCrashUploadingEnabled:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->isNetworkAvailable()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->allowUploadCrashDump()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lorg/chromium/base/CommandLine;->getInstance()Lorg/chromium/base/CommandLine;

    move-result-object v0

    const-string/jumbo v1, "force-dump-upload"

    invoke-virtual {v0, v1}, Lorg/chromium/base/CommandLine;->hasSwitch(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public didOptOutStateChange()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 309
    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string/jumbo v2, "opt_out_state"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string/jumbo v3, "opt_out_previous_state"

    invoke-interface {v2, v3, v0}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    if-eq v1, v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public disableCrashUploading()V
    .locals 1

    .prologue
    .line 932
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mCrashUploadingEnabled:Z

    .line 933
    return-void
.end method

.method public getAllowLowEndDeviceUi()Z
    .locals 3

    .prologue
    .line 622
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "allow_low_end_device_ui"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getBreakpadUploadFailCount()I
    .locals 3

    .prologue
    .line 170
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "breakpad_upload_fail"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public getBreakpadUploadSuccessCount()I
    .locals 3

    .prologue
    .line 152
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "breakpad_upload_success"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public getDelaySync()Z
    .locals 3

    .prologue
    .line 365
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "delay_sync_setup"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getDisplayedAccountManagementUpgradeScreen()Z
    .locals 3

    .prologue
    .line 430
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "displayed_account_management_upgrade_screen"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getDisplayedDataReductionInfoBar()Z
    .locals 3

    .prologue
    .line 413
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "displayed_data_reduction_infobar"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getDisplayedDataReductionPromo()Z
    .locals 3

    .prologue
    .line 378
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "displayed_data_reduction_promo"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getFirstRunEulaAccepted()Z
    .locals 1

    .prologue
    .line 443
    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->isFirstRunEulaAccepted()Z

    move-result v0

    return v0
.end method

.method public getFirstRunFlowComplete()Z
    .locals 3

    .prologue
    .line 189
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "first_run_flow"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getFirstRunFlowSignInAccountName()Ljava/lang/String;
    .locals 3

    .prologue
    .line 223
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "first_run_signin_account_name"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getFirstRunFlowSignInComplete()Z
    .locals 3

    .prologue
    .line 206
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "first_run_signin_complete"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getFirstRunFlowSignInSetupSync()Z
    .locals 3

    .prologue
    .line 240
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "first_run_signin_setup_sync"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getFirstRunIntroPageVisitedFlags()J
    .locals 4

    .prologue
    .line 257
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "first_run_intro_pages_bitmap"

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public getOptOutShownCount()J
    .locals 4

    .prologue
    .line 327
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "opt_out_shown_count"

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public getPrefAutofillPreference()Z
    .locals 3

    .prologue
    .line 486
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->setSharedPreferenceValueFromNative()V

    .line 487
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "autofill"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getPrefCrashDumpUploadPreference()Ljava/lang/String;
    .locals 3

    .prologue
    .line 605
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "crash_dump_upload"

    iget-object v2, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mCrashDumpNeverUpload:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPrefDoNotTrackPreference()Z
    .locals 3

    .prologue
    .line 494
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->setSharedPreferenceValueFromNative()V

    .line 495
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "do_not_track"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getPrefEnableProtectedContent()Z
    .locals 3

    .prologue
    .line 586
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->setSharedPreferenceValueFromNative()V

    .line 587
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "enable_protected_content"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getPrefEnableTranslate()Z
    .locals 3

    .prologue
    .line 595
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->setSharedPreferenceValueFromNative()V

    .line 596
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "enable_translate"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getPrefEnableWebAccelerationPreference()Z
    .locals 3

    .prologue
    .line 576
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->setSharedPreferenceValueFromNative()V

    .line 577
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "enable_web_acceleration"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getPrefHomepageCustomUriPreference()Ljava/lang/String;
    .locals 3

    .prologue
    .line 530
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "homepage_custom_uri"

    const-string/jumbo v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPrefHomepagePartnerEnabledPreference()Z
    .locals 3

    .prologue
    .line 548
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "homepage_partner_enabled"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getPrefHomepagePreference()Z
    .locals 3

    .prologue
    .line 511
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "homepage"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getPrefSavePasswordsPreference()Z
    .locals 3

    .prologue
    .line 567
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->setSharedPreferenceValueFromNative()V

    .line 568
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "save_passwords"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getPrefSearchEnginePreference()I
    .locals 3

    .prologue
    .line 503
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->setSharedPreferenceValueFromNative()V

    .line 504
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "search_engine"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public getPromosSkippedOnFirstStart()Z
    .locals 3

    .prologue
    .line 395
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "promos_skipped_on_first_start"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getRlzNotified()Z
    .locals 3

    .prologue
    .line 451
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "rlz_first_search_notified"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getShowSigninPromo()Z
    .locals 3

    .prologue
    .line 1034
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "show_signin_promo"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getSignOutAllowed()Z
    .locals 3

    .prologue
    .line 461
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "auto_signed_in_school_account"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getSigninPromoShown()Z
    .locals 4

    .prologue
    .line 1015
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "signin_promo_last_timestamp_key"

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    .line 1016
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long v0, v2, v0

    const-wide/32 v2, 0x5265c00

    div-long/2addr v0, v2

    .line 1018
    const-wide/16 v2, 0x78

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasAttemptedMigrationOnUpgrade()Z
    .locals 3

    .prologue
    .line 352
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "migration_on_upgrade_attempted"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public incrementBreakpadUploadFailCount()V
    .locals 1

    .prologue
    .line 180
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getBreakpadUploadFailCount()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->setBreakpadUploadFailCount(I)V

    .line 181
    return-void
.end method

.method public incrementBreakpadUploadSuccessCount()V
    .locals 1

    .prologue
    .line 162
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getBreakpadUploadSuccessCount()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->setBreakpadUploadSuccessCount(I)V

    .line 163
    return-void
.end method

.method public incrementOptOutShownCount()V
    .locals 6

    .prologue
    .line 318
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 319
    const-string/jumbo v1, "opt_out_shown_count"

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getOptOutShownCount()J

    move-result-wide v2

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 320
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 321
    return-void
.end method

.method public isMobileNetworkCapable()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 896
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mContext:Landroid/content/Context;

    const-string/jumbo v2, "connectivity"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    invoke-virtual {v0, v1}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public isNetworkAvailable()Z
    .locals 1

    .prologue
    .line 884
    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 885
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isNeverUploadCrashDump()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 954
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->isMobileNetworkCapable()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 955
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "crash_dump_upload"

    iget-object v2, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mCrashDumpNeverUpload:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mCrashDumpNeverUpload:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 960
    :cond_0
    :goto_0
    return v0

    .line 958
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string/jumbo v2, "crash_dump_upload_no_cellular"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isOptOutCleanUpPending()Z
    .locals 3

    .prologue
    .line 334
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "opt_out_clean_up_pending"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public isOptOutPromoDismissed()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 280
    iget-object v2, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string/jumbo v3, "opt_out_state"

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public isOptedOutOfDocumentMode()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 273
    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string/jumbo v2, "opt_out_state"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public isWiFiOrEthernetNetwork()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 889
    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v1

    .line 890
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getType()I

    move-result v2

    if-eq v2, v0, :cond_0

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getType()I

    move-result v1

    const/16 v2, 0x9

    if-ne v1, v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public migrateNetworkPredictionPreferences()V
    .locals 7

    .prologue
    const/4 v1, 0x1

    .line 683
    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v2

    .line 686
    const/4 v0, 0x0

    .line 688
    :try_start_0
    iget-object v3, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string/jumbo v4, "network_predictions"

    const-string/jumbo v5, ""

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 695
    :goto_0
    if-nez v0, :cond_1

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->networkPredictionOptionsHasUserSetting()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 770
    :cond_0
    :goto_1
    return-void

    .line 690
    :catch_0
    move-exception v0

    move v0, v1

    goto :goto_0

    .line 703
    :cond_1
    sget-object v3, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthType;->PRERENDER_ON_WIFI:Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthType;

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthType;->title()Ljava/lang/String;

    move-result-object v3

    .line 704
    iget-object v4, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string/jumbo v5, "prefetch_bandwidth"

    invoke-interface {v4, v5, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 706
    iget-object v5, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string/jumbo v6, "prefetch_bandwidth_no_cellular"

    invoke-interface {v5, v6, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    .line 710
    if-nez v0, :cond_2

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    if-eq v5, v1, :cond_0

    .line 716
    :cond_2
    sget-object v0, Lcom/google/android/apps/chrome/preferences/NetworkPredictionOptions;->DEFAULT:Lcom/google/android/apps/chrome/preferences/NetworkPredictionOptions;

    .line 718
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->isMobileNetworkCapable()Z

    move-result v1

    if-eqz v1, :cond_b

    .line 719
    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string/jumbo v3, "prefetch_bandwidth"

    invoke-interface {v1, v3}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 720
    invoke-static {v4}, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthType;->getBandwidthFromTitle(Ljava/lang/String;)Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthType;

    move-result-object v1

    .line 722
    sget-object v3, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthType;->NEVER_PRERENDER:Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthType;

    invoke-virtual {v3, v1}, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthType;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 723
    sget-object v0, Lcom/google/android/apps/chrome/preferences/NetworkPredictionOptions;->NETWORK_PREDICTION_NEVER:Lcom/google/android/apps/chrome/preferences/NetworkPredictionOptions;

    .line 741
    :cond_3
    :goto_2
    invoke-virtual {v2}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->networkPredictionEnabledHasUserSetting()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getNetworkPredictionEnabledUserPrefValue()Z

    move-result v1

    if-nez v1, :cond_4

    .line 743
    sget-object v0, Lcom/google/android/apps/chrome/preferences/NetworkPredictionOptions;->NETWORK_PREDICTION_NEVER:Lcom/google/android/apps/chrome/preferences/NetworkPredictionOptions;

    .line 746
    :cond_4
    invoke-virtual {v2, v0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->setNetworkPredictionOptions(Lcom/google/android/apps/chrome/preferences/NetworkPredictionOptions;)V

    .line 749
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 751
    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string/jumbo v2, "prefetch_bandwidth"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 752
    const-string/jumbo v1, "prefetch_bandwidth"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 754
    :cond_5
    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string/jumbo v2, "prefetch_bandwidth_no_cellular"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 755
    const-string/jumbo v1, "prefetch_bandwidth_no_cellular"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 759
    :cond_6
    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string/jumbo v2, "allow_prefetch"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 760
    const-string/jumbo v1, "allow_prefetch"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 766
    :cond_7
    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string/jumbo v2, "network_predictions"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 767
    const-string/jumbo v1, "network_predictions"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 769
    :cond_8
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto/16 :goto_1

    .line 724
    :cond_9
    sget-object v3, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthType;->PRERENDER_ON_WIFI:Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthType;

    invoke-virtual {v3, v1}, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthType;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 725
    sget-object v0, Lcom/google/android/apps/chrome/preferences/NetworkPredictionOptions;->NETWORK_PREDICTION_WIFI_ONLY:Lcom/google/android/apps/chrome/preferences/NetworkPredictionOptions;

    goto :goto_2

    .line 726
    :cond_a
    sget-object v3, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthType;->ALWAYS_PRERENDER:Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthType;

    invoke-virtual {v3, v1}, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthType;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 727
    sget-object v0, Lcom/google/android/apps/chrome/preferences/NetworkPredictionOptions;->NETWORK_PREDICTION_ALWAYS:Lcom/google/android/apps/chrome/preferences/NetworkPredictionOptions;

    goto :goto_2

    .line 732
    :cond_b
    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string/jumbo v3, "prefetch_bandwidth_no_cellular"

    invoke-interface {v1, v3}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 733
    if-eqz v5, :cond_c

    .line 734
    sget-object v0, Lcom/google/android/apps/chrome/preferences/NetworkPredictionOptions;->NETWORK_PREDICTION_WIFI_ONLY:Lcom/google/android/apps/chrome/preferences/NetworkPredictionOptions;

    goto/16 :goto_2

    .line 736
    :cond_c
    sget-object v0, Lcom/google/android/apps/chrome/preferences/NetworkPredictionOptions;->NETWORK_PREDICTION_NEVER:Lcom/google/android/apps/chrome/preferences/NetworkPredictionOptions;

    goto/16 :goto_2
.end method

.method public savePreviousOptOutState()V
    .locals 3

    .prologue
    .line 299
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "opt_out_state"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 300
    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 301
    const-string/jumbo v2, "opt_out_previous_state"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 302
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 303
    return-void
.end method

.method public setAllowLowEndDeviceUi()V
    .locals 3

    .prologue
    .line 613
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 614
    const-string/jumbo v1, "allow_low_end_device_ui"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 615
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 616
    return-void
.end method

.method public setAttemptedMigrationOnUpgrade()V
    .locals 3

    .prologue
    .line 359
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 360
    const-string/jumbo v1, "migration_on_upgrade_attempted"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 361
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 362
    return-void
.end method

.method public setAutofillEnabled(Ljava/lang/Boolean;)V
    .locals 1

    .prologue
    .line 850
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-static {v0}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager;->setAutofillEnabled(Z)V

    .line 851
    return-void
.end method

.method public setBreakpadUploadFailCount(I)V
    .locals 2

    .prologue
    .line 174
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 175
    const-string/jumbo v1, "breakpad_upload_fail"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 176
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 177
    return-void
.end method

.method public setBreakpadUploadSuccessCount(I)V
    .locals 2

    .prologue
    .line 156
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 157
    const-string/jumbo v1, "breakpad_upload_success"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 158
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 159
    return-void
.end method

.method public setDelaySync(Z)V
    .locals 2

    .prologue
    .line 369
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 370
    const-string/jumbo v1, "delay_sync_setup"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 371
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 372
    return-void
.end method

.method public setDisplayedAccountManagementUpgradeScreen()V
    .locals 3

    .prologue
    .line 437
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 438
    const-string/jumbo v1, "displayed_account_management_upgrade_screen"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 439
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 440
    return-void
.end method

.method public setDisplayedDataReductionInfoBar(Z)V
    .locals 2

    .prologue
    .line 421
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 422
    const-string/jumbo v1, "displayed_data_reduction_infobar"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 423
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 424
    return-void
.end method

.method public setDisplayedDataReductionPromo(Z)V
    .locals 2

    .prologue
    .line 386
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 387
    const-string/jumbo v1, "displayed_data_reduction_promo"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 388
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 389
    return-void
.end method

.method public setDoNotTrackEnabled(Ljava/lang/Boolean;)V
    .locals 2

    .prologue
    .line 858
    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->setDoNotTrackEnabled(Z)V

    .line 859
    return-void
.end method

.method public setFirstRunEulaAccepted()V
    .locals 1

    .prologue
    .line 447
    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->setEulaAccepted()V

    .line 448
    return-void
.end method

.method public setFirstRunFlowComplete(Z)V
    .locals 2

    .prologue
    .line 197
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 198
    const-string/jumbo v1, "first_run_flow"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 199
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 200
    return-void
.end method

.method public setFirstRunFlowSignInAccountName(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 231
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 232
    const-string/jumbo v1, "first_run_signin_account_name"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 233
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 234
    return-void
.end method

.method public setFirstRunFlowSignInComplete(Z)V
    .locals 2

    .prologue
    .line 214
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 215
    const-string/jumbo v1, "first_run_signin_complete"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 216
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 217
    return-void
.end method

.method public setFirstRunFlowSignInSetupSync(Z)V
    .locals 2

    .prologue
    .line 248
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 249
    const-string/jumbo v1, "first_run_signin_setup_sync"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 250
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 251
    return-void
.end method

.method public setFirstRunIntroPageVisitedFlags(J)V
    .locals 3

    .prologue
    .line 264
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 265
    const-string/jumbo v1, "first_run_intro_pages_bitmap"

    invoke-interface {v0, v1, p1, p2}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 266
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 267
    return-void
.end method

.method public setOptOutCleanUpPending(Z)V
    .locals 2

    .prologue
    .line 343
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 344
    const-string/jumbo v1, "opt_out_clean_up_pending"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 345
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 346
    return-void
.end method

.method public setOptedOutState(I)V
    .locals 3

    .prologue
    .line 288
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "opt_out_state"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 289
    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 290
    const-string/jumbo v2, "opt_out_previous_state"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 291
    const-string/jumbo v0, "opt_out_state"

    invoke-interface {v1, v0, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 292
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 293
    return-void
.end method

.method public setPrefHomepageCustomUriPreference(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 538
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 539
    const-string/jumbo v1, "homepage_custom_uri"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 541
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 542
    return-void
.end method

.method public setPrefHomepagePartnerEnabledPreference(Z)V
    .locals 2

    .prologue
    .line 556
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 557
    const-string/jumbo v1, "homepage_partner_enabled"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 559
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 560
    return-void
.end method

.method public setPrefHomepagePreference(Z)V
    .locals 2

    .prologue
    .line 519
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 520
    const-string/jumbo v1, "homepage"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 521
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 522
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mContext:Landroid/content/Context;

    const/16 v1, 0x46

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastImmediateNotification(Landroid/content/Context;I)V

    .line 524
    return-void
.end method

.method public setPreferenceChanged(Landroid/preference/Preference;Ljava/lang/Object;)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 788
    const-string/jumbo v2, "accept_cookies"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 789
    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v0

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->setAllowCookiesEnabled(Z)V

    .line 827
    :goto_0
    return-void

    .line 790
    :cond_0
    const-string/jumbo v2, "enable_location"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 791
    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v0

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->setAllowLocationEnabled(Z)V

    goto :goto_0

    .line 792
    :cond_1
    const-string/jumbo v2, "enable_javascript"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 793
    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v0

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->setJavaScriptEnabled(Z)V

    goto :goto_0

    .line 794
    :cond_2
    const-string/jumbo v2, "block_popups"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 795
    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v2

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-nez v3, :cond_3

    :goto_1
    invoke-virtual {v2, v0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->setAllowPopupsEnabled(Z)V

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1

    .line 796
    :cond_4
    const-string/jumbo v2, "search_suggestions"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 797
    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v0

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->setSearchSuggestEnabled(Z)V

    goto :goto_0

    .line 798
    :cond_5
    const-string/jumbo v2, "network_predictions"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 799
    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v0

    check-cast p2, Ljava/lang/String;

    invoke-static {p2}, Lcom/google/android/apps/chrome/preferences/NetworkPredictionOptions;->stringToEnum(Ljava/lang/String;)Lcom/google/android/apps/chrome/preferences/NetworkPredictionOptions;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->setNetworkPredictionOptions(Lcom/google/android/apps/chrome/preferences/NetworkPredictionOptions;)V

    .line 801
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mContext:Landroid/content/Context;

    invoke-static {p0, v0}, Lcom/google/android/apps/chrome/precache/PrecacheLauncher;->updatePrecachingEnabled(Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;Landroid/content/Context;)V

    goto/16 :goto_0

    .line 802
    :cond_6
    const-string/jumbo v2, "network_predictions_no_cellular"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 803
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 804
    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/chrome/preferences/NetworkPredictionOptions;->NETWORK_PREDICTION_ALWAYS:Lcom/google/android/apps/chrome/preferences/NetworkPredictionOptions;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->setNetworkPredictionOptions(Lcom/google/android/apps/chrome/preferences/NetworkPredictionOptions;)V

    .line 810
    :goto_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mContext:Landroid/content/Context;

    invoke-static {p0, v0}, Lcom/google/android/apps/chrome/precache/PrecacheLauncher;->updatePrecachingEnabled(Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;Landroid/content/Context;)V

    goto/16 :goto_0

    .line 807
    :cond_7
    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/chrome/preferences/NetworkPredictionOptions;->NETWORK_PREDICTION_NEVER:Lcom/google/android/apps/chrome/preferences/NetworkPredictionOptions;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->setNetworkPredictionOptions(Lcom/google/android/apps/chrome/preferences/NetworkPredictionOptions;)V

    goto :goto_2

    .line 811
    :cond_8
    const-string/jumbo v2, "navigation_error"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 812
    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v0

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->setResolveNavigationErrorEnabled(Z)V

    goto/16 :goto_0

    .line 814
    :cond_9
    const-string/jumbo v2, "enable_web_acceleration"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 815
    invoke-static {}, Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings;->getInstance()Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings;

    move-result-object v0

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings;->setDataReductionProxyEnabled(Z)V

    goto/16 :goto_0

    .line 817
    :cond_a
    const-string/jumbo v2, "do_not_track"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 818
    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v0

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->setDoNotTrackEnabled(Z)V

    goto/16 :goto_0

    .line 819
    :cond_b
    const-string/jumbo v2, "enable_translate"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 820
    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v0

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->setTranslateEnabled(Z)V

    goto/16 :goto_0

    .line 821
    :cond_c
    const-string/jumbo v2, "crash_dump_upload_no_cellular"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 822
    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v0

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->setCrashReporting(Z)V

    goto/16 :goto_0

    .line 823
    :cond_d
    const-string/jumbo v2, "crash_dump_upload"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_f

    .line 825
    iget-object v2, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mCrashDumpNeverUpload:Ljava/lang/String;

    invoke-virtual {p2, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_e

    .line 826
    :goto_3
    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->setCrashReporting(Z)V

    goto/16 :goto_0

    :cond_e
    move v0, v1

    .line 825
    goto :goto_3

    .line 828
    :cond_f
    new-instance v0, Ljava/lang/AssertionError;

    const-string/jumbo v1, "This should never be reached!"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method public setPromosSkippedOnFirstStart(Z)V
    .locals 2

    .prologue
    .line 404
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 405
    const-string/jumbo v1, "promos_skipped_on_first_start"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 406
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 407
    return-void
.end method

.method public setProtectedContent(Z)V
    .locals 1

    .prologue
    .line 475
    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->setProtectedMediaIdentifierEnabled(Z)V

    .line 476
    return-void
.end method

.method public setRememberPasswordsEnabled(Ljava/lang/Boolean;)V
    .locals 2

    .prologue
    .line 866
    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->setRememberPasswordsEnabled(Z)V

    .line 867
    return-void
.end method

.method public setRlzNotified(Z)V
    .locals 2

    .prologue
    .line 455
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 456
    const-string/jumbo v1, "rlz_first_search_notified"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 457
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 458
    return-void
.end method

.method public setSearchEngine(I)V
    .locals 3

    .prologue
    .line 837
    invoke-static {}, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;->getInstance()Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;

    move-result-object v0

    .line 838
    invoke-virtual {v0, p1}, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;->setSearchEngine(I)V

    .line 839
    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string/jumbo v2, "default_search_engine_is_google"

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;->isDefaultSearchEngineGoogle()Z

    move-result v0

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 841
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mContext:Landroid/content/Context;

    const/16 v1, 0x2a

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastImmediateNotification(Landroid/content/Context;I)V

    .line 843
    return-void
.end method

.method public setSharedPreferenceValueFromNative()V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 629
    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->update()V

    .line 630
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->migrateNetworkPredictionPreferences()V

    .line 631
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    .line 633
    const-string/jumbo v0, "search_engine"

    invoke-static {}, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;->getInstance()Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;

    move-result-object v4

    invoke-virtual {v4}, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;->getDefaultSearchEngineIndex()I

    move-result v4

    invoke-interface {v3, v0, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 635
    const-string/jumbo v0, "accept_cookies"

    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->isAcceptCookiesEnabled()Z

    move-result v4

    invoke-interface {v3, v0, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 637
    const-string/jumbo v0, "save_passwords"

    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->isRememberPasswordsEnabled()Z

    move-result v4

    invoke-interface {v3, v0, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 639
    const-string/jumbo v0, "autofill"

    invoke-static {}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager;->isAutofillEnabled()Z

    move-result v4

    invoke-interface {v3, v0, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 641
    const-string/jumbo v0, "do_not_track"

    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->isDoNotTrackEnabled()Z

    move-result v4

    invoke-interface {v3, v0, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 643
    const-string/jumbo v0, "enable_location"

    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->isAllowLocationEnabled()Z

    move-result v4

    invoke-interface {v3, v0, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 645
    const-string/jumbo v0, "enable_javascript"

    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->javaScriptEnabled()Z

    move-result v4

    invoke-interface {v3, v0, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 647
    const-string/jumbo v4, "block_popups"

    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->popupsEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-interface {v3, v4, v0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 649
    const-string/jumbo v0, "search_suggestions"

    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->isSearchSuggestEnabled()Z

    move-result v4

    invoke-interface {v3, v0, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 651
    const-string/jumbo v0, "navigation_error"

    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->isResolveNavigationErrorEnabled()Z

    move-result v4

    invoke-interface {v3, v0, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 653
    const-string/jumbo v0, "enable_web_acceleration"

    invoke-static {}, Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings;->getInstance()Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings;

    move-result-object v4

    invoke-virtual {v4}, Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings;->isDataReductionProxyEnabled()Z

    move-result v4

    invoke-interface {v3, v0, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 655
    const-string/jumbo v0, "enable_protected_content"

    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->isProtectedMediaIdentifierEnabled()Z

    move-result v4

    invoke-interface {v3, v0, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 657
    const-string/jumbo v0, "enable_translate"

    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->isTranslateEnabled()Z

    move-result v4

    invoke-interface {v3, v0, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 660
    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getNetworkPredictionOptions()Lcom/google/android/apps/chrome/preferences/NetworkPredictionOptions;

    move-result-object v0

    .line 662
    const-string/jumbo v4, "network_predictions"

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/NetworkPredictionOptions;->enumToString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 664
    sget-object v4, Lcom/google/android/apps/chrome/preferences/NetworkPredictionOptions;->NETWORK_PREDICTION_NEVER:Lcom/google/android/apps/chrome/preferences/NetworkPredictionOptions;

    if-ne v0, v4, :cond_1

    .line 665
    const-string/jumbo v0, "network_predictions_no_cellular"

    invoke-interface {v3, v0, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 670
    :goto_1
    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 671
    return-void

    :cond_0
    move v0, v2

    .line 647
    goto :goto_0

    .line 667
    :cond_1
    const-string/jumbo v0, "network_predictions_no_cellular"

    invoke-interface {v3, v0, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    goto :goto_1
.end method

.method public setSharedPreferenceValueFromNativeForSearchEngine()V
    .locals 3

    .prologue
    .line 776
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 777
    const-string/jumbo v1, "search_engine"

    invoke-static {}, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;->getInstance()Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;

    move-result-object v2

    invoke-virtual {v2}, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;->getDefaultSearchEngineIndex()I

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 779
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 780
    return-void
.end method

.method public setShowSigninPromo(Z)V
    .locals 2

    .prologue
    .line 1042
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1043
    const-string/jumbo v1, "show_signin_promo"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 1045
    if-eqz p1, :cond_0

    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->recordAndroidSigninPromoEnabled()V

    .line 1046
    :cond_0
    return-void
.end method

.method public setSignOutAllowed(Z)V
    .locals 2

    .prologue
    .line 465
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 466
    const-string/jumbo v1, "auto_signed_in_school_account"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 467
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 468
    return-void
.end method

.method public setSigninPromoShown()V
    .locals 4

    .prologue
    .line 1025
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1026
    const-string/jumbo v1, "signin_promo_last_timestamp_key"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 1027
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 1028
    return-void
.end method

.method public setTranslate(Z)V
    .locals 1

    .prologue
    .line 479
    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->setTranslateEnabled(Z)V

    .line 480
    return-void
.end method

.method public setWebAccelerationEnabled(Ljava/lang/Boolean;)V
    .locals 2

    .prologue
    .line 874
    invoke-static {}, Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings;->getInstance()Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings;->setDataReductionProxyEnabled(Z)V

    .line 875
    return-void
.end method

.method public shouldPrerender()Z
    .locals 1

    .prologue
    .line 905
    invoke-static {}, Lcom/google/android/apps/chrome/device/DeviceClassManager;->enablePrerendering()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 907
    :goto_0
    return v0

    .line 906
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->migrateNetworkPredictionPreferences()V

    .line 907
    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->canPredictNetworkActions()Z

    move-result v0

    goto :goto_0
.end method
