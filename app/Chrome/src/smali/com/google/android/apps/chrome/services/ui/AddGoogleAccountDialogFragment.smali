.class public Lcom/google/android/apps/chrome/services/ui/AddGoogleAccountDialogFragment;
.super Lcom/google/android/apps/chrome/preferences/ChromeBaseDialogFragment;
.source "AddGoogleAccountDialogFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field private mListener:Lcom/google/android/apps/chrome/services/ui/AddGoogleAccountDialogFragment$Listener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBaseDialogFragment;-><init>()V

    .line 25
    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2

    .prologue
    .line 43
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 44
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/ui/AddGoogleAccountDialogFragment;->mListener:Lcom/google/android/apps/chrome/services/ui/AddGoogleAccountDialogFragment$Listener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/services/ui/AddGoogleAccountDialogFragment;->mListener:Lcom/google/android/apps/chrome/services/ui/AddGoogleAccountDialogFragment$Listener;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/services/ui/AddGoogleAccountDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/chrome/services/ui/AddGoogleAccountDialogFragment$Listener;->onAddAccount(Landroid/app/Activity;)V

    .line 46
    :cond_0
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 2

    .prologue
    .line 33
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/services/ui/AddGoogleAccountDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v1, Lcom/google/android/apps/chrome/R$string;->add_account_title:I

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$string;->add_account_continue:I

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$string;->cancel:I

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$string;->add_account_message:I

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method public setListener(Lcom/google/android/apps/chrome/services/ui/AddGoogleAccountDialogFragment$Listener;)V
    .locals 0

    .prologue
    .line 53
    iput-object p1, p0, Lcom/google/android/apps/chrome/services/ui/AddGoogleAccountDialogFragment;->mListener:Lcom/google/android/apps/chrome/services/ui/AddGoogleAccountDialogFragment$Listener;

    .line 54
    return-void
.end method
