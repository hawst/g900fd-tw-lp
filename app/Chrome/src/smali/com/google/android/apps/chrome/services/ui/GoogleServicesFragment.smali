.class public Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;
.super Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;
.source "GoogleServicesFragment.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;
.implements Landroid/preference/Preference$OnPreferenceClickListener;
.implements Lcom/google/android/apps/chrome/services/ui/SignOutDialogFragment$Listener;
.implements Lorg/chromium/chrome/browser/sync/ProfileSyncService$SyncStateChangedListener;


# static fields
.field public static final PREF_ACCOUNTS:Ljava/lang/String; = "accounts"

.field public static final PREF_ACCOUNTS_LIST:Ljava/lang/String; = "accounts_list"

.field public static final PREF_AUTO_LOGIN:Ljava/lang/String; = "auto_login"

.field public static final PREF_SEND_TO_DEVICE:Ljava/lang/String; = "send_to_device"

.field public static final PREF_SERVICES:Ljava/lang/String; = "services"

.field public static final PREF_SYNC:Ljava/lang/String; = "sync"


# instance fields
.field private mAccount:Landroid/accounts/Account;

.field private mAccountsListPref:Landroid/preference/Preference;

.field private mApplicationContext:Landroid/content/Context;

.field private mAutologinPref:Landroid/preference/CheckBoxPreference;

.field private mChromeSigninController:Lorg/chromium/sync/signin/ChromeSigninController;

.field private mGoogleServicesManager:Lcom/google/android/apps/chrome/services/GoogleServicesManager;

.field private mMainThreadHandler:Landroid/os/Handler;

.field private mProfileSyncService:Lorg/chromium/chrome/browser/sync/ProfileSyncService;

.field private mSendToDevicePreference:Landroid/preference/CheckBoxPreference;

.field private mSyncPref:Landroid/preference/Preference;

.field private mSyncStatusHelper:Lorg/chromium/sync/notifier/SyncStatusHelper;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;-><init>()V

    .line 265
    return-void
.end method

.method private displaySignOutDialog()V
    .locals 3

    .prologue
    .line 338
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/sync/signin/ChromeSigninController;->get(Landroid/content/Context;)Lorg/chromium/sync/signin/ChromeSigninController;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/sync/signin/ChromeSigninController;->isSignedIn()Z

    move-result v0

    if-nez v0, :cond_0

    .line 346
    :goto_0
    return-void

    .line 343
    :cond_0
    new-instance v0, Lcom/google/android/apps/chrome/services/ui/SignOutDialogFragment;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/services/ui/SignOutDialogFragment;-><init>()V

    .line 344
    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Lcom/google/android/apps/chrome/services/ui/SignOutDialogFragment;->setTargetFragment(Landroid/app/Fragment;I)V

    .line 345
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/services/ui/SignOutDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private errorSummary(I)Landroid/text/Spannable;
    .locals 1

    .prologue
    .line 395
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->errorSummary(Ljava/lang/String;)Landroid/text/Spannable;

    move-result-object v0

    return-object v0
.end method

.method public static errorSummary(Ljava/lang/String;)Landroid/text/Spannable;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 405
    const-string/jumbo v0, "red"

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    .line 406
    new-instance v1, Landroid/text/SpannableString;

    invoke-direct {v1, p0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 407
    new-instance v2, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v2, v0}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {v1}, Landroid/text/SpannableString;->length()I

    move-result v0

    invoke-virtual {v1, v2, v3, v0, v3}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 408
    return-object v1
.end method

.method private listenForPreferenceChanges()V
    .locals 1

    .prologue
    .line 361
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->mSyncPref:Landroid/preference/Preference;

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 362
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->mSendToDevicePreference:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 363
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->mAutologinPref:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 364
    return-void
.end method

.method private setActionBarTitle()V
    .locals 2

    .prologue
    .line 134
    invoke-static {}, Lorg/chromium/chrome/browser/signin/SigninManager;->isNewProfileManagementEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 135
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$string;->sign_in_accounts:I

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setTitle(I)V

    .line 139
    :goto_0
    return-void

    .line 137
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->mAccount:Landroid/accounts/Account;

    if-nez v0, :cond_1

    const-string/jumbo v0, ""

    :goto_1
    invoke-virtual {v1, v0}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->mAccount:Landroid/accounts/Account;

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    goto :goto_1
.end method

.method private setDefaultSyncPrefSummary()V
    .locals 5

    .prologue
    .line 276
    invoke-static {}, Lorg/chromium/chrome/browser/signin/SigninManager;->isNewProfileManagementEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 277
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->mSyncStatusHelper:Lorg/chromium/sync/notifier/SyncStatusHelper;

    iget-object v1, p0, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->mAccount:Landroid/accounts/Account;

    invoke-virtual {v0, v1}, Lorg/chromium/sync/notifier/SyncStatusHelper;->isSyncEnabled(Landroid/accounts/Account;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 278
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->mSyncPref:Landroid/preference/Preference;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/chrome/R$string;->text_off:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 289
    :goto_0
    return-void

    .line 282
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$string;->sync_pref_is_on:I

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->mAccount:Landroid/accounts/Account;

    iget-object v4, v4, Landroid/accounts/Account;->name:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 283
    iget-object v1, p0, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->mSyncPref:Landroid/preference/Preference;

    .line 285
    :goto_1
    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->mSyncPref:Landroid/preference/Preference;

    iget-object v0, p0, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->mSyncStatusHelper:Lorg/chromium/sync/notifier/SyncStatusHelper;

    iget-object v2, p0, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->mAccount:Landroid/accounts/Account;

    invoke-virtual {v0, v2}, Lorg/chromium/sync/notifier/SyncStatusHelper;->isSyncEnabled(Landroid/accounts/Account;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Lcom/google/android/apps/chrome/R$string;->text_on:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Lcom/google/android/apps/chrome/R$string;->text_off:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method private setPreferencesEnabled(Z)V
    .locals 1

    .prologue
    .line 355
    const-string/jumbo v0, "auto_login"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 356
    const-string/jumbo v0, "send_to_device"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 357
    const-string/jumbo v0, "sync"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 358
    return-void
.end method

.method private stopListeningForPreferenceChanges()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 367
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->mSyncPref:Landroid/preference/Preference;

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 368
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->mSendToDevicePreference:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 369
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->mAutologinPref:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 370
    return-void
.end method

.method private temporarilyDisablePreference(Landroid/preference/Preference;)V
    .locals 4

    .prologue
    .line 379
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 380
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->mMainThreadHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment$2;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment$2;-><init>(Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;Landroid/preference/Preference;)V

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 386
    return-void
.end method

.method private updateCheckedStatusForPreferences()V
    .locals 2

    .prologue
    .line 350
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->mSendToDevicePreference:Landroid/preference/CheckBoxPreference;

    iget-object v1, p0, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->mApplicationContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->legacyIsEnabled(Landroid/content/Context;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 351
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->mAutologinPref:Landroid/preference/CheckBoxPreference;

    iget-object v1, p0, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->mApplicationContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->isAutoLoginEnabled(Landroid/content/Context;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 352
    return-void
.end method

.method private updateSyncStatusDisplay()V
    .locals 3

    .prologue
    .line 240
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->mSyncPref:Landroid/preference/Preference;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 241
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->mChromeSigninController:Lorg/chromium/sync/signin/ChromeSigninController;

    invoke-virtual {v0}, Lorg/chromium/sync/signin/ChromeSigninController;->isSignedIn()Z

    move-result v0

    if-nez v0, :cond_1

    .line 242
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->mApplicationContext:Landroid/content/Context;

    invoke-static {v0}, Lorg/chromium/sync/notifier/SyncStatusHelper;->get(Landroid/content/Context;)Lorg/chromium/sync/notifier/SyncStatusHelper;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/sync/notifier/SyncStatusHelper;->isMasterSyncAutomaticallyEnabled()Z

    move-result v0

    if-nez v0, :cond_4

    .line 243
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->mSyncPref:Landroid/preference/Preference;

    sget v1, Lcom/google/android/apps/chrome/R$string;->sync_android_master_sync_disabled:I

    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->errorSummary(I)Landroid/text/Spannable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 269
    :goto_0
    invoke-static {}, Lorg/chromium/chrome/browser/signin/SigninManager;->isNewProfileManagementEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 270
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/sync/signin/AccountManagerHelper;->get(Landroid/content/Context;)Lorg/chromium/sync/signin/AccountManagerHelper;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/sync/signin/AccountManagerHelper;->getGoogleAccountNames()Ljava/util/List;

    move-result-object v0

    .line 271
    iget-object v1, p0, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->mAccountsListPref:Landroid/preference/Preference;

    const-string/jumbo v2, "\n"

    invoke-static {v2, v0}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 273
    :cond_0
    return-void

    .line 247
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->mProfileSyncService:Lorg/chromium/chrome/browser/sync/ProfileSyncService;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->getAuthError()Lorg/chromium/chrome/browser/sync/GoogleServiceAuthError$State;

    move-result-object v0

    sget-object v1, Lorg/chromium/chrome/browser/sync/GoogleServiceAuthError$State;->NONE:Lorg/chromium/chrome/browser/sync/GoogleServiceAuthError$State;

    if-eq v0, v1, :cond_2

    .line 248
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->mSyncPref:Landroid/preference/Preference;

    iget-object v1, p0, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->mProfileSyncService:Lorg/chromium/chrome/browser/sync/ProfileSyncService;

    invoke-virtual {v1}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->getAuthError()Lorg/chromium/chrome/browser/sync/GoogleServiceAuthError$State;

    move-result-object v1

    invoke-virtual {v1}, Lorg/chromium/chrome/browser/sync/GoogleServiceAuthError$State;->getMessage()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->errorSummary(I)Landroid/text/Spannable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 249
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->mSyncStatusHelper:Lorg/chromium/sync/notifier/SyncStatusHelper;

    invoke-virtual {v0}, Lorg/chromium/sync/notifier/SyncStatusHelper;->isSyncEnabled()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 250
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->mProfileSyncService:Lorg/chromium/chrome/browser/sync/ProfileSyncService;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->isSyncInitialized()Z

    move-result v0

    if-nez v0, :cond_3

    .line 252
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->mSyncPref:Landroid/preference/Preference;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/chrome/R$string;->sync_setup_progress:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 254
    :cond_3
    sget-object v0, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment$3;->$SwitchMap$org$chromium$sync$internal_api$pub$SyncDecryptionPassphraseType:[I

    iget-object v1, p0, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->mProfileSyncService:Lorg/chromium/chrome/browser/sync/ProfileSyncService;

    invoke-virtual {v1}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->getSyncDecryptionPassphraseTypeIfRequired()Lorg/chromium/sync/internal_api/pub/SyncDecryptionPassphraseType;

    move-result-object v1

    invoke-virtual {v1}, Lorg/chromium/sync/internal_api/pub/SyncDecryptionPassphraseType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 265
    :cond_4
    invoke-direct {p0}, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->setDefaultSyncPrefSummary()V

    goto :goto_0

    .line 256
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->mSyncPref:Landroid/preference/Preference;

    sget v1, Lcom/google/android/apps/chrome/R$string;->sync_need_password:I

    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->errorSummary(I)Landroid/text/Spannable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 260
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->mSyncPref:Landroid/preference/Preference;

    sget v1, Lcom/google/android/apps/chrome/R$string;->sync_need_passphrase:I

    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->errorSummary(I)Landroid/text/Spannable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 254
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 94
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    .line 95
    sget v0, Lcom/google/android/apps/chrome/R$xml;->sign_in_preferences:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->addPreferencesFromResource(I)V

    .line 96
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->mApplicationContext:Landroid/content/Context;

    .line 97
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->mApplicationContext:Landroid/content/Context;

    invoke-static {v0}, Lorg/chromium/sync/signin/ChromeSigninController;->get(Landroid/content/Context;)Lorg/chromium/sync/signin/ChromeSigninController;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->mChromeSigninController:Lorg/chromium/sync/signin/ChromeSigninController;

    .line 98
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->mApplicationContext:Landroid/content/Context;

    invoke-static {v0}, Lorg/chromium/sync/notifier/SyncStatusHelper;->get(Landroid/content/Context;)Lorg/chromium/sync/notifier/SyncStatusHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->mSyncStatusHelper:Lorg/chromium/sync/notifier/SyncStatusHelper;

    .line 99
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->mApplicationContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->get(Landroid/content/Context;)Lcom/google/android/apps/chrome/services/GoogleServicesManager;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->mGoogleServicesManager:Lcom/google/android/apps/chrome/services/GoogleServicesManager;

    .line 100
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->mApplicationContext:Landroid/content/Context;

    invoke-static {v0}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->get(Landroid/content/Context;)Lorg/chromium/chrome/browser/sync/ProfileSyncService;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->mProfileSyncService:Lorg/chromium/chrome/browser/sync/ProfileSyncService;

    .line 101
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->mChromeSigninController:Lorg/chromium/sync/signin/ChromeSigninController;

    invoke-virtual {v0}, Lorg/chromium/sync/signin/ChromeSigninController;->getSignedInUser()Landroid/accounts/Account;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->mAccount:Landroid/accounts/Account;

    .line 103
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/ui/base/DeviceFormFactor;->isTablet(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 104
    invoke-direct {p0}, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->setActionBarTitle()V

    .line 107
    :cond_0
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->mMainThreadHandler:Landroid/os/Handler;

    .line 109
    const-string/jumbo v0, "sync"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->mSyncPref:Landroid/preference/Preference;

    .line 110
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->mSyncPref:Landroid/preference/Preference;

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 112
    invoke-static {}, Lorg/chromium/chrome/browser/signin/SigninManager;->isNewProfileManagementEnabled()Z

    move-result v0

    if-nez v0, :cond_1

    .line 113
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    const-string/jumbo v1, "accounts"

    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 114
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->mAccountsListPref:Landroid/preference/Preference;

    .line 119
    :goto_0
    const-string/jumbo v0, "send_to_device"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->mSendToDevicePreference:Landroid/preference/CheckBoxPreference;

    .line 120
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->mSendToDevicePreference:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 122
    const-string/jumbo v0, "auto_login"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->mAutologinPref:Landroid/preference/CheckBoxPreference;

    .line 124
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->mAutologinPref:Landroid/preference/CheckBoxPreference;

    sget v1, Lcom/google/android/apps/chrome/R$string;->sign_in_auto_login_description:I

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setSummary(I)V

    .line 126
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->mSendToDevicePreference:Landroid/preference/CheckBoxPreference;

    sget v1, Lcom/google/android/apps/chrome/R$string;->sign_in_send_to_device_description:I

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setSummary(I)V

    .line 128
    invoke-direct {p0}, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->updateSyncStatusDisplay()V

    .line 130
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->setHasOptionsMenu(Z)V

    .line 131
    return-void

    .line 116
    :cond_1
    const-string/jumbo v0, "accounts_list"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->mAccountsListPref:Landroid/preference/Preference;

    goto :goto_0
.end method

.method public onDismissedSignOut()V
    .locals 0

    .prologue
    .line 335
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 413
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    .line 414
    sget v2, Lcom/google/android/apps/chrome/R$id;->menu_id_disconnect_sync_account:I

    if-ne v1, v2, :cond_0

    .line 415
    invoke-direct {p0}, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->displaySignOutDialog()V

    .line 421
    :goto_0
    return v0

    .line 417
    :cond_0
    sget v2, Lcom/google/android/apps/chrome/R$id;->menu_id_help_sync_account:I

    if-ne v1, v2, :cond_1

    .line 418
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-string/jumbo v2, "mobile_general"

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/chrome/help/GoogleHelpUtils;->show(Landroid/app/Activity;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 421
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 163
    invoke-super {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;->onPause()V

    .line 165
    invoke-direct {p0}, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->stopListeningForPreferenceChanges()V

    .line 166
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->mProfileSyncService:Lorg/chromium/chrome/browser/sync/ProfileSyncService;

    invoke-virtual {v0, p0}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->removeSyncStateChangedListener(Lorg/chromium/chrome/browser/sync/ProfileSyncService$SyncStateChangedListener;)V

    .line 167
    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 212
    const-string/jumbo v0, "GoogleServicesFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "onPreferenceChange(...) "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " := "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 213
    invoke-static {}, Lcom/google/android/apps/chrome/services/GoogleServicesStates;->create()Lcom/google/android/apps/chrome/services/GoogleServicesStates$GoogleServicesStatesBuilder;

    move-result-object v0

    .line 216
    check-cast p2, Ljava/lang/Boolean;

    .line 217
    iget-object v1, p0, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->mSendToDevicePreference:Landroid/preference/CheckBoxPreference;

    if-ne v1, p1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->mSendToDevicePreference:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v1

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eq v1, v2, :cond_2

    .line 219
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/services/GoogleServicesStates$GoogleServicesStatesBuilder;->chromeToMobile(Z)Lcom/google/android/apps/chrome/services/GoogleServicesStates$GoogleServicesStatesBuilder;

    .line 223
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->mGoogleServicesManager:Lcom/google/android/apps/chrome/services/GoogleServicesManager;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/services/GoogleServicesStates$GoogleServicesStatesBuilder;->build()Lcom/google/android/apps/chrome/services/GoogleServicesStates;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->setStates(Lcom/google/android/apps/chrome/services/GoogleServicesStates;)V

    .line 225
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->mAutologinPref:Landroid/preference/CheckBoxPreference;

    if-eq p1, v0, :cond_1

    .line 229
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->temporarilyDisablePreference(Landroid/preference/Preference;)V

    .line 231
    :cond_1
    const/4 v0, 0x1

    return v0

    .line 220
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->mAutologinPref:Landroid/preference/CheckBoxPreference;

    if-ne v1, p1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->mAutologinPref:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v1

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eq v1, v2, :cond_0

    .line 221
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/services/GoogleServicesStates$GoogleServicesStatesBuilder;->autoLogin(Z)Lcom/google/android/apps/chrome/services/GoogleServicesStates$GoogleServicesStatesBuilder;

    goto :goto_0
.end method

.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 8

    .prologue
    const/4 v4, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 176
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/sync/signin/ChromeSigninController;->get(Landroid/content/Context;)Lorg/chromium/sync/signin/ChromeSigninController;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/sync/signin/ChromeSigninController;->isSignedIn()Z

    move-result v0

    if-nez v0, :cond_1

    .line 194
    :cond_0
    :goto_0
    return v6

    .line 180
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->mSyncPref:Landroid/preference/Preference;

    if-ne v0, p1, :cond_2

    .line 181
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 182
    const-string/jumbo v0, "account"

    iget-object v1, p0, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->mAccount:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 183
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceActivity;

    const-class v1, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    sget v3, Lcom/google/android/apps/chrome/R$string;->sign_in_sync:I

    move-object v5, v4

    invoke-virtual/range {v0 .. v6}, Landroid/preference/PreferenceActivity;->startPreferencePanel(Ljava/lang/String;Landroid/os/Bundle;ILjava/lang/CharSequence;Landroid/app/Fragment;I)V

    move v6, v7

    .line 186
    goto :goto_0

    .line 187
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->mSendToDevicePreference:Landroid/preference/CheckBoxPreference;

    if-ne v0, p1, :cond_0

    .line 188
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 189
    invoke-static {v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->createInitializeIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    .line 191
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/snapshot/IntentServiceWithWakeLock;->startServiceWithWakeLock(Landroid/content/Context;Landroid/content/Intent;)V

    move v6, v7

    .line 192
    goto :goto_0
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 143
    invoke-super {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;->onResume()V

    .line 145
    invoke-direct {p0}, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->updateCheckedStatusForPreferences()V

    .line 148
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->mAccount:Landroid/accounts/Account;

    if-eqz v0, :cond_0

    .line 150
    invoke-direct {p0}, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->listenForPreferenceChanges()V

    .line 154
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->mAccount:Landroid/accounts/Account;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->setPreferencesEnabled(Z)V

    .line 156
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->mProfileSyncService:Lorg/chromium/chrome/browser/sync/ProfileSyncService;

    invoke-virtual {v0, p0}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->addSyncStateChangedListener(Lorg/chromium/chrome/browser/sync/ProfileSyncService$SyncStateChangedListener;)V

    .line 158
    invoke-direct {p0}, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->updateSyncStatusDisplay()V

    .line 159
    return-void

    .line 154
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onSignedOut()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 296
    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->setPreferencesEnabled(Z)V

    .line 299
    invoke-direct {p0}, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->stopListeningForPreferenceChanges()V

    .line 300
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->mSendToDevicePreference:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 301
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->mAutologinPref:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 303
    new-instance v0, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment$1;-><init>(Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;)V

    .line 326
    iget-object v1, p0, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->mApplicationContext:Landroid/content/Context;

    invoke-static {v1}, Lorg/chromium/sync/signin/ChromeSigninController;->get(Landroid/content/Context;)Lorg/chromium/sync/signin/ChromeSigninController;

    move-result-object v1

    invoke-virtual {v1}, Lorg/chromium/sync/signin/ChromeSigninController;->isSignedIn()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 327
    iget-object v1, p0, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->mGoogleServicesManager:Lcom/google/android/apps/chrome/services/GoogleServicesManager;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->signOut(Landroid/app/Activity;Ljava/lang/Runnable;)V

    .line 331
    :goto_0
    return-void

    .line 329
    :cond_0
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-virtual {v1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public syncStateChanged()V
    .locals 1

    .prologue
    .line 202
    invoke-direct {p0}, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->updateSyncStatusDisplay()V

    .line 204
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->mAccount:Landroid/accounts/Account;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->setPreferencesEnabled(Z)V

    .line 205
    return-void

    .line 204
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
