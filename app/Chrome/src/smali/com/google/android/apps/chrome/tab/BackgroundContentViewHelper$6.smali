.class Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper$6;
.super Lorg/chromium/content/browser/ContentViewClient;
.source "BackgroundContentViewHelper.java"


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;)V
    .locals 0

    .prologue
    .line 446
    iput-object p1, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper$6;->this$0:Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;

    invoke-direct {p0}, Lorg/chromium/content/browser/ContentViewClient;-><init>()V

    return-void
.end method


# virtual methods
.method public onOffsetsForFullscreenChanged(FFF)V
    .locals 2

    .prologue
    .line 455
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper$6;->this$0:Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;

    # getter for: Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mPaintedNonEmpty:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->access$400(Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper$6;->this$0:Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->getContentWidthCss()F

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper$6;->this$0:Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;

    # getter for: Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mTab:Lorg/chromium/chrome/browser/Tab;
    invoke-static {v1}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->access$700(Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;)Lorg/chromium/chrome/browser/Tab;

    move-result-object v1

    invoke-virtual {v1}, Lorg/chromium/chrome/browser/Tab;->getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v1

    invoke-virtual {v1}, Lorg/chromium/content/browser/ContentViewCore;->getContentWidthCss()F

    move-result v1

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper$6;->this$0:Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;

    # getter for: Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mSwapInProgress:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->access$800(Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 466
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper$6;->this$0:Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;

    # operator++ for: Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mNumRendererFramesReceived:I
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->access$908(Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;)I

    .line 467
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper$6;->this$0:Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;

    # invokes: Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->trySwappingBackgroundView()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->access$300(Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;)V

    .line 469
    :cond_0
    return-void
.end method
