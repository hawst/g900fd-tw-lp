.class public Lcom/google/android/apps/chrome/MediaCaptureNotificationService;
.super Landroid/app/Service;
.source "MediaCaptureNotificationService.java"


# static fields
.field public static final LOG_TAG:Ljava/lang/String; = "MediaCaptureNotificationService"

.field public static final MEDIATYPE_AUDIO_AND_VIDEO:I = 0x1

.field public static final MEDIATYPE_AUDIO_ONLY:I = 0x3

.field public static final MEDIATYPE_NO_MEDIA:I = 0x0

.field public static final MEDIATYPE_VIDEO_ONLY:I = 0x2

.field public static final NOTIFICATION_ID:Ljava/lang/String; = "NotificationId"

.field public static final NOTIFICATION_MEDIA_TYPE:Ljava/lang/String; = "NotificationMediaType"

.field public static final NOTIFICATION_MEDIA_URL:Ljava/lang/String; = "NotificationMediaUrl"

.field public static final WEBRTC_NOTIFICATION_IDS:Ljava/lang/String; = "WebRTCNotificationIds"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mNotificationManager:Landroid/app/NotificationManager;

.field private final mNotifications:Landroid/util/SparseIntArray;

.field private mSharedPreferences:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 52
    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/MediaCaptureNotificationService;->mNotifications:Landroid/util/SparseIntArray;

    return-void
.end method

.method private cancelPreviousWebRtcNotifications()V
    .locals 3

    .prologue
    .line 98
    iget-object v0, p0, Lcom/google/android/apps/chrome/MediaCaptureNotificationService;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "WebRTCNotificationIds"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getStringSet(Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    .line 100
    if-nez v0, :cond_0

    .line 108
    :goto_0
    return-void

    .line 101
    :cond_0
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 102
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 103
    iget-object v2, p0, Lcom/google/android/apps/chrome/MediaCaptureNotificationService;->mNotificationManager:Landroid/app/NotificationManager;

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/app/NotificationManager;->cancel(I)V

    goto :goto_1

    .line 105
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/MediaCaptureNotificationService;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 106
    const-string/jumbo v1, "WebRTCNotificationIds"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 107
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_0
.end method

.method private createMediaTabOpenIntent(I)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 185
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.action.MAIN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 186
    const-string/jumbo v1, "com.android.browser.application_id"

    iget-object v2, p0, Lcom/google/android/apps/chrome/MediaCaptureNotificationService;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 187
    sget-object v1, Lcom/google/android/apps/chrome/IntentHandler$TabOpenType;->BRING_TAB_TO_FRONT:Lcom/google/android/apps/chrome/IntentHandler$TabOpenType;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/IntentHandler$TabOpenType;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 188
    iget-object v1, p0, Lcom/google/android/apps/chrome/MediaCaptureNotificationService;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 189
    return-object v0
.end method

.method private createNotification(IILjava/lang/String;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v1, 0x0

    .line 148
    .line 150
    if-ne p2, v7, :cond_0

    .line 151
    sget v2, Lcom/google/android/apps/chrome/R$string;->video_audio_call_notification_text_2:I

    .line 152
    sget v0, Lcom/google/android/apps/chrome/R$drawable;->webrtc_video:I

    .line 160
    :goto_0
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/MediaCaptureNotificationService;->createMediaTabOpenIntent(I)Landroid/content/Intent;

    move-result-object v3

    .line 161
    iget-object v4, p0, Lcom/google/android/apps/chrome/MediaCaptureNotificationService;->mContext:Landroid/content/Context;

    invoke-static {v4, p1, v3, v1}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    .line 163
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/google/android/apps/chrome/MediaCaptureNotificationService;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v4, ". "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, p0, Lcom/google/android/apps/chrome/MediaCaptureNotificationService;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/google/android/apps/chrome/R$string;->webrtc_call_notification_link_text:I

    new-array v6, v7, [Ljava/lang/Object;

    aput-object p3, v6, v1

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 165
    new-instance v4, Landroid/support/v4/app/L;

    iget-object v5, p0, Lcom/google/android/apps/chrome/MediaCaptureNotificationService;->mContext:Landroid/content/Context;

    invoke-direct {v4, v5}, Landroid/support/v4/app/L;-><init>(Landroid/content/Context;)V

    invoke-virtual {v4, v1}, Landroid/support/v4/app/L;->b(Z)Landroid/support/v4/app/L;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/support/v4/app/L;->a(Z)Landroid/support/v4/app/L;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/support/v4/app/L;->a(Landroid/app/PendingIntent;)Landroid/support/v4/app/L;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/chrome/MediaCaptureNotificationService;->mContext:Landroid/content/Context;

    sget v5, Lcom/google/android/apps/chrome/R$string;->app_name:I

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/support/v4/app/L;->a(Ljava/lang/CharSequence;)Landroid/support/v4/app/L;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/support/v4/app/L;->b(Ljava/lang/CharSequence;)Landroid/support/v4/app/L;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/support/v4/app/L;->a(I)Landroid/support/v4/app/L;

    move-result-object v0

    .line 172
    invoke-static {v0, v7}, Lcom/google/android/apps/chrome/utilities/UnreleasedApiCompatibilityUtils;->setLocalOnly(Landroid/support/v4/app/L;Z)Landroid/support/v4/app/L;

    move-result-object v0

    .line 174
    new-instance v3, Landroid/support/v4/app/K;

    invoke-direct {v3, v0}, Landroid/support/v4/app/K;-><init>(Landroid/support/v4/app/L;)V

    invoke-virtual {v3, v2}, Landroid/support/v4/app/K;->a(Ljava/lang/CharSequence;)Landroid/support/v4/app/K;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/K;->a()Landroid/app/Notification;

    move-result-object v0

    .line 176
    iget-object v2, p0, Lcom/google/android/apps/chrome/MediaCaptureNotificationService;->mNotificationManager:Landroid/app/NotificationManager;

    invoke-virtual {v2, p1, v0}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 177
    iget-object v0, p0, Lcom/google/android/apps/chrome/MediaCaptureNotificationService;->mNotifications:Landroid/util/SparseIntArray;

    invoke-virtual {v0, p1, p2}, Landroid/util/SparseIntArray;->put(II)V

    .line 178
    invoke-direct {p0, p1, v1}, Lcom/google/android/apps/chrome/MediaCaptureNotificationService;->updateSharedPreferencesEntry(IZ)V

    .line 179
    return-void

    .line 153
    :cond_0
    const/4 v0, 0x2

    if-ne p2, v0, :cond_1

    .line 154
    sget v2, Lcom/google/android/apps/chrome/R$string;->video_call_notification_text_2:I

    .line 155
    sget v0, Lcom/google/android/apps/chrome/R$drawable;->webrtc_video:I

    goto/16 :goto_0

    .line 156
    :cond_1
    const/4 v0, 0x3

    if-ne p2, v0, :cond_2

    .line 157
    sget v2, Lcom/google/android/apps/chrome/R$string;->audio_call_notification_text_2:I

    .line 158
    sget v0, Lcom/google/android/apps/chrome/R$drawable;->webrtc_audio:I

    goto/16 :goto_0

    :cond_2
    move v0, v1

    move v2, v1

    goto/16 :goto_0
.end method

.method private destroyNotification(I)V
    .locals 1

    .prologue
    .line 134
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/MediaCaptureNotificationService;->doesNotificationExist(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 135
    iget-object v0, p0, Lcom/google/android/apps/chrome/MediaCaptureNotificationService;->mNotificationManager:Landroid/app/NotificationManager;

    invoke-virtual {v0, p1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 136
    iget-object v0, p0, Lcom/google/android/apps/chrome/MediaCaptureNotificationService;->mNotifications:Landroid/util/SparseIntArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseIntArray;->delete(I)V

    .line 137
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/chrome/MediaCaptureNotificationService;->updateSharedPreferencesEntry(IZ)V

    .line 139
    :cond_0
    return-void
.end method

.method private doesNotificationExist(I)Z
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/android/apps/chrome/MediaCaptureNotificationService;->mNotifications:Landroid/util/SparseIntArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseIntArray;->indexOfKey(I)I

    move-result v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private doesNotificationNeedUpdate(II)Z
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/android/apps/chrome/MediaCaptureNotificationService;->mNotifications:Landroid/util/SparseIntArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseIntArray;->get(I)I

    move-result v0

    if-eq v0, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getMediaType(ZZ)I
    .locals 1

    .prologue
    .line 235
    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    .line 236
    const/4 v0, 0x1

    .line 242
    :goto_0
    return v0

    .line 237
    :cond_0
    if-eqz p0, :cond_1

    if-nez p1, :cond_1

    .line 238
    const/4 v0, 0x3

    goto :goto_0

    .line 239
    :cond_1
    if-nez p0, :cond_2

    if-eqz p1, :cond_2

    .line 240
    const/4 v0, 0x2

    goto :goto_0

    .line 242
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static shouldStartService(Landroid/content/Context;II)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 247
    if-eqz p1, :cond_1

    .line 256
    :cond_0
    :goto_0
    return v0

    .line 248
    :cond_1
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 250
    const-string/jumbo v2, "WebRTCNotificationIds"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getStringSet(Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v1

    .line 251
    if-eqz v1, :cond_2

    invoke-interface {v1}, Ljava/util/Set;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 256
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static updateMediaNotificationForTab(Landroid/content/Context;IZZLjava/lang/String;)V
    .locals 5

    .prologue
    .line 269
    invoke-static {p2, p3}, Lcom/google/android/apps/chrome/MediaCaptureNotificationService;->getMediaType(ZZ)I

    move-result v0

    .line 270
    invoke-static {p0, v0, p1}, Lcom/google/android/apps/chrome/MediaCaptureNotificationService;->shouldStartService(Landroid/content/Context;II)Z

    move-result v1

    if-nez v1, :cond_0

    .line 283
    :goto_0
    return-void

    .line 271
    :cond_0
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/google/android/apps/chrome/MediaCaptureNotificationService;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 272
    const-string/jumbo v2, "NotificationId"

    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 275
    :try_start_0
    new-instance v2, Ljava/net/URL;

    invoke-direct {v2, p4}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 276
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "://"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p4

    .line 280
    :goto_1
    const-string/jumbo v2, "NotificationMediaUrl"

    invoke-virtual {v1, v2, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 281
    const-string/jumbo v2, "NotificationMediaType"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 282
    invoke-virtual {p0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0

    .line 277
    :catch_0
    move-exception v2

    .line 278
    const-string/jumbo v2, "MediaCaptureNotificationService"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Error parsing the webrtc url "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private updateNotification(IILjava/lang/String;)V
    .locals 1

    .prologue
    .line 118
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/MediaCaptureNotificationService;->doesNotificationExist(I)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/MediaCaptureNotificationService;->doesNotificationNeedUpdate(II)Z

    move-result v0

    if-nez v0, :cond_1

    .line 127
    :cond_0
    :goto_0
    return-void

    .line 122
    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/MediaCaptureNotificationService;->destroyNotification(I)V

    .line 123
    if-eqz p2, :cond_2

    .line 124
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/chrome/MediaCaptureNotificationService;->createNotification(IILjava/lang/String;)V

    .line 126
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/MediaCaptureNotificationService;->mNotifications:Landroid/util/SparseIntArray;

    invoke-virtual {v0}, Landroid/util/SparseIntArray;->size()I

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/MediaCaptureNotificationService;->stopSelf()V

    goto :goto_0
.end method

.method private updateSharedPreferencesEntry(IZ)V
    .locals 4

    .prologue
    .line 198
    new-instance v0, Ljava/util/HashSet;

    iget-object v1, p0, Lcom/google/android/apps/chrome/MediaCaptureNotificationService;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string/jumbo v2, "WebRTCNotificationIds"

    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getStringSet(Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 201
    if-eqz p2, :cond_1

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 203
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 207
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/MediaCaptureNotificationService;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 208
    const-string/jumbo v2, "WebRTCNotificationIds"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putStringSet(Ljava/lang/String;Ljava/util/Set;)Landroid/content/SharedPreferences$Editor;

    .line 209
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 210
    return-void

    .line 204
    :cond_1
    if-nez p2, :cond_0

    .line 205
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 226
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 56
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/MediaCaptureNotificationService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/MediaCaptureNotificationService;->mContext:Landroid/content/Context;

    .line 57
    iget-object v0, p0, Lcom/google/android/apps/chrome/MediaCaptureNotificationService;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "notification"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    iput-object v0, p0, Lcom/google/android/apps/chrome/MediaCaptureNotificationService;->mNotificationManager:Landroid/app/NotificationManager;

    .line 59
    iget-object v0, p0, Lcom/google/android/apps/chrome/MediaCaptureNotificationService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/MediaCaptureNotificationService;->mSharedPreferences:Landroid/content/SharedPreferences;

    .line 60
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 61
    return-void
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 214
    invoke-direct {p0}, Lcom/google/android/apps/chrome/MediaCaptureNotificationService;->cancelPreviousWebRtcNotifications()V

    .line 215
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 216
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 3

    .prologue
    .line 83
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-nez v0, :cond_1

    .line 84
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/MediaCaptureNotificationService;->cancelPreviousWebRtcNotifications()V

    .line 90
    :goto_0
    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->onStartCommand(Landroid/content/Intent;II)I

    move-result v0

    return v0

    .line 86
    :cond_1
    const-string/jumbo v0, "NotificationId"

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    const-string/jumbo v1, "NotificationMediaType"

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    const-string/jumbo v2, "NotificationMediaUrl"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/apps/chrome/MediaCaptureNotificationService;->updateNotification(IILjava/lang/String;)V

    goto :goto_0
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .locals 1

    .prologue
    .line 220
    invoke-direct {p0}, Lcom/google/android/apps/chrome/MediaCaptureNotificationService;->cancelPreviousWebRtcNotifications()V

    .line 221
    invoke-super {p0, p1}, Landroid/app/Service;->onUnbind(Landroid/content/Intent;)Z

    move-result v0

    return v0
.end method
