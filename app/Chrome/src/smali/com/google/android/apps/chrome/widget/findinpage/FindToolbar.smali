.class public Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;
.super Landroid/widget/LinearLayout;
.source "FindToolbar.java"

# interfaces
.implements Lcom/google/android/apps/chrome/tab/ChromeTab$FindMatchRectsListener;
.implements Lcom/google/android/apps/chrome/tab/ChromeTab$FindResultListener;


# static fields
.field private static final NOTIFICATIONS:[I


# instance fields
.field private mActive:Z

.field protected mCloseFindButton:Landroid/widget/ImageButton;

.field private mCurrentTab:Lcom/google/android/apps/chrome/tab/ChromeTab;

.field protected mFindNextButton:Landroid/widget/ImageButton;

.field protected mFindPrevButton:Landroid/widget/ImageButton;

.field protected mFindQuery:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$FindQuery;

.field private mFindStatus:Landroid/widget/TextView;

.field private mLastUserSearch:Ljava/lang/String;

.field private final mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

.field private mResultBar:Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;

.field private mSearchKeyShouldTriggerSearch:Z

.field private mSettingFindTextProgrammatically:Z

.field private mShowKeyboardOnceWindowIsFocused:Z

.field protected mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 222
    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->NOTIFICATIONS:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x0
        0x3
        0xc
        0x8
        0x24
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 105
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 47
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mResultBar:Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;

    .line 53
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mLastUserSearch:Ljava/lang/String;

    .line 56
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mSettingFindTextProgrammatically:Z

    .line 59
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mSearchKeyShouldTriggerSearch:Z

    .line 61
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mActive:Z

    .line 230
    new-instance v0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$7;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$7;-><init>(Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    .line 258
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mShowKeyboardOnceWindowIsFocused:Z

    .line 106
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->sendHideFindToolbarNotification()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;Z)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->hideKeyboardAndStartFinding(Z)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;)Z
    .locals 1

    .prologue
    .line 38
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mSearchKeyShouldTriggerSearch:Z

    return v0
.end method

.method static synthetic access$202(Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;Z)Z
    .locals 0

    .prologue
    .line 38
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mSearchKeyShouldTriggerSearch:Z

    return p1
.end method

.method static synthetic access$300(Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;)Z
    .locals 1

    .prologue
    .line 38
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mSettingFindTextProgrammatically:Z

    return v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;)Lcom/google/android/apps/chrome/tab/ChromeTab;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mCurrentTab:Lcom/google/android/apps/chrome/tab/ChromeTab;

    return-object v0
.end method

.method static synthetic access$502(Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 38
    iput-object p1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mLastUserSearch:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$600(Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->showKeyboard()V

    return-void
.end method

.method private hideKeyboardAndStartFinding(Z)V
    .locals 3

    .prologue
    .line 215
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mFindQuery:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$FindQuery;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$FindQuery;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 216
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_0

    .line 220
    :goto_0
    return-void

    .line 218
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mFindQuery:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$FindQuery;

    invoke-static {v1}, Lorg/chromium/ui/UiUtils;->hideKeyboard(Landroid/view/View;)Z

    .line 219
    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mCurrentTab:Lcom/google/android/apps/chrome/tab/ChromeTab;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, p1, v2}, Lcom/google/android/apps/chrome/tab/ChromeTab;->startFinding(Ljava/lang/String;ZZ)V

    goto :goto_0
.end method

.method private populateFindText(Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 462
    if-nez p1, :cond_1

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mSettingFindTextProgrammatically:Z

    .line 463
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mSettingFindTextProgrammatically:Z

    if-eqz v0, :cond_2

    .line 464
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mCurrentTab:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getPreviousFindText()Ljava/lang/String;

    move-result-object v0

    .line 465
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mCurrentTab:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tab/ChromeTab;->isIncognito()Z

    move-result v3

    if-nez v3, :cond_0

    .line 466
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mLastUserSearch:Ljava/lang/String;

    .line 468
    :cond_0
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mSearchKeyShouldTriggerSearch:Z

    .line 472
    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mFindQuery:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$FindQuery;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$FindQuery;->setText(Ljava/lang/CharSequence;)V

    .line 473
    iput-boolean v2, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mSettingFindTextProgrammatically:Z

    .line 474
    return-void

    :cond_1
    move v0, v2

    .line 462
    goto :goto_0

    .line 470
    :cond_2
    iput-boolean v2, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mSearchKeyShouldTriggerSearch:Z

    move-object v0, p1

    goto :goto_1
.end method

.method private sendHideFindToolbarNotification()V
    .locals 2

    .prologue
    .line 254
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->getContext()Landroid/content/Context;

    move-result-object v0

    const/16 v1, 0x2d

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastImmediateNotification(Landroid/content/Context;I)V

    .line 256
    return-void
.end method

.method private setResultsBarVisibility(Z)V
    .locals 3

    .prologue
    .line 485
    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mResultBar:Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mCurrentTab:Lcom/google/android/apps/chrome/tab/ChromeTab;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mCurrentTab:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 487
    new-instance v0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mCurrentTab:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;-><init>(Landroid/content/Context;Lcom/google/android/apps/chrome/tab/ChromeTab;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mResultBar:Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;

    .line 494
    :cond_0
    :goto_0
    return-void

    .line 488
    :cond_1
    if-nez p1, :cond_0

    .line 489
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mResultBar:Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;

    if-eqz v0, :cond_0

    .line 490
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mResultBar:Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->dismiss()V

    .line 491
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mResultBar:Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;

    goto :goto_0
.end method

.method private setStatus(Ljava/lang/String;Z)V
    .locals 2

    .prologue
    .line 497
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mFindStatus:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 498
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->isIncognitoSelected()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 499
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mFindStatus:Landroid/widget/TextView;

    invoke-virtual {p0, p2, v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->getStatusColor(ZZ)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 500
    return-void

    .line 498
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private showKeyboard()V
    .locals 1

    .prologue
    .line 519
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mFindQuery:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$FindQuery;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$FindQuery;->hasWindowFocus()Z

    move-result v0

    if-nez v0, :cond_0

    .line 529
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mShowKeyboardOnceWindowIsFocused:Z

    .line 533
    :goto_0
    return-void

    .line 532
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mFindQuery:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$FindQuery;

    invoke-static {v0}, Lorg/chromium/ui/UiUtils;->showKeyboard(Landroid/view/View;)V

    goto :goto_0
.end method


# virtual methods
.method public activate(Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 393
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mActive:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->isViewAvailable()Z

    move-result v0

    if-nez v0, :cond_1

    .line 412
    :cond_0
    :goto_0
    return-void

    .line 395
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->getCurrentTab()Lorg/chromium/chrome/browser/Tab;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->fromTab(Lorg/chromium/chrome/browser/Tab;)Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mCurrentTab:Lcom/google/android/apps/chrome/tab/ChromeTab;

    .line 396
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mCurrentTab:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->setFindResultListener(Lcom/google/android/apps/chrome/tab/ChromeTab$FindResultListener;)V

    .line 397
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mCurrentTab:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->setFindMatchRectsListener(Lcom/google/android/apps/chrome/tab/ChromeTab$FindMatchRectsListener;)V

    .line 398
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->populateFindText(Ljava/lang/String;)V

    .line 399
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mFindQuery:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$FindQuery;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$FindQuery;->requestFocus()Z

    .line 401
    invoke-direct {p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->showKeyboard()V

    .line 403
    invoke-direct {p0, v3}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->setResultsBarVisibility(Z)V

    .line 404
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->NOTIFICATIONS:[I

    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->registerForNotifications(Landroid/content/Context;[ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    .line 406
    iput-boolean v3, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mActive:Z

    .line 407
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->isIncognitoSelected()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->updateVisualsForTabModel(Z)V

    .line 410
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->getContext()Landroid/content/Context;

    move-result-object v0

    const/16 v1, 0x3b

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastImmediateNotification(Landroid/content/Context;I)V

    goto :goto_0
.end method

.method protected clearResults()V
    .locals 2

    .prologue
    .line 478
    const-string/jumbo v0, ""

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->setStatus(Ljava/lang/String;Z)V

    .line 479
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mResultBar:Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;

    if-eqz v0, :cond_0

    .line 480
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mResultBar:Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->clearMatchRects()V

    .line 482
    :cond_0
    return-void
.end method

.method public deactivate()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 416
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mActive:Z

    if-nez v0, :cond_0

    .line 429
    :goto_0
    return-void

    .line 418
    :cond_0
    invoke-direct {p0, v3}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->setResultsBarVisibility(Z)V

    .line 419
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mCurrentTab:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->setFindResultListener(Lcom/google/android/apps/chrome/tab/ChromeTab$FindResultListener;)V

    .line 420
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mCurrentTab:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->setFindMatchRectsListener(Lcom/google/android/apps/chrome/tab/ChromeTab$FindMatchRectsListener;)V

    .line 421
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mFindQuery:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$FindQuery;

    invoke-static {v0}, Lorg/chromium/ui/UiUtils;->hideKeyboard(Landroid/view/View;)Z

    .line 422
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mFindQuery:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$FindQuery;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$FindQuery;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-lez v0, :cond_1

    .line 423
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->clearResults()V

    .line 424
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mCurrentTab:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->stopFinding()V

    .line 426
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->NOTIFICATIONS:[I

    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->unregisterForNotifications(Landroid/content/Context;[ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    .line 428
    iput-boolean v3, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mActive:Z

    goto :goto_0
.end method

.method protected findResultSelected(Landroid/graphics/Rect;)V
    .locals 0

    .prologue
    .line 212
    return-void
.end method

.method public getFindResultBar()Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;
    .locals 1

    .prologue
    .line 447
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mResultBar:Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;

    return-object v0
.end method

.method protected getStatusColor(ZZ)I
    .locals 2

    .prologue
    .line 508
    if-eqz p1, :cond_0

    sget v0, Lcom/google/android/apps/chrome/R$color;->find_in_page_failed_results_status_color:I

    .line 510
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    return v0

    .line 508
    :cond_0
    sget v0, Lcom/google/android/apps/chrome/R$color;->find_in_page_results_status_color:I

    goto :goto_0
.end method

.method protected isViewAvailable()Z
    .locals 1

    .prologue
    .line 384
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->getCurrentTab()Lorg/chromium/chrome/browser/Tab;

    move-result-object v0

    .line 385
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/Tab;->getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onAttachedToWindow()V
    .locals 3

    .prologue
    .line 537
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mActive:Z

    if-eqz v0, :cond_0

    .line 538
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->NOTIFICATIONS:[I

    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->registerForNotifications(Landroid/content/Context;[ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    .line 541
    :cond_0
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 542
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 3

    .prologue
    .line 546
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->NOTIFICATIONS:[I

    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->unregisterForNotifications(Landroid/content/Context;[ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    .line 548
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    .line 549
    return-void
.end method

.method public onFindMatchRects(Lorg/chromium/chrome/browser/FindMatchRectsDetails;)V
    .locals 4

    .prologue
    .line 282
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mResultBar:Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;

    if-nez v0, :cond_0

    .line 295
    :goto_0
    return-void

    .line 283
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mFindQuery:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$FindQuery;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$FindQuery;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-lez v0, :cond_1

    .line 284
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mResultBar:Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;

    iget v1, p1, Lorg/chromium/chrome/browser/FindMatchRectsDetails;->version:I

    iget-object v2, p1, Lorg/chromium/chrome/browser/FindMatchRectsDetails;->rects:[Landroid/graphics/RectF;

    iget-object v3, p1, Lorg/chromium/chrome/browser/FindMatchRectsDetails;->activeRect:Landroid/graphics/RectF;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->setMatchRects(I[Landroid/graphics/RectF;Landroid/graphics/RectF;)V

    goto :goto_0

    .line 293
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mResultBar:Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->clearMatchRects()V

    goto :goto_0
.end method

.method public onFindResult(Lorg/chromium/chrome/browser/FindNotificationDetails;)V
    .locals 7

    .prologue
    const/4 v0, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 299
    iget-object v3, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mResultBar:Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mResultBar:Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;

    iput-boolean v2, v3, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mWaitingForActivateAck:Z

    .line 301
    :cond_0
    iget v3, p1, Lorg/chromium/chrome/browser/FindNotificationDetails;->activeMatchOrdinal:I

    if-eq v3, v0, :cond_1

    iget v3, p1, Lorg/chromium/chrome/browser/FindNotificationDetails;->numberOfMatches:I

    if-ne v3, v1, :cond_3

    :cond_1
    iget-boolean v3, p1, Lorg/chromium/chrome/browser/FindNotificationDetails;->finalUpdate:Z

    if-nez v3, :cond_3

    .line 355
    :cond_2
    :goto_0
    return-void

    .line 315
    :cond_3
    iget-boolean v3, p1, Lorg/chromium/chrome/browser/FindNotificationDetails;->finalUpdate:Z

    if-eqz v3, :cond_5

    .line 316
    iget v3, p1, Lorg/chromium/chrome/browser/FindNotificationDetails;->numberOfMatches:I

    if-lez v3, :cond_6

    .line 318
    iget-object v3, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mCurrentTab:Lcom/google/android/apps/chrome/tab/ChromeTab;

    iget-object v4, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mResultBar:Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;

    if-eqz v4, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mResultBar:Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;

    iget v0, v0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mRectsVersion:I

    :cond_4
    invoke-virtual {v3, v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->requestFindMatchRects(I)V

    .line 324
    :goto_1
    iget-object v0, p1, Lorg/chromium/chrome/browser/FindNotificationDetails;->rendererSelectionRect:Landroid/graphics/Rect;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->findResultSelected(Landroid/graphics/Rect;)V

    .line 334
    :cond_5
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->getContext()Landroid/content/Context;

    move-result-object v3

    .line 335
    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v4, Lcom/google/android/apps/chrome/R$string;->find_in_page_count:I

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    iget v6, p1, Lorg/chromium/chrome/browser/FindNotificationDetails;->activeMatchOrdinal:I

    invoke-static {v6, v2}, Ljava/lang/Math;->max(II)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v2

    iget v6, p1, Lorg/chromium/chrome/browser/FindNotificationDetails;->numberOfMatches:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v1

    invoke-virtual {v0, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 339
    iget v0, p1, Lorg/chromium/chrome/browser/FindNotificationDetails;->numberOfMatches:I

    if-nez v0, :cond_7

    move v0, v1

    :goto_2
    invoke-direct {p0, v4, v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->setStatus(Ljava/lang/String;Z)V

    .line 342
    iget v0, p1, Lorg/chromium/chrome/browser/FindNotificationDetails;->numberOfMatches:I

    if-nez v0, :cond_2

    iget-boolean v0, p1, Lorg/chromium/chrome/browser/FindNotificationDetails;->finalUpdate:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mCurrentTab:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getPreviousFindText()Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mFindQuery:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$FindQuery;

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$FindQuery;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 345
    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v4, "haptic_feedback_enabled"

    invoke-static {v0, v4, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-ne v0, v1, :cond_8

    .line 348
    :goto_3
    if-eqz v1, :cond_2

    .line 349
    const-string/jumbo v0, "vibrator"

    invoke-virtual {v3, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Vibrator;

    .line 351
    const-wide/16 v2, 0x32

    invoke-virtual {v0, v2, v3}, Landroid/os/Vibrator;->vibrate(J)V

    goto/16 :goto_0

    .line 321
    :cond_6
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->clearResults()V

    goto :goto_1

    :cond_7
    move v0, v2

    .line 339
    goto :goto_2

    :cond_8
    move v1, v2

    .line 345
    goto :goto_3
.end method

.method public onFinishInflate()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 110
    invoke-virtual {p0, v2}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->setOrientation(I)V

    .line 111
    const/16 v0, 0x10

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->setGravity(I)V

    .line 113
    sget v0, Lcom/google/android/apps/chrome/R$id;->find_query:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$FindQuery;

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mFindQuery:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$FindQuery;

    .line 114
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mFindQuery:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$FindQuery;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$FindQuery;->setFindToolbar(Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;)V

    .line 115
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mFindQuery:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$FindQuery;

    const/16 v1, 0xb0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$FindQuery;->setInputType(I)V

    .line 116
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mFindQuery:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$FindQuery;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$FindQuery;->setSelectAllOnFocus(Z)V

    .line 117
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mFindQuery:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$FindQuery;

    new-instance v1, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$1;-><init>(Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$FindQuery;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 128
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mFindQuery:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$FindQuery;

    new-instance v1, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$2;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$2;-><init>(Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$FindQuery;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 164
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mFindQuery:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$FindQuery;

    new-instance v1, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$3;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$3;-><init>(Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$FindQuery;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 181
    sget v0, Lcom/google/android/apps/chrome/R$id;->find_status:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mFindStatus:Landroid/widget/TextView;

    .line 183
    sget v0, Lcom/google/android/apps/chrome/R$id;->find_prev_button:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mFindPrevButton:Landroid/widget/ImageButton;

    .line 184
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mFindPrevButton:Landroid/widget/ImageButton;

    new-instance v1, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$4;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$4;-><init>(Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 191
    sget v0, Lcom/google/android/apps/chrome/R$id;->find_next_button:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mFindNextButton:Landroid/widget/ImageButton;

    .line 192
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mFindNextButton:Landroid/widget/ImageButton;

    new-instance v1, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$5;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$5;-><init>(Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 199
    invoke-virtual {p0, v2}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->setPrevNextEnabled(Z)V

    .line 201
    sget v0, Lcom/google/android/apps/chrome/R$id;->close_find_button:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mCloseFindButton:Landroid/widget/ImageButton;

    .line 202
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mCloseFindButton:Landroid/widget/ImageButton;

    new-instance v1, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$6;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$6;-><init>(Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 208
    return-void
.end method

.method protected onHideAnimationStart()V
    .locals 1

    .prologue
    .line 442
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->setResultsBarVisibility(Z)V

    .line 443
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 4

    .prologue
    .line 262
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onWindowFocusChanged(Z)V

    .line 264
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mShowKeyboardOnceWindowIsFocused:Z

    if-eqz v0, :cond_0

    .line 265
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mShowKeyboardOnceWindowIsFocused:Z

    .line 271
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$8;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$8;-><init>(Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;)V

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 278
    :cond_0
    return-void
.end method

.method public setActionModeCallbackForTextEdit(Landroid/view/ActionMode$Callback;)V
    .locals 1

    .prologue
    .line 377
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mFindQuery:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$FindQuery;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$FindQuery;->setCustomSelectionActionModeCallback(Landroid/view/ActionMode$Callback;)V

    .line 378
    return-void
.end method

.method public setFindText(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 453
    if-nez p1, :cond_0

    const-string/jumbo p1, ""

    .line 454
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->populateFindText(Ljava/lang/String;)V

    .line 455
    return-void
.end method

.method protected setPrevNextEnabled(Z)V
    .locals 1

    .prologue
    .line 514
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mFindPrevButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 515
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mFindNextButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 516
    return-void
.end method

.method public setTabModelSelector(Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;)V
    .locals 1

    .prologue
    .line 359
    iput-object p1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    .line 360
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->isIncognitoSelected()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->updateVisualsForTabModel(Z)V

    .line 361
    return-void

    .line 360
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected updateVisualsForTabModel(Z)V
    .locals 0

    .prologue
    .line 368
    return-void
.end method
