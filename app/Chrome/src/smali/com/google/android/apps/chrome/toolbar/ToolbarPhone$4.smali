.class Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$4;
.super Lcom/google/android/apps/chrome/toolbar/KeyboardNavigationListener;
.source "ToolbarPhone.java"


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)V
    .locals 0

    .prologue
    .line 257
    iput-object p1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$4;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/toolbar/KeyboardNavigationListener;-><init>()V

    return-void
.end method


# virtual methods
.method public getNextFocusBackward()Landroid/view/View;
    .locals 1

    .prologue
    .line 265
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$4;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mToggleTabStackButton:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$500(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Landroid/widget/ImageView;

    move-result-object v0

    return-object v0
.end method

.method public getNextFocusForward()Landroid/view/View;
    .locals 1

    .prologue
    .line 260
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$4;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->getDelegate()Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->getCurrentView()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected handleEnterKeyPress()Z
    .locals 1

    .prologue
    .line 270
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$4;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->getDelegate()Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->getMenuButtonHelper()Lorg/chromium/chrome/browser/appmenu/AppMenuButtonHelper;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/appmenu/AppMenuButtonHelper;->onEnterKeyPress()Z

    move-result v0

    return v0
.end method
