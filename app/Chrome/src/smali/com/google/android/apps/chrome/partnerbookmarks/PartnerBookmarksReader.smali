.class public Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader;
.super Ljava/lang/Object;
.source "PartnerBookmarksReader.java"


# static fields
.field static final synthetic $assertionsDisabled:Z

.field static final INVALID_BOOKMARK_ID:J = -0x1L

.field static final ROOT_FOLDER_ID:J

.field private static sForceDisableEditing:Z

.field private static sInitialized:Z


# instance fields
.field protected mContext:Landroid/content/Context;

.field private mNativePartnerBookmarksReader:J

.field private mTask:Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$ReadBookmarksTask;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 19
    const-class v0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader;->$assertionsDisabled:Z

    .line 22
    sput-boolean v1, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader;->sInitialized:Z

    .line 23
    sput-boolean v1, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader;->sForceDisableEditing:Z

    return-void

    :cond_0
    move v0, v1

    .line 19
    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader;->mNativePartnerBookmarksReader:J

    .line 83
    iput-object p1, p0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader;->mContext:Landroid/content/Context;

    .line 84
    invoke-direct {p0}, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader;->nativeInit()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader;->mNativePartnerBookmarksReader:J

    .line 85
    invoke-static {}, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader;->initializeAndDisableEditingIfNecessary()V

    .line 86
    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader;Ljava/lang/String;Ljava/lang/String;ZJ[B[B)J
    .locals 2

    .prologue
    .line 19
    invoke-direct/range {p0 .. p7}, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader;->onBookmarkPush(Ljava/lang/String;Ljava/lang/String;ZJ[B[B)J

    move-result-wide v0

    return-wide v0
.end method

.method public static disablePartnerBookmarksEditing()V
    .locals 1

    .prologue
    .line 281
    const/4 v0, 0x1

    sput-boolean v0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader;->sForceDisableEditing:Z

    .line 282
    sget-boolean v0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader;->sInitialized:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader;->nativeDisablePartnerBookmarksEditing()V

    .line 283
    :cond_0
    return-void
.end method

.method private static initializeAndDisableEditingIfNecessary()V
    .locals 1

    .prologue
    .line 286
    const/4 v0, 0x1

    sput-boolean v0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader;->sInitialized:Z

    .line 287
    sget-boolean v0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader;->sForceDisableEditing:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader;->disablePartnerBookmarksEditing()V

    .line 288
    :cond_0
    return-void
.end method

.method private isTaskCancelled()Z
    .locals 2

    .prologue
    .line 113
    iget-object v0, p0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader;->mTask:Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$ReadBookmarksTask;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 115
    :goto_0
    return v0

    .line 114
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader;->mTask:Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$ReadBookmarksTask;

    # getter for: Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$ReadBookmarksTask;->mRootSync:Ljava/lang/Object;
    invoke-static {v0}, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$ReadBookmarksTask;->access$100(Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$ReadBookmarksTask;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 115
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader;->mTask:Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$ReadBookmarksTask;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$ReadBookmarksTask;->isCancelled()Z

    move-result v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 116
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private native nativeAddPartnerBookmark(JLjava/lang/String;Ljava/lang/String;ZJ[B[B)J
.end method

.method private native nativeDestroy(J)V
.end method

.method private static native nativeDisablePartnerBookmarksEditing()V
.end method

.method private native nativeInit()J
.end method

.method private native nativePartnerBookmarksCreationComplete(J)V
.end method

.method private native nativeReset(J)V
.end method

.method private onBookmarkPush(Ljava/lang/String;Ljava/lang/String;ZJ[B[B)J
    .locals 10

    .prologue
    .line 131
    invoke-direct {p0}, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader;->isTaskCancelled()Z

    move-result v0

    if-eqz v0, :cond_0

    const-wide/16 v0, -0x1

    .line 132
    :goto_0
    return-wide v0

    :cond_0
    iget-wide v1, p0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader;->mNativePartnerBookmarksReader:J

    move-object v0, p0

    move-object v3, p1

    move-object v4, p2

    move v5, p3

    move-wide v6, p4

    move-object/from16 v8, p6

    move-object/from16 v9, p7

    invoke-direct/range {v0 .. v9}, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader;->nativeAddPartnerBookmark(JLjava/lang/String;Ljava/lang/String;ZJ[B[B)J

    move-result-wide v0

    goto :goto_0
.end method


# virtual methods
.method public cancelReading()V
    .locals 4

    .prologue
    .line 105
    iget-object v0, p0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader;->mTask:Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$ReadBookmarksTask;

    if-nez v0, :cond_0

    .line 109
    :goto_0
    return-void

    .line 106
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader;->mTask:Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$ReadBookmarksTask;

    # getter for: Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$ReadBookmarksTask;->mRootSync:Ljava/lang/Object;
    invoke-static {v0}, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$ReadBookmarksTask;->access$100(Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$ReadBookmarksTask;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 107
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader;->mTask:Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$ReadBookmarksTask;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$ReadBookmarksTask;->cancel(Z)Z

    .line 108
    iget-wide v2, p0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader;->mNativePartnerBookmarksReader:J

    invoke-direct {p0, v2, v3}, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader;->nativeReset(J)V

    .line 109
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method protected getAvailableBookmarks()Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$BookmarkIterator;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksProviderIterator;->createIfAvailable(Landroid/content/ContentResolver;)Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksProviderIterator;

    move-result-object v0

    return-object v0
.end method

.method protected onBookmarksRead()V
    .locals 2

    .prologue
    .line 138
    invoke-direct {p0}, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader;->isTaskCancelled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 142
    :goto_0
    return-void

    .line 139
    :cond_0
    iget-wide v0, p0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader;->mNativePartnerBookmarksReader:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader;->nativePartnerBookmarksCreationComplete(J)V

    .line 140
    iget-wide v0, p0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader;->mNativePartnerBookmarksReader:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader;->nativeDestroy(J)V

    .line 141
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader;->mNativePartnerBookmarksReader:J

    goto :goto_0
.end method

.method public readBookmarks()V
    .locals 4

    .prologue
    .line 92
    iget-wide v0, p0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader;->mNativePartnerBookmarksReader:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 93
    sget-boolean v0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    const-string/jumbo v1, "readBookmarks called after nativeDestroy."

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 96
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader;->cancelReading()V

    .line 97
    new-instance v0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$ReadBookmarksTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$ReadBookmarksTask;-><init>(Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader;Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$1;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader;->mTask:Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$ReadBookmarksTask;

    .line 98
    iget-object v0, p0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader;->mTask:Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$ReadBookmarksTask;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksReader$ReadBookmarksTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 99
    :cond_1
    return-void
.end method
