.class public final enum Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$AuditEvent;
.super Ljava/lang/Enum;
.source "KnoxAuditLogger.java"


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$AuditEvent;

.field public static final enum AUTOFILL_SELECTED:Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$AuditEvent;

.field public static final enum OPEN_POPUP_URL_SUCCESS:Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$AuditEvent;

.field public static final enum OPEN_URL_FAILURE:Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$AuditEvent;

.field public static final enum OPEN_URL_SUCCESS:Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$AuditEvent;


# instance fields
.field private mGroup:I

.field private mName:Ljava/lang/String;

.field private mOutcome:Z

.field private mSeverity:I


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .prologue
    const/4 v15, 0x3

    const/4 v14, 0x2

    const/4 v2, 0x0

    const/4 v6, 0x1

    const/4 v4, 0x5

    .line 21
    new-instance v0, Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$AuditEvent;

    const-string/jumbo v1, "OPEN_URL_SUCCESS"

    const-string/jumbo v3, "Opening url"

    move v5, v4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$AuditEvent;-><init>(Ljava/lang/String;ILjava/lang/String;IIZ)V

    sput-object v0, Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$AuditEvent;->OPEN_URL_SUCCESS:Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$AuditEvent;

    .line 22
    new-instance v7, Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$AuditEvent;

    const-string/jumbo v8, "OPEN_URL_FAILURE"

    const-string/jumbo v10, "Opening url"

    move v9, v6

    move v11, v4

    move v12, v4

    move v13, v2

    invoke-direct/range {v7 .. v13}, Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$AuditEvent;-><init>(Ljava/lang/String;ILjava/lang/String;IIZ)V

    sput-object v7, Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$AuditEvent;->OPEN_URL_FAILURE:Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$AuditEvent;

    .line 23
    new-instance v7, Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$AuditEvent;

    const-string/jumbo v8, "OPEN_POPUP_URL_SUCCESS"

    const-string/jumbo v10, "Opening pop-up for url"

    move v9, v14

    move v11, v4

    move v12, v4

    move v13, v6

    invoke-direct/range {v7 .. v13}, Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$AuditEvent;-><init>(Ljava/lang/String;ILjava/lang/String;IIZ)V

    sput-object v7, Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$AuditEvent;->OPEN_POPUP_URL_SUCCESS:Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$AuditEvent;

    .line 24
    new-instance v7, Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$AuditEvent;

    const-string/jumbo v8, "AUTOFILL_SELECTED"

    const-string/jumbo v10, "Auto-completing data for field"

    move v9, v15

    move v11, v4

    move v12, v4

    move v13, v6

    invoke-direct/range {v7 .. v13}, Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$AuditEvent;-><init>(Ljava/lang/String;ILjava/lang/String;IIZ)V

    sput-object v7, Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$AuditEvent;->AUTOFILL_SELECTED:Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$AuditEvent;

    .line 20
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$AuditEvent;

    sget-object v1, Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$AuditEvent;->OPEN_URL_SUCCESS:Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$AuditEvent;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$AuditEvent;->OPEN_URL_FAILURE:Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$AuditEvent;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$AuditEvent;->OPEN_POPUP_URL_SUCCESS:Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$AuditEvent;

    aput-object v1, v0, v14

    sget-object v1, Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$AuditEvent;->AUTOFILL_SELECTED:Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$AuditEvent;

    aput-object v1, v0, v15

    sput-object v0, Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$AuditEvent;->$VALUES:[Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$AuditEvent;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;IIZ)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 32
    iput-object p3, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$AuditEvent;->mName:Ljava/lang/String;

    .line 33
    iput p4, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$AuditEvent;->mSeverity:I

    .line 34
    iput p5, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$AuditEvent;->mGroup:I

    .line 35
    iput-boolean p6, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$AuditEvent;->mOutcome:Z

    .line 36
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$AuditEvent;
    .locals 1

    .prologue
    .line 20
    const-class v0, Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$AuditEvent;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$AuditEvent;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$AuditEvent;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$AuditEvent;->$VALUES:[Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$AuditEvent;

    invoke-virtual {v0}, [Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$AuditEvent;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$AuditEvent;

    return-object v0
.end method


# virtual methods
.method public final getGroup()I
    .locals 1

    .prologue
    .line 47
    iget v0, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$AuditEvent;->mGroup:I

    return v0
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$AuditEvent;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public final getOutcome()Z
    .locals 1

    .prologue
    .line 51
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$AuditEvent;->mOutcome:Z

    return v0
.end method

.method public final getOutcomeAsString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 55
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$AuditEvent;->mOutcome:Z

    if-eqz v0, :cond_0

    const-string/jumbo v0, "succeeded"

    :goto_0
    return-object v0

    :cond_0
    const-string/jumbo v0, "failed"

    goto :goto_0
.end method

.method public final getSeverity()I
    .locals 1

    .prologue
    .line 43
    iget v0, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$AuditEvent;->mSeverity:I

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$AuditEvent;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
