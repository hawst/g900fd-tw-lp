.class public Lcom/google/android/apps/chrome/videofling/NotificationTransportControl$ListenerService;
.super Landroid/app/Service;
.source "NotificationTransportControl.java"


# static fields
.field public static final ACTION_ID_MUTE:I = 0x3

.field public static final ACTION_ID_PAUSE:I = 0x1

.field public static final ACTION_ID_PLAY:I = 0x0

.field public static final ACTION_ID_SEEK:I = 0x2

.field public static final ACTION_ID_SELECT:I = 0x6

.field public static final ACTION_ID_STOP:I = 0x5

.field public static final ACTION_ID_UNMUTE:I = 0x4

.field private static final ACTION_PREFIX:Ljava/lang/String;

.field private static final ACTION_VERBS:[Ljava/lang/String;

.field public static final SEEK_POSITION:Ljava/lang/String; = "SEEK_POSITION"


# instance fields
.field private mPendingIntents:[Landroid/app/PendingIntent;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 52
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl$ListenerService;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl$ListenerService;->ACTION_PREFIX:Ljava/lang/String;

    .line 67
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "PLAY"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string/jumbo v2, "PAUSE"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string/jumbo v2, "SEEK"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string/jumbo v2, "MUTE"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string/jumbo v2, "UNMUTE"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string/jumbo v2, "STOP"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "SELECT"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl$ListenerService;->ACTION_VERBS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method


# virtual methods
.method public getPendingIntent(I)Landroid/app/PendingIntent;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl$ListenerService;->mPendingIntents:[Landroid/app/PendingIntent;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 78
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 83
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 86
    sget-object v0, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl$ListenerService;->ACTION_VERBS:[Ljava/lang/String;

    array-length v2, v0

    .line 87
    new-array v0, v2, [Landroid/app/PendingIntent;

    iput-object v0, p0, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl$ListenerService;->mPendingIntents:[Landroid/app/PendingIntent;

    move v0, v1

    .line 88
    :goto_0
    if-ge v0, v2, :cond_0

    .line 89
    new-instance v3, Landroid/content/Intent;

    const-class v4, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl$ListenerService;

    invoke-direct {v3, p0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 90
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl$ListenerService;->ACTION_PREFIX:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl$ListenerService;->ACTION_VERBS:[Ljava/lang/String;

    aget-object v5, v5, v0

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 91
    iget-object v4, p0, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl$ListenerService;->mPendingIntents:[Landroid/app/PendingIntent;

    const/high16 v5, 0x10000000

    invoke-static {p0, v1, v3, v5}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    aput-object v3, v4, v0

    .line 88
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 94
    :cond_0
    # getter for: Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->sInstance:Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;
    invoke-static {}, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->access$000()Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;

    move-result-object v0

    if-nez v0, :cond_1

    .line 98
    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->instance(J)Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;

    move-result-object v0

    .line 101
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->getDefaultMediaRouteController()Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    move-result-object v1

    .line 102
    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->setCurrentMediaRouteController(Lcom/google/android/apps/chrome/videofling/MediaRouteController;)V

    .line 103
    const-string/jumbo v2, ""

    invoke-virtual {v1, v2}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->initialize(Ljava/lang/String;)Z

    .line 104
    invoke-virtual {v1}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->reconnectAnyExistingRoute()V

    .line 105
    invoke-virtual {v1}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->prepareMediaRoute()V

    .line 106
    invoke-static {p0, v1}, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->getOrCreate(Landroid/content/Context;Lcom/google/android/apps/chrome/videofling/MediaRouteController;)Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;

    .line 107
    # getter for: Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->sInstance:Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;
    invoke-static {}, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->access$000()Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->addListener(Lcom/google/android/apps/chrome/videofling/TransportControl$Listener;)V

    .line 109
    :cond_1
    # invokes: Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->onServiceStarted(Lcom/google/android/apps/chrome/videofling/NotificationTransportControl$ListenerService;)V
    invoke-static {p0}, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->access$100(Lcom/google/android/apps/chrome/videofling/NotificationTransportControl$ListenerService;)V

    .line 110
    return-void
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 114
    # invokes: Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->onServiceDestroyed()V
    invoke-static {}, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->access$200()V

    .line 115
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 119
    # getter for: Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->sInstance:Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;
    invoke-static {}, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->access$000()Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;

    move-result-object v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    # invokes: Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->checkState(Z)V
    invoke-static {v0}, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->access$300(Z)V

    .line 120
    if-eqz p1, :cond_7

    .line 121
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 122
    if-eqz v0, :cond_7

    sget-object v3, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl$ListenerService;->ACTION_PREFIX:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 123
    # getter for: Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->sInstance:Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;
    invoke-static {}, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->access$000()Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->getListeners()Ljava/util/Set;

    move-result-object v3

    .line 126
    sget-object v4, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl$ListenerService;->ACTION_PREFIX:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 127
    sget-object v4, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl$ListenerService;->ACTION_VERBS:[Ljava/lang/String;

    aget-object v4, v4, v2

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 128
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/videofling/TransportControl$Listener;

    .line 129
    invoke-interface {v0}, Lcom/google/android/apps/chrome/videofling/TransportControl$Listener;->onPlay()V

    goto :goto_1

    :cond_0
    move v0, v2

    .line 119
    goto :goto_0

    .line 131
    :cond_1
    sget-object v4, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl$ListenerService;->ACTION_VERBS:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 132
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/videofling/TransportControl$Listener;

    .line 133
    invoke-interface {v0}, Lcom/google/android/apps/chrome/videofling/TransportControl$Listener;->onPause()V

    goto :goto_2

    .line 135
    :cond_2
    sget-object v4, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl$ListenerService;->ACTION_VERBS:[Ljava/lang/String;

    const/4 v5, 0x2

    aget-object v4, v4, v5

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 136
    const-string/jumbo v0, "SEEK_POSITION"

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 137
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/videofling/TransportControl$Listener;

    .line 138
    invoke-interface {v0, v2}, Lcom/google/android/apps/chrome/videofling/TransportControl$Listener;->onSeek(I)V

    goto :goto_3

    .line 140
    :cond_3
    sget-object v2, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl$ListenerService;->ACTION_VERBS:[Ljava/lang/String;

    const/4 v4, 0x3

    aget-object v2, v2, v4

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 141
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/videofling/TransportControl$Listener;

    .line 142
    invoke-interface {v0}, Lcom/google/android/apps/chrome/videofling/TransportControl$Listener;->onMute()V

    goto :goto_4

    .line 144
    :cond_4
    sget-object v2, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl$ListenerService;->ACTION_VERBS:[Ljava/lang/String;

    const/4 v4, 0x4

    aget-object v2, v2, v4

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 145
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_5
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/videofling/TransportControl$Listener;

    .line 146
    invoke-interface {v0}, Lcom/google/android/apps/chrome/videofling/TransportControl$Listener;->onUnmute()V

    goto :goto_5

    .line 148
    :cond_5
    sget-object v2, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl$ListenerService;->ACTION_VERBS:[Ljava/lang/String;

    const/4 v4, 0x5

    aget-object v2, v2, v4

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 149
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_6
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/videofling/TransportControl$Listener;

    .line 150
    invoke-interface {v0}, Lcom/google/android/apps/chrome/videofling/TransportControl$Listener;->onStop()V

    .line 151
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl$ListenerService;->stopSelf()V

    goto :goto_6

    .line 153
    :cond_6
    sget-object v2, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl$ListenerService;->ACTION_VERBS:[Ljava/lang/String;

    const/4 v4, 0x6

    aget-object v2, v2, v4

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 154
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_7
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/videofling/TransportControl$Listener;

    .line 155
    invoke-interface {v0, p0}, Lcom/google/android/apps/chrome/videofling/TransportControl$Listener;->onSelect(Landroid/content/Context;)V

    goto :goto_7

    .line 161
    :cond_7
    return v1
.end method
