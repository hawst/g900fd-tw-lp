.class public Lcom/google/android/apps/chrome/utilities/StringLinkifier;
.super Ljava/lang/Object;
.source "StringLinkifier.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const-class v0, Lcom/google/android/apps/chrome/utilities/StringLinkifier;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/utilities/StringLinkifier;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    return-void
.end method

.method public static varargs linkify(Ljava/lang/String;[Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;)Landroid/text/SpannableString;
    .locals 10

    .prologue
    const/4 v9, -0x1

    const/4 v1, 0x0

    .line 79
    array-length v2, p1

    move v0, v1

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, p1, v0

    .line 80
    iget-object v4, v3, Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;->startTag:Ljava/lang/String;

    invoke-virtual {p0, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    # setter for: Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;->mStartTagIndex:I
    invoke-static {v3, v4}, Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;->access$002(Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;I)I

    .line 81
    iget-object v4, v3, Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;->endTag:Ljava/lang/String;

    # getter for: Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;->mStartTagIndex:I
    invoke-static {v3}, Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;->access$000(Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;)I

    move-result v5

    iget-object v6, v3, Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;->startTag:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-virtual {p0, v4, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v4

    # setter for: Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;->mEndTagIndex:I
    invoke-static {v3, v4}, Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;->access$102(Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;I)I

    .line 79
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 86
    :cond_0
    invoke-static {p1}, Ljava/util/Arrays;->sort([Ljava/lang/Object;)V

    .line 91
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-direct {v3, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 93
    array-length v4, p1

    move v2, v1

    move v0, v1

    :goto_1
    if-ge v2, v4, :cond_4

    aget-object v5, p1, v2

    .line 95
    # getter for: Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;->mStartTagIndex:I
    invoke-static {v5}, Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;->access$000(Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;)I

    move-result v6

    if-eq v6, v9, :cond_1

    # getter for: Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;->mEndTagIndex:I
    invoke-static {v5}, Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;->access$100(Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;)I

    move-result v6

    if-eq v6, v9, :cond_1

    # getter for: Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;->mStartTagIndex:I
    invoke-static {v5}, Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;->access$000(Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;)I

    move-result v6

    if-ge v6, v0, :cond_2

    .line 97
    :cond_1
    # setter for: Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;->mStartTagIndex:I
    invoke-static {v5, v9}, Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;->access$002(Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;I)I

    .line 98
    const-string/jumbo v6, "Input string is missing tags %s%s: %s"

    const/4 v7, 0x3

    new-array v7, v7, [Ljava/lang/Object;

    iget-object v8, v5, Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;->startTag:Ljava/lang/String;

    aput-object v8, v7, v1

    const/4 v8, 0x1

    iget-object v5, v5, Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;->endTag:Ljava/lang/String;

    aput-object v5, v7, v8

    const/4 v5, 0x2

    aput-object p0, v7, v5

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 100
    const-string/jumbo v6, "StringLinkifier"

    invoke-static {v6, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 101
    sget-boolean v6, Lcom/google/android/apps/chrome/utilities/StringLinkifier;->$assertionsDisabled:Z

    if-nez v6, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0, v5}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 105
    :cond_2
    # getter for: Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;->mStartTagIndex:I
    invoke-static {v5}, Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;->access$000(Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;)I

    move-result v6

    invoke-virtual {v3, p0, v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;II)Ljava/lang/StringBuilder;

    .line 106
    # getter for: Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;->mStartTagIndex:I
    invoke-static {v5}, Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;->access$000(Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;)I

    move-result v0

    iget-object v6, v5, Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;->startTag:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v0, v6

    .line 107
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v6

    # setter for: Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;->mStartTagIndex:I
    invoke-static {v5, v6}, Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;->access$002(Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;I)I

    .line 109
    # getter for: Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;->mEndTagIndex:I
    invoke-static {v5}, Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;->access$100(Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;)I

    move-result v6

    invoke-virtual {v3, p0, v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;II)Ljava/lang/StringBuilder;

    .line 110
    # getter for: Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;->mEndTagIndex:I
    invoke-static {v5}, Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;->access$100(Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;)I

    move-result v0

    iget-object v6, v5, Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;->endTag:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v0, v6

    .line 111
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v6

    # setter for: Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;->mEndTagIndex:I
    invoke-static {v5, v6}, Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;->access$102(Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;I)I

    .line 93
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 113
    :cond_4
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v3, p0, v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;II)Ljava/lang/StringBuilder;

    .line 115
    new-instance v2, Landroid/text/SpannableString;

    invoke-direct {v2, v3}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 116
    array-length v3, p1

    move v0, v1

    :goto_2
    if-ge v0, v3, :cond_6

    aget-object v4, p1, v0

    .line 117
    # getter for: Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;->mStartTagIndex:I
    invoke-static {v4}, Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;->access$000(Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;)I

    move-result v5

    if-eq v5, v9, :cond_5

    .line 118
    iget-object v5, v4, Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;->span:Ljava/lang/Object;

    # getter for: Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;->mStartTagIndex:I
    invoke-static {v4}, Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;->access$000(Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;)I

    move-result v6

    # getter for: Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;->mEndTagIndex:I
    invoke-static {v4}, Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;->access$100(Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;)I

    move-result v4

    invoke-virtual {v2, v5, v6, v4, v1}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 116
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 122
    :cond_6
    return-object v2
.end method
