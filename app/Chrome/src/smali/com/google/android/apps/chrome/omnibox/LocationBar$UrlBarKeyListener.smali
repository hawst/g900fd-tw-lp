.class final Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarKeyListener;
.super Ljava/lang/Object;
.source "LocationBar.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/chrome/omnibox/LocationBar;)V
    .locals 0

    .prologue
    .line 354
    iput-object p1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarKeyListener;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/chrome/omnibox/LocationBar;Lcom/google/android/apps/chrome/omnibox/LocationBar$1;)V
    .locals 0

    .prologue
    .line 354
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarKeyListener;-><init>(Lcom/google/android/apps/chrome/omnibox/LocationBar;)V

    return-void
.end method

.method static synthetic access$2000(Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarKeyListener;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 354
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarKeyListener;->findMatchAndLoadUrl(Ljava/lang/String;)V

    return-void
.end method

.method private findMatchAndLoadUrl(Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 429
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarKeyListener;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    # getter for: Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSuggestionList:Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->access$700(Lcom/google/android/apps/chrome/omnibox/LocationBar;)Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarKeyListener;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    # getter for: Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSuggestionList:Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->access$700(Lcom/google/android/apps/chrome/omnibox/LocationBar;)Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;->isShown()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarKeyListener;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    # getter for: Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSuggestionList:Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->access$700(Lcom/google/android/apps/chrome/omnibox/LocationBar;)Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;->getSelectedItemPosition()I

    move-result v0

    const/4 v2, -0x1

    if-eq v0, v2, :cond_1

    .line 435
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarKeyListener;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    # getter for: Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSuggestionList:Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->access$700(Lcom/google/android/apps/chrome/omnibox/LocationBar;)Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;->getSelectedItemPosition()I

    move-result v1

    .line 437
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarKeyListener;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    # getter for: Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSuggestionListAdapter:Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter;
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->access$1600(Lcom/google/android/apps/chrome/omnibox/LocationBar;)Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter$OmniboxResultItem;

    .line 439
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter$OmniboxResultItem;->getSuggestion()Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;

    move-result-object v0

    .line 457
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarKeyListener;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    # invokes: Lcom/google/android/apps/chrome/omnibox/LocationBar;->updateSuggestionUrlIfNeeded(Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;I)Ljava/lang/String;
    invoke-static {v2, v0, v1}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->access$2300(Lcom/google/android/apps/chrome/omnibox/LocationBar;Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;I)Ljava/lang/String;

    move-result-object v2

    .line 463
    iget-object v3, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarKeyListener;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;->getTransition()I

    move-result v4

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;->getType()Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

    move-result-object v0

    # invokes: Lcom/google/android/apps/chrome/omnibox/LocationBar;->loadUrlFromOmniboxMatch(Ljava/lang/String;IILorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;)V
    invoke-static {v3, v2, v4, v1, v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->access$2400(Lcom/google/android/apps/chrome/omnibox/LocationBar;Ljava/lang/String;IILorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;)V

    .line 465
    :goto_1
    return-void

    .line 440
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarKeyListener;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    # getter for: Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSuggestionItems:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->access$2100(Lcom/google/android/apps/chrome/omnibox/LocationBar;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarKeyListener;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    # getter for: Lcom/google/android/apps/chrome/omnibox/LocationBar;->mUrlTextAfterSuggestionsReceived:Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->access$2200(Lcom/google/android/apps/chrome/omnibox/LocationBar;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 443
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarKeyListener;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    # getter for: Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSuggestionItems:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->access$2100(Lcom/google/android/apps/chrome/omnibox/LocationBar;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter$OmniboxResultItem;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter$OmniboxResultItem;->getSuggestion()Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;

    move-result-object v0

    goto :goto_0

    .line 450
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarKeyListener;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    iget-object v0, v0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mAutocomplete:Lcom/google/android/apps/chrome/omnibox/ChromeAutocompleteController;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/omnibox/ChromeAutocompleteController;->classify(Ljava/lang/String;)Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;

    move-result-object v0

    .line 454
    if-nez v0, :cond_0

    goto :goto_1
.end method


# virtual methods
.method public final onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, -0x1

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 357
    invoke-static {p3}, Lorg/chromium/chrome/browser/util/KeyNavigationUtil;->isGoDown(Landroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarKeyListener;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    # getter for: Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSuggestionList:Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->access$700(Lcom/google/android/apps/chrome/omnibox/LocationBar;)Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarKeyListener;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    # getter for: Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSuggestionList:Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->access$700(Lcom/google/android/apps/chrome/omnibox/LocationBar;)Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;->isShown()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 360
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarKeyListener;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    # getter for: Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSuggestionListAdapter:Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter;
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->access$1600(Lcom/google/android/apps/chrome/omnibox/LocationBar;)Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter;->getCount()I

    move-result v0

    .line 361
    iget-object v3, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarKeyListener;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    # getter for: Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSuggestionList:Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;
    invoke-static {v3}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->access$700(Lcom/google/android/apps/chrome/omnibox/LocationBar;)Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;->getSelectedItemPosition()I

    move-result v3

    add-int/lit8 v4, v0, -0x1

    if-ge v3, v4, :cond_1

    .line 362
    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarKeyListener;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    # setter for: Lcom/google/android/apps/chrome/omnibox/LocationBar;->mIgnoreOmniboxItemSelection:Z
    invoke-static {v0, v2}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->access$1702(Lcom/google/android/apps/chrome/omnibox/LocationBar;Z)Z

    .line 369
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarKeyListener;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    # getter for: Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSuggestionList:Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->access$700(Lcom/google/android/apps/chrome/omnibox/LocationBar;)Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;->getSelectedItemPosition()I

    move-result v0

    if-ne v0, v5, :cond_2

    .line 375
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarKeyListener;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    # getter for: Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSuggestionList:Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->access$700(Lcom/google/android/apps/chrome/omnibox/LocationBar;)Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    .line 376
    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarKeyListener;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    # getter for: Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSuggestionList:Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;
    invoke-static {v1}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->access$700(Lcom/google/android/apps/chrome/omnibox/LocationBar;)Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;->setSelection(I)V

    .line 422
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 366
    goto :goto_0

    .line 379
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarKeyListener;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    # getter for: Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSuggestionList:Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->access$700(Lcom/google/android/apps/chrome/omnibox/LocationBar;)Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0

    .line 381
    :cond_3
    invoke-static {p3}, Lorg/chromium/chrome/browser/util/KeyNavigationUtil;->isGoUp(Landroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarKeyListener;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    # getter for: Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSuggestionList:Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->access$700(Lcom/google/android/apps/chrome/omnibox/LocationBar;)Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;

    move-result-object v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarKeyListener;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    # getter for: Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSuggestionList:Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->access$700(Lcom/google/android/apps/chrome/omnibox/LocationBar;)Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;->isShown()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 384
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarKeyListener;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    # getter for: Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSuggestionList:Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->access$700(Lcom/google/android/apps/chrome/omnibox/LocationBar;)Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;->getSelectedItemPosition()I

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarKeyListener;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    # getter for: Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSuggestionListAdapter:Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter;
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->access$1600(Lcom/google/android/apps/chrome/omnibox/LocationBar;)Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter;->getCount()I

    move-result v0

    if-lez v0, :cond_4

    .line 386
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarKeyListener;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    # setter for: Lcom/google/android/apps/chrome/omnibox/LocationBar;->mIgnoreOmniboxItemSelection:Z
    invoke-static {v0, v2}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->access$1702(Lcom/google/android/apps/chrome/omnibox/LocationBar;Z)Z

    .line 388
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarKeyListener;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    # getter for: Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSuggestionList:Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->access$700(Lcom/google/android/apps/chrome/omnibox/LocationBar;)Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0

    .line 389
    :cond_5
    invoke-static {p3}, Lorg/chromium/chrome/browser/util/KeyNavigationUtil;->isGoRight(Landroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarKeyListener;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    # getter for: Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSuggestionList:Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->access$700(Lcom/google/android/apps/chrome/omnibox/LocationBar;)Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;

    move-result-object v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarKeyListener;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    # getter for: Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSuggestionList:Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->access$700(Lcom/google/android/apps/chrome/omnibox/LocationBar;)Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;->isShown()Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarKeyListener;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    # getter for: Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSuggestionList:Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->access$700(Lcom/google/android/apps/chrome/omnibox/LocationBar;)Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;->getSelectedItemPosition()I

    move-result v0

    if-eq v0, v5, :cond_6

    .line 394
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarKeyListener;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    # getter for: Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSuggestionListAdapter:Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter;
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->access$1600(Lcom/google/android/apps/chrome/omnibox/LocationBar;)Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarKeyListener;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    # getter for: Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSuggestionList:Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;
    invoke-static {v3}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->access$700(Lcom/google/android/apps/chrome/omnibox/LocationBar;)Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;->getSelectedItemPosition()I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter$OmniboxResultItem;

    .line 399
    iget-object v3, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarKeyListener;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    const-string/jumbo v4, ""

    # invokes: Lcom/google/android/apps/chrome/omnibox/LocationBar;->setUrlBarText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    invoke-static {v3, v4, v6, v6}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->access$1800(Lcom/google/android/apps/chrome/omnibox/LocationBar;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    .line 400
    iget-object v3, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarKeyListener;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    iget-object v3, v3, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter$OmniboxResultItem;->getSuggestion()Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;->getFillIntoEdit()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->setText(Ljava/lang/CharSequence;)V

    .line 401
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarKeyListener;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    # getter for: Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSuggestionList:Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->access$700(Lcom/google/android/apps/chrome/omnibox/LocationBar;)Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;->setSelection(I)V

    .line 402
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarKeyListener;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    iget-object v0, v0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    iget-object v2, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarKeyListener;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    iget-object v2, v2, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->length()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->setSelection(I)V

    move v0, v1

    .line 403
    goto/16 :goto_0

    .line 404
    :cond_6
    invoke-static {p3}, Lorg/chromium/chrome/browser/util/KeyNavigationUtil;->isEnter(Landroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarKeyListener;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->getVisibility()I

    move-result v0

    if-nez v0, :cond_8

    .line 406
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarKeyListener;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    iget-object v0, v0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    invoke-static {v0}, Lorg/chromium/ui/UiUtils;->hideKeyboard(Landroid/view/View;)Z

    .line 407
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarKeyListener;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    # setter for: Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSuggestionSelectionInProgress:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->access$1902(Lcom/google/android/apps/chrome/omnibox/LocationBar;Z)Z

    .line 408
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarKeyListener;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    iget-object v0, v0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->getQueryText()Ljava/lang/String;

    move-result-object v0

    .line 409
    iget-object v2, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarKeyListener;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    # getter for: Lcom/google/android/apps/chrome/omnibox/LocationBar;->mNativeInitialized:Z
    invoke-static {v2}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->access$500(Lcom/google/android/apps/chrome/omnibox/LocationBar;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 410
    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarKeyListener;->findMatchAndLoadUrl(Ljava/lang/String;)V

    :goto_1
    move v0, v1

    .line 419
    goto/16 :goto_0

    .line 412
    :cond_7
    iget-object v2, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarKeyListener;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    # getter for: Lcom/google/android/apps/chrome/omnibox/LocationBar;->mDeferredNativeRunnables:Ljava/util/List;
    invoke-static {v2}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->access$1300(Lcom/google/android/apps/chrome/omnibox/LocationBar;)Ljava/util/List;

    move-result-object v2

    new-instance v3, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarKeyListener$1;

    invoke-direct {v3, p0, v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarKeyListener$1;-><init>(Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarKeyListener;Ljava/lang/String;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_8
    move v0, v2

    .line 422
    goto/16 :goto_0
.end method
