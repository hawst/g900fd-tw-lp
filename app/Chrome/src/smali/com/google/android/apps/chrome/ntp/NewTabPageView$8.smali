.class Lcom/google/android/apps/chrome/ntp/NewTabPageView$8;
.super Ljava/lang/Object;
.source "NewTabPageView.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/ntp/NewTabPageView;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/ntp/NewTabPageView;)V
    .locals 0

    .prologue
    .line 357
    iput-object p1, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView$8;->this$0:Lcom/google/android/apps/chrome/ntp/NewTabPageView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 360
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView$8;->this$0:Lcom/google/android/apps/chrome/ntp/NewTabPageView;

    # getter for: Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mPendingSnapScroll:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->access$300(Lcom/google/android/apps/chrome/ntp/NewTabPageView;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 367
    :goto_0
    return-void

    .line 361
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView$8;->this$0:Lcom/google/android/apps/chrome/ntp/NewTabPageView;

    # getter for: Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mScrollView:Lcom/google/android/apps/chrome/ntp/NewTabScrollView;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->access$400(Lcom/google/android/apps/chrome/ntp/NewTabPageView;)Lcom/google/android/apps/chrome/ntp/NewTabScrollView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ntp/NewTabScrollView;->getScrollY()I

    move-result v2

    .line 362
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView$8;->this$0:Lcom/google/android/apps/chrome/ntp/NewTabPageView;

    # getter for: Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mMostVisitedLayout:Lcom/google/android/apps/chrome/ntp/MostVisitedLayout;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->access$500(Lcom/google/android/apps/chrome/ntp/NewTabPageView;)Lcom/google/android/apps/chrome/ntp/MostVisitedLayout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ntp/MostVisitedLayout;->getTop()I

    move-result v0

    iget-object v3, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView$8;->this$0:Lcom/google/android/apps/chrome/ntp/NewTabPageView;

    # getter for: Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mContentView:Landroid/view/ViewGroup;
    invoke-static {v3}, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->access$600(Lcom/google/android/apps/chrome/ntp/NewTabPageView;)Landroid/view/ViewGroup;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getPaddingTop()I

    move-result v3

    sub-int/2addr v0, v3

    .line 363
    if-lez v2, :cond_2

    if-ge v2, v0, :cond_2

    .line 364
    iget-object v3, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView$8;->this$0:Lcom/google/android/apps/chrome/ntp/NewTabPageView;

    # getter for: Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mScrollView:Lcom/google/android/apps/chrome/ntp/NewTabScrollView;
    invoke-static {v3}, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->access$400(Lcom/google/android/apps/chrome/ntp/NewTabPageView;)Lcom/google/android/apps/chrome/ntp/NewTabScrollView;

    move-result-object v3

    div-int/lit8 v4, v0, 0x2

    if-ge v2, v4, :cond_1

    move v0, v1

    :cond_1
    invoke-virtual {v3, v1, v0}, Lcom/google/android/apps/chrome/ntp/NewTabScrollView;->smoothScrollTo(II)V

    .line 366
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView$8;->this$0:Lcom/google/android/apps/chrome/ntp/NewTabPageView;

    # setter for: Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mPendingSnapScroll:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->access$302(Lcom/google/android/apps/chrome/ntp/NewTabPageView;Z)Z

    goto :goto_0
.end method
