.class public Lcom/google/android/apps/chrome/infobar/OpenURLWithIntentInfoBar;
.super Lorg/chromium/chrome/browser/infobar/ConfirmInfoBar;
.source "OpenURLWithIntentInfoBar.java"


# instance fields
.field private mActivityContext:Landroid/content/Context;

.field private mUrl:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 10

    .prologue
    const/4 v4, 0x0

    .line 30
    const-wide/16 v2, 0x0

    sget v5, Lcom/google/android/apps/chrome/R$drawable;->infobar_update_uma:I

    move-object v1, p0

    move-object v6, p2

    move-object v7, v4

    move-object v8, p3

    move-object v9, v4

    invoke-direct/range {v1 .. v9}, Lorg/chromium/chrome/browser/infobar/ConfirmInfoBar;-><init>(JLorg/chromium/chrome/browser/infobar/InfoBarListeners$Confirm;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 31
    iput-object p1, p0, Lcom/google/android/apps/chrome/infobar/OpenURLWithIntentInfoBar;->mActivityContext:Landroid/content/Context;

    .line 32
    iput-object p4, p0, Lcom/google/android/apps/chrome/infobar/OpenURLWithIntentInfoBar;->mUrl:Ljava/lang/String;

    .line 33
    return-void
.end method


# virtual methods
.method public getUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/apps/chrome/infobar/OpenURLWithIntentInfoBar;->mUrl:Ljava/lang/String;

    return-object v0
.end method

.method public onButtonClicked(Z)V
    .locals 3

    .prologue
    .line 37
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/infobar/OpenURLWithIntentInfoBar;->dismissJavaOnlyInfoBar()V

    .line 41
    :try_start_0
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.action.VIEW"

    iget-object v2, p0, Lcom/google/android/apps/chrome/infobar/OpenURLWithIntentInfoBar;->mUrl:Ljava/lang/String;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 42
    iget-object v1, p0, Lcom/google/android/apps/chrome/infobar/OpenURLWithIntentInfoBar;->mActivityContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 46
    :goto_0
    return-void

    .line 43
    :catch_0
    move-exception v0

    .line 44
    const-string/jumbo v0, "OpenURLWithIntentInfoBar"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Failed to launch Activity for: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/apps/chrome/infobar/OpenURLWithIntentInfoBar;->mUrl:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
