.class public Lcom/google/android/apps/chrome/preferences/ContextualSearchEnabler;
.super Lcom/google/android/apps/chrome/preferences/ChromeBaseSwitchEnabler;
.source "ContextualSearchEnabler.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/widget/Switch;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/preferences/ChromeBaseSwitchEnabler;-><init>(Landroid/content/Context;Landroid/widget/Switch;)V

    .line 22
    return-void
.end method

.method private isEnabledOrUninitialized()Z
    .locals 1

    .prologue
    .line 42
    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->isContextualSearchDisabled()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected isSwitchEnabled()Z
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/ContextualSearchEnabler;->isEnabledOrUninitialized()Z

    move-result v0

    return v0
.end method

.method public onValueChanged(Z)V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/ContextualSearchEnabler;->isEnabledOrUninitialized()Z

    move-result v0

    if-eq v0, p1, :cond_0

    .line 30
    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->setContextualSearchState(Z)V

    .line 31
    invoke-static {p1}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma;->logPreferenceChange(Z)V

    .line 33
    :cond_0
    return-void
.end method
