.class public abstract Lcom/google/android/apps/chrome/eventfilter/EventFilter;
.super Ljava/lang/Object;
.source "EventFilter.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private mAutoOffset:Z

.field protected mCurrentTouchOffsetX:F

.field protected mCurrentTouchOffsetY:F

.field private mGestureStarted:Z

.field protected final mHost:Lcom/google/android/apps/chrome/eventfilter/EventFilterHost;

.field protected final mPxToDp:F

.field private mSimulateIntercepting:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    const-class v0, Lcom/google/android/apps/chrome/eventfilter/EventFilter;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/eventfilter/EventFilter;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/google/android/apps/chrome/eventfilter/EventFilterHost;)V
    .locals 1

    .prologue
    .line 30
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/chrome/eventfilter/EventFilter;-><init>(Lcom/google/android/apps/chrome/eventfilter/EventFilterHost;Z)V

    .line 31
    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/chrome/eventfilter/EventFilterHost;Z)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-boolean v0, p0, Lcom/google/android/apps/chrome/eventfilter/EventFilter;->mGestureStarted:Z

    .line 19
    iput-boolean v0, p0, Lcom/google/android/apps/chrome/eventfilter/EventFilter;->mSimulateIntercepting:Z

    .line 21
    iput-boolean v0, p0, Lcom/google/android/apps/chrome/eventfilter/EventFilter;->mAutoOffset:Z

    .line 43
    iput-object p1, p0, Lcom/google/android/apps/chrome/eventfilter/EventFilter;->mHost:Lcom/google/android/apps/chrome/eventfilter/EventFilterHost;

    .line 44
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-interface {p1}, Lcom/google/android/apps/chrome/eventfilter/EventFilterHost;->getView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    div-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/chrome/eventfilter/EventFilter;->mPxToDp:F

    .line 45
    iput-boolean p2, p0, Lcom/google/android/apps/chrome/eventfilter/EventFilter;->mAutoOffset:Z

    .line 46
    return-void
.end method


# virtual methods
.method protected autoOffsetEvents()Z
    .locals 1

    .prologue
    .line 135
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/eventfilter/EventFilter;->mAutoOffset:Z

    return v0
.end method

.method public final onInterceptTouchEvent(Landroid/view/MotionEvent;Z)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 55
    sget-boolean v0, Lcom/google/android/apps/chrome/eventfilter/EventFilter;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/eventfilter/EventFilter;->mGestureStarted:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 57
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/eventfilter/EventFilter;->mAutoOffset:Z

    if-eqz v0, :cond_3

    iget v0, p0, Lcom/google/android/apps/chrome/eventfilter/EventFilter;->mCurrentTouchOffsetX:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_1

    iget v0, p0, Lcom/google/android/apps/chrome/eventfilter/EventFilter;->mCurrentTouchOffsetY:F

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_3

    .line 58
    :cond_1
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v0

    .line 59
    iget v1, p0, Lcom/google/android/apps/chrome/eventfilter/EventFilter;->mCurrentTouchOffsetX:F

    iget v2, p0, Lcom/google/android/apps/chrome/eventfilter/EventFilter;->mCurrentTouchOffsetY:F

    invoke-virtual {v0, v1, v2}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    .line 61
    :goto_0
    invoke-virtual {p0, v0, p2}, Lcom/google/android/apps/chrome/eventfilter/EventFilter;->onInterceptTouchEventInternal(Landroid/view/MotionEvent;Z)Z

    move-result v1

    .line 62
    if-eq v0, p1, :cond_2

    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    .line 63
    :cond_2
    return v1

    :cond_3
    move-object v0, p1

    goto :goto_0
.end method

.method protected abstract onInterceptTouchEventInternal(Landroid/view/MotionEvent;Z)Z
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 94
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/eventfilter/EventFilter;->mAutoOffset:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/chrome/eventfilter/EventFilter;->mCurrentTouchOffsetX:F

    iget v1, p0, Lcom/google/android/apps/chrome/eventfilter/EventFilter;->mCurrentTouchOffsetY:F

    invoke-virtual {p1, v0, v1}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    .line 95
    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/eventfilter/EventFilter;->onTouchEventInternal(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 96
    if-eqz v0, :cond_1

    iget-boolean v1, p0, Lcom/google/android/apps/chrome/eventfilter/EventFilter;->mGestureStarted:Z

    if-nez v1, :cond_1

    .line 97
    iget-object v1, p0, Lcom/google/android/apps/chrome/eventfilter/EventFilter;->mHost:Lcom/google/android/apps/chrome/eventfilter/EventFilterHost;

    invoke-interface {v1}, Lcom/google/android/apps/chrome/eventfilter/EventFilterHost;->onStartGesture()V

    .line 98
    iput-boolean v3, p0, Lcom/google/android/apps/chrome/eventfilter/EventFilter;->mGestureStarted:Z

    .line 100
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v1

    .line 101
    iget-boolean v2, p0, Lcom/google/android/apps/chrome/eventfilter/EventFilter;->mGestureStarted:Z

    if-eqz v2, :cond_3

    if-eq v1, v3, :cond_2

    const/4 v2, 0x3

    if-ne v1, v2, :cond_3

    .line 103
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/chrome/eventfilter/EventFilter;->mHost:Lcom/google/android/apps/chrome/eventfilter/EventFilterHost;

    invoke-interface {v1}, Lcom/google/android/apps/chrome/eventfilter/EventFilterHost;->onEndGesture()V

    .line 104
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/apps/chrome/eventfilter/EventFilter;->mGestureStarted:Z

    .line 106
    :cond_3
    return v0
.end method

.method protected abstract onTouchEventInternal(Landroid/view/MotionEvent;)Z
.end method

.method public setCurrentMotionEventOffsets(FF)V
    .locals 0

    .prologue
    .line 73
    iput p1, p0, Lcom/google/android/apps/chrome/eventfilter/EventFilter;->mCurrentTouchOffsetX:F

    .line 74
    iput p2, p0, Lcom/google/android/apps/chrome/eventfilter/EventFilter;->mCurrentTouchOffsetY:F

    .line 75
    return-void
.end method

.method public simulateTouchEvent(Landroid/view/MotionEvent;Z)Z
    .locals 1

    .prologue
    .line 125
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/eventfilter/EventFilter;->mSimulateIntercepting:Z

    if-nez v0, :cond_1

    .line 126
    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/chrome/eventfilter/EventFilter;->onInterceptTouchEvent(Landroid/view/MotionEvent;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/eventfilter/EventFilter;->mSimulateIntercepting:Z

    .line 128
    :cond_1
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/eventfilter/EventFilter;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method
