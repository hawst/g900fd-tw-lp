.class Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;
.super Ljava/lang/Object;
.source "StackLayout.java"


# instance fields
.field protected mHeight:F

.field protected mWidth:F

.field final synthetic this$0:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;)V
    .locals 1

    .prologue
    .line 613
    iput-object p1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;->this$0:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 614
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->getWidth()F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;->mWidth:F

    .line 615
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->getHeightMinusTopControls()F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;->mHeight:F

    .line 616
    return-void
.end method


# virtual methods
.method getClampedRenderedScrollOffset()F
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 619
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;->this$0:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;

    # getter for: Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;
    invoke-static {v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->access$400(Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;)[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    move-result-object v1

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->isDisplayable()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;->this$0:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;

    # getter for: Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mFlingFromModelChange:Z
    invoke-static {v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->access$500(Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 620
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;->this$0:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;

    # getter for: Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mRenderedScrollOffset:F
    invoke-static {v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->access$600(Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;)F

    move-result v1

    const/high16 v2, -0x40800000    # -1.0f

    invoke-static {v1, v0, v2}, Lorg/chromium/chrome/browser/util/MathUtils;->clamp(FFF)F

    move-result v0

    .line 622
    :cond_1
    return v0
.end method

.method getHeight()F
    .locals 1

    .prologue
    .line 655
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;->mHeight:F

    return v0
.end method

.method getInnerMargin()F
    .locals 4

    .prologue
    .line 627
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;->this$0:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;

    # getter for: Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mInnerMarginPercent:F
    invoke-static {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->access$700(Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;)F

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;->this$0:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;

    # getter for: Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mMinMaxInnerMargin:I
    invoke-static {v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->access$800(Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;)I

    move-result v1

    int-to-float v1, v1

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;->mWidth:F

    const v3, 0x3e2e147b    # 0.17f

    mul-float/2addr v2, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->max(FF)F

    move-result v1

    mul-float/2addr v0, v1

    .line 629
    return v0
.end method

.method getStack0Left()F
    .locals 3

    .prologue
    .line 644
    invoke-static {}, Lorg/chromium/ui/base/LocalizationUtils;->isLayoutRtl()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;->getInnerMargin()F

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;->getClampedRenderedScrollOffset()F

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;->this$0:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;

    # invokes: Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->getFullScrollDistance()F
    invoke-static {v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->access$900(Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;)F

    move-result v2

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;->getClampedRenderedScrollOffset()F

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;->this$0:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;

    # invokes: Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->getFullScrollDistance()F
    invoke-static {v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->access$900(Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;)F

    move-result v1

    mul-float/2addr v0, v1

    goto :goto_0
.end method

.method getStack0ToStack1TranslationX()F
    .locals 2

    .prologue
    .line 663
    invoke-static {}, Lorg/chromium/ui/base/LocalizationUtils;->isLayoutRtl()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;->mWidth:F

    neg-float v0, v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;->getInnerMargin()F

    move-result v1

    add-float/2addr v0, v1

    :goto_0
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    int-to-float v0, v0

    return v0

    :cond_0
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;->mWidth:F

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;->getInnerMargin()F

    move-result v1

    sub-float/2addr v0, v1

    goto :goto_0
.end method

.method getStack0ToStack1TranslationY()F
    .locals 1

    .prologue
    .line 669
    const/4 v0, 0x0

    return v0
.end method

.method getStack0Top()F
    .locals 1

    .prologue
    .line 659
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;->getTopHeightOffset()F

    move-result v0

    return v0
.end method

.method getStackIndexAt(FF)I
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 633
    invoke-static {}, Lorg/chromium/ui/base/LocalizationUtils;->isLayoutRtl()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 635
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;->getStack0Left()F

    move-result v2

    .line 636
    cmpg-float v2, p1, v2

    if-gez v2, :cond_1

    .line 639
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 636
    goto :goto_0

    .line 638
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;->getStack0Left()F

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;->getWidth()F

    move-result v3

    add-float/2addr v2, v3

    .line 639
    cmpg-float v2, p1, v2

    if-gez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method getTopHeightOffset()F
    .locals 2

    .prologue
    .line 673
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;->this$0:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->getHeight()F

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;->this$0:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->getHeightMinusTopControls()F

    move-result v1

    sub-float/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;->this$0:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;

    # getter for: Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStackOffsetYPercent:F
    invoke-static {v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->access$1000(Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;)F

    move-result v1

    mul-float/2addr v0, v1

    return v0
.end method

.method getWidth()F
    .locals 2

    .prologue
    .line 651
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;->mWidth:F

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;->getInnerMargin()F

    move-result v1

    sub-float/2addr v0, v1

    return v0
.end method
