.class Lcom/google/android/apps/chrome/services/GoogleServicesManager$2;
.super Ljava/lang/Object;
.source "GoogleServicesManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/services/GoogleServicesManager;

.field final synthetic val$dataTypes:Ljava/util/Set;

.field final synthetic val$newName:Ljava/lang/String;

.field final synthetic val$states:Lcom/google/android/apps/chrome/services/GoogleServicesStates;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/services/GoogleServicesManager;Ljava/lang/String;Lcom/google/android/apps/chrome/services/GoogleServicesStates;Ljava/util/Set;)V
    .locals 0

    .prologue
    .line 322
    iput-object p1, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager$2;->this$0:Lcom/google/android/apps/chrome/services/GoogleServicesManager;

    iput-object p2, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager$2;->val$newName:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager$2;->val$states:Lcom/google/android/apps/chrome/services/GoogleServicesStates;

    iput-object p4, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager$2;->val$dataTypes:Ljava/util/Set;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 328
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager$2;->this$0:Lcom/google/android/apps/chrome/services/GoogleServicesManager;

    iget-object v0, v0, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->mContext:Landroid/content/Context;

    # invokes: Lcom/google/android/apps/chrome/services/GoogleServicesManager;->clearNewSignedInAccountName(Landroid/content/Context;)V
    invoke-static {v0}, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->access$100(Landroid/content/Context;)V

    .line 329
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager$2;->this$0:Lcom/google/android/apps/chrome/services/GoogleServicesManager;

    iget-object v1, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager$2;->val$newName:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager$2;->val$states:Lcom/google/android/apps/chrome/services/GoogleServicesStates;

    iget-object v3, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager$2;->val$dataTypes:Ljava/util/Set;

    # invokes: Lcom/google/android/apps/chrome/services/GoogleServicesManager;->performResignin(Ljava/lang/String;Lcom/google/android/apps/chrome/services/GoogleServicesStates;Ljava/util/Set;)V
    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->access$200(Lcom/google/android/apps/chrome/services/GoogleServicesManager;Ljava/lang/String;Lcom/google/android/apps/chrome/services/GoogleServicesStates;Ljava/util/Set;)V

    .line 330
    return-void
.end method
