.class public interface abstract Lcom/google/android/apps/chrome/icing/GoogleApiIcingClient;
.super Ljava/lang/Object;
.source "GoogleApiIcingClient.java"


# virtual methods
.method public abstract clearData()Z
.end method

.method public abstract clearLegacyCorpusData()Z
.end method

.method public abstract connectWithTimeout(J)Z
.end method

.method public abstract disconnect()V
.end method

.method public abstract getLastCommittedSeqno()J
.end method

.method public abstract isGooglePlayServicesAvailable()Z
.end method

.method public abstract reportContext(Ljava/lang/String;Ljava/lang/String;)Z
.end method

.method public varargs abstract reportPageVisits([Lcom/google/android/apps/chrome/icing/UsageReport;)Z
.end method

.method public abstract requestIndexing(J)V
.end method
