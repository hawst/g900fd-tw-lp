.class public Lcom/google/android/apps/chrome/widget/FadingShadowView;
.super Landroid/view/View;
.source "FadingShadowView.java"


# instance fields
.field private mFadingShadow:Lcom/google/android/apps/chrome/widget/FadingShadow;

.field private mPosition:I

.field private mStrength:F


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 27
    return-void
.end method


# virtual methods
.method public init(II)V
    .locals 1

    .prologue
    .line 35
    new-instance v0, Lcom/google/android/apps/chrome/widget/FadingShadow;

    invoke-direct {v0, p1}, Lcom/google/android/apps/chrome/widget/FadingShadow;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/FadingShadowView;->mFadingShadow:Lcom/google/android/apps/chrome/widget/FadingShadow;

    .line 36
    iput p2, p0, Lcom/google/android/apps/chrome/widget/FadingShadowView;->mPosition:I

    .line 37
    invoke-static {p0}, Lorg/chromium/base/ApiCompatibilityUtils;->postInvalidateOnAnimation(Landroid/view/View;)V

    .line 38
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    .line 61
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 62
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/FadingShadowView;->mFadingShadow:Lcom/google/android/apps/chrome/widget/FadingShadow;

    if-eqz v0, :cond_0

    .line 63
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/FadingShadowView;->mFadingShadow:Lcom/google/android/apps/chrome/widget/FadingShadow;

    iget v3, p0, Lcom/google/android/apps/chrome/widget/FadingShadowView;->mPosition:I

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/FadingShadowView;->getHeight()I

    move-result v1

    int-to-float v4, v1

    iget v5, p0, Lcom/google/android/apps/chrome/widget/FadingShadowView;->mStrength:F

    move-object v1, p0

    move-object v2, p1

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/chrome/widget/FadingShadow;->drawShadow(Landroid/view/View;Landroid/graphics/Canvas;IFF)V

    .line 65
    :cond_0
    return-void
.end method

.method public setStrength(F)V
    .locals 1

    .prologue
    .line 45
    iget v0, p0, Lcom/google/android/apps/chrome/widget/FadingShadowView;->mStrength:F

    cmpl-float v0, v0, p1

    if-eqz v0, :cond_0

    .line 46
    iput p1, p0, Lcom/google/android/apps/chrome/widget/FadingShadowView;->mStrength:F

    .line 47
    invoke-static {p0}, Lorg/chromium/base/ApiCompatibilityUtils;->postInvalidateOnAnimation(Landroid/view/View;)V

    .line 49
    :cond_0
    return-void
.end method
