.class public final enum Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment$DialogOption;
.super Ljava/lang/Enum;
.source "ClearBrowsingDataDialogFragment.java"


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment$DialogOption;

.field public static final enum CLEAR_BOOKMARKS_DATA:Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment$DialogOption;

.field public static final enum CLEAR_CACHE:Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment$DialogOption;

.field public static final enum CLEAR_COOKIES_AND_SITE_DATA:Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment$DialogOption;

.field public static final enum CLEAR_FORM_DATA:Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment$DialogOption;

.field public static final enum CLEAR_HISTORY:Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment$DialogOption;

.field public static final enum CLEAR_PASSWORDS:Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment$DialogOption;


# instance fields
.field private final mResourceId:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 44
    new-instance v0, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment$DialogOption;

    const-string/jumbo v1, "CLEAR_HISTORY"

    sget v2, Lcom/google/android/apps/chrome/R$string;->clear_history_title:I

    invoke-direct {v0, v1, v4, v2}, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment$DialogOption;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment$DialogOption;->CLEAR_HISTORY:Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment$DialogOption;

    .line 45
    new-instance v0, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment$DialogOption;

    const-string/jumbo v1, "CLEAR_CACHE"

    sget v2, Lcom/google/android/apps/chrome/R$string;->clear_cache_title:I

    invoke-direct {v0, v1, v5, v2}, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment$DialogOption;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment$DialogOption;->CLEAR_CACHE:Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment$DialogOption;

    .line 46
    new-instance v0, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment$DialogOption;

    const-string/jumbo v1, "CLEAR_COOKIES_AND_SITE_DATA"

    sget v2, Lcom/google/android/apps/chrome/R$string;->clear_cookies_and_site_data_title:I

    invoke-direct {v0, v1, v6, v2}, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment$DialogOption;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment$DialogOption;->CLEAR_COOKIES_AND_SITE_DATA:Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment$DialogOption;

    .line 47
    new-instance v0, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment$DialogOption;

    const-string/jumbo v1, "CLEAR_PASSWORDS"

    sget v2, Lcom/google/android/apps/chrome/R$string;->clear_passwords_title:I

    invoke-direct {v0, v1, v7, v2}, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment$DialogOption;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment$DialogOption;->CLEAR_PASSWORDS:Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment$DialogOption;

    .line 48
    new-instance v0, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment$DialogOption;

    const-string/jumbo v1, "CLEAR_FORM_DATA"

    sget v2, Lcom/google/android/apps/chrome/R$string;->clear_formdata_title:I

    invoke-direct {v0, v1, v8, v2}, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment$DialogOption;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment$DialogOption;->CLEAR_FORM_DATA:Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment$DialogOption;

    .line 50
    new-instance v0, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment$DialogOption;

    const-string/jumbo v1, "CLEAR_BOOKMARKS_DATA"

    const/4 v2, 0x5

    sget v3, Lcom/google/android/apps/chrome/R$string;->clear_bookmarks_title:I

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment$DialogOption;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment$DialogOption;->CLEAR_BOOKMARKS_DATA:Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment$DialogOption;

    .line 43
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment$DialogOption;

    sget-object v1, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment$DialogOption;->CLEAR_HISTORY:Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment$DialogOption;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment$DialogOption;->CLEAR_CACHE:Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment$DialogOption;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment$DialogOption;->CLEAR_COOKIES_AND_SITE_DATA:Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment$DialogOption;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment$DialogOption;->CLEAR_PASSWORDS:Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment$DialogOption;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment$DialogOption;->CLEAR_FORM_DATA:Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment$DialogOption;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment$DialogOption;->CLEAR_BOOKMARKS_DATA:Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment$DialogOption;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment$DialogOption;->$VALUES:[Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment$DialogOption;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 55
    iput p3, p0, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment$DialogOption;->mResourceId:I

    .line 56
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment$DialogOption;
    .locals 1

    .prologue
    .line 43
    const-class v0, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment$DialogOption;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment$DialogOption;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment$DialogOption;
    .locals 1

    .prologue
    .line 43
    sget-object v0, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment$DialogOption;->$VALUES:[Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment$DialogOption;

    invoke-virtual {v0}, [Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment$DialogOption;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment$DialogOption;

    return-object v0
.end method


# virtual methods
.method public final getResourceId()I
    .locals 1

    .prologue
    .line 62
    iget v0, p0, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment$DialogOption;->mResourceId:I

    return v0
.end method
