.class public abstract Lcom/google/android/apps/chrome/icing/ConnectedTask;
.super Ljava/lang/Object;
.source "ConnectedTask.java"

# interfaces
.implements Ljava/lang/Runnable;


# static fields
.field private static final CONNECTION_RETRY_TIME_MS:J

.field private static final CONNECTION_TIMEOUT_MS:J

.field static final RETRY_NUMBER_LIMIT:I = 0x5


# instance fields
.field private final mClient:Lcom/google/android/apps/chrome/icing/GoogleApiIcingClient;

.field private final mExecutor:Ljava/util/concurrent/ScheduledThreadPoolExecutor;

.field private mRetryNumber:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 28
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x5

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/apps/chrome/icing/ConnectedTask;->CONNECTION_TIMEOUT_MS:J

    .line 29
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0xa

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/apps/chrome/icing/ConnectedTask;->CONNECTION_RETRY_TIME_MS:J

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/chrome/icing/GoogleApiIcingClient;Ljava/util/concurrent/ScheduledThreadPoolExecutor;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/google/android/apps/chrome/icing/ConnectedTask;->mClient:Lcom/google/android/apps/chrome/icing/GoogleApiIcingClient;

    .line 39
    iput-object p2, p0, Lcom/google/android/apps/chrome/icing/ConnectedTask;->mExecutor:Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    .line 40
    return-void
.end method


# virtual methods
.method protected cleanUp()V
    .locals 0

    .prologue
    .line 60
    return-void
.end method

.method protected abstract doWhenConnected(Lcom/google/android/apps/chrome/icing/GoogleApiIcingClient;)V
.end method

.method protected abstract getName()Ljava/lang/String;
.end method

.method protected reschedule(J)V
    .locals 5

    .prologue
    .line 102
    const-string/jumbo v0, "ConnectedTask"

    const-string/jumbo v1, "%s rescheduled"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/icing/ConnectedTask;->getName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/icing/IcingDebugLogger;->debug(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 103
    iget-object v0, p0, Lcom/google/android/apps/chrome/icing/ConnectedTask;->mExecutor:Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, p0, p1, p2, v1}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    .line 104
    return-void
.end method

.method public final run()V
    .locals 6

    .prologue
    .line 64
    const-string/jumbo v0, "Icing:ConnectedTask:run"

    invoke-static {v0}, Lorg/chromium/base/TraceEvent;->begin(Ljava/lang/String;)V

    .line 66
    :try_start_0
    const-string/jumbo v0, "ConnectedTask"

    const-string/jumbo v1, "%s started"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/icing/ConnectedTask;->getName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/icing/IcingDebugLogger;->debug(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 67
    iget-object v0, p0, Lcom/google/android/apps/chrome/icing/ConnectedTask;->mClient:Lcom/google/android/apps/chrome/icing/GoogleApiIcingClient;

    sget-wide v2, Lcom/google/android/apps/chrome/icing/ConnectedTask;->CONNECTION_TIMEOUT_MS:J

    invoke-interface {v0, v2, v3}, Lcom/google/android/apps/chrome/icing/GoogleApiIcingClient;->connectWithTimeout(J)Z
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result v0

    if-eqz v0, :cond_0

    .line 69
    :try_start_1
    const-string/jumbo v0, "ConnectedTask"

    const-string/jumbo v1, "%s connected"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/icing/ConnectedTask;->getName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/icing/IcingDebugLogger;->debug(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 70
    iget-object v0, p0, Lcom/google/android/apps/chrome/icing/ConnectedTask;->mClient:Lcom/google/android/apps/chrome/icing/GoogleApiIcingClient;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/icing/ConnectedTask;->doWhenConnected(Lcom/google/android/apps/chrome/icing/GoogleApiIcingClient;)V

    .line 71
    const-string/jumbo v0, "ConnectedTask"

    const-string/jumbo v1, "%s finished"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/icing/ConnectedTask;->getName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/icing/IcingDebugLogger;->debug(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 73
    :try_start_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/icing/ConnectedTask;->mClient:Lcom/google/android/apps/chrome/icing/GoogleApiIcingClient;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/icing/GoogleApiIcingClient;->disconnect()V

    .line 74
    const-string/jumbo v0, "ConnectedTask"

    const-string/jumbo v1, "%s disconnected"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/icing/ConnectedTask;->getName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/icing/IcingDebugLogger;->debug(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 75
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/icing/ConnectedTask;->cleanUp()V

    .line 76
    const-string/jumbo v0, "ConnectedTask"

    const-string/jumbo v1, "%s cleaned up"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/icing/ConnectedTask;->getName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/icing/IcingDebugLogger;->debug(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 93
    :goto_0
    const-string/jumbo v0, "Icing:ConnectedTask:run"

    invoke-static {v0}, Lorg/chromium/base/TraceEvent;->end(Ljava/lang/String;)V

    .line 94
    return-void

    .line 73
    :catchall_0
    move-exception v0

    :try_start_3
    iget-object v1, p0, Lcom/google/android/apps/chrome/icing/ConnectedTask;->mClient:Lcom/google/android/apps/chrome/icing/GoogleApiIcingClient;

    invoke-interface {v1}, Lcom/google/android/apps/chrome/icing/GoogleApiIcingClient;->disconnect()V

    .line 74
    const-string/jumbo v1, "ConnectedTask"

    const-string/jumbo v2, "%s disconnected"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/icing/ConnectedTask;->getName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/chrome/icing/IcingDebugLogger;->debug(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 75
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/icing/ConnectedTask;->cleanUp()V

    .line 76
    const-string/jumbo v1, "ConnectedTask"

    const-string/jumbo v2, "%s cleaned up"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/icing/ConnectedTask;->getName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/chrome/icing/IcingDebugLogger;->debug(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    throw v0
    :try_end_3
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 88
    :catch_0
    move-exception v0

    .line 89
    :try_start_4
    const-string/jumbo v1, "ConnectedTask"

    const-string/jumbo v2, "%s runtime exception %s: %s"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/icing/ConnectedTask;->getName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    invoke-virtual {v0}, Ljava/lang/RuntimeException;->getMessage()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/chrome/icing/IcingDebugLogger;->error(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 91
    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 93
    :catchall_1
    move-exception v0

    const-string/jumbo v1, "Icing:ConnectedTask:run"

    invoke-static {v1}, Lorg/chromium/base/TraceEvent;->end(Ljava/lang/String;)V

    throw v0

    .line 79
    :cond_0
    :try_start_5
    iget v0, p0, Lcom/google/android/apps/chrome/icing/ConnectedTask;->mRetryNumber:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/chrome/icing/ConnectedTask;->mRetryNumber:I

    .line 80
    iget v0, p0, Lcom/google/android/apps/chrome/icing/ConnectedTask;->mRetryNumber:I

    const/4 v1, 0x5

    if-ge v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/icing/ConnectedTask;->mClient:Lcom/google/android/apps/chrome/icing/GoogleApiIcingClient;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/icing/GoogleApiIcingClient;->isGooglePlayServicesAvailable()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 81
    sget-wide v0, Lcom/google/android/apps/chrome/icing/ConnectedTask;->CONNECTION_RETRY_TIME_MS:J

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/chrome/icing/ConnectedTask;->reschedule(J)V

    goto :goto_0

    .line 83
    :cond_1
    const-string/jumbo v0, "ConnectedTask"

    const-string/jumbo v1, "%s number of retries exceeded"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/icing/ConnectedTask;->getName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/icing/IcingDebugLogger;->debug(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 84
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/icing/ConnectedTask;->cleanUp()V

    .line 85
    const-string/jumbo v0, "ConnectedTask"

    const-string/jumbo v1, "%s cleaned up"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/icing/ConnectedTask;->getName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/icing/IcingDebugLogger;->debug(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_5
    .catch Ljava/lang/RuntimeException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto/16 :goto_0
.end method
