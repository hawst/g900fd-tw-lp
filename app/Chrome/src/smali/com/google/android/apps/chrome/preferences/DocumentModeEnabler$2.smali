.class Lcom/google/android/apps/chrome/preferences/DocumentModeEnabler$2;
.super Ljava/lang/Object;
.source "DocumentModeEnabler.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/preferences/DocumentModeEnabler;

.field final synthetic val$optOut:Z


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/preferences/DocumentModeEnabler;Z)V
    .locals 0

    .prologue
    .line 57
    iput-object p1, p0, Lcom/google/android/apps/chrome/preferences/DocumentModeEnabler$2;->this$0:Lcom/google/android/apps/chrome/preferences/DocumentModeEnabler;

    iput-boolean p2, p0, Lcom/google/android/apps/chrome/preferences/DocumentModeEnabler$2;->val$optOut:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 60
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 61
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/preferences/DocumentModeEnabler$2;->val$optOut:Z

    invoke-static {v0}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->recordDocumentUserOptOut(Z)V

    .line 62
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/DocumentModeEnabler$2;->this$0:Lcom/google/android/apps/chrome/preferences/DocumentModeEnabler;

    # getter for: Lcom/google/android/apps/chrome/preferences/DocumentModeEnabler;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/google/android/apps/chrome/preferences/DocumentModeEnabler;->access$100(Lcom/google/android/apps/chrome/preferences/DocumentModeEnabler;)Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v2

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/preferences/DocumentModeEnabler$2;->val$optOut:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {v2, v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->setOptedOutState(I)V

    .line 65
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/DocumentModeEnabler$2;->this$0:Lcom/google/android/apps/chrome/preferences/DocumentModeEnabler;

    # getter for: Lcom/google/android/apps/chrome/preferences/DocumentModeEnabler;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/google/android/apps/chrome/preferences/DocumentModeEnabler;->access$100(Lcom/google/android/apps/chrome/preferences/DocumentModeEnabler;)Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->setOptOutCleanUpPending(Z)V

    .line 67
    invoke-static {}, Lcom/google/android/apps/chrome/utilities/DocumentApiDelegate;->create()Lcom/google/android/apps/chrome/utilities/DocumentApiDelegate;

    move-result-object v0

    .line 68
    if-eqz v0, :cond_0

    .line 69
    iget-boolean v1, p0, Lcom/google/android/apps/chrome/preferences/DocumentModeEnabler$2;->val$optOut:Z

    iget-object v2, p0, Lcom/google/android/apps/chrome/preferences/DocumentModeEnabler$2;->this$0:Lcom/google/android/apps/chrome/preferences/DocumentModeEnabler;

    # getter for: Lcom/google/android/apps/chrome/preferences/DocumentModeEnabler;->mActivity:Landroid/app/Activity;
    invoke-static {v2}, Lcom/google/android/apps/chrome/preferences/DocumentModeEnabler;->access$100(Lcom/google/android/apps/chrome/preferences/DocumentModeEnabler;)Landroid/app/Activity;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/chrome/preferences/DocumentModeEnabler$2;->this$0:Lcom/google/android/apps/chrome/preferences/DocumentModeEnabler;

    iget-boolean v4, p0, Lcom/google/android/apps/chrome/preferences/DocumentModeEnabler$2;->val$optOut:Z

    # invokes: Lcom/google/android/apps/chrome/preferences/DocumentModeEnabler;->isRestartNeeded(Z)Z
    invoke-static {v3, v4}, Lcom/google/android/apps/chrome/preferences/DocumentModeEnabler;->access$200(Lcom/google/android/apps/chrome/preferences/DocumentModeEnabler;Z)Z

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/chrome/utilities/DocumentApiDelegate;->migrateTabs(ZLandroid/app/Activity;Z)V

    .line 72
    :cond_0
    return-void

    .line 62
    :cond_1
    const/4 v0, 0x2

    goto :goto_0
.end method
