.class public Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;
.super Lcom/google/android/apps/chrome/tabs/layout/Layout;
.source "ToolbarSwipeLayout.java"

# interfaces
.implements Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final mCommitDistanceFromEdge:F

.field private final mEdgeInterpolator:Landroid/view/animation/Interpolator;

.field private mFromTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

.field private mLeftTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

.field private mMoveToolbar:Z

.field private mOffset:F

.field private mOffsetStart:F

.field private mOffsetTarget:F

.field private mRightTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

.field private final mSpaceBetweenTabs:F

.field private mToTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const-class v0, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;Lcom/google/android/apps/chrome/eventfilter/EventFilter;)V
    .locals 6

    .prologue
    .line 79
    new-instance v5, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StaticStripStacker;

    invoke-direct {v5}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StaticStripStacker;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/chrome/tabs/layout/Layout;-><init>(Landroid/content/Context;Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;Lcom/google/android/apps/chrome/eventfilter/EventFilter;Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripStacker;)V

    .line 69
    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->mEdgeInterpolator:Landroid/view/animation/Interpolator;

    .line 80
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 81
    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->density:F

    div-float/2addr v1, v2

    .line 82
    sget v2, Lcom/google/android/apps/chrome/R$dimen;->toolbar_swipe_commit_distance:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    mul-float/2addr v2, v1

    iput v2, p0, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->mCommitDistanceFromEdge:F

    .line 83
    sget v2, Lcom/google/android/apps/chrome/R$dimen;->toolbar_swipe_space_between_tabs:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->mSpaceBetweenTabs:F

    .line 84
    return-void
.end method

.method private init()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 316
    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    .line 317
    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->mFromTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    .line 318
    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->mLeftTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    .line 319
    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->mRightTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    .line 320
    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->mToTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    .line 321
    iput v1, p0, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->mOffsetStart:F

    .line 322
    iput v1, p0, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->mOffset:F

    .line 323
    iput v1, p0, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->mOffsetTarget:F

    .line 324
    return-void
.end method

.method private prepareLayoutTabForSwipe(Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;Z)V
    .locals 3

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    .line 178
    sget-boolean v0, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 179
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->shouldStall()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1, v1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setSaturation(F)V

    .line 180
    :cond_1
    invoke-virtual {p1, v2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setScale(F)V

    .line 181
    invoke-virtual {p1, v2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setBorderScale(F)V

    .line 182
    invoke-virtual {p1, v1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setDecorationAlpha(F)V

    .line 183
    invoke-virtual {p1, v1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setY(F)V

    .line 184
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->mMoveToolbar:Z

    invoke-virtual {p1, v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setShowToolbar(Z)V

    .line 185
    if-eqz p2, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setAnonymizeToolbar(Z)V

    .line 186
    return-void

    .line 185
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private smoothInput(FF)F
    .locals 2

    .prologue
    const/high16 v1, 0x41f00000    # 30.0f

    .line 311
    sub-float v0, p2, v1

    add-float/2addr v1, p2

    invoke-static {p1, v0, v1}, Lorg/chromium/chrome/browser/util/MathUtils;->clamp(FFF)F

    move-result v0

    .line 312
    const v1, 0x3f4ccccd    # 0.8f

    invoke-static {v0, p2, v1}, Lcom/google/android/apps/chrome/utilities/ChromeMathUtils;->interpolate(FFF)F

    move-result v0

    return v0
.end method


# virtual methods
.method public getSizingFlags()I
    .locals 1

    .prologue
    .line 96
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->mMoveToolbar:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/16 v0, 0x110

    goto :goto_0
.end method

.method public setMovesToolbar(Z)V
    .locals 0

    .prologue
    .line 91
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->mMoveToolbar:Z

    .line 92
    return-void
.end method

.method public setProperty(Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout$Property;F)V
    .locals 1

    .prologue
    .line 333
    sget-object v0, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout$Property;->OFFSET:Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout$Property;

    if-ne p1, v0, :cond_0

    .line 334
    iput p2, p0, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->mOffset:F

    .line 335
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->mOffset:F

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->mOffsetTarget:F

    .line 337
    :cond_0
    return-void
.end method

.method public bridge synthetic setProperty(Ljava/lang/Enum;F)V
    .locals 0

    .prologue
    .line 32
    check-cast p1, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout$Property;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->setProperty(Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout$Property;F)V

    return-void
.end method

.method public show(JZ)V
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 102
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->show(JZ)V

    .line 103
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->init()V

    .line 104
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    if-nez v0, :cond_1

    .line 111
    :cond_0
    :goto_0
    return-void

    .line 105
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->getCurrentModel()Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v0

    .line 106
    if-eqz v0, :cond_0

    .line 107
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    invoke-interface {v1}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->getCurrentTabId()I

    move-result v1

    .line 108
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 109
    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->isIncognito()Z

    move-result v0

    const/4 v2, 0x1

    invoke-virtual {p0, v1, v0, v3, v2}, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->createLayoutTab(IZZZ)Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->mFromTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    .line 110
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->mFromTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    invoke-direct {p0, v0, v3}, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->prepareLayoutTabForSwipe(Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;Z)V

    goto :goto_0
.end method

.method public swipeFinished(J)V
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const/4 v5, 0x0

    .line 207
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->mFromTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    if-nez v0, :cond_1

    .line 237
    :cond_0
    :goto_0
    return-void

    .line 210
    :cond_1
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->mCommitDistanceFromEdge:F

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->getWidth()F

    move-result v1

    const/high16 v2, 0x40400000    # 3.0f

    div-float/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 212
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->mFromTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    iput-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->mToTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    .line 213
    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->mOffsetTarget:F

    cmpl-float v1, v1, v0

    if-lez v1, :cond_5

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->mLeftTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    if-eqz v1, :cond_5

    .line 214
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->mLeftTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->mToTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    .line 215
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->getWidth()F

    move-result v0

    add-float/2addr v5, v0

    .line 221
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->mToTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->mFromTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    if-eq v0, v1, :cond_3

    .line 222
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->tabSideSwipeFinished()V

    .line 225
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->mToTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getId()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->startHiding(I)V

    .line 228
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->forceAnimationToFinish()V

    .line 229
    iget v4, p0, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->mOffsetTarget:F

    .line 231
    const/high16 v0, 0x43fa0000    # 500.0f

    sub-float v1, v4, v5

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    mul-float/2addr v0, v1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->getWidth()F

    move-result v1

    div-float/2addr v0, v1

    float-to-long v6, v0

    .line 232
    cmp-long v0, v6, v8

    if-lez v0, :cond_4

    .line 233
    sget-object v3, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout$Property;->OFFSET:Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout$Property;

    move-object v1, p0

    move-object v2, p0

    invoke-virtual/range {v1 .. v9}, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->addToAnimation(Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJ)V

    .line 236
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->requestRender()V

    goto :goto_0

    .line 216
    :cond_5
    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->mOffsetTarget:F

    neg-float v0, v0

    cmpg-float v0, v1, v0

    if-gez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->mRightTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    if-eqz v0, :cond_2

    .line 217
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->mRightTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->mToTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    .line 218
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->getWidth()F

    move-result v0

    sub-float/2addr v5, v0

    goto :goto_1
.end method

.method public swipeFlingOccurred(JFFFFFF)V
    .locals 11

    .prologue
    .line 198
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->getWidth()F

    move-result v0

    const/high16 v1, 0x3f000000    # 0.5f

    mul-float/2addr v0, v1

    .line 199
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->getHeight()F

    move-result v1

    const/high16 v2, 0x3f000000    # 0.5f

    mul-float/2addr v1, v2

    .line 200
    const v2, 0x3d088889

    mul-float v2, v2, p7

    neg-float v3, v0

    invoke-static {v2, v3, v0}, Lorg/chromium/chrome/browser/util/MathUtils;->clamp(FFF)F

    move-result v0

    .line 201
    const v2, 0x3d088889

    mul-float v2, v2, p8

    neg-float v3, v1

    invoke-static {v2, v3, v1}, Lorg/chromium/chrome/browser/util/MathUtils;->clamp(FFF)F

    move-result v1

    .line 202
    const/4 v6, 0x0

    const/4 v7, 0x0

    add-float v8, p5, v0

    add-float v9, p6, v1

    move-object v1, p0

    move-wide v2, p1

    move v4, p3

    move v5, p4

    invoke-virtual/range {v1 .. v9}, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->swipeUpdated(JFFFFFF)V

    .line 203
    return-void
.end method

.method public swipeStarted(JLcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;FF)V
    .locals 11

    .prologue
    .line 115
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->mToTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;->DOWN:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    if-ne p3, v0, :cond_1

    .line 175
    :cond_0
    :goto_0
    return-void

    .line 119
    :cond_1
    sget-object v0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;->RIGHT:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    if-ne p3, v0, :cond_4

    const/4 v0, 0x1

    .line 121
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->forceAnimationToFinish()V

    .line 124
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    invoke-interface {v1}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->getCurrentModel()Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v5

    .line 125
    if-eqz v5, :cond_0

    .line 126
    invoke-interface {v5}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->index()I

    move-result v3

    .line 127
    const/4 v1, -0x1

    if-eq v3, v1, :cond_0

    .line 130
    invoke-static {}, Lorg/chromium/ui/base/LocalizationUtils;->isLayoutRtl()Z

    move-result v1

    xor-int/2addr v1, v0

    if-eqz v1, :cond_5

    add-int/lit8 v2, v3, -0x1

    .line 132
    :goto_2
    if-eqz v0, :cond_6

    move v4, v2

    .line 133
    :goto_3
    if-nez v0, :cond_7

    move v1, v2

    .line 135
    :goto_4
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 136
    if-ltz v4, :cond_2

    invoke-interface {v5}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->getCount()I

    move-result v7

    if-ge v4, v7, :cond_2

    .line 137
    invoke-interface {v5, v4}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->getTabAt(I)Lorg/chromium/chrome/browser/Tab;

    move-result-object v7

    invoke-virtual {v7}, Lorg/chromium/chrome/browser/Tab;->getId()I

    move-result v7

    .line 138
    invoke-interface {v5}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->isIncognito()Z

    move-result v8

    const/4 v9, 0x0

    const/4 v10, 0x1

    invoke-virtual {p0, v7, v8, v9, v10}, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->createLayoutTab(IZZZ)Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v8

    iput-object v8, p0, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->mLeftTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    .line 139
    iget-object v8, p0, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->mLeftTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    if-eq v4, v3, :cond_8

    const/4 v4, 0x1

    :goto_5
    invoke-direct {p0, v8, v4}, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->prepareLayoutTabForSwipe(Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;Z)V

    .line 140
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v6, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 142
    :cond_2
    if-ltz v1, :cond_3

    invoke-interface {v5}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->getCount()I

    move-result v4

    if-ge v1, v4, :cond_3

    .line 143
    invoke-interface {v5, v1}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->getTabAt(I)Lorg/chromium/chrome/browser/Tab;

    move-result-object v4

    invoke-virtual {v4}, Lorg/chromium/chrome/browser/Tab;->getId()I

    move-result v4

    .line 144
    invoke-interface {v5}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->isIncognito()Z

    move-result v5

    const/4 v7, 0x0

    const/4 v8, 0x1

    invoke-virtual {p0, v4, v5, v7, v8}, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->createLayoutTab(IZZZ)Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->mRightTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    .line 146
    iget-object v5, p0, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->mRightTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    if-eq v1, v3, :cond_9

    const/4 v1, 0x1

    :goto_6
    invoke-direct {p0, v5, v1}, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->prepareLayoutTabForSwipe(Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;Z)V

    .line 147
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v6, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 150
    :cond_3
    invoke-virtual {p0, v6}, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->updateCacheVisibleIds(Ljava/util/List;)V

    .line 152
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->mToTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    .line 155
    if-eqz v0, :cond_a

    const/4 v0, 0x0

    :goto_7
    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->mOffsetStart:F

    .line 156
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->mOffset:F

    .line 157
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->mOffsetTarget:F

    .line 159
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->mLeftTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->mRightTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    if-eqz v0, :cond_b

    .line 160
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    const/4 v1, 0x0

    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->mLeftTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    aput-object v3, v0, v1

    const/4 v1, 0x1

    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->mRightTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    aput-object v3, v0, v1

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    .line 169
    :goto_8
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 170
    const-string/jumbo v1, "toIndex"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 171
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    const/16 v2, 0x47

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastNotification(Landroid/content/Context;ILandroid/os/Bundle;)V

    .line 174
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->requestUpdate()V

    goto/16 :goto_0

    .line 119
    :cond_4
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 130
    :cond_5
    add-int/lit8 v2, v3, 0x1

    goto/16 :goto_2

    :cond_6
    move v4, v3

    .line 132
    goto/16 :goto_3

    :cond_7
    move v1, v3

    .line 133
    goto/16 :goto_4

    .line 139
    :cond_8
    const/4 v4, 0x0

    goto/16 :goto_5

    .line 146
    :cond_9
    const/4 v1, 0x0

    goto :goto_6

    .line 155
    :cond_a
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->getWidth()F

    move-result v0

    goto :goto_7

    .line 161
    :cond_b
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->mLeftTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    if-eqz v0, :cond_c

    .line 162
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    const/4 v1, 0x0

    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->mLeftTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    aput-object v3, v0, v1

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    goto :goto_8

    .line 163
    :cond_c
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->mRightTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    if-eqz v0, :cond_d

    .line 164
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    const/4 v1, 0x0

    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->mRightTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    aput-object v3, v0, v1

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    goto :goto_8

    .line 166
    :cond_d
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    goto :goto_8
.end method

.method public swipeUpdated(JFFFFFF)V
    .locals 3

    .prologue
    .line 190
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->mOffsetStart:F

    add-float/2addr v0, p7

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->getWidth()F

    move-result v2

    invoke-static {v0, v1, v2}, Lorg/chromium/chrome/browser/util/MathUtils;->clamp(FFF)F

    move-result v0

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->mOffsetStart:F

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->mOffsetTarget:F

    .line 191
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->requestUpdate()V

    .line 192
    return-void
.end method

.method protected updateLayout(JJ)V
    .locals 9

    .prologue
    const/high16 v5, 0x3f800000    # 1.0f

    const/high16 v7, 0x40000000    # 2.0f

    const/4 v4, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 246
    invoke-super {p0, p1, p2, p3, p4}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->updateLayout(JJ)V

    .line 248
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->mFromTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    if-nez v0, :cond_1

    .line 296
    :cond_0
    :goto_0
    return-void

    .line 250
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->mLeftTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->mRightTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->mFromTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->mRightTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    .line 252
    :cond_2
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->mOffset:F

    iget v3, p0, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->mOffsetTarget:F

    invoke-direct {p0, v0, v3}, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->smoothInput(FF)F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->mOffset:F

    .line 253
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->mOffset:F

    iget v3, p0, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->mOffsetTarget:F

    sub-float/2addr v0, v3

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const v3, 0x3dcccccd    # 0.1f

    cmpl-float v0, v0, v3

    if-ltz v0, :cond_7

    move v0, v1

    .line 255
    :goto_1
    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->mLeftTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    if-eqz v3, :cond_8

    move v3, v1

    :goto_2
    iget-object v6, p0, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->mRightTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    if-eqz v6, :cond_9

    move v6, v1

    :goto_3
    xor-int/2addr v3, v6

    .line 260
    if-eqz v3, :cond_a

    .line 261
    iget v3, p0, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->mOffset:F

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->getWidth()F

    move-result v4

    div-float/2addr v3, v4

    .line 262
    invoke-static {v3}, Ljava/lang/Math;->signum(F)F

    move-result v4

    .line 263
    iget-object v5, p0, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->mEdgeInterpolator:Landroid/view/animation/Interpolator;

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    invoke-interface {v5, v3}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v3

    .line 265
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->getWidth()F

    move-result v5

    const/high16 v6, 0x40a00000    # 5.0f

    div-float/2addr v5, v6

    .line 266
    mul-float/2addr v3, v4

    mul-float/2addr v3, v5

    move v4, v3

    .line 286
    :goto_4
    iget-object v5, p0, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->mLeftTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    if-eqz v5, :cond_4

    .line 287
    iget-object v5, p0, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->mLeftTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    invoke-virtual {v5, v3}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setX(F)V

    .line 288
    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->mLeftTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    invoke-virtual {v3, p3, p4}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->updateSnap(J)Z

    move-result v3

    if-nez v3, :cond_3

    if-eqz v0, :cond_e

    :cond_3
    move v0, v1

    .line 291
    :cond_4
    :goto_5
    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->mRightTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    if-eqz v3, :cond_f

    .line 292
    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->mRightTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    invoke-virtual {v3, v4}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setX(F)V

    .line 293
    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->mRightTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    invoke-virtual {v3, p3, p4}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->updateSnap(J)Z

    move-result v3

    if-nez v3, :cond_5

    if-eqz v0, :cond_6

    :cond_5
    move v2, v1

    .line 295
    :cond_6
    :goto_6
    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->requestUpdate()V

    goto/16 :goto_0

    :cond_7
    move v0, v2

    .line 253
    goto :goto_1

    :cond_8
    move v3, v2

    .line 255
    goto :goto_2

    :cond_9
    move v6, v2

    goto :goto_3

    .line 269
    :cond_a
    iget v3, p0, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->mOffset:F

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->getWidth()F

    move-result v6

    div-float v6, v3, v6

    .line 270
    iget v3, p0, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->mOffsetStart:F

    cmpl-float v3, v3, v4

    if-nez v3, :cond_b

    move v3, v4

    :goto_7
    add-float/2addr v3, v6

    .line 271
    invoke-static {v3, v4, v5}, Lorg/chromium/chrome/browser/util/MathUtils;->clamp(FFF)F

    move-result v3

    .line 273
    sget-boolean v5, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->$assertionsDisabled:Z

    if-nez v5, :cond_c

    iget-object v5, p0, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->mLeftTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    if-nez v5, :cond_c

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_b
    move v3, v5

    .line 270
    goto :goto_7

    .line 274
    :cond_c
    sget-boolean v5, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->$assertionsDisabled:Z

    if-nez v5, :cond_d

    iget-object v5, p0, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->mRightTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    if-nez v5, :cond_d

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 275
    :cond_d
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->getWidth()F

    move-result v5

    iget v6, p0, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->mSpaceBetweenTabs:F

    add-float/2addr v5, v6

    invoke-static {v4, v5, v3}, Lcom/google/android/apps/chrome/utilities/ChromeMathUtils;->interpolate(FFF)F

    move-result v3

    .line 277
    iget v4, p0, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->mSpaceBetweenTabs:F

    sub-float v4, v3, v4

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->getWidth()F

    move-result v5

    iget-object v6, p0, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->mLeftTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    invoke-virtual {v6}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getOriginalContentWidth()F

    move-result v6

    invoke-static {v5, v6}, Ljava/lang/Math;->min(FF)F

    move-result v5

    sub-float v5, v4, v5

    .line 281
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->getWidth()F

    move-result v4

    div-float v6, v4, v7

    .line 282
    iget-object v4, p0, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->mRightTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getFinalContentWidth()F

    move-result v4

    div-float/2addr v4, v7

    sub-float v4, v6, v4

    invoke-static {v4, v3}, Ljava/lang/Math;->max(FF)F

    move-result v4

    .line 283
    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->mLeftTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getFinalContentWidth()F

    move-result v3

    div-float/2addr v3, v7

    sub-float v3, v6, v3

    invoke-static {v3, v5}, Ljava/lang/Math;->min(FF)F

    move-result v3

    goto/16 :goto_4

    :cond_e
    move v0, v2

    .line 288
    goto/16 :goto_5

    :cond_f
    move v2, v0

    goto/16 :goto_6
.end method
