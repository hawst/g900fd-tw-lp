.class public final Lcom/google/android/apps/chrome/icing/SsbContextHelper;
.super Ljava/lang/Object;
.source "SsbContextHelper.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    return-void
.end method

.method public static getContextStubForSsb(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/c/f;
    .locals 4

    .prologue
    .line 29
    new-instance v0, Lcom/google/android/c/a;

    invoke-direct {v0}, Lcom/google/android/c/a;-><init>()V

    .line 30
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/c/a;->a:Ljava/lang/String;

    .line 31
    if-eqz p2, :cond_1

    :goto_0
    iput-object p2, v0, Lcom/google/android/c/a;->b:Ljava/lang/String;

    .line 33
    new-instance v1, Lcom/google/android/c/c;

    invoke-direct {v1}, Lcom/google/android/c/c;-><init>()V

    .line 34
    if-eqz p1, :cond_2

    :goto_1
    iput-object p1, v1, Lcom/google/android/c/c;->b:Ljava/lang/String;

    .line 35
    iput-object v0, v1, Lcom/google/android/c/c;->a:Lcom/google/android/c/a;

    .line 37
    new-instance v0, Lcom/google/android/c/f;

    invoke-direct {v0}, Lcom/google/android/c/f;-><init>()V

    .line 38
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, v0, Lcom/google/android/c/f;->a:Ljava/lang/Long;

    .line 39
    iput-object v1, v0, Lcom/google/android/c/f;->b:Lcom/google/android/c/c;

    .line 41
    invoke-static {p0}, Lorg/chromium/sync/signin/ChromeSigninController;->get(Landroid/content/Context;)Lorg/chromium/sync/signin/ChromeSigninController;

    move-result-object v1

    invoke-virtual {v1}, Lorg/chromium/sync/signin/ChromeSigninController;->getSignedInUser()Landroid/accounts/Account;

    move-result-object v1

    .line 42
    if-eqz v1, :cond_0

    iget-object v2, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 43
    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/c/f;->d:Ljava/lang/String;

    .line 45
    :cond_0
    return-object v0

    .line 31
    :cond_1
    const-string/jumbo p2, ""

    goto :goto_0

    .line 34
    :cond_2
    const-string/jumbo p1, ""

    goto :goto_1
.end method
