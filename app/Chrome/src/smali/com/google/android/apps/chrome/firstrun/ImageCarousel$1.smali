.class final Lcom/google/android/apps/chrome/firstrun/ImageCarousel$1;
.super Landroid/util/Property;
.source "ImageCarousel.java"


# direct methods
.method constructor <init>(Ljava/lang/Class;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 100
    invoke-direct {p0, p1, p2}, Landroid/util/Property;-><init>(Ljava/lang/Class;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final get(Lcom/google/android/apps/chrome/firstrun/ImageCarousel;)Ljava/lang/Float;
    .locals 1

    .prologue
    .line 103
    # getter for: Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->mPosition:F
    invoke-static {p1}, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->access$000(Lcom/google/android/apps/chrome/firstrun/ImageCarousel;)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 100
    check-cast p1, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/firstrun/ImageCarousel$1;->get(Lcom/google/android/apps/chrome/firstrun/ImageCarousel;)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public final set(Lcom/google/android/apps/chrome/firstrun/ImageCarousel;Ljava/lang/Float;)V
    .locals 1

    .prologue
    .line 108
    invoke-virtual {p2}, Ljava/lang/Float;->floatValue()F

    move-result v0

    # invokes: Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->setPosition(F)V
    invoke-static {p1, v0}, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->access$100(Lcom/google/android/apps/chrome/firstrun/ImageCarousel;F)V

    .line 109
    return-void
.end method

.method public final bridge synthetic set(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 100
    check-cast p1, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;

    check-cast p2, Ljava/lang/Float;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/chrome/firstrun/ImageCarousel$1;->set(Lcom/google/android/apps/chrome/firstrun/ImageCarousel;Ljava/lang/Float;)V

    return-void
.end method
