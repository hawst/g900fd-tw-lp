.class Lcom/google/android/apps/chrome/services/SyncSignInAccountListener$2;
.super Ljava/lang/Object;
.source "SyncSignInAccountListener.java"

# interfaces
.implements Ljava/lang/Runnable;


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/services/SyncSignInAccountListener;

.field final synthetic val$preferencesActivity:Lcom/google/android/apps/chrome/preferences/Preferences;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 81
    const-class v0, Lcom/google/android/apps/chrome/services/SyncSignInAccountListener;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/services/SyncSignInAccountListener$2;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lcom/google/android/apps/chrome/services/SyncSignInAccountListener;Lcom/google/android/apps/chrome/preferences/Preferences;)V
    .locals 0

    .prologue
    .line 81
    iput-object p1, p0, Lcom/google/android/apps/chrome/services/SyncSignInAccountListener$2;->this$0:Lcom/google/android/apps/chrome/services/SyncSignInAccountListener;

    iput-object p2, p0, Lcom/google/android/apps/chrome/services/SyncSignInAccountListener$2;->val$preferencesActivity:Lcom/google/android/apps/chrome/preferences/Preferences;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 85
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/SyncSignInAccountListener$2;->val$preferencesActivity:Lcom/google/android/apps/chrome/preferences/Preferences;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/Preferences;->updatePreferencesHeadersAndMenu()V

    .line 86
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/SyncSignInAccountListener$2;->val$preferencesActivity:Lcom/google/android/apps/chrome/preferences/Preferences;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/Preferences;->getCurrentFragment()Landroid/app/Fragment;

    move-result-object v0

    .line 87
    if-eqz v0, :cond_1

    .line 88
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-class v2, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 89
    sget-boolean v1, Lcom/google/android/apps/chrome/services/SyncSignInAccountListener$2;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    invoke-static {}, Lorg/chromium/chrome/browser/signin/SigninManager;->isNewProfileManagementEnabled()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 90
    :cond_0
    check-cast v0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->updateAccountsList()V

    .line 96
    :cond_1
    :goto_0
    return-void

    .line 91
    :cond_2
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-class v2, Lcom/google/android/apps/chrome/preferences/BasicsPreferences;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 92
    sget-boolean v1, Lcom/google/android/apps/chrome/services/SyncSignInAccountListener$2;->$assertionsDisabled:Z

    if-nez v1, :cond_3

    invoke-static {}, Lorg/chromium/chrome/browser/signin/SigninManager;->isNewProfileManagementEnabled()Z

    move-result v1

    if-eqz v1, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 93
    :cond_3
    check-cast v0, Lcom/google/android/apps/chrome/preferences/BasicsPreferences;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/BasicsPreferences;->updateAccountHeader()V

    goto :goto_0
.end method
