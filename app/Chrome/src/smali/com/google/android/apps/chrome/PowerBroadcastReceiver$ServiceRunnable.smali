.class public Lcom/google/android/apps/chrome/PowerBroadcastReceiver$ServiceRunnable;
.super Ljava/lang/Object;
.source "PowerBroadcastReceiver.java"

# interfaces
.implements Ljava/lang/Runnable;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public delayToRun()J
    .locals 2

    .prologue
    .line 65
    const-wide/16 v0, 0x1388

    return-wide v0
.end method

.method public run()V
    .locals 2

    .prologue
    .line 73
    invoke-static {}, Lorg/chromium/base/ApplicationStatus;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 76
    invoke-static {}, Lorg/chromium/chrome/browser/BrowserVersion;->isOfficialBuild()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 77
    invoke-static {v0}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->createInitializeIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    .line 78
    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 81
    :cond_0
    invoke-static {}, Lorg/chromium/chrome/browser/sync/DelayedSyncController;->getInstance()Lorg/chromium/chrome/browser/sync/DelayedSyncController;

    move-result-object v1

    invoke-virtual {v1, v0}, Lorg/chromium/chrome/browser/sync/DelayedSyncController;->resumeDelayedSyncs(Landroid/content/Context;)Z

    .line 82
    return-void
.end method
