.class Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog$3;
.super Ljava/lang/Object;
.source "EnhancedBookmarkDetailDialog.java"

# interfaces
.implements Lcom/google/android/apps/chrome/enhancedbookmark/ChromeEnhancedBookmarkBridge$SalientImageCallback;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;)V
    .locals 0

    .prologue
    .line 192
    iput-object p1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog$3;->this$0:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSalientImageReady(Landroid/graphics/Bitmap;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 195
    if-nez p1, :cond_0

    .line 197
    :goto_0
    return-void

    .line 196
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog$3;->this$0:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;

    # getter for: Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mImageView:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->access$500(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method
