.class public Lcom/google/android/apps/chrome/preferences/AboutChromePreferences;
.super Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;
.source "AboutChromePreferences.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 20
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    .line 21
    sget v0, Lcom/google/android/apps/chrome/R$xml;->about_chrome_preferences:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/AboutChromePreferences;->addPreferencesFromResource(I)V

    .line 22
    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v0

    .line 23
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getAboutVersionStrings()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences$AboutVersionStrings;

    move-result-object v1

    .line 25
    const-string/jumbo v2, "application_version"

    invoke-virtual {p0, v2}, Lcom/google/android/apps/chrome/preferences/AboutChromePreferences;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    .line 26
    invoke-virtual {v1}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences$AboutVersionStrings;->getApplicationVersion()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 27
    const-string/jumbo v2, "webkit_version"

    invoke-virtual {p0, v2}, Lcom/google/android/apps/chrome/preferences/AboutChromePreferences;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    .line 28
    invoke-virtual {v1}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences$AboutVersionStrings;->getWebkitVersion()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 29
    const-string/jumbo v2, "javascript_version"

    invoke-virtual {p0, v2}, Lcom/google/android/apps/chrome/preferences/AboutChromePreferences;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    .line 30
    invoke-virtual {v1}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences$AboutVersionStrings;->getJavascriptVersion()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 31
    const-string/jumbo v2, "os_version"

    invoke-virtual {p0, v2}, Lcom/google/android/apps/chrome/preferences/AboutChromePreferences;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    .line 32
    invoke-virtual {v1}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences$AboutVersionStrings;->getOSVersion()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 33
    const-string/jumbo v1, "executable_path"

    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/preferences/AboutChromePreferences;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    .line 34
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/AboutChromePreferences;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getPackageCodePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 35
    const-string/jumbo v1, "profile_path"

    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/preferences/AboutChromePreferences;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    .line 36
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getProfilePathValue()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 37
    const-string/jumbo v0, "legal_information"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/AboutChromePreferences;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 38
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/util/Calendar;->get(I)I

    move-result v1

    .line 39
    sget v2, Lcom/google/android/apps/chrome/R$string;->category_legal_information_summary:I

    new-array v3, v4, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v3, v4

    invoke-virtual {p0, v2, v3}, Lcom/google/android/apps/chrome/preferences/AboutChromePreferences;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 40
    return-void
.end method
