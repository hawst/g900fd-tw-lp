.class Lcom/google/android/apps/chrome/tab/TransitionPageHelper$5;
.super Lorg/chromium/content/browser/ContentViewClient;
.source "TransitionPageHelper.java"


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/tab/TransitionPageHelper;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/tab/TransitionPageHelper;)V
    .locals 0

    .prologue
    .line 465
    iput-object p1, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper$5;->this$0:Lcom/google/android/apps/chrome/tab/TransitionPageHelper;

    invoke-direct {p0}, Lorg/chromium/content/browser/ContentViewClient;-><init>()V

    return-void
.end method


# virtual methods
.method public onOffsetsForFullscreenChanged(FFF)V
    .locals 2

    .prologue
    .line 471
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper$5;->this$0:Lcom/google/android/apps/chrome/tab/TransitionPageHelper;

    # getter for: Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mState:I
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->access$800(Lcom/google/android/apps/chrome/tab/TransitionPageHelper;)I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    .line 479
    :cond_0
    :goto_0
    return-void

    .line 474
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper$5;->this$0:Lcom/google/android/apps/chrome/tab/TransitionPageHelper;

    # getter for: Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mTransitionContentViewCore:Lorg/chromium/content/browser/ContentViewCore;
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->access$000(Lcom/google/android/apps/chrome/tab/TransitionPageHelper;)Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->getContentWidthCss()F

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper$5;->this$0:Lcom/google/android/apps/chrome/tab/TransitionPageHelper;

    # getter for: Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mTab:Lorg/chromium/chrome/browser/Tab;
    invoke-static {v1}, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->access$900(Lcom/google/android/apps/chrome/tab/TransitionPageHelper;)Lorg/chromium/chrome/browser/Tab;

    move-result-object v1

    invoke-virtual {v1}, Lorg/chromium/chrome/browser/Tab;->getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v1

    invoke-virtual {v1}, Lorg/chromium/content/browser/ContentViewCore;->getContentWidthCss()F

    move-result v1

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    .line 476
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper$5;->this$0:Lcom/google/android/apps/chrome/tab/TransitionPageHelper;

    # operator++ for: Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mTransitionFramesPainted:I
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->access$308(Lcom/google/android/apps/chrome/tab/TransitionPageHelper;)I

    .line 477
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper$5;->this$0:Lcom/google/android/apps/chrome/tab/TransitionPageHelper;

    # invokes: Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->updateState()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->access$200(Lcom/google/android/apps/chrome/tab/TransitionPageHelper;)V

    goto :goto_0
.end method
