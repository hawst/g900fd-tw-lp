.class public Lcom/google/android/apps/chrome/preferences/ProtectedContentResetCredentialConfirmDialogFragment;
.super Lcom/google/android/apps/chrome/preferences/ChromeBaseDialogFragment;
.source "ProtectedContentResetCredentialConfirmDialogFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private mListener:Lcom/google/android/apps/chrome/preferences/ProtectedContentResetCredentialConfirmDialogFragment$Listener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const-class v0, Lcom/google/android/apps/chrome/preferences/ProtectedContentResetCredentialConfirmDialogFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/preferences/ProtectedContentResetCredentialConfirmDialogFragment;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBaseDialogFragment;-><init>()V

    .line 35
    sget-boolean v0, Lcom/google/android/apps/chrome/preferences/ProtectedContentResetCredentialConfirmDialogFragment;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 36
    :cond_0
    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/chrome/preferences/ProtectedContentResetCredentialConfirmDialogFragment$Listener;)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBaseDialogFragment;-><init>()V

    .line 42
    iput-object p1, p0, Lcom/google/android/apps/chrome/preferences/ProtectedContentResetCredentialConfirmDialogFragment;->mListener:Lcom/google/android/apps/chrome/preferences/ProtectedContentResetCredentialConfirmDialogFragment$Listener;

    .line 43
    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 1

    .prologue
    .line 64
    packed-switch p2, :pswitch_data_0

    .line 71
    sget-boolean v0, Lcom/google/android/apps/chrome/preferences/ProtectedContentResetCredentialConfirmDialogFragment;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 66
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ProtectedContentResetCredentialConfirmDialogFragment;->mListener:Lcom/google/android/apps/chrome/preferences/ProtectedContentResetCredentialConfirmDialogFragment$Listener;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/preferences/ProtectedContentResetCredentialConfirmDialogFragment$Listener;->resetDeviceCredential()V

    .line 74
    :cond_0
    :pswitch_1
    return-void

    .line 64
    nop

    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 2

    .prologue
    .line 50
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/preferences/ChromeBaseDialogFragment;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;

    .line 51
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/ProtectedContentResetCredentialConfirmDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v1, Lcom/google/android/apps/chrome/R$string;->protected_content_prefs_reset_title:I

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$string;->protected_content_prefs_reset_message:I

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$string;->cancel:I

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$string;->delete:I

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method
