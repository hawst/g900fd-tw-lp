.class public Lcom/google/android/apps/chrome/icing/GoogleApiIcingClientImpl;
.super Ljava/lang/Object;
.source "GoogleApiIcingClientImpl.java"

# interfaces
.implements Lcom/google/android/apps/chrome/icing/GoogleApiIcingClient;


# static fields
.field private static final REPORT_USAGE_TIMEOUT_MS:J

.field private static final SSB_CONTEXT_SECTION_INFO:Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;


# instance fields
.field private final mApplicationContext:Landroid/content/Context;

.field private final mClient:Lcom/google/android/gms/common/api/i;

.field private final mCorpusName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 49
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/apps/chrome/icing/GoogleApiIcingClientImpl;->REPORT_USAGE_TIMEOUT_MS:J

    .line 53
    new-instance v0, Lcom/google/android/gms/appdatasearch/d;

    const-string/jumbo v1, "SsbContext"

    invoke-direct {v0, v1}, Lcom/google/android/gms/appdatasearch/d;-><init>(Ljava/lang/String;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/appdatasearch/d;->a(Z)Lcom/google/android/gms/appdatasearch/d;

    move-result-object v0

    const-string/jumbo v1, "blob"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/appdatasearch/d;->a(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/appdatasearch/d;->a()Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/chrome/icing/GoogleApiIcingClientImpl;->SSB_CONTEXT_SECTION_INFO:Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/icing/GoogleApiIcingClientImpl;->mApplicationContext:Landroid/content/Context;

    .line 66
    new-instance v0, Lcom/google/android/gms/common/api/j;

    iget-object v1, p0, Lcom/google/android/apps/chrome/icing/GoogleApiIcingClientImpl;->mApplicationContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/api/j;-><init>(Landroid/content/Context;)V

    sget-object v1, Lcom/google/android/gms/appdatasearch/a;->b:Lcom/google/android/gms/common/api/a;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/j;->a(Lcom/google/android/gms/common/api/a;)Lcom/google/android/gms/common/api/j;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/search/a;->b:Lcom/google/android/gms/common/api/a;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/j;->a(Lcom/google/android/gms/common/api/a;)Lcom/google/android/gms/common/api/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/j;->a()Lcom/google/android/gms/common/api/i;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/icing/GoogleApiIcingClientImpl;->mClient:Lcom/google/android/gms/common/api/i;

    .line 68
    iget-object v0, p0, Lcom/google/android/apps/chrome/icing/GoogleApiIcingClientImpl;->mApplicationContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$string;->search_corpus_name:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/icing/GoogleApiIcingClientImpl;->mCorpusName:Ljava/lang/String;

    .line 69
    return-void
.end method

.method private hasCorpus(Lcom/google/android/gms/search/corpora/GetCorpusStatusCall$Response;)Z
    .locals 1

    .prologue
    .line 185
    iget-object v0, p1, Lcom/google/android/gms/search/corpora/GetCorpusStatusCall$Response;->b:Lcom/google/android/gms/appdatasearch/CorpusStatus;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public clearData()Z
    .locals 4

    .prologue
    .line 171
    sget-object v0, Lcom/google/android/gms/search/a;->c:Lcom/google/android/gms/search/corpora/d;

    iget-object v1, p0, Lcom/google/android/apps/chrome/icing/GoogleApiIcingClientImpl;->mClient:Lcom/google/android/gms/common/api/i;

    iget-object v2, p0, Lcom/google/android/apps/chrome/icing/GoogleApiIcingClientImpl;->mApplicationContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/chrome/icing/GoogleApiIcingClientImpl;->mCorpusName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/search/corpora/d;->a(Lcom/google/android/gms/common/api/i;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    .line 174
    invoke-interface {v0}, Lcom/google/android/gms/common/api/l;->a()Lcom/google/android/gms/common/api/n;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/search/corpora/ClearCorpusCall$Response;

    .line 175
    iget-object v1, v0, Lcom/google/android/gms/search/corpora/ClearCorpusCall$Response;->a:Lcom/google/android/gms/common/api/Status;

    invoke-virtual {v1}, Lcom/google/android/gms/common/api/Status;->e()Z

    move-result v1

    if-nez v1, :cond_0

    .line 176
    const-string/jumbo v1, "GoogleApiIcingClientImpl"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "clearData unsuccessful. Status Code: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, v0, Lcom/google/android/gms/search/corpora/ClearCorpusCall$Response;->a:Lcom/google/android/gms/common/api/Status;

    invoke-virtual {v3}, Lcom/google/android/gms/common/api/Status;->f()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " Message: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v0, Lcom/google/android/gms/search/corpora/ClearCorpusCall$Response;->a:Lcom/google/android/gms/common/api/Status;

    invoke-virtual {v3}, Lcom/google/android/gms/common/api/Status;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 181
    :goto_0
    iget-object v0, v0, Lcom/google/android/gms/search/corpora/ClearCorpusCall$Response;->a:Lcom/google/android/gms/common/api/Status;

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->e()Z

    move-result v0

    return v0

    .line 179
    :cond_0
    const-string/jumbo v1, "GoogleApiIcingClientImpl"

    const-string/jumbo v2, "clearData  successful."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/chrome/icing/IcingDebugLogger;->debug(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public clearLegacyCorpusData()Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 191
    sget-object v0, Lcom/google/android/gms/search/a;->c:Lcom/google/android/gms/search/corpora/d;

    iget-object v2, p0, Lcom/google/android/apps/chrome/icing/GoogleApiIcingClientImpl;->mClient:Lcom/google/android/gms/common/api/i;

    iget-object v3, p0, Lcom/google/android/apps/chrome/icing/GoogleApiIcingClientImpl;->mApplicationContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "history"

    invoke-virtual {v0, v2, v3, v4}, Lcom/google/android/gms/search/corpora/d;->b(Lcom/google/android/gms/common/api/i;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    .line 194
    invoke-interface {v0}, Lcom/google/android/gms/common/api/l;->a()Lcom/google/android/gms/common/api/n;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/search/corpora/GetCorpusStatusCall$Response;

    .line 195
    iget-object v2, v0, Lcom/google/android/gms/search/corpora/GetCorpusStatusCall$Response;->a:Lcom/google/android/gms/common/api/Status;

    invoke-virtual {v2}, Lcom/google/android/gms/common/api/Status;->e()Z

    move-result v2

    if-nez v2, :cond_0

    .line 196
    const-string/jumbo v0, "GoogleApiIcingClientImpl"

    const-string/jumbo v2, "Failed to clear legacy corpus; can\'t obtain status."

    new-array v3, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lcom/google/android/apps/chrome/icing/IcingDebugLogger;->debug(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 216
    :goto_0
    return v0

    .line 199
    :cond_0
    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/icing/GoogleApiIcingClientImpl;->hasCorpus(Lcom/google/android/gms/search/corpora/GetCorpusStatusCall$Response;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 200
    const-string/jumbo v0, "GoogleApiIcingClientImpl"

    const-string/jumbo v2, "Clearing legacy corpus successful.; corpus does not exist."

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v1}, Lcom/google/android/apps/chrome/icing/IcingDebugLogger;->debug(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 202
    const/4 v0, 0x1

    goto :goto_0

    .line 205
    :cond_1
    sget-object v0, Lcom/google/android/gms/search/a;->c:Lcom/google/android/gms/search/corpora/d;

    iget-object v2, p0, Lcom/google/android/apps/chrome/icing/GoogleApiIcingClientImpl;->mClient:Lcom/google/android/gms/common/api/i;

    iget-object v3, p0, Lcom/google/android/apps/chrome/icing/GoogleApiIcingClientImpl;->mApplicationContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "history"

    invoke-virtual {v0, v2, v3, v4}, Lcom/google/android/gms/search/corpora/d;->a(Lcom/google/android/gms/common/api/i;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    .line 208
    invoke-interface {v0}, Lcom/google/android/gms/common/api/l;->a()Lcom/google/android/gms/common/api/n;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/search/corpora/ClearCorpusCall$Response;

    .line 209
    iget-object v2, v0, Lcom/google/android/gms/search/corpora/ClearCorpusCall$Response;->a:Lcom/google/android/gms/common/api/Status;

    invoke-virtual {v2}, Lcom/google/android/gms/common/api/Status;->e()Z

    move-result v2

    if-nez v2, :cond_2

    .line 210
    const-string/jumbo v1, "GoogleApiIcingClientImpl"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Failed to clear legacy corpus. Status Code: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, v0, Lcom/google/android/gms/search/corpora/ClearCorpusCall$Response;->a:Lcom/google/android/gms/common/api/Status;

    invoke-virtual {v3}, Lcom/google/android/gms/common/api/Status;->f()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " Message: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v0, Lcom/google/android/gms/search/corpora/ClearCorpusCall$Response;->a:Lcom/google/android/gms/common/api/Status;

    invoke-virtual {v3}, Lcom/google/android/gms/common/api/Status;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 216
    :goto_1
    iget-object v0, v0, Lcom/google/android/gms/search/corpora/ClearCorpusCall$Response;->a:Lcom/google/android/gms/common/api/Status;

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->e()Z

    move-result v0

    goto :goto_0

    .line 214
    :cond_2
    const-string/jumbo v2, "GoogleApiIcingClientImpl"

    const-string/jumbo v3, "Clearing legacy corpus successful."

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v2, v3, v1}, Lcom/google/android/apps/chrome/icing/IcingDebugLogger;->debug(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public connectWithTimeout(J)Z
    .locals 5

    .prologue
    .line 74
    :try_start_0
    const-string/jumbo v0, "GoogleApiIcingClientImpl:connectWithTimeout"

    invoke-static {v0}, Lorg/chromium/base/TraceEvent;->begin(Ljava/lang/String;)V

    .line 75
    iget-object v0, p0, Lcom/google/android/apps/chrome/icing/GoogleApiIcingClientImpl;->mClient:Lcom/google/android/gms/common/api/i;

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, p1, p2, v1}, Lcom/google/android/gms/common/api/i;->a(JLjava/util/concurrent/TimeUnit;)Lcom/google/android/gms/common/ConnectionResult;

    move-result-object v0

    .line 76
    invoke-virtual {v0}, Lcom/google/android/gms/common/ConnectionResult;->isSuccess()Z

    move-result v1

    if-nez v1, :cond_0

    .line 77
    const-string/jumbo v1, "GoogleApiIcingClientImpl"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Connection to GmsCore unsuccessful. Error "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/gms/common/ConnectionResult;->getErrorCode()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 81
    :goto_0
    invoke-virtual {v0}, Lcom/google/android/gms/common/ConnectionResult;->isSuccess()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 83
    const-string/jumbo v1, "GoogleApiIcingClientImpl:connectWithTimeout"

    invoke-static {v1}, Lorg/chromium/base/TraceEvent;->end(Ljava/lang/String;)V

    return v0

    .line 79
    :cond_0
    :try_start_1
    const-string/jumbo v1, "GoogleApiIcingClientImpl"

    const-string/jumbo v2, "Connection to GmsCore successful."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/chrome/icing/IcingDebugLogger;->debug(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 83
    :catchall_0
    move-exception v0

    const-string/jumbo v1, "GoogleApiIcingClientImpl:connectWithTimeout"

    invoke-static {v1}, Lorg/chromium/base/TraceEvent;->end(Ljava/lang/String;)V

    throw v0
.end method

.method public disconnect()V
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/google/android/apps/chrome/icing/GoogleApiIcingClientImpl;->mClient:Lcom/google/android/gms/common/api/i;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/i;->b()V

    .line 90
    return-void
.end method

.method public getLastCommittedSeqno()J
    .locals 4

    .prologue
    .line 155
    sget-object v0, Lcom/google/android/gms/search/a;->c:Lcom/google/android/gms/search/corpora/d;

    iget-object v1, p0, Lcom/google/android/apps/chrome/icing/GoogleApiIcingClientImpl;->mClient:Lcom/google/android/gms/common/api/i;

    iget-object v2, p0, Lcom/google/android/apps/chrome/icing/GoogleApiIcingClientImpl;->mApplicationContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/chrome/icing/GoogleApiIcingClientImpl;->mCorpusName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/search/corpora/d;->b(Lcom/google/android/gms/common/api/i;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    .line 158
    invoke-interface {v0}, Lcom/google/android/gms/common/api/l;->a()Lcom/google/android/gms/common/api/n;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/search/corpora/GetCorpusStatusCall$Response;

    .line 159
    iget-object v1, v0, Lcom/google/android/gms/search/corpora/GetCorpusStatusCall$Response;->a:Lcom/google/android/gms/common/api/Status;

    invoke-virtual {v1}, Lcom/google/android/gms/common/api/Status;->e()Z

    move-result v1

    if-nez v1, :cond_0

    .line 160
    const-string/jumbo v1, "GoogleApiIcingClientImpl"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "getLastCommittedSeqno unsuccessful. Status Code: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, v0, Lcom/google/android/gms/search/corpora/GetCorpusStatusCall$Response;->a:Lcom/google/android/gms/common/api/Status;

    invoke-virtual {v3}, Lcom/google/android/gms/common/api/Status;->f()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " Message: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v0, Lcom/google/android/gms/search/corpora/GetCorpusStatusCall$Response;->a:Lcom/google/android/gms/common/api/Status;

    invoke-virtual {v3}, Lcom/google/android/gms/common/api/Status;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 166
    :goto_0
    iget-object v1, v0, Lcom/google/android/gms/search/corpora/GetCorpusStatusCall$Response;->a:Lcom/google/android/gms/common/api/Status;

    invoke-virtual {v1}, Lcom/google/android/gms/common/api/Status;->e()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v0, v0, Lcom/google/android/gms/search/corpora/GetCorpusStatusCall$Response;->b:Lcom/google/android/gms/appdatasearch/CorpusStatus;

    invoke-virtual {v0}, Lcom/google/android/gms/appdatasearch/CorpusStatus;->a()J

    move-result-wide v0

    :goto_1
    return-wide v0

    .line 164
    :cond_0
    const-string/jumbo v1, "GoogleApiIcingClientImpl"

    const-string/jumbo v2, "getLastCommittedSeqno  successful."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/chrome/icing/IcingDebugLogger;->debug(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 166
    :cond_1
    const-wide/16 v0, 0x0

    goto :goto_1
.end method

.method public isGooglePlayServicesAvailable()Z
    .locals 2

    .prologue
    .line 95
    :try_start_0
    const-string/jumbo v0, "GoogleApiIcingClientImpl:isGooglePlayServicesAvailable"

    invoke-static {v0}, Lorg/chromium/base/TraceEvent;->begin(Ljava/lang/String;)V

    .line 96
    iget-object v0, p0, Lcom/google/android/apps/chrome/icing/GoogleApiIcingClientImpl;->mApplicationContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/common/f;->a(Landroid/content/Context;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 97
    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 99
    :goto_0
    const-string/jumbo v1, "GoogleApiIcingClientImpl:isGooglePlayServicesAvailable"

    invoke-static {v1}, Lorg/chromium/base/TraceEvent;->end(Ljava/lang/String;)V

    return v0

    .line 97
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 99
    :catchall_0
    move-exception v0

    const-string/jumbo v1, "GoogleApiIcingClientImpl:isGooglePlayServicesAvailable"

    invoke-static {v1}, Lorg/chromium/base/TraceEvent;->end(Ljava/lang/String;)V

    throw v0
.end method

.method public reportContext(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 11

    .prologue
    const/4 v4, 0x4

    const/4 v10, 0x1

    const/4 v7, 0x0

    .line 221
    new-instance v1, Lcom/google/android/gms/appdatasearch/DocumentId;

    iget-object v0, p0, Lcom/google/android/apps/chrome/icing/GoogleApiIcingClientImpl;->mApplicationContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v2, ""

    invoke-direct {v1, v0, v2, p1}, Lcom/google/android/gms/appdatasearch/DocumentId;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    iget-object v0, p0, Lcom/google/android/apps/chrome/icing/GoogleApiIcingClientImpl;->mApplicationContext:Landroid/content/Context;

    invoke-static {v0, p2, p1}, Lcom/google/android/apps/chrome/icing/SsbContextHelper;->getContextStubForSsb(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/c/f;

    move-result-object v0

    .line 228
    const/4 v6, 0x0

    .line 231
    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/chrome/icing/GoogleApiIcingClientImpl;->mApplicationContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/chrome/R$integer;->google_play_services_version:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    .line 233
    const v3, 0x5bc048

    if-ge v2, v3, :cond_0

    .line 234
    const-class v2, Lcom/google/android/gms/appdatasearch/DocumentContents;

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Class;

    const/4 v5, 0x0

    const-class v8, Ljava/lang/String;

    aput-object v8, v3, v5

    const/4 v5, 0x1

    sget-object v8, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v8, v3, v5

    const/4 v5, 0x2

    const-class v8, Lcom/google/android/gms/appdatasearch/DocumentSection;

    aput-object v8, v3, v5

    invoke-virtual {v2, v3}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v2

    .line 236
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/reflect/Constructor;->setAccessible(Z)V

    .line 237
    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-string/jumbo v8, "WebPage"

    aput-object v8, v3, v5

    const/4 v5, 0x1

    const/4 v8, 0x0

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    aput-object v8, v3, v5

    const/4 v5, 0x2

    new-instance v8, Lcom/google/android/gms/appdatasearch/DocumentSection;

    invoke-static {v0}, Lcom/google/protobuf/nano/MessageNano;->toByteArray(Lcom/google/protobuf/nano/MessageNano;)[B

    move-result-object v0

    sget-object v9, Lcom/google/android/apps/chrome/icing/GoogleApiIcingClientImpl;->SSB_CONTEXT_SECTION_INFO:Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;

    invoke-direct {v8, v0, v9}, Lcom/google/android/gms/appdatasearch/DocumentSection;-><init>([BLcom/google/android/gms/appdatasearch/RegisterSectionInfo;)V

    aput-object v8, v3, v5

    invoke-virtual {v2, v3}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/appdatasearch/DocumentContents;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_3

    move-object v6, v0

    .line 259
    :goto_0
    new-instance v0, Lcom/google/android/gms/appdatasearch/UsageInfo;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-string/jumbo v5, ""

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/appdatasearch/UsageInfo;-><init>(Lcom/google/android/gms/appdatasearch/DocumentId;JILjava/lang/String;Lcom/google/android/gms/appdatasearch/DocumentContents;)V

    .line 261
    sget-object v1, Lcom/google/android/gms/appdatasearch/a;->c:Lcom/google/android/gms/appdatasearch/l;

    iget-object v2, p0, Lcom/google/android/apps/chrome/icing/GoogleApiIcingClientImpl;->mClient:Lcom/google/android/gms/common/api/i;

    new-array v3, v10, [Lcom/google/android/gms/appdatasearch/UsageInfo;

    aput-object v0, v3, v7

    invoke-interface {v1, v2, v3}, Lcom/google/android/gms/appdatasearch/l;->a(Lcom/google/android/gms/common/api/i;[Lcom/google/android/gms/appdatasearch/UsageInfo;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    .line 262
    sget-wide v2, Lcom/google/android/apps/chrome/icing/GoogleApiIcingClientImpl;->REPORT_USAGE_TIMEOUT_MS:J

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v2, v3, v1}, Lcom/google/android/gms/common/api/l;->a(JLjava/util/concurrent/TimeUnit;)Lcom/google/android/gms/common/api/n;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/api/Status;

    .line 263
    if-nez v0, :cond_1

    move v0, v7

    .line 270
    :goto_1
    return v0

    .line 241
    :cond_0
    :try_start_1
    const-class v2, Lcom/google/android/gms/appdatasearch/DocumentContents;

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Class;

    const/4 v5, 0x0

    const-class v8, Ljava/lang/String;

    aput-object v8, v3, v5

    const/4 v5, 0x1

    sget-object v8, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v8, v3, v5

    const/4 v5, 0x2

    const-class v8, Landroid/accounts/Account;

    aput-object v8, v3, v5

    const/4 v5, 0x3

    const-class v8, Lcom/google/android/gms/appdatasearch/DocumentSection;

    aput-object v8, v3, v5

    invoke-virtual {v2, v3}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v2

    .line 243
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/reflect/Constructor;->setAccessible(Z)V

    .line 244
    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-string/jumbo v8, "WebPage"

    aput-object v8, v3, v5

    const/4 v5, 0x1

    const/4 v8, 0x0

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    aput-object v8, v3, v5

    const/4 v5, 0x2

    iget-object v8, p0, Lcom/google/android/apps/chrome/icing/GoogleApiIcingClientImpl;->mApplicationContext:Landroid/content/Context;

    invoke-static {v8}, Lorg/chromium/sync/signin/ChromeSigninController;->get(Landroid/content/Context;)Lorg/chromium/sync/signin/ChromeSigninController;

    move-result-object v8

    invoke-virtual {v8}, Lorg/chromium/sync/signin/ChromeSigninController;->getSignedInUser()Landroid/accounts/Account;

    move-result-object v8

    aput-object v8, v3, v5

    const/4 v5, 0x3

    new-instance v8, Lcom/google/android/gms/appdatasearch/DocumentSection;

    invoke-static {v0}, Lcom/google/protobuf/nano/MessageNano;->toByteArray(Lcom/google/protobuf/nano/MessageNano;)[B

    move-result-object v0

    sget-object v9, Lcom/google/android/apps/chrome/icing/GoogleApiIcingClientImpl;->SSB_CONTEXT_SECTION_INFO:Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;

    invoke-direct {v8, v0, v9}, Lcom/google/android/gms/appdatasearch/DocumentSection;-><init>([BLcom/google/android/gms/appdatasearch/RegisterSectionInfo;)V

    aput-object v8, v3, v5

    invoke-virtual {v2, v3}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/appdatasearch/DocumentContents;
    :try_end_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/InstantiationException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_3

    move-object v6, v0

    .line 257
    goto :goto_0

    .line 249
    :catch_0
    move-exception v0

    .line 250
    const-string/jumbo v0, "GoogleApiIcingClientImpl"

    const-string/jumbo v2, "NoSuchMethodException while trying to construct DocumentContents"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 251
    :catch_1
    move-exception v0

    .line 252
    const-string/jumbo v0, "GoogleApiIcingClientImpl"

    const-string/jumbo v2, "InstantiationException while trying to construct DocumentContents"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 253
    :catch_2
    move-exception v0

    .line 254
    const-string/jumbo v0, "GoogleApiIcingClientImpl"

    const-string/jumbo v2, "IllegalAccessException while trying to construct DocumentContents"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 255
    :catch_3
    move-exception v0

    .line 256
    const-string/jumbo v0, "GoogleApiIcingClientImpl"

    const-string/jumbo v2, "InvocationTargetException while trying to construct DocumentContents"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 264
    :cond_1
    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->e()Z

    move-result v1

    if-nez v1, :cond_2

    .line 265
    const-string/jumbo v1, "GoogleApiIcingClientImpl"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "reportContext unsuccessful. Status Code: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->f()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " Message: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 270
    :goto_2
    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->e()Z

    move-result v0

    goto/16 :goto_1

    .line 268
    :cond_2
    const-string/jumbo v1, "GoogleApiIcingClientImpl"

    const-string/jumbo v2, "reportContext successful."

    new-array v3, v7, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/chrome/icing/IcingDebugLogger;->debug(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2
.end method

.method public varargs reportPageVisits([Lcom/google/android/apps/chrome/icing/UsageReport;)Z
    .locals 11

    .prologue
    const/16 v10, 0x20

    const/4 v1, 0x0

    .line 105
    array-length v0, p1

    new-array v3, v0, [Lcom/google/android/gms/appdatasearch/UsageInfo;

    move v0, v1

    .line 106
    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_1

    .line 107
    aget-object v2, p1, v0

    iget-boolean v2, v2, Lcom/google/android/apps/chrome/icing/UsageReport;->typedVisit:Z

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    .line 109
    :goto_1
    new-instance v4, Lcom/google/android/gms/appdatasearch/UsageInfo;

    new-instance v5, Lcom/google/android/gms/appdatasearch/DocumentId;

    iget-object v6, p0, Lcom/google/android/apps/chrome/icing/GoogleApiIcingClientImpl;->mApplicationContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/apps/chrome/icing/GoogleApiIcingClientImpl;->mCorpusName:Ljava/lang/String;

    aget-object v8, p1, v0

    iget-object v8, v8, Lcom/google/android/apps/chrome/icing/UsageReport;->pageId:Ljava/lang/String;

    invoke-direct {v5, v6, v7, v8}, Lcom/google/android/gms/appdatasearch/DocumentId;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aget-object v6, p1, v0

    iget-wide v6, v6, Lcom/google/android/apps/chrome/icing/UsageReport;->timestampMs:J

    invoke-direct {v4, v5, v6, v7, v2}, Lcom/google/android/gms/appdatasearch/UsageInfo;-><init>(Lcom/google/android/gms/appdatasearch/DocumentId;JI)V

    aput-object v4, v3, v0

    .line 106
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    move v2, v1

    .line 107
    goto :goto_1

    .line 116
    :cond_1
    invoke-static {}, Lcom/google/android/apps/chrome/icing/IcingDebugLogger;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 117
    new-instance v2, Ljava/lang/StringBuilder;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Reporting "

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v4, p1

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v4, " usage reports:"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 118
    array-length v4, p1

    move v0, v1

    :goto_2
    if-ge v0, v4, :cond_2

    aget-object v5, p1, v0

    .line 119
    const/16 v6, 0xa

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v5, Lcom/google/android/apps/chrome/icing/UsageReport;->pageId:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-wide v8, v5, Lcom/google/android/apps/chrome/icing/UsageReport;->timestampMs:J

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-boolean v5, v5, Lcom/google/android/apps/chrome/icing/UsageReport;->typedVisit:Z

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 118
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 122
    :cond_2
    const-string/jumbo v0, "GoogleApiIcingClientImpl"

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v4}, Lcom/google/android/apps/chrome/icing/IcingDebugLogger;->debug(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 124
    :cond_3
    sget-object v0, Lcom/google/android/gms/appdatasearch/a;->c:Lcom/google/android/gms/appdatasearch/l;

    iget-object v2, p0, Lcom/google/android/apps/chrome/icing/GoogleApiIcingClientImpl;->mClient:Lcom/google/android/gms/common/api/i;

    invoke-interface {v0, v2, v3}, Lcom/google/android/gms/appdatasearch/l;->a(Lcom/google/android/gms/common/api/i;[Lcom/google/android/gms/appdatasearch/UsageInfo;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    .line 125
    sget-wide v2, Lcom/google/android/apps/chrome/icing/GoogleApiIcingClientImpl;->REPORT_USAGE_TIMEOUT_MS:J

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v2, v3, v4}, Lcom/google/android/gms/common/api/l;->a(JLjava/util/concurrent/TimeUnit;)Lcom/google/android/gms/common/api/n;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/api/Status;

    .line 126
    if-nez v0, :cond_4

    .line 135
    :goto_3
    return v1

    .line 129
    :cond_4
    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->e()Z

    move-result v2

    if-nez v2, :cond_5

    .line 130
    const-string/jumbo v1, "GoogleApiIcingClientImpl"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "reportPageVisit unsuccessful. Status Code: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->f()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " Message: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 135
    :goto_4
    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->e()Z

    move-result v1

    goto :goto_3

    .line 133
    :cond_5
    const-string/jumbo v2, "GoogleApiIcingClientImpl"

    const-string/jumbo v3, "reportPageVisit  successful."

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v2, v3, v1}, Lcom/google/android/apps/chrome/icing/IcingDebugLogger;->debug(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_4
.end method

.method public requestIndexing(J)V
    .locals 7

    .prologue
    .line 140
    sget-object v0, Lcom/google/android/gms/search/a;->c:Lcom/google/android/gms/search/corpora/d;

    iget-object v1, p0, Lcom/google/android/apps/chrome/icing/GoogleApiIcingClientImpl;->mClient:Lcom/google/android/gms/common/api/i;

    iget-object v2, p0, Lcom/google/android/apps/chrome/icing/GoogleApiIcingClientImpl;->mApplicationContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/chrome/icing/GoogleApiIcingClientImpl;->mCorpusName:Ljava/lang/String;

    move-wide v4, p1

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/search/corpora/d;->a(Lcom/google/android/gms/common/api/i;Ljava/lang/String;Ljava/lang/String;J)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    .line 143
    invoke-interface {v0}, Lcom/google/android/gms/common/api/l;->a()Lcom/google/android/gms/common/api/n;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/search/corpora/RequestIndexingCall$Response;

    .line 144
    iget-object v1, v0, Lcom/google/android/gms/search/corpora/RequestIndexingCall$Response;->a:Lcom/google/android/gms/common/api/Status;

    invoke-virtual {v1}, Lcom/google/android/gms/common/api/Status;->e()Z

    move-result v1

    if-nez v1, :cond_0

    .line 145
    const-string/jumbo v1, "GoogleApiIcingClientImpl"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "requestIndexing unsuccessful. Status Code: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, v0, Lcom/google/android/gms/search/corpora/RequestIndexingCall$Response;->a:Lcom/google/android/gms/common/api/Status;

    invoke-virtual {v3}, Lcom/google/android/gms/common/api/Status;->f()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " Message: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, v0, Lcom/google/android/gms/search/corpora/RequestIndexingCall$Response;->a:Lcom/google/android/gms/common/api/Status;

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 151
    :goto_0
    return-void

    .line 149
    :cond_0
    const-string/jumbo v0, "GoogleApiIcingClientImpl"

    const-string/jumbo v1, "requestIndexing  successful."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/icing/IcingDebugLogger;->debug(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method
