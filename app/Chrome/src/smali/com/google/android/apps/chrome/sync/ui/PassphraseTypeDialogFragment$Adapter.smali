.class Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment$Adapter;
.super Landroid/widget/ArrayAdapter;
.source "PassphraseTypeDialogFragment.java"


# instance fields
.field private final mElementsContainer:Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment$PassphraseTypeUiElementContainer;

.field final synthetic this$0:Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment;Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment$PassphraseTypeUiElementContainer;[Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 117
    iput-object p1, p0, Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment$Adapter;->this$0:Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment;

    .line 118
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x109000f

    const v2, 0x1020014

    invoke-direct {p0, v0, v1, v2, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;II[Ljava/lang/Object;)V

    .line 120
    iput-object p2, p0, Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment$Adapter;->mElementsContainer:Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment$PassphraseTypeUiElementContainer;

    .line 121
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment;Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment$PassphraseTypeUiElementContainer;[Ljava/lang/String;Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment$1;)V
    .locals 0

    .prologue
    .line 108
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment$Adapter;-><init>(Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment;Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment$PassphraseTypeUiElementContainer;[Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public getItemId(I)J
    .locals 2

    .prologue
    .line 130
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment$Adapter;->getType(I)Lorg/chromium/sync/internal_api/pub/SyncDecryptionPassphraseType;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/sync/internal_api/pub/SyncDecryptionPassphraseType;->internalValue()I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method

.method public getPositionForType(Lorg/chromium/sync/internal_api/pub/SyncDecryptionPassphraseType;)I
    .locals 3

    .prologue
    .line 138
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment$Adapter;->mElementsContainer:Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment$PassphraseTypeUiElementContainer;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment$PassphraseTypeUiElementContainer;->getElements()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment$PassphraseTypeUiElement;

    .line 139
    # getter for: Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment$PassphraseTypeUiElement;->mPassphraseType:Lorg/chromium/sync/internal_api/pub/SyncDecryptionPassphraseType;
    invoke-static {v0}, Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment$PassphraseTypeUiElement;->access$500(Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment$PassphraseTypeUiElement;)Lorg/chromium/sync/internal_api/pub/SyncDecryptionPassphraseType;

    move-result-object v2

    if-ne v2, p1, :cond_0

    .line 140
    # getter for: Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment$PassphraseTypeUiElement;->mPosition:I
    invoke-static {v0}, Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment$PassphraseTypeUiElement;->access$000(Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment$PassphraseTypeUiElement;)I

    move-result v0

    .line 143
    :goto_0
    return v0

    :cond_1
    sget-object v0, Lorg/chromium/sync/internal_api/pub/SyncDecryptionPassphraseType;->INVALID:Lorg/chromium/sync/internal_api/pub/SyncDecryptionPassphraseType;

    invoke-virtual {v0}, Lorg/chromium/sync/internal_api/pub/SyncDecryptionPassphraseType;->internalValue()I

    move-result v0

    goto :goto_0
.end method

.method public getType(I)Lorg/chromium/sync/internal_api/pub/SyncDecryptionPassphraseType;
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment$Adapter;->mElementsContainer:Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment$PassphraseTypeUiElementContainer;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment$PassphraseTypeUiElementContainer;->getElements()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment$PassphraseTypeUiElement;

    # getter for: Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment$PassphraseTypeUiElement;->mPassphraseType:Lorg/chromium/sync/internal_api/pub/SyncDecryptionPassphraseType;
    invoke-static {v0}, Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment$PassphraseTypeUiElement;->access$500(Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment$PassphraseTypeUiElement;)Lorg/chromium/sync/internal_api/pub/SyncDecryptionPassphraseType;

    move-result-object v0

    return-object v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    .line 148
    invoke-super {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckedTextView;

    .line 149
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment$Adapter;->getType(I)Lorg/chromium/sync/internal_api/pub/SyncDecryptionPassphraseType;

    move-result-object v2

    .line 150
    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment$Adapter;->this$0:Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment;->getCurrentTypeFromArguments()Lorg/chromium/sync/internal_api/pub/SyncDecryptionPassphraseType;

    move-result-object v1

    .line 151
    iget-object v3, p0, Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment$Adapter;->this$0:Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment;

    # invokes: Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment;->getEncryptEverythingAllowedFromArguments()Z
    invoke-static {v3}, Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment;->access$600(Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment;)Z

    move-result v3

    invoke-virtual {v1, v3}, Lorg/chromium/sync/internal_api/pub/SyncDecryptionPassphraseType;->getAllowedTypes(Z)Ljava/util/Set;

    move-result-object v3

    .line 155
    if-ne v2, v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    .line 157
    invoke-interface {v3, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckedTextView;->setEnabled(Z)V

    .line 158
    return-object v0

    .line 155
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public hasStableIds()Z
    .locals 1

    .prologue
    .line 125
    const/4 v0, 0x1

    return v0
.end method
