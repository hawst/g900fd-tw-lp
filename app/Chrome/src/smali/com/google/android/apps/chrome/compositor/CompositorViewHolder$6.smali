.class Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$6;
.super Ljava/lang/Object;
.source "CompositorViewHolder.java"

# interfaces
.implements Lorg/chromium/chrome/browser/tabmodel/TabModelSelector$ChangeListener;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;)V
    .locals 0

    .prologue
    .line 868
    iput-object p1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$6;->this$0:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onChange()V
    .locals 1

    .prologue
    .line 871
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$6;->this$0:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    # invokes: Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->modelChanged()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->access$000(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;)V

    .line 872
    return-void
.end method

.method public onNewTabCreated(Lorg/chromium/chrome/browser/Tab;)V
    .locals 2

    .prologue
    .line 875
    invoke-virtual {p1}, Lorg/chromium/chrome/browser/Tab;->getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    .line 876
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$6;->this$0:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    # invokes: Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->setContentViewCoreSize(Lorg/chromium/content/browser/ContentViewCore;)V
    invoke-static {v1, v0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->access$1000(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;Lorg/chromium/content/browser/ContentViewCore;)V

    .line 877
    :cond_0
    return-void
.end method
