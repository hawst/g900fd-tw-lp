.class public Lcom/google/android/apps/chrome/ChromeWindow;
.super Lorg/chromium/ui/base/ActivityWindowAndroid;
.source "ChromeWindow.java"


# direct methods
.method public constructor <init>(Lcom/google/android/apps/chrome/ChromeActivity;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lorg/chromium/ui/base/ActivityWindowAndroid;-><init>(Landroid/app/Activity;)V

    .line 27
    return-void
.end method


# virtual methods
.method public getGooglePlayServicesErrorDialog(II)Landroid/app/Dialog;
    .locals 1

    .prologue
    .line 33
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeWindow;->getActivity()Ljava/lang/ref/WeakReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-static {p1, v0, p2}, Lcom/google/android/gms/common/f;->a(ILandroid/app/Activity;I)Landroid/app/Dialog;

    move-result-object v0

    return-object v0
.end method

.method protected showCallbackNonExistentError(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 41
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeWindow;->getActivity()Ljava/lang/ref/WeakReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 45
    if-eqz v0, :cond_0

    check-cast v0, Lcom/google/android/apps/chrome/ChromeActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeActivity;->getCurrentTab()Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    .line 47
    :goto_0
    if-eqz v0, :cond_1

    .line 49
    new-instance v1, Lorg/chromium/chrome/browser/infobar/MessageInfoBar;

    invoke-direct {v1, p1}, Lorg/chromium/chrome/browser/infobar/MessageInfoBar;-><init>(Ljava/lang/CharSequence;)V

    .line 50
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lorg/chromium/chrome/browser/infobar/MessageInfoBar;->setExpireOnNavigation(Z)V

    .line 51
    invoke-virtual {v0}, Lorg/chromium/chrome/browser/Tab;->getInfoBarContainer()Lorg/chromium/chrome/browser/infobar/InfoBarContainer;

    move-result-object v0

    invoke-virtual {v0, v1}, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->addInfoBar(Lorg/chromium/chrome/browser/infobar/InfoBar;)V

    .line 55
    :goto_1
    return-void

    .line 45
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 53
    :cond_1
    invoke-super {p0, p1}, Lorg/chromium/ui/base/ActivityWindowAndroid;->showCallbackNonExistentError(Ljava/lang/String;)V

    goto :goto_1
.end method
