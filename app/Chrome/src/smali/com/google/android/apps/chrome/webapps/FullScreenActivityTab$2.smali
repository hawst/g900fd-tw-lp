.class Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab$2;
.super Ljava/lang/Object;
.source "FullScreenActivityTab.java"

# interfaces
.implements Lorg/chromium/chrome/browser/contextmenu/ContextMenuPopulator;


# instance fields
.field private final mClipboard:Lorg/chromium/ui/base/Clipboard;

.field final synthetic this$0:Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab;)V
    .locals 2

    .prologue
    .line 184
    iput-object p1, p0, Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab$2;->this$0:Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 189
    new-instance v0, Lorg/chromium/ui/base/Clipboard;

    iget-object v1, p0, Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab$2;->this$0:Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab;

    # invokes: Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab;->getApplicationContext()Landroid/content/Context;
    invoke-static {v1}, Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab;->access$100(Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab;)Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/chromium/ui/base/Clipboard;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab$2;->mClipboard:Lorg/chromium/ui/base/Clipboard;

    .line 190
    return-void
.end method


# virtual methods
.method public buildContextMenu(Landroid/view/ContextMenu;Landroid/content/Context;Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 223
    sget v0, Lorg/chromium/chrome/R$id;->contextmenu_copy_link_address_text:I

    sget v1, Lorg/chromium/chrome/R$string;->contextmenu_copy_link_address:I

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    .line 226
    invoke-virtual {p3}, Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;->getLinkText()Ljava/lang/String;

    move-result-object v0

    .line 227
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 229
    :cond_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 230
    sget v0, Lorg/chromium/chrome/R$id;->contextmenu_copy_link_text:I

    sget v1, Lorg/chromium/chrome/R$string;->contextmenu_copy_link_text:I

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    .line 234
    :cond_1
    sget v0, Lcom/google/android/apps/chrome/R$id;->contextmenu_webapp_open_in_chrome:I

    sget v1, Lcom/google/android/apps/chrome/R$string;->contextmenu_webapp_open_in_chrome:I

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    .line 236
    return-void
.end method

.method public onItemSelected(Lorg/chromium/chrome/browser/contextmenu/ContextMenuHelper;Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;I)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 200
    sget v1, Lorg/chromium/chrome/R$id;->contextmenu_copy_link_address_text:I

    if-ne p3, v1, :cond_0

    .line 201
    invoke-virtual {p2}, Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;->getUnfilteredLinkUrl()Ljava/lang/String;

    move-result-object v1

    .line 202
    iget-object v2, p0, Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab$2;->mClipboard:Lorg/chromium/ui/base/Clipboard;

    invoke-virtual {v2, v1, v1}, Lorg/chromium/ui/base/Clipboard;->setText(Ljava/lang/String;Ljava/lang/String;)V

    .line 217
    :goto_0
    return v0

    .line 204
    :cond_0
    sget v1, Lorg/chromium/chrome/R$id;->contextmenu_copy_link_text:I

    if-ne p3, v1, :cond_1

    .line 205
    invoke-virtual {p2}, Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;->getLinkText()Ljava/lang/String;

    move-result-object v1

    .line 206
    iget-object v2, p0, Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab$2;->mClipboard:Lorg/chromium/ui/base/Clipboard;

    invoke-virtual {v2, v1, v1}, Lorg/chromium/ui/base/Clipboard;->setText(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 208
    :cond_1
    sget v1, Lcom/google/android/apps/chrome/R$id;->contextmenu_webapp_open_in_chrome:I

    if-ne p3, v1, :cond_2

    .line 209
    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v2, "android.intent.action.VIEW"

    invoke-virtual {p2}, Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;->getLinkUrl()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 211
    iget-object v2, p0, Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab$2;->this$0:Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab;

    # invokes: Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab;->getApplicationContext()Landroid/content/Context;
    invoke-static {v2}, Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab;->access$200(Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 212
    const/high16 v2, 0x10000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 213
    iget-object v2, p0, Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab$2;->this$0:Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab;

    # invokes: Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab;->getApplicationContext()Landroid/content/Context;
    invoke-static {v2}, Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab;->access$300(Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 217
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public shouldShowContextMenu(Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;)Z
    .locals 1

    .prologue
    .line 194
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lorg/chromium/chrome/browser/contextmenu/ContextMenuParams;->isAnchor()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
