.class Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$GeolocationInfoFetcher;
.super Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$CallChain;
.source "WebsitePreferences.java"


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences;)V
    .locals 1

    .prologue
    .line 81
    iput-object p1, p0, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$GeolocationInfoFetcher;->this$0:Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$CallChain;-><init>(Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences;Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$1;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences;Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$1;)V
    .locals 0

    .prologue
    .line 81
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$GeolocationInfoFetcher;-><init>(Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences;)V

    return-void
.end method


# virtual methods
.method run()V
    .locals 4

    .prologue
    .line 84
    invoke-static {}, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferenceBridge;->getGeolocationInfo()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/preferences/website/GeolocationInfo;

    .line 85
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/website/GeolocationInfo;->getOrigin()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/chrome/preferences/website/WebsiteAddress;->create(Ljava/lang/String;)Lcom/google/android/apps/chrome/preferences/website/WebsiteAddress;

    move-result-object v2

    .line 86
    if-eqz v2, :cond_0

    .line 87
    iget-object v3, p0, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$GeolocationInfoFetcher;->this$0:Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences;

    # invokes: Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences;->createSiteByOrigin(Lcom/google/android/apps/chrome/preferences/website/WebsiteAddress;)Lcom/google/android/apps/chrome/preferences/website/Website;
    invoke-static {v3, v2}, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences;->access$900(Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences;Lcom/google/android/apps/chrome/preferences/website/WebsiteAddress;)Lcom/google/android/apps/chrome/preferences/website/Website;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/android/apps/chrome/preferences/website/Website;->setGeolocationInfo(Lcom/google/android/apps/chrome/preferences/website/GeolocationInfo;)V

    goto :goto_0

    .line 89
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$GeolocationInfoFetcher;->next()V

    .line 90
    return-void
.end method
