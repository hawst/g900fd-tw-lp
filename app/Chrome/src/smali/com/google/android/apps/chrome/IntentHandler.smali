.class public Lcom/google/android/apps/chrome/IntentHandler;
.super Ljava/lang/Object;
.source "IntentHandler.java"


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final DOCUMENT_SCHEME:Ljava/lang/String; = "document"

.field public static final EXTRA_APPEND_TASK:Ljava/lang/String; = "com.android.chrome.append_task"

.field public static final EXTRA_INVOKED_FROM_FRE:Ljava/lang/String; = "com.android.chrome.invoked_from_fre"

.field public static final EXTRA_NATIVE_WEB_CONTENTS:Ljava/lang/String; = "com.android.chrome.native_web_contents"

.field public static final EXTRA_OPEN_IN_BG:Ljava/lang/String; = "com.android.chrome.open_with_affiliation"

.field public static final EXTRA_OPEN_NEW_INCOGNITO_TAB:Ljava/lang/String; = "com.google.android.apps.chrome.EXTRA_OPEN_NEW_INCOGNITO_TAB"

.field public static final EXTRA_ORIGINAL_INTENT:Ljava/lang/String; = "com.android.chrome.original_intent"

.field public static final EXTRA_PAGE_TRANSITION_TYPE:Ljava/lang/String; = "com.google.chrome.transition_type"

.field public static final EXTRA_PARENT_TAB_ID:Ljava/lang/String; = "com.android.chrome.parent_tab_id"

.field public static final EXTRA_PRESERVE_TASK:Ljava/lang/String; = "com.android.chrome.preserve_task"

.field public static final EXTRA_STARTED_BY:Ljava/lang/String; = "com.android.chrome.started_by"

.field public static final EXTRA_USE_DESKTOP_USER_AGENT:Ljava/lang/String; = "com.android.chrome.use_desktop_user_agent"

.field public static final GOOGLECHROME_NAVIGATE_PREFIX:Ljava/lang/String; = "googlechrome://navigate?url="

.field public static final GOOGLECHROME_SCHEME:Ljava/lang/String; = "googlechrome"

.field private static final LOCK:Ljava/lang/Object;

.field private static sFakeComponentName:Landroid/content/ComponentName;

.field private static sTestIntentsEnabled:Z


# instance fields
.field private final mDelegate:Lcom/google/android/apps/chrome/IntentHandler$IntentHandlerDelegate;

.field private mKeyguardManager:Landroid/app/KeyguardManager;

.field private final mPackageName:Ljava/lang/String;

.field private mPowerManager:Landroid/os/PowerManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 46
    const-class v0, Lcom/google/android/apps/chrome/IntentHandler;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/IntentHandler;->$assertionsDisabled:Z

    .line 113
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/apps/chrome/IntentHandler;->sFakeComponentName:Landroid/content/ComponentName;

    .line 115
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/apps/chrome/IntentHandler;->LOCK:Ljava/lang/Object;

    return-void

    .line 46
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/google/android/apps/chrome/IntentHandler$IntentHandlerDelegate;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 194
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 195
    iput-object p1, p0, Lcom/google/android/apps/chrome/IntentHandler;->mDelegate:Lcom/google/android/apps/chrome/IntentHandler$IntentHandlerDelegate;

    .line 196
    iput-object p2, p0, Lcom/google/android/apps/chrome/IntentHandler;->mPackageName:Ljava/lang/String;

    .line 197
    return-void
.end method

.method public static addTrustedIntentExtras(Landroid/content/Intent;Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 340
    invoke-static {p0, p1}, Lcom/google/android/apps/chrome/IntentHandler;->willChromeHandleIntent(Landroid/content/Intent;Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 344
    const-string/jumbo v0, "trusted_application_code_extra"

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/chrome/IntentHandler;->getAuthenticationToken(Landroid/content/Context;)Landroid/app/PendingIntent;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 350
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 352
    :cond_0
    return-void
.end method

.method public static determineExternalIntentSource(Ljava/lang/String;Landroid/content/Intent;)Lcom/google/android/apps/chrome/IntentHandler$ExternalAppId;
    .locals 3

    .prologue
    .line 206
    const-string/jumbo v0, "com.android.browser.application_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 207
    sget-object v0, Lcom/google/android/apps/chrome/IntentHandler$ExternalAppId;->OTHER:Lcom/google/android/apps/chrome/IntentHandler$ExternalAppId;

    .line 208
    if-nez v1, :cond_2

    .line 209
    invoke-static {p1}, Lcom/google/android/apps/chrome/IntentHandler;->getUrlFromIntent(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v1

    .line 210
    if-eqz v1, :cond_1

    const-string/jumbo v2, "http://t.co/"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 211
    sget-object v0, Lcom/google/android/apps/chrome/IntentHandler$ExternalAppId;->TWITTER:Lcom/google/android/apps/chrome/IntentHandler$ExternalAppId;

    .line 220
    :cond_0
    :goto_0
    return-object v0

    .line 212
    :cond_1
    if-eqz v1, :cond_0

    const-string/jumbo v2, "http://m.facebook.com/l.php?"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 213
    sget-object v0, Lcom/google/android/apps/chrome/IntentHandler$ExternalAppId;->FACEBOOK:Lcom/google/android/apps/chrome/IntentHandler$ExternalAppId;

    goto :goto_0

    .line 216
    :cond_2
    const-string/jumbo v2, "com.google.android.apps.plus"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    sget-object v0, Lcom/google/android/apps/chrome/IntentHandler$ExternalAppId;->PLUS:Lcom/google/android/apps/chrome/IntentHandler$ExternalAppId;

    goto :goto_0

    .line 217
    :cond_3
    const-string/jumbo v2, "com.google.android.gm"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    sget-object v0, Lcom/google/android/apps/chrome/IntentHandler$ExternalAppId;->GMAIL:Lcom/google/android/apps/chrome/IntentHandler$ExternalAppId;

    goto :goto_0

    .line 218
    :cond_4
    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v0, Lcom/google/android/apps/chrome/IntentHandler$ExternalAppId;->CHROME:Lcom/google/android/apps/chrome/IntentHandler$ExternalAppId;

    goto :goto_0
.end method

.method private static getAuthenticationToken(Landroid/content/Context;)Landroid/app/PendingIntent;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 313
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 314
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/chrome/IntentHandler;->getFakeComponentName(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 315
    invoke-static {p0, v2, v0, v2}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method private getExtraHeadersFromIntent(Landroid/content/Intent;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 373
    const-string/jumbo v0, "com.android.browser.headers"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    .line 374
    if-nez v1, :cond_0

    const/4 v0, 0x0

    .line 384
    :goto_0
    return-object v0

    .line 375
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 376
    invoke-virtual {v1}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 377
    :cond_1
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 378
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 379
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 380
    const-string/jumbo v4, ": "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 381
    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 382
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string/jumbo v0, "\n"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 384
    :cond_2
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static getFakeComponentName(Ljava/lang/String;)Landroid/content/ComponentName;
    .locals 3

    .prologue
    .line 136
    sget-object v1, Lcom/google/android/apps/chrome/IntentHandler;->LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 137
    :try_start_0
    sget-object v0, Lcom/google/android/apps/chrome/IntentHandler;->sFakeComponentName:Landroid/content/ComponentName;

    if-nez v0, :cond_0

    .line 138
    new-instance v0, Landroid/content/ComponentName;

    const-string/jumbo v2, "FakeClass"

    invoke-direct {v0, p0, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/chrome/IntentHandler;->sFakeComponentName:Landroid/content/ComponentName;

    .line 140
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 142
    sget-object v0, Lcom/google/android/apps/chrome/IntentHandler;->sFakeComponentName:Landroid/content/ComponentName;

    return-object v0

    .line 140
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private getSanitizedUrlScheme(Ljava/lang/String;)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 544
    if-nez p1, :cond_1

    .line 570
    :cond_0
    :goto_0
    return-object v0

    .line 548
    :cond_1
    const-string/jumbo v2, ":"

    invoke-virtual {p1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    .line 549
    if-ltz v2, :cond_0

    .line 554
    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 560
    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v3

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_2

    aget-char v5, v3, v2

    .line 561
    invoke-static {v5}, Ljava/lang/Character;->isLetterOrDigit(C)Z

    move-result v6

    if-nez v6, :cond_3

    const/16 v6, 0x2d

    if-eq v5, v6, :cond_3

    const/16 v6, 0x2b

    if-eq v5, v6, :cond_3

    const/16 v6, 0x2e

    if-eq v5, v6, :cond_3

    .line 562
    const/4 v1, 0x1

    .line 567
    :cond_2
    if-eqz v1, :cond_0

    .line 568
    const-string/jumbo v1, "[^a-z1-9.+-]"

    const-string/jumbo v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 560
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method public static getTabIdFromIntent(Landroid/content/Intent;)I
    .locals 4

    .prologue
    const/4 v0, -0x1

    .line 641
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    if-nez v1, :cond_1

    .line 651
    :cond_0
    :goto_0
    return v0

    .line 643
    :cond_1
    invoke-virtual {p0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    .line 644
    invoke-virtual {v1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "document"

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 649
    :try_start_0
    invoke-virtual {v1}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    .line 651
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method private getTabOpenType(Landroid/content/Intent;)Lcom/google/android/apps/chrome/IntentHandler$TabOpenType;
    .locals 3

    .prologue
    const/4 v1, -0x1

    const/4 v2, 0x0

    .line 505
    const-string/jumbo v0, "REUSE_URL_MATCHING_TAB_ELSE_NEW_TAB"

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 507
    sget-object v0, Lcom/google/android/apps/chrome/IntentHandler$TabOpenType;->REUSE_URL_MATCHING_TAB_ELSE_NEW_TAB:Lcom/google/android/apps/chrome/IntentHandler$TabOpenType;

    .line 527
    :goto_0
    return-object v0

    .line 510
    :cond_0
    const-string/jumbo v0, "com.google.android.apps.chrome.EXTRA_OPEN_NEW_INCOGNITO_TAB"

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 511
    sget-object v0, Lcom/google/android/apps/chrome/IntentHandler$TabOpenType;->OPEN_NEW_INCOGNITO_TAB:Lcom/google/android/apps/chrome/IntentHandler$TabOpenType;

    goto :goto_0

    .line 514
    :cond_1
    sget-object v0, Lcom/google/android/apps/chrome/IntentHandler$TabOpenType;->BRING_TAB_TO_FRONT:Lcom/google/android/apps/chrome/IntentHandler$TabOpenType;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/IntentHandler$TabOpenType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    if-eq v0, v1, :cond_2

    .line 516
    sget-object v0, Lcom/google/android/apps/chrome/IntentHandler$TabOpenType;->BRING_TAB_TO_FRONT:Lcom/google/android/apps/chrome/IntentHandler$TabOpenType;

    goto :goto_0

    .line 519
    :cond_2
    const-string/jumbo v0, "com.android.browser.application_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 521
    if-eqz v0, :cond_3

    const-string/jumbo v1, "create_new_tab"

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 522
    :cond_3
    sget-object v0, Lcom/google/android/apps/chrome/IntentHandler$TabOpenType;->OPEN_NEW_TAB:Lcom/google/android/apps/chrome/IntentHandler$TabOpenType;

    goto :goto_0

    .line 527
    :cond_4
    iget-object v1, p0, Lcom/google/android/apps/chrome/IntentHandler;->mPackageName:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    sget-object v0, Lcom/google/android/apps/chrome/IntentHandler$TabOpenType;->CLOBBER_CURRENT_TAB:Lcom/google/android/apps/chrome/IntentHandler$TabOpenType;

    goto :goto_0

    :cond_5
    sget-object v0, Lcom/google/android/apps/chrome/IntentHandler$TabOpenType;->REUSE_APP_ID_MATCHING_TAB_ELSE_NEW_TAB:Lcom/google/android/apps/chrome/IntentHandler$TabOpenType;

    goto :goto_0
.end method

.method public static getUrlForDocument(Landroid/content/Intent;)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 604
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    if-nez v1, :cond_1

    .line 606
    :cond_0
    :goto_0
    return-object v0

    .line 605
    :cond_1
    invoke-virtual {p0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    .line 606
    invoke-virtual {v1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "document"

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Landroid/net/Uri;->getQuery()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static getUrlFromGoogleChromeSchemeUrl(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 617
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p0, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "googlechrome://navigate?url="

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 618
    const/16 v0, 0x1c

    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 621
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getUrlFromIntent(Landroid/content/Intent;)Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 584
    if-nez p0, :cond_1

    move-object v0, v1

    .line 595
    :cond_0
    :goto_0
    return-object v0

    .line 586
    :cond_1
    invoke-static {p0}, Lcom/google/android/apps/chrome/IntentHandler;->getUrlFromVoiceSearchResult(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v0

    .line 587
    if-nez v0, :cond_2

    invoke-static {p0}, Lcom/google/android/apps/chrome/IntentHandler;->getUrlForDocument(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v0

    .line 588
    :cond_2
    if-nez v0, :cond_3

    invoke-virtual {p0}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v0

    .line 589
    :cond_3
    if-nez v0, :cond_4

    move-object v0, v1

    goto :goto_0

    .line 591
    :cond_4
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 592
    invoke-static {v0}, Lcom/google/android/apps/chrome/IntentHandler;->isGoogleChromeScheme(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 593
    invoke-static {v0}, Lcom/google/android/apps/chrome/IntentHandler;->getUrlFromGoogleChromeSchemeUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 595
    :cond_5
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    move-object v0, v1

    goto :goto_0
.end method

.method static getUrlFromVoiceSearchResult(Landroid/content/Intent;)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x0

    .line 265
    const-string/jumbo v1, "android.speech.action.VOICE_SEARCH_RESULTS"

    invoke-virtual {p0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 293
    :cond_0
    :goto_0
    return-object v0

    .line 268
    :cond_1
    const-string/jumbo v1, "android.speech.extras.VOICE_SEARCH_RESULT_STRINGS"

    invoke-virtual {p0, v1}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    .line 273
    if-nez v1, :cond_2

    sget-boolean v2, Lcom/google/android/apps/chrome/IntentHandler;->sTestIntentsEnabled:Z

    if-eqz v2, :cond_2

    .line 274
    const-string/jumbo v2, "android.speech.extras.VOICE_SEARCH_RESULT_STRINGS"

    invoke-virtual {p0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 276
    if-eqz v2, :cond_2

    .line 277
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 278
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 281
    :cond_2
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-eqz v2, :cond_0

    .line 282
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 283
    invoke-static {v0}, Lorg/chromium/chrome/browser/omnibox/AutocompleteController;->nativeQualifyPartialURLQuery(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 284
    if-nez v1, :cond_4

    .line 285
    const-string/jumbo v1, "android.speech.extras.VOICE_SEARCH_RESULT_URLS"

    invoke-virtual {p0, v1}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    .line 287
    if-eqz v1, :cond_3

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_3

    .line 288
    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0

    .line 290
    :cond_3
    invoke-static {}, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;->getInstance()Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;

    move-result-object v1

    invoke-virtual {v1, v0}, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;->getUrlForVoiceSearchQuery(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_4
    move-object v0, v1

    goto :goto_0
.end method

.method private static isDeviceProvisioned(Landroid/content/Context;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 478
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x11

    if-ge v2, v3, :cond_1

    .line 481
    :cond_0
    :goto_0
    return v0

    .line 479
    :cond_1
    if-eqz p0, :cond_0

    .line 480
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 481
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v3, "device_provisioned"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public static isGoogleChromeScheme(Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 629
    if-nez p0, :cond_1

    .line 631
    :cond_0
    :goto_0
    return v0

    .line 630
    :cond_1
    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    .line 631
    if-eqz v1, :cond_0

    const-string/jumbo v2, "googlechrome"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static isIntentChromeOrFirstParty(Landroid/content/Intent;Landroid/content/Context;)Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 446
    if-nez p0, :cond_0

    move v0, v1

    .line 473
    :goto_0
    return v0

    .line 453
    :cond_0
    :try_start_0
    const-string/jumbo v0, "trusted_application_code_extra"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/app/PendingIntent;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 458
    if-nez v0, :cond_1

    move v0, v1

    goto :goto_0

    .line 455
    :catch_0
    move-exception v0

    move v0, v1

    goto :goto_0

    .line 461
    :cond_1
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    .line 462
    invoke-static {v3}, Lcom/google/android/apps/chrome/IntentHandler;->getAuthenticationToken(Landroid/content/Context;)Landroid/app/PendingIntent;

    move-result-object v3

    .line 466
    invoke-virtual {v3, v0}, Landroid/app/PendingIntent;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    move v0, v2

    .line 467
    goto :goto_0

    .line 469
    :cond_2
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-static {v0}, Lorg/chromium/base/ApiCompatibilityUtils;->getCreatorPackage(Landroid/app/PendingIntent;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/google/android/apps/chrome/externalauth/ExternalAuthUtils;->checkIsGoogleSigned(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v2

    .line 471
    goto :goto_0

    :cond_3
    move v0, v1

    .line 473
    goto :goto_0
.end method

.method private isIntentUserVisible(Landroid/content/Context;)Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 488
    iget-object v0, p0, Lcom/google/android/apps/chrome/IntentHandler;->mKeyguardManager:Landroid/app/KeyguardManager;

    if-nez v0, :cond_0

    .line 489
    const-string/jumbo v0, "keyguard"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/KeyguardManager;

    iput-object v0, p0, Lcom/google/android/apps/chrome/IntentHandler;->mKeyguardManager:Landroid/app/KeyguardManager;

    .line 492
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/IntentHandler;->mPowerManager:Landroid/os/PowerManager;

    if-nez v0, :cond_1

    .line 493
    const-string/jumbo v0, "power"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    iput-object v0, p0, Lcom/google/android/apps/chrome/IntentHandler;->mPowerManager:Landroid/os/PowerManager;

    .line 496
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/IntentHandler;->mPowerManager:Landroid/os/PowerManager;

    invoke-static {v0}, Lcom/google/android/apps/chrome/utilities/UnreleasedApiCompatibilityUtils;->isInteractive(Landroid/os/PowerManager;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 497
    :goto_0
    return v0

    :cond_2
    invoke-static {p1}, Lcom/google/android/apps/chrome/IntentHandler;->isDeviceProvisioned(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/chrome/IntentHandler;->mKeyguardManager:Landroid/app/KeyguardManager;

    invoke-virtual {v0}, Landroid/app/KeyguardManager;->inKeyguardRestrictedInputMode()Z

    move-result v0

    if-nez v0, :cond_4

    :cond_3
    const/4 v0, 0x1

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method private isInvalidScheme(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 532
    if-eqz p1, :cond_1

    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p1, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "javascript"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p1, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "jar"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isJavascriptSchemeOrInvalidUrl(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 574
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/IntentHandler;->getSanitizedUrlScheme(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 575
    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/IntentHandler;->isInvalidScheme(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private recordExternalIntentSourceUMA(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 224
    iget-object v0, p0, Lcom/google/android/apps/chrome/IntentHandler;->mPackageName:Ljava/lang/String;

    invoke-static {v0, p1}, Lcom/google/android/apps/chrome/IntentHandler;->determineExternalIntentSource(Ljava/lang/String;Landroid/content/Intent;)Lcom/google/android/apps/chrome/IntentHandler$ExternalAppId;

    move-result-object v0

    .line 225
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/IntentHandler$ExternalAppId;->ordinal()I

    move-result v0

    sget-object v1, Lcom/google/android/apps/chrome/IntentHandler$ExternalAppId;->INDEX_BOUNDARY:Lcom/google/android/apps/chrome/IntentHandler$ExternalAppId;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/IntentHandler$ExternalAppId;->ordinal()I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->recordExternalIntentSource(II)V

    .line 227
    return-void
.end method

.method static setTestIntentsEnabled(Z)V
    .locals 0

    .prologue
    .line 191
    sput-boolean p0, Lcom/google/android/apps/chrome/IntentHandler;->sTestIntentsEnabled:Z

    .line 192
    return-void
.end method

.method public static startActivityForTrustedIntent(Landroid/content/Intent;Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 327
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p0}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    .line 328
    invoke-static {v0, p1}, Lcom/google/android/apps/chrome/IntentHandler;->addTrustedIntentExtras(Landroid/content/Intent;Landroid/content/Context;)V

    .line 331
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 332
    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 333
    return-void
.end method

.method private static willChromeHandleIntent(Landroid/content/Intent;Landroid/content/Context;)Z
    .locals 2

    .prologue
    .line 361
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const/high16 v1, 0x10000

    invoke-virtual {v0, p0, v1}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v0

    .line 363
    if-eqz v0, :cond_0

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method handleWebSearchIntent(Landroid/content/Intent;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 297
    if-nez p1, :cond_1

    .line 309
    :cond_0
    :goto_0
    return v0

    .line 299
    :cond_1
    const/4 v1, 0x0

    .line 300
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    .line 301
    const-string/jumbo v3, "android.intent.action.SEARCH"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string/jumbo v3, "android.intent.action.MEDIA_SEARCH"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 303
    :cond_2
    const-string/jumbo v1, "query"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 306
    :cond_3
    if-eqz v1, :cond_0

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 308
    iget-object v0, p0, Lcom/google/android/apps/chrome/IntentHandler;->mDelegate:Lcom/google/android/apps/chrome/IntentHandler$IntentHandlerDelegate;

    invoke-interface {v0, v1}, Lcom/google/android/apps/chrome/IntentHandler$IntentHandlerDelegate;->processWebSearchIntent(Ljava/lang/String;)V

    .line 309
    const/4 v0, 0x1

    goto :goto_0
.end method

.method intentHasValidUrl(Landroid/content/Intent;)Z
    .locals 1

    .prologue
    .line 430
    invoke-static {p1}, Lcom/google/android/apps/chrome/IntentHandler;->getUrlFromIntent(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v0

    .line 433
    if-eqz v0, :cond_0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/IntentHandler;->isJavascriptSchemeOrInvalidUrl(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 434
    const/4 v0, 0x0

    .line 436
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method onNewIntent(Landroid/content/Intent;)Z
    .locals 8

    .prologue
    const/4 v2, -0x1

    .line 236
    sget-boolean v0, Lcom/google/android/apps/chrome/IntentHandler;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/IntentHandler;->intentHasValidUrl(Landroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 237
    :cond_0
    invoke-static {p1}, Lcom/google/android/apps/chrome/IntentHandler;->getUrlFromIntent(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v1

    .line 239
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/IntentHandler;->getTabOpenType(Landroid/content/Intent;)Lcom/google/android/apps/chrome/IntentHandler$TabOpenType;

    move-result-object v3

    .line 240
    sget-object v0, Lcom/google/android/apps/chrome/IntentHandler$TabOpenType;->BRING_TAB_TO_FRONT:Lcom/google/android/apps/chrome/IntentHandler$TabOpenType;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/IntentHandler$TabOpenType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v6

    .line 242
    if-nez v1, :cond_1

    if-ne v6, v2, :cond_1

    sget-object v0, Lcom/google/android/apps/chrome/IntentHandler$TabOpenType;->OPEN_NEW_INCOGNITO_TAB:Lcom/google/android/apps/chrome/IntentHandler$TabOpenType;

    if-eq v3, v0, :cond_1

    .line 244
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/IntentHandler;->handleWebSearchIntent(Landroid/content/Intent;)Z

    move-result v0

    .line 257
    :goto_0
    return v0

    .line 249
    :cond_1
    const-string/jumbo v0, "Chrome_SnapshotID"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 251
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/IntentHandler;->getExtraHeadersFromIntent(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v2

    .line 254
    iget-object v0, p0, Lcom/google/android/apps/chrome/IntentHandler;->mDelegate:Lcom/google/android/apps/chrome/IntentHandler$IntentHandlerDelegate;

    const-string/jumbo v5, "com.android.browser.application_id"

    invoke-virtual {p1, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    move-object v7, p1

    invoke-interface/range {v0 .. v7}, Lcom/google/android/apps/chrome/IntentHandler$IntentHandlerDelegate;->processUrlViewIntent(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/chrome/IntentHandler$TabOpenType;Ljava/lang/String;Ljava/lang/String;ILandroid/content/Intent;)V

    .line 256
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/IntentHandler;->recordExternalIntentSourceUMA(Landroid/content/Intent;)V

    .line 257
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public shouldIgnoreIntent(Landroid/content/Context;Landroid/content/Intent;)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 401
    :try_start_0
    invoke-virtual {p0, p2}, Lcom/google/android/apps/chrome/IntentHandler;->intentHasValidUrl(Landroid/content/Intent;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 424
    :cond_0
    :goto_0
    return v0

    .line 406
    :cond_1
    invoke-static {p2, p1}, Lcom/google/android/apps/chrome/IntentHandler;->isIntentChromeOrFirstParty(Landroid/content/Intent;Landroid/content/Context;)Z

    move-result v2

    .line 407
    if-nez v2, :cond_2

    const-string/jumbo v3, "com.google.android.apps.chrome.EXTRA_OPEN_NEW_INCOGNITO_TAB"

    const/4 v4, 0x0

    invoke-virtual {p2, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    if-nez v3, :cond_0

    .line 414
    :cond_2
    invoke-static {p2}, Lcom/google/android/apps/chrome/IntentHandler;->getUrlFromIntent(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v3

    .line 415
    if-nez v3, :cond_3

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "android.intent.action.MAIN"

    if-ne v3, v4, :cond_3

    move v0, v1

    .line 416
    goto :goto_0

    .line 421
    :cond_3
    if-nez v2, :cond_4

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/IntentHandler;->isIntentUserVisible(Landroid/content/Context;)Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    if-eqz v2, :cond_5

    :cond_4
    move v2, v0

    .line 422
    :goto_1
    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_5
    move v2, v1

    .line 421
    goto :goto_1

    .line 424
    :catch_0
    move-exception v1

    goto :goto_0
.end method
