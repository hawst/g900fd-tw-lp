.class Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$4;
.super Lcom/google/android/apps/chrome/toolbar/KeyboardNavigationListener;
.source "ToolbarTablet.java"


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;)V
    .locals 0

    .prologue
    .line 148
    iput-object p1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$4;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/toolbar/KeyboardNavigationListener;-><init>()V

    return-void
.end method


# virtual methods
.method public getNextFocusBackward()Landroid/view/View;
    .locals 2

    .prologue
    .line 156
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$4;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mForwardButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->access$100(Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ImageButton;->isFocusable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 157
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$4;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mForwardButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->access$100(Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;)Landroid/widget/ImageButton;

    move-result-object v0

    .line 163
    :goto_0
    return-object v0

    .line 158
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$4;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mBackButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->access$000(Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ImageButton;->isFocusable()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 159
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$4;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mBackButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->access$000(Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;)Landroid/widget/ImageButton;

    move-result-object v0

    goto :goto_0

    .line 160
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$4;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mHomeButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->access$200(Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getVisibility()I

    move-result v0

    if-nez v0, :cond_2

    .line 161
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$4;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;

    sget v1, Lcom/google/android/apps/chrome/R$id;->home_button:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 163
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$4;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;

    sget v1, Lcom/google/android/apps/chrome/R$id;->menu_button:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public getNextFocusForward()Landroid/view/View;
    .locals 2

    .prologue
    .line 151
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$4;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;

    sget v1, Lcom/google/android/apps/chrome/R$id;->url_bar:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
