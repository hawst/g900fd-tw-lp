.class public Lcom/google/android/apps/chrome/ChromeNotificationCenter;
.super Ljava/lang/Object;
.source "ChromeNotificationCenter.java"


# static fields
.field public static final ACCOUNT_ADDED:I = 0x32
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final ACTIVITY_HASHCODE_DATA_NAME:Ljava/lang/String; = "activity_hashcode"

.field public static final ANIMATION_FINISHED:I = 0x29
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final APP_ON_DESTORY:I = 0x27
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final AUTO_LOGIN_DISABLED:I = 0x3a
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final CONTEXTUAL_SEARCH_DISABLED:I = 0x4d
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final CONTEXTUAL_SEARCH_DISMISSED:I = 0x4c
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final CONTEXTUAL_SEARCH_ENABLED:I = 0x4e
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final DEFAULT_SEARCH_ENGINE_CHANGED:I = 0x2a
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final DEFERRED_STARTUP:I = 0x26
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final FAVICON_CHANGED:I = 0x14
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final FIND_TOOLBAR_HIDE:I = 0x2d
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final FIND_TOOLBAR_SHOW:I = 0x2c
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final FIND_TOOLBAR_SHOWN:I = 0x3b
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final FIRST_DRAW_COMPLETE:I = 0x40
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final HOME_BUTTON_PREFERENCE_CHANGED:I = 0x46
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final INCOGNITO_MODE_UNAVAILBLE:I = 0x4a
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final LOAD_PROGRESS_CHANGED:I = 0xb
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final LOAD_STARTED:I = 0x1a
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final LOAD_STOPPED:I = 0x1b
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final NATIVE_LIBRARY_READY:I = 0x25
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final NEW_TAB_PAGE_READY:I = 0x20
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final OMNIBOX_FOCUSED:I = 0x43
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final OMNIBOX_FULLY_FUNCTIONAL:I = 0x42
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final OMNIBOX_INTERACTIVE:I = 0x41
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final ON_TAP_CONTEXTUAL_SEARCH_CONTROL:I = 0x4b
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final OUT_OF_MEMORY:I = 0x31
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final OVERVIEW_MODE_HIDE_FINISHED:I = 0x1
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final OVERVIEW_MODE_HIDE_START:I = 0xf
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final OVERVIEW_MODE_SHOW_FINISHED:I = 0xd
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final OVERVIEW_MODE_SHOW_START:I = 0x0
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final PAGE_LOAD_FAILED:I = 0x1c
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final PAGE_LOAD_FINISHED:I = 0x9
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final PAGE_LOAD_STARTED:I = 0x8
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final PAGE_TITLE_CHANGED:I = 0xa
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final PAGE_URL_CHANGED:I = 0x19
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final PARTNER_BROWSER_PROVIDER_INITIALIZED:I = 0x48
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final PRERENDERED_PAGE_SHOWN:I = 0x23
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final READER_MODE_STATUS_CHANGED:I = 0x4f
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final SIDE_SWIPE_STARTED:I = 0x47
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final SIDE_SWITCH_MODE_END:I = 0x12
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final SIDE_SWITCH_MODE_START:I = 0x11
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final TABS_CLOSE_ALL_REQUEST:I = 0x2b
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final TABS_STATE_LOADED:I = 0x10
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final TABS_VIEW_HIDDEN:I = 0x35
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final TABS_VIEW_SHOWN:I = 0x34
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final TAB_BACKGROUND_COLOR_CHANGED:I = 0x3f
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final TAB_CANCEL_PENDING_CLOSURE:I = 0x45
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final TAB_CLOSED:I = 0x5
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final TAB_CLOSING:I = 0x24
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final TAB_COMMIT_PENDING_CLOSURE:I = 0x49
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final TAB_CONTENT_VIEW_CHANGED:I = 0x16
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final TAB_CONTEXTUAL_ACTION_BAR_STATE_CHANGED:I = 0x22
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final TAB_CRASHED:I = 0x6
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final TAB_CREATED:I = 0x2
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final TAB_CREATING:I = 0x30
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final TAB_MODEL_SELECTED:I = 0xc
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final TAB_MOVED:I = 0x4
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final TAB_PENDING_CLOSURE:I = 0x44
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final TAB_PRE_SELECTED:I = 0x15
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final TAB_SELECTED:I = 0x3
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field public static final THUMBNAIL_UPDATED:I = 0x3d
    .annotation runtime Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;
    .end annotation
.end field

.field private static sNotificationMap:Ljava/util/Map;


# instance fields
.field private mActivityBroadcasters:Ljava/util/Map;

.field private final mApplicationBroadcaster:Lcom/google/android/apps/chrome/third_party/Broadcaster;

.field private mApplicationBroadcasterDestroyed:Z

.field private mNotificationLogger:Landroid/os/Handler;


# direct methods
.method protected constructor <init>()V
    .locals 1

    .prologue
    .line 761
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 759
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->mActivityBroadcasters:Ljava/util/Map;

    .line 762
    new-instance v0, Lcom/google/android/apps/chrome/ChromeNotificationCenter$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter$2;-><init>(Lcom/google/android/apps/chrome/ChromeNotificationCenter;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->mApplicationBroadcaster:Lcom/google/android/apps/chrome/third_party/Broadcaster;

    .line 769
    return-void
.end method

.method static synthetic access$300(Landroid/content/Context;)Lcom/google/android/apps/chrome/third_party/Broadcaster;
    .locals 1

    .prologue
    .line 35
    invoke-static {p0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->getBroadcaster(Landroid/content/Context;)Lcom/google/android/apps/chrome/third_party/Broadcaster;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/chrome/ChromeNotificationCenter;)Z
    .locals 1

    .prologue
    .line 35
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->mApplicationBroadcasterDestroyed:Z

    return v0
.end method

.method static synthetic access$600()Ljava/util/Map;
    .locals 1

    .prologue
    .line 35
    invoke-static {}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->getNotificationTypeMap()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$700(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 35
    invoke-static {p0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->isApplicationContext(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method public static broadcastImmediateNotification(I)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 660
    invoke-static {v0, p0, v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastImmediateNotification(Landroid/content/Context;ILandroid/os/Bundle;)V

    .line 661
    return-void
.end method

.method public static broadcastImmediateNotification(ILandroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 631
    const/4 v0, 0x0

    invoke-static {v0, p0, p1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastImmediateNotification(Landroid/content/Context;ILandroid/os/Bundle;)V

    .line 632
    return-void
.end method

.method public static broadcastImmediateNotification(Landroid/content/Context;I)V
    .locals 1

    .prologue
    .line 673
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastImmediateNotification(Landroid/content/Context;ILandroid/os/Bundle;)V

    .line 674
    return-void
.end method

.method public static broadcastImmediateNotification(Landroid/content/Context;ILandroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 645
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 646
    iput p1, v0, Landroid/os/Message;->what:I

    .line 647
    if-eqz p2, :cond_0

    invoke-virtual {v0, p2}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 648
    :cond_0
    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastMessageInternal(Landroid/content/Context;Landroid/os/Message;Z)V

    .line 649
    return-void
.end method

.method private static broadcastMessageInternal(Landroid/content/Context;Landroid/os/Message;Z)V
    .locals 3

    .prologue
    .line 677
    instance-of v0, p0, Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 678
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "activity_hashcode"

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 682
    :cond_0
    invoke-static {p0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->isApplicationContext(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 683
    invoke-static {}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->getInstance()Lcom/google/android/apps/chrome/ChromeNotificationCenter;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->mApplicationBroadcaster:Lcom/google/android/apps/chrome/third_party/Broadcaster;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/chrome/third_party/Broadcaster;->broadcast(Landroid/os/Message;Z)V

    .line 685
    invoke-static {}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->getInstance()Lcom/google/android/apps/chrome/ChromeNotificationCenter;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->mActivityBroadcasters:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/third_party/Broadcaster;

    .line 686
    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/chrome/third_party/Broadcaster;->broadcast(Landroid/os/Message;Z)V

    goto :goto_0

    .line 689
    :cond_1
    invoke-static {p0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->getBroadcaster(Landroid/content/Context;)Lcom/google/android/apps/chrome/third_party/Broadcaster;

    move-result-object v0

    .line 690
    if-eqz v0, :cond_2

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/chrome/third_party/Broadcaster;->broadcast(Landroid/os/Message;Z)V

    .line 692
    :cond_2
    iget v0, p1, Landroid/os/Message;->what:I

    const/16 v1, 0x27

    if-eq v0, v1, :cond_3

    .line 693
    invoke-static {}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->getInstance()Lcom/google/android/apps/chrome/ChromeNotificationCenter;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->mApplicationBroadcaster:Lcom/google/android/apps/chrome/third_party/Broadcaster;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/chrome/third_party/Broadcaster;->broadcast(Landroid/os/Message;Z)V

    .line 697
    :cond_3
    invoke-virtual {p1}, Landroid/os/Message;->recycle()V

    .line 698
    return-void
.end method

.method public static broadcastNotification(I)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 607
    invoke-static {v0, p0, v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastNotification(Landroid/content/Context;ILandroid/os/Bundle;)V

    .line 608
    return-void
.end method

.method public static broadcastNotification(Landroid/content/Context;I)V
    .locals 1

    .prologue
    .line 617
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastNotification(Landroid/content/Context;ILandroid/os/Bundle;)V

    .line 618
    return-void
.end method

.method public static broadcastNotification(Landroid/content/Context;ILandroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 594
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 595
    iput p1, v0, Landroid/os/Message;->what:I

    .line 596
    if-eqz p2, :cond_0

    invoke-virtual {v0, p2}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 598
    :cond_0
    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastMessageInternal(Landroid/content/Context;Landroid/os/Message;Z)V

    .line 599
    return-void
.end method

.method protected static disable(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 740
    invoke-static {p0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->isApplicationContext(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 741
    invoke-static {}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->getInstance()Lcom/google/android/apps/chrome/ChromeNotificationCenter;

    move-result-object v0

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->mApplicationBroadcasterDestroyed:Z

    .line 745
    :goto_0
    return-void

    .line 743
    :cond_0
    invoke-static {}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->getInstance()Lcom/google/android/apps/chrome/ChromeNotificationCenter;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->mActivityBroadcasters:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method protected static enable(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 711
    invoke-static {p0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->isApplicationContext(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 712
    invoke-static {}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->getInstance()Lcom/google/android/apps/chrome/ChromeNotificationCenter;

    move-result-object v0

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->mApplicationBroadcasterDestroyed:Z

    .line 727
    :cond_0
    :goto_0
    return-void

    .line 717
    :cond_1
    invoke-static {p0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->getBroadcaster(Landroid/content/Context;)Lcom/google/android/apps/chrome/third_party/Broadcaster;

    move-result-object v0

    if-nez v0, :cond_0

    .line 719
    new-instance v0, Lcom/google/android/apps/chrome/ChromeNotificationCenter$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter$1;-><init>(Landroid/content/Context;)V

    .line 726
    invoke-static {}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->getInstance()Lcom/google/android/apps/chrome/ChromeNotificationCenter;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->mActivityBroadcasters:Ljava/util/Map;

    invoke-interface {v1, p0, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method private static getBroadcaster(Landroid/content/Context;)Lcom/google/android/apps/chrome/third_party/Broadcaster;
    .locals 1

    .prologue
    .line 772
    invoke-static {p0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->isApplicationContext(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 773
    invoke-static {}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->getInstance()Lcom/google/android/apps/chrome/ChromeNotificationCenter;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->mApplicationBroadcaster:Lcom/google/android/apps/chrome/third_party/Broadcaster;

    .line 775
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->getInstance()Lcom/google/android/apps/chrome/ChromeNotificationCenter;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->mActivityBroadcasters:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/third_party/Broadcaster;

    goto :goto_0
.end method

.method public static getInstance()Lcom/google/android/apps/chrome/ChromeNotificationCenter;
    .locals 1

    .prologue
    .line 461
    # getter for: Lcom/google/android/apps/chrome/ChromeNotificationCenter$LazyHolder;->INSTANCE:Lcom/google/android/apps/chrome/ChromeNotificationCenter;
    invoke-static {}, Lcom/google/android/apps/chrome/ChromeNotificationCenter$LazyHolder;->access$000()Lcom/google/android/apps/chrome/ChromeNotificationCenter;

    move-result-object v0

    return-object v0
.end method

.method private static getNotificationTypeMap()Ljava/util/Map;
    .locals 6

    .prologue
    .line 827
    sget-object v0, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->sNotificationMap:Ljava/util/Map;

    if-nez v0, :cond_1

    .line 828
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    sput-object v0, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->sNotificationMap:Ljava/util/Map;

    .line 829
    const-class v0, Lcom/google/android/apps/chrome/ChromeNotificationCenter;

    invoke-virtual {v0}, Ljava/lang/Class;->getDeclaredFields()[Ljava/lang/reflect/Field;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 830
    const-class v4, Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationType;

    invoke-virtual {v3, v4}, Ljava/lang/reflect/Field;->isAnnotationPresent(Ljava/lang/Class;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 833
    const/4 v4, 0x0

    :try_start_0
    invoke-virtual {v3, v4}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    .line 837
    sget-object v5, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->sNotificationMap:Ljava/util/Map;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v5, v4, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 838
    new-instance v0, Ljava/lang/RuntimeException;

    const-string/jumbo v1, "Duplicate ChromeNotificationType defined"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 834
    :catch_0
    move-exception v0

    .line 835
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 829
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 843
    :cond_1
    sget-object v0, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->sNotificationMap:Ljava/util/Map;

    return-object v0
.end method

.method static getNotificationTypes()[Ljava/lang/Integer;
    .locals 2

    .prologue
    .line 751
    invoke-static {}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->getNotificationTypeMap()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Integer;

    invoke-interface {v0, v1}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Integer;

    return-object v0
.end method

.method private static isApplicationContext(Landroid/content/Context;)Z
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 812
    if-eqz p0, :cond_0

    invoke-static {}, Lorg/chromium/base/ApplicationStatus;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    if-ne p0, v0, :cond_1

    :cond_0
    move v0, v2

    .line 823
    :goto_0
    return v0

    .line 817
    :cond_1
    instance-of v0, p0, Landroid/content/ContextWrapper;

    if-eqz v0, :cond_2

    move-object v0, p0

    check-cast v0, Landroid/content/ContextWrapper;

    invoke-virtual {v0}, Landroid/content/ContextWrapper;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    if-nez v0, :cond_2

    move v0, v1

    .line 820
    goto :goto_0

    .line 823
    :cond_2
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    if-ne p0, v0, :cond_3

    move v0, v2

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public static registerForNotification(ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V
    .locals 1

    .prologue
    .line 474
    const/4 v0, 0x0

    invoke-static {v0, p0, p1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->registerForNotification(Landroid/content/Context;ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    .line 475
    return-void
.end method

.method public static registerForNotification(Landroid/content/Context;ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V
    .locals 2

    .prologue
    .line 489
    invoke-static {p0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->getBroadcaster(Landroid/content/Context;)Lcom/google/android/apps/chrome/third_party/Broadcaster;

    move-result-object v0

    .line 490
    if-nez v0, :cond_0

    .line 494
    :goto_0
    return-void

    .line 492
    :cond_0
    # invokes: Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;->setContext(Landroid/content/Context;)V
    invoke-static {p2, p0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;->access$100(Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;Landroid/content/Context;)V

    .line 493
    # invokes: Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;->getHandler()Landroid/os/Handler;
    invoke-static {p2}, Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;->access$200(Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v0, p1, v1, p1}, Lcom/google/android/apps/chrome/third_party/Broadcaster;->request(ILandroid/os/Handler;I)V

    goto :goto_0
.end method

.method public static registerForNotifications(Landroid/content/Context;[ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V
    .locals 5

    .prologue
    .line 519
    invoke-static {p0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->getBroadcaster(Landroid/content/Context;)Lcom/google/android/apps/chrome/third_party/Broadcaster;

    move-result-object v1

    .line 520
    if-nez v1, :cond_1

    .line 526
    :cond_0
    return-void

    .line 522
    :cond_1
    # invokes: Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;->setContext(Landroid/content/Context;)V
    invoke-static {p2, p0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;->access$100(Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;Landroid/content/Context;)V

    .line 523
    array-length v2, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget v3, p1, v0

    .line 524
    # invokes: Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;->getHandler()Landroid/os/Handler;
    invoke-static {p2}, Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;->access$200(Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)Landroid/os/Handler;

    move-result-object v4

    invoke-virtual {v1, v3, v4, v3}, Lcom/google/android/apps/chrome/third_party/Broadcaster;->request(ILandroid/os/Handler;I)V

    .line 523
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static registerForNotifications([ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V
    .locals 1

    .prologue
    .line 505
    const/4 v0, 0x0

    invoke-static {v0, p0, p1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->registerForNotifications(Landroid/content/Context;[ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    .line 506
    return-void
.end method

.method public static unregisterForNotification(ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V
    .locals 1

    .prologue
    .line 537
    const/4 v0, 0x0

    invoke-static {v0, p0, p1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->unregisterForNotification(Landroid/content/Context;ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    .line 538
    return-void
.end method

.method public static unregisterForNotification(Landroid/content/Context;ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V
    .locals 2

    .prologue
    .line 549
    invoke-static {p0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->getBroadcaster(Landroid/content/Context;)Lcom/google/android/apps/chrome/third_party/Broadcaster;

    move-result-object v0

    .line 550
    if-nez v0, :cond_0

    .line 554
    :goto_0
    return-void

    .line 552
    :cond_0
    # invokes: Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;->getHandler()Landroid/os/Handler;
    invoke-static {p2}, Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;->access$200(Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v0, p1, v1, p1}, Lcom/google/android/apps/chrome/third_party/Broadcaster;->cancelRequest(ILandroid/os/Handler;I)V

    .line 553
    # invokes: Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;->getHandler()Landroid/os/Handler;
    invoke-static {p2}, Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;->access$200(Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_0
.end method

.method public static unregisterForNotifications(Landroid/content/Context;[ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V
    .locals 5

    .prologue
    .line 577
    invoke-static {p0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->getBroadcaster(Landroid/content/Context;)Lcom/google/android/apps/chrome/third_party/Broadcaster;

    move-result-object v1

    .line 578
    if-nez v1, :cond_1

    .line 584
    :cond_0
    return-void

    .line 580
    :cond_1
    array-length v2, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget v3, p1, v0

    .line 581
    # invokes: Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;->getHandler()Landroid/os/Handler;
    invoke-static {p2}, Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;->access$200(Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)Landroid/os/Handler;

    move-result-object v4

    invoke-virtual {v1, v3, v4, v3}, Lcom/google/android/apps/chrome/third_party/Broadcaster;->cancelRequest(ILandroid/os/Handler;I)V

    .line 582
    # invokes: Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;->getHandler()Landroid/os/Handler;
    invoke-static {p2}, Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;->access$200(Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)Landroid/os/Handler;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 580
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static unregisterForNotifications([ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V
    .locals 1

    .prologue
    .line 565
    const/4 v0, 0x0

    invoke-static {v0, p0, p1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->unregisterForNotifications(Landroid/content/Context;[ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    .line 566
    return-void
.end method


# virtual methods
.method public enableLogging()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 790
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->mNotificationLogger:Landroid/os/Handler;

    if-nez v0, :cond_0

    .line 791
    new-instance v0, Lcom/google/android/apps/chrome/ChromeNotificationCenter$NotificationLogger;

    invoke-direct {v0, v6}, Lcom/google/android/apps/chrome/ChromeNotificationCenter$NotificationLogger;-><init>(Lcom/google/android/apps/chrome/ChromeNotificationCenter$1;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->mNotificationLogger:Landroid/os/Handler;

    .line 792
    invoke-static {}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->getNotificationTypes()[Ljava/lang/Integer;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 793
    invoke-static {v6}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->getBroadcaster(Landroid/content/Context;)Lcom/google/android/apps/chrome/third_party/Broadcaster;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->mNotificationLogger:Landroid/os/Handler;

    invoke-virtual {v4, v3, v5, v3}, Lcom/google/android/apps/chrome/third_party/Broadcaster;->request(ILandroid/os/Handler;I)V

    .line 792
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 796
    :cond_0
    return-void
.end method

.method protected getBroadcasterForTest(Landroid/content/Context;)Lcom/google/android/apps/chrome/third_party/Broadcaster;
    .locals 1

    .prologue
    .line 783
    invoke-static {p1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->getBroadcaster(Landroid/content/Context;)Lcom/google/android/apps/chrome/third_party/Broadcaster;

    move-result-object v0

    return-object v0
.end method
