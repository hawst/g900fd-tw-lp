.class public Lcom/google/android/apps/chrome/infobar/AutoLoginProcessorImpl;
.super Ljava/lang/Object;
.source "AutoLoginProcessorImpl.java"

# interfaces
.implements Lorg/chromium/chrome/browser/infobar/AutoLoginProcessor;


# instance fields
.field private final mSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;


# direct methods
.method public constructor <init>(Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/google/android/apps/chrome/infobar/AutoLoginProcessorImpl;->mSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    .line 21
    return-void
.end method


# virtual methods
.method public processAutoLoginResult(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V
    .locals 5

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/apps/chrome/infobar/AutoLoginProcessorImpl;->mSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->getModels()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/tabmodel/TabModel;

    .line 35
    const/4 v1, 0x0

    :goto_0
    if-eqz v0, :cond_0

    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->getCount()I

    move-result v3

    if-ge v1, v3, :cond_0

    .line 36
    invoke-interface {v0, v1}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->getTabAt(I)Lorg/chromium/chrome/browser/Tab;

    move-result-object v3

    .line 37
    if-eqz v3, :cond_1

    invoke-virtual {v3}, Lorg/chromium/chrome/browser/Tab;->getInfoBarContainer()Lorg/chromium/chrome/browser/infobar/InfoBarContainer;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 38
    invoke-virtual {v3}, Lorg/chromium/chrome/browser/Tab;->getInfoBarContainer()Lorg/chromium/chrome/browser/infobar/InfoBarContainer;

    move-result-object v3

    invoke-virtual {v3, p1, p2, p3, p4}, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->processAutoLogin(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    .line 35
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 43
    :cond_2
    return-void
.end method
