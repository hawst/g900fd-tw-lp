.class Lcom/google/android/apps/chrome/omnibox/SuggestionView$3;
.super Ljava/lang/Object;
.source "SuggestionView.java"

# interfaces
.implements Lorg/chromium/chrome/browser/omnibox/AnswersImage$AnswersImageObserver;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/omnibox/SuggestionView;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/omnibox/SuggestionView;)V
    .locals 0

    .prologue
    .line 536
    iput-object p1, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$3;->this$0:Lcom/google/android/apps/chrome/omnibox/SuggestionView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnswersImageChanged(Landroid/graphics/Bitmap;)V
    .locals 1

    .prologue
    .line 539
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$3;->this$0:Lcom/google/android/apps/chrome/omnibox/SuggestionView;

    # getter for: Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mContentsView:Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->access$1000(Lcom/google/android/apps/chrome/omnibox/SuggestionView;)Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;

    move-result-object v0

    # getter for: Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mAnswerImage:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->access$300(Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 540
    return-void
.end method
