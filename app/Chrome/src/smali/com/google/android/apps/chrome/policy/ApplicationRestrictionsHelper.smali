.class public Lcom/google/android/apps/chrome/policy/ApplicationRestrictionsHelper;
.super Ljava/lang/Object;
.source "ApplicationRestrictionsHelper.java"


# instance fields
.field private final mUserManager:Landroid/os/UserManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/policy/ApplicationRestrictionsHelper;->isSupported()Z

    move-result v0

    if-nez v0, :cond_0

    .line 26
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/policy/ApplicationRestrictionsHelper;->mUserManager:Landroid/os/UserManager;

    .line 30
    :goto_0
    return-void

    .line 29
    :cond_0
    const-string/jumbo v0, "user"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/UserManager;

    iput-object v0, p0, Lcom/google/android/apps/chrome/policy/ApplicationRestrictionsHelper;->mUserManager:Landroid/os/UserManager;

    goto :goto_0
.end method


# virtual methods
.method public getRestrictions(Ljava/lang/String;)Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/apps/chrome/policy/ApplicationRestrictionsHelper;->mUserManager:Landroid/os/UserManager;

    invoke-virtual {v0, p1}, Landroid/os/UserManager;->getApplicationRestrictions(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method public isSupported()Z
    .locals 2

    .prologue
    .line 36
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
