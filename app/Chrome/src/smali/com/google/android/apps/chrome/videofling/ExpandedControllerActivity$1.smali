.class Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity$1;
.super Landroid/support/v4/media/l;
.source "ExpandedControllerActivity.java"


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;)V
    .locals 0

    .prologue
    .line 57
    iput-object p1, p0, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity$1;->this$0:Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;

    invoke-direct {p0}, Landroid/support/v4/media/l;-><init>()V

    return-void
.end method


# virtual methods
.method public onGetCurrentPosition()J
    .locals 2

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity$1;->this$0:Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;

    # getter for: Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;->mRemoteMediaPlayerController:Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;
    invoke-static {v0}, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;->access$000(Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;)Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->getPosition()I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method

.method public onGetDuration()J
    .locals 2

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity$1;->this$0:Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;

    # getter for: Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;->mRemoteMediaPlayerController:Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;
    invoke-static {v0}, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;->access$000(Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;)Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->getDuration()I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method

.method public onGetTransportControlFlags()I
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity$1;->this$0:Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;

    # getter for: Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;->mRemoteMediaPlayerController:Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;
    invoke-static {v0}, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;->access$000(Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;)Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 99
    const/16 v0, 0x52

    .line 103
    :goto_0
    return v0

    .line 101
    :cond_0
    const/16 v0, 0x46

    goto :goto_0
.end method

.method public onIsPlaying()Z
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity$1;->this$0:Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;

    # getter for: Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;->mRemoteMediaPlayerController:Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;
    invoke-static {v0}, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;->access$000(Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;)Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->isPlaying()Z

    move-result v0

    return v0
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity$1;->this$0:Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;

    # getter for: Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;->mRemoteMediaPlayerController:Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;
    invoke-static {v0}, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;->access$000(Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;)Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->pause()V

    .line 72
    return-void
.end method

.method public onSeekTo(J)V
    .locals 3

    .prologue
    .line 86
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity$1;->this$0:Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;

    # getter for: Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;->mRemoteMediaPlayerController:Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;
    invoke-static {v0}, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;->access$000(Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;)Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;

    move-result-object v0

    long-to-int v1, p1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->seekTo(I)V

    .line 87
    return-void
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity$1;->this$0:Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;

    # getter for: Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;->mRemoteMediaPlayerController:Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;
    invoke-static {v0}, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;->access$000(Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;)Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->resume()V

    .line 61
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 65
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity$1;->onPause()V

    .line 66
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity$1;->this$0:Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;

    # getter for: Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;->mMediaRouteController:Lcom/google/android/apps/chrome/videofling/MediaRouteController;
    invoke-static {v0}, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;->access$100(Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;)Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->release()V

    .line 67
    return-void
.end method
