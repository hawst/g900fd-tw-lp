.class Lcom/google/android/apps/chrome/autofill/AutofillDataProvider$3;
.super Ljava/lang/Object;
.source "AutofillDataProvider.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/autofill/AutofillDataProvider;

.field final synthetic val$guidOrNull:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/autofill/AutofillDataProvider;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 318
    iput-object p1, p0, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider$3;->this$0:Lcom/google/android/apps/chrome/autofill/AutofillDataProvider;

    iput-object p2, p0, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider$3;->val$guidOrNull:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 318
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider$3;->call()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public call()Ljava/util/List;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 321
    invoke-static {}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager;->isAutofillEnabled()Z

    move-result v1

    if-nez v1, :cond_1

    .line 333
    :cond_0
    :goto_0
    return-object v0

    .line 323
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider$3;->val$guidOrNull:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 324
    invoke-static {}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager;->getInstance()Lorg/chromium/chrome/browser/autofill/PersonalDataManager;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager;->getProfiles()Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 327
    :cond_2
    invoke-static {}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager;->getInstance()Lorg/chromium/chrome/browser/autofill/PersonalDataManager;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider$3;->val$guidOrNull:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager;->getProfile(Ljava/lang/String;)Lorg/chromium/chrome/browser/autofill/PersonalDataManager$AutofillProfile;

    move-result-object v1

    .line 329
    if-eqz v1, :cond_0

    .line 331
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 332
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method
