.class public Lcom/google/android/apps/chrome/ntp/NewTabPage;
.super Ljava/lang/Object;
.source "NewTabPage.java"

# interfaces
.implements Landroid/view/View$OnAttachStateChangeListener;
.implements Lcom/google/android/apps/chrome/compositor/InvalidationAwareThumbnailProvider;
.implements Lorg/chromium/chrome/browser/NativePage;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field static final ID_OPEN_IN_INCOGNITO_TAB:I = 0x1

.field static final ID_OPEN_IN_NEW_TAB:I = 0x0

.field static final ID_REMOVE:I = 0x2

.field private static sMostVisitedSitesForTests:Lorg/chromium/chrome/browser/profiles/MostVisitedSites;


# instance fields
.field private final mActivity:Landroid/app/Activity;

.field private final mBackgroundColor:I

.field private final mConstructedTimeNs:J

.field private mFaviconHelper:Lorg/chromium/chrome/browser/favicon/FaviconHelper;

.field private mIsDestroyed:Z

.field private mLocationBar:Lcom/google/android/apps/chrome/omnibox/LocationBar;

.field private mLogoBridge:Lorg/chromium/chrome/browser/LogoBridge;

.field private mMostVisitedSites:Lorg/chromium/chrome/browser/profiles/MostVisitedSites;

.field private final mNewTabPageManager:Lcom/google/android/apps/chrome/ntp/NewTabPageView$NewTabPageManager;

.field private final mNewTabPageView:Lcom/google/android/apps/chrome/ntp/NewTabPageView;

.field private final mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

.field private mOnLogoClickUrl:Ljava/lang/String;

.field private final mProfile:Lorg/chromium/chrome/browser/profiles/Profile;

.field private mSearchProviderHasLogo:Z

.field private final mTab:Lcom/google/android/apps/chrome/tab/ChromeTab;

.field private final mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

.field private final mTabObserver:Lorg/chromium/chrome/browser/TabObserver;

.field private final mTitle:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 59
    const-class v0, Lcom/google/android/apps/chrome/ntp/NewTabPage;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/ntp/NewTabPage;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/apps/chrome/tab/ChromeTab;Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;)V
    .locals 3

    .prologue
    .line 296
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 121
    new-instance v0, Lcom/google/android/apps/chrome/ntp/NewTabPage$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/ntp/NewTabPage$1;-><init>(Lcom/google/android/apps/chrome/ntp/NewTabPage;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    .line 130
    new-instance v0, Lcom/google/android/apps/chrome/ntp/NewTabPage$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/ntp/NewTabPage$2;-><init>(Lcom/google/android/apps/chrome/ntp/NewTabPage;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage;->mNewTabPageManager:Lcom/google/android/apps/chrome/ntp/NewTabPageView$NewTabPageManager;

    .line 297
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage;->mConstructedTimeNs:J

    .line 299
    iput-object p2, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage;->mTab:Lcom/google/android/apps/chrome/tab/ChromeTab;

    .line 300
    iput-object p1, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage;->mActivity:Landroid/app/Activity;

    .line 301
    iput-object p3, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    .line 302
    invoke-virtual {p2}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getProfile()Lorg/chromium/chrome/browser/profiles/Profile;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage;->mProfile:Lorg/chromium/chrome/browser/profiles/Profile;

    .line 304
    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$string;->button_new_tab:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage;->mTitle:Ljava/lang/String;

    .line 305
    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$color;->new_ntp_bg:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage;->mBackgroundColor:I

    .line 306
    new-instance v0, Lcom/google/android/apps/chrome/ntp/NewTabPage$3;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/ntp/NewTabPage$3;-><init>(Lcom/google/android/apps/chrome/ntp/NewTabPage;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage;->mTabObserver:Lorg/chromium/chrome/browser/TabObserver;

    .line 322
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage;->mTab:Lcom/google/android/apps/chrome/tab/ChromeTab;

    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage;->mTabObserver:Lorg/chromium/chrome/browser/TabObserver;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->addObserver(Lorg/chromium/chrome/browser/TabObserver;)V

    .line 324
    new-instance v0, Lorg/chromium/chrome/browser/favicon/FaviconHelper;

    invoke-direct {v0}, Lorg/chromium/chrome/browser/favicon/FaviconHelper;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage;->mFaviconHelper:Lorg/chromium/chrome/browser/favicon/FaviconHelper;

    .line 325
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage;->mProfile:Lorg/chromium/chrome/browser/profiles/Profile;

    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/NewTabPage;->buildMostVisitedSites(Lorg/chromium/chrome/browser/profiles/Profile;)Lorg/chromium/chrome/browser/profiles/MostVisitedSites;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage;->mMostVisitedSites:Lorg/chromium/chrome/browser/profiles/MostVisitedSites;

    .line 326
    new-instance v0, Lorg/chromium/chrome/browser/LogoBridge;

    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage;->mProfile:Lorg/chromium/chrome/browser/profiles/Profile;

    invoke-direct {v0, v1}, Lorg/chromium/chrome/browser/LogoBridge;-><init>(Lorg/chromium/chrome/browser/profiles/Profile;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage;->mLogoBridge:Lorg/chromium/chrome/browser/LogoBridge;

    .line 328
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 329
    sget v1, Lcom/google/android/apps/chrome/R$layout;->new_tab_page:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;

    iput-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage;->mNewTabPageView:Lcom/google/android/apps/chrome/ntp/NewTabPageView;

    .line 331
    invoke-direct {p0}, Lcom/google/android/apps/chrome/ntp/NewTabPage;->onSearchEngineUpdated()V

    .line 332
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage;->mNewTabPageView:Lcom/google/android/apps/chrome/ntp/NewTabPageView;

    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage;->mNewTabPageManager:Lcom/google/android/apps/chrome/ntp/NewTabPageView$NewTabPageManager;

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/ntp/NewTabPage;->isInSingleUrlBarMode(Landroid/content/Context;)Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->initialize(Lcom/google/android/apps/chrome/ntp/NewTabPageView$NewTabPageManager;Z)V

    .line 333
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage;->mNewTabPageView:Lcom/google/android/apps/chrome/ntp/NewTabPageView;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    .line 334
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/ntp/NewTabPage;)V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/google/android/apps/chrome/ntp/NewTabPage;->onSearchEngineUpdated()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/ntp/NewTabPage;)Z
    .locals 1

    .prologue
    .line 59
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage;->mIsDestroyed:Z

    return v0
.end method

.method static synthetic access$1000(Lcom/google/android/apps/chrome/ntp/NewTabPage;)Lorg/chromium/chrome/browser/favicon/FaviconHelper;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage;->mFaviconHelper:Lorg/chromium/chrome/browser/favicon/FaviconHelper;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/google/android/apps/chrome/ntp/NewTabPage;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage;->mOnLogoClickUrl:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1102(Lcom/google/android/apps/chrome/ntp/NewTabPage;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 59
    iput-object p1, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage;->mOnLogoClickUrl:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1200(Lcom/google/android/apps/chrome/ntp/NewTabPage;)Lorg/chromium/chrome/browser/LogoBridge;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage;->mLogoBridge:Lorg/chromium/chrome/browser/LogoBridge;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/google/android/apps/chrome/ntp/NewTabPage;)J
    .locals 2

    .prologue
    .line 59
    iget-wide v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage;->mConstructedTimeNs:J

    return-wide v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/ntp/NewTabPage;)Lcom/google/android/apps/chrome/ntp/NewTabPageView;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage;->mNewTabPageView:Lcom/google/android/apps/chrome/ntp/NewTabPageView;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/chrome/ntp/NewTabPage;Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 59
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/ntp/NewTabPage;->isInSingleUrlBarMode(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/chrome/ntp/NewTabPage;)Lorg/chromium/chrome/browser/profiles/MostVisitedSites;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage;->mMostVisitedSites:Lorg/chromium/chrome/browser/profiles/MostVisitedSites;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/chrome/ntp/NewTabPage;)Lcom/google/android/apps/chrome/tab/ChromeTab;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage;->mTab:Lcom/google/android/apps/chrome/tab/ChromeTab;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/chrome/ntp/NewTabPage;)Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/apps/chrome/ntp/NewTabPage;)Lorg/chromium/chrome/browser/profiles/Profile;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage;->mProfile:Lorg/chromium/chrome/browser/profiles/Profile;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/apps/chrome/ntp/NewTabPage;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage;->mActivity:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/apps/chrome/ntp/NewTabPage;)Lcom/google/android/apps/chrome/omnibox/LocationBar;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage;->mLocationBar:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    return-object v0
.end method

.method private static buildMostVisitedSites(Lorg/chromium/chrome/browser/profiles/Profile;)Lorg/chromium/chrome/browser/profiles/MostVisitedSites;
    .locals 1

    .prologue
    .line 337
    sget-object v0, Lcom/google/android/apps/chrome/ntp/NewTabPage;->sMostVisitedSitesForTests:Lorg/chromium/chrome/browser/profiles/MostVisitedSites;

    if-eqz v0, :cond_0

    .line 338
    sget-object v0, Lcom/google/android/apps/chrome/ntp/NewTabPage;->sMostVisitedSitesForTests:Lorg/chromium/chrome/browser/profiles/MostVisitedSites;

    .line 340
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lorg/chromium/chrome/browser/profiles/MostVisitedSites;

    invoke-direct {v0, p0}, Lorg/chromium/chrome/browser/profiles/MostVisitedSites;-><init>(Lorg/chromium/chrome/browser/profiles/Profile;)V

    goto :goto_0
.end method

.method private isInSingleUrlBarMode(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 351
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage;->mSearchProviderHasLogo:Z

    if-eqz v0, :cond_0

    invoke-static {p1}, Lorg/chromium/ui/base/DeviceFormFactor;->isTablet(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isNTPUrl(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 113
    if-eqz p0, :cond_0

    const-string/jumbo v0, "chrome-native://newtab/"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private onSearchEngineUpdated()V
    .locals 2

    .prologue
    .line 356
    invoke-static {}, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;->getInstance()Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;->isDefaultSearchEngineGoogle()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage;->mSearchProviderHasLogo:Z

    .line 357
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage;->mNewTabPageView:Lcom/google/android/apps/chrome/ntp/NewTabPageView;

    iget-boolean v1, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage;->mSearchProviderHasLogo:Z

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->setSearchProviderHasLogo(Z)V

    .line 358
    return-void
.end method

.method static setMostVisitedSitesForTests(Lorg/chromium/chrome/browser/profiles/MostVisitedSites;)V
    .locals 0

    .prologue
    .line 118
    sput-object p0, Lcom/google/android/apps/chrome/ntp/NewTabPage;->sMostVisitedSitesForTests:Lorg/chromium/chrome/browser/profiles/MostVisitedSites;

    .line 119
    return-void
.end method


# virtual methods
.method public captureThumbnail(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 476
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage;->mNewTabPageView:Lcom/google/android/apps/chrome/ntp/NewTabPageView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->captureThumbnail(Landroid/graphics/Canvas;)V

    .line 477
    return-void
.end method

.method public destroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 421
    sget-boolean v0, Lcom/google/android/apps/chrome/ntp/NewTabPage;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ntp/NewTabPage;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    const-string/jumbo v1, "Destroy called before removed from window"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 422
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage;->mFaviconHelper:Lorg/chromium/chrome/browser/favicon/FaviconHelper;

    if-eqz v0, :cond_1

    .line 423
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage;->mFaviconHelper:Lorg/chromium/chrome/browser/favicon/FaviconHelper;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/favicon/FaviconHelper;->destroy()V

    .line 424
    iput-object v1, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage;->mFaviconHelper:Lorg/chromium/chrome/browser/favicon/FaviconHelper;

    .line 426
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage;->mMostVisitedSites:Lorg/chromium/chrome/browser/profiles/MostVisitedSites;

    if-eqz v0, :cond_2

    .line 427
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage;->mMostVisitedSites:Lorg/chromium/chrome/browser/profiles/MostVisitedSites;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/profiles/MostVisitedSites;->destroy()V

    .line 428
    iput-object v1, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage;->mMostVisitedSites:Lorg/chromium/chrome/browser/profiles/MostVisitedSites;

    .line 430
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage;->mLogoBridge:Lorg/chromium/chrome/browser/LogoBridge;

    if-eqz v0, :cond_3

    .line 431
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage;->mLogoBridge:Lorg/chromium/chrome/browser/LogoBridge;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/LogoBridge;->destroy()V

    .line 432
    iput-object v1, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage;->mLogoBridge:Lorg/chromium/chrome/browser/LogoBridge;

    .line 434
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage;->mTab:Lcom/google/android/apps/chrome/tab/ChromeTab;

    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage;->mTabObserver:Lorg/chromium/chrome/browser/TabObserver;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->removeObserver(Lorg/chromium/chrome/browser/TabObserver;)V

    .line 435
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage;->mIsDestroyed:Z

    .line 436
    return-void
.end method

.method public getBackgroundColor()I
    .locals 1

    .prologue
    .line 450
    iget v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage;->mBackgroundColor:I

    return v0
.end method

.method public getHost()Ljava/lang/String;
    .locals 1

    .prologue
    .line 460
    const-string/jumbo v0, "newtab"

    return-object v0
.end method

.method getNewTabPageView()Lcom/google/android/apps/chrome/ntp/NewTabPageView;
    .locals 1

    .prologue
    .line 347
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage;->mNewTabPageView:Lcom/google/android/apps/chrome/ntp/NewTabPageView;

    return-object v0
.end method

.method public getSearchBoxBounds(Landroid/graphics/Rect;)V
    .locals 1

    .prologue
    .line 378
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage;->mNewTabPageView:Lcom/google/android/apps/chrome/ntp/NewTabPageView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->getSearchBoxBounds(Landroid/graphics/Rect;)V

    .line 379
    return-void
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 445
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage;->mTitle:Ljava/lang/String;

    return-object v0
.end method

.method public getUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 440
    const-string/jumbo v0, "chrome-native://newtab/"

    return-object v0
.end method

.method public getView()Landroid/view/View;
    .locals 1

    .prologue
    .line 455
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage;->mNewTabPageView:Lcom/google/android/apps/chrome/ntp/NewTabPageView;

    return-object v0
.end method

.method public isLocationBarShownInNTP()Z
    .locals 1

    .prologue
    .line 385
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage;->mNewTabPageManager:Lcom/google/android/apps/chrome/ntp/NewTabPageView$NewTabPageManager;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/ntp/NewTabPageView$NewTabPageManager;->isLocationBarShownInNTP()Z

    move-result v0

    return v0
.end method

.method public onViewAttachedToWindow(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 407
    const/16 v0, 0x2a

    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->registerForNotification(ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    .line 409
    return-void
.end method

.method public onViewDetachedFromWindow(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 413
    const/16 v0, 0x2a

    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->unregisterForNotification(ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    .line 415
    return-void
.end method

.method public setLocationBar(Lcom/google/android/apps/chrome/omnibox/LocationBar;)V
    .locals 0

    .prologue
    .line 400
    iput-object p1, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage;->mLocationBar:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    .line 401
    return-void
.end method

.method public setSearchBoxScrollListener(Lcom/google/android/apps/chrome/ntp/NewTabPage$OnSearchBoxScrollListener;)V
    .locals 1

    .prologue
    .line 393
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage;->mNewTabPageView:Lcom/google/android/apps/chrome/ntp/NewTabPageView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->setSearchBoxScrollListener(Lcom/google/android/apps/chrome/ntp/NewTabPage$OnSearchBoxScrollListener;)V

    .line 394
    return-void
.end method

.method public setUrlFocusChangeAnimationPercent(F)V
    .locals 1

    .prologue
    .line 368
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage;->mNewTabPageView:Lcom/google/android/apps/chrome/ntp/NewTabPageView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->setUrlFocusChangeAnimationPercent(F)V

    .line 369
    return-void
.end method

.method public shouldCaptureThumbnail()Z
    .locals 1

    .prologue
    .line 471
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage;->mNewTabPageView:Lcom/google/android/apps/chrome/ntp/NewTabPageView;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->shouldCaptureThumbnail()Z

    move-result v0

    return v0
.end method

.method public updateForUrl(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 465
    return-void
.end method
