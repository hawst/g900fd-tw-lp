.class public Lcom/google/android/apps/chrome/document/home/MostVisitedPage;
.super Ljava/lang/Object;
.source "MostVisitedPage.java"

# interfaces
.implements Lorg/chromium/chrome/browser/NativePage;


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final mActivity:Lcom/google/android/apps/chrome/document/DocumentActivity;

.field private final mBackgroundColor:I

.field private mFaviconHelper:Lorg/chromium/chrome/browser/favicon/FaviconHelper;

.field private mMostVisitedSites:Lorg/chromium/chrome/browser/profiles/MostVisitedSites;

.field private final mNewTabPageManager:Lcom/google/android/apps/chrome/ntp/NewTabPageView$NewTabPageManager;

.field private final mNewTabPageView:Lcom/google/android/apps/chrome/ntp/NewTabPageView;

.field private final mProfile:Lorg/chromium/chrome/browser/profiles/Profile;

.field private final mTab:Lcom/google/android/apps/chrome/tab/ChromeTab;

.field private final mTitle:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 54
    const-class v0, Lcom/google/android/apps/chrome/document/home/MostVisitedPage;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/document/home/MostVisitedPage;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/google/android/apps/chrome/document/DocumentActivity;Lcom/google/android/apps/chrome/tab/ChromeTab;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 213
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    new-instance v0, Lcom/google/android/apps/chrome/document/home/MostVisitedPage$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/document/home/MostVisitedPage$1;-><init>(Lcom/google/android/apps/chrome/document/home/MostVisitedPage;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/document/home/MostVisitedPage;->mNewTabPageManager:Lcom/google/android/apps/chrome/ntp/NewTabPageView$NewTabPageManager;

    .line 214
    invoke-static {}, Lcom/google/android/apps/chrome/document/DocumentManager;->getInstance()Lcom/google/android/apps/chrome/document/DocumentManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/document/DocumentManager;->getCurrentProfile()Lorg/chromium/chrome/browser/profiles/Profile;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/profiles/Profile;->getOriginalProfile()Lorg/chromium/chrome/browser/profiles/Profile;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/document/home/MostVisitedPage;->mProfile:Lorg/chromium/chrome/browser/profiles/Profile;

    .line 215
    iput-object p1, p0, Lcom/google/android/apps/chrome/document/home/MostVisitedPage;->mActivity:Lcom/google/android/apps/chrome/document/DocumentActivity;

    .line 216
    iput-object p2, p0, Lcom/google/android/apps/chrome/document/home/MostVisitedPage;->mTab:Lcom/google/android/apps/chrome/tab/ChromeTab;

    .line 218
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/document/DocumentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$string;->button_new_tab:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/document/home/MostVisitedPage;->mTitle:Ljava/lang/String;

    .line 219
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/document/DocumentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$color;->new_ntp_bg:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/document/home/MostVisitedPage;->mBackgroundColor:I

    .line 221
    new-instance v0, Lorg/chromium/chrome/browser/favicon/FaviconHelper;

    invoke-direct {v0}, Lorg/chromium/chrome/browser/favicon/FaviconHelper;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/document/home/MostVisitedPage;->mFaviconHelper:Lorg/chromium/chrome/browser/favicon/FaviconHelper;

    .line 222
    new-instance v0, Lorg/chromium/chrome/browser/profiles/MostVisitedSites;

    iget-object v1, p0, Lcom/google/android/apps/chrome/document/home/MostVisitedPage;->mProfile:Lorg/chromium/chrome/browser/profiles/Profile;

    invoke-direct {v0, v1}, Lorg/chromium/chrome/browser/profiles/MostVisitedSites;-><init>(Lorg/chromium/chrome/browser/profiles/Profile;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/document/home/MostVisitedPage;->mMostVisitedSites:Lorg/chromium/chrome/browser/profiles/MostVisitedSites;

    .line 224
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 225
    sget v1, Lcom/google/android/apps/chrome/R$layout;->new_tab_page:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;

    iput-object v0, p0, Lcom/google/android/apps/chrome/document/home/MostVisitedPage;->mNewTabPageView:Lcom/google/android/apps/chrome/ntp/NewTabPageView;

    .line 227
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/home/MostVisitedPage;->mNewTabPageView:Lcom/google/android/apps/chrome/ntp/NewTabPageView;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->setSearchProviderHasLogo(Z)V

    .line 228
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/home/MostVisitedPage;->mNewTabPageView:Lcom/google/android/apps/chrome/ntp/NewTabPageView;

    iget-object v1, p0, Lcom/google/android/apps/chrome/document/home/MostVisitedPage;->mNewTabPageManager:Lcom/google/android/apps/chrome/ntp/NewTabPageView$NewTabPageManager;

    invoke-virtual {v0, v1, v3}, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->initialize(Lcom/google/android/apps/chrome/ntp/NewTabPageView$NewTabPageManager;Z)V

    .line 229
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/document/home/MostVisitedPage;)Lcom/google/android/apps/chrome/tab/ChromeTab;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/home/MostVisitedPage;->mTab:Lcom/google/android/apps/chrome/tab/ChromeTab;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/document/home/MostVisitedPage;)Lcom/google/android/apps/chrome/document/DocumentActivity;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/home/MostVisitedPage;->mActivity:Lcom/google/android/apps/chrome/document/DocumentActivity;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/document/home/MostVisitedPage;)Lorg/chromium/chrome/browser/profiles/MostVisitedSites;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/home/MostVisitedPage;->mMostVisitedSites:Lorg/chromium/chrome/browser/profiles/MostVisitedSites;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/chrome/document/home/MostVisitedPage;)Lorg/chromium/chrome/browser/profiles/Profile;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/home/MostVisitedPage;->mProfile:Lorg/chromium/chrome/browser/profiles/Profile;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/chrome/document/home/MostVisitedPage;)Lorg/chromium/chrome/browser/favicon/FaviconHelper;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/home/MostVisitedPage;->mFaviconHelper:Lorg/chromium/chrome/browser/favicon/FaviconHelper;

    return-object v0
.end method

.method public static launchBookmarksDialog(Lcom/google/android/apps/chrome/document/DocumentActivity;Lorg/chromium/chrome/browser/Tab;)V
    .locals 3

    .prologue
    .line 298
    new-instance v0, Lcom/google/android/apps/chrome/document/home/MostVisitedPage$BookmarkDialogSelectedListener;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/document/home/MostVisitedPage$BookmarkDialogSelectedListener;-><init>(Lcom/google/android/apps/chrome/document/DocumentActivity;)V

    .line 299
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->getTabModelSelector()Lcom/google/android/apps/chrome/tabmodel/SingleTabModelSelector;

    move-result-object v1

    invoke-static {}, Lorg/chromium/chrome/browser/profiles/Profile;->getLastUsedProfile()Lorg/chromium/chrome/browser/profiles/Profile;

    move-result-object v2

    invoke-static {p0, p1, v1, v2, v0}, Lcom/google/android/apps/chrome/ntp/BookmarksPage;->buildPageHomeActivityMode(Landroid/content/Context;Lorg/chromium/chrome/browser/Tab;Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;Lorg/chromium/chrome/browser/profiles/Profile;Lcom/google/android/apps/chrome/ntp/BookmarksPage$BookmarkSelectedListener;)Lcom/google/android/apps/chrome/ntp/BookmarksPage;

    move-result-object v1

    .line 302
    const-string/jumbo v2, "chrome-native://bookmarks/"

    invoke-interface {v1, v2}, Lorg/chromium/chrome/browser/NativePage;->updateForUrl(Ljava/lang/String;)V

    .line 303
    new-instance v2, Lcom/google/android/apps/chrome/ntp/NativePageDialog;

    invoke-direct {v2, p0, v1}, Lcom/google/android/apps/chrome/ntp/NativePageDialog;-><init>(Landroid/content/Context;Lorg/chromium/chrome/browser/NativePage;)V

    .line 304
    invoke-virtual {v0, v2}, Lcom/google/android/apps/chrome/document/home/MostVisitedPage$BookmarkDialogSelectedListener;->setDialog(Landroid/app/Dialog;)V

    .line 305
    invoke-virtual {v2}, Landroid/app/Dialog;->show()V

    .line 306
    return-void
.end method

.method public static launchRecentTabsDialog(Landroid/app/Activity;Lcom/google/android/apps/chrome/tab/ChromeTab;Z)V
    .locals 3

    .prologue
    .line 310
    new-instance v0, Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;

    invoke-direct {v0, p1, p0, p2}, Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;-><init>(Lcom/google/android/apps/chrome/tab/ChromeTab;Landroid/app/Activity;Z)V

    .line 312
    new-instance v1, Lcom/google/android/apps/chrome/ntp/RecentTabsPage;

    invoke-direct {v1, p0, v0}, Lcom/google/android/apps/chrome/ntp/RecentTabsPage;-><init>(Landroid/app/Activity;Lcom/google/android/apps/chrome/ntp/RecentTabsManager;)V

    .line 313
    const-string/jumbo v2, "chrome-native://recent-tabs/"

    invoke-interface {v1, v2}, Lorg/chromium/chrome/browser/NativePage;->updateForUrl(Ljava/lang/String;)V

    .line 314
    new-instance v2, Lcom/google/android/apps/chrome/ntp/NativePageDialog;

    invoke-direct {v2, p0, v1}, Lcom/google/android/apps/chrome/ntp/NativePageDialog;-><init>(Landroid/content/Context;Lorg/chromium/chrome/browser/NativePage;)V

    .line 315
    invoke-virtual {v0, v2}, Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;->setDialog(Landroid/app/Dialog;)V

    .line 316
    invoke-virtual {v2}, Landroid/app/Dialog;->show()V

    .line 317
    return-void
.end method


# virtual methods
.method public destroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 238
    sget-boolean v0, Lcom/google/android/apps/chrome/document/home/MostVisitedPage;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/home/MostVisitedPage;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    const-string/jumbo v1, "Destroy called before removed from window"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 239
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/home/MostVisitedPage;->mFaviconHelper:Lorg/chromium/chrome/browser/favicon/FaviconHelper;

    if-eqz v0, :cond_1

    .line 240
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/home/MostVisitedPage;->mFaviconHelper:Lorg/chromium/chrome/browser/favicon/FaviconHelper;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/favicon/FaviconHelper;->destroy()V

    .line 241
    iput-object v1, p0, Lcom/google/android/apps/chrome/document/home/MostVisitedPage;->mFaviconHelper:Lorg/chromium/chrome/browser/favicon/FaviconHelper;

    .line 243
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/home/MostVisitedPage;->mMostVisitedSites:Lorg/chromium/chrome/browser/profiles/MostVisitedSites;

    if-eqz v0, :cond_2

    .line 244
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/home/MostVisitedPage;->mMostVisitedSites:Lorg/chromium/chrome/browser/profiles/MostVisitedSites;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/profiles/MostVisitedSites;->destroy()V

    .line 245
    iput-object v1, p0, Lcom/google/android/apps/chrome/document/home/MostVisitedPage;->mMostVisitedSites:Lorg/chromium/chrome/browser/profiles/MostVisitedSites;

    .line 247
    :cond_2
    return-void
.end method

.method public getBackgroundColor()I
    .locals 1

    .prologue
    .line 266
    iget v0, p0, Lcom/google/android/apps/chrome/document/home/MostVisitedPage;->mBackgroundColor:I

    return v0
.end method

.method public getHost()Ljava/lang/String;
    .locals 1

    .prologue
    .line 261
    const-string/jumbo v0, "newtab"

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 251
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/home/MostVisitedPage;->mTitle:Ljava/lang/String;

    return-object v0
.end method

.method public getUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 256
    const-string/jumbo v0, "chrome-native://newtab/"

    return-object v0
.end method

.method public getView()Landroid/view/View;
    .locals 1

    .prologue
    .line 233
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/home/MostVisitedPage;->mNewTabPageView:Lcom/google/android/apps/chrome/ntp/NewTabPageView;

    return-object v0
.end method

.method public updateForUrl(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 271
    return-void
.end method
