.class final Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterDataFactory;
.super Ljava/lang/Object;
.source "PrinterDataFactory.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 135
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 136
    return-void
.end method

.method static createDefaultNonChangingPrinterData()Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;
    .locals 2

    .prologue
    .line 74
    invoke-static {}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterDataFactory;->getDefaultCapabilities()Ljava/lang/String;

    move-result-object v0

    .line 75
    invoke-static {}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->create()Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->withCapabilities(Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->withDefaultValues(Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;

    move-result-object v1

    invoke-static {v0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterDataFactory;->getUnsaltedMd5Hash(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->withCapabilitiesHash(Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;

    move-result-object v0

    return-object v0
.end method

.method static createDefaultPrinterData(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;
    .locals 2

    .prologue
    .line 60
    invoke-static {}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterDataFactory;->createDefaultNonChangingPrinterData()Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;

    move-result-object v0

    invoke-static {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterDataFactory;->getDefaultId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->withId(Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;

    move-result-object v0

    invoke-static {}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterDataFactory;->getDefaultProxy()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->withProxy(Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;

    move-result-object v0

    invoke-static {}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterDataFactory;->getDefaultType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->withType(Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;

    move-result-object v0

    invoke-static {}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterDataFactory;->getDefaultName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->withName(Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;

    move-result-object v0

    invoke-static {}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterDataFactory;->getDefaultDescription()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->withDescription(Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;

    move-result-object v0

    invoke-static {}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterDataFactory;->getDefaultStatus()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->withStatus(Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;

    move-result-object v0

    invoke-static {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterDataFactory;->getDefaultApplicationVersionTag(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->withApplicationVersionTag(Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;

    move-result-object v0

    invoke-static {}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterDataFactory;->getDefaultProtocolVersionTag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->withProtocolVersionTag(Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->withC2dmRegistrationIdTag(Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;

    move-result-object v0

    return-object v0
.end method

.method private static getDefaultApplicationVersionTag(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 128
    invoke-static {p0}, Lorg/chromium/base/BuildInfo;->getPackageVersionName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static getDefaultCapabilities()Ljava/lang/String;
    .locals 1

    .prologue
    .line 116
    const-string/jumbo v0, "*PPD-Adobe: \"4.3\"*OpenUI *LanguageLevel/Language Level: PickOne*DefaultLanguageLevel: 2*LanguageLevel 2/Two 2:*CloseUI *LanguageLevel*OpenUI *ColorDevice/Color Device: PickOne*DefaultColorDevice: True*ColorDevice True/True:*CloseUI *ColorDevice*OpenUI *ColorSpace/Color Space: PickOne*DefaultColorSpace: CMYK*ColorSpace CMYK/CMYK:*CloseUI *ColorSpace*OpenUI *TTRasterizer/TT Rasterizer: PickOne*DefaultTTRasterizer: Type42*TTRasterizer Type42/Type42:*CloseUI *TTRasterizer*OpenUI *FileSystem/File System: PickOne*DefaultFileSystem: False*FileSystem False/False:*CloseUI *FileSystem*OpenUI *Throughput/Throughput: PickOne*DefaultThroughput: 10*Throughput 10/10:*CloseUI *Throughput"

    return-object v0
.end method

.method private static getDefaultDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 112
    const-string/jumbo v0, ""

    return-object v0
.end method

.method private static getDefaultId(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 82
    invoke-static {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->getPrinterId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static getDefaultName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 108
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    return-object v0
.end method

.method private static getDefaultProtocolVersionTag()Ljava/lang/String;
    .locals 2

    .prologue
    .line 132
    const-wide/16 v0, 0x3

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static getDefaultProxy()Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 86
    const-string/jumbo v1, "GSERVICES_ANDROID_ID"

    invoke-static {v1}, Lorg/chromium/chrome/browser/identity/UniqueIdentificationGeneratorFactory;->getInstance(Ljava/lang/String;)Lorg/chromium/chrome/browser/identity/UniqueIdentificationGenerator;

    move-result-object v1

    invoke-interface {v1, v0}, Lorg/chromium/chrome/browser/identity/UniqueIdentificationGenerator;->getUniqueId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 88
    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 89
    const-string/jumbo v1, "PrinterDataFactory"

    const-string/jumbo v2, "Unable to generate proxy ID based on Gservices ID."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 92
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "ChromeMobile_"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static getDefaultStatus()Ljava/lang/String;
    .locals 1

    .prologue
    .line 124
    const-string/jumbo v0, "0"

    return-object v0
.end method

.method private static getDefaultType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 96
    const-string/jumbo v0, "ANDROID_CHROME_SNAPSHOT"

    return-object v0
.end method

.method private static getUnsaltedMd5Hash(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 120
    new-instance v0, Lorg/chromium/chrome/browser/util/HashUtil$Params;

    invoke-direct {v0, p0}, Lorg/chromium/chrome/browser/util/HashUtil$Params;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lorg/chromium/chrome/browser/util/HashUtil;->getMd5Hash(Lorg/chromium/chrome/browser/util/HashUtil$Params;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
