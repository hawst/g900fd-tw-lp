.class Lcom/google/android/apps/chrome/webapps/WebappFullscreenManager$1;
.super Ljava/lang/Object;
.source "WebappFullscreenManager.java"

# interfaces
.implements Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler$FullscreenHtmlApiDelegate;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/webapps/WebappFullscreenManager;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/webapps/WebappFullscreenManager;)V
    .locals 0

    .prologue
    .line 64
    iput-object p1, p0, Lcom/google/android/apps/chrome/webapps/WebappFullscreenManager$1;->this$0:Lcom/google/android/apps/chrome/webapps/WebappFullscreenManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public cancelPendingEnterFullscreen()Z
    .locals 3

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/WebappFullscreenManager$1;->this$0:Lcom/google/android/apps/chrome/webapps/WebappFullscreenManager;

    # getter for: Lcom/google/android/apps/chrome/webapps/WebappFullscreenManager;->mTriggerFullscreenOnAnimationDone:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/webapps/WebappFullscreenManager;->access$200(Lcom/google/android/apps/chrome/webapps/WebappFullscreenManager;)Z

    move-result v0

    .line 84
    iget-object v1, p0, Lcom/google/android/apps/chrome/webapps/WebappFullscreenManager$1;->this$0:Lcom/google/android/apps/chrome/webapps/WebappFullscreenManager;

    const/4 v2, 0x0

    # setter for: Lcom/google/android/apps/chrome/webapps/WebappFullscreenManager;->mTriggerFullscreenOnAnimationDone:Z
    invoke-static {v1, v2}, Lcom/google/android/apps/chrome/webapps/WebappFullscreenManager;->access$202(Lcom/google/android/apps/chrome/webapps/WebappFullscreenManager;Z)Z

    .line 85
    iget-object v1, p0, Lcom/google/android/apps/chrome/webapps/WebappFullscreenManager$1;->this$0:Lcom/google/android/apps/chrome/webapps/WebappFullscreenManager;

    # getter for: Lcom/google/android/apps/chrome/webapps/WebappFullscreenManager;->mSlidingLayout:Lcom/google/android/apps/chrome/webapps/SlidingLayout;
    invoke-static {v1}, Lcom/google/android/apps/chrome/webapps/WebappFullscreenManager;->access$100(Lcom/google/android/apps/chrome/webapps/WebappFullscreenManager;)Lcom/google/android/apps/chrome/webapps/SlidingLayout;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/webapps/WebappFullscreenManager$1;->this$0:Lcom/google/android/apps/chrome/webapps/WebappFullscreenManager;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/chrome/webapps/SlidingLayout;->removeAnimationListener(Lcom/google/android/apps/chrome/webapps/SlidingLayout$AnimationListener;)V

    .line 86
    return v0
.end method

.method public getNotificationAnchorView()Landroid/view/View;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/WebappFullscreenManager$1;->this$0:Lcom/google/android/apps/chrome/webapps/WebappFullscreenManager;

    # getter for: Lcom/google/android/apps/chrome/webapps/WebappFullscreenManager;->mUrlBarView:Landroid/view/View;
    invoke-static {v0}, Lcom/google/android/apps/chrome/webapps/WebappFullscreenManager;->access$500(Lcom/google/android/apps/chrome/webapps/WebappFullscreenManager;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public getNotificationOffsetY()I
    .locals 1

    .prologue
    .line 91
    const/4 v0, 0x0

    return v0
.end method

.method public onEnterFullscreen()V
    .locals 2

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/WebappFullscreenManager$1;->this$0:Lcom/google/android/apps/chrome/webapps/WebappFullscreenManager;

    # getter for: Lcom/google/android/apps/chrome/webapps/WebappFullscreenManager;->mDelegate:Lcom/google/android/apps/chrome/webapps/WebappFullscreenManager$WebappFullscreenDelegate;
    invoke-static {v0}, Lcom/google/android/apps/chrome/webapps/WebappFullscreenManager;->access$000(Lcom/google/android/apps/chrome/webapps/WebappFullscreenManager;)Lcom/google/android/apps/chrome/webapps/WebappFullscreenManager$WebappFullscreenDelegate;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/webapps/WebappFullscreenManager$WebappFullscreenDelegate;->updateUrlBarVisibility()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 73
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/WebappFullscreenManager$1;->this$0:Lcom/google/android/apps/chrome/webapps/WebappFullscreenManager;

    # getter for: Lcom/google/android/apps/chrome/webapps/WebappFullscreenManager;->mSlidingLayout:Lcom/google/android/apps/chrome/webapps/SlidingLayout;
    invoke-static {v0}, Lcom/google/android/apps/chrome/webapps/WebappFullscreenManager;->access$100(Lcom/google/android/apps/chrome/webapps/WebappFullscreenManager;)Lcom/google/android/apps/chrome/webapps/SlidingLayout;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/webapps/WebappFullscreenManager$1;->this$0:Lcom/google/android/apps/chrome/webapps/WebappFullscreenManager;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/webapps/SlidingLayout;->addAnimationListener(Lcom/google/android/apps/chrome/webapps/SlidingLayout$AnimationListener;)V

    .line 74
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/WebappFullscreenManager$1;->this$0:Lcom/google/android/apps/chrome/webapps/WebappFullscreenManager;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/apps/chrome/webapps/WebappFullscreenManager;->mTriggerFullscreenOnAnimationDone:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/webapps/WebappFullscreenManager;->access$202(Lcom/google/android/apps/chrome/webapps/WebappFullscreenManager;Z)Z

    .line 79
    :goto_0
    return-void

    .line 76
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/WebappFullscreenManager$1;->this$0:Lcom/google/android/apps/chrome/webapps/WebappFullscreenManager;

    # invokes: Lcom/google/android/apps/chrome/webapps/WebappFullscreenManager;->getHtmlApiHandler()Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;
    invoke-static {v0}, Lcom/google/android/apps/chrome/webapps/WebappFullscreenManager;->access$400(Lcom/google/android/apps/chrome/webapps/WebappFullscreenManager;)Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/webapps/WebappFullscreenManager$1;->this$0:Lcom/google/android/apps/chrome/webapps/WebappFullscreenManager;

    # invokes: Lcom/google/android/apps/chrome/webapps/WebappFullscreenManager;->getTabModelSelector()Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;
    invoke-static {v1}, Lcom/google/android/apps/chrome/webapps/WebappFullscreenManager;->access$300(Lcom/google/android/apps/chrome/webapps/WebappFullscreenManager;)Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    move-result-object v1

    invoke-interface {v1}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->getCurrentTab()Lorg/chromium/chrome/browser/Tab;

    move-result-object v1

    invoke-virtual {v1}, Lorg/chromium/chrome/browser/Tab;->getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;->enterFullscreen(Lorg/chromium/content/browser/ContentViewCore;)V

    goto :goto_0
.end method

.method public onFullscreenExited(Lorg/chromium/content/browser/ContentViewCore;)V
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/WebappFullscreenManager$1;->this$0:Lcom/google/android/apps/chrome/webapps/WebappFullscreenManager;

    # getter for: Lcom/google/android/apps/chrome/webapps/WebappFullscreenManager;->mDelegate:Lcom/google/android/apps/chrome/webapps/WebappFullscreenManager$WebappFullscreenDelegate;
    invoke-static {v0}, Lcom/google/android/apps/chrome/webapps/WebappFullscreenManager;->access$000(Lcom/google/android/apps/chrome/webapps/WebappFullscreenManager;)Lcom/google/android/apps/chrome/webapps/WebappFullscreenManager$WebappFullscreenDelegate;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/webapps/WebappFullscreenManager$WebappFullscreenDelegate;->updateUrlBarVisibility()Z

    .line 68
    return-void
.end method

.method public shouldShowNotificationBubble()Z
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/WebappFullscreenManager$1;->this$0:Lcom/google/android/apps/chrome/webapps/WebappFullscreenManager;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/webapps/WebappFullscreenManager;->isOverlayVideoMode()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
