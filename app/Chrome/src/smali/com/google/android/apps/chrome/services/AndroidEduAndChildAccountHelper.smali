.class public abstract Lcom/google/android/apps/chrome/services/AndroidEduAndChildAccountHelper;
.super Ljava/lang/Object;
.source "AndroidEduAndChildAccountHelper.java"

# interfaces
.implements Lcom/google/android/apps/chrome/services/AndroidEdu$OwnerCheckCallback;
.implements Lcom/google/android/apps/chrome/services/ChildAccountManager$HasChildAccountCallback;


# instance fields
.field private mHasChildAccount:Ljava/lang/Boolean;

.field private mIsAndroidEduDevice:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private checkDone()V
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/AndroidEduAndChildAccountHelper;->mIsAndroidEduDevice:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/services/AndroidEduAndChildAccountHelper;->mHasChildAccount:Ljava/lang/Boolean;

    if-nez v0, :cond_1

    .line 46
    :cond_0
    :goto_0
    return-void

    .line 45
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/services/AndroidEduAndChildAccountHelper;->onParametersReady()V

    goto :goto_0
.end method


# virtual methods
.method public hasChildAccount()Z
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/AndroidEduAndChildAccountHelper;->mHasChildAccount:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public isAndroidEduDevice()Z
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/AndroidEduAndChildAccountHelper;->mIsAndroidEduDevice:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public onChildAccountChecked(Z)V
    .locals 1

    .prologue
    .line 58
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/services/AndroidEduAndChildAccountHelper;->mHasChildAccount:Ljava/lang/Boolean;

    .line 59
    invoke-direct {p0}, Lcom/google/android/apps/chrome/services/AndroidEduAndChildAccountHelper;->checkDone()V

    .line 60
    return-void
.end method

.method public abstract onParametersReady()V
.end method

.method public onSchoolCheckDone(Z)V
    .locals 1

    .prologue
    .line 51
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/services/AndroidEduAndChildAccountHelper;->mIsAndroidEduDevice:Ljava/lang/Boolean;

    .line 52
    invoke-direct {p0}, Lcom/google/android/apps/chrome/services/AndroidEduAndChildAccountHelper;->checkDone()V

    .line 53
    return-void
.end method

.method public start(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 38
    invoke-static {p1}, Lcom/google/android/apps/chrome/services/ChildAccountManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/services/ChildAccountManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/apps/chrome/services/ChildAccountManager;->checkHasChildAccount(Lcom/google/android/apps/chrome/services/ChildAccountManager$HasChildAccountCallback;)V

    .line 39
    invoke-virtual {p1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p0}, Lcom/google/android/apps/chrome/services/AndroidEdu;->checkIsAndroidEduDevice(Landroid/content/Context;Lcom/google/android/apps/chrome/services/AndroidEdu$OwnerCheckCallback;)V

    .line 41
    return-void
.end method
