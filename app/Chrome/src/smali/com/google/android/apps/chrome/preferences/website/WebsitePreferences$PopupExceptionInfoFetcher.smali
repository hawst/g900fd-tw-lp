.class Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$PopupExceptionInfoFetcher;
.super Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$CallChain;
.source "WebsitePreferences.java"


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences;)V
    .locals 1

    .prologue
    .line 105
    iput-object p1, p0, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$PopupExceptionInfoFetcher;->this$0:Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$CallChain;-><init>(Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences;Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$1;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences;Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$1;)V
    .locals 0

    .prologue
    .line 105
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$PopupExceptionInfoFetcher;-><init>(Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences;)V

    return-void
.end method


# virtual methods
.method run()V
    .locals 4

    .prologue
    .line 108
    invoke-static {}, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferenceBridge;->getPopupExceptionInfo()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/preferences/website/PopupExceptionInfo;

    .line 110
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/website/PopupExceptionInfo;->getPattern()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v3, "*"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 111
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/website/PopupExceptionInfo;->getPattern()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/chrome/preferences/website/WebsiteAddress;->create(Ljava/lang/String;)Lcom/google/android/apps/chrome/preferences/website/WebsiteAddress;

    move-result-object v1

    .line 112
    if-eqz v1, :cond_0

    .line 113
    iget-object v3, p0, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$PopupExceptionInfoFetcher;->this$0:Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences;

    # invokes: Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences;->findOrCreateSitesByHost(Lcom/google/android/apps/chrome/preferences/website/WebsiteAddress;)Ljava/util/Set;
    invoke-static {v3, v1}, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences;->access$1000(Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences;Lcom/google/android/apps/chrome/preferences/website/WebsiteAddress;)Ljava/util/Set;

    move-result-object v1

    .line 114
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/chrome/preferences/website/Website;

    .line 115
    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/preferences/website/Website;->setPopupExceptionInfo(Lcom/google/android/apps/chrome/preferences/website/PopupExceptionInfo;)V

    goto :goto_0

    .line 118
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$PopupExceptionInfoFetcher;->next()V

    .line 119
    return-void
.end method
