.class Lcom/google/android/apps/chrome/document/DocumentSuggestionProvider$3;
.super Ljava/lang/Object;
.source "DocumentSuggestionProvider.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/document/DocumentSuggestionProvider;

.field final synthetic val$selection:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/document/DocumentSuggestionProvider;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 138
    iput-object p1, p0, Lcom/google/android/apps/chrome/document/DocumentSuggestionProvider$3;->this$0:Lcom/google/android/apps/chrome/document/DocumentSuggestionProvider;

    iput-object p2, p0, Lcom/google/android/apps/chrome/document/DocumentSuggestionProvider$3;->val$selection:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 138
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentSuggestionProvider$3;->call()Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;

    move-result-object v0

    return-object v0
.end method

.method public call()Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;
    .locals 2

    .prologue
    .line 141
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentSuggestionProvider$3;->this$0:Lcom/google/android/apps/chrome/document/DocumentSuggestionProvider;

    # getter for: Lcom/google/android/apps/chrome/document/DocumentSuggestionProvider;->mController:Lorg/chromium/chrome/browser/omnibox/AutocompleteController;
    invoke-static {v0}, Lcom/google/android/apps/chrome/document/DocumentSuggestionProvider;->access$000(Lcom/google/android/apps/chrome/document/DocumentSuggestionProvider;)Lorg/chromium/chrome/browser/omnibox/AutocompleteController;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/document/DocumentSuggestionProvider$3;->val$selection:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lorg/chromium/chrome/browser/omnibox/AutocompleteController;->getTopSynchronousMatch(Ljava/lang/String;)Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;

    move-result-object v0

    return-object v0
.end method
