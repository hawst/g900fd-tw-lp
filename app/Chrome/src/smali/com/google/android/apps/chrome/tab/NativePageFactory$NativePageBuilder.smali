.class public Lcom/google/android/apps/chrome/tab/NativePageFactory$NativePageBuilder;
.super Ljava/lang/Object;
.source "NativePageFactory.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method protected buildBookmarksPage(Landroid/app/Activity;Lorg/chromium/chrome/browser/Tab;Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;)Lorg/chromium/chrome/browser/NativePage;
    .locals 1

    .prologue
    .line 52
    invoke-static {p1, p2, p3}, Lcom/google/android/apps/chrome/ntp/BookmarksPage;->buildPage(Landroid/content/Context;Lorg/chromium/chrome/browser/Tab;Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;)Lcom/google/android/apps/chrome/ntp/BookmarksPage;

    move-result-object v0

    return-object v0
.end method

.method protected buildNewTabPage(Landroid/app/Activity;Lcom/google/android/apps/chrome/tab/ChromeTab;Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;)Lorg/chromium/chrome/browser/NativePage;
    .locals 1

    .prologue
    .line 43
    invoke-virtual {p2}, Lcom/google/android/apps/chrome/tab/ChromeTab;->isIncognito()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 44
    new-instance v0, Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPage;

    invoke-direct {v0, p1, p2}, Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPage;-><init>(Landroid/content/Context;Lorg/chromium/chrome/browser/Tab;)V

    .line 46
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/android/apps/chrome/ntp/NewTabPage;

    invoke-direct {v0, p1, p2, p3}, Lcom/google/android/apps/chrome/ntp/NewTabPage;-><init>(Landroid/app/Activity;Lcom/google/android/apps/chrome/tab/ChromeTab;Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;)V

    goto :goto_0
.end method

.method protected buildRecentTabsPage(Landroid/app/Activity;Lcom/google/android/apps/chrome/tab/ChromeTab;)Lorg/chromium/chrome/browser/NativePage;
    .locals 2

    .prologue
    .line 56
    new-instance v0, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;

    invoke-virtual {p2}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getProfile()Lorg/chromium/chrome/browser/profiles/Profile;

    move-result-object v1

    invoke-direct {v0, p2, v1, p1}, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;-><init>(Lcom/google/android/apps/chrome/tab/ChromeTab;Lorg/chromium/chrome/browser/profiles/Profile;Landroid/content/Context;)V

    .line 58
    new-instance v1, Lcom/google/android/apps/chrome/ntp/RecentTabsPage;

    invoke-direct {v1, p1, v0}, Lcom/google/android/apps/chrome/ntp/RecentTabsPage;-><init>(Landroid/app/Activity;Lcom/google/android/apps/chrome/ntp/RecentTabsManager;)V

    return-object v1
.end method
