.class public Lcom/google/android/apps/chrome/icing/IcingNativeApiClientInfo;
.super Ljava/lang/Object;
.source "IcingNativeApiClientInfo.java"


# instance fields
.field public final sharedLibAbsoluteFilename:Ljava/lang/String;

.field public final sharedLibExtensionAbsoluteFilename:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    if-nez p1, :cond_0

    .line 37
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "sharedLibAbsoluteFilename must not be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 39
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 40
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "sharedLibAbsoluteFilename must be non-empty."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 42
    :cond_1
    if-eqz p2, :cond_2

    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 44
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "sharedLibExtensionAbsoluteFilename must not be the empty string"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 47
    :cond_2
    iput-object p1, p0, Lcom/google/android/apps/chrome/icing/IcingNativeApiClientInfo;->sharedLibAbsoluteFilename:Ljava/lang/String;

    .line 48
    iput-object p2, p0, Lcom/google/android/apps/chrome/icing/IcingNativeApiClientInfo;->sharedLibExtensionAbsoluteFilename:Ljava/lang/String;

    .line 49
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 66
    if-ne p0, p1, :cond_1

    .line 84
    :cond_0
    :goto_0
    return v0

    .line 68
    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    .line 69
    goto :goto_0

    .line 70
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 71
    goto :goto_0

    .line 72
    :cond_3
    check-cast p1, Lcom/google/android/apps/chrome/icing/IcingNativeApiClientInfo;

    .line 73
    iget-object v2, p0, Lcom/google/android/apps/chrome/icing/IcingNativeApiClientInfo;->sharedLibAbsoluteFilename:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 74
    iget-object v2, p1, Lcom/google/android/apps/chrome/icing/IcingNativeApiClientInfo;->sharedLibAbsoluteFilename:Ljava/lang/String;

    if-eqz v2, :cond_5

    move v0, v1

    .line 75
    goto :goto_0

    .line 76
    :cond_4
    iget-object v2, p0, Lcom/google/android/apps/chrome/icing/IcingNativeApiClientInfo;->sharedLibAbsoluteFilename:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/apps/chrome/icing/IcingNativeApiClientInfo;->sharedLibAbsoluteFilename:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 77
    goto :goto_0

    .line 78
    :cond_5
    iget-object v2, p0, Lcom/google/android/apps/chrome/icing/IcingNativeApiClientInfo;->sharedLibExtensionAbsoluteFilename:Ljava/lang/String;

    if-nez v2, :cond_6

    .line 79
    iget-object v2, p1, Lcom/google/android/apps/chrome/icing/IcingNativeApiClientInfo;->sharedLibExtensionAbsoluteFilename:Ljava/lang/String;

    if-eqz v2, :cond_0

    move v0, v1

    .line 80
    goto :goto_0

    .line 81
    :cond_6
    iget-object v2, p0, Lcom/google/android/apps/chrome/icing/IcingNativeApiClientInfo;->sharedLibExtensionAbsoluteFilename:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/apps/chrome/icing/IcingNativeApiClientInfo;->sharedLibExtensionAbsoluteFilename:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 83
    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 54
    iget-object v0, p0, Lcom/google/android/apps/chrome/icing/IcingNativeApiClientInfo;->sharedLibAbsoluteFilename:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit8 v0, v0, 0x1f

    .line 58
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/apps/chrome/icing/IcingNativeApiClientInfo;->sharedLibExtensionAbsoluteFilename:Ljava/lang/String;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 60
    return v0

    .line 54
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/icing/IcingNativeApiClientInfo;->sharedLibAbsoluteFilename:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 58
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/chrome/icing/IcingNativeApiClientInfo;->sharedLibExtensionAbsoluteFilename:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 90
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "IcingNativeApiClientInfo sharedLibAbsoluteFilename="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/chrome/icing/IcingNativeApiClientInfo;->sharedLibAbsoluteFilename:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", sharedLibExtensionAbsoluteFilename="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/icing/IcingNativeApiClientInfo;->sharedLibExtensionAbsoluteFilename:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
