.class Lcom/google/android/apps/chrome/document/DocumentToolbarControls$1;
.super Ljava/lang/Object;
.source "DocumentToolbarControls.java"

# interfaces
.implements Lcom/google/android/apps/chrome/document/TitleBar$TitleBarListener;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/document/DocumentToolbarControls;

.field final synthetic val$activity:Lcom/google/android/apps/chrome/ChromeActivity;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/document/DocumentToolbarControls;Lcom/google/android/apps/chrome/ChromeActivity;)V
    .locals 0

    .prologue
    .line 53
    iput-object p1, p0, Lcom/google/android/apps/chrome/document/DocumentToolbarControls$1;->this$0:Lcom/google/android/apps/chrome/document/DocumentToolbarControls;

    iput-object p2, p0, Lcom/google/android/apps/chrome/document/DocumentToolbarControls$1;->val$activity:Lcom/google/android/apps/chrome/ChromeActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMicClicked()V
    .locals 0

    .prologue
    .line 68
    return-void
.end method

.method public onTitleClicked()V
    .locals 0

    .prologue
    .line 56
    return-void
.end method

.method public onTitleLongClicked()V
    .locals 2

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentToolbarControls$1;->this$0:Lcom/google/android/apps/chrome/document/DocumentToolbarControls;

    # getter for: Lcom/google/android/apps/chrome/document/DocumentToolbarControls;->mTab:Lorg/chromium/chrome/browser/Tab;
    invoke-static {v0}, Lcom/google/android/apps/chrome/document/DocumentToolbarControls;->access$000(Lcom/google/android/apps/chrome/document/DocumentToolbarControls;)Lorg/chromium/chrome/browser/Tab;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/Tab;->getWebContents()Lorg/chromium/content_public/browser/WebContents;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentToolbarControls$1;->this$0:Lcom/google/android/apps/chrome/document/DocumentToolbarControls;

    # getter for: Lcom/google/android/apps/chrome/document/DocumentToolbarControls;->mTab:Lorg/chromium/chrome/browser/Tab;
    invoke-static {v0}, Lcom/google/android/apps/chrome/document/DocumentToolbarControls;->access$000(Lcom/google/android/apps/chrome/document/DocumentToolbarControls;)Lorg/chromium/chrome/browser/Tab;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/Tab;->getSecurityLevel()I

    move-result v0

    if-eqz v0, :cond_0

    .line 62
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentToolbarControls$1;->val$activity:Lcom/google/android/apps/chrome/ChromeActivity;

    iget-object v1, p0, Lcom/google/android/apps/chrome/document/DocumentToolbarControls$1;->this$0:Lcom/google/android/apps/chrome/document/DocumentToolbarControls;

    # getter for: Lcom/google/android/apps/chrome/document/DocumentToolbarControls;->mTab:Lorg/chromium/chrome/browser/Tab;
    invoke-static {v1}, Lcom/google/android/apps/chrome/document/DocumentToolbarControls;->access$000(Lcom/google/android/apps/chrome/document/DocumentToolbarControls;)Lorg/chromium/chrome/browser/Tab;

    move-result-object v1

    invoke-virtual {v1}, Lorg/chromium/chrome/browser/Tab;->getWebContents()Lorg/chromium/content_public/browser/WebContents;

    move-result-object v1

    invoke-static {v0, v1}, Lorg/chromium/chrome/browser/WebsiteSettingsPopup;->show(Landroid/content/Context;Lorg/chromium/content_public/browser/WebContents;)V

    .line 64
    :cond_0
    return-void
.end method
