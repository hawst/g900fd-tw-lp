.class Lcom/google/android/apps/chrome/omnibox/GeolocationTracker;
.super Ljava/lang/Object;
.source "GeolocationTracker.java"


# static fields
.field private static sListener:Lcom/google/android/apps/chrome/omnibox/GeolocationTracker$SelfCancelingListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/apps/chrome/omnibox/GeolocationTracker;->sListener:Lcom/google/android/apps/chrome/omnibox/GeolocationTracker$SelfCancelingListener;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    return-void
.end method

.method static synthetic access$102(Lcom/google/android/apps/chrome/omnibox/GeolocationTracker$SelfCancelingListener;)Lcom/google/android/apps/chrome/omnibox/GeolocationTracker$SelfCancelingListener;
    .locals 0

    .prologue
    .line 22
    sput-object p0, Lcom/google/android/apps/chrome/omnibox/GeolocationTracker;->sListener:Lcom/google/android/apps/chrome/omnibox/GeolocationTracker$SelfCancelingListener;

    return-object p0
.end method

.method static getLastKnownLocation(Landroid/content/Context;)Landroid/location/Location;
    .locals 2

    .prologue
    .line 80
    const-string/jumbo v0, "location"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    .line 82
    const-string/jumbo v1, "network"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v0

    return-object v0
.end method

.method static getLocationAge(Landroid/location/Location;)J
    .locals 4

    .prologue
    .line 72
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-virtual {p0}, Landroid/location/Location;->getTime()J

    move-result-wide v2

    sub-long/2addr v0, v2

    .line 73
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-ltz v2, :cond_0

    :goto_0
    return-wide v0

    :cond_0
    const-wide v0, 0x7fffffffffffffffL

    goto :goto_0
.end method

.method static refreshLastKnownLocation(Landroid/content/Context;J)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 91
    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    .line 94
    sget-object v0, Lcom/google/android/apps/chrome/omnibox/GeolocationTracker;->sListener:Lcom/google/android/apps/chrome/omnibox/GeolocationTracker$SelfCancelingListener;

    if-eqz v0, :cond_1

    .line 106
    :cond_0
    :goto_0
    return-void

    .line 96
    :cond_1
    const-string/jumbo v0, "location"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    .line 98
    const-string/jumbo v1, "network"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v1

    .line 99
    if-eqz v1, :cond_2

    invoke-static {v1}, Lcom/google/android/apps/chrome/omnibox/GeolocationTracker;->getLocationAge(Landroid/location/Location;)J

    move-result-wide v2

    cmp-long v1, v2, p1

    if-lez v1, :cond_0

    .line 100
    :cond_2
    const-string/jumbo v1, "network"

    .line 101
    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 102
    new-instance v2, Lcom/google/android/apps/chrome/omnibox/GeolocationTracker$SelfCancelingListener;

    invoke-direct {v2, v0, v4}, Lcom/google/android/apps/chrome/omnibox/GeolocationTracker$SelfCancelingListener;-><init>(Landroid/location/LocationManager;Lcom/google/android/apps/chrome/omnibox/GeolocationTracker$1;)V

    sput-object v2, Lcom/google/android/apps/chrome/omnibox/GeolocationTracker;->sListener:Lcom/google/android/apps/chrome/omnibox/GeolocationTracker$SelfCancelingListener;

    .line 103
    sget-object v2, Lcom/google/android/apps/chrome/omnibox/GeolocationTracker;->sListener:Lcom/google/android/apps/chrome/omnibox/GeolocationTracker$SelfCancelingListener;

    invoke-virtual {v0, v1, v2, v4}, Landroid/location/LocationManager;->requestSingleUpdate(Ljava/lang/String;Landroid/location/LocationListener;Landroid/os/Looper;)V

    goto :goto_0
.end method
