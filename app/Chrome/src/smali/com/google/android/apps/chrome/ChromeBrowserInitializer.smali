.class public Lcom/google/android/apps/chrome/ChromeBrowserInitializer;
.super Ljava/lang/Object;
.source "ChromeBrowserInitializer.java"


# static fields
.field private static sChromeApplicationDelegate:Lcom/google/android/apps/chrome/ChromeBrowserInitializer;


# instance fields
.field private final mApplication:Lcom/google/android/apps/chrome/ChromeMobileApplication;

.field private mDeferredStartupComplete:Z

.field private final mHandler:Landroid/os/Handler;

.field private mNativeInitializationComplete:Z

.field private mPartnerBookmarksShim:Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksShim;

.field private mPostInflationStartupComplete:Z

.field private mPreInflationStartupComplete:Z


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 147
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 148
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/ChromeMobileApplication;

    iput-object v0, p0, Lcom/google/android/apps/chrome/ChromeBrowserInitializer;->mApplication:Lcom/google/android/apps/chrome/ChromeMobileApplication;

    .line 149
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/ChromeBrowserInitializer;->mHandler:Landroid/os/Handler;

    .line 150
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/ChromeBrowserInitializer;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeBrowserInitializer;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/ChromeBrowserInitializer;)Lcom/google/android/apps/chrome/ChromeMobileApplication;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeBrowserInitializer;->mApplication:Lcom/google/android/apps/chrome/ChromeMobileApplication;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/ChromeBrowserInitializer;)V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/google/android/apps/chrome/ChromeBrowserInitializer;->onStartNativeInitialization()V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/apps/chrome/ChromeBrowserInitializer;)V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/google/android/apps/chrome/ChromeBrowserInitializer;->onFinishNativeInitialization()V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/apps/chrome/ChromeBrowserInitializer;)V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/google/android/apps/chrome/ChromeBrowserInitializer;->importBookmarksAfterOTAIfNeeded()V

    return-void
.end method

.method private static configureStrictMode()V
    .locals 4

    .prologue
    .line 410
    invoke-static {}, Lorg/chromium/base/CommandLine;->getInstance()Lorg/chromium/base/CommandLine;

    move-result-object v1

    .line 411
    const-string/jumbo v0, "eng"

    sget-object v2, Landroid/os/Build;->TYPE:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "strict-mode"

    invoke-virtual {v1, v0}, Lorg/chromium/base/CommandLine;->hasSwitch(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 412
    :cond_0
    invoke-static {}, Landroid/os/StrictMode;->enableDefaults()V

    .line 413
    new-instance v0, Landroid/os/StrictMode$ThreadPolicy$Builder;

    invoke-static {}, Landroid/os/StrictMode;->getThreadPolicy()Landroid/os/StrictMode$ThreadPolicy;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/os/StrictMode$ThreadPolicy$Builder;-><init>(Landroid/os/StrictMode$ThreadPolicy;)V

    .line 415
    invoke-virtual {v0}, Landroid/os/StrictMode$ThreadPolicy$Builder;->detectAll()Landroid/os/StrictMode$ThreadPolicy$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/StrictMode$ThreadPolicy$Builder;->penaltyFlashScreen()Landroid/os/StrictMode$ThreadPolicy$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/StrictMode$ThreadPolicy$Builder;->penaltyDeathOnNetwork()Landroid/os/StrictMode$ThreadPolicy$Builder;

    move-result-object v0

    .line 418
    const-string/jumbo v2, "death"

    const-string/jumbo v3, "strict-mode"

    invoke-virtual {v1, v3}, Lorg/chromium/base/CommandLine;->getSwitchValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 419
    invoke-virtual {v0}, Landroid/os/StrictMode$ThreadPolicy$Builder;->penaltyDeath()Landroid/os/StrictMode$ThreadPolicy$Builder;

    move-result-object v0

    .line 420
    new-instance v1, Landroid/os/StrictMode$VmPolicy$Builder;

    invoke-static {}, Landroid/os/StrictMode;->getVmPolicy()Landroid/os/StrictMode$VmPolicy;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/StrictMode$VmPolicy$Builder;-><init>(Landroid/os/StrictMode$VmPolicy;)V

    invoke-virtual {v1}, Landroid/os/StrictMode$VmPolicy$Builder;->penaltyDeath()Landroid/os/StrictMode$VmPolicy$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/StrictMode$VmPolicy$Builder;->build()Landroid/os/StrictMode$VmPolicy;

    move-result-object v1

    invoke-static {v1}, Landroid/os/StrictMode;->setVmPolicy(Landroid/os/StrictMode$VmPolicy;)V

    .line 424
    :cond_1
    invoke-virtual {v0}, Landroid/os/StrictMode$ThreadPolicy$Builder;->build()Landroid/os/StrictMode$ThreadPolicy;

    move-result-object v0

    invoke-static {v0}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    .line 426
    :cond_2
    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/ChromeBrowserInitializer;
    .locals 1

    .prologue
    .line 141
    sget-object v0, Lcom/google/android/apps/chrome/ChromeBrowserInitializer;->sChromeApplicationDelegate:Lcom/google/android/apps/chrome/ChromeBrowserInitializer;

    if-nez v0, :cond_0

    .line 142
    new-instance v0, Lcom/google/android/apps/chrome/ChromeBrowserInitializer;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/ChromeBrowserInitializer;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/apps/chrome/ChromeBrowserInitializer;->sChromeApplicationDelegate:Lcom/google/android/apps/chrome/ChromeBrowserInitializer;

    .line 144
    :cond_0
    sget-object v0, Lcom/google/android/apps/chrome/ChromeBrowserInitializer;->sChromeApplicationDelegate:Lcom/google/android/apps/chrome/ChromeBrowserInitializer;

    return-object v0
.end method

.method private importBookmarksAfterOTAIfNeeded()V
    .locals 2

    .prologue
    .line 404
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeBrowserInitializer;->mApplication:Lcom/google/android/apps/chrome/ChromeMobileApplication;

    invoke-static {v0}, Lcom/google/android/apps/chrome/bookmark/AndroidBrowserImporter;->isDatabaseFilePresent(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 407
    :goto_0
    return-void

    .line 405
    :cond_0
    new-instance v0, Lcom/google/android/apps/chrome/bookmark/AndroidBrowserImporter;

    iget-object v1, p0, Lcom/google/android/apps/chrome/ChromeBrowserInitializer;->mApplication:Lcom/google/android/apps/chrome/ChromeMobileApplication;

    invoke-direct {v0, v1}, Lcom/google/android/apps/chrome/bookmark/AndroidBrowserImporter;-><init>(Landroid/content/Context;)V

    .line 406
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/bookmark/AndroidBrowserImporter;->importFromDatabaseAfterOTA()Z

    goto :goto_0
.end method

.method private onFinishNativeInitialization()V
    .locals 1

    .prologue
    .line 393
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/ChromeBrowserInitializer;->mNativeInitializationComplete:Z

    if-eqz v0, :cond_0

    .line 400
    :goto_0
    return-void

    .line 396
    :cond_0
    new-instance v0, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksShim;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksShim;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/ChromeBrowserInitializer;->mPartnerBookmarksShim:Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksShim;

    .line 398
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/ChromeBrowserInitializer;->mNativeInitializationComplete:Z

    .line 399
    new-instance v0, Lorg/chromium/chrome/browser/FileProviderHelper;

    invoke-direct {v0}, Lorg/chromium/chrome/browser/FileProviderHelper;-><init>()V

    invoke-static {v0}, Lorg/chromium/base/ContentUriUtils;->setFileProviderUtil(Lorg/chromium/base/ContentUriUtils$FileProviderUtil;)V

    goto :goto_0
.end method

.method private onStartNativeInitialization()V
    .locals 2

    .prologue
    .line 380
    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    .line 381
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/ChromeBrowserInitializer;->mNativeInitializationComplete:Z

    if-eqz v0, :cond_0

    .line 390
    :goto_0
    return-void

    .line 383
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeBrowserInitializer;->mApplication:Lcom/google/android/apps/chrome/ChromeMobileApplication;

    invoke-static {v0}, Lorg/chromium/content/browser/SpeechRecognition;->initialize(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 384
    invoke-static {}, Lorg/chromium/base/CommandLine;->getInstance()Lorg/chromium/base/CommandLine;

    move-result-object v0

    const-string/jumbo v1, "enable-speech-recognition"

    invoke-virtual {v0, v1}, Lorg/chromium/base/CommandLine;->appendSwitch(Ljava/lang/String;)V

    .line 388
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeBrowserInitializer;->mApplication:Lcom/google/android/apps/chrome/ChromeMobileApplication;

    invoke-static {v0}, Lorg/chromium/net/NetworkChangeNotifier;->init(Landroid/content/Context;)Lorg/chromium/net/NetworkChangeNotifier;

    .line 389
    const/4 v0, 0x1

    invoke-static {v0}, Lorg/chromium/net/NetworkChangeNotifier;->setAutoDetectConnectivityState(Z)V

    goto :goto_0
.end method

.method private postInflationStartup()V
    .locals 3

    .prologue
    .line 186
    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    .line 187
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/ChromeBrowserInitializer;->mPostInflationStartupComplete:Z

    if-eqz v0, :cond_0

    .line 207
    :goto_0
    return-void

    .line 192
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeBrowserInitializer;->mApplication:Lcom/google/android/apps/chrome/ChromeMobileApplication;

    invoke-static {v0}, Lcom/google/android/apps/chrome/omaha/OmahaClient;->isFreshInstallOrDataHasBeenCleared(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 193
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeBrowserInitializer;->mApplication:Lcom/google/android/apps/chrome/ChromeMobileApplication;

    invoke-static {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->setAllowLowEndDeviceUi()V

    .line 196
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeBrowserInitializer;->mApplication:Lcom/google/android/apps/chrome/ChromeMobileApplication;

    invoke-static {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getAllowLowEndDeviceUi()Z

    move-result v0

    if-nez v0, :cond_2

    .line 197
    invoke-static {}, Lorg/chromium/base/CommandLine;->getInstance()Lorg/chromium/base/CommandLine;

    move-result-object v0

    const-string/jumbo v1, "low-end-device-mode"

    const-string/jumbo v2, "0"

    invoke-virtual {v0, v1, v2}, Lorg/chromium/base/CommandLine;->appendSwitchWithValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 204
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeBrowserInitializer;->mApplication:Lcom/google/android/apps/chrome/ChromeMobileApplication;

    invoke-static {v0}, Lorg/chromium/base/ResourceExtractor;->get(Landroid/content/Context;)Lorg/chromium/base/ResourceExtractor;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/base/ResourceExtractor;->startExtractingResources()V

    .line 206
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/ChromeBrowserInitializer;->mPostInflationStartupComplete:Z

    goto :goto_0
.end method

.method private preInflationStartup()V
    .locals 1

    .prologue
    .line 167
    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    .line 168
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/ChromeBrowserInitializer;->mPreInflationStartupComplete:Z

    if-eqz v0, :cond_0

    .line 183
    :goto_0
    return-void

    .line 173
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeBrowserInitializer;->mApplication:Lcom/google/android/apps/chrome/ChromeMobileApplication;

    invoke-static {v0}, Lcom/google/android/apps/chrome/ChromeMobileApplication;->initCommandLine(Landroid/content/Context;)V

    .line 174
    invoke-direct {p0}, Lcom/google/android/apps/chrome/ChromeBrowserInitializer;->waitForDebuggerIfNeeded()V

    .line 175
    invoke-static {}, Lcom/google/android/apps/chrome/ChromeBrowserInitializer;->configureStrictMode()V

    .line 178
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeBrowserInitializer;->mApplication:Lcom/google/android/apps/chrome/ChromeMobileApplication;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    .line 180
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeBrowserInitializer;->mApplication:Lcom/google/android/apps/chrome/ChromeMobileApplication;

    invoke-static {v0}, Lorg/chromium/content/browser/DeviceUtils;->addDeviceSpecificUserAgentSwitch(Landroid/content/Context;)V

    .line 182
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/ChromeBrowserInitializer;->mPreInflationStartupComplete:Z

    goto :goto_0
.end method

.method private startChromeBrowserProcesses(Lorg/chromium/content/browser/BrowserStartupController$StartupCallback;)V
    .locals 1

    .prologue
    .line 370
    invoke-static {}, Lorg/chromium/base/TraceEvent;->begin()V

    .line 372
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeBrowserInitializer;->mApplication:Lcom/google/android/apps/chrome/ChromeMobileApplication;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/ChromeMobileApplication;->startChromeBrowserProcessesAsync(Lorg/chromium/content/browser/BrowserStartupController$StartupCallback;)V
    :try_end_0
    .catch Lorg/chromium/base/library_loader/ProcessInitException; {:try_start_0 .. :try_end_0} :catch_0

    .line 376
    :goto_0
    invoke-static {}, Lorg/chromium/base/TraceEvent;->end()V

    .line 377
    return-void

    .line 373
    :catch_0
    move-exception v0

    .line 374
    invoke-static {v0}, Lcom/google/android/apps/chrome/ChromeMobileApplication;->reportStartupErrorAndExit(Lorg/chromium/base/library_loader/ProcessInitException;)V

    goto :goto_0
.end method

.method private waitForDebuggerIfNeeded()V
    .locals 2

    .prologue
    .line 429
    invoke-static {}, Lorg/chromium/base/CommandLine;->getInstance()Lorg/chromium/base/CommandLine;

    move-result-object v0

    const-string/jumbo v1, "wait-for-java-debugger"

    invoke-virtual {v0, v1}, Lorg/chromium/base/CommandLine;->hasSwitch(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 430
    const-string/jumbo v0, "ChromeBrowserInitializer"

    const-string/jumbo v1, "Waiting for Java debugger to connect..."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 431
    invoke-static {}, Landroid/os/Debug;->waitForDebugger()V

    .line 432
    const-string/jumbo v0, "ChromeBrowserInitializer"

    const-string/jumbo v1, "Java debugger connected. Resuming execution."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 434
    :cond_0
    return-void
.end method


# virtual methods
.method public handlePostNativeStartup(Lcom/google/android/apps/chrome/ChromeBrowserInitializer$BrowserParts;)V
    .locals 2

    .prologue
    .line 216
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 232
    new-instance v1, Lcom/google/android/apps/chrome/ChromeBrowserInitializer$1;

    invoke-direct {v1, p0, v0}, Lcom/google/android/apps/chrome/ChromeBrowserInitializer$1;-><init>(Lcom/google/android/apps/chrome/ChromeBrowserInitializer;Ljava/util/LinkedList;)V

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 239
    new-instance v1, Lcom/google/android/apps/chrome/ChromeBrowserInitializer$2;

    invoke-direct {v1, p0, v0}, Lcom/google/android/apps/chrome/ChromeBrowserInitializer$2;-><init>(Lcom/google/android/apps/chrome/ChromeBrowserInitializer;Ljava/util/LinkedList;)V

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 246
    new-instance v1, Lcom/google/android/apps/chrome/ChromeBrowserInitializer$3;

    invoke-direct {v1, p0, v0, p1}, Lcom/google/android/apps/chrome/ChromeBrowserInitializer$3;-><init>(Lcom/google/android/apps/chrome/ChromeBrowserInitializer;Ljava/util/LinkedList;Lcom/google/android/apps/chrome/ChromeBrowserInitializer$BrowserParts;)V

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 254
    new-instance v1, Lcom/google/android/apps/chrome/ChromeBrowserInitializer$4;

    invoke-direct {v1, p0, v0, p1}, Lcom/google/android/apps/chrome/ChromeBrowserInitializer$4;-><init>(Lcom/google/android/apps/chrome/ChromeBrowserInitializer;Ljava/util/LinkedList;Lcom/google/android/apps/chrome/ChromeBrowserInitializer$BrowserParts;)V

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 262
    new-instance v1, Lcom/google/android/apps/chrome/ChromeBrowserInitializer$5;

    invoke-direct {v1, p0, v0}, Lcom/google/android/apps/chrome/ChromeBrowserInitializer$5;-><init>(Lcom/google/android/apps/chrome/ChromeBrowserInitializer;Ljava/util/LinkedList;)V

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 269
    new-instance v1, Lcom/google/android/apps/chrome/ChromeBrowserInitializer$6;

    invoke-direct {v1, p0, v0, p1}, Lcom/google/android/apps/chrome/ChromeBrowserInitializer$6;-><init>(Lcom/google/android/apps/chrome/ChromeBrowserInitializer;Ljava/util/LinkedList;Lcom/google/android/apps/chrome/ChromeBrowserInitializer$BrowserParts;)V

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 280
    new-instance v1, Lcom/google/android/apps/chrome/ChromeBrowserInitializer$7;

    invoke-direct {v1, p0, p1, v0}, Lcom/google/android/apps/chrome/ChromeBrowserInitializer$7;-><init>(Lcom/google/android/apps/chrome/ChromeBrowserInitializer;Lcom/google/android/apps/chrome/ChromeBrowserInitializer$BrowserParts;Ljava/util/LinkedList;)V

    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/ChromeBrowserInitializer;->startChromeBrowserProcesses(Lorg/chromium/content/browser/BrowserStartupController$StartupCallback;)V

    .line 291
    return-void
.end method

.method public handlePreNativeStartup(Lcom/google/android/apps/chrome/ChromeBrowserInitializer$BrowserParts;)V
    .locals 0

    .prologue
    .line 159
    invoke-direct {p0}, Lcom/google/android/apps/chrome/ChromeBrowserInitializer;->preInflationStartup()V

    .line 160
    invoke-interface {p1}, Lcom/google/android/apps/chrome/ChromeBrowserInitializer$BrowserParts;->preInflationStartup()V

    .line 161
    invoke-interface {p1}, Lcom/google/android/apps/chrome/ChromeBrowserInitializer$BrowserParts;->setContentViewAndLoadLibrary()V

    .line 162
    invoke-direct {p0}, Lcom/google/android/apps/chrome/ChromeBrowserInitializer;->postInflationStartup()V

    .line 163
    invoke-interface {p1}, Lcom/google/android/apps/chrome/ChromeBrowserInitializer$BrowserParts;->postInflationStartup()V

    .line 164
    return-void
.end method

.method public onDeferredStartup(Z)V
    .locals 3

    .prologue
    .line 300
    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    .line 301
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/ChromeBrowserInitializer;->mDeferredStartupComplete:Z

    if-eqz v0, :cond_0

    .line 366
    :goto_0
    return-void

    .line 304
    :cond_0
    new-instance v0, Lcom/google/android/apps/chrome/ChromeBrowserInitializer$8;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/chrome/ChromeBrowserInitializer$8;-><init>(Lcom/google/android/apps/chrome/ChromeBrowserInitializer;Z)V

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/ChromeBrowserInitializer$8;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 344
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeBrowserInitializer;->mPartnerBookmarksShim:Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksShim;

    iget-object v1, p0, Lcom/google/android/apps/chrome/ChromeBrowserInitializer;->mApplication:Lcom/google/android/apps/chrome/ChromeMobileApplication;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksShim;->initialize(Landroid/content/Context;)V

    .line 347
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeBrowserInitializer;->mPartnerBookmarksShim:Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksShim;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/partnerbookmarks/PartnerBookmarksShim;->kickOffReading()V

    .line 349
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeBrowserInitializer;->mApplication:Lcom/google/android/apps/chrome/ChromeMobileApplication;

    invoke-static {v0}, Lorg/chromium/base/PowerMonitor;->create(Landroid/content/Context;)V

    .line 352
    invoke-static {}, Lcom/google/android/apps/chrome/autofill/AutofillDialogControllerImpl;->setAsDialogFactory()V

    .line 354
    invoke-static {}, Lcom/google/android/apps/chrome/icing/IcingProvider;->start()V

    .line 356
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeBrowserInitializer;->mApplication:Lcom/google/android/apps/chrome/ChromeMobileApplication;

    invoke-static {v0}, Lcom/google/android/apps/chrome/DownloadManagerService;->getDownloadManagerService(Landroid/content/Context;)Lcom/google/android/apps/chrome/DownloadManagerService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/DownloadManagerService;->clearPendingDownloadNotifications()V

    .line 359
    invoke-static {}, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->registerAccountManagementScreenManager()V

    .line 360
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeBrowserInitializer;->mApplication:Lcom/google/android/apps/chrome/ChromeMobileApplication;

    invoke-static {}, Lorg/chromium/chrome/browser/profiles/Profile;->getLastUsedProfile()Lorg/chromium/chrome/browser/profiles/Profile;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->prefetchUserNamePicture(Landroid/content/Context;Lorg/chromium/chrome/browser/profiles/Profile;)V

    .line 363
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeBrowserInitializer;->mApplication:Lcom/google/android/apps/chrome/ChromeMobileApplication;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeMobileApplication;->initializeSharedClasses()V

    .line 365
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/ChromeBrowserInitializer;->mDeferredStartupComplete:Z

    goto :goto_0
.end method
