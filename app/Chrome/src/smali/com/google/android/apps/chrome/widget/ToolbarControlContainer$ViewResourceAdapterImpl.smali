.class Lcom/google/android/apps/chrome/widget/ToolbarControlContainer$ViewResourceAdapterImpl;
.super Lcom/google/android/apps/chrome/widget/ControlContainer$ControlContainerResourceAdapter;
.source "ToolbarControlContainer.java"


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;)V
    .locals 0

    .prologue
    .line 127
    iput-object p1, p0, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer$ViewResourceAdapterImpl;->this$0:Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/widget/ControlContainer$ControlContainerResourceAdapter;-><init>(Lcom/google/android/apps/chrome/widget/ControlContainer;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;Lcom/google/android/apps/chrome/widget/ToolbarControlContainer$1;)V
    .locals 0

    .prologue
    .line 127
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer$ViewResourceAdapterImpl;-><init>(Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;)V

    return-void
.end method


# virtual methods
.method protected computeContentAperture(Landroid/graphics/Rect;)V
    .locals 3

    .prologue
    .line 160
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer$ViewResourceAdapterImpl;->this$0:Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;

    # getter for: Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->mToolbar:Lcom/google/android/apps/chrome/toolbar/Toolbar;
    invoke-static {v0}, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->access$300(Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;)Lcom/google/android/apps/chrome/toolbar/Toolbar;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/toolbar/Toolbar;->getDelegate()Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->getLocationBarContentRect(Landroid/graphics/Rect;)V

    .line 161
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer$ViewResourceAdapterImpl;->this$0:Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer$ViewResourceAdapterImpl;->this$0:Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;

    # getter for: Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->mToolbar:Lcom/google/android/apps/chrome/toolbar/Toolbar;
    invoke-static {v1}, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->access$300(Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;)Lcom/google/android/apps/chrome/toolbar/Toolbar;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/chrome/toolbar/Toolbar;->getView()Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer$ViewResourceAdapterImpl;->this$0:Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;

    # getter for: Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->mTempPosition:[I
    invoke-static {v2}, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->access$400(Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;)[I

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/utilities/ViewUtils;->getRelativeDrawPosition(Landroid/view/View;Landroid/view/View;[I)V

    .line 163
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer$ViewResourceAdapterImpl;->this$0:Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;

    # getter for: Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->mTempPosition:[I
    invoke-static {v0}, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->access$400(Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;)[I

    move-result-object v0

    const/4 v1, 0x0

    aget v0, v0, v1

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer$ViewResourceAdapterImpl;->this$0:Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;

    # getter for: Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->mTempPosition:[I
    invoke-static {v1}, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->access$400(Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;)[I

    move-result-object v1

    const/4 v2, 0x1

    aget v1, v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Rect;->offset(II)V

    .line 164
    return-void
.end method

.method public forceInvalidate()V
    .locals 1

    .prologue
    .line 133
    const/4 v0, 0x0

    invoke-super {p0, v0}, Lcom/google/android/apps/chrome/widget/ControlContainer$ControlContainerResourceAdapter;->invalidate(Landroid/graphics/Rect;)V

    .line 134
    return-void
.end method

.method public invalidate(Landroid/graphics/Rect;)V
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer$ViewResourceAdapterImpl;->this$0:Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;

    # getter for: Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->mToolbar:Lcom/google/android/apps/chrome/toolbar/Toolbar;
    invoke-static {v0}, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->access$300(Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;)Lcom/google/android/apps/chrome/toolbar/Toolbar;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/toolbar/Toolbar;->getDelegate()Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->isOverviewMode()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer$ViewResourceAdapterImpl;->this$0:Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;

    # getter for: Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->mToolbar:Lcom/google/android/apps/chrome/toolbar/Toolbar;
    invoke-static {v0}, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->access$300(Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;)Lcom/google/android/apps/chrome/toolbar/Toolbar;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/toolbar/Toolbar;->getDelegate()Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->isAnimatingForOverview()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 144
    :cond_0
    :goto_0
    return-void

    .line 143
    :cond_1
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/widget/ControlContainer$ControlContainerResourceAdapter;->invalidate(Landroid/graphics/Rect;)V

    goto :goto_0
.end method

.method protected onCaptureEnd()V
    .locals 2

    .prologue
    .line 155
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer$ViewResourceAdapterImpl;->this$0:Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;

    # getter for: Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->mToolbar:Lcom/google/android/apps/chrome/toolbar/Toolbar;
    invoke-static {v0}, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->access$300(Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;)Lcom/google/android/apps/chrome/toolbar/Toolbar;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/toolbar/Toolbar;->getDelegate()Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->setTextureCaptureMode(Z)V

    .line 156
    return-void
.end method

.method protected onCaptureStart(Landroid/graphics/Canvas;Landroid/graphics/Rect;)V
    .locals 2

    .prologue
    .line 148
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer$ViewResourceAdapterImpl;->this$0:Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;

    # getter for: Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->mToolbar:Lcom/google/android/apps/chrome/toolbar/Toolbar;
    invoke-static {v0}, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->access$300(Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;)Lcom/google/android/apps/chrome/toolbar/Toolbar;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/toolbar/Toolbar;->getDelegate()Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->setTextureCaptureMode(Z)V

    .line 150
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/chrome/widget/ControlContainer$ControlContainerResourceAdapter;->onCaptureStart(Landroid/graphics/Canvas;Landroid/graphics/Rect;)V

    .line 151
    return-void
.end method
