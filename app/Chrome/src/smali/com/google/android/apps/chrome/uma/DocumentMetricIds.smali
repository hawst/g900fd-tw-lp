.class public Lcom/google/android/apps/chrome/uma/DocumentMetricIds;
.super Ljava/lang/Object;
.source "DocumentMetricIds.java"


# static fields
.field public static final CLICK_ACTION_COUNT:I = 0x5

.field public static final CLICK_ACTION_HOME_SEARCH_BUTTON:I = 0x1

.field public static final CLICK_ACTION_MENU_BUTTON:I = 0x2

.field public static final CLICK_ACTION_RELOAD_BUTTON:I = 0x3

.field public static final CLICK_ACTION_TITLE:I = 0x0

.field public static final CLICK_ACTION_TITLE_LONG:I = 0x4

.field public static final EDIT_URL_ACTION_CHANGED_DOMAIN:I = 0x0

.field public static final EDIT_URL_ACTION_COPY_URL:I = 0x2

.field public static final EDIT_URL_ACTION_COUNT:I = 0x5

.field public static final EDIT_URL_ACTION_DISMISSED:I = 0x4

.field public static final EDIT_URL_ACTION_EMPTY_FIELD:I = 0x1

.field public static final EDIT_URL_ACTION_GO_PRESSED:I = 0x3

.field public static final HOME_EXIT_ACTION_BOOKMARKS_BUTTON:I = 0x3

.field public static final HOME_EXIT_ACTION_COUNT:I = 0x6

.field public static final HOME_EXIT_ACTION_LAST_VIEWED_ITEM:I = 0x2

.field public static final HOME_EXIT_ACTION_MOST_VISITED_ITEM:I = 0x1

.field public static final HOME_EXIT_ACTION_OTHER:I = 0x0

.field public static final HOME_EXIT_ACTION_RECENT_TABS_BUTTON:I = 0x4

.field public static final HOME_EXIT_ACTION_SEARCHBOX:I = 0x5

.field public static final OPT_OUT_CLICK_COUNT:I = 0x2

.field public static final OPT_OUT_CLICK_GOT_IT:I = 0x0

.field public static final OPT_OUT_CLICK_SETTINGS:I = 0x1

.field public static final STARTED_BY_ACTIVITY_BROUGHT_TO_FOREGROUND:I = 0x3

.field public static final STARTED_BY_ACTIVITY_RESTARTED:I = 0x2

.field public static final STARTED_BY_CHROME_HOME_BOOKMARK:I = 0x65

.field public static final STARTED_BY_CHROME_HOME_MOST_VISITED:I = 0x64

.field public static final STARTED_BY_CHROME_HOME_RECENT_TABS:I = 0x66

.field public static final STARTED_BY_CONTEXTUAL_SEARCH:I = 0x1f4

.field public static final STARTED_BY_CONTEXT_MENU:I = 0xc9

.field public static final STARTED_BY_EXTERNAL_APP_CHROME:I = 0x194

.field public static final STARTED_BY_EXTERNAL_APP_FACEBOOK:I = 0x191

.field public static final STARTED_BY_EXTERNAL_APP_GMAIL:I = 0x190

.field public static final STARTED_BY_EXTERNAL_APP_OTHER:I = 0x195

.field public static final STARTED_BY_EXTERNAL_APP_PLUS:I = 0x192

.field public static final STARTED_BY_EXTERNAL_APP_TWITTER:I = 0x193

.field public static final STARTED_BY_LAUNCHER:I = 0x1

.field public static final STARTED_BY_OPTIONS_MENU:I = 0xca

.field public static final STARTED_BY_SEARCH_RESULT_PAGE:I = 0x12c

.field public static final STARTED_BY_SEARCH_SUGGESTION_CHROME:I = 0x12e

.field public static final STARTED_BY_SEARCH_SUGGESTION_EXTERNAL:I = 0x12d

.field public static final STARTED_BY_UNKNOWN:I = 0x0

.field public static final STARTED_BY_WINDOW_OPEN:I = 0xc8


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
