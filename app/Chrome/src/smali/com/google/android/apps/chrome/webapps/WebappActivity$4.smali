.class Lcom/google/android/apps/chrome/webapps/WebappActivity$4;
.super Lorg/chromium/chrome/browser/EmptyTabObserver;
.source "WebappActivity.java"


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/webapps/WebappActivity;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/webapps/WebappActivity;)V
    .locals 0

    .prologue
    .line 288
    iput-object p1, p0, Lcom/google/android/apps/chrome/webapps/WebappActivity$4;->this$0:Lcom/google/android/apps/chrome/webapps/WebappActivity;

    invoke-direct {p0}, Lorg/chromium/chrome/browser/EmptyTabObserver;-><init>()V

    return-void
.end method


# virtual methods
.method public onDidChangeThemeColor(I)V
    .locals 2

    .prologue
    .line 303
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/WebappActivity$4;->this$0:Lcom/google/android/apps/chrome/webapps/WebappActivity;

    # invokes: Lcom/google/android/apps/chrome/webapps/WebappActivity;->isWebappDomain()Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/webapps/WebappActivity;->access$300(Lcom/google/android/apps/chrome/webapps/WebappActivity;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 306
    :goto_0
    return-void

    .line 304
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/WebappActivity$4;->this$0:Lcom/google/android/apps/chrome/webapps/WebappActivity;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    # setter for: Lcom/google/android/apps/chrome/webapps/WebappActivity;->mBrandColor:Ljava/lang/Integer;
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/webapps/WebappActivity;->access$402(Lcom/google/android/apps/chrome/webapps/WebappActivity;Ljava/lang/Integer;)Ljava/lang/Integer;

    .line 305
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/WebappActivity$4;->this$0:Lcom/google/android/apps/chrome/webapps/WebappActivity;

    # invokes: Lcom/google/android/apps/chrome/webapps/WebappActivity;->updateTaskDescription()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/webapps/WebappActivity;->access$500(Lcom/google/android/apps/chrome/webapps/WebappActivity;)V

    goto :goto_0
.end method

.method public onDidStartProvisionalLoadForFrame(Lorg/chromium/chrome/browser/Tab;JJZLjava/lang/String;ZZ)V
    .locals 1

    .prologue
    .line 298
    if-eqz p6, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/WebappActivity$4;->this$0:Lcom/google/android/apps/chrome/webapps/WebappActivity;

    # invokes: Lcom/google/android/apps/chrome/webapps/WebappActivity;->updateUrlBar()Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/webapps/WebappActivity;->access$000(Lcom/google/android/apps/chrome/webapps/WebappActivity;)Z

    .line 299
    :cond_0
    return-void
.end method

.method public onFaviconUpdated(Lorg/chromium/chrome/browser/Tab;)V
    .locals 1

    .prologue
    .line 316
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/WebappActivity$4;->this$0:Lcom/google/android/apps/chrome/webapps/WebappActivity;

    # invokes: Lcom/google/android/apps/chrome/webapps/WebappActivity;->isWebappDomain()Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/webapps/WebappActivity;->access$300(Lcom/google/android/apps/chrome/webapps/WebappActivity;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 318
    :goto_0
    return-void

    .line 317
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/WebappActivity$4;->this$0:Lcom/google/android/apps/chrome/webapps/WebappActivity;

    # invokes: Lcom/google/android/apps/chrome/webapps/WebappActivity;->updateTaskDescription()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/webapps/WebappActivity;->access$500(Lcom/google/android/apps/chrome/webapps/WebappActivity;)V

    goto :goto_0
.end method

.method public onSSLStateUpdated(Lorg/chromium/chrome/browser/Tab;)V
    .locals 1

    .prologue
    .line 291
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/WebappActivity$4;->this$0:Lcom/google/android/apps/chrome/webapps/WebappActivity;

    # invokes: Lcom/google/android/apps/chrome/webapps/WebappActivity;->updateUrlBar()Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/webapps/WebappActivity;->access$000(Lcom/google/android/apps/chrome/webapps/WebappActivity;)Z

    .line 292
    return-void
.end method

.method public onTitleUpdated(Lorg/chromium/chrome/browser/Tab;)V
    .locals 1

    .prologue
    .line 310
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/WebappActivity$4;->this$0:Lcom/google/android/apps/chrome/webapps/WebappActivity;

    # invokes: Lcom/google/android/apps/chrome/webapps/WebappActivity;->isWebappDomain()Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/webapps/WebappActivity;->access$300(Lcom/google/android/apps/chrome/webapps/WebappActivity;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 312
    :goto_0
    return-void

    .line 311
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/WebappActivity$4;->this$0:Lcom/google/android/apps/chrome/webapps/WebappActivity;

    # invokes: Lcom/google/android/apps/chrome/webapps/WebappActivity;->updateTaskDescription()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/webapps/WebappActivity;->access$500(Lcom/google/android/apps/chrome/webapps/WebappActivity;)V

    goto :goto_0
.end method
