.class public Lcom/google/android/apps/chrome/compositor/TransitionPageContainer;
.super Ljava/lang/Object;
.source "TransitionPageContainer.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

.field private mNativeTransitionPageContainerPtr:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    const-class v0, Lcom/google/android/apps/chrome/compositor/TransitionPageContainer;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/compositor/TransitionPageContainer;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(J)V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-wide p1, p0, Lcom/google/android/apps/chrome/compositor/TransitionPageContainer;->mNativeTransitionPageContainerPtr:J

    .line 27
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/compositor/TransitionPageContainer;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    .line 28
    return-void
.end method

.method private static create(J)Lcom/google/android/apps/chrome/compositor/TransitionPageContainer;
    .locals 2

    .prologue
    .line 79
    new-instance v0, Lcom/google/android/apps/chrome/compositor/TransitionPageContainer;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/chrome/compositor/TransitionPageContainer;-><init>(J)V

    return-object v0
.end method

.method private destroy()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 84
    sget-boolean v0, Lcom/google/android/apps/chrome/compositor/TransitionPageContainer;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/apps/chrome/compositor/TransitionPageContainer;->mNativeTransitionPageContainerPtr:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 85
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/compositor/TransitionPageContainer;->releaseContentViewCore()V

    .line 86
    iput-wide v2, p0, Lcom/google/android/apps/chrome/compositor/TransitionPageContainer;->mNativeTransitionPageContainerPtr:J

    .line 87
    return-void
.end method

.method private native nativeAddChild(JLorg/chromium/content/browser/ContentViewCore;)V
.end method

.method private native nativeHide(JLorg/chromium/content/browser/ContentViewCore;)V
.end method

.method private native nativeRemoveChild(JLorg/chromium/content/browser/ContentViewCore;)V
.end method

.method private native nativeShow(JLorg/chromium/content/browser/ContentViewCore;)V
.end method

.method private releaseContentViewCore()V
    .locals 4

    .prologue
    .line 68
    iget-wide v0, p0, Lcom/google/android/apps/chrome/compositor/TransitionPageContainer;->mNativeTransitionPageContainerPtr:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 75
    :goto_0
    return-void

    .line 70
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/TransitionPageContainer;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    if-eqz v0, :cond_1

    .line 71
    iget-wide v0, p0, Lcom/google/android/apps/chrome/compositor/TransitionPageContainer;->mNativeTransitionPageContainerPtr:J

    iget-object v2, p0, Lcom/google/android/apps/chrome/compositor/TransitionPageContainer;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/apps/chrome/compositor/TransitionPageContainer;->nativeRemoveChild(JLorg/chromium/content/browser/ContentViewCore;)V

    .line 74
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/compositor/TransitionPageContainer;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    goto :goto_0
.end method


# virtual methods
.method public setContentViewCore(Lorg/chromium/content/browser/ContentViewCore;)V
    .locals 4

    .prologue
    .line 57
    iget-wide v0, p0, Lcom/google/android/apps/chrome/compositor/TransitionPageContainer;->mNativeTransitionPageContainerPtr:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 65
    :cond_0
    :goto_0
    return-void

    .line 59
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/chrome/compositor/TransitionPageContainer;->releaseContentViewCore()V

    .line 60
    iput-object p1, p0, Lcom/google/android/apps/chrome/compositor/TransitionPageContainer;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    .line 62
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/TransitionPageContainer;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    if-eqz v0, :cond_0

    .line 63
    iget-wide v0, p0, Lcom/google/android/apps/chrome/compositor/TransitionPageContainer;->mNativeTransitionPageContainerPtr:J

    iget-object v2, p0, Lcom/google/android/apps/chrome/compositor/TransitionPageContainer;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/apps/chrome/compositor/TransitionPageContainer;->nativeAddChild(JLorg/chromium/content/browser/ContentViewCore;)V

    goto :goto_0
.end method

.method public setVisible(Z)V
    .locals 4

    .prologue
    .line 42
    iget-wide v0, p0, Lcom/google/android/apps/chrome/compositor/TransitionPageContainer;->mNativeTransitionPageContainerPtr:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 50
    :cond_0
    :goto_0
    return-void

    .line 43
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/TransitionPageContainer;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    if-eqz v0, :cond_0

    .line 45
    if-eqz p1, :cond_2

    .line 46
    iget-wide v0, p0, Lcom/google/android/apps/chrome/compositor/TransitionPageContainer;->mNativeTransitionPageContainerPtr:J

    iget-object v2, p0, Lcom/google/android/apps/chrome/compositor/TransitionPageContainer;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/apps/chrome/compositor/TransitionPageContainer;->nativeShow(JLorg/chromium/content/browser/ContentViewCore;)V

    goto :goto_0

    .line 48
    :cond_2
    iget-wide v0, p0, Lcom/google/android/apps/chrome/compositor/TransitionPageContainer;->mNativeTransitionPageContainerPtr:J

    iget-object v2, p0, Lcom/google/android/apps/chrome/compositor/TransitionPageContainer;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/apps/chrome/compositor/TransitionPageContainer;->nativeHide(JLorg/chromium/content/browser/ContentViewCore;)V

    goto :goto_0
.end method
