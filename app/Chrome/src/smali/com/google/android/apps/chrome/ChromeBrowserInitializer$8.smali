.class Lcom/google/android/apps/chrome/ChromeBrowserInitializer$8;
.super Landroid/os/AsyncTask;
.source "ChromeBrowserInitializer.java"


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/ChromeBrowserInitializer;

.field final synthetic val$crashDumpUploadingDisabled:Z


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/ChromeBrowserInitializer;Z)V
    .locals 0

    .prologue
    .line 304
    iput-object p1, p0, Lcom/google/android/apps/chrome/ChromeBrowserInitializer$8;->this$0:Lcom/google/android/apps/chrome/ChromeBrowserInitializer;

    iput-boolean p2, p0, Lcom/google/android/apps/chrome/ChromeBrowserInitializer$8;->val$crashDumpUploadingDisabled:Z

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 304
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/ChromeBrowserInitializer$8;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 2

    .prologue
    .line 307
    invoke-static {}, Lorg/chromium/base/TraceEvent;->begin()V

    .line 309
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/ChromeBrowserInitializer$8;->val$crashDumpUploadingDisabled:Z

    if-eqz v0, :cond_0

    .line 310
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeBrowserInitializer$8;->this$0:Lcom/google/android/apps/chrome/ChromeBrowserInitializer;

    # getter for: Lcom/google/android/apps/chrome/ChromeBrowserInitializer;->mApplication:Lcom/google/android/apps/chrome/ChromeMobileApplication;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ChromeBrowserInitializer;->access$100(Lcom/google/android/apps/chrome/ChromeBrowserInitializer;)Lcom/google/android/apps/chrome/ChromeMobileApplication;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->disableCrashUploading()V

    .line 314
    :goto_0
    new-instance v0, Lcom/google/android/apps/chrome/crash/CrashFileManager;

    iget-object v1, p0, Lcom/google/android/apps/chrome/ChromeBrowserInitializer$8;->this$0:Lcom/google/android/apps/chrome/ChromeBrowserInitializer;

    # getter for: Lcom/google/android/apps/chrome/ChromeBrowserInitializer;->mApplication:Lcom/google/android/apps/chrome/ChromeMobileApplication;
    invoke-static {v1}, Lcom/google/android/apps/chrome/ChromeBrowserInitializer;->access$100(Lcom/google/android/apps/chrome/ChromeBrowserInitializer;)Lcom/google/android/apps/chrome/ChromeMobileApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/ChromeMobileApplication;->getCacheDir()Ljava/io/File;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/chrome/crash/CrashFileManager;-><init>(Ljava/io/File;)V

    .line 316
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/crash/CrashFileManager;->cleanOutAllNonFreshMinidumpFiles()V

    .line 318
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeBrowserInitializer$8;->this$0:Lcom/google/android/apps/chrome/ChromeBrowserInitializer;

    # getter for: Lcom/google/android/apps/chrome/ChromeBrowserInitializer;->mApplication:Lcom/google/android/apps/chrome/ChromeMobileApplication;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ChromeBrowserInitializer;->access$100(Lcom/google/android/apps/chrome/ChromeBrowserInitializer;)Lcom/google/android/apps/chrome/ChromeMobileApplication;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/crash/MinidumpUploadService;->storeBreakpadUploadAttemptsInUma(Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;)V

    .line 322
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeBrowserInitializer$8;->this$0:Lcom/google/android/apps/chrome/ChromeBrowserInitializer;

    # invokes: Lcom/google/android/apps/chrome/ChromeBrowserInitializer;->importBookmarksAfterOTAIfNeeded()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/ChromeBrowserInitializer;->access$400(Lcom/google/android/apps/chrome/ChromeBrowserInitializer;)V

    .line 326
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeBrowserInitializer$8;->this$0:Lcom/google/android/apps/chrome/ChromeBrowserInitializer;

    # getter for: Lcom/google/android/apps/chrome/ChromeBrowserInitializer;->mApplication:Lcom/google/android/apps/chrome/ChromeMobileApplication;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ChromeBrowserInitializer;->access$100(Lcom/google/android/apps/chrome/ChromeBrowserInitializer;)Lcom/google/android/apps/chrome/ChromeMobileApplication;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkThumbnailWidgetProvider;->refreshAllWidgets(Landroid/content/Context;)V

    .line 329
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeBrowserInitializer$8;->this$0:Lcom/google/android/apps/chrome/ChromeBrowserInitializer;

    # getter for: Lcom/google/android/apps/chrome/ChromeBrowserInitializer;->mApplication:Lcom/google/android/apps/chrome/ChromeMobileApplication;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ChromeBrowserInitializer;->access$100(Lcom/google/android/apps/chrome/ChromeBrowserInitializer;)Lcom/google/android/apps/chrome/ChromeMobileApplication;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->createInitializeIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 331
    iget-object v1, p0, Lcom/google/android/apps/chrome/ChromeBrowserInitializer$8;->this$0:Lcom/google/android/apps/chrome/ChromeBrowserInitializer;

    # getter for: Lcom/google/android/apps/chrome/ChromeBrowserInitializer;->mApplication:Lcom/google/android/apps/chrome/ChromeMobileApplication;
    invoke-static {v1}, Lcom/google/android/apps/chrome/ChromeBrowserInitializer;->access$100(Lcom/google/android/apps/chrome/ChromeBrowserInitializer;)Lcom/google/android/apps/chrome/ChromeMobileApplication;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/ChromeMobileApplication;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 334
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeBrowserInitializer$8;->this$0:Lcom/google/android/apps/chrome/ChromeBrowserInitializer;

    # getter for: Lcom/google/android/apps/chrome/ChromeBrowserInitializer;->mApplication:Lcom/google/android/apps/chrome/ChromeMobileApplication;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ChromeBrowserInitializer;->access$100(Lcom/google/android/apps/chrome/ChromeBrowserInitializer;)Lcom/google/android/apps/chrome/ChromeMobileApplication;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/ChromeBrowserInitializer$8;->this$0:Lcom/google/android/apps/chrome/ChromeBrowserInitializer;

    # getter for: Lcom/google/android/apps/chrome/ChromeBrowserInitializer;->mApplication:Lcom/google/android/apps/chrome/ChromeMobileApplication;
    invoke-static {v1}, Lcom/google/android/apps/chrome/ChromeBrowserInitializer;->access$100(Lcom/google/android/apps/chrome/ChromeBrowserInitializer;)Lcom/google/android/apps/chrome/ChromeMobileApplication;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/precache/PrecacheLauncher;->updatePrecachingEnabled(Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;Landroid/content/Context;)V

    .line 337
    const/16 v0, 0x26

    invoke-static {v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastNotification(I)V

    .line 339
    invoke-static {}, Lorg/chromium/base/TraceEvent;->end()V

    .line 340
    const/4 v0, 0x0

    return-object v0

    .line 312
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeBrowserInitializer$8;->this$0:Lcom/google/android/apps/chrome/ChromeBrowserInitializer;

    # getter for: Lcom/google/android/apps/chrome/ChromeBrowserInitializer;->mApplication:Lcom/google/android/apps/chrome/ChromeMobileApplication;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ChromeBrowserInitializer;->access$100(Lcom/google/android/apps/chrome/ChromeBrowserInitializer;)Lcom/google/android/apps/chrome/ChromeMobileApplication;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/crash/MinidumpUploadService;->tryUploadAllCrashDumps(Landroid/content/Context;)V

    goto :goto_0
.end method
