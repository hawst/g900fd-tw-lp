.class public final Lcom/google/android/apps/chrome/nfc/BeamController;
.super Ljava/lang/Object;
.source "BeamController.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static registerForBeam(Landroid/app/Activity;Lcom/google/android/apps/chrome/nfc/BeamProvider;)V
    .locals 3

    .prologue
    .line 24
    invoke-static {p0}, Landroid/nfc/NfcAdapter;->getDefaultAdapter(Landroid/content/Context;)Landroid/nfc/NfcAdapter;

    move-result-object v0

    .line 25
    if-nez v0, :cond_0

    .line 33
    :goto_0
    return-void

    .line 27
    :cond_0
    :try_start_0
    new-instance v1, Lcom/google/android/apps/chrome/nfc/BeamCallback;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/chrome/nfc/BeamCallback;-><init>(Landroid/app/Activity;Lcom/google/android/apps/chrome/nfc/BeamProvider;)V

    .line 28
    const/4 v2, 0x0

    new-array v2, v2, [Landroid/app/Activity;

    invoke-virtual {v0, v1, p0, v2}, Landroid/nfc/NfcAdapter;->setNdefPushMessageCallback(Landroid/nfc/NfcAdapter$CreateNdefMessageCallback;Landroid/app/Activity;[Landroid/app/Activity;)V

    .line 29
    const/4 v2, 0x0

    new-array v2, v2, [Landroid/app/Activity;

    invoke-virtual {v0, v1, p0, v2}, Landroid/nfc/NfcAdapter;->setOnNdefPushCompleteCallback(Landroid/nfc/NfcAdapter$OnNdefPushCompleteCallback;Landroid/app/Activity;[Landroid/app/Activity;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 30
    :catch_0
    move-exception v0

    .line 31
    const-string/jumbo v0, "BeamController"

    const-string/jumbo v1, "NFC registration failure. Can\'t retry, giving up."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
