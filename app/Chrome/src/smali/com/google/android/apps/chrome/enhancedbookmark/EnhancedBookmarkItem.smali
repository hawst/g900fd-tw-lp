.class Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;
.super Landroid/widget/LinearLayout;
.source "EnhancedBookmarkItem.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private mBackgroundSalientImage:Landroid/widget/ImageView;

.field private mBackgroundSolidColor:Landroid/widget/ImageView;

.field private mBookmarkId:Lorg/chromium/components/bookmarks/BookmarkId;

.field private mDelegate:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;

.field private mFolderTitleView:Landroid/widget/TextView;

.field private mMoreIcon:Landroid/widget/ImageView;

.field private mPopupMenu:Landroid/widget/ListPopupWindow;

.field private mSalientImageCallback:Lcom/google/android/apps/chrome/enhancedbookmark/ChromeEnhancedBookmarkBridge$SalientImageCallback;

.field private mTitleView:Landroid/widget/TextView;

.field private mUrlView:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    const-class v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 134
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 135
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;)Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;->mDelegate:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;)Lorg/chromium/components/bookmarks/BookmarkId;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;->mBookmarkId:Lorg/chromium/components/bookmarks/BookmarkId;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;)Landroid/widget/ListPopupWindow;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;->mPopupMenu:Landroid/widget/ListPopupWindow;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;)Lcom/google/android/apps/chrome/enhancedbookmark/ChromeEnhancedBookmarkBridge$SalientImageCallback;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;->mSalientImageCallback:Lcom/google/android/apps/chrome/enhancedbookmark/ChromeEnhancedBookmarkBridge$SalientImageCallback;

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;Lcom/google/android/apps/chrome/enhancedbookmark/ChromeEnhancedBookmarkBridge$SalientImageCallback;)Lcom/google/android/apps/chrome/enhancedbookmark/ChromeEnhancedBookmarkBridge$SalientImageCallback;
    .locals 0

    .prologue
    .line 44
    iput-object p1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;->mSalientImageCallback:Lcom/google/android/apps/chrome/enhancedbookmark/ChromeEnhancedBookmarkBridge$SalientImageCallback;

    return-object p1
.end method

.method static synthetic access$400(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;->mBackgroundSalientImage:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;->mBackgroundSolidColor:Landroid/widget/ImageView;

    return-object v0
.end method

.method private showMenu(Landroid/view/View;)V
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 193
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;->mPopupMenu:Landroid/widget/ListPopupWindow;

    if-nez v0, :cond_0

    .line 194
    new-instance v0, Landroid/widget/ListPopupWindow;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/google/android/apps/chrome/R$style;->EnhancedBookmarkMenuStyle:I

    invoke-direct {v0, v1, v9, v7, v2}, Landroid/widget/ListPopupWindow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;->mPopupMenu:Landroid/widget/ListPopupWindow;

    .line 196
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;->mPopupMenu:Landroid/widget/ListPopupWindow;

    new-instance v1, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;->getContext()Landroid/content/Context;

    move-result-object v2

    sget v3, Lcom/google/android/apps/chrome/R$layout;->eb_popup_item:I

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;->getContext()Landroid/content/Context;

    move-result-object v5

    sget v6, Lcom/google/android/apps/chrome/R$string;->enhanced_bookmark_item_edit:I

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;->getContext()Landroid/content/Context;

    move-result-object v5

    sget v6, Lcom/google/android/apps/chrome/R$string;->enhanced_bookmark_item_move:I

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v8

    const/4 v5, 0x2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;->getContext()Landroid/content/Context;

    move-result-object v6

    sget v7, Lcom/google/android/apps/chrome/R$string;->enhanced_bookmark_item_delete:I

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-direct {v1, v2, v3, v4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListPopupWindow;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 201
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;->mPopupMenu:Landroid/widget/ListPopupWindow;

    invoke-virtual {v0, p1}, Landroid/widget/ListPopupWindow;->setAnchorView(Landroid/view/View;)V

    .line 202
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;->mPopupMenu:Landroid/widget/ListPopupWindow;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/chrome/R$dimen;->enhanced_bookmark_item_popup_width:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ListPopupWindow;->setWidth(I)V

    .line 204
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;->mPopupMenu:Landroid/widget/ListPopupWindow;

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v1

    neg-int v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/ListPopupWindow;->setVerticalOffset(I)V

    .line 205
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;->mPopupMenu:Landroid/widget/ListPopupWindow;

    invoke-virtual {v0, v8}, Landroid/widget/ListPopupWindow;->setModal(Z)V

    .line 206
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;->mPopupMenu:Landroid/widget/ListPopupWindow;

    new-instance v1, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem$1;-><init>(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListPopupWindow;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 223
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;->mPopupMenu:Landroid/widget/ListPopupWindow;

    invoke-virtual {v0}, Landroid/widget/ListPopupWindow;->show()V

    .line 224
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;->mPopupMenu:Landroid/widget/ListPopupWindow;

    invoke-virtual {v0}, Landroid/widget/ListPopupWindow;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/widget/ListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 225
    return-void
.end method


# virtual methods
.method getBookmarkId()Lorg/chromium/components/bookmarks/BookmarkId;
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;->mBookmarkId:Lorg/chromium/components/bookmarks/BookmarkId;

    return-object v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 309
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;->mMoreIcon:Landroid/widget/ImageView;

    if-ne p1, v0, :cond_1

    .line 310
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;->showMenu(Landroid/view/View;)V

    .line 314
    :cond_0
    return-void

    .line 312
    :cond_1
    sget-boolean v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    const-string/jumbo v1, "Unhandled onClick."

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 139
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 141
    sget v0, Lcom/google/android/apps/chrome/R$id;->background_solid_color:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;->mBackgroundSolidColor:Landroid/widget/ImageView;

    .line 142
    sget v0, Lcom/google/android/apps/chrome/R$id;->background_salient_image:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;->mBackgroundSalientImage:Landroid/widget/ImageView;

    .line 143
    sget v0, Lcom/google/android/apps/chrome/R$id;->more:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;->mMoreIcon:Landroid/widget/ImageView;

    .line 144
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;->mMoreIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 145
    sget v0, Lcom/google/android/apps/chrome/R$id;->folder_title:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;->mFolderTitleView:Landroid/widget/TextView;

    .line 146
    sget v0, Lcom/google/android/apps/chrome/R$id;->title:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;->mTitleView:Landroid/widget/TextView;

    .line 147
    sget v0, Lcom/google/android/apps/chrome/R$id;->url:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;->mUrlView:Landroid/widget/TextView;

    .line 148
    return-void
.end method

.method public onMeasure(II)V
    .locals 4

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    .line 152
    sget-boolean v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    if-eq v0, v3, :cond_0

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 154
    :cond_0
    sget-boolean v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 157
    :cond_1
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    invoke-static {v0, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 160
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    int-to-float v1, v1

    const v2, 0x3f8a3d71    # 1.08f

    mul-float/2addr v1, v2

    const/high16 v2, 0x3f000000    # 0.5f

    add-float/2addr v1, v2

    float-to-int v1, v1

    invoke-static {v1, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 163
    invoke-super {p0, v0, v1}, Landroid/widget/LinearLayout;->onMeasure(II)V

    .line 164
    return-void
.end method

.method setBookmarkId(Lorg/chromium/components/bookmarks/BookmarkId;)V
    .locals 1

    .prologue
    .line 175
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;->mBookmarkId:Lorg/chromium/components/bookmarks/BookmarkId;

    if-eq v0, p1, :cond_0

    .line 176
    iput-object p1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;->mBookmarkId:Lorg/chromium/components/bookmarks/BookmarkId;

    .line 177
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;->updateBookmarkInfo()V

    .line 179
    :cond_0
    return-void
.end method

.method setEnhancedBookmarkDelegate(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;)V
    .locals 0

    .prologue
    .line 167
    iput-object p1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;->mDelegate:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;

    .line 168
    return-void
.end method

.method updateBookmarkInfo()V
    .locals 6

    .prologue
    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v4, 0x4

    .line 231
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;->mBookmarkId:Lorg/chromium/components/bookmarks/BookmarkId;

    if-nez v0, :cond_1

    .line 305
    :cond_0
    :goto_0
    return-void

    .line 233
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;->mDelegate:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;->getModel()Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;->mBookmarkId:Lorg/chromium/components/bookmarks/BookmarkId;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;->getBookmarkById(Lorg/chromium/components/bookmarks/BookmarkId;)Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;

    move-result-object v0

    .line 235
    iget-object v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;->mTitleView:Landroid/widget/TextView;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 236
    iget-object v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;->mUrlView:Landroid/widget/TextView;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;->getUrl()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 238
    iget-object v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;->mFolderTitleView:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 239
    invoke-virtual {v0}, Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;->getParentId()Lorg/chromium/components/bookmarks/BookmarkId;

    move-result-object v1

    .line 240
    if-eqz v1, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;->mDelegate:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;

    invoke-interface {v2}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;->getModel()Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;->getTopLevelFolderParentIDs()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 242
    iget-object v2, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;->mDelegate:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;

    invoke-interface {v2}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;->getModel()Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;->getBookmarkById(Lorg/chromium/components/bookmarks/BookmarkId;)Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;

    move-result-object v1

    .line 243
    if-eqz v1, :cond_2

    .line 244
    iget-object v2, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;->mFolderTitleView:Landroid/widget/TextView;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 245
    iget-object v2, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;->mFolderTitleView:Landroid/widget/TextView;

    invoke-virtual {v1}, Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 250
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;->mBackgroundSolidColor:Landroid/widget/ImageView;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 251
    iget-object v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;->mBackgroundSolidColor:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 252
    iget-object v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;->mBackgroundSalientImage:Landroid/widget/ImageView;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 253
    iget-object v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;->mBackgroundSalientImage:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 254
    iget-object v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;->mBackgroundSalientImage:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->clearAnimation()V

    .line 256
    new-instance v1, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem$2;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem$2;-><init>(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;)V

    iput-object v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;->mSalientImageCallback:Lcom/google/android/apps/chrome/enhancedbookmark/ChromeEnhancedBookmarkBridge$SalientImageCallback;

    .line 297
    iget-object v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;->mDelegate:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;

    invoke-interface {v1}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;->getModel()Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;

    move-result-object v1

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;->getUrl()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;->mSalientImageCallback:Lcom/google/android/apps/chrome/enhancedbookmark/ChromeEnhancedBookmarkBridge$SalientImageCallback;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;->salientImageForUrl(Ljava/lang/String;Lcom/google/android/apps/chrome/enhancedbookmark/ChromeEnhancedBookmarkBridge$SalientImageCallback;)V

    .line 299
    iget-object v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;->mBackgroundSalientImage:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getVisibility()I

    move-result v1

    if-ne v1, v4, :cond_0

    .line 301
    invoke-static {v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkColor;->generateBackgroundColor(Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;)I

    move-result v0

    .line 302
    iget-object v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;->mBackgroundSolidColor:Landroid/widget/ImageView;

    new-instance v2, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem$RoundedTopCornerDrawable;

    invoke-direct {v2, p0, v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem$RoundedTopCornerDrawable;-><init>(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;I)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 303
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItem;->mBackgroundSolidColor:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_0
.end method
