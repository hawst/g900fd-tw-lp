.class Lcom/google/android/apps/chrome/tabmodel/OffTheRecordTabModel;
.super Ljava/lang/Object;
.source "OffTheRecordTabModel.java"

# interfaces
.implements Lorg/chromium/chrome/browser/tabmodel/TabModel;


# instance fields
.field private final mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

.field private mDelegateModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

.field private final mModelDelegate:Lorg/chromium/chrome/browser/tabmodel/TabModelDelegate;

.field private final mOrderController:Lorg/chromium/chrome/browser/tabmodel/TabModelOrderController;

.field private final mTabContentManager:Lcom/google/android/apps/chrome/compositor/TabContentManager;

.field private final mTabSaver:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;

.field private final mUma:Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorUma;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/ChromeActivity;Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorUma;Lorg/chromium/chrome/browser/tabmodel/TabModelOrderController;Lcom/google/android/apps/chrome/compositor/TabContentManager;Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;Lorg/chromium/chrome/browser/tabmodel/TabModelDelegate;)V
    .locals 1

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    invoke-static {}, Lorg/chromium/chrome/browser/tabmodel/EmptyTabModel;->getInstance()Lorg/chromium/chrome/browser/tabmodel/EmptyTabModel;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/OffTheRecordTabModel;->mDelegateModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    .line 57
    iput-object p1, p0, Lcom/google/android/apps/chrome/tabmodel/OffTheRecordTabModel;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    .line 58
    iput-object p2, p0, Lcom/google/android/apps/chrome/tabmodel/OffTheRecordTabModel;->mUma:Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorUma;

    .line 59
    iput-object p3, p0, Lcom/google/android/apps/chrome/tabmodel/OffTheRecordTabModel;->mOrderController:Lorg/chromium/chrome/browser/tabmodel/TabModelOrderController;

    .line 60
    iput-object p4, p0, Lcom/google/android/apps/chrome/tabmodel/OffTheRecordTabModel;->mTabContentManager:Lcom/google/android/apps/chrome/compositor/TabContentManager;

    .line 61
    iput-object p5, p0, Lcom/google/android/apps/chrome/tabmodel/OffTheRecordTabModel;->mTabSaver:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;

    .line 62
    iput-object p6, p0, Lcom/google/android/apps/chrome/tabmodel/OffTheRecordTabModel;->mModelDelegate:Lorg/chromium/chrome/browser/tabmodel/TabModelDelegate;

    .line 63
    return-void
.end method

.method private destroyIncognitoIfNecessary()V
    .locals 2

    .prologue
    .line 73
    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    .line 74
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabmodel/OffTheRecordTabModel;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/OffTheRecordTabModel;->mDelegateModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    instance-of v0, v0, Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;

    if-nez v0, :cond_1

    .line 88
    :cond_0
    :goto_0
    return-void

    .line 78
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/OffTheRecordTabModel;->mDelegateModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    check-cast v0, Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;

    .line 79
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;->getProfile()Lorg/chromium/chrome/browser/profiles/Profile;

    move-result-object v1

    .line 80
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;->destroy()V

    .line 83
    if-eqz v1, :cond_2

    invoke-static {}, Lcom/google/android/apps/chrome/tabmodel/TabWindowManager;->getInstance()Lcom/google/android/apps/chrome/tabmodel/TabWindowManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabmodel/TabWindowManager;->getIncognitoTabCount()I

    move-result v0

    if-nez v0, :cond_2

    .line 84
    invoke-static {v1}, Lcom/google/android/apps/chrome/ChromeMobileApplication;->destroyProfile(Lorg/chromium/chrome/browser/profiles/Profile;)V

    .line 87
    :cond_2
    invoke-static {}, Lorg/chromium/chrome/browser/tabmodel/EmptyTabModel;->getInstance()Lorg/chromium/chrome/browser/tabmodel/EmptyTabModel;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/OffTheRecordTabModel;->mDelegateModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    goto :goto_0
.end method

.method private ensureTabModelImpl()V
    .locals 8

    .prologue
    .line 66
    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    .line 67
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/OffTheRecordTabModel;->mDelegateModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    instance-of v0, v0, Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;

    if-eqz v0, :cond_0

    .line 70
    :goto_0
    return-void

    .line 68
    :cond_0
    new-instance v0, Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabmodel/OffTheRecordTabModel;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    iget-object v3, p0, Lcom/google/android/apps/chrome/tabmodel/OffTheRecordTabModel;->mUma:Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorUma;

    iget-object v4, p0, Lcom/google/android/apps/chrome/tabmodel/OffTheRecordTabModel;->mOrderController:Lorg/chromium/chrome/browser/tabmodel/TabModelOrderController;

    iget-object v5, p0, Lcom/google/android/apps/chrome/tabmodel/OffTheRecordTabModel;->mTabContentManager:Lcom/google/android/apps/chrome/compositor/TabContentManager;

    iget-object v6, p0, Lcom/google/android/apps/chrome/tabmodel/OffTheRecordTabModel;->mTabSaver:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;

    iget-object v7, p0, Lcom/google/android/apps/chrome/tabmodel/OffTheRecordTabModel;->mModelDelegate:Lorg/chromium/chrome/browser/tabmodel/TabModelDelegate;

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;-><init>(ZLcom/google/android/apps/chrome/ChromeActivity;Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorUma;Lorg/chromium/chrome/browser/tabmodel/TabModelOrderController;Lcom/google/android/apps/chrome/compositor/TabContentManager;Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;Lorg/chromium/chrome/browser/tabmodel/TabModelDelegate;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/OffTheRecordTabModel;->mDelegateModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    goto :goto_0
.end method

.method private isEmpty()Z
    .locals 1

    .prologue
    .line 91
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabmodel/OffTheRecordTabModel;->getComprehensiveModel()Lorg/chromium/chrome/browser/tabmodel/TabList;

    move-result-object v0

    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabList;->getCount()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public addTab(Lorg/chromium/chrome/browser/Tab;ILorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;)V
    .locals 1

    .prologue
    .line 201
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabmodel/OffTheRecordTabModel;->ensureTabModelImpl()V

    .line 202
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/OffTheRecordTabModel;->mDelegateModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    invoke-interface {v0, p1, p2, p3}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->addTab(Lorg/chromium/chrome/browser/Tab;ILorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;)V

    .line 203
    return-void
.end method

.method public cancelTabClosure(I)V
    .locals 1

    .prologue
    .line 196
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/OffTheRecordTabModel;->mDelegateModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    invoke-interface {v0, p1}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->cancelTabClosure(I)V

    .line 197
    return-void
.end method

.method public closeAllTabs()V
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/OffTheRecordTabModel;->mDelegateModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->closeAllTabs()V

    .line 126
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabmodel/OffTheRecordTabModel;->destroyIncognitoIfNecessary()V

    .line 127
    return-void
.end method

.method public closeTab(Lorg/chromium/chrome/browser/Tab;)Z
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/OffTheRecordTabModel;->mDelegateModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    invoke-interface {v0, p1}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->closeTab(Lorg/chromium/chrome/browser/Tab;)Z

    move-result v0

    .line 107
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabmodel/OffTheRecordTabModel;->destroyIncognitoIfNecessary()V

    .line 108
    return v0
.end method

.method public closeTab(Lorg/chromium/chrome/browser/Tab;ZZZ)Z
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/OffTheRecordTabModel;->mDelegateModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    invoke-interface {v0, p1, p2, p3, p4}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->closeTab(Lorg/chromium/chrome/browser/Tab;ZZZ)Z

    move-result v0

    .line 114
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabmodel/OffTheRecordTabModel;->destroyIncognitoIfNecessary()V

    .line 115
    return v0
.end method

.method public commitAllTabClosures()V
    .locals 1

    .prologue
    .line 183
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabmodel/OffTheRecordTabModel;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 186
    :goto_0
    return-void

    .line 184
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/OffTheRecordTabModel;->mDelegateModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->commitAllTabClosures()V

    .line 185
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabmodel/OffTheRecordTabModel;->destroyIncognitoIfNecessary()V

    goto :goto_0
.end method

.method public commitTabClosure(I)V
    .locals 1

    .prologue
    .line 190
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/OffTheRecordTabModel;->mDelegateModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    invoke-interface {v0, p1}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->commitTabClosure(I)V

    .line 191
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabmodel/OffTheRecordTabModel;->destroyIncognitoIfNecessary()V

    .line 192
    return-void
.end method

.method public destroy()V
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/OffTheRecordTabModel;->mDelegateModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->destroy()V

    .line 162
    return-void
.end method

.method public getComprehensiveModel()Lorg/chromium/chrome/browser/tabmodel/TabList;
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/OffTheRecordTabModel;->mDelegateModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->getComprehensiveModel()Lorg/chromium/chrome/browser/tabmodel/TabList;

    move-result-object v0

    return-object v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/OffTheRecordTabModel;->mDelegateModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->getCount()I

    move-result v0

    return v0
.end method

.method public getNextTabIfClosed(I)Lorg/chromium/chrome/browser/Tab;
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/OffTheRecordTabModel;->mDelegateModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    invoke-interface {v0, p1}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->getNextTabIfClosed(I)Lorg/chromium/chrome/browser/Tab;

    move-result-object v0

    return-object v0
.end method

.method public getProfile()Lorg/chromium/chrome/browser/profiles/Profile;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/OffTheRecordTabModel;->mDelegateModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->getProfile()Lorg/chromium/chrome/browser/profiles/Profile;

    move-result-object v0

    return-object v0
.end method

.method public getTabAt(I)Lorg/chromium/chrome/browser/Tab;
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/OffTheRecordTabModel;->mDelegateModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    invoke-interface {v0, p1}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->getTabAt(I)Lorg/chromium/chrome/browser/Tab;

    move-result-object v0

    return-object v0
.end method

.method public index()I
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/OffTheRecordTabModel;->mDelegateModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->index()I

    move-result v0

    return v0
.end method

.method public indexOf(Lorg/chromium/chrome/browser/Tab;)I
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/OffTheRecordTabModel;->mDelegateModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    invoke-interface {v0, p1}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->indexOf(Lorg/chromium/chrome/browser/Tab;)I

    move-result v0

    return v0
.end method

.method public isClosurePending(I)Z
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/OffTheRecordTabModel;->mDelegateModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    invoke-interface {v0, p1}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->isClosurePending(I)Z

    move-result v0

    return v0
.end method

.method public isIncognito()Z
    .locals 1

    .prologue
    .line 101
    const/4 v0, 0x1

    return v0
.end method

.method public moveTab(II)V
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/OffTheRecordTabModel;->mDelegateModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    invoke-interface {v0, p1, p2}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->moveTab(II)V

    .line 157
    return-void
.end method

.method public setIndex(ILorg/chromium/chrome/browser/tabmodel/TabModel$TabSelectionType;)V
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/OffTheRecordTabModel;->mDelegateModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    invoke-interface {v0, p1, p2}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->setIndex(ILorg/chromium/chrome/browser/tabmodel/TabModel$TabSelectionType;)V

    .line 152
    return-void
.end method

.method public supportsPendingClosures()Z
    .locals 1

    .prologue
    .line 171
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/OffTheRecordTabModel;->mDelegateModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->supportsPendingClosures()Z

    move-result v0

    return v0
.end method
