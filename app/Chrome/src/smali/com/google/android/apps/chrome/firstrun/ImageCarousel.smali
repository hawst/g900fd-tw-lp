.class public Lcom/google/android/apps/chrome/firstrun/ImageCarousel;
.super Landroid/widget/FrameLayout;
.source "ImageCarousel.java"

# interfaces
.implements Landroid/view/GestureDetector$OnGestureListener;


# static fields
.field private static final BACKGROUND_IMAGE_ALPHA:Landroid/util/Property;

.field private static final BITMAP_OFFSETS:[I

.field private static final ORDER_OFFSETS:[I

.field private static final POSITION_OFFSETS:[I

.field private static final POSITION_PROPERTY:Landroid/util/Property;


# instance fields
.field private mAccountSelected:Z

.field private mCarouselWidth:I

.field private mFadeInOutAnimator:Landroid/animation/Animator;

.field private mFlingScalingFactor:F

.field private mGestureDetector:Landroid/view/GestureDetector;

.field private mImageWidth:I

.field private mImages:[Landroid/graphics/Bitmap;

.field private mLastPosition:I

.field private mListener:Lcom/google/android/apps/chrome/firstrun/ImageCarousel$ImageCarouselPositionChangeListener;

.field private mNeedsPositionUpdates:Z

.field private mPosition:F

.field private mScrollAnimator:Landroid/animation/Animator;

.field private mScrollScalingFactor:F

.field private mScrollingDisabled:Z

.field private mTranslationFactor:F

.field private mViews:[Landroid/widget/ImageView;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v1, 0x4

    .line 90
    new-array v0, v1, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->ORDER_OFFSETS:[I

    .line 92
    new-array v0, v1, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->POSITION_OFFSETS:[I

    .line 94
    new-array v0, v1, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->BITMAP_OFFSETS:[I

    .line 99
    new-instance v0, Lcom/google/android/apps/chrome/firstrun/ImageCarousel$1;

    const-class v1, Ljava/lang/Float;

    const-string/jumbo v2, ""

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/chrome/firstrun/ImageCarousel$1;-><init>(Ljava/lang/Class;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->POSITION_PROPERTY:Landroid/util/Property;

    .line 116
    new-instance v0, Lcom/google/android/apps/chrome/firstrun/ImageCarousel$2;

    const-class v1, Ljava/lang/Float;

    const-string/jumbo v2, ""

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/chrome/firstrun/ImageCarousel$2;-><init>(Ljava/lang/Class;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->BACKGROUND_IMAGE_ALPHA:Landroid/util/Property;

    return-void

    .line 90
    nop

    :array_0
    .array-data 4
        0x2
        0x1
        0x3
        0x0
    .end array-data

    .line 92
    :array_1
    .array-data 4
        0x0
        -0x1
        0x2
        0x1
    .end array-data

    .line 94
    :array_2
    .array-data 4
        0x2
        0x1
        -0x1
        0x0
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 164
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 138
    const/4 v0, 0x4

    new-array v0, v0, [Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->mViews:[Landroid/widget/ImageView;

    .line 148
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->mPosition:F

    .line 151
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->mLastPosition:I

    .line 152
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->mNeedsPositionUpdates:Z

    .line 165
    new-instance v0, Landroid/view/GestureDetector;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->mGestureDetector:Landroid/view/GestureDetector;

    .line 166
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/firstrun/ImageCarousel;)F
    .locals 1

    .prologue
    .line 50
    iget v0, p0, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->mPosition:F

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/firstrun/ImageCarousel;F)V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->setPosition(F)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/firstrun/ImageCarousel;)[Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->mViews:[Landroid/widget/ImageView;

    return-object v0
.end method

.method private getCenterPosition()I
    .locals 2

    .prologue
    .line 397
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->mImages:[Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 398
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->mPosition:F

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->mImages:[Landroid/graphics/Bitmap;

    array-length v1, v1

    rem-int/2addr v0, v1

    goto :goto_0
.end method

.method private setLayoutParamsForCheckmark(Landroid/view/View;)V
    .locals 4

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    .line 402
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$dimen;->fre_checkmark_size:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 403
    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v0, v0}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 405
    const/16 v2, 0x11

    iput v2, v1, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 406
    invoke-virtual {p1, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 407
    iget v1, p0, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->mImageWidth:I

    sub-int/2addr v1, v0

    int-to-float v1, v1

    div-float/2addr v1, v3

    invoke-virtual {p1, v1}, Landroid/view/View;->setTranslationX(F)V

    .line 408
    iget v1, p0, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->mImageWidth:I

    sub-int v0, v1, v0

    int-to-float v0, v0

    div-float/2addr v0, v3

    invoke-virtual {p1, v0}, Landroid/view/View;->setTranslationY(F)V

    .line 409
    return-void
.end method

.method private setPosition(F)V
    .locals 2

    .prologue
    .line 378
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->mImages:[Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 379
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->mImages:[Landroid/graphics/Bitmap;

    array-length v0, v0

    int-to-float v0, v0

    rem-float v0, p1, v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->mImages:[Landroid/graphics/Bitmap;

    array-length v1, v1

    int-to-float v1, v1

    add-float/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->mImages:[Landroid/graphics/Bitmap;

    array-length v1, v1

    int-to-float v1, v1

    rem-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->mPosition:F

    .line 382
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->getCenterPosition()I

    move-result v0

    .line 383
    iget v1, p0, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->mLastPosition:I

    if-eq v0, v1, :cond_1

    .line 384
    iput v0, p0, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->mLastPosition:I

    .line 385
    iget-object v1, p0, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->mListener:Lcom/google/android/apps/chrome/firstrun/ImageCarousel$ImageCarouselPositionChangeListener;

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->mNeedsPositionUpdates:Z

    if-eqz v1, :cond_1

    .line 386
    iget-object v1, p0, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->mListener:Lcom/google/android/apps/chrome/firstrun/ImageCarousel$ImageCarouselPositionChangeListener;

    invoke-interface {v1, v0}, Lcom/google/android/apps/chrome/firstrun/ImageCarousel$ImageCarouselPositionChangeListener;->onPositionChanged(I)V

    .line 392
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->updateImageViews()V

    .line 393
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->invalidate()V

    .line 394
    return-void
.end method

.method private updateBitmap(I)V
    .locals 4

    .prologue
    .line 371
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->mImages:[Landroid/graphics/Bitmap;

    array-length v0, v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x3

    if-ge p1, v0, :cond_0

    .line 375
    :goto_0
    return-void

    .line 372
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->mViews:[Landroid/widget/ImageView;

    const/4 v1, 0x4

    invoke-virtual {p0, v1, p1}, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->getChildDrawingOrder(II)I

    move-result v1

    aget-object v0, v0, v1

    .line 373
    iget-object v1, p0, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->mImages:[Landroid/graphics/Bitmap;

    iget-object v2, p0, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->mImages:[Landroid/graphics/Bitmap;

    array-length v2, v2

    iget v3, p0, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->mPosition:F

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    add-int/2addr v2, v3

    sget-object v3, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->BITMAP_OFFSETS:[I

    aget v3, v3, p1

    add-int/2addr v2, v3

    iget-object v3, p0, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->mImages:[Landroid/graphics/Bitmap;

    array-length v3, v3

    rem-int/2addr v2, v3

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method private updateImageViews()V
    .locals 10

    .prologue
    const-wide v8, 0x400921fb54442d18L    # Math.PI

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    .line 340
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->mImages:[Landroid/graphics/Bitmap;

    if-nez v0, :cond_1

    .line 368
    :cond_0
    return-void

    .line 342
    :cond_1
    const/4 v0, 0x0

    :goto_0
    const/4 v1, 0x4

    if-ge v0, v1, :cond_0

    .line 343
    iget-boolean v1, p0, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->mAccountSelected:Z

    if-eqz v1, :cond_2

    invoke-direct {p0}, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->getCenterPosition()I

    move-result v1

    if-ne v0, v1, :cond_3

    .line 345
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->mViews:[Landroid/widget/ImageView;

    aget-object v1, v1, v0

    .line 347
    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->updateBitmap(I)V

    .line 349
    iget v2, p0, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->mPosition:F

    sget-object v3, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->POSITION_OFFSETS:[I

    aget v3, v3, v0

    int-to-float v3, v3

    add-float/2addr v2, v3

    .line 353
    iget v3, p0, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->mTranslationFactor:F

    neg-float v3, v3

    float-to-double v4, v2

    mul-double/2addr v4, v8

    div-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    double-to-float v4, v4

    mul-float/2addr v3, v4

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setTranslationX(F)V

    .line 359
    float-to-double v4, v2

    mul-double/2addr v4, v8

    div-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    double-to-float v3, v4

    const/high16 v4, 0x40400000    # 3.0f

    div-float/2addr v3, v4

    const v4, 0x3f2aaaab

    add-float/2addr v3, v4

    .line 360
    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setScaleY(F)V

    .line 361
    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setScaleX(F)V

    .line 365
    float-to-double v2, v2

    mul-double/2addr v2, v8

    const-wide/high16 v4, 0x4010000000000000L    # 4.0

    div-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v2

    invoke-static {v2, v3, v6, v7}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    double-to-float v2, v2

    .line 366
    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 342
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method protected getChildDrawingOrder(II)I
    .locals 2

    .prologue
    .line 270
    const/4 v0, 0x4

    if-lt p2, v0, :cond_0

    .line 273
    :goto_0
    return p2

    :cond_0
    iget v0, p0, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->mPosition:F

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    sget-object v1, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->ORDER_OFFSETS:[I

    aget v1, v1, p2

    add-int/2addr v0, v1

    rem-int/lit8 p2, v0, 0x4

    goto :goto_0
.end method

.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 293
    const/4 v0, 0x1

    return v0
.end method

.method public onFinishInflate()V
    .locals 5

    .prologue
    .line 242
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 244
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$dimen;->fre_image_carousel_height:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->mImageWidth:I

    .line 245
    const/4 v0, 0x0

    :goto_0
    const/4 v1, 0x4

    if-ge v0, v1, :cond_0

    .line 246
    new-instance v1, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 247
    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    iget v3, p0, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->mImageWidth:I

    iget v4, p0, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->mImageWidth:I

    invoke-direct {v2, v3, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 249
    const/16 v3, 0x11

    iput v3, v2, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 250
    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 251
    iget-object v2, p0, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->mViews:[Landroid/widget/ImageView;

    aput-object v1, v2, v0

    .line 252
    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->addView(Landroid/view/View;)V

    .line 245
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 255
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$dimen;->fre_image_carousel_width:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->mCarouselWidth:I

    .line 256
    const v0, 0x3eeb851f    # 0.46f

    iget v1, p0, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->mCarouselWidth:I

    int-to-float v1, v1

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->mScrollScalingFactor:F

    .line 257
    const v0, 0x41133333    # 9.2f

    iget v1, p0, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->mCarouselWidth:I

    int-to-float v1, v1

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->mFlingScalingFactor:F

    .line 258
    const v0, 0x3f23d70a    # 0.64f

    iget v1, p0, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->mImageWidth:I

    int-to-float v1, v1

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->mTranslationFactor:F

    .line 260
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->setChildrenDrawingOrderEnabled(Z)V

    .line 261
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->setPosition(F)V

    .line 262
    return-void
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 328
    iput-boolean v2, p0, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->mNeedsPositionUpdates:Z

    .line 329
    iget v0, p0, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->mPosition:F

    iget v1, p0, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->mFlingScalingFactor:F

    div-float v1, p3, v1

    sub-float/2addr v0, v1

    invoke-virtual {p0, v0, v2, v2}, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->scrollTo(FZZ)V

    .line 330
    return v2
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 0

    .prologue
    .line 324
    return-void
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 316
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 318
    iput-boolean v2, p0, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->mNeedsPositionUpdates:Z

    .line 319
    iget v0, p0, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->mPosition:F

    iget v1, p0, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->mScrollScalingFactor:F

    div-float v1, p3, v1

    add-float/2addr v0, v1

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->setPosition(F)V

    .line 320
    return v2
.end method

.method public onShowPress(Landroid/view/MotionEvent;)V
    .locals 0

    .prologue
    .line 297
    return-void
.end method

.method public onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 7

    .prologue
    const/high16 v6, 0x40000000    # 2.0f

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 301
    iput-boolean v0, p0, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->mNeedsPositionUpdates:Z

    .line 302
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    iget v3, p0, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->mCarouselWidth:I

    iget v4, p0, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->mImageWidth:I

    sub-int/2addr v3, v4

    int-to-float v3, v3

    div-float/2addr v3, v6

    cmpg-float v2, v2, v3

    if-gez v2, :cond_0

    .line 303
    iget v2, p0, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->mPosition:F

    sub-float/2addr v2, v5

    invoke-virtual {p0, v2, v1, v0}, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->scrollTo(FZZ)V

    .line 309
    :goto_0
    return v0

    .line 305
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    iget v3, p0, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->mCarouselWidth:I

    iget v4, p0, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->mImageWidth:I

    add-int/2addr v3, v4

    int-to-float v3, v3

    div-float/2addr v3, v6

    cmpl-float v2, v2, v3

    if-lez v2, :cond_1

    .line 306
    iget v2, p0, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->mPosition:F

    add-float/2addr v2, v5

    invoke-virtual {p0, v2, v1, v0}, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->scrollTo(FZZ)V

    goto :goto_0

    :cond_1
    move v0, v1

    .line 309
    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 278
    iget-boolean v2, p0, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->mScrollingDisabled:Z

    if-eqz v2, :cond_1

    .line 286
    :cond_0
    :goto_0
    return v0

    .line 279
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->mGestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v2, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v2

    if-eqz v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 281
    :cond_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-eq v2, v1, :cond_3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    const/4 v3, 0x3

    if-ne v2, v3, :cond_0

    .line 283
    :cond_3
    iget v2, p0, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->mPosition:F

    invoke-virtual {p0, v2, v0, v1}, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->scrollTo(FZZ)V

    goto :goto_0
.end method

.method public scrollTo(FZZ)V
    .locals 5

    .prologue
    .line 176
    iput-boolean p3, p0, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->mNeedsPositionUpdates:Z

    .line 177
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->mScrollAnimator:Landroid/animation/Animator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->mScrollAnimator:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->cancel()V

    .line 179
    :cond_0
    invoke-static {p1}, Ljava/lang/Math;->round(F)I

    move-result v0

    int-to-float v0, v0

    .line 180
    sget-object v1, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->POSITION_PROPERTY:Landroid/util/Property;

    const/4 v2, 0x2

    new-array v2, v2, [F

    const/4 v3, 0x0

    iget v4, p0, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->mPosition:F

    aput v4, v2, v3

    const/4 v3, 0x1

    aput v0, v2, v3

    invoke-static {p0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->mScrollAnimator:Landroid/animation/Animator;

    .line 181
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->mScrollAnimator:Landroid/animation/Animator;

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 182
    if-eqz p2, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->mScrollAnimator:Landroid/animation/Animator;

    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 183
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->mScrollAnimator:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    .line 184
    return-void
.end method

.method public setImages([Landroid/graphics/Bitmap;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 197
    array-length v0, p1

    packed-switch v0, :pswitch_data_0

    .line 208
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->mAccountSelected:Z

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->mScrollingDisabled:Z

    .line 209
    array-length v0, p1

    invoke-static {p1, v0}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->mImages:[Landroid/graphics/Bitmap;

    .line 213
    :goto_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->updateImageViews()V

    .line 214
    return-void

    .line 199
    :pswitch_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->mImages:[Landroid/graphics/Bitmap;

    .line 200
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->mScrollingDisabled:Z

    goto :goto_0

    .line 203
    :pswitch_1
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->mScrollingDisabled:Z

    .line 204
    array-length v0, p1

    invoke-static {p1, v0}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->mImages:[Landroid/graphics/Bitmap;

    goto :goto_0

    .line 197
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public setListener(Lcom/google/android/apps/chrome/firstrun/ImageCarousel$ImageCarouselPositionChangeListener;)V
    .locals 0

    .prologue
    .line 190
    iput-object p1, p0, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->mListener:Lcom/google/android/apps/chrome/firstrun/ImageCarousel$ImageCarouselPositionChangeListener;

    .line 191
    return-void
.end method

.method public setSignedInMode()V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 221
    iput-boolean v6, p0, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->mScrollingDisabled:Z

    .line 222
    iput-boolean v6, p0, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->mAccountSelected:Z

    .line 223
    invoke-direct {p0}, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->getCenterPosition()I

    move-result v0

    int-to-float v0, v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->setPosition(F)V

    .line 225
    new-instance v0, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 226
    sget v1, Lcom/google/android/apps/chrome/R$drawable;->fre_checkmark:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 227
    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->setLayoutParamsForCheckmark(Landroid/view/View;)V

    .line 228
    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->addView(Landroid/view/View;)V

    .line 230
    iget-object v1, p0, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->mFadeInOutAnimator:Landroid/animation/Animator;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->mFadeInOutAnimator:Landroid/animation/Animator;

    invoke-virtual {v1}, Landroid/animation/Animator;->cancel()V

    .line 231
    :cond_0
    new-instance v1, Landroid/animation/AnimatorSet;

    invoke-direct {v1}, Landroid/animation/AnimatorSet;-><init>()V

    .line 232
    new-array v2, v8, [Landroid/animation/Animator;

    sget-object v3, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->BACKGROUND_IMAGE_ALPHA:Landroid/util/Property;

    new-array v4, v6, [F

    const/4 v5, 0x0

    aput v5, v4, v7

    invoke-static {p0, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    aput-object v3, v2, v7

    sget-object v3, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v4, v8, [F

    fill-array-data v4, :array_0

    invoke-static {v0, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    aput-object v0, v2, v6

    invoke-virtual {v1, v2}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 235
    iput-object v1, p0, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->mFadeInOutAnimator:Landroid/animation/Animator;

    .line 236
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->mFadeInOutAnimator:Landroid/animation/Animator;

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 237
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->mFadeInOutAnimator:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    .line 238
    return-void

    .line 232
    nop

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method
