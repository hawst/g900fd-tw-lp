.class public Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPageView;
.super Landroid/widget/FrameLayout;
.source "IncognitoNewTabPageView.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private mFirstShow:Z

.field private mLoadHasCompleted:Z

.field private mManager:Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPageView$IncognitoNewTabPageManager;

.field private mPendingLoadTasks:I

.field private mScrollView:Lcom/google/android/apps/chrome/ntp/NewTabScrollView;

.field private mSnapshotHeight:I

.field private mSnapshotScrollY:I

.field private mSnapshotWidth:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    const-class v0, Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPageView;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPageView;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 54
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 22
    iput-boolean v0, p0, Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPageView;->mFirstShow:Z

    .line 30
    iput v0, p0, Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPageView;->mPendingLoadTasks:I

    .line 55
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPageView;)Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPageView$IncognitoNewTabPageManager;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPageView;->mManager:Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPageView$IncognitoNewTabPageManager;

    return-object v0
.end method

.method private loadTaskCompleted()V
    .locals 1

    .prologue
    .line 89
    sget-boolean v0, Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPageView;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPageView;->mPendingLoadTasks:I

    if-gtz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 90
    :cond_0
    iget v0, p0, Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPageView;->mPendingLoadTasks:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPageView;->mPendingLoadTasks:I

    .line 91
    iget v0, p0, Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPageView;->mPendingLoadTasks:I

    if-nez v0, :cond_2

    .line 92
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPageView;->mLoadHasCompleted:Z

    if-eqz v0, :cond_1

    .line 93
    sget-boolean v0, Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPageView;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 95
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPageView;->mLoadHasCompleted:Z

    .line 96
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPageView;->mManager:Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPageView$IncognitoNewTabPageManager;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPageView$IncognitoNewTabPageManager;->notifyLoadingComplete()V

    .line 99
    :cond_2
    return-void
.end method


# virtual methods
.method captureThumbnail(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 118
    invoke-static {p0, p1}, Lcom/google/android/apps/chrome/utilities/ViewUtils;->captureBitmap(Landroid/view/View;Landroid/graphics/Canvas;)V

    .line 119
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPageView;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPageView;->mSnapshotWidth:I

    .line 120
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPageView;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPageView;->mSnapshotHeight:I

    .line 121
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPageView;->mScrollView:Lcom/google/android/apps/chrome/ntp/NewTabScrollView;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ntp/NewTabScrollView;->getScrollY()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPageView;->mSnapshotScrollY:I

    .line 122
    return-void
.end method

.method initialize(Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPageView$IncognitoNewTabPageManager;)V
    .locals 0

    .prologue
    .line 81
    iput-object p1, p0, Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPageView;->mManager:Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPageView$IncognitoNewTabPageManager;

    .line 82
    return-void
.end method

.method public onAttachedToWindow()V
    .locals 1

    .prologue
    .line 128
    sget-boolean v0, Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPageView;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPageView;->mManager:Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPageView$IncognitoNewTabPageManager;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 129
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPageView;->mFirstShow:Z

    if-eqz v0, :cond_1

    .line 130
    invoke-direct {p0}, Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPageView;->loadTaskCompleted()V

    .line 131
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPageView;->mFirstShow:Z

    .line 133
    :cond_1
    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 59
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 61
    sget v0, Lcom/google/android/apps/chrome/R$id;->ntp_scrollview:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPageView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/ntp/NewTabScrollView;

    iput-object v0, p0, Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPageView;->mScrollView:Lcom/google/android/apps/chrome/ntp/NewTabScrollView;

    .line 65
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPageView;->mScrollView:Lcom/google/android/apps/chrome/ntp/NewTabScrollView;

    const/high16 v1, 0x20000

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/ntp/NewTabScrollView;->setDescendantFocusability(I)V

    .line 67
    sget v0, Lcom/google/android/apps/chrome/R$id;->learn_more:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPageView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 68
    new-instance v1, Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPageView$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPageView$1;-><init>(Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPageView;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 74
    return-void
.end method

.method shouldCaptureThumbnail()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 106
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPageView;->getWidth()I

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPageView;->getHeight()I

    move-result v1

    if-nez v1, :cond_1

    .line 108
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPageView;->getWidth()I

    move-result v1

    iget v2, p0, Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPageView;->mSnapshotWidth:I

    if-ne v1, v2, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPageView;->getHeight()I

    move-result v1

    iget v2, p0, Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPageView;->mSnapshotHeight:I

    if-ne v1, v2, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPageView;->mScrollView:Lcom/google/android/apps/chrome/ntp/NewTabScrollView;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/ntp/NewTabScrollView;->getScrollY()I

    move-result v1

    iget v2, p0, Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPageView;->mSnapshotScrollY:I

    if-eq v1, v2, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method
