.class Lcom/google/android/apps/chrome/document/ssb/ChromeSsbServiceCallback$1;
.super Ljava/lang/Object;
.source "ChromeSsbServiceCallback.java"

# interfaces
.implements Lcom/google/android/apps/chrome/document/ssb/SsbServiceCallback;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/document/ssb/ChromeSsbServiceCallback;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/document/ssb/ChromeSsbServiceCallback;)V
    .locals 0

    .prologue
    .line 30
    iput-object p1, p0, Lcom/google/android/apps/chrome/document/ssb/ChromeSsbServiceCallback$1;->this$0:Lcom/google/android/apps/chrome/document/ssb/ChromeSsbServiceCallback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public updateSsb([B)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 35
    :try_start_0
    invoke-static {p1}, Lcom/google/android/c/h;->a([B)Lcom/google/android/c/h;

    move-result-object v2

    .line 36
    iget-object v0, v2, Lcom/google/android/c/h;->a:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 37
    iget-object v3, v2, Lcom/google/android/c/h;->b:[Lcom/google/android/c/g;

    array-length v4, v3

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_0

    aget-object v5, v3, v2

    .line 38
    iget-object v6, v5, Lcom/google/android/c/g;->b:Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    iget-object v5, v5, Lcom/google/android/c/g;->a:Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v6, v5}, Lcom/google/android/apps/chrome/uma/UmaSessionStats;->registerSyntheticFieldTrial(ILjava/lang/String;)V
    :try_end_0
    .catch Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException; {:try_start_0 .. :try_end_0} :catch_0

    .line 37
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 41
    :catch_0
    move-exception v0

    .line 42
    const-string/jumbo v2, "ChromeSsbServiceCallback"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Error while parsing ssb context proto "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    .line 45
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/document/ssb/ChromeSsbServiceCallback$1;->this$0:Lcom/google/android/apps/chrome/document/ssb/ChromeSsbServiceCallback;

    # getter for: Lcom/google/android/apps/chrome/document/ssb/ChromeSsbServiceCallback;->mPreferences:Landroid/content/SharedPreferences;
    invoke-static {v1}, Lcom/google/android/apps/chrome/document/ssb/ChromeSsbServiceCallback;->access$000(Lcom/google/android/apps/chrome/document/ssb/ChromeSsbServiceCallback;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string/jumbo v2, "show_hotword_enabled"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 46
    iget-object v1, p0, Lcom/google/android/apps/chrome/document/ssb/ChromeSsbServiceCallback$1;->this$0:Lcom/google/android/apps/chrome/document/ssb/ChromeSsbServiceCallback;

    # invokes: Lcom/google/android/apps/chrome/document/ssb/ChromeSsbServiceCallback;->notifyShowHotwordEnabledChange(Z)V
    invoke-static {v1, v0}, Lcom/google/android/apps/chrome/document/ssb/ChromeSsbServiceCallback;->access$100(Lcom/google/android/apps/chrome/document/ssb/ChromeSsbServiceCallback;Z)V

    .line 47
    return-void
.end method
