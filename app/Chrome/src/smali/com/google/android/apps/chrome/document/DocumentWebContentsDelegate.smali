.class public Lcom/google/android/apps/chrome/document/DocumentWebContentsDelegate;
.super Lorg/chromium/components/web_contents_delegate_android/WebContentsDelegateAndroid;
.source "DocumentWebContentsDelegate.java"


# static fields
.field private static sInstance:Lcom/google/android/apps/chrome/document/DocumentWebContentsDelegate;


# instance fields
.field private mNativePtr:J


# direct methods
.method private constructor <init>()V
    .locals 2

    .prologue
    .line 40
    invoke-direct {p0}, Lorg/chromium/components/web_contents_delegate_android/WebContentsDelegateAndroid;-><init>()V

    .line 41
    invoke-direct {p0}, Lcom/google/android/apps/chrome/document/DocumentWebContentsDelegate;->nativeInitialize()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/chrome/document/DocumentWebContentsDelegate;->mNativePtr:J

    .line 42
    return-void
.end method

.method public static getInstance()Lcom/google/android/apps/chrome/document/DocumentWebContentsDelegate;
    .locals 1

    .prologue
    .line 28
    sget-object v0, Lcom/google/android/apps/chrome/document/DocumentWebContentsDelegate;->sInstance:Lcom/google/android/apps/chrome/document/DocumentWebContentsDelegate;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/chrome/document/DocumentWebContentsDelegate;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/document/DocumentWebContentsDelegate;-><init>()V

    sput-object v0, Lcom/google/android/apps/chrome/document/DocumentWebContentsDelegate;->sInstance:Lcom/google/android/apps/chrome/document/DocumentWebContentsDelegate;

    .line 29
    :cond_0
    sget-object v0, Lcom/google/android/apps/chrome/document/DocumentWebContentsDelegate;->sInstance:Lcom/google/android/apps/chrome/document/DocumentWebContentsDelegate;

    return-object v0
.end method

.method private native nativeAttachContents(JJ)V
.end method

.method private native nativeInitialize()J
.end method


# virtual methods
.method public attachDelegate(J)V
    .locals 3

    .prologue
    .line 37
    iget-wide v0, p0, Lcom/google/android/apps/chrome/document/DocumentWebContentsDelegate;->mNativePtr:J

    invoke-direct {p0, v0, v1, p1, p2}, Lcom/google/android/apps/chrome/document/DocumentWebContentsDelegate;->nativeAttachContents(JJ)V

    .line 38
    return-void
.end method
