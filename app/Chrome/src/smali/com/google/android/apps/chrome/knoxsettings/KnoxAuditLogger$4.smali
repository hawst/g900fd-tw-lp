.class final Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$4;
.super Ljava/lang/Object;
.source "KnoxAuditLogger.java"

# interfaces
.implements Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Observer;


# instance fields
.field final synthetic val$componentName:Ljava/lang/String;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$event:Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$AuditEvent;

.field final synthetic val$knoxSettings:Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;

.field final synthetic val$message:Ljava/lang/String;

.field final synthetic val$url:Ljava/lang/String;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$AuditEvent;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;)V
    .locals 0

    .prologue
    .line 115
    iput-object p1, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$4;->val$context:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$4;->val$event:Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$AuditEvent;

    iput-object p3, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$4;->val$componentName:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$4;->val$url:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$4;->val$message:Ljava/lang/String;

    iput-object p6, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$4;->val$knoxSettings:Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onSettingsAvailable(Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;)V
    .locals 5

    .prologue
    .line 118
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->getAuditLogEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 119
    iget-object v0, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$4;->val$context:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$4;->val$event:Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$AuditEvent;

    iget-object v2, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$4;->val$componentName:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$4;->val$url:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$4;->val$message:Ljava/lang/String;

    # invokes: Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger;->log(Landroid/content/Context;Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$AuditEvent;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger;->access$100(Landroid/content/Context;Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$AuditEvent;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$4;->val$knoxSettings:Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;->removeObserver(Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Observer;)V

    .line 122
    return-void
.end method
