.class Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$1;
.super Ljava/lang/Object;
.source "FindToolbar.java"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;)V
    .locals 0

    .prologue
    .line 117
    iput-object p1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$1;->this$0:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFocusChange(Landroid/view/View;Z)V
    .locals 2

    .prologue
    .line 120
    if-nez p2, :cond_1

    .line 121
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$1;->this$0:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;

    iget-object v0, v0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mFindQuery:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$FindQuery;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$FindQuery;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 122
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$1;->this$0:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mSearchKeyShouldTriggerSearch:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->access$202(Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;Z)Z

    .line 124
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$1;->this$0:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;

    iget-object v0, v0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->mFindQuery:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$FindQuery;

    invoke-static {v0}, Lorg/chromium/ui/UiUtils;->hideKeyboard(Landroid/view/View;)Z

    .line 126
    :cond_1
    return-void
.end method
