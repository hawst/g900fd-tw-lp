.class public Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter;
.super Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;
.source "ContextualSearchEventFilter.java"


# instance fields
.field private mEventTarget:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter$EventTarget;

.field private final mGestureDetector:Landroid/view/GestureDetector;

.field private mGestureOrientation:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter$GestureOrientation;

.field private mHasChangedEventTarget:Z

.field private mHasDeterminedEventTarget:Z

.field private mHasDeterminedGestureOrientation:Z

.field private mInitialEventY:F

.field private mIsDeterminingEventTarget:Z

.field private mIsRecordingEvents:Z

.field private mManagementDelegate:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManagementDelegate;

.field private mMayChangeEventTarget:Z

.field private mPreviousEventTarget:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter$EventTarget;

.field private final mRecordedEvents:Ljava/util/ArrayList;

.field private mSearchState:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;

.field private mSyntheticActionDownX:F

.field private mSyntheticActionDownY:F

.field private final mTouchSlopSquarePx:F

.field private mWasActionDownEventSynthetic:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/chrome/eventfilter/EventFilterHost;Lcom/google/android/apps/chrome/eventfilter/GestureHandler;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 149
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v5, v4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;-><init>(Landroid/content/Context;Lcom/google/android/apps/chrome/eventfilter/EventFilterHost;Lcom/google/android/apps/chrome/eventfilter/GestureHandler;ZZ)V

    .line 131
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter;->mRecordedEvents:Ljava/util/ArrayList;

    .line 151
    new-instance v0, Landroid/view/GestureDetector;

    new-instance v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter$InternalGestureDetector;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter$InternalGestureDetector;-><init>(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter$1;)V

    invoke-direct {v0, p1, v1}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter;->mGestureDetector:Landroid/view/GestureDetector;

    .line 152
    iput-object p4, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter;->mSearchState:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;

    .line 156
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    int-to-float v0, v0

    .line 157
    mul-float/2addr v0, v0

    iput v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter;->mTouchSlopSquarePx:F

    .line 159
    invoke-direct {p0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter;->reset()V

    .line 160
    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter;Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter;->handleSingleTapUp(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter;Landroid/view/MotionEvent;Landroid/view/MotionEvent;F)Z
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter;->handleScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;F)Z

    move-result v0

    return v0
.end method

.method private createEvent(Landroid/view/MotionEvent;IF)Landroid/view/MotionEvent;
    .locals 8

    .prologue
    .line 383
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getDownTime()J

    move-result-wide v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v5

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getMetaState()I

    move-result v7

    move v4, p2

    move v6, p3

    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v0

    return-object v0
.end method

.method private determineEventTarget(F)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 453
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter;->mGestureOrientation:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter$GestureOrientation;

    sget-object v3, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter$GestureOrientation;->VERTICAL:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter$GestureOrientation;

    if-ne v0, v3, :cond_1

    move v0, v1

    .line 456
    :goto_0
    iget-object v3, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter;->mSearchState:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->isMaximized()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 459
    cmpg-float v3, p1, v4

    if-gez v3, :cond_2

    move v3, v1

    .line 460
    :goto_1
    if-eqz v0, :cond_3

    if-eqz v3, :cond_3

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter;->getSearchContentViewVerticalScroll()F

    move-result v0

    cmpl-float v0, v0, v4

    if-nez v0, :cond_3

    move v0, v1

    .line 469
    :cond_0
    :goto_2
    iget-object v3, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter;->mEventTarget:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter$EventTarget;

    iput-object v3, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter;->mPreviousEventTarget:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter$EventTarget;

    .line 470
    if-eqz v0, :cond_4

    sget-object v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter$EventTarget;->SEARCH_PANEL:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter$EventTarget;

    :goto_3
    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter;->setEventTarget(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter$EventTarget;)V

    .line 473
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter;->mEventTarget:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter$EventTarget;

    iget-object v3, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter;->mPreviousEventTarget:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter$EventTarget;

    if-eq v0, v3, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter;->mPreviousEventTarget:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter$EventTarget;

    sget-object v3, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter$EventTarget;->UNDETERMINED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter$EventTarget;

    if-eq v0, v3, :cond_5

    :goto_4
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter;->mHasChangedEventTarget:Z

    .line 475
    return-void

    :cond_1
    move v0, v2

    .line 453
    goto :goto_0

    :cond_2
    move v3, v2

    .line 459
    goto :goto_1

    :cond_3
    move v0, v2

    .line 460
    goto :goto_2

    .line 470
    :cond_4
    sget-object v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter$EventTarget;->SEARCH_CONTENT_VIEW:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter$EventTarget;

    goto :goto_3

    :cond_5
    move v1, v2

    .line 473
    goto :goto_4
.end method

.method private determineGestureOrientation(Landroid/view/MotionEvent;Landroid/view/MotionEvent;)V
    .locals 3

    .prologue
    .line 438
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    .line 439
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    sub-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    .line 440
    const/high16 v2, 0x3fa00000    # 1.25f

    mul-float/2addr v1, v2

    cmpl-float v0, v1, v0

    if-lez v0, :cond_0

    sget-object v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter$GestureOrientation;->VERTICAL:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter$GestureOrientation;

    :goto_0
    iput-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter;->mGestureOrientation:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter$GestureOrientation;

    .line 442
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter;->mHasDeterminedGestureOrientation:Z

    .line 443
    return-void

    .line 440
    :cond_0
    sget-object v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter$GestureOrientation;->HORIZONTAL:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter$GestureOrientation;

    goto :goto_0
.end method

.method private handleScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;F)Z
    .locals 1

    .prologue
    .line 416
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter;->mHasDeterminedGestureOrientation:Z

    if-nez v0, :cond_0

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter;->isDistanceGreaterThanTouchSlop(Landroid/view/MotionEvent;Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 417
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter;->determineGestureOrientation(Landroid/view/MotionEvent;Landroid/view/MotionEvent;)V

    .line 424
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter;->mHasDeterminedGestureOrientation:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter;->mHasDeterminedEventTarget:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter;->mMayChangeEventTarget:Z

    if-eqz v0, :cond_2

    .line 426
    :cond_1
    invoke-direct {p0, p3}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter;->determineEventTarget(F)V

    .line 429
    :cond_2
    const/4 v0, 0x0

    return v0
.end method

.method private handleSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    .line 398
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter;->mSearchState:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    iget v2, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter;->mPxToDp:F

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->isYCoordinateInsideSearchContentView(F)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter$EventTarget;->SEARCH_CONTENT_VIEW:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter$EventTarget;

    :goto_0
    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter;->setEventTarget(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter$EventTarget;)V

    .line 401
    const/4 v0, 0x0

    return v0

    .line 398
    :cond_0
    sget-object v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter$EventTarget;->SEARCH_PANEL:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter$EventTarget;

    goto :goto_0
.end method

.method private isDistanceGreaterThanTouchSlop(FF)Z
    .locals 2

    .prologue
    .line 506
    mul-float v0, p1, p1

    mul-float v1, p2, p2

    add-float/2addr v0, v1

    iget v1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter;->mTouchSlopSquarePx:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isDistanceGreaterThanTouchSlop(Landroid/view/MotionEvent;Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    .line 494
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    sub-float/2addr v0, v1

    .line 495
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    sub-float/2addr v1, v2

    .line 497
    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter;->isDistanceGreaterThanTouchSlop(FF)Z

    move-result v0

    return v0
.end method

.method private propagateAndRecycleEvent(Landroid/view/MotionEvent;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter$EventTarget;)V
    .locals 0

    .prologue
    .line 313
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter;->propagateEvent(Landroid/view/MotionEvent;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter$EventTarget;)V

    .line 314
    invoke-virtual {p1}, Landroid/view/MotionEvent;->recycle()V

    .line 315
    return-void
.end method

.method private propagateEvent(Landroid/view/MotionEvent;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter$EventTarget;)V
    .locals 1

    .prologue
    .line 323
    sget-object v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter$EventTarget;->SEARCH_PANEL:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter$EventTarget;

    if-ne p2, v0, :cond_1

    .line 324
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->onTouchEventInternal(Landroid/view/MotionEvent;)Z

    .line 328
    :cond_0
    :goto_0
    return-void

    .line 325
    :cond_1
    sget-object v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter$EventTarget;->SEARCH_CONTENT_VIEW:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter$EventTarget;

    if-ne p2, v0, :cond_0

    .line 326
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter;->propagateEventToSearchContentView(Landroid/view/MotionEvent;)V

    goto :goto_0
.end method

.method private reset()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 234
    sget-object v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter$EventTarget;->UNDETERMINED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter$EventTarget;

    iput-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter;->mEventTarget:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter$EventTarget;

    .line 235
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter;->mIsDeterminingEventTarget:Z

    .line 236
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter;->mHasDeterminedEventTarget:Z

    .line 238
    sget-object v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter$EventTarget;->UNDETERMINED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter$EventTarget;

    iput-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter;->mPreviousEventTarget:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter$EventTarget;

    .line 239
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter;->mHasChangedEventTarget:Z

    .line 240
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter;->mMayChangeEventTarget:Z

    .line 242
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter;->mWasActionDownEventSynthetic:Z

    .line 244
    sget-object v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter$GestureOrientation;->UNDETERMINED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter$GestureOrientation;

    iput-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter;->mGestureOrientation:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter$GestureOrientation;

    .line 245
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter;->mHasDeterminedGestureOrientation:Z

    .line 246
    return-void
.end method

.method private resumeAndPropagateEvent(Landroid/view/MotionEvent;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 261
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter;->mIsRecordingEvents:Z

    if-eqz v0, :cond_0

    .line 262
    invoke-direct {p0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter;->resumeRecordedEvents()V

    .line 265
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter;->mHasChangedEventTarget:Z

    if-eqz v0, :cond_1

    .line 267
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    .line 271
    const/4 v1, 0x3

    invoke-direct {p0, p1, v1, v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter;->createEvent(Landroid/view/MotionEvent;IF)Landroid/view/MotionEvent;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter;->mPreviousEventTarget:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter$EventTarget;

    invoke-direct {p0, v1, v2}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter;->propagateAndRecycleEvent(Landroid/view/MotionEvent;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter$EventTarget;)V

    .line 276
    invoke-direct {p0, p1, v4, v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter;->createEvent(Landroid/view/MotionEvent;IF)Landroid/view/MotionEvent;

    move-result-object v0

    .line 280
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter;->mWasActionDownEventSynthetic:Z

    .line 281
    invoke-virtual {v0}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    iput v1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter;->mSyntheticActionDownX:F

    .line 282
    invoke-virtual {v0}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter;->mSearchState:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->getSearchContentViewOffsetY()F

    move-result v2

    iget v3, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter;->mPxToDp:F

    div-float/2addr v2, v3

    sub-float/2addr v1, v2

    iput v1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter;->mSyntheticActionDownY:F

    .line 285
    iget-object v1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter;->mEventTarget:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter$EventTarget;

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter;->propagateAndRecycleEvent(Landroid/view/MotionEvent;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter$EventTarget;)V

    .line 287
    iput-boolean v4, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter;->mHasChangedEventTarget:Z

    .line 290
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter;->mEventTarget:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter$EventTarget;

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter;->propagateEvent(Landroid/view/MotionEvent;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter$EventTarget;)V

    .line 291
    return-void
.end method

.method private resumeRecordedEvents()V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 297
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter;->mRecordedEvents:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v1, v2

    :goto_0
    if-ge v1, v3, :cond_0

    .line 298
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter;->mRecordedEvents:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/MotionEvent;

    iget-object v4, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter;->mEventTarget:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter$EventTarget;

    invoke-direct {p0, v0, v4}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter;->propagateAndRecycleEvent(Landroid/view/MotionEvent;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter$EventTarget;)V

    .line 297
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 301
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter;->mRecordedEvents:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 302
    iput-boolean v2, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter;->mIsRecordingEvents:Z

    .line 303
    return-void
.end method

.method private setEventTarget(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter$EventTarget;)V
    .locals 1

    .prologue
    .line 482
    iput-object p1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter;->mEventTarget:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter$EventTarget;

    .line 484
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter;->mIsDeterminingEventTarget:Z

    .line 485
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter;->mHasDeterminedEventTarget:Z

    .line 486
    return-void
.end method


# virtual methods
.method protected getSearchContentViewVerticalScroll()F
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter;->mManagementDelegate:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManagementDelegate;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManagementDelegate;->getSearchContentViewVerticalScroll()F

    move-result v0

    return v0
.end method

.method public onTouchEventInternal(Landroid/view/MotionEvent;)Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 181
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    .line 183
    const/4 v1, 0x5

    if-ne v0, v1, :cond_3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter;->mSearchState:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->isMaximized()Z

    move-result v1

    if-nez v1, :cond_3

    .line 189
    sget-object v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter$EventTarget;->SEARCH_PANEL:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter$EventTarget;

    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter;->setEventTarget(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter$EventTarget;)V

    .line 208
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter;->mGestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v1, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 210
    iget-boolean v1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter;->mHasDeterminedEventTarget:Z

    if-eqz v1, :cond_5

    .line 213
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter;->resumeAndPropagateEvent(Landroid/view/MotionEvent;)V

    .line 222
    :goto_1
    if-eq v0, v4, :cond_1

    const/4 v1, 0x3

    if-ne v0, v1, :cond_2

    .line 223
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter;->reset()V

    .line 226
    :cond_2
    return v4

    .line 190
    :cond_3
    iget-boolean v1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter;->mIsDeterminingEventTarget:Z

    if-nez v1, :cond_0

    if-nez v0, :cond_0

    .line 191
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    iput v1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter;->mInitialEventY:F

    .line 192
    iget-object v1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter;->mSearchState:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;

    iget v2, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter;->mInitialEventY:F

    iget v3, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter;->mPxToDp:F

    mul-float/2addr v2, v3

    invoke-virtual {v1, v2}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->isYCoordinateInsideSearchContentView(F)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 197
    iput-boolean v4, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter;->mIsDeterminingEventTarget:Z

    .line 198
    iput-boolean v4, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter;->mMayChangeEventTarget:Z

    goto :goto_0

    .line 202
    :cond_4
    sget-object v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter$EventTarget;->SEARCH_PANEL:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter$EventTarget;

    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter;->setEventTarget(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter$EventTarget;)V

    .line 203
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter;->mMayChangeEventTarget:Z

    goto :goto_0

    .line 217
    :cond_5
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v1

    .line 218
    iget-object v2, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter;->mRecordedEvents:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 219
    iput-boolean v4, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter;->mIsRecordingEvents:Z

    goto :goto_1
.end method

.method protected propagateEventToSearchContentView(Landroid/view/MotionEvent;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 336
    .line 338
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter;->mGestureOrientation:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter$GestureOrientation;

    sget-object v3, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter$GestureOrientation;->HORIZONTAL:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter$GestureOrientation;

    if-ne v0, v3, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter;->mSearchState:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->isMaximized()Z

    move-result v0

    if-nez v0, :cond_3

    .line 343
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    iget v3, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter;->mInitialEventY:F

    invoke-direct {p0, p1, v0, v3}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter;->createEvent(Landroid/view/MotionEvent;IF)Landroid/view/MotionEvent;

    move-result-object p1

    move v0, v1

    .line 347
    :goto_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v3

    .line 348
    iget-object v4, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter;->mSearchState:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->getSearchContentViewOffsetY()F

    move-result v4

    iget v5, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter;->mPxToDp:F

    div-float/2addr v4, v5

    .line 351
    const/4 v5, 0x0

    neg-float v4, v4

    invoke-virtual {p1, v5, v4}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    .line 354
    iget-boolean v4, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter;->mWasActionDownEventSynthetic:Z

    if-eqz v4, :cond_0

    if-ne v3, v1, :cond_0

    .line 355
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    iget v4, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter;->mSyntheticActionDownX:F

    sub-float/2addr v3, v4

    .line 356
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    iget v5, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter;->mSyntheticActionDownY:F

    sub-float/2addr v4, v5

    .line 361
    invoke-direct {p0, v3, v4}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter;->isDistanceGreaterThanTouchSlop(FF)Z

    move-result v3

    if-nez v3, :cond_0

    .line 362
    const/4 v2, 0x3

    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->setAction(I)V

    .line 363
    iget-object v2, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter;->mHost:Lcom/google/android/apps/chrome/eventfilter/EventFilterHost;

    invoke-interface {v2, p1}, Lcom/google/android/apps/chrome/eventfilter/EventFilterHost;->propagateEvent(Landroid/view/MotionEvent;)Z

    move v2, v1

    .line 369
    :cond_0
    if-nez v2, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter;->mHost:Lcom/google/android/apps/chrome/eventfilter/EventFilterHost;

    invoke-interface {v1, p1}, Lcom/google/android/apps/chrome/eventfilter/EventFilterHost;->propagateEvent(Landroid/view/MotionEvent;)Z

    .line 372
    :cond_1
    if-eqz v0, :cond_2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->recycle()V

    .line 373
    :cond_2
    return-void

    :cond_3
    move v0, v2

    goto :goto_0
.end method

.method public setContextualSearchManagementDelegate(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManagementDelegate;)V
    .locals 0

    .prologue
    .line 166
    iput-object p1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter;->mManagementDelegate:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManagementDelegate;

    .line 167
    return-void
.end method
