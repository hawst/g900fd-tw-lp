.class public Lcom/google/android/apps/chrome/preferences/ContentPreferences;
.super Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;
.source "ContentPreferences.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field private mLocationPreference:Landroid/preference/Preference;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;-><init>()V

    .line 27
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/preferences/ContentPreferences;->mLocationPreference:Landroid/preference/Preference;

    return-void
.end method

.method private setProtectedStateSummary()V
    .locals 2

    .prologue
    .line 105
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-ge v0, v1, :cond_1

    .line 116
    :cond_0
    :goto_0
    return-void

    .line 107
    :cond_1
    const-string/jumbo v0, "protected_content"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/ContentPreferences;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceScreen;

    .line 109
    if-eqz v0, :cond_0

    .line 110
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/ContentPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getPrefEnableProtectedContent()Z

    move-result v1

    .line 113
    if-eqz v1, :cond_2

    sget v1, Lcom/google/android/apps/chrome/R$string;->text_on:I

    :goto_1
    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->setSummary(I)V

    goto :goto_0

    :cond_2
    sget v1, Lcom/google/android/apps/chrome/R$string;->text_off:I

    goto :goto_1
.end method

.method private setTranslateStateSummary()V
    .locals 2

    .prologue
    .line 119
    const-string/jumbo v0, "translate"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/ContentPreferences;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceScreen;

    .line 121
    if-eqz v0, :cond_0

    .line 122
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/ContentPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getPrefEnableTranslate()Z

    move-result v1

    .line 124
    if-eqz v1, :cond_1

    sget v1, Lcom/google/android/apps/chrome/R$string;->text_on:I

    :goto_0
    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->setSummary(I)V

    .line 127
    :cond_0
    return-void

    .line 124
    :cond_1
    sget v1, Lcom/google/android/apps/chrome/R$string;->text_off:I

    goto :goto_0
.end method

.method private updateSummaries()V
    .locals 2

    .prologue
    .line 76
    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/ContentPreferences;->setProtectedStateSummary()V

    .line 77
    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/ContentPreferences;->setTranslateStateSummary()V

    .line 82
    const-string/jumbo v0, "translate"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/ContentPreferences;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 83
    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->isTranslateManaged()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 85
    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ManagedPreferencesUtils;->getManagedByEnterpriseIconId()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setIcon(I)V

    .line 87
    :cond_0
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 31
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    .line 32
    sget v0, Lcom/google/android/apps/chrome/R$xml;->content_preferences:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/ContentPreferences;->addPreferencesFromResource(I)V

    .line 33
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-ge v0, v1, :cond_0

    .line 34
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/ContentPreferences;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    const-string/jumbo v1, "protected_content"

    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/preferences/ContentPreferences;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 37
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 38
    const-string/jumbo v0, "accept_cookies"

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 39
    const-string/jumbo v0, "enable_javascript"

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 40
    const-string/jumbo v0, "block_popups"

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 42
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/ContentPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/preferences/GoogleLocationSettingsUtil;->useInternalLocationSetting(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 45
    const-string/jumbo v0, "enable_location"

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 46
    const-string/jumbo v0, "google_location_settings"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/ContentPreferences;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceScreen;

    .line 48
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/ContentPreferences;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 49
    const-string/jumbo v0, "enable_location"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/ContentPreferences;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 65
    :goto_0
    iput-object v0, p0, Lcom/google/android/apps/chrome/preferences/ContentPreferences;->mLocationPreference:Landroid/preference/Preference;

    .line 67
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 68
    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/ContentPreferences;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 69
    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    goto :goto_1

    .line 52
    :cond_1
    const-string/jumbo v0, "enable_location"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/ContentPreferences;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/preferences/ChromeBaseCheckBoxPreference;

    .line 55
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/ContentPreferences;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 56
    const-string/jumbo v0, "google_location_settings"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/ContentPreferences;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceScreen;

    .line 58
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/ContentPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/chrome/preferences/GoogleLocationSettingsUtil;->getLocationSettingsTitleResource(Landroid/content/Context;)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/preference/PreferenceScreen;->setTitle(I)V

    .line 61
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/ContentPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/chrome/preferences/GoogleLocationSettingsUtil;->getLocationServicesIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/preference/PreferenceScreen;->setIntent(Landroid/content/Intent;)V

    goto :goto_0

    .line 72
    :cond_2
    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/ContentPreferences;->updateSummaries()V

    .line 73
    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 98
    invoke-virtual {p1}, Landroid/preference/Preference;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 101
    :goto_0
    return v0

    .line 99
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/ContentPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->setPreferenceChanged(Landroid/preference/Preference;Ljava/lang/Object;)V

    .line 101
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 91
    invoke-super {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;->onResume()V

    .line 92
    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->update()V

    .line 93
    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/ContentPreferences;->updateSummaries()V

    .line 94
    return-void
.end method
