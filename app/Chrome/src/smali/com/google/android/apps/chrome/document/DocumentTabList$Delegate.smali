.class public interface abstract Lcom/google/android/apps/chrome/document/DocumentTabList$Delegate;
.super Ljava/lang/Object;
.source "DocumentTabList.java"


# virtual methods
.method public abstract bringTabToForeground(ZI)V
.end method

.method public abstract closeTab(ZI)V
.end method

.method public abstract createTab(ZJI)V
.end method

.method public abstract getLastUsedTabId()I
.end method

.method public abstract getTasksFromRecents(Z)Landroid/util/SparseArray;
.end method

.method public abstract isValidActivity(ZLandroid/content/Intent;)Z
.end method
