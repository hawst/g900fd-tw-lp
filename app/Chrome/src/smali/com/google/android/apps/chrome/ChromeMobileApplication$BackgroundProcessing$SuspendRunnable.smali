.class Lcom/google/android/apps/chrome/ChromeMobileApplication$BackgroundProcessing$SuspendRunnable;
.super Ljava/lang/Object;
.source "ChromeMobileApplication.java"

# interfaces
.implements Ljava/lang/Runnable;


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/ChromeMobileApplication$BackgroundProcessing;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 110
    const-class v0, Lcom/google/android/apps/chrome/ChromeMobileApplication;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/ChromeMobileApplication$BackgroundProcessing$SuspendRunnable;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Lcom/google/android/apps/chrome/ChromeMobileApplication$BackgroundProcessing;)V
    .locals 0

    .prologue
    .line 110
    iput-object p1, p0, Lcom/google/android/apps/chrome/ChromeMobileApplication$BackgroundProcessing$SuspendRunnable;->this$0:Lcom/google/android/apps/chrome/ChromeMobileApplication$BackgroundProcessing;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/chrome/ChromeMobileApplication$BackgroundProcessing;Lcom/google/android/apps/chrome/ChromeMobileApplication$1;)V
    .locals 0

    .prologue
    .line 110
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/ChromeMobileApplication$BackgroundProcessing$SuspendRunnable;-><init>(Lcom/google/android/apps/chrome/ChromeMobileApplication$BackgroundProcessing;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 113
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeMobileApplication$BackgroundProcessing$SuspendRunnable;->this$0:Lcom/google/android/apps/chrome/ChromeMobileApplication$BackgroundProcessing;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/apps/chrome/ChromeMobileApplication$BackgroundProcessing;->mSuspendRunnable:Lcom/google/android/apps/chrome/ChromeMobileApplication$BackgroundProcessing$SuspendRunnable;
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/ChromeMobileApplication$BackgroundProcessing;->access$002(Lcom/google/android/apps/chrome/ChromeMobileApplication$BackgroundProcessing;Lcom/google/android/apps/chrome/ChromeMobileApplication$BackgroundProcessing$SuspendRunnable;)Lcom/google/android/apps/chrome/ChromeMobileApplication$BackgroundProcessing$SuspendRunnable;

    .line 114
    sget-boolean v0, Lcom/google/android/apps/chrome/ChromeMobileApplication$BackgroundProcessing$SuspendRunnable;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeMobileApplication$BackgroundProcessing$SuspendRunnable;->this$0:Lcom/google/android/apps/chrome/ChromeMobileApplication$BackgroundProcessing;

    # getter for: Lcom/google/android/apps/chrome/ChromeMobileApplication$BackgroundProcessing;->mWebKitTimersAreSuspended:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/ChromeMobileApplication$BackgroundProcessing;->access$100(Lcom/google/android/apps/chrome/ChromeMobileApplication$BackgroundProcessing;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 115
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeMobileApplication$BackgroundProcessing$SuspendRunnable;->this$0:Lcom/google/android/apps/chrome/ChromeMobileApplication$BackgroundProcessing;

    # setter for: Lcom/google/android/apps/chrome/ChromeMobileApplication$BackgroundProcessing;->mWebKitTimersAreSuspended:Z
    invoke-static {v0, v2}, Lcom/google/android/apps/chrome/ChromeMobileApplication$BackgroundProcessing;->access$102(Lcom/google/android/apps/chrome/ChromeMobileApplication$BackgroundProcessing;Z)Z

    .line 116
    invoke-static {v2}, Lorg/chromium/content/browser/ContentViewStatics;->setWebKitSharedTimersSuspended(Z)V

    .line 117
    return-void
.end method
