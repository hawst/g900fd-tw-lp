.class Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsContainer$2;
.super Ljava/lang/Object;
.source "EnhancedBookmarkItemsContainer.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsContainer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 76
    const-class v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsContainer;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsContainer$2;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsContainer;)V
    .locals 0

    .prologue
    .line 76
    iput-object p1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsContainer$2;->this$0:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsContainer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsContainer$2;->this$0:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsContainer;

    # getter for: Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsContainer;->mItemsAdapter:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsAdapter;
    invoke-static {v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsContainer;->access$100(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsContainer;)Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsAdapter;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsAdapter;->getItemViewType(I)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 89
    sget-boolean v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsContainer$2;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 81
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsContainer$2;->this$0:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsContainer;

    # getter for: Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsContainer;->mDelegate:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;
    invoke-static {v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsContainer;->access$000(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsContainer;)Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsContainer$2;->this$0:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsContainer;

    # getter for: Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsContainer;->mItemsAdapter:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsAdapter;
    invoke-static {v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsContainer;->access$100(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsContainer;)Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsAdapter;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/components/bookmarks/BookmarkId;

    invoke-interface {v1, v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;->setFolderMode(Lorg/chromium/components/bookmarks/BookmarkId;)V

    .line 92
    :cond_0
    :goto_0
    :pswitch_1
    return-void

    .line 86
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsContainer$2;->this$0:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsContainer;

    # getter for: Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsContainer;->mDelegate:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;
    invoke-static {v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsContainer;->access$000(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsContainer;)Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsContainer$2;->this$0:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsContainer;

    # getter for: Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsContainer;->mItemsAdapter:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsAdapter;
    invoke-static {v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsContainer;->access$100(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsContainer;)Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsAdapter;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/components/bookmarks/BookmarkId;

    invoke-interface {v1, v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;->openBookmark(Lorg/chromium/components/bookmarks/BookmarkId;)V

    goto :goto_0

    .line 79
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
