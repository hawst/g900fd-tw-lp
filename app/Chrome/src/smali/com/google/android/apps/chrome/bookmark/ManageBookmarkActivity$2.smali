.class Lcom/google/android/apps/chrome/bookmark/ManageBookmarkActivity$2;
.super Ljava/lang/Object;
.source "ManageBookmarkActivity.java"

# interfaces
.implements Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$OnActionListener;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/bookmark/ManageBookmarkActivity;

.field final synthetic val$addEditFragment:Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/bookmark/ManageBookmarkActivity;Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;)V
    .locals 0

    .prologue
    .line 242
    iput-object p1, p0, Lcom/google/android/apps/chrome/bookmark/ManageBookmarkActivity$2;->this$0:Lcom/google/android/apps/chrome/bookmark/ManageBookmarkActivity;

    iput-object p2, p0, Lcom/google/android/apps/chrome/bookmark/ManageBookmarkActivity$2;->val$addEditFragment:Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private finishAddEdit()V
    .locals 1

    .prologue
    .line 297
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/ManageBookmarkActivity$2;->this$0:Lcom/google/android/apps/chrome/bookmark/ManageBookmarkActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/bookmark/ManageBookmarkActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->getBackStackEntryCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 298
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/ManageBookmarkActivity$2;->this$0:Lcom/google/android/apps/chrome/bookmark/ManageBookmarkActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/bookmark/ManageBookmarkActivity;->finish()V

    .line 302
    :goto_0
    return-void

    .line 300
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/ManageBookmarkActivity$2;->this$0:Lcom/google/android/apps/chrome/bookmark/ManageBookmarkActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/bookmark/ManageBookmarkActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->popBackStackImmediate()Z

    goto :goto_0
.end method


# virtual methods
.method public onCancel()V
    .locals 0

    .prologue
    .line 245
    invoke-direct {p0}, Lcom/google/android/apps/chrome/bookmark/ManageBookmarkActivity$2;->finishAddEdit()V

    .line 246
    return-void
.end method

.method public onFolderCreated(JLjava/lang/String;)V
    .locals 1

    .prologue
    .line 256
    invoke-direct {p0}, Lcom/google/android/apps/chrome/bookmark/ManageBookmarkActivity$2;->finishAddEdit()V

    .line 257
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/ManageBookmarkActivity$2;->this$0:Lcom/google/android/apps/chrome/bookmark/ManageBookmarkActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/bookmark/ManageBookmarkActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->getBackStackEntryCount()I

    move-result v0

    if-eqz v0, :cond_0

    .line 258
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/ManageBookmarkActivity$2;->val$addEditFragment:Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment;->executeFolderSelection(JLjava/lang/String;)V

    .line 261
    :cond_0
    return-void
.end method

.method public onNodeEdited(J)V
    .locals 0

    .prologue
    .line 251
    invoke-direct {p0}, Lcom/google/android/apps/chrome/bookmark/ManageBookmarkActivity$2;->finishAddEdit()V

    .line 252
    return-void
.end method

.method public onRemove()V
    .locals 0

    .prologue
    .line 288
    invoke-direct {p0}, Lcom/google/android/apps/chrome/bookmark/ManageBookmarkActivity$2;->finishAddEdit()V

    .line 289
    return-void
.end method

.method public setBackEnabled(Z)V
    .locals 1

    .prologue
    .line 293
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/ManageBookmarkActivity$2;->this$0:Lcom/google/android/apps/chrome/bookmark/ManageBookmarkActivity;

    # setter for: Lcom/google/android/apps/chrome/bookmark/ManageBookmarkActivity;->mIsBackEnabled:Z
    invoke-static {v0, p1}, Lcom/google/android/apps/chrome/bookmark/ManageBookmarkActivity;->access$102(Lcom/google/android/apps/chrome/bookmark/ManageBookmarkActivity;Z)Z

    .line 294
    return-void
.end method

.method public triggerFolderSelection()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 265
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/ManageBookmarkActivity$2;->this$0:Lcom/google/android/apps/chrome/bookmark/ManageBookmarkActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/bookmark/ManageBookmarkActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v2

    .line 267
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/ManageBookmarkActivity$2;->this$0:Lcom/google/android/apps/chrome/bookmark/ManageBookmarkActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/bookmark/ManageBookmarkActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->getBackStackEntryCount()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 268
    :goto_0
    iget-object v3, p0, Lcom/google/android/apps/chrome/bookmark/ManageBookmarkActivity$2;->val$addEditFragment:Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->getParentFolderId()J

    move-result-wide v4

    iget-object v3, p0, Lcom/google/android/apps/chrome/bookmark/ManageBookmarkActivity$2;->val$addEditFragment:Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->isFolder()Z

    move-result v3

    invoke-static {v0, v4, v5, v3}, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment;->newInstance(ZJZ)Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment;

    move-result-object v3

    .line 275
    iget-object v4, p0, Lcom/google/android/apps/chrome/bookmark/ManageBookmarkActivity$2;->val$addEditFragment:Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;

    invoke-virtual {v3, v4, v1}, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment;->setTargetFragment(Landroid/app/Fragment;I)V

    .line 276
    iget-object v1, p0, Lcom/google/android/apps/chrome/bookmark/ManageBookmarkActivity$2;->this$0:Lcom/google/android/apps/chrome/bookmark/ManageBookmarkActivity;

    # invokes: Lcom/google/android/apps/chrome/bookmark/ManageBookmarkActivity;->setActionListenerOnFolderSelection(Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment;)V
    invoke-static {v1, v3}, Lcom/google/android/apps/chrome/bookmark/ManageBookmarkActivity;->access$000(Lcom/google/android/apps/chrome/bookmark/ManageBookmarkActivity;Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment;)V

    .line 278
    if-eqz v0, :cond_1

    const-string/jumbo v0, "SelectFolder"

    .line 280
    :goto_1
    sget v1, Lcom/google/android/apps/chrome/R$id;->fragment_container:I

    invoke-virtual {v2, v1, v3, v0}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 281
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/ManageBookmarkActivity$2;->val$addEditFragment:Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;

    invoke-virtual {v2, v0}, Landroid/app/FragmentTransaction;->hide(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 282
    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 283
    invoke-virtual {v2}, Landroid/app/FragmentTransaction;->commit()I

    .line 284
    return-void

    :cond_0
    move v0, v1

    .line 267
    goto :goto_0

    .line 278
    :cond_1
    const-string/jumbo v0, "AddFolderSelectFolder"

    goto :goto_1
.end method
