.class Lcom/google/android/apps/chrome/document/DocumentActivity$4;
.super Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;
.source "DocumentActivity.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/document/DocumentActivity;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 592
    const-class v0, Lcom/google/android/apps/chrome/document/DocumentActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/document/DocumentActivity$4;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lcom/google/android/apps/chrome/document/DocumentActivity;)V
    .locals 0

    .prologue
    .line 592
    iput-object p1, p0, Lcom/google/android/apps/chrome/document/DocumentActivity$4;->this$0:Lcom/google/android/apps/chrome/document/DocumentActivity;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 1

    .prologue
    .line 595
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 607
    :pswitch_0
    sget-boolean v0, Lcom/google/android/apps/chrome/document/DocumentActivity$4;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 599
    :pswitch_1
    invoke-static {}, Lcom/google/android/apps/chrome/partnercustomizations/PartnerBrowserCustomizations;->isIncognitoDisabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 600
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity$4;->this$0:Lcom/google/android/apps/chrome/document/DocumentActivity;

    invoke-static {v0}, Lcom/google/android/apps/chrome/document/ChromeLauncherActivity;->closeAllIncognitoTabs(Landroid/content/Context;)V

    .line 609
    :cond_0
    :goto_0
    return-void

    .line 604
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity$4;->this$0:Lcom/google/android/apps/chrome/document/DocumentActivity;

    invoke-static {v0}, Lcom/google/android/apps/chrome/document/ChromeLauncherActivity;->closeAllIncognitoTabs(Landroid/content/Context;)V

    goto :goto_0

    .line 595
    nop

    :pswitch_data_0
    .packed-switch 0x48
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
