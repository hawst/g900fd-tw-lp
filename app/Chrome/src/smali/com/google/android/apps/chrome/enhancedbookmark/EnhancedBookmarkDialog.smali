.class public Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;
.super Landroid/app/DialogFragment;
.source "EnhancedBookmarkDialog.java"

# interfaces
.implements Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;
.implements Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsContainer$ItemsGridScrollListener;


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private mBookmarkModelObserver:Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkModelObserver;

.field private mEnhancedBookmarksModel:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;

.field private mFragmentManager:Landroid/app/FragmentManager;

.field private final mFullShadowScrollDistancePxInv:F

.field private mIsInitializedWithBookmarksBridge:Z

.field private mMode:I

.field private mModeDetail:Ljava/lang/Object;

.field private mShadow:Lcom/google/android/apps/chrome/widget/FadingShadowView;

.field private mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

.field private mUIObservers:Ljava/util/ArrayList;

.field private mUndoController:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkUndoController;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    const-class v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/app/Activity;Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;)V
    .locals 3

    .prologue
    .line 137
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 52
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;->mUIObservers:Ljava/util/ArrayList;

    .line 72
    new-instance v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog$1;-><init>(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;->mBookmarkModelObserver:Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkModelObserver;

    .line 138
    sget-boolean v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 140
    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/chrome/R$dimen;->enhanced_bookmark_full_shadow_scroll_distance:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    div-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;->mFullShadowScrollDistancePxInv:F

    .line 142
    invoke-virtual {p1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;->mFragmentManager:Landroid/app/FragmentManager;

    .line 143
    iput-object p2, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    .line 145
    const-string/jumbo v0, "activity"

    invoke-virtual {p1, v0}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 147
    invoke-virtual {v0}, Landroid/app/ActivityManager;->getMemoryClass()I

    move-result v0

    div-int/lit8 v0, v0, 0x4

    shl-int/lit8 v0, v0, 0xa

    shl-int/lit8 v0, v0, 0xa

    const/high16 v1, 0x2000000

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 149
    new-instance v1, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;

    const/4 v2, 0x0

    invoke-interface {p2, v2}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->getModel(Z)Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v2

    invoke-interface {v2}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->getProfile()Lorg/chromium/chrome/browser/profiles/Profile;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;-><init>(Lorg/chromium/chrome/browser/profiles/Profile;I)V

    iput-object v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;->mEnhancedBookmarksModel:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;

    .line 151
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;->mEnhancedBookmarksModel:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;

    iget-object v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;->mBookmarkModelObserver:Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkModelObserver;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;->addModelObserver(Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkModelObserver;)V

    .line 152
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;)I
    .locals 1

    .prologue
    .line 42
    iget v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;->mMode:I

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;->mModeDetail:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;)Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;->mEnhancedBookmarksModel:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;)Z
    .locals 1

    .prologue
    .line 42
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;->mIsInitializedWithBookmarksBridge:Z

    return v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;->initializeWithBookmarksBridge()V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;->bookmarkModelRefreshed()V

    return-void
.end method

.method private bookmarkModelRefreshed()V
    .locals 2

    .prologue
    .line 204
    iget v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;->mMode:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 205
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;->mModeDetail:Ljava/lang/Object;

    check-cast v0, Lorg/chromium/components/bookmarks/BookmarkId;

    .line 206
    iget-object v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;->mEnhancedBookmarksModel:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;->doesBookmarkExist(Lorg/chromium/components/bookmarks/BookmarkId;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 207
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;->setAllBookmarksMode()V

    .line 210
    :cond_0
    return-void
.end method

.method private initializeWithBookmarksBridge()V
    .locals 1

    .prologue
    .line 192
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;->mIsInitializedWithBookmarksBridge:Z

    .line 193
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;->setAllBookmarksMode()V

    .line 194
    return-void
.end method

.method public static isEnhancedBookmarkEnabled(Lorg/chromium/chrome/browser/profiles/Profile;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 101
    invoke-static {}, Lcom/google/android/apps/chrome/utilities/ChromeBuildInfo;->isLocalBuild()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {}, Lcom/google/android/apps/chrome/utilities/ChromeBuildInfo;->isCanaryBuild()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {}, Lcom/google/android/apps/chrome/utilities/ChromeBuildInfo;->isDevBuild()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    move v2, v0

    .line 104
    :goto_0
    if-eqz v2, :cond_2

    invoke-static {p0}, Lorg/chromium/chrome/browser/BookmarksBridge;->isEnhancedBookmarksEnabled(Lorg/chromium/chrome/browser/profiles/Profile;)Z

    move-result v2

    if-eqz v2, :cond_2

    :goto_1
    return v0

    :cond_1
    move v2, v1

    .line 101
    goto :goto_0

    :cond_2
    move v0, v1

    .line 104
    goto :goto_1
.end method

.method private notifyEnhancedBookmarkDelegateInitialized(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;)V
    .locals 2

    .prologue
    .line 197
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;->mUIObservers:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkUIObserver;

    .line 198
    invoke-interface {v0, p1}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkUIObserver;->onEnhancedBookmarkDelegateInitialized(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;)V

    goto :goto_0

    .line 200
    :cond_0
    return-void
.end method


# virtual methods
.method public closeDialog()V
    .locals 0

    .prologue
    .line 306
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;->dismiss()V

    .line 307
    return-void
.end method

.method public getModel()Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;
    .locals 1

    .prologue
    .line 311
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;->mEnhancedBookmarksModel:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;

    return-object v0
.end method

.method public notifyCurrentModeSet(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkUIObserver;)V
    .locals 3

    .prologue
    .line 259
    iget v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;->mMode:I

    packed-switch v0, :pswitch_data_0

    .line 273
    sget-boolean v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Invalid mode "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;->mMode:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 261
    :pswitch_0
    invoke-interface {p1}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkUIObserver;->onEmptyModeSet()V

    .line 275
    :cond_0
    :goto_0
    return-void

    .line 264
    :pswitch_1
    invoke-interface {p1}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkUIObserver;->onAllBookmarksModeSet()V

    goto :goto_0

    .line 267
    :pswitch_2
    invoke-interface {p1}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkUIObserver;->onUncategorizedModeSet()V

    goto :goto_0

    .line 270
    :pswitch_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;->mModeDetail:Ljava/lang/Object;

    check-cast v0, Lorg/chromium/components/bookmarks/BookmarkId;

    invoke-interface {p1, v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkUIObserver;->onFolderModeSet(Lorg/chromium/components/bookmarks/BookmarkId;)V

    goto :goto_0

    .line 259
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 133
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 134
    const/4 v0, 0x2

    sget v1, Lcom/google/android/apps/chrome/R$style;->EnhancedBookmarkDialog:I

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;->setStyle(II)V

    .line 135
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    .prologue
    .line 178
    new-instance v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog$2;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;->getTheme()I

    move-result v2

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog$2;-><init>(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;Landroid/content/Context;I)V

    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    .prologue
    .line 110
    sget v0, Lcom/google/android/apps/chrome/R$layout;->eb_dialog:I

    invoke-virtual {p1, v0, p2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 111
    iget-object v2, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;->mUIObservers:Ljava/util/ArrayList;

    sget v0, Lcom/google/android/apps/chrome/R$id;->eb_drawer_list:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkUIObserver;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 112
    iget-object v2, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;->mUIObservers:Ljava/util/ArrayList;

    sget v0, Lcom/google/android/apps/chrome/R$id;->eb_action_bar:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkUIObserver;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 113
    sget v0, Lcom/google/android/apps/chrome/R$id;->eb_items_container:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsContainer;

    .line 115
    invoke-virtual {v0, p0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsContainer;->setOnScrollListener(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkItemsContainer$ItemsGridScrollListener;)V

    .line 116
    iget-object v2, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;->mUIObservers:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 118
    invoke-direct {p0, p0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;->notifyEnhancedBookmarkDelegateInitialized(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;)V

    .line 119
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;->mEnhancedBookmarksModel:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;->isBookmarkModelLoaded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 120
    invoke-direct {p0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;->initializeWithBookmarksBridge()V

    .line 123
    :cond_0
    sget v0, Lcom/google/android/apps/chrome/R$id;->shadow:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/widget/FadingShadowView;

    iput-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;->mShadow:Lcom/google/android/apps/chrome/widget/FadingShadowView;

    .line 124
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;->mShadow:Lcom/google/android/apps/chrome/widget/FadingShadowView;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/chrome/R$color;->enhanced_bookmark_shadow_color:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/chrome/widget/FadingShadowView;->init(II)V

    .line 127
    new-instance v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkUndoController;

    iget-object v2, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;->mEnhancedBookmarksModel:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkUndoController;-><init>(Landroid/view/View;Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;->mUndoController:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkUndoController;

    .line 128
    return-object v1
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 163
    invoke-super {p0}, Landroid/app/DialogFragment;->onDestroy()V

    .line 164
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;->mUIObservers:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkUIObserver;

    .line 165
    invoke-interface {v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkUIObserver;->onDestroy()V

    goto :goto_0

    .line 167
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;->mUndoController:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkUndoController;

    if-eqz v0, :cond_1

    .line 168
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;->mUndoController:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkUndoController;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkUndoController;->destroy()V

    .line 169
    iput-object v2, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;->mUndoController:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkUndoController;

    .line 171
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;->mEnhancedBookmarksModel:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;

    iget-object v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;->mBookmarkModelObserver:Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkModelObserver;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;->removeModelObserver(Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkModelObserver;)V

    .line 172
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;->mEnhancedBookmarksModel:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;->destroy()V

    .line 173
    iput-object v2, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;->mEnhancedBookmarksModel:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;

    .line 174
    return-void
.end method

.method public onVerticalScrollOffsetChanged(I)V
    .locals 3

    .prologue
    .line 216
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;->mShadow:Lcom/google/android/apps/chrome/widget/FadingShadowView;

    int-to-float v1, p1

    iget v2, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;->mFullShadowScrollDistancePxInv:F

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/widget/FadingShadowView;->setStrength(F)V

    .line 217
    return-void
.end method

.method public openBookmark(Lorg/chromium/components/bookmarks/BookmarkId;)V
    .locals 5

    .prologue
    .line 290
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->getCurrentTab()Lorg/chromium/chrome/browser/Tab;

    move-result-object v0

    .line 292
    new-instance v1, Lorg/chromium/content_public/browser/LoadUrlParams;

    iget-object v2, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;->mEnhancedBookmarksModel:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;->getBookmarkById(Lorg/chromium/components/bookmarks/BookmarkId;)Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;

    move-result-object v2

    invoke-virtual {v2}, Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;->getUrl()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/chromium/content_public/browser/LoadUrlParams;-><init>(Ljava/lang/String;)V

    .line 294
    if-nez v0, :cond_0

    .line 295
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    sget-object v2, Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;->FROM_MENU_OR_OVERVIEW:Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    invoke-interface {v4}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->isIncognitoSelected()Z

    move-result v4

    invoke-interface {v0, v1, v2, v3, v4}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->openNewTab(Lorg/chromium/content_public/browser/LoadUrlParams;Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;Lorg/chromium/chrome/browser/Tab;Z)Lorg/chromium/chrome/browser/Tab;

    .line 301
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;->closeDialog()V

    .line 302
    return-void

    .line 298
    :cond_0
    invoke-virtual {v0, v1}, Lorg/chromium/chrome/browser/Tab;->loadUrl(Lorg/chromium/content_public/browser/LoadUrlParams;)I

    goto :goto_0
.end method

.method public setAllBookmarksMode()V
    .locals 2

    .prologue
    .line 232
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;->mMode:I

    .line 233
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;->mModeDetail:Ljava/lang/Object;

    .line 234
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;->mUIObservers:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkUIObserver;

    .line 235
    invoke-interface {v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkUIObserver;->onAllBookmarksModeSet()V

    goto :goto_0

    .line 237
    :cond_0
    return-void
.end method

.method public setFolderMode(Lorg/chromium/components/bookmarks/BookmarkId;)V
    .locals 2

    .prologue
    .line 250
    const/4 v0, 0x3

    iput v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;->mMode:I

    .line 251
    iput-object p1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;->mModeDetail:Ljava/lang/Object;

    .line 252
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;->mUIObservers:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkUIObserver;

    .line 253
    invoke-interface {v0, p1}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkUIObserver;->onFolderModeSet(Lorg/chromium/components/bookmarks/BookmarkId;)V

    goto :goto_0

    .line 255
    :cond_0
    return-void
.end method

.method public setUncategorizedMode()V
    .locals 2

    .prologue
    .line 241
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;->mMode:I

    .line 242
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;->mModeDetail:Ljava/lang/Object;

    .line 243
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;->mUIObservers:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkUIObserver;

    .line 244
    invoke-interface {v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkUIObserver;->onUncategorizedModeSet()V

    goto :goto_0

    .line 246
    :cond_0
    return-void
.end method

.method public show()V
    .locals 2

    .prologue
    .line 158
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;->mFragmentManager:Landroid/app/FragmentManager;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-super {p0, v0, v1}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 159
    return-void
.end method

.method public toggleDrawer()V
    .locals 3

    .prologue
    const v2, 0x800003

    .line 279
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;->getView()Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_0

    .line 286
    :goto_0
    return-void

    .line 280
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;->getView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$id;->eb_drawer_layout:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/DrawerLayout;

    .line 281
    invoke-virtual {v0, v2}, Landroid/support/v4/widget/DrawerLayout;->e(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 282
    invoke-virtual {v0, v2}, Landroid/support/v4/widget/DrawerLayout;->d(I)V

    goto :goto_0

    .line 284
    :cond_1
    invoke-virtual {v0, v2}, Landroid/support/v4/widget/DrawerLayout;->c(I)V

    goto :goto_0
.end method
