.class Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV18$PlaybackPositionUpdateListener;
.super Ljava/lang/Object;
.source "LockScreenTransportControlV18.java"

# interfaces
.implements Landroid/media/RemoteControlClient$OnPlaybackPositionUpdateListener;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV18;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV18;)V
    .locals 0

    .prologue
    .line 71
    iput-object p1, p0, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV18$PlaybackPositionUpdateListener;->this$0:Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV18;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV18;Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV18$1;)V
    .locals 0

    .prologue
    .line 71
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV18$PlaybackPositionUpdateListener;-><init>(Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV18;)V

    return-void
.end method


# virtual methods
.method public onPlaybackPositionUpdate(J)V
    .locals 3

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV18$PlaybackPositionUpdateListener;->this$0:Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV18;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/videofling/LockScreenTransportControlV18;->getListeners()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/videofling/TransportControl$Listener;

    .line 77
    long-to-int v2, p1

    invoke-interface {v0, v2}, Lcom/google/android/apps/chrome/videofling/TransportControl$Listener;->onSeek(I)V

    goto :goto_0

    .line 78
    :cond_0
    return-void
.end method
