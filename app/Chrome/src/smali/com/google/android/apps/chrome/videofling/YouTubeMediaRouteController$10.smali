.class Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$10;
.super Ljava/lang/Object;
.source "YouTubeMediaRouteController.java"

# interfaces
.implements Lcom/google/android/gms/common/api/o;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;)V
    .locals 0

    .prologue
    .line 682
    iput-object p1, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$10;->this$0:Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onResult(Lcom/google/android/gms/cast/c;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 685
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$10;->this$0:Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;

    # getter for: Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mDebug:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->access$1100(Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "YouTubeMediaRouteController"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "launchApplication.onResult(): "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 687
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$10;->this$0:Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;

    # getter for: Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mConnectedClient:Lcom/google/android/gms/common/api/i;
    invoke-static {v0}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->access$300(Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;)Lcom/google/android/gms/common/api/i;

    move-result-object v0

    if-nez v0, :cond_2

    .line 735
    :cond_1
    :goto_0
    return-void

    .line 689
    :cond_2
    invoke-interface {p1}, Lcom/google/android/gms/cast/c;->a()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    .line 690
    iget-object v1, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$10;->this$0:Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;

    invoke-interface {p1}, Lcom/google/android/gms/cast/c;->b()Ljava/lang/String;

    move-result-object v2

    # setter for: Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mYouTubeSessionId:Ljava/lang/String;
    invoke-static {v1, v2}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->access$1402(Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;Ljava/lang/String;)Ljava/lang/String;

    .line 691
    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->e()Z

    move-result v0

    if-nez v0, :cond_3

    .line 692
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$10;->this$0:Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->release()V

    goto :goto_0

    .line 696
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$10;->this$0:Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;

    # getter for: Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mYouTubeLocalUrl:Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->access$600(Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_5

    .line 697
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$10;->this$0:Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;

    # getter for: Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mDebug:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->access$1100(Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string/jumbo v0, "YouTubeMediaRouteController"

    const-string/jumbo v1, "No youtube url to pass to the receiver"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 698
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$10;->this$0:Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->release()V

    .line 699
    invoke-static {}, Lorg/chromium/base/library_loader/LibraryLoader;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 700
    invoke-static {v4}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->castYouTubePlayerResult(Z)V

    goto :goto_0

    .line 705
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$10;->this$0:Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;

    iget-object v1, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$10;->this$0:Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;

    # getter for: Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mYouTubeLocalUrl:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->access$600(Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;)Ljava/lang/String;

    move-result-object v1

    # invokes: Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->getVideoId(Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->access$1500(Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 706
    iget-object v1, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$10;->this$0:Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;

    # getter for: Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mDebug:Z
    invoke-static {v1}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->access$1100(Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;)Z

    move-result v1

    if-eqz v1, :cond_6

    const-string/jumbo v1, "YouTubeMediaRouteController"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "videoId is "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 708
    :cond_6
    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$10;->this$0:Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;

    const-wide/16 v2, 0x0

    # invokes: Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->createSendMessage(Ljava/lang/String;J)Ljava/lang/String;
    invoke-static {v1, v0, v2, v3}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->access$1600(Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v0

    .line 709
    sget-object v1, Lcom/google/android/gms/cast/a;->c:Lcom/google/android/gms/cast/d;

    iget-object v2, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$10;->this$0:Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;

    # getter for: Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mConnectedClient:Lcom/google/android/gms/common/api/i;
    invoke-static {v2}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->access$300(Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;)Lcom/google/android/gms/common/api/i;

    move-result-object v2

    const-string/jumbo v3, "urn:x-cast:com.google.youtube.mdx"

    invoke-virtual {v1, v2, v3, v0}, Lcom/google/android/gms/cast/d;->a(Lcom/google/android/gms/common/api/i;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$10$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$10$1;-><init>(Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$10;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/l;->a(Lcom/google/android/gms/common/api/o;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 728
    :catch_0
    move-exception v0

    .line 729
    const-string/jumbo v1, "YouTubeMediaRouteController"

    const-string/jumbo v2, "Failed to send the message with video id"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 730
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$10;->this$0:Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->release()V

    .line 731
    invoke-static {}, Lorg/chromium/base/library_loader/LibraryLoader;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 732
    invoke-static {v4}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->castYouTubePlayerResult(Z)V

    goto/16 :goto_0
.end method

.method public bridge synthetic onResult(Lcom/google/android/gms/common/api/n;)V
    .locals 0

    .prologue
    .line 682
    check-cast p1, Lcom/google/android/gms/cast/c;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$10;->onResult(Lcom/google/android/gms/cast/c;)V

    return-void
.end method
