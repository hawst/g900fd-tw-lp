.class public Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;
.super Lcom/google/android/apps/chrome/firstrun/FirstRunViewContainer;
.source "AccountFirstRunView.java"

# interfaces
.implements Lcom/google/android/apps/chrome/firstrun/ImageCarousel$ImageCarouselPositionChangeListener;
.implements Lorg/chromium/chrome/browser/profiles/ProfileDownloader$Observer;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final NOTIFICATIONS:[I


# instance fields
.field private mAccountManagerHelper:Lorg/chromium/sync/signin/AccountManagerHelper;

.field private mAccountName:Ljava/lang/String;

.field private mAccountNames:Ljava/util/List;

.field private mAddAnotherAccount:Ljava/lang/String;

.field private mArrayAdapter:Landroid/widget/ArrayAdapter;

.field private final mChromeNotificationsHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

.field private mDescriptionText:Landroid/widget/TextView;

.field private mDescriptionTextId:I

.field private mForcedAccountName:Ljava/lang/String;

.field private mImageCarousel:Lcom/google/android/apps/chrome/firstrun/ImageCarousel;

.field private mListener:Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView$Listener;

.field private mNegativeButton:Landroid/widget/Button;

.field private mPositionSetProgrammatically:Z

.field private mPositiveButton:Landroid/widget/Button;

.field private mProfileData:Lcom/google/android/apps/chrome/firstrun/ProfileDataCache;

.field private mSignedIn:Z

.field private mSpinner:Landroid/widget/Spinner;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 41
    const-class v0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->$assertionsDisabled:Z

    .line 106
    new-array v0, v1, [I

    const/16 v1, 0x32

    aput v1, v0, v2

    sput-object v0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->NOTIFICATIONS:[I

    return-void

    :cond_0
    move v0, v2

    .line 41
    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 148
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/firstrun/FirstRunViewContainer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 110
    new-instance v0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView$1;-><init>(Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mChromeNotificationsHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    .line 149
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mAddAnotherAccount:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mAccountName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$102(Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 41
    iput-object p1, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mAccountName:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;)Landroid/widget/ArrayAdapter;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mArrayAdapter:Landroid/widget/ArrayAdapter;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;)Landroid/widget/Spinner;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mSpinner:Landroid/widget/Spinner;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;)Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView$Listener;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mListener:Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView$Listener;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;)Z
    .locals 1

    .prologue
    .line 41
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mPositionSetProgrammatically:Z

    return v0
.end method

.method static synthetic access$502(Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;Z)Z
    .locals 0

    .prologue
    .line 41
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mPositionSetProgrammatically:Z

    return p1
.end method

.method static synthetic access$600(Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;)Lcom/google/android/apps/chrome/firstrun/ImageCarousel;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mImageCarousel:Lcom/google/android/apps/chrome/firstrun/ImageCarousel;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->updateAccounts()V

    return-void
.end method

.method private static getIndexOfNewElement(Ljava/util/List;Ljava/util/List;I)I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 391
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 398
    :cond_0
    :goto_0
    return v1

    .line 392
    :cond_1
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    if-ne v0, v2, :cond_2

    invoke-interface {p0, p1}, Ljava/util/List;->containsAll(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v1, p2

    goto :goto_0

    .line 393
    :cond_2
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    if-ne v0, v2, :cond_0

    move v0, v1

    .line 394
    :goto_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 395
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {p0, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    move v1, v0

    goto :goto_0

    .line 394
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method private registerForUpdates()V
    .locals 2

    .prologue
    .line 407
    sget-object v0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->NOTIFICATIONS:[I

    iget-object v1, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mChromeNotificationsHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->registerForNotifications([ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    .line 409
    return-void
.end method

.method private unregisterForUpdates()V
    .locals 2

    .prologue
    .line 402
    sget-object v0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->NOTIFICATIONS:[I

    iget-object v1, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mChromeNotificationsHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->unregisterForNotifications([ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    .line 404
    return-void
.end method

.method private updateAccounts()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 326
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mSignedIn:Z

    if-eqz v0, :cond_0

    .line 377
    :goto_0
    return-void

    .line 327
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->setButtonsEnabled(Z)V

    .line 329
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/sync/signin/AccountManagerHelper;->get(Landroid/content/Context;)Lorg/chromium/sync/signin/AccountManagerHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mAccountManagerHelper:Lorg/chromium/sync/signin/AccountManagerHelper;

    .line 331
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mAccountNames:Ljava/util/List;

    .line 332
    iget-object v1, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mAccountManagerHelper:Lorg/chromium/sync/signin/AccountManagerHelper;

    invoke-virtual {v1}, Lorg/chromium/sync/signin/AccountManagerHelper;->getGoogleAccountNames()Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mAccountNames:Ljava/util/List;

    .line 333
    iget-object v1, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mForcedAccountName:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 335
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mAccountNames:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mForcedAccountName:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 336
    if-gez v0, :cond_4

    .line 337
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mListener:Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView$Listener;

    iget-object v1, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mForcedAccountName:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView$Listener;->onFailedToSetForcedAccount(Ljava/lang/String;)V

    goto :goto_0

    .line 341
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mAccountNames:Ljava/util/List;

    iget-object v2, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mSpinner:Landroid/widget/Spinner;

    invoke-virtual {v2}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v2

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->getIndexOfNewElement(Ljava/util/List;Ljava/util/List;I)I

    move-result v0

    move v1, v0

    .line 345
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mArrayAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->clear()V

    .line 346
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mAccountNames:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 347
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mSpinner:Landroid/widget/Spinner;

    invoke-virtual {v0, v3}, Landroid/widget/Spinner;->setVisibility(I)V

    .line 348
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mArrayAdapter:Landroid/widget/ArrayAdapter;

    iget-object v2, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mAccountNames:Ljava/util/List;

    invoke-virtual {v0, v2}, Landroid/widget/ArrayAdapter;->addAll(Ljava/util/Collection;)V

    .line 349
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mArrayAdapter:Landroid/widget/ArrayAdapter;

    iget-object v2, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mAddAnotherAccount:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 350
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mPositiveButton:Landroid/widget/Button;

    sget v2, Lcom/google/android/apps/chrome/R$string;->choose_account_sign_in:I

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(I)V

    .line 351
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mPositiveButton:Landroid/widget/Button;

    new-instance v2, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView$3;

    invoke-direct {v2, p0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView$3;-><init>(Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 357
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mDescriptionText:Landroid/widget/TextView;

    iget v2, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mDescriptionTextId:I

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 371
    :goto_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mProfileData:Lcom/google/android/apps/chrome/firstrun/ProfileDataCache;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mProfileData:Lcom/google/android/apps/chrome/firstrun/ProfileDataCache;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/firstrun/ProfileDataCache;->update()V

    .line 372
    :cond_2
    invoke-direct {p0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->updateProfileImages()V

    .line 374
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mSpinner:Landroid/widget/Spinner;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 375
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mArrayAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mAccountName:Ljava/lang/String;

    .line 376
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mImageCarousel:Lcom/google/android/apps/chrome/firstrun/ImageCarousel;

    int-to-float v1, v1

    invoke-virtual {v0, v1, v3, v3}, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->scrollTo(FZZ)V

    goto/16 :goto_0

    .line 359
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mSpinner:Landroid/widget/Spinner;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/Spinner;->setVisibility(I)V

    .line 360
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mArrayAdapter:Landroid/widget/ArrayAdapter;

    iget-object v2, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mAddAnotherAccount:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 361
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mPositiveButton:Landroid/widget/Button;

    sget v2, Lcom/google/android/apps/chrome/R$string;->fre_no_accounts:I

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(I)V

    .line 362
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mPositiveButton:Landroid/widget/Button;

    new-instance v2, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView$4;

    invoke-direct {v2, p0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView$4;-><init>(Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 368
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mDescriptionText:Landroid/widget/TextView;

    sget v2, Lcom/google/android/apps/chrome/R$string;->fre_no_account_choice_description:I

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    goto :goto_2

    :cond_4
    move v1, v0

    goto/16 :goto_1
.end method

.method private updateProfileImages()V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 417
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mProfileData:Lcom/google/android/apps/chrome/firstrun/ProfileDataCache;

    if-nez v0, :cond_0

    .line 434
    :goto_0
    return-void

    .line 419
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mAccountNames:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    .line 422
    if-nez v3, :cond_1

    .line 423
    const/4 v0, 0x1

    new-array v0, v0, [Landroid/graphics/Bitmap;

    .line 424
    iget-object v1, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mProfileData:Lcom/google/android/apps/chrome/firstrun/ProfileDataCache;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Lcom/google/android/apps/chrome/firstrun/ProfileDataCache;->getImage(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v1

    aput-object v1, v0, v2

    .line 432
    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mImageCarousel:Lcom/google/android/apps/chrome/firstrun/ImageCarousel;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->setImages([Landroid/graphics/Bitmap;)V

    .line 433
    invoke-direct {p0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->updateProfileName()V

    goto :goto_0

    .line 426
    :cond_1
    new-array v1, v3, [Landroid/graphics/Bitmap;

    .line 427
    :goto_2
    if-ge v2, v3, :cond_2

    .line 428
    iget-object v4, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mProfileData:Lcom/google/android/apps/chrome/firstrun/ProfileDataCache;

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mAccountNames:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v4, v0}, Lcom/google/android/apps/chrome/firstrun/ProfileDataCache;->getImage(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    aput-object v0, v1, v2

    .line 427
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    :cond_2
    move-object v0, v1

    goto :goto_1
.end method

.method private updateProfileName()V
    .locals 4

    .prologue
    .line 437
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mSignedIn:Z

    if-eqz v0, :cond_1

    .line 438
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mProfileData:Lcom/google/android/apps/chrome/firstrun/ProfileDataCache;

    iget-object v1, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mAccountName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/firstrun/ProfileDataCache;->getFullName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 439
    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mAccountName:Ljava/lang/String;

    .line 440
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/chrome/R$string;->fre_hi_name:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 441
    sget v0, Lcom/google/android/apps/chrome/R$id;->title:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 443
    :cond_1
    return-void
.end method


# virtual methods
.method public configureForAddAccountPromo()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 265
    invoke-static {}, Lorg/chromium/chrome/browser/signin/SigninManager;->getAndroidSigninPromoExperimentGroup()I

    move-result v1

    .line 266
    sget-boolean v0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    if-ltz v1, :cond_0

    const/4 v0, 0x7

    if-le v1, v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 268
    :cond_1
    sget v0, Lcom/google/android/apps/chrome/R$id;->title:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 269
    and-int/lit8 v2, v1, 0x1

    if-eqz v2, :cond_2

    .line 270
    sget v2, Lcom/google/android/apps/chrome/R$string;->make_chrome_yours:I

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 273
    :cond_2
    and-int/lit8 v0, v1, 0x2

    if-eqz v0, :cond_4

    sget v0, Lcom/google/android/apps/chrome/R$string;->sign_in_to_chrome_summary_variant:I

    :goto_0
    iput v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mDescriptionTextId:I

    .line 276
    and-int/lit8 v0, v1, 0x4

    if-eqz v0, :cond_3

    .line 277
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mImageCarousel:Lcom/google/android/apps/chrome/firstrun/ImageCarousel;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->setVisibility(I)V

    .line 279
    new-instance v1, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 280
    sget v0, Lcom/google/android/apps/chrome/R$drawable;->signin_promo_illustration:I

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 281
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Lcom/google/android/apps/chrome/R$dimen;->android_signin_promo_illustration_padding_bottom:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    invoke-virtual {v1, v3, v3, v3, v0}, Landroid/widget/ImageView;->setPadding(IIII)V

    .line 284
    sget v0, Lcom/google/android/apps/chrome/R$id;->fre_account_linear_layout:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 285
    invoke-virtual {v0, v1, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    .line 287
    :cond_3
    return-void

    .line 273
    :cond_4
    sget v0, Lcom/google/android/apps/chrome/R$string;->sign_in_to_chrome_summary:I

    goto :goto_0
.end method

.method public configureForRecentTabsPage()V
    .locals 4

    .prologue
    const/4 v3, -0x2

    const/4 v2, 0x0

    .line 228
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->disableHorizontalMode()V

    .line 230
    sget v0, Lcom/google/android/apps/chrome/R$color;->ntp_bg:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->setBackgroundResource(I)V

    .line 231
    sget v0, Lcom/google/android/apps/chrome/R$id;->title:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 232
    sget v1, Lcom/google/android/apps/chrome/R$string;->sign_in_to_chrome:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 236
    sget v0, Lcom/google/android/apps/chrome/R$id;->button_bar_separator:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 237
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 239
    sget v0, Lcom/google/android/apps/chrome/R$id;->button_bar:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 240
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setGravity(I)V

    .line 241
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mPositiveButton:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 243
    iput v3, v0, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 244
    iput v3, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 245
    const/4 v1, 0x0

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 246
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$dimen;->sign_in_promo_padding_bottom:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    invoke-virtual {p0, v2, v2, v2, v0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->setPadding(IIII)V

    .line 249
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$dimen;->sign_in_promo_button_padding_vertical:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    .line 251
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/chrome/R$dimen;->sign_in_promo_button_padding_horizontal:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    .line 253
    iget-object v2, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mPositiveButton:Landroid/widget/Button;

    invoke-static {v2, v1, v0, v1, v0}, Lorg/chromium/base/ApiCompatibilityUtils;->setPaddingRelative(Landroid/view/View;IIII)V

    .line 256
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mPositiveButton:Landroid/widget/Button;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(I)V

    .line 257
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mPositiveButton:Landroid/widget/Button;

    sget v1, Lcom/google/android/apps/chrome/R$drawable;->btn_infobar_blue:I

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 258
    return-void
.end method

.method public init(Lcom/google/android/apps/chrome/firstrun/ProfileDataCache;)V
    .locals 0

    .prologue
    .line 156
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->setProfileDataCache(Lcom/google/android/apps/chrome/firstrun/ProfileDataCache;)V

    .line 157
    return-void
.end method

.method public isInForcedAccountMode()Z
    .locals 1

    .prologue
    .line 498
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mForcedAccountName:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSignedIn()Z
    .locals 1

    .prologue
    .line 491
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mSignedIn:Z

    return v0
.end method

.method protected onAttachedToWindow()V
    .locals 0

    .prologue
    .line 204
    invoke-super {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunViewContainer;->onAttachedToWindow()V

    .line 205
    invoke-direct {p0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->registerForUpdates()V

    .line 206
    invoke-direct {p0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->updateAccounts()V

    .line 207
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    .prologue
    .line 211
    invoke-super {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunViewContainer;->onDetachedFromWindow()V

    .line 212
    invoke-direct {p0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->unregisterForUpdates()V

    .line 213
    return-void
.end method

.method protected onFinishInflate()V
    .locals 3

    .prologue
    .line 171
    invoke-super {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunViewContainer;->onFinishInflate()V

    .line 173
    sget v0, Lcom/google/android/apps/chrome/R$id;->image_slider:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;

    iput-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mImageCarousel:Lcom/google/android/apps/chrome/firstrun/ImageCarousel;

    .line 174
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mImageCarousel:Lcom/google/android/apps/chrome/firstrun/ImageCarousel;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->setListener(Lcom/google/android/apps/chrome/firstrun/ImageCarousel$ImageCarouselPositionChangeListener;)V

    .line 176
    sget v0, Lcom/google/android/apps/chrome/R$id;->positive_button:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mPositiveButton:Landroid/widget/Button;

    .line 177
    sget v0, Lcom/google/android/apps/chrome/R$id;->negative_button:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mNegativeButton:Landroid/widget/Button;

    .line 178
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mNegativeButton:Landroid/widget/Button;

    new-instance v1, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView$2;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView$2;-><init>(Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 186
    sget v0, Lcom/google/android/apps/chrome/R$id;->description:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mDescriptionText:Landroid/widget/TextView;

    .line 187
    sget v0, Lcom/google/android/apps/chrome/R$string;->fre_account_choice_description:I

    iput v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mDescriptionTextId:I

    .line 189
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$string;->fre_add_account:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mAddAnotherAccount:Ljava/lang/String;

    .line 191
    sget v0, Lcom/google/android/apps/chrome/R$id;->google_accounts_spinner:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mSpinner:Landroid/widget/Spinner;

    .line 192
    new-instance v0, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/google/android/apps/chrome/R$layout;->fre_spinner_text:I

    invoke-direct {v0, v1, v2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mArrayAdapter:Landroid/widget/ArrayAdapter;

    .line 195
    invoke-direct {p0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->updateAccounts()V

    .line 197
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mArrayAdapter:Landroid/widget/ArrayAdapter;

    sget v1, Lcom/google/android/apps/chrome/R$layout;->fre_spinner_dropdown:I

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 198
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mSpinner:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mArrayAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 199
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mSpinner:Landroid/widget/Spinner;

    new-instance v1, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView$SpinnerOnItemSelectedListener;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView$SpinnerOnItemSelectedListener;-><init>(Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView$1;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 200
    return-void
.end method

.method public onPositionChanged(I)V
    .locals 1

    .prologue
    .line 503
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mPositionSetProgrammatically:Z

    .line 504
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mSpinner:Landroid/widget/Spinner;

    invoke-virtual {v0, p1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 505
    return-void
.end method

.method public onProfileDownloaded(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 0

    .prologue
    .line 413
    invoke-direct {p0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->updateProfileImages()V

    .line 414
    return-void
.end method

.method public onWindowVisibilityChanged(I)V
    .locals 0

    .prologue
    .line 217
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/firstrun/FirstRunViewContainer;->onWindowVisibilityChanged(I)V

    .line 218
    if-nez p1, :cond_0

    .line 219
    invoke-direct {p0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->updateAccounts()V

    .line 221
    :cond_0
    return-void
.end method

.method public setButtonsEnabled(Z)V
    .locals 1

    .prologue
    .line 295
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mPositiveButton:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 296
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mNegativeButton:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 297
    return-void
.end method

.method public setCanCancel(Z)V
    .locals 2

    .prologue
    .line 317
    iget-object v1, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mNegativeButton:Landroid/widget/Button;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setVisibility(I)V

    .line 318
    iget-object v1, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mPositiveButton:Landroid/widget/Button;

    if-eqz p1, :cond_1

    const v0, 0x800015

    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setGravity(I)V

    .line 320
    return-void

    .line 317
    :cond_0
    const/16 v0, 0x8

    goto :goto_0

    .line 318
    :cond_1
    const/16 v0, 0x11

    goto :goto_1
.end method

.method public setListener(Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView$Listener;)V
    .locals 0

    .prologue
    .line 305
    iput-object p1, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mListener:Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView$Listener;

    .line 306
    return-void
.end method

.method public setProfileDataCache(Lcom/google/android/apps/chrome/firstrun/ProfileDataCache;)V
    .locals 1

    .prologue
    .line 164
    iput-object p1, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mProfileData:Lcom/google/android/apps/chrome/firstrun/ProfileDataCache;

    .line 165
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mProfileData:Lcom/google/android/apps/chrome/firstrun/ProfileDataCache;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/chrome/firstrun/ProfileDataCache;->setObserver(Lorg/chromium/chrome/browser/profiles/ProfileDownloader$Observer;)V

    .line 166
    invoke-direct {p0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->updateProfileImages()V

    .line 167
    return-void
.end method

.method public switchToForcedAccountMode(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 480
    iput-object p1, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mForcedAccountName:Ljava/lang/String;

    .line 481
    invoke-direct {p0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->updateAccounts()V

    .line 482
    sget-boolean v0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mAccountName:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mForcedAccountName:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 483
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->switchToSignedMode()V

    .line 484
    sget-boolean v0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mAccountName:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mForcedAccountName:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 485
    :cond_1
    return-void
.end method

.method public switchToSignedMode()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 449
    iput-boolean v4, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mSignedIn:Z

    .line 450
    invoke-direct {p0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->updateProfileName()V

    .line 452
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mSpinner:Landroid/widget/Spinner;

    invoke-virtual {v0, v3}, Landroid/widget/Spinner;->setEnabled(Z)V

    .line 453
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mSpinner:Landroid/widget/Spinner;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lorg/chromium/base/ApiCompatibilityUtils;->setBackgroundForView(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 454
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mPositiveButton:Landroid/widget/Button;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/chrome/R$string;->fre_done:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 455
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mPositiveButton:Landroid/widget/Button;

    new-instance v1, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView$5;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView$5;-><init>(Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 461
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mNegativeButton:Landroid/widget/Button;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/chrome/R$string;->fre_settings:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 462
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mNegativeButton:Landroid/widget/Button;

    new-instance v1, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView$6;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView$6;-><init>(Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 468
    invoke-virtual {p0, v4}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->setButtonsEnabled(Z)V

    .line 469
    sget v0, Lcom/google/android/apps/chrome/R$id;->description:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/chrome/R$string;->fre_signed_in_description:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 471
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mImageCarousel:Lcom/google/android/apps/chrome/firstrun/ImageCarousel;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->setVisibility(I)V

    .line 472
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->mImageCarousel:Lcom/google/android/apps/chrome/firstrun/ImageCarousel;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/firstrun/ImageCarousel;->setSignedInMode()V

    .line 473
    return-void
.end method
