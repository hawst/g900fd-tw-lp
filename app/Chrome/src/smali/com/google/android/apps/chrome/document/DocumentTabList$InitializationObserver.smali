.class public abstract Lcom/google/android/apps/chrome/document/DocumentTabList$InitializationObserver;
.super Ljava/lang/Object;
.source "DocumentTabList.java"


# instance fields
.field private final mTabList:Lcom/google/android/apps/chrome/document/DocumentTabList;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/chrome/document/DocumentTabList;)V
    .locals 0

    .prologue
    .line 172
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 173
    iput-object p1, p0, Lcom/google/android/apps/chrome/document/DocumentTabList$InitializationObserver;->mTabList:Lcom/google/android/apps/chrome/document/DocumentTabList;

    .line 174
    return-void
.end method


# virtual methods
.method public abstract isCanceled()Z
.end method

.method public abstract isSatisfied()Z
.end method

.method protected abstract runImmediately()V
.end method

.method public final runWhenReady()V
    .locals 1

    .prologue
    .line 187
    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    .line 188
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentTabList$InitializationObserver;->isSatisfied()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 189
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentTabList$InitializationObserver;->runImmediately()V

    .line 193
    :goto_0
    return-void

    .line 191
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentTabList$InitializationObserver;->mTabList:Lcom/google/android/apps/chrome/document/DocumentTabList;

    # getter for: Lcom/google/android/apps/chrome/document/DocumentTabList;->mObservers:Lorg/chromium/base/ObserverList;
    invoke-static {v0}, Lcom/google/android/apps/chrome/document/DocumentTabList;->access$000(Lcom/google/android/apps/chrome/document/DocumentTabList;)Lorg/chromium/base/ObserverList;

    move-result-object v0

    invoke-virtual {v0, p0}, Lorg/chromium/base/ObserverList;->addObserver(Ljava/lang/Object;)Z

    goto :goto_0
.end method
