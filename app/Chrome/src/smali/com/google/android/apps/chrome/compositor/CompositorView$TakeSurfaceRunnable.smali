.class Lcom/google/android/apps/chrome/compositor/CompositorView$TakeSurfaceRunnable;
.super Ljava/lang/Object;
.source "CompositorView.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private final mFormat:I

.field private final mHeight:I

.field private final mHolder:Landroid/view/SurfaceHolder;

.field private final mWidth:I

.field final synthetic this$0:Lcom/google/android/apps/chrome/compositor/CompositorView;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/chrome/compositor/CompositorView;Landroid/view/SurfaceHolder;III)V
    .locals 0

    .prologue
    .line 107
    iput-object p1, p0, Lcom/google/android/apps/chrome/compositor/CompositorView$TakeSurfaceRunnable;->this$0:Lcom/google/android/apps/chrome/compositor/CompositorView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 108
    iput-object p2, p0, Lcom/google/android/apps/chrome/compositor/CompositorView$TakeSurfaceRunnable;->mHolder:Landroid/view/SurfaceHolder;

    .line 109
    iput p3, p0, Lcom/google/android/apps/chrome/compositor/CompositorView$TakeSurfaceRunnable;->mFormat:I

    .line 110
    iput p4, p0, Lcom/google/android/apps/chrome/compositor/CompositorView$TakeSurfaceRunnable;->mWidth:I

    .line 111
    iput p5, p0, Lcom/google/android/apps/chrome/compositor/CompositorView$TakeSurfaceRunnable;->mHeight:I

    .line 112
    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 116
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorView$TakeSurfaceRunnable;->this$0:Lcom/google/android/apps/chrome/compositor/CompositorView;

    iget-object v1, p0, Lcom/google/android/apps/chrome/compositor/CompositorView$TakeSurfaceRunnable;->this$0:Lcom/google/android/apps/chrome/compositor/CompositorView;

    # getter for: Lcom/google/android/apps/chrome/compositor/CompositorView;->mNativeCompositorView:J
    invoke-static {v1}, Lcom/google/android/apps/chrome/compositor/CompositorView;->access$000(Lcom/google/android/apps/chrome/compositor/CompositorView;)J

    move-result-wide v2

    # invokes: Lcom/google/android/apps/chrome/compositor/CompositorView;->nativeSurfaceCreated(J)V
    invoke-static {v0, v2, v3}, Lcom/google/android/apps/chrome/compositor/CompositorView;->access$100(Lcom/google/android/apps/chrome/compositor/CompositorView;J)V

    .line 117
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorView$TakeSurfaceRunnable;->this$0:Lcom/google/android/apps/chrome/compositor/CompositorView;

    # getter for: Lcom/google/android/apps/chrome/compositor/CompositorView;->mRenderHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;
    invoke-static {v0}, Lcom/google/android/apps/chrome/compositor/CompositorView;->access$200(Lcom/google/android/apps/chrome/compositor/CompositorView;)Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;->onSurfaceCreated()V

    .line 118
    iget-object v1, p0, Lcom/google/android/apps/chrome/compositor/CompositorView$TakeSurfaceRunnable;->this$0:Lcom/google/android/apps/chrome/compositor/CompositorView;

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorView$TakeSurfaceRunnable;->this$0:Lcom/google/android/apps/chrome/compositor/CompositorView;

    # getter for: Lcom/google/android/apps/chrome/compositor/CompositorView;->mNativeCompositorView:J
    invoke-static {v0}, Lcom/google/android/apps/chrome/compositor/CompositorView;->access$000(Lcom/google/android/apps/chrome/compositor/CompositorView;)J

    move-result-wide v2

    iget v4, p0, Lcom/google/android/apps/chrome/compositor/CompositorView$TakeSurfaceRunnable;->mFormat:I

    iget v5, p0, Lcom/google/android/apps/chrome/compositor/CompositorView$TakeSurfaceRunnable;->mWidth:I

    iget v6, p0, Lcom/google/android/apps/chrome/compositor/CompositorView$TakeSurfaceRunnable;->mHeight:I

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorView$TakeSurfaceRunnable;->mHolder:Landroid/view/SurfaceHolder;

    invoke-interface {v0}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    move-result-object v7

    # invokes: Lcom/google/android/apps/chrome/compositor/CompositorView;->nativeSurfaceChanged(JIIILandroid/view/Surface;)V
    invoke-static/range {v1 .. v7}, Lcom/google/android/apps/chrome/compositor/CompositorView;->access$300(Lcom/google/android/apps/chrome/compositor/CompositorView;JIIILandroid/view/Surface;)V

    .line 120
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorView$TakeSurfaceRunnable;->this$0:Lcom/google/android/apps/chrome/compositor/CompositorView;

    # getter for: Lcom/google/android/apps/chrome/compositor/CompositorView;->mRenderHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;
    invoke-static {v0}, Lcom/google/android/apps/chrome/compositor/CompositorView;->access$200(Lcom/google/android/apps/chrome/compositor/CompositorView;)Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/chrome/compositor/CompositorView$TakeSurfaceRunnable;->mWidth:I

    iget v2, p0, Lcom/google/android/apps/chrome/compositor/CompositorView$TakeSurfaceRunnable;->mHeight:I

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;->onPhysicalBackingSizeChanged(II)V

    .line 121
    return-void
.end method
