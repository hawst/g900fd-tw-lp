.class Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$LocationBarContainer;
.super Landroid/widget/RelativeLayout;
.source "ToolbarTablet.java"


# instance fields
.field private mTouchEventForwarder:Lcom/google/android/apps/chrome/utilities/TouchEventForwarder;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 451
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 452
    return-void
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 5

    .prologue
    .line 456
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 457
    sget v0, Lcom/google/android/apps/chrome/R$id;->url_bar:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$LocationBarContainer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/omnibox/UrlBar;

    .line 458
    sget v1, Lcom/google/android/apps/chrome/R$id;->delete_button:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$LocationBarContainer;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    .line 459
    new-instance v2, Lcom/google/android/apps/chrome/utilities/TouchEventForwarder;

    const/4 v3, 0x2

    new-array v3, v3, [Landroid/view/View;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    const/4 v0, 0x1

    aput-object v1, v3, v0

    invoke-direct {v2, p0, v3}, Lcom/google/android/apps/chrome/utilities/TouchEventForwarder;-><init>(Landroid/view/ViewGroup;[Landroid/view/View;)V

    iput-object v2, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$LocationBarContainer;->mTouchEventForwarder:Lcom/google/android/apps/chrome/utilities/TouchEventForwarder;

    .line 461
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 465
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$LocationBarContainer;->mTouchEventForwarder:Lcom/google/android/apps/chrome/utilities/TouchEventForwarder;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$LocationBarContainer;->mTouchEventForwarder:Lcom/google/android/apps/chrome/utilities/TouchEventForwarder;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/utilities/TouchEventForwarder;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method
