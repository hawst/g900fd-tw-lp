.class public Lcom/google/android/apps/chrome/omnibox/VoiceSuggestionProvider$VoiceResult;
.super Ljava/lang/Object;
.source "VoiceSuggestionProvider.java"


# instance fields
.field private final mConfidence:F

.field private final mMatch:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;F)V
    .locals 0

    .prologue
    .line 181
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 182
    iput-object p1, p0, Lcom/google/android/apps/chrome/omnibox/VoiceSuggestionProvider$VoiceResult;->mMatch:Ljava/lang/String;

    .line 183
    iput p2, p0, Lcom/google/android/apps/chrome/omnibox/VoiceSuggestionProvider$VoiceResult;->mConfidence:F

    .line 184
    return-void
.end method


# virtual methods
.method public getConfidence()F
    .locals 1

    .prologue
    .line 197
    iget v0, p0, Lcom/google/android/apps/chrome/omnibox/VoiceSuggestionProvider$VoiceResult;->mConfidence:F

    return v0
.end method

.method public getMatch()Ljava/lang/String;
    .locals 1

    .prologue
    .line 190
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/VoiceSuggestionProvider$VoiceResult;->mMatch:Ljava/lang/String;

    return-object v0
.end method
