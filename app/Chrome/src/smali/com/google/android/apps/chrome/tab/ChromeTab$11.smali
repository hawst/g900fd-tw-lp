.class Lcom/google/android/apps/chrome/tab/ChromeTab$11;
.super Lorg/chromium/chrome/browser/EmptyTabObserver;
.source "ChromeTab.java"


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/tab/ChromeTab;)V
    .locals 0

    .prologue
    .line 3154
    iput-object p1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$11;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-direct {p0}, Lorg/chromium/chrome/browser/EmptyTabObserver;-><init>()V

    return-void
.end method


# virtual methods
.method public onContentChanged(Lorg/chromium/chrome/browser/Tab;)V
    .locals 2

    .prologue
    .line 3157
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$11;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/apps/chrome/tab/ChromeTab;->mLoadProgressAtViewSwapInTime:I
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$1302(Lcom/google/android/apps/chrome/tab/ChromeTab;I)I

    .line 3158
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$11;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # invokes: Lcom/google/android/apps/chrome/tab/ChromeTab;->notifyTabContentViewChanged()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$6600(Lcom/google/android/apps/chrome/tab/ChromeTab;)V

    .line 3159
    return-void
.end method

.method public onFaviconUpdated(Lorg/chromium/chrome/browser/Tab;)V
    .locals 3

    .prologue
    .line 3163
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 3164
    const-string/jumbo v1, "tabId"

    iget-object v2, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$11;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getId()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 3165
    iget-object v1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$11;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # getter for: Lcom/google/android/apps/chrome/tab/ChromeTab;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;
    invoke-static {v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$900(Lcom/google/android/apps/chrome/tab/ChromeTab;)Lcom/google/android/apps/chrome/ChromeActivity;

    move-result-object v1

    const/16 v2, 0x14

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastNotification(Landroid/content/Context;ILandroid/os/Bundle;)V

    .line 3166
    return-void
.end method

.method public onSSLStateUpdated(Lorg/chromium/chrome/browser/Tab;)V
    .locals 4

    .prologue
    .line 3186
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$11;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    iget-object v1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$11;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # getter for: Lcom/google/android/apps/chrome/tab/ChromeTab;->mNativeChromeTab:J
    invoke-static {v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$5700(Lcom/google/android/apps/chrome/tab/ChromeTab;)J

    move-result-wide v2

    # invokes: Lcom/google/android/apps/chrome/tab/ChromeTab;->nativeGetKnoxCertificateFailure(J)I
    invoke-static {v0, v2, v3}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$5800(Lcom/google/android/apps/chrome/tab/ChromeTab;J)I

    move-result v0

    .line 3188
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 3189
    iget-object v1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$11;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # invokes: Lcom/google/android/apps/chrome/tab/ChromeTab;->getApplicationContext()Landroid/content/Context;
    invoke-static {v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$6900(Lcom/google/android/apps/chrome/tab/ChromeTab;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger;->logCertificateFailure(Landroid/content/Context;I)V

    .line 3192
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$11;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->updateFullscreenEnabledState()V

    .line 3193
    return-void
.end method

.method public onTitleUpdated(Lorg/chromium/chrome/browser/Tab;)V
    .locals 3

    .prologue
    .line 3170
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$11;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # invokes: Lcom/google/android/apps/chrome/tab/ChromeTab;->updateTitle()Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$6700(Lcom/google/android/apps/chrome/tab/ChromeTab;)Z

    .line 3171
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 3172
    const-string/jumbo v1, "tabId"

    iget-object v2, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$11;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getId()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 3173
    const-string/jumbo v1, "title"

    iget-object v2, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$11;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # getter for: Lcom/google/android/apps/chrome/tab/ChromeTab;->mTitle:Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$6800(Lcom/google/android/apps/chrome/tab/ChromeTab;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 3174
    iget-object v1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$11;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # getter for: Lcom/google/android/apps/chrome/tab/ChromeTab;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;
    invoke-static {v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$900(Lcom/google/android/apps/chrome/tab/ChromeTab;)Lcom/google/android/apps/chrome/ChromeActivity;

    move-result-object v1

    const/16 v2, 0xa

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastNotification(Landroid/content/Context;ILandroid/os/Bundle;)V

    .line 3175
    return-void
.end method

.method public onToggleFullscreenMode(Lorg/chromium/chrome/browser/Tab;Z)V
    .locals 1

    .prologue
    .line 3197
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$11;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # getter for: Lcom/google/android/apps/chrome/tab/ChromeTab;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$2400(Lcom/google/android/apps/chrome/tab/ChromeTab;)Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    move-result-object v0

    if-nez v0, :cond_0

    .line 3199
    :goto_0
    return-void

    .line 3198
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$11;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # getter for: Lcom/google/android/apps/chrome/tab/ChromeTab;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$2400(Lcom/google/android/apps/chrome/tab/ChromeTab;)Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->setPersistentFullscreenMode(Z)V

    goto :goto_0
.end method

.method public onUrlUpdated(Lorg/chromium/chrome/browser/Tab;)V
    .locals 3

    .prologue
    .line 3179
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 3180
    const-string/jumbo v1, "tabId"

    iget-object v2, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$11;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getId()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 3181
    iget-object v1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$11;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # getter for: Lcom/google/android/apps/chrome/tab/ChromeTab;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;
    invoke-static {v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$900(Lcom/google/android/apps/chrome/tab/ChromeTab;)Lcom/google/android/apps/chrome/ChromeActivity;

    move-result-object v1

    const/16 v2, 0x19

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastNotification(Landroid/content/Context;ILandroid/os/Bundle;)V

    .line 3182
    return-void
.end method

.method public onWebContentsSwapped(Lorg/chromium/chrome/browser/Tab;ZZ)V
    .locals 3

    .prologue
    .line 3203
    if-nez p2, :cond_1

    .line 3219
    :cond_0
    :goto_0
    return-void

    .line 3205
    :cond_1
    invoke-virtual {p1}, Lorg/chromium/chrome/browser/Tab;->getUrl()Ljava/lang/String;

    move-result-object v0

    .line 3207
    iget-object v1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$11;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    const/4 v2, 0x0

    # invokes: Lcom/google/android/apps/chrome/tab/ChromeTab;->didStartPageLoad(Ljava/lang/String;Z)V
    invoke-static {v1, v0, v2}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$5200(Lcom/google/android/apps/chrome/tab/ChromeTab;Ljava/lang/String;Z)V

    .line 3208
    if-eqz p3, :cond_2

    .line 3210
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$11;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # invokes: Lcom/google/android/apps/chrome/tab/ChromeTab;->didFinishPageLoad()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$4500(Lcom/google/android/apps/chrome/tab/ChromeTab;)V

    .line 3212
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$11;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # getter for: Lcom/google/android/apps/chrome/tab/ChromeTab;->mBackgroundContentViewHelper:Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$1400(Lcom/google/android/apps/chrome/tab/ChromeTab;)Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->isPageSwappingInProgress()Z

    move-result v0

    if-nez v0, :cond_0

    .line 3213
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 3214
    const-string/jumbo v1, "tabId"

    iget-object v2, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$11;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getId()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 3215
    const-string/jumbo v1, "fullyPrerendered"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 3216
    iget-object v1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$11;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # getter for: Lcom/google/android/apps/chrome/tab/ChromeTab;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;
    invoke-static {v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$900(Lcom/google/android/apps/chrome/tab/ChromeTab;)Lcom/google/android/apps/chrome/ChromeActivity;

    move-result-object v1

    const/16 v2, 0x23

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastNotification(Landroid/content/Context;ILandroid/os/Bundle;)V

    goto :goto_0
.end method
