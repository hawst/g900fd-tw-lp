.class Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$10$1;
.super Ljava/lang/Object;
.source "YouTubeMediaRouteController.java"

# interfaces
.implements Lcom/google/android/gms/common/api/o;


# instance fields
.field final synthetic this$1:Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$10;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$10;)V
    .locals 0

    .prologue
    .line 713
    iput-object p1, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$10$1;->this$1:Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$10;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onResult(Lcom/google/android/gms/common/api/Status;)V
    .locals 3

    .prologue
    .line 716
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$10$1;->this$1:Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$10;

    iget-object v0, v0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$10;->this$0:Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;

    # getter for: Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mDebug:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->access$1100(Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "YouTubeMediaRouteController"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "fling video message onResult(): "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 717
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/common/api/Status;->e()Z

    move-result v0

    if-nez v0, :cond_1

    .line 718
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$10$1;->this$1:Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$10;

    iget-object v0, v0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$10;->this$0:Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->release()V

    .line 719
    invoke-static {}, Lorg/chromium/base/library_loader/LibraryLoader;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 720
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->castYouTubePlayerResult(Z)V

    .line 723
    :cond_1
    invoke-static {}, Lorg/chromium/base/library_loader/LibraryLoader;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 724
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->castYouTubePlayerResult(Z)V

    .line 726
    :cond_2
    return-void
.end method

.method public bridge synthetic onResult(Lcom/google/android/gms/common/api/n;)V
    .locals 0

    .prologue
    .line 713
    check-cast p1, Lcom/google/android/gms/common/api/Status;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$10$1;->onResult(Lcom/google/android/gms/common/api/Status;)V

    return-void
.end method
