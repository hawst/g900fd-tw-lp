.class public Lcom/google/android/apps/chrome/preferences/ManagedSwitch;
.super Landroid/widget/Switch;
.source "ManagedSwitch.java"


# instance fields
.field private final mManagedByPolicy:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Z)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 24
    invoke-direct {p0, p1}, Landroid/widget/Switch;-><init>(Landroid/content/Context;)V

    .line 25
    iput-boolean p2, p0, Lcom/google/android/apps/chrome/preferences/ManagedSwitch;->mManagedByPolicy:Z

    .line 27
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/preferences/ManagedSwitch;->mManagedByPolicy:Z

    if-eqz v0, :cond_0

    .line 28
    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/preferences/ManagedSwitch;->setEnabled(Z)V

    .line 29
    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ManagedPreferencesUtils;->getManagedByEnterpriseIconId()I

    move-result v0

    invoke-static {p0, v0, v1, v1, v1}, Lorg/chromium/base/ApiCompatibilityUtils;->setCompoundDrawablesRelativeWithIntrinsicBounds(Landroid/widget/TextView;IIII)V

    .line 32
    :cond_0
    return-void
.end method


# virtual methods
.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 36
    iget-boolean v1, p0, Lcom/google/android/apps/chrome/preferences/ManagedSwitch;->mManagedByPolicy:Z

    if-eqz v1, :cond_1

    .line 37
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v1

    if-ne v1, v0, :cond_0

    .line 38
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/ManagedSwitch;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/chrome/preferences/ManagedPreferencesUtils;->showManagedByAdministratorToast(Landroid/content/Context;)V

    .line 42
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-super {p0, p1}, Landroid/widget/Switch;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method
