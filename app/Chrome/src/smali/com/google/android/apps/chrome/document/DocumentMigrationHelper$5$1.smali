.class Lcom/google/android/apps/chrome/document/DocumentMigrationHelper$5$1;
.super Ljava/lang/Object;
.source "DocumentMigrationHelper.java"

# interfaces
.implements Lcom/google/android/apps/chrome/compositor/TabContentManager$DecompressThumbnailCallback;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/document/DocumentMigrationHelper$5;

.field final synthetic val$favicon:Landroid/graphics/Bitmap;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/document/DocumentMigrationHelper$5;Landroid/graphics/Bitmap;)V
    .locals 0

    .prologue
    .line 363
    iput-object p1, p0, Lcom/google/android/apps/chrome/document/DocumentMigrationHelper$5$1;->this$0:Lcom/google/android/apps/chrome/document/DocumentMigrationHelper$5;

    iput-object p2, p0, Lcom/google/android/apps/chrome/document/DocumentMigrationHelper$5$1;->val$favicon:Landroid/graphics/Bitmap;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFinishGetBitmap(Landroid/graphics/Bitmap;)V
    .locals 7

    .prologue
    .line 366
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentMigrationHelper$5$1;->this$0:Lcom/google/android/apps/chrome/document/DocumentMigrationHelper$5;

    iget-object v0, v0, Lcom/google/android/apps/chrome/document/DocumentMigrationHelper$5;->val$url:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/tab/NativePageFactory;->isNativePageUrl(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentMigrationHelper$5$1;->this$0:Lcom/google/android/apps/chrome/document/DocumentMigrationHelper$5;

    iget-object v0, v0, Lcom/google/android/apps/chrome/document/DocumentMigrationHelper$5;->val$url:Ljava/lang/String;

    const-string/jumbo v1, "chrome://"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 368
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentMigrationHelper$5$1;->this$0:Lcom/google/android/apps/chrome/document/DocumentMigrationHelper$5;

    iget-object v0, v0, Lcom/google/android/apps/chrome/document/DocumentMigrationHelper$5;->val$activity:Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/android/apps/chrome/document/DocumentMigrationHelper$5$1;->this$0:Lcom/google/android/apps/chrome/document/DocumentMigrationHelper$5;

    iget v1, v1, Lcom/google/android/apps/chrome/document/DocumentMigrationHelper$5;->val$tabId:I

    iget-object v2, p0, Lcom/google/android/apps/chrome/document/DocumentMigrationHelper$5$1;->this$0:Lcom/google/android/apps/chrome/document/DocumentMigrationHelper$5;

    iget-object v2, v2, Lcom/google/android/apps/chrome/document/DocumentMigrationHelper$5;->val$tabList:Lcom/google/android/apps/chrome/document/DocumentMigrationHelper$MigrationTabList;

    iget-object v3, p0, Lcom/google/android/apps/chrome/document/DocumentMigrationHelper$5$1;->this$0:Lcom/google/android/apps/chrome/document/DocumentMigrationHelper$5;

    iget v3, v3, Lcom/google/android/apps/chrome/document/DocumentMigrationHelper$5;->val$tabId:I

    invoke-virtual {v2, v3}, Lcom/google/android/apps/chrome/document/DocumentMigrationHelper$MigrationTabList;->getTabStateForDocument(I)Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/chrome/document/DocumentMigrationHelper$5$1;->this$0:Lcom/google/android/apps/chrome/document/DocumentMigrationHelper$5;

    iget-object v3, v3, Lcom/google/android/apps/chrome/document/DocumentMigrationHelper$5;->val$url:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/chrome/document/DocumentMigrationHelper$5$1;->this$0:Lcom/google/android/apps/chrome/document/DocumentMigrationHelper$5;

    iget-object v4, v4, Lcom/google/android/apps/chrome/document/DocumentMigrationHelper$5;->val$title:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/apps/chrome/document/DocumentMigrationHelper$5$1;->val$favicon:Landroid/graphics/Bitmap;

    move-object v6, p1

    # invokes: Lcom/google/android/apps/chrome/document/DocumentMigrationHelper;->addAppTask(Landroid/app/Activity;ILcom/google/android/apps/chrome/tab/ChromeTab$TabState;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)V
    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/chrome/document/DocumentMigrationHelper;->access$100(Landroid/app/Activity;ILcom/google/android/apps/chrome/tab/ChromeTab$TabState;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)V

    .line 374
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentMigrationHelper$5$1;->this$0:Lcom/google/android/apps/chrome/document/DocumentMigrationHelper$5;

    iget-boolean v0, v0, Lcom/google/android/apps/chrome/document/DocumentMigrationHelper$5;->val$finalizeWhenDone:Z

    if-eqz v0, :cond_1

    .line 375
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentMigrationHelper$5$1;->this$0:Lcom/google/android/apps/chrome/document/DocumentMigrationHelper$5;

    iget-object v0, v0, Lcom/google/android/apps/chrome/document/DocumentMigrationHelper$5;->val$activity:Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/android/apps/chrome/document/DocumentMigrationHelper$5$1;->this$0:Lcom/google/android/apps/chrome/document/DocumentMigrationHelper$5;

    iget v1, v1, Lcom/google/android/apps/chrome/document/DocumentMigrationHelper$5;->val$finalizeMode:I

    # invokes: Lcom/google/android/apps/chrome/document/DocumentMigrationHelper;->finalizeMigration(Landroid/app/Activity;I)V
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/document/DocumentMigrationHelper;->access$200(Landroid/app/Activity;I)V

    .line 377
    :cond_1
    return-void
.end method
