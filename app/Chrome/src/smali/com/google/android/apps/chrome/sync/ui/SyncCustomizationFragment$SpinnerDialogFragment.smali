.class Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment$SpinnerDialogFragment;
.super Lcom/google/android/apps/chrome/preferences/ChromeBaseDialogFragment;
.source "SyncCustomizationFragment.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 624
    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBaseDialogFragment;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment$1;)V
    .locals 0

    .prologue
    .line 624
    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment$SpinnerDialogFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 1

    .prologue
    .line 634
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/preferences/ChromeBaseDialogFragment;->onCancel(Landroid/content/DialogInterface;)V

    .line 635
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment$SpinnerDialogFragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;

    # invokes: Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->closePage()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->access$600(Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;)V

    .line 636
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    .prologue
    .line 627
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment$SpinnerDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 628
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment$SpinnerDialogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/chrome/R$string;->sync_loading:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 629
    return-object v0
.end method
