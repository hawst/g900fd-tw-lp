.class public Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;
.super Lorg/chromium/chrome/browser/tabmodel/TabModelBase;
.source "TabModelImpl.java"


# static fields
.field public static final UNKNOWN_APP_ID:Ljava/lang/String; = "com.google.android.apps.chrome.unknown_app"


# instance fields
.field private final mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

.field private final mObserver:Lorg/chromium/chrome/browser/tabmodel/TabModelObserver;

.field private final mTabContentManager:Lcom/google/android/apps/chrome/compositor/TabContentManager;

.field private final mTabSaver:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;

.field private final mUma:Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorUma;


# direct methods
.method public constructor <init>(ZLcom/google/android/apps/chrome/ChromeActivity;Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorUma;Lorg/chromium/chrome/browser/tabmodel/TabModelOrderController;Lcom/google/android/apps/chrome/compositor/TabContentManager;Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;Lorg/chromium/chrome/browser/tabmodel/TabModelDelegate;)V
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0, p1, p4, p7}, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;-><init>(ZLorg/chromium/chrome/browser/tabmodel/TabModelOrderController;Lorg/chromium/chrome/browser/tabmodel/TabModelDelegate;)V

    .line 57
    new-instance v0, Lcom/google/android/apps/chrome/tabmodel/TabModelImpl$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/tabmodel/TabModelImpl$1;-><init>(Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;->mObserver:Lorg/chromium/chrome/browser/tabmodel/TabModelObserver;

    .line 50
    iput-object p2, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    .line 51
    iput-object p3, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;->mUma:Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorUma;

    .line 52
    iput-object p5, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;->mTabContentManager:Lcom/google/android/apps/chrome/compositor/TabContentManager;

    .line 53
    iput-object p6, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;->mTabSaver:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;

    .line 54
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;->mObserver:Lorg/chromium/chrome/browser/tabmodel/TabModelObserver;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;->addObserver(Lorg/chromium/chrome/browser/tabmodel/TabModelObserver;)V

    .line 55
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;)Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorUma;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;->mUma:Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorUma;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;IILorg/chromium/chrome/browser/tabmodel/TabModel$TabSelectionType;Z)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;->notifyTabSelected(IILorg/chromium/chrome/browser/tabmodel/TabModel$TabSelectionType;Z)V

    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;IIIZ)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;->notifyTabMoved(IIIZ)V

    return-void
.end method

.method static synthetic access$1100(Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;IZ)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;->notifyTabClosureUndone(IZ)V

    return-void
.end method

.method static synthetic access$1200(Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;IZ)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;->notifyTabClosureCommitted(IZ)V

    return-void
.end method

.method static synthetic access$1300(Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;ILjava/lang/String;Z)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;->notifyTabPendingClosure(ILjava/lang/String;Z)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;IZ)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;->blockingNotifyTabClosing(IZ)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;IZ)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;->notifyTabClosed(IZ)V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;)Lcom/google/android/apps/chrome/compositor/TabContentManager;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;->mTabContentManager:Lcom/google/android/apps/chrome/compositor/TabContentManager;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;)Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;->mTabSaver:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;)Lorg/chromium/chrome/browser/tabmodel/TabModelDelegate;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;->mModelDelegate:Lorg/chromium/chrome/browser/tabmodel/TabModelDelegate;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;IILjava/lang/String;Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;Z)V
    .locals 0

    .prologue
    .line 31
    invoke-direct/range {p0 .. p5}, Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;->notifyTabCreated(IILjava/lang/String;Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;Z)V

    return-void
.end method

.method static synthetic access$800(Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;)Lorg/chromium/chrome/browser/tabmodel/TabModelDelegate;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;->mModelDelegate:Lorg/chromium/chrome/browser/tabmodel/TabModelDelegate;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;IILjava/lang/String;Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;Z)V
    .locals 0

    .prologue
    .line 31
    invoke-direct/range {p0 .. p5}, Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;->notifyTabCreating(IILjava/lang/String;Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;Z)V

    return-void
.end method

.method private blockingNotifyTabClosing(IZ)V
    .locals 3

    .prologue
    .line 300
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 301
    const-string/jumbo v1, "tabId"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 302
    const-string/jumbo v1, "animate"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 303
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    const/16 v2, 0x24

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastImmediateNotification(Landroid/content/Context;ILandroid/os/Bundle;)V

    .line 304
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;->mUma:Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorUma;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorUma;->onTabClosing(I)V

    .line 305
    return-void
.end method

.method private notifyTabClosed(IZ)V
    .locals 3

    .prologue
    .line 227
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 228
    const-string/jumbo v1, "tabId"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 229
    const-string/jumbo v1, "incognito"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 230
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    const/4 v2, 0x5

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastNotification(Landroid/content/Context;ILandroid/os/Bundle;)V

    .line 231
    return-void
.end method

.method private notifyTabClosureCommitted(IZ)V
    .locals 3

    .prologue
    .line 269
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 270
    const-string/jumbo v1, "tabId"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 271
    const-string/jumbo v1, "incognito"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 272
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    const/16 v2, 0x49

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastImmediateNotification(Landroid/content/Context;ILandroid/os/Bundle;)V

    .line 274
    return-void
.end method

.method private notifyTabClosureUndone(IZ)V
    .locals 3

    .prologue
    .line 255
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 256
    const-string/jumbo v1, "tabId"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 257
    const-string/jumbo v1, "incognito"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 258
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    const/16 v2, 0x45

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastImmediateNotification(Landroid/content/Context;ILandroid/os/Bundle;)V

    .line 260
    return-void
.end method

.method private notifyTabCreated(IILjava/lang/String;Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;Z)V
    .locals 3

    .prologue
    .line 193
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 194
    const-string/jumbo v1, "tabId"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 195
    const-string/jumbo v1, "sourceTabId"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 196
    const-string/jumbo v1, "url"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 197
    const-string/jumbo v1, "type"

    invoke-virtual {p4}, Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 198
    const-string/jumbo v1, "incognito"

    invoke-virtual {v0, v1, p5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 199
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    const/4 v2, 0x2

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastNotification(Landroid/content/Context;ILandroid/os/Bundle;)V

    .line 200
    return-void
.end method

.method private notifyTabCreating(IILjava/lang/String;Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;Z)V
    .locals 3

    .prologue
    .line 171
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 172
    const-string/jumbo v1, "tabId"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 173
    const-string/jumbo v1, "sourceTabId"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 174
    const-string/jumbo v1, "url"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    const-string/jumbo v1, "type"

    invoke-virtual {p4}, Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    const-string/jumbo v1, "incognito"

    invoke-virtual {v0, v1, p5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 177
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    const/16 v2, 0x30

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastImmediateNotification(Landroid/content/Context;ILandroid/os/Bundle;)V

    .line 178
    return-void
.end method

.method private notifyTabMoved(IIIZ)V
    .locals 3

    .prologue
    .line 285
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 286
    const-string/jumbo v1, "tabId"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 287
    const-string/jumbo v1, "fromPosition"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 288
    const-string/jumbo v1, "toPosition"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 289
    const-string/jumbo v1, "incognito"

    invoke-virtual {v0, v1, p4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 290
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    const/4 v2, 0x4

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastNotification(Landroid/content/Context;ILandroid/os/Bundle;)V

    .line 291
    return-void
.end method

.method private notifyTabPendingClosure(ILjava/lang/String;Z)V
    .locals 3

    .prologue
    .line 241
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 242
    const-string/jumbo v1, "tabId"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 243
    const-string/jumbo v1, "title"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 244
    const-string/jumbo v1, "incognito"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 245
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    const/16 v2, 0x44

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastImmediateNotification(Landroid/content/Context;ILandroid/os/Bundle;)V

    .line 246
    return-void
.end method

.method private notifyTabSelected(IILorg/chromium/chrome/browser/tabmodel/TabModel$TabSelectionType;Z)V
    .locals 3

    .prologue
    .line 212
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 213
    const-string/jumbo v1, "tabId"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 214
    const-string/jumbo v1, "lastId"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 215
    const-string/jumbo v1, "type"

    invoke-virtual {p3}, Lorg/chromium/chrome/browser/tabmodel/TabModel$TabSelectionType;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 216
    const-string/jumbo v1, "incognito"

    invoke-virtual {v0, v1, p4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 217
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    const/4 v2, 0x3

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastImmediateNotification(Landroid/content/Context;ILandroid/os/Bundle;)V

    .line 218
    return-void
.end method


# virtual methods
.method public closeAllTabs()V
    .locals 3

    .prologue
    .line 133
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;->mTabSaver:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;->isIncognito()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->cancelLoadingTabs(Z)V

    .line 135
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;->mModelDelegate:Lorg/chromium/chrome/browser/tabmodel/TabModelDelegate;

    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelDelegate;->isInOverviewMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 136
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 137
    const-string/jumbo v1, "incognito"

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;->isIncognito()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 138
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    const/16 v2, 0x2b

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastNotification(Landroid/content/Context;ILandroid/os/Bundle;)V

    .line 143
    :goto_0
    return-void

    .line 142
    :cond_0
    invoke-super {p0}, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->closeAllTabs()V

    goto :goto_0
.end method

.method protected createNewTabForDevTools(Ljava/lang/String;)Lorg/chromium/chrome/browser/Tab;
    .locals 4

    .prologue
    .line 154
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/ChromeActivity;->getTabCreator(Z)Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;

    move-result-object v0

    new-instance v1, Lorg/chromium/content_public/browser/LoadUrlParams;

    invoke-direct {v1, p1}, Lorg/chromium/content_public/browser/LoadUrlParams;-><init>(Ljava/lang/String;)V

    sget-object v2, Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;->FROM_MENU_OR_OVERVIEW:Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;->createNewTab(Lorg/chromium/content_public/browser/LoadUrlParams;Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;Lorg/chromium/chrome/browser/Tab;)Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    return-object v0
.end method

.method protected createTabWithNativeContents(ZJI)Lorg/chromium/chrome/browser/Tab;
    .locals 2

    .prologue
    .line 148
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/ChromeActivity;->getTabCreator(Z)Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;

    move-result-object v0

    sget-object v1, Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;->FROM_LONGPRESS_BACKGROUND:Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;

    invoke-virtual {v0, p2, p3, p4, v1}, Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;->createTabWithNativeContents(JILorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;)Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    return-object v0
.end method

.method public supportsPendingClosures()Z
    .locals 1

    .prologue
    .line 128
    invoke-super {p0}, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->supportsPendingClosures()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabModelImpl;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    invoke-static {v0}, Lcom/google/android/apps/chrome/device/DeviceClassManager;->enableUndo(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
