.class Lcom/google/android/apps/chrome/videofling/MediaRouteController$2;
.super Ljava/lang/Object;
.source "MediaRouteController.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/videofling/MediaRouteController;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/videofling/MediaRouteController;)V
    .locals 0

    .prologue
    .line 330
    iput-object p1, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController$2;->this$0:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 333
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController$2;->this$0:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    # getter for: Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mReconnecting:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->access$100(Lcom/google/android/apps/chrome/videofling/MediaRouteController;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 334
    const-string/jumbo v0, "MediaRouteController"

    const-string/jumbo v1, "Reconnection timed out"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 336
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController$2;->this$0:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mReconnecting:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->access$102(Lcom/google/android/apps/chrome/videofling/MediaRouteController;Z)Z

    .line 337
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController$2;->this$0:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->release()V

    .line 339
    :cond_0
    return-void
.end method
