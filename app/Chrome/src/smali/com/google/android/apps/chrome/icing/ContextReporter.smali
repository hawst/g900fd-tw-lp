.class public Lcom/google/android/apps/chrome/icing/ContextReporter;
.super Ljava/lang/Object;
.source "ContextReporter.java"


# static fields
.field private static final NOTIFICATIONS:[I


# instance fields
.field private final mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

.field private final mController:Lcom/google/android/apps/chrome/icing/IcingController;

.field private mLastContextWasTitleChange:Z

.field private final mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/apps/chrome/icing/ContextReporter;->NOTIFICATIONS:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x3
        0x19
        0xa
    .end array-data
.end method

.method public constructor <init>(Lcom/google/android/apps/chrome/ChromeActivity;Lcom/google/android/apps/chrome/icing/IcingController;)V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    new-instance v0, Lcom/google/android/apps/chrome/icing/ContextReporter$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/icing/ContextReporter$1;-><init>(Lcom/google/android/apps/chrome/icing/ContextReporter;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/icing/ContextReporter;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    .line 36
    iput-object p1, p0, Lcom/google/android/apps/chrome/icing/ContextReporter;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    .line 37
    iput-object p2, p0, Lcom/google/android/apps/chrome/icing/ContextReporter;->mController:Lcom/google/android/apps/chrome/icing/IcingController;

    .line 38
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/icing/ContextReporter;)Lcom/google/android/apps/chrome/ChromeActivity;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/google/android/apps/chrome/icing/ContextReporter;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/icing/ContextReporter;Lorg/chromium/chrome/browser/Tab;)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/icing/ContextReporter;->report(Lorg/chromium/chrome/browser/Tab;)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/icing/ContextReporter;)Z
    .locals 1

    .prologue
    .line 18
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/icing/ContextReporter;->mLastContextWasTitleChange:Z

    return v0
.end method

.method static synthetic access$202(Lcom/google/android/apps/chrome/icing/ContextReporter;Z)Z
    .locals 0

    .prologue
    .line 18
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/icing/ContextReporter;->mLastContextWasTitleChange:Z

    return p1
.end method

.method private report(Lorg/chromium/chrome/browser/Tab;)V
    .locals 3

    .prologue
    .line 103
    iget-object v0, p0, Lcom/google/android/apps/chrome/icing/ContextReporter;->mController:Lcom/google/android/apps/chrome/icing/IcingController;

    invoke-virtual {p1}, Lorg/chromium/chrome/browser/Tab;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lorg/chromium/chrome/browser/Tab;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/icing/IcingController;->reportContext(Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    return-void
.end method


# virtual methods
.method public disable()V
    .locals 3

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/apps/chrome/icing/ContextReporter;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    sget-object v1, Lcom/google/android/apps/chrome/icing/ContextReporter;->NOTIFICATIONS:[I

    iget-object v2, p0, Lcom/google/android/apps/chrome/icing/ContextReporter;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->unregisterForNotifications(Landroid/content/Context;[ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    .line 59
    return-void
.end method

.method public enable()V
    .locals 3

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/apps/chrome/icing/ContextReporter;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeActivity;->getCurrentTab()Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    .line 45
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/Tab;->isIncognito()Z

    move-result v1

    if-nez v1, :cond_0

    .line 46
    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/icing/ContextReporter;->report(Lorg/chromium/chrome/browser/Tab;)V

    .line 47
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/icing/ContextReporter;->mLastContextWasTitleChange:Z

    .line 49
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/icing/ContextReporter;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    sget-object v1, Lcom/google/android/apps/chrome/icing/ContextReporter;->NOTIFICATIONS:[I

    iget-object v2, p0, Lcom/google/android/apps/chrome/icing/ContextReporter;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->registerForNotifications(Landroid/content/Context;[ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    .line 51
    return-void
.end method
