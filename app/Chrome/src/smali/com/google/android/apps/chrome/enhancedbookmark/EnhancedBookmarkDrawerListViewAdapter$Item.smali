.class Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListViewAdapter$Item;
.super Ljava/lang/Object;
.source "EnhancedBookmarkDrawerListViewAdapter.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field mFolderId:Lorg/chromium/components/bookmarks/BookmarkId;

.field mType:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    const-class v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListViewAdapter;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListViewAdapter$Item;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(I)V
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput p1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListViewAdapter$Item;->mType:I

    .line 46
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListViewAdapter$Item;->mFolderId:Lorg/chromium/components/bookmarks/BookmarkId;

    .line 47
    return-void
.end method

.method constructor <init>(Lorg/chromium/components/bookmarks/BookmarkId;)V
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    sget-boolean v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListViewAdapter$Item;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 51
    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListViewAdapter$Item;->mType:I

    .line 52
    iput-object p1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListViewAdapter$Item;->mFolderId:Lorg/chromium/components/bookmarks/BookmarkId;

    .line 53
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 57
    instance-of v1, p1, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListViewAdapter$Item;

    if-nez v1, :cond_1

    .line 59
    :cond_0
    :goto_0
    return v0

    .line 58
    :cond_1
    check-cast p1, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListViewAdapter$Item;

    .line 59
    iget v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListViewAdapter$Item;->mType:I

    iget v2, p1, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListViewAdapter$Item;->mType:I

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListViewAdapter$Item;->mFolderId:Lorg/chromium/components/bookmarks/BookmarkId;

    iget-object v2, p1, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListViewAdapter$Item;->mFolderId:Lorg/chromium/components/bookmarks/BookmarkId;

    invoke-virtual {v1, v2}, Lorg/chromium/components/bookmarks/BookmarkId;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListViewAdapter$Item;->mFolderId:Lorg/chromium/components/bookmarks/BookmarkId;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    iget v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListViewAdapter$Item;->mType:I

    mul-int/lit8 v1, v1, 0x25

    add-int/2addr v0, v1

    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListViewAdapter$Item;->mFolderId:Lorg/chromium/components/bookmarks/BookmarkId;

    invoke-virtual {v0}, Lorg/chromium/components/bookmarks/BookmarkId;->hashCode()I

    move-result v0

    goto :goto_0
.end method
