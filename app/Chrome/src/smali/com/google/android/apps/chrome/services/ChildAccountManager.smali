.class public Lcom/google/android/apps/chrome/services/ChildAccountManager;
.super Ljava/lang/Object;
.source "ChildAccountManager.java"


# static fields
.field private static sChildAccountManager:Lcom/google/android/apps/chrome/services/ChildAccountManager;

.field private static final sLock:Ljava/lang/Object;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mNativeChildAccountManager:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/apps/chrome/services/ChildAccountManager;->sLock:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    invoke-direct {p0}, Lcom/google/android/apps/chrome/services/ChildAccountManager;->nativeInit()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/chrome/services/ChildAccountManager;->mNativeChildAccountManager:J

    .line 48
    iput-object p1, p0, Lcom/google/android/apps/chrome/services/ChildAccountManager;->mContext:Landroid/content/Context;

    .line 53
    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->update()V

    .line 54
    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/services/ChildAccountManager;
    .locals 3

    .prologue
    .line 62
    sget-object v1, Lcom/google/android/apps/chrome/services/ChildAccountManager;->sLock:Ljava/lang/Object;

    monitor-enter v1

    .line 63
    :try_start_0
    sget-object v0, Lcom/google/android/apps/chrome/services/ChildAccountManager;->sChildAccountManager:Lcom/google/android/apps/chrome/services/ChildAccountManager;

    if-nez v0, :cond_0

    .line 64
    new-instance v0, Lcom/google/android/apps/chrome/services/ChildAccountManager;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/google/android/apps/chrome/services/ChildAccountManager;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/apps/chrome/services/ChildAccountManager;->sChildAccountManager:Lcom/google/android/apps/chrome/services/ChildAccountManager;

    .line 66
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 67
    sget-object v0, Lcom/google/android/apps/chrome/services/ChildAccountManager;->sChildAccountManager:Lcom/google/android/apps/chrome/services/ChildAccountManager;

    return-object v0

    .line 66
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private native nativeInit()J
.end method

.method private native nativeOnChildAccountSigninComplete(J)V
.end method


# virtual methods
.method public checkHasChildAccount(Lcom/google/android/apps/chrome/services/ChildAccountManager$HasChildAccountCallback;)V
    .locals 1

    .prologue
    .line 88
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Lcom/google/android/apps/chrome/services/ChildAccountManager$HasChildAccountCallback;->onChildAccountChecked(Z)V

    .line 89
    return-void
.end method

.method public hasChildAccount()Z
    .locals 1

    .prologue
    .line 92
    const/4 v0, 0x0

    return v0
.end method

.method public onChildAccountSigninComplete()V
    .locals 2

    .prologue
    .line 96
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/ChildAccountManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->setSignOutAllowed(Z)V

    .line 98
    iget-wide v0, p0, Lcom/google/android/apps/chrome/services/ChildAccountManager;->mNativeChildAccountManager:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/services/ChildAccountManager;->nativeOnChildAccountSigninComplete(J)V

    .line 99
    return-void
.end method
