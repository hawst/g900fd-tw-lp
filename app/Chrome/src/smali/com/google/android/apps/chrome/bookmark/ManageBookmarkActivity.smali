.class public Lcom/google/android/apps/chrome/bookmark/ManageBookmarkActivity;
.super Landroid/support/v4/app/k;
.source "ManageBookmarkActivity.java"


# static fields
.field public static final ADD_FOLDER_FRAGMENT_TAG:Ljava/lang/String; = "AddFolder"

.field protected static final ADD_FOLDER_SELECT_FOLDER_FRAGMENT_TAG:Ljava/lang/String; = "AddFolderSelectFolder"

.field public static final BASE_ADD_EDIT_FRAGMENT_TAG:Ljava/lang/String; = "AddEdit"

.field public static final BASE_SELECT_FOLDER_FRAGMENT_TAG:Ljava/lang/String; = "SelectFolder"

.field public static final BOOKMARK_INTENT_ID:Ljava/lang/String; = "_id"

.field public static final BOOKMARK_INTENT_IS_FOLDER:Ljava/lang/String; = "folder"

.field public static final BOOKMARK_INTENT_TITLE:Ljava/lang/String; = "title"

.field public static final BOOKMARK_INTENT_URL:Ljava/lang/String; = "url"


# instance fields
.field private mIsBackEnabled:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Landroid/support/v4/app/k;-><init>()V

    .line 74
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/bookmark/ManageBookmarkActivity;->mIsBackEnabled:Z

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/bookmark/ManageBookmarkActivity;Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/bookmark/ManageBookmarkActivity;->setActionListenerOnFolderSelection(Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment;)V

    return-void
.end method

.method static synthetic access$102(Lcom/google/android/apps/chrome/bookmark/ManageBookmarkActivity;Z)Z
    .locals 0

    .prologue
    .line 31
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/bookmark/ManageBookmarkActivity;->mIsBackEnabled:Z

    return p1
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/bookmark/ManageBookmarkActivity;Landroid/app/Fragment;JLjava/lang/String;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/chrome/bookmark/ManageBookmarkActivity;->newFolder(Landroid/app/Fragment;JLjava/lang/String;)V

    return-void
.end method

.method private generateBaseFragment()Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;
    .locals 7

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 200
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/bookmark/ManageBookmarkActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    if-nez v2, :cond_0

    .line 201
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "intent can not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 203
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/bookmark/ManageBookmarkActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 204
    invoke-virtual {v2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v3

    .line 209
    if-eqz v3, :cond_2

    invoke-virtual {v3}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "editbookmark"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 210
    const-string/jumbo v2, "isfolder"

    invoke-virtual {v3, v2, v1}, Landroid/net/Uri;->getBooleanQueryParameter(Ljava/lang/String;Z)Z

    move-result v1

    .line 211
    const-string/jumbo v2, "id"

    invoke-virtual {v3, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 212
    if-eqz v2, :cond_1

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 213
    :cond_1
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->newEditInstance(ZJ)Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;

    move-result-object v0

    .line 233
    :goto_0
    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/bookmark/ManageBookmarkActivity;->setActionListenerOnAddEdit(Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;)V

    .line 234
    return-object v0

    .line 215
    :cond_2
    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    .line 218
    if-eqz v4, :cond_6

    .line 219
    const-string/jumbo v2, "folder"

    invoke-virtual {v4, v2, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    .line 221
    const-string/jumbo v1, "title"

    invoke-virtual {v4, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 222
    const-string/jumbo v1, "title"

    invoke-virtual {v4, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 224
    :goto_1
    const-string/jumbo v2, "url"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 225
    const-string/jumbo v2, "url"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 227
    :goto_2
    const-string/jumbo v5, "_id"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 228
    const-string/jumbo v0, "_id"

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    move v6, v3

    move-object v3, v2

    move-object v2, v0

    move v0, v6

    .line 231
    :goto_3
    invoke-static {v0, v2, v1, v3}, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->newInstance(ZLjava/lang/Long;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;

    move-result-object v0

    goto :goto_0

    :cond_3
    move v6, v3

    move-object v3, v2

    move-object v2, v0

    move v0, v6

    goto :goto_3

    :cond_4
    move-object v2, v0

    goto :goto_2

    :cond_5
    move-object v1, v0

    goto :goto_1

    :cond_6
    move-object v2, v0

    move-object v3, v0

    move-object v6, v0

    move v0, v1

    move-object v1, v6

    goto :goto_3
.end method

.method private initializeFragmentState()V
    .locals 4

    .prologue
    .line 158
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/bookmark/ManageBookmarkActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const-string/jumbo v1, "AddEdit"

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;

    .line 160
    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/bookmark/ManageBookmarkActivity;->setActionListenerOnAddEdit(Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;)V

    .line 162
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/bookmark/ManageBookmarkActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string/jumbo v2, "SelectFolder"

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v2

    .line 164
    if-eqz v2, :cond_0

    move-object v1, v2

    .line 165
    check-cast v1, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment;

    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/bookmark/ManageBookmarkActivity;->setActionListenerOnFolderSelection(Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment;)V

    .line 168
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/bookmark/ManageBookmarkActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v3

    .line 169
    invoke-virtual {v3, v0}, Landroid/app/FragmentTransaction;->hide(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 171
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/bookmark/ManageBookmarkActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const-string/jumbo v1, "AddFolder"

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    .line 173
    if-eqz v1, :cond_2

    .line 174
    invoke-virtual {v3, v2}, Landroid/app/FragmentTransaction;->hide(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-object v0, v1

    .line 175
    check-cast v0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/bookmark/ManageBookmarkActivity;->setActionListenerOnAddEdit(Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;)V

    .line 177
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/bookmark/ManageBookmarkActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const-string/jumbo v2, "AddFolderSelectFolder"

    invoke-virtual {v0, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v2

    .line 179
    if-eqz v2, :cond_1

    move-object v0, v2

    .line 180
    check-cast v0, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment;

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/bookmark/ManageBookmarkActivity;->setActionListenerOnFolderSelection(Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment;)V

    .line 182
    invoke-virtual {v3, v1}, Landroid/app/FragmentTransaction;->hide(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 183
    invoke-virtual {v3, v2}, Landroid/app/FragmentTransaction;->show(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 190
    :goto_0
    invoke-virtual {v3}, Landroid/app/FragmentTransaction;->commit()I

    .line 192
    :cond_0
    return-void

    .line 185
    :cond_1
    invoke-virtual {v3, v1}, Landroid/app/FragmentTransaction;->show(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    goto :goto_0

    .line 188
    :cond_2
    invoke-virtual {v3, v2}, Landroid/app/FragmentTransaction;->show(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    goto :goto_0
.end method

.method private newFolder(Landroid/app/Fragment;JLjava/lang/String;)V
    .locals 4

    .prologue
    .line 328
    invoke-static {p2, p3, p4}, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->newAddNewFolderInstance(JLjava/lang/String;)Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;

    move-result-object v0

    .line 330
    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->setTargetFragment(Landroid/app/Fragment;I)V

    .line 331
    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/bookmark/ManageBookmarkActivity;->setActionListenerOnAddEdit(Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;)V

    .line 332
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/bookmark/ManageBookmarkActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/app/FragmentTransaction;->hide(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v1

    sget v2, Lcom/google/android/apps/chrome/R$id;->fragment_container:I

    const-string/jumbo v3, "AddFolder"

    invoke-virtual {v1, v2, v0, v3}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    .line 337
    return-void
.end method

.method private setActionListenerOnAddEdit(Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;)V
    .locals 1

    .prologue
    .line 242
    new-instance v0, Lcom/google/android/apps/chrome/bookmark/ManageBookmarkActivity$2;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/chrome/bookmark/ManageBookmarkActivity$2;-><init>(Lcom/google/android/apps/chrome/bookmark/ManageBookmarkActivity;Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;)V

    invoke-virtual {p1, v0}, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->setOnActionListener(Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$OnActionListener;)V

    .line 304
    return-void
.end method

.method private setActionListenerOnFolderSelection(Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment;)V
    .locals 1

    .prologue
    .line 312
    new-instance v0, Lcom/google/android/apps/chrome/bookmark/ManageBookmarkActivity$3;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/chrome/bookmark/ManageBookmarkActivity$3;-><init>(Lcom/google/android/apps/chrome/bookmark/ManageBookmarkActivity;Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment;)V

    invoke-virtual {p1, v0}, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment;->setOnActionListener(Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment$OnActionListener;)V

    .line 318
    return-void
.end method


# virtual methods
.method public finish()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 122
    invoke-super {p0}, Landroid/support/v4/app/k;->finish()V

    .line 125
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/bookmark/ManageBookmarkActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/content/res/Configuration;->isLayoutSizeAtLeast(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 127
    invoke-virtual {p0, v2, v2}, Lcom/google/android/apps/chrome/bookmark/ManageBookmarkActivity;->overridePendingTransition(II)V

    .line 129
    :cond_0
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 139
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/bookmark/ManageBookmarkActivity;->mIsBackEnabled:Z

    if-nez v0, :cond_0

    .line 145
    :goto_0
    return-void

    .line 140
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/bookmark/ManageBookmarkActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->getBackStackEntryCount()I

    move-result v0

    if-nez v0, :cond_1

    .line 141
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/bookmark/ManageBookmarkActivity;->finish()V

    goto :goto_0

    .line 143
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/bookmark/ManageBookmarkActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->popBackStackImmediate()Z

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 78
    invoke-super {p0, p1}, Landroid/support/v4/app/k;->onCreate(Landroid/os/Bundle;)V

    .line 80
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/bookmark/ManageBookmarkActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    instance-of v0, v0, Lcom/google/android/apps/chrome/ChromeMobileApplication;

    if-eqz v0, :cond_0

    .line 81
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/bookmark/ManageBookmarkActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/ChromeMobileApplication;

    const/4 v1, 0x1

    invoke-virtual {v0, p0, v1}, Lcom/google/android/apps/chrome/ChromeMobileApplication;->startBrowserProcessesAndLoadLibrariesSync(Landroid/content/Context;Z)V
    :try_end_0
    .catch Lorg/chromium/base/library_loader/ProcessInitException; {:try_start_0 .. :try_end_0} :catch_0

    .line 89
    :cond_0
    invoke-static {p0}, Lorg/chromium/ui/base/DeviceFormFactor;->isTablet(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 90
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/bookmark/ManageBookmarkActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x12

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 94
    :cond_1
    sget v0, Lcom/google/android/apps/chrome/R$layout;->manage_bookmark:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/bookmark/ManageBookmarkActivity;->setContentView(I)V

    .line 96
    if-nez p1, :cond_3

    .line 97
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/bookmark/ManageBookmarkActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 98
    sget v1, Lcom/google/android/apps/chrome/R$id;->fragment_container:I

    invoke-direct {p0}, Lcom/google/android/apps/chrome/bookmark/ManageBookmarkActivity;->generateBaseFragment()Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;

    move-result-object v2

    const-string/jumbo v3, "AddEdit"

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 100
    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    .line 107
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/bookmark/ManageBookmarkActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/chrome/bookmark/ManageBookmarkActivity$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/bookmark/ManageBookmarkActivity$1;-><init>(Lcom/google/android/apps/chrome/bookmark/ManageBookmarkActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->addOnBackStackChangedListener(Landroid/app/FragmentManager$OnBackStackChangedListener;)V

    .line 116
    invoke-static {p0}, Landroid/nfc/NfcAdapter;->getDefaultAdapter(Landroid/content/Context;)Landroid/nfc/NfcAdapter;

    move-result-object v0

    .line 117
    if-eqz v0, :cond_2

    const/4 v1, 0x0

    const/4 v2, 0x0

    new-array v2, v2, [Landroid/app/Activity;

    invoke-virtual {v0, v1, p0, v2}, Landroid/nfc/NfcAdapter;->setNdefPushMessage(Landroid/nfc/NdefMessage;Landroid/app/Activity;[Landroid/app/Activity;)V

    .line 118
    :cond_2
    :goto_1
    return-void

    .line 84
    :catch_0
    move-exception v0

    .line 85
    const-string/jumbo v1, "ManageBookmarkActivity"

    const-string/jumbo v2, "Unable to load native library."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 86
    invoke-static {v0}, Lcom/google/android/apps/chrome/ChromeMobileApplication;->reportStartupErrorAndExit(Lorg/chromium/base/library_loader/ProcessInitException;)V

    goto :goto_1

    .line 102
    :cond_3
    invoke-direct {p0}, Lcom/google/android/apps/chrome/bookmark/ManageBookmarkActivity;->initializeFragmentState()V

    goto :goto_0
.end method

.method protected onUserLeaveHint()V
    .locals 0

    .prologue
    .line 133
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/bookmark/ManageBookmarkActivity;->finish()V

    .line 134
    invoke-super {p0}, Landroid/support/v4/app/k;->onUserLeaveHint()V

    .line 135
    return-void
.end method
