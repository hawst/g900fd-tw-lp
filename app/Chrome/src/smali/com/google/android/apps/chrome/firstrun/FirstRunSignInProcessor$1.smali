.class Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor$1;
.super Lcom/google/android/apps/chrome/services/AndroidEduAndChildAccountHelper;
.source "FirstRunSignInProcessor.java"


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;)V
    .locals 0

    .prologue
    .line 84
    iput-object p1, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor$1;->this$0:Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/services/AndroidEduAndChildAccountHelper;-><init>()V

    return-void
.end method


# virtual methods
.method public onParametersReady()V
    .locals 2

    .prologue
    .line 87
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor$1;->this$0:Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor$1;->isAndroidEduDevice()Z

    move-result v1

    # setter for: Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;->mIsAndroidEduDevice:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;->access$002(Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;Z)Z

    .line 88
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor$1;->this$0:Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor$1;->hasChildAccount()Z

    move-result v1

    # setter for: Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;->mHasChildAccount:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;->access$102(Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;Z)Z

    .line 89
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor$1;->this$0:Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;

    # getter for: Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;->mSignInManager:Lorg/chromium/chrome/browser/signin/SigninManager;
    invoke-static {v0}, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;->access$200(Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;)Lorg/chromium/chrome/browser/signin/SigninManager;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/signin/SigninManager;->onFirstRunCheckDone()V

    .line 90
    iget-object v1, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor$1;->this$0:Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor$1;->this$0:Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;

    # getter for: Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;->mHasChildAccount:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;->access$100(Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x2

    :goto_0
    # setter for: Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;->mSignInType:I
    invoke-static {v1, v0}, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;->access$302(Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;I)I

    .line 100
    invoke-static {}, Lorg/chromium/base/CommandLine;->getInstance()Lorg/chromium/base/CommandLine;

    move-result-object v0

    const-string/jumbo v1, "disable-fre"

    invoke-virtual {v0, v1}, Lorg/chromium/base/CommandLine;->hasSwitch(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor$1;->this$0:Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;

    # getter for: Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;->mPrefManager:Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;
    invoke-static {v0}, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;->access$400(Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getFirstRunFlowComplete()Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor$1;->this$0:Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;

    # getter for: Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;->access$500(Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;)Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/ToSAckedReceiver;->checkAnyUserHasSeenToS(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 104
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor$1;->this$0:Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;

    # getter for: Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;->mSignInManager:Lorg/chromium/chrome/browser/signin/SigninManager;
    invoke-static {v0}, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;->access$200(Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;)Lorg/chromium/chrome/browser/signin/SigninManager;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/signin/SigninManager;->onFirstRunCheckDone()V

    .line 105
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor$1;->this$0:Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;

    # getter for: Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;->mObserver:Lorg/chromium/chrome/browser/signin/SigninManager$SignInFlowObserver;
    invoke-static {v0}, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;->access$600(Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;)Lorg/chromium/chrome/browser/signin/SigninManager$SignInFlowObserver;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor$1;->this$0:Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;

    # getter for: Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;->mObserver:Lorg/chromium/chrome/browser/signin/SigninManager$SignInFlowObserver;
    invoke-static {v0}, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;->access$600(Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;)Lorg/chromium/chrome/browser/signin/SigninManager$SignInFlowObserver;

    move-result-object v0

    invoke-interface {v0}, Lorg/chromium/chrome/browser/signin/SigninManager$SignInFlowObserver;->onSigninComplete()V

    .line 123
    :cond_1
    :goto_1
    return-void

    .line 90
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor$1;->this$0:Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;

    # getter for: Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;->mIsAndroidEduDevice:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;->access$000(Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    .line 110
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor$1;->this$0:Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;

    # getter for: Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;->mPrefManager:Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;
    invoke-static {v0}, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;->access$400(Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getFirstRunFlowComplete()Z

    move-result v0

    if-nez v0, :cond_5

    .line 111
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor$1;->this$0:Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;

    # invokes: Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;->requestToFireIntentAndFinish()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;->access$700(Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;)V

    goto :goto_1

    .line 115
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor$1;->this$0:Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;

    # getter for: Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;->mPrefManager:Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;
    invoke-static {v0}, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;->access$400(Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getFirstRunFlowSignInComplete()Z

    move-result v0

    if-nez v0, :cond_6

    .line 119
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor$1;->this$0:Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;

    # invokes: Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;->completeFreSignInRequest()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;->access$800(Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;)V

    goto :goto_1

    .line 121
    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor$1;->this$0:Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;

    # invokes: Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;->processAutomaticSignIn()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;->access$900(Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;)V

    goto :goto_1
.end method
