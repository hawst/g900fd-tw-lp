.class Lcom/google/android/apps/chrome/icing/IcingController$4;
.super Lcom/google/android/apps/chrome/icing/ConnectedTask;
.source "IcingController.java"


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/icing/IcingController;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/icing/IcingController;Lcom/google/android/apps/chrome/icing/GoogleApiIcingClient;Ljava/util/concurrent/ScheduledThreadPoolExecutor;)V
    .locals 0

    .prologue
    .line 148
    iput-object p1, p0, Lcom/google/android/apps/chrome/icing/IcingController$4;->this$0:Lcom/google/android/apps/chrome/icing/IcingController;

    invoke-direct {p0, p2, p3}, Lcom/google/android/apps/chrome/icing/ConnectedTask;-><init>(Lcom/google/android/apps/chrome/icing/GoogleApiIcingClient;Ljava/util/concurrent/ScheduledThreadPoolExecutor;)V

    return-void
.end method


# virtual methods
.method protected doWhenConnected(Lcom/google/android/apps/chrome/icing/GoogleApiIcingClient;)V
    .locals 3

    .prologue
    .line 156
    invoke-interface {p1}, Lcom/google/android/apps/chrome/icing/GoogleApiIcingClient;->clearLegacyCorpusData()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 157
    iget-object v0, p0, Lcom/google/android/apps/chrome/icing/IcingController$4;->this$0:Lcom/google/android/apps/chrome/icing/IcingController;

    # getter for: Lcom/google/android/apps/chrome/icing/IcingController;->mPrefs:Landroid/content/SharedPreferences;
    invoke-static {v0}, Lcom/google/android/apps/chrome/icing/IcingController;->access$000(Lcom/google/android/apps/chrome/icing/IcingController;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "com.google.android.apps.chrome.icing.IcingController.LEGACY_CORPUS_DATA_CLEARED_PREFS_NAME"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 160
    :cond_0
    return-void
.end method

.method protected getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 151
    const-string/jumbo v0, "ClearLegacyCorpusData"

    return-object v0
.end method
