.class Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$1;
.super Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider$Observer;
.source "KnoxSettings.java"


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;)V
    .locals 0

    .prologue
    .line 46
    iput-object p1, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$1;->this$0:Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider$Observer;-><init>()V

    return-void
.end method


# virtual methods
.method public onAuditLogEnabledChanged()V
    .locals 2

    .prologue
    .line 148
    iget-object v0, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$1;->this$0:Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;

    new-instance v1, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$1$10;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$1$10;-><init>(Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$1;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;->postUpdateSettingsTask(Landroid/os/AsyncTask;)V

    .line 154
    return-void
.end method

.method public onAutofillEnabledChanged()V
    .locals 2

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$1;->this$0:Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;

    new-instance v1, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$1$2;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$1$2;-><init>(Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$1;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;->postUpdateSettingsTask(Landroid/os/AsyncTask;)V

    .line 74
    return-void
.end method

.method public onCertificateRemoved()V
    .locals 0

    .prologue
    .line 161
    invoke-static {}, Lorg/chromium/chrome/browser/SSLClientCertificateRequest;->notifyClientCertificatesChangedOnIOThread()V

    .line 162
    return-void
.end method

.method public onCookiesEnabledChanged()V
    .locals 2

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$1;->this$0:Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;

    new-instance v1, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$1$3;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$1$3;-><init>(Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$1;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;->postUpdateSettingsTask(Landroid/os/AsyncTask;)V

    .line 84
    return-void
.end method

.method public onDeviceAdminRemoved()V
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$1;->this$0:Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;->refreshSettings()V

    .line 54
    return-void
.end method

.method public onHttpProxyHostPortChanged()V
    .locals 2

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$1;->this$0:Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;

    new-instance v1, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$1$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$1$1;-><init>(Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$1;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;->postUpdateSettingsTask(Landroid/os/AsyncTask;)V

    .line 64
    return-void
.end method

.method public onJavascriptEnabledChanged()V
    .locals 2

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$1;->this$0:Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;

    new-instance v1, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$1$4;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$1$4;-><init>(Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$1;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;->postUpdateSettingsTask(Landroid/os/AsyncTask;)V

    .line 94
    return-void
.end method

.method public onPopupsEnabledChanged()V
    .locals 2

    .prologue
    .line 98
    iget-object v0, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$1;->this$0:Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;

    new-instance v1, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$1$5;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$1$5;-><init>(Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$1;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;->postUpdateSettingsTask(Landroid/os/AsyncTask;)V

    .line 104
    return-void
.end method

.method public onSmartCardAuthenticationEnabledChanged()V
    .locals 2

    .prologue
    .line 108
    iget-object v0, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$1;->this$0:Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;

    new-instance v1, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$1$6;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$1$6;-><init>(Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$1;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;->postUpdateSettingsTask(Landroid/os/AsyncTask;)V

    .line 114
    return-void
.end method

.method public onUrlBlacklistChanged()V
    .locals 2

    .prologue
    .line 118
    iget-object v0, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$1;->this$0:Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;

    new-instance v1, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$1$7;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$1$7;-><init>(Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$1;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;->postUpdateSettingsTask(Landroid/os/AsyncTask;)V

    .line 124
    return-void
.end method

.method public onUrlFilterEnabledChanged()V
    .locals 2

    .prologue
    .line 128
    iget-object v0, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$1;->this$0:Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;

    new-instance v1, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$1$8;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$1$8;-><init>(Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$1;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;->postUpdateSettingsTask(Landroid/os/AsyncTask;)V

    .line 134
    return-void
.end method

.method public onUrlFilterReportEnabledChanged()V
    .locals 2

    .prologue
    .line 138
    iget-object v0, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$1;->this$0:Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;

    new-instance v1, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$1$9;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$1$9;-><init>(Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$1;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;->postUpdateSettingsTask(Landroid/os/AsyncTask;)V

    .line 144
    return-void
.end method
