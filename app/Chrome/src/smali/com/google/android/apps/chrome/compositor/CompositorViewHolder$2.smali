.class Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$2;
.super Ljava/lang/Object;
.source "CompositorViewHolder.java"

# interfaces
.implements Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper$BackgroundContentViewListener;


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 235
    const-class v0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$2;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;)V
    .locals 0

    .prologue
    .line 235
    iput-object p1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$2;->this$0:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onBackgroundViewCreated(Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;)V
    .locals 3

    .prologue
    .line 238
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$2;->this$0:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    # getter for: Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mTabVisible:Lcom/google/android/apps/chrome/tab/ChromeTab;
    invoke-static {v0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->access$100(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;)Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getBackgroundContentViewHelper()Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;

    move-result-object v0

    if-eq p1, v0, :cond_0

    .line 246
    :goto_0
    return-void

    .line 240
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$2;->this$0:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v1

    # setter for: Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mBackgroundContentViewCore:Lorg/chromium/content/browser/ContentViewCore;
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->access$202(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;Lorg/chromium/content/browser/ContentViewCore;)Lorg/chromium/content/browser/ContentViewCore;

    .line 241
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$2;->this$0:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    iget-object v1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$2;->this$0:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    # getter for: Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mBackgroundContentViewCore:Lorg/chromium/content/browser/ContentViewCore;
    invoke-static {v1}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->access$200(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;)Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v1

    const/4 v2, 0x1

    # invokes: Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->setupContentView(Lorg/chromium/content/browser/ContentViewCore;Z)V
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->access$300(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;Lorg/chromium/content/browser/ContentViewCore;Z)V

    .line 244
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$2;->this$0:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    # getter for: Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mBackgroundContentViewCore:Lorg/chromium/content/browser/ContentViewCore;
    invoke-static {v0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->access$200(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;)Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$2;->this$0:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->getFullscreenManager()Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->getTopControlsHeight()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/chromium/content/browser/ContentViewCore;->setViewportSizeOffset(II)V

    goto :goto_0
.end method

.method public onBackgroundViewRemoved(Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;)V
    .locals 2

    .prologue
    .line 250
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$2;->this$0:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    # getter for: Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mTabVisible:Lcom/google/android/apps/chrome/tab/ChromeTab;
    invoke-static {v0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->access$100(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;)Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getBackgroundContentViewHelper()Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;

    move-result-object v0

    if-eq p1, v0, :cond_1

    .line 255
    :cond_0
    :goto_0
    return-void

    .line 251
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$2;->this$0:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    # getter for: Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mBackgroundContentViewCore:Lorg/chromium/content/browser/ContentViewCore;
    invoke-static {v0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->access$200(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;)Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 252
    sget-boolean v0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$2;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$2;->this$0:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    # getter for: Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mCompositorView:Lcom/google/android/apps/chrome/compositor/CompositorView;
    invoke-static {v0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->access$400(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;)Lcom/google/android/apps/chrome/compositor/CompositorView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/compositor/CompositorView;->getInstantPageContainer()Lcom/google/android/apps/chrome/compositor/InstantPageContainer;

    move-result-object v0

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 253
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$2;->this$0:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    # getter for: Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mCompositorView:Lcom/google/android/apps/chrome/compositor/CompositorView;
    invoke-static {v0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->access$400(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;)Lcom/google/android/apps/chrome/compositor/CompositorView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/compositor/CompositorView;->getInstantPageContainer()Lcom/google/android/apps/chrome/compositor/InstantPageContainer;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$2;->this$0:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    # getter for: Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mBackgroundContentViewCore:Lorg/chromium/content/browser/ContentViewCore;
    invoke-static {v1}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->access$200(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;)Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/compositor/InstantPageContainer;->removeChild(Lorg/chromium/content/browser/ContentViewCore;)V

    .line 254
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$2;->this$0:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mBackgroundContentViewCore:Lorg/chromium/content/browser/ContentViewCore;
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->access$202(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;Lorg/chromium/content/browser/ContentViewCore;)Lorg/chromium/content/browser/ContentViewCore;

    goto :goto_0
.end method
