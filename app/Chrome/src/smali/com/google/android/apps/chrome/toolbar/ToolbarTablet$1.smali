.class Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$1;
.super Lcom/google/android/apps/chrome/toolbar/KeyboardNavigationListener;
.source "ToolbarTablet.java"


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;)V
    .locals 0

    .prologue
    .line 84
    iput-object p1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$1;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/toolbar/KeyboardNavigationListener;-><init>()V

    return-void
.end method


# virtual methods
.method public getNextFocusBackward()Landroid/view/View;
    .locals 2

    .prologue
    .line 98
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$1;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;

    sget v1, Lcom/google/android/apps/chrome/R$id;->menu_button:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public getNextFocusForward()Landroid/view/View;
    .locals 2

    .prologue
    .line 87
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$1;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mBackButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->access$000(Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ImageButton;->isFocusable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 88
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$1;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;

    sget v1, Lcom/google/android/apps/chrome/R$id;->back_button:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 92
    :goto_0
    return-object v0

    .line 89
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$1;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->mForwardButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->access$100(Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ImageButton;->isFocusable()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 90
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$1;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;

    sget v1, Lcom/google/android/apps/chrome/R$id;->forward_button:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 92
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet$1;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;

    sget v1, Lcom/google/android/apps/chrome/R$id;->refresh_button:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarTablet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method
