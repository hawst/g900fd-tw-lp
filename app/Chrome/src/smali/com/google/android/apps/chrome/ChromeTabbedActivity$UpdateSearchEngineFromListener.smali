.class Lcom/google/android/apps/chrome/ChromeTabbedActivity$UpdateSearchEngineFromListener;
.super Ljava/lang/Object;
.source "ChromeTabbedActivity.java"

# interfaces
.implements Lorg/chromium/chrome/browser/search_engines/TemplateUrlService$LoadListener;


# instance fields
.field private final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1557
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1558
    iput-object p1, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity$UpdateSearchEngineFromListener;->mContext:Landroid/content/Context;

    .line 1559
    return-void
.end method


# virtual methods
.method public onTemplateUrlServiceLoaded()V
    .locals 1

    .prologue
    .line 1563
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity$UpdateSearchEngineFromListener;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->setSharedPreferenceValueFromNativeForSearchEngine()V

    .line 1565
    invoke-static {}, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;->getInstance()Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;

    move-result-object v0

    invoke-virtual {v0, p0}, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;->unregisterLoadListener(Lorg/chromium/chrome/browser/search_engines/TemplateUrlService$LoadListener;)V

    .line 1566
    return-void
.end method
