.class public Lcom/google/android/apps/chrome/videofling/PosterManager;
.super Ljava/lang/Object;
.source "PosterManager.java"


# instance fields
.field private mLastBitmap:Landroid/graphics/Bitmap;

.field private mPlayers:Ljava/util/Map;

.field private mPosterBitmaps:Ljava/util/Map;

.field private final mTabObserver:Lorg/chromium/chrome/browser/TabObserver;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/videofling/PosterManager;->mPosterBitmaps:Ljava/util/Map;

    .line 29
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/videofling/PosterManager;->mPlayers:Ljava/util/Map;

    .line 66
    new-instance v0, Lcom/google/android/apps/chrome/videofling/PosterManager$CastTabObserver;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/chrome/videofling/PosterManager$CastTabObserver;-><init>(Lcom/google/android/apps/chrome/videofling/PosterManager;Lcom/google/android/apps/chrome/videofling/PosterManager$1;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/videofling/PosterManager;->mTabObserver:Lorg/chromium/chrome/browser/TabObserver;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/videofling/PosterManager;Lorg/chromium/chrome/browser/Tab;)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/videofling/PosterManager;->cleanupTab(Lorg/chromium/chrome/browser/Tab;)V

    return-void
.end method

.method private cleanupTab(Lorg/chromium/chrome/browser/Tab;)V
    .locals 4

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/PosterManager;->mPlayers:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 73
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/PosterManager;->mPlayers:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/SparseArray;

    .line 74
    const/4 v1, 0x0

    move v2, v1

    :goto_0
    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v1

    if-ge v2, v1, :cond_0

    .line 75
    invoke-virtual {v0, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 76
    iget-object v3, p0, Lcom/google/android/apps/chrome/videofling/PosterManager;->mPosterBitmaps:Ljava/util/Map;

    invoke-interface {v3, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 74
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 78
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/PosterManager;->mPlayers:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 80
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/PosterManager;->mTabObserver:Lorg/chromium/chrome/browser/TabObserver;

    invoke-virtual {p1, v0}, Lorg/chromium/chrome/browser/Tab;->removeObserver(Lorg/chromium/chrome/browser/TabObserver;)V

    .line 81
    return-void
.end method


# virtual methods
.method public clearCurrentBitmap()V
    .locals 1

    .prologue
    .line 132
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/videofling/PosterManager;->mLastBitmap:Landroid/graphics/Bitmap;

    .line 133
    return-void
.end method

.method public getPoster(Lorg/chromium/chrome/browser/Tab;I)Landroid/graphics/Bitmap;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 121
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/PosterManager;->mLastBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/PosterManager;->mLastBitmap:Landroid/graphics/Bitmap;

    .line 128
    :goto_0
    return-object v0

    .line 123
    :cond_0
    if-nez p1, :cond_1

    move-object v0, v1

    goto :goto_0

    .line 124
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/PosterManager;->mPlayers:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/SparseArray;

    .line 125
    if-nez v0, :cond_2

    move-object v0, v1

    goto :goto_0

    .line 126
    :cond_2
    invoke-virtual {v0, p2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 127
    iget-object v1, p0, Lcom/google/android/apps/chrome/videofling/PosterManager;->mPosterBitmaps:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/google/android/apps/chrome/videofling/PosterManager;->mLastBitmap:Landroid/graphics/Bitmap;

    .line 128
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/PosterManager;->mLastBitmap:Landroid/graphics/Bitmap;

    goto :goto_0
.end method

.method public setPosterBitmap(Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 1

    .prologue
    .line 92
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/PosterManager;->mPosterBitmaps:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 93
    :cond_0
    return-void
.end method

.method public setPosterUrlForPlayer(Lorg/chromium/chrome/browser/Tab;ILjava/lang/String;)V
    .locals 2

    .prologue
    .line 103
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/PosterManager;->mPlayers:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 104
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/PosterManager;->mPlayers:Ljava/util/Map;

    new-instance v1, Landroid/util/SparseArray;

    invoke-direct {v1}, Landroid/util/SparseArray;-><init>()V

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 105
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/PosterManager;->mTabObserver:Lorg/chromium/chrome/browser/TabObserver;

    invoke-virtual {p1, v0}, Lorg/chromium/chrome/browser/Tab;->addObserver(Lorg/chromium/chrome/browser/TabObserver;)V

    .line 108
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/PosterManager;->mPlayers:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/SparseArray;

    .line 109
    invoke-virtual {v0, p2, p3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 110
    return-void
.end method
