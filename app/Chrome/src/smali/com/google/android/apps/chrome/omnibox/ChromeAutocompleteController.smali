.class public Lcom/google/android/apps/chrome/omnibox/ChromeAutocompleteController;
.super Lorg/chromium/chrome/browser/omnibox/AutocompleteController;
.source "ChromeAutocompleteController.java"


# instance fields
.field private final mVoiceSuggestionProvider:Lcom/google/android/apps/chrome/omnibox/VoiceSuggestionProvider;


# direct methods
.method public constructor <init>(Lorg/chromium/chrome/browser/omnibox/AutocompleteController$OnSuggestionsReceivedListener;)V
    .locals 1

    .prologue
    .line 28
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, Lorg/chromium/chrome/browser/omnibox/AutocompleteController;-><init>(Lorg/chromium/chrome/browser/profiles/Profile;Lorg/chromium/chrome/browser/omnibox/AutocompleteController$OnSuggestionsReceivedListener;)V

    .line 25
    new-instance v0, Lcom/google/android/apps/chrome/omnibox/VoiceSuggestionProvider;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/omnibox/VoiceSuggestionProvider;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/omnibox/ChromeAutocompleteController;->mVoiceSuggestionProvider:Lcom/google/android/apps/chrome/omnibox/VoiceSuggestionProvider;

    .line 29
    return-void
.end method


# virtual methods
.method protected onSuggestionSelected(ILorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;Ljava/lang/String;ZZJLorg/chromium/content_public/browser/WebContents;)V
    .locals 2

    .prologue
    .line 76
    sget-object v0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;->VOICE_SUGGEST:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

    if-ne p2, v0, :cond_0

    .line 79
    :goto_0
    return-void

    .line 77
    :cond_0
    invoke-super/range {p0 .. p8}, Lorg/chromium/chrome/browser/omnibox/AutocompleteController;->onSuggestionSelected(ILorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;Ljava/lang/String;ZZJLorg/chromium/content_public/browser/WebContents;)V

    goto :goto_0
.end method

.method protected onSuggestionsReceived(Ljava/util/List;Ljava/lang/String;J)V
    .locals 3

    .prologue
    const/4 v1, 0x5

    .line 46
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v1, :cond_0

    .line 48
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-interface {p1, v1, v0}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 52
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/ChromeAutocompleteController;->mVoiceSuggestionProvider:Lcom/google/android/apps/chrome/omnibox/VoiceSuggestionProvider;

    const/4 v1, 0x3

    invoke-virtual {v0, p1, v1}, Lcom/google/android/apps/chrome/omnibox/VoiceSuggestionProvider;->addVoiceSuggestions(Ljava/util/List;I)Ljava/util/List;

    move-result-object v0

    .line 55
    invoke-super {p0, v0, p2, p3, p4}, Lorg/chromium/chrome/browser/omnibox/AutocompleteController;->onSuggestionsReceived(Ljava/util/List;Ljava/lang/String;J)V

    .line 57
    return-void
.end method

.method public onVoiceResults(Landroid/os/Bundle;)Lcom/google/android/apps/chrome/omnibox/VoiceSuggestionProvider$VoiceResult;
    .locals 2

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/ChromeAutocompleteController;->mVoiceSuggestionProvider:Lcom/google/android/apps/chrome/omnibox/VoiceSuggestionProvider;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/omnibox/VoiceSuggestionProvider;->setVoiceResultsFromIntentBundle(Landroid/os/Bundle;)V

    .line 67
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/ChromeAutocompleteController;->mVoiceSuggestionProvider:Lcom/google/android/apps/chrome/omnibox/VoiceSuggestionProvider;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omnibox/VoiceSuggestionProvider;->getResults()Ljava/util/List;

    move-result-object v0

    .line 68
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/omnibox/VoiceSuggestionProvider$VoiceResult;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public stop(Z)V
    .locals 1

    .prologue
    .line 37
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/ChromeAutocompleteController;->mVoiceSuggestionProvider:Lcom/google/android/apps/chrome/omnibox/VoiceSuggestionProvider;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omnibox/VoiceSuggestionProvider;->clearVoiceSearchResults()V

    .line 38
    :cond_0
    invoke-super {p0, p1}, Lorg/chromium/chrome/browser/omnibox/AutocompleteController;->stop(Z)V

    .line 39
    return-void
.end method
