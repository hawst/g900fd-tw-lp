.class public Lcom/google/android/apps/chrome/document/DocumentActivityDatabase;
.super Ljava/lang/Object;
.source "DocumentActivityDatabase.java"


# static fields
.field private static sInstance:Lcom/google/android/apps/chrome/document/DocumentActivityDatabase;

.field private static final sPendingDocumentData:Landroid/util/SparseArray;

.field private static sPrioritizedTabId:I


# instance fields
.field private final mDelegate:Lcom/google/android/apps/chrome/document/DocumentTabList$Delegate;

.field private mIncognitoTabList:Lcom/google/android/apps/chrome/document/DocumentTabList;

.field private final mRegularTabList:Lcom/google/android/apps/chrome/document/DocumentTabList;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 54
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    sput-object v0, Lcom/google/android/apps/chrome/document/DocumentActivityDatabase;->sPendingDocumentData:Landroid/util/SparseArray;

    .line 75
    const/4 v0, -0x1

    sput v0, Lcom/google/android/apps/chrome/document/DocumentActivityDatabase;->sPrioritizedTabId:I

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    .line 194
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 195
    invoke-direct {p0}, Lcom/google/android/apps/chrome/document/DocumentActivityDatabase;->createDelegate()Lcom/google/android/apps/chrome/document/DocumentTabList$Delegate;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivityDatabase;->mDelegate:Lcom/google/android/apps/chrome/document/DocumentTabList$Delegate;

    .line 196
    new-instance v0, Lcom/google/android/apps/chrome/document/DocumentTabList;

    iget-object v1, p0, Lcom/google/android/apps/chrome/document/DocumentActivityDatabase;->mDelegate:Lcom/google/android/apps/chrome/document/DocumentTabList$Delegate;

    const/4 v2, 0x0

    sget v3, Lcom/google/android/apps/chrome/document/DocumentActivityDatabase;->sPrioritizedTabId:I

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/chrome/document/DocumentTabList;-><init>(Lcom/google/android/apps/chrome/document/DocumentTabList$Delegate;ZI)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivityDatabase;->mRegularTabList:Lcom/google/android/apps/chrome/document/DocumentTabList;

    .line 197
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivityDatabase;->mDelegate:Lcom/google/android/apps/chrome/document/DocumentTabList$Delegate;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/google/android/apps/chrome/document/DocumentTabList$Delegate;->getTasksFromRecents(Z)Landroid/util/SparseArray;

    move-result-object v0

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-eqz v0, :cond_0

    .line 198
    invoke-direct {p0}, Lcom/google/android/apps/chrome/document/DocumentActivityDatabase;->createIncognitoTabList()V

    .line 200
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/document/DocumentActivityDatabase;->initializeTabIdCounter()V

    .line 201
    return-void
.end method

.method public static addPendingDocumentData(ILcom/google/android/apps/chrome/document/DocumentActivityDatabase$PendingDocumentData;)V
    .locals 1

    .prologue
    .line 161
    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    .line 162
    sget-object v0, Lcom/google/android/apps/chrome/document/DocumentActivityDatabase;->sPendingDocumentData:Landroid/util/SparseArray;

    invoke-virtual {v0, p0, p1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 163
    return-void
.end method

.method private createDelegate()Lcom/google/android/apps/chrome/document/DocumentTabList$Delegate;
    .locals 1

    .prologue
    .line 229
    new-instance v0, Lcom/google/android/apps/chrome/document/DocumentActivityDatabase$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/document/DocumentActivityDatabase$1;-><init>(Lcom/google/android/apps/chrome/document/DocumentActivityDatabase;)V

    return-object v0
.end method

.method public static createDocumentDataString(ILjava/lang/String;)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 151
    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    const-string/jumbo v1, "document"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-static {p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/net/Uri$Builder;->query(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method private createIncognitoTabList()V
    .locals 4

    .prologue
    .line 222
    new-instance v0, Lcom/google/android/apps/chrome/document/DocumentTabList;

    iget-object v1, p0, Lcom/google/android/apps/chrome/document/DocumentActivityDatabase;->mDelegate:Lcom/google/android/apps/chrome/document/DocumentTabList$Delegate;

    const/4 v2, 0x1

    sget v3, Lcom/google/android/apps/chrome/document/DocumentActivityDatabase;->sPrioritizedTabId:I

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/chrome/document/DocumentTabList;-><init>(Lcom/google/android/apps/chrome/document/DocumentTabList$Delegate;ZI)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivityDatabase;->mIncognitoTabList:Lcom/google/android/apps/chrome/document/DocumentTabList;

    .line 223
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivityDatabase;->mRegularTabList:Lcom/google/android/apps/chrome/document/DocumentTabList;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/document/DocumentTabList;->isNativeCreated()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 224
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivityDatabase;->mIncognitoTabList:Lcom/google/android/apps/chrome/document/DocumentTabList;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/document/DocumentTabList;->initializeNative()V

    .line 226
    :cond_0
    return-void
.end method

.method private static ensureInitialized()V
    .locals 1

    .prologue
    .line 102
    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    .line 103
    sget-object v0, Lcom/google/android/apps/chrome/document/DocumentActivityDatabase;->sInstance:Lcom/google/android/apps/chrome/document/DocumentActivityDatabase;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/chrome/document/DocumentActivityDatabase;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/document/DocumentActivityDatabase;-><init>()V

    sput-object v0, Lcom/google/android/apps/chrome/document/DocumentActivityDatabase;->sInstance:Lcom/google/android/apps/chrome/document/DocumentActivityDatabase;

    .line 104
    :cond_0
    return-void
.end method

.method public static generateValidTabId()I
    .locals 1

    .prologue
    .line 117
    invoke-static {}, Lcom/google/android/apps/chrome/document/DocumentActivityDatabase;->ensureInitialized()V

    .line 118
    const/4 v0, -0x1

    invoke-static {v0}, Lorg/chromium/chrome/browser/Tab;->generateValidId(I)I

    move-result v0

    return v0
.end method

.method private static getInstance()Lcom/google/android/apps/chrome/document/DocumentActivityDatabase;
    .locals 1

    .prologue
    .line 107
    invoke-static {}, Lcom/google/android/apps/chrome/document/DocumentActivityDatabase;->ensureInitialized()V

    .line 108
    sget-object v0, Lcom/google/android/apps/chrome/document/DocumentActivityDatabase;->sInstance:Lcom/google/android/apps/chrome/document/DocumentActivityDatabase;

    return-object v0
.end method

.method private getLargestTaskIdFromRecents()I
    .locals 3

    .prologue
    .line 309
    const/4 v1, -0x1

    .line 310
    invoke-static {}, Lorg/chromium/base/ApplicationStatus;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 311
    const-string/jumbo v2, "activity"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 313
    invoke-virtual {v0}, Landroid/app/ActivityManager;->getAppTasks()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager$AppTask;

    .line 314
    invoke-virtual {v0}, Landroid/app/ActivityManager$AppTask;->getTaskInfo()Landroid/app/ActivityManager$RecentTaskInfo;

    move-result-object v0

    iget v0, v0, Landroid/app/ActivityManager$RecentTaskInfo;->persistentId:I

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    move v1, v0

    .line 315
    goto :goto_0

    .line 316
    :cond_0
    return v1
.end method

.method private getMaxTabId(Lcom/google/android/apps/chrome/document/DocumentTabList;I)I
    .locals 3

    .prologue
    .line 213
    .line 214
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/document/DocumentTabList;->getCount()I

    move-result v1

    .line 215
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 216
    invoke-virtual {p1, v0}, Lcom/google/android/apps/chrome/document/DocumentTabList;->getTabIdAtIndex(I)I

    move-result v2

    invoke-static {p2, v2}, Ljava/lang/Math;->max(II)I

    move-result p2

    .line 215
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 218
    :cond_0
    return p2
.end method

.method public static getTabList(Z)Lcom/google/android/apps/chrome/document/DocumentTabList;
    .locals 2

    .prologue
    .line 135
    invoke-static {}, Lcom/google/android/apps/chrome/document/DocumentActivityDatabase;->getInstance()Lcom/google/android/apps/chrome/document/DocumentActivityDatabase;

    move-result-object v0

    .line 136
    if-nez p0, :cond_0

    iget-object v0, v0, Lcom/google/android/apps/chrome/document/DocumentActivityDatabase;->mRegularTabList:Lcom/google/android/apps/chrome/document/DocumentTabList;

    .line 140
    :goto_0
    return-object v0

    .line 137
    :cond_0
    iget-object v1, v0, Lcom/google/android/apps/chrome/document/DocumentActivityDatabase;->mIncognitoTabList:Lcom/google/android/apps/chrome/document/DocumentTabList;

    if-nez v1, :cond_1

    .line 138
    invoke-direct {v0}, Lcom/google/android/apps/chrome/document/DocumentActivityDatabase;->createIncognitoTabList()V

    .line 140
    :cond_1
    iget-object v0, v0, Lcom/google/android/apps/chrome/document/DocumentActivityDatabase;->mIncognitoTabList:Lcom/google/android/apps/chrome/document/DocumentTabList;

    goto :goto_0
.end method

.method private initializeTabIdCounter()V
    .locals 2

    .prologue
    .line 204
    invoke-direct {p0}, Lcom/google/android/apps/chrome/document/DocumentActivityDatabase;->getLargestTaskIdFromRecents()I

    move-result v0

    .line 205
    iget-object v1, p0, Lcom/google/android/apps/chrome/document/DocumentActivityDatabase;->mRegularTabList:Lcom/google/android/apps/chrome/document/DocumentTabList;

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/chrome/document/DocumentActivityDatabase;->getMaxTabId(Lcom/google/android/apps/chrome/document/DocumentTabList;I)I

    move-result v0

    .line 206
    iget-object v1, p0, Lcom/google/android/apps/chrome/document/DocumentActivityDatabase;->mIncognitoTabList:Lcom/google/android/apps/chrome/document/DocumentTabList;

    if-eqz v1, :cond_0

    .line 207
    iget-object v1, p0, Lcom/google/android/apps/chrome/document/DocumentActivityDatabase;->mIncognitoTabList:Lcom/google/android/apps/chrome/document/DocumentTabList;

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/chrome/document/DocumentActivityDatabase;->getMaxTabId(Lcom/google/android/apps/chrome/document/DocumentTabList;I)I

    move-result v0

    .line 209
    :cond_0
    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Lorg/chromium/chrome/browser/Tab;->incrementIdCounterTo(I)V

    .line 210
    return-void
.end method

.method public static onNativeLibraryReady()V
    .locals 2

    .prologue
    .line 189
    invoke-static {}, Lcom/google/android/apps/chrome/document/DocumentActivityDatabase;->getInstance()Lcom/google/android/apps/chrome/document/DocumentActivityDatabase;

    move-result-object v0

    .line 190
    iget-object v1, v0, Lcom/google/android/apps/chrome/document/DocumentActivityDatabase;->mRegularTabList:Lcom/google/android/apps/chrome/document/DocumentTabList;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/document/DocumentTabList;->initializeNative()V

    .line 191
    iget-object v1, v0, Lcom/google/android/apps/chrome/document/DocumentActivityDatabase;->mIncognitoTabList:Lcom/google/android/apps/chrome/document/DocumentTabList;

    if-eqz v1, :cond_0

    iget-object v0, v0, Lcom/google/android/apps/chrome/document/DocumentActivityDatabase;->mIncognitoTabList:Lcom/google/android/apps/chrome/document/DocumentTabList;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/document/DocumentTabList;->initializeNative()V

    .line 192
    :cond_0
    return-void
.end method

.method public static removePendingDocumentData(I)Lcom/google/android/apps/chrome/document/DocumentActivityDatabase$PendingDocumentData;
    .locals 2

    .prologue
    .line 169
    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    .line 170
    sget-object v0, Lcom/google/android/apps/chrome/document/DocumentActivityDatabase;->sPendingDocumentData:Landroid/util/SparseArray;

    invoke-virtual {v0, p0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/document/DocumentActivityDatabase$PendingDocumentData;

    .line 171
    sget-object v1, Lcom/google/android/apps/chrome/document/DocumentActivityDatabase;->sPendingDocumentData:Landroid/util/SparseArray;

    invoke-virtual {v1, p0}, Landroid/util/SparseArray;->remove(I)V

    .line 172
    return-object v0
.end method

.method public static setPrioritizedTabId(I)V
    .locals 0

    .prologue
    .line 126
    sput p0, Lcom/google/android/apps/chrome/document/DocumentActivityDatabase;->sPrioritizedTabId:I

    .line 127
    return-void
.end method

.method public static updateRecentlyClosed()V
    .locals 2

    .prologue
    .line 179
    invoke-static {}, Lcom/google/android/apps/chrome/document/DocumentActivityDatabase;->getInstance()Lcom/google/android/apps/chrome/document/DocumentActivityDatabase;

    move-result-object v0

    .line 180
    iget-object v1, v0, Lcom/google/android/apps/chrome/document/DocumentActivityDatabase;->mRegularTabList:Lcom/google/android/apps/chrome/document/DocumentTabList;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/document/DocumentTabList;->updateRecentlyClosed()V

    .line 181
    iget-object v1, v0, Lcom/google/android/apps/chrome/document/DocumentActivityDatabase;->mIncognitoTabList:Lcom/google/android/apps/chrome/document/DocumentTabList;

    if-eqz v1, :cond_0

    iget-object v0, v0, Lcom/google/android/apps/chrome/document/DocumentActivityDatabase;->mIncognitoTabList:Lcom/google/android/apps/chrome/document/DocumentTabList;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/document/DocumentTabList;->updateRecentlyClosed()V

    .line 182
    :cond_0
    return-void
.end method
