.class public Lcom/google/android/apps/chrome/preferences/website/SingleWebsitePreferences;
.super Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;
.source "SingleWebsitePreferences.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final EXTRA_SITE:Ljava/lang/String; = "com.google.android.apps.chrome.preferences.site"

.field public static final PREF_CLEAR_DATA:Ljava/lang/String; = "clear_data"

.field public static final PREF_LOCATION_ACCESS:Ljava/lang/String; = "location_access"

.field public static final PREF_MIDI_SYSEX_PERMISSION:Ljava/lang/String; = "midi_sysex_permission"

.field public static final PREF_POPUP_PERMISSION:Ljava/lang/String; = "popup_permission"

.field public static final PREF_PROTECTED_MEDIA_IDENTIFIER_PERMISSION:Ljava/lang/String; = "protected_media_identifier_permission"

.field public static final PREF_SITE_TITLE:Ljava/lang/String; = "site_title"

.field public static final PREF_VOICE_AND_VIDEO_CAPTURE_PERMISSION:Ljava/lang/String; = "voice_and_video_capture_permission"


# instance fields
.field private mSite:Lcom/google/android/apps/chrome/preferences/website/Website;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const-class v0, Lcom/google/android/apps/chrome/preferences/website/SingleWebsitePreferences;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/preferences/website/SingleWebsitePreferences;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/preferences/website/SingleWebsitePreferences;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/website/SingleWebsitePreferences;->popBackIfNoSettings()V

    return-void
.end method

.method private configureVoiceAndVideoPreference(Landroid/preference/CheckBoxPreference;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 98
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/SingleWebsitePreferences;->mSite:Lcom/google/android/apps/chrome/preferences/website/Website;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/website/Website;->getMediaAccessType()I

    move-result v0

    .line 99
    packed-switch v0, :pswitch_data_0

    .line 126
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/website/SingleWebsitePreferences;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 128
    :goto_0
    if-eq v0, v2, :cond_0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 131
    :cond_0
    invoke-virtual {p1, v2}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 133
    :cond_1
    return-void

    .line 102
    :pswitch_0
    sget v1, Lcom/google/android/apps/chrome/R$string;->website_settings_title_video:I

    invoke-virtual {p1, v1}, Landroid/preference/CheckBoxPreference;->setTitle(I)V

    .line 103
    sget v1, Lcom/google/android/apps/chrome/R$string;->website_settings_summary_text_for_allow_camera_access:I

    invoke-virtual {p1, v1}, Landroid/preference/CheckBoxPreference;->setSummaryOn(I)V

    .line 105
    sget v1, Lcom/google/android/apps/chrome/R$string;->website_settings_summary_text_for_deny_camera_access:I

    invoke-virtual {p1, v1}, Landroid/preference/CheckBoxPreference;->setSummaryOff(I)V

    goto :goto_0

    .line 110
    :pswitch_1
    sget v1, Lcom/google/android/apps/chrome/R$string;->website_settings_title_voice:I

    invoke-virtual {p1, v1}, Landroid/preference/CheckBoxPreference;->setTitle(I)V

    .line 111
    sget v1, Lcom/google/android/apps/chrome/R$string;->website_settings_summary_text_for_allow_mic_access:I

    invoke-virtual {p1, v1}, Landroid/preference/CheckBoxPreference;->setSummaryOn(I)V

    .line 113
    sget v1, Lcom/google/android/apps/chrome/R$string;->website_settings_summary_text_for_deny_mic_access:I

    invoke-virtual {p1, v1}, Landroid/preference/CheckBoxPreference;->setSummaryOff(I)V

    goto :goto_0

    .line 118
    :pswitch_2
    sget v1, Lcom/google/android/apps/chrome/R$string;->website_settings_title_voice_and_video:I

    invoke-virtual {p1, v1}, Landroid/preference/CheckBoxPreference;->setTitle(I)V

    .line 119
    sget v1, Lcom/google/android/apps/chrome/R$string;->website_settings_summary_text_for_allow_mic_and_camera_access:I

    invoke-virtual {p1, v1}, Landroid/preference/CheckBoxPreference;->setSummaryOn(I)V

    .line 121
    sget v1, Lcom/google/android/apps/chrome/R$string;->website_settings_summary_text_for_deny_mic_and_camera_access:I

    invoke-virtual {p1, v1}, Landroid/preference/CheckBoxPreference;->setSummaryOff(I)V

    goto :goto_0

    .line 99
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method private popBackIfNoSettings()V
    .locals 3

    .prologue
    .line 149
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/website/SingleWebsitePreferences;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0}, Landroid/preference/PreferenceScreen;->getPreferenceCount()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/website/SingleWebsitePreferences;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0}, Landroid/preference/PreferenceScreen;->getPreferenceCount()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    const-string/jumbo v0, "site_title"

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/website/SingleWebsitePreferences;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/preference/PreferenceScreen;->getPreference(I)Landroid/preference/Preference;

    move-result-object v1

    invoke-virtual {v1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 152
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/website/SingleWebsitePreferences;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceActivity;

    .line 153
    const/4 v1, -0x1

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    invoke-virtual {v0, p0, v1, v2}, Landroid/preference/PreferenceActivity;->finishPreferencePanel(Landroid/app/Fragment;ILandroid/content/Intent;)V

    .line 155
    :cond_1
    return-void
.end method

.method private setVoiceAndVideoCaptureSetting(Z)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 158
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/SingleWebsitePreferences;->mSite:Lcom/google/android/apps/chrome/preferences/website/Website;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/website/Website;->getMediaAccessType()I

    move-result v0

    .line 159
    if-eqz p1, :cond_1

    .line 160
    packed-switch v0, :pswitch_data_0

    .line 172
    sget-boolean v0, Lcom/google/android/apps/chrome/preferences/website/SingleWebsitePreferences;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 162
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/SingleWebsitePreferences;->mSite:Lcom/google/android/apps/chrome/preferences/website/Website;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/preferences/website/Website;->setVideoCaptureInfoAllowed(Ljava/lang/Boolean;)V

    .line 190
    :cond_0
    :goto_0
    return-void

    .line 165
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/SingleWebsitePreferences;->mSite:Lcom/google/android/apps/chrome/preferences/website/Website;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/preferences/website/Website;->setVoiceCaptureInfoAllowed(Ljava/lang/Boolean;)V

    goto :goto_0

    .line 168
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/SingleWebsitePreferences;->mSite:Lcom/google/android/apps/chrome/preferences/website/Website;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/preferences/website/Website;->setVideoCaptureInfoAllowed(Ljava/lang/Boolean;)V

    .line 169
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/SingleWebsitePreferences;->mSite:Lcom/google/android/apps/chrome/preferences/website/Website;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/preferences/website/Website;->setVoiceCaptureInfoAllowed(Ljava/lang/Boolean;)V

    goto :goto_0

    .line 175
    :cond_1
    packed-switch v0, :pswitch_data_1

    .line 187
    sget-boolean v0, Lcom/google/android/apps/chrome/preferences/website/SingleWebsitePreferences;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 177
    :pswitch_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/SingleWebsitePreferences;->mSite:Lcom/google/android/apps/chrome/preferences/website/Website;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/preferences/website/Website;->setVideoCaptureInfoAllowed(Ljava/lang/Boolean;)V

    goto :goto_0

    .line 180
    :pswitch_4
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/SingleWebsitePreferences;->mSite:Lcom/google/android/apps/chrome/preferences/website/Website;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/preferences/website/Website;->setVoiceCaptureInfoAllowed(Ljava/lang/Boolean;)V

    goto :goto_0

    .line 183
    :pswitch_5
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/SingleWebsitePreferences;->mSite:Lcom/google/android/apps/chrome/preferences/website/Website;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/preferences/website/Website;->setVideoCaptureInfoAllowed(Ljava/lang/Boolean;)V

    .line 184
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/SingleWebsitePreferences;->mSite:Lcom/google/android/apps/chrome/preferences/website/Website;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/preferences/website/Website;->setVoiceCaptureInfoAllowed(Ljava/lang/Boolean;)V

    goto :goto_0

    .line 160
    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch

    .line 175
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_5
        :pswitch_4
    .end packed-switch
.end method


# virtual methods
.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 43
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/website/SingleWebsitePreferences;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "com.google.android.apps.chrome.preferences.site"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/preferences/website/Website;

    iput-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/SingleWebsitePreferences;->mSite:Lcom/google/android/apps/chrome/preferences/website/Website;

    .line 44
    sget v0, Lcom/google/android/apps/chrome/R$xml;->single_website_settings_preferences:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/website/SingleWebsitePreferences;->addPreferencesFromResource(I)V

    .line 45
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/website/SingleWebsitePreferences;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0}, Landroid/preference/PreferenceScreen;->getRootAdapter()Landroid/widget/ListAdapter;

    move-result-object v3

    move v1, v2

    .line 46
    :goto_0
    invoke-interface {v3}, Landroid/widget/ListAdapter;->getCount()I

    move-result v0

    if-ge v1, v0, :cond_c

    .line 47
    invoke-interface {v3, v1}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/preference/Preference;

    .line 48
    const-string/jumbo v4, "site_title"

    invoke-virtual {v0}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 49
    iget-object v4, p0, Lcom/google/android/apps/chrome/preferences/website/SingleWebsitePreferences;->mSite:Lcom/google/android/apps/chrome/preferences/website/Website;

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/preferences/website/Website;->getTitle()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 46
    :cond_0
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 50
    :cond_1
    const-string/jumbo v4, "clear_data"

    invoke-virtual {v0}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 51
    iget-object v4, p0, Lcom/google/android/apps/chrome/preferences/website/SingleWebsitePreferences;->mSite:Lcom/google/android/apps/chrome/preferences/website/Website;

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/preferences/website/Website;->getTotalUsage()J

    move-result-wide v4

    .line 52
    const-wide/16 v6, 0x0

    cmp-long v6, v4, v6

    if-lez v6, :cond_2

    .line 53
    invoke-virtual {v0}, Landroid/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object v6

    .line 54
    sget v7, Lcom/google/android/apps/chrome/R$string;->origin_settings_storage_usage:I

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    invoke-static {v6, v4, v5}, Lcom/google/android/apps/chrome/preferences/website/WebsitePreference;->sizeValueToString(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v8, v2

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 57
    check-cast v0, Lcom/google/android/apps/chrome/preferences/website/ClearWebsiteStorage;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/chrome/preferences/website/ClearWebsiteStorage;->setConfirmationListener(Landroid/content/DialogInterface$OnClickListener;)V

    goto :goto_1

    .line 59
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/website/SingleWebsitePreferences;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v4

    invoke-virtual {v4, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_1

    .line 60
    :cond_3
    const-string/jumbo v4, "location_access"

    invoke-virtual {v0}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 61
    iget-object v4, p0, Lcom/google/android/apps/chrome/preferences/website/SingleWebsitePreferences;->mSite:Lcom/google/android/apps/chrome/preferences/website/Website;

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/preferences/website/Website;->isGeolocationAccessAllowed()Ljava/lang/Boolean;

    move-result-object v4

    .line 62
    if-eqz v4, :cond_4

    .line 63
    check-cast v0, Landroid/preference/CheckBoxPreference;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    invoke-virtual {v0, v4}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    goto :goto_1

    .line 65
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/website/SingleWebsitePreferences;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v4

    invoke-virtual {v4, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_1

    .line 66
    :cond_5
    const-string/jumbo v4, "midi_sysex_permission"

    invoke-virtual {v0}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 67
    iget-object v4, p0, Lcom/google/android/apps/chrome/preferences/website/SingleWebsitePreferences;->mSite:Lcom/google/android/apps/chrome/preferences/website/Website;

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/preferences/website/Website;->isMidiAccessAllowed()Ljava/lang/Boolean;

    move-result-object v4

    .line 68
    if-eqz v4, :cond_6

    .line 69
    check-cast v0, Landroid/preference/CheckBoxPreference;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    invoke-virtual {v0, v4}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    goto/16 :goto_1

    .line 71
    :cond_6
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/website/SingleWebsitePreferences;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v4

    invoke-virtual {v4, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto/16 :goto_1

    .line 72
    :cond_7
    const-string/jumbo v4, "popup_permission"

    invoke-virtual {v0}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 73
    iget-object v4, p0, Lcom/google/android/apps/chrome/preferences/website/SingleWebsitePreferences;->mSite:Lcom/google/android/apps/chrome/preferences/website/Website;

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/preferences/website/Website;->isPopupExceptionAllowed()Ljava/lang/Boolean;

    move-result-object v4

    .line 74
    if-eqz v4, :cond_8

    .line 75
    check-cast v0, Landroid/preference/CheckBoxPreference;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    invoke-virtual {v0, v4}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    goto/16 :goto_1

    .line 77
    :cond_8
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/website/SingleWebsitePreferences;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v4

    invoke-virtual {v4, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto/16 :goto_1

    .line 78
    :cond_9
    const-string/jumbo v4, "protected_media_identifier_permission"

    invoke-virtual {v0}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_b

    .line 79
    iget-object v4, p0, Lcom/google/android/apps/chrome/preferences/website/SingleWebsitePreferences;->mSite:Lcom/google/android/apps/chrome/preferences/website/Website;

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/preferences/website/Website;->isProtectedMediaIdentifierAccessAllowed()Ljava/lang/Boolean;

    move-result-object v4

    .line 80
    if-eqz v4, :cond_a

    .line 81
    check-cast v0, Landroid/preference/CheckBoxPreference;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    invoke-virtual {v0, v4}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    goto/16 :goto_1

    .line 83
    :cond_a
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/website/SingleWebsitePreferences;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v4

    invoke-virtual {v4, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto/16 :goto_1

    .line 84
    :cond_b
    const-string/jumbo v4, "voice_and_video_capture_permission"

    invoke-virtual {v0}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 85
    check-cast v0, Landroid/preference/CheckBoxPreference;

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/preferences/website/SingleWebsitePreferences;->configureVoiceAndVideoPreference(Landroid/preference/CheckBoxPreference;)V

    goto/16 :goto_1

    .line 88
    :cond_c
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 89
    return-void
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2

    .prologue
    .line 137
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/SingleWebsitePreferences;->mSite:Lcom/google/android/apps/chrome/preferences/website/Website;

    new-instance v1, Lcom/google/android/apps/chrome/preferences/website/SingleWebsitePreferences$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/preferences/website/SingleWebsitePreferences$1;-><init>(Lcom/google/android/apps/chrome/preferences/website/SingleWebsitePreferences;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/preferences/website/Website;->clearAllStoredData(Lcom/google/android/apps/chrome/preferences/website/Website$StoredDataClearedCallback;)V

    .line 146
    return-void
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 194
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;->onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z

    move-result v0

    .line 195
    const-string/jumbo v1, "location_access"

    invoke-virtual {p2}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 196
    check-cast p2, Landroid/preference/CheckBoxPreference;

    invoke-virtual {p2}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 197
    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/website/SingleWebsitePreferences;->mSite:Lcom/google/android/apps/chrome/preferences/website/Website;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/chrome/preferences/website/Website;->setGeolocationInfoAllowed(Ljava/lang/Boolean;)V

    .line 218
    :cond_0
    :goto_0
    return v0

    .line 199
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/website/SingleWebsitePreferences;->mSite:Lcom/google/android/apps/chrome/preferences/website/Website;

    invoke-virtual {v1, v4}, Lcom/google/android/apps/chrome/preferences/website/Website;->setGeolocationInfoAllowed(Ljava/lang/Boolean;)V

    goto :goto_0

    .line 200
    :cond_2
    const-string/jumbo v1, "midi_sysex_permission"

    invoke-virtual {p2}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 201
    check-cast p2, Landroid/preference/CheckBoxPreference;

    invoke-virtual {p2}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 202
    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/website/SingleWebsitePreferences;->mSite:Lcom/google/android/apps/chrome/preferences/website/Website;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/chrome/preferences/website/Website;->setMidiInfoAllowed(Ljava/lang/Boolean;)V

    goto :goto_0

    .line 204
    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/website/SingleWebsitePreferences;->mSite:Lcom/google/android/apps/chrome/preferences/website/Website;

    invoke-virtual {v1, v4}, Lcom/google/android/apps/chrome/preferences/website/Website;->setMidiInfoAllowed(Ljava/lang/Boolean;)V

    goto :goto_0

    .line 205
    :cond_4
    const-string/jumbo v1, "popup_permission"

    invoke-virtual {p2}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 206
    check-cast p2, Landroid/preference/CheckBoxPreference;

    invoke-virtual {p2}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 207
    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/website/SingleWebsitePreferences;->mSite:Lcom/google/android/apps/chrome/preferences/website/Website;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/chrome/preferences/website/Website;->setPopupExceptionInfoAllowed(Ljava/lang/Boolean;)V

    goto :goto_0

    .line 209
    :cond_5
    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/website/SingleWebsitePreferences;->mSite:Lcom/google/android/apps/chrome/preferences/website/Website;

    invoke-virtual {v1, v4}, Lcom/google/android/apps/chrome/preferences/website/Website;->setPopupExceptionInfoAllowed(Ljava/lang/Boolean;)V

    goto :goto_0

    .line 210
    :cond_6
    const-string/jumbo v1, "protected_media_identifier_permission"

    invoke-virtual {p2}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 211
    check-cast p2, Landroid/preference/CheckBoxPreference;

    invoke-virtual {p2}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 212
    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/website/SingleWebsitePreferences;->mSite:Lcom/google/android/apps/chrome/preferences/website/Website;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/chrome/preferences/website/Website;->setProtectedMediaIdentifierInfoAllowed(Ljava/lang/Boolean;)V

    goto :goto_0

    .line 214
    :cond_7
    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/website/SingleWebsitePreferences;->mSite:Lcom/google/android/apps/chrome/preferences/website/Website;

    invoke-virtual {v1, v4}, Lcom/google/android/apps/chrome/preferences/website/Website;->setProtectedMediaIdentifierInfoAllowed(Ljava/lang/Boolean;)V

    goto :goto_0

    .line 215
    :cond_8
    const-string/jumbo v1, "voice_and_video_capture_permission"

    invoke-virtual {p2}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 216
    check-cast p2, Landroid/preference/CheckBoxPreference;

    invoke-virtual {p2}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/preferences/website/SingleWebsitePreferences;->setVoiceAndVideoCaptureSetting(Z)V

    goto/16 :goto_0
.end method
