.class Lcom/google/android/apps/chrome/tab/TransitionPageHelper$3;
.super Ljava/lang/Object;
.source "TransitionPageHelper.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/tab/TransitionPageHelper;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/tab/TransitionPageHelper;)V
    .locals 0

    .prologue
    .line 245
    iput-object p1, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper$3;->this$0:Lcom/google/android/apps/chrome/tab/TransitionPageHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 248
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper$3;->this$0:Lcom/google/android/apps/chrome/tab/TransitionPageHelper;

    # setter for: Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mTransitionFinished:Z
    invoke-static {v0, v5}, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->access$402(Lcom/google/android/apps/chrome/tab/TransitionPageHelper;Z)Z

    .line 249
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper$3;->this$0:Lcom/google/android/apps/chrome/tab/TransitionPageHelper;

    # getter for: Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mTransitionContentViewCore:Lorg/chromium/content/browser/ContentViewCore;
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->access$000(Lcom/google/android/apps/chrome/tab/TransitionPageHelper;)Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->getWebContents()Lorg/chromium/content_public/browser/WebContents;

    move-result-object v0

    const-string/jumbo v1, "@-webkit-keyframes __transition_crossfade {from {background: %s; }%nto {background: %s; }}%n-webkit-animation: __transition_crossfade %dms forwards;"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper$3;->this$0:Lcom/google/android/apps/chrome/tab/TransitionPageHelper;

    # getter for: Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mSourceColor:Ljava/lang/String;
    invoke-static {v4}, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->access$500(Lcom/google/android/apps/chrome/tab/TransitionPageHelper;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    iget-object v3, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper$3;->this$0:Lcom/google/android/apps/chrome/tab/TransitionPageHelper;

    # getter for: Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mTransitionColor:Ljava/lang/String;
    invoke-static {v3}, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->access$600(Lcom/google/android/apps/chrome/tab/TransitionPageHelper;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    const/4 v3, 0x2

    const/16 v4, 0x12c

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/chromium/content_public/browser/WebContents;->insertCSS(Ljava/lang/String;)V

    .line 255
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper$3;->this$0:Lcom/google/android/apps/chrome/tab/TransitionPageHelper;

    # invokes: Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->scheduleTransitionFramesTimeout()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->access$700(Lcom/google/android/apps/chrome/tab/TransitionPageHelper;)V

    .line 256
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper$3;->this$0:Lcom/google/android/apps/chrome/tab/TransitionPageHelper;

    # invokes: Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->updateState()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->access$200(Lcom/google/android/apps/chrome/tab/TransitionPageHelper;)V

    .line 257
    return-void
.end method
