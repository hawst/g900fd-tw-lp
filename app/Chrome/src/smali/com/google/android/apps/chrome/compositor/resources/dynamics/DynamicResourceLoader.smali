.class public Lcom/google/android/apps/chrome/compositor/resources/dynamics/DynamicResourceLoader;
.super Ljava/lang/Object;
.source "DynamicResourceLoader.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final mCallback:Lcom/google/android/apps/chrome/compositor/resources/ResourceLoaderCallback;

.field private final mDynamicResources:Landroid/util/SparseArray;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    const-class v0, Lcom/google/android/apps/chrome/compositor/resources/dynamics/DynamicResourceLoader;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/compositor/resources/dynamics/DynamicResourceLoader;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/google/android/apps/chrome/compositor/resources/ResourceLoaderCallback;)V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/compositor/resources/dynamics/DynamicResourceLoader;->mDynamicResources:Landroid/util/SparseArray;

    .line 27
    iput-object p1, p0, Lcom/google/android/apps/chrome/compositor/resources/dynamics/DynamicResourceLoader;->mCallback:Lcom/google/android/apps/chrome/compositor/resources/ResourceLoaderCallback;

    .line 28
    return-void
.end method


# virtual methods
.method public loadResource(I)Z
    .locals 2

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/resources/dynamics/DynamicResourceLoader;->mDynamicResources:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/compositor/resources/dynamics/DynamicResource;

    .line 57
    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 60
    :goto_0
    return v0

    .line 59
    :cond_0
    invoke-interface {v0}, Lcom/google/android/apps/chrome/compositor/resources/dynamics/DynamicResource;->isDirty()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/chrome/compositor/resources/dynamics/DynamicResourceLoader;->mCallback:Lcom/google/android/apps/chrome/compositor/resources/ResourceLoaderCallback;

    invoke-interface {v1, p1, v0}, Lcom/google/android/apps/chrome/compositor/resources/ResourceLoaderCallback;->onResourceLoaded(ILcom/google/android/apps/chrome/compositor/resources/Resource;)V

    .line 60
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public registerResource(ILcom/google/android/apps/chrome/compositor/resources/dynamics/DynamicResource;)V
    .locals 1

    .prologue
    .line 36
    sget-boolean v0, Lcom/google/android/apps/chrome/compositor/resources/dynamics/DynamicResourceLoader;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/resources/dynamics/DynamicResourceLoader;->mDynamicResources:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 37
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/resources/dynamics/DynamicResourceLoader;->mDynamicResources:Landroid/util/SparseArray;

    invoke-virtual {v0, p1, p2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 38
    return-void
.end method

.method public unregisterResource(I)V
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/resources/dynamics/DynamicResourceLoader;->mDynamicResources:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->remove(I)V

    .line 46
    return-void
.end method
