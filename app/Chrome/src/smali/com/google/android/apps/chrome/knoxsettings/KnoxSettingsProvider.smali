.class public Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider;
.super Ljava/lang/Object;
.source "KnoxSettingsProvider.java"


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static sHasPermission:Ljava/lang/Boolean;

.field private static final sLock:Ljava/lang/Object;

.field private static sVerifiedSystemPackage:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 28
    const-class v0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider;->$assertionsDisabled:Z

    .line 37
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider;->sLock:Ljava/lang/Object;

    .line 63
    sput-object v1, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider;->sHasPermission:Ljava/lang/Boolean;

    .line 64
    sput-object v1, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider;->sVerifiedSystemPackage:Ljava/lang/Boolean;

    return-void

    .line 28
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    return-void
.end method

.method public static addObserver(Landroid/content/Context;Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider$Observer;)Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider$ObserverHandle;
    .locals 4

    .prologue
    .line 224
    sget-boolean v0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-static {}, Lorg/chromium/base/ThreadUtils;->runningOnUiThread()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 226
    :cond_0
    invoke-static {p0}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider;->verifyDevicePolicyProvider(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 227
    const/4 v0, 0x0

    .line 305
    :goto_0
    return-object v0

    .line 230
    :cond_1
    new-instance v1, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider$1;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    invoke-direct {v1, v0, p1}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider$1;-><init>(Landroid/os/Handler;Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider$Observer;)V

    .line 303
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider;->getContentProviderBaseUri()Landroid/net/Uri;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3, v1}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 305
    new-instance v0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider$ObserverHandle;

    invoke-direct {v0, v1}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider$ObserverHandle;-><init>(Landroid/database/ContentObserver;)V

    goto :goto_0
.end method

.method public static getAuditLogEnabled(Landroid/content/Context;)Z
    .locals 3

    .prologue
    .line 220
    const-string/jumbo v0, "AuditLog"

    const-string/jumbo v1, "isAuditLogEnabled"

    const/4 v2, 0x0

    invoke-static {p0, v0, v1, v2}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider;->readBoolean(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static getAutofillEnabled(Landroid/content/Context;)Z
    .locals 3

    .prologue
    .line 122
    const-string/jumbo v0, "BrowserPolicy"

    const-string/jumbo v1, "getAutoFillSetting"

    const/4 v2, 0x1

    invoke-static {p0, v0, v1, v2}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider;->readBoolean(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static getClientCertificateAlias(Landroid/content/Context;Ljava/lang/String;I)Ljava/lang/String;
    .locals 5

    .prologue
    .line 142
    const-string/jumbo v0, "SmartCardBrowserPolicy"

    const-string/jumbo v1, "getClientCertificateAlias"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {p0, v0, v1, v2}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider;->readString(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static getContentProviderBaseUri()Landroid/net/Uri;
    .locals 2

    .prologue
    .line 339
    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    const-string/jumbo v1, "content"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string/jumbo v1, "com.sec.knox.provider"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method private static getContentProviderUri(Ljava/lang/String;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 346
    invoke-static {}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider;->getContentProviderBaseUri()Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0, p0}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static getCookiesEnabled(Landroid/content/Context;)Z
    .locals 3

    .prologue
    .line 126
    const-string/jumbo v0, "BrowserPolicy"

    const-string/jumbo v1, "getCookiesSetting"

    const/4 v2, 0x1

    invoke-static {p0, v0, v1, v2}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider;->readBoolean(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method private static getCursor(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 356
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {p1}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider;->getContentProviderUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, p2

    move-object v4, p3

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public static getHttpProxyHostPort(Landroid/content/Context;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 118
    const-string/jumbo v0, "BrowserPolicy"

    const-string/jumbo v1, "getHttpProxy"

    const/4 v2, 0x0

    invoke-static {p0, v0, v1, v2}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider;->readString(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getJavascriptEnabled(Landroid/content/Context;)Z
    .locals 3

    .prologue
    .line 130
    const-string/jumbo v0, "BrowserPolicy"

    const-string/jumbo v1, "getJavaScriptSetting"

    const/4 v2, 0x1

    invoke-static {p0, v0, v1, v2}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider;->readBoolean(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static getPopupsEnabled(Landroid/content/Context;)Z
    .locals 3

    .prologue
    .line 134
    const-string/jumbo v0, "BrowserPolicy"

    const-string/jumbo v1, "getPopupsSetting"

    const/4 v2, 0x1

    invoke-static {p0, v0, v1, v2}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider;->readBoolean(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static getSmartCardAuthenticationEnabled(Landroid/content/Context;)Z
    .locals 3

    .prologue
    .line 138
    const-string/jumbo v0, "SmartCardBrowserPolicy"

    const-string/jumbo v1, "isAuthenticationEnabled"

    const/4 v2, 0x0

    invoke-static {p0, v0, v1, v2}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider;->readBoolean(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static getUrlBlacklist(Landroid/content/Context;)Ljava/util/List;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 147
    invoke-static {p0}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider;->verifyDevicePolicyProvider(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 148
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 173
    :cond_0
    :goto_0
    return-object v0

    .line 152
    :cond_1
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    .line 154
    :try_start_0
    const-string/jumbo v2, "FirewallPolicy"

    const-string/jumbo v3, "getURLFilterList"

    const/4 v4, 0x0

    invoke-static {p0, v2, v3, v4}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider;->getCursor(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 155
    if-eqz v1, :cond_2

    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-nez v2, :cond_3

    .line 160
    :cond_2
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 168
    if-eqz v1, :cond_0

    .line 169
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 163
    :cond_3
    :goto_1
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v2

    if-nez v2, :cond_5

    .line 164
    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 165
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 168
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_4

    .line 169
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0

    .line 168
    :cond_5
    if-eqz v1, :cond_0

    .line 169
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method

.method public static getUrlFilterEnabled(Landroid/content/Context;)Z
    .locals 3

    .prologue
    .line 177
    const-string/jumbo v0, "FirewallPolicy"

    const-string/jumbo v1, "getURLFilterEnabled"

    const/4 v2, 0x0

    invoke-static {p0, v0, v1, v2}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider;->readBoolean(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static getUrlFilterReportEnabled(Landroid/content/Context;)Z
    .locals 3

    .prologue
    .line 181
    const-string/jumbo v0, "FirewallPolicy"

    const-string/jumbo v1, "getURLFilterReportEnabled"

    const/4 v2, 0x0

    invoke-static {p0, v0, v1, v2}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider;->readBoolean(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static notifyCertificateFailure(Landroid/content/Context;I)V
    .locals 3

    .prologue
    .line 200
    invoke-static {p0}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider;->verifyDevicePolicyProvider(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 217
    :goto_0
    return-void

    .line 204
    :cond_0
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 205
    const-string/jumbo v1, "API"

    const-string/jumbo v2, "notifyCertificateFailure"

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 206
    const-string/jumbo v1, "module"

    const-string/jumbo v2, "Chrome_module"

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 207
    const-string/jumbo v1, "fail"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 213
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "CertificatePolicy"

    invoke-static {v2}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider;->getContentProviderUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 217
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private static readBoolean(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)Z
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 362
    invoke-static {p0}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider;->verifyDevicePolicyProvider(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 396
    :cond_0
    :goto_0
    return p3

    .line 368
    :cond_1
    const/4 v3, 0x0

    :try_start_0
    invoke-static {p0, p1, p2, v3}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider;->getCursor(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 369
    if-eqz v2, :cond_2

    :try_start_1
    invoke-interface {v2}, Landroid/database/Cursor;->getColumnCount()I

    move-result v3

    if-ne v3, v0, :cond_2

    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v3

    if-nez v3, :cond_3

    .line 374
    :cond_2
    if-eqz v2, :cond_0

    .line 396
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 376
    :cond_3
    const/4 v3, 0x0

    :try_start_2
    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 377
    const-string/jumbo v4, "true"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result v4

    if-eqz v4, :cond_5

    .line 378
    if-eqz v2, :cond_4

    .line 396
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_4
    move p3, v0

    goto :goto_0

    .line 380
    :cond_5
    :try_start_3
    const-string/jumbo v4, "false"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result v4

    if-eqz v4, :cond_7

    .line 381
    if-eqz v2, :cond_6

    .line 396
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_6
    move p3, v1

    goto :goto_0

    .line 383
    :cond_7
    const/4 v4, 0x0

    :try_start_4
    invoke-interface {v2, v4}, Landroid/database/Cursor;->getInt(I)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move-result v4

    .line 384
    if-ne v4, v0, :cond_9

    .line 385
    if-eqz v2, :cond_8

    .line 396
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_8
    move p3, v0

    goto :goto_0

    .line 387
    :cond_9
    if-nez v4, :cond_b

    .line 388
    if-eqz v2, :cond_a

    .line 396
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_a
    move p3, v1

    goto :goto_0

    .line 390
    :cond_b
    :try_start_5
    const-string/jumbo v0, "KnoxSettingsProvider"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "Unexpected result when reading "

    invoke-direct {v1, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v5, " from "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider;->getContentProviderUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v5, ": \""

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v3, "\" (int "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v3, ")"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 393
    if-eqz v2, :cond_0

    .line 396
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 395
    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_1
    if-eqz v1, :cond_c

    .line 396
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_c
    throw v0

    .line 395
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_1
.end method

.method private static readString(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 403
    invoke-static {p0}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider;->verifyDevicePolicyProvider(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 421
    :cond_0
    :goto_0
    return-object v0

    .line 409
    :cond_1
    :try_start_0
    invoke-static {p0, p1, p2, p3}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider;->getCursor(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 410
    if-eqz v1, :cond_2

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->getColumnCount()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_2

    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v2

    if-nez v2, :cond_3

    .line 416
    :cond_2
    if-eqz v1, :cond_0

    .line 421
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 418
    :cond_3
    const/4 v0, 0x0

    :try_start_2
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v0

    .line 420
    if-eqz v1, :cond_0

    .line 421
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 420
    :catchall_0
    move-exception v1

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    :goto_1
    if-eqz v1, :cond_4

    .line 421
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0

    .line 420
    :catchall_1
    move-exception v0

    goto :goto_1
.end method

.method public static removeObserver(Landroid/content/Context;Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider$ObserverHandle;)V
    .locals 2

    .prologue
    .line 309
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider$ObserverHandle;->getContentObserver()Landroid/database/ContentObserver;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 310
    return-void
.end method

.method public static saveToAuditLog(Landroid/content/Context;Ljava/lang/String;JIIZILjava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 314
    invoke-static {p0}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider;->verifyDevicePolicyProvider(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 336
    :goto_0
    return-void

    .line 318
    :cond_0
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 319
    const-string/jumbo v1, "AuditEvent"

    invoke-virtual {v0, v1, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 320
    const-string/jumbo v1, "timestamp"

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 321
    const-string/jumbo v1, "severity"

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 322
    const-string/jumbo v1, "group"

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 323
    const-string/jumbo v1, "outcome"

    invoke-static {p6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 324
    const-string/jumbo v1, "uid"

    invoke-static {p7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 325
    const-string/jumbo v1, "component"

    invoke-virtual {v0, v1, p8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 326
    const-string/jumbo v1, "message"

    invoke-virtual {v0, v1, p9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 332
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "AuditLog"

    invoke-static {v2}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider;->getContentProviderUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 333
    :catch_0
    move-exception v0

    .line 334
    const-string/jumbo v1, "KnoxSettingsProvider"

    const-string/jumbo v2, "Error inserting audit log"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public static saveURLBlockedReport(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 185
    invoke-static {p0}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider;->verifyDevicePolicyProvider(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 197
    :goto_0
    return-void

    .line 189
    :cond_0
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 190
    const-string/jumbo v1, "API"

    const-string/jumbo v2, "saveURLBlockedReport"

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    const-string/jumbo v1, "url"

    invoke-virtual {v0, v1, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 196
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "FirewallPolicy"

    invoke-static {v2}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider;->getContentProviderUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    goto :goto_0
.end method

.method private static verifyDevicePolicyProvider(Landroid/content/Context;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 427
    sget-object v3, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider;->sLock:Ljava/lang/Object;

    monitor-enter v3

    .line 428
    :try_start_0
    sget-object v2, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider;->sHasPermission:Ljava/lang/Boolean;

    if-nez v2, :cond_0

    .line 429
    const-string/jumbo v2, "com.sec.enterprise.knox.MDM_CONTENT_PROVIDER"

    invoke-virtual {p0, v2}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_2

    move v2, v1

    :goto_0
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 432
    sput-object v2, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider;->sHasPermission:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_0

    .line 433
    const-string/jumbo v2, "KnoxSettingsProvider"

    const-string/jumbo v4, "Permission to read device policy denied."

    invoke-static {v2, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 437
    :cond_0
    sget-object v2, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider;->sVerifiedSystemPackage:Ljava/lang/Boolean;

    if-nez v2, :cond_4

    .line 438
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    sput-object v2, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider;->sVerifiedSystemPackage:Ljava/lang/Boolean;

    .line 439
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 440
    const-string/jumbo v4, "com.sec.knox.provider"

    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Landroid/content/pm/PackageManager;->resolveContentProvider(Ljava/lang/String;I)Landroid/content/pm/ProviderInfo;

    move-result-object v4

    .line 442
    if-nez v4, :cond_3

    .line 443
    const-string/jumbo v1, "KnoxSettingsProvider"

    const-string/jumbo v2, "Device policy content provider not found."

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 444
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 464
    :cond_1
    :goto_1
    return v0

    :cond_2
    move v2, v0

    .line 429
    goto :goto_0

    .line 448
    :cond_3
    :try_start_1
    iget-object v4, v4, Landroid/content/pm/ProviderInfo;->packageName:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v2

    .line 449
    if-eqz v2, :cond_5

    iget v2, v2, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_5

    move v2, v1

    :goto_2
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    sput-object v2, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider;->sVerifiedSystemPackage:Ljava/lang/Boolean;
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 458
    :try_start_2
    sget-object v2, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider;->sVerifiedSystemPackage:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_4

    .line 459
    const-string/jumbo v2, "KnoxSettingsProvider"

    const-string/jumbo v4, "Unable to verify content provider is in the system image."

    invoke-static {v2, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 462
    :cond_4
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 464
    sget-object v2, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider;->sHasPermission:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_1

    sget-object v2, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider;->sVerifiedSystemPackage:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_1

    move v0, v1

    goto :goto_1

    :cond_5
    move v2, v0

    .line 449
    goto :goto_2

    .line 452
    :catch_0
    move-exception v1

    :try_start_3
    const-string/jumbo v1, "KnoxSettingsProvider"

    const-string/jumbo v2, "Unable to resolve application info for policy content provider"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 454
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 462
    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0
.end method
