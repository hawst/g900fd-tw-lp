.class public Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;
.super Landroid/widget/FrameLayout;
.source "TabStrip.java"


# static fields
.field public static final FADE_DURATION_MS:I = 0x4b

.field private static final NOTIFICATIONS:[I

.field private static final TAN_OF_TILT_FOR_DOWN_DRAG:F


# instance fields
.field private mCurrentNewTabAnimation:Landroid/animation/ObjectAnimator;

.field private mCurrentTabWidth:F

.field private mCurrentlyFaded:Z

.field private mCurrentlyScrollingForFastExpand:Z

.field private mCurrentlySelectedView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

.field private final mDownDragStartThresholdDip:F

.field private final mDragEndDip:F

.field private final mDragSpeed:F

.field private final mDragStartDip:F

.field private mDraggedXOffset:F

.field private mFadeView:Landroid/view/View;

.field private mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;

.field private mFullscreenToken:I

.field private final mGesture:Landroid/view/GestureDetector;

.field private final mGestureDetectorListener:Landroid/view/GestureDetector$SimpleOnGestureListener;

.field private final mInternalHandler:Landroid/os/Handler;

.field private mKeyboardHider:Lcom/google/android/apps/chrome/KeyboardHider;

.field private mLastDragScrollTimeMs:J

.field private mLastXDragPosition:F

.field private final mMaxTabWidth:F

.field private mMinScrollOffset:F

.field private final mMinTabWidth:F

.field private mNewTabButton:Landroid/widget/ImageButton;

.field private final mNewTabButtonOverlap:F

.field private final mNewTabButtonSpace:F

.field private final mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

.field private final mObservers:Lorg/chromium/base/ObserverList;

.field private mPotentialDragTabView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

.field private mPressedTabView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

.field private mRefreshTabVisualsDueToResize:Z

.field private mScrollFlags:I

.field private mScrollOffset:F

.field private final mScroller:Lcom/google/android/apps/chrome/third_party/OverScroller;

.field private mSelectedTabViewToDrag:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

.field private mStrokeView:Landroid/view/View;

.field private mTabContentManager:Lcom/google/android/apps/chrome/compositor/TabContentManager;

.field private mTabCreator:Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;

.field private final mTabEndStackWidth:F

.field private final mTabHeight:F

.field private mTabModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

.field private final mTabOverlap:F

.field private final mTabViewHandler:Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabViewHandler;

.field private final mTabViews:Ljava/util/List;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 86
    const-wide v0, 0x3fe921fb54442d18L    # 0.7853981633974483

    invoke-static {v0, v1}, Ljava/lang/Math;->tan(D)D

    move-result-wide v0

    double-to-float v0, v0

    sput v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->TAN_OF_TILT_FOR_DOWN_DRAG:F

    .line 1287
    const/16 v0, 0xd

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->NOTIFICATIONS:[I

    return-void

    :array_0
    .array-data 4
        0x2
        0x3
        0x5
        0x1a
        0x1b
        0xa
        0x11
        0x12
        0x14
        0x15
        0x8
        0x9
        0x1c
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 171
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 95
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabViews:Ljava/util/List;

    .line 98
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mFullscreenToken:I

    .line 105
    iput-object v4, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mPressedTabView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    .line 107
    iput-object v4, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mPotentialDragTabView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    .line 108
    iput-object v4, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mSelectedTabViewToDrag:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    .line 109
    iput v2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mDraggedXOffset:F

    .line 111
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mLastDragScrollTimeMs:J

    .line 112
    iput v3, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScrollFlags:I

    .line 118
    iput-boolean v3, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentlyScrollingForFastExpand:Z

    .line 119
    iput-boolean v3, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mRefreshTabVisualsDueToResize:Z

    .line 120
    iput-boolean v3, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentlyFaded:Z

    .line 123
    iput v2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScrollOffset:F

    .line 125
    iput v2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mMinScrollOffset:F

    .line 126
    iput v2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentTabWidth:F

    .line 145
    new-instance v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;

    invoke-direct {v0, p0, v4}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabViewHandlerImpl;-><init>(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$1;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabViewHandler:Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabViewHandler;

    .line 146
    new-instance v0, Lorg/chromium/base/ObserverList;

    invoke-direct {v0}, Lorg/chromium/base/ObserverList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mObservers:Lorg/chromium/base/ObserverList;

    .line 1303
    new-instance v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$3;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$3;-><init>(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    .line 1367
    new-instance v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$4;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$4;-><init>(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mInternalHandler:Landroid/os/Handler;

    .line 1381
    new-instance v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$5;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$5;-><init>(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mGestureDetectorListener:Landroid/view/GestureDetector$SimpleOnGestureListener;

    .line 173
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    .line 174
    const/high16 v1, 0x40800000    # 4.0f

    mul-float/2addr v1, v0

    iput v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabEndStackWidth:F

    .line 175
    const/high16 v1, 0x41b00000    # 22.0f

    mul-float/2addr v1, v0

    iput v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabOverlap:F

    .line 176
    const/high16 v1, 0x423c0000    # 47.0f

    mul-float/2addr v1, v0

    iput v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mNewTabButtonSpace:F

    .line 177
    const/high16 v1, 0x43870000    # 270.0f

    mul-float/2addr v1, v0

    iput v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mMaxTabWidth:F

    .line 178
    const/high16 v1, 0x43480000    # 200.0f

    mul-float/2addr v1, v0

    iput v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mMinTabWidth:F

    .line 179
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/chrome/R$dimen;->tab_strip_height:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    iput v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabHeight:F

    .line 180
    const v1, 0x42aecccd    # 87.4f

    mul-float/2addr v1, v0

    iput v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mDragStartDip:F

    .line 181
    const v1, 0x41933333    # 18.4f

    mul-float/2addr v1, v0

    iput v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mDragEndDip:F

    .line 182
    const/high16 v1, 0x447a0000    # 1000.0f

    mul-float/2addr v1, v0

    iput v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mDragSpeed:F

    .line 183
    const/high16 v1, 0x41600000    # 14.0f

    mul-float/2addr v1, v0

    iput v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mNewTabButtonOverlap:F

    .line 184
    const/high16 v1, 0x42480000    # 50.0f

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mDownDragStartThresholdDip:F

    .line 186
    iget v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mMaxTabWidth:F

    iput v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentTabWidth:F

    .line 188
    new-instance v0, Lcom/google/android/apps/chrome/third_party/OverScroller;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/chrome/third_party/OverScroller;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScroller:Lcom/google/android/apps/chrome/third_party/OverScroller;

    .line 200
    new-instance v0, Landroid/view/GestureDetector;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mGestureDetectorListener:Landroid/view/GestureDetector$SimpleOnGestureListener;

    invoke-direct {v0, v1, v2}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mGesture:Landroid/view/GestureDetector;

    .line 201
    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)Landroid/widget/ImageButton;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mNewTabButton:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)F
    .locals 1

    .prologue
    .line 53
    iget v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mMinScrollOffset:F

    return v0
.end method

.method static synthetic access$1100(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)Ljava/util/List;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabViews:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)Lorg/chromium/chrome/browser/tabmodel/TabModel;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)Lcom/google/android/apps/chrome/widget/tabstrip/TabView;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentlySelectedView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;IZ)V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->tabSelected(IZ)V

    return-void
.end method

.method static synthetic access$1500(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;IZ)V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->updateCurrentTabWidth(IZ)V

    return-void
.end method

.method static synthetic access$1600(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;Z)V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->resetResizeTimeout(Z)V

    return-void
.end method

.method static synthetic access$1700(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)F
    .locals 1

    .prologue
    .line 53
    iget v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentTabWidth:F

    return v0
.end method

.method static synthetic access$1800(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)F
    .locals 1

    .prologue
    .line 53
    iget v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabOverlap:F

    return v0
.end method

.method static synthetic access$1900(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)F
    .locals 1

    .prologue
    .line 53
    iget v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mNewTabButtonSpace:F

    return v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabCreator:Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)Lcom/google/android/apps/chrome/third_party/OverScroller;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScroller:Lcom/google/android/apps/chrome/third_party/OverScroller;

    return-object v0
.end method

.method static synthetic access$2100(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;IZ)V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->tabCreated(IZ)V

    return-void
.end method

.method static synthetic access$2200(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;I)V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->tabClosed(I)V

    return-void
.end method

.method static synthetic access$2300(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;I)V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->tabLoadStarted(I)V

    return-void
.end method

.method static synthetic access$2400(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;I)V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->tabLoadFinished(I)V

    return-void
.end method

.method static synthetic access$2500(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->tabTitleChanged(ILjava/lang/String;)V

    return-void
.end method

.method static synthetic access$2600(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;Z)V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->setBackgroundTabFading(Z)V

    return-void
.end method

.method static synthetic access$2700(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;IIIZ)V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->tabMoved(IIIZ)V

    return-void
.end method

.method static synthetic access$2800(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;I)V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->tabFaviconChanged(I)V

    return-void
.end method

.method static synthetic access$2900(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;I)Lcom/google/android/apps/chrome/widget/tabstrip/TabView;
    .locals 1

    .prologue
    .line 53
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->findTabViewById(I)Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)Lcom/google/android/apps/chrome/KeyboardHider;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mKeyboardHider:Lcom/google/android/apps/chrome/KeyboardHider;

    return-object v0
.end method

.method static synthetic access$3000(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;Lcom/google/android/apps/chrome/widget/tabstrip/TabView;ZZZ)F
    .locals 1

    .prologue
    .line 53
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->calculateOffsetToMakeTabVisible(Lcom/google/android/apps/chrome/widget/tabstrip/TabView;ZZZ)F

    move-result v0

    return v0
.end method

.method static synthetic access$3100(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;I)V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->startFastExpand(I)V

    return-void
.end method

.method static synthetic access$3200(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;I)V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->tabPageLoadStarted(I)V

    return-void
.end method

.method static synthetic access$3300(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;I)V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->tabPageLoadFinished(I)V

    return-void
.end method

.method static synthetic access$3400(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)Lcom/google/android/apps/chrome/widget/tabstrip/TabView;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mSelectedTabViewToDrag:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    return-object v0
.end method

.method static synthetic access$3500(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;FZZ)V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->setScrollOffsetPosition(FZZ)V

    return-void
.end method

.method static synthetic access$3600(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)F
    .locals 1

    .prologue
    .line 53
    iget v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mDownDragStartThresholdDip:F

    return v0
.end method

.method static synthetic access$3700()F
    .locals 1

    .prologue
    .line 53
    sget v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->TAN_OF_TILT_FOR_DOWN_DRAG:F

    return v0
.end method

.method static synthetic access$3800(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;F)V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->startDrag(F)V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;Z)V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->updateTabStripPositions(Z)V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;Z)V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->finishAnimationsForFullscreen(Z)V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)Lcom/google/android/apps/chrome/widget/tabstrip/TabView;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mPressedTabView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    return-object v0
.end method

.method static synthetic access$602(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;Lcom/google/android/apps/chrome/widget/tabstrip/TabView;)Lcom/google/android/apps/chrome/widget/tabstrip/TabView;
    .locals 0

    .prologue
    .line 53
    iput-object p1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mPressedTabView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    return-object p1
.end method

.method static synthetic access$702(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;Lcom/google/android/apps/chrome/widget/tabstrip/TabView;)Lcom/google/android/apps/chrome/widget/tabstrip/TabView;
    .locals 0

    .prologue
    .line 53
    iput-object p1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mPotentialDragTabView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    return-object p1
.end method

.method static synthetic access$800(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)Z
    .locals 1

    .prologue
    .line 53
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentlyScrollingForFastExpand:Z

    return v0
.end method

.method static synthetic access$802(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;Z)Z
    .locals 0

    .prologue
    .line 53
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentlyScrollingForFastExpand:Z

    return p1
.end method

.method static synthetic access$900(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)F
    .locals 1

    .prologue
    .line 53
    iget v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScrollOffset:F

    return v0
.end method

.method private calculateOffsetToMakeTabVisible(Lcom/google/android/apps/chrome/widget/tabstrip/TabView;ZZZ)F
    .locals 9

    .prologue
    const/4 v3, 0x0

    .line 1137
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentlySelectedView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    if-ne p1, v0, :cond_1

    if-nez p2, :cond_1

    :cond_0
    move v0, v3

    .line 1169
    :goto_0
    return v0

    .line 1139
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->getTabId()I

    move-result v1

    invoke-static {v0, v1}, Lorg/chromium/chrome/browser/tabmodel/TabModelUtils;->getTabIndexById(Lorg/chromium/chrome/browser/tabmodel/TabList;I)I

    move-result v4

    .line 1140
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentlySelectedView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentlySelectedView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->getTabId()I

    move-result v1

    invoke-static {v0, v1}, Lorg/chromium/chrome/browser/tabmodel/TabModelUtils;->getTabIndexById(Lorg/chromium/chrome/browser/tabmodel/TabList;I)I

    move-result v0

    .line 1148
    :goto_1
    neg-int v1, v4

    int-to-float v1, v1

    iget v2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentTabWidth:F

    iget v5, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabOverlap:F

    sub-float/2addr v2, v5

    mul-float/2addr v2, v1

    .line 1151
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->getWidth()I

    move-result v1

    int-to-float v1, v1

    iget v5, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mNewTabButtonSpace:F

    sub-float/2addr v1, v5

    add-int/lit8 v5, v4, 0x1

    int-to-float v5, v5

    iget v6, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentTabWidth:F

    iget v7, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabOverlap:F

    sub-float/2addr v6, v7

    mul-float/2addr v5, v6

    sub-float/2addr v1, v5

    .line 1154
    if-ge v4, v0, :cond_3

    .line 1155
    iget v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentTabWidth:F

    iget v4, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabOverlap:F

    sub-float/2addr v0, v4

    sub-float v0, v1, v0

    move v1, v2

    .line 1162
    :goto_2
    iget v2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScrollOffset:F

    cmpg-float v2, v2, v1

    if-gez v2, :cond_4

    if-eqz p3, :cond_4

    .line 1163
    iget v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScrollOffset:F

    sub-float v0, v1, v0

    goto :goto_0

    .line 1140
    :cond_2
    const/4 v0, -0x1

    goto :goto_1

    .line 1156
    :cond_3
    if-le v4, v0, :cond_6

    .line 1157
    iget v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentTabWidth:F

    iget v4, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabOverlap:F

    sub-float/2addr v0, v4

    add-float/2addr v0, v2

    move v8, v1

    move v1, v0

    move v0, v8

    goto :goto_2

    .line 1164
    :cond_4
    iget v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScrollOffset:F

    cmpl-float v1, v1, v0

    if-lez v1, :cond_5

    if-eqz p4, :cond_5

    .line 1165
    iget v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScrollOffset:F

    sub-float/2addr v0, v1

    goto :goto_0

    :cond_5
    move v0, v3

    .line 1169
    goto :goto_0

    :cond_6
    move v0, v1

    move v1, v2

    goto :goto_2
.end method

.method private checkDragPosition()Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 352
    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabViews:Ljava/util/List;

    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mSelectedTabViewToDrag:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    invoke-interface {v1, v2}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    .line 353
    const v2, 0x3f07ae14    # 0.53f

    iget v3, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentTabWidth:F

    iget v4, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabOverlap:F

    sub-float/2addr v3, v4

    mul-float/2addr v2, v3

    .line 356
    iget v3, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mDraggedXOffset:F

    cmpl-float v3, v3, v2

    if-lez v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabViews:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-ge v1, v3, :cond_0

    .line 357
    iget v2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mDraggedXOffset:F

    iget v3, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentTabWidth:F

    iget v4, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabOverlap:F

    sub-float/2addr v3, v4

    sub-float/2addr v2, v3

    iput v2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mDraggedXOffset:F

    .line 358
    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mSelectedTabViewToDrag:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->getTabId()I

    move-result v2

    add-int/lit8 v3, v1, 0x2

    invoke-direct {p0, v2, v1, v3, v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->tabMoved(IIIZ)V

    .line 359
    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    iget-object v3, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mSelectedTabViewToDrag:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->getTabId()I

    move-result v3

    add-int/lit8 v1, v1, 0x2

    invoke-interface {v2, v3, v1}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->moveTab(II)V

    .line 367
    :goto_0
    return v0

    .line 361
    :cond_0
    iget v3, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mDraggedXOffset:F

    neg-float v2, v2

    cmpg-float v2, v3, v2

    if-gez v2, :cond_1

    if-lez v1, :cond_1

    .line 362
    iget v2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mDraggedXOffset:F

    iget v3, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentTabWidth:F

    iget v4, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabOverlap:F

    sub-float/2addr v3, v4

    add-float/2addr v2, v3

    iput v2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mDraggedXOffset:F

    .line 363
    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mSelectedTabViewToDrag:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->getTabId()I

    move-result v2

    add-int/lit8 v3, v1, -0x1

    invoke-direct {p0, v2, v1, v3, v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->tabMoved(IIIZ)V

    .line 364
    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    iget-object v3, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mSelectedTabViewToDrag:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->getTabId()I

    move-result v3

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v2, v3, v1}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->moveTab(II)V

    goto :goto_0

    .line 367
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private createTabViewForTab(Lorg/chromium/chrome/browser/Tab;Z)Lcom/google/android/apps/chrome/widget/tabstrip/TabView;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1115
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$layout;->tab:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    .line 1117
    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabViewHandler:Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabViewHandler;

    invoke-static {p1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->fromTab(Lorg/chromium/chrome/browser/Tab;)Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v2

    iget v3, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabHeight:F

    invoke-virtual {v0, v1, v2, v3, p2}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->initializeControl(Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabViewHandler;Lcom/google/android/apps/chrome/tab/ChromeTab;FZ)V

    .line 1118
    iget v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentTabWidth:F

    invoke-virtual {v0, v1, v4}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->setTabWidth(FZ)V

    .line 1119
    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->getTabWidth()F

    move-result v2

    float-to-int v2, v2

    const/4 v3, -0x1

    invoke-direct {v1, v2, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1121
    return-object v0
.end method

.method private findTabViewById(I)Lcom/google/android/apps/chrome/widget/tabstrip/TabView;
    .locals 3

    .prologue
    .line 1070
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabViews:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    .line 1071
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->getTabId()I

    move-result v2

    if-ne v2, p1, :cond_0

    .line 1076
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private finishAnimationsForFullscreen(Z)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 498
    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;

    if-eqz v1, :cond_3

    iget v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mFullscreenToken:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->getControlOffset()F

    move-result v1

    const/4 v2, 0x0

    cmpg-float v1, v1, v2

    if-gez v1, :cond_3

    .line 501
    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScroller:Lcom/google/android/apps/chrome/third_party/OverScroller;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/third_party/OverScroller;->isFinished()Z

    move-result v1

    if-nez v1, :cond_0

    .line 502
    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScroller:Lcom/google/android/apps/chrome/third_party/OverScroller;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/third_party/OverScroller;->abortAnimation()V

    .line 503
    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScroller:Lcom/google/android/apps/chrome/third_party/OverScroller;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/third_party/OverScroller;->getCurrX()I

    move-result v1

    int-to-float v1, v1

    const/4 v2, 0x1

    invoke-direct {p0, v1, v2, v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->setScrollOffsetPosition(FZZ)V

    .line 504
    iput-boolean v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentlyScrollingForFastExpand:Z

    :cond_0
    move v1, v0

    move v2, v0

    .line 506
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabViews:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 507
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabViews:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    .line 508
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->isAnimating()Z

    move-result v3

    or-int/2addr v2, v3

    .line 509
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->finishAnimation()V

    .line 506
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 514
    :cond_1
    if-nez v2, :cond_2

    if-eqz p1, :cond_3

    .line 515
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->getRootView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$id;->control_container:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/widget/ControlContainer;

    .line 517
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/ControlContainer;->getResourceAdapter()Lcom/google/android/apps/chrome/compositor/resources/dynamics/ViewResourceAdapter;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/compositor/resources/dynamics/ViewResourceAdapter;->invalidate(Landroid/graphics/Rect;)V

    .line 520
    :cond_3
    return-void
.end method

.method private handleAnimationUpdates()V
    .locals 12

    .prologue
    const-wide/16 v10, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 737
    iget v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScrollOffset:F

    .line 740
    iget-object v3, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScroller:Lcom/google/android/apps/chrome/third_party/OverScroller;

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/third_party/OverScroller;->isFinished()Z

    move-result v3

    if-nez v3, :cond_c

    .line 741
    iget-object v3, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScroller:Lcom/google/android/apps/chrome/third_party/OverScroller;

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/third_party/OverScroller;->computeScrollOffset()Z

    move-result v3

    if-eqz v3, :cond_c

    .line 742
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScroller:Lcom/google/android/apps/chrome/third_party/OverScroller;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/third_party/OverScroller;->getCurrX()I

    move-result v0

    int-to-float v0, v0

    move v3, v0

    move v0, v1

    .line 748
    :goto_0
    iget-object v4, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScroller:Lcom/google/android/apps/chrome/third_party/OverScroller;

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/third_party/OverScroller;->isFinished()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 749
    iget-boolean v4, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentlyScrollingForFastExpand:Z

    if-eqz v4, :cond_0

    iput-boolean v2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentlyScrollingForFastExpand:Z

    .line 751
    :cond_0
    iget-object v4, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mSelectedTabViewToDrag:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    if-eqz v4, :cond_1

    .line 754
    const/4 v4, 0x0

    .line 755
    iget-wide v6, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mLastDragScrollTimeMs:J

    cmp-long v5, v6, v10

    if-nez v5, :cond_3

    .line 756
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mLastDragScrollTimeMs:J

    .line 763
    :goto_1
    iget-object v5, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mSelectedTabViewToDrag:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    invoke-virtual {v5}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->getTranslationX()F

    move-result v5

    invoke-static {}, Lorg/chromium/ui/base/LocalizationUtils;->isLayoutRtl()Z

    move-result v6

    invoke-static {v5, v6}, Lcom/google/android/apps/chrome/utilities/ChromeMathUtils;->flipSignIf(FZ)F

    move-result v5

    .line 770
    iget v6, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mDragStartDip:F

    .line 771
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->getWidth()I

    move-result v7

    int-to-float v7, v7

    iget v8, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mNewTabButtonSpace:F

    sub-float/2addr v7, v8

    iget v8, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentTabWidth:F

    sub-float/2addr v7, v8

    iget v8, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mDragStartDip:F

    sub-float/2addr v7, v8

    .line 779
    cmpg-float v8, v5, v6

    if-gez v8, :cond_4

    iget v8, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScrollFlags:I

    and-int/lit8 v8, v8, 0x1

    if-eqz v8, :cond_4

    .line 780
    iget v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mDragEndDip:F

    .line 784
    invoke-static {v5, v0, v6}, Lorg/chromium/chrome/browser/util/MathUtils;->clamp(FFF)F

    move-result v5

    sub-float v5, v6, v5

    sub-float v0, v6, v0

    div-float v0, v5, v0

    .line 786
    iget v5, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mDragSpeed:F

    mul-float/2addr v0, v5

    mul-float/2addr v0, v4

    add-float/2addr v3, v0

    move v0, v1

    .line 805
    :cond_1
    :goto_2
    new-instance v5, Ljava/util/LinkedList;

    invoke-direct {v5}, Ljava/util/LinkedList;-><init>()V

    .line 807
    iget-object v4, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabViews:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v4, v0

    :cond_2
    :goto_3
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    .line 808
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->update()Z

    move-result v7

    or-int/2addr v4, v7

    .line 810
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->getTabState()Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;

    move-result-object v7

    sget-object v8, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;->CLOSED:Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;

    if-ne v7, v8, :cond_2

    .line 811
    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 758
    :cond_3
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    .line 759
    iget-wide v4, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mLastDragScrollTimeMs:J

    sub-long v4, v6, v4

    long-to-float v4, v4

    const/high16 v5, 0x447a0000    # 1000.0f

    div-float/2addr v4, v5

    .line 760
    iput-wide v6, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mLastDragScrollTimeMs:J

    goto :goto_1

    .line 788
    :cond_4
    cmpl-float v6, v5, v7

    if-lez v6, :cond_5

    iget v6, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScrollFlags:I

    and-int/lit8 v6, v6, 0x2

    if-eqz v6, :cond_5

    .line 790
    iget v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mDragStartDip:F

    add-float/2addr v0, v7

    iget v6, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mDragEndDip:F

    sub-float/2addr v0, v6

    .line 794
    invoke-static {v5, v7, v0}, Lorg/chromium/chrome/browser/util/MathUtils;->clamp(FFF)F

    move-result v5

    sub-float/2addr v5, v7

    sub-float/2addr v0, v7

    div-float v0, v5, v0

    .line 797
    iget v5, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mDragSpeed:F

    mul-float/2addr v0, v5

    mul-float/2addr v0, v4

    sub-float/2addr v3, v0

    move v0, v1

    .line 799
    goto :goto_2

    .line 800
    :cond_5
    iput-wide v10, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mLastDragScrollTimeMs:J

    goto :goto_2

    .line 816
    :cond_6
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_7
    :goto_4
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    .line 817
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->die()V

    .line 818
    iget-object v6, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->getTabId()I

    move-result v7

    invoke-static {v6, v7, v1}, Lorg/chromium/chrome/browser/tabmodel/TabModelUtils;->closeTabById(Lorg/chromium/chrome/browser/tabmodel/TabModel;IZ)Z

    .line 819
    iget-object v6, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentlySelectedView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    if-ne v0, v6, :cond_7

    move v2, v1

    goto :goto_4

    .line 822
    :cond_8
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    invoke-static {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelUtils;->getCurrentTab(Lorg/chromium/chrome/browser/tabmodel/TabList;)Lorg/chromium/chrome/browser/Tab;

    move-result-object v0

    .line 823
    if-eqz v0, :cond_9

    if-eqz v2, :cond_9

    .line 824
    invoke-virtual {v0}, Lorg/chromium/chrome/browser/Tab;->getId()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->findTabViewById(I)Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    move-result-object v0

    .line 825
    if-eqz v0, :cond_9

    .line 826
    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentlySelectedView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    .line 827
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentlySelectedView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->selected()V

    .line 828
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->requestLayout()V

    .line 832
    :cond_9
    if-nez v4, :cond_a

    iget v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScrollOffset:F

    cmpl-float v0, v3, v0

    if-eqz v0, :cond_b

    .line 833
    :cond_a
    invoke-direct {p0, v3, v4, v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->updateScrollOffsetLimits(FZZ)V

    .line 835
    :cond_b
    return-void

    :cond_c
    move v3, v0

    move v0, v2

    goto/16 :goto_0
.end method

.method private handleTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 9

    .prologue
    const/4 v1, 0x0

    const/4 v8, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 241
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mGesture:Landroid/view/GestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v7

    .line 243
    invoke-direct {p0, v2}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->resetResizeTimeout(Z)V

    .line 247
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 287
    :cond_0
    :goto_0
    return v7

    .line 249
    :pswitch_0
    iput-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mPressedTabView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    .line 250
    invoke-direct {p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->stopDrag()V

    .line 251
    iget v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScrollOffset:F

    iget v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mMinScrollOffset:F

    cmpg-float v0, v0, v1

    if-ltz v0, :cond_1

    iget v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScrollOffset:F

    cmpl-float v0, v0, v3

    if-lez v0, :cond_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScroller:Lcom/google/android/apps/chrome/third_party/OverScroller;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/third_party/OverScroller;->isFinished()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 253
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScroller:Lcom/google/android/apps/chrome/third_party/OverScroller;

    iget v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScrollOffset:F

    float-to-int v1, v1

    iget v3, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mMinScrollOffset:F

    float-to-int v3, v3

    move v4, v2

    move v5, v2

    move v6, v2

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/apps/chrome/third_party/OverScroller;->springBack(IIIIII)Z

    .line 255
    invoke-direct {p0, v8}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->updateTabStripPositions(Z)V

    goto :goto_0

    .line 259
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mSelectedTabViewToDrag:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    if-eqz v0, :cond_0

    .line 260
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iget v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mLastXDragPosition:F

    sub-float/2addr v0, v1

    invoke-static {}, Lorg/chromium/ui/base/LocalizationUtils;->isLayoutRtl()Z

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/utilities/ChromeMathUtils;->flipSignIf(FZ)F

    move-result v0

    float-to-int v0, v0

    int-to-float v0, v0

    .line 262
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v1

    const/high16 v2, 0x3f800000    # 1.0f

    cmpl-float v1, v1, v2

    if-ltz v1, :cond_0

    .line 265
    cmpl-float v1, v0, v3

    if-lez v1, :cond_2

    .line 266
    iget v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScrollFlags:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScrollFlags:I

    .line 271
    :goto_1
    iget v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mDraggedXOffset:F

    add-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mDraggedXOffset:F

    .line 272
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mLastXDragPosition:F

    .line 274
    invoke-direct {p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->checkDragPosition()Z

    move-result v0

    if-nez v0, :cond_0

    .line 277
    invoke-direct {p0, v8}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->updateTabStripPositions(Z)V

    goto :goto_0

    .line 268
    :cond_2
    iget v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScrollFlags:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScrollFlags:I

    goto :goto_1

    .line 283
    :pswitch_2
    iput-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mPotentialDragTabView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    goto :goto_0

    .line 247
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private refreshTabStrip()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, -0x1

    const/4 v1, 0x0

    .line 1080
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabViews:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1081
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->removeAllViews()V

    .line 1083
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentlySelectedView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    .line 1085
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    if-eqz v0, :cond_2

    .line 1086
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->index()I

    move-result v2

    move v0, v1

    .line 1087
    :goto_0
    iget-object v3, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    invoke-interface {v3}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->getCount()I

    move-result v3

    if-ge v0, v3, :cond_2

    .line 1088
    iget-object v3, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    invoke-interface {v3, v0}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->getTabAt(I)Lorg/chromium/chrome/browser/Tab;

    move-result-object v3

    invoke-direct {p0, v3, v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->createTabViewForTab(Lorg/chromium/chrome/browser/Tab;Z)Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    move-result-object v3

    .line 1090
    if-ne v0, v2, :cond_0

    .line 1091
    invoke-virtual {v3}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->selected()V

    .line 1092
    iput-object v3, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentlySelectedView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    .line 1095
    :cond_0
    iget-object v4, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabViews:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1097
    if-gt v0, v2, :cond_1

    .line 1098
    invoke-virtual {v3}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    invoke-virtual {p0, v3, v5, v4}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 1087
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1100
    :cond_1
    invoke-virtual {v3}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    invoke-virtual {p0, v3, v2, v4}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    goto :goto_1

    .line 1105
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mNewTabButton:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mNewTabButton:Landroid/widget/ImageButton;

    invoke-virtual {v2}, Landroid/widget/ImageButton;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    invoke-virtual {p0, v0, v5, v2}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 1106
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mStrokeView:Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->getChildCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x2

    invoke-static {v2, v1}, Ljava/lang/Math;->max(II)I

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mStrokeView:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    invoke-virtual {p0, v0, v2, v3}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 1107
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mFadeView:Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->getChildCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x2

    invoke-static {v2, v1}, Ljava/lang/Math;->max(II)I

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mFadeView:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    invoke-virtual {p0, v0, v2, v3}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 1109
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabViews:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->updateCurrentTabWidth(IZ)V

    .line 1110
    invoke-direct {p0, v6, v6}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->updateScrollOffsetLimits(ZZ)V

    .line 1111
    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->finishAnimationsForFullscreen(Z)V

    .line 1112
    return-void
.end method

.method private resetResizeTimeout(Z)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1259
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mInternalHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    .line 1261
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mInternalHandler:Landroid/os/Handler;

    invoke-virtual {v1, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 1263
    :cond_0
    if-nez v0, :cond_1

    if-eqz p1, :cond_2

    .line 1264
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mInternalHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x5dc

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 1266
    :cond_2
    return-void
.end method

.method private setBackgroundTabFading(Z)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x4b

    const/16 v3, 0x4b

    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 838
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentlyFaded:Z

    if-ne p1, v0, :cond_0

    .line 860
    :goto_0
    return-void

    .line 840
    :cond_0
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentlyFaded:Z

    .line 841
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mFadeView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/TransitionDrawable;

    .line 843
    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentNewTabAnimation:Landroid/animation/ObjectAnimator;

    if-eqz v1, :cond_1

    .line 844
    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentNewTabAnimation:Landroid/animation/ObjectAnimator;

    invoke-virtual {v1}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 845
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentNewTabAnimation:Landroid/animation/ObjectAnimator;

    .line 848
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentlyFaded:Z

    if-eqz v1, :cond_3

    .line 849
    invoke-virtual {v0, v3}, Landroid/graphics/drawable/TransitionDrawable;->startTransition(I)V

    .line 850
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mNewTabButton:Landroid/widget/ImageButton;

    const-string/jumbo v1, "alpha"

    new-array v2, v2, [F

    const/4 v3, 0x0

    aput v3, v2, v4

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v0, v6, v7}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentNewTabAnimation:Landroid/animation/ObjectAnimator;

    .line 858
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentNewTabAnimation:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentNewTabAnimation:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 859
    :cond_2
    invoke-direct {p0, v4}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->updateTabStripPositions(Z)V

    goto :goto_0

    .line 853
    :cond_3
    invoke-virtual {v0, v3}, Landroid/graphics/drawable/TransitionDrawable;->reverseTransition(I)V

    .line 854
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mNewTabButton:Landroid/widget/ImageButton;

    const-string/jumbo v1, "alpha"

    new-array v2, v2, [F

    const/high16 v3, 0x3f800000    # 1.0f

    aput v3, v2, v4

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v0, v6, v7}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentNewTabAnimation:Landroid/animation/ObjectAnimator;

    goto :goto_1
.end method

.method private setScrollOffsetPosition(FZZ)V
    .locals 3

    .prologue
    .line 719
    iget v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScrollOffset:F

    .line 721
    iget v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mMinScrollOffset:F

    const/4 v2, 0x0

    invoke-static {p1, v1, v2}, Lorg/chromium/chrome/browser/util/MathUtils;->clamp(FFF)F

    move-result v1

    iput v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScrollOffset:F

    .line 726
    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mSelectedTabViewToDrag:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentlyScrollingForFastExpand:Z

    if-nez v1, :cond_0

    .line 727
    iget v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mDraggedXOffset:F

    iget v2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScrollOffset:F

    sub-float/2addr v2, v0

    sub-float/2addr v1, v2

    iput v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mDraggedXOffset:F

    .line 728
    invoke-direct {p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->checkDragPosition()Z

    .line 731
    :cond_0
    iget v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScrollOffset:F

    cmpl-float v0, v1, v0

    if-nez v0, :cond_1

    if-eqz p2, :cond_2

    .line 732
    :cond_1
    invoke-direct {p0, p3}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->updateTabStripPositions(Z)V

    .line 734
    :cond_2
    return-void
.end method

.method private startDrag(F)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 307
    iget v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScrollOffset:F

    iget v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mMinScrollOffset:F

    cmpg-float v0, v0, v1

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScrollOffset:F

    cmpl-float v0, v0, v3

    if-lez v0, :cond_1

    .line 331
    :cond_0
    return-void

    .line 309
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mPotentialDragTabView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->getCount()I

    move-result v0

    if-le v0, v2, :cond_0

    .line 310
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mPotentialDragTabView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->getTabId()I

    move-result v0

    invoke-direct {p0, v0, v4}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->tabSelected(IZ)V

    .line 312
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mPotentialDragTabView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mSelectedTabViewToDrag:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    .line 313
    iput-object v5, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mPotentialDragTabView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    .line 314
    iput-object v5, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mPressedTabView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    .line 315
    iput v3, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mDraggedXOffset:F

    .line 316
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mLastDragScrollTimeMs:J

    .line 317
    iput v4, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScrollFlags:I

    .line 319
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mSelectedTabViewToDrag:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    invoke-direct {p0, v0, v2, v2, v2}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->calculateOffsetToMakeTabVisible(Lcom/google/android/apps/chrome/widget/tabstrip/TabView;ZZZ)F

    move-result v0

    float-to-int v0, v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->startFastExpand(I)V

    .line 321
    iput p1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mLastXDragPosition:F

    .line 323
    invoke-direct {p0, v2}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->setBackgroundTabFading(Z)V

    .line 324
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mSelectedTabViewToDrag:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->getTabId()I

    move-result v2

    invoke-static {v1, v2}, Lorg/chromium/chrome/browser/tabmodel/TabModelUtils;->getTabIndexById(Lorg/chromium/chrome/browser/tabmodel/TabList;I)I

    move-result v1

    invoke-static {v0, v1}, Lorg/chromium/chrome/browser/tabmodel/TabModelUtils;->setIndex(Lorg/chromium/chrome/browser/tabmodel/TabModel;I)V

    .line 327
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mObservers:Lorg/chromium/base/ObserverList;

    invoke-virtual {v0}, Lorg/chromium/base/ObserverList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabStripObserver;

    .line 328
    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mSelectedTabViewToDrag:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->getTabId()I

    move-result v2

    invoke-interface {v0, v2}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabStripObserver;->onDragStart(I)V

    goto :goto_0
.end method

.method private startFastExpand(I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1125
    if-nez p1, :cond_0

    .line 1132
    :goto_0
    return-void

    .line 1129
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentlyScrollingForFastExpand:Z

    .line 1130
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScroller:Lcom/google/android/apps/chrome/third_party/OverScroller;

    iget v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScrollOffset:F

    float-to-int v1, v1

    invoke-virtual {v0, v1, v2, p1, v2}, Lcom/google/android/apps/chrome/third_party/OverScroller;->startScroll(IIII)V

    .line 1131
    invoke-static {p0}, Lorg/chromium/base/ApiCompatibilityUtils;->postInvalidateOnAnimation(Landroid/view/View;)V

    goto :goto_0
.end method

.method private stopDrag()V
    .locals 2

    .prologue
    .line 334
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mSelectedTabViewToDrag:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    if-eqz v0, :cond_0

    .line 335
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mSelectedTabViewToDrag:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    .line 336
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->setBackgroundTabFading(Z)V

    .line 337
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->updateTabStripPositions(Z)V

    .line 339
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mObservers:Lorg/chromium/base/ObserverList;

    invoke-virtual {v0}, Lorg/chromium/base/ObserverList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabStripObserver;

    .line 340
    invoke-interface {v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabStripObserver;->onDragStop()V

    goto :goto_0

    .line 343
    :cond_0
    return-void
.end method

.method private tabClosed(I)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 923
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->findTabViewById(I)Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    move-result-object v3

    .line 925
    if-eqz v3, :cond_1

    .line 926
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentlySelectedView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    if-ne v3, v0, :cond_0

    .line 927
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentlySelectedView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    .line 930
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabViews:Ljava/util/List;

    iget-object v4, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabViews:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-ne v0, v3, :cond_2

    move v0, v1

    .line 932
    :goto_0
    iget-object v4, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabViews:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 933
    invoke-virtual {p0, v3}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->removeView(Landroid/view/View;)V

    .line 935
    if-eqz v0, :cond_3

    .line 936
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabViews:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {p0, v0, v2}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->updateCurrentTabWidth(IZ)V

    .line 940
    :goto_1
    invoke-direct {p0, v1, v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->updateScrollOffsetLimits(ZZ)V

    .line 941
    invoke-direct {p0, v2}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->finishAnimationsForFullscreen(Z)V

    .line 942
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->requestLayout()V

    .line 944
    :cond_1
    return-void

    :cond_2
    move v0, v2

    .line 930
    goto :goto_0

    .line 938
    :cond_3
    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->resetResizeTimeout(Z)V

    goto :goto_1
.end method

.method private tabCreated(IZ)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 864
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    if-nez v0, :cond_1

    .line 920
    :cond_0
    :goto_0
    return-void

    .line 867
    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->findTabViewById(I)Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    move-result-object v0

    if-nez v0, :cond_0

    .line 871
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    invoke-static {v0, p1}, Lorg/chromium/chrome/browser/tabmodel/TabModelUtils;->getTabById(Lorg/chromium/chrome/browser/tabmodel/TabList;I)Lorg/chromium/chrome/browser/Tab;

    move-result-object v4

    .line 874
    if-eqz v4, :cond_0

    .line 878
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->getCount()I

    move-result v0

    iget-object v3, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabViews:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    if-le v0, v3, :cond_2

    .line 879
    invoke-direct {p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->refreshTabStrip()V

    goto :goto_0

    .line 883
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->getCount()I

    move-result v0

    if-le v0, v1, :cond_5

    move v0, v1

    :goto_1
    invoke-direct {p0, v4, v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->createTabViewForTab(Lorg/chromium/chrome/browser/Tab;Z)Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    move-result-object v3

    .line 884
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    invoke-interface {v0, v4}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->indexOf(Lorg/chromium/chrome/browser/Tab;)I

    move-result v0

    .line 885
    iget-object v4, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabViews:Ljava/util/List;

    invoke-interface {v4, v0, v3}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 886
    new-instance v4, Landroid/widget/FrameLayout$LayoutParams;

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->getTabWidth()F

    move-result v5

    float-to-int v5, v5

    const/4 v6, -0x1

    invoke-direct {v4, v5, v6}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v3, v0, v4}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 889
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentlySelectedView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    .line 892
    if-eqz p2, :cond_6

    .line 893
    iget-object v4, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentlySelectedView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    if-eqz v4, :cond_3

    .line 894
    iget-object v4, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentlySelectedView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->unselected()V

    .line 895
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentlySelectedView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    .line 898
    :cond_3
    if-eqz v3, :cond_7

    .line 899
    iput-object v3, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentlySelectedView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    .line 900
    iget-object v3, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentlySelectedView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->selected()V

    move-object v3, v0

    move v0, v2

    .line 907
    :goto_2
    iget-object v4, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabViews:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    invoke-direct {p0, v4, v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->updateCurrentTabWidth(IZ)V

    .line 908
    invoke-direct {p0, v1, v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->updateScrollOffsetLimits(ZZ)V

    .line 910
    if-eqz v3, :cond_4

    .line 911
    invoke-direct {p0, v3, v2, v0, v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->calculateOffsetToMakeTabVisible(Lcom/google/android/apps/chrome/widget/tabstrip/TabView;ZZZ)F

    move-result v0

    .line 913
    const/4 v1, 0x0

    cmpl-float v1, v0, v1

    if-eqz v1, :cond_4

    .line 914
    float-to-int v0, v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->startFastExpand(I)V

    .line 918
    :cond_4
    invoke-direct {p0, v2}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->finishAnimationsForFullscreen(Z)V

    .line 919
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->requestLayout()V

    goto/16 :goto_0

    :cond_5
    move v0, v2

    .line 883
    goto :goto_1

    :cond_6
    move v0, v1

    .line 904
    goto :goto_2

    :cond_7
    move-object v3, v0

    move v0, v2

    goto :goto_2
.end method

.method private tabFaviconChanged(I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1061
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->findTabViewById(I)Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    move-result-object v0

    .line 1063
    if-eqz v0, :cond_0

    .line 1064
    invoke-virtual {v0, v1, v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->updateContentFromTab(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1065
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->finishAnimationsForFullscreen(Z)V

    .line 1067
    :cond_0
    return-void
.end method

.method private tabLoadFinished(I)V
    .locals 1

    .prologue
    .line 1028
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->findTabViewById(I)Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    move-result-object v0

    .line 1030
    if-eqz v0, :cond_0

    .line 1031
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->loadingFinished()V

    .line 1033
    :cond_0
    return-void
.end method

.method private tabLoadStarted(I)V
    .locals 1

    .prologue
    .line 1020
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->findTabViewById(I)Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    move-result-object v0

    .line 1022
    if-eqz v0, :cond_0

    .line 1023
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->loadingStarted()V

    .line 1025
    :cond_0
    return-void
.end method

.method private tabMoved(IIIZ)V
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 986
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->findTabViewById(I)Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    move-result-object v0

    .line 987
    if-nez v0, :cond_1

    .line 1017
    :cond_0
    :goto_0
    return-void

    .line 989
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabViews:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    .line 990
    if-eq v1, p3, :cond_0

    if-eq v1, p2, :cond_2

    iget-object v3, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mSelectedTabViewToDrag:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    if-eq v0, v3, :cond_0

    .line 992
    :cond_2
    iget-object v3, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabViews:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 994
    if-ge v1, p3, :cond_3

    add-int/lit8 p3, p3, -0x1

    .line 996
    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabViews:Ljava/util/List;

    invoke-interface {v1, p3, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 999
    if-eqz p4, :cond_7

    .line 1000
    if-ge p3, p2, :cond_4

    add-int/lit8 v0, p3, 0x1

    move v5, v0

    .line 1001
    :goto_1
    if-ge p3, p2, :cond_5

    const/4 v0, -0x1

    move v1, v0

    :goto_2
    move v3, v4

    .line 1004
    :goto_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabViews:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_6

    .line 1005
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabViews:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->finishAnimation()V

    .line 1004
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_3

    .line 1000
    :cond_4
    add-int/lit8 v0, p3, -0x1

    move v5, v0

    goto :goto_1

    :cond_5
    move v1, v2

    .line 1001
    goto :goto_2

    .line 1008
    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabViews:Ljava/util/List;

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    .line 1009
    iget-object v3, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mSelectedTabViewToDrag:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    if-eq v0, v3, :cond_7

    .line 1010
    int-to-float v1, v1

    iget v3, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentTabWidth:F

    iget v5, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabOverlap:F

    sub-float/2addr v3, v5

    mul-float/2addr v1, v3

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->animateSlideFrom(F)V

    .line 1014
    :cond_7
    invoke-direct {p0, v2}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->updateTabStripPositions(Z)V

    .line 1015
    invoke-direct {p0, v4}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->finishAnimationsForFullscreen(Z)V

    .line 1016
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->requestLayout()V

    goto :goto_0
.end method

.method private tabPageLoadFinished(I)V
    .locals 1

    .prologue
    .line 1044
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->findTabViewById(I)Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    move-result-object v0

    .line 1046
    if-eqz v0, :cond_0

    .line 1047
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->pageLoadingFinished()V

    .line 1049
    :cond_0
    return-void
.end method

.method private tabPageLoadStarted(I)V
    .locals 1

    .prologue
    .line 1036
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->findTabViewById(I)Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    move-result-object v0

    .line 1038
    if-eqz v0, :cond_0

    .line 1039
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->pageLoadingStarted()V

    .line 1041
    :cond_0
    return-void
.end method

.method private tabSelected(IZ)V
    .locals 2

    .prologue
    .line 947
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentlySelectedView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentlySelectedView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->getTabId()I

    move-result v0

    if-ne v0, p1, :cond_1

    if-nez p2, :cond_1

    .line 983
    :cond_0
    :goto_0
    return-void

    .line 951
    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->findTabViewById(I)Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    move-result-object v0

    .line 953
    if-nez v0, :cond_2

    .line 954
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->tabCreated(IZ)V

    .line 955
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->findTabViewById(I)Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    move-result-object v0

    .line 957
    if-eqz v0, :cond_0

    .line 960
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mSelectedTabViewToDrag:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mSelectedTabViewToDrag:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->getTabId()I

    move-result v1

    if-eq v1, p1, :cond_3

    .line 961
    invoke-direct {p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->stopDrag()V

    .line 964
    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentlySelectedView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    if-eqz v1, :cond_4

    .line 965
    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentlySelectedView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->unselected()V

    .line 966
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentlySelectedView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    .line 969
    :cond_4
    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentlySelectedView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    .line 971
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentlySelectedView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    if-eqz v0, :cond_5

    .line 972
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentlySelectedView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->selected()V

    .line 975
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mKeyboardHider:Lcom/google/android/apps/chrome/KeyboardHider;

    new-instance v1, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$2;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$2;-><init>(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)V

    invoke-interface {v0, v1}, Lcom/google/android/apps/chrome/KeyboardHider;->hideKeyboard(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method private tabTitleChanged(ILjava/lang/String;)V
    .locals 2

    .prologue
    .line 1052
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->findTabViewById(I)Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    move-result-object v0

    .line 1054
    if-eqz v0, :cond_0

    .line 1055
    const/4 v1, 0x0

    invoke-virtual {v0, p2, v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->updateContentFromTab(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1056
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->finishAnimationsForFullscreen(Z)V

    .line 1058
    :cond_0
    return-void
.end method

.method private updateCurrentTabWidth(IZ)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 522
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mInternalHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 524
    invoke-static {v4, p1}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 526
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->getWidth()I

    move-result v0

    int-to-float v0, v0

    iget v2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mNewTabButtonSpace:F

    sub-float/2addr v0, v2

    iget v2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabOverlap:F

    add-int/lit8 v3, v1, -0x1

    int-to-float v3, v3

    mul-float/2addr v2, v3

    add-float/2addr v0, v2

    int-to-float v2, v1

    div-float/2addr v0, v2

    .line 529
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->getWidth()I

    move-result v2

    if-nez v2, :cond_0

    if-ne v1, v4, :cond_0

    iget v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mMaxTabWidth:F

    .line 531
    :cond_0
    iget v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mMaxTabWidth:F

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    iget v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mMinTabWidth:F

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentTabWidth:F

    .line 533
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabViews:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    .line 534
    iget v2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentTabWidth:F

    invoke-virtual {v0, v2, p2}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->setTabWidth(FZ)V

    goto :goto_0

    .line 537
    :cond_1
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->finishAnimationsForFullscreen(Z)V

    .line 538
    return-void
.end method

.method private updateScrollOffsetLimits(FZZ)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 704
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->getWidth()I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabOverlap:F

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mNewTabButtonSpace:F

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mMinScrollOffset:F

    .line 705
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabViews:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    .line 706
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->getTabWidth()F

    move-result v2

    iget v3, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabOverlap:F

    sub-float/2addr v2, v3

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->getTranslationY()F

    move-result v0

    iget v4, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabHeight:F

    div-float/2addr v0, v4

    sub-float v0, v3, v0

    mul-float/2addr v0, v2

    .line 708
    iget v2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mMinScrollOffset:F

    invoke-static {v0, v5}, Ljava/lang/Math;->max(FF)F

    move-result v0

    sub-float v0, v2, v0

    iput v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mMinScrollOffset:F

    goto :goto_0

    .line 711
    :cond_0
    iget v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mMinScrollOffset:F

    const v1, -0x457ced91    # -0.001f

    cmpl-float v0, v0, v1

    if-lez v0, :cond_1

    iget v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mMinScrollOffset:F

    const v1, 0x3a83126f    # 0.001f

    cmpg-float v0, v0, v1

    if-gez v0, :cond_1

    iput v5, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mMinScrollOffset:F

    .line 712
    :cond_1
    iget v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mMinScrollOffset:F

    invoke-static {v5, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mMinScrollOffset:F

    .line 714
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->setScrollOffsetPosition(FZZ)V

    .line 715
    return-void
.end method

.method private updateScrollOffsetLimits(ZZ)V
    .locals 1

    .prologue
    .line 699
    iget v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScrollOffset:F

    invoke-direct {p0, v0, p1, p2}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->updateScrollOffsetLimits(FZZ)V

    .line 700
    return-void
.end method

.method private updateTabStripPositions(Z)V
    .locals 18

    .prologue
    .line 541
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentlySelectedView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    if-eqz v1, :cond_b

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabViews:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentlySelectedView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    invoke-interface {v1, v2}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    move v3, v1

    .line 545
    :goto_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentlySelectedView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    if-eqz v1, :cond_c

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentlySelectedView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->getTabWidth()F

    move-result v1

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabEndStackWidth:F

    sub-float/2addr v1, v2

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabOverlap:F

    sub-float/2addr v1, v2

    move v4, v1

    .line 548
    :goto_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabViews:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v13

    .line 549
    move-object/from16 v0, p0

    iget v14, v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabEndStackWidth:F

    .line 551
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->getWidth()I

    move-result v1

    int-to-float v1, v1

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mNewTabButtonSpace:F

    sub-float v6, v1, v2

    .line 554
    move-object/from16 v0, p0

    iget v1, v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScrollOffset:F

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mMinScrollOffset:F

    const/4 v5, 0x0

    invoke-static {v1, v2, v5}, Lorg/chromium/chrome/browser/util/MathUtils;->clamp(FFF)F

    move-result v8

    .line 556
    const/4 v7, 0x0

    .line 557
    const/4 v5, 0x0

    .line 558
    const/4 v2, 0x0

    .line 559
    const/4 v1, 0x0

    move-object v9, v5

    move v10, v7

    move v11, v8

    move v8, v2

    move v7, v1

    :goto_2
    if-ge v7, v13, :cond_15

    .line 560
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabViews:Ljava/util/List;

    invoke-interface {v1, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    .line 562
    invoke-virtual {v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->getTabWidth()F

    move-result v15

    .line 566
    invoke-virtual {v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/FrameLayout$LayoutParams;

    .line 567
    if-eqz v2, :cond_0

    iget v2, v2, Landroid/widget/FrameLayout$LayoutParams;->width:I

    float-to-int v5, v15

    if-eq v2, v5, :cond_1

    .line 568
    :cond_0
    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    float-to-int v5, v15

    const/4 v12, -0x1

    invoke-direct {v2, v5, v12}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 569
    invoke-virtual {v1, v2}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 573
    :cond_1
    if-gt v7, v3, :cond_d

    const/4 v2, 0x4

    invoke-static {v7, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    move v5, v2

    .line 577
    :goto_3
    if-lt v7, v3, :cond_e

    add-int/lit8 v2, v13, -0x1

    sub-int/2addr v2, v7

    const/4 v12, 0x4

    invoke-static {v2, v12}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 582
    :goto_4
    int-to-float v5, v5

    mul-float v12, v14, v5

    .line 583
    int-to-float v2, v2

    mul-float/2addr v2, v14

    sub-float v2, v6, v2

    .line 589
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mSelectedTabViewToDrag:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    if-ne v1, v5, :cond_f

    add-int/lit8 v5, v13, -0x1

    if-lt v7, v5, :cond_2

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mDraggedXOffset:F

    const/16 v16, 0x0

    cmpg-float v5, v5, v16

    if-gez v5, :cond_f

    .line 590
    :cond_2
    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mDraggedXOffset:F

    add-float/2addr v5, v11

    .line 596
    :goto_5
    sub-float v16, v2, v15

    move/from16 v0, v16

    invoke-static {v5, v12, v0}, Lorg/chromium/chrome/browser/util/MathUtils;->clamp(FFF)F

    move-result v16

    .line 601
    invoke-virtual {v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->getTabState()Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;

    move-result-object v5

    sget-object v17, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;->CLOSING:Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;

    move-object/from16 v0, v17

    if-eq v5, v0, :cond_3

    .line 602
    invoke-static {}, Lorg/chromium/ui/base/LocalizationUtils;->isLayoutRtl()Z

    move-result v5

    move/from16 v0, v16

    invoke-static {v0, v5}, Lcom/google/android/apps/chrome/utilities/ChromeMathUtils;->flipSignIf(FZ)F

    move-result v5

    invoke-virtual {v1, v5}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->setTranslationX(F)V

    .line 608
    :cond_3
    if-eqz v9, :cond_11

    invoke-virtual {v9}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->getTranslationX()F

    move-result v5

    float-to-int v5, v5

    move/from16 v0, v16

    float-to-int v0, v0

    move/from16 v17, v0

    move/from16 v0, v17

    if-ne v5, v0, :cond_11

    invoke-virtual {v9}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->getTranslationY()F

    move-result v5

    float-to-int v5, v5

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->getTranslationY()F

    move-result v17

    move/from16 v0, v17

    float-to-int v0, v0

    move/from16 v17, v0

    move/from16 v0, v17

    if-ne v5, v0, :cond_11

    .line 610
    if-gt v7, v3, :cond_10

    invoke-virtual {v9}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->getVisibility()I

    move-result v5

    const/16 v17, 0x4

    move/from16 v0, v17

    if-eq v5, v0, :cond_10

    .line 613
    const/4 v5, 0x4

    invoke-virtual {v9, v5}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->setVisibility(I)V

    .line 628
    :cond_4
    :goto_6
    if-ne v7, v3, :cond_5

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->getVisibility()I

    move-result v5

    if-eqz v5, :cond_5

    .line 629
    const/4 v5, 0x0

    invoke-virtual {v1, v5}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->setVisibility(I)V

    .line 632
    :cond_5
    invoke-virtual {v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->getVisibility()I

    move-result v5

    if-nez v5, :cond_9

    .line 635
    const/high16 v5, 0x3f800000    # 1.0f

    .line 636
    if-eq v7, v3, :cond_7

    .line 639
    if-le v7, v3, :cond_13

    add-float v5, v12, v4

    .line 641
    :goto_7
    if-ge v7, v3, :cond_6

    sub-float/2addr v2, v4

    .line 646
    :cond_6
    add-float v9, v11, v15

    invoke-static {v9, v2}, Ljava/lang/Math;->min(FF)F

    move-result v2

    invoke-static {v11, v5}, Ljava/lang/Math;->max(FF)F

    move-result v5

    sub-float/2addr v2, v5

    div-float/2addr v2, v15

    const/4 v5, 0x0

    const/high16 v9, 0x3f800000    # 1.0f

    invoke-static {v2, v5, v9}, Lorg/chromium/chrome/browser/util/MathUtils;->clamp(FFF)F

    move-result v2

    move v5, v2

    .line 654
    :cond_7
    if-le v7, v3, :cond_8

    add-int/lit8 v2, v3, 0x1

    if-ne v7, v2, :cond_14

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mSelectedTabViewToDrag:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    if-eqz v2, :cond_14

    :cond_8
    const/4 v2, 0x0

    .line 660
    :goto_8
    move/from16 v0, p1

    invoke-virtual {v1, v2, v5, v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->handleLayout(FFZ)V

    .line 666
    :cond_9
    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->getTranslationY()F

    move-result v5

    move-object/from16 v0, p0

    iget v9, v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabHeight:F

    div-float/2addr v5, v9

    sub-float/2addr v2, v5

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabOverlap:F

    sub-float v5, v15, v5

    mul-float/2addr v2, v5

    const/4 v5, 0x0

    invoke-static {v2, v5}, Ljava/lang/Math;->max(FF)F

    move-result v2

    .line 670
    add-float v5, v16, v2

    invoke-static {v8, v5}, Ljava/lang/Math;->max(FF)F

    move-result v8

    .line 673
    add-float v9, v11, v2

    .line 674
    add-float v2, v2, v16

    .line 679
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mSelectedTabViewToDrag:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    if-ne v1, v5, :cond_a

    add-int/lit8 v5, v13, -0x1

    if-ne v7, v5, :cond_a

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mDraggedXOffset:F

    const/4 v10, 0x0

    cmpg-float v5, v5, v10

    if-gez v5, :cond_a

    .line 680
    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mDraggedXOffset:F

    sub-float/2addr v2, v5

    .line 559
    :cond_a
    add-int/lit8 v5, v7, 0x1

    move v7, v5

    move v10, v2

    move v11, v9

    move-object v9, v1

    goto/16 :goto_2

    .line 541
    :cond_b
    const/4 v1, 0x0

    move v3, v1

    goto/16 :goto_0

    .line 545
    :cond_c
    const/4 v1, 0x0

    move v4, v1

    goto/16 :goto_1

    .line 573
    :cond_d
    const/4 v2, 0x4

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    const/4 v5, 0x4

    sub-int v12, v7, v3

    invoke-static {v5, v12}, Ljava/lang/Math;->min(II)I

    move-result v5

    add-int/2addr v2, v5

    move v5, v2

    goto/16 :goto_3

    .line 577
    :cond_e
    add-int/lit8 v2, v13, -0x1

    sub-int/2addr v2, v3

    const/4 v12, 0x4

    invoke-static {v2, v12}, Ljava/lang/Math;->min(II)I

    move-result v2

    sub-int v12, v3, v7

    const/16 v16, 0x4

    move/from16 v0, v16

    invoke-static {v12, v0}, Ljava/lang/Math;->min(II)I

    move-result v12

    add-int/2addr v2, v12

    goto/16 :goto_4

    .line 592
    :cond_f
    invoke-virtual {v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->getOffsetX()F

    move-result v5

    add-float/2addr v5, v11

    goto/16 :goto_5

    .line 614
    :cond_10
    if-le v7, v3, :cond_4

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->getVisibility()I

    move-result v5

    const/4 v9, 0x4

    if-eq v5, v9, :cond_4

    .line 617
    const/4 v5, 0x4

    invoke-virtual {v1, v5}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->setVisibility(I)V

    goto/16 :goto_6

    .line 619
    :cond_11
    if-eqz v9, :cond_4

    invoke-virtual {v9}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->getTranslationX()F

    move-result v5

    float-to-int v5, v5

    move/from16 v0, v16

    float-to-int v0, v0

    move/from16 v17, v0

    move/from16 v0, v17

    if-eq v5, v0, :cond_4

    .line 621
    if-gt v7, v3, :cond_12

    invoke-virtual {v9}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->getVisibility()I

    move-result v5

    if-eqz v5, :cond_12

    .line 622
    const/4 v5, 0x0

    invoke-virtual {v9, v5}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->setVisibility(I)V

    goto/16 :goto_6

    .line 623
    :cond_12
    if-le v7, v3, :cond_4

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->getVisibility()I

    move-result v5

    if-eqz v5, :cond_4

    .line 624
    const/4 v5, 0x0

    invoke-virtual {v1, v5}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->setVisibility(I)V

    goto/16 :goto_6

    :cond_13
    move v5, v12

    .line 639
    goto/16 :goto_7

    .line 654
    :cond_14
    sub-float v2, v10, v16

    const/4 v9, 0x0

    invoke-static {v2, v9}, Ljava/lang/Math;->max(FF)F

    move-result v2

    div-float/2addr v2, v15

    goto/16 :goto_8

    .line 686
    :cond_15
    move-object/from16 v0, p0

    iget v1, v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabOverlap:F

    add-float/2addr v1, v8

    invoke-static {v1, v6}, Ljava/lang/Math;->min(FF)F

    move-result v1

    .line 687
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mMinScrollOffset:F

    sub-float/2addr v2, v3

    const/4 v3, 0x0

    cmpl-float v2, v2, v3

    if-lez v2, :cond_16

    move v1, v6

    .line 691
    :cond_16
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mNewTabButton:Landroid/widget/ImageButton;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mNewTabButtonOverlap:F

    sub-float/2addr v1, v3

    invoke-static {}, Lorg/chromium/ui/base/LocalizationUtils;->isLayoutRtl()Z

    move-result v3

    invoke-static {v1, v3}, Lcom/google/android/apps/chrome/utilities/ChromeMathUtils;->flipSignIf(FZ)F

    move-result v1

    invoke-virtual {v2, v1}, Landroid/widget/ImageButton;->setTranslationX(F)V

    .line 693
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mNewTabButton:Landroid/widget/ImageButton;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->getHeight()I

    move-result v2

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabHeight:F

    sub-float/2addr v2, v3

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setTranslationY(F)V

    .line 695
    invoke-static/range {p0 .. p0}, Lorg/chromium/base/ApiCompatibilityUtils;->postInvalidateOnAnimation(Landroid/view/View;)V

    .line 696
    return-void
.end method


# virtual methods
.method public getTabCount()I
    .locals 1

    .prologue
    .line 1501
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabViews:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method protected onAttachedToWindow()V
    .locals 3

    .prologue
    .line 1270
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->NOTIFICATIONS:[I

    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->registerForNotifications(Landroid/content/Context;[ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    .line 1272
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabViews:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    invoke-interface {v1}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->getCount()I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 1275
    invoke-direct {p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->refreshTabStrip()V

    .line 1277
    :cond_0
    invoke-super {p0}, Landroid/widget/FrameLayout;->onAttachedToWindow()V

    .line 1278
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 3

    .prologue
    .line 1282
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->NOTIFICATIONS:[I

    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->unregisterForNotifications(Landroid/content/Context;[ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    .line 1284
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    .line 1285
    return-void
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v4, -0x1

    const/4 v1, 0x0

    .line 373
    const-string/jumbo v0, "TabStrip:onDrawFPS"

    invoke-static {v0}, Lorg/chromium/base/PerfTraceEvent;->instant(Ljava/lang/String;)V

    .line 374
    invoke-direct {p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->handleAnimationUpdates()V

    .line 376
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mRefreshTabVisualsDueToResize:Z

    if-eqz v0, :cond_1

    .line 377
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabViews:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    .line 378
    invoke-virtual {v0, v2}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->updateVisualsFromLayoutParams(Z)V

    .line 379
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->requestLayout()V

    goto :goto_0

    .line 381
    :cond_0
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mRefreshTabVisualsDueToResize:Z

    .line 384
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScroller:Lcom/google/android/apps/chrome/third_party/OverScroller;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/third_party/OverScroller;->isFinished()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mSelectedTabViewToDrag:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mInternalHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    move v0, v2

    :goto_1
    move v2, v1

    move v3, v0

    .line 387
    :goto_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabViews:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_4

    .line 388
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabViews:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    .line 389
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->isAnimating()Z

    move-result v0

    or-int/2addr v3, v0

    .line 387
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    :cond_3
    move v0, v1

    .line 384
    goto :goto_1

    .line 392
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;

    if-eqz v0, :cond_5

    .line 393
    if-eqz v3, :cond_7

    iget v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mFullscreenToken:I

    if-ne v0, v4, :cond_7

    .line 394
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->getControlOffset()F

    move-result v0

    const/4 v2, 0x0

    cmpl-float v0, v0, v2

    if-nez v0, :cond_6

    .line 395
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->showControlsPersistent()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mFullscreenToken:I

    .line 405
    :cond_5
    :goto_3
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onDraw(Landroid/graphics/Canvas;)V

    .line 406
    return-void

    .line 397
    :cond_6
    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->finishAnimationsForFullscreen(Z)V

    goto :goto_3

    .line 399
    :cond_7
    if-nez v3, :cond_5

    iget v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mFullscreenToken:I

    if-eq v0, v4, :cond_5

    .line 400
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;

    iget v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mFullscreenToken:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->hideControlsPersistent(I)V

    .line 401
    iput v4, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mFullscreenToken:I

    goto :goto_3
.end method

.method public onFinishInflate()V
    .locals 2

    .prologue
    .line 205
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 207
    sget v0, Lcom/google/android/apps/chrome/R$id;->new_tab_button:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mNewTabButton:Landroid/widget/ImageButton;

    .line 208
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mNewTabButton:Landroid/widget/ImageButton;

    new-instance v1, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$1;-><init>(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 223
    sget v0, Lcom/google/android/apps/chrome/R$id;->fader_view:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mFadeView:Landroid/view/View;

    .line 225
    sget v0, Lcom/google/android/apps/chrome/R$id;->bottom_stroke:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mStrokeView:Landroid/view/View;

    .line 226
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 237
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->handleTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method protected onLayout(ZIIII)V
    .locals 7

    .prologue
    const/4 v6, -0x1

    const/4 v2, 0x0

    const/4 v5, 0x1

    .line 467
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->removeAllViewsInLayout()V

    .line 470
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentlySelectedView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabViews:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mCurrentlySelectedView:Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    invoke-interface {v0, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    move v1, v0

    :goto_0
    move v3, v2

    .line 473
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabViews:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_2

    .line 474
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabViews:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    .line 475
    if-gt v3, v1, :cond_1

    .line 476
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    invoke-virtual {p0, v0, v6, v4, v5}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->addViewInLayout(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;Z)Z

    .line 473
    :goto_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    :cond_0
    move v1, v2

    .line 470
    goto :goto_0

    .line 478
    :cond_1
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    invoke-virtual {p0, v0, v1, v4, v5}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->addViewInLayout(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;Z)Z

    goto :goto_2

    .line 483
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mNewTabButton:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mNewTabButton:Landroid/widget/ImageButton;

    invoke-virtual {v1}, Landroid/widget/ImageButton;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    invoke-virtual {p0, v0, v6, v1, v5}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->addViewInLayout(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;Z)Z

    .line 489
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mStrokeView:Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->getChildCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    iget-object v3, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mStrokeView:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    invoke-virtual {p0, v0, v1, v3, v5}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->addViewInLayout(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;Z)Z

    .line 491
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mFadeView:Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->getChildCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mFadeView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2, v5}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->addViewInLayout(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;Z)Z

    .line 494
    invoke-super/range {p0 .. p5}, Landroid/widget/FrameLayout;->onLayout(ZIIII)V

    .line 495
    return-void
.end method

.method public onSizeChanged(IIII)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 458
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/FrameLayout;->onSizeChanged(IIII)V

    .line 459
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabViews:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->updateCurrentTabWidth(IZ)V

    .line 460
    invoke-direct {p0, v3, v3}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->updateScrollOffsetLimits(ZZ)V

    .line 461
    iget v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScrollOffset:F

    iget v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mMinScrollOffset:F

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lorg/chromium/chrome/browser/util/MathUtils;->clamp(FFF)F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mScrollOffset:F

    .line 462
    iput-boolean v3, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mRefreshTabVisualsDueToResize:Z

    .line 463
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 230
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->handleTouchEvent(Landroid/view/MotionEvent;)Z

    .line 232
    const/4 v0, 0x1

    return v0
.end method

.method public refreshSelection()V
    .locals 2

    .prologue
    .line 450
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    if-eqz v0, :cond_0

    .line 451
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    invoke-static {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelUtils;->getCurrentTab(Lorg/chromium/chrome/browser/tabmodel/TabList;)Lorg/chromium/chrome/browser/Tab;

    move-result-object v0

    .line 452
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/Tab;->getId()I

    move-result v0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->tabSelected(IZ)V

    .line 454
    :cond_0
    return-void
.end method

.method public registerObserver(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$TabStripObserver;)V
    .locals 1

    .prologue
    .line 295
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mObservers:Lorg/chromium/base/ObserverList;

    invoke-virtual {v0, p1}, Lorg/chromium/base/ObserverList;->addObserver(Ljava/lang/Object;)Z

    .line 296
    return-void
.end method

.method public setKeyboardHider(Lcom/google/android/apps/chrome/KeyboardHider;)V
    .locals 0

    .prologue
    .line 440
    iput-object p1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mKeyboardHider:Lcom/google/android/apps/chrome/KeyboardHider;

    .line 441
    return-void
.end method

.method public setTabModel(Lorg/chromium/chrome/browser/tabmodel/TabModel;Lcom/google/android/apps/chrome/compositor/TabContentManager;Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;)V
    .locals 3

    .prologue
    .line 419
    iput-object p1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    .line 420
    iput-object p3, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabCreator:Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;

    .line 421
    iput-object p2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabContentManager:Lcom/google/android/apps/chrome/compositor/TabContentManager;

    .line 422
    iput-object p4, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;

    .line 424
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->isIncognito()Z

    move-result v1

    .line 426
    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mNewTabButton:Landroid/widget/ImageButton;

    if-eqz v1, :cond_0

    sget v0, Lcom/google/android/apps/chrome/R$drawable;->btn_tabstrip_new_incognito_tab:I

    :goto_0
    invoke-virtual {v2, v0}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 429
    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mStrokeView:Landroid/view/View;

    if-eqz v1, :cond_1

    sget v0, Lcom/google/android/apps/chrome/R$color;->tab_strip_bottom_stroke_incognito:I

    :goto_1
    invoke-virtual {v2, v0}, Landroid/view/View;->setBackgroundResource(I)V

    .line 431
    invoke-direct {p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->refreshTabStrip()V

    .line 432
    return-void

    .line 426
    :cond_0
    sget v0, Lcom/google/android/apps/chrome/R$drawable;->btn_tabstrip_new_tab:I

    goto :goto_0

    .line 429
    :cond_1
    sget v0, Lcom/google/android/apps/chrome/R$color;->tab_strip_bottom_stroke:I

    goto :goto_1
.end method

.method public testFling(F)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1520
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mGestureDetectorListener:Landroid/view/GestureDetector$SimpleOnGestureListener;

    const/4 v1, 0x0

    invoke-virtual {v0, v2, v2, p1, v1}, Landroid/view/GestureDetector$SimpleOnGestureListener;->onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z

    .line 1521
    return-void
.end method

.method public testSetScrollOffset(F)V
    .locals 2

    .prologue
    .line 1510
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->setScrollOffsetPosition(FZZ)V

    .line 1511
    return-void
.end method
