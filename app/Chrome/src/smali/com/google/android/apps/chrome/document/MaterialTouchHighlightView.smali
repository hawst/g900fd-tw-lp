.class public Lcom/google/android/apps/chrome/document/MaterialTouchHighlightView;
.super Landroid/view/View;
.source "MaterialTouchHighlightView.java"


# instance fields
.field private final mClipRect:Landroid/graphics/Rect;

.field private mIsTouchHighlightVisible:Z

.field private final mTouchColorOpaque:I

.field private final mTouchColorTranslucent:I

.field private final mTouchHighlight:Landroid/graphics/drawable/ShapeDrawable;

.field private final mTouchHighlightRadius:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 44
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 29
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/document/MaterialTouchHighlightView;->mClipRect:Landroid/graphics/Rect;

    .line 46
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/MaterialTouchHighlightView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 47
    sget v1, Lcom/google/android/apps/chrome/R$dimen;->document_titlebar_touch_highlight_diameter:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/android/apps/chrome/document/MaterialTouchHighlightView;->mTouchHighlightRadius:I

    .line 49
    sget v1, Lcom/google/android/apps/chrome/R$color;->titlebar_highlight_opaque:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/chrome/document/MaterialTouchHighlightView;->mTouchColorOpaque:I

    .line 50
    sget v1, Lcom/google/android/apps/chrome/R$color;->titlebar_highlight_translucent:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/document/MaterialTouchHighlightView;->mTouchColorTranslucent:I

    .line 52
    new-instance v0, Landroid/graphics/drawable/ShapeDrawable;

    new-instance v1, Landroid/graphics/drawable/shapes/OvalShape;

    invoke-direct {v1}, Landroid/graphics/drawable/shapes/OvalShape;-><init>()V

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ShapeDrawable;-><init>(Landroid/graphics/drawable/shapes/Shape;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/document/MaterialTouchHighlightView;->mTouchHighlight:Landroid/graphics/drawable/ShapeDrawable;

    .line 53
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/MaterialTouchHighlightView;->mTouchHighlight:Landroid/graphics/drawable/ShapeDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/ShapeDrawable;->getPaint()Landroid/graphics/Paint;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/chrome/document/MaterialTouchHighlightView;->mTouchColorOpaque:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 54
    return-void
.end method


# virtual methods
.method initializeBackgroundParameters()V
    .locals 6

    .prologue
    .line 61
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/MaterialTouchHighlightView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$dimen;->document_titlebar_box_margin:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 62
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    .line 63
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/MaterialTouchHighlightView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    .line 65
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/MaterialTouchHighlightView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 66
    iget v3, v2, Landroid/graphics/Rect;->top:I

    sub-int v3, v1, v3

    iput v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 67
    iget v3, v2, Landroid/graphics/Rect;->left:I

    sub-int v3, v1, v3

    iput v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 68
    iget v3, v2, Landroid/graphics/Rect;->right:I

    sub-int v3, v1, v3

    iput v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    .line 69
    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    .line 70
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/MaterialTouchHighlightView;->requestLayout()V

    .line 73
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/MaterialTouchHighlightView;->mClipRect:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/MaterialTouchHighlightView;->getPaddingLeft()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/MaterialTouchHighlightView;->getPaddingTop()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/MaterialTouchHighlightView;->getWidth()I

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/MaterialTouchHighlightView;->getPaddingRight()I

    move-result v4

    sub-int/2addr v3, v4

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/MaterialTouchHighlightView;->getHeight()I

    move-result v4

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/MaterialTouchHighlightView;->getPaddingBottom()I

    move-result v5

    sub-int/2addr v4, v5

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 75
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    .line 79
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 81
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/document/MaterialTouchHighlightView;->mIsTouchHighlightVisible:Z

    if-nez v0, :cond_0

    .line 88
    :goto_0
    return-void

    .line 82
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/MaterialTouchHighlightView;->mClipRect:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/MaterialTouchHighlightView;->getPaddingLeft()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/MaterialTouchHighlightView;->getPaddingTop()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/MaterialTouchHighlightView;->getWidth()I

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/MaterialTouchHighlightView;->getPaddingRight()I

    move-result v4

    sub-int/2addr v3, v4

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/MaterialTouchHighlightView;->getHeight()I

    move-result v4

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/MaterialTouchHighlightView;->getPaddingBottom()I

    move-result v5

    sub-int/2addr v4, v5

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 84
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 85
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/MaterialTouchHighlightView;->mClipRect:Landroid/graphics/Rect;

    sget-object v1, Landroid/graphics/Region$Op;->INTERSECT:Landroid/graphics/Region$Op;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;Landroid/graphics/Region$Op;)Z

    .line 86
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/MaterialTouchHighlightView;->mTouchHighlight:Landroid/graphics/drawable/ShapeDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/ShapeDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 87
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    goto :goto_0
.end method

.method setTouchHighlightCoordinates(II)V
    .locals 5

    .prologue
    .line 113
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/MaterialTouchHighlightView;->mTouchHighlight:Landroid/graphics/drawable/ShapeDrawable;

    iget v1, p0, Lcom/google/android/apps/chrome/document/MaterialTouchHighlightView;->mTouchHighlightRadius:I

    sub-int v1, p1, v1

    iget v2, p0, Lcom/google/android/apps/chrome/document/MaterialTouchHighlightView;->mTouchHighlightRadius:I

    sub-int v2, p2, v2

    iget v3, p0, Lcom/google/android/apps/chrome/document/MaterialTouchHighlightView;->mTouchHighlightRadius:I

    add-int/2addr v3, p1

    iget v4, p0, Lcom/google/android/apps/chrome/document/MaterialTouchHighlightView;->mTouchHighlightRadius:I

    add-int/2addr v4, p2

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/drawable/ShapeDrawable;->setBounds(IIII)V

    .line 115
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/MaterialTouchHighlightView;->invalidate()V

    .line 116
    return-void
.end method

.method setTouchHighlightIsOpaque(Z)V
    .locals 2

    .prologue
    .line 95
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/MaterialTouchHighlightView;->mTouchHighlight:Landroid/graphics/drawable/ShapeDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/ShapeDrawable;->getPaint()Landroid/graphics/Paint;

    move-result-object v1

    if-eqz p1, :cond_0

    iget v0, p0, Lcom/google/android/apps/chrome/document/MaterialTouchHighlightView;->mTouchColorOpaque:I

    :goto_0
    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 96
    return-void

    .line 95
    :cond_0
    iget v0, p0, Lcom/google/android/apps/chrome/document/MaterialTouchHighlightView;->mTouchColorTranslucent:I

    goto :goto_0
.end method

.method setTouchHighlightVisible(Z)V
    .locals 0

    .prologue
    .line 103
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/document/MaterialTouchHighlightView;->mIsTouchHighlightVisible:Z

    .line 104
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/MaterialTouchHighlightView;->invalidate()V

    .line 105
    return-void
.end method
