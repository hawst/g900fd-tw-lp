.class public interface abstract Lcom/google/android/apps/chrome/ChromeBrowserInitializer$BrowserParts;
.super Ljava/lang/Object;
.source "ChromeBrowserInitializer.java"


# virtual methods
.method public abstract finishNativeInitialization()V
.end method

.method public abstract initializeCompositor()V
.end method

.method public abstract initializeState()V
.end method

.method public abstract isActivityDestroyed()Z
.end method

.method public abstract onStartupFailure()V
.end method

.method public abstract postInflationStartup()V
.end method

.method public abstract preInflationStartup()V
.end method

.method public abstract setContentViewAndLoadLibrary()V
.end method
