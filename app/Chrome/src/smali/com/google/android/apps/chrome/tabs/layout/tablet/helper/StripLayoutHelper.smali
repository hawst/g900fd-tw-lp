.class public Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;
.super Ljava/lang/Object;
.source "StripLayoutHelper.java"


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final TAN_OF_REORDER_ANGLE_START_THRESHOLD:F


# instance fields
.field private mCachedTabWidth:F

.field private mContext:Landroid/content/Context;

.field private mHeight:F

.field private mInReorderMode:Z

.field private final mIncognito:Z

.field private mInteractingTab:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

.field private mLastPressedCloseButton:Lcom/google/android/apps/chrome/tabs/CompositorButton;

.field private mLastReorderScrollTime:J

.field private mLastReorderX:F

.field private mLastSpinnerUpdate:J

.field private mLayoutAnimations:Lcom/google/android/apps/chrome/ChromeAnimation;

.field private mLeftMargin:F

.field private final mMaxTabWidth:F

.field private mMinScrollOffset:F

.field private final mMinTabWidth:F

.field private mModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

.field private final mNewTabButton:Lcom/google/android/apps/chrome/tabs/CompositorButton;

.field private mNewTabButtonWidth:F

.field private final mRenderHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;

.field private final mReorderMoveStartThreshold:F

.field private mReorderState:I

.field private mRightMargin:F

.field private mScrollOffset:I

.field private mScroller:Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;

.field private mStripStacker:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripStacker;

.field private final mStripTabEventHandler:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper$StripTabEventHandler;

.field private mStripTabs:[Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

.field private mStripTabsToRender:[Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

.field private mStripTabsVisuallyOrdered:[Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

.field private mTabContentManager:Lcom/google/android/apps/chrome/compositor/TabContentManager;

.field private mTabCreator:Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;

.field private final mTabLoadTrackerHost:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/TabLoadTracker$TabLoadTrackerCallback;

.field private final mTabOverlapWidth:F

.field private final mTabStackWidth:F

.field private final mUpdateHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;

.field private mWidth:F


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 45
    const-class v0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->$assertionsDisabled:Z

    .line 54
    const-wide v0, 0x3fe921fb54442d18L    # 0.7853981633974483

    invoke-static {v0, v1}, Ljava/lang/Math;->tan(D)D

    move-result-wide v0

    double-to-float v0, v0

    sput v0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->TAN_OF_REORDER_ANGLE_START_THRESHOLD:F

    return-void

    .line 45
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;Z)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    const/high16 v4, 0x42680000    # 58.0f

    const/high16 v5, 0x40800000    # 4.0f

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 143
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 92
    new-instance v0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StaticStripStacker;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StaticStripStacker;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mStripStacker:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripStacker;

    .line 95
    new-array v0, v2, [Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mStripTabs:[Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    .line 96
    new-array v0, v2, [Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mStripTabsVisuallyOrdered:[Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    .line 97
    new-array v0, v2, [Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mStripTabsToRender:[Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    .line 98
    new-instance v0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper$StripTabEventHandler;

    invoke-direct {v0, p0, v3}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper$StripTabEventHandler;-><init>(Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper$1;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mStripTabEventHandler:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper$StripTabEventHandler;

    .line 99
    new-instance v0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper$TabLoadTrackerCallbackImpl;

    invoke-direct {v0, p0, v3}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper$TabLoadTrackerCallbackImpl;-><init>(Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper$1;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mTabLoadTrackerHost:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/TabLoadTracker$TabLoadTrackerCallback;

    .line 119
    iput v2, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mReorderState:I

    .line 120
    iput-boolean v2, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mInReorderMode:Z

    .line 144
    iput v5, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mTabStackWidth:F

    .line 145
    const/high16 v0, 0x41c00000    # 24.0f

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mTabOverlapWidth:F

    .line 146
    iput v4, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mNewTabButtonWidth:F

    .line 148
    invoke-static {}, Lorg/chromium/ui/base/LocalizationUtils;->isLayoutRtl()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 150
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mNewTabButtonWidth:F

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mTabOverlapWidth:F

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    sub-float/2addr v0, v2

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mNewTabButtonWidth:F

    .line 152
    :cond_0
    invoke-static {}, Lorg/chromium/ui/base/LocalizationUtils;->isLayoutRtl()Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_0
    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mRightMargin:F

    .line 153
    invoke-static {}, Lorg/chromium/ui/base/LocalizationUtils;->isLayoutRtl()Z

    move-result v0

    if-eqz v0, :cond_1

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mNewTabButtonWidth:F

    :cond_1
    iput v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mLeftMargin:F

    .line 154
    const/high16 v0, 0x433e0000    # 190.0f

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mMinTabWidth:F

    .line 155
    const v0, 0x43848000    # 265.0f

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mMaxTabWidth:F

    .line 156
    const/high16 v0, 0x42480000    # 50.0f

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mReorderMoveStartThreshold:F

    .line 157
    iput-object p2, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mUpdateHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;

    .line 158
    iput-object p3, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mRenderHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;

    .line 159
    new-instance v0, Lcom/google/android/apps/chrome/tabs/CompositorButton;

    const/high16 v1, 0x42020000    # 32.5f

    invoke-direct {v0, p1, v4, v1}, Lcom/google/android/apps/chrome/tabs/CompositorButton;-><init>(Landroid/content/Context;FF)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mNewTabButton:Lcom/google/android/apps/chrome/tabs/CompositorButton;

    .line 161
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mNewTabButton:Lcom/google/android/apps/chrome/tabs/CompositorButton;

    sget v1, Lcom/google/android/apps/chrome/R$drawable;->btn_tabstrip_new_tab_normal:I

    sget v2, Lcom/google/android/apps/chrome/R$drawable;->btn_tabstrip_new_tab_pressed:I

    sget v3, Lcom/google/android/apps/chrome/R$drawable;->btn_tabstrip_new_incognito_tab_normal:I

    sget v4, Lcom/google/android/apps/chrome/R$drawable;->btn_tabstrip_new_tab_pressed:I

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/apps/chrome/tabs/CompositorButton;->setResources(IIII)V

    .line 165
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mNewTabButton:Lcom/google/android/apps/chrome/tabs/CompositorButton;

    invoke-virtual {v0, p4}, Lcom/google/android/apps/chrome/tabs/CompositorButton;->setIncognito(Z)V

    .line 166
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mNewTabButton:Lcom/google/android/apps/chrome/tabs/CompositorButton;

    const/high16 v1, 0x40c00000    # 6.0f

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/tabs/CompositorButton;->setY(F)V

    .line 167
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mNewTabButton:Lcom/google/android/apps/chrome/tabs/CompositorButton;

    invoke-virtual {v0, v5}, Lcom/google/android/apps/chrome/tabs/CompositorButton;->setClickSlop(F)V

    .line 168
    iput-object p1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mContext:Landroid/content/Context;

    .line 169
    iput-boolean p4, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mIncognito:Z

    .line 170
    return-void

    .line 152
    :cond_2
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mNewTabButtonWidth:F

    goto :goto_0
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;Z)V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->computeAndUpdateTabWidth(Z)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;)Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mUpdateHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;

    return-object v0
.end method

.method private static buildTabClosedAnimation(Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;)Lcom/google/android/apps/chrome/ChromeAnimation$Animation;
    .locals 10

    .prologue
    .line 1369
    sget-object v1, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab$Property;->Y_OFFSET:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab$Property;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->getOffsetY()F

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->getHeight()F

    move-result v3

    const-wide/16 v4, 0x96

    const-wide/16 v6, 0x0

    const/4 v8, 0x0

    invoke-static {}, Lcom/google/android/apps/chrome/ChromeAnimation;->getLinearInterpolator()Landroid/view/animation/LinearInterpolator;

    move-result-object v9

    move-object v0, p0

    invoke-static/range {v0 .. v9}, Lcom/google/android/apps/chrome/ChromeAnimation$AnimatableAnimation;->createAnimation(Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJZLandroid/view/animation/Interpolator;)Lcom/google/android/apps/chrome/ChromeAnimation$Animation;

    move-result-object v0

    return-object v0
.end method

.method private static buildTabCreatedAnimation(Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;)Lcom/google/android/apps/chrome/ChromeAnimation$Animation;
    .locals 10

    .prologue
    .line 1364
    sget-object v1, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab$Property;->Y_OFFSET:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab$Property;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->getHeight()F

    move-result v2

    const/4 v3, 0x0

    const-wide/16 v4, 0x96

    const-wide/16 v6, 0x0

    const/4 v8, 0x0

    invoke-static {}, Lcom/google/android/apps/chrome/ChromeAnimation;->getLinearInterpolator()Landroid/view/animation/LinearInterpolator;

    move-result-object v9

    move-object v0, p0

    invoke-static/range {v0 .. v9}, Lcom/google/android/apps/chrome/ChromeAnimation$AnimatableAnimation;->createAnimation(Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJZLandroid/view/animation/Interpolator;)Lcom/google/android/apps/chrome/ChromeAnimation$Animation;

    move-result-object v0

    return-object v0
.end method

.method private static buildTabMoveAnimation(Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;F)Lcom/google/android/apps/chrome/ChromeAnimation$Animation;
    .locals 10

    .prologue
    .line 1382
    sget-object v1, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab$Property;->X_OFFSET:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab$Property;

    const/4 v3, 0x0

    const-wide/16 v4, 0x7d

    const-wide/16 v6, 0x0

    const/4 v8, 0x0

    invoke-static {}, Lcom/google/android/apps/chrome/ChromeAnimation;->getLinearInterpolator()Landroid/view/animation/LinearInterpolator;

    move-result-object v9

    move-object v0, p0

    move v2, p1

    invoke-static/range {v0 .. v9}, Lcom/google/android/apps/chrome/ChromeAnimation$AnimatableAnimation;->createAnimation(Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJZLandroid/view/animation/Interpolator;)Lcom/google/android/apps/chrome/ChromeAnimation$Animation;

    move-result-object v0

    return-object v0
.end method

.method private static buildTabResizeAnimation(Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;F)Lcom/google/android/apps/chrome/ChromeAnimation$Animation;
    .locals 10

    .prologue
    .line 1376
    sget-object v1, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab$Property;->WIDTH:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab$Property;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->getWidth()F

    move-result v2

    const-wide/16 v4, 0x96

    const-wide/16 v6, 0x0

    const/4 v8, 0x0

    invoke-static {}, Lcom/google/android/apps/chrome/ChromeAnimation;->getLinearInterpolator()Landroid/view/animation/LinearInterpolator;

    move-result-object v9

    move-object v0, p0

    move v3, p1

    invoke-static/range {v0 .. v9}, Lcom/google/android/apps/chrome/ChromeAnimation$AnimatableAnimation;->createAnimation(Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJZLandroid/view/animation/Interpolator;)Lcom/google/android/apps/chrome/ChromeAnimation$Animation;

    move-result-object v0

    return-object v0
.end method

.method private calculateOffsetToMakeTabVisible(Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;ZZZ)F
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 1071
    if-nez p1, :cond_0

    move v0, v2

    .line 1107
    :goto_0
    return v0

    .line 1073
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->index()I

    move-result v3

    .line 1074
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->getId()I

    move-result v1

    invoke-static {v0, v1}, Lorg/chromium/chrome/browser/tabmodel/TabModelUtils;->getTabIndexById(Lorg/chromium/chrome/browser/tabmodel/TabList;I)I

    move-result v4

    .line 1077
    if-ne v3, v4, :cond_1

    if-nez p2, :cond_1

    move v0, v2

    goto :goto_0

    .line 1080
    :cond_1
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mWidth:F

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mLeftMargin:F

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mRightMargin:F

    sub-float v1, v0, v1

    .line 1081
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mCachedTabWidth:F

    iget v5, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mTabOverlapWidth:F

    sub-float v5, v0, v5

    .line 1087
    neg-int v0, v4

    int-to-float v0, v0

    mul-float/2addr v0, v5

    .line 1088
    add-int/lit8 v6, v4, 0x1

    int-to-float v6, v6

    mul-float/2addr v6, v5

    sub-float/2addr v1, v6

    .line 1092
    if-ge v4, v3, :cond_3

    .line 1093
    sub-float/2addr v1, v5

    .line 1100
    :cond_2
    :goto_1
    iget v3, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mScrollOffset:I

    int-to-float v3, v3

    cmpg-float v3, v3, v0

    if-gez v3, :cond_4

    if-eqz p3, :cond_4

    .line 1101
    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mScrollOffset:I

    int-to-float v1, v1

    sub-float/2addr v0, v1

    goto :goto_0

    .line 1094
    :cond_3
    if-le v4, v3, :cond_2

    .line 1095
    add-float/2addr v0, v5

    goto :goto_1

    .line 1102
    :cond_4
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mScrollOffset:I

    int-to-float v0, v0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_5

    if-eqz p4, :cond_5

    .line 1103
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mScrollOffset:I

    int-to-float v0, v0

    sub-float v0, v1, v0

    goto :goto_0

    :cond_5
    move v0, v2

    .line 1107
    goto :goto_0
.end method

.method private computeAndUpdateTabOrders(Z)V
    .locals 5

    .prologue
    .line 776
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->getCount()I

    move-result v2

    .line 777
    new-array v3, v2, [Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    .line 779
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 780
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    invoke-interface {v0, v1}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->getTabAt(I)Lorg/chromium/chrome/browser/Tab;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/Tab;->getId()I

    move-result v4

    .line 781
    invoke-virtual {p0, v4}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->findTabById(I)Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    move-result-object v0

    .line 782
    if-eqz v0, :cond_0

    :goto_1
    aput-object v0, v3, v1

    .line 779
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 782
    :cond_0
    invoke-direct {p0, v4}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->createStripTab(I)Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    move-result-object v0

    goto :goto_1

    .line 785
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mStripTabs:[Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    array-length v0, v0

    .line 786
    iput-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mStripTabs:[Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    .line 788
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mStripTabs:[Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    array-length v1, v1

    if-eq v1, v0, :cond_2

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->resizeTabStrip(Z)V

    .line 790
    :cond_2
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->updateVisualTabOrdering()V

    .line 791
    return-void
.end method

.method private computeAndUpdateTabWidth(Z)V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 847
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mStripTabEventHandler:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper$StripTabEventHandler;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper$StripTabEventHandler;->removeMessages(I)V

    .line 849
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mStripTabs:[Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    array-length v0, v0

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 852
    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mWidth:F

    iget v3, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mLeftMargin:F

    sub-float/2addr v2, v3

    iget v3, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mRightMargin:F

    sub-float/2addr v2, v3

    .line 855
    iget v3, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mTabOverlapWidth:F

    add-int/lit8 v4, v0, -0x1

    int-to-float v4, v4

    mul-float/2addr v3, v4

    .line 858
    add-float/2addr v2, v3

    int-to-float v0, v0

    div-float v0, v2, v0

    .line 861
    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mMinTabWidth:F

    iget v3, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mMaxTabWidth:F

    invoke-static {v0, v2, v3}, Lorg/chromium/chrome/browser/util/MathUtils;->clamp(FFF)F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mCachedTabWidth:F

    move v0, v1

    .line 864
    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mStripTabs:[Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 865
    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mStripTabs:[Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    aget-object v2, v2, v0

    .line 866
    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->isDying()Z

    move-result v3

    if-nez v3, :cond_0

    .line 868
    if-eqz p1, :cond_1

    .line 869
    iget v3, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mCachedTabWidth:F

    invoke-static {v2, v3}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->buildTabResizeAnimation(Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;F)Lcom/google/android/apps/chrome/ChromeAnimation$Animation;

    move-result-object v2

    invoke-direct {p0, v2, v1}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->startAnimation(Lcom/google/android/apps/chrome/ChromeAnimation$Animation;Z)V

    .line 864
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 871
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mStripTabs:[Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    aget-object v2, v2, v0

    iget v3, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mCachedTabWidth:F

    invoke-virtual {v2, v3}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->setWidth(F)V

    goto :goto_1

    .line 874
    :cond_2
    return-void
.end method

.method private computeTabInitialPositions()V
    .locals 6

    .prologue
    .line 916
    invoke-static {}, Lorg/chromium/ui/base/LocalizationUtils;->isLayoutRtl()Z

    move-result v0

    if-nez v0, :cond_0

    .line 917
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mScrollOffset:I

    int-to-float v0, v0

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mLeftMargin:F

    add-float/2addr v0, v1

    .line 922
    :goto_0
    const/4 v1, 0x0

    move v5, v1

    move v1, v0

    move v0, v5

    :goto_1
    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mStripTabs:[Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 923
    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mStripTabs:[Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    aget-object v2, v2, v0

    .line 924
    invoke-virtual {v2, v1}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->setIdealX(F)V

    .line 925
    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->getWidth()F

    move-result v3

    iget v4, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mTabOverlapWidth:F

    sub-float/2addr v3, v4

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->getWidthWeight()F

    move-result v2

    mul-float/2addr v2, v3

    .line 926
    invoke-static {}, Lorg/chromium/ui/base/LocalizationUtils;->isLayoutRtl()Z

    move-result v3

    invoke-static {v2, v3}, Lcom/google/android/apps/chrome/utilities/ChromeMathUtils;->flipSignIf(FZ)F

    move-result v2

    .line 927
    add-float/2addr v1, v2

    .line 922
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 919
    :cond_0
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mWidth:F

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mCachedTabWidth:F

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mScrollOffset:I

    int-to-float v1, v1

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mRightMargin:F

    sub-float/2addr v0, v1

    goto :goto_0

    .line 929
    :cond_1
    return-void
.end method

.method private computeTabOffsetHelper()V
    .locals 12

    .prologue
    .line 932
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->index()I

    move-result v3

    .line 936
    if-ltz v3, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mStripTabs:[Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    aget-object v0, v0, v3

    .line 937
    :goto_0
    if-eqz v0, :cond_5

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->getWidth()F

    move-result v0

    .line 938
    :goto_1
    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mTabStackWidth:F

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mTabOverlapWidth:F

    sub-float v6, v0, v1

    .line 940
    const/4 v0, 0x0

    :goto_2
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mStripTabs:[Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    array-length v1, v1

    if-ge v0, v1, :cond_d

    .line 941
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mStripTabs:[Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    aget-object v7, v1, v0

    .line 943
    invoke-virtual {v7}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->getIdealX()F

    move-result v8

    .line 947
    if-ge v0, v3, :cond_6

    const/4 v1, 0x4

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 951
    :goto_3
    if-lt v0, v3, :cond_7

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mStripTabs:[Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    sub-int/2addr v1, v0

    const/4 v4, 0x4

    invoke-static {v1, v4}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 956
    :goto_4
    invoke-static {}, Lorg/chromium/ui/base/LocalizationUtils;->isLayoutRtl()Z

    move-result v4

    if-eqz v4, :cond_0

    move v11, v2

    move v2, v1

    move v1, v11

    .line 964
    :cond_0
    iget v4, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mTabStackWidth:F

    int-to-float v2, v2

    mul-float/2addr v2, v4

    iget v4, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mLeftMargin:F

    add-float/2addr v4, v2

    .line 965
    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mWidth:F

    iget v5, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mTabStackWidth:F

    int-to-float v1, v1

    mul-float/2addr v1, v5

    sub-float v1, v2, v1

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mRightMargin:F

    sub-float/2addr v1, v2

    .line 967
    invoke-virtual {v7}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->getOffsetX()F

    move-result v2

    add-float/2addr v2, v8

    invoke-virtual {v7}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->getWidth()F

    move-result v5

    sub-float v5, v1, v5

    invoke-static {v2, v4, v5}, Lorg/chromium/chrome/browser/util/MathUtils;->clamp(FFF)F

    move-result v9

    .line 971
    invoke-virtual {v7, v9}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->setDrawX(F)V

    .line 972
    invoke-virtual {v7}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->getOffsetY()F

    move-result v2

    invoke-virtual {v7, v2}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->setDrawY(F)V

    .line 975
    const/high16 v2, 0x3f800000    # 1.0f

    .line 976
    if-eq v0, v3, :cond_e

    .line 977
    invoke-virtual {v7}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->getWidth()F

    move-result v2

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-static {v2, v5}, Ljava/lang/Math;->max(FF)F

    move-result v10

    .line 978
    invoke-static {}, Lorg/chromium/ui/base/LocalizationUtils;->isLayoutRtl()Z

    move-result v2

    if-eqz v2, :cond_9

    if-le v0, v3, :cond_8

    const/4 v2, 0x1

    move v5, v2

    .line 980
    :goto_5
    if-nez v5, :cond_b

    add-float v2, v4, v6

    .line 982
    :goto_6
    if-eqz v5, :cond_1

    sub-float/2addr v1, v6

    .line 985
    :cond_1
    add-float v4, v8, v10

    invoke-static {v4, v1}, Ljava/lang/Math;->min(FF)F

    move-result v1

    invoke-static {v8, v2}, Ljava/lang/Math;->max(FF)F

    move-result v2

    sub-float/2addr v1, v2

    .line 987
    div-float/2addr v1, v10

    const/4 v2, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-static {v1, v2, v4}, Lorg/chromium/chrome/browser/util/MathUtils;->clamp(FFF)F

    move-result v1

    .line 989
    :goto_7
    invoke-virtual {v7, v1}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->setVisiblePercentage(F)V

    .line 993
    iget-boolean v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mInReorderMode:Z

    if-eqz v1, :cond_c

    add-int/lit8 v1, v3, 0x1

    move v2, v1

    .line 996
    :goto_8
    const/4 v1, 0x0

    .line 997
    if-le v0, v2, :cond_3

    if-lez v0, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mStripStacker:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripStacker;

    invoke-interface {v2}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripStacker;->canSlideTitleText()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 999
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mStripTabs:[Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    add-int/lit8 v2, v0, -0x1

    aget-object v1, v1, v2

    .line 1000
    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->getWidth()F

    move-result v2

    iget v4, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mTabOverlapWidth:F

    sub-float/2addr v2, v4

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->getWidthWeight()F

    move-result v4

    mul-float/2addr v2, v4

    .line 1002
    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->getDrawX()F

    move-result v1

    .line 1003
    invoke-static {}, Lorg/chromium/ui/base/LocalizationUtils;->isLayoutRtl()Z

    move-result v4

    if-nez v4, :cond_2

    add-float/2addr v1, v2

    .line 1007
    :cond_2
    sub-float/2addr v1, v9

    const/4 v4, 0x0

    invoke-static {v1, v4}, Ljava/lang/Math;->max(FF)F

    move-result v1

    .line 1008
    invoke-static {}, Lorg/chromium/ui/base/LocalizationUtils;->isLayoutRtl()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1010
    sub-float v1, v2, v1

    .line 1014
    :cond_3
    invoke-virtual {v7, v1}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->setContentOffsetX(F)V

    .line 940
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_2

    .line 936
    :cond_4
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 937
    :cond_5
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 947
    :cond_6
    const/4 v1, 0x4

    invoke-static {v1, v3}, Ljava/lang/Math;->min(II)I

    move-result v1

    const/4 v2, 0x4

    sub-int v4, v0, v3

    invoke-static {v2, v4}, Ljava/lang/Math;->min(II)I

    move-result v2

    add-int/2addr v2, v1

    goto/16 :goto_3

    .line 951
    :cond_7
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mStripTabs:[Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    sub-int/2addr v1, v3

    const/4 v4, 0x4

    invoke-static {v1, v4}, Ljava/lang/Math;->min(II)I

    move-result v1

    sub-int v4, v3, v0

    const/4 v5, 0x4

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v4

    add-int/2addr v1, v4

    goto/16 :goto_4

    .line 978
    :cond_8
    const/4 v2, 0x0

    move v5, v2

    goto/16 :goto_5

    :cond_9
    if-ge v0, v3, :cond_a

    const/4 v2, 0x1

    move v5, v2

    goto/16 :goto_5

    :cond_a
    const/4 v2, 0x0

    move v5, v2

    goto/16 :goto_5

    :cond_b
    move v2, v4

    .line 980
    goto/16 :goto_6

    :cond_c
    move v2, v3

    .line 993
    goto :goto_8

    .line 1016
    :cond_d
    return-void

    :cond_e
    move v1, v2

    goto/16 :goto_7
.end method

.method private createRenderList()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1020
    move v0, v1

    move v2, v1

    .line 1021
    :goto_0
    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mStripTabsVisuallyOrdered:[Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    array-length v3, v3

    if-ge v0, v3, :cond_1

    .line 1022
    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mStripTabsVisuallyOrdered:[Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    aget-object v3, v3, v0

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->isVisible()Z

    move-result v3

    if-eqz v3, :cond_0

    add-int/lit8 v2, v2, 0x1

    .line 1021
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1026
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mStripTabsToRender:[Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    array-length v0, v0

    if-eq v0, v2, :cond_2

    .line 1027
    new-array v0, v2, [Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mStripTabsToRender:[Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    :cond_2
    move v0, v1

    .line 1032
    :goto_1
    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mStripTabsVisuallyOrdered:[Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    array-length v2, v2

    if-ge v0, v2, :cond_4

    .line 1033
    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mStripTabsVisuallyOrdered:[Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->isVisible()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1034
    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mStripTabsToRender:[Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    add-int/lit8 v2, v1, 0x1

    iget-object v4, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mStripTabsVisuallyOrdered:[Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    aget-object v4, v4, v0

    aput-object v4, v3, v1

    move v1, v2

    .line 1032
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1037
    :cond_4
    return-void
.end method

.method private createStripTab(I)Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;
    .locals 6

    .prologue
    .line 812
    new-instance v0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mTabLoadTrackerHost:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/TabLoadTracker$TabLoadTrackerCallback;

    iget-object v4, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mRenderHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;

    iget-boolean v5, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mIncognito:Z

    move v2, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;-><init>(Landroid/content/Context;ILcom/google/android/apps/chrome/tabs/layout/tablet/helper/TabLoadTracker$TabLoadTrackerCallback;Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;Z)V

    .line 814
    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mHeight:F

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->setHeight(F)V

    .line 815
    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->pushStackerPropertiesToTab(Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;)V

    .line 816
    return-object v0
.end method

.method private findIndexForTab(I)I
    .locals 3

    .prologue
    const/4 v1, -0x1

    .line 838
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mStripTabs:[Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    if-nez v0, :cond_1

    move v0, v1

    .line 842
    :cond_0
    :goto_0
    return v0

    .line 839
    :cond_1
    const/4 v0, 0x0

    :goto_1
    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mStripTabs:[Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 840
    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mStripTabs:[Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->getId()I

    move-result v2

    if-eq v2, p1, :cond_0

    .line 839
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    move v0, v1

    .line 842
    goto :goto_0
.end method

.method private getTabAtPosition(F)Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;
    .locals 4

    .prologue
    .line 1111
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mStripTabsVisuallyOrdered:[Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    .line 1112
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mStripTabsVisuallyOrdered:[Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    aget-object v0, v0, v1

    .line 1113
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->isVisible()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->getDrawX()F

    move-result v2

    cmpg-float v2, v2, p1

    if-gtz v2, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->getDrawX()F

    move-result v2

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->getWidth()F

    move-result v3

    add-float/2addr v2, v3

    cmpg-float v2, p1, v2

    if-gtz v2, :cond_0

    .line 1118
    :goto_1
    return-object v0

    .line 1111
    :cond_0
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 1118
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private handleReorderAutoScrolling(J)V
    .locals 11

    .prologue
    const/high16 v10, 0x447a0000    # 1000.0f

    const v7, 0x42aecccd    # 87.4f

    const v9, 0x41933333    # 18.4f

    const/high16 v8, -0x3d760000    # -69.0f

    const/4 v1, 0x0

    .line 1284
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mInReorderMode:Z

    if-nez v0, :cond_0

    .line 1327
    :goto_0
    return-void

    .line 1287
    :cond_0
    iget-wide v2, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mLastReorderScrollTime:J

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-nez v0, :cond_1

    move v0, v1

    .line 1289
    :goto_1
    iput-wide p1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mLastReorderScrollTime:J

    .line 1291
    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mInteractingTab:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->getDrawX()F

    move-result v2

    .line 1296
    iget v3, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mLeftMargin:F

    add-float/2addr v3, v7

    .line 1298
    iget v4, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mLeftMargin:F

    add-float/2addr v4, v9

    .line 1299
    iget v5, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mWidth:F

    iget v6, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mLeftMargin:F

    sub-float/2addr v5, v6

    iget v6, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mRightMargin:F

    sub-float/2addr v5, v6

    sub-float/2addr v5, v7

    .line 1301
    iget v6, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mWidth:F

    iget v7, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mLeftMargin:F

    sub-float/2addr v6, v7

    iget v7, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mRightMargin:F

    sub-float/2addr v6, v7

    sub-float/2addr v6, v9

    .line 1308
    iget v7, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mReorderState:I

    and-int/lit8 v7, v7, 0x1

    if-eqz v7, :cond_2

    cmpg-float v7, v2, v3

    if-gez v7, :cond_2

    .line 1309
    invoke-static {v2, v4}, Ljava/lang/Math;->max(FF)F

    move-result v2

    sub-float v2, v3, v2

    neg-float v2, v2

    div-float/2addr v2, v8

    .line 1314
    :goto_2
    invoke-static {}, Lorg/chromium/ui/base/LocalizationUtils;->isLayoutRtl()Z

    move-result v3

    invoke-static {v2, v3}, Lcom/google/android/apps/chrome/utilities/ChromeMathUtils;->flipSignIf(FZ)F

    move-result v2

    .line 1317
    cmpl-float v1, v2, v1

    if-eqz v1, :cond_3

    .line 1319
    mul-float v1, v10, v2

    .line 1320
    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mScrollOffset:I

    int-to-float v2, v2

    mul-float/2addr v0, v1

    add-float/2addr v0, v2

    float-to-int v0, v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->updateScrollOffsetPosition(I)V

    .line 1322
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mUpdateHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;->requestUpdate()V

    goto :goto_0

    .line 1287
    :cond_1
    iget-wide v2, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mLastReorderScrollTime:J

    sub-long v2, p1, v2

    long-to-float v0, v2

    div-float/2addr v0, v10

    goto :goto_1

    .line 1310
    :cond_2
    iget v3, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mReorderState:I

    and-int/lit8 v3, v3, 0x2

    if-eqz v3, :cond_4

    iget v3, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mCachedTabWidth:F

    add-float/2addr v3, v2

    cmpl-float v3, v3, v5

    if-lez v3, :cond_4

    .line 1311
    iget v3, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mCachedTabWidth:F

    add-float/2addr v2, v3

    invoke-static {v2, v6}, Ljava/lang/Math;->min(FF)F

    move-result v2

    sub-float/2addr v2, v5

    div-float/2addr v2, v8

    goto :goto_2

    .line 1325
    :cond_3
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mLastReorderScrollTime:J

    goto :goto_0

    :cond_4
    move v2, v1

    goto :goto_2
.end method

.method private static moveElement([Ljava/lang/Object;II)V
    .locals 0

    .prologue
    .line 1387
    if-gt p1, p2, :cond_0

    .line 1388
    invoke-static {p0, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->moveElementUp([Ljava/lang/Object;II)V

    .line 1392
    :goto_0
    return-void

    .line 1390
    :cond_0
    invoke-static {p0, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->moveElementDown([Ljava/lang/Object;II)V

    goto :goto_0
.end method

.method private static moveElementDown([Ljava/lang/Object;II)V
    .locals 4

    .prologue
    .line 1406
    sget-boolean v0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-ge p1, p2, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1407
    :cond_0
    if-ne p1, p2, :cond_1

    .line 1414
    :goto_0
    return-void

    .line 1409
    :cond_1
    aget-object v1, p0, p1

    .line 1410
    add-int/lit8 v0, p1, -0x1

    :goto_1
    if-lt v0, p2, :cond_2

    .line 1411
    add-int/lit8 v2, v0, 0x1

    aget-object v3, p0, v0

    aput-object v3, p0, v2

    .line 1410
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    .line 1413
    :cond_2
    aput-object v1, p0, p2

    goto :goto_0
.end method

.method private static moveElementUp([Ljava/lang/Object;II)V
    .locals 2

    .prologue
    .line 1395
    sget-boolean v0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-le p1, p2, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1396
    :cond_0
    if-eq p1, p2, :cond_1

    add-int/lit8 v0, p1, 0x1

    if-ne v0, p2, :cond_2

    .line 1403
    :cond_1
    :goto_0
    return-void

    .line 1398
    :cond_2
    aget-object v0, p0, p1

    .line 1399
    :goto_1
    add-int/lit8 v1, p2, -0x1

    if-ge p1, v1, :cond_3

    .line 1400
    add-int/lit8 v1, p1, 0x1

    aget-object v1, p0, v1

    aput-object v1, p0, p1

    .line 1399
    add-int/lit8 p1, p1, 0x1

    goto :goto_1

    .line 1402
    :cond_3
    add-int/lit8 v1, p2, -0x1

    aput-object v0, p0, v1

    goto :goto_0
.end method

.method private onUpdateAnimation(JZ)Z
    .locals 7

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x1

    .line 629
    if-nez p3, :cond_0

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->updateScrollOffset(J)V

    .line 632
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->handleReorderAutoScrolling(J)V

    .line 637
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mLayoutAnimations:Lcom/google/android/apps/chrome/ChromeAnimation;

    if-eqz v0, :cond_8

    .line 638
    if-eqz p3, :cond_4

    .line 639
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mLayoutAnimations:Lcom/google/android/apps/chrome/ChromeAnimation;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeAnimation;->finished()Z

    move-result v0

    .line 643
    :goto_0
    if-nez p3, :cond_1

    if-eqz v0, :cond_2

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->finishAnimation()V

    :cond_2
    move v2, v1

    .line 649
    :goto_1
    iget-object v4, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mStripTabs:[Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    array-length v4, v4

    if-ge v3, v4, :cond_5

    .line 650
    iget-object v4, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mStripTabs:[Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    aget-object v4, v4, v3

    .line 651
    invoke-virtual {v4}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->isAnimating()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 653
    invoke-virtual {v4, p1, p2, p3}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->onUpdateAnimation(JZ)Z

    move-result v2

    and-int/2addr v0, v2

    move v2, v1

    .line 649
    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 641
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mLayoutAnimations:Lcom/google/android/apps/chrome/ChromeAnimation;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/chrome/ChromeAnimation;->update(J)Z

    move-result v0

    goto :goto_0

    .line 658
    :cond_5
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->updateSpinners(J)V

    .line 661
    if-eqz p3, :cond_6

    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mScroller:Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;

    invoke-virtual {v3, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->forceFinished(Z)V

    .line 664
    :cond_6
    if-eqz v2, :cond_7

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mUpdateHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;

    invoke-interface {v1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;->requestUpdate()V

    .line 666
    :cond_7
    return v0

    :cond_8
    move v0, v1

    move v2, v3

    goto :goto_1
.end method

.method private pushStackerPropertiesToTab(Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;)V
    .locals 1

    .prologue
    .line 820
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mStripStacker:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripStacker;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripStacker;->canShowCloseButton()Z

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->setCanShowCloseButton(Z)V

    .line 822
    return-void
.end method

.method private reorderTab(IIIZ)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 1255
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->findTabById(I)Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    move-result-object v0

    .line 1256
    if-eqz v0, :cond_0

    if-ne p2, p3, :cond_1

    .line 1281
    :cond_0
    :goto_0
    return-void

    .line 1259
    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->findIndexForTab(I)I

    move-result v2

    .line 1260
    if-eq v2, p3, :cond_0

    .line 1264
    iget-boolean v3, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mInReorderMode:Z

    if-eqz v3, :cond_2

    if-eq v2, p2, :cond_2

    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mInteractingTab:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    if-eq v0, v3, :cond_0

    .line 1267
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mStripTabs:[Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    invoke-static {v0, v2, p3}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->moveElement([Ljava/lang/Object;II)V

    .line 1270
    if-ge v2, p3, :cond_3

    add-int/lit8 p3, p3, -0x1

    .line 1273
    :cond_3
    if-eqz p4, :cond_0

    .line 1274
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mCachedTabWidth:F

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mTabOverlapWidth:F

    sub-float v2, v0, v2

    .line 1275
    if-gt p2, p3, :cond_4

    move v0, v1

    .line 1276
    :goto_1
    int-to-float v3, v0

    mul-float/2addr v2, v3

    invoke-static {}, Lorg/chromium/ui/base/LocalizationUtils;->isLayoutRtl()Z

    move-result v3

    invoke-static {v2, v3}, Lcom/google/android/apps/chrome/utilities/ChromeMathUtils;->flipSignIf(FZ)F

    move-result v2

    .line 1278
    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mStripTabs:[Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    sub-int v0, p3, v0

    aget-object v0, v3, v0

    .line 1279
    invoke-static {v0, v2}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->buildTabMoveAnimation(Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;F)Lcom/google/android/apps/chrome/ChromeAnimation$Animation;

    move-result-object v0

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->startAnimation(Lcom/google/android/apps/chrome/ChromeAnimation$Animation;Z)V

    goto :goto_0

    .line 1275
    :cond_4
    const/4 v0, -0x1

    goto :goto_1
.end method

.method private resetResizeTimeout(Z)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1330
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mStripTabEventHandler:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper$StripTabEventHandler;

    invoke-virtual {v0, v4}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper$StripTabEventHandler;->hasMessages(I)Z

    move-result v0

    .line 1332
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mStripTabEventHandler:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper$StripTabEventHandler;

    invoke-virtual {v1, v4}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper$StripTabEventHandler;->removeMessages(I)V

    .line 1334
    :cond_0
    if-nez v0, :cond_1

    if-eqz p1, :cond_2

    .line 1335
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mStripTabEventHandler:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper$StripTabEventHandler;

    const-wide/16 v2, 0x5dc

    invoke-virtual {v0, v4, v2, v3}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper$StripTabEventHandler;->sendEmptyMessageAtTime(IJ)Z

    .line 1337
    :cond_2
    return-void
.end method

.method private resizeTabStrip(Z)V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 794
    if-eqz p1, :cond_0

    .line 795
    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->resetResizeTimeout(Z)V

    .line 799
    :goto_0
    return-void

    .line 797
    :cond_0
    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->computeAndUpdateTabWidth(Z)V

    goto :goto_0
.end method

.method private startAnimation(Lcom/google/android/apps/chrome/ChromeAnimation$Animation;Z)V
    .locals 1

    .prologue
    .line 704
    if-eqz p2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->finishAnimation()V

    .line 706
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mLayoutAnimations:Lcom/google/android/apps/chrome/ChromeAnimation;

    if-nez v0, :cond_1

    .line 707
    new-instance v0, Lcom/google/android/apps/chrome/ChromeAnimation;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/ChromeAnimation;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mLayoutAnimations:Lcom/google/android/apps/chrome/ChromeAnimation;

    .line 710
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mLayoutAnimations:Lcom/google/android/apps/chrome/ChromeAnimation;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/ChromeAnimation;->add(Lcom/google/android/apps/chrome/ChromeAnimation$Animation;)V

    .line 712
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mUpdateHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;->requestUpdate()V

    .line 713
    return-void
.end method

.method private startReorderMode(JFF)V
    .locals 9

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1145
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mInReorderMode:Z

    if-eqz v0, :cond_1

    .line 1173
    :cond_0
    :goto_0
    return-void

    .line 1148
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mLastPressedCloseButton:Lcom/google/android/apps/chrome/tabs/CompositorButton;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mLastPressedCloseButton:Lcom/google/android/apps/chrome/tabs/CompositorButton;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/CompositorButton;->isPressed()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1149
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mLastPressedCloseButton:Lcom/google/android/apps/chrome/tabs/CompositorButton;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/chrome/tabs/CompositorButton;->setPressed(Z)V

    .line 1151
    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mLastPressedCloseButton:Lcom/google/android/apps/chrome/tabs/CompositorButton;

    .line 1154
    invoke-direct {p0, p4}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->getTabAtPosition(F)Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mInteractingTab:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    .line 1155
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mInteractingTab:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    if-eqz v0, :cond_0

    .line 1158
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mLastReorderScrollTime:J

    .line 1159
    iput v3, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mReorderState:I

    .line 1160
    iput p4, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mLastReorderX:F

    .line 1161
    iput-boolean v4, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mInReorderMode:Z

    .line 1164
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mInteractingTab:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->getId()I

    move-result v2

    invoke-static {v1, v2}, Lorg/chromium/chrome/browser/tabmodel/TabModelUtils;->getTabIndexById(Lorg/chromium/chrome/browser/tabmodel/TabList;I)I

    move-result v1

    invoke-static {v0, v1}, Lorg/chromium/chrome/browser/tabmodel/TabModelUtils;->setIndex(Lorg/chromium/chrome/browser/tabmodel/TabModel;I)V

    .line 1168
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mInteractingTab:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    invoke-direct {p0, v0, v4, v4, v4}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->calculateOffsetToMakeTabVisible(Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;ZZZ)F

    move-result v0

    .line 1169
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mScroller:Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mScrollOffset:I

    float-to-int v4, v0

    const/16 v8, 0xfa

    move v5, v3

    move-wide v6, p1

    invoke-virtual/range {v1 .. v8}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->startScroll(IIIIJI)V

    .line 1172
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mUpdateHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;->requestUpdate()V

    goto :goto_0
.end method

.method private stopReorderMode()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1176
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mInReorderMode:Z

    if-nez v0, :cond_0

    .line 1189
    :goto_0
    return-void

    .line 1179
    :cond_0
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mLastReorderScrollTime:J

    .line 1180
    iput v2, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mReorderState:I

    .line 1181
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mLastReorderX:F

    .line 1182
    iput-boolean v2, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mInReorderMode:Z

    .line 1185
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mInteractingTab:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mInteractingTab:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->getOffsetX()F

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->buildTabMoveAnimation(Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;F)Lcom/google/android/apps/chrome/ChromeAnimation$Animation;

    move-result-object v0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->startAnimation(Lcom/google/android/apps/chrome/ChromeAnimation$Animation;Z)V

    .line 1188
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mUpdateHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;->requestUpdate()V

    goto :goto_0
.end method

.method private updateNewTabButtonState()V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1041
    iget-boolean v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mInReorderMode:Z

    if-eqz v1, :cond_0

    .line 1042
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mNewTabButton:Lcom/google/android/apps/chrome/tabs/CompositorButton;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/tabs/CompositorButton;->setVisible(Z)V

    .line 1067
    :goto_0
    return-void

    .line 1045
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mNewTabButton:Lcom/google/android/apps/chrome/tabs/CompositorButton;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/android/apps/chrome/tabs/CompositorButton;->setVisible(Z)V

    .line 1047
    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mWidth:F

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mRightMargin:F

    sub-float v2, v1, v2

    .line 1048
    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mLeftMargin:F

    .line 1050
    :goto_1
    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mStripTabs:[Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    array-length v3, v3

    if-ge v0, v3, :cond_1

    .line 1051
    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mStripTabs:[Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    aget-object v3, v3, v0

    .line 1052
    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->getWidth()F

    move-result v4

    iget v5, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mTabOverlapWidth:F

    sub-float/2addr v4, v5

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->getWidthWeight()F

    move-result v5

    mul-float/2addr v4, v5

    .line 1053
    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->getDrawX()F

    move-result v5

    add-float/2addr v4, v5

    invoke-static {v4, v1}, Ljava/lang/Math;->max(FF)F

    move-result v1

    .line 1054
    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->getDrawX()F

    move-result v3

    invoke-static {v3, v2}, Ljava/lang/Math;->min(FF)F

    move-result v2

    .line 1050
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1056
    :cond_1
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mTabOverlapWidth:F

    add-float/2addr v0, v1

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mWidth:F

    iget v3, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mRightMargin:F

    sub-float/2addr v1, v3

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 1057
    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mLeftMargin:F

    invoke-static {v2, v1}, Ljava/lang/Math;->max(FF)F

    move-result v1

    .line 1059
    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mTabOverlapWidth:F

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    sub-float/2addr v0, v2

    .line 1062
    invoke-static {}, Lorg/chromium/ui/base/LocalizationUtils;->isLayoutRtl()Z

    move-result v2

    if-nez v2, :cond_2

    .line 1063
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mNewTabButton:Lcom/google/android/apps/chrome/tabs/CompositorButton;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/tabs/CompositorButton;->setX(F)V

    goto :goto_0

    .line 1065
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mNewTabButton:Lcom/google/android/apps/chrome/tabs/CompositorButton;

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mNewTabButtonWidth:F

    sub-float/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/tabs/CompositorButton;->setX(F)V

    goto :goto_0
.end method

.method private updateReorderPosition(F)V
    .locals 13

    .prologue
    const/4 v4, -0x1

    const/4 v11, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1192
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mInReorderMode:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mInteractingTab:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    if-nez v0, :cond_1

    .line 1252
    :cond_0
    :goto_0
    return-void

    .line 1194
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mInteractingTab:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->getOffsetX()F

    move-result v0

    add-float v6, v0, p1

    .line 1195
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mInteractingTab:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->getId()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->findIndexForTab(I)I

    move-result v5

    .line 1198
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mCachedTabWidth:F

    iget v3, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mTabOverlapWidth:F

    sub-float v9, v0, v3

    .line 1199
    const v0, 0x3f07ae14    # 0.53f

    mul-float/2addr v0, v9

    .line 1203
    neg-float v3, v0

    cmpg-float v3, v6, v3

    if-gez v3, :cond_5

    move v7, v1

    .line 1204
    :goto_1
    cmpl-float v0, v6, v0

    if-lez v0, :cond_6

    move v0, v1

    .line 1205
    :goto_2
    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mStripTabs:[Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    array-length v3, v3

    add-int/lit8 v3, v3, -0x1

    if-ge v5, v3, :cond_7

    move v8, v1

    .line 1206
    :goto_3
    if-lez v5, :cond_8

    move v3, v1

    .line 1208
    :goto_4
    invoke-static {}, Lorg/chromium/ui/base/LocalizationUtils;->isLayoutRtl()Z

    move-result v10

    if-eqz v10, :cond_10

    .line 1214
    :goto_5
    if-eqz v7, :cond_9

    if-eqz v8, :cond_9

    .line 1215
    add-int/lit8 v0, v5, 0x2

    .line 1221
    :goto_6
    if-eq v0, v4, :cond_e

    .line 1224
    invoke-static {}, Lorg/chromium/ui/base/LocalizationUtils;->isLayoutRtl()Z

    move-result v3

    if-eqz v3, :cond_a

    if-ge v0, v5, :cond_2

    move v2, v1

    .line 1226
    :cond_2
    :goto_7
    invoke-static {v9, v2}, Lcom/google/android/apps/chrome/utilities/ChromeMathUtils;->flipSignIf(FZ)F

    move-result v2

    add-float/2addr v2, v6

    .line 1229
    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mInteractingTab:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->getId()I

    move-result v3

    invoke-direct {p0, v3, v5, v0, v1}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->reorderTab(IIIZ)V

    .line 1230
    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    iget-object v6, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mInteractingTab:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    invoke-virtual {v6}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->getId()I

    move-result v6

    invoke-interface {v3, v6, v0}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->moveTab(II)V

    .line 1233
    if-le v0, v5, :cond_b

    :goto_8
    add-int v0, v5, v1

    .line 1236
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->updateVisualTabOrdering()V

    move v1, v0

    move v0, v2

    .line 1241
    :goto_9
    if-nez v1, :cond_3

    .line 1242
    invoke-static {}, Lorg/chromium/ui/base/LocalizationUtils;->isLayoutRtl()Z

    move-result v2

    if-eqz v2, :cond_c

    invoke-static {v11, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 1245
    :cond_3
    :goto_a
    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mStripTabs:[Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ne v1, v2, :cond_4

    .line 1246
    invoke-static {}, Lorg/chromium/ui/base/LocalizationUtils;->isLayoutRtl()Z

    move-result v1

    if-eqz v1, :cond_d

    invoke-static {v11, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 1251
    :cond_4
    :goto_b
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mInteractingTab:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->setOffsetX(F)V

    goto/16 :goto_0

    :cond_5
    move v7, v2

    .line 1203
    goto :goto_1

    :cond_6
    move v0, v2

    .line 1204
    goto :goto_2

    :cond_7
    move v8, v2

    .line 1205
    goto :goto_3

    :cond_8
    move v3, v2

    .line 1206
    goto :goto_4

    .line 1216
    :cond_9
    if-eqz v0, :cond_f

    if-eqz v3, :cond_f

    .line 1217
    add-int/lit8 v0, v5, -0x1

    goto :goto_6

    .line 1224
    :cond_a
    if-le v0, v5, :cond_2

    move v2, v1

    goto :goto_7

    :cond_b
    move v1, v4

    .line 1233
    goto :goto_8

    .line 1242
    :cond_c
    invoke-static {v11, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    goto :goto_a

    .line 1246
    :cond_d
    invoke-static {v11, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    goto :goto_b

    :cond_e
    move v1, v5

    move v0, v6

    goto :goto_9

    :cond_f
    move v0, v4

    goto :goto_6

    :cond_10
    move v12, v7

    move v7, v0

    move v0, v12

    goto :goto_5
.end method

.method private updateScrollOffset(J)V
    .locals 1

    .prologue
    .line 747
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mScroller:Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->computeScrollOffset(J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 748
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mScroller:Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->getCurrX()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->updateScrollOffsetPosition(I)V

    .line 749
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mUpdateHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;->requestUpdate()V

    .line 751
    :cond_0
    return-void
.end method

.method private updateScrollOffsetLimits()V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 755
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mWidth:F

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mLeftMargin:F

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mRightMargin:F

    sub-float v3, v0, v1

    .line 759
    const/4 v0, 0x0

    move v1, v2

    :goto_0
    iget-object v4, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mStripTabs:[Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    array-length v4, v4

    if-ge v0, v4, :cond_0

    .line 760
    iget-object v4, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mStripTabs:[Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    aget-object v4, v4, v0

    .line 761
    invoke-virtual {v4}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->getWidth()F

    move-result v5

    iget v6, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mTabOverlapWidth:F

    sub-float/2addr v5, v6

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->getWidthWeight()F

    move-result v4

    mul-float/2addr v4, v5

    add-float/2addr v1, v4

    .line 759
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 765
    :cond_0
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mTabOverlapWidth:F

    add-float/2addr v0, v1

    .line 768
    sub-float v0, v3, v0

    invoke-static {v2, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mMinScrollOffset:F

    .line 769
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mMinScrollOffset:F

    const v1, -0x457ced91    # -0.001f

    cmpl-float v0, v0, v1

    if-lez v0, :cond_1

    iput v2, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mMinScrollOffset:F

    .line 772
    :cond_1
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mScrollOffset:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->updateScrollOffsetPosition(I)V

    .line 773
    return-void
.end method

.method private updateScrollOffsetPosition(I)V
    .locals 3

    .prologue
    .line 736
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mScrollOffset:I

    .line 737
    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mMinScrollOffset:F

    float-to-int v1, v1

    const/4 v2, 0x0

    invoke-static {p1, v1, v2}, Lorg/chromium/chrome/browser/util/MathUtils;->clamp(III)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mScrollOffset:I

    .line 739
    iget-boolean v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mInReorderMode:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mScroller:Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->isFinished()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 740
    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mScrollOffset:I

    sub-int/2addr v0, v1

    invoke-static {}, Lorg/chromium/ui/base/LocalizationUtils;->isLayoutRtl()Z

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/utilities/ChromeMathUtils;->flipSignIf(IZ)I

    move-result v0

    .line 742
    int-to-float v0, v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->updateReorderPosition(F)V

    .line 744
    :cond_0
    return-void
.end method

.method private updateSpinners(J)V
    .locals 7

    .prologue
    const/4 v5, 0x2

    const/4 v0, 0x0

    .line 716
    iget-wide v2, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mLastSpinnerUpdate:J

    sub-long v2, p1, v2

    .line 717
    long-to-float v1, v2

    const v2, 0x3ea8f5c3    # 0.33f

    mul-float/2addr v2, v1

    move v1, v0

    .line 719
    :goto_0
    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mStripTabs:[Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    array-length v3, v3

    if-ge v0, v3, :cond_1

    .line 720
    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mStripTabs:[Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    aget-object v3, v3, v0

    .line 722
    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->isLoading()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 723
    invoke-virtual {v3, v2}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->addLoadingSpinnerRotation(F)V

    .line 724
    const/4 v1, 0x1

    .line 719
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 727
    :cond_1
    iput-wide p1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mLastSpinnerUpdate:J

    .line 728
    if-eqz v1, :cond_2

    .line 729
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mStripTabEventHandler:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper$StripTabEventHandler;

    invoke-virtual {v0, v5}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper$StripTabEventHandler;->removeMessages(I)V

    .line 730
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mStripTabEventHandler:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper$StripTabEventHandler;

    const-wide/16 v2, 0x42

    invoke-virtual {v0, v5, v2, v3}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper$StripTabEventHandler;->sendEmptyMessageDelayed(IJ)Z

    .line 733
    :cond_2
    return-void
.end method

.method private updateStrip(JJ)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 877
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    if-nez v1, :cond_1

    .line 910
    :cond_0
    :goto_0
    return-void

    .line 881
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mStripTabs:[Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    invoke-interface {v1}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->getCount()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mStripTabs:[Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    array-length v2, v2

    if-eq v1, v2, :cond_3

    .line 882
    :cond_2
    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->computeAndUpdateTabOrders(Z)V

    .line 886
    :cond_3
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->updateScrollOffsetLimits()V

    .line 889
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->computeTabInitialPositions()V

    .line 892
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->computeTabOffsetHelper()V

    .line 895
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mStripStacker:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripStacker;

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    invoke-interface {v2}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->index()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mStripTabs:[Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    invoke-interface {v1, v2, v3}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripStacker;->performOcclusionPass(I[Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;)V

    .line 898
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->createRenderList()V

    .line 901
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->updateNewTabButtonState()V

    .line 904
    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mStripTabs:[Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 905
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mStripTabs:[Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->isAnimating()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 906
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mUpdateHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;->requestUpdate()V

    goto :goto_0

    .line 904
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method private updateVisualTabOrdering()V
    .locals 4

    .prologue
    .line 802
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mStripTabs:[Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    array-length v0, v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mStripTabsVisuallyOrdered:[Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    array-length v1, v1

    if-eq v0, v1, :cond_0

    .line 803
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mStripTabs:[Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    array-length v0, v0

    new-array v0, v0, [Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mStripTabsVisuallyOrdered:[Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    .line 806
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mStripStacker:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripStacker;

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    invoke-interface {v1}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->index()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mStripTabs:[Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mStripTabsVisuallyOrdered:[Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripStacker;->createVisualOrdering(I[Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;[Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;)V

    .line 808
    return-void
.end method


# virtual methods
.method public click(JFF)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 576
    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->resetResizeTimeout(Z)V

    .line 578
    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mNewTabButton:Lcom/google/android/apps/chrome/tabs/CompositorButton;

    invoke-virtual {v2, p3, p4}, Lcom/google/android/apps/chrome/tabs/CompositorButton;->click(FF)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    if-eqz v2, :cond_1

    .line 579
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mTabCreator:Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;->launchNTP()Lcom/google/android/apps/chrome/tab/ChromeTab;

    .line 605
    :cond_0
    :goto_0
    return-void

    .line 583
    :cond_1
    invoke-direct {p0, p3}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->getTabAtPosition(F)Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    move-result-object v2

    .line 584
    if-eqz v2, :cond_0

    .line 585
    invoke-virtual {v2, p3, p4}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->checkCloseHitTest(FF)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 587
    invoke-static {v2}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->buildTabClosedAnimation(Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;)Lcom/google/android/apps/chrome/ChromeAnimation$Animation;

    move-result-object v3

    invoke-direct {p0, v3, v0}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->startAnimation(Lcom/google/android/apps/chrome/ChromeAnimation$Animation;Z)V

    .line 590
    invoke-virtual {v2, v0}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->setIsDying(Z)V

    .line 593
    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->getId()I

    move-result v4

    invoke-interface {v3, v4}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->getNextTabIfClosed(I)Lorg/chromium/chrome/browser/Tab;

    move-result-object v3

    .line 594
    if-eqz v3, :cond_2

    invoke-virtual {v3}, Lorg/chromium/chrome/browser/Tab;->getId()I

    move-result v3

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->getId()I

    move-result v4

    invoke-virtual {p0, p1, p2, v3, v4}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->tabSelected(JII)V

    .line 597
    :cond_2
    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mStripTabs:[Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    iget-object v4, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mStripTabs:[Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    array-length v4, v4

    add-int/lit8 v4, v4, -0x1

    aget-object v3, v3, v4

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->getId()I

    move-result v3

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->getId()I

    move-result v2

    if-ne v3, v2, :cond_3

    move v2, v0

    .line 600
    :goto_1
    if-nez v2, :cond_4

    :goto_2
    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->resizeTabStrip(Z)V

    goto :goto_0

    :cond_3
    move v2, v1

    .line 597
    goto :goto_1

    :cond_4
    move v0, v1

    .line 600
    goto :goto_2

    .line 602
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->getId()I

    move-result v1

    invoke-static {v0, v1}, Lorg/chromium/chrome/browser/tabmodel/TabModelUtils;->getTabIndexById(Lorg/chromium/chrome/browser/tabmodel/TabList;I)I

    move-result v0

    .line 603
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    invoke-static {v1, v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelUtils;->setIndex(Lorg/chromium/chrome/browser/tabmodel/TabModel;I)V

    goto :goto_0
.end method

.method public drag(JFFFFFF)V
    .locals 9

    .prologue
    .line 427
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->resetResizeTimeout(Z)V

    .line 429
    invoke-static {}, Lorg/chromium/ui/base/LocalizationUtils;->isLayoutRtl()Z

    move-result v0

    invoke-static {p5, v0}, Lcom/google/android/apps/chrome/utilities/ChromeMathUtils;->flipSignIf(FZ)F

    move-result v0

    .line 432
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mNewTabButton:Lcom/google/android/apps/chrome/tabs/CompositorButton;

    invoke-virtual {v1, p3, p4}, Lcom/google/android/apps/chrome/tabs/CompositorButton;->drag(FF)Z

    .line 433
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mLastPressedCloseButton:Lcom/google/android/apps/chrome/tabs/CompositorButton;

    if-eqz v1, :cond_0

    .line 434
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mLastPressedCloseButton:Lcom/google/android/apps/chrome/tabs/CompositorButton;

    invoke-virtual {v1, p3, p4}, Lcom/google/android/apps/chrome/tabs/CompositorButton;->drag(FF)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mLastPressedCloseButton:Lcom/google/android/apps/chrome/tabs/CompositorButton;

    .line 437
    :cond_0
    iget-boolean v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mInReorderMode:Z

    if-eqz v1, :cond_8

    .line 441
    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mLastReorderX:F

    sub-float v1, p3, v1

    .line 443
    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v2

    const/high16 v3, 0x3f800000    # 1.0f

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_2

    .line 444
    invoke-static {}, Lorg/chromium/ui/base/LocalizationUtils;->isLayoutRtl()Z

    move-result v2

    if-nez v2, :cond_6

    .line 445
    const/high16 v2, 0x3f800000    # 1.0f

    cmpl-float v2, v0, v2

    if-ltz v2, :cond_5

    .line 446
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mReorderState:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mReorderState:I

    .line 458
    :cond_1
    :goto_0
    iput p3, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mLastReorderX:F

    .line 459
    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->updateReorderPosition(F)V

    .line 481
    :cond_2
    :goto_1
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mInReorderMode:Z

    if-nez v0, :cond_3

    .line 482
    invoke-static/range {p7 .. p7}, Ljava/lang/Math;->abs(F)F

    move-result v0

    .line 483
    invoke-static/range {p8 .. p8}, Ljava/lang/Math;->abs(F)F

    move-result v1

    .line 484
    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mReorderMoveStartThreshold:F

    cmpl-float v2, p8, v2

    if-lez v2, :cond_3

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mReorderMoveStartThreshold:F

    const/high16 v3, 0x40000000    # 2.0f

    mul-float/2addr v2, v3

    cmpg-float v2, v0, v2

    if-gez v2, :cond_3

    const v2, 0x3a83126f    # 0.001f

    cmpl-float v2, v0, v2

    if-lez v2, :cond_3

    div-float v0, v1, v0

    sget v1, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->TAN_OF_REORDER_ANGLE_START_THRESHOLD:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_3

    .line 487
    sub-float v0, p3, p7

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->startReorderMode(JFF)V

    .line 494
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mInReorderMode:Z

    if-nez v0, :cond_4

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mInteractingTab:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    .line 495
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mUpdateHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;->requestUpdate()V

    .line 496
    return-void

    .line 447
    :cond_5
    const/high16 v2, -0x40800000    # -1.0f

    cmpg-float v0, v0, v2

    if-gtz v0, :cond_1

    .line 448
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mReorderState:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mReorderState:I

    goto :goto_0

    .line 451
    :cond_6
    const/high16 v2, 0x3f800000    # 1.0f

    cmpl-float v2, v0, v2

    if-ltz v2, :cond_7

    .line 452
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mReorderState:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mReorderState:I

    goto :goto_0

    .line 453
    :cond_7
    const/high16 v2, -0x40800000    # -1.0f

    cmpg-float v0, v0, v2

    if-gtz v0, :cond_1

    .line 454
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mReorderState:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mReorderState:I

    goto :goto_0

    .line 461
    :cond_8
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mScroller:Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->isFinished()Z

    move-result v1

    if-nez v1, :cond_9

    .line 463
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mScroller:Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mScroller:Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->getFinalX()I

    move-result v2

    int-to-float v2, v2

    add-float/2addr v0, v2

    float-to-int v0, v0

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->setFinalX(I)V

    goto :goto_1

    .line 466
    :cond_9
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mInteractingTab:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    const/4 v2, 0x1

    const/4 v3, 0x1

    const/4 v4, 0x1

    invoke-direct {p0, v1, v2, v3, v4}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->calculateOffsetToMakeTabVisible(Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;ZZZ)F

    move-result v4

    .line 469
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mInteractingTab:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    if-eqz v1, :cond_c

    const/4 v1, 0x0

    cmpl-float v1, v4, v1

    if-eqz v1, :cond_c

    .line 470
    const/4 v1, 0x0

    cmpl-float v1, v4, v1

    if-lez v1, :cond_a

    const/4 v1, 0x0

    cmpl-float v1, v0, v1

    if-gtz v1, :cond_b

    :cond_a
    const/4 v1, 0x0

    cmpg-float v1, v4, v1

    if-gez v1, :cond_2

    const/4 v1, 0x0

    cmpg-float v0, v0, v1

    if-gez v0, :cond_2

    .line 472
    :cond_b
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mScroller:Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mScrollOffset:I

    const/4 v3, 0x0

    float-to-int v4, v4

    const/4 v5, 0x0

    const/16 v8, 0xfa

    move-wide v6, p1

    invoke-virtual/range {v1 .. v8}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->startScroll(IIIIJI)V

    goto/16 :goto_1

    .line 476
    :cond_c
    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mScrollOffset:I

    int-to-float v1, v1

    add-float/2addr v0, v1

    float-to-int v0, v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->updateScrollOffsetPosition(I)V

    goto/16 :goto_1
.end method

.method public findTabById(I)Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 830
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mStripTabs:[Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    if-nez v0, :cond_0

    move-object v0, v1

    .line 834
    :goto_0
    return-object v0

    .line 831
    :cond_0
    const/4 v0, 0x0

    :goto_1
    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mStripTabs:[Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 832
    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mStripTabs:[Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->getId()I

    move-result v2

    if-ne v2, p1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mStripTabs:[Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    aget-object v0, v1, v0

    goto :goto_0

    .line 831
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    move-object v0, v1

    .line 834
    goto :goto_0
.end method

.method public finishAnimation()V
    .locals 5

    .prologue
    .line 682
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mLayoutAnimations:Lcom/google/android/apps/chrome/ChromeAnimation;

    if-nez v0, :cond_1

    .line 701
    :cond_0
    :goto_0
    return-void

    .line 685
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mLayoutAnimations:Lcom/google/android/apps/chrome/ChromeAnimation;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeAnimation;->updateAndFinish()V

    .line 686
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mLayoutAnimations:Lcom/google/android/apps/chrome/ChromeAnimation;

    .line 689
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 690
    const/4 v0, 0x0

    :goto_1
    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mStripTabs:[Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 691
    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mStripTabs:[Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    aget-object v2, v2, v0

    .line 692
    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->isDying()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 690
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 696
    :cond_3
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    .line 697
    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->getId()I

    move-result v0

    const/4 v4, 0x1

    invoke-static {v3, v0, v4}, Lorg/chromium/chrome/browser/tabmodel/TabModelUtils;->closeTabById(Lorg/chromium/chrome/browser/tabmodel/TabModel;IZ)Z

    goto :goto_2

    .line 700
    :cond_4
    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mUpdateHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;->requestUpdate()V

    goto :goto_0
.end method

.method public fling(JFFFF)V
    .locals 17

    .prologue
    .line 507
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->resetResizeTimeout(Z)V

    .line 509
    invoke-static {}, Lorg/chromium/ui/base/LocalizationUtils;->isLayoutRtl()Z

    move-result v2

    move/from16 v0, p5

    invoke-static {v0, v2}, Lcom/google/android/apps/chrome/utilities/ChromeMathUtils;->flipSignIf(FZ)F

    move-result v6

    .line 512
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mInReorderMode:Z

    if-eqz v2, :cond_0

    .line 529
    :goto_0
    return-void

    .line 516
    :cond_0
    const/4 v2, 0x0

    .line 517
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mScroller:Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->isFinished()Z

    move-result v3

    if-nez v3, :cond_1

    .line 518
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mScroller:Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->getFinalX()I

    move-result v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mScrollOffset:I

    sub-int/2addr v2, v3

    .line 520
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mInteractingTab:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    .line 521
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mScroller:Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->forceFinished(Z)V

    .line 525
    :cond_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mScroller:Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mScrollOffset:I

    const/4 v5, 0x0

    float-to-int v6, v6

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget v8, v0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mMinScrollOffset:F

    float-to-int v8, v8

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-wide/from16 v14, p1

    invoke-virtual/range {v3 .. v15}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->fling(IIIIIIIIIIJ)V

    .line 527
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mScroller:Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mScroller:Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->getFinalX()I

    move-result v4

    add-int/2addr v2, v4

    invoke-virtual {v3, v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->setFinalX(I)V

    .line 528
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mUpdateHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;

    invoke-interface {v2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;->requestUpdate()V

    goto :goto_0
.end method

.method public getNewTabButton()Lcom/google/android/apps/chrome/tabs/CompositorButton;
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mNewTabButton:Lcom/google/android/apps/chrome/tabs/CompositorButton;

    return-object v0
.end method

.method public getStripBrightness()F
    .locals 1

    .prologue
    .line 195
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mInReorderMode:Z

    if-eqz v0, :cond_0

    const/high16 v0, 0x3f400000    # 0.75f

    :goto_0
    return v0

    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_0
.end method

.method public getStripLayoutTabsToRender()[Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mStripTabsToRender:[Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    return-object v0
.end method

.method public getTabCount()I
    .locals 1

    .prologue
    .line 181
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mStripTabs:[Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    array-length v0, v0

    return v0
.end method

.method public isAnimating()Z
    .locals 1

    .prologue
    .line 674
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mLayoutAnimations:Lcom/google/android/apps/chrome/ChromeAnimation;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mScroller:Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->isFinished()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isForegroundTab(Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;)Z
    .locals 2

    .prologue
    .line 1141
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mStripTabsVisuallyOrdered:[Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mStripTabsVisuallyOrdered:[Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    aget-object v0, v0, v1

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onContextChanged(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 254
    new-instance v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;

    invoke-direct {v0, p1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mScroller:Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;

    .line 255
    iput-object p1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mContext:Landroid/content/Context;

    .line 256
    return-void
.end method

.method public onDown(JFF)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v5, 0x1

    const/4 v2, -0x1

    .line 538
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->resetResizeTimeout(Z)V

    .line 540
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mNewTabButton:Lcom/google/android/apps/chrome/tabs/CompositorButton;

    invoke-virtual {v0, p3, p4}, Lcom/google/android/apps/chrome/tabs/CompositorButton;->onDown(FF)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 556
    :cond_0
    :goto_0
    return-void

    .line 542
    :cond_1
    invoke-direct {p0, p3}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->getTabAtPosition(F)Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    move-result-object v3

    .line 543
    if-eqz v3, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->getId()I

    move-result v4

    invoke-static {v0, v4}, Lorg/chromium/chrome/browser/tabmodel/TabModelUtils;->getTabIndexById(Lorg/chromium/chrome/browser/tabmodel/TabList;I)I

    move-result v0

    .line 546
    :goto_1
    if-eq v0, v2, :cond_4

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mStripTabs:[Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    aget-object v0, v2, v0

    :goto_2
    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mInteractingTab:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    .line 547
    if-eqz v3, :cond_2

    invoke-virtual {v3, p3, p4}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->checkCloseHitTest(FF)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 548
    invoke-virtual {v3, v5}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->setClosePressed(Z)V

    .line 549
    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->getCloseButton()Lcom/google/android/apps/chrome/tabs/CompositorButton;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mLastPressedCloseButton:Lcom/google/android/apps/chrome/tabs/CompositorButton;

    .line 552
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mScroller:Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->isFinished()Z

    move-result v0

    if-nez v0, :cond_0

    .line 553
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mScroller:Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;

    invoke-virtual {v0, v5}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->forceFinished(Z)V

    .line 554
    iput-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mInteractingTab:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    goto :goto_0

    :cond_3
    move v0, v2

    .line 543
    goto :goto_1

    :cond_4
    move-object v0, v1

    .line 546
    goto :goto_2
.end method

.method public onLongPress(JFF)V
    .locals 1

    .prologue
    .line 565
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->resetResizeTimeout(Z)V

    .line 566
    invoke-direct {p0, p1, p2, p3, p3}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->startReorderMode(JFF)V

    .line 567
    return-void
.end method

.method public onSizeChanged(FF)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 238
    iput p1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mWidth:F

    .line 239
    iput p2, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mHeight:F

    move v0, v1

    .line 241
    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mStripTabs:[Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 242
    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mStripTabs:[Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    aget-object v2, v2, v0

    iget v3, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mHeight:F

    invoke-virtual {v2, v3}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->setHeight(F)V

    .line 241
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 245
    :cond_0
    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->computeAndUpdateTabWidth(Z)V

    .line 246
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mStripTabs:[Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    array-length v0, v0

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mUpdateHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;->requestUpdate()V

    .line 247
    :cond_1
    return-void
.end method

.method public onUpOrCancel(J)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 613
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mLastPressedCloseButton:Lcom/google/android/apps/chrome/tabs/CompositorButton;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mLastPressedCloseButton:Lcom/google/android/apps/chrome/tabs/CompositorButton;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/CompositorButton;->onUpOrCancel()Z

    .line 614
    :cond_0
    iput-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mLastPressedCloseButton:Lcom/google/android/apps/chrome/tabs/CompositorButton;

    .line 617
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->stopReorderMode()V

    .line 620
    iput-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mInteractingTab:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    .line 621
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mReorderState:I

    .line 622
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mNewTabButton:Lcom/google/android/apps/chrome/tabs/CompositorButton;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/CompositorButton;->onUpOrCancel()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    if-eqz v0, :cond_1

    .line 623
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mTabCreator:Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;->launchNTP()Lcom/google/android/apps/chrome/tab/ChromeTab;

    .line 625
    :cond_1
    return-void
.end method

.method public setRightMargin(F)V
    .locals 2

    .prologue
    .line 228
    iput p1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mRightMargin:F

    .line 229
    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mRightMargin:F

    invoke-static {}, Lorg/chromium/ui/base/LocalizationUtils;->isLayoutRtl()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mRightMargin:F

    .line 230
    return-void

    .line 229
    :cond_0
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mNewTabButtonWidth:F

    goto :goto_0
.end method

.method public setTabModel(Lorg/chromium/chrome/browser/tabmodel/TabModel;Lcom/google/android/apps/chrome/compositor/TabContentManager;Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;)V
    .locals 1

    .prologue
    .line 264
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    if-ne v0, p1, :cond_0

    .line 269
    :goto_0
    return-void

    .line 265
    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    .line 266
    iput-object p2, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mTabContentManager:Lcom/google/android/apps/chrome/compositor/TabContentManager;

    .line 267
    iput-object p3, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mTabCreator:Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;

    .line 268
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->computeAndUpdateTabOrders(Z)V

    goto :goto_0
.end method

.method public setTabStacker(Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripStacker;)V
    .locals 2

    .prologue
    .line 205
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mStripStacker:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripStacker;

    if-eq p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mUpdateHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;->requestUpdate()V

    .line 206
    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mStripStacker:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripStacker;

    .line 209
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mStripTabs:[Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 210
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mStripTabs:[Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    aget-object v1, v1, v0

    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->pushStackerPropertiesToTab(Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;)V

    .line 209
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 212
    :cond_1
    return-void
.end method

.method public tabClosed(JI)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 320
    invoke-virtual {p0, p3}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->findTabById(I)Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    move-result-object v2

    if-nez v2, :cond_0

    .line 330
    :goto_0
    return-void

    .line 324
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mStripTabs:[Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mStripTabs:[Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    array-length v3, v3

    add-int/lit8 v3, v3, -0x1

    aget-object v2, v2, v3

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->getId()I

    move-result v2

    if-ne v2, p3, :cond_1

    move v2, v0

    .line 327
    :goto_1
    if-nez v2, :cond_2

    :goto_2
    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->computeAndUpdateTabOrders(Z)V

    .line 329
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mUpdateHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;->requestUpdate()V

    goto :goto_0

    :cond_1
    move v2, v1

    .line 324
    goto :goto_1

    :cond_2
    move v0, v1

    .line 327
    goto :goto_2
.end method

.method public tabClosureCancelled(JI)V
    .locals 7

    .prologue
    .line 338
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    invoke-static {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelUtils;->getCurrentTabId(Lorg/chromium/chrome/browser/tabmodel/TabList;)I

    move-result v0

    if-ne v0, p3, :cond_0

    const/4 v6, 0x1

    .line 339
    :goto_0
    const/4 v5, -0x1

    move-object v1, p0

    move-wide v2, p1

    move v4, p3

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->tabCreated(JIIZ)V

    .line 340
    return-void

    .line 338
    :cond_0
    const/4 v6, 0x0

    goto :goto_0
.end method

.method public tabCreated(JIIZ)V
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 350
    invoke-virtual {p0, p3}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->findTabById(I)Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 377
    :goto_0
    return-void

    .line 353
    :cond_0
    invoke-direct {p0, v3}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->computeAndUpdateTabOrders(Z)V

    .line 356
    invoke-virtual {p0, p3}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->findTabById(I)Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    move-result-object v0

    .line 357
    if-eqz v0, :cond_1

    invoke-static {v0}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->buildTabCreatedAnimation(Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;)Lcom/google/android/apps/chrome/ChromeAnimation$Animation;

    move-result-object v2

    invoke-direct {p0, v2, v1}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->startAnimation(Lcom/google/android/apps/chrome/ChromeAnimation$Animation;Z)V

    .line 360
    :cond_1
    invoke-virtual {p0, p4}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->findTabById(I)Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    move-result-object v2

    .line 362
    if-nez p5, :cond_3

    move-object v2, v0

    move v0, v1

    .line 368
    :goto_1
    if-eqz v2, :cond_2

    .line 369
    invoke-direct {p0, v2, v3, v0, v1}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->calculateOffsetToMakeTabVisible(Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;ZZZ)F

    move-result v0

    .line 371
    const/4 v1, 0x0

    cmpl-float v1, v0, v1

    if-eqz v1, :cond_2

    .line 372
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mScroller:Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mScrollOffset:I

    float-to-int v4, v0

    const/16 v8, 0xfa

    move v5, v3

    move-wide v6, p1

    invoke-virtual/range {v1 .. v8}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackScroller;->startScroll(IIIIJI)V

    .line 376
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mUpdateHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;->requestUpdate()V

    goto :goto_0

    :cond_3
    move v0, v3

    goto :goto_1
.end method

.method public tabLoadFinished(I)V
    .locals 1

    .prologue
    .line 411
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->findTabById(I)Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    move-result-object v0

    .line 412
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->loadingFinished()V

    .line 413
    :cond_0
    return-void
.end method

.method public tabLoadStarted(I)V
    .locals 1

    .prologue
    .line 402
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->findTabById(I)Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    move-result-object v0

    .line 403
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->loadingStarted()V

    .line 404
    :cond_0
    return-void
.end method

.method public tabMoved(JIII)V
    .locals 1

    .prologue
    .line 307
    const/4 v0, 0x0

    invoke-direct {p0, p3, p4, p5, v0}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->reorderTab(IIIZ)V

    .line 309
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->updateVisualTabOrdering()V

    .line 310
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mUpdateHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;->requestUpdate()V

    .line 311
    return-void
.end method

.method public tabPageLoadFinished(I)V
    .locals 1

    .prologue
    .line 393
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->findTabById(I)Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    move-result-object v0

    .line 394
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->pageLoadingFinished()V

    .line 395
    :cond_0
    return-void
.end method

.method public tabPageLoadStarted(I)V
    .locals 1

    .prologue
    .line 384
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->findTabById(I)Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    move-result-object v0

    .line 385
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->pageLoadingStarted()V

    .line 386
    :cond_0
    return-void
.end method

.method public tabSelected(JII)V
    .locals 7

    .prologue
    .line 291
    invoke-virtual {p0, p3}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->findTabById(I)Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    move-result-object v0

    if-nez v0, :cond_0

    .line 292
    const/4 v6, 0x1

    move-object v1, p0

    move-wide v2, p1

    move v4, p3

    move v5, p4

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->tabCreated(JIIZ)V

    .line 297
    :goto_0
    return-void

    .line 294
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->updateVisualTabOrdering()V

    .line 295
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mUpdateHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;->requestUpdate()V

    goto :goto_0
.end method

.method public testFling(F)V
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 1432
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    move-object v1, p0

    move v5, v4

    move v6, p1

    move v7, v4

    invoke-virtual/range {v1 .. v7}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->fling(JFFFF)V

    .line 1433
    return-void
.end method

.method public testSetScrollOffset(I)V
    .locals 0

    .prologue
    .line 1422
    iput p1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mScrollOffset:I

    .line 1423
    return-void
.end method

.method public updateLayout(JJ)Z
    .locals 1

    .prologue
    .line 278
    const-string/jumbo v0, "StripLayoutHelper:updateLayout"

    invoke-static {v0}, Lorg/chromium/base/PerfTraceEvent;->instant(Ljava/lang/String;)V

    .line 279
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->onUpdateAnimation(JZ)Z

    move-result v0

    .line 280
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->updateStrip(JJ)V

    .line 281
    return v0
.end method

.method public visualIndexOfTab(Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;)I
    .locals 2

    .prologue
    .line 1127
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mStripTabsVisuallyOrdered:[Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 1128
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelper;->mStripTabsVisuallyOrdered:[Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    aget-object v1, v1, v0

    if-ne v1, p1, :cond_0

    .line 1132
    :goto_1
    return v0

    .line 1127
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1132
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method
