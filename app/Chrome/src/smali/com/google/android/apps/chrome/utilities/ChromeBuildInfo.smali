.class public Lcom/google/android/apps/chrome/utilities/ChromeBuildInfo;
.super Ljava/lang/Object;
.source "ChromeBuildInfo.java"


# static fields
.field private static sChannel:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    const/high16 v0, 0x10000000

    sput v0, Lcom/google/android/apps/chrome/utilities/ChromeBuildInfo;->sChannel:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getBuildChannel()I
    .locals 2

    .prologue
    .line 101
    sget v0, Lcom/google/android/apps/chrome/utilities/ChromeBuildInfo;->sChannel:I

    const/high16 v1, 0x10000000

    if-ne v0, v1, :cond_0

    .line 102
    new-instance v0, Ljava/lang/RuntimeException;

    const-string/jumbo v1, "ChromeBuildInfo.init() was not called"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 104
    :cond_0
    sget v0, Lcom/google/android/apps/chrome/utilities/ChromeBuildInfo;->sChannel:I

    return v0
.end method

.method public static init(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 45
    sget v0, Lcom/google/android/apps/chrome/R$string;->channel_name:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 46
    const-string/jumbo v1, "stable"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 47
    const/high16 v0, 0x10000

    sput v0, Lcom/google/android/apps/chrome/utilities/ChromeBuildInfo;->sChannel:I

    .line 57
    :goto_0
    return-void

    .line 48
    :cond_0
    const-string/jumbo v1, "beta"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 49
    const/16 v0, 0x1000

    sput v0, Lcom/google/android/apps/chrome/utilities/ChromeBuildInfo;->sChannel:I

    goto :goto_0

    .line 50
    :cond_1
    const-string/jumbo v1, "dev"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 51
    const/16 v0, 0x100

    sput v0, Lcom/google/android/apps/chrome/utilities/ChromeBuildInfo;->sChannel:I

    goto :goto_0

    .line 52
    :cond_2
    const-string/jumbo v1, "canary"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 53
    const/16 v0, 0x10

    sput v0, Lcom/google/android/apps/chrome/utilities/ChromeBuildInfo;->sChannel:I

    goto :goto_0

    .line 55
    :cond_3
    const/4 v0, 0x1

    sput v0, Lcom/google/android/apps/chrome/utilities/ChromeBuildInfo;->sChannel:I

    goto :goto_0
.end method

.method public static isBetaBuild()Z
    .locals 2

    .prologue
    .line 84
    invoke-static {}, Lcom/google/android/apps/chrome/utilities/ChromeBuildInfo;->getBuildChannel()I

    move-result v0

    const/16 v1, 0x1000

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isCanaryBuild()Z
    .locals 2

    .prologue
    .line 70
    invoke-static {}, Lcom/google/android/apps/chrome/utilities/ChromeBuildInfo;->getBuildChannel()I

    move-result v0

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isDevBuild()Z
    .locals 2

    .prologue
    .line 77
    invoke-static {}, Lcom/google/android/apps/chrome/utilities/ChromeBuildInfo;->getBuildChannel()I

    move-result v0

    const/16 v1, 0x100

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isLocalBuild()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 63
    invoke-static {}, Lcom/google/android/apps/chrome/utilities/ChromeBuildInfo;->getBuildChannel()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isStableBuild()Z
    .locals 2

    .prologue
    .line 91
    invoke-static {}, Lcom/google/android/apps/chrome/utilities/ChromeBuildInfo;->getBuildChannel()I

    move-result v0

    const/high16 v1, 0x10000

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
