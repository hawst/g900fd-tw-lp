.class public Lcom/google/android/apps/chrome/partnercustomizations/HomepageManager;
.super Ljava/lang/Object;
.source "HomepageManager.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getHomepageUri(Landroid/content/Context;)Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 37
    invoke-static {p0}, Lcom/google/android/apps/chrome/partnercustomizations/HomepageManager;->isHomepageEnabled(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    move-object v0, v1

    .line 44
    :cond_0
    :goto_0
    return-object v0

    .line 39
    :cond_1
    invoke-static {p0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v0

    .line 41
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getPrefHomepagePartnerEnabledPreference()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-static {}, Lcom/google/android/apps/chrome/partnercustomizations/PartnerBrowserCustomizations;->getHomePageUrl()Ljava/lang/String;

    move-result-object v0

    .line 44
    :goto_1
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    move-object v0, v1

    goto :goto_0

    .line 41
    :cond_2
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getPrefHomepageCustomUriPreference()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public static isHomepageEnabled(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 22
    invoke-static {}, Lcom/google/android/apps/chrome/partnercustomizations/PartnerBrowserCustomizations;->isHomepageProviderAvailableAndEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getPrefHomepagePreference()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static shouldShowHomepageSetting()Z
    .locals 1

    .prologue
    .line 30
    invoke-static {}, Lcom/google/android/apps/chrome/partnercustomizations/PartnerBrowserCustomizations;->isHomepageProviderAvailableAndEnabled()Z

    move-result v0

    return v0
.end method
