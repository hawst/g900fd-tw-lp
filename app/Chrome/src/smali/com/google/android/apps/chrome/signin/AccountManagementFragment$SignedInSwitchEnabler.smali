.class Lcom/google/android/apps/chrome/signin/AccountManagementFragment$SignedInSwitchEnabler;
.super Lcom/google/android/apps/chrome/preferences/ChromeBaseSwitchEnabler;
.source "AccountManagementFragment.java"


# instance fields
.field private final mContext:Landroid/content/Context;

.field final synthetic this$0:Lcom/google/android/apps/chrome/signin/AccountManagementFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/chrome/signin/AccountManagementFragment;Landroid/content/Context;Landroid/widget/Switch;)V
    .locals 0

    .prologue
    .line 136
    iput-object p1, p0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment$SignedInSwitchEnabler;->this$0:Lcom/google/android/apps/chrome/signin/AccountManagementFragment;

    .line 137
    invoke-direct {p0, p2, p3}, Lcom/google/android/apps/chrome/preferences/ChromeBaseSwitchEnabler;-><init>(Landroid/content/Context;Landroid/widget/Switch;)V

    .line 138
    iput-object p2, p0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment$SignedInSwitchEnabler;->mContext:Landroid/content/Context;

    .line 139
    return-void
.end method


# virtual methods
.method protected isSwitchEnabled()Z
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment$SignedInSwitchEnabler;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lorg/chromium/sync/signin/ChromeSigninController;->get(Landroid/content/Context;)Lorg/chromium/sync/signin/ChromeSigninController;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/sync/signin/ChromeSigninController;->isSignedIn()Z

    move-result v0

    return v0
.end method

.method public onValueChanged(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 143
    iget-object v0, p0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment$SignedInSwitchEnabler;->this$0:Lcom/google/android/apps/chrome/signin/AccountManagementFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getSignOutAllowed()Z

    move-result v0

    .line 145
    if-nez v0, :cond_1

    if-nez p1, :cond_1

    .line 159
    :cond_0
    :goto_0
    return-void

    .line 147
    :cond_1
    if-eqz p1, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment$SignedInSwitchEnabler;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lorg/chromium/sync/signin/ChromeSigninController;->get(Landroid/content/Context;)Lorg/chromium/sync/signin/ChromeSigninController;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/sync/signin/ChromeSigninController;->isSignedIn()Z

    move-result v0

    if-nez v0, :cond_2

    .line 148
    iget-object v0, p0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment$SignedInSwitchEnabler;->this$0:Lcom/google/android/apps/chrome/signin/AccountManagementFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/preferences/Preferences;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/Preferences;->displayAccountPicker()V

    goto :goto_0

    .line 149
    :cond_2
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment$SignedInSwitchEnabler;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lorg/chromium/sync/signin/ChromeSigninController;->get(Landroid/content/Context;)Lorg/chromium/sync/signin/ChromeSigninController;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/sync/signin/ChromeSigninController;->isSignedIn()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 150
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment$SignedInSwitchEnabler;->this$0:Lcom/google/android/apps/chrome/signin/AccountManagementFragment;

    # getter for: Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->mGaiaServiceType:I
    invoke-static {v1}, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->access$000(Lcom/google/android/apps/chrome/signin/AccountManagementFragment;)I

    move-result v1

    invoke-static {v0, v1}, Lorg/chromium/chrome/browser/signin/AccountManagementScreenHelper;->logEvent(II)V

    .line 154
    iget-object v0, p0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment$SignedInSwitchEnabler;->this$0:Lcom/google/android/apps/chrome/signin/AccountManagementFragment;

    # setter for: Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->mSignedOut:Z
    invoke-static {v0, v2}, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->access$102(Lcom/google/android/apps/chrome/signin/AccountManagementFragment;Z)Z

    .line 155
    new-instance v0, Lcom/google/android/apps/chrome/services/ui/SignOutDialogFragment;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/services/ui/SignOutDialogFragment;-><init>()V

    .line 156
    iget-object v1, p0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment$SignedInSwitchEnabler;->this$0:Lcom/google/android/apps/chrome/signin/AccountManagementFragment;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/services/ui/SignOutDialogFragment;->setTargetFragment(Landroid/app/Fragment;I)V

    .line 157
    iget-object v1, p0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment$SignedInSwitchEnabler;->this$0:Lcom/google/android/apps/chrome/signin/AccountManagementFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/services/ui/SignOutDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0
.end method
