.class public abstract Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;
.super Ljava/lang/Object;
.source "ChromeNotificationCenter.java"


# instance fields
.field private final mHandler:Landroid/os/Handler;

.field private mIsApplicationContext:Z

.field private mWeakContext:Ljava/lang/ref/WeakReference;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 872
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 883
    new-instance v0, Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler$1;-><init>(Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 872
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;->setContext(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 872
    invoke-direct {p0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;->getHandler()Landroid/os/Handler;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)Ljava/lang/ref/WeakReference;
    .locals 1

    .prologue
    .line 872
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;->mWeakContext:Ljava/lang/ref/WeakReference;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)Z
    .locals 1

    .prologue
    .line 872
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;->mIsApplicationContext:Z

    return v0
.end method

.method private getHandler()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 910
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private setContext(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 877
    # invokes: Lcom/google/android/apps/chrome/ChromeNotificationCenter;->isApplicationContext(Landroid/content/Context;)Z
    invoke-static {p1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->access$700(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;->mIsApplicationContext:Z

    .line 878
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;->mIsApplicationContext:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;->mWeakContext:Ljava/lang/ref/WeakReference;

    .line 879
    :cond_0
    return-void
.end method


# virtual methods
.method public abstract handleMessage(Landroid/os/Message;)V
.end method
