.class Lcom/google/android/apps/chrome/videofling/ChromeMediaRouteChooserDialog;
.super Landroid/support/v7/app/q;
.source "ChromeMediaRouteChooserDialog.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0, p1}, Landroid/support/v7/app/q;-><init>(Landroid/content/Context;)V

    .line 21
    return-void
.end method

.method private static hasSameCastMRPRoute(Landroid/support/v7/media/MediaRouter$RouteInfo;Ljava/util/List;)Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 39
    move v1, v2

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 40
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/media/MediaRouter$RouteInfo;

    .line 41
    if-eq v0, p0, :cond_1

    invoke-static {v0}, Lcom/google/android/apps/chrome/videofling/ChromeMediaRouteChooserDialog;->isCastMRPRoute(Landroid/support/v7/media/MediaRouter$RouteInfo;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 42
    invoke-virtual {v0}, Landroid/support/v7/media/MediaRouter$RouteInfo;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Landroid/support/v7/media/MediaRouter$RouteInfo;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v2, 0x1

    .line 44
    :cond_0
    return v2

    .line 39
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method private static isCastMRPRoute(Landroid/support/v7/media/MediaRouter$RouteInfo;)Z
    .locals 1

    .prologue
    .line 52
    const-string/jumbo v0, "233637DE"

    invoke-static {v0}, Lcom/google/android/gms/cast/CastMediaControlIntent;->categoryForRemotePlayback(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/support/v7/media/MediaRouter$RouteInfo;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private static isYouTubeMRPRoute(Landroid/support/v7/media/MediaRouter$RouteInfo;)Z
    .locals 1

    .prologue
    .line 48
    const-string/jumbo v0, "EXTERNAL_MDX_MEDIA_ROUTE_CONTROL_CATEGORY"

    invoke-virtual {p0, v0}, Landroid/support/v7/media/MediaRouter$RouteInfo;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public onFilterRoutes(Ljava/util/List;)V
    .locals 3

    .prologue
    .line 25
    invoke-super {p0, p1}, Landroid/support/v7/app/q;->onFilterRoutes(Ljava/util/List;)V

    .line 28
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    .line 29
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/media/MediaRouter$RouteInfo;

    .line 30
    invoke-static {v0}, Lcom/google/android/apps/chrome/videofling/ChromeMediaRouteChooserDialog;->isYouTubeMRPRoute(Landroid/support/v7/media/MediaRouter$RouteInfo;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 31
    invoke-static {v0, p1}, Lcom/google/android/apps/chrome/videofling/ChromeMediaRouteChooserDialog;->hasSameCastMRPRoute(Landroid/support/v7/media/MediaRouter$RouteInfo;Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 28
    :cond_0
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 33
    :cond_1
    return-void
.end method
