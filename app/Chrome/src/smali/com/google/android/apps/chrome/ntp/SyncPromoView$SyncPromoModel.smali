.class public interface abstract Lcom/google/android/apps/chrome/ntp/SyncPromoView$SyncPromoModel;
.super Ljava/lang/Object;
.source "SyncPromoView.java"


# virtual methods
.method public abstract enableSync()V
.end method

.method public abstract isSignedIn()Z
.end method

.method public abstract isSyncEnabled()Z
.end method

.method public abstract registerForSyncUpdates(Lorg/chromium/sync/notifier/SyncStatusHelper$SyncSettingsChangedObserver;)V
.end method

.method public abstract unregisterForSyncUpdates(Lorg/chromium/sync/notifier/SyncStatusHelper$SyncSettingsChangedObserver;)V
.end method
