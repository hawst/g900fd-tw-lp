.class public Lcom/google/android/apps/chrome/widget/tabstrip/TabView;
.super Landroid/widget/FrameLayout;
.source "TabView.java"


# instance fields
.field private mCloseButton:Landroid/widget/ImageButton;

.field private mCloseButtonVisible:Z

.field private mCrashed:Z

.field private mCurrentCloseButtonAnimator:Landroid/animation/AnimatorSet;

.field private mCurrentTranslationYAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

.field private mCurrentWidthAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

.field private mCurrentXOffsetAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

.field private mGesture:Landroid/view/GestureDetector;

.field private mHandler:Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabViewHandler;

.field private mHeight:F

.field private mIsIncognito:Z

.field private mLeftSideHiddenAmount:F

.field private final mLoadRunnable:Ljava/lang/Runnable;

.field private mLoading:Z

.field private mOffsetX:F

.field private final mPageLoadRunnable:Ljava/lang/Runnable;

.field private mPageLoading:Z

.field private mProgressBar:Landroid/widget/ProgressBar;

.field private mSelected:Z

.field private mShowTitleTruncator:Z

.field private mTab:Lcom/google/android/apps/chrome/tab/ChromeTab;

.field private mTabIcon:Landroid/widget/ImageView;

.field private mTabId:I

.field private mTabState:Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;

.field private mTabTitle:Landroid/widget/TextView;

.field private mTabVisibleAmount:F

.field private mWidth:F


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 153
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 123
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mTabId:I

    .line 125
    sget-object v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;->OPENING:Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mTabState:Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;

    .line 126
    iput-boolean v3, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCrashed:Z

    .line 127
    iput-boolean v3, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mLoading:Z

    .line 128
    iput-boolean v3, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mPageLoading:Z

    .line 129
    iput-boolean v3, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mSelected:Z

    .line 130
    iput-boolean v4, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mShowTitleTruncator:Z

    .line 134
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mTabVisibleAmount:F

    .line 135
    iput v2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mLeftSideHiddenAmount:F

    .line 136
    iput v2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mOffsetX:F

    .line 138
    iput-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentWidthAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    .line 139
    iput-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentTranslationYAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    .line 140
    iput-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentXOffsetAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    .line 142
    iput-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mGesture:Landroid/view/GestureDetector;

    .line 143
    iput-boolean v4, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCloseButtonVisible:Z

    .line 145
    iput-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentCloseButtonAnimator:Landroid/animation/AnimatorSet;

    .line 339
    new-instance v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$4;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$4;-><init>(Lcom/google/android/apps/chrome/widget/tabstrip/TabView;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mLoadRunnable:Ljava/lang/Runnable;

    .line 347
    new-instance v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$5;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$5;-><init>(Lcom/google/android/apps/chrome/widget/tabstrip/TabView;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mPageLoadRunnable:Ljava/lang/Runnable;

    .line 155
    iput v2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mWidth:F

    .line 156
    iput v2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mHeight:F

    .line 157
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/widget/tabstrip/TabView;)Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mTabState:Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/widget/tabstrip/TabView;)Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabViewHandler;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mHandler:Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabViewHandler;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/widget/tabstrip/TabView;)Landroid/view/GestureDetector;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mGesture:Landroid/view/GestureDetector;

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/android/apps/chrome/widget/tabstrip/TabView;Z)Z
    .locals 0

    .prologue
    .line 46
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mLoading:Z

    return p1
.end method

.method static synthetic access$402(Lcom/google/android/apps/chrome/widget/tabstrip/TabView;Z)Z
    .locals 0

    .prologue
    .line 46
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mPageLoading:Z

    return p1
.end method

.method static synthetic access$500(Lcom/google/android/apps/chrome/widget/tabstrip/TabView;)Landroid/widget/ImageButton;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCloseButton:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$602(Lcom/google/android/apps/chrome/widget/tabstrip/TabView;Landroid/animation/AnimatorSet;)Landroid/animation/AnimatorSet;
    .locals 0

    .prologue
    .line 46
    iput-object p1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentCloseButtonAnimator:Landroid/animation/AnimatorSet;

    return-object p1
.end method

.method static synthetic access$700(Lcom/google/android/apps/chrome/widget/tabstrip/TabView;)V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->setTouchDelegateForCloseButton()V

    return-void
.end method

.method static synthetic access$802(Lcom/google/android/apps/chrome/widget/tabstrip/TabView;F)F
    .locals 0

    .prologue
    .line 46
    iput p1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mOffsetX:F

    return p1
.end method

.method static synthetic access$902(Lcom/google/android/apps/chrome/widget/tabstrip/TabView;F)F
    .locals 0

    .prologue
    .line 46
    iput p1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mWidth:F

    return p1
.end method

.method private animateClose()V
    .locals 11

    .prologue
    .line 764
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentTranslationYAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    if-eqz v0, :cond_0

    .line 765
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentTranslationYAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeAnimation;->updateAndFinish()V

    .line 766
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentTranslationYAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    .line 769
    :cond_0
    new-instance v0, Lcom/google/android/apps/chrome/ChromeAnimation;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/ChromeAnimation;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentTranslationYAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    .line 770
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentTranslationYAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    new-instance v1, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$15;

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->getHeight()I

    move-result v2

    int-to-float v5, v2

    const-wide/16 v6, 0x96

    const-wide/16 v8, 0x0

    new-instance v10, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v10}, Landroid/view/animation/LinearInterpolator;-><init>()V

    move-object v2, p0

    move-object v3, p0

    invoke-direct/range {v1 .. v10}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$15;-><init>(Lcom/google/android/apps/chrome/widget/tabstrip/TabView;Lcom/google/android/apps/chrome/widget/tabstrip/TabView;FFJJLandroid/view/animation/Interpolator;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/ChromeAnimation;->add(Lcom/google/android/apps/chrome/ChromeAnimation$Animation;)V

    .line 778
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentTranslationYAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeAnimation;->start()V

    .line 779
    return-void
.end method

.method private animateOpen()V
    .locals 11

    .prologue
    .line 716
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentTranslationYAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    if-eqz v0, :cond_0

    .line 717
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentTranslationYAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeAnimation;->updateAndFinish()V

    .line 718
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentTranslationYAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    .line 721
    :cond_0
    new-instance v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$11;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$11;-><init>(Lcom/google/android/apps/chrome/widget/tabstrip/TabView;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentTranslationYAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    .line 727
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentTranslationYAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    new-instance v1, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$12;

    iget v4, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mHeight:F

    const/4 v5, 0x0

    const-wide/16 v6, 0x96

    const-wide/16 v8, 0x0

    new-instance v10, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v10}, Landroid/view/animation/LinearInterpolator;-><init>()V

    move-object v2, p0

    move-object v3, p0

    invoke-direct/range {v1 .. v10}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$12;-><init>(Lcom/google/android/apps/chrome/widget/tabstrip/TabView;Lcom/google/android/apps/chrome/widget/tabstrip/TabView;FFJJLandroid/view/animation/Interpolator;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/ChromeAnimation;->add(Lcom/google/android/apps/chrome/ChromeAnimation$Animation;)V

    .line 734
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentTranslationYAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeAnimation;->start()V

    .line 735
    return-void
.end method

.method private animateWidth(FJ)V
    .locals 12

    .prologue
    const-wide/16 v8, 0x0

    .line 738
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentWidthAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    if-eqz v0, :cond_0

    .line 739
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentWidthAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeAnimation;->updateAndFinish()V

    .line 740
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentWidthAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    .line 743
    :cond_0
    cmp-long v0, p2, v8

    if-nez v0, :cond_1

    .line 744
    iput p1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mWidth:F

    .line 761
    :goto_0
    return-void

    .line 746
    :cond_1
    new-instance v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$13;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$13;-><init>(Lcom/google/android/apps/chrome/widget/tabstrip/TabView;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentWidthAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    .line 752
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentWidthAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    new-instance v1, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$14;

    iget v4, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mWidth:F

    new-instance v10, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v10}, Landroid/view/animation/LinearInterpolator;-><init>()V

    move-object v2, p0

    move-object v3, p0

    move v5, p1

    move-wide v6, p2

    invoke-direct/range {v1 .. v10}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$14;-><init>(Lcom/google/android/apps/chrome/widget/tabstrip/TabView;Lcom/google/android/apps/chrome/widget/tabstrip/TabView;FFJJLandroid/view/animation/Interpolator;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/ChromeAnimation;->add(Lcom/google/android/apps/chrome/ChromeAnimation$Animation;)V

    .line 759
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentWidthAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeAnimation;->start()V

    goto :goto_0
.end method

.method private setTouchDelegateForCloseButton()V
    .locals 1

    .prologue
    .line 644
    new-instance v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$8;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$8;-><init>(Lcom/google/android/apps/chrome/widget/tabstrip/TabView;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->post(Ljava/lang/Runnable;)Z

    .line 659
    return-void
.end method

.method private updateTitleTruncator()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 282
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mShowTitleTruncator:Z

    if-nez v0, :cond_0

    .line 283
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCloseButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    .line 301
    :goto_0
    return-void

    .line 288
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mIsIncognito:Z

    if-eqz v0, :cond_2

    .line 289
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mSelected:Z

    if-eqz v0, :cond_1

    sget v0, Lcom/google/android/apps/chrome/R$drawable;->tabstrip_foreground_incognito_tab_text_truncator:I

    .line 298
    :goto_1
    invoke-static {p0, v1}, Lorg/chromium/base/ApiCompatibilityUtils;->setLayoutDirection(Landroid/view/View;I)V

    .line 299
    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCloseButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    .line 300
    const/4 v0, 0x3

    invoke-static {p0, v0}, Lorg/chromium/base/ApiCompatibilityUtils;->setLayoutDirection(Landroid/view/View;I)V

    goto :goto_0

    .line 289
    :cond_1
    sget v0, Lcom/google/android/apps/chrome/R$drawable;->tabstrip_background_incognito_tab_text_truncator:I

    goto :goto_1

    .line 293
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mSelected:Z

    if-eqz v0, :cond_3

    sget v0, Lcom/google/android/apps/chrome/R$drawable;->tabstrip_foreground_tab_text_truncator:I

    goto :goto_1

    :cond_3
    sget v0, Lcom/google/android/apps/chrome/R$drawable;->tabstrip_background_tab_text_truncator:I

    goto :goto_1
.end method


# virtual methods
.method public animateSlideFrom(F)V
    .locals 11

    .prologue
    .line 693
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentXOffsetAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    if-eqz v0, :cond_0

    .line 694
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentXOffsetAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeAnimation;->updateAndFinish()V

    .line 695
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentXOffsetAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    .line 698
    :cond_0
    new-instance v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$9;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$9;-><init>(Lcom/google/android/apps/chrome/widget/tabstrip/TabView;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentXOffsetAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    .line 705
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentXOffsetAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    new-instance v1, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$10;

    const/4 v5, 0x0

    const-wide/16 v6, 0x7d

    const-wide/16 v8, 0x0

    new-instance v10, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v10}, Landroid/view/animation/LinearInterpolator;-><init>()V

    move-object v2, p0

    move-object v3, p0

    move v4, p1

    invoke-direct/range {v1 .. v10}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$10;-><init>(Lcom/google/android/apps/chrome/widget/tabstrip/TabView;Lcom/google/android/apps/chrome/widget/tabstrip/TabView;FFJJLandroid/view/animation/Interpolator;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/ChromeAnimation;->add(Lcom/google/android/apps/chrome/ChromeAnimation$Animation;)V

    .line 712
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentXOffsetAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeAnimation;->start()V

    .line 713
    return-void
.end method

.method public close()V
    .locals 1

    .prologue
    .line 248
    sget-object v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;->CLOSING:Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mTabState:Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;

    .line 250
    invoke-direct {p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->animateClose()V

    .line 251
    return-void
.end method

.method public die()V
    .locals 1

    .prologue
    .line 257
    sget-object v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;->DYING:Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mTabState:Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;

    .line 258
    return-void
.end method

.method protected drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z
    .locals 5

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v3, 0x0

    .line 553
    const/4 v0, 0x0

    .line 555
    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mTabTitle:Landroid/widget/TextView;

    if-eq p2, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mTabIcon:Landroid/widget/ImageView;

    if-eq p2, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mProgressBar:Landroid/widget/ProgressBar;

    if-ne p2, v1, :cond_1

    .line 556
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 557
    invoke-static {}, Lorg/chromium/ui/base/LocalizationUtils;->isLayoutRtl()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 558
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCloseButton:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getTranslationX()F

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCloseButton:Landroid/widget/ImageButton;

    invoke-virtual {v1}, Landroid/widget/ImageButton;->getRight()I

    move-result v1

    int-to-float v1, v1

    add-float/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCloseButton:Landroid/widget/ImageButton;

    invoke-virtual {v1}, Landroid/widget/ImageButton;->getPaddingRight()I

    move-result v1

    int-to-float v1, v1

    sub-float/2addr v0, v1

    add-float/2addr v0, v4

    invoke-virtual {p2}, Landroid/view/View;->getRight()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->getMeasuredHeight()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p1, v0, v3, v1, v2}, Landroid/graphics/Canvas;->clipRect(FFFF)Z

    .line 566
    :goto_0
    const/4 v0, 0x1

    .line 568
    :cond_1
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/FrameLayout;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    move-result v1

    .line 569
    if-eqz v0, :cond_2

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 570
    :cond_2
    return v1

    .line 562
    :cond_3
    invoke-virtual {p2}, Landroid/view/View;->getLeft()I

    move-result v0

    int-to-float v0, v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCloseButton:Landroid/widget/ImageButton;

    invoke-virtual {v1}, Landroid/widget/ImageButton;->getTranslationX()F

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCloseButton:Landroid/widget/ImageButton;

    invoke-virtual {v2}, Landroid/widget/ImageButton;->getLeft()I

    move-result v2

    int-to-float v2, v2

    add-float/2addr v1, v2

    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCloseButton:Landroid/widget/ImageButton;

    invoke-virtual {v2}, Landroid/widget/ImageButton;->getPaddingLeft()I

    move-result v2

    int-to-float v2, v2

    add-float/2addr v1, v2

    sub-float/2addr v1, v4

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->getMeasuredHeight()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p1, v0, v3, v1, v2}, Landroid/graphics/Canvas;->clipRect(FFFF)Z

    goto :goto_0
.end method

.method public finishAnimation()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 672
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentWidthAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    if-eqz v0, :cond_0

    .line 673
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentWidthAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeAnimation;->updateAndFinish()V

    .line 674
    iput-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentWidthAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    .line 677
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentTranslationYAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    if-eqz v0, :cond_1

    .line 678
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentTranslationYAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeAnimation;->updateAndFinish()V

    .line 679
    iput-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentTranslationYAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    .line 682
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentXOffsetAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    if-eqz v0, :cond_2

    .line 683
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentXOffsetAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeAnimation;->updateAndFinish()V

    .line 684
    iput-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentXOffsetAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    .line 686
    :cond_2
    return-void
.end method

.method public getOffsetX()F
    .locals 1

    .prologue
    .line 665
    iget v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mOffsetX:F

    return v0
.end method

.method public getTabId()I
    .locals 1

    .prologue
    .line 234
    iget v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mTabId:I

    return v0
.end method

.method public getTabState()Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;
    .locals 1

    .prologue
    .line 241
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mTabState:Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;

    return-object v0
.end method

.method public getTabVisibleAmount()F
    .locals 1

    .prologue
    .line 453
    iget v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mTabVisibleAmount:F

    return v0
.end method

.method public getTabWidth()F
    .locals 1

    .prologue
    .line 363
    iget v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mWidth:F

    return v0
.end method

.method public handleLayout(FFZ)V
    .locals 0

    .prologue
    .line 476
    iput p2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mTabVisibleAmount:F

    .line 477
    iput p1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mLeftSideHiddenAmount:F

    .line 478
    invoke-virtual {p0, p3}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->updateVisualsFromLayoutParams(Z)V

    .line 479
    return-void
.end method

.method public hasTabCrashed()Z
    .locals 1

    .prologue
    .line 786
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCrashed:Z

    return v0
.end method

.method public initializeControl(Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabViewHandler;Lcom/google/android/apps/chrome/tab/ChromeTab;FZ)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 170
    const/4 v0, 0x3

    invoke-static {p0, v0}, Lorg/chromium/base/ApiCompatibilityUtils;->setLayoutDirection(Landroid/view/View;I)V

    .line 171
    iput p3, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mHeight:F

    .line 172
    iput-object p1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mHandler:Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabViewHandler;

    .line 173
    iput-object p2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mTab:Lcom/google/android/apps/chrome/tab/ChromeTab;

    .line 174
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mTab:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getId()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mTabId:I

    .line 175
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mTab:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->isIncognito()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mIsIncognito:Z

    .line 177
    sget v0, Lcom/google/android/apps/chrome/R$id;->tab_icon:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mTabIcon:Landroid/widget/ImageView;

    .line 178
    sget v0, Lcom/google/android/apps/chrome/R$id;->progress:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mProgressBar:Landroid/widget/ProgressBar;

    .line 179
    sget v0, Lcom/google/android/apps/chrome/R$id;->tab_title:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mTabTitle:Landroid/widget/TextView;

    .line 180
    sget v0, Lcom/google/android/apps/chrome/R$id;->tab_close_btn:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCloseButton:Landroid/widget/ImageButton;

    .line 182
    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mTabTitle:Landroid/widget/TextView;

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mIsIncognito:Z

    if-eqz v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 183
    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCloseButton:Landroid/widget/ImageButton;

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mIsIncognito:Z

    if-eqz v0, :cond_1

    sget v0, Lcom/google/android/apps/chrome/R$drawable;->btn_tab_close_white:I

    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 186
    new-instance v0, Landroid/view/GestureDetector;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$1;

    invoke-direct {v2, p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$1;-><init>(Lcom/google/android/apps/chrome/widget/tabstrip/TabView;)V

    invoke-direct {v0, v1, v2}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mGesture:Landroid/view/GestureDetector;

    .line 197
    new-instance v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$2;-><init>(Lcom/google/android/apps/chrome/widget/tabstrip/TabView;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 208
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCloseButton:Landroid/widget/ImageButton;

    new-instance v1, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$3;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$3;-><init>(Lcom/google/android/apps/chrome/widget/tabstrip/TabView;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 218
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCloseButton:Landroid/widget/ImageButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 220
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mTabTitle:Landroid/widget/TextView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setHorizontalFadingEdgeEnabled(Z)V

    .line 222
    if-eqz p4, :cond_2

    .line 223
    invoke-direct {p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->animateOpen()V

    .line 227
    :goto_2
    invoke-virtual {p0, v3, v3}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->updateContentFromTab(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 228
    return-void

    .line 182
    :cond_0
    const/high16 v0, -0x1000000

    goto :goto_0

    .line 183
    :cond_1
    sget v0, Lcom/google/android/apps/chrome/R$drawable;->btn_tab_close:I

    goto :goto_1

    .line 225
    :cond_2
    sget-object v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;->IDLE:Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mTabState:Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;

    goto :goto_2
.end method

.method public isAnimating()Z
    .locals 2

    .prologue
    .line 395
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentWidthAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentWidthAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeAnimation;->finished()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentTranslationYAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentTranslationYAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeAnimation;->finished()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentXOffsetAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentXOffsetAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeAnimation;->finished()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mTabState:Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;

    sget-object v1, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;->CLOSING:Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;

    if-eq v0, v1, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mTabState:Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;

    sget-object v1, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;->OPENING:Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;

    if-ne v0, v1, :cond_4

    :cond_3
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isTabSelected()Z
    .locals 1

    .prologue
    .line 794
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mSelected:Z

    return v0
.end method

.method public loadingFinished()V
    .locals 4

    .prologue
    .line 317
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mLoadRunnable:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 318
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mLoadRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x64

    invoke-virtual {p0, v0, v2, v3}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 319
    return-void
.end method

.method public loadingStarted()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 307
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCrashed:Z

    .line 308
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mLoading:Z

    .line 309
    invoke-virtual {p0, v1, v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->updateContentFromTab(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 310
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mLoadRunnable:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 311
    return-void
.end method

.method public pageLoadingFinished()V
    .locals 4

    .prologue
    .line 335
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mPageLoadRunnable:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 336
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mPageLoadRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x64

    invoke-virtual {p0, v0, v2, v3}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 337
    return-void
.end method

.method public pageLoadingStarted()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 325
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCrashed:Z

    .line 326
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mPageLoading:Z

    .line 327
    invoke-virtual {p0, v1, v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->updateContentFromTab(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 328
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mPageLoadRunnable:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 329
    return-void
.end method

.method public selected()V
    .locals 1

    .prologue
    .line 264
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mSelected:Z

    .line 265
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mIsIncognito:Z

    if-eqz v0, :cond_0

    sget v0, Lcom/google/android/apps/chrome/R$drawable;->bg_tabstrip_incognito_tab:I

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->setBackgroundResource(I)V

    .line 267
    invoke-direct {p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->updateTitleTruncator()V

    .line 268
    return-void

    .line 265
    :cond_0
    sget v0, Lcom/google/android/apps/chrome/R$drawable;->bg_tabstrip_tab:I

    goto :goto_0
.end method

.method public setTabWidth(FZ)V
    .locals 2

    .prologue
    .line 374
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mTabState:Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;

    sget-object v1, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;->CLOSED:Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mTabState:Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;

    sget-object v1, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;->CLOSING:Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mTabState:Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;

    sget-object v1, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;->DYING:Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;

    if-ne v0, v1, :cond_1

    .line 389
    :cond_0
    :goto_0
    return-void

    .line 379
    :cond_1
    if-eqz p2, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mTabState:Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;

    sget-object v1, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;->OPENING:Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;

    if-eq v0, v1, :cond_2

    .line 380
    const-wide/16 v0, 0x96

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->animateWidth(FJ)V

    goto :goto_0

    .line 382
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentWidthAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    if-eqz v0, :cond_3

    .line 383
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentWidthAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeAnimation;->updateAndFinish()V

    .line 384
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentWidthAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    .line 386
    :cond_3
    iput p1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mWidth:F

    .line 387
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->updateVisualsFromLayoutParams(Z)V

    goto :goto_0
.end method

.method public unselected()V
    .locals 1

    .prologue
    .line 274
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mSelected:Z

    .line 275
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mIsIncognito:Z

    if-eqz v0, :cond_0

    sget v0, Lcom/google/android/apps/chrome/R$drawable;->bg_tabstrip_incognito_background_tab:I

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->setBackgroundResource(I)V

    .line 278
    invoke-direct {p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->updateTitleTruncator()V

    .line 279
    return-void

    .line 275
    :cond_0
    sget v0, Lcom/google/android/apps/chrome/R$drawable;->bg_tabstrip_background_tab:I

    goto :goto_0
.end method

.method public update()Z
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x1

    .line 408
    const/4 v0, 0x0

    .line 410
    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentWidthAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    if-eqz v2, :cond_1

    .line 411
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentWidthAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeAnimation;->update()Z

    move-result v0

    if-eqz v0, :cond_0

    iput-object v3, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentWidthAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    :cond_0
    move v0, v1

    .line 415
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentTranslationYAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    if-eqz v2, :cond_3

    .line 416
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentTranslationYAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeAnimation;->update()Z

    move-result v0

    if-eqz v0, :cond_2

    iput-object v3, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentTranslationYAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    :cond_2
    move v0, v1

    .line 420
    :cond_3
    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentXOffsetAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    if-eqz v2, :cond_5

    .line 421
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentXOffsetAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeAnimation;->update()Z

    move-result v0

    if-eqz v0, :cond_4

    iput-object v3, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentXOffsetAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    :cond_4
    move v0, v1

    .line 426
    :cond_5
    sget-object v2, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$16;->$SwitchMap$com$google$android$apps$chrome$widget$tabstrip$TabView$TabState:[I

    iget-object v3, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mTabState:Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    move v1, v0

    .line 444
    :cond_6
    :goto_0
    return v1

    .line 429
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentTranslationYAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    if-nez v0, :cond_6

    .line 430
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->setVisibility(I)V

    .line 431
    sget-object v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;->CLOSED:Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mTabState:Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;

    goto :goto_0

    .line 437
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentTranslationYAnimation:Lcom/google/android/apps/chrome/ChromeAnimation;

    if-nez v0, :cond_6

    .line 438
    sget-object v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;->IDLE:Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mTabState:Lcom/google/android/apps/chrome/widget/tabstrip/TabView$TabState;

    goto :goto_0

    .line 426
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public updateContentFromTab(Ljava/lang/String;Ljava/lang/Integer;)V
    .locals 5

    .prologue
    const/4 v2, 0x4

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 585
    if-eqz p2, :cond_2

    .line 586
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mTabIcon:Landroid/widget/ImageView;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 602
    :goto_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 603
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mTab:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getTitle()Ljava/lang/String;

    move-result-object p1

    .line 604
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCrashed:Z

    if-nez v0, :cond_0

    .line 605
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mTab:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getUrl()Ljava/lang/String;

    move-result-object p1

    .line 606
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 607
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$string;->tab_loading_default_title:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 612
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mTabTitle:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 613
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mTabTitle:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 614
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$string;->accessibility_tabstrip_tab:I

    new-array v2, v4, [Ljava/lang/Object;

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 621
    new-instance v1, Ljava/text/Bidi;

    invoke-static {}, Lorg/chromium/ui/base/LocalizationUtils;->isLayoutRtl()Z

    move-result v0

    if-eqz v0, :cond_6

    const/4 v0, -0x1

    :goto_1
    invoke-direct {v1, p1, v0}, Ljava/text/Bidi;-><init>(Ljava/lang/String;I)V

    .line 624
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCloseButton:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    .line 625
    invoke-static {}, Lorg/chromium/ui/base/LocalizationUtils;->isLayoutRtl()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-virtual {v1}, Ljava/text/Bidi;->baseIsLeftToRight()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 627
    iput-boolean v3, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mShowTitleTruncator:Z

    .line 628
    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mTabTitle:Landroid/widget/TextView;

    invoke-virtual {v1, v0, v3, v3, v3}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 637
    :goto_2
    invoke-direct {p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->updateTitleTruncator()V

    .line 639
    :cond_1
    return-void

    .line 587
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mLoading:Z

    if-nez v0, :cond_3

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mPageLoading:Z

    if-eqz v0, :cond_4

    .line 588
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mTabIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 589
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto/16 :goto_0

    .line 591
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mTab:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getFavicon()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 592
    if-eqz v0, :cond_5

    .line 593
    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mTabIcon:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 598
    :goto_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mTabIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 599
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto/16 :goto_0

    .line 595
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mTabIcon:Landroid/widget/ImageView;

    sget v1, Lorg/chromium/chrome/R$drawable;->globe_favicon:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_3

    .line 621
    :cond_6
    const/4 v0, -0x2

    goto :goto_1

    .line 629
    :cond_7
    invoke-static {}, Lorg/chromium/ui/base/LocalizationUtils;->isLayoutRtl()Z

    move-result v2

    if-nez v2, :cond_8

    invoke-virtual {v1}, Ljava/text/Bidi;->baseIsLeftToRight()Z

    move-result v1

    if-nez v1, :cond_8

    .line 631
    iput-boolean v3, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mShowTitleTruncator:Z

    .line 632
    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mTabTitle:Landroid/widget/TextView;

    invoke-virtual {v1, v3, v3, v0, v3}, Landroid/widget/TextView;->setPadding(IIII)V

    goto :goto_2

    .line 634
    :cond_8
    iput-boolean v4, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mShowTitleTruncator:Z

    .line 635
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mTabTitle:Landroid/widget/TextView;

    invoke-virtual {v0, v3, v3, v3, v3}, Landroid/widget/TextView;->setPadding(IIII)V

    goto :goto_2
.end method

.method public updateVisualsFromLayoutParams(Z)V
    .locals 10

    .prologue
    const v2, 0x3c23d70a    # 0.01f

    const/4 v5, 0x0

    const-wide/16 v8, 0x64

    const/4 v4, 0x1

    const/4 v6, 0x0

    .line 489
    iget v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mLeftSideHiddenAmount:F

    iget v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mWidth:F

    mul-float/2addr v0, v1

    invoke-static {}, Lorg/chromium/ui/base/LocalizationUtils;->isLayoutRtl()Z

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/utilities/ChromeMathUtils;->flipSignIf(FZ)F

    move-result v0

    .line 492
    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mTabIcon:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setTranslationX(F)V

    .line 493
    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v1, v0}, Landroid/widget/ProgressBar;->setTranslationX(F)V

    .line 494
    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mTabTitle:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTranslationX(F)V

    .line 496
    const/high16 v0, 0x3f800000    # 1.0f

    iget v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mTabVisibleAmount:F

    sub-float/2addr v0, v1

    .line 497
    cmpl-float v1, v0, v2

    if-ltz v1, :cond_2

    iget-boolean v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCloseButtonVisible:Z

    if-eqz v1, :cond_2

    .line 499
    iput-boolean v6, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCloseButtonVisible:Z

    .line 501
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentCloseButtonAnimator:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentCloseButtonAnimator:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->cancel()V

    .line 502
    :cond_0
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentCloseButtonAnimator:Landroid/animation/AnimatorSet;

    .line 504
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentCloseButtonAnimator:Landroid/animation/AnimatorSet;

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCloseButton:Landroid/widget/ImageButton;

    const-string/jumbo v2, "alpha"

    new-array v3, v4, [F

    aput v5, v3, v6

    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    invoke-virtual {v1, v8, v9}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 506
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentCloseButtonAnimator:Landroid/animation/AnimatorSet;

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCloseButton:Landroid/widget/ImageButton;

    const-string/jumbo v2, "translationX"

    new-array v3, v4, [F

    iget-object v4, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCloseButton:Landroid/widget/ImageButton;

    invoke-virtual {v4}, Landroid/widget/ImageButton;->getWidth()I

    move-result v4

    invoke-static {}, Lorg/chromium/ui/base/LocalizationUtils;->isLayoutRtl()Z

    move-result v5

    invoke-static {v4, v5}, Lcom/google/android/apps/chrome/utilities/ChromeMathUtils;->flipSignIf(IZ)I

    move-result v4

    int-to-float v4, v4

    aput v4, v3, v6

    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    const-wide/16 v2, 0xc8

    invoke-virtual {v1, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 511
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentCloseButtonAnimator:Landroid/animation/AnimatorSet;

    new-instance v1, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$6;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$6;-><init>(Lcom/google/android/apps/chrome/widget/tabstrip/TabView;)V

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 518
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentCloseButtonAnimator:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    .line 519
    if-nez p1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentCloseButtonAnimator:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->end()V

    .line 549
    :cond_1
    :goto_0
    return-void

    .line 520
    :cond_2
    cmpg-float v0, v0, v2

    if-gez v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCloseButtonVisible:Z

    if-nez v0, :cond_1

    .line 522
    iput-boolean v4, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCloseButtonVisible:Z

    .line 524
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentCloseButtonAnimator:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentCloseButtonAnimator:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->cancel()V

    .line 526
    :cond_3
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentCloseButtonAnimator:Landroid/animation/AnimatorSet;

    .line 527
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCloseButton:Landroid/widget/ImageButton;

    const-string/jumbo v1, "alpha"

    new-array v2, v4, [F

    const/high16 v3, 0x3f800000    # 1.0f

    aput v3, v2, v6

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v0, v8, v9}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 529
    invoke-virtual {v0, v8, v9}, Landroid/animation/ObjectAnimator;->setStartDelay(J)V

    .line 530
    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentCloseButtonAnimator:Landroid/animation/AnimatorSet;

    invoke-virtual {v1, v0}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 531
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentCloseButtonAnimator:Landroid/animation/AnimatorSet;

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCloseButton:Landroid/widget/ImageButton;

    const-string/jumbo v2, "translationX"

    new-array v3, v4, [F

    aput v5, v3, v6

    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    const-wide/16 v2, 0xc8

    invoke-virtual {v1, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 534
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentCloseButtonAnimator:Landroid/animation/AnimatorSet;

    new-instance v1, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$7;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabView$7;-><init>(Lcom/google/android/apps/chrome/widget/tabstrip/TabView;)V

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 546
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentCloseButtonAnimator:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    .line 547
    if-nez p1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabView;->mCurrentCloseButtonAnimator:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->end()V

    goto :goto_0
.end method
