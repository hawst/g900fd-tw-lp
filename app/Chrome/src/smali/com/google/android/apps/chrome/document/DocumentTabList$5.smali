.class Lcom/google/android/apps/chrome/document/DocumentTabList$5;
.super Landroid/os/AsyncTask;
.source "DocumentTabList.java"


# instance fields
.field private mList:Lcom/google/android/apps/chrome/document/DocumentDatabase$DocumentList;

.field final synthetic this$0:Lcom/google/android/apps/chrome/document/DocumentTabList;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/document/DocumentTabList;)V
    .locals 0

    .prologue
    .line 849
    iput-object p1, p0, Lcom/google/android/apps/chrome/document/DocumentTabList$5;->this$0:Lcom/google/android/apps/chrome/document/DocumentTabList;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 849
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/document/DocumentTabList$5;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 871
    .line 873
    :try_start_0
    invoke-static {}, Lorg/chromium/base/ApplicationStatus;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 874
    iget-object v1, p0, Lcom/google/android/apps/chrome/document/DocumentTabList$5;->this$0:Lcom/google/android/apps/chrome/document/DocumentTabList;

    # getter for: Lcom/google/android/apps/chrome/document/DocumentTabList;->mFilename:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/apps/chrome/document/DocumentTabList;->access$1300(Lcom/google/android/apps/chrome/document/DocumentTabList;)Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 875
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentTabList$5;->mList:Lcom/google/android/apps/chrome/document/DocumentDatabase$DocumentList;

    invoke-static {v0}, Lcom/google/protobuf/nano/MessageNano;->toByteArray(Lcom/google/protobuf/nano/MessageNano;)[B

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/FileOutputStream;->write([B)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 881
    invoke-static {v1}, Lorg/chromium/chrome/browser/util/StreamUtil;->closeQuietly(Ljava/io/Closeable;)V

    .line 884
    :goto_0
    return-object v2

    .line 876
    :catch_0
    move-exception v0

    move-object v1, v2

    .line 877
    :goto_1
    :try_start_2
    const-string/jumbo v3, "DocumentTabList"

    const-string/jumbo v4, "Database file not found"

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 881
    invoke-static {v1}, Lorg/chromium/chrome/browser/util/StreamUtil;->closeQuietly(Ljava/io/Closeable;)V

    goto :goto_0

    .line 878
    :catch_1
    move-exception v0

    move-object v1, v2

    .line 879
    :goto_2
    :try_start_3
    const-string/jumbo v3, "DocumentTabList"

    const-string/jumbo v4, "I/O exception"

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 881
    invoke-static {v1}, Lorg/chromium/chrome/browser/util/StreamUtil;->closeQuietly(Ljava/io/Closeable;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_3
    invoke-static {v1}, Lorg/chromium/chrome/browser/util/StreamUtil;->closeQuietly(Ljava/io/Closeable;)V

    throw v0

    :catchall_1
    move-exception v0

    goto :goto_3

    .line 878
    :catch_2
    move-exception v0

    goto :goto_2

    .line 876
    :catch_3
    move-exception v0

    goto :goto_1
.end method

.method protected onPreExecute()V
    .locals 5

    .prologue
    .line 854
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 855
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentTabList$5;->this$0:Lcom/google/android/apps/chrome/document/DocumentTabList;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/document/DocumentTabList;->getCount()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 856
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentTabList$5;->this$0:Lcom/google/android/apps/chrome/document/DocumentTabList;

    # getter for: Lcom/google/android/apps/chrome/document/DocumentTabList;->mEntryMap:Landroid/util/SparseArray;
    invoke-static {v0}, Lcom/google/android/apps/chrome/document/DocumentTabList;->access$400(Lcom/google/android/apps/chrome/document/DocumentTabList;)Landroid/util/SparseArray;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/apps/chrome/document/DocumentTabList$5;->this$0:Lcom/google/android/apps/chrome/document/DocumentTabList;

    invoke-virtual {v3, v1}, Lcom/google/android/apps/chrome/document/DocumentTabList;->getTabIdAtIndex(I)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/document/DocumentTabList$Entry;

    .line 857
    if-eqz v0, :cond_0

    .line 859
    new-instance v3, Lcom/google/android/apps/chrome/document/DocumentDatabase$DocumentEntry;

    invoke-direct {v3}, Lcom/google/android/apps/chrome/document/DocumentDatabase$DocumentEntry;-><init>()V

    .line 860
    iget v4, v0, Lcom/google/android/apps/chrome/document/DocumentTabList$Entry;->tabId:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    iput-object v4, v3, Lcom/google/android/apps/chrome/document/DocumentDatabase$DocumentEntry;->tabId:Ljava/lang/Integer;

    .line 861
    iget-boolean v0, v0, Lcom/google/android/apps/chrome/document/DocumentTabList$Entry;->canGoBack:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v3, Lcom/google/android/apps/chrome/document/DocumentDatabase$DocumentEntry;->canGoBack:Ljava/lang/Boolean;

    .line 863
    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 855
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 865
    :cond_1
    new-instance v0, Lcom/google/android/apps/chrome/document/DocumentDatabase$DocumentList;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/document/DocumentDatabase$DocumentList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentTabList$5;->mList:Lcom/google/android/apps/chrome/document/DocumentDatabase$DocumentList;

    .line 866
    iget-object v1, p0, Lcom/google/android/apps/chrome/document/DocumentTabList$5;->mList:Lcom/google/android/apps/chrome/document/DocumentDatabase$DocumentList;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lcom/google/android/apps/chrome/document/DocumentDatabase$DocumentEntry;

    invoke-interface {v2, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/chrome/document/DocumentDatabase$DocumentEntry;

    iput-object v0, v1, Lcom/google/android/apps/chrome/document/DocumentDatabase$DocumentList;->entries:[Lcom/google/android/apps/chrome/document/DocumentDatabase$DocumentEntry;

    .line 867
    return-void
.end method
