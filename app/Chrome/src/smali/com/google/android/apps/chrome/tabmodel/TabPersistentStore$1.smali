.class Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$1;
.super Ljava/lang/Object;
.source "TabPersistentStore.java"

# interfaces
.implements Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$OnTabStateReadCallback;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;

.field final synthetic val$isIncognitoSelected:Z

.field final synthetic val$restoreList:Ljava/util/Deque;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;Ljava/util/Deque;Z)V
    .locals 0

    .prologue
    .line 532
    iput-object p1, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$1;->this$0:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;

    iput-object p2, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$1;->val$restoreList:Ljava/util/Deque;

    iput-boolean p3, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$1;->val$isIncognitoSelected:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onStateRead(IILjava/lang/String;ZZ)V
    .locals 2

    .prologue
    .line 538
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$1;->val$restoreList:Ljava/util/Deque;

    if-nez v0, :cond_0

    .line 550
    :goto_0
    return-void

    .line 543
    :cond_0
    if-eqz p5, :cond_1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$1;->val$isIncognitoSelected:Z

    if-nez v0, :cond_2

    :cond_1
    if-eqz p4, :cond_3

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$1;->val$isIncognitoSelected:Z

    if-nez v0, :cond_3

    .line 546
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$1;->val$restoreList:Ljava/util/Deque;

    new-instance v1, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$TabRestoreDetails;

    invoke-direct {v1, p2, p1, p3}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$TabRestoreDetails;-><init>(IILjava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/Deque;->addFirst(Ljava/lang/Object;)V

    goto :goto_0

    .line 548
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$1;->val$restoreList:Ljava/util/Deque;

    new-instance v1, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$TabRestoreDetails;

    invoke-direct {v1, p2, p1, p3}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$TabRestoreDetails;-><init>(IILjava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/Deque;->addLast(Ljava/lang/Object;)V

    goto :goto_0
.end method
