.class public final Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterDataFormatter;
.super Ljava/lang/Object;
.source "PrinterDataFormatter.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 172
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 173
    return-void
.end method

.method private static addC2dmInformation(Ljava/util/List;Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;)V
    .locals 3

    .prologue
    .line 159
    new-instance v0, Lcom/google/android/common/http/StringPart;

    const-string/jumbo v1, "remove_tag"

    const-string/jumbo v2, "__c2dm__reg_id=.*"

    invoke-direct {v0, v1, v2}, Lcom/google/android/common/http/StringPart;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 162
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->hasC2dmRegistrationIdTag()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 163
    const-string/jumbo v0, "__c2dm__reg_id"

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->getC2dmRegistrationIdTag()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterDataFormatter;->getTagParam(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/common/http/StringPart;

    move-result-object v0

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 166
    :cond_0
    return-void
.end method

.method private static addVersionInformation(Landroid/content/Context;Ljava/util/List;Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;)V
    .locals 3

    .prologue
    .line 142
    new-instance v0, Lcom/google/android/common/http/StringPart;

    const-string/jumbo v1, "remove_tag"

    const-string/jumbo v2, "__version__application=.*"

    invoke-direct {v0, v1, v2}, Lcom/google/android/common/http/StringPart;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 145
    const-string/jumbo v0, "__version__application"

    invoke-virtual {p2}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->getApplicationVersionTag()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterDataFormatter;->getTagParam(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/common/http/StringPart;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 147
    new-instance v0, Lcom/google/android/common/http/StringPart;

    const-string/jumbo v1, "remove_tag"

    const-string/jumbo v2, "__version__protocol=.*"

    invoke-direct {v0, v1, v2}, Lcom/google/android/common/http/StringPart;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 150
    const-string/jumbo v0, "__version__protocol"

    invoke-virtual {p2}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->getProtocolVersionTag()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterDataFormatter;->getTagParam(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/common/http/StringPart;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 152
    return-void
.end method

.method private static getDescriptionOrEmpty(Lorg/json/JSONObject;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 73
    const-string/jumbo v0, "description"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 75
    if-nez v0, :cond_0

    const-string/jumbo v0, ""

    :cond_0
    return-object v0
.end method

.method private static getIdOrEmpty(Lorg/json/JSONObject;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 95
    const-string/jumbo v0, "id"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 96
    if-nez v0, :cond_0

    const-string/jumbo v0, ""

    :cond_0
    return-object v0
.end method

.method private static getNameOrEmpty(Lorg/json/JSONObject;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 79
    const-string/jumbo v0, "name"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 81
    if-nez v0, :cond_0

    const-string/jumbo v0, ""

    :cond_0
    return-object v0
.end method

.method private static getProxyOrEmpty(Lorg/json/JSONObject;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 90
    const-string/jumbo v0, "proxy"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 91
    if-nez v0, :cond_0

    const-string/jumbo v0, ""

    :cond_0
    return-object v0
.end method

.method private static getStatusOrEmpty(Lorg/json/JSONObject;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 68
    const-string/jumbo v0, "status"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 69
    if-nez v0, :cond_0

    const-string/jumbo v0, ""

    :cond_0
    return-object v0
.end method

.method private static getStringParts(Landroid/content/Context;ZLcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;)Ljava/util/List;
    .locals 4

    .prologue
    .line 110
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 111
    if-eqz p1, :cond_0

    .line 112
    new-instance v1, Lcom/google/android/common/http/StringPart;

    const-string/jumbo v2, "printerid"

    invoke-virtual {p2}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/google/android/common/http/StringPart;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 115
    :cond_0
    new-instance v1, Lcom/google/android/common/http/StringPart;

    const-string/jumbo v2, "proxy"

    invoke-virtual {p2}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->getProxy()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/google/android/common/http/StringPart;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 117
    new-instance v1, Lcom/google/android/common/http/StringPart;

    const-string/jumbo v2, "printer"

    invoke-virtual {p2}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/google/android/common/http/StringPart;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 119
    new-instance v1, Lcom/google/android/common/http/StringPart;

    const-string/jumbo v2, "description"

    invoke-virtual {p2}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->getDescription()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/google/android/common/http/StringPart;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 121
    new-instance v1, Lcom/google/android/common/http/StringPart;

    const-string/jumbo v2, "status"

    invoke-virtual {p2}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->getStatus()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/google/android/common/http/StringPart;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 123
    new-instance v1, Lcom/google/android/common/http/StringPart;

    const-string/jumbo v2, "capabilities"

    invoke-virtual {p2}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->getCapabilities()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/google/android/common/http/StringPart;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 125
    new-instance v1, Lcom/google/android/common/http/StringPart;

    const-string/jumbo v2, "default"

    invoke-virtual {p2}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->getDefaultValues()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/google/android/common/http/StringPart;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 127
    new-instance v1, Lcom/google/android/common/http/StringPart;

    const-string/jumbo v2, "capsHash"

    invoke-virtual {p2}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->getCapabilitiesHash()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/google/android/common/http/StringPart;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 129
    new-instance v1, Lcom/google/android/common/http/StringPart;

    const-string/jumbo v2, "type"

    invoke-virtual {p2}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->getType()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/google/android/common/http/StringPart;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 131
    invoke-static {p0, v0, p2}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterDataFormatter;->addVersionInformation(Landroid/content/Context;Ljava/util/List;Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;)V

    .line 132
    invoke-static {v0, p2}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterDataFormatter;->addC2dmInformation(Ljava/util/List;Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;)V

    .line 133
    return-object v0
.end method

.method private static getTagParam(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/common/http/StringPart;
    .locals 4

    .prologue
    .line 169
    new-instance v0, Lcom/google/android/common/http/StringPart;

    const-string/jumbo v1, "tag"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/common/http/StringPart;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method private static getTypeOrEmpty(Lorg/json/JSONObject;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 85
    const-string/jumbo v0, "type"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 86
    if-nez v0, :cond_0

    const-string/jumbo v0, ""

    :cond_0
    return-object v0
.end method

.method public static parse(Lorg/json/JSONObject;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;
    .locals 2

    .prologue
    .line 28
    invoke-static {}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterDataFactory;->createDefaultNonChangingPrinterData()Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;

    move-result-object v0

    .line 29
    invoke-static {v0, p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterDataFormatter;->parseTags(Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;Lorg/json/JSONObject;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;

    move-result-object v0

    .line 30
    invoke-static {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterDataFormatter;->getIdOrEmpty(Lorg/json/JSONObject;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->withId(Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;

    move-result-object v0

    invoke-static {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterDataFormatter;->getProxyOrEmpty(Lorg/json/JSONObject;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->withProxy(Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;

    move-result-object v0

    invoke-static {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterDataFormatter;->getTypeOrEmpty(Lorg/json/JSONObject;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->withType(Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;

    move-result-object v0

    invoke-static {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterDataFormatter;->getNameOrEmpty(Lorg/json/JSONObject;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->withName(Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;

    move-result-object v0

    invoke-static {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterDataFormatter;->getDescriptionOrEmpty(Lorg/json/JSONObject;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->withDescription(Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;

    move-result-object v0

    invoke-static {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterDataFormatter;->getStatusOrEmpty(Lorg/json/JSONObject;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->withStatus(Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;

    move-result-object v0

    return-object v0
.end method

.method private static parseSplitTag(Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;[Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 57
    const-string/jumbo v0, "__version__application"

    aget-object v1, p1, v2

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 58
    aget-object v0, p1, v3

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->withApplicationVersionTag(Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;

    move-result-object p0

    .line 64
    :cond_0
    :goto_0
    return-object p0

    .line 59
    :cond_1
    const-string/jumbo v0, "__version__protocol"

    aget-object v1, p1, v2

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 60
    aget-object v0, p1, v3

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->withProtocolVersionTag(Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;

    move-result-object p0

    goto :goto_0

    .line 61
    :cond_2
    const-string/jumbo v0, "__c2dm__reg_id"

    aget-object v1, p1, v2

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 62
    aget-object v0, p1, v3

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->withC2dmRegistrationIdTag(Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;

    move-result-object p0

    goto :goto_0
.end method

.method private static parseTags(Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;Lorg/json/JSONObject;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;
    .locals 5

    .prologue
    .line 41
    const-string/jumbo v0, "tags"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    .line 42
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v0

    if-nez v0, :cond_2

    .line 43
    :cond_0
    const-string/jumbo v0, "PrinterDataFormatter"

    const-string/jumbo v1, "Could not find any tags for printer"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 53
    :cond_1
    return-object p0

    .line 46
    :cond_2
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 47
    invoke-virtual {v1, v0}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 48
    const-string/jumbo v3, "="

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 49
    array-length v3, v2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_3

    .line 50
    invoke-static {p0, v2}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterDataFormatter;->parseSplitTag(Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;[Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;

    move-result-object p0

    .line 46
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static toMultipartEntity(Landroid/content/Context;ZLcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;)Lcom/google/android/common/http/MultipartEntity;
    .locals 3

    .prologue
    .line 101
    invoke-virtual {p2}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->getCapabilitiesHash()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 102
    const/4 v0, 0x0

    .line 105
    :goto_0
    return-object v0

    .line 104
    :cond_0
    invoke-static {p0, p1, p2}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterDataFormatter;->getStringParts(Landroid/content/Context;ZLcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;)Ljava/util/List;

    move-result-object v0

    .line 105
    new-instance v1, Lcom/google/android/common/http/MultipartEntity;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [Lcom/google/android/common/http/Part;

    invoke-interface {v0, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/common/http/Part;

    invoke-direct {v1, v0}, Lcom/google/android/common/http/MultipartEntity;-><init>([Lcom/google/android/common/http/Part;)V

    move-object v0, v1

    goto :goto_0
.end method
