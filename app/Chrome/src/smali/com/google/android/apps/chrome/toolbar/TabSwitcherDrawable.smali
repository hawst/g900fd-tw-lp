.class public Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;
.super Landroid/graphics/drawable/LevelListDrawable;
.source "TabSwitcherDrawable.java"


# instance fields
.field private final mDefaultTextColor:I

.field private final mDoubleDigitTextSize:F

.field private mIncognito:Z

.field private final mSelectedTextColor:I

.field private final mSingleDigitTextSize:F

.field private mTabCount:I

.field private final mTextBounds:Landroid/graphics/Rect;

.field private final mTextPaint:Landroid/text/TextPaint;


# direct methods
.method private constructor <init>(Landroid/content/res/Resources;ZI)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 65
    invoke-direct {p0}, Landroid/graphics/drawable/LevelListDrawable;-><init>()V

    .line 28
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;->mTextBounds:Landroid/graphics/Rect;

    .line 66
    invoke-virtual {p1, p3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v1, v1, v0}, Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;->addLevel(IILandroid/graphics/drawable/Drawable;)V

    .line 67
    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;->setLevel(I)Z

    .line 69
    sget v0, Lcom/google/android/apps/chrome/R$dimen;->toolbar_tab_count_text_size_1_digit:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;->mSingleDigitTextSize:F

    .line 71
    sget v0, Lcom/google/android/apps/chrome/R$dimen;->toolbar_tab_count_text_size_2_digit:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;->mDoubleDigitTextSize:F

    .line 73
    if-eqz p2, :cond_0

    sget v0, Lcom/google/android/apps/chrome/R$color;->toolbar_tab_count_color_white:I

    :goto_0
    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;->mDefaultTextColor:I

    .line 75
    if-eqz p2, :cond_1

    sget v0, Lcom/google/android/apps/chrome/R$color;->toolbar_tab_count_color_white_pressed:I

    :goto_1
    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;->mSelectedTextColor:I

    .line 79
    new-instance v0, Landroid/text/TextPaint;

    invoke-direct {v0}, Landroid/text/TextPaint;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;->mTextPaint:Landroid/text/TextPaint;

    .line 80
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;->mTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, v2}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 81
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;->mTextPaint:Landroid/text/TextPaint;

    sget-object v1, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 82
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;->mTextPaint:Landroid/text/TextPaint;

    const-string/jumbo v1, "sans-serif-condensed"

    invoke-static {v1, v2}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 83
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;->mTextPaint:Landroid/text/TextPaint;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;->getColorForState()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setColor(I)V

    .line 84
    return-void

    .line 73
    :cond_0
    sget v0, Lcom/google/android/apps/chrome/R$color;->toolbar_tab_count_color:I

    goto :goto_0

    .line 75
    :cond_1
    sget v0, Lcom/google/android/apps/chrome/R$color;->toolbar_tab_count_color_pressed:I

    goto :goto_1
.end method

.method public static createTabSwitcherDrawable(Landroid/content/res/Resources;Z)Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;
    .locals 2

    .prologue
    .line 45
    if-eqz p1, :cond_0

    sget v0, Lcom/google/android/apps/chrome/R$drawable;->btn_tabswitcher_single_white:I

    .line 47
    :goto_0
    new-instance v1, Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;

    invoke-direct {v1, p0, p1, v0}, Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;-><init>(Landroid/content/res/Resources;ZI)V

    return-object v1

    .line 45
    :cond_0
    sget v0, Lcom/google/android/apps/chrome/R$drawable;->btn_tabswitcher_single:I

    goto :goto_0
.end method

.method public static createTabSwitcherDrawableForTexture(Landroid/content/res/Resources;Z)Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;
    .locals 2

    .prologue
    .line 59
    if-eqz p1, :cond_0

    sget v0, Lcom/google/android/apps/chrome/R$drawable;->btn_tabswitcher_single_white_normal:I

    .line 62
    :goto_0
    new-instance v1, Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;

    invoke-direct {v1, p0, p1, v0}, Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;-><init>(Landroid/content/res/Resources;ZI)V

    return-object v1

    .line 59
    :cond_0
    sget v0, Lcom/google/android/apps/chrome/R$drawable;->btn_tabswitcher_single_normal:I

    goto :goto_0
.end method

.method private getColorForState()I
    .locals 4

    .prologue
    .line 142
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;->getCurrent()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getState()[I

    move-result-object v1

    .line 143
    if-nez v1, :cond_0

    iget v0, p0, Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;->mDefaultTextColor:I

    .line 154
    :goto_0
    return v0

    .line 145
    :cond_0
    const/4 v0, 0x0

    :goto_1
    array-length v2, v1

    if-ge v0, v2, :cond_3

    .line 146
    aget v2, v1, v0

    .line 147
    const v3, 0x101009c

    if-eq v2, v3, :cond_1

    const v3, 0x10100a7

    if-eq v2, v3, :cond_1

    const v3, 0x10100a1

    if-ne v2, v3, :cond_2

    .line 150
    :cond_1
    iget v0, p0, Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;->mSelectedTextColor:I

    goto :goto_0

    .line 145
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 154
    :cond_3
    iget v0, p0, Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;->mDefaultTextColor:I

    goto :goto_0
.end method

.method private getTabCountString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 132
    iget v0, p0, Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;->mTabCount:I

    if-gtz v0, :cond_0

    .line 133
    const-string/jumbo v0, ""

    .line 137
    :goto_0
    return-object v0

    .line 134
    :cond_0
    iget v0, p0, Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;->mTabCount:I

    const/16 v1, 0x63

    if-le v0, v1, :cond_2

    .line 135
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;->mIncognito:Z

    if-eqz v0, :cond_1

    const-string/jumbo v0, ";)"

    goto :goto_0

    :cond_1
    const-string/jumbo v0, ":D"

    goto :goto_0

    .line 137
    :cond_2
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    const-string/jumbo v1, "%d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;->mTabCount:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .locals 5

    .prologue
    .line 95
    invoke-super {p0, p1}, Landroid/graphics/drawable/LevelListDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 97
    invoke-direct {p0}, Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;->getTabCountString()Ljava/lang/String;

    move-result-object v0

    .line 98
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 99
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;->mTextPaint:Landroid/text/TextPaint;

    const/4 v2, 0x0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;->mTextBounds:Landroid/graphics/Rect;

    invoke-virtual {v1, v0, v2, v3, v4}, Landroid/text/TextPaint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 101
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v1

    .line 102
    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    .line 103
    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    iget-object v3, p0, Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;->mTextBounds:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    iget-object v4, p0, Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;->mTextBounds:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    sub-int/2addr v3, v4

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v1, v3

    iget-object v3, p0, Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;->mTextBounds:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v1, v3

    .line 106
    int-to-float v2, v2

    int-to-float v1, v1

    iget-object v3, p0, Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;->mTextPaint:Landroid/text/TextPaint;

    invoke-virtual {p1, v0, v2, v1, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 108
    :cond_0
    return-void
.end method

.method public getTabCount()I
    .locals 1

    .prologue
    .line 115
    iget v0, p0, Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;->mTabCount:I

    return v0
.end method

.method protected onStateChange([I)Z
    .locals 3

    .prologue
    .line 88
    invoke-super {p0, p1}, Landroid/graphics/drawable/LevelListDrawable;->onStateChange([I)Z

    move-result v0

    .line 89
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;->mTextPaint:Landroid/text/TextPaint;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;->getColorForState()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    .line 90
    :cond_0
    return v0
.end method

.method public updateForTabCount(IZ)V
    .locals 2

    .prologue
    .line 123
    iget v0, p0, Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;->mTabCount:I

    if-ne p1, v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;->mIncognito:Z

    if-ne p2, v0, :cond_0

    .line 129
    :goto_0
    return-void

    .line 124
    :cond_0
    iput p1, p0, Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;->mTabCount:I

    .line 125
    iput-boolean p2, p0, Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;->mIncognito:Z

    .line 126
    iget v0, p0, Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;->mTabCount:I

    const/16 v1, 0x9

    if-le v0, v1, :cond_1

    iget v0, p0, Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;->mDoubleDigitTextSize:F

    .line 127
    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;->mTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v0}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 128
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;->invalidateSelf()V

    goto :goto_0

    .line 126
    :cond_1
    iget v0, p0, Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;->mSingleDigitTextSize:F

    goto :goto_1
.end method
