.class Lcom/google/android/apps/chrome/ntp/BookmarkItemView;
.super Landroid/widget/TextView;
.source "BookmarkItemView.java"

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnCreateContextMenuListener;


# static fields
.field static final ID_DELETE:I = 0x2

.field static final ID_EDIT:I = 0x3

.field static final ID_OPEN_IN_INCOGNITO_TAB:I = 0x1

.field static final ID_OPEN_IN_NEW_TAB:I


# instance fields
.field private final mDrawingData:Lcom/google/android/apps/chrome/ntp/BookmarkItemView$DrawingData;

.field private mFavicon:Landroid/graphics/Bitmap;

.field private mId:Lorg/chromium/components/bookmarks/BookmarkId;

.field private mIsEditable:Z

.field private mIsFolder:Z

.field private mIsManaged:Z

.field private final mManager:Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarksPageManager;

.field private mTitle:Ljava/lang/String;

.field private mUrl:Ljava/lang/String;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarksPageManager;Lorg/chromium/components/bookmarks/BookmarkId;Ljava/lang/String;Ljava/lang/String;ZZLcom/google/android/apps/chrome/ntp/BookmarkItemView$DrawingData;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 94
    invoke-direct {p0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 95
    iput-object p2, p0, Lcom/google/android/apps/chrome/ntp/BookmarkItemView;->mManager:Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarksPageManager;

    .line 96
    iput-object p8, p0, Lcom/google/android/apps/chrome/ntp/BookmarkItemView;->mDrawingData:Lcom/google/android/apps/chrome/ntp/BookmarkItemView$DrawingData;

    move-object v0, p0

    move-object v1, p3

    move-object v2, p4

    move-object v3, p5

    move v4, p6

    move v5, p7

    .line 97
    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/chrome/ntp/BookmarkItemView;->reset(Lorg/chromium/components/bookmarks/BookmarkId;Ljava/lang/String;Ljava/lang/String;ZZ)Z

    .line 98
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarkItemView;->mDrawingData:Lcom/google/android/apps/chrome/ntp/BookmarkItemView$DrawingData;

    # getter for: Lcom/google/android/apps/chrome/ntp/BookmarkItemView$DrawingData;->mTextColor:I
    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/BookmarkItemView$DrawingData;->access$000(Lcom/google/android/apps/chrome/ntp/BookmarkItemView$DrawingData;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ntp/BookmarkItemView;->setTextColor(I)V

    .line 99
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarkItemView;->mDrawingData:Lcom/google/android/apps/chrome/ntp/BookmarkItemView$DrawingData;

    # getter for: Lcom/google/android/apps/chrome/ntp/BookmarkItemView$DrawingData;->mTextSize:I
    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/BookmarkItemView$DrawingData;->access$100(Lcom/google/android/apps/chrome/ntp/BookmarkItemView$DrawingData;)I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p0, v6, v0}, Lcom/google/android/apps/chrome/ntp/BookmarkItemView;->setTextSize(IF)V

    .line 100
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarkItemView;->mDrawingData:Lcom/google/android/apps/chrome/ntp/BookmarkItemView$DrawingData;

    # getter for: Lcom/google/android/apps/chrome/ntp/BookmarkItemView$DrawingData;->mMinHeight:I
    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/BookmarkItemView$DrawingData;->access$200(Lcom/google/android/apps/chrome/ntp/BookmarkItemView$DrawingData;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ntp/BookmarkItemView;->setMinimumHeight(I)V

    .line 101
    const/16 v0, 0x10

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ntp/BookmarkItemView;->setGravity(I)V

    .line 102
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ntp/BookmarkItemView;->setSingleLine()V

    .line 103
    sget-object v0, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ntp/BookmarkItemView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 104
    const/4 v0, 0x5

    invoke-static {p0, v0}, Lorg/chromium/base/ApiCompatibilityUtils;->setTextAlignment(Landroid/view/View;I)V

    .line 106
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [I

    const v2, 0x10102f0

    aput v2, v1, v6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 108
    invoke-virtual {v0, v6}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 109
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 110
    invoke-static {p0, v1}, Lorg/chromium/base/ApiCompatibilityUtils;->setBackgroundForView(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 112
    invoke-virtual {p0, p0}, Lcom/google/android/apps/chrome/ntp/BookmarkItemView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 113
    invoke-virtual {p0, p0}, Lcom/google/android/apps/chrome/ntp/BookmarkItemView;->setOnCreateContextMenuListener(Landroid/view/View$OnCreateContextMenuListener;)V

    .line 114
    return-void
.end method


# virtual methods
.method public getBookmarkId()Lorg/chromium/components/bookmarks/BookmarkId;
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarkItemView;->mId:Lorg/chromium/components/bookmarks/BookmarkId;

    return-object v0
.end method

.method public getFavicon()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarkItemView;->mFavicon:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarkItemView;->mTitle:Ljava/lang/String;

    return-object v0
.end method

.method public getUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarkItemView;->mUrl:Ljava/lang/String;

    return-object v0
.end method

.method public isFolder()Z
    .locals 1

    .prologue
    .line 162
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarkItemView;->mIsFolder:Z

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 254
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarkItemView;->mManager:Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarksPageManager;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarksPageManager;->isDestroyed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 256
    :goto_0
    return-void

    .line 255
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarkItemView;->mManager:Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarksPageManager;

    invoke-interface {v0, p0}, Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarksPageManager;->open(Lcom/google/android/apps/chrome/ntp/BookmarkItemView;)V

    goto :goto_0
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 211
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarkItemView;->mManager:Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarksPageManager;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarksPageManager;->isDestroyed()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 229
    :cond_0
    :goto_0
    return-void

    .line 212
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarkItemView;->mManager:Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarksPageManager;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarksPageManager;->isContextMenuEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 213
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarkItemView;->mIsFolder:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarkItemView;->mManager:Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarksPageManager;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarksPageManager;->shouldShowOpenInNewTab()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 214
    sget v0, Lcom/google/android/apps/chrome/R$string;->contextmenu_open_in_new_tab:I

    invoke-interface {p1, v2, v2, v2, v0}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, p0}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 217
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarkItemView;->mIsFolder:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarkItemView;->mManager:Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarksPageManager;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarksPageManager;->shouldShowOpenInNewIncognitoTab()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 218
    const/4 v0, 0x1

    sget v1, Lcom/google/android/apps/chrome/R$string;->contextmenu_open_in_incognito_tab:I

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, p0}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 221
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarkItemView;->mIsEditable:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarkItemView;->mManager:Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarksPageManager;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarksPageManager;->isIncognito()Z

    move-result v0

    if-nez v0, :cond_0

    .line 222
    const/4 v1, 0x3

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarkItemView;->mIsFolder:Z

    if-eqz v0, :cond_4

    sget v0, Lcom/google/android/apps/chrome/R$string;->contextmenu_edit_folder:I

    :goto_1
    invoke-interface {p1, v2, v1, v2, v0}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, p0}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 225
    const/4 v1, 0x2

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarkItemView;->mIsFolder:Z

    if-eqz v0, :cond_5

    sget v0, Lcom/google/android/apps/chrome/R$string;->contextmenu_delete_folder:I

    :goto_2
    invoke-interface {p1, v2, v1, v2, v0}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, p0}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    goto :goto_0

    .line 222
    :cond_4
    sget v0, Lcom/google/android/apps/chrome/R$string;->contextmenu_edit_bookmark:I

    goto :goto_1

    .line 225
    :cond_5
    sget v0, Lcom/google/android/apps/chrome/R$string;->contextmenu_delete_bookmark:I

    goto :goto_2
.end method

.method public onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 233
    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/BookmarkItemView;->mManager:Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarksPageManager;

    invoke-interface {v1}, Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarksPageManager;->isDestroyed()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 248
    :goto_0
    return v0

    .line 234
    :cond_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 248
    const/4 v0, 0x0

    goto :goto_0

    .line 236
    :pswitch_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/BookmarkItemView;->mManager:Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarksPageManager;

    invoke-interface {v1, p0}, Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarksPageManager;->openInNewTab(Lcom/google/android/apps/chrome/ntp/BookmarkItemView;)V

    goto :goto_0

    .line 239
    :pswitch_1
    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/BookmarkItemView;->mManager:Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarksPageManager;

    invoke-interface {v1, p0}, Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarksPageManager;->openInNewIncognitoTab(Lcom/google/android/apps/chrome/ntp/BookmarkItemView;)V

    goto :goto_0

    .line 242
    :pswitch_2
    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/BookmarkItemView;->mManager:Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarksPageManager;

    invoke-interface {v1, p0}, Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarksPageManager;->delete(Lcom/google/android/apps/chrome/ntp/BookmarkItemView;)V

    goto :goto_0

    .line 245
    :pswitch_3
    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/BookmarkItemView;->mManager:Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarksPageManager;

    invoke-interface {v1, p0}, Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarksPageManager;->edit(Lcom/google/android/apps/chrome/ntp/BookmarkItemView;)V

    goto :goto_0

    .line 234
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public reset(Lorg/chromium/components/bookmarks/BookmarkId;Ljava/lang/String;Ljava/lang/String;ZZ)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 128
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ntp/BookmarkItemView;->jumpDrawablesToCurrentState()V

    .line 130
    iget-object v2, p0, Lcom/google/android/apps/chrome/ntp/BookmarkItemView;->mId:Lorg/chromium/components/bookmarks/BookmarkId;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/chrome/ntp/BookmarkItemView;->mId:Lorg/chromium/components/bookmarks/BookmarkId;

    invoke-virtual {v2, p1}, Lorg/chromium/components/bookmarks/BookmarkId;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/chrome/ntp/BookmarkItemView;->mTitle:Ljava/lang/String;

    invoke-static {p2, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/chrome/ntp/BookmarkItemView;->mUrl:Ljava/lang/String;

    invoke-static {p3, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lcom/google/android/apps/chrome/ntp/BookmarkItemView;->mIsEditable:Z

    if-ne p4, v2, :cond_0

    iget-boolean v2, p0, Lcom/google/android/apps/chrome/ntp/BookmarkItemView;->mIsManaged:Z

    if-ne p5, v2, :cond_0

    .line 147
    :goto_0
    return v0

    .line 135
    :cond_0
    iput-object p2, p0, Lcom/google/android/apps/chrome/ntp/BookmarkItemView;->mTitle:Ljava/lang/String;

    .line 136
    iput-object p3, p0, Lcom/google/android/apps/chrome/ntp/BookmarkItemView;->mUrl:Ljava/lang/String;

    .line 137
    iget-object v2, p0, Lcom/google/android/apps/chrome/ntp/BookmarkItemView;->mUrl:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/apps/chrome/ntp/BookmarkItemView;->mIsFolder:Z

    .line 138
    iput-boolean p4, p0, Lcom/google/android/apps/chrome/ntp/BookmarkItemView;->mIsEditable:Z

    .line 139
    iput-boolean p5, p0, Lcom/google/android/apps/chrome/ntp/BookmarkItemView;->mIsManaged:Z

    .line 140
    iput-object p1, p0, Lcom/google/android/apps/chrome/ntp/BookmarkItemView;->mId:Lorg/chromium/components/bookmarks/BookmarkId;

    .line 141
    iget-object v2, p0, Lcom/google/android/apps/chrome/ntp/BookmarkItemView;->mTitle:Ljava/lang/String;

    invoke-virtual {p0, v2}, Lcom/google/android/apps/chrome/ntp/BookmarkItemView;->setText(Ljava/lang/CharSequence;)V

    .line 142
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/google/android/apps/chrome/ntp/BookmarkItemView;->setFavicon(Landroid/graphics/Bitmap;)V

    .line 143
    iget-boolean v2, p0, Lcom/google/android/apps/chrome/ntp/BookmarkItemView;->mIsFolder:Z

    if-eqz v2, :cond_1

    .line 144
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ntp/BookmarkItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/chrome/R$string;->accessibility_bookmark_folder:I

    new-array v4, v1, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/google/android/apps/chrome/ntp/BookmarkItemView;->mTitle:Ljava/lang/String;

    aput-object v5, v4, v0

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ntp/BookmarkItemView;->setContentDescription(Ljava/lang/CharSequence;)V

    :cond_1
    move v0, v1

    .line 147
    goto :goto_0
.end method

.method setFavicon(Landroid/graphics/Bitmap;)V
    .locals 8

    .prologue
    const/4 v5, 0x0

    const/4 v7, 0x0

    .line 180
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarkItemView;->mDrawingData:Lcom/google/android/apps/chrome/ntp/BookmarkItemView$DrawingData;

    # getter for: Lcom/google/android/apps/chrome/ntp/BookmarkItemView$DrawingData;->mPadding:I
    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/BookmarkItemView$DrawingData;->access$300(Lcom/google/android/apps/chrome/ntp/BookmarkItemView$DrawingData;)I

    move-result v3

    .line 182
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarkItemView;->mDrawingData:Lcom/google/android/apps/chrome/ntp/BookmarkItemView$DrawingData;

    # getter for: Lcom/google/android/apps/chrome/ntp/BookmarkItemView$DrawingData;->mPadding:I
    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/BookmarkItemView$DrawingData;->access$300(Lcom/google/android/apps/chrome/ntp/BookmarkItemView$DrawingData;)I

    move-result v1

    .line 184
    iput-object p1, p0, Lcom/google/android/apps/chrome/ntp/BookmarkItemView;->mFavicon:Landroid/graphics/Bitmap;

    .line 185
    if-nez p1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarkItemView;->mIsFolder:Z

    if-eqz v0, :cond_3

    .line 187
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarkItemView;->mIsFolder:Z

    if-eqz v0, :cond_2

    .line 188
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarkItemView;->mIsManaged:Z

    if-eqz v0, :cond_1

    .line 189
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ntp/BookmarkItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Lcom/google/android/apps/chrome/R$drawable;->managed_folder_icon:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 193
    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/chrome/ntp/BookmarkItemView;->mDrawingData:Lcom/google/android/apps/chrome/ntp/BookmarkItemView$DrawingData;

    # getter for: Lcom/google/android/apps/chrome/ntp/BookmarkItemView$DrawingData;->mFaviconContainerSize:I
    invoke-static {v2}, Lcom/google/android/apps/chrome/ntp/BookmarkItemView$DrawingData;->access$400(Lcom/google/android/apps/chrome/ntp/BookmarkItemView$DrawingData;)I

    move-result v2

    move v4, v2

    move v2, v3

    .line 200
    :goto_1
    invoke-virtual {v0, v7, v7, v4, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 201
    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/ntp/BookmarkItemView;->setCompoundDrawablePadding(I)V

    .line 205
    :goto_2
    invoke-static {p0, v2, v7, v3, v7}, Lorg/chromium/base/ApiCompatibilityUtils;->setPaddingRelative(Landroid/view/View;IIII)V

    .line 206
    invoke-static {p0, v0, v5, v5, v5}, Lorg/chromium/base/ApiCompatibilityUtils;->setCompoundDrawablesRelative(Landroid/widget/TextView;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 207
    return-void

    .line 191
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ntp/BookmarkItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Lcom/google/android/apps/chrome/R$drawable;->folder_icon:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    .line 195
    :cond_2
    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ntp/BookmarkItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v0, v2, p1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 196
    iget-object v2, p0, Lcom/google/android/apps/chrome/ntp/BookmarkItemView;->mDrawingData:Lcom/google/android/apps/chrome/ntp/BookmarkItemView$DrawingData;

    # getter for: Lcom/google/android/apps/chrome/ntp/BookmarkItemView$DrawingData;->mFaviconSize:I
    invoke-static {v2}, Lcom/google/android/apps/chrome/ntp/BookmarkItemView$DrawingData;->access$500(Lcom/google/android/apps/chrome/ntp/BookmarkItemView$DrawingData;)I

    move-result v4

    .line 197
    iget-object v2, p0, Lcom/google/android/apps/chrome/ntp/BookmarkItemView;->mDrawingData:Lcom/google/android/apps/chrome/ntp/BookmarkItemView$DrawingData;

    # getter for: Lcom/google/android/apps/chrome/ntp/BookmarkItemView$DrawingData;->mFaviconContainerSize:I
    invoke-static {v2}, Lcom/google/android/apps/chrome/ntp/BookmarkItemView$DrawingData;->access$400(Lcom/google/android/apps/chrome/ntp/BookmarkItemView$DrawingData;)I

    move-result v2

    sub-int/2addr v2, v4

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v2, v3

    .line 198
    iget-object v6, p0, Lcom/google/android/apps/chrome/ntp/BookmarkItemView;->mDrawingData:Lcom/google/android/apps/chrome/ntp/BookmarkItemView$DrawingData;

    # getter for: Lcom/google/android/apps/chrome/ntp/BookmarkItemView$DrawingData;->mFaviconContainerSize:I
    invoke-static {v6}, Lcom/google/android/apps/chrome/ntp/BookmarkItemView$DrawingData;->access$400(Lcom/google/android/apps/chrome/ntp/BookmarkItemView$DrawingData;)I

    move-result v6

    sub-int/2addr v6, v4

    add-int/lit8 v6, v6, 0x1

    div-int/lit8 v6, v6, 0x2

    add-int/2addr v1, v6

    goto :goto_1

    .line 203
    :cond_3
    mul-int/lit8 v0, v3, 0x2

    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/BookmarkItemView;->mDrawingData:Lcom/google/android/apps/chrome/ntp/BookmarkItemView$DrawingData;

    # getter for: Lcom/google/android/apps/chrome/ntp/BookmarkItemView$DrawingData;->mFaviconContainerSize:I
    invoke-static {v1}, Lcom/google/android/apps/chrome/ntp/BookmarkItemView$DrawingData;->access$400(Lcom/google/android/apps/chrome/ntp/BookmarkItemView$DrawingData;)I

    move-result v1

    add-int v2, v0, v1

    move-object v0, v5

    goto :goto_2
.end method
