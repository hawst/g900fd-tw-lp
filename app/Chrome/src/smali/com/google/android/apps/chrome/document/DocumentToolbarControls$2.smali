.class Lcom/google/android/apps/chrome/document/DocumentToolbarControls$2;
.super Lcom/google/android/apps/chrome/document/DocumentTab$DocumentTabObserver;
.source "DocumentToolbarControls.java"


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/document/DocumentToolbarControls;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/document/DocumentToolbarControls;)V
    .locals 0

    .prologue
    .line 76
    iput-object p1, p0, Lcom/google/android/apps/chrome/document/DocumentToolbarControls$2;->this$0:Lcom/google/android/apps/chrome/document/DocumentToolbarControls;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/document/DocumentTab$DocumentTabObserver;-><init>()V

    return-void
.end method


# virtual methods
.method public onLoadProgressChanged(Lorg/chromium/chrome/browser/Tab;I)V
    .locals 2

    .prologue
    .line 94
    const/4 v0, 0x5

    invoke-static {p2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 95
    iget-object v1, p0, Lcom/google/android/apps/chrome/document/DocumentToolbarControls$2;->this$0:Lcom/google/android/apps/chrome/document/DocumentToolbarControls;

    # getter for: Lcom/google/android/apps/chrome/document/DocumentToolbarControls;->mProgressBar:Landroid/widget/ProgressBar;
    invoke-static {v1}, Lcom/google/android/apps/chrome/document/DocumentToolbarControls;->access$200(Lcom/google/android/apps/chrome/document/DocumentToolbarControls;)Landroid/widget/ProgressBar;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 96
    return-void
.end method

.method public onLoadStarted(Lorg/chromium/chrome/browser/Tab;)V
    .locals 2

    .prologue
    .line 89
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentToolbarControls$2;->this$0:Lcom/google/android/apps/chrome/document/DocumentToolbarControls;

    # getter for: Lcom/google/android/apps/chrome/document/DocumentToolbarControls;->mProgressBar:Landroid/widget/ProgressBar;
    invoke-static {v0}, Lcom/google/android/apps/chrome/document/DocumentToolbarControls;->access$200(Lcom/google/android/apps/chrome/document/DocumentToolbarControls;)Landroid/widget/ProgressBar;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 90
    return-void
.end method

.method public onLoadStopped(Lorg/chromium/chrome/browser/Tab;)V
    .locals 2

    .prologue
    .line 100
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentToolbarControls$2;->this$0:Lcom/google/android/apps/chrome/document/DocumentToolbarControls;

    # getter for: Lcom/google/android/apps/chrome/document/DocumentToolbarControls;->mProgressBar:Landroid/widget/ProgressBar;
    invoke-static {v0}, Lcom/google/android/apps/chrome/document/DocumentToolbarControls;->access$200(Lcom/google/android/apps/chrome/document/DocumentToolbarControls;)Landroid/widget/ProgressBar;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 101
    return-void
.end method

.method public onSSLStateUpdated(Lorg/chromium/chrome/browser/Tab;)V
    .locals 2

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentToolbarControls$2;->this$0:Lcom/google/android/apps/chrome/document/DocumentToolbarControls;

    # getter for: Lcom/google/android/apps/chrome/document/DocumentToolbarControls;->mTitleBar:Lcom/google/android/apps/chrome/document/TitleBar;
    invoke-static {v0}, Lcom/google/android/apps/chrome/document/DocumentToolbarControls;->access$100(Lcom/google/android/apps/chrome/document/DocumentToolbarControls;)Lcom/google/android/apps/chrome/document/TitleBar;

    move-result-object v0

    invoke-virtual {p1}, Lorg/chromium/chrome/browser/Tab;->getSecurityLevel()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/document/TitleBar;->onSSLStateChanged(I)V

    .line 85
    return-void
.end method

.method public onUrlUpdated(Lorg/chromium/chrome/browser/Tab;)V
    .locals 2

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentToolbarControls$2;->this$0:Lcom/google/android/apps/chrome/document/DocumentToolbarControls;

    # getter for: Lcom/google/android/apps/chrome/document/DocumentToolbarControls;->mTitleBar:Lcom/google/android/apps/chrome/document/TitleBar;
    invoke-static {v0}, Lcom/google/android/apps/chrome/document/DocumentToolbarControls;->access$100(Lcom/google/android/apps/chrome/document/DocumentToolbarControls;)Lcom/google/android/apps/chrome/document/TitleBar;

    move-result-object v0

    invoke-virtual {p1}, Lorg/chromium/chrome/browser/Tab;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/document/TitleBar;->onUrlChanged(Ljava/lang/String;)V

    .line 80
    return-void
.end method
