.class public abstract Lcom/google/android/apps/chrome/ChromeActivity;
.super Landroid/support/v4/app/k;
.source "ChromeActivity.java"

# interfaces
.implements Lcom/google/android/apps/chrome/ChromeActivityNativeDelegate;
.implements Lcom/google/android/apps/chrome/ChromeBrowserInitializer$BrowserParts;
.implements Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreatorManager;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final NOTIFICATIONS:[I


# instance fields
.field private mContextReporter:Lcom/google/android/apps/chrome/icing/ContextReporter;

.field private mCurrentOrientation:I

.field private mDeferredStartupNotified:Z

.field protected mDestroyed:Z

.field protected final mHandler:Landroid/os/Handler;

.field private mIncognitoTabCreator:Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;

.field protected mIntentHandler:Lcom/google/android/apps/chrome/IntentHandler;

.field protected mIsTablet:Z

.field private mLastUserInteractionTime:J

.field protected mNativeInitializationController:Lcom/google/android/apps/chrome/NativeInitializationController;

.field private final mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

.field protected mOnCreateTimestampMs:J

.field private mPartnerBrowserRefreshNeeded:Z

.field private mPolicyManager:Lcom/google/android/apps/chrome/policy/PolicyManager;

.field private mRegularTabCreator:Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;

.field protected mSavedInstanceState:Landroid/os/Bundle;

.field private mTabContentManager:Lcom/google/android/apps/chrome/compositor/TabContentManager;

.field private mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

.field private mUmaSessionStats:Lcom/google/android/apps/chrome/uma/UmaSessionStats;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 74
    const-class v0, Lcom/google/android/apps/chrome/ChromeActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/ChromeActivity;->$assertionsDisabled:Z

    .line 90
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/apps/chrome/ChromeActivity;->NOTIFICATIONS:[I

    return-void

    .line 74
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 90
    :array_0
    .array-data 4
        0x6
        0x9
        0x1b
    .end array-data
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 122
    invoke-direct {p0}, Landroid/support/v4/app/k;-><init>()V

    .line 107
    iput v0, p0, Lcom/google/android/apps/chrome/ChromeActivity;->mCurrentOrientation:I

    .line 108
    iput-boolean v0, p0, Lcom/google/android/apps/chrome/ChromeActivity;->mPartnerBrowserRefreshNeeded:Z

    .line 116
    iput-boolean v0, p0, Lcom/google/android/apps/chrome/ChromeActivity;->mIsTablet:Z

    .line 123
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/ChromeActivity;->mHandler:Landroid/os/Handler;

    .line 124
    invoke-direct {p0}, Lcom/google/android/apps/chrome/ChromeActivity;->createNotificationHandler()Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/ChromeActivity;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    .line 125
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/ChromeActivity;)V
    .locals 0

    .prologue
    .line 74
    invoke-direct {p0}, Lcom/google/android/apps/chrome/ChromeActivity;->checkOrientation()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/ChromeActivity;)Z
    .locals 1

    .prologue
    .line 74
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/ChromeActivity;->mDeferredStartupNotified:Z

    return v0
.end method

.method static synthetic access$102(Lcom/google/android/apps/chrome/ChromeActivity;Z)Z
    .locals 0

    .prologue
    .line 74
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/ChromeActivity;->mDeferredStartupNotified:Z

    return p1
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/ChromeActivity;)V
    .locals 0

    .prologue
    .line 74
    invoke-direct {p0}, Lcom/google/android/apps/chrome/ChromeActivity;->postDeferredStartupIfNeeded()V

    return-void
.end method

.method private checkOrientation()V
    .locals 2

    .prologue
    .line 641
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    .line 642
    if-nez v0, :cond_1

    .line 651
    :cond_0
    :goto_0
    return-void

    .line 644
    :cond_1
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 645
    if-eqz v0, :cond_0

    .line 647
    iget v1, p0, Lcom/google/android/apps/chrome/ChromeActivity;->mCurrentOrientation:I

    .line 648
    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/ChromeActivity;->mCurrentOrientation:I

    .line 650
    iget v0, p0, Lcom/google/android/apps/chrome/ChromeActivity;->mCurrentOrientation:I

    if-eq v1, v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/chrome/ChromeActivity;->mCurrentOrientation:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ChromeActivity;->onOrientationChange(I)V

    goto :goto_0
.end method

.method private createNotificationHandler()Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;
    .locals 1

    .prologue
    .line 745
    new-instance v0, Lcom/google/android/apps/chrome/ChromeActivity$6;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/ChromeActivity$6;-><init>(Lcom/google/android/apps/chrome/ChromeActivity;)V

    return-object v0
.end method

.method private displayArea()I
    .locals 2

    .prologue
    .line 633
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 634
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 635
    iget v1, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    mul-int/2addr v0, v1

    .line 637
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method private markSessionEnd()V
    .locals 4

    .prologue
    .line 612
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeActivity;->mUmaSessionStats:Lcom/google/android/apps/chrome/uma/UmaSessionStats;

    if-nez v0, :cond_0

    .line 614
    sget-boolean v0, Lcom/google/android/apps/chrome/ChromeActivity;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 618
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeActivity;->mUmaSessionStats:Lcom/google/android/apps/chrome/uma/UmaSessionStats;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/ChromeActivity;->windowArea()I

    move-result v1

    invoke-direct {p0}, Lcom/google/android/apps/chrome/ChromeActivity;->displayArea()I

    move-result v2

    invoke-static {}, Lcom/google/android/apps/chrome/tabmodel/TabWindowManager;->getInstance()Lcom/google/android/apps/chrome/tabmodel/TabWindowManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabmodel/TabWindowManager;->getNumberOfAssignedTabModelSelectors()I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/chrome/uma/UmaSessionStats;->logMultiWindowStats(III)V

    .line 620
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeActivity;->mUmaSessionStats:Lcom/google/android/apps/chrome/uma/UmaSessionStats;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/uma/UmaSessionStats;->logAndEndSession()V

    .line 621
    :cond_1
    return-void
.end method

.method private markSessionResume()V
    .locals 2

    .prologue
    .line 600
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeActivity;->mUmaSessionStats:Lcom/google/android/apps/chrome/uma/UmaSessionStats;

    if-nez v0, :cond_0

    .line 601
    new-instance v0, Lcom/google/android/apps/chrome/uma/UmaSessionStats;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/uma/UmaSessionStats;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/ChromeActivity;->mUmaSessionStats:Lcom/google/android/apps/chrome/uma/UmaSessionStats;

    .line 604
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeActivity;->mUmaSessionStats:Lcom/google/android/apps/chrome/uma/UmaSessionStats;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/uma/UmaSessionStats;->updateMetricsServiceState()V

    .line 605
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeActivity;->mUmaSessionStats:Lcom/google/android/apps/chrome/uma/UmaSessionStats;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeActivity;->getTabModelSelector()Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/uma/UmaSessionStats;->startNewSession(Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;)V

    .line 606
    return-void
.end method

.method private postDeferredStartupIfNeeded()V
    .locals 4

    .prologue
    .line 723
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/ChromeActivity;->mDeferredStartupNotified:Z

    if-nez v0, :cond_0

    .line 726
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeActivity;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/chrome/ChromeActivity$5;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/ChromeActivity$5;-><init>(Lcom/google/android/apps/chrome/ChromeActivity;)V

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 742
    :cond_0
    return-void
.end method

.method private windowArea()I
    .locals 2

    .prologue
    .line 624
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 625
    if-eqz v0, :cond_0

    .line 626
    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    .line 627
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v1

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    mul-int/2addr v0, v1

    .line 629
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method


# virtual methods
.method protected createIntentHandlerDelegate()Lcom/google/android/apps/chrome/IntentHandler$IntentHandlerDelegate;
    .locals 1

    .prologue
    .line 569
    new-instance v0, Lcom/google/android/apps/chrome/ChromeActivity$3;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/ChromeActivity$3;-><init>(Lcom/google/android/apps/chrome/ChromeActivity;)V

    return-object v0
.end method

.method public finishNativeInitialization()V
    .locals 2

    .prologue
    .line 298
    invoke-direct {p0}, Lcom/google/android/apps/chrome/ChromeActivity;->checkOrientation()V

    .line 299
    const v0, 0x1020002

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ChromeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/chrome/ChromeActivity$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/ChromeActivity$1;-><init>(Lcom/google/android/apps/chrome/ChromeActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 309
    new-instance v0, Lcom/google/android/apps/chrome/policy/PolicyManager;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/chrome/policy/PolicyManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/ChromeActivity;->mPolicyManager:Lcom/google/android/apps/chrome/policy/PolicyManager;

    .line 311
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeActivity;->mNativeInitializationController:Lcom/google/android/apps/chrome/NativeInitializationController;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/NativeInitializationController;->onNativeInitializationComplete()V

    .line 313
    sget-object v0, Lcom/google/android/apps/chrome/ChromeActivity;->NOTIFICATIONS:[I

    iget-object v1, p0, Lcom/google/android/apps/chrome/ChromeActivity;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-static {p0, v0, v1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->registerForNotifications(Landroid/content/Context;[ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    .line 315
    return-void
.end method

.method protected getChromeApplication()Lcom/google/android/apps/chrome/ChromeMobileApplication;
    .locals 1

    .prologue
    .line 689
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/ChromeMobileApplication;

    return-object v0
.end method

.method public getContentOffsetProvider()Lcom/google/android/apps/chrome/compositor/ContentOffsetProvider;
    .locals 1

    .prologue
    .line 234
    const/4 v0, 0x0

    return-object v0
.end method

.method public getContentReadbackHandler()Lorg/chromium/content/browser/ContentReadbackHandler;
    .locals 1

    .prologue
    .line 241
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getControlContainerHeight(Landroid/content/res/Resources;)F
    .locals 1

    .prologue
    .line 590
    sget v0, Lcom/google/android/apps/chrome/R$dimen;->control_container_height:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    return v0
.end method

.method public getCurrentContentViewCore()Lorg/chromium/content/browser/ContentViewCore;
    .locals 1

    .prologue
    .line 219
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeActivity;->getCurrentTabModel()Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelUtils;->getCurrentContentViewCore(Lorg/chromium/chrome/browser/tabmodel/TabList;)Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    return-object v0
.end method

.method public getCurrentTab()Lcom/google/android/apps/chrome/tab/ChromeTab;
    .locals 1

    .prologue
    .line 211
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeActivity;->getCurrentTabModel()Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelUtils;->getCurrentTab(Lorg/chromium/chrome/browser/tabmodel/TabList;)Lorg/chromium/chrome/browser/Tab;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->fromTab(Lorg/chromium/chrome/browser/Tab;)Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    return-object v0
.end method

.method public getCurrentTabCreator()Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;
    .locals 1

    .prologue
    .line 176
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeActivity;->getTabModelSelector()Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    move-result-object v0

    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->isIncognitoSelected()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ChromeActivity;->getTabCreator(Z)Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;

    move-result-object v0

    return-object v0
.end method

.method public getCurrentTabModel()Lorg/chromium/chrome/browser/tabmodel/TabModel;
    .locals 1

    .prologue
    .line 202
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeActivity;->getTabModelSelector()Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    move-result-object v0

    .line 203
    if-nez v0, :cond_0

    invoke-static {}, Lorg/chromium/chrome/browser/tabmodel/EmptyTabModel;->getInstance()Lorg/chromium/chrome/browser/tabmodel/EmptyTabModel;

    move-result-object v0

    .line 204
    :goto_0
    return-object v0

    :cond_0
    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->getCurrentModel()Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v0

    goto :goto_0
.end method

.method public getFullscreenManager()Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;
    .locals 1

    .prologue
    .line 227
    const/4 v0, 0x0

    return-object v0
.end method

.method public getLastUserInteractionTime()J
    .locals 2

    .prologue
    .line 565
    iget-wide v0, p0, Lcom/google/android/apps/chrome/ChromeActivity;->mLastUserInteractionTime:J

    return-wide v0
.end method

.method public getTabContentManager()Lcom/google/android/apps/chrome/compositor/TabContentManager;
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeActivity;->mTabContentManager:Lcom/google/android/apps/chrome/compositor/TabContentManager;

    return-object v0
.end method

.method public getTabCreator(Z)Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;
    .locals 1

    .prologue
    .line 158
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeActivity;->mIncognitoTabCreator:Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeActivity;->mRegularTabCreator:Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;

    goto :goto_0
.end method

.method public getTabModelSelector()Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeActivity;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    return-object v0
.end method

.method public hasDoneFirstDraw()Z
    .locals 1

    .prologue
    .line 433
    const/4 v0, 0x1

    return v0
.end method

.method public initializeCompositor()V
    .locals 0

    .prologue
    .line 286
    return-void
.end method

.method public initializeState()V
    .locals 3

    .prologue
    .line 290
    invoke-static {}, Lorg/chromium/base/CommandLine;->getInstance()Lorg/chromium/base/CommandLine;

    move-result-object v0

    const-string/jumbo v1, "enable-test-intents"

    invoke-virtual {v0, v1}, Lorg/chromium/base/CommandLine;->hasSwitch(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/IntentHandler;->setTestIntentsEnabled(Z)V

    .line 292
    new-instance v0, Lcom/google/android/apps/chrome/IntentHandler;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeActivity;->createIntentHandlerDelegate()Lcom/google/android/apps/chrome/IntentHandler$IntentHandlerDelegate;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeActivity;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/chrome/IntentHandler;-><init>(Lcom/google/android/apps/chrome/IntentHandler$IntentHandlerDelegate;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/ChromeActivity;->mIntentHandler:Lcom/google/android/apps/chrome/IntentHandler;

    .line 293
    return-void
.end method

.method public isActivityDestroyed()Z
    .locals 1

    .prologue
    .line 428
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/ChromeActivity;->mDestroyed:Z

    return v0
.end method

.method public isInOverviewMode()Z
    .locals 1

    .prologue
    .line 544
    const/4 v0, 0x0

    return v0
.end method

.method public isOverlayVisible()Z
    .locals 1

    .prologue
    .line 777
    const/4 v0, 0x0

    return v0
.end method

.method public isTablet()Z
    .locals 1

    .prologue
    .line 537
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/ChromeActivity;->mIsTablet:Z

    return v0
.end method

.method public mayShowUpdateInfoBar()Z
    .locals 1

    .prologue
    .line 696
    const/4 v0, 0x1

    return v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 388
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeActivity;->mNativeInitializationController:Lcom/google/android/apps/chrome/NativeInitializationController;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/apps/chrome/NativeInitializationController;->onActivityResult(IILandroid/content/Intent;)V

    .line 389
    return-void
.end method

.method public onActivityResultWithNative(IILandroid/content/Intent;)Z
    .locals 1

    .prologue
    .line 445
    const/4 v0, 0x0

    return v0
.end method

.method protected final onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 333
    invoke-super {p0, p1}, Landroid/support/v4/app/k;->onCreate(Landroid/os/Bundle;)V

    .line 334
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/chrome/ChromeActivity;->mOnCreateTimestampMs:J

    .line 335
    iput-object p1, p0, Lcom/google/android/apps/chrome/ChromeActivity;->mSavedInstanceState:Landroid/os/Bundle;

    .line 337
    invoke-static {p0}, Lcom/google/android/apps/chrome/ChromeBrowserInitializer;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/ChromeBrowserInitializer;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/apps/chrome/ChromeBrowserInitializer;->handlePreNativeStartup(Lcom/google/android/apps/chrome/ChromeBrowserInitializer$BrowserParts;)V

    .line 338
    return-void
.end method

.method public final onCreateWithNative()V
    .locals 1

    .prologue
    .line 393
    invoke-static {p0}, Lcom/google/android/apps/chrome/ChromeBrowserInitializer;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/ChromeBrowserInitializer;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/apps/chrome/ChromeBrowserInitializer;->handlePostNativeStartup(Lcom/google/android/apps/chrome/ChromeBrowserInitializer$BrowserParts;)V

    .line 394
    return-void
.end method

.method protected onDeferredStartup()V
    .locals 2

    .prologue
    .line 705
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "disable_crash_dump_uploading"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 708
    :goto_0
    invoke-static {p0}, Lcom/google/android/apps/chrome/ChromeBrowserInitializer;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/ChromeBrowserInitializer;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/ChromeBrowserInitializer;->onDeferredStartup(Z)V

    .line 710
    invoke-static {p0}, Lcom/google/android/apps/chrome/icing/IcingProvider;->getContextReporter(Lcom/google/android/apps/chrome/ChromeActivity;)Lcom/google/android/apps/chrome/icing/ContextReporter;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/ChromeActivity;->mContextReporter:Lcom/google/android/apps/chrome/icing/ContextReporter;

    .line 711
    new-instance v0, Lcom/google/android/apps/chrome/ChromeActivity$4;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/ChromeActivity$4;-><init>(Lcom/google/android/apps/chrome/ChromeActivity;)V

    invoke-static {p0, v0}, Lcom/google/android/apps/chrome/nfc/BeamController;->registerForBeam(Landroid/app/Activity;Lcom/google/android/apps/chrome/nfc/BeamProvider;)V

    .line 719
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeActivity;->getChromeApplication()Lcom/google/android/apps/chrome/ChromeMobileApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeMobileApplication;->getUpdateInfoBarHelper()Lcom/google/android/apps/chrome/omaha/UpdateInfoBarHelper;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/apps/chrome/omaha/UpdateInfoBarHelper;->checkForUpdateOnBackgroundThread(Lcom/google/android/apps/chrome/ChromeActivity;)V

    .line 720
    return-void

    .line 705
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 129
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/ChromeActivity;->mDestroyed:Z

    .line 130
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeActivity;->mPolicyManager:Lcom/google/android/apps/chrome/policy/PolicyManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeActivity;->mPolicyManager:Lcom/google/android/apps/chrome/policy/PolicyManager;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/policy/PolicyManager;->destroy()V

    .line 131
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeActivity;->mTabContentManager:Lcom/google/android/apps/chrome/compositor/TabContentManager;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeActivity;->mTabContentManager:Lcom/google/android/apps/chrome/compositor/TabContentManager;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/compositor/TabContentManager;->destroy()V

    .line 132
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeActivity;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    if-eqz v0, :cond_2

    .line 133
    sget-object v0, Lcom/google/android/apps/chrome/ChromeActivity;->NOTIFICATIONS:[I

    iget-object v1, p0, Lcom/google/android/apps/chrome/ChromeActivity;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-static {p0, v0, v1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->unregisterForNotifications(Landroid/content/Context;[ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    .line 136
    :cond_2
    invoke-super {p0}, Landroid/support/v4/app/k;->onDestroy()V

    .line 137
    return-void
.end method

.method public onMenuOrKeyboardAction(I)Z
    .locals 1

    .prologue
    .line 479
    const/4 v0, 0x0

    return v0
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 382
    if-nez p1, :cond_0

    .line 384
    :goto_0
    return-void

    .line 383
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeActivity;->mNativeInitializationController:Lcom/google/android/apps/chrome/NativeInitializationController;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/NativeInitializationController;->onNewIntent(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public onNewIntentWithNative(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 438
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeActivity;->mIntentHandler:Lcom/google/android/apps/chrome/IntentHandler;

    invoke-virtual {v0, p0, p1}, Lcom/google/android/apps/chrome/IntentHandler;->shouldIgnoreIntent(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 441
    :goto_0
    return-void

    .line 440
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeActivity;->mIntentHandler:Lcom/google/android/apps/chrome/IntentHandler;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/IntentHandler;->onNewIntent(Landroid/content/Intent;)Z

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 460
    if-eqz p1, :cond_0

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ChromeActivity;->onMenuOrKeyboardAction(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 461
    const/4 v0, 0x1

    .line 463
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/support/v4/app/k;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onOrientationChange(I)V
    .locals 0

    .prologue
    .line 456
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 366
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeActivity;->mNativeInitializationController:Lcom/google/android/apps/chrome/NativeInitializationController;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/NativeInitializationController;->onPause()V

    .line 367
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeActivity;->mContextReporter:Lcom/google/android/apps/chrome/icing/ContextReporter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeActivity;->mContextReporter:Lcom/google/android/apps/chrome/icing/ContextReporter;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/icing/ContextReporter;->disable()V

    .line 368
    :cond_0
    invoke-super {p0}, Landroid/support/v4/app/k;->onPause()V

    .line 369
    return-void
.end method

.method public onPauseWithNative()V
    .locals 1

    .prologue
    .line 414
    invoke-direct {p0}, Lcom/google/android/apps/chrome/ChromeActivity;->markSessionEnd()V

    .line 415
    const-string/jumbo v0, "COOKIES.DAT"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ChromeActivity;->getFileStreamPath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    .line 417
    invoke-static {v0}, Lcom/google/android/apps/chrome/CookiesFetcher;->fetchFromCookieJar(Ljava/lang/String;)V

    .line 418
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 359
    invoke-super {p0}, Landroid/support/v4/app/k;->onResume()V

    .line 360
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeActivity;->mNativeInitializationController:Lcom/google/android/apps/chrome/NativeInitializationController;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/NativeInitializationController;->onResume()V

    .line 361
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeActivity;->mContextReporter:Lcom/google/android/apps/chrome/icing/ContextReporter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeActivity;->mContextReporter:Lcom/google/android/apps/chrome/icing/ContextReporter;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/icing/ContextReporter;->enable()V

    .line 362
    :cond_0
    return-void
.end method

.method public onResumeWithNative()V
    .locals 1

    .prologue
    .line 406
    invoke-direct {p0}, Lcom/google/android/apps/chrome/ChromeActivity;->markSessionResume()V

    .line 407
    const-string/jumbo v0, "COOKIES.DAT"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ChromeActivity;->getFileStreamPath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    .line 409
    invoke-static {v0}, Lcom/google/android/apps/chrome/CookiesFetcher;->restoreToCookieJar(Ljava/lang/String;)V

    .line 410
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 342
    invoke-super {p0, p1}, Landroid/support/v4/app/k;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 343
    return-void
.end method

.method public onShareMenuItemSelected(Lcom/google/android/apps/chrome/tab/ChromeTab;Lorg/chromium/ui/base/WindowAndroid;ZZI)V
    .locals 6

    .prologue
    .line 495
    if-nez p1, :cond_0

    .line 531
    :goto_0
    return-void

    .line 498
    :cond_0
    new-instance v0, Lcom/google/android/apps/chrome/ChromeActivity$2;

    move-object v1, p0

    move v2, p3

    move-object v3, p0

    move-object v4, p1

    move v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/chrome/ChromeActivity$2;-><init>(Lcom/google/android/apps/chrome/ChromeActivity;ZLandroid/app/Activity;Lcom/google/android/apps/chrome/tab/ChromeTab;I)V

    .line 525
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeActivity;->getContentReadbackHandler()Lorg/chromium/content/browser/ContentReadbackHandler;

    move-result-object v1

    .line 526
    if-nez p4, :cond_1

    if-eqz v1, :cond_1

    if-nez p2, :cond_2

    .line 527
    :cond_1
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lorg/chromium/content/browser/ContentReadbackHandler$GetBitmapCallback;->onFinishGetBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 529
    :cond_2
    invoke-virtual {v1, p2, v0}, Lorg/chromium/content/browser/ContentReadbackHandler;->getCompositorBitmapAsync(Lorg/chromium/ui/base/WindowAndroid;Lorg/chromium/content/browser/ContentReadbackHandler$GetBitmapCallback;)V

    goto :goto_0
.end method

.method public onStart()V
    .locals 4

    .prologue
    .line 347
    invoke-super {p0}, Landroid/support/v4/app/k;->onStart()V

    .line 348
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeActivity;->mNativeInitializationController:Lcom/google/android/apps/chrome/NativeInitializationController;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/NativeInitializationController;->onStart()V

    .line 350
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/ChromeActivity;->mPartnerBrowserRefreshNeeded:Z

    if-eqz v0, :cond_0

    .line 351
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/ChromeActivity;->mPartnerBrowserRefreshNeeded:Z

    .line 352
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-wide/16 v2, 0x2710

    invoke-static {v0, v2, v3}, Lcom/google/android/apps/chrome/partnercustomizations/PartnerBrowserCustomizations;->initializeAsync(Landroid/content/Context;J)V

    .line 355
    :cond_0
    return-void
.end method

.method public onStartWithNative()V
    .locals 1

    .prologue
    .line 398
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeActivity;->getChromeApplication()Lcom/google/android/apps/chrome/ChromeMobileApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeMobileApplication;->onStartWithNative()V

    .line 399
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeActivity;->getCurrentTab()Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    .line 400
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->onActivityStart()V

    .line 401
    :cond_0
    invoke-static {p0}, Lcom/google/android/apps/chrome/utilities/FeatureUtilitiesInternal;->isDocumentMode(Landroid/content/Context;)Z

    move-result v0

    invoke-static {v0}, Lorg/chromium/chrome/browser/util/FeatureUtilities;->setDocumentModeEnabled(Z)V

    .line 402
    return-void
.end method

.method public onStartupFailure()V
    .locals 2

    .prologue
    .line 319
    new-instance v0, Lorg/chromium/base/library_loader/ProcessInitException;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Lorg/chromium/base/library_loader/ProcessInitException;-><init>(I)V

    .line 321
    invoke-static {v0}, Lcom/google/android/apps/chrome/ChromeMobileApplication;->reportStartupErrorAndExit(Lorg/chromium/base/library_loader/ProcessInitException;)V

    .line 322
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeActivity;->finish()V

    .line 323
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 373
    invoke-super {p0}, Landroid/support/v4/app/k;->onStop()V

    .line 374
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeActivity;->mNativeInitializationController:Lcom/google/android/apps/chrome/NativeInitializationController;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/NativeInitializationController;->onStop()V

    .line 377
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/ChromeActivity;->mPartnerBrowserRefreshNeeded:Z

    .line 378
    return-void
.end method

.method public onStopWithNative()V
    .locals 1

    .prologue
    .line 422
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeActivity;->getCurrentTab()Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    .line 423
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->onActivityStop()V

    .line 424
    :cond_0
    return-void
.end method

.method public onUserInteraction()V
    .locals 2

    .prologue
    .line 468
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/chrome/ChromeActivity;->mLastUserInteractionTime:J

    .line 469
    return-void
.end method

.method public postInflationStartup()V
    .locals 0

    .prologue
    .line 283
    return-void
.end method

.method public preInflationStartup()V
    .locals 2

    .prologue
    .line 262
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/ChromeActivity;->getControlContainerHeight(Landroid/content/res/Resources;)F

    move-result v1

    invoke-static {v0, p0, v1}, Lcom/google/android/apps/chrome/ApplicationInitialization;->enableFullscreenFlags(Landroid/content/res/Resources;Landroid/content/Context;F)V

    .line 264
    invoke-static {p0}, Lorg/chromium/ui/base/DeviceFormFactor;->isTablet(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/ChromeActivity;->mIsTablet:Z

    .line 265
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$color;->light_background_color:I

    invoke-virtual {v0, v1}, Landroid/view/Window;->setBackgroundDrawableResource(I)V

    .line 266
    return-void
.end method

.method public prepareMenu(Landroid/view/Menu;)V
    .locals 0

    .prologue
    .line 559
    return-void
.end method

.method protected removeWindowBackground()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x1

    .line 657
    .line 659
    :try_start_0
    const-class v0, Landroid/provider/Settings$Secure;

    const-string/jumbo v2, "ACCESSIBILITY_DISPLAY_MAGNIFICATION_ENABLED"

    invoke-virtual {v0, v2}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    .line 661
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 663
    invoke-virtual {v0}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v2

    const-class v3, Ljava/lang/String;

    if-ne v2, v3, :cond_1

    .line 664
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 668
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-static {v2, v0}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    if-ne v0, v1, :cond_1

    .line 670
    const/4 v0, 0x0

    :goto_0
    move v1, v0

    .line 682
    :goto_1
    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 683
    :cond_0
    return-void

    .line 679
    :catch_0
    move-exception v0

    goto :goto_1

    .line 677
    :catch_1
    move-exception v0

    goto :goto_1

    .line 675
    :catch_2
    move-exception v0

    goto :goto_1

    .line 673
    :catch_3
    move-exception v0

    goto :goto_1

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method protected abstract setContentView()V
.end method

.method public final setContentViewAndLoadLibrary()V
    .locals 1

    .prologue
    .line 276
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeActivity;->setContentView()V

    .line 278
    new-instance v0, Lcom/google/android/apps/chrome/NativeInitializationController;

    invoke-direct {v0, p0, p0}, Lcom/google/android/apps/chrome/NativeInitializationController;-><init>(Landroid/content/Context;Lcom/google/android/apps/chrome/ChromeActivityNativeDelegate;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/ChromeActivity;->mNativeInitializationController:Lcom/google/android/apps/chrome/NativeInitializationController;

    .line 279
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeActivity;->mNativeInitializationController:Lcom/google/android/apps/chrome/NativeInitializationController;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/NativeInitializationController;->startBackgroundTasks()V

    .line 280
    return-void
.end method

.method protected setTabContentManager(Lcom/google/android/apps/chrome/compositor/TabContentManager;)V
    .locals 0

    .prologue
    .line 192
    iput-object p1, p0, Lcom/google/android/apps/chrome/ChromeActivity;->mTabContentManager:Lcom/google/android/apps/chrome/compositor/TabContentManager;

    .line 193
    return-void
.end method

.method public setTabCreators(Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;)V
    .locals 0

    .prologue
    .line 167
    iput-object p1, p0, Lcom/google/android/apps/chrome/ChromeActivity;->mRegularTabCreator:Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;

    .line 168
    iput-object p2, p0, Lcom/google/android/apps/chrome/ChromeActivity;->mIncognitoTabCreator:Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;

    .line 169
    return-void
.end method

.method protected setTabModelSelector(Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;)V
    .locals 0

    .prologue
    .line 153
    iput-object p1, p0, Lcom/google/android/apps/chrome/ChromeActivity;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    .line 154
    return-void
.end method

.method public shouldShowAppMenu()Z
    .locals 1

    .prologue
    .line 551
    const/4 v0, 0x0

    return v0
.end method

.method public startActivityForResult(Landroid/content/Intent;I)V
    .locals 2

    .prologue
    .line 250
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v0

    .line 251
    if-eqz v0, :cond_0

    .line 252
    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    .line 253
    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 254
    invoke-static {}, Lcom/google/android/apps/chrome/ActivityLaunchNotifier;->getInstance()Lcom/google/android/apps/chrome/ActivityLaunchNotifier;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/android/apps/chrome/ActivityLaunchNotifier;->onLaunchActivity(Landroid/content/Intent;Landroid/view/Window;)Z

    .line 257
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/k;->startActivityForResult(Landroid/content/Intent;I)V

    .line 258
    return-void
.end method
