.class Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;
.super Landroid/content/BroadcastReceiver;
.source "ChromeDownloadListener.java"

# interfaces
.implements Lorg/chromium/chrome/browser/infobar/InfoBarListeners$Confirm;
.implements Lorg/chromium/content/browser/ContentViewDownloadDelegate;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mPendingAutoOpenDownloads:Ljava/util/HashSet;

.field private mPendingRequest:Lorg/chromium/content/browser/DownloadInfo;

.field private final mTab:Lcom/google/android/apps/chrome/tab/ChromeTab;

.field private final mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;Lcom/google/android/apps/chrome/tab/ChromeTab;)V
    .locals 1

    .prologue
    .line 173
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 68
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;->mPendingAutoOpenDownloads:Ljava/util/HashSet;

    .line 174
    iput-object p1, p0, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;->mContext:Landroid/content/Context;

    .line 175
    iput-object p3, p0, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;->mTab:Lcom/google/android/apps/chrome/tab/ChromeTab;

    .line 176
    iput-object p2, p0, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    .line 177
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;->mPendingRequest:Lorg/chromium/content/browser/DownloadInfo;

    .line 178
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;)Ljava/util/HashSet;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;->mPendingAutoOpenDownloads:Ljava/util/HashSet;

    return-object v0
.end method

.method private alertDownloadFailure(I)V
    .locals 2

    .prologue
    .line 559
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;->mContext:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 560
    return-void
.end method

.method private checkExternalStorageAndNotify(Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 534
    if-eqz p1, :cond_0

    const-string/jumbo v0, "null"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 535
    sget v0, Lcom/google/android/apps/chrome/R$string;->download_no_sdcard_dlg_title:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;->alertDownloadFailure(I)V

    move v0, v1

    .line 555
    :goto_0
    return v0

    .line 540
    :cond_0
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v0

    .line 541
    const-string/jumbo v2, "mounted"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 545
    const-string/jumbo v2, "shared"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 546
    sget v0, Lcom/google/android/apps/chrome/R$string;->download_sdcard_busy_dlg_title:I

    .line 551
    :goto_1
    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;->alertDownloadFailure(I)V

    move v0, v1

    .line 552
    goto :goto_0

    .line 548
    :cond_1
    sget v0, Lcom/google/android/apps/chrome/R$string;->download_no_sdcard_dlg_title:I

    goto :goto_1

    .line 555
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private closeBlankTab()V
    .locals 2

    .prologue
    .line 637
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;->mTab:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->isInitialNavigation()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 639
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    iget-object v1, p0, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;->mTab:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-interface {v0, v1}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->closeTab(Lorg/chromium/chrome/browser/Tab;)Z

    .line 641
    :cond_0
    return-void
.end method

.method private confirmDangerousDownload(Lorg/chromium/content/browser/DownloadInfo;)V
    .locals 10

    .prologue
    .line 429
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;->mPendingRequest:Lorg/chromium/content/browser/DownloadInfo;

    if-eqz v0, :cond_0

    .line 444
    :goto_0
    return-void

    .line 434
    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;->mPendingRequest:Lorg/chromium/content/browser/DownloadInfo;

    .line 437
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;->mTab:Lcom/google/android/apps/chrome/tab/ChromeTab;

    iget-object v1, p0, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;->mPendingRequest:Lorg/chromium/content/browser/DownloadInfo;

    invoke-virtual {v1}, Lorg/chromium/content/browser/DownloadInfo;->getFileName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getDownloadWarningText(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 439
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$string;->ok:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 440
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$string;->cancel:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 442
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;->mTab:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getInfoBarContainer()Lorg/chromium/chrome/browser/infobar/InfoBarContainer;

    move-result-object v0

    new-instance v1, Lorg/chromium/chrome/browser/infobar/ConfirmInfoBar;

    const-wide/16 v2, 0x0

    const/4 v5, 0x0

    const/4 v7, 0x0

    move-object v4, p0

    invoke-direct/range {v1 .. v9}, Lorg/chromium/chrome/browser/infobar/ConfirmInfoBar;-><init>(JLorg/chromium/chrome/browser/infobar/InfoBarListeners$Confirm;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->addInfoBar(Lorg/chromium/chrome/browser/infobar/InfoBar;)V

    goto :goto_0
.end method

.method private static discardFile(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 623
    new-instance v0, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener$3;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener$3;-><init>(Ljava/lang/String;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener$3;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 634
    return-void
.end method

.method private downloadPath(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 181
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;->mContext:Landroid/content/Context;

    sget-object v2, Landroid/os/Environment;->DIRECTORY_DOWNLOADS:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/content/Context;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private encodePath(Ljava/lang/String;)Ljava/lang/String;
    .locals 8

    .prologue
    const/16 v7, 0x5d

    const/16 v6, 0x5b

    const/4 v0, 0x0

    .line 282
    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v2

    .line 285
    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_6

    aget-char v4, v2, v1

    .line 286
    if-eq v4, v6, :cond_0

    if-ne v4, v7, :cond_1

    .line 287
    :cond_0
    const/4 v1, 0x1

    .line 291
    :goto_1
    if-nez v1, :cond_2

    .line 305
    :goto_2
    return-object p1

    .line 285
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 295
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v3, ""

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 296
    array-length v3, v2

    :goto_3
    if-ge v0, v3, :cond_5

    aget-char v4, v2, v0

    .line 297
    if-eq v4, v6, :cond_3

    if-ne v4, v7, :cond_4

    .line 298
    :cond_3
    const/16 v5, 0x25

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 299
    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 296
    :goto_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 301
    :cond_4
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_4

    .line 305
    :cond_5
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_2

    :cond_6
    move v1, v0

    goto :goto_1
.end method

.method private enqueueDownloadManagerRequest(Lorg/chromium/content/browser/DownloadInfo;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 460
    invoke-virtual {p1}, Lorg/chromium/content/browser/DownloadInfo;->getUrl()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 463
    :try_start_0
    new-instance v1, Landroid/app/DownloadManager$Request;

    invoke-direct {v1, v0}, Landroid/app/DownloadManager$Request;-><init>(Landroid/net/Uri;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 470
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;->closeBlankTab()V

    .line 471
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;->showDownloadStartNotification()V

    .line 472
    invoke-virtual {p1}, Lorg/chromium/content/browser/DownloadInfo;->getMimeType()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/app/DownloadManager$Request;->setMimeType(Ljava/lang/String;)Landroid/app/DownloadManager$Request;

    .line 477
    :try_start_1
    sget-object v0, Landroid/os/Environment;->DIRECTORY_DOWNLOADS:Ljava/lang/String;

    invoke-virtual {p1}, Lorg/chromium/content/browser/DownloadInfo;->getFileName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/app/DownloadManager$Request;->setDestinationInExternalPublicDir(Ljava/lang/String;Ljava/lang/String;)Landroid/app/DownloadManager$Request;
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_1

    .line 486
    invoke-virtual {v1}, Landroid/app/DownloadManager$Request;->allowScanningByMediaScanner()V

    .line 487
    invoke-virtual {p1}, Lorg/chromium/content/browser/DownloadInfo;->getDescription()Ljava/lang/String;

    move-result-object v0

    .line 488
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 489
    invoke-virtual {p1}, Lorg/chromium/content/browser/DownloadInfo;->getFileName()Ljava/lang/String;

    move-result-object v0

    .line 491
    :cond_0
    invoke-virtual {v1, v0}, Landroid/app/DownloadManager$Request;->setDescription(Ljava/lang/CharSequence;)Landroid/app/DownloadManager$Request;

    .line 492
    const-string/jumbo v0, "Cookie"

    invoke-virtual {p1}, Lorg/chromium/content/browser/DownloadInfo;->getCookie()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/app/DownloadManager$Request;->addRequestHeader(Ljava/lang/String;Ljava/lang/String;)Landroid/app/DownloadManager$Request;

    .line 493
    const-string/jumbo v0, "Referer"

    invoke-virtual {p1}, Lorg/chromium/content/browser/DownloadInfo;->getReferer()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/app/DownloadManager$Request;->addRequestHeader(Ljava/lang/String;Ljava/lang/String;)Landroid/app/DownloadManager$Request;

    .line 494
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Landroid/app/DownloadManager$Request;->setNotificationVisibility(I)Landroid/app/DownloadManager$Request;

    .line 496
    const-string/jumbo v0, "User-Agent"

    invoke-virtual {p1}, Lorg/chromium/content/browser/DownloadInfo;->getUserAgent()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/app/DownloadManager$Request;->addRequestHeader(Ljava/lang/String;Ljava/lang/String;)Landroid/app/DownloadManager$Request;

    .line 497
    invoke-static {p1}, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;->shouldOpenAfterDownload(Lorg/chromium/content/browser/DownloadInfo;)Z

    move-result v2

    .line 498
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;->mContext:Landroid/content/Context;

    const-string/jumbo v3, "download"

    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/DownloadManager;

    .line 500
    new-instance v3, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener$2;

    invoke-direct {v3, p0, v0, v1, v2}, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener$2;-><init>(Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;Landroid/app/DownloadManager;Landroid/app/DownloadManager$Request;Z)V

    new-array v0, v4, [Ljava/lang/Void;

    invoke-virtual {v3, v0}, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener$2;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 531
    :goto_0
    return-void

    .line 465
    :catch_0
    move-exception v0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;->mContext:Landroid/content/Context;

    sget v1, Lcom/google/android/apps/chrome/R$string;->cannot_download_http_or_https:I

    invoke-static {v0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 480
    :catch_1
    move-exception v0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;->mContext:Landroid/content/Context;

    sget v1, Lcom/google/android/apps/chrome/R$string;->cannot_create_download_directory_title:I

    invoke-static {v0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method static fileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 323
    invoke-static {p0, p2, p1}, Landroid/webkit/URLUtil;->guessFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 324
    const/16 v1, 0x2e

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    .line 325
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v2, 0x1

    if-le v1, v2, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 329
    invoke-static {}, Landroid/webkit/MimeTypeMap;->getSingleton()Landroid/webkit/MimeTypeMap;

    move-result-object v2

    .line 331
    const/4 v3, 0x0

    invoke-virtual {v0, v3, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 332
    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 333
    invoke-virtual {v2, v1}, Landroid/webkit/MimeTypeMap;->getMimeTypeFromExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 339
    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 340
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, "."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 350
    :cond_0
    :goto_0
    return-object v0

    .line 342
    :cond_1
    invoke-virtual {v2, p1}, Landroid/webkit/MimeTypeMap;->getExtensionFromMimeType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 345
    if-eqz v2, :cond_0

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 346
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static isAttachment(Ljava/lang/String;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 357
    if-eqz p0, :cond_0

    const-string/jumbo v3, "attachment"

    const/16 v5, 0xa

    move-object v0, p0

    move v4, v2

    invoke-virtual/range {v0 .. v5}, Ljava/lang/String;->regionMatches(ZILjava/lang/String;II)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return v1

    :cond_0
    move v1, v2

    goto :goto_0
.end method

.method private static isDangerousExtension(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 619
    const-string/jumbo v0, "apk"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private isDangerousFile(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 614
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;->mTab:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->isDownloadDangerous(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Landroid/webkit/MimeTypeMap;->getSingleton()Landroid/webkit/MimeTypeMap;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/webkit/MimeTypeMap;->getExtensionFromMimeType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;->isDangerousExtension(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static remapGenericMimeType(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 594
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "text/plain"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "application/octet-stream"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "octet/stream"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "application/force-download"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 599
    :cond_0
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 602
    :goto_0
    invoke-static {p2}, Landroid/webkit/MimeTypeMap;->getFileExtensionFromUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 603
    invoke-static {}, Landroid/webkit/MimeTypeMap;->getSingleton()Landroid/webkit/MimeTypeMap;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/webkit/MimeTypeMap;->getMimeTypeFromExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 604
    if-eqz v0, :cond_2

    move-object p0, v0

    .line 610
    :cond_1
    :goto_1
    return-object p0

    .line 606
    :cond_2
    const-string/jumbo v0, "dm"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 607
    const-string/jumbo p0, "application/vnd.oma.drm.message"

    goto :goto_1

    :cond_3
    move-object p2, p1

    goto :goto_0
.end method

.method private saveDataUri(Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 190
    :try_start_0
    new-instance v3, Lcom/google/android/apps/chrome/third_party/DataUri;

    invoke-direct {v3, p1}, Lcom/google/android/apps/chrome/third_party/DataUri;-><init>(Ljava/lang/String;)V

    .line 194
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string/jumbo v1, "yyyy-MM-dd-HH-mm-ss-"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 195
    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 196
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/third_party/DataUri;->getMimeType()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {p1, v1, v2}, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;->fileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 197
    invoke-direct {p0, v5}, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;->downloadPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 199
    invoke-direct {p0, v2}, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;->checkExternalStorageAndNotify(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 236
    :goto_0
    return-void

    .line 203
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "download"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/DownloadManager;

    .line 206
    new-instance v0, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener$1;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener$1;-><init>(Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;Ljava/lang/String;Lcom/google/android/apps/chrome/third_party/DataUri;Landroid/app/DownloadManager;Ljava/lang/String;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener$1;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 233
    :catch_0
    move-exception v0

    .line 234
    const-string/jumbo v1, "ChromeDownloadListener"

    const-string/jumbo v2, "Invalid data url "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method static shouldOpenAfterDownload(Lorg/chromium/content/browser/DownloadInfo;)Z
    .locals 2

    .prologue
    .line 367
    invoke-virtual {p0}, Lorg/chromium/content/browser/DownloadInfo;->hasUserGesture()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/chromium/content/browser/DownloadInfo;->getContentDisposition()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;->isAttachment(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lorg/chromium/content/browser/DownloadInfo;->getMimeType()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "application/pdf"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private showDownloadStartNotification()V
    .locals 3

    .prologue
    .line 574
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;->mContext:Landroid/content/Context;

    sget v1, Lcom/google/android/apps/chrome/R$string;->download_pending:I

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 575
    return-void
.end method


# virtual methods
.method public onConfirmInfoBarButtonClicked(Lorg/chromium/chrome/browser/infobar/ConfirmInfoBar;Z)V
    .locals 2

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;->mPendingRequest:Lorg/chromium/content/browser/DownloadInfo;

    invoke-virtual {v0}, Lorg/chromium/content/browser/DownloadInfo;->hasDownloadId()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 73
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;->mTab:Lcom/google/android/apps/chrome/tab/ChromeTab;

    iget-object v1, p0, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;->mPendingRequest:Lorg/chromium/content/browser/DownloadInfo;

    invoke-virtual {v1}, Lorg/chromium/content/browser/DownloadInfo;->getDownloadId()I

    move-result v1

    invoke-virtual {v0, v1, p2}, Lcom/google/android/apps/chrome/tab/ChromeTab;->dangerousDownloadValidated(IZ)V

    .line 74
    if-eqz p2, :cond_0

    .line 75
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;->showDownloadStartNotification()V

    .line 77
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;->closeBlankTab()V

    .line 94
    :cond_1
    :goto_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;->mPendingRequest:Lorg/chromium/content/browser/DownloadInfo;

    .line 95
    invoke-virtual {p1}, Lorg/chromium/chrome/browser/infobar/ConfirmInfoBar;->dismissJavaOnlyInfoBar()V

    .line 96
    return-void

    .line 78
    :cond_2
    if-eqz p2, :cond_4

    .line 80
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;->mPendingRequest:Lorg/chromium/content/browser/DownloadInfo;

    invoke-virtual {v0}, Lorg/chromium/content/browser/DownloadInfo;->isGETRequest()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 81
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;->mPendingRequest:Lorg/chromium/content/browser/DownloadInfo;

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;->enqueueDownloadManagerRequest(Lorg/chromium/content/browser/DownloadInfo;)V

    goto :goto_0

    .line 83
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;->mPendingRequest:Lorg/chromium/content/browser/DownloadInfo;

    invoke-static {v0}, Lorg/chromium/content/browser/DownloadInfo$Builder;->fromDownloadInfo(Lorg/chromium/content/browser/DownloadInfo;)Lorg/chromium/content/browser/DownloadInfo$Builder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lorg/chromium/content/browser/DownloadInfo$Builder;->setIsSuccessful(Z)Lorg/chromium/content/browser/DownloadInfo$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/content/browser/DownloadInfo$Builder;->build()Lorg/chromium/content/browser/DownloadInfo;

    move-result-object v0

    .line 85
    iget-object v1, p0, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/apps/chrome/DownloadManagerService;->getDownloadManagerService(Landroid/content/Context;)Lcom/google/android/apps/chrome/DownloadManagerService;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/DownloadManagerService;->onDownloadCompleted(Lorg/chromium/content/browser/DownloadInfo;)V

    goto :goto_0

    .line 90
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;->mPendingRequest:Lorg/chromium/content/browser/DownloadInfo;

    invoke-virtual {v0}, Lorg/chromium/content/browser/DownloadInfo;->isGETRequest()Z

    move-result v0

    if-nez v0, :cond_1

    .line 91
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;->mPendingRequest:Lorg/chromium/content/browser/DownloadInfo;

    invoke-virtual {v0}, Lorg/chromium/content/browser/DownloadInfo;->getFilePath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;->discardFile(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onDangerousDownload(Ljava/lang/String;I)V
    .locals 2

    .prologue
    .line 448
    new-instance v0, Lorg/chromium/content/browser/DownloadInfo$Builder;

    invoke-direct {v0}, Lorg/chromium/content/browser/DownloadInfo$Builder;-><init>()V

    invoke-virtual {v0, p1}, Lorg/chromium/content/browser/DownloadInfo$Builder;->setFileName(Ljava/lang/String;)Lorg/chromium/content/browser/DownloadInfo$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/chromium/content/browser/DownloadInfo$Builder;->setDescription(Ljava/lang/String;)Lorg/chromium/content/browser/DownloadInfo$Builder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lorg/chromium/content/browser/DownloadInfo$Builder;->setHasDownloadId(Z)Lorg/chromium/content/browser/DownloadInfo$Builder;

    move-result-object v0

    invoke-virtual {v0, p2}, Lorg/chromium/content/browser/DownloadInfo$Builder;->setDownloadId(I)Lorg/chromium/content/browser/DownloadInfo$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/content/browser/DownloadInfo$Builder;->build()Lorg/chromium/content/browser/DownloadInfo;

    move-result-object v0

    .line 453
    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;->confirmDangerousDownload(Lorg/chromium/content/browser/DownloadInfo;)V

    .line 454
    return-void
.end method

.method onDownloadStartNoStream(Lorg/chromium/content/browser/DownloadInfo;)V
    .locals 5

    .prologue
    .line 379
    invoke-virtual {p1}, Lorg/chromium/content/browser/DownloadInfo;->getMimeType()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lorg/chromium/content/browser/DownloadInfo;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lorg/chromium/content/browser/DownloadInfo;->getFileName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;->remapGenericMimeType(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 383
    invoke-virtual {p1}, Lorg/chromium/content/browser/DownloadInfo;->getFileName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lorg/chromium/content/browser/DownloadInfo;->getUrl()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lorg/chromium/content/browser/DownloadInfo;->getContentDisposition()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;->fileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 387
    :goto_0
    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;->downloadPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;->checkExternalStorageAndNotify(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 426
    :cond_0
    :goto_1
    return-void

    .line 383
    :cond_1
    invoke-virtual {p1}, Lorg/chromium/content/browser/DownloadInfo;->getFileName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 391
    :cond_2
    invoke-virtual {p1}, Lorg/chromium/content/browser/DownloadInfo;->getUrl()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/chrome/third_party/DataUri;->isDataUri(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 392
    invoke-virtual {p1}, Lorg/chromium/content/browser/DownloadInfo;->getUrl()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;->saveDataUri(Ljava/lang/String;)V

    goto :goto_1

    .line 399
    :cond_3
    :try_start_0
    new-instance v2, Lcom/google/android/apps/chrome/third_party/WebAddress;

    invoke-virtual {p1}, Lorg/chromium/content/browser/DownloadInfo;->getUrl()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/android/apps/chrome/third_party/WebAddress;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 407
    invoke-virtual {v2}, Lcom/google/android/apps/chrome/third_party/WebAddress;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;->encodePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/chrome/third_party/WebAddress;->setPath(Ljava/lang/String;)V

    .line 408
    invoke-static {p1}, Lorg/chromium/content/browser/DownloadInfo$Builder;->fromDownloadInfo(Lorg/chromium/content/browser/DownloadInfo;)Lorg/chromium/content/browser/DownloadInfo$Builder;

    move-result-object v3

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/third_party/WebAddress;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lorg/chromium/content/browser/DownloadInfo$Builder;->setUrl(Ljava/lang/String;)Lorg/chromium/content/browser/DownloadInfo$Builder;

    move-result-object v3

    invoke-virtual {v3, v1}, Lorg/chromium/content/browser/DownloadInfo$Builder;->setMimeType(Ljava/lang/String;)Lorg/chromium/content/browser/DownloadInfo$Builder;

    move-result-object v3

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/third_party/WebAddress;->getHost()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Lorg/chromium/content/browser/DownloadInfo$Builder;->setDescription(Ljava/lang/String;)Lorg/chromium/content/browser/DownloadInfo$Builder;

    move-result-object v2

    invoke-virtual {v2, v0}, Lorg/chromium/content/browser/DownloadInfo$Builder;->setFileName(Ljava/lang/String;)Lorg/chromium/content/browser/DownloadInfo$Builder;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lorg/chromium/content/browser/DownloadInfo$Builder;->setIsGETRequest(Z)Lorg/chromium/content/browser/DownloadInfo$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lorg/chromium/content/browser/DownloadInfo$Builder;->build()Lorg/chromium/content/browser/DownloadInfo;

    move-result-object v2

    .line 416
    const-string/jumbo v3, "application/x-shockwave-flash"

    invoke-virtual {v2}, Lorg/chromium/content/browser/DownloadInfo;->getMimeType()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 420
    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;->isDangerousFile(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 421
    invoke-direct {p0, v2}, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;->confirmDangerousDownload(Lorg/chromium/content/browser/DownloadInfo;)V

    goto :goto_1

    .line 403
    :catch_0
    move-exception v0

    const-string/jumbo v0, "ChromeDownloadListener"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Exception trying to parse url: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lorg/chromium/content/browser/DownloadInfo;->getUrl()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 424
    :cond_4
    invoke-direct {p0, v2}, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;->enqueueDownloadManagerRequest(Lorg/chromium/content/browser/DownloadInfo;)V

    goto/16 :goto_1
.end method

.method public onDownloadStarted(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 564
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;->isDangerousFile(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 565
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;->showDownloadStartNotification()V

    .line 566
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;->closeBlankTab()V

    .line 568
    :cond_0
    return-void
.end method

.method public onInfoBarDismissed(Lorg/chromium/chrome/browser/infobar/InfoBar;)V
    .locals 3

    .prologue
    .line 100
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;->mPendingRequest:Lorg/chromium/content/browser/DownloadInfo;

    if-eqz v0, :cond_0

    .line 101
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;->mPendingRequest:Lorg/chromium/content/browser/DownloadInfo;

    invoke-virtual {v0}, Lorg/chromium/content/browser/DownloadInfo;->hasDownloadId()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 102
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;->mTab:Lcom/google/android/apps/chrome/tab/ChromeTab;

    iget-object v1, p0, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;->mPendingRequest:Lorg/chromium/content/browser/DownloadInfo;

    invoke-virtual {v1}, Lorg/chromium/content/browser/DownloadInfo;->getDownloadId()I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/tab/ChromeTab;->dangerousDownloadValidated(IZ)V

    .line 109
    :cond_0
    :goto_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;->mPendingRequest:Lorg/chromium/content/browser/DownloadInfo;

    .line 110
    return-void

    .line 103
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;->mPendingRequest:Lorg/chromium/content/browser/DownloadInfo;

    invoke-virtual {v0}, Lorg/chromium/content/browser/DownloadInfo;->isGETRequest()Z

    move-result v0

    if-nez v0, :cond_0

    .line 105
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;->mPendingRequest:Lorg/chromium/content/browser/DownloadInfo;

    invoke-virtual {v0}, Lorg/chromium/content/browser/DownloadInfo;->getFilePath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;->discardFile(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8

    .prologue
    const-wide/16 v4, -0x1

    .line 114
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 115
    const-string/jumbo v1, "android.intent.action.DOWNLOAD_COMPLETE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 116
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "download"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/DownloadManager;

    .line 119
    const-string/jumbo v1, "extra_download_id"

    invoke-virtual {p2, v1, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    .line 120
    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    .line 161
    :cond_0
    :goto_0
    return-void

    .line 122
    :cond_1
    new-instance v1, Landroid/app/DownloadManager$Query;

    invoke-direct {v1}, Landroid/app/DownloadManager$Query;-><init>()V

    const/4 v4, 0x1

    new-array v4, v4, [J

    const/4 v5, 0x0

    aput-wide v2, v4, v5

    invoke-virtual {v1, v4}, Landroid/app/DownloadManager$Query;->setFilterById([J)Landroid/app/DownloadManager$Query;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/DownloadManager;->query(Landroid/app/DownloadManager$Query;)Landroid/database/Cursor;

    move-result-object v1

    .line 123
    const-string/jumbo v4, "status"

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    .line 125
    :goto_1
    iget-object v5, p0, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;->mPendingAutoOpenDownloads:Ljava/util/HashSet;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 126
    invoke-interface {v1, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    .line 128
    sparse-switch v5, :sswitch_data_0

    goto :goto_1

    .line 131
    :sswitch_0
    :try_start_0
    invoke-virtual {v0, v2, v3}, Landroid/app/DownloadManager;->getUriForDownloadedFile(J)Landroid/net/Uri;

    move-result-object v5

    .line 132
    new-instance v6, Landroid/content/Intent;

    const-string/jumbo v7, "android.intent.action.VIEW"

    invoke-direct {v6, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 134
    invoke-virtual {v0, v2, v3}, Landroid/app/DownloadManager;->getMimeTypeForDownloadedFile(J)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v5, v7}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 136
    const/high16 v5, 0x10000000

    invoke-virtual {v6, v5}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 138
    iget-object v5, p0, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;->mContext:Landroid/content/Context;

    invoke-virtual {v5, v6}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 140
    invoke-virtual {v0, v2, v3}, Landroid/app/DownloadManager;->openDownloadedFile(J)Landroid/os/ParcelFileDescriptor;
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1

    .line 147
    :goto_2
    iget-object v5, p0, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;->mPendingAutoOpenDownloads:Ljava/util/HashSet;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    goto :goto_1

    .line 141
    :catch_0
    move-exception v5

    .line 142
    const-string/jumbo v5, "ChromeDownloadListener"

    const-string/jumbo v6, "Activity not found."

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 143
    :catch_1
    move-exception v5

    .line 144
    const-string/jumbo v5, "ChromeDownloadListener"

    const-string/jumbo v6, "Opening downloaded file failed."

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 150
    :sswitch_1
    iget-object v5, p0, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;->mPendingAutoOpenDownloads:Ljava/util/HashSet;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    goto :goto_1

    .line 157
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;->mPendingAutoOpenDownloads:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 158
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;->mContext:Landroid/content/Context;

    invoke-virtual {v0, p0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    goto/16 :goto_0

    .line 128
    nop

    :sswitch_data_0
    .sparse-switch
        0x8 -> :sswitch_0
        0x10 -> :sswitch_1
    .end sparse-switch
.end method

.method public requestHttpGetDownload(Lorg/chromium/content/browser/DownloadInfo;)V
    .locals 5

    .prologue
    .line 250
    invoke-virtual {p1}, Lorg/chromium/content/browser/DownloadInfo;->getContentDisposition()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;->isAttachment(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 252
    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v0, "android.intent.action.VIEW"

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 253
    invoke-virtual {p1}, Lorg/chromium/content/browser/DownloadInfo;->getUrl()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p1}, Lorg/chromium/content/browser/DownloadInfo;->getMimeType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 254
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const/high16 v2, 0x10000

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v2

    .line 256
    if-eqz v2, :cond_1

    .line 257
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v0

    .line 260
    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v3

    iget-object v4, v2, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v4, v4, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v0}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v0

    iget-object v2, v2, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v2, v2, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 265
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 276
    :goto_0
    return-void

    .line 267
    :catch_0
    move-exception v0

    .line 268
    const-string/jumbo v1, "ChromeDownloadListener"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "activity not found for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lorg/chromium/content/browser/DownloadInfo;->getMimeType()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " over "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lorg/chromium/content/browser/DownloadInfo;->getUrl()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 275
    :cond_1
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;->onDownloadStartNoStream(Lorg/chromium/content/browser/DownloadInfo;)V

    goto :goto_0
.end method

.method public shouldInterceptContextMenuDownload(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 651
    invoke-static {p1}, Landroid/webkit/MimeTypeMap;->getFileExtensionFromUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 653
    const-string/jumbo v1, "dm"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 654
    new-instance v0, Lorg/chromium/content/browser/DownloadInfo$Builder;

    invoke-direct {v0}, Lorg/chromium/content/browser/DownloadInfo$Builder;-><init>()V

    invoke-virtual {v0, p1}, Lorg/chromium/content/browser/DownloadInfo$Builder;->setUrl(Ljava/lang/String;)Lorg/chromium/content/browser/DownloadInfo$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/content/browser/DownloadInfo$Builder;->build()Lorg/chromium/content/browser/DownloadInfo;

    move-result-object v0

    .line 655
    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;->onDownloadStartNoStream(Lorg/chromium/content/browser/DownloadInfo;)V

    .line 656
    const/4 v0, 0x1

    .line 658
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
