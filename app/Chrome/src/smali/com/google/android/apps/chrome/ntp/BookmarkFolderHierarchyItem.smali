.class public Lcom/google/android/apps/chrome/ntp/BookmarkFolderHierarchyItem;
.super Landroid/widget/TextView;
.source "BookmarkFolderHierarchyItem.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final mId:Lorg/chromium/components/bookmarks/BookmarkId;

.field private final mManager:Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarksPageManager;

.field private final mTitle:Ljava/lang/String;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarksPageManager;Lorg/chromium/components/bookmarks/BookmarkId;Ljava/lang/String;Z)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 36
    invoke-direct {p0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 38
    iput-object p2, p0, Lcom/google/android/apps/chrome/ntp/BookmarkFolderHierarchyItem;->mManager:Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarksPageManager;

    .line 39
    iput-object p4, p0, Lcom/google/android/apps/chrome/ntp/BookmarkFolderHierarchyItem;->mTitle:Ljava/lang/String;

    .line 40
    iput-object p3, p0, Lcom/google/android/apps/chrome/ntp/BookmarkFolderHierarchyItem;->mId:Lorg/chromium/components/bookmarks/BookmarkId;

    .line 41
    if-nez p5, :cond_0

    invoke-virtual {p0, p0}, Lcom/google/android/apps/chrome/ntp/BookmarkFolderHierarchyItem;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 42
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarkFolderHierarchyItem;->mTitle:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ntp/BookmarkFolderHierarchyItem;->setText(Ljava/lang/CharSequence;)V

    .line 43
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ntp/BookmarkFolderHierarchyItem;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    .line 44
    const/high16 v1, 0x40a00000    # 5.0f

    mul-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v1

    .line 45
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ntp/BookmarkFolderHierarchyItem;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Lcom/google/android/apps/chrome/R$dimen;->bookmark_folder_min_height:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ntp/BookmarkFolderHierarchyItem;->setMinHeight(I)V

    .line 46
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ntp/BookmarkFolderHierarchyItem;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Lcom/google/android/apps/chrome/R$dimen;->bookmark_folder_text_size:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    invoke-virtual {p0, v4, v0}, Lcom/google/android/apps/chrome/ntp/BookmarkFolderHierarchyItem;->setTextSize(IF)V

    .line 48
    if-eqz p5, :cond_1

    sget v0, Lcom/google/android/apps/chrome/R$color;->active_control_color:I

    .line 50
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ntp/BookmarkFolderHierarchyItem;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ntp/BookmarkFolderHierarchyItem;->setTextColor(I)V

    .line 51
    const/16 v0, 0x10

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ntp/BookmarkFolderHierarchyItem;->setGravity(I)V

    .line 53
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    const/4 v2, 0x1

    new-array v2, v2, [I

    const v3, 0x10102f0

    aput v3, v2, v4

    invoke-virtual {v0, v2}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 55
    invoke-virtual {v0, v4}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 56
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 57
    invoke-static {p0, v2}, Lorg/chromium/base/ApiCompatibilityUtils;->setBackgroundForView(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 58
    invoke-virtual {p0, v1, v4, v1, v4}, Lcom/google/android/apps/chrome/ntp/BookmarkFolderHierarchyItem;->setPadding(IIII)V

    .line 59
    return-void

    .line 48
    :cond_1
    sget v0, Lcom/google/android/apps/chrome/R$color;->ntp_list_header_subtext:I

    goto :goto_0
.end method


# virtual methods
.method public getFolderId()Lorg/chromium/components/bookmarks/BookmarkId;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarkFolderHierarchyItem;->mId:Lorg/chromium/components/bookmarks/BookmarkId;

    return-object v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarkFolderHierarchyItem;->mManager:Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarksPageManager;

    invoke-interface {v0, p0}, Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarksPageManager;->openFolder(Lcom/google/android/apps/chrome/ntp/BookmarkFolderHierarchyItem;)V

    .line 64
    return-void
.end method
