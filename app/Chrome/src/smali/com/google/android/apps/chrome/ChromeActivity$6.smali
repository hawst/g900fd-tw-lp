.class Lcom/google/android/apps/chrome/ChromeActivity$6;
.super Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;
.source "ChromeActivity.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/ChromeActivity;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 745
    const-class v0, Lcom/google/android/apps/chrome/ChromeActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/ChromeActivity$6;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lcom/google/android/apps/chrome/ChromeActivity;)V
    .locals 0

    .prologue
    .line 745
    iput-object p1, p0, Lcom/google/android/apps/chrome/ChromeActivity$6;->this$0:Lcom/google/android/apps/chrome/ChromeActivity;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;-><init>()V

    return-void
.end method

.method private showUpdateInfoBarIfNecessary()V
    .locals 2

    .prologue
    .line 766
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeActivity$6;->this$0:Lcom/google/android/apps/chrome/ChromeActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeActivity;->getChromeApplication()Lcom/google/android/apps/chrome/ChromeMobileApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeMobileApplication;->getUpdateInfoBarHelper()Lcom/google/android/apps/chrome/omaha/UpdateInfoBarHelper;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/ChromeActivity$6;->this$0:Lcom/google/android/apps/chrome/ChromeActivity;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/omaha/UpdateInfoBarHelper;->showUpdateInfobarIfNecessary(Lcom/google/android/apps/chrome/ChromeActivity;)V

    .line 768
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 1

    .prologue
    .line 748
    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    .line 761
    sget-boolean v0, Lcom/google/android/apps/chrome/ChromeActivity$6;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 750
    :sswitch_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeActivity$6;->this$0:Lcom/google/android/apps/chrome/ChromeActivity;

    # invokes: Lcom/google/android/apps/chrome/ChromeActivity;->postDeferredStartupIfNeeded()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/ChromeActivity;->access$200(Lcom/google/android/apps/chrome/ChromeActivity;)V

    .line 763
    :cond_0
    :goto_0
    return-void

    .line 753
    :sswitch_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeActivity$6;->this$0:Lcom/google/android/apps/chrome/ChromeActivity;

    # invokes: Lcom/google/android/apps/chrome/ChromeActivity;->postDeferredStartupIfNeeded()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/ChromeActivity;->access$200(Lcom/google/android/apps/chrome/ChromeActivity;)V

    .line 754
    invoke-direct {p0}, Lcom/google/android/apps/chrome/ChromeActivity$6;->showUpdateInfoBarIfNecessary()V

    goto :goto_0

    .line 757
    :sswitch_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeActivity$6;->this$0:Lcom/google/android/apps/chrome/ChromeActivity;

    # invokes: Lcom/google/android/apps/chrome/ChromeActivity;->postDeferredStartupIfNeeded()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/ChromeActivity;->access$200(Lcom/google/android/apps/chrome/ChromeActivity;)V

    .line 758
    invoke-direct {p0}, Lcom/google/android/apps/chrome/ChromeActivity$6;->showUpdateInfoBarIfNecessary()V

    goto :goto_0

    .line 748
    nop

    :sswitch_data_0
    .sparse-switch
        0x6 -> :sswitch_0
        0x9 -> :sswitch_1
        0x1b -> :sswitch_2
    .end sparse-switch
.end method
