.class public interface abstract Lcom/google/android/apps/chrome/third_party/datausagechart/ChartAxis;
.super Ljava/lang/Object;
.source "ChartAxis.java"


# virtual methods
.method public abstract convertToPoint(J)F
.end method

.method public abstract convertToValue(F)J
.end method

.method public abstract getTickPoints()[F
.end method

.method public abstract setBounds(JJ)Z
.end method

.method public abstract setSize(F)Z
.end method
