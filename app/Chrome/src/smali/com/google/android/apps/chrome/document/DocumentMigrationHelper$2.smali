.class final Lcom/google/android/apps/chrome/document/DocumentMigrationHelper$2;
.super Ljava/lang/Object;
.source "DocumentMigrationHelper.java"

# interfaces
.implements Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$OnTabStateReadCallback;


# instance fields
.field final synthetic val$editor:Landroid/content/SharedPreferences$Editor;


# direct methods
.method constructor <init>(Landroid/content/SharedPreferences$Editor;)V
    .locals 0

    .prologue
    .line 255
    iput-object p1, p0, Lcom/google/android/apps/chrome/document/DocumentMigrationHelper$2;->val$editor:Landroid/content/SharedPreferences$Editor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onStateRead(IILjava/lang/String;ZZ)V
    .locals 2

    .prologue
    .line 260
    add-int/lit8 v0, p2, 0x1

    invoke-static {v0}, Lorg/chromium/chrome/browser/Tab;->incrementIdCounterTo(I)V

    .line 261
    if-nez p4, :cond_0

    .line 267
    :goto_0
    return-void

    .line 265
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentMigrationHelper$2;->val$editor:Landroid/content/SharedPreferences$Editor;

    const-string/jumbo v1, "pref_last_used_tab_id"

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 266
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentMigrationHelper$2;->val$editor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_0
.end method
