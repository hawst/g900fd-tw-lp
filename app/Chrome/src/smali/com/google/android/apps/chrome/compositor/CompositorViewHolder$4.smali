.class Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$4;
.super Ljava/lang/Object;
.source "CompositorViewHolder.java"

# interfaces
.implements Landroid/view/View$OnLayoutChangeListener;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;)V
    .locals 0

    .prologue
    .line 289
    iput-object p1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$4;->this$0:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLayoutChange(Landroid/view/View;IIIIIIII)V
    .locals 4

    .prologue
    .line 294
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$4;->this$0:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    sub-int v1, p4, p2

    sub-int v2, p5, p3

    # invokes: Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->propagateViewportToLayouts(II)V
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->access$700(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;II)V

    .line 301
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$4;->this$0:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    # getter for: Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mPostHideKeyboardTask:Ljava/lang/Runnable;
    invoke-static {v0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->access$800(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;)Ljava/lang/Runnable;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 302
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$4;->this$0:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    # getter for: Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mPostHideKeyboardTask:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->access$800(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;)Ljava/lang/Runnable;

    move-result-object v1

    const-wide/16 v2, 0x1e

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 303
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$4;->this$0:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mPostHideKeyboardTask:Ljava/lang/Runnable;
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->access$802(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 305
    :cond_0
    return-void
.end method
