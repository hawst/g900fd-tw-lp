.class public Lcom/google/android/apps/chrome/compositor/resources/statics/StaticResource;
.super Ljava/lang/Object;
.source "StaticResource.java"

# interfaces
.implements Lcom/google/android/apps/chrome/compositor/resources/Resource;


# instance fields
.field private final mBitmap:Landroid/graphics/Bitmap;

.field private final mBitmapSize:Landroid/graphics/Rect;

.field private final mNinePatchData:Lcom/google/android/apps/chrome/compositor/resources/statics/NinePatchData;


# direct methods
.method private constructor <init>(Landroid/graphics/Bitmap;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/google/android/apps/chrome/compositor/resources/statics/StaticResource;->mBitmap:Landroid/graphics/Bitmap;

    .line 27
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/resources/statics/StaticResource;->mBitmap:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/google/android/apps/chrome/compositor/resources/statics/NinePatchData;->create(Landroid/graphics/Bitmap;)Lcom/google/android/apps/chrome/compositor/resources/statics/NinePatchData;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/compositor/resources/statics/StaticResource;->mNinePatchData:Lcom/google/android/apps/chrome/compositor/resources/statics/NinePatchData;

    .line 28
    new-instance v0, Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/google/android/apps/chrome/compositor/resources/statics/StaticResource;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/compositor/resources/statics/StaticResource;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    invoke-direct {v0, v3, v3, v1, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/compositor/resources/statics/StaticResource;->mBitmapSize:Landroid/graphics/Rect;

    .line 29
    return-void
.end method

.method public static create(Landroid/content/res/Resources;I)Lcom/google/android/apps/chrome/compositor/resources/statics/StaticResource;
    .locals 2

    .prologue
    .line 61
    invoke-static {p0, p1}, Lcom/google/android/apps/chrome/compositor/resources/statics/StaticResource;->decodeBitmap(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 62
    if-nez v0, :cond_1

    invoke-static {p0, p1}, Lcom/google/android/apps/chrome/compositor/resources/statics/StaticResource;->decodeDrawable(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    move-object v1, v0

    .line 63
    :goto_0
    if-nez v1, :cond_0

    const/4 v0, 0x0

    .line 65
    :goto_1
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/android/apps/chrome/compositor/resources/statics/StaticResource;

    invoke-direct {v0, v1}, Lcom/google/android/apps/chrome/compositor/resources/statics/StaticResource;-><init>(Landroid/graphics/Bitmap;)V

    goto :goto_1

    :cond_1
    move-object v1, v0

    goto :goto_0
.end method

.method private static decodeBitmap(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v5, 0x0

    .line 69
    new-instance v2, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v2}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 70
    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iput-object v1, v2, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 71
    invoke-static {p0, p1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 73
    if-nez v1, :cond_0

    .line 81
    :goto_0
    return-object v0

    .line 74
    :cond_0
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v3

    iget-object v4, v2, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    if-ne v3, v4, :cond_1

    move-object v0, v1

    goto :goto_0

    .line 76
    :cond_1
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    iget-object v2, v2, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v4, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 78
    new-instance v3, Landroid/graphics/Canvas;

    invoke-direct {v3, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 79
    invoke-virtual {v3, v1, v5, v5, v0}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 80
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    move-object v0, v2

    .line 81
    goto :goto_0
.end method

.method private static decodeDrawable(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;
    .locals 7

    .prologue
    .line 86
    :try_start_0
    invoke-virtual {p0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 87
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getMinimumWidth()I

    move-result v0

    const/4 v2, 0x1

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 88
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getMinimumHeight()I

    move-result v0

    const/4 v3, 0x1

    invoke-static {v0, v3}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 89
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v2, v3, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 90
    new-instance v4, Landroid/graphics/Canvas;

    invoke-direct {v4, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 91
    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v1, v5, v6, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 92
    invoke-virtual {v1, v4}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 95
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getAperture()Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/resources/statics/StaticResource;->mNinePatchData:Lcom/google/android/apps/chrome/compositor/resources/statics/NinePatchData;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/resources/statics/StaticResource;->mNinePatchData:Lcom/google/android/apps/chrome/compositor/resources/statics/NinePatchData;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/compositor/resources/statics/NinePatchData;->getAperture()Landroid/graphics/Rect;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/resources/statics/StaticResource;->mBitmapSize:Landroid/graphics/Rect;

    goto :goto_0
.end method

.method public getBitmap()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/resources/statics/StaticResource;->mBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getBitmapSize()Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/resources/statics/StaticResource;->mBitmapSize:Landroid/graphics/Rect;

    return-object v0
.end method

.method public getPadding()Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/resources/statics/StaticResource;->mNinePatchData:Lcom/google/android/apps/chrome/compositor/resources/statics/NinePatchData;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/resources/statics/StaticResource;->mNinePatchData:Lcom/google/android/apps/chrome/compositor/resources/statics/NinePatchData;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/compositor/resources/statics/NinePatchData;->getPadding()Landroid/graphics/Rect;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/resources/statics/StaticResource;->mBitmapSize:Landroid/graphics/Rect;

    goto :goto_0
.end method
