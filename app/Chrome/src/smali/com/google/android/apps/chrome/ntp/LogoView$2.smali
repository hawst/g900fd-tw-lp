.class Lcom/google/android/apps/chrome/ntp/LogoView$2;
.super Ljava/lang/Object;
.source "LogoView.java"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/ntp/LogoView;

.field final synthetic val$contentDescription:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/ntp/LogoView;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 130
    iput-object p1, p0, Lcom/google/android/apps/chrome/ntp/LogoView$2;->this$0:Lcom/google/android/apps/chrome/ntp/LogoView;

    iput-object p2, p0, Lcom/google/android/apps/chrome/ntp/LogoView$2;->val$contentDescription:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 1

    .prologue
    .line 153
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/ntp/LogoView$2;->onAnimationEnd(Landroid/animation/Animator;)V

    .line 154
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/LogoView$2;->this$0:Lcom/google/android/apps/chrome/ntp/LogoView;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ntp/LogoView;->invalidate()V

    .line 155
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 141
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/LogoView$2;->this$0:Lcom/google/android/apps/chrome/ntp/LogoView;

    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/LogoView$2;->this$0:Lcom/google/android/apps/chrome/ntp/LogoView;

    # getter for: Lcom/google/android/apps/chrome/ntp/LogoView;->mNewLogo:Landroid/graphics/Bitmap;
    invoke-static {v1}, Lcom/google/android/apps/chrome/ntp/LogoView;->access$200(Lcom/google/android/apps/chrome/ntp/LogoView;)Landroid/graphics/Bitmap;

    move-result-object v1

    # setter for: Lcom/google/android/apps/chrome/ntp/LogoView;->mLogo:Landroid/graphics/Bitmap;
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/ntp/LogoView;->access$102(Lcom/google/android/apps/chrome/ntp/LogoView;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 142
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/LogoView$2;->this$0:Lcom/google/android/apps/chrome/ntp/LogoView;

    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/LogoView$2;->this$0:Lcom/google/android/apps/chrome/ntp/LogoView;

    # getter for: Lcom/google/android/apps/chrome/ntp/LogoView;->mNewLogoMatrix:Landroid/graphics/Matrix;
    invoke-static {v1}, Lcom/google/android/apps/chrome/ntp/LogoView;->access$400(Lcom/google/android/apps/chrome/ntp/LogoView;)Landroid/graphics/Matrix;

    move-result-object v1

    # setter for: Lcom/google/android/apps/chrome/ntp/LogoView;->mLogoMatrix:Landroid/graphics/Matrix;
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/ntp/LogoView;->access$302(Lcom/google/android/apps/chrome/ntp/LogoView;Landroid/graphics/Matrix;)Landroid/graphics/Matrix;

    .line 143
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/LogoView$2;->this$0:Lcom/google/android/apps/chrome/ntp/LogoView;

    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/LogoView$2;->this$0:Lcom/google/android/apps/chrome/ntp/LogoView;

    # getter for: Lcom/google/android/apps/chrome/ntp/LogoView;->mNewLogoIsDefault:Z
    invoke-static {v1}, Lcom/google/android/apps/chrome/ntp/LogoView;->access$600(Lcom/google/android/apps/chrome/ntp/LogoView;)Z

    move-result v1

    # setter for: Lcom/google/android/apps/chrome/ntp/LogoView;->mLogoIsDefault:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/ntp/LogoView;->access$502(Lcom/google/android/apps/chrome/ntp/LogoView;Z)Z

    .line 144
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/LogoView$2;->this$0:Lcom/google/android/apps/chrome/ntp/LogoView;

    # setter for: Lcom/google/android/apps/chrome/ntp/LogoView;->mNewLogo:Landroid/graphics/Bitmap;
    invoke-static {v0, v2}, Lcom/google/android/apps/chrome/ntp/LogoView;->access$202(Lcom/google/android/apps/chrome/ntp/LogoView;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 145
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/LogoView$2;->this$0:Lcom/google/android/apps/chrome/ntp/LogoView;

    # setter for: Lcom/google/android/apps/chrome/ntp/LogoView;->mNewLogoMatrix:Landroid/graphics/Matrix;
    invoke-static {v0, v2}, Lcom/google/android/apps/chrome/ntp/LogoView;->access$402(Lcom/google/android/apps/chrome/ntp/LogoView;Landroid/graphics/Matrix;)Landroid/graphics/Matrix;

    .line 146
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/LogoView$2;->this$0:Lcom/google/android/apps/chrome/ntp/LogoView;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/apps/chrome/ntp/LogoView;->mTransitionAmount:F
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/ntp/LogoView;->access$002(Lcom/google/android/apps/chrome/ntp/LogoView;F)F

    .line 147
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/LogoView$2;->this$0:Lcom/google/android/apps/chrome/ntp/LogoView;

    # setter for: Lcom/google/android/apps/chrome/ntp/LogoView;->mAnimation:Landroid/animation/ObjectAnimator;
    invoke-static {v0, v2}, Lcom/google/android/apps/chrome/ntp/LogoView;->access$702(Lcom/google/android/apps/chrome/ntp/LogoView;Landroid/animation/ObjectAnimator;)Landroid/animation/ObjectAnimator;

    .line 148
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/LogoView$2;->this$0:Lcom/google/android/apps/chrome/ntp/LogoView;

    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/LogoView$2;->val$contentDescription:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/ntp/LogoView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 149
    return-void
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0

    .prologue
    .line 137
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 0

    .prologue
    .line 133
    return-void
.end method
