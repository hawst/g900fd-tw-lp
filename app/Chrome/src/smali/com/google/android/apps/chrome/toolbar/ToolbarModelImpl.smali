.class Lcom/google/android/apps/chrome/toolbar/ToolbarModelImpl;
.super Lorg/chromium/chrome/browser/toolbar/ToolbarModel;
.source "ToolbarModelImpl.java"

# interfaces
.implements Lcom/google/android/apps/chrome/toolbar/ToolbarDataProvider;
.implements Lorg/chromium/chrome/browser/toolbar/ToolbarModel$ToolbarModelDelegate;


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private mIsIncognito:Z

.field private mIsUsingBrandColor:Z

.field private mLoadProgress:I

.field private mPrimaryColor:I

.field private mTab:Lcom/google/android/apps/chrome/tab/ChromeTab;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const-class v0, Lcom/google/android/apps/chrome/toolbar/ToolbarModelImpl;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/toolbar/ToolbarModelImpl;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Lorg/chromium/chrome/browser/toolbar/ToolbarModel;-><init>()V

    return-void
.end method


# virtual methods
.method public getActiveWebContents()Lorg/chromium/content_public/browser/WebContents;
    .locals 3

    .prologue
    .line 41
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarModelImpl;->getTab()Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v1

    .line 42
    if-nez v1, :cond_0

    const/4 v0, 0x0

    .line 49
    :goto_0
    return-object v0

    .line 43
    :cond_0
    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getBackgroundContentViewHelper()Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;

    move-result-object v2

    .line 44
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->hasPendingBackgroundPage()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 46
    :goto_1
    if-eqz v0, :cond_2

    .line 47
    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->getWebContents()Lorg/chromium/content_public/browser/WebContents;

    move-result-object v0

    goto :goto_0

    .line 44
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 49
    :cond_2
    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getWebContents()Lorg/chromium/content_public/browser/WebContents;

    move-result-object v0

    goto :goto_0
.end method

.method public getLoadProgress()I
    .locals 1

    .prologue
    .line 101
    iget v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarModelImpl;->mLoadProgress:I

    return v0
.end method

.method public getNewTabPageForCurrentTab()Lcom/google/android/apps/chrome/ntp/NewTabPage;
    .locals 2

    .prologue
    .line 76
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarModelImpl;->getTab()Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    .line 77
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/Tab;->getNativePage()Lorg/chromium/chrome/browser/NativePage;

    move-result-object v1

    instance-of v1, v1, Lcom/google/android/apps/chrome/ntp/NewTabPage;

    if-eqz v1, :cond_0

    .line 78
    invoke-virtual {v0}, Lorg/chromium/chrome/browser/Tab;->getNativePage()Lorg/chromium/chrome/browser/NativePage;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/ntp/NewTabPage;

    .line 80
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getPrimaryColor()I
    .locals 1

    .prologue
    .line 119
    iget v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarModelImpl;->mPrimaryColor:I

    return v0
.end method

.method public getTab()Lcom/google/android/apps/chrome/tab/ChromeTab;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarModelImpl;->mTab:Lcom/google/android/apps/chrome/tab/ChromeTab;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarModelImpl;->mTab:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarModelImpl;->mTab:Lcom/google/android/apps/chrome/tab/ChromeTab;

    goto :goto_0
.end method

.method public bridge synthetic getTab()Lorg/chromium/chrome/browser/Tab;
    .locals 1

    .prologue
    .line 24
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarModelImpl;->getTab()Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    return-object v0
.end method

.method public initializeWithNative()V
    .locals 0

    .prologue
    .line 36
    invoke-virtual {p0, p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarModelImpl;->initialize(Lorg/chromium/chrome/browser/toolbar/ToolbarModel$ToolbarModelDelegate;)V

    .line 37
    return-void
.end method

.method public isIncognito()Z
    .locals 1

    .prologue
    .line 85
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarModelImpl;->mIsIncognito:Z

    return v0
.end method

.method public isUsingBrandColor()Z
    .locals 1

    .prologue
    .line 124
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarModelImpl;->mIsUsingBrandColor:Z

    return v0
.end method

.method public setLoadProgress(I)V
    .locals 1

    .prologue
    .line 93
    sget-boolean v0, Lcom/google/android/apps/chrome/toolbar/ToolbarModelImpl;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-gez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 94
    :cond_0
    sget-boolean v0, Lcom/google/android/apps/chrome/toolbar/ToolbarModelImpl;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    const/16 v0, 0x64

    if-le p1, v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 96
    :cond_1
    iput p1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarModelImpl;->mLoadProgress:I

    .line 97
    return-void
.end method

.method public setPrimaryColor(I)V
    .locals 1

    .prologue
    .line 109
    iput p1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarModelImpl;->mPrimaryColor:I

    .line 110
    invoke-static {}, Lorg/chromium/base/ApplicationStatus;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 111
    invoke-static {v0}, Lcom/google/android/apps/chrome/utilities/FeatureUtilitiesInternal;->isDocumentMode(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarModelImpl;->isIncognito()Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarModelImpl;->mPrimaryColor:I

    invoke-static {v0}, Lcom/google/android/apps/chrome/document/BrandColorUtils;->isDefaultPrimaryColor(I)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarModelImpl;->getTab()Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarModelImpl;->getTab()Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->isNativePage()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarModelImpl;->mIsUsingBrandColor:Z

    .line 115
    return-void

    .line 111
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setTab(Lcom/google/android/apps/chrome/tab/ChromeTab;Z)V
    .locals 1

    .prologue
    .line 59
    iput-object p1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarModelImpl;->mTab:Lcom/google/android/apps/chrome/tab/ChromeTab;

    .line 60
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarModelImpl;->mTab:Lcom/google/android/apps/chrome/tab/ChromeTab;

    if-eqz v0, :cond_0

    .line 61
    sget-boolean v0, Lcom/google/android/apps/chrome/toolbar/ToolbarModelImpl;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarModelImpl;->mTab:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->isIncognito()Z

    move-result v0

    if-eq v0, p2, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 63
    :cond_0
    iput-boolean p2, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarModelImpl;->mIsIncognito:Z

    .line 64
    return-void
.end method
