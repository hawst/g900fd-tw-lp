.class Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager$1;
.super Lorg/chromium/net/IRemoteAndroidKeyStoreCallbacks$Stub;
.source "DelegatedPKCS11Manager.java"


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;)V
    .locals 0

    .prologue
    .line 67
    iput-object p1, p0, Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager$1;->this$0:Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;

    invoke-direct {p0}, Lorg/chromium/net/IRemoteAndroidKeyStoreCallbacks$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onDisabled()V
    .locals 2

    .prologue
    .line 70
    const-string/jumbo v0, "DelegatedPKCS11Manager"

    const-string/jumbo v1, "onDisabled"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 71
    iget-object v0, p0, Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager$1;->this$0:Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;

    # invokes: Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;->disconnectFromRemote()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;->access$000(Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;)V

    .line 72
    # getter for: Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;->CONNECTION_LOCK:Ljava/lang/Object;
    invoke-static {}, Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;->access$100()Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 73
    :try_start_0
    # getter for: Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;->CONNECTION_LOCK:Ljava/lang/Object;
    invoke-static {}, Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;->access$100()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 74
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public onInitComplete()V
    .locals 2

    .prologue
    .line 79
    const-string/jumbo v0, "DelegatedPKCS11Manager"

    const-string/jumbo v1, "onInitializationComplete"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 80
    iget-object v0, p0, Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager$1;->this$0:Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;->mRemoteIsReady:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;->access$202(Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;Z)Z

    .line 81
    invoke-static {}, Lorg/chromium/chrome/browser/SSLClientCertificateRequest;->notifyClientCertificatesChangedOnIOThread()V

    .line 82
    # getter for: Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;->CONNECTION_LOCK:Ljava/lang/Object;
    invoke-static {}, Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;->access$100()Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 83
    :try_start_0
    # getter for: Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;->CONNECTION_LOCK:Ljava/lang/Object;
    invoke-static {}, Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;->access$100()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 84
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
