.class public final enum Lcom/google/android/apps/chrome/preferences/NetworkPredictionOptions;
.super Ljava/lang/Enum;
.source "NetworkPredictionOptions.java"


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/chrome/preferences/NetworkPredictionOptions;

.field static final synthetic $assertionsDisabled:Z

.field public static final DEFAULT:Lcom/google/android/apps/chrome/preferences/NetworkPredictionOptions;

.field public static final enum NETWORK_PREDICTION_ALWAYS:Lcom/google/android/apps/chrome/preferences/NetworkPredictionOptions;

.field public static final enum NETWORK_PREDICTION_NEVER:Lcom/google/android/apps/chrome/preferences/NetworkPredictionOptions;

.field public static final enum NETWORK_PREDICTION_WIFI_ONLY:Lcom/google/android/apps/chrome/preferences/NetworkPredictionOptions;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 15
    const-class v0, Lcom/google/android/apps/chrome/preferences/NetworkPredictionOptions;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/preferences/NetworkPredictionOptions;->$assertionsDisabled:Z

    .line 16
    new-instance v0, Lcom/google/android/apps/chrome/preferences/NetworkPredictionOptions;

    const-string/jumbo v3, "NETWORK_PREDICTION_ALWAYS"

    invoke-direct {v0, v3, v2}, Lcom/google/android/apps/chrome/preferences/NetworkPredictionOptions;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/preferences/NetworkPredictionOptions;->NETWORK_PREDICTION_ALWAYS:Lcom/google/android/apps/chrome/preferences/NetworkPredictionOptions;

    .line 17
    new-instance v0, Lcom/google/android/apps/chrome/preferences/NetworkPredictionOptions;

    const-string/jumbo v3, "NETWORK_PREDICTION_WIFI_ONLY"

    invoke-direct {v0, v3, v1}, Lcom/google/android/apps/chrome/preferences/NetworkPredictionOptions;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/preferences/NetworkPredictionOptions;->NETWORK_PREDICTION_WIFI_ONLY:Lcom/google/android/apps/chrome/preferences/NetworkPredictionOptions;

    .line 18
    new-instance v0, Lcom/google/android/apps/chrome/preferences/NetworkPredictionOptions;

    const-string/jumbo v3, "NETWORK_PREDICTION_NEVER"

    invoke-direct {v0, v3, v4}, Lcom/google/android/apps/chrome/preferences/NetworkPredictionOptions;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/preferences/NetworkPredictionOptions;->NETWORK_PREDICTION_NEVER:Lcom/google/android/apps/chrome/preferences/NetworkPredictionOptions;

    .line 15
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/apps/chrome/preferences/NetworkPredictionOptions;

    sget-object v3, Lcom/google/android/apps/chrome/preferences/NetworkPredictionOptions;->NETWORK_PREDICTION_ALWAYS:Lcom/google/android/apps/chrome/preferences/NetworkPredictionOptions;

    aput-object v3, v0, v2

    sget-object v2, Lcom/google/android/apps/chrome/preferences/NetworkPredictionOptions;->NETWORK_PREDICTION_WIFI_ONLY:Lcom/google/android/apps/chrome/preferences/NetworkPredictionOptions;

    aput-object v2, v0, v1

    sget-object v1, Lcom/google/android/apps/chrome/preferences/NetworkPredictionOptions;->NETWORK_PREDICTION_NEVER:Lcom/google/android/apps/chrome/preferences/NetworkPredictionOptions;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/apps/chrome/preferences/NetworkPredictionOptions;->$VALUES:[Lcom/google/android/apps/chrome/preferences/NetworkPredictionOptions;

    .line 20
    sget-object v0, Lcom/google/android/apps/chrome/preferences/NetworkPredictionOptions;->NETWORK_PREDICTION_WIFI_ONLY:Lcom/google/android/apps/chrome/preferences/NetworkPredictionOptions;

    sput-object v0, Lcom/google/android/apps/chrome/preferences/NetworkPredictionOptions;->DEFAULT:Lcom/google/android/apps/chrome/preferences/NetworkPredictionOptions;

    return-void

    :cond_0
    move v0, v2

    .line 15
    goto :goto_0
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 43
    return-void
.end method

.method public static choiceCount()I
    .locals 1

    .prologue
    .line 26
    invoke-static {}, Lcom/google/android/apps/chrome/preferences/NetworkPredictionOptions;->values()[Lcom/google/android/apps/chrome/preferences/NetworkPredictionOptions;

    move-result-object v0

    array-length v0, v0

    return v0
.end method

.method public static intToEnum(I)Lcom/google/android/apps/chrome/preferences/NetworkPredictionOptions;
    .locals 1

    .prologue
    .line 52
    invoke-static {}, Lcom/google/android/apps/chrome/preferences/NetworkPredictionOptions;->values()[Lcom/google/android/apps/chrome/preferences/NetworkPredictionOptions;

    move-result-object v0

    aget-object v0, v0, p0

    return-object v0
.end method

.method public static stringToEnum(Ljava/lang/String;)Lcom/google/android/apps/chrome/preferences/NetworkPredictionOptions;
    .locals 1

    .prologue
    .line 68
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p0, v0}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/preferences/NetworkPredictionOptions;->valueOf(Ljava/lang/String;)Lcom/google/android/apps/chrome/preferences/NetworkPredictionOptions;

    move-result-object v0

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/chrome/preferences/NetworkPredictionOptions;
    .locals 1

    .prologue
    .line 15
    const-class v0, Lcom/google/android/apps/chrome/preferences/NetworkPredictionOptions;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/preferences/NetworkPredictionOptions;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/chrome/preferences/NetworkPredictionOptions;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/google/android/apps/chrome/preferences/NetworkPredictionOptions;->$VALUES:[Lcom/google/android/apps/chrome/preferences/NetworkPredictionOptions;

    invoke-virtual {v0}, [Lcom/google/android/apps/chrome/preferences/NetworkPredictionOptions;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/chrome/preferences/NetworkPredictionOptions;

    return-object v0
.end method


# virtual methods
.method public final enumToInt()I
    .locals 1

    .prologue
    .line 60
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/NetworkPredictionOptions;->ordinal()I

    move-result v0

    return v0
.end method

.method public final enumToString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 76
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/NetworkPredictionOptions;->name()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getDisplayTitle()I
    .locals 2

    .prologue
    .line 34
    sget-object v0, Lcom/google/android/apps/chrome/preferences/NetworkPredictionOptions$1;->$SwitchMap$com$google$android$apps$chrome$preferences$NetworkPredictionOptions:[I

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/NetworkPredictionOptions;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 42
    sget-boolean v0, Lcom/google/android/apps/chrome/preferences/NetworkPredictionOptions;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 36
    :pswitch_0
    sget v0, Lcom/google/android/apps/chrome/R$string;->always_prefetch_bandwidth_entry:I

    .line 43
    :goto_0
    return v0

    .line 38
    :pswitch_1
    sget v0, Lcom/google/android/apps/chrome/R$string;->wifi_prefetch_bandwidth_entry:I

    goto :goto_0

    .line 40
    :pswitch_2
    sget v0, Lcom/google/android/apps/chrome/R$string;->never_prefetch_bandwidth_entry:I

    goto :goto_0

    .line 43
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 34
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
