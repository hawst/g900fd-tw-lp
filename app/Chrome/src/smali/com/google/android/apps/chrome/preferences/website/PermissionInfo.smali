.class public abstract Lcom/google/android/apps/chrome/preferences/website/PermissionInfo;
.super Ljava/lang/Object;
.source "PermissionInfo.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private final mEmbedder:Ljava/lang/String;

.field private final mOrigin:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object p1, p0, Lcom/google/android/apps/chrome/preferences/website/PermissionInfo;->mOrigin:Ljava/lang/String;

    .line 18
    iput-object p2, p0, Lcom/google/android/apps/chrome/preferences/website/PermissionInfo;->mEmbedder:Ljava/lang/String;

    .line 19
    return-void
.end method

.method private getEmbedderSafe()Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/PermissionInfo;->mEmbedder:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/PermissionInfo;->mEmbedder:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/PermissionInfo;->mOrigin:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public getAllowed()Ljava/lang/Boolean;
    .locals 2

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/PermissionInfo;->mOrigin:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/website/PermissionInfo;->getEmbedderSafe()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/chrome/preferences/website/PermissionInfo;->getNativePreferenceValue(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 40
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 36
    :pswitch_0
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    goto :goto_0

    .line 38
    :pswitch_1
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    goto :goto_0

    .line 34
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getEmbedder()Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/PermissionInfo;->mEmbedder:Ljava/lang/String;

    return-object v0
.end method

.method protected abstract getNativePreferenceValue(Ljava/lang/String;Ljava/lang/String;)I
.end method

.method public getOrigin()Ljava/lang/String;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/PermissionInfo;->mOrigin:Ljava/lang/String;

    return-object v0
.end method

.method public setAllowed(Ljava/lang/Boolean;)V
    .locals 3

    .prologue
    .line 45
    const/4 v0, -0x1

    .line 46
    if-eqz p1, :cond_0

    .line 47
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 49
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/website/PermissionInfo;->mOrigin:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/website/PermissionInfo;->getEmbedderSafe()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2, v0}, Lcom/google/android/apps/chrome/preferences/website/PermissionInfo;->setNativePreferenceValue(Ljava/lang/String;Ljava/lang/String;I)V

    .line 50
    return-void

    .line 47
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected abstract setNativePreferenceValue(Ljava/lang/String;Ljava/lang/String;I)V
.end method
