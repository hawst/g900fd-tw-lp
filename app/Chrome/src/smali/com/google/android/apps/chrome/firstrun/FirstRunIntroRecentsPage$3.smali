.class Lcom/google/android/apps/chrome/firstrun/FirstRunIntroRecentsPage$3;
.super Ljava/lang/Object;
.source "FirstRunIntroRecentsPage.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/firstrun/FirstRunIntroRecentsPage;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/firstrun/FirstRunIntroRecentsPage;)V
    .locals 0

    .prologue
    .line 109
    iput-object p1, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunIntroRecentsPage$3;->this$0:Lcom/google/android/apps/chrome/firstrun/FirstRunIntroRecentsPage;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    .line 112
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunIntroRecentsPage$3;->this$0:Lcom/google/android/apps/chrome/firstrun/FirstRunIntroRecentsPage;

    new-instance v1, Landroid/animation/AnimatorSet;

    invoke-direct {v1}, Landroid/animation/AnimatorSet;-><init>()V

    # setter for: Lcom/google/android/apps/chrome/firstrun/FirstRunIntroRecentsPage;->mLoupeAnimation:Landroid/animation/AnimatorSet;
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/firstrun/FirstRunIntroRecentsPage;->access$102(Lcom/google/android/apps/chrome/firstrun/FirstRunIntroRecentsPage;Landroid/animation/AnimatorSet;)Landroid/animation/AnimatorSet;

    .line 113
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunIntroRecentsPage$3;->this$0:Lcom/google/android/apps/chrome/firstrun/FirstRunIntroRecentsPage;

    # getter for: Lcom/google/android/apps/chrome/firstrun/FirstRunIntroRecentsPage;->mLoupeAnimation:Landroid/animation/AnimatorSet;
    invoke-static {v0}, Lcom/google/android/apps/chrome/firstrun/FirstRunIntroRecentsPage;->access$100(Lcom/google/android/apps/chrome/firstrun/FirstRunIntroRecentsPage;)Landroid/animation/AnimatorSet;

    move-result-object v0

    new-array v1, v6, [Landroid/animation/Animator;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunIntroRecentsPage$3;->this$0:Lcom/google/android/apps/chrome/firstrun/FirstRunIntroRecentsPage;

    # getter for: Lcom/google/android/apps/chrome/firstrun/FirstRunIntroRecentsPage;->mLoupeImageView:Landroid/widget/ImageView;
    invoke-static {v3}, Lcom/google/android/apps/chrome/firstrun/FirstRunIntroRecentsPage;->access$200(Lcom/google/android/apps/chrome/firstrun/FirstRunIntroRecentsPage;)Landroid/widget/ImageView;

    move-result-object v3

    sget-object v4, Landroid/view/View;->SCALE_X:Landroid/util/Property;

    new-array v5, v6, [F

    fill-array-data v5, :array_0

    invoke-static {v3, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunIntroRecentsPage$3;->this$0:Lcom/google/android/apps/chrome/firstrun/FirstRunIntroRecentsPage;

    # getter for: Lcom/google/android/apps/chrome/firstrun/FirstRunIntroRecentsPage;->mLoupeImageView:Landroid/widget/ImageView;
    invoke-static {v3}, Lcom/google/android/apps/chrome/firstrun/FirstRunIntroRecentsPage;->access$200(Lcom/google/android/apps/chrome/firstrun/FirstRunIntroRecentsPage;)Landroid/widget/ImageView;

    move-result-object v3

    sget-object v4, Landroid/view/View;->SCALE_Y:Landroid/util/Property;

    new-array v5, v6, [F

    fill-array-data v5, :array_1

    invoke-static {v3, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 118
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunIntroRecentsPage$3;->this$0:Lcom/google/android/apps/chrome/firstrun/FirstRunIntroRecentsPage;

    # getter for: Lcom/google/android/apps/chrome/firstrun/FirstRunIntroRecentsPage;->mLoupeAnimation:Landroid/animation/AnimatorSet;
    invoke-static {v0}, Lcom/google/android/apps/chrome/firstrun/FirstRunIntroRecentsPage;->access$100(Lcom/google/android/apps/chrome/firstrun/FirstRunIntroRecentsPage;)Landroid/animation/AnimatorSet;

    move-result-object v0

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v2, v3}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 119
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunIntroRecentsPage$3;->this$0:Lcom/google/android/apps/chrome/firstrun/FirstRunIntroRecentsPage;

    # getter for: Lcom/google/android/apps/chrome/firstrun/FirstRunIntroRecentsPage;->mLoupeAnimation:Landroid/animation/AnimatorSet;
    invoke-static {v0}, Lcom/google/android/apps/chrome/firstrun/FirstRunIntroRecentsPage;->access$100(Lcom/google/android/apps/chrome/firstrun/FirstRunIntroRecentsPage;)Landroid/animation/AnimatorSet;

    move-result-object v0

    new-instance v1, Landroid/view/animation/BounceInterpolator;

    invoke-direct {v1}, Landroid/view/animation/BounceInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 120
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunIntroRecentsPage$3;->this$0:Lcom/google/android/apps/chrome/firstrun/FirstRunIntroRecentsPage;

    # getter for: Lcom/google/android/apps/chrome/firstrun/FirstRunIntroRecentsPage;->mLoupeAnimation:Landroid/animation/AnimatorSet;
    invoke-static {v0}, Lcom/google/android/apps/chrome/firstrun/FirstRunIntroRecentsPage;->access$100(Lcom/google/android/apps/chrome/firstrun/FirstRunIntroRecentsPage;)Landroid/animation/AnimatorSet;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    .line 121
    return-void

    .line 113
    nop

    :array_0
    .array-data 4
        0x3f000000    # 0.5f
        0x3f800000    # 1.0f
    .end array-data

    :array_1
    .array-data 4
        0x3f000000    # 0.5f
        0x3f800000    # 1.0f
    .end array-data
.end method
