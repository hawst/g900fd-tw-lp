.class public Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarPhone;
.super Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;
.source "FindToolbarPhone.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 25
    return-void
.end method


# virtual methods
.method public activate(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarPhone;->isViewAvailable()Z

    move-result v0

    if-nez v0, :cond_0

    .line 32
    :goto_0
    return-void

    .line 30
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarPhone;->setVisibility(I)V

    .line 31
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->activate(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public deactivate()V
    .locals 1

    .prologue
    .line 36
    invoke-super {p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->deactivate()V

    .line 37
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarPhone;->setVisibility(I)V

    .line 38
    return-void
.end method

.method protected getStatusColor(ZZ)I
    .locals 2

    .prologue
    .line 61
    if-nez p1, :cond_0

    if-eqz p2, :cond_0

    .line 62
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarPhone;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$color;->find_in_page_results_status_white_color:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 66
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->getStatusColor(ZZ)I

    move-result v0

    goto :goto_0
.end method

.method protected updateVisualsForTabModel(Z)V
    .locals 3

    .prologue
    .line 43
    if-eqz p1, :cond_0

    .line 44
    sget v0, Lcom/google/android/apps/chrome/R$color;->incognito_primary_color:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarPhone;->setBackgroundResource(I)V

    .line 45
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarPhone;->mFindNextButton:Landroid/widget/ImageButton;

    sget v1, Lcom/google/android/apps/chrome/R$drawable;->btn_find_next_white:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 46
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarPhone;->mFindPrevButton:Landroid/widget/ImageButton;

    sget v1, Lcom/google/android/apps/chrome/R$drawable;->btn_find_prev_white:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 47
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarPhone;->mCloseFindButton:Landroid/widget/ImageButton;

    sget v1, Lcom/google/android/apps/chrome/R$drawable;->btn_toolbar_stop_loading_white:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 48
    sget v0, Lcom/google/android/apps/chrome/R$color;->find_in_page_query_white_color:I

    .line 56
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarPhone;->mFindQuery:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$FindQuery;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarPhone;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$FindQuery;->setTextColor(I)V

    .line 57
    return-void

    .line 50
    :cond_0
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarPhone;->setBackgroundColor(I)V

    .line 51
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarPhone;->mFindNextButton:Landroid/widget/ImageButton;

    sget v1, Lcom/google/android/apps/chrome/R$drawable;->btn_find_next:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 52
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarPhone;->mFindPrevButton:Landroid/widget/ImageButton;

    sget v1, Lcom/google/android/apps/chrome/R$drawable;->btn_find_prev:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 53
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarPhone;->mCloseFindButton:Landroid/widget/ImageButton;

    sget v1, Lcom/google/android/apps/chrome/R$drawable;->btn_toolbar_stop_loading:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 54
    sget v0, Lcom/google/android/apps/chrome/R$color;->find_in_page_query_color:I

    goto :goto_0
.end method
