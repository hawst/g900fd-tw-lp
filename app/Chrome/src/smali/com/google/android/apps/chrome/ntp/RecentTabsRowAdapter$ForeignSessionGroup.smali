.class Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$ForeignSessionGroup;
.super Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$Group;
.source "RecentTabsRowAdapter.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final mForeignSession:Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSession;

.field final synthetic this$0:Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 281
    const-class v0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$ForeignSessionGroup;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSession;)V
    .locals 0

    .prologue
    .line 284
    iput-object p1, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$ForeignSessionGroup;->this$0:Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$Group;-><init>(Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;)V

    .line 285
    iput-object p2, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$ForeignSessionGroup;->mForeignSession:Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSession;

    .line 286
    return-void
.end method

.method static synthetic access$600(Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$ForeignSessionGroup;)Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSession;
    .locals 1

    .prologue
    .line 281
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$ForeignSessionGroup;->mForeignSession:Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSession;

    return-object v0
.end method


# virtual methods
.method public configureChildView(ILcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$ViewHolder;)V
    .locals 3

    .prologue
    .line 321
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$ForeignSessionGroup;->getChild(I)Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSessionTab;

    move-result-object v1

    .line 322
    iget-object v2, p2, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$ViewHolder;->textView:Landroid/widget/TextView;

    iget-object v0, v1, Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSessionTab;->title:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, v1, Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSessionTab;->url:Ljava/lang/String;

    :goto_0
    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 324
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$ForeignSessionGroup;->this$0:Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;

    iget-object v1, v1, Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSessionTab;->url:Ljava/lang/String;

    # invokes: Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->loadSyncedFavicon(Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$ViewHolder;Ljava/lang/String;)V
    invoke-static {v0, p2, v1}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->access$500(Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$ViewHolder;Ljava/lang/String;)V

    .line 325
    return-void

    .line 322
    :cond_0
    iget-object v0, v1, Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSessionTab;->title:Ljava/lang/String;

    goto :goto_0
.end method

.method public configureGroupView(Lcom/google/android/apps/chrome/ntp/RecentTabsGroupView;Z)V
    .locals 1

    .prologue
    .line 329
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$ForeignSessionGroup;->mForeignSession:Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSession;

    invoke-virtual {p1, v0, p2}, Lcom/google/android/apps/chrome/ntp/RecentTabsGroupView;->configureForForeignSession(Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSession;Z)V

    .line 330
    return-void
.end method

.method public bridge synthetic getChild(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 281
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$ForeignSessionGroup;->getChild(I)Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSessionTab;

    move-result-object v0

    return-object v0
.end method

.method public getChild(I)Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSessionTab;
    .locals 3

    .prologue
    .line 309
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$ForeignSessionGroup;->mForeignSession:Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSession;

    iget-object v0, v0, Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSession;->windows:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSessionWindow;

    .line 310
    iget-object v2, v0, Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSessionWindow;->tabs:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge p1, v2, :cond_0

    .line 311
    iget-object v0, v0, Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSessionWindow;->tabs:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSessionTab;

    .line 316
    :goto_1
    return-object v0

    .line 313
    :cond_0
    iget-object v0, v0, Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSessionWindow;->tabs:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    sub-int/2addr p1, v0

    .line 314
    goto :goto_0

    .line 315
    :cond_1
    sget-boolean v0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$ForeignSessionGroup;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 316
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public getChildType()Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$ChildType;
    .locals 1

    .prologue
    .line 304
    sget-object v0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$ChildType;->DEFAULT_CONTENT:Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$ChildType;

    return-object v0
.end method

.method public getChildrenCount()I
    .locals 3

    .prologue
    .line 295
    const/4 v0, 0x0

    .line 296
    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$ForeignSessionGroup;->mForeignSession:Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSession;

    iget-object v1, v1, Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSession;->windows:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSessionWindow;

    .line 297
    iget-object v0, v0, Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSessionWindow;->tabs:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    .line 298
    goto :goto_0

    .line 299
    :cond_0
    return v1
.end method

.method public getGroupType()Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$GroupType;
    .locals 1

    .prologue
    .line 290
    sget-object v0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$GroupType;->CONTENT:Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$GroupType;

    return-object v0
.end method

.method public isCollapsed()Z
    .locals 2

    .prologue
    .line 339
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$ForeignSessionGroup;->this$0:Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;

    # getter for: Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->mRecentTabsManager:Lcom/google/android/apps/chrome/ntp/RecentTabsManager;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->access$300(Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;)Lcom/google/android/apps/chrome/ntp/RecentTabsManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$ForeignSessionGroup;->mForeignSession:Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSession;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->getForeignSessionCollapsed(Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSession;)Z

    move-result v0

    return v0
.end method

.method public onChildClick(I)Z
    .locals 4

    .prologue
    .line 344
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$ForeignSessionGroup;->getChild(I)Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSessionTab;

    move-result-object v0

    .line 345
    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$ForeignSessionGroup;->this$0:Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;

    # getter for: Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->mRecentTabsManager:Lcom/google/android/apps/chrome/ntp/RecentTabsManager;
    invoke-static {v1}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->access$300(Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;)Lcom/google/android/apps/chrome/ntp/RecentTabsManager;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$ForeignSessionGroup;->mForeignSession:Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSession;

    const/4 v3, 0x2

    invoke-virtual {v1, v2, v0, v3}, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->openForeignSessionTab(Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSession;Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSessionTab;I)V

    .line 347
    const/4 v0, 0x1

    return v0
.end method

.method public onCreateContextMenuForChild(ILandroid/view/ContextMenu;Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 366
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$ForeignSessionGroup;->getChild(I)Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSessionTab;

    move-result-object v0

    .line 367
    new-instance v1, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$ForeignSessionGroup$2;

    invoke-direct {v1, p0, v0}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$ForeignSessionGroup$2;-><init>(Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$ForeignSessionGroup;Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSessionTab;)V

    .line 375
    sget v0, Lcom/google/android/apps/chrome/R$string;->contextmenu_open_in_new_tab:I

    invoke-interface {p2, v0}, Landroid/view/ContextMenu;->add(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 376
    return-void
.end method

.method public onCreateContextMenuForGroup(Landroid/view/ContextMenu;Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 352
    new-instance v0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$ForeignSessionGroup$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$ForeignSessionGroup$1;-><init>(Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$ForeignSessionGroup;)V

    .line 359
    sget v1, Lcom/google/android/apps/chrome/R$string;->ntp_recent_tabs_remove_menu_option:I

    invoke-interface {p1, v1}, Landroid/view/ContextMenu;->add(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 361
    return-void
.end method

.method public setCollapsed(Z)V
    .locals 2

    .prologue
    .line 334
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$ForeignSessionGroup;->this$0:Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;

    # getter for: Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->mRecentTabsManager:Lcom/google/android/apps/chrome/ntp/RecentTabsManager;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->access$300(Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;)Lcom/google/android/apps/chrome/ntp/RecentTabsManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$ForeignSessionGroup;->mForeignSession:Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSession;

    invoke-virtual {v0, v1, p1}, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->setForeignSessionCollapsed(Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSession;Z)V

    .line 335
    return-void
.end method
