.class public Lcom/google/android/apps/chrome/widget/SmoothProgressBar;
.super Landroid/widget/ProgressBar;
.source "SmoothProgressBar.java"


# instance fields
.field private mIsAnimated:Z

.field private mResolutionMutiplier:I

.field private mTargetProgress:I

.field private mUpdateProgressRunnable:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 56
    invoke-direct {p0, p1, p2}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 26
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/widget/SmoothProgressBar;->mIsAnimated:Z

    .line 32
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/apps/chrome/widget/SmoothProgressBar;->mResolutionMutiplier:I

    .line 34
    new-instance v0, Lcom/google/android/apps/chrome/widget/SmoothProgressBar$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/widget/SmoothProgressBar$1;-><init>(Lcom/google/android/apps/chrome/widget/SmoothProgressBar;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/SmoothProgressBar;->mUpdateProgressRunnable:Ljava/lang/Runnable;

    .line 57
    iget v0, p0, Lcom/google/android/apps/chrome/widget/SmoothProgressBar;->mResolutionMutiplier:I

    mul-int/lit8 v0, v0, 0x64

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/SmoothProgressBar;->setMax(I)V

    .line 58
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/widget/SmoothProgressBar;)I
    .locals 1

    .prologue
    .line 20
    iget v0, p0, Lcom/google/android/apps/chrome/widget/SmoothProgressBar;->mTargetProgress:I

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/widget/SmoothProgressBar;)Z
    .locals 1

    .prologue
    .line 20
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widget/SmoothProgressBar;->mIsAnimated:Z

    return v0
.end method


# virtual methods
.method protected onSizeChanged(IIII)V
    .locals 3

    .prologue
    .line 62
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/ProgressBar;->onSizeChanged(IIII)V

    .line 64
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/SmoothProgressBar;->getProgress()I

    move-result v0

    iget v1, p0, Lcom/google/android/apps/chrome/widget/SmoothProgressBar;->mResolutionMutiplier:I

    div-int/2addr v0, v1

    .line 67
    const/4 v1, 0x1

    add-int/lit8 v2, p1, 0x64

    add-int/lit8 v2, v2, -0x1

    div-int/lit8 v2, v2, 0x64

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/chrome/widget/SmoothProgressBar;->mResolutionMutiplier:I

    .line 68
    iget v1, p0, Lcom/google/android/apps/chrome/widget/SmoothProgressBar;->mResolutionMutiplier:I

    mul-int/lit8 v1, v1, 0x64

    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/widget/SmoothProgressBar;->setMax(I)V

    .line 69
    iget v1, p0, Lcom/google/android/apps/chrome/widget/SmoothProgressBar;->mResolutionMutiplier:I

    mul-int/2addr v0, v1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/SmoothProgressBar;->setProgressInternal(I)V

    .line 70
    return-void
.end method

.method public declared-synchronized setProgress(I)V
    .locals 2

    .prologue
    .line 74
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/android/apps/chrome/widget/SmoothProgressBar;->mResolutionMutiplier:I

    mul-int/2addr v0, p1

    .line 75
    iget v1, p0, Lcom/google/android/apps/chrome/widget/SmoothProgressBar;->mTargetProgress:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v1, v0, :cond_0

    .line 79
    :goto_0
    monitor-exit p0

    return-void

    .line 76
    :cond_0
    :try_start_1
    iput v0, p0, Lcom/google/android/apps/chrome/widget/SmoothProgressBar;->mTargetProgress:I

    .line 77
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/SmoothProgressBar;->mUpdateProgressRunnable:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/SmoothProgressBar;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 78
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/SmoothProgressBar;->mUpdateProgressRunnable:Ljava/lang/Runnable;

    invoke-static {p0, v0}, Lorg/chromium/base/ApiCompatibilityUtils;->postOnAnimation(Landroid/view/View;Ljava/lang/Runnable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 74
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected setProgressInternal(I)V
    .locals 0

    .prologue
    .line 90
    invoke-super {p0, p1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 91
    return-void
.end method
