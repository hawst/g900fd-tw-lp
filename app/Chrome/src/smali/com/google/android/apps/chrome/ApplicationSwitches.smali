.class public abstract Lcom/google/android/apps/chrome/ApplicationSwitches;
.super Ljava/lang/Object;
.source "ApplicationSwitches.java"


# static fields
.field public static final ALWAYS_SHOW_NEW_PROFILE_MANAGEMENT_UPGRADE_SCREEN:Ljava/lang/String; = "always-show-new-profile-management-upgrade-screen"

.field public static final APPROXIMATION_SCALE:Ljava/lang/String; = "approximation-scale"

.field public static final APPROXIMATION_THUMBNAILS:Ljava/lang/String; = "approximation-thumbnails"

.field public static final CHILD_ACCOUNT:Ljava/lang/String; = "child-account"

.field public static final COMPROMISE_SIZE_THUMBNAILS:Ljava/lang/String; = "compromise-size-thumbnails"

.field public static final CONTEXTUAL_SEARCH_SCRIPT_URL:Ljava/lang/String; = "contextual-search-script-url"

.field public static final DEBUG_TOOLBAR_TEXTURE:Ljava/lang/String; = "debug-toolbar-texture"

.field public static final DEFAULT_COUNTRY_CODE_AT_INSTALL:Ljava/lang/String; = "default-country-code"

.field public static final DISABLE_CAST:Ljava/lang/String; = "disable-cast"

.field public static final DISABLE_CAST_RECONNECTION:Ljava/lang/String; = "disable-cast-reconnection"

.field public static final DISABLE_CHROME_TO_MOBILE_OAUTH2:Ljava/lang/String; = "disable-chrome-to-mobile-oauth2"

.field public static final DISABLE_COMPOSITOR_TAB_STRIP:Ljava/lang/String; = "disable-compositor-tab-strip"

.field public static final DISABLE_CONTEXTUAL_SEARCH:Ljava/lang/String; = "disable-contextual-search"

.field public static final DISABLE_EXTERNAL_INTENT_REQUESTS:Ljava/lang/String; = "disable-external-intent-requests"

.field public static final DISABLE_FIRST_RUN_EXPERIENCE:Ljava/lang/String; = "disable-fre"

.field public static final DISABLE_FULLSCREEN:Ljava/lang/String; = "disable-fullscreen"

.field public static final DISABLE_INSTANT:Ljava/lang/String; = "disable-instant"

.field public static final DISABLE_PERSISTENT_FULLSCREEN:Ljava/lang/String; = "disable-persistent-fullscreen"

.field public static final ENABLE_ACCESSIBILITY_TAB_SWITCHER:Ljava/lang/String; = "enable-accessibility-tab-switcher"

.field public static final ENABLE_BEGIN_FRAME_SCHEDULING:Ljava/lang/String; = "enable-begin-frame-scheduling"

.field public static final ENABLE_CAST_DEBUG_LOGS:Ljava/lang/String; = "enable-cast-debug"

.field public static final ENABLE_CONTEXTUAL_SEARCH:Ljava/lang/String; = "enable-contextual-search"

.field public static final ENABLE_CONTEXTUAL_SEARCH_FOR_TESTING:Ljava/lang/String; = "enable-contextual-search-for-testing"

.field public static final ENABLE_DOM_DISTILLER:Ljava/lang/String; = "enable-dom-distiller"

.field public static final ENABLE_E200_SUGGESTIONS:Ljava/lang/String; = "enable-e200-suggestions"

.field public static final ENABLE_HIGH_END_UI_UNDO:Ljava/lang/String; = "enable-high-end-ui-undo"

.field public static final ENABLE_INSTANT_SEARCH_CLICKS:Ljava/lang/String; = "enable-instant-search-clicks"

.field public static final ENABLE_READER_MODE_TOOLBAR_ICON:Ljava/lang/String; = "enable-reader-mode-toolbar-icon"

.field public static final ENABLE_TABLET_TAB_STACK:Ljava/lang/String; = "enable-tablet-tab-stack"

.field public static final FORCE_CRASH_DUMP_UPLOAD:Ljava/lang/String; = "force-dump-upload"

.field public static final FULL_SIZE_THUMBNAILS:Ljava/lang/String; = "full-size-thumbnails"

.field public static final HARDWARE_ACCELERATION:Ljava/lang/String; = "hardware-acceleration"

.field public static final MANUAL_ENHANCED_BOOKMARKS:Ljava/lang/String; = "manual-enhanced-bookmarks"

.field public static final NOTIFICATION_CENTER_LOGGING:Ljava/lang/String; = "notification-center-logging"

.field public static final NO_RESTORE_STATE:Ljava/lang/String; = "no-restore-state"

.field public static final RENDER_PROCESS_LIMIT:Ljava/lang/String; = "renderer-process-limit"

.field public static final SIXTEEN_BIT_THUMBNAILS:Ljava/lang/String; = "sixteen-bit-thumbnails"

.field public static final SMALL_SIZE_THUMBNAILS:Ljava/lang/String; = "small-size-thumbnails"

.field public static final STRICT_MODE:Ljava/lang/String; = "strict-mode"

.field public static final TAKE_SURFACE:Ljava/lang/String; = "take-surface"

.field public static final THIRTYTWO_BIT_THUMBNAILS:Ljava/lang/String; = "thirtytwo-bit-thumbnails"

.field public static final THUMBNAILS:Ljava/lang/String; = "thumbnails"

.field public static final USE_SANDBOX_WALLET_ENVIRONMENT:Ljava/lang/String; = "wallet-service-use-sandbox"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 156
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
