.class public Lcom/google/android/apps/chrome/document/HomeToolbarControls;
.super Ljava/lang/Object;
.source "HomeToolbarControls.java"

# interfaces
.implements Lcom/google/android/apps/chrome/document/DocumentToolbarHelper$ToolbarControls;


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final mTitleBar:Lcom/google/android/apps/chrome/document/TitleBar;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    const-class v0, Lcom/google/android/apps/chrome/document/HomeToolbarControls;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/document/HomeToolbarControls;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/view/View;Lcom/google/android/apps/chrome/document/TitleBar$DocumentDelegate;Lorg/chromium/chrome/browser/appmenu/AppMenuHandler;Lcom/google/android/apps/chrome/document/TitleBar$TitleBarListener;)V
    .locals 2

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    sget v0, Lcom/google/android/apps/chrome/R$id;->titlebar:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/document/TitleBar;

    iput-object v0, p0, Lcom/google/android/apps/chrome/document/HomeToolbarControls;->mTitleBar:Lcom/google/android/apps/chrome/document/TitleBar;

    .line 24
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/HomeToolbarControls;->mTitleBar:Lcom/google/android/apps/chrome/document/TitleBar;

    const-string/jumbo v1, ""

    invoke-virtual {v0, p2, p3, v1}, Lcom/google/android/apps/chrome/document/TitleBar;->initialize(Lcom/google/android/apps/chrome/document/TitleBar$DocumentDelegate;Lorg/chromium/chrome/browser/appmenu/AppMenuHandler;Ljava/lang/String;)V

    .line 28
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/HomeToolbarControls;->mTitleBar:Lcom/google/android/apps/chrome/document/TitleBar;

    invoke-virtual {v0, p4}, Lcom/google/android/apps/chrome/document/TitleBar;->addListener(Lcom/google/android/apps/chrome/document/TitleBar$TitleBarListener;)V

    .line 29
    return-void
.end method


# virtual methods
.method public destroy()V
    .locals 0

    .prologue
    .line 48
    return-void
.end method

.method public focusUrlBar()V
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/HomeToolbarControls;->mTitleBar:Lcom/google/android/apps/chrome/document/TitleBar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/document/TitleBar;->animateTransitionToSearch()V

    .line 39
    return-void
.end method

.method public getMenuAnchor()Landroid/view/View;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/HomeToolbarControls;->mTitleBar:Lcom/google/android/apps/chrome/document/TitleBar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/document/TitleBar;->getMenuAnchor()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/HomeToolbarControls;->mTitleBar:Lcom/google/android/apps/chrome/document/TitleBar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/document/TitleBar;->animateTransitionFromSearch()V

    .line 53
    return-void
.end method

.method public setThemeColor(I)V
    .locals 1

    .prologue
    .line 43
    sget-boolean v0, Lcom/google/android/apps/chrome/document/HomeToolbarControls;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 44
    :cond_0
    return-void
.end method
