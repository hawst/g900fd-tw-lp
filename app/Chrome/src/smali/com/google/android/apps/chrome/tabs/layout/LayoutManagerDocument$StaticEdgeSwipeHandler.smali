.class Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument$StaticEdgeSwipeHandler;
.super Lcom/google/android/apps/chrome/eventfilter/EmptyEdgeSwipeHandler;
.source "LayoutManagerDocument.java"


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;)V
    .locals 0

    .prologue
    .line 264
    iput-object p1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument$StaticEdgeSwipeHandler;->this$0:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/eventfilter/EmptyEdgeSwipeHandler;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument$1;)V
    .locals 0

    .prologue
    .line 264
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument$StaticEdgeSwipeHandler;-><init>(Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;)V

    return-void
.end method


# virtual methods
.method public isSwipeEnabled(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;)Z
    .locals 2

    .prologue
    .line 278
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument$StaticEdgeSwipeHandler;->this$0:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;

    iget-object v0, v0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;->mHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;->getFullscreenManager()Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;

    move-result-object v0

    .line 279
    sget-object v1, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;->DOWN:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    if-ne p1, v1, :cond_0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->getPersistentFullscreenMode()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public swipeStarted(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;FF)V
    .locals 2

    .prologue
    .line 267
    sget-object v0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;->DOWN:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    if-eq p1, v0, :cond_1

    .line 274
    :cond_0
    :goto_0
    return-void

    .line 268
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument$StaticEdgeSwipeHandler;->this$0:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;

    iget-object v0, v0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;->mHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;->getFullscreenManager()Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;

    move-result-object v0

    .line 269
    if-eqz v0, :cond_0

    .line 271
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->isOverlayVideoMode()Z

    move-result v1

    if-nez v1, :cond_0

    .line 272
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->setPersistentFullscreenMode(Z)V

    goto :goto_0
.end method
