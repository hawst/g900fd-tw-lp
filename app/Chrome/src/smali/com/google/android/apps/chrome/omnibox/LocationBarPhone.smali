.class public Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;
.super Lcom/google/android/apps/chrome/omnibox/LocationBar;
.source "LocationBarPhone.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private mFirstVisibleFocusedView:Landroid/view/View;

.field private mIncognitoBadge:Landroid/view/View;

.field private mIncognitoBadgePadding:I

.field private mKeyboardResizeModeTask:Ljava/lang/Runnable;

.field private mOmniboxBackgroundAnimator:Landroid/animation/ObjectAnimator;

.field private mUrlActionsContainer:Landroid/view/View;

.field private mUrlFocusChangeInProgress:Z

.field private mVoiceSearchEnabled:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-class v0, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/omnibox/LocationBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 48
    return-void
.end method

.method static synthetic access$002(Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;Ljava/lang/Runnable;)Ljava/lang/Runnable;
    .locals 0

    .prologue
    .line 28
    iput-object p1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->mKeyboardResizeModeTask:Ljava/lang/Runnable;

    return-object p1
.end method

.method private updateIncognitoBadgePadding()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 225
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->mIncognitoBadge:Landroid/view/View;

    if-nez v0, :cond_0

    .line 233
    :goto_0
    return-void

    .line 227
    :cond_0
    sget v0, Lcom/google/android/apps/chrome/R$id;->location_bar_icon:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_1

    .line 228
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->mIncognitoBadge:Landroid/view/View;

    iget v1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->mIncognitoBadgePadding:I

    invoke-static {v0, v2, v2, v1, v2}, Lorg/chromium/base/ApiCompatibilityUtils;->setPaddingRelative(Landroid/view/View;IIII)V

    goto :goto_0

    .line 231
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->mIncognitoBadge:Landroid/view/View;

    invoke-static {v0, v2, v2, v2, v2}, Lorg/chromium/base/ApiCompatibilityUtils;->setPaddingRelative(Landroid/view/View;IIII)V

    goto :goto_0
.end method


# virtual methods
.method protected drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 105
    .line 106
    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->mUrlBar:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    if-ne p2, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->mUrlActionsContainer:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    .line 107
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 112
    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->mUrlBar:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->getLeft()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->mUrlActionsContainer:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    move-result v2

    if-ge v1, v2, :cond_2

    .line 113
    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->mUrlActionsContainer:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getX()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->getBottom()I

    move-result v2

    invoke-virtual {p1, v0, v0, v1, v2}, Landroid/graphics/Canvas;->clipRect(IIII)Z

    .line 118
    :goto_0
    const/4 v0, 0x1

    .line 120
    :cond_0
    invoke-super {p0, p1, p2, p3, p4}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    move-result v1

    .line 121
    if-eqz v0, :cond_1

    .line 122
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 124
    :cond_1
    return v1

    .line 115
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->mUrlActionsContainer:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getX()F

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->mUrlActionsContainer:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v1

    int-to-float v1, v1

    add-float/2addr v0, v1

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->getWidth()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->getBottom()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->clipRect(FFFF)Z

    goto :goto_0
.end method

.method public finishUrlFocusChange(Z)V
    .locals 5

    .prologue
    const/16 v2, 0x20

    const/4 v4, 0x0

    .line 134
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->getWindowDelegate()Lcom/google/android/apps/chrome/WindowDelegate;

    move-result-object v0

    .line 135
    if-nez p1, :cond_3

    .line 150
    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->mUrlBar:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-static {v1, v4}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    .line 154
    new-instance v1, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone$1;-><init>(Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;)V

    const-wide/16 v2, 0x96

    invoke-virtual {p0, v1, v2, v3}, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 162
    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->mKeyboardResizeModeTask:Ljava/lang/Runnable;

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/WindowDelegate;->getWindowSoftInputMode()I

    move-result v1

    const/16 v2, 0x10

    if-eq v1, v2, :cond_0

    .line 165
    new-instance v1, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone$2;

    invoke-direct {v1, p0, v0}, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone$2;-><init>(Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;Lcom/google/android/apps/chrome/WindowDelegate;)V

    iput-object v1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->mKeyboardResizeModeTask:Ljava/lang/Runnable;

    .line 173
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->mKeyboardResizeModeTask:Ljava/lang/Runnable;

    const-wide/16 v2, 0x12c

    invoke-virtual {p0, v0, v2, v3}, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 175
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->mUrlActionsContainer:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 193
    :cond_1
    :goto_0
    iput-boolean v4, p0, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->mUrlFocusChangeInProgress:Z

    .line 195
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->getToolbarDataProvider()Lcom/google/android/apps/chrome/toolbar/ToolbarDataProvider;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarDataProvider;->getNewTabPageForCurrentTab()Lcom/google/android/apps/chrome/ntp/NewTabPage;

    move-result-object v0

    .line 196
    if-eqz p1, :cond_2

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ntp/NewTabPage;->isLocationBarShownInNTP()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 197
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->fadeInOmniboxResultsContainerBackground()V

    .line 199
    :cond_2
    return-void

    .line 177
    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->mKeyboardResizeModeTask:Ljava/lang/Runnable;

    if-eqz v1, :cond_4

    .line 178
    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->mKeyboardResizeModeTask:Ljava/lang/Runnable;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 179
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->mKeyboardResizeModeTask:Ljava/lang/Runnable;

    .line 181
    :cond_4
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/WindowDelegate;->getWindowSoftInputMode()I

    move-result v1

    if-eq v1, v2, :cond_5

    .line 183
    invoke-virtual {v0, v2}, Lcom/google/android/apps/chrome/WindowDelegate;->setWindowSoftInputMode(I)V

    .line 186
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->mUrlBar:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->openKeyboard()V

    .line 189
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->getSuggestionList()Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->getSuggestionList()Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;->isShown()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 190
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->getSuggestionList()Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;->invalidateSuggestionViews()V

    goto :goto_0
.end method

.method public getContentRect(Landroid/graphics/Rect;)V
    .locals 2

    .prologue
    .line 69
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->getContentRect(Landroid/graphics/Rect;)V

    .line 70
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->mIncognitoBadge:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    .line 77
    :goto_0
    return-void

    .line 72
    :cond_0
    invoke-static {p0}, Lorg/chromium/base/ApiCompatibilityUtils;->isLayoutRtl(Landroid/view/View;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 73
    iget v0, p1, Landroid/graphics/Rect;->left:I

    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->mIncognitoBadge:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p1, Landroid/graphics/Rect;->left:I

    goto :goto_0

    .line 75
    :cond_1
    iget v0, p1, Landroid/graphics/Rect;->right:I

    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->mIncognitoBadge:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v1

    sub-int/2addr v0, v1

    iput v0, p1, Landroid/graphics/Rect;->right:I

    goto :goto_0
.end method

.method public getFirstViewVisibleWhenFocused()Landroid/view/View;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->mFirstVisibleFocusedView:Landroid/view/View;

    return-object v0
.end method

.method protected onFinishInflate()V
    .locals 3

    .prologue
    .line 52
    invoke-super {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->onFinishInflate()V

    .line 54
    sget v0, Lcom/google/android/apps/chrome/R$id;->corpus_chip_text_view:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->mFirstVisibleFocusedView:Landroid/view/View;

    .line 55
    sget v0, Lcom/google/android/apps/chrome/R$id;->incognito_badge:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->mIncognitoBadge:Landroid/view/View;

    .line 56
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$dimen;->location_bar_incognito_badge_padding:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->mIncognitoBadgePadding:I

    .line 58
    sget v0, Lcom/google/android/apps/chrome/R$id;->url_action_container:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->mUrlActionsContainer:Landroid/view/View;

    .line 59
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 60
    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->mUrlActionsContainer:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    .line 61
    iget v1, v0, Landroid/graphics/Rect;->left:I

    add-int/lit8 v1, v1, -0xf

    iput v1, v0, Landroid/graphics/Rect;->left:I

    .line 62
    new-instance v1, Landroid/view/TouchDelegate;

    iget-object v2, p0, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->mUrlActionsContainer:Landroid/view/View;

    invoke-direct {v1, v0, v2}, Landroid/view/TouchDelegate;-><init>(Landroid/graphics/Rect;Landroid/view/View;)V

    .line 63
    sget-boolean v0, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->mUrlActionsContainer:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eq v0, p0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 64
    :cond_0
    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->setTouchDelegate(Landroid/view/TouchDelegate;)V

    .line 65
    return-void
.end method

.method public onUrlFocusChange(Z)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 88
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->mOmniboxBackgroundAnimator:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->mOmniboxBackgroundAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 89
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->mOmniboxBackgroundAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 90
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->mOmniboxBackgroundAnimator:Landroid/animation/ObjectAnimator;

    .line 92
    :cond_0
    if-eqz p1, :cond_1

    .line 95
    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->setFocusable(Z)V

    .line 96
    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->setFocusableInTouchMode(Z)V

    .line 98
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->mUrlFocusChangeInProgress:Z

    .line 99
    if-eqz p1, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->mUrlActionsContainer:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 100
    :cond_2
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->onUrlFocusChange(Z)V

    .line 101
    return-void
.end method

.method public setLayoutDirection(I)V
    .locals 0

    .prologue
    .line 252
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->setLayoutDirection(I)V

    .line 253
    invoke-direct {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->updateIncognitoBadgePadding()V

    .line 254
    return-void
.end method

.method protected shouldAnimateIconChanges()Z
    .locals 1

    .prologue
    .line 247
    invoke-super {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->shouldAnimateIconChanges()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->mUrlFocusChangeInProgress:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected updateDeleteButton(Z)V
    .locals 4

    .prologue
    const/4 v2, 0x4

    const/4 v1, 0x0

    .line 203
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->mDeleteButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 204
    iget-object v3, p0, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->mDeleteButton:Landroid/widget/ImageButton;

    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 205
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->mMicButton:Landroid/widget/ImageButton;

    iget-boolean v3, p0, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->mVoiceSearchEnabled:Z

    if-eqz v3, :cond_1

    if-nez p1, :cond_1

    iget-object v3, p0, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->mUrlBar:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->hasFocus()Z

    move-result v3

    if-eqz v3, :cond_1

    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 207
    return-void

    :cond_0
    move v0, v2

    .line 204
    goto :goto_0

    :cond_1
    move v1, v2

    .line 205
    goto :goto_1
.end method

.method protected updateLocationBarIconContainerVisibility()V
    .locals 0

    .prologue
    .line 218
    invoke-super {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->updateLocationBarIconContainerVisibility()V

    .line 219
    invoke-direct {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->updateIncognitoBadgePadding()V

    .line 220
    return-void
.end method

.method public updateMicButtonState()V
    .locals 2

    .prologue
    .line 211
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->isVoiceSearchEnabled()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->mVoiceSearchEnabled:Z

    .line 212
    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->mMicButton:Landroid/widget/ImageButton;

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->mVoiceSearchEnabled:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->mUrlBar:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->mDeleteButton:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 214
    return-void

    .line 212
    :cond_0
    const/4 v0, 0x4

    goto :goto_0
.end method

.method public updateVisualsForState()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 237
    invoke-super {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->updateVisualsForState()V

    .line 239
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->getCurrentTab()Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    .line 240
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/Tab;->isIncognito()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 241
    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->mIncognitoBadge:Landroid/view/View;

    if-eqz v0, :cond_1

    :goto_1
    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 242
    invoke-direct {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->updateIncognitoBadgePadding()V

    .line 243
    return-void

    :cond_0
    move v0, v1

    .line 240
    goto :goto_0

    .line 241
    :cond_1
    const/16 v1, 0x8

    goto :goto_1
.end method
