.class Lcom/google/android/apps/chrome/enhancedbookmark/ChromeEnhancedBookmarkBridge;
.super Ljava/lang/Object;
.source "ChromeEnhancedBookmarkBridge.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private mNativeChromeEnhancedBookmarkBridge:J

.field private mSalientImageCache:Landroid/util/LruCache;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-class v0, Lcom/google/android/apps/chrome/enhancedbookmark/ChromeEnhancedBookmarkBridge;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/enhancedbookmark/ChromeEnhancedBookmarkBridge;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lorg/chromium/chrome/browser/profiles/Profile;I)V
    .locals 2

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/enhancedbookmark/ChromeEnhancedBookmarkBridge;->nativeInit(Lorg/chromium/chrome/browser/profiles/Profile;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/ChromeEnhancedBookmarkBridge;->mNativeChromeEnhancedBookmarkBridge:J

    .line 44
    new-instance v0, Lcom/google/android/apps/chrome/enhancedbookmark/ChromeEnhancedBookmarkBridge$1;

    invoke-direct {v0, p0, p2}, Lcom/google/android/apps/chrome/enhancedbookmark/ChromeEnhancedBookmarkBridge$1;-><init>(Lcom/google/android/apps/chrome/enhancedbookmark/ChromeEnhancedBookmarkBridge;I)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/ChromeEnhancedBookmarkBridge;->mSalientImageCache:Landroid/util/LruCache;

    .line 50
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/enhancedbookmark/ChromeEnhancedBookmarkBridge;)Landroid/util/LruCache;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/ChromeEnhancedBookmarkBridge;->mSalientImageCache:Landroid/util/LruCache;

    return-object v0
.end method

.method private native nativeDestroy(J)V
.end method

.method private native nativeInit(Lorg/chromium/chrome/browser/profiles/Profile;)J
.end method

.method private static native nativeSalientImageForUrl(JLjava/lang/String;Lcom/google/android/apps/chrome/enhancedbookmark/ChromeEnhancedBookmarkBridge$SalientImageCallback;)V
.end method


# virtual methods
.method destroy()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 53
    sget-boolean v0, Lcom/google/android/apps/chrome/enhancedbookmark/ChromeEnhancedBookmarkBridge;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/ChromeEnhancedBookmarkBridge;->mNativeChromeEnhancedBookmarkBridge:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 54
    :cond_0
    iget-wide v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/ChromeEnhancedBookmarkBridge;->mNativeChromeEnhancedBookmarkBridge:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/enhancedbookmark/ChromeEnhancedBookmarkBridge;->nativeDestroy(J)V

    .line 55
    iput-wide v2, p0, Lcom/google/android/apps/chrome/enhancedbookmark/ChromeEnhancedBookmarkBridge;->mNativeChromeEnhancedBookmarkBridge:J

    .line 57
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/ChromeEnhancedBookmarkBridge;->mSalientImageCache:Landroid/util/LruCache;

    if-eqz v0, :cond_2

    .line 59
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/ChromeEnhancedBookmarkBridge;->mSalientImageCache:Landroid/util/LruCache;

    invoke-virtual {v0}, Landroid/util/LruCache;->snapshot()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 60
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_0

    .line 62
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/ChromeEnhancedBookmarkBridge;->mSalientImageCache:Landroid/util/LruCache;

    invoke-virtual {v0}, Landroid/util/LruCache;->evictAll()V

    .line 64
    :cond_2
    return-void
.end method

.method salientImageForUrl(Ljava/lang/String;Lcom/google/android/apps/chrome/enhancedbookmark/ChromeEnhancedBookmarkBridge$SalientImageCallback;)V
    .locals 2

    .prologue
    .line 71
    sget-boolean v0, Lcom/google/android/apps/chrome/enhancedbookmark/ChromeEnhancedBookmarkBridge;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 74
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/ChromeEnhancedBookmarkBridge;->mSalientImageCache:Landroid/util/LruCache;

    if-eqz v0, :cond_2

    .line 75
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/ChromeEnhancedBookmarkBridge;->mSalientImageCache:Landroid/util/LruCache;

    invoke-virtual {v0, p1}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .line 76
    if-eqz v0, :cond_1

    .line 77
    iget-object v1, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Landroid/graphics/Bitmap;

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-interface {p2, v1, v0}, Lcom/google/android/apps/chrome/enhancedbookmark/ChromeEnhancedBookmarkBridge$SalientImageCallback;->onSalientImageReady(Landroid/graphics/Bitmap;Ljava/lang/String;)V

    .line 93
    :goto_0
    return-void

    .line 81
    :cond_1
    new-instance v0, Lcom/google/android/apps/chrome/enhancedbookmark/ChromeEnhancedBookmarkBridge$2;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/apps/chrome/enhancedbookmark/ChromeEnhancedBookmarkBridge$2;-><init>(Lcom/google/android/apps/chrome/enhancedbookmark/ChromeEnhancedBookmarkBridge;Ljava/lang/String;Lcom/google/android/apps/chrome/enhancedbookmark/ChromeEnhancedBookmarkBridge$SalientImageCallback;)V

    move-object p2, v0

    .line 92
    :cond_2
    iget-wide v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/ChromeEnhancedBookmarkBridge;->mNativeChromeEnhancedBookmarkBridge:J

    invoke-static {v0, v1, p1, p2}, Lcom/google/android/apps/chrome/enhancedbookmark/ChromeEnhancedBookmarkBridge;->nativeSalientImageForUrl(JLjava/lang/String;Lcom/google/android/apps/chrome/enhancedbookmark/ChromeEnhancedBookmarkBridge$SalientImageCallback;)V

    goto :goto_0
.end method
