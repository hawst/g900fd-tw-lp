.class public Lcom/google/android/apps/chrome/snapshot/DownloadInfo;
.super Ljava/lang/Object;
.source "DownloadInfo.java"


# instance fields
.field private final mDownloadId:J

.field private final mMediaType:Ljava/lang/String;

.field private final mReason:I

.field private final mStatus:I


# direct methods
.method public constructor <init>(JIILjava/lang/String;)V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-wide p1, p0, Lcom/google/android/apps/chrome/snapshot/DownloadInfo;->mDownloadId:J

    .line 18
    iput p3, p0, Lcom/google/android/apps/chrome/snapshot/DownloadInfo;->mStatus:I

    .line 19
    iput p4, p0, Lcom/google/android/apps/chrome/snapshot/DownloadInfo;->mReason:I

    .line 20
    iput-object p5, p0, Lcom/google/android/apps/chrome/snapshot/DownloadInfo;->mMediaType:Ljava/lang/String;

    .line 21
    return-void
.end method


# virtual methods
.method public getDownloadId()J
    .locals 2

    .prologue
    .line 24
    iget-wide v0, p0, Lcom/google/android/apps/chrome/snapshot/DownloadInfo;->mDownloadId:J

    return-wide v0
.end method

.method public getMediaType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/DownloadInfo;->mMediaType:Ljava/lang/String;

    return-object v0
.end method

.method public getReason()I
    .locals 1

    .prologue
    .line 32
    iget v0, p0, Lcom/google/android/apps/chrome/snapshot/DownloadInfo;->mReason:I

    return v0
.end method

.method public getStatus()I
    .locals 1

    .prologue
    .line 28
    iget v0, p0, Lcom/google/android/apps/chrome/snapshot/DownloadInfo;->mStatus:I

    return v0
.end method
