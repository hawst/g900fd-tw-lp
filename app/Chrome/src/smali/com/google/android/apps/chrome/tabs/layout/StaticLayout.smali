.class public Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;
.super Lcom/google/android/apps/chrome/tabs/layout/Layout;
.source "StaticLayout.java"


# static fields
.field public static final TAG:Ljava/lang/String; = "StaticLayout"


# instance fields
.field private final mHandler:Landroid/os/Handler;

.field private mHandlesTabLifecycles:Z

.field private final mUnstallRunnable:Lcom/google/android/apps/chrome/tabs/layout/StaticLayout$UnstallRunnable;

.field private mUnstalling:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;Lcom/google/android/apps/chrome/eventfilter/EventFilter;)V
    .locals 6

    .prologue
    .line 58
    new-instance v5, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StaticStripStacker;

    invoke-direct {v5}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StaticStripStacker;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/chrome/tabs/layout/Layout;-><init>(Landroid/content/Context;Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;Lcom/google/android/apps/chrome/eventfilter/EventFilter;Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripStacker;)V

    .line 60
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->mHandler:Landroid/os/Handler;

    .line 61
    new-instance v0, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout$UnstallRunnable;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout$UnstallRunnable;-><init>(Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;Lcom/google/android/apps/chrome/tabs/layout/StaticLayout$1;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->mUnstallRunnable:Lcom/google/android/apps/chrome/tabs/layout/StaticLayout$UnstallRunnable;

    .line 62
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->mUnstalling:Z

    .line 63
    return-void
.end method

.method static synthetic access$002(Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;Z)Z
    .locals 0

    .prologue
    .line 24
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->mUnstalling:Z

    return p1
.end method

.method private setPostHideState()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 135
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->mUnstallRunnable:Lcom/google/android/apps/chrome/tabs/layout/StaticLayout$UnstallRunnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 136
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    aget-object v0, v0, v2

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setStaticToViewBlend(F)V

    .line 137
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    aget-object v0, v0, v2

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setSaturation(F)V

    .line 138
    iput-boolean v2, p0, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->mUnstalling:Z

    .line 139
    return-void
.end method

.method private setPreHideState()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 128
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->mUnstallRunnable:Lcom/google/android/apps/chrome/tabs/layout/StaticLayout$UnstallRunnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 129
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    aget-object v0, v0, v2

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setStaticToViewBlend(F)V

    .line 130
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    aget-object v0, v0, v2

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setSaturation(F)V

    .line 131
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->mUnstalling:Z

    .line 132
    return-void
.end method

.method private setStaticTab(I)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 142
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    array-length v0, v0

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    aget-object v0, v0, v4

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getId()I

    move-result v0

    if-ne v0, p1, :cond_1

    .line 143
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    aget-object v0, v0, v4

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->shouldStall()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->setPostHideState()V

    .line 159
    :cond_0
    :goto_0
    return-void

    .line 146
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    invoke-interface {v0, p1}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->getModelForTabId(I)Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v0

    .line 147
    if-eqz v0, :cond_0

    .line 148
    new-instance v1, Ljava/util/LinkedList;

    new-array v2, v5, [Ljava/lang/Integer;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/LinkedList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->updateCacheVisibleIds(Ljava/util/List;)V

    .line 149
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    array-length v1, v1

    if-eq v1, v5, :cond_3

    :cond_2
    new-array v1, v5, [Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    iput-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    .line 150
    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->isIncognito()Z

    move-result v0

    invoke-virtual {p0, p1, v0, v4, v4}, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->createLayoutTab(IZZZ)Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v0

    aput-object v0, v1, v4

    .line 151
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    aget-object v0, v0, v4

    invoke-virtual {v0, v4}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setDrawDecoration(Z)V

    .line 152
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    aget-object v0, v0, v4

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->shouldStall()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 153
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->setPreHideState()V

    .line 154
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->mUnstallRunnable:Lcom/google/android/apps/chrome/tabs/layout/StaticLayout$UnstallRunnable;

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 158
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->requestRender()V

    goto :goto_0

    .line 156
    :cond_4
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->setPostHideState()V

    goto :goto_1
.end method


# virtual methods
.method public getSizingFlags()I
    .locals 1

    .prologue
    .line 75
    const/16 v0, 0x111

    return v0
.end method

.method public handlesCloseAll()Z
    .locals 1

    .prologue
    .line 190
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->mHandlesTabLifecycles:Z

    return v0
.end method

.method public handlesTabClosing()Z
    .locals 1

    .prologue
    .line 185
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->mHandlesTabLifecycles:Z

    return v0
.end method

.method public handlesTabCreating()Z
    .locals 1

    .prologue
    .line 180
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->mHandlesTabLifecycles:Z

    return v0
.end method

.method public isTabInteractive()Z
    .locals 1

    .prologue
    .line 200
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    array-length v0, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onTabCreated(JIIIZZFF)V
    .locals 1

    .prologue
    .line 110
    invoke-super/range {p0 .. p9}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->onTabCreated(JIIIZZFF)V

    .line 112
    if-nez p7, :cond_0

    invoke-direct {p0, p3}, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->setStaticTab(I)V

    .line 113
    :cond_0
    return-void
.end method

.method public onTabModelSwitched(Z)V
    .locals 1

    .prologue
    .line 117
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->onTabModelSwitched(Z)V

    .line 118
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->getCurrentTabId()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->setStaticTab(I)V

    .line 119
    return-void
.end method

.method public onTabPageLoadFinished(IZ)V
    .locals 0

    .prologue
    .line 123
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->onTabPageLoadFinished(IZ)V

    .line 124
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->unstallImmediately(I)V

    .line 125
    return-void
.end method

.method public onTabSelected(JIIZ)V
    .locals 1

    .prologue
    .line 97
    invoke-direct {p0, p3}, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->setStaticTab(I)V

    .line 98
    invoke-super/range {p0 .. p5}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->onTabSelected(JIIZ)V

    .line 99
    return-void
.end method

.method public onTabSelecting(JI)V
    .locals 1

    .prologue
    .line 103
    invoke-direct {p0, p3}, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->setStaticTab(I)V

    .line 104
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->onTabSelecting(JI)V

    .line 105
    return-void
.end method

.method public setLayoutHandlesTabLifecycles(Z)V
    .locals 0

    .prologue
    .line 70
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->mHandlesTabLifecycles:Z

    .line 71
    return-void
.end method

.method public shouldDisplayContentOverlay()Z
    .locals 1

    .prologue
    .line 195
    const/4 v0, 0x1

    return v0
.end method

.method public show(JZ)V
    .locals 1

    .prologue
    .line 85
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    .line 86
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->getCurrentTabId()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->setStaticTab(I)V

    .line 87
    return-void
.end method

.method public unstallImmediately()V
    .locals 2

    .prologue
    .line 171
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    array-length v0, v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->shouldStall()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->mUnstalling:Z

    if-eqz v0, :cond_0

    .line 173
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->mUnstallRunnable:Lcom/google/android/apps/chrome/tabs/layout/StaticLayout$UnstallRunnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 174
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->mUnstallRunnable:Lcom/google/android/apps/chrome/tabs/layout/StaticLayout$UnstallRunnable;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout$UnstallRunnable;->run()V

    .line 176
    :cond_0
    return-void
.end method

.method public unstallImmediately(I)V
    .locals 2

    .prologue
    .line 164
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    array-length v0, v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getId()I

    move-result v0

    if-ne v0, p1, :cond_0

    .line 165
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->unstallImmediately()V

    .line 167
    :cond_0
    return-void
.end method

.method protected updateLayout(JJ)V
    .locals 3

    .prologue
    .line 91
    invoke-super {p0, p1, p2, p3, p4}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->updateLayout(JJ)V

    .line 92
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    array-length v0, v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0, p3, p4}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->updateSnap(J)Z

    .line 93
    :cond_0
    return-void
.end method
