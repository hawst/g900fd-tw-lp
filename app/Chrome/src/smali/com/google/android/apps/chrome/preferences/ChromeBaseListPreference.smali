.class public Lcom/google/android/apps/chrome/preferences/ChromeBaseListPreference;
.super Landroid/preference/ListPreference;
.source "ChromeBaseListPreference.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0, p1}, Landroid/preference/ListPreference;-><init>(Landroid/content/Context;)V

    .line 19
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0, p1, p2}, Landroid/preference/ListPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 23
    return-void
.end method


# virtual methods
.method protected onBindView(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 27
    invoke-super {p0, p1}, Landroid/preference/ListPreference;->onBindView(Landroid/view/View;)V

    .line 28
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBaseListPreference;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->changeViewStyle(Landroid/view/View;Landroid/content/Context;)V

    .line 29
    invoke-static {p0, p1}, Lcom/google/android/apps/chrome/preferences/ManagedPreferencesUtils;->onBindViewToPreference(Landroid/preference/Preference;Landroid/view/View;)V

    .line 30
    return-void
.end method

.method protected onClick()V
    .locals 1

    .prologue
    .line 34
    invoke-static {p0}, Lcom/google/android/apps/chrome/preferences/ManagedPreferencesUtils;->onClickPreference(Landroid/preference/Preference;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 38
    :goto_0
    return-void

    .line 37
    :cond_0
    invoke-super {p0}, Landroid/preference/ListPreference;->onClick()V

    goto :goto_0
.end method
