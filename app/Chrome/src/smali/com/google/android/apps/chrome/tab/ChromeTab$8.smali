.class Lcom/google/android/apps/chrome/tab/ChromeTab$8;
.super Lorg/chromium/content_public/browser/GestureStateListener;
.source "ChromeTab.java"


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/tab/ChromeTab;)V
    .locals 0

    .prologue
    .line 1761
    iput-object p1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$8;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-direct {p0}, Lorg/chromium/content_public/browser/GestureStateListener;-><init>()V

    return-void
.end method

.method private onScrollingStateChanged()V
    .locals 2

    .prologue
    .line 1787
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$8;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # getter for: Lcom/google/android/apps/chrome/tab/ChromeTab;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$2400(Lcom/google/android/apps/chrome/tab/ChromeTab;)Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1790
    :goto_0
    return-void

    .line 1788
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$8;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # getter for: Lcom/google/android/apps/chrome/tab/ChromeTab;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$2400(Lcom/google/android/apps/chrome/tab/ChromeTab;)Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$8;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$8;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->isScrollInProgress()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->onContentViewScrollingStateChanged(Z)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public onFlingEndGesture(II)V
    .locals 1

    .prologue
    .line 1770
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$8;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # invokes: Lcom/google/android/apps/chrome/tab/ChromeTab;->rescheduleThumbnailCapture()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$6200(Lcom/google/android/apps/chrome/tab/ChromeTab;)V

    .line 1771
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab$8;->onScrollingStateChanged()V

    .line 1772
    return-void
.end method

.method public onFlingStartGesture(IIII)V
    .locals 1

    .prologue
    .line 1764
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$8;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # invokes: Lcom/google/android/apps/chrome/tab/ChromeTab;->cancelThumbnailCapture()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$4100(Lcom/google/android/apps/chrome/tab/ChromeTab;)V

    .line 1765
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab$8;->onScrollingStateChanged()V

    .line 1766
    return-void
.end method

.method public onScrollEnded(II)V
    .locals 1

    .prologue
    .line 1782
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$8;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # invokes: Lcom/google/android/apps/chrome/tab/ChromeTab;->rescheduleThumbnailCapture()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$6200(Lcom/google/android/apps/chrome/tab/ChromeTab;)V

    .line 1783
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab$8;->onScrollingStateChanged()V

    .line 1784
    return-void
.end method

.method public onScrollStarted(II)V
    .locals 1

    .prologue
    .line 1776
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$8;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # invokes: Lcom/google/android/apps/chrome/tab/ChromeTab;->cancelThumbnailCapture()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$4100(Lcom/google/android/apps/chrome/tab/ChromeTab;)V

    .line 1777
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab$8;->onScrollingStateChanged()V

    .line 1778
    return-void
.end method
