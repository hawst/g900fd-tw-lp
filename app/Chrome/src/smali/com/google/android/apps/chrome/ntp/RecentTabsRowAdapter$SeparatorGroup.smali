.class Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$SeparatorGroup;
.super Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$Group;
.source "RecentTabsRowAdapter.java"


# instance fields
.field private final mIsVisible:Z

.field final synthetic this$0:Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;Z)V
    .locals 0

    .prologue
    .line 565
    iput-object p1, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$SeparatorGroup;->this$0:Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$Group;-><init>(Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;)V

    .line 566
    iput-boolean p2, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$SeparatorGroup;->mIsVisible:Z

    .line 567
    return-void
.end method


# virtual methods
.method public configureChildView(ILcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$ViewHolder;)V
    .locals 0

    .prologue
    .line 601
    return-void
.end method

.method public configureGroupView(Lcom/google/android/apps/chrome/ntp/RecentTabsGroupView;Z)V
    .locals 0

    .prologue
    .line 597
    return-void
.end method

.method public getChild(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 605
    const/4 v0, 0x0

    return-object v0
.end method

.method public getChildType()Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$ChildType;
    .locals 1

    .prologue
    .line 576
    sget-object v0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$ChildType;->NONE:Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$ChildType;

    return-object v0
.end method

.method public getChildrenCount()I
    .locals 1

    .prologue
    .line 581
    const/4 v0, 0x0

    return v0
.end method

.method public getGroupType()Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$GroupType;
    .locals 1

    .prologue
    .line 571
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$SeparatorGroup;->mIsVisible:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$GroupType;->VISIBLE_SEPARATOR:Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$GroupType;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$GroupType;->INVISIBLE_SEPARATOR:Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$GroupType;

    goto :goto_0
.end method

.method public getGroupView(ZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 586
    if-nez p2, :cond_0

    .line 587
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$SeparatorGroup;->mIsVisible:Z

    if-eqz v0, :cond_1

    sget v0, Lcom/google/android/apps/chrome/R$layout;->recent_tabs_group_separator_visible:I

    .line 590
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$SeparatorGroup;->this$0:Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;

    # getter for: Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->mActivity:Landroid/app/Activity;
    invoke-static {v1}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->access$000(Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;)Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v0, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 592
    :cond_0
    return-object p2

    .line 587
    :cond_1
    sget v0, Lcom/google/android/apps/chrome/R$layout;->recent_tabs_group_separator_invisible:I

    goto :goto_0
.end method

.method public isCollapsed()Z
    .locals 1

    .prologue
    .line 614
    const/4 v0, 0x0

    return v0
.end method

.method public onChildClick(I)Z
    .locals 1

    .prologue
    .line 619
    const/4 v0, 0x0

    return v0
.end method

.method public setCollapsed(Z)V
    .locals 0

    .prologue
    .line 610
    return-void
.end method
