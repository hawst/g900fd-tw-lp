.class public Lcom/google/android/apps/chrome/sync/SyncDisabler;
.super Ljava/lang/Object;
.source "SyncDisabler.java"

# interfaces
.implements Lorg/chromium/chrome/browser/sync/ProfileSyncService$SyncStateChangedListener;


# instance fields
.field private final mChromeSigninController:Lorg/chromium/sync/signin/ChromeSigninController;

.field private final mGoogleServicesManager:Lcom/google/android/apps/chrome/services/GoogleServicesManager;

.field private final mProfileSyncService:Lorg/chromium/chrome/browser/sync/ProfileSyncService;

.field private final mSyncStatusHelper:Lorg/chromium/sync/notifier/SyncStatusHelper;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/chrome/services/GoogleServicesManager;Lorg/chromium/sync/signin/ChromeSigninController;Lorg/chromium/sync/notifier/SyncStatusHelper;Lorg/chromium/chrome/browser/sync/ProfileSyncService;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/google/android/apps/chrome/sync/SyncDisabler;->mGoogleServicesManager:Lcom/google/android/apps/chrome/services/GoogleServicesManager;

    .line 39
    iput-object p2, p0, Lcom/google/android/apps/chrome/sync/SyncDisabler;->mChromeSigninController:Lorg/chromium/sync/signin/ChromeSigninController;

    .line 40
    iput-object p3, p0, Lcom/google/android/apps/chrome/sync/SyncDisabler;->mSyncStatusHelper:Lorg/chromium/sync/notifier/SyncStatusHelper;

    .line 41
    iput-object p4, p0, Lcom/google/android/apps/chrome/sync/SyncDisabler;->mProfileSyncService:Lorg/chromium/chrome/browser/sync/ProfileSyncService;

    .line 42
    return-void
.end method

.method private isSyncSuppressStart()Z
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncDisabler;->mProfileSyncService:Lorg/chromium/chrome/browser/sync/ProfileSyncService;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->isStartSuppressed()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public syncStateChanged()V
    .locals 3

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncDisabler;->mChromeSigninController:Lorg/chromium/sync/signin/ChromeSigninController;

    invoke-virtual {v0}, Lorg/chromium/sync/signin/ChromeSigninController;->getSignedInUser()Landroid/accounts/Account;

    move-result-object v0

    .line 47
    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/SyncDisabler;->isSyncSuppressStart()Z

    move-result v1

    .line 48
    iget-object v2, p0, Lcom/google/android/apps/chrome/sync/SyncDisabler;->mSyncStatusHelper:Lorg/chromium/sync/notifier/SyncStatusHelper;

    invoke-virtual {v2, v0}, Lorg/chromium/sync/notifier/SyncStatusHelper;->isSyncEnabled(Landroid/accounts/Account;)Z

    move-result v2

    .line 49
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    if-eqz v2, :cond_0

    .line 50
    const-string/jumbo v1, "SyncDisabler"

    const-string/jumbo v2, "Disabling sync because it was disabled externally"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 51
    invoke-static {}, Lcom/google/android/apps/chrome/services/GoogleServicesStates;->create()Lcom/google/android/apps/chrome/services/GoogleServicesStates$GoogleServicesStatesBuilder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/apps/chrome/services/GoogleServicesStates$GoogleServicesStatesBuilder;->sync(Z)Lcom/google/android/apps/chrome/services/GoogleServicesStates$GoogleServicesStatesBuilder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/services/GoogleServicesStates$GoogleServicesStatesBuilder;->build()Lcom/google/android/apps/chrome/services/GoogleServicesStates;

    move-result-object v1

    .line 52
    iget-object v2, p0, Lcom/google/android/apps/chrome/sync/SyncDisabler;->mGoogleServicesManager:Lcom/google/android/apps/chrome/services/GoogleServicesManager;

    invoke-virtual {v2, v1, v0}, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->setStates(Lcom/google/android/apps/chrome/services/GoogleServicesStates;Landroid/accounts/Account;)V

    .line 54
    :cond_0
    return-void
.end method
