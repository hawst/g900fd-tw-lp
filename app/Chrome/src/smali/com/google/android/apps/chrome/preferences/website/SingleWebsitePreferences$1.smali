.class Lcom/google/android/apps/chrome/preferences/website/SingleWebsitePreferences$1;
.super Ljava/lang/Object;
.source "SingleWebsitePreferences.java"

# interfaces
.implements Lcom/google/android/apps/chrome/preferences/website/Website$StoredDataClearedCallback;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/preferences/website/SingleWebsitePreferences;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/preferences/website/SingleWebsitePreferences;)V
    .locals 0

    .prologue
    .line 138
    iput-object p1, p0, Lcom/google/android/apps/chrome/preferences/website/SingleWebsitePreferences$1;->this$0:Lcom/google/android/apps/chrome/preferences/website/SingleWebsitePreferences;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onStoredDataCleared()V
    .locals 3

    .prologue
    .line 141
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/SingleWebsitePreferences$1;->this$0:Lcom/google/android/apps/chrome/preferences/website/SingleWebsitePreferences;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/website/SingleWebsitePreferences;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/website/SingleWebsitePreferences$1;->this$0:Lcom/google/android/apps/chrome/preferences/website/SingleWebsitePreferences;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/preferences/website/SingleWebsitePreferences;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    const-string/jumbo v2, "clear_data"

    invoke-virtual {v1, v2}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 143
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/SingleWebsitePreferences$1;->this$0:Lcom/google/android/apps/chrome/preferences/website/SingleWebsitePreferences;

    # invokes: Lcom/google/android/apps/chrome/preferences/website/SingleWebsitePreferences;->popBackIfNoSettings()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/preferences/website/SingleWebsitePreferences;->access$000(Lcom/google/android/apps/chrome/preferences/website/SingleWebsitePreferences;)V

    .line 144
    return-void
.end method
