.class Lcom/google/android/apps/chrome/videofling/MediaRouteController$5;
.super Ljava/lang/Object;
.source "MediaRouteController.java"

# interfaces
.implements Lcom/google/android/apps/chrome/videofling/MediaRouteController$ResultBundleHandler;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

.field final synthetic val$preferredTitle:Ljava/lang/String;

.field final synthetic val$startPositionMillis:J

.field final synthetic val$videoUri:Landroid/net/Uri;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/videofling/MediaRouteController;Landroid/net/Uri;Ljava/lang/String;J)V
    .locals 0

    .prologue
    .line 528
    iput-object p1, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController$5;->this$0:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    iput-object p2, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController$5;->val$videoUri:Landroid/net/Uri;

    iput-object p3, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController$5;->val$preferredTitle:Ljava/lang/String;

    iput-wide p4, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController$5;->val$startPositionMillis:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 543
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController$5;->this$0:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->release()V

    .line 544
    invoke-static {}, Lorg/chromium/base/library_loader/LibraryLoader;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-static {v0}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->castDefaultPlayerResult(Z)V

    .line 545
    :cond_0
    return-void
.end method

.method public onResult(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 531
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController$5;->this$0:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    const-string/jumbo v1, "android.media.intent.extra.SESSION_ID"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mCurrentSessionId:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->access$202(Lcom/google/android/apps/chrome/videofling/MediaRouteController;Ljava/lang/String;)Ljava/lang/String;

    .line 532
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController$5;->this$0:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    # getter for: Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->access$300(Lcom/google/android/apps/chrome/videofling/MediaRouteController;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController$5;->this$0:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    # getter for: Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mCurrentSessionId:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->access$200(Lcom/google/android/apps/chrome/videofling/MediaRouteController;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/videofling/RemotePlaybackSettings;->setSessionId(Landroid/content/Context;Ljava/lang/String;)V

    .line 533
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController$5;->this$0:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    # getter for: Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mDebug:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->access$400(Lcom/google/android/apps/chrome/videofling/MediaRouteController;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "MediaRouteController"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Got a session id: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController$5;->this$0:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    # getter for: Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mCurrentSessionId:Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->access$200(Lcom/google/android/apps/chrome/videofling/MediaRouteController;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 535
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController$5;->this$0:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    iget-object v1, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController$5;->val$videoUri:Landroid/net/Uri;

    # setter for: Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mVideoUriToStart:Landroid/net/Uri;
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->access$502(Lcom/google/android/apps/chrome/videofling/MediaRouteController;Landroid/net/Uri;)Landroid/net/Uri;

    .line 536
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController$5;->this$0:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    # getter for: Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->access$300(Lcom/google/android/apps/chrome/videofling/MediaRouteController;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController$5;->val$videoUri:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/videofling/RemotePlaybackSettings;->setUriPlaying(Landroid/content/Context;Ljava/lang/String;)V

    .line 537
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController$5;->this$0:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    iget-object v1, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController$5;->val$preferredTitle:Ljava/lang/String;

    # setter for: Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mPreferredTitle:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->access$602(Lcom/google/android/apps/chrome/videofling/MediaRouteController;Ljava/lang/String;)Ljava/lang/String;

    .line 538
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController$5;->this$0:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    iget-wide v2, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController$5;->val$startPositionMillis:J

    # setter for: Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mStartPositionMillis:J
    invoke-static {v0, v2, v3}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->access$702(Lcom/google/android/apps/chrome/videofling/MediaRouteController;J)J

    .line 539
    return-void
.end method
