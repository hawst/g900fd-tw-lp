.class public Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;
.super Lcom/google/android/apps/chrome/tabs/layout/Layout;
.source "AccessibilityOverviewLayout.java"

# interfaces
.implements Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelAdapter$AccessibilityTabModelAdapterListener;


# instance fields
.field private final mDpToPx:F

.field private mTabModelWrapper:Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelWrapper;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;Lcom/google/android/apps/chrome/eventfilter/EventFilter;)V
    .locals 6

    .prologue
    .line 40
    new-instance v5, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StaticStripStacker;

    invoke-direct {v5}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StaticStripStacker;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/chrome/tabs/layout/Layout;-><init>(Landroid/content/Context;Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;Lcom/google/android/apps/chrome/eventfilter/EventFilter;Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripStacker;)V

    .line 41
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    iput v0, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->mDpToPx:F

    .line 42
    return-void
.end method

.method private adjustForFullscreen()V
    .locals 3

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->mTabModelWrapper:Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelWrapper;

    if-nez v0, :cond_1

    .line 78
    :cond_0
    :goto_0
    return-void

    .line 73
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->mTabModelWrapper:Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelWrapper;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelWrapper;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 75
    if-eqz v0, :cond_0

    .line 76
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->getHeight()F

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->getHeightMinusTopControls()F

    move-result v2

    sub-float/2addr v1, v2

    iget v2, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->mDpToPx:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 77
    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->mTabModelWrapper:Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelWrapper;

    invoke-virtual {v1, v0}, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelWrapper;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method


# virtual methods
.method public attachViews(Landroid/view/ViewGroup;)V
    .locals 3

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->mTabModelWrapper:Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelWrapper;

    if-nez v0, :cond_0

    .line 47
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lorg/chromium/chrome/R$layout;->accessibility_tab_switcher:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelWrapper;

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->mTabModelWrapper:Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelWrapper;

    .line 49
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->mTabModelWrapper:Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelWrapper;

    invoke-virtual {v0, p0}, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelWrapper;->setup(Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelAdapter$AccessibilityTabModelAdapterListener;)V

    .line 50
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->mTabModelWrapper:Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelWrapper;

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    invoke-virtual {v0, v1}, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelWrapper;->setTabModelSelector(Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;)V

    .line 51
    invoke-direct {p0}, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->adjustForFullscreen()V

    .line 54
    :cond_0
    if-nez p1, :cond_2

    .line 59
    :cond_1
    :goto_0
    return-void

    .line 56
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->mTabModelWrapper:Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelWrapper;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelWrapper;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-nez v0, :cond_1

    .line 57
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->mTabModelWrapper:Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelWrapper;

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_0
.end method

.method public detachViews()V
    .locals 2

    .prologue
    .line 129
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->commitAllTabClosures()V

    .line 130
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->mTabModelWrapper:Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelWrapper;

    if-eqz v0, :cond_1

    .line 131
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->mTabModelWrapper:Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelWrapper;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelWrapper;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 132
    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->mTabModelWrapper:Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelWrapper;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 134
    :cond_1
    return-void
.end method

.method public getAccessibilityContainer()Landroid/view/ViewGroup;
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->mTabModelWrapper:Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelWrapper;

    return-object v0
.end method

.method public getSizingFlags()I
    .locals 1

    .prologue
    .line 63
    const/16 v0, 0x1000

    return v0
.end method

.method public handlesCloseAll()Z
    .locals 1

    .prologue
    .line 87
    const/4 v0, 0x1

    return v0
.end method

.method public handlesTabClosing()Z
    .locals 1

    .prologue
    .line 82
    const/4 v0, 0x1

    return v0
.end method

.method protected notifySizeChanged(FFI)V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->adjustForFullscreen()V

    .line 69
    return-void
.end method

.method public onTabClosureCommitted(JIZ)V
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->mTabModelWrapper:Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelWrapper;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelWrapper;->setStateBasedOnModel()V

    .line 167
    return-void
.end method

.method public onTabCreated(JIIIZZFF)V
    .locals 1

    .prologue
    .line 99
    invoke-super/range {p0 .. p9}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->onTabCreated(JIIIZZFF)V

    .line 101
    invoke-virtual {p0, p3}, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->startHiding(I)V

    .line 102
    return-void
.end method

.method public onTabCreating(I)V
    .locals 0

    .prologue
    .line 92
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->onTabCreating(I)V

    .line 93
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->startHiding(I)V

    .line 94
    return-void
.end method

.method public onTabModelSwitched(Z)V
    .locals 1

    .prologue
    .line 106
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->onTabModelSwitched(Z)V

    .line 107
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->mTabModelWrapper:Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelWrapper;

    if-nez v0, :cond_0

    .line 109
    :goto_0
    return-void

    .line 108
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->mTabModelWrapper:Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelWrapper;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelWrapper;->setStateBasedOnModel()V

    goto :goto_0
.end method

.method public onTabsAllClosing(JZ)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 152
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->onTabsAllClosing(JZ)V

    .line 154
    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    invoke-interface {v1, p3}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->getModel(Z)Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v1

    .line 155
    :goto_0
    invoke-interface {v1}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->getCount()I

    move-result v2

    if-lez v2, :cond_0

    invoke-static {v1, v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelUtils;->closeTabByIndex(Lorg/chromium/chrome/browser/tabmodel/TabModel;I)Z

    goto :goto_0

    .line 157
    :cond_0
    if-eqz p3, :cond_2

    .line 158
    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    if-nez p3, :cond_1

    const/4 v0, 0x1

    :cond_1
    invoke-interface {v1, v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->selectModel(Z)V

    .line 160
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->mTabModelWrapper:Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelWrapper;

    if-nez v0, :cond_3

    .line 162
    :goto_1
    return-void

    .line 161
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->mTabModelWrapper:Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelWrapper;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelWrapper;->setStateBasedOnModel()V

    goto :goto_1
.end method

.method public setTabModelSelector(Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;Lcom/google/android/apps/chrome/compositor/TabContentManager;)V
    .locals 1

    .prologue
    .line 139
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->setTabModelSelector(Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;Lcom/google/android/apps/chrome/compositor/TabContentManager;)V

    .line 141
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->mTabModelWrapper:Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelWrapper;

    if-nez v0, :cond_0

    .line 143
    :goto_0
    return-void

    .line 142
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->mTabModelWrapper:Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelWrapper;

    invoke-virtual {v0, p1}, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelWrapper;->setTabModelSelector(Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;)V

    goto :goto_0
.end method

.method public show(JZ)V
    .locals 3

    .prologue
    .line 113
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->show(JZ)V

    .line 114
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->mTabModelWrapper:Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelWrapper;

    if-nez v0, :cond_0

    .line 118
    :goto_0
    return-void

    .line 115
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->mTabModelWrapper:Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelWrapper;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/widget/accessibility/AccessibilityTabModelWrapper;->setStateBasedOnModel()V

    .line 116
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    const/16 v1, 0xd

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastNotification(Landroid/content/Context;I)V

    goto :goto_0
.end method

.method public showTab(I)V
    .locals 2

    .prologue
    .line 171
    const-wide/16 v0, 0x0

    invoke-virtual {p0, v0, v1, p1}, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->onTabSelecting(JI)V

    .line 172
    return-void
.end method

.method public startHiding(I)V
    .locals 0

    .prologue
    .line 122
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->startHiding(I)V

    .line 124
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->doneHiding()V

    .line 125
    return-void
.end method
