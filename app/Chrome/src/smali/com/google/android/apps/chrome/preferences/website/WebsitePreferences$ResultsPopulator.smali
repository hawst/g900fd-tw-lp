.class Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$ResultsPopulator;
.super Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$CallChain;
.source "WebsitePreferences.java"


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences;)V
    .locals 1

    .prologue
    .line 201
    iput-object p1, p0, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$ResultsPopulator;->this$0:Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$CallChain;-><init>(Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences;Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$1;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences;Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$1;)V
    .locals 0

    .prologue
    .line 201
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$ResultsPopulator;-><init>(Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences;)V

    return-void
.end method


# virtual methods
.method run()V
    .locals 7

    .prologue
    .line 208
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$ResultsPopulator;->this$0:Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_0

    .line 209
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$ResultsPopulator;->next()V

    .line 240
    :goto_0
    return-void

    .line 213
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 214
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 215
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$ResultsPopulator;->this$0:Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences;

    # getter for: Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences;->mSitesByOrigin:Ljava/util/Map;
    invoke-static {v0}, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences;->access$1200(Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 216
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/preferences/website/Website;

    .line 217
    new-instance v5, Lcom/google/android/apps/chrome/preferences/website/WebsitePreference;

    iget-object v6, p0, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$ResultsPopulator;->this$0:Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences;

    invoke-virtual {v6}, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-direct {v5, v6, v0}, Lcom/google/android/apps/chrome/preferences/website/WebsitePreference;-><init>(Landroid/content/Context;Lcom/google/android/apps/chrome/preferences/website/Website;)V

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 218
    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 222
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$ResultsPopulator;->this$0:Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences;

    # getter for: Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences;->mSitesByHost:Ljava/util/Map;
    invoke-static {v0}, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences;->access$1300(Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 223
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_4
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/preferences/website/Website;

    .line 224
    invoke-interface {v2, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_4

    .line 225
    new-instance v5, Lcom/google/android/apps/chrome/preferences/website/WebsitePreference;

    iget-object v6, p0, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$ResultsPopulator;->this$0:Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences;

    invoke-virtual {v6}, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-direct {v5, v6, v0}, Lcom/google/android/apps/chrome/preferences/website/WebsitePreference;-><init>(Landroid/content/Context;Lcom/google/android/apps/chrome/preferences/website/Website;)V

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 226
    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 230
    :cond_5
    invoke-static {v1}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 231
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$ResultsPopulator;->this$0:Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0}, Landroid/preference/PreferenceScreen;->removeAll()V

    .line 232
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_6

    .line 233
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/preferences/website/WebsitePreference;

    .line 234
    iget-object v2, p0, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$ResultsPopulator;->this$0:Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    goto :goto_3

    .line 237
    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$ResultsPopulator;->this$0:Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences;

    # invokes: Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences;->displayEmptyScreenMessage()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences;->access$1400(Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences;)V

    .line 239
    :cond_7
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$ResultsPopulator;->next()V

    goto/16 :goto_0
.end method
