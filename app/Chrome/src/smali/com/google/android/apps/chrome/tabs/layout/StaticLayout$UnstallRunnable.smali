.class Lcom/google/android/apps/chrome/tabs/layout/StaticLayout$UnstallRunnable;
.super Ljava/lang/Object;
.source "StaticLayout.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;)V
    .locals 0

    .prologue
    .line 32
    iput-object p1, p0, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout$UnstallRunnable;->this$0:Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;Lcom/google/android/apps/chrome/tabs/layout/StaticLayout$1;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout$UnstallRunnable;-><init>(Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 11

    .prologue
    const-wide/16 v6, 0x1f4

    const-wide/16 v8, 0x0

    const/4 v10, 0x0

    .line 35
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout$UnstallRunnable;->this$0:Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;

    # setter for: Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->mUnstalling:Z
    invoke-static {v0, v10}, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->access$002(Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;Z)Z

    .line 36
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout$UnstallRunnable;->this$0:Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;

    iget-object v0, v0, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout$UnstallRunnable;->this$0:Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;

    iget-object v0, v0, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    array-length v0, v0

    if-nez v0, :cond_1

    .line 42
    :cond_0
    :goto_0
    return-void

    .line 37
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout$UnstallRunnable;->this$0:Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout$UnstallRunnable;->this$0:Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;

    iget-object v0, v0, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    aget-object v2, v0, v10

    sget-object v3, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;->SATURATION:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout$UnstallRunnable;->this$0:Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;

    iget-object v0, v0, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    aget-object v0, v0, v10

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getSaturation()F

    move-result v4

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-virtual/range {v1 .. v9}, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->addToAnimation(Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJ)V

    .line 39
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout$UnstallRunnable;->this$0:Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout$UnstallRunnable;->this$0:Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;

    iget-object v0, v0, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    aget-object v2, v0, v10

    sget-object v3, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;->STATIC_TO_VIEW_BLEND:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout$UnstallRunnable;->this$0:Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;

    iget-object v0, v0, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    aget-object v0, v0, v10

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getStaticToViewBlend()F

    move-result v4

    const/4 v5, 0x0

    invoke-virtual/range {v1 .. v9}, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->addToAnimation(Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJ)V

    .line 41
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout$UnstallRunnable;->this$0:Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;

    iget-object v0, v0, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    aget-object v0, v0, v10

    invoke-virtual {v0, v10}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setShouldStall(Z)V

    goto :goto_0
.end method
