.class Lcom/google/android/apps/chrome/tab/ChromeDownloadListener$1;
.super Landroid/os/AsyncTask;
.source "ChromeDownloadListener.java"


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;

.field final synthetic val$fileName:Ljava/lang/String;

.field final synthetic val$manager:Landroid/app/DownloadManager;

.field final synthetic val$path:Ljava/lang/String;

.field final synthetic val$uri:Lcom/google/android/apps/chrome/third_party/DataUri;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;Ljava/lang/String;Lcom/google/android/apps/chrome/third_party/DataUri;Landroid/app/DownloadManager;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 206
    iput-object p1, p0, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener$1;->this$0:Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;

    iput-object p2, p0, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener$1;->val$path:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener$1;->val$uri:Lcom/google/android/apps/chrome/third_party/DataUri;

    iput-object p4, p0, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener$1;->val$manager:Landroid/app/DownloadManager;

    iput-object p5, p0, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener$1;->val$fileName:Ljava/lang/String;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 206
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener$1;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method public varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 11

    .prologue
    const/4 v10, 0x0

    .line 209
    .line 211
    :try_start_0
    new-instance v9, Ljava/io/FileOutputStream;

    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener$1;->val$path:Ljava/lang/String;

    invoke-direct {v9, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 212
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener$1;->val$uri:Lcom/google/android/apps/chrome/third_party/DataUri;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/third_party/DataUri;->getData()[B

    move-result-object v0

    invoke-virtual {v9, v0}, Ljava/io/FileOutputStream;->write([B)V

    .line 214
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener$1;->val$manager:Landroid/app/DownloadManager;

    iget-object v1, p0, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener$1;->val$fileName:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener$1;->val$fileName:Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener$1;->val$uri:Lcom/google/android/apps/chrome/third_party/DataUri;

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/third_party/DataUri;->getMimeType()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener$1;->val$path:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener$1;->val$uri:Lcom/google/android/apps/chrome/third_party/DataUri;

    invoke-virtual {v6}, Lcom/google/android/apps/chrome/third_party/DataUri;->getData()[B

    move-result-object v6

    array-length v6, v6

    int-to-long v6, v6

    const/4 v8, 0x1

    invoke-virtual/range {v0 .. v8}, Landroid/app/DownloadManager;->addCompletedDownload(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;JZ)J
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 222
    :try_start_2
    invoke-virtual {v9}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    .line 230
    :cond_0
    :goto_0
    return-object v10

    .line 219
    :catch_0
    move-exception v0

    move-object v1, v10

    .line 220
    :goto_1
    :try_start_3
    const-string/jumbo v2, "ChromeDownloadListener"

    const-string/jumbo v3, "Could not save data URL "

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 222
    if-eqz v1, :cond_0

    .line 224
    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_0

    .line 227
    :catch_1
    move-exception v0

    goto :goto_0

    .line 222
    :catchall_0
    move-exception v0

    move-object v9, v10

    :goto_2
    if-eqz v9, :cond_1

    .line 224
    :try_start_5
    invoke-virtual {v9}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 227
    :cond_1
    :goto_3
    throw v0

    :catch_2
    move-exception v0

    goto :goto_0

    :catch_3
    move-exception v1

    goto :goto_3

    .line 222
    :catchall_1
    move-exception v0

    goto :goto_2

    :catchall_2
    move-exception v0

    move-object v9, v1

    goto :goto_2

    .line 219
    :catch_4
    move-exception v0

    move-object v1, v9

    goto :goto_1
.end method
