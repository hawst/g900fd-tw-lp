.class Lcom/google/android/apps/chrome/ContextualMenuBar$3;
.super Ljava/lang/Object;
.source "ContextualMenuBar.java"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/ContextualMenuBar;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/ContextualMenuBar;)V
    .locals 0

    .prologue
    .line 152
    iput-object p1, p0, Lcom/google/android/apps/chrome/ContextualMenuBar$3;->this$0:Lcom/google/android/apps/chrome/ContextualMenuBar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 5

    .prologue
    .line 155
    iget-object v0, p0, Lcom/google/android/apps/chrome/ContextualMenuBar$3;->this$0:Lcom/google/android/apps/chrome/ContextualMenuBar;

    # getter for: Lcom/google/android/apps/chrome/ContextualMenuBar;->mActionBarDelegate:Lcom/google/android/apps/chrome/ContextualMenuBar$ActionBarDelegate;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ContextualMenuBar;->access$100(Lcom/google/android/apps/chrome/ContextualMenuBar;)Lcom/google/android/apps/chrome/ContextualMenuBar$ActionBarDelegate;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/ContextualMenuBar$ActionBarDelegate;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 156
    if-eqz v0, :cond_0

    .line 157
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/chrome/ContextualMenuBar$3;->this$0:Lcom/google/android/apps/chrome/ContextualMenuBar;

    # invokes: Lcom/google/android/apps/chrome/ContextualMenuBar;->queryCurrentActionBarHeight()I
    invoke-static {v3}, Lcom/google/android/apps/chrome/ContextualMenuBar;->access$200(Lcom/google/android/apps/chrome/ContextualMenuBar;)I

    move-result v3

    int-to-float v3, v3

    iget-object v4, p0, Lcom/google/android/apps/chrome/ContextualMenuBar$3;->this$0:Lcom/google/android/apps/chrome/ContextualMenuBar;

    # getter for: Lcom/google/android/apps/chrome/ContextualMenuBar;->mTabStripHeight:F
    invoke-static {v4}, Lcom/google/android/apps/chrome/ContextualMenuBar;->access$300(Lcom/google/android/apps/chrome/ContextualMenuBar;)F

    move-result v4

    sub-float/2addr v3, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->max(FF)F

    move-result v2

    float-to-int v2, v2

    aput v2, v0, v1

    invoke-virtual {p1, v0}, Landroid/animation/ValueAnimator;->setIntValues([I)V

    .line 160
    :cond_0
    return-void
.end method
