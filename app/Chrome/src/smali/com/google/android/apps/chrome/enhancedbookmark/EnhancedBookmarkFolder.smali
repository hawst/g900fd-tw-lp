.class Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolder;
.super Landroid/widget/TextView;
.source "EnhancedBookmarkFolder.java"


# instance fields
.field private mBookmarkId:Lorg/chromium/components/bookmarks/BookmarkId;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 24
    return-void
.end method


# virtual methods
.method getBookmarkId()Lorg/chromium/components/bookmarks/BookmarkId;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolder;->mBookmarkId:Lorg/chromium/components/bookmarks/BookmarkId;

    return-object v0
.end method

.method setBookmarkId(Lorg/chromium/components/bookmarks/BookmarkId;)V
    .locals 0

    .prologue
    .line 30
    iput-object p1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolder;->mBookmarkId:Lorg/chromium/components/bookmarks/BookmarkId;

    .line 31
    return-void
.end method
