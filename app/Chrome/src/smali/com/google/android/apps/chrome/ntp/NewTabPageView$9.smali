.class Lcom/google/android/apps/chrome/ntp/NewTabPageView$9;
.super Ljava/lang/Object;
.source "NewTabPageView.java"

# interfaces
.implements Lcom/google/android/apps/chrome/ntp/NewTabScrollView$OnScrollListener;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/ntp/NewTabPageView;

.field final synthetic val$mSnapScrollRunnable:Ljava/lang/Runnable;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/ntp/NewTabPageView;Ljava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 369
    iput-object p1, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView$9;->this$0:Lcom/google/android/apps/chrome/ntp/NewTabPageView;

    iput-object p2, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView$9;->val$mSnapScrollRunnable:Ljava/lang/Runnable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onScrollChanged(IIII)V
    .locals 4

    .prologue
    .line 372
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView$9;->this$0:Lcom/google/android/apps/chrome/ntp/NewTabPageView;

    # getter for: Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mPendingSnapScroll:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->access$300(Lcom/google/android/apps/chrome/ntp/NewTabPageView;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 373
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView$9;->this$0:Lcom/google/android/apps/chrome/ntp/NewTabPageView;

    # getter for: Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mScrollView:Lcom/google/android/apps/chrome/ntp/NewTabScrollView;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->access$400(Lcom/google/android/apps/chrome/ntp/NewTabPageView;)Lcom/google/android/apps/chrome/ntp/NewTabScrollView;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView$9;->val$mSnapScrollRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/ntp/NewTabScrollView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 374
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView$9;->this$0:Lcom/google/android/apps/chrome/ntp/NewTabPageView;

    # getter for: Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mScrollView:Lcom/google/android/apps/chrome/ntp/NewTabScrollView;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->access$400(Lcom/google/android/apps/chrome/ntp/NewTabPageView;)Lcom/google/android/apps/chrome/ntp/NewTabScrollView;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView$9;->val$mSnapScrollRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1e

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/chrome/ntp/NewTabScrollView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 376
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView$9;->this$0:Lcom/google/android/apps/chrome/ntp/NewTabPageView;

    # invokes: Lcom/google/android/apps/chrome/ntp/NewTabPageView;->updateSearchBoxOnScroll()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->access$700(Lcom/google/android/apps/chrome/ntp/NewTabPageView;)V

    .line 377
    return-void
.end method
