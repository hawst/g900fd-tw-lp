.class Lcom/google/android/apps/chrome/ntp/NewTabPageView$6;
.super Landroid/text/style/ClickableSpan;
.source "NewTabPageView.java"


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/ntp/NewTabPageView;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/ntp/NewTabPageView;)V
    .locals 0

    .prologue
    .line 297
    iput-object p1, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView$6;->this$0:Lcom/google/android/apps/chrome/ntp/NewTabPageView;

    invoke-direct {p0}, Landroid/text/style/ClickableSpan;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 300
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView$6;->this$0:Lcom/google/android/apps/chrome/ntp/NewTabPageView;

    # getter for: Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mManager:Lcom/google/android/apps/chrome/ntp/NewTabPageView$NewTabPageManager;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->access$000(Lcom/google/android/apps/chrome/ntp/NewTabPageView;)Lcom/google/android/apps/chrome/ntp/NewTabPageView$NewTabPageManager;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/ntp/NewTabPageView$NewTabPageManager;->optOutPromoSettingsSelected()V

    .line 301
    return-void
.end method

.method public updateDrawState(Landroid/text/TextPaint;)V
    .locals 2

    .prologue
    .line 306
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView$6;->this$0:Lcom/google/android/apps/chrome/ntp/NewTabPageView;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$color;->active_control_color:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 308
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setUnderlineText(Z)V

    .line 309
    return-void
.end method
