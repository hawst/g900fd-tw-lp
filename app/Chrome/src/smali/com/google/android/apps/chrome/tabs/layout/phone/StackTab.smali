.class public Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;
.super Ljava/lang/Object;
.source "StackTab.java"

# interfaces
.implements Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;


# static fields
.field public static sStackBufferHeight:F

.field public static sStackBufferWidth:F

.field public static sStackedTabVisibleSize:F


# instance fields
.field private mAlpha:F

.field private mCacheStackVisibility:F

.field private mCachedIndexDistance:F

.field private mCachedVisibleArea:F

.field private mDiscardAmount:F

.field private mDiscardFromClick:Z

.field private mDiscardOriginX:F

.field private mDiscardOriginY:F

.field protected mDying:Z

.field private mIndex:I

.field private mLayoutTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

.field private mOrderSortingValue:I

.field private mScale:F

.field private mScrollOffset:F

.field private mVisiblitySortingValue:J

.field private mXInStackInfluence:F

.field private mXInStackOffset:F

.field private mXOutOfStack:F

.field private mYInStackInfluence:F

.field private mYInStackOffset:F

.field private mYOutOfStack:F


# direct methods
.method public constructor <init>(Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v0, 0x0

    .line 88
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mXInStackInfluence:F

    .line 46
    iput v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mYInStackInfluence:F

    .line 49
    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mScrollOffset:F

    .line 50
    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mXInStackOffset:F

    .line 51
    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mYInStackOffset:F

    .line 54
    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mXOutOfStack:F

    .line 55
    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mYOutOfStack:F

    .line 58
    iput v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mAlpha:F

    .line 59
    iput v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mScale:F

    .line 60
    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mDiscardAmount:F

    .line 71
    iput-boolean v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mDying:Z

    .line 77
    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mCachedVisibleArea:F

    .line 78
    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mCachedIndexDistance:F

    .line 79
    iput v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mCacheStackVisibility:F

    .line 80
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mVisiblitySortingValue:J

    .line 81
    iput v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mOrderSortingValue:I

    .line 89
    iput-object p1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mLayoutTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    .line 90
    return-void
.end method

.method private static computeOrderSortingValue(FF)I
    .locals 3

    .prologue
    .line 467
    const/high16 v0, 0x3f800000    # 1.0f

    add-float/2addr v0, p0

    const v1, 0x3dcccccd    # 0.1f

    const v2, 0x3f666666    # 0.9f

    mul-float/2addr v2, p1

    add-float/2addr v1, v2

    div-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method private static computeVisibilitySortingValue(FFF)J
    .locals 2

    .prologue
    .line 452
    mul-float v0, p0, p2

    sub-float/2addr v0, p1

    float-to-long v0, v0

    return-wide v0
.end method

.method public static resetDimensionConstants(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 395
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 396
    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->density:F

    div-float/2addr v1, v2

    .line 397
    sget v2, Lcom/google/android/apps/chrome/R$dimen;->stacked_tab_visible_size:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v2, v1

    sput v2, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->sStackedTabVisibleSize:F

    .line 399
    sget v2, Lcom/google/android/apps/chrome/R$dimen;->stack_buffer_width:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v2, v1

    sput v2, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->sStackBufferWidth:F

    .line 400
    sget v2, Lcom/google/android/apps/chrome/R$dimen;->stack_buffer_height:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, v1

    sput v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->sStackBufferHeight:F

    .line 401
    return-void
.end method

.method public static screenToScroll(FF)F
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 373
    cmpg-float v1, p0, v0

    if-gtz v1, :cond_0

    .line 375
    :goto_0
    return v0

    .line 374
    :cond_0
    cmpl-float v0, p0, p1

    if-ltz v0, :cond_1

    add-float v0, p0, p1

    goto :goto_0

    .line 375
    :cond_1
    mul-float v0, p0, p1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v0, v0

    const/high16 v1, 0x40000000    # 2.0f

    mul-float/2addr v0, v1

    goto :goto_0
.end method

.method public static scrollToScreen(FF)F
    .locals 3

    .prologue
    const/high16 v2, 0x40000000    # 2.0f

    const/4 v0, 0x0

    .line 355
    cmpg-float v1, p0, v0

    if-gtz v1, :cond_0

    .line 358
    :goto_0
    return v0

    .line 356
    :cond_0
    mul-float v0, p1, v2

    cmpl-float v0, p0, v0

    if-ltz v0, :cond_1

    sub-float v0, p0, p1

    goto :goto_0

    .line 357
    :cond_1
    sub-float v0, p0, p1

    mul-float v1, p1, v2

    div-float/2addr v0, v1

    const/high16 v1, 0x3f000000    # 0.5f

    add-float/2addr v0, v1

    .line 358
    mul-float/2addr v0, v0

    mul-float/2addr v0, p1

    goto :goto_0
.end method


# virtual methods
.method public addToDiscardAmount(F)V
    .locals 1

    .prologue
    .line 271
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mDiscardAmount:F

    add-float/2addr v0, p1

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mDiscardAmount:F

    .line 272
    return-void
.end method

.method public getAlpha()F
    .locals 1

    .prologue
    .line 198
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mAlpha:F

    return v0
.end method

.method public getDiscardAmount()F
    .locals 1

    .prologue
    .line 279
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mDiscardAmount:F

    return v0
.end method

.method public getDiscardFromClick()Z
    .locals 1

    .prologue
    .line 321
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mDiscardFromClick:Z

    return v0
.end method

.method public getDiscardOriginX()F
    .locals 1

    .prologue
    .line 300
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mDiscardOriginX:F

    return v0
.end method

.method public getDiscardOriginY()F
    .locals 1

    .prologue
    .line 307
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mDiscardOriginY:F

    return v0
.end method

.method public getId()I
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mLayoutTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getId()I

    move-result v0

    return v0
.end method

.method public getIndex()I
    .locals 1

    .prologue
    .line 103
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mIndex:I

    return v0
.end method

.method public getLayoutTab()Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mLayoutTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    return-object v0
.end method

.method public getOrderSortingValue()I
    .locals 1

    .prologue
    .line 475
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mOrderSortingValue:I

    return v0
.end method

.method public getScale()F
    .locals 1

    .prologue
    .line 242
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mScale:F

    return v0
.end method

.method public getScrollOffset()F
    .locals 1

    .prologue
    .line 256
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mScrollOffset:F

    return v0
.end method

.method public getSizeInScrollDirection(I)F
    .locals 1

    .prologue
    .line 383
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 384
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mLayoutTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getScaledContentHeight()F

    move-result v0

    .line 386
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mLayoutTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getScaledContentWidth()F

    move-result v0

    goto :goto_0
.end method

.method public getVisiblitySortingValue()J
    .locals 2

    .prologue
    .line 459
    iget-wide v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mVisiblitySortingValue:J

    return-wide v0
.end method

.method public getXInStackInfluence()F
    .locals 1

    .prologue
    .line 213
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mXInStackInfluence:F

    return v0
.end method

.method public getXInStackOffset()F
    .locals 1

    .prologue
    .line 152
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mXInStackOffset:F

    return v0
.end method

.method public getXOutOfStack()F
    .locals 1

    .prologue
    .line 180
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mXOutOfStack:F

    return v0
.end method

.method public getYInStackInfluence()F
    .locals 1

    .prologue
    .line 228
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mYInStackInfluence:F

    return v0
.end method

.method public getYInStackOffset()F
    .locals 1

    .prologue
    .line 138
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mYInStackOffset:F

    return v0
.end method

.method public getYOutOfStack()F
    .locals 1

    .prologue
    .line 166
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mYOutOfStack:F

    return v0
.end method

.method public isDying()Z
    .locals 1

    .prologue
    .line 337
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mDying:Z

    return v0
.end method

.method public resetOffset()V
    .locals 2

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v0, 0x0

    .line 407
    iput v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mXInStackInfluence:F

    .line 408
    iput v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mYInStackInfluence:F

    .line 409
    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mScrollOffset:F

    .line 410
    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mXInStackOffset:F

    .line 411
    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mYInStackOffset:F

    .line 412
    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mXOutOfStack:F

    .line 413
    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mYOutOfStack:F

    .line 414
    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mDiscardOriginX:F

    .line 415
    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mDiscardOriginY:F

    .line 416
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mDiscardFromClick:Z

    .line 417
    return-void
.end method

.method public setAlpha(F)V
    .locals 0

    .prologue
    .line 191
    iput p1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mAlpha:F

    .line 192
    return-void
.end method

.method public setDiscardAmount(F)V
    .locals 0

    .prologue
    .line 264
    iput p1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mDiscardAmount:F

    .line 265
    return-void
.end method

.method public setDiscardFromClick(Z)V
    .locals 0

    .prologue
    .line 314
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mDiscardFromClick:Z

    .line 315
    return-void
.end method

.method public setDiscardOriginX(F)V
    .locals 0

    .prologue
    .line 286
    iput p1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mDiscardOriginX:F

    .line 287
    return-void
.end method

.method public setDiscardOriginY(F)V
    .locals 0

    .prologue
    .line 293
    iput p1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mDiscardOriginY:F

    .line 294
    return-void
.end method

.method public setDying(Z)V
    .locals 0

    .prologue
    .line 329
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mDying:Z

    .line 330
    return-void
.end method

.method public setLayoutTab(Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;)V
    .locals 0

    .prologue
    .line 117
    iput-object p1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mLayoutTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    .line 118
    return-void
.end method

.method public setNewIndex(I)V
    .locals 0

    .prologue
    .line 96
    iput p1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mIndex:I

    .line 97
    return-void
.end method

.method public setProperty(Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab$Property;F)V
    .locals 2

    .prologue
    .line 486
    sget-object v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab$1;->$SwitchMap$com$google$android$apps$chrome$tabs$layout$phone$StackTab$Property:[I

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab$Property;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 518
    :goto_0
    return-void

    .line 488
    :pswitch_0
    invoke-virtual {p0, p2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->setScale(F)V

    goto :goto_0

    .line 491
    :pswitch_1
    invoke-virtual {p0, p2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->setScrollOffset(F)V

    goto :goto_0

    .line 494
    :pswitch_2
    invoke-virtual {p0, p2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->setAlpha(F)V

    goto :goto_0

    .line 497
    :pswitch_3
    invoke-virtual {p0, p2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->setXInStackInfluence(F)V

    goto :goto_0

    .line 500
    :pswitch_4
    invoke-virtual {p0, p2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->setXInStackOffset(F)V

    goto :goto_0

    .line 503
    :pswitch_5
    invoke-virtual {p0, p2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->setXOutOfStack(F)V

    goto :goto_0

    .line 506
    :pswitch_6
    invoke-virtual {p0, p2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->setYInStackInfluence(F)V

    goto :goto_0

    .line 509
    :pswitch_7
    invoke-virtual {p0, p2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->setYInStackOffset(F)V

    goto :goto_0

    .line 512
    :pswitch_8
    invoke-virtual {p0, p2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->setYOutOfStack(F)V

    goto :goto_0

    .line 515
    :pswitch_9
    invoke-virtual {p0, p2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->setDiscardAmount(F)V

    goto :goto_0

    .line 486
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method public bridge synthetic setProperty(Ljava/lang/Enum;F)V
    .locals 0

    .prologue
    .line 21
    check-cast p1, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab$Property;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->setProperty(Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab$Property;F)V

    return-void
.end method

.method public setScale(F)V
    .locals 0

    .prologue
    .line 235
    iput p1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mScale:F

    .line 236
    return-void
.end method

.method public setScrollOffset(F)V
    .locals 0

    .prologue
    .line 249
    iput p1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mScrollOffset:F

    .line 250
    return-void
.end method

.method public setXInStackInfluence(F)V
    .locals 0

    .prologue
    .line 206
    iput p1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mXInStackInfluence:F

    .line 207
    return-void
.end method

.method public setXInStackOffset(F)V
    .locals 0

    .prologue
    .line 145
    iput p1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mXInStackOffset:F

    .line 146
    return-void
.end method

.method public setXOutOfStack(F)V
    .locals 0

    .prologue
    .line 173
    iput p1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mXOutOfStack:F

    .line 174
    return-void
.end method

.method public setYInStackInfluence(F)V
    .locals 0

    .prologue
    .line 221
    iput p1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mYInStackInfluence:F

    .line 222
    return-void
.end method

.method public setYInStackOffset(F)V
    .locals 0

    .prologue
    .line 131
    iput p1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mYInStackOffset:F

    .line 132
    return-void
.end method

.method public setYOutOfStack(F)V
    .locals 0

    .prologue
    .line 159
    iput p1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mYOutOfStack:F

    .line 160
    return-void
.end method

.method public updateStackVisiblityValue(F)V
    .locals 3

    .prologue
    .line 436
    iput p1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mCacheStackVisibility:F

    .line 437
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mCachedIndexDistance:F

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mCacheStackVisibility:F

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->computeOrderSortingValue(FF)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mOrderSortingValue:I

    .line 438
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mCachedVisibleArea:F

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mOrderSortingValue:I

    int-to-float v1, v1

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mCacheStackVisibility:F

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->computeVisibilitySortingValue(FFF)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mVisiblitySortingValue:J

    .line 440
    return-void
.end method

.method public updateVisiblityValue(I)V
    .locals 3

    .prologue
    .line 424
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mLayoutTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->computeVisibleArea()F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mCachedVisibleArea:F

    .line 425
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mIndex:I

    sub-int/2addr v0, p1

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mCachedIndexDistance:F

    .line 426
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mCachedIndexDistance:F

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mCacheStackVisibility:F

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->computeOrderSortingValue(FF)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mOrderSortingValue:I

    .line 427
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mCachedVisibleArea:F

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mOrderSortingValue:I

    int-to-float v1, v1

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mCacheStackVisibility:F

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->computeVisibilitySortingValue(FFF)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->mVisiblitySortingValue:J

    .line 429
    return-void
.end method
