.class Lcom/google/android/apps/chrome/document/DocumentActivity$1;
.super Lcom/google/android/apps/chrome/tabmodel/SingleTabModelSelector;
.source "DocumentActivity.java"


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/document/DocumentActivity;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/document/DocumentActivity;Landroid/app/Activity;ZZ)V
    .locals 0

    .prologue
    .line 198
    iput-object p1, p0, Lcom/google/android/apps/chrome/document/DocumentActivity$1;->this$0:Lcom/google/android/apps/chrome/document/DocumentActivity;

    invoke-direct {p0, p2, p3, p4}, Lcom/google/android/apps/chrome/tabmodel/SingleTabModelSelector;-><init>(Landroid/app/Activity;ZZ)V

    return-void
.end method


# virtual methods
.method public openNewTab(Lorg/chromium/content_public/browser/LoadUrlParams;Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;Lorg/chromium/chrome/browser/Tab;Z)Lorg/chromium/chrome/browser/Tab;
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v2, 0x0

    .line 202
    .line 203
    invoke-virtual {p1}, Lorg/chromium/content_public/browser/LoadUrlParams;->getPostData()[B

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lorg/chromium/content_public/browser/LoadUrlParams;->getVerbatimHeaders()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lorg/chromium/content_public/browser/LoadUrlParams;->getReferrer()Lorg/chromium/content_public/common/Referrer;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 206
    :cond_0
    new-instance v7, Lcom/google/android/apps/chrome/document/DocumentActivityDatabase$PendingDocumentData;

    invoke-direct {v7}, Lcom/google/android/apps/chrome/document/DocumentActivityDatabase$PendingDocumentData;-><init>()V

    .line 207
    invoke-virtual {p1}, Lorg/chromium/content_public/browser/LoadUrlParams;->getPostData()[B

    move-result-object v0

    iput-object v0, v7, Lcom/google/android/apps/chrome/document/DocumentActivityDatabase$PendingDocumentData;->postData:[B

    .line 208
    invoke-virtual {p1}, Lorg/chromium/content_public/browser/LoadUrlParams;->getVerbatimHeaders()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v7, Lcom/google/android/apps/chrome/document/DocumentActivityDatabase$PendingDocumentData;->extraHeaders:Ljava/lang/String;

    .line 209
    invoke-virtual {p1}, Lorg/chromium/content_public/browser/LoadUrlParams;->getReferrer()Lorg/chromium/content_public/common/Referrer;

    move-result-object v0

    iput-object v0, v7, Lcom/google/android/apps/chrome/document/DocumentActivityDatabase$PendingDocumentData;->referrer:Lorg/chromium/content_public/common/Referrer;

    .line 212
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity$1;->this$0:Lcom/google/android/apps/chrome/document/DocumentActivity;

    invoke-virtual {p1}, Lorg/chromium/content_public/browser/LoadUrlParams;->getUrl()Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0xc8

    invoke-virtual {p1}, Lorg/chromium/content_public/browser/LoadUrlParams;->getTransitionType()I

    move-result v5

    move v1, p4

    move v6, v2

    invoke-static/range {v0 .. v7}, Lcom/google/android/apps/chrome/document/ChromeLauncherActivity;->launchInstance(Landroid/app/Activity;ZILjava/lang/String;IIZLcom/google/android/apps/chrome/document/DocumentActivityDatabase$PendingDocumentData;)V

    .line 216
    return-object v8

    :cond_1
    move-object v7, v8

    goto :goto_0
.end method
