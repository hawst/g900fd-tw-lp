.class Lcom/google/android/apps/chrome/ChromeMobileApplication$1;
.super Ljava/lang/Object;
.source "ChromeMobileApplication.java"

# interfaces
.implements Lorg/chromium/ui/UiUtils$KeyboardShowingDelegate;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/ChromeMobileApplication;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/ChromeMobileApplication;)V
    .locals 0

    .prologue
    .line 211
    iput-object p1, p0, Lcom/google/android/apps/chrome/ChromeMobileApplication$1;->this$0:Lcom/google/android/apps/chrome/ChromeMobileApplication;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public disableKeyboardCheck(Landroid/content/Context;Landroid/view/View;)Z
    .locals 2

    .prologue
    .line 214
    const/4 v0, 0x0

    .line 215
    instance-of v1, p1, Landroid/app/Activity;

    if-eqz v1, :cond_0

    .line 216
    check-cast p1, Landroid/app/Activity;

    .line 222
    :goto_0
    if-eqz p1, :cond_1

    invoke-static {p1}, Lcom/google/android/apps/chrome/third_party/samsung/MultiWindowUtils;->isMultiWindow(Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    return v0

    .line 217
    :cond_0
    if-eqz p2, :cond_2

    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    instance-of v1, v1, Landroid/app/Activity;

    if-eqz v1, :cond_2

    .line 218
    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    move-object p1, v0

    goto :goto_0

    .line 222
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    move-object p1, v0

    goto :goto_0
.end method
