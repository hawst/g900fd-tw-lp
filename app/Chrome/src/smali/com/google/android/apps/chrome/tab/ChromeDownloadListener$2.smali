.class Lcom/google/android/apps/chrome/tab/ChromeDownloadListener$2;
.super Landroid/os/AsyncTask;
.source "ChromeDownloadListener.java"


# instance fields
.field mDownloadFailed:Z

.field mDownloadId:J

.field final synthetic this$0:Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;

.field final synthetic val$manager:Landroid/app/DownloadManager;

.field final synthetic val$request:Landroid/app/DownloadManager$Request;

.field final synthetic val$shouldOpenAfterDownload:Z


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;Landroid/app/DownloadManager;Landroid/app/DownloadManager$Request;Z)V
    .locals 1

    .prologue
    .line 500
    iput-object p1, p0, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener$2;->this$0:Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;

    iput-object p2, p0, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener$2;->val$manager:Landroid/app/DownloadManager;

    iput-object p3, p0, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener$2;->val$request:Landroid/app/DownloadManager$Request;

    iput-boolean p4, p0, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener$2;->val$shouldOpenAfterDownload:Z

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 501
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener$2;->mDownloadFailed:Z

    return-void
.end method


# virtual methods
.method public bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 500
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener$2;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method public varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 4

    .prologue
    .line 506
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener$2;->val$manager:Landroid/app/DownloadManager;

    iget-object v1, p0, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener$2;->val$request:Landroid/app/DownloadManager$Request;

    invoke-virtual {v0, v1}, Landroid/app/DownloadManager;->enqueue(Landroid/app/DownloadManager$Request;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener$2;->mDownloadId:J
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 512
    :goto_0
    const/4 v0, 0x0

    return-object v0

    .line 507
    :catch_0
    move-exception v0

    .line 509
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener$2;->mDownloadFailed:Z

    .line 510
    const-string/jumbo v1, "ChromeDownloadListener"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Download failed: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 500
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener$2;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 4

    .prologue
    .line 517
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener$2;->mDownloadFailed:Z

    if-eqz v0, :cond_1

    .line 518
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener$2;->this$0:Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;

    # getter for: Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;->access$000(Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;)Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$string;->cannot_download_generic:I

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 528
    :cond_0
    :goto_0
    return-void

    .line 520
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener$2;->val$shouldOpenAfterDownload:Z

    if-eqz v0, :cond_0

    .line 521
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener$2;->this$0:Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;

    # getter for: Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;->mPendingAutoOpenDownloads:Ljava/util/HashSet;
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;->access$100(Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;)Ljava/util/HashSet;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/HashSet;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 522
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener$2;->this$0:Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;

    # getter for: Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;->access$000(Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener$2;->this$0:Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;

    new-instance v2, Landroid/content/IntentFilter;

    const-string/jumbo v3, "android.intent.action.DOWNLOAD_COMPLETE"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 526
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener$2;->this$0:Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;

    # getter for: Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;->mPendingAutoOpenDownloads:Ljava/util/HashSet;
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;->access$100(Lcom/google/android/apps/chrome/tab/ChromeDownloadListener;)Ljava/util/HashSet;

    move-result-object v0

    iget-wide v2, p0, Lcom/google/android/apps/chrome/tab/ChromeDownloadListener$2;->mDownloadId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method
