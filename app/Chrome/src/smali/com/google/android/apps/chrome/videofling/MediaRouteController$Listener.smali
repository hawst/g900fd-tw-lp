.class public interface abstract Lcom/google/android/apps/chrome/videofling/MediaRouteController$Listener;
.super Ljava/lang/Object;
.source "MediaRouteController.java"


# virtual methods
.method public abstract onDurationUpdated(I)V
.end method

.method public abstract onError(ILjava/lang/String;)V
.end method

.method public abstract onPlaybackStateChanged(Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;Lorg/chromium/chrome/browser/Tab;I)V
.end method

.method public abstract onPositionChanged(I)V
.end method

.method public abstract onPrepared(Lcom/google/android/apps/chrome/videofling/MediaRouteController;)V
.end method

.method public abstract onRouteSelected(Ljava/lang/String;Lorg/chromium/chrome/browser/Tab;ILcom/google/android/apps/chrome/videofling/MediaRouteController;)V
.end method

.method public abstract onRouteUnselected(Lorg/chromium/chrome/browser/Tab;ILcom/google/android/apps/chrome/videofling/MediaRouteController;)V
.end method

.method public abstract onSeekCompleted()V
.end method

.method public abstract onTitleChanged(Ljava/lang/String;)V
.end method
