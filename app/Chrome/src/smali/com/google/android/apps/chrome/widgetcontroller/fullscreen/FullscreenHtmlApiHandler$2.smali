.class Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler$2;
.super Ljava/lang/Object;
.source "FullscreenHtmlApiHandler.java"

# interfaces
.implements Landroid/view/View$OnLayoutChangeListener;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;

.field final synthetic val$contentView:Landroid/view/View;

.field final synthetic val$contentViewCore:Lorg/chromium/content/browser/ContentViewCore;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;Lorg/chromium/content/browser/ContentViewCore;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 246
    iput-object p1, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler$2;->this$0:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;

    iput-object p2, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler$2;->val$contentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    iput-object p3, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler$2;->val$contentView:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLayoutChange(Landroid/view/View;IIIIIIII)V
    .locals 2

    .prologue
    .line 250
    sub-int v0, p5, p3

    sub-int v1, p9, p7

    if-ge v0, v1, :cond_0

    .line 251
    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler$2;->this$0:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;

    # getter for: Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;->mDelegate:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler$FullscreenHtmlApiDelegate;
    invoke-static {v0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;->access$200(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;)Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler$FullscreenHtmlApiDelegate;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler$2;->val$contentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    invoke-interface {v0, v1}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler$FullscreenHtmlApiDelegate;->onFullscreenExited(Lorg/chromium/content/browser/ContentViewCore;)V

    .line 252
    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler$2;->val$contentView:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->removeOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 254
    :cond_0
    return-void
.end method
