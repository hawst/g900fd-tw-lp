.class final Lcom/google/android/apps/chrome/playlog/PlayLogTracker$1;
.super Ljava/lang/Object;
.source "PlayLogTracker.java"

# interfaces
.implements Lcom/google/android/gms/b/b;


# instance fields
.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 36
    iput-object p1, p0, Lcom/google/android/apps/chrome/playlog/PlayLogTracker$1;->val$context:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onLoggerConnected()V
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 50
    # getter for: Lcom/google/android/apps/chrome/playlog/PlayLogTracker;->sPlayLogger:Lcom/google/android/gms/b/a;
    invoke-static {}, Lcom/google/android/apps/chrome/playlog/PlayLogTracker;->access$000()Lcom/google/android/gms/b/a;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 51
    # getter for: Lcom/google/android/apps/chrome/playlog/PlayLogTracker;->sPlayLogger:Lcom/google/android/gms/b/a;
    invoke-static {}, Lcom/google/android/apps/chrome/playlog/PlayLogTracker;->access$000()Lcom/google/android/gms/b/a;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/apps/chrome/playlog/PlayLogTracker$1;->val$context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    move-object v6, v5

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/gms/b/a;->a(JLjava/lang/String;[B[Ljava/lang/String;)V

    .line 53
    :cond_0
    return-void
.end method

.method public final onLoggerFailedConnection()V
    .locals 2

    .prologue
    .line 45
    const-string/jumbo v0, "PlayLogTracker"

    const-string/jumbo v1, "onLoggerFailedConnection"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 46
    return-void
.end method

.method public final onLoggerFailedConnectionWithResolution(Landroid/app/PendingIntent;)V
    .locals 3

    .prologue
    .line 39
    const-string/jumbo v0, "PlayLogTracker"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "onLoggerFailedConnectionWithResolution intent="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/app/PendingIntent;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 41
    return-void
.end method
