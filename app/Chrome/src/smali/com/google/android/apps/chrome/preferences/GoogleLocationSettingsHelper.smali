.class public final Lcom/google/android/apps/chrome/preferences/GoogleLocationSettingsHelper;
.super Ljava/lang/Object;
.source "GoogleLocationSettingsHelper.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static isGoogleAppsLocationSettingEnabled(Landroid/content/Context;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 38
    invoke-static {p0}, Lcom/google/android/apps/chrome/preferences/GoogleLocationSettingsUtil;->isGoogleLocationSettingsAvailable(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 39
    invoke-static {p0}, Lcom/google/android/apps/chrome/preferences/GoogleLocationSettingsUtil;->getUseLocationForServices(Landroid/content/Context;)I

    move-result v1

    if-ne v1, v0, :cond_1

    .line 42
    :cond_0
    :goto_0
    return v0

    .line 39
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isMasterLocationSettingEnabled(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 26
    invoke-static {p0}, Lcom/google/android/apps/chrome/preferences/GoogleLocationSettingsUtil;->useInternalLocationSetting(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 27
    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->isAllowLocationEnabled()Z

    move-result v0

    .line 29
    :goto_0
    return v0

    :cond_0
    invoke-static {p0}, Lcom/google/android/apps/chrome/preferences/GoogleLocationSettingsUtil;->isSystemLocationProviderEnabled(Landroid/content/Context;)Z

    move-result v0

    goto :goto_0
.end method

.method public static isSystemLocationSettingEnabled(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 17
    invoke-static {p0}, Lcom/google/android/apps/chrome/preferences/GoogleLocationSettingsHelper;->isMasterLocationSettingEnabled(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/google/android/apps/chrome/preferences/GoogleLocationSettingsHelper;->isGoogleAppsLocationSettingEnabled(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
