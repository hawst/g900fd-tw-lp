.class abstract Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$CallChain;
.super Ljava/lang/Object;
.source "WebsitePreferences.java"


# instance fields
.field private mNext:Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$CallChain;

.field final synthetic this$0:Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences;)V
    .locals 0

    .prologue
    .line 41
    iput-object p1, p0, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$CallChain;->this$0:Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences;Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$1;)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$CallChain;-><init>(Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences;)V

    return-void
.end method


# virtual methods
.method chain(Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$CallChain;)Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$CallChain;
    .locals 0

    .prologue
    .line 45
    iput-object p1, p0, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$CallChain;->mNext:Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$CallChain;

    .line 46
    return-object p1
.end method

.method protected next()V
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$CallChain;->mNext:Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$CallChain;

    if-eqz v0, :cond_0

    .line 53
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$CallChain;->mNext:Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$CallChain;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$CallChain;->run()V

    .line 54
    :cond_0
    return-void
.end method

.method abstract run()V
.end method
