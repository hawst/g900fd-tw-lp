.class public Lcom/google/android/apps/chrome/snapshot/HeaderField;
.super Ljava/lang/Object;
.source "HeaderField.java"


# instance fields
.field private final mName:Ljava/lang/String;

.field private final mValue:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/google/android/apps/chrome/snapshot/HeaderField;->mName:Ljava/lang/String;

    .line 17
    iput-object p2, p0, Lcom/google/android/apps/chrome/snapshot/HeaderField;->mValue:Ljava/lang/String;

    .line 18
    return-void
.end method

.method public static create(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/HeaderField;
    .locals 1

    .prologue
    .line 21
    new-instance v0, Lcom/google/android/apps/chrome/snapshot/HeaderField;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/chrome/snapshot/HeaderField;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/HeaderField;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/HeaderField;->mValue:Ljava/lang/String;

    return-object v0
.end method
