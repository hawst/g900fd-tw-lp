.class public Lcom/google/android/apps/chrome/document/DocumentSuggestionProvider;
.super Landroid/content/ContentProvider;
.source "DocumentSuggestionProvider.java"


# static fields
.field public static final APPLICATION_ID:Ljava/lang/String; = "chrome"

.field private static final COLUMNS:[Ljava/lang/String;


# instance fields
.field private mController:Lorg/chromium/chrome/browser/omnibox/AutocompleteController;

.field private mInitialized:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 48
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "suggestions"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/chrome/document/DocumentSuggestionProvider;->COLUMNS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    .line 55
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/document/DocumentSuggestionProvider;->mInitialized:Z

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/document/DocumentSuggestionProvider;)Lorg/chromium/chrome/browser/omnibox/AutocompleteController;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentSuggestionProvider;->mController:Lorg/chromium/chrome/browser/omnibox/AutocompleteController;

    return-object v0
.end method

.method static synthetic access$102(Lcom/google/android/apps/chrome/document/DocumentSuggestionProvider;Z)Z
    .locals 0

    .prologue
    .line 39
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/document/DocumentSuggestionProvider;->mInitialized:Z

    return p1
.end method

.method private getSuggestion(Ljava/lang/String;)Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;
    .locals 3

    .prologue
    .line 138
    :try_start_0
    new-instance v0, Lcom/google/android/apps/chrome/document/DocumentSuggestionProvider$3;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/chrome/document/DocumentSuggestionProvider$3;-><init>(Lcom/google/android/apps/chrome/document/DocumentSuggestionProvider;Ljava/lang/String;)V

    invoke-static {v0}, Lorg/chromium/base/ThreadUtils;->runOnUiThreadBlocking(Ljava/util/concurrent/Callable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    .line 146
    :goto_0
    return-object v0

    .line 144
    :catch_0
    move-exception v0

    .line 145
    const-string/jumbo v1, "DocumentSuggestionProvider"

    const-string/jumbo v2, "Exception retrieving suggestions"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 146
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isValidCaller()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 155
    invoke-static {}, Lcom/google/android/apps/chrome/utilities/ChromeBuildInfo;->isLocalBuild()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 157
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentSuggestionProvider;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v0}, Lcom/google/android/apps/chrome/externalauth/ExternalAuthUtils;->checkCallerIsValid(Landroid/content/Context;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 158
    const-string/jumbo v0, "DocumentSuggestionProvider"

    const-string/jumbo v2, "Invalid caller for DocumentSuggestionProvider"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 161
    :goto_1
    return v1

    :cond_0
    move v0, v2

    .line 155
    goto :goto_0

    :cond_1
    move v1, v2

    .line 161
    goto :goto_1
.end method


# virtual methods
.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 166
    const/4 v0, 0x0

    return v0
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/google/android/apps/chrome/document/DocumentSuggestionProvider;->isValidCaller()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 60
    :goto_0
    return-object v0

    :cond_0
    const-string/jumbo v0, "vnd.android.cursor.item/vnd.google.chrome.suggestion"

    goto :goto_0
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 171
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()Z
    .locals 2

    .prologue
    .line 65
    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    .line 66
    new-instance v0, Lorg/chromium/chrome/browser/omnibox/AutocompleteController;

    new-instance v1, Lcom/google/android/apps/chrome/document/DocumentSuggestionProvider$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/document/DocumentSuggestionProvider$1;-><init>(Lcom/google/android/apps/chrome/document/DocumentSuggestionProvider;)V

    invoke-direct {v0, v1}, Lorg/chromium/chrome/browser/omnibox/AutocompleteController;-><init>(Lorg/chromium/chrome/browser/omnibox/AutocompleteController$OnSuggestionsReceivedListener;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentSuggestionProvider;->mController:Lorg/chromium/chrome/browser/omnibox/AutocompleteController;

    .line 73
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentSuggestionProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/content/browser/BrowserStartupController;->get(Landroid/content/Context;)Lorg/chromium/content/browser/BrowserStartupController;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/chrome/document/DocumentSuggestionProvider$2;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/document/DocumentSuggestionProvider$2;-><init>(Lcom/google/android/apps/chrome/document/DocumentSuggestionProvider;)V

    invoke-virtual {v0, v1}, Lorg/chromium/content/browser/BrowserStartupController;->addStartupCompletedObserver(Lorg/chromium/content/browser/BrowserStartupController$StartupCallback;)V

    .line 86
    const/4 v0, 0x1

    return v0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 92
    invoke-direct {p0}, Lcom/google/android/apps/chrome/document/DocumentSuggestionProvider;->isValidCaller()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/apps/chrome/document/DocumentSuggestionProvider;->mInitialized:Z

    if-nez v1, :cond_1

    .line 129
    :cond_0
    :goto_0
    return-object v0

    .line 93
    :cond_1
    invoke-direct {p0, p3}, Lcom/google/android/apps/chrome/document/DocumentSuggestionProvider;->getSuggestion(Ljava/lang/String;)Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;

    move-result-object v1

    .line 94
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;->getType()Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

    move-result-object v2

    sget-object v3, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;->SEARCH_WHAT_YOU_TYPED:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

    if-eq v2, v3, :cond_0

    invoke-virtual {v1}, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;->getType()Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

    move-result-object v2

    sget-object v3, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;->URL_WHAT_YOU_TYPED:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

    if-eq v2, v3, :cond_0

    invoke-virtual {v1}, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;->isUrlSuggestion()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;->getRelevance()I

    move-result v2

    const/16 v3, 0x514

    if-lt v2, v3, :cond_0

    .line 104
    new-instance v0, Lcom/google/android/c/j;

    invoke-direct {v0}, Lcom/google/android/c/j;-><init>()V

    .line 105
    invoke-virtual {v1}, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;->getRelevance()I

    move-result v2

    int-to-long v2, v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, v0, Lcom/google/android/c/j;->a:Ljava/lang/Long;

    .line 106
    invoke-virtual {v1}, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;->getUrl()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/google/android/c/j;->b:Ljava/lang/String;

    .line 107
    invoke-virtual {v1}, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;->getDisplayText()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/google/android/c/j;->c:Ljava/lang/String;

    .line 108
    invoke-virtual {v1}, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;->getFormattedUrl()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/google/android/c/j;->d:Ljava/lang/String;

    .line 109
    new-instance v2, Landroid/content/Intent;

    const-string/jumbo v3, "android.intent.action.VIEW"

    invoke-virtual {v1}, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-direct {v2, v3, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 113
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentSuggestionProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    const-class v3, Lcom/google/android/apps/chrome/document/ChromeLauncherActivity;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    .line 114
    const-string/jumbo v1, "com.android.chrome.preserve_task"

    invoke-virtual {v2, v1, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 115
    const-string/jumbo v1, "com.android.chrome.started_by"

    const/16 v3, 0x12d

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 117
    invoke-virtual {v2, v4}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/c/j;->e:Ljava/lang/String;

    .line 119
    new-instance v1, Lcom/google/android/c/i;

    invoke-direct {v1}, Lcom/google/android/c/i;-><init>()V

    .line 120
    const-wide/16 v2, 0x1

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/c/i;->a:Ljava/lang/Long;

    .line 121
    new-array v2, v5, [Lcom/google/android/c/j;

    aput-object v0, v2, v4

    iput-object v2, v1, Lcom/google/android/c/i;->b:[Lcom/google/android/c/j;

    .line 123
    new-instance v2, Lcom/google/android/c/k;

    invoke-direct {v2}, Lcom/google/android/c/k;-><init>()V

    .line 124
    const-string/jumbo v0, "chrome"

    iput-object v0, v2, Lcom/google/android/c/k;->b:Ljava/lang/String;

    .line 125
    new-array v0, v5, [Lcom/google/android/c/i;

    aput-object v1, v0, v4

    iput-object v0, v2, Lcom/google/android/c/k;->c:[Lcom/google/android/c/i;

    .line 127
    new-instance v0, Landroid/database/MatrixCursor;

    sget-object v1, Lcom/google/android/apps/chrome/document/DocumentSuggestionProvider;->COLUMNS:[Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 128
    invoke-virtual {v0}, Landroid/database/MatrixCursor;->newRow()Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v1

    invoke-static {v2}, Lcom/google/protobuf/nano/MessageNano;->toByteArray(Lcom/google/protobuf/nano/MessageNano;)[B

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    goto/16 :goto_0
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 176
    const/4 v0, 0x0

    return v0
.end method
