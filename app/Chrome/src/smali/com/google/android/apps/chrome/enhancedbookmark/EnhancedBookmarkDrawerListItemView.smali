.class Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListItemView;
.super Landroid/widget/CheckedTextView;
.source "EnhancedBookmarkDrawerListItemView.java"


# static fields
.field static final synthetic $assertionsDisabled:Z

.field static final ICON_TYPE_FILTER:I = 0x4

.field static final ICON_TYPE_FOLDER:I = 0x3

.field static final ICON_TYPE_NONE:I = 0x0

.field static final ICON_TYPE_STAR:I = 0x1

.field static final ICON_TYPE_UNCATEGORIZED:I = 0x2


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    const-class v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListItemView;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListItemView;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Landroid/widget/CheckedTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 24
    return-void
.end method


# virtual methods
.method setIcon(I)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 28
    packed-switch p1, :pswitch_data_0

    .line 45
    sget-boolean v1, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListItemView;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 33
    :pswitch_0
    sget v0, Lcom/google/android/apps/chrome/R$drawable;->eb_navigation_item_star:I

    .line 48
    :cond_0
    :goto_0
    :pswitch_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListItemView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-static {p0, v0, v2, v2, v2}, Lorg/chromium/base/ApiCompatibilityUtils;->setCompoundDrawablesRelativeWithIntrinsicBounds(Landroid/widget/TextView;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 50
    return-void

    .line 36
    :pswitch_2
    sget v0, Lcom/google/android/apps/chrome/R$drawable;->eb_navigation_item_uncategorized:I

    goto :goto_0

    .line 39
    :pswitch_3
    sget v0, Lcom/google/android/apps/chrome/R$drawable;->eb_navigation_item_folder:I

    goto :goto_0

    .line 42
    :pswitch_4
    sget v0, Lcom/google/android/apps/chrome/R$drawable;->eb_navigation_item_filter:I

    goto :goto_0

    .line 28
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
