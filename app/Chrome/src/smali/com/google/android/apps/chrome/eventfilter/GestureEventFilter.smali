.class public Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;
.super Lcom/google/android/apps/chrome/eventfilter/EventFilter;
.source "GestureEventFilter.java"


# instance fields
.field private final mDetector:Landroid/view/GestureDetector;

.field private final mHandler:Lcom/google/android/apps/chrome/eventfilter/GestureHandler;

.field private mLongPressHandler:Landroid/os/Handler;

.field private mLongPressRunnable:Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter$LongPressRunnable;

.field private final mLongPressTimeoutMs:I

.field private final mScaledTouchSlop:I

.field private mSeenFirstScrollEvent:Z

.field private mSingleInput:Z

.field private final mUseDefaultLongPress:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/chrome/eventfilter/EventFilterHost;Lcom/google/android/apps/chrome/eventfilter/GestureHandler;)V
    .locals 1

    .prologue
    .line 65
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;-><init>(Landroid/content/Context;Lcom/google/android/apps/chrome/eventfilter/EventFilterHost;Lcom/google/android/apps/chrome/eventfilter/GestureHandler;Z)V

    .line 66
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/chrome/eventfilter/EventFilterHost;Lcom/google/android/apps/chrome/eventfilter/GestureHandler;Z)V
    .locals 6

    .prologue
    .line 73
    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;-><init>(Landroid/content/Context;Lcom/google/android/apps/chrome/eventfilter/EventFilterHost;Lcom/google/android/apps/chrome/eventfilter/GestureHandler;ZZ)V

    .line 74
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/chrome/eventfilter/EventFilterHost;Lcom/google/android/apps/chrome/eventfilter/GestureHandler;ZZ)V
    .locals 2

    .prologue
    .line 84
    invoke-direct {p0, p2, p4}, Lcom/google/android/apps/chrome/eventfilter/EventFilter;-><init>(Lcom/google/android/apps/chrome/eventfilter/EventFilterHost;Z)V

    .line 22
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->mSingleInput:Z

    .line 24
    new-instance v0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter$LongPressRunnable;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter$LongPressRunnable;-><init>(Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter$1;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->mLongPressRunnable:Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter$LongPressRunnable;

    .line 25
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->mLongPressHandler:Landroid/os/Handler;

    .line 85
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->mScaledTouchSlop:I

    .line 86
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    invoke-static {}, Landroid/view/ViewConfiguration;->getLongPressTimeout()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->mLongPressTimeoutMs:I

    .line 87
    iput-boolean p5, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->mUseDefaultLongPress:Z

    .line 88
    iput-object p3, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->mHandler:Lcom/google/android/apps/chrome/eventfilter/GestureHandler;

    .line 89
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    .line 91
    new-instance v0, Landroid/view/GestureDetector;

    new-instance v1, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter$1;-><init>(Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;)V

    invoke-direct {v0, p1, v1}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->mDetector:Landroid/view/GestureDetector;

    .line 158
    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->mDetector:Landroid/view/GestureDetector;

    iget-boolean v1, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->mUseDefaultLongPress:Z

    invoke-virtual {v0, v1}, Landroid/view/GestureDetector;->setIsLongpressEnabled(Z)V

    .line 159
    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;Landroid/view/MotionEvent;)V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->longPress(Landroid/view/MotionEvent;)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;)Z
    .locals 1

    .prologue
    .line 16
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->mSeenFirstScrollEvent:Z

    return v0
.end method

.method static synthetic access$202(Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;Z)Z
    .locals 0

    .prologue
    .line 16
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->mSeenFirstScrollEvent:Z

    return p1
.end method

.method static synthetic access$300(Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;)I
    .locals 1

    .prologue
    .line 16
    iget v0, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->mScaledTouchSlop:I

    return v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;)Lcom/google/android/apps/chrome/eventfilter/GestureHandler;
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->mHandler:Lcom/google/android/apps/chrome/eventfilter/GestureHandler;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;)Z
    .locals 1

    .prologue
    .line 16
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->mSingleInput:Z

    return v0
.end method

.method private cancelLongPress()V
    .locals 2

    .prologue
    .line 173
    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->mLongPressHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->mLongPressRunnable:Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter$LongPressRunnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 174
    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->mLongPressRunnable:Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter$LongPressRunnable;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter$LongPressRunnable;->cancel()V

    .line 175
    return-void
.end method

.method private longPress(Landroid/view/MotionEvent;)V
    .locals 4

    .prologue
    .line 162
    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->mHandler:Lcom/google/android/apps/chrome/eventfilter/GestureHandler;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->mSingleInput:Z

    if-eqz v0, :cond_0

    .line 163
    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->mHandler:Lcom/google/android/apps/chrome/eventfilter/GestureHandler;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    iget v2, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->mPxToDp:F

    mul-float/2addr v1, v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    iget v3, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->mPxToDp:F

    mul-float/2addr v2, v3

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/chrome/eventfilter/GestureHandler;->onLongPress(FF)V

    .line 165
    :cond_0
    return-void
.end method


# virtual methods
.method public onInterceptTouchEventInternal(Landroid/view/MotionEvent;Z)Z
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->mHandler:Lcom/google/android/apps/chrome/eventfilter/GestureHandler;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onTouchEventInternal(Landroid/view/MotionEvent;)Z
    .locals 10

    .prologue
    const/4 v9, 0x3

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 179
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v8

    .line 183
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->mUseDefaultLongPress:Z

    if-nez v0, :cond_0

    .line 184
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v0

    if-le v0, v6, :cond_3

    .line 186
    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->mLongPressRunnable:Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter$LongPressRunnable;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter$LongPressRunnable;->isPending()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 187
    invoke-direct {p0}, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->cancelLongPress()V

    .line 216
    :cond_0
    :goto_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v0

    if-le v0, v6, :cond_9

    .line 217
    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->mHandler:Lcom/google/android/apps/chrome/eventfilter/GestureHandler;

    invoke-virtual {p1, v7}, Landroid/view/MotionEvent;->getX(I)F

    move-result v1

    iget v2, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->mPxToDp:F

    mul-float/2addr v1, v2

    invoke-virtual {p1, v7}, Landroid/view/MotionEvent;->getY(I)F

    move-result v2

    iget v3, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->mPxToDp:F

    mul-float/2addr v2, v3

    invoke-virtual {p1, v6}, Landroid/view/MotionEvent;->getX(I)F

    move-result v3

    iget v4, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->mPxToDp:F

    mul-float/2addr v3, v4

    invoke-virtual {p1, v6}, Landroid/view/MotionEvent;->getY(I)F

    move-result v4

    iget v5, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->mPxToDp:F

    mul-float/2addr v4, v5

    const/4 v5, 0x5

    if-ne v8, v5, :cond_8

    move v5, v6

    :goto_1
    invoke-interface/range {v0 .. v5}, Lcom/google/android/apps/chrome/eventfilter/GestureHandler;->onPinch(FFFFZ)V

    .line 220
    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->mDetector:Landroid/view/GestureDetector;

    invoke-virtual {v0, v7}, Landroid/view/GestureDetector;->setIsLongpressEnabled(Z)V

    .line 221
    iput-boolean v7, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->mSingleInput:Z

    .line 226
    :goto_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->mDetector:Landroid/view/GestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 229
    if-eq v8, v6, :cond_1

    if-ne v8, v9, :cond_2

    .line 230
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->mHandler:Lcom/google/android/apps/chrome/eventfilter/GestureHandler;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/eventfilter/GestureHandler;->onUpOrCancel()V

    .line 232
    :cond_2
    return v6

    .line 189
    :cond_3
    if-nez v8, :cond_5

    .line 191
    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->mLongPressRunnable:Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter$LongPressRunnable;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter$LongPressRunnable;->isPending()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 192
    invoke-direct {p0}, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->cancelLongPress()V

    .line 194
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->mLongPressRunnable:Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter$LongPressRunnable;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter$LongPressRunnable;->init(Landroid/view/MotionEvent;)V

    .line 195
    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->mLongPressHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->mLongPressRunnable:Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter$LongPressRunnable;

    iget v2, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->mLongPressTimeoutMs:I

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 196
    :cond_5
    if-eq v8, v6, :cond_6

    if-ne v8, v9, :cond_7

    .line 197
    :cond_6
    invoke-direct {p0}, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->cancelLongPress()V

    goto :goto_0

    .line 198
    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->mLongPressRunnable:Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter$LongPressRunnable;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter$LongPressRunnable;->isPending()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 200
    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->mLongPressRunnable:Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter$LongPressRunnable;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter$LongPressRunnable;->getInitialEvent()Landroid/view/MotionEvent;

    move-result-object v0

    .line 201
    invoke-virtual {v0}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    sub-float/2addr v1, v2

    .line 202
    invoke-virtual {v0}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    sub-float/2addr v0, v2

    .line 203
    mul-float/2addr v1, v1

    mul-float/2addr v0, v0

    add-float/2addr v0, v1

    .line 206
    iget v1, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->mScaledTouchSlop:I

    iget v2, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->mScaledTouchSlop:I

    mul-int/2addr v1, v2

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 207
    invoke-direct {p0}, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->cancelLongPress()V

    goto/16 :goto_0

    :cond_8
    move v5, v7

    .line 217
    goto :goto_1

    .line 223
    :cond_9
    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->mDetector:Landroid/view/GestureDetector;

    iget-boolean v1, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->mUseDefaultLongPress:Z

    invoke-virtual {v0, v1}, Landroid/view/GestureDetector;->setIsLongpressEnabled(Z)V

    .line 224
    iput-boolean v6, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->mSingleInput:Z

    goto :goto_2
.end method
