.class public Lcom/google/android/apps/chrome/third_party/OverScroller;
.super Ljava/lang/Object;
.source "OverScroller.java"


# static fields
.field private static sViscousFluidNormalize:F

.field private static sViscousFluidScale:F


# instance fields
.field private final mFlywheel:Z

.field private final mInterpolator:Landroid/view/animation/Interpolator;

.field private mMode:I

.field private final mScrollerX:Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;

.field private final mScrollerY:Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/high16 v0, 0x41000000    # 8.0f

    const/high16 v1, 0x3f800000    # 1.0f

    .line 54
    sput v0, Lcom/google/android/apps/chrome/third_party/OverScroller;->sViscousFluidScale:F

    .line 55
    invoke-static {v1, v0, v1}, Lcom/google/android/apps/chrome/third_party/OverScroller;->viscousFluid(FFF)F

    move-result v0

    div-float v0, v1, v0

    sput v0, Lcom/google/android/apps/chrome/third_party/OverScroller;->sViscousFluidNormalize:F

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 62
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/chrome/third_party/OverScroller;-><init>(Landroid/content/Context;Landroid/view/animation/Interpolator;)V

    .line 63
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/view/animation/Interpolator;)V
    .locals 1

    .prologue
    .line 72
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/chrome/third_party/OverScroller;-><init>(Landroid/content/Context;Landroid/view/animation/Interpolator;Z)V

    .line 73
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/view/animation/Interpolator;Z)V
    .locals 1

    .prologue
    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 84
    iput-object p2, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mInterpolator:Landroid/view/animation/Interpolator;

    .line 85
    iput-boolean p3, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mFlywheel:Z

    .line 86
    new-instance v0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mScrollerX:Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;

    .line 87
    new-instance v0, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mScrollerY:Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;

    .line 89
    invoke-static {p1}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->initFromContext(Landroid/content/Context;)V

    .line 90
    return-void
.end method

.method private getInterpolatedTime(JI)F
    .locals 3

    .prologue
    .line 1022
    long-to-float v0, p1

    int-to-float v1, p3

    div-float/2addr v0, v1

    .line 1024
    iget-object v1, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mInterpolator:Landroid/view/animation/Interpolator;

    if-nez v1, :cond_0

    .line 1025
    invoke-static {v0}, Lcom/google/android/apps/chrome/third_party/OverScroller;->viscousFluid(F)F

    move-result v0

    .line 1027
    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mInterpolator:Landroid/view/animation/Interpolator;

    invoke-interface {v1, v0}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v0

    goto :goto_0
.end method

.method private static viscousFluid(F)F
    .locals 2

    .prologue
    .line 1001
    sget v0, Lcom/google/android/apps/chrome/third_party/OverScroller;->sViscousFluidScale:F

    sget v1, Lcom/google/android/apps/chrome/third_party/OverScroller;->sViscousFluidNormalize:F

    invoke-static {p0, v0, v1}, Lcom/google/android/apps/chrome/third_party/OverScroller;->viscousFluid(FFF)F

    move-result v0

    return v0
.end method

.method private static viscousFluid(FFF)F
    .locals 5

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    .line 1005
    mul-float v0, p0, p1

    .line 1006
    cmpg-float v1, v0, v4

    if-gez v1, :cond_0

    .line 1007
    neg-float v1, v0

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->exp(D)D

    move-result-wide v2

    double-to-float v1, v2

    sub-float v1, v4, v1

    sub-float/2addr v0, v1

    .line 1013
    :goto_0
    mul-float/2addr v0, p2

    .line 1014
    return v0

    .line 1009
    :cond_0
    sub-float v0, v4, v0

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->exp(D)D

    move-result-wide v0

    double-to-float v0, v0

    sub-float v0, v4, v0

    .line 1011
    const v1, 0x3ebc5ab2

    const v2, 0x3f21d2a7

    mul-float/2addr v0, v2

    add-float/2addr v0, v1

    goto :goto_0
.end method


# virtual methods
.method public abortAnimation()V
    .locals 1

    .prologue
    .line 505
    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mScrollerX:Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->finish()V

    .line 506
    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mScrollerY:Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->finish()V

    .line 507
    return-void
.end method

.method public computeScrollOffset()Z
    .locals 6

    .prologue
    .line 291
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/third_party/OverScroller;->isFinished()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 292
    const/4 v0, 0x0

    .line 332
    :goto_0
    return v0

    .line 295
    :cond_0
    iget v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mMode:I

    packed-switch v0, :pswitch_data_0

    .line 332
    :cond_1
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 297
    :pswitch_0
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v0

    .line 300
    iget-object v2, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mScrollerX:Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;

    # getter for: Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mStartTime:J
    invoke-static {v2}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->access$600(Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;)J

    move-result-wide v2

    sub-long/2addr v0, v2

    .line 302
    iget-object v2, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mScrollerX:Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;

    # getter for: Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mDuration:I
    invoke-static {v2}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->access$500(Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;)I

    move-result v2

    .line 303
    int-to-long v4, v2

    cmp-long v3, v0, v4

    if-gez v3, :cond_2

    .line 304
    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/apps/chrome/third_party/OverScroller;->getInterpolatedTime(JI)F

    move-result v0

    .line 305
    iget-object v1, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mScrollerX:Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->updateScroll(F)V

    .line 306
    iget-object v1, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mScrollerY:Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->updateScroll(F)V

    goto :goto_1

    .line 308
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/third_party/OverScroller;->abortAnimation()V

    goto :goto_1

    .line 313
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mScrollerX:Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;

    # getter for: Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mFinished:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->access$000(Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 314
    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mScrollerX:Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->update()Z

    move-result v0

    if-nez v0, :cond_3

    .line 315
    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mScrollerX:Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->continueWhenFinished()Z

    move-result v0

    if-nez v0, :cond_3

    .line 316
    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mScrollerX:Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->finish()V

    .line 321
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mScrollerY:Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;

    # getter for: Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mFinished:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->access$000(Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 322
    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mScrollerY:Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->update()Z

    move-result v0

    if-nez v0, :cond_1

    .line 323
    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mScrollerY:Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->continueWhenFinished()Z

    move-result v0

    if-nez v0, :cond_1

    .line 324
    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mScrollerY:Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->finish()V

    goto :goto_1

    .line 295
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public fling(IIIIIIIIII)V
    .locals 6

    .prologue
    .line 428
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mFlywheel:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/third_party/OverScroller;->isFinished()Z

    move-result v0

    if-nez v0, :cond_0

    .line 429
    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mScrollerX:Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;

    # getter for: Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mCurrVelocity:F
    invoke-static {v0}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->access$200(Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;)F

    move-result v0

    .line 430
    iget-object v1, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mScrollerY:Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;

    # getter for: Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mCurrVelocity:F
    invoke-static {v1}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->access$200(Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;)F

    move-result v1

    .line 431
    int-to-float v2, p3

    invoke-static {v2}, Ljava/lang/Math;->signum(F)F

    move-result v2

    invoke-static {v0}, Ljava/lang/Math;->signum(F)F

    move-result v3

    cmpl-float v2, v2, v3

    if-nez v2, :cond_0

    int-to-float v2, p4

    invoke-static {v2}, Ljava/lang/Math;->signum(F)F

    move-result v2

    invoke-static {v1}, Ljava/lang/Math;->signum(F)F

    move-result v3

    cmpl-float v2, v2, v3

    if-nez v2, :cond_0

    .line 433
    int-to-float v2, p3

    add-float/2addr v0, v2

    float-to-int p3, v0

    .line 434
    int-to-float v0, p4

    add-float/2addr v0, v1

    float-to-int p4, v0

    move v2, p3

    .line 438
    :goto_0
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mMode:I

    .line 439
    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mScrollerX:Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;

    move v1, p1

    move v3, p5

    move v4, p6

    move v5, p9

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->fling(IIIII)V

    .line 440
    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mScrollerY:Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;

    move v1, p2

    move v2, p4

    move v3, p7

    move v4, p8

    move/from16 v5, p10

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->fling(IIIII)V

    .line 441
    return-void

    :cond_0
    move v2, p3

    goto :goto_0
.end method

.method public final forceFinished(Z)V
    .locals 2

    .prologue
    .line 158
    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mScrollerX:Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;

    iget-object v1, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mScrollerY:Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;

    # setter for: Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mFinished:Z
    invoke-static {v1, p1}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->access$002(Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;Z)Z

    move-result v1

    # setter for: Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mFinished:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->access$002(Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;Z)Z

    .line 159
    return-void
.end method

.method public final getCurrX()I
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mScrollerX:Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;

    # getter for: Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mCurrentPosition:I
    invoke-static {v0}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->access$100(Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;)I

    move-result v0

    return v0
.end method

.method public final getFinalX()I
    .locals 1

    .prologue
    .line 214
    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mScrollerX:Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;

    # getter for: Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mFinal:I
    invoke-static {v0}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->access$400(Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;)I

    move-result v0

    return v0
.end method

.method public final isFinished()Z
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mScrollerX:Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;

    # getter for: Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mFinished:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->access$000(Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mScrollerY:Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;

    # getter for: Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->mFinished:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->access$000(Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setFinalX(I)V
    .locals 1

    .prologue
    .line 270
    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mScrollerX:Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->setFinalPosition(I)V

    .line 271
    return-void
.end method

.method public springBack(IIIIII)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 385
    iput v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mMode:I

    .line 388
    iget-object v1, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mScrollerX:Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;

    invoke-virtual {v1, p1, p3, p4}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->springback(III)Z

    move-result v1

    .line 389
    iget-object v2, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mScrollerY:Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;

    invoke-virtual {v2, p2, p5, p6}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->springback(III)Z

    move-result v2

    .line 390
    if-nez v1, :cond_0

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public startScroll(IIII)V
    .locals 6

    .prologue
    .line 350
    const/16 v5, 0xfa

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/chrome/third_party/OverScroller;->startScroll(IIIII)V

    .line 351
    return-void
.end method

.method public startScroll(IIIII)V
    .locals 1

    .prologue
    .line 367
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mMode:I

    .line 368
    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mScrollerX:Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;

    invoke-virtual {v0, p1, p3, p5}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->startScroll(III)V

    .line 369
    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/OverScroller;->mScrollerY:Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;

    invoke-virtual {v0, p2, p4, p5}, Lcom/google/android/apps/chrome/third_party/OverScroller$SplineOverScroller;->startScroll(III)V

    .line 370
    return-void
.end method
