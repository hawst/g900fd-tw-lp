.class public Lcom/google/android/apps/chrome/icing/IcingController;
.super Ljava/lang/Object;
.source "IcingController.java"

# interfaces
.implements Lcom/google/android/apps/chrome/icing/SearchJniBridge$DataChangeObserver;


# static fields
.field static final DATA_CLEARED_PREFS_NAME:Ljava/lang/String; = "com.google.android.apps.chrome.icing.IcingController.DATA_CLEARED"

.field private static final DOCUMENT_MODE_REQUEST_INDEXING_DELAY_MS:J

.field private static final REQUEST_INDEXING_DELAY_MS:J

.field protected static final USAGE_REPORTS_DELAY_MS:J


# instance fields
.field private final mClearDataTask:Lcom/google/android/apps/chrome/icing/ConnectedTask;

.field private final mExecutor:Ljava/util/concurrent/ScheduledThreadPoolExecutor;

.field private final mIcingClient:Lcom/google/android/apps/chrome/icing/GoogleApiIcingClient;

.field final mIndexingRequested:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final mJniBridge:Lcom/google/android/apps/chrome/icing/SearchJniBridge;

.field private final mPrefs:Landroid/content/SharedPreferences;

.field private final mRequestIndexingDelayMs:J

.field private final mRequestIndexingTask:Lcom/google/android/apps/chrome/icing/ConnectedTask;

.field private final mUsageReportingEnabled:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final mUsageReportingTask:Lcom/google/android/apps/chrome/icing/ConnectedTask;

.field private final mUsageReportingTaskActive:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 43
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x5

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/apps/chrome/icing/IcingController;->DOCUMENT_MODE_REQUEST_INDEXING_DELAY_MS:J

    .line 45
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1e

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/apps/chrome/icing/IcingController;->REQUEST_INDEXING_DELAY_MS:J

    .line 47
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/apps/chrome/icing/IcingController;->USAGE_REPORTS_DELAY_MS:J

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/chrome/icing/GoogleApiIcingClient;Lcom/google/android/apps/chrome/icing/SearchJniBridge;Landroid/content/SharedPreferences;)V
    .locals 7

    .prologue
    .line 138
    invoke-static {p1}, Lcom/google/android/apps/chrome/utilities/FeatureUtilitiesInternal;->isDocumentMode(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-wide v4, Lcom/google/android/apps/chrome/icing/IcingController;->DOCUMENT_MODE_REQUEST_INDEXING_DELAY_MS:J

    :goto_0
    move-object v1, p0

    move-object v2, p2

    move-object v3, p3

    move-object v6, p4

    invoke-direct/range {v1 .. v6}, Lcom/google/android/apps/chrome/icing/IcingController;-><init>(Lcom/google/android/apps/chrome/icing/GoogleApiIcingClient;Lcom/google/android/apps/chrome/icing/SearchJniBridge;JLandroid/content/SharedPreferences;)V

    .line 144
    return-void

    .line 138
    :cond_0
    sget-wide v4, Lcom/google/android/apps/chrome/icing/IcingController;->REQUEST_INDEXING_DELAY_MS:J

    goto :goto_0
.end method

.method constructor <init>(Lcom/google/android/apps/chrome/icing/GoogleApiIcingClient;Lcom/google/android/apps/chrome/icing/SearchJniBridge;JLandroid/content/SharedPreferences;)V
    .locals 3

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    iput-object p2, p0, Lcom/google/android/apps/chrome/icing/IcingController;->mJniBridge:Lcom/google/android/apps/chrome/icing/SearchJniBridge;

    .line 66
    iput-object p1, p0, Lcom/google/android/apps/chrome/icing/IcingController;->mIcingClient:Lcom/google/android/apps/chrome/icing/GoogleApiIcingClient;

    .line 67
    iput-wide p3, p0, Lcom/google/android/apps/chrome/icing/IcingController;->mRequestIndexingDelayMs:J

    .line 68
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/icing/IcingController;->mUsageReportingEnabled:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 69
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/icing/IcingController;->mIndexingRequested:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 70
    new-instance v0, Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/icing/IcingController;->mExecutor:Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    .line 71
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/icing/IcingController;->mUsageReportingTaskActive:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 72
    iput-object p5, p0, Lcom/google/android/apps/chrome/icing/IcingController;->mPrefs:Landroid/content/SharedPreferences;

    .line 73
    new-instance v0, Lcom/google/android/apps/chrome/icing/IcingController$1;

    iget-object v1, p0, Lcom/google/android/apps/chrome/icing/IcingController;->mIcingClient:Lcom/google/android/apps/chrome/icing/GoogleApiIcingClient;

    iget-object v2, p0, Lcom/google/android/apps/chrome/icing/IcingController;->mExecutor:Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/apps/chrome/icing/IcingController$1;-><init>(Lcom/google/android/apps/chrome/icing/IcingController;Lcom/google/android/apps/chrome/icing/GoogleApiIcingClient;Ljava/util/concurrent/ScheduledThreadPoolExecutor;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/icing/IcingController;->mRequestIndexingTask:Lcom/google/android/apps/chrome/icing/ConnectedTask;

    .line 99
    new-instance v0, Lcom/google/android/apps/chrome/icing/IcingController$2;

    iget-object v1, p0, Lcom/google/android/apps/chrome/icing/IcingController;->mIcingClient:Lcom/google/android/apps/chrome/icing/GoogleApiIcingClient;

    iget-object v2, p0, Lcom/google/android/apps/chrome/icing/IcingController;->mExecutor:Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/apps/chrome/icing/IcingController$2;-><init>(Lcom/google/android/apps/chrome/icing/IcingController;Lcom/google/android/apps/chrome/icing/GoogleApiIcingClient;Ljava/util/concurrent/ScheduledThreadPoolExecutor;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/icing/IcingController;->mClearDataTask:Lcom/google/android/apps/chrome/icing/ConnectedTask;

    .line 115
    new-instance v0, Lcom/google/android/apps/chrome/icing/IcingController$3;

    iget-object v1, p0, Lcom/google/android/apps/chrome/icing/IcingController;->mIcingClient:Lcom/google/android/apps/chrome/icing/GoogleApiIcingClient;

    iget-object v2, p0, Lcom/google/android/apps/chrome/icing/IcingController;->mExecutor:Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/apps/chrome/icing/IcingController$3;-><init>(Lcom/google/android/apps/chrome/icing/IcingController;Lcom/google/android/apps/chrome/icing/GoogleApiIcingClient;Ljava/util/concurrent/ScheduledThreadPoolExecutor;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/icing/IcingController;->mUsageReportingTask:Lcom/google/android/apps/chrome/icing/ConnectedTask;

    .line 134
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/icing/IcingController;)Landroid/content/SharedPreferences;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/android/apps/chrome/icing/IcingController;->mPrefs:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/icing/IcingController;)Lcom/google/android/apps/chrome/icing/SearchJniBridge;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/android/apps/chrome/icing/IcingController;->mJniBridge:Lcom/google/android/apps/chrome/icing/SearchJniBridge;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/icing/IcingController;)Ljava/util/concurrent/atomic/AtomicBoolean;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/android/apps/chrome/icing/IcingController;->mUsageReportingEnabled:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/chrome/icing/IcingController;)Ljava/util/concurrent/atomic/AtomicBoolean;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/android/apps/chrome/icing/IcingController;->mUsageReportingTaskActive:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-object v0
.end method

.method private clearData()V
    .locals 3

    .prologue
    .line 183
    iget-object v0, p0, Lcom/google/android/apps/chrome/icing/IcingController;->mPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "com.google.android.apps.chrome.icing.IcingController.DATA_CLEARED"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 186
    iget-object v0, p0, Lcom/google/android/apps/chrome/icing/IcingController;->mExecutor:Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    iget-object v1, p0, Lcom/google/android/apps/chrome/icing/IcingController;->mClearDataTask:Lcom/google/android/apps/chrome/icing/ConnectedTask;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;->execute(Ljava/lang/Runnable;)V

    .line 187
    return-void
.end method


# virtual methods
.method public dump(Ljava/io/PrintWriter;)V
    .locals 4

    .prologue
    .line 226
    const-string/jumbo v0, "\nIcingController ["

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    .line 227
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "indexing requested: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/chrome/icing/IcingController;->mIndexingRequested:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    .line 228
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, ", indexing request delay: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, Lcom/google/android/apps/chrome/icing/IcingController;->mRequestIndexingDelayMs:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    .line 229
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, ", usage reporting enabled: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/chrome/icing/IcingController;->mUsageReportingEnabled:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    .line 230
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, ", reporting usage: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/chrome/icing/IcingController;->mUsageReportingTaskActive:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    .line 231
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, ", completed tasks: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/chrome/icing/IcingController;->mExecutor:Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    invoke-virtual {v1}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;->getCompletedTaskCount()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    .line 232
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, ", scheduled tasks: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/chrome/icing/IcingController;->mExecutor:Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    invoke-virtual {v1}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;->getTaskCount()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    .line 233
    const-string/jumbo v0, "]"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    .line 234
    return-void
.end method

.method public enableUsageReporting()V
    .locals 2

    .prologue
    .line 195
    iget-object v0, p0, Lcom/google/android/apps/chrome/icing/IcingController;->mUsageReportingEnabled:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 196
    return-void
.end method

.method hasPendingTasksForTest()Z
    .locals 4

    .prologue
    .line 191
    iget-object v0, p0, Lcom/google/android/apps/chrome/icing/IcingController;->mExecutor:Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    invoke-virtual {v0}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;->getTaskCount()J

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/apps/chrome/icing/IcingController;->mExecutor:Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    invoke-virtual {v2}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;->getCompletedTaskCount()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public init(I)V
    .locals 4

    .prologue
    .line 147
    iget-object v0, p0, Lcom/google/android/apps/chrome/icing/IcingController;->mPrefs:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "com.google.android.apps.chrome.icing.IcingController.LEGACY_CORPUS_DATA_CLEARED_PREFS_NAME"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 148
    iget-object v0, p0, Lcom/google/android/apps/chrome/icing/IcingController;->mExecutor:Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    new-instance v1, Lcom/google/android/apps/chrome/icing/IcingController$4;

    iget-object v2, p0, Lcom/google/android/apps/chrome/icing/IcingController;->mIcingClient:Lcom/google/android/apps/chrome/icing/GoogleApiIcingClient;

    iget-object v3, p0, Lcom/google/android/apps/chrome/icing/IcingController;->mExecutor:Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    invoke-direct {v1, p0, v2, v3}, Lcom/google/android/apps/chrome/icing/IcingController$4;-><init>(Lcom/google/android/apps/chrome/icing/IcingController;Lcom/google/android/apps/chrome/icing/GoogleApiIcingClient;Ljava/util/concurrent/ScheduledThreadPoolExecutor;)V

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;->execute(Ljava/lang/Runnable;)V

    .line 163
    :cond_0
    const v0, 0x4de180

    if-lt p1, v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/icing/IcingController;->mPrefs:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "com.google.android.apps.chrome.icing.IcingController.VERSION"

    const/4 v2, -0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    .line 165
    invoke-direct {p0}, Lcom/google/android/apps/chrome/icing/IcingController;->clearData()V

    .line 167
    :cond_1
    return-void
.end method

.method public onDataChanged()V
    .locals 5

    .prologue
    .line 171
    iget-object v0, p0, Lcom/google/android/apps/chrome/icing/IcingController;->mIndexingRequested:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 172
    iget-object v0, p0, Lcom/google/android/apps/chrome/icing/IcingController;->mExecutor:Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    iget-object v1, p0, Lcom/google/android/apps/chrome/icing/IcingController;->mRequestIndexingTask:Lcom/google/android/apps/chrome/icing/ConnectedTask;

    iget-wide v2, p0, Lcom/google/android/apps/chrome/icing/IcingController;->mRequestIndexingDelayMs:J

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v1, v2, v3, v4}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    .line 175
    :cond_0
    return-void
.end method

.method public onDataCleared()V
    .locals 0

    .prologue
    .line 179
    invoke-direct {p0}, Lcom/google/android/apps/chrome/icing/IcingController;->clearData()V

    .line 180
    return-void
.end method

.method public reportContext(Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 212
    iget-object v6, p0, Lcom/google/android/apps/chrome/icing/IcingController;->mExecutor:Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    new-instance v0, Lcom/google/android/apps/chrome/icing/IcingController$5;

    iget-object v2, p0, Lcom/google/android/apps/chrome/icing/IcingController;->mIcingClient:Lcom/google/android/apps/chrome/icing/GoogleApiIcingClient;

    iget-object v3, p0, Lcom/google/android/apps/chrome/icing/IcingController;->mExecutor:Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/chrome/icing/IcingController$5;-><init>(Lcom/google/android/apps/chrome/icing/IcingController;Lcom/google/android/apps/chrome/icing/GoogleApiIcingClient;Ljava/util/concurrent/ScheduledThreadPoolExecutor;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;->execute(Ljava/lang/Runnable;)V

    .line 223
    return-void
.end method

.method public startReportingTask()V
    .locals 5

    .prologue
    .line 201
    iget-object v0, p0, Lcom/google/android/apps/chrome/icing/IcingController;->mUsageReportingEnabled:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/icing/IcingController;->mUsageReportingTaskActive:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 202
    iget-object v0, p0, Lcom/google/android/apps/chrome/icing/IcingController;->mExecutor:Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    iget-object v1, p0, Lcom/google/android/apps/chrome/icing/IcingController;->mUsageReportingTask:Lcom/google/android/apps/chrome/icing/ConnectedTask;

    sget-wide v2, Lcom/google/android/apps/chrome/icing/IcingController;->USAGE_REPORTS_DELAY_MS:J

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v1, v2, v3, v4}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    .line 204
    :cond_0
    return-void
.end method

.method public stopReportingTask()V
    .locals 2

    .prologue
    .line 208
    iget-object v0, p0, Lcom/google/android/apps/chrome/icing/IcingController;->mUsageReportingTaskActive:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 209
    return-void
.end method
