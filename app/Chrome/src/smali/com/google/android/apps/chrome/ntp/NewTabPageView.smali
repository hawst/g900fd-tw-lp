.class public Lcom/google/android/apps/chrome/ntp/NewTabPageView;
.super Landroid/widget/FrameLayout;
.source "NewTabPageView.java"

# interfaces
.implements Landroid/view/View$OnLayoutChangeListener;
.implements Lorg/chromium/chrome/browser/profiles/MostVisitedSites$MostVisitedURLsObserver;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final FAVICON_BACKGROUND_COLOR:I

.field public static final NUM_MOST_VISITED_SITES:I = 0x6


# instance fields
.field private mContentView:Landroid/view/ViewGroup;

.field private mDisableUrlFocusChangeAnimations:Z

.field private mFaviconGenerator:Lcom/google/android/apps/chrome/widget/TabIconGenerator;

.field private mFirstShow:Z

.field private mHasReceivedMostVisitedSites:Z

.field private mLoadHasCompleted:Z

.field private mManager:Lcom/google/android/apps/chrome/ntp/NewTabPageView$NewTabPageManager;

.field private mMostVisitedLayout:Lcom/google/android/apps/chrome/ntp/MostVisitedLayout;

.field private mMostVisitedPlaceholder:Landroid/view/View;

.field private mNoSearchLogoSpacer:Landroid/view/View;

.field private mOptOutView:Landroid/view/View;

.field private mPendingLoadTasks:I

.field private mPendingSnapScroll:Z

.field private mScrollView:Lcom/google/android/apps/chrome/ntp/NewTabScrollView;

.field private mSearchBoxScrollListener:Lcom/google/android/apps/chrome/ntp/NewTabPage$OnSearchBoxScrollListener;

.field private mSearchBoxTextView:Landroid/widget/TextView;

.field private mSearchBoxView:Landroid/view/View;

.field private mSearchProviderHasLogo:Z

.field private mSearchProviderLogoView:Lcom/google/android/apps/chrome/ntp/LogoView;

.field private mSnapshotHeight:I

.field private mSnapshotMostVisitedChanged:Z

.field private mSnapshotScrollY:I

.field private mSnapshotWidth:I

.field private mUrlFocusChangePercent:F


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/16 v1, 0x96

    .line 53
    const-class v0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->$assertionsDisabled:Z

    .line 63
    invoke-static {v1, v1, v1}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    sput v0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->FAVICON_BACKGROUND_COLOR:I

    return-void

    .line 53
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 193
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 82
    iput-boolean v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mFirstShow:Z

    .line 83
    iput-boolean v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mSearchProviderHasLogo:Z

    .line 94
    iput v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mPendingLoadTasks:I

    .line 195
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$dimen;->most_visited_favicon_size:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    div-float/2addr v0, v1

    float-to-int v2, v0

    .line 197
    new-instance v0, Lcom/google/android/apps/chrome/widget/TabIconGenerator;

    const/4 v3, 0x2

    const/16 v4, 0xa

    sget v5, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->FAVICON_BACKGROUND_COLOR:I

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/chrome/widget/TabIconGenerator;-><init>(Landroid/content/Context;IIII)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mFaviconGenerator:Lcom/google/android/apps/chrome/widget/TabIconGenerator;

    .line 200
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/ntp/NewTabPageView;)Lcom/google/android/apps/chrome/ntp/NewTabPageView$NewTabPageManager;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mManager:Lcom/google/android/apps/chrome/ntp/NewTabPageView$NewTabPageManager;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/ntp/NewTabPageView;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mSearchBoxTextView:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/google/android/apps/chrome/ntp/NewTabPageView;)V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->loadTaskCompleted()V

    return-void
.end method

.method static synthetic access$1100(Lcom/google/android/apps/chrome/ntp/NewTabPageView;)Lcom/google/android/apps/chrome/widget/TabIconGenerator;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mFaviconGenerator:Lcom/google/android/apps/chrome/widget/TabIconGenerator;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/ntp/NewTabPageView;)Landroid/view/View;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mOptOutView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/chrome/ntp/NewTabPageView;)Z
    .locals 1

    .prologue
    .line 53
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mPendingSnapScroll:Z

    return v0
.end method

.method static synthetic access$302(Lcom/google/android/apps/chrome/ntp/NewTabPageView;Z)Z
    .locals 0

    .prologue
    .line 53
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mPendingSnapScroll:Z

    return p1
.end method

.method static synthetic access$400(Lcom/google/android/apps/chrome/ntp/NewTabPageView;)Lcom/google/android/apps/chrome/ntp/NewTabScrollView;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mScrollView:Lcom/google/android/apps/chrome/ntp/NewTabScrollView;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/chrome/ntp/NewTabPageView;)Lcom/google/android/apps/chrome/ntp/MostVisitedLayout;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mMostVisitedLayout:Lcom/google/android/apps/chrome/ntp/MostVisitedLayout;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/chrome/ntp/NewTabPageView;)Landroid/view/ViewGroup;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mContentView:Landroid/view/ViewGroup;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/apps/chrome/ntp/NewTabPageView;)V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->updateSearchBoxOnScroll()V

    return-void
.end method

.method static synthetic access$800(Lcom/google/android/apps/chrome/ntp/NewTabPageView;)Lcom/google/android/apps/chrome/ntp/LogoView;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mSearchProviderLogoView:Lcom/google/android/apps/chrome/ntp/LogoView;

    return-object v0
.end method

.method static synthetic access$902(Lcom/google/android/apps/chrome/ntp/NewTabPageView;Z)Z
    .locals 0

    .prologue
    .line 53
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mSnapshotMostVisitedChanged:Z

    return p1
.end method

.method static getTitleForDisplay(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 178
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    if-eqz p1, :cond_3

    .line 179
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 180
    invoke-virtual {v1}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v0

    .line 181
    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    .line 182
    if-nez v0, :cond_0

    const-string/jumbo v0, ""

    .line 183
    :cond_0
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string/jumbo v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_1
    const-string/jumbo v1, ""

    .line 184
    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 186
    :cond_3
    return-object p0
.end method

.method private initializeSearchBoxScrollHandling()V
    .locals 3

    .prologue
    .line 357
    new-instance v0, Lcom/google/android/apps/chrome/ntp/NewTabPageView$8;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/ntp/NewTabPageView$8;-><init>(Lcom/google/android/apps/chrome/ntp/NewTabPageView;)V

    .line 369
    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mScrollView:Lcom/google/android/apps/chrome/ntp/NewTabScrollView;

    new-instance v2, Lcom/google/android/apps/chrome/ntp/NewTabPageView$9;

    invoke-direct {v2, p0, v0}, Lcom/google/android/apps/chrome/ntp/NewTabPageView$9;-><init>(Lcom/google/android/apps/chrome/ntp/NewTabPageView;Ljava/lang/Runnable;)V

    invoke-virtual {v1, v2}, Lcom/google/android/apps/chrome/ntp/NewTabScrollView;->setOnScrollListener(Lcom/google/android/apps/chrome/ntp/NewTabScrollView$OnScrollListener;)V

    .line 379
    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mScrollView:Lcom/google/android/apps/chrome/ntp/NewTabScrollView;

    new-instance v2, Lcom/google/android/apps/chrome/ntp/NewTabPageView$10;

    invoke-direct {v2, p0, v0}, Lcom/google/android/apps/chrome/ntp/NewTabPageView$10;-><init>(Lcom/google/android/apps/chrome/ntp/NewTabPageView;Ljava/lang/Runnable;)V

    invoke-virtual {v1, v2}, Lcom/google/android/apps/chrome/ntp/NewTabScrollView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 395
    return-void
.end method

.method private loadSearchProviderLogo()V
    .locals 2

    .prologue
    .line 420
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mManager:Lcom/google/android/apps/chrome/ntp/NewTabPageView$NewTabPageManager;

    new-instance v1, Lcom/google/android/apps/chrome/ntp/NewTabPageView$11;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/ntp/NewTabPageView$11;-><init>(Lcom/google/android/apps/chrome/ntp/NewTabPageView;)V

    invoke-interface {v0, v1}, Lcom/google/android/apps/chrome/ntp/NewTabPageView$NewTabPageManager;->getSearchProviderLogo(Lorg/chromium/chrome/browser/LogoBridge$LogoObserver;)V

    .line 429
    return-void
.end method

.method private loadTaskCompleted()V
    .locals 1

    .prologue
    .line 402
    sget-boolean v0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mPendingLoadTasks:I

    if-gtz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 403
    :cond_0
    iget v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mPendingLoadTasks:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mPendingLoadTasks:I

    .line 404
    iget v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mPendingLoadTasks:I

    if-nez v0, :cond_2

    .line 405
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mLoadHasCompleted:Z

    if-eqz v0, :cond_1

    .line 406
    sget-boolean v0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 408
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mLoadHasCompleted:Z

    .line 409
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mManager:Lcom/google/android/apps/chrome/ntp/NewTabPageView$NewTabPageManager;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/ntp/NewTabPageView$NewTabPageManager;->notifyLoadingComplete()V

    .line 411
    invoke-direct {p0}, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->loadSearchProviderLogo()V

    .line 414
    :cond_2
    return-void
.end method

.method private setUrlFocusChangeAnimationPercentInternal(F)V
    .locals 3

    .prologue
    .line 504
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mContentView:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mMostVisitedLayout:Lcom/google/android/apps/chrome/ntp/MostVisitedLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/ntp/MostVisitedLayout;->getTop()I

    move-result v1

    neg-int v1, v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mScrollView:Lcom/google/android/apps/chrome/ntp/NewTabScrollView;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/ntp/NewTabScrollView;->getScrollY()I

    move-result v2

    add-int/2addr v1, v2

    iget-object v2, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mContentView:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getPaddingTop()I

    move-result v2

    add-int/2addr v1, v2

    int-to-float v1, v1

    mul-float/2addr v1, p1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setTranslationY(F)V

    .line 506
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->updateVisualsForToolbarTransition(F)V

    .line 507
    return-void
.end method

.method private showOptOutPromo()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 292
    sget v0, Lcom/google/android/apps/chrome/R$id;->opt_out_promo_stub:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 293
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mOptOutView:Landroid/view/View;

    .line 295
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mOptOutView:Landroid/view/View;

    sget v1, Lcom/google/android/apps/chrome/R$id;->opt_out_text:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 297
    new-instance v1, Lcom/google/android/apps/chrome/ntp/NewTabPageView$6;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/ntp/NewTabPageView$6;-><init>(Lcom/google/android/apps/chrome/ntp/NewTabPageView;)V

    .line 312
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->getContext()Landroid/content/Context;

    move-result-object v2

    sget v3, Lcom/google/android/apps/chrome/R$string;->tabs_and_apps_opt_out:I

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;

    new-instance v4, Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;

    const-string/jumbo v5, "<link>"

    const-string/jumbo v6, "</link>"

    invoke-direct {v4, v5, v6, v1}, Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    aput-object v4, v3, v7

    invoke-static {v2, v3}, Lcom/google/android/apps/chrome/utilities/StringLinkifier;->linkify(Ljava/lang/String;[Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;)Landroid/text/SpannableString;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 315
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 317
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v1

    .line 319
    invoke-virtual {v1}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->incrementOptOutShownCount()V

    .line 320
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->recordDocumentOptOutShown()V

    .line 322
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mOptOutView:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    .line 324
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mOptOutView:Landroid/view/View;

    sget v2, Lcom/google/android/apps/chrome/R$id;->got_it_button:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 325
    new-instance v2, Lcom/google/android/apps/chrome/ntp/NewTabPageView$7;

    invoke-direct {v2, p0, v1}, Lcom/google/android/apps/chrome/ntp/NewTabPageView$7;-><init>(Lcom/google/android/apps/chrome/ntp/NewTabPageView;Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 335
    return-void
.end method

.method private updateMostVisitedPlaceholderVisibility()V
    .locals 5

    .prologue
    const/16 v3, 0x8

    const/4 v1, 0x0

    .line 724
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mHasReceivedMostVisitedSites:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mMostVisitedLayout:Lcom/google/android/apps/chrome/ntp/MostVisitedLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ntp/MostVisitedLayout;->getChildCount()I

    move-result v0

    if-nez v0, :cond_3

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mSearchProviderHasLogo:Z

    if-nez v0, :cond_3

    const/4 v0, 0x1

    .line 728
    :goto_0
    iget-object v4, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mNoSearchLogoSpacer:Landroid/view/View;

    iget-boolean v2, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mSearchProviderHasLogo:Z

    if-nez v2, :cond_0

    if-eqz v0, :cond_4

    :cond_0
    move v2, v3

    :goto_1
    invoke-virtual {v4, v2}, Landroid/view/View;->setVisibility(I)V

    .line 731
    if-eqz v0, :cond_5

    .line 732
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mMostVisitedPlaceholder:Landroid/view/View;

    if-nez v0, :cond_1

    .line 733
    sget v0, Lcom/google/android/apps/chrome/R$id;->most_visited_placeholder_stub:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 735
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mMostVisitedPlaceholder:Landroid/view/View;

    .line 737
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mMostVisitedLayout:Lcom/google/android/apps/chrome/ntp/MostVisitedLayout;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/chrome/ntp/MostVisitedLayout;->setVisibility(I)V

    .line 738
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mMostVisitedPlaceholder:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 744
    :cond_2
    :goto_2
    return-void

    :cond_3
    move v0, v1

    .line 724
    goto :goto_0

    .line 728
    :cond_4
    const/4 v2, 0x4

    goto :goto_1

    .line 740
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mMostVisitedPlaceholder:Landroid/view/View;

    if-eqz v0, :cond_2

    .line 741
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mMostVisitedLayout:Lcom/google/android/apps/chrome/ntp/MostVisitedLayout;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/ntp/MostVisitedLayout;->setVisibility(I)V

    .line 742
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mMostVisitedPlaceholder:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2
.end method

.method private updateSearchBoxOnScroll()V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 338
    iget-boolean v1, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mDisableUrlFocusChangeAnimations:Z

    if-eqz v1, :cond_1

    .line 354
    :cond_0
    :goto_0
    return-void

    .line 343
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mScrollView:Lcom/google/android/apps/chrome/ntp/NewTabScrollView;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/ntp/NewTabScrollView;->getHeight()I

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mSearchBoxView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v1

    if-eqz v1, :cond_2

    .line 344
    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mScrollView:Lcom/google/android/apps/chrome/ntp/NewTabScrollView;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/ntp/NewTabScrollView;->getScrollY()I

    move-result v1

    .line 345
    const/high16 v2, 0x3f800000    # 1.0f

    int-to-float v1, v1

    iget-object v3, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mSearchBoxView:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v1, v3

    invoke-static {v2, v1}, Ljava/lang/Math;->min(FF)F

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 349
    :cond_2
    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->updateVisualsForToolbarTransition(F)V

    .line 351
    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mSearchBoxScrollListener:Lcom/google/android/apps/chrome/ntp/NewTabPage$OnSearchBoxScrollListener;

    if-eqz v1, :cond_0

    .line 352
    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mSearchBoxScrollListener:Lcom/google/android/apps/chrome/ntp/NewTabPage$OnSearchBoxScrollListener;

    invoke-interface {v1, v0}, Lcom/google/android/apps/chrome/ntp/NewTabPage$OnSearchBoxScrollListener;->onScrollChanged(F)V

    goto :goto_0
.end method

.method private updateVisualsForToolbarTransition(F)V
    .locals 3

    .prologue
    const v2, 0x3ecccccd    # 0.4f

    const/4 v1, 0x0

    .line 511
    cmpl-float v0, p1, v2

    if-ltz v0, :cond_1

    move v0, v1

    .line 514
    :goto_0
    cmpl-float v1, p1, v1

    if-nez v1, :cond_0

    const/high16 v0, 0x3f800000    # 1.0f

    .line 516
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mSearchProviderLogoView:Lcom/google/android/apps/chrome/ntp/LogoView;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/ntp/LogoView;->setAlpha(F)V

    .line 517
    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mSearchBoxView:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setAlpha(F)V

    .line 518
    return-void

    .line 511
    :cond_1
    sub-float v0, v2, p1

    const/high16 v2, 0x40200000    # 2.5f

    mul-float/2addr v0, v2

    goto :goto_0
.end method


# virtual methods
.method captureThumbnail(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 608
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mSearchProviderLogoView:Lcom/google/android/apps/chrome/ntp/LogoView;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ntp/LogoView;->endAnimation()V

    .line 609
    invoke-static {p0, p1}, Lcom/google/android/apps/chrome/utilities/ViewUtils;->captureBitmap(Landroid/view/View;Landroid/graphics/Canvas;)V

    .line 610
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mSnapshotWidth:I

    .line 611
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mSnapshotHeight:I

    .line 612
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mScrollView:Lcom/google/android/apps/chrome/ntp/NewTabScrollView;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ntp/NewTabScrollView;->getScrollY()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mSnapshotScrollY:I

    .line 613
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mSnapshotMostVisitedChanged:Z

    .line 614
    return-void
.end method

.method getSearchBoxBounds(Landroid/graphics/Rect;)V
    .locals 5

    .prologue
    .line 527
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mSearchBoxView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getX()F

    move-result v0

    float-to-int v0, v0

    .line 528
    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mSearchBoxView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getY()F

    move-result v1

    float-to-int v1, v1

    .line 529
    iget-object v2, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mSearchBoxView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getPaddingLeft()I

    move-result v2

    add-int/2addr v2, v0

    iget-object v3, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mSearchBoxView:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getPaddingTop()I

    move-result v3

    add-int/2addr v3, v1

    iget-object v4, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mSearchBoxView:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getWidth()I

    move-result v4

    add-int/2addr v0, v4

    iget-object v4, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mSearchBoxView:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getPaddingRight()I

    move-result v4

    sub-int/2addr v0, v4

    iget-object v4, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mSearchBoxView:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getHeight()I

    move-result v4

    add-int/2addr v1, v4

    iget-object v4, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mSearchBoxView:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getPaddingBottom()I

    move-result v4

    sub-int/2addr v1, v4

    invoke-virtual {p1, v2, v3, v0, v1}, Landroid/graphics/Rect;->set(IIII)V

    .line 534
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mSearchBoxView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 535
    :goto_0
    if-eqz v0, :cond_0

    .line 536
    invoke-virtual {v0}, Landroid/view/View;->getScrollX()I

    move-result v1

    neg-int v1, v1

    invoke-virtual {v0}, Landroid/view/View;->getScrollY()I

    move-result v2

    neg-int v2, v2

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Rect;->offset(II)V

    .line 537
    if-eq v0, p0, :cond_0

    .line 538
    invoke-virtual {v0}, Landroid/view/View;->getX()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v0}, Landroid/view/View;->getY()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Rect;->offset(II)V

    .line 539
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    goto :goto_0

    .line 541
    :cond_0
    return-void
.end method

.method getUrlFocusChangeAnimationPercent()F
    .locals 1

    .prologue
    .line 495
    iget v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mUrlFocusChangePercent:F

    return v0
.end method

.method public initialize(Lcom/google/android/apps/chrome/ntp/NewTabPageView$NewTabPageManager;Z)V
    .locals 2

    .prologue
    .line 279
    iput-object p1, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mManager:Lcom/google/android/apps/chrome/ntp/NewTabPageView$NewTabPageManager;

    .line 280
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mManager:Lcom/google/android/apps/chrome/ntp/NewTabPageView$NewTabPageManager;

    const/4 v1, 0x6

    invoke-interface {v0, p0, v1}, Lcom/google/android/apps/chrome/ntp/NewTabPageView$NewTabPageManager;->setMostVisitedURLsObserver(Lorg/chromium/chrome/browser/profiles/MostVisitedSites$MostVisitedURLsObserver;I)V

    .line 282
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mScrollView:Lcom/google/android/apps/chrome/ntp/NewTabScrollView;

    const/high16 v1, 0x11000000

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/ntp/NewTabScrollView;->enableBottomShadow(I)V

    .line 283
    if-eqz p2, :cond_0

    .line 284
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mSearchBoxView:Landroid/view/View;

    sget v1, Lcom/google/android/apps/chrome/R$id;->search_box_text:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 285
    sget v1, Lcom/google/android/apps/chrome/R$string;->search_or_type_url:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setHint(I)V

    .line 288
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mManager:Lcom/google/android/apps/chrome/ntp/NewTabPageView$NewTabPageManager;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/ntp/NewTabPageView$NewTabPageManager;->shouldShowOptOutPromo()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->showOptOutPromo()V

    .line 289
    :cond_1
    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .prologue
    .line 554
    invoke-super {p0}, Landroid/widget/FrameLayout;->onAttachedToWindow()V

    .line 555
    sget-boolean v0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mManager:Lcom/google/android/apps/chrome/ntp/NewTabPageView$NewTabPageManager;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 557
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mFirstShow:Z

    if-eqz v0, :cond_2

    .line 558
    invoke-direct {p0}, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->loadTaskCompleted()V

    .line 559
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mFirstShow:Z

    .line 565
    :cond_1
    :goto_0
    return-void

    .line 563
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mManager:Lcom/google/android/apps/chrome/ntp/NewTabPageView$NewTabPageManager;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/ntp/NewTabPageView$NewTabPageManager;->isLocationBarShownInNTP()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->updateSearchBoxOnScroll()V

    goto :goto_0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 569
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    .line 570
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->setUrlFocusChangeAnimationPercent(F)V

    .line 571
    return-void
.end method

.method protected onFinishInflate()V
    .locals 6

    .prologue
    .line 204
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 206
    sget v0, Lcom/google/android/apps/chrome/R$id;->ntp_scrollview:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/ntp/NewTabScrollView;

    iput-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mScrollView:Lcom/google/android/apps/chrome/ntp/NewTabScrollView;

    .line 207
    sget v0, Lcom/google/android/apps/chrome/R$id;->most_visited_layout:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/ntp/MostVisitedLayout;

    iput-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mMostVisitedLayout:Lcom/google/android/apps/chrome/ntp/MostVisitedLayout;

    .line 208
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$dimen;->most_visited_spacing:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    .line 209
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Lcom/google/android/apps/chrome/R$dimen;->most_visited_tile_width:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 210
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mMostVisitedLayout:Lcom/google/android/apps/chrome/ntp/MostVisitedLayout;

    const/4 v3, 0x1

    const/4 v4, 0x3

    move v2, v1

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/chrome/ntp/MostVisitedLayout;->init(IIIII)V

    .line 213
    sget v0, Lcom/google/android/apps/chrome/R$id;->search_provider_logo:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/ntp/LogoView;

    iput-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mSearchProviderLogoView:Lcom/google/android/apps/chrome/ntp/LogoView;

    .line 214
    sget v0, Lcom/google/android/apps/chrome/R$id;->search_box:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mSearchBoxView:Landroid/view/View;

    .line 215
    sget v0, Lcom/google/android/apps/chrome/R$id;->no_search_logo_spacer:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mNoSearchLogoSpacer:Landroid/view/View;

    .line 217
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mSearchBoxView:Landroid/view/View;

    sget v1, Lcom/google/android/apps/chrome/R$id;->search_box_text:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mSearchBoxTextView:Landroid/widget/TextView;

    .line 219
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mSearchBoxTextView:Landroid/widget/TextView;

    new-instance v1, Lcom/google/android/apps/chrome/ntp/NewTabPageView$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/ntp/NewTabPageView$1;-><init>(Lcom/google/android/apps/chrome/ntp/NewTabPageView;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 225
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mSearchBoxTextView:Landroid/widget/TextView;

    new-instance v1, Lcom/google/android/apps/chrome/ntp/NewTabPageView$2;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/ntp/NewTabPageView$2;-><init>(Lcom/google/android/apps/chrome/ntp/NewTabPageView;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 242
    sget v0, Lcom/google/android/apps/chrome/R$id;->voice_search_button:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 243
    new-instance v1, Lcom/google/android/apps/chrome/ntp/NewTabPageView$3;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/ntp/NewTabPageView$3;-><init>(Lcom/google/android/apps/chrome/ntp/NewTabPageView;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 250
    sget v0, Lcom/google/android/apps/chrome/R$id;->ntp_toolbar:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/ntp/NewTabPageToolbar;

    .line 251
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ntp/NewTabPageToolbar;->getRecentTabsButton()Landroid/view/View;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/chrome/ntp/NewTabPageView$4;

    invoke-direct {v2, p0}, Lcom/google/android/apps/chrome/ntp/NewTabPageView$4;-><init>(Lcom/google/android/apps/chrome/ntp/NewTabPageView;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 257
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ntp/NewTabPageToolbar;->getBookmarksButton()Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/chrome/ntp/NewTabPageView$5;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/ntp/NewTabPageView$5;-><init>(Lcom/google/android/apps/chrome/ntp/NewTabPageView;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 264
    invoke-direct {p0}, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->initializeSearchBoxScrollHandling()V

    .line 266
    iget v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mPendingLoadTasks:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mPendingLoadTasks:I

    .line 268
    sget v0, Lcom/google/android/apps/chrome/R$id;->ntp_content:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mContentView:Landroid/view/ViewGroup;

    .line 269
    invoke-virtual {p0, p0}, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 270
    return-void
.end method

.method public onLayoutChange(Landroid/view/View;IIIIIIII)V
    .locals 2

    .prologue
    .line 621
    sub-int v0, p8, p6

    .line 622
    sub-int v1, p4, p2

    .line 623
    if-ne v0, v1, :cond_0

    .line 628
    :goto_0
    return-void

    .line 627
    :cond_0
    iget v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mUrlFocusChangePercent:F

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->setUrlFocusChangeAnimationPercent(F)V

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 4

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    .line 575
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onMeasure(II)V

    .line 578
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mMostVisitedLayout:Lcom/google/android/apps/chrome/ntp/MostVisitedLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ntp/MostVisitedLayout;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-eq v0, v1, :cond_0

    .line 579
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mMostVisitedLayout:Lcom/google/android/apps/chrome/ntp/MostVisitedLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ntp/MostVisitedLayout;->getMeasuredWidth()I

    move-result v0

    invoke-static {v0, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 581
    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mSearchBoxView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    invoke-static {v1, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 583
    iget-object v2, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mSearchProviderLogoView:Lcom/google/android/apps/chrome/ntp/LogoView;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/ntp/LogoView;->getMeasuredHeight()I

    move-result v2

    invoke-static {v2, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 585
    iget-object v3, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mSearchBoxView:Landroid/view/View;

    invoke-virtual {v3, v0, v1}, Landroid/view/View;->measure(II)V

    .line 586
    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mSearchProviderLogoView:Lcom/google/android/apps/chrome/ntp/LogoView;

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/chrome/ntp/LogoView;->measure(II)V

    .line 588
    :cond_0
    return-void
.end method

.method public onMostVisitedURLsAvailable([Ljava/lang/String;[Ljava/lang/String;)V
    .locals 11

    .prologue
    .line 634
    const/4 v0, 0x0

    .line 635
    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mMostVisitedLayout:Lcom/google/android/apps/chrome/ntp/MostVisitedLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/ntp/MostVisitedLayout;->getChildCount()I

    move-result v4

    .line 636
    if-lez v4, :cond_0

    .line 637
    new-array v1, v4, [Lcom/google/android/apps/chrome/ntp/MostVisitedItem;

    .line 638
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v4, :cond_1

    .line 639
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mMostVisitedLayout:Lcom/google/android/apps/chrome/ntp/MostVisitedLayout;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/chrome/ntp/MostVisitedLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/ntp/MostVisitedItem;

    aput-object v0, v1, v2

    .line 638
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_0
    move-object v1, v0

    .line 643
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Lcom/google/android/apps/chrome/R$dimen;->most_visited_favicon_size:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 645
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mHasReceivedMostVisitedSites:Z

    if-nez v0, :cond_3

    const/4 v0, 0x1

    move v2, v0

    .line 646
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v6

    .line 649
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mMostVisitedLayout:Lcom/google/android/apps/chrome/ntp/MostVisitedLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ntp/MostVisitedLayout;->removeAllViews()V

    .line 652
    const/4 v0, 0x0

    move v3, v0

    :goto_2
    array-length v0, p1

    if-ge v3, v0, :cond_8

    .line 653
    aget-object v7, p2, v3

    .line 654
    aget-object v8, p1, v3

    .line 660
    const-string/jumbo v0, "chrome://welcome/"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 663
    const/4 v0, 0x0

    :goto_3
    if-ge v0, v4, :cond_5

    .line 664
    aget-object v9, v1, v0

    .line 665
    if-eqz v9, :cond_4

    invoke-virtual {v9}, Lcom/google/android/apps/chrome/ntp/MostVisitedItem;->getUrl()Ljava/lang/String;

    move-result-object v10

    invoke-static {v7, v10}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_4

    invoke-virtual {v9}, Lcom/google/android/apps/chrome/ntp/MostVisitedItem;->getTitle()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v10}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 667
    invoke-virtual {v9, v3}, Lcom/google/android/apps/chrome/ntp/MostVisitedItem;->setIndex(I)V

    .line 668
    iget-object v7, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mMostVisitedLayout:Lcom/google/android/apps/chrome/ntp/MostVisitedLayout;

    invoke-virtual {v7, v9}, Lcom/google/android/apps/chrome/ntp/MostVisitedLayout;->addView(Landroid/view/View;)V

    .line 669
    const/4 v7, 0x0

    aput-object v7, v1, v0

    .line 652
    :cond_2
    :goto_4
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_2

    .line 645
    :cond_3
    const/4 v0, 0x0

    move v2, v0

    goto :goto_1

    .line 663
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 675
    :cond_5
    sget v0, Lcom/google/android/apps/chrome/R$layout;->most_visited_item:I

    iget-object v9, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mMostVisitedLayout:Lcom/google/android/apps/chrome/ntp/MostVisitedLayout;

    const/4 v10, 0x0

    invoke-virtual {v6, v0, v9, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/ntp/MostVisitedItem;

    .line 677
    iget-object v9, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mManager:Lcom/google/android/apps/chrome/ntp/NewTabPageView$NewTabPageManager;

    invoke-virtual {v0, v9, v8, v7, v3}, Lcom/google/android/apps/chrome/ntp/MostVisitedItem;->init(Lcom/google/android/apps/chrome/ntp/MostVisitedItem$MostVisitedItemManager;Ljava/lang/String;Ljava/lang/String;I)V

    .line 678
    iget-object v8, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mMostVisitedLayout:Lcom/google/android/apps/chrome/ntp/MostVisitedLayout;

    invoke-virtual {v8, v0}, Lcom/google/android/apps/chrome/ntp/MostVisitedLayout;->addView(Landroid/view/View;)V

    .line 680
    new-instance v8, Lcom/google/android/apps/chrome/ntp/NewTabPageView$12;

    invoke-direct {v8, p0, v0, v2}, Lcom/google/android/apps/chrome/ntp/NewTabPageView$12;-><init>(Lcom/google/android/apps/chrome/ntp/NewTabPageView;Lcom/google/android/apps/chrome/ntp/MostVisitedItem;Z)V

    .line 688
    if-eqz v2, :cond_6

    iget v9, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mPendingLoadTasks:I

    add-int/lit8 v9, v9, 0x1

    iput v9, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mPendingLoadTasks:I

    .line 689
    :cond_6
    iget-object v9, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mManager:Lcom/google/android/apps/chrome/ntp/NewTabPageView$NewTabPageManager;

    invoke-interface {v9, v7, v8}, Lcom/google/android/apps/chrome/ntp/NewTabPageView$NewTabPageManager;->getURLThumbnail(Ljava/lang/String;Lorg/chromium/chrome/browser/profiles/MostVisitedSites$ThumbnailCallback;)V

    .line 691
    new-instance v8, Lcom/google/android/apps/chrome/ntp/NewTabPageView$13;

    invoke-direct {v8, p0, v7, v0, v2}, Lcom/google/android/apps/chrome/ntp/NewTabPageView$13;-><init>(Lcom/google/android/apps/chrome/ntp/NewTabPageView;Ljava/lang/String;Lcom/google/android/apps/chrome/ntp/MostVisitedItem;Z)V

    .line 702
    if-eqz v2, :cond_7

    iget v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mPendingLoadTasks:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mPendingLoadTasks:I

    .line 703
    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mManager:Lcom/google/android/apps/chrome/ntp/NewTabPageView$NewTabPageManager;

    invoke-interface {v0, v7, v5, v8}, Lcom/google/android/apps/chrome/ntp/NewTabPageView$NewTabPageManager;->getLocalFaviconImageForURL(Ljava/lang/String;ILorg/chromium/chrome/browser/favicon/FaviconHelper$FaviconImageCallback;)V

    goto :goto_4

    .line 706
    :cond_8
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mHasReceivedMostVisitedSites:Z

    .line 707
    invoke-direct {p0}, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->updateMostVisitedPlaceholderVisibility()V

    .line 709
    if-eqz v2, :cond_9

    .line 710
    invoke-direct {p0}, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->loadTaskCompleted()V

    .line 714
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mContentView:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 716
    :cond_9
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mSnapshotMostVisitedChanged:Z

    .line 717
    return-void
.end method

.method setSearchBoxScrollListener(Lcom/google/android/apps/chrome/ntp/NewTabPage$OnSearchBoxScrollListener;)V
    .locals 1

    .prologue
    .line 548
    iput-object p1, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mSearchBoxScrollListener:Lcom/google/android/apps/chrome/ntp/NewTabPage$OnSearchBoxScrollListener;

    .line 549
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mSearchBoxScrollListener:Lcom/google/android/apps/chrome/ntp/NewTabPage$OnSearchBoxScrollListener;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->updateSearchBoxOnScroll()V

    .line 550
    :cond_0
    return-void
.end method

.method public setSearchProviderHasLogo(Z)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 437
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mSearchProviderHasLogo:Z

    if-ne p1, v0, :cond_0

    .line 457
    :goto_0
    return-void

    .line 438
    :cond_0
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mSearchProviderHasLogo:Z

    .line 440
    if-nez p1, :cond_1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->setUrlFocusChangeAnimationPercentInternal(F)V

    .line 443
    :cond_1
    if-eqz p1, :cond_3

    move v0, v1

    .line 444
    :goto_1
    iget-object v2, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mContentView:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    .line 445
    :goto_2
    if-ge v1, v2, :cond_4

    .line 446
    iget-object v3, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mContentView:Landroid/view/ViewGroup;

    invoke-virtual {v3, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 447
    iget-object v4, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mMostVisitedLayout:Lcom/google/android/apps/chrome/ntp/MostVisitedLayout;

    if-eq v3, v4, :cond_4

    .line 449
    instance-of v4, v3, Landroid/view/ViewStub;

    if-nez v4, :cond_2

    .line 450
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 445
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 443
    :cond_3
    const/16 v0, 0x8

    goto :goto_1

    .line 453
    :cond_4
    invoke-direct {p0}, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->updateMostVisitedPlaceholderVisibility()V

    .line 455
    if-eqz p1, :cond_5

    iget v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mUrlFocusChangePercent:F

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->setUrlFocusChangeAnimationPercent(F)V

    .line 456
    :cond_5
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mSnapshotMostVisitedChanged:Z

    goto :goto_0
.end method

.method setUrlFocusAnimationsDisabled(Z)V
    .locals 1

    .prologue
    .line 464
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mDisableUrlFocusChangeAnimations:Z

    if-ne p1, v0, :cond_1

    .line 467
    :cond_0
    :goto_0
    return-void

    .line 465
    :cond_1
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mDisableUrlFocusChangeAnimations:Z

    .line 466
    if-nez p1, :cond_0

    iget v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mUrlFocusChangePercent:F

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->setUrlFocusChangeAnimationPercent(F)V

    goto :goto_0
.end method

.method setUrlFocusChangeAnimationPercent(F)V
    .locals 1

    .prologue
    .line 484
    iput p1, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mUrlFocusChangePercent:F

    .line 485
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mDisableUrlFocusChangeAnimations:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mSearchProviderHasLogo:Z

    if-eqz v0, :cond_0

    .line 486
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->setUrlFocusChangeAnimationPercentInternal(F)V

    .line 488
    :cond_0
    return-void
.end method

.method shouldCaptureThumbnail()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 595
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->getWidth()I

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->getHeight()I

    move-result v1

    if-nez v1, :cond_1

    .line 597
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-boolean v1, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mSnapshotMostVisitedChanged:Z

    if-nez v1, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->getWidth()I

    move-result v1

    iget v2, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mSnapshotWidth:I

    if-ne v1, v2, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->getHeight()I

    move-result v1

    iget v2, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mSnapshotHeight:I

    if-ne v1, v2, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mScrollView:Lcom/google/android/apps/chrome/ntp/NewTabScrollView;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/ntp/NewTabScrollView;->getScrollY()I

    move-result v1

    iget v2, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mSnapshotScrollY:I

    if-eq v1, v2, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method urlFocusAnimationsDisabled()Z
    .locals 1

    .prologue
    .line 473
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mDisableUrlFocusChangeAnimations:Z

    return v0
.end method
