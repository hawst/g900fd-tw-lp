.class Lcom/google/android/apps/chrome/ntp/BookmarksPageView$2;
.super Ljava/lang/Object;
.source "BookmarksPageView.java"

# interfaces
.implements Landroid/view/View$OnLayoutChangeListener;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/ntp/BookmarksPageView;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/ntp/BookmarksPageView;)V
    .locals 0

    .prologue
    .line 283
    iput-object p1, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPageView$2;->this$0:Lcom/google/android/apps/chrome/ntp/BookmarksPageView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLayoutChange(Landroid/view/View;IIIIIIII)V
    .locals 2

    .prologue
    .line 287
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPageView$2;->this$0:Lcom/google/android/apps/chrome/ntp/BookmarksPageView;

    # getter for: Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->mHierarchyLayout:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->access$300(Lcom/google/android/apps/chrome/ntp/BookmarksPageView;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->removeOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 288
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPageView$2;->this$0:Lcom/google/android/apps/chrome/ntp/BookmarksPageView;

    # getter for: Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->mHierarchyContainer:Landroid/widget/HorizontalScrollView;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->access$400(Lcom/google/android/apps/chrome/ntp/BookmarksPageView;)Landroid/widget/HorizontalScrollView;

    move-result-object v1

    invoke-static {}, Lorg/chromium/ui/base/LocalizationUtils;->isLayoutRtl()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x11

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/HorizontalScrollView;->fullScroll(I)Z

    .line 290
    return-void

    .line 288
    :cond_0
    const/16 v0, 0x42

    goto :goto_0
.end method
