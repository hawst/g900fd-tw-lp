.class final Lcom/google/android/apps/chrome/webapps/WebappManager$2;
.super Ljava/lang/Object;
.source "WebappManager.java"

# interfaces
.implements Lorg/chromium/chrome/browser/ShortcutHelper$OnInitialized;


# instance fields
.field final synthetic val$dialog:Landroid/app/AlertDialog;

.field final synthetic val$input:Landroid/widget/EditText;


# direct methods
.method constructor <init>(Landroid/app/AlertDialog;Landroid/widget/EditText;)V
    .locals 0

    .prologue
    .line 151
    iput-object p1, p0, Lcom/google/android/apps/chrome/webapps/WebappManager$2;->val$dialog:Landroid/app/AlertDialog;

    iput-object p2, p0, Lcom/google/android/apps/chrome/webapps/WebappManager$2;->val$input:Landroid/widget/EditText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onInitialized(Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 154
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/WebappManager$2;->val$dialog:Landroid/app/AlertDialog;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 156
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/WebappManager$2;->val$input:Landroid/widget/EditText;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 157
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/WebappManager$2;->val$input:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 158
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/WebappManager$2;->val$input:Landroid/widget/EditText;

    const/4 v1, 0x0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/EditText;->setSelection(II)V

    .line 159
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/WebappManager$2;->val$input:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 160
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/WebappManager$2;->val$input:Landroid/widget/EditText;

    new-instance v1, Lcom/google/android/apps/chrome/webapps/WebappManager$2$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/webapps/WebappManager$2$1;-><init>(Lcom/google/android/apps/chrome/webapps/WebappManager$2;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->post(Ljava/lang/Runnable;)Z

    .line 166
    return-void
.end method
