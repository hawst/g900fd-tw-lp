.class public abstract Lcom/google/android/apps/chrome/preferences/ChromeBaseSwitchEnabler;
.super Ljava/lang/Object;
.source "ChromeBaseSwitchEnabler.java"

# interfaces
.implements Lcom/google/android/apps/chrome/preferences/WidgetEnabler;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mSwitch:Landroid/widget/Switch;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/widget/Switch;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/google/android/apps/chrome/preferences/ChromeBaseSwitchEnabler;->mContext:Landroid/content/Context;

    .line 28
    iput-object p2, p0, Lcom/google/android/apps/chrome/preferences/ChromeBaseSwitchEnabler;->mSwitch:Landroid/widget/Switch;

    .line 29
    return-void
.end method

.method private updateSwitchValue()V
    .locals 3

    .prologue
    .line 66
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBaseSwitchEnabler;->isSwitchEnabled()Z

    move-result v0

    .line 67
    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/ChromeBaseSwitchEnabler;->mSwitch:Landroid/widget/Switch;

    invoke-virtual {v1, v0}, Landroid/widget/Switch;->setChecked(Z)V

    .line 68
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromeBaseSwitchEnabler;->mSwitch:Landroid/widget/Switch;

    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/ChromeBaseSwitchEnabler;->mContext:Landroid/content/Context;

    sget v2, Lcom/google/android/apps/chrome/R$style;->PreferenceHeaderSwitchText:I

    invoke-virtual {v0, v1, v2}, Landroid/widget/Switch;->setSwitchTextAppearance(Landroid/content/Context;I)V

    .line 69
    return-void
.end method


# virtual methods
.method public attach()V
    .locals 2

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromeBaseSwitchEnabler;->mSwitch:Landroid/widget/Switch;

    new-instance v1, Lcom/google/android/apps/chrome/preferences/ChromeBaseSwitchEnabler$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/preferences/ChromeBaseSwitchEnabler$1;-><init>(Lcom/google/android/apps/chrome/preferences/ChromeBaseSwitchEnabler;)V

    invoke-virtual {v0, v1}, Landroid/widget/Switch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 52
    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBaseSwitchEnabler;->updateSwitchValue()V

    .line 53
    return-void
.end method

.method public destroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 58
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromeBaseSwitchEnabler;->mSwitch:Landroid/widget/Switch;

    invoke-virtual {v0, v1}, Landroid/widget/Switch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 59
    iput-object v1, p0, Lcom/google/android/apps/chrome/preferences/ChromeBaseSwitchEnabler;->mSwitch:Landroid/widget/Switch;

    .line 60
    return-void
.end method

.method protected abstract isSwitchEnabled()Z
.end method

.method public abstract onValueChanged(Z)V
.end method
