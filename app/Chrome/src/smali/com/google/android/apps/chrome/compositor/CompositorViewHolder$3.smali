.class Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$3;
.super Ljava/lang/Object;
.source "CompositorViewHolder.java"

# interfaces
.implements Lcom/google/android/apps/chrome/tab/TransitionPageHelper$TransitionPageHelperListener;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;)V
    .locals 0

    .prologue
    .line 258
    iput-object p1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$3;->this$0:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTransitionPageCreated(Lcom/google/android/apps/chrome/tab/TransitionPageHelper;)V
    .locals 2

    .prologue
    .line 261
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$3;->this$0:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    # getter for: Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mTabVisible:Lcom/google/android/apps/chrome/tab/ChromeTab;
    invoke-static {v0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->access$100(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;)Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getTransitionPageHelper()Lcom/google/android/apps/chrome/tab/TransitionPageHelper;

    move-result-object v0

    if-eq p1, v0, :cond_0

    .line 267
    :goto_0
    return-void

    .line 263
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$3;->this$0:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->getTransitionContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v1

    # setter for: Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mTransitionPageViewCore:Lorg/chromium/content/browser/ContentViewCore;
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->access$502(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;Lorg/chromium/content/browser/ContentViewCore;)Lorg/chromium/content/browser/ContentViewCore;

    .line 264
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$3;->this$0:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    # invokes: Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->setupTransitionPageView()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->access$600(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;)V

    .line 265
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$3;->this$0:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    # getter for: Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mCompositorView:Lcom/google/android/apps/chrome/compositor/CompositorView;
    invoke-static {v0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->access$400(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;)Lcom/google/android/apps/chrome/compositor/CompositorView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/compositor/CompositorView;->getTransitionPageContainer()Lcom/google/android/apps/chrome/compositor/TransitionPageContainer;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$3;->this$0:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    # getter for: Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mTransitionPageViewCore:Lorg/chromium/content/browser/ContentViewCore;
    invoke-static {v1}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->access$500(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;)Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/compositor/TransitionPageContainer;->setContentViewCore(Lorg/chromium/content/browser/ContentViewCore;)V

    goto :goto_0
.end method

.method public onTransitionPageDestroyed(Lcom/google/android/apps/chrome/tab/TransitionPageHelper;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 271
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$3;->this$0:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    # getter for: Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mTabVisible:Lcom/google/android/apps/chrome/tab/ChromeTab;
    invoke-static {v0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->access$100(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;)Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getTransitionPageHelper()Lcom/google/android/apps/chrome/tab/TransitionPageHelper;

    move-result-object v0

    if-eq p1, v0, :cond_0

    .line 275
    :goto_0
    return-void

    .line 273
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$3;->this$0:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    # setter for: Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mTransitionPageViewCore:Lorg/chromium/content/browser/ContentViewCore;
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->access$502(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;Lorg/chromium/content/browser/ContentViewCore;)Lorg/chromium/content/browser/ContentViewCore;

    .line 274
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$3;->this$0:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    # getter for: Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mCompositorView:Lcom/google/android/apps/chrome/compositor/CompositorView;
    invoke-static {v0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->access$400(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;)Lcom/google/android/apps/chrome/compositor/CompositorView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/compositor/CompositorView;->getTransitionPageContainer()Lcom/google/android/apps/chrome/compositor/TransitionPageContainer;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/compositor/TransitionPageContainer;->setContentViewCore(Lorg/chromium/content/browser/ContentViewCore;)V

    goto :goto_0
.end method

.method public onTransitionPageVisibility(Lcom/google/android/apps/chrome/tab/TransitionPageHelper;Z)V
    .locals 1

    .prologue
    .line 279
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$3;->this$0:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    # getter for: Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mTabVisible:Lcom/google/android/apps/chrome/tab/ChromeTab;
    invoke-static {v0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->access$100(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;)Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getTransitionPageHelper()Lcom/google/android/apps/chrome/tab/TransitionPageHelper;

    move-result-object v0

    if-eq p1, v0, :cond_0

    .line 282
    :goto_0
    return-void

    .line 281
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$3;->this$0:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    # getter for: Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mCompositorView:Lcom/google/android/apps/chrome/compositor/CompositorView;
    invoke-static {v0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->access$400(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;)Lcom/google/android/apps/chrome/compositor/CompositorView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/compositor/CompositorView;->getTransitionPageContainer()Lcom/google/android/apps/chrome/compositor/TransitionPageContainer;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/apps/chrome/compositor/TransitionPageContainer;->setVisible(Z)V

    goto :goto_0
.end method
