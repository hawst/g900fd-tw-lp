.class Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl$1;
.super Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;
.source "PasswordAuthenticationDelegateImpl.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 62
    const-class v0, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl$1;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl;)V
    .locals 0

    .prologue
    .line 62
    iput-object p1, p0, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl$1;->this$0:Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 65
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 88
    sget-boolean v0, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl$1;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Unexpected notification type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 67
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl$1;->this$0:Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl;

    # setter for: Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl;->mOverviewModeShown:Z
    invoke-static {v0, v2}, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl;->access$002(Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl;Z)Z

    .line 68
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "activity_hashcode"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 69
    iget-object v0, p0, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl$1;->this$0:Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl;

    # getter for: Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl;->mOverviewModePerActivityMap:Landroid/util/SparseBooleanArray;
    invoke-static {v0}, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl;->access$100(Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl;)Landroid/util/SparseBooleanArray;

    move-result-object v1

    monitor-enter v1

    .line 70
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl$1;->this$0:Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl;

    # getter for: Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl;->mOverviewModePerActivityMap:Landroid/util/SparseBooleanArray;
    invoke-static {v0}, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl;->access$100(Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl;)Landroid/util/SparseBooleanArray;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    const-string/jumbo v3, "activity_hashcode"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 72
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 91
    :cond_0
    return-void

    .line 72
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 76
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl$1;->this$0:Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl;

    # setter for: Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl;->mOverviewModeShown:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl;->access$002(Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl;Z)Z

    .line 77
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "activity_hashcode"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 78
    iget-object v0, p0, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl$1;->this$0:Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl;

    # getter for: Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl;->mOverviewModePerActivityMap:Landroid/util/SparseBooleanArray;
    invoke-static {v0}, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl;->access$100(Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl;)Landroid/util/SparseBooleanArray;

    move-result-object v1

    monitor-enter v1

    .line 79
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl$1;->this$0:Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl;

    # getter for: Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl;->mOverviewModePerActivityMap:Landroid/util/SparseBooleanArray;
    invoke-static {v0}, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl;->access$100(Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl;)Landroid/util/SparseBooleanArray;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    const-string/jumbo v3, "activity_hashcode"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 81
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 83
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl$1;->this$0:Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl;

    # getter for: Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl;->mPendingTabCallbacks:Lorg/chromium/base/ObserverList;
    invoke-static {v0}, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl;->access$200(Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl;)Lorg/chromium/base/ObserverList;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/base/ObserverList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl$TabCallbackHandler;

    .line 84
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/passwordmanager/PasswordAuthenticationDelegateImpl$TabCallbackHandler;->triggerAuthenticationIfApplicable()V

    goto :goto_0

    .line 81
    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0

    .line 65
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
