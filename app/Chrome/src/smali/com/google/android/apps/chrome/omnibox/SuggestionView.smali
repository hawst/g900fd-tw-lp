.class Lcom/google/android/apps/chrome/omnibox/SuggestionView;
.super Landroid/view/ViewGroup;
.source "SuggestionView.java"


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final TITLE_COLOR_STANDARD_FONT_DARK:I

.field private static final TITLE_COLOR_STANDARD_FONT_LIGHT:I

.field private static final URL_COLOR:I


# instance fields
.field private mContentsView:Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;

.field private mCorpusChipTextView:Landroid/widget/TextView;

.field private mLocationBar:Lcom/google/android/apps/chrome/omnibox/LocationBar;

.field private mNavigationButton:Landroid/widget/ImageView;

.field private mPosition:I

.field private mRefineIcon:Landroid/graphics/drawable/Drawable;

.field private mRefineView:Landroid/view/View;

.field private mRefineWidth:I

.field private mSuggestion:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;

.field private mSuggestionAnswerHeight:I

.field private mSuggestionDelegate:Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter$OmniboxSuggestionDelegate;

.field private mSuggestionHeight:I

.field private mSuggestionItem:Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter$OmniboxResultItem;

.field private mUrlBar:Lcom/google/android/apps/chrome/omnibox/UrlBar;

.field private mUseDarkColors:Z

.field private final mViewPositionHolder:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/16 v2, 0xff

    const/16 v1, 0x33

    .line 47
    const-class v0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->$assertionsDisabled:Z

    .line 61
    invoke-static {v1, v1, v1}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    sput v0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->TITLE_COLOR_STANDARD_FONT_DARK:I

    .line 62
    invoke-static {v2, v2, v2}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    sput v0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->TITLE_COLOR_STANDARD_FONT_LIGHT:I

    .line 63
    const/16 v0, 0x55

    const/16 v1, 0x95

    const/16 v2, 0xfe

    invoke-static {v0, v1, v2}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    sput v0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->URL_COLOR:I

    return-void

    .line 47
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/chrome/omnibox/LocationBar;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 98
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 89
    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mViewPositionHolder:[I

    .line 99
    iput-object p2, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mLocationBar:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    .line 101
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$dimen;->omnibox_suggestion_height:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mSuggestionHeight:I

    .line 103
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$dimen;->omnibox_suggestion_answer_height:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mSuggestionAnswerHeight:I

    .line 107
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->getContext()Landroid/content/Context;

    move-result-object v0

    new-array v1, v5, [I

    const v2, 0x101030e

    aput v2, v1, v4

    invoke-virtual {v0, v1}, Landroid/content/Context;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 109
    invoke-virtual {v0, v4}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 110
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 112
    new-instance v0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;

    invoke-direct {v0, p0, p1, v1}, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;-><init>(Lcom/google/android/apps/chrome/omnibox/SuggestionView;Landroid/content/Context;Landroid/graphics/drawable/Drawable;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mContentsView:Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;

    .line 113
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mContentsView:Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->addView(Landroid/view/View;)V

    .line 115
    new-instance v0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$1;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/chrome/omnibox/SuggestionView$1;-><init>(Lcom/google/android/apps/chrome/omnibox/SuggestionView;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mRefineView:Landroid/view/View;

    .line 151
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mRefineView:Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->getContext()Landroid/content/Context;

    move-result-object v2

    sget v3, Lcom/google/android/apps/chrome/R$string;->accessibility_omnibox_btn_refine:I

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 157
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mRefineView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable$ConstantState;->newDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-static {v0, v1}, Lorg/chromium/base/ApiCompatibilityUtils;->setBackgroundForView(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 159
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mRefineView:Landroid/view/View;

    sget v1, Lcom/google/android/apps/chrome/R$id;->refine_view_id:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setId(I)V

    .line 160
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mRefineView:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setClickable(Z)V

    .line 161
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mRefineView:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setFocusable(Z)V

    .line 162
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mRefineView:Landroid/view/View;

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v1, v4, v4}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 163
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mRefineView:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->addView(Landroid/view/View;)V

    .line 165
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    const/high16 v1, 0x42400000    # 48.0f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mRefineWidth:I

    .line 167
    sget v0, Lcom/google/android/apps/chrome/R$id;->url_bar:I

    invoke-virtual {p2, v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/omnibox/UrlBar;

    iput-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mUrlBar:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    .line 168
    sget v0, Lcom/google/android/apps/chrome/R$id;->corpus_chip_text_view:I

    invoke-virtual {p2, v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mCorpusChipTextView:Landroid/widget/TextView;

    .line 169
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/omnibox/SuggestionView;)Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mRefineIcon:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/google/android/apps/chrome/omnibox/SuggestionView;)Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mContentsView:Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/google/android/apps/chrome/omnibox/SuggestionView;)Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mSuggestion:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/google/android/apps/chrome/omnibox/SuggestionView;)I
    .locals 1

    .prologue
    .line 47
    iget v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mPosition:I

    return v0
.end method

.method static synthetic access$1300(Lcom/google/android/apps/chrome/omnibox/SuggestionView;)Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter$OmniboxSuggestionDelegate;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mSuggestionDelegate:Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter$OmniboxSuggestionDelegate;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/google/android/apps/chrome/omnibox/SuggestionView;)I
    .locals 1

    .prologue
    .line 47
    iget v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mSuggestionHeight:I

    return v0
.end method

.method static synthetic access$1600(Lcom/google/android/apps/chrome/omnibox/SuggestionView;)I
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->getStandardFontColor()I

    move-result v0

    return v0
.end method

.method static synthetic access$1700(Lcom/google/android/apps/chrome/omnibox/SuggestionView;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mNavigationButton:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$1702(Lcom/google/android/apps/chrome/omnibox/SuggestionView;Landroid/widget/ImageView;)Landroid/widget/ImageView;
    .locals 0

    .prologue
    .line 47
    iput-object p1, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mNavigationButton:Landroid/widget/ImageView;

    return-object p1
.end method

.method static synthetic access$1800(Lcom/google/android/apps/chrome/omnibox/SuggestionView;)Lcom/google/android/apps/chrome/omnibox/UrlBar;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mUrlBar:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    return-object v0
.end method

.method static synthetic access$1802(Lcom/google/android/apps/chrome/omnibox/SuggestionView;Lcom/google/android/apps/chrome/omnibox/UrlBar;)Lcom/google/android/apps/chrome/omnibox/UrlBar;
    .locals 0

    .prologue
    .line 47
    iput-object p1, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mUrlBar:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    return-object p1
.end method

.method static synthetic access$1900(Lcom/google/android/apps/chrome/omnibox/SuggestionView;)Lcom/google/android/apps/chrome/omnibox/LocationBar;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mLocationBar:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/google/android/apps/chrome/omnibox/SuggestionView;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mCorpusChipTextView:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$2100(Lcom/google/android/apps/chrome/omnibox/SuggestionView;)I
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->getCorpusChipTextViewLeftPosition()I

    move-result v0

    return v0
.end method

.method static synthetic access$2200(Lcom/google/android/apps/chrome/omnibox/SuggestionView;)I
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->getUrlBarTextLeftPosition()I

    move-result v0

    return v0
.end method

.method static synthetic access$2300(Lcom/google/android/apps/chrome/omnibox/SuggestionView;)[I
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mViewPositionHolder:[I

    return-object v0
.end method

.method static synthetic access$2400(Lcom/google/android/apps/chrome/omnibox/SuggestionView;)I
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->getCorpusChipTextViewRightPosition()I

    move-result v0

    return v0
.end method

.method static synthetic access$2500(Lcom/google/android/apps/chrome/omnibox/SuggestionView;)I
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->getUrlBarTextRightPosition()I

    move-result v0

    return v0
.end method

.method static synthetic access$2600(Lcom/google/android/apps/chrome/omnibox/SuggestionView;)Z
    .locals 1

    .prologue
    .line 47
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mUseDarkColors:Z

    return v0
.end method

.method private getCorpusChipTextViewLeftPosition()I
    .locals 2

    .prologue
    .line 585
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mCorpusChipTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mViewPositionHolder:[I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->getLocationOnScreen([I)V

    .line 586
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mViewPositionHolder:[I

    const/4 v1, 0x0

    aget v0, v0, v1

    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mCorpusChipTextView:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getPaddingLeft()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method private getCorpusChipTextViewRightPosition()I
    .locals 2

    .prologue
    .line 593
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mCorpusChipTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mViewPositionHolder:[I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->getLocationOnScreen([I)V

    .line 594
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mViewPositionHolder:[I

    const/4 v1, 0x0

    aget v0, v0, v1

    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mCorpusChipTextView:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getWidth()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mCorpusChipTextView:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getPaddingRight()I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method private getStandardFontColor()I
    .locals 1

    .prologue
    .line 365
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mUseDarkColors:Z

    if-eqz v0, :cond_0

    sget v0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->TITLE_COLOR_STANDARD_FONT_DARK:I

    :goto_0
    return v0

    :cond_0
    sget v0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->TITLE_COLOR_STANDARD_FONT_LIGHT:I

    goto :goto_0
.end method

.method private getUrlBarTextLeftPosition()I
    .locals 2

    .prologue
    .line 569
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mUrlBar:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mViewPositionHolder:[I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->getLocationOnScreen([I)V

    .line 570
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mViewPositionHolder:[I

    const/4 v1, 0x0

    aget v0, v0, v1

    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mUrlBar:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->getPaddingLeft()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method private getUrlBarTextRightPosition()I
    .locals 2

    .prologue
    .line 577
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mUrlBar:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mViewPositionHolder:[I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->getLocationOnScreen([I)V

    .line 578
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mViewPositionHolder:[I

    const/4 v1, 0x0

    aget v0, v0, v1

    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mUrlBar:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->getWidth()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mUrlBar:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->getPaddingRight()I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method private setAnswer(Lorg/chromium/chrome/browser/omnibox/SuggestionAnswer;)V
    .locals 4

    .prologue
    .line 509
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    .line 511
    invoke-virtual {p1}, Lorg/chromium/chrome/browser/omnibox/SuggestionAnswer;->getFirstLine()Lorg/chromium/chrome/browser/omnibox/SuggestionAnswer$ImageLine;

    move-result-object v1

    .line 512
    iget-object v2, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mContentsView:Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;

    # getter for: Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mTextLine1:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->access$200(Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;)Landroid/widget/TextView;

    move-result-object v2

    invoke-static {v1}, Lcom/google/android/apps/chrome/omnibox/AnswerTextBuilder;->getMaxTextHeightSp(Lorg/chromium/chrome/browser/omnibox/SuggestionAnswer$ImageLine;)I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextSize(F)V

    .line 513
    iget-object v2, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mContentsView:Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;

    # getter for: Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mTextLine1:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->access$200(Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;)Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v2

    invoke-virtual {v2}, Landroid/text/TextPaint;->getFontMetrics()Landroid/graphics/Paint$FontMetrics;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/chrome/omnibox/AnswerTextBuilder;->buildSpannable(Lorg/chromium/chrome/browser/omnibox/SuggestionAnswer$ImageLine;Landroid/graphics/Paint$FontMetrics;F)Landroid/text/Spannable;

    move-result-object v1

    .line 515
    iget-object v2, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mContentsView:Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;

    # getter for: Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mTextLine1:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->access$200(Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;)Landroid/widget/TextView;

    move-result-object v2

    sget-object v3, Landroid/widget/TextView$BufferType;->SPANNABLE:Landroid/widget/TextView$BufferType;

    invoke-virtual {v2, v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    .line 517
    invoke-virtual {p1}, Lorg/chromium/chrome/browser/omnibox/SuggestionAnswer;->getSecondLine()Lorg/chromium/chrome/browser/omnibox/SuggestionAnswer$ImageLine;

    move-result-object v1

    .line 518
    iget-object v2, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mContentsView:Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;

    # getter for: Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mTextLine2:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->access$500(Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;)Landroid/widget/TextView;

    move-result-object v2

    invoke-static {v1}, Lcom/google/android/apps/chrome/omnibox/AnswerTextBuilder;->getMaxTextHeightSp(Lorg/chromium/chrome/browser/omnibox/SuggestionAnswer$ImageLine;)I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextSize(F)V

    .line 519
    iget-object v2, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mContentsView:Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;

    # getter for: Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mTextLine2:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->access$500(Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;)Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v2

    invoke-virtual {v2}, Landroid/text/TextPaint;->getFontMetrics()Landroid/graphics/Paint$FontMetrics;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/chrome/omnibox/AnswerTextBuilder;->buildSpannable(Lorg/chromium/chrome/browser/omnibox/SuggestionAnswer$ImageLine;Landroid/graphics/Paint$FontMetrics;F)Landroid/text/Spannable;

    move-result-object v0

    .line 521
    iget-object v2, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mContentsView:Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;

    # getter for: Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mTextLine2:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->access$500(Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;)Landroid/widget/TextView;

    move-result-object v2

    sget-object v3, Landroid/widget/TextView$BufferType;->SPANNABLE:Landroid/widget/TextView$BufferType;

    invoke-virtual {v2, v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    .line 523
    invoke-virtual {v1}, Lorg/chromium/chrome/browser/omnibox/SuggestionAnswer$ImageLine;->hasImage()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 524
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mContentsView:Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;

    # getter for: Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mAnswerImage:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->access$300(Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;)Landroid/widget/ImageView;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 526
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mContentsView:Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;

    # getter for: Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mTextLine2:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->access$500(Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/TextView;->getTextSize()F

    move-result v0

    .line 527
    const v2, 0x3f933333    # 1.15f

    mul-float/2addr v0, v2

    float-to-int v0, v0

    .line 528
    iget-object v2, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mContentsView:Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;

    # getter for: Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mAnswerImage:Landroid/widget/ImageView;
    invoke-static {v2}, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->access$300(Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;)Landroid/widget/ImageView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    iput v0, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 529
    iget-object v2, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mContentsView:Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;

    # getter for: Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mAnswerImage:Landroid/widget/ImageView;
    invoke-static {v2}, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->access$300(Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;)Landroid/widget/ImageView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    iput v0, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 530
    iget-object v2, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mContentsView:Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;

    # setter for: Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mAnswerImageMaxSize:I
    invoke-static {v2, v0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->access$402(Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;I)I

    .line 532
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "https:"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lorg/chromium/chrome/browser/omnibox/SuggestionAnswer$ImageLine;->getImage()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "\\/"

    const-string/jumbo v3, "/"

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 533
    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mLocationBar:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->getCurrentTab()Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getProfile()Lorg/chromium/chrome/browser/profiles/Profile;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/chrome/omnibox/SuggestionView$3;

    invoke-direct {v2, p0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView$3;-><init>(Lcom/google/android/apps/chrome/omnibox/SuggestionView;)V

    invoke-static {v1, v0, v2}, Lorg/chromium/chrome/browser/omnibox/AnswersImage;->requestAnswersImage(Lorg/chromium/chrome/browser/profiles/Profile;Ljava/lang/String;Lorg/chromium/chrome/browser/omnibox/AnswersImage$AnswersImageObserver;)I

    .line 543
    :cond_0
    return-void
.end method

.method private setRefinable(ZZ)V
    .locals 2

    .prologue
    .line 346
    if-eqz p1, :cond_0

    .line 347
    invoke-direct {p0, p2}, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->setRefineIcon(Z)V

    .line 348
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mRefineView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 349
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mRefineView:Landroid/view/View;

    new-instance v1, Lcom/google/android/apps/chrome/omnibox/SuggestionView$2;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView$2;-><init>(Lcom/google/android/apps/chrome/omnibox/SuggestionView;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 362
    :goto_0
    return-void

    .line 359
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mRefineView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 360
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mRefineView:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private setRefineIcon(Z)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 377
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mRefineIcon:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 387
    :goto_0
    return-void

    .line 379
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mUseDarkColors:Z

    if-eqz v0, :cond_1

    sget v0, Lcom/google/android/apps/chrome/R$drawable;->btn_suggestion_refine:I

    .line 381
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mRefineIcon:Landroid/graphics/drawable/Drawable;

    .line 382
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mRefineIcon:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mRefineIcon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mRefineIcon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 386
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mRefineView:Landroid/view/View;

    invoke-static {v0}, Lorg/chromium/base/ApiCompatibilityUtils;->postInvalidateOnAnimation(Landroid/view/View;)V

    goto :goto_0

    .line 379
    :cond_1
    sget v0, Lcom/google/android/apps/chrome/R$drawable;->btn_suggestion_refine_white:I

    goto :goto_1
.end method

.method private setSuggestedQuery(Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter$OmniboxResultItem;ZZZ)V
    .locals 8

    .prologue
    const/4 v3, -0x1

    const/4 v7, 0x0

    .line 434
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter$OmniboxResultItem;->getMatchedQuery()Ljava/lang/String;

    move-result-object v1

    .line 435
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter$OmniboxResultItem;->getSuggestion()Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;

    move-result-object v2

    .line 437
    if-eqz p2, :cond_0

    invoke-virtual {v2}, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;->getUrl()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {v2}, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;->getDescription()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 439
    invoke-virtual {v2}, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;->getDescription()Ljava/lang/String;

    move-result-object v0

    .line 443
    :goto_0
    if-nez v0, :cond_6

    .line 444
    sget-boolean v0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    const-string/jumbo v1, "Invalid suggestion sent with no displayable text"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 441
    :cond_0
    invoke-virtual {v2}, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;->getDisplayText()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 445
    :cond_1
    const-string/jumbo v0, ""

    .line 452
    :cond_2
    :goto_1
    iget-object v2, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mSuggestion:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;

    invoke-virtual {v2}, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;->getType()Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

    move-result-object v2

    sget-object v4, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;->SEARCH_SUGGEST_INFINITE:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

    if-ne v2, v4, :cond_4

    .line 453
    iget-object v2, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mSuggestion:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;

    invoke-virtual {v2}, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;->getFillIntoEdit()Ljava/lang/String;

    move-result-object v4

    .line 455
    invoke-virtual {v4, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {v4, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    if-ge v2, v5, :cond_4

    .line 458
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    sub-int/2addr v2, v5

    invoke-virtual {v4, v7, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    .line 460
    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "\u2026 "

    invoke-direct {v2, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 462
    invoke-virtual {v1, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 463
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "\u2026 "

    invoke-direct {v0, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v1, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 465
    :goto_2
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lorg/chromium/ui/base/DeviceFormFactor;->isTablet(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 466
    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mContentsView:Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;

    # getter for: Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mTextLine1:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->access$200(Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v1

    .line 467
    iget-object v5, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mContentsView:Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v6

    invoke-virtual {v1, v4, v7, v6}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;II)F

    move-result v4

    # setter for: Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mRequiredWidth:F
    invoke-static {v5, v4}, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->access$802(Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;F)F

    .line 469
    iget-object v4, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mContentsView:Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v1, v2, v7, v5}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;II)F

    move-result v1

    # setter for: Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mMatchContentsWidth:F
    invoke-static {v4, v1}, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->access$902(Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;F)F

    .line 474
    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mSuggestionDelegate:Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter$OmniboxSuggestionDelegate;

    iget-object v4, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mContentsView:Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;

    # getter for: Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mRequiredWidth:F
    invoke-static {v4}, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->access$800(Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;)F

    move-result v4

    iget-object v5, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mContentsView:Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;

    # getter for: Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mMatchContentsWidth:F
    invoke-static {v5}, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->access$900(Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;)F

    move-result v5

    invoke-interface {v1, v4, v5}, Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter$OmniboxSuggestionDelegate;->onTextWidthsUpdated(FF)V

    :cond_3
    move-object v1, v0

    move-object v0, v2

    .line 480
    :cond_4
    invoke-static {v0}, Landroid/text/SpannableString;->valueOf(Ljava/lang/CharSequence;)Landroid/text/SpannableString;

    move-result-object v4

    .line 481
    if-eqz p4, :cond_7

    move v2, v3

    .line 483
    :goto_3
    if-eq v2, v3, :cond_5

    .line 484
    if-eqz p3, :cond_8

    .line 488
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    add-int/2addr v0, v2

    move v1, v2

    .line 493
    :goto_4
    if-eq v1, v0, :cond_5

    .line 494
    new-instance v2, Landroid/text/style/StyleSpan;

    const/4 v3, 0x1

    invoke-direct {v2, v3}, Landroid/text/style/StyleSpan;-><init>(I)V

    const/16 v3, 0x21

    invoke-interface {v4, v2, v1, v0, v3}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 500
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mContentsView:Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;

    # getter for: Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mTextLine1:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->access$200(Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;)Landroid/widget/TextView;

    move-result-object v0

    sget-object v1, Landroid/widget/TextView$BufferType;->SPANNABLE:Landroid/widget/TextView$BufferType;

    invoke-virtual {v0, v4, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    .line 501
    return-void

    .line 446
    :cond_6
    invoke-virtual {v2}, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;->getUrl()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 449
    invoke-virtual {v2}, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;->getFormattedUrl()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 481
    :cond_7
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    goto :goto_3

    .line 490
    :cond_8
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    add-int v1, v2, v0

    .line 491
    invoke-interface {v4}, Landroid/text/Spannable;->length()I

    move-result v0

    goto :goto_4

    :cond_9
    move-object v0, v1

    goto/16 :goto_2
.end method

.method private setUrlText(Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter$OmniboxResultItem;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 396
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter$OmniboxResultItem;->getMatchedQuery()Ljava/lang/String;

    move-result-object v1

    .line 397
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter$OmniboxResultItem;->getSuggestion()Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;

    move-result-object v2

    invoke-virtual {v2}, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;->getFormattedUrl()Ljava/lang/String;

    move-result-object v2

    .line 398
    invoke-virtual {v2, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    .line 399
    invoke-static {v2}, Landroid/text/SpannableString;->valueOf(Ljava/lang/CharSequence;)Landroid/text/SpannableString;

    move-result-object v2

    .line 400
    if-ltz v3, :cond_0

    .line 402
    new-instance v4, Landroid/text/style/StyleSpan;

    invoke-direct {v4, v0}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/2addr v1, v3

    const/16 v5, 0x21

    invoke-interface {v2, v4, v3, v1, v5}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 405
    :cond_0
    sget v1, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->URL_COLOR:I

    invoke-direct {p0, v2, v1}, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->showDescriptionLine(Landroid/text/Spannable;I)V

    .line 406
    if-ltz v3, :cond_1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private showDescriptionLine(Landroid/text/Spannable;I)V
    .locals 2

    .prologue
    .line 415
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mContentsView:Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;

    # getter for: Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mTextLine2:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->access$500(Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    .line 416
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mContentsView:Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;

    # getter for: Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mTextLine2:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->access$500(Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;)Landroid/widget/TextView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 418
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mContentsView:Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;

    # getter for: Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mTextLine2:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->access$500(Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 419
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mContentsView:Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;

    # getter for: Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mTextLine2:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->access$500(Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;)Landroid/widget/TextView;

    move-result-object v0

    sget-object v1, Landroid/widget/TextView$BufferType;->SPANNABLE:Landroid/widget/TextView$BufferType;

    invoke-virtual {v0, p1, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    .line 420
    return-void
.end method


# virtual methods
.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 234
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mSuggestionDelegate:Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter$OmniboxSuggestionDelegate;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter$OmniboxSuggestionDelegate;->onGestureDown()V

    .line 235
    :cond_0
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public init(Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter$OmniboxResultItem;Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter$OmniboxSuggestionDelegate;IZ)V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 250
    iput p3, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mPosition:I

    .line 251
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->jumpDrawablesToCurrentState()V

    .line 252
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mSuggestionItem:Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter$OmniboxResultItem;

    invoke-virtual {p1, v0}, Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter$OmniboxResultItem;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mUseDarkColors:Z

    if-ne v0, p4, :cond_1

    .line 343
    :cond_0
    :goto_0
    return-void

    .line 253
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mUseDarkColors:Z

    if-eq v0, p4, :cond_3

    move v0, v1

    .line 254
    :goto_1
    iput-boolean p4, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mUseDarkColors:Z

    .line 255
    if-eqz v0, :cond_2

    iget-object v3, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mContentsView:Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;

    # getter for: Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mTextLine1:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->access$200(Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;)Landroid/widget/TextView;

    move-result-object v3

    invoke-direct {p0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->getStandardFontColor()I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 257
    :cond_2
    iput-object p1, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mSuggestionItem:Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter$OmniboxResultItem;

    .line 258
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter$OmniboxResultItem;->getSuggestion()Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mSuggestion:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;

    .line 259
    iput-object p2, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mSuggestionDelegate:Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter$OmniboxSuggestionDelegate;

    .line 261
    iget-object v3, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mContentsView:Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;

    # invokes: Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->resetTextWidths()V
    invoke-static {v3}, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->access$100(Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;)V

    .line 262
    iget-object v3, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mContentsView:Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;

    # getter for: Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mAnswerImage:Landroid/widget/ImageView;
    invoke-static {v3}, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->access$300(Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;)Landroid/widget/ImageView;

    move-result-object v3

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 263
    iget-object v3, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mContentsView:Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;

    # getter for: Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mAnswerImage:Landroid/widget/ImageView;
    invoke-static {v3}, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->access$300(Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;)Landroid/widget/ImageView;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    iput v2, v3, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 264
    iget-object v3, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mContentsView:Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;

    # getter for: Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mAnswerImage:Landroid/widget/ImageView;
    invoke-static {v3}, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->access$300(Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;)Landroid/widget/ImageView;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    iput v2, v3, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 265
    iget-object v3, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mContentsView:Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;

    # getter for: Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mAnswerImage:Landroid/widget/ImageView;
    invoke-static {v3}, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->access$300(Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;)Landroid/widget/ImageView;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 266
    iget-object v3, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mContentsView:Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;

    # setter for: Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mAnswerImageMaxSize:I
    invoke-static {v3, v2}, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->access$402(Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;I)I

    .line 267
    iget-object v3, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mContentsView:Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;

    # getter for: Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mTextLine1:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->access$200(Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;)Landroid/widget/TextView;

    move-result-object v3

    const/high16 v4, 0x41880000    # 17.0f

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextSize(F)V

    .line 268
    iget-object v3, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mContentsView:Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;

    # getter for: Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mTextLine2:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->access$500(Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;)Landroid/widget/TextView;

    move-result-object v3

    const/high16 v4, 0x41600000    # 14.0f

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextSize(F)V

    .line 272
    iget-object v3, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mSuggestion:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;

    invoke-virtual {v3}, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;->hasAnswer()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 273
    iget-object v3, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mSuggestion:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;

    invoke-virtual {v3}, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;->getAnswer()Lorg/chromium/chrome/browser/omnibox/SuggestionAnswer;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->setAnswer(Lorg/chromium/chrome/browser/omnibox/SuggestionAnswer;)V

    .line 274
    iget-object v3, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mContentsView:Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;

    sget-object v4, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionIconType;->MAGNIFIER:Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionIconType;

    # invokes: Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->setSuggestionIcon(Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionIconType;Z)V
    invoke-static {v3, v4, v0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->access$600(Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionIconType;Z)V

    .line 275
    iget-object v3, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mContentsView:Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;

    # getter for: Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mTextLine2:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->access$500(Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;)Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 276
    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->setRefinable(ZZ)V

    goto/16 :goto_0

    :cond_3
    move v0, v2

    .line 253
    goto/16 :goto_1

    .line 280
    :cond_4
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter$OmniboxResultItem;->getMatchedQuery()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mSuggestion:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;

    invoke-virtual {v4}, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;->getDisplayText()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    .line 282
    iget-object v3, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mSuggestion:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;

    invoke-virtual {v3}, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;->getType()Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

    move-result-object v4

    .line 283
    sget-object v3, Lcom/google/android/apps/chrome/omnibox/SuggestionView$4;->$SwitchMap$org$chromium$chrome$browser$omnibox$OmniboxSuggestion$Type:[I

    invoke-virtual {v4}, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;->ordinal()I

    move-result v6

    aget v3, v3, v6

    packed-switch v3, :pswitch_data_0

    .line 340
    sget-boolean v0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Suggestion type ("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mSuggestion:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;

    invoke-virtual {v2}, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;->getType()Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ") is not handled"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 291
    :pswitch_0
    iget-object v3, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mSuggestion:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;

    invoke-virtual {v3}, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;->isStarred()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 292
    iget-object v3, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mContentsView:Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;

    sget-object v4, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionIconType;->BOOKMARK:Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionIconType;

    # invokes: Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->setSuggestionIcon(Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionIconType;Z)V
    invoke-static {v3, v4, v0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->access$600(Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionIconType;Z)V

    .line 298
    :goto_2
    iget-object v3, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mSuggestion:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;

    invoke-virtual {v3}, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;->getUrl()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_7

    move v4, v1

    .line 300
    :goto_3
    if-eqz v4, :cond_8

    .line 301
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->setUrlText(Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter$OmniboxResultItem;)Z

    move-result v3

    .line 305
    :goto_4
    invoke-direct {p0, p1, v1, v4, v3}, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->setSuggestedQuery(Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter$OmniboxResultItem;ZZZ)V

    .line 306
    if-nez v5, :cond_9

    :goto_5
    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->setRefinable(ZZ)V

    goto/16 :goto_0

    .line 293
    :cond_5
    sget-object v3, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;->HISTORY_URL:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

    if-ne v4, v3, :cond_6

    .line 294
    iget-object v3, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mContentsView:Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;

    sget-object v4, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionIconType;->HISTORY:Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionIconType;

    # invokes: Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->setSuggestionIcon(Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionIconType;Z)V
    invoke-static {v3, v4, v0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->access$600(Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionIconType;Z)V

    goto :goto_2

    .line 296
    :cond_6
    iget-object v3, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mContentsView:Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;

    sget-object v4, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionIconType;->GLOBE:Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionIconType;

    # invokes: Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->setSuggestionIcon(Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionIconType;Z)V
    invoke-static {v3, v4, v0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->access$600(Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionIconType;Z)V

    goto :goto_2

    :cond_7
    move v4, v2

    .line 298
    goto :goto_3

    .line 303
    :cond_8
    iget-object v3, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mContentsView:Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;

    # getter for: Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mTextLine2:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->access$500(Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;)Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setVisibility(I)V

    move v3, v2

    goto :goto_4

    :cond_9
    move v1, v2

    .line 306
    goto :goto_5

    .line 319
    :pswitch_1
    sget-object v3, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionIconType;->MAGNIFIER:Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionIconType;

    .line 320
    sget-object v6, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;->VOICE_SUGGEST:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

    if-ne v4, v6, :cond_c

    .line 321
    sget-object v3, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionIconType;->VOICE:Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionIconType;

    .line 327
    :cond_a
    :goto_6
    iget-object v6, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mContentsView:Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;

    # invokes: Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->setSuggestionIcon(Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionIconType;Z)V
    invoke-static {v6, v3, v0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->access$600(Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionIconType;Z)V

    .line 328
    if-nez v5, :cond_e

    :goto_7
    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->setRefinable(ZZ)V

    .line 329
    invoke-direct {p0, p1, v2, v2, v2}, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->setSuggestedQuery(Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter$OmniboxResultItem;ZZZ)V

    .line 330
    sget-object v0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;->SEARCH_SUGGEST_ENTITY:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

    if-eq v4, v0, :cond_b

    sget-object v0, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;->SEARCH_SUGGEST_PROFILE:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

    if-ne v4, v0, :cond_f

    .line 332
    :cond_b
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mSuggestion:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;->getDescription()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/SpannableString;->valueOf(Ljava/lang/CharSequence;)Landroid/text/SpannableString;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->getStandardFontColor()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->showDescriptionLine(Landroid/text/Spannable;I)V

    goto/16 :goto_0

    .line 322
    :cond_c
    sget-object v6, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;->SEARCH_SUGGEST_PERSONALIZED:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

    if-eq v4, v6, :cond_d

    sget-object v6, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;->SEARCH_HISTORY:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

    if-ne v4, v6, :cond_a

    .line 325
    :cond_d
    sget-object v3, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionIconType;->HISTORY:Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionIconType;

    goto :goto_6

    :cond_e
    move v1, v2

    .line 328
    goto :goto_7

    .line 336
    :cond_f
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mContentsView:Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;

    # getter for: Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mTextLine2:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->access$500(Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0

    .line 283
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public invalidate()V
    .locals 1

    .prologue
    .line 226
    invoke-super {p0}, Landroid/view/ViewGroup;->invalidate()V

    .line 227
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mContentsView:Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->invalidate()V

    .line 228
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 173
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->getMeasuredWidth()I

    move-result v0

    if-nez v0, :cond_0

    .line 194
    :goto_0
    return-void

    .line 175
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mSuggestion:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;->getType()Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

    move-result-object v0

    sget-object v2, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;->SEARCH_SUGGEST_INFINITE:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

    if-eq v0, v2, :cond_1

    .line 176
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mContentsView:Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;

    # invokes: Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->resetTextWidths()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->access$100(Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;)V

    .line 179
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mRefineView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    .line 180
    :goto_1
    invoke-static {p0}, Lorg/chromium/base/ApiCompatibilityUtils;->isLayoutRtl(Landroid/view/View;)Z

    move-result v3

    .line 181
    if-eqz v3, :cond_3

    iget v2, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mRefineWidth:I

    .line 182
    :goto_2
    if-nez v0, :cond_5

    move v0, v1

    .line 183
    :goto_3
    iget-object v2, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mContentsView:Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;

    iget-object v4, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mContentsView:Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v4, v0

    iget-object v5, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mContentsView:Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;

    invoke-virtual {v5}, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->getMeasuredHeight()I

    move-result v5

    invoke-virtual {v2, v0, v1, v4, v5}, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->layout(IIII)V

    .line 188
    if-eqz v3, :cond_4

    move v0, v1

    .line 189
    :goto_4
    iget-object v2, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mRefineView:Landroid/view/View;

    iget v3, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mRefineWidth:I

    add-int/2addr v3, v0

    iget-object v4, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mContentsView:Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->getMeasuredHeight()I

    move-result v4

    invoke-virtual {v2, v0, v1, v3, v4}, Landroid/view/View;->layout(IIII)V

    goto :goto_0

    :cond_2
    move v0, v1

    .line 179
    goto :goto_1

    :cond_3
    move v2, v1

    .line 181
    goto :goto_2

    .line 188
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->getMeasuredWidth()I

    move-result v0

    iget v2, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mRefineWidth:I

    sub-int/2addr v0, v2

    goto :goto_4

    :cond_5
    move v0, v2

    goto :goto_3
.end method

.method protected onMeasure(II)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/high16 v4, 0x40000000    # 2.0f

    .line 198
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    .line 199
    iget v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mSuggestionHeight:I

    .line 200
    iget-object v2, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mSuggestion:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;

    invoke-virtual {v2}, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;->getAnswerContents()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 201
    iget v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mSuggestionAnswerHeight:I

    .line 203
    :cond_0
    invoke-virtual {p0, v3, v0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->setMeasuredDimension(II)V

    .line 207
    if-nez v3, :cond_1

    .line 222
    :goto_0
    return-void

    .line 209
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mRefineView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v2

    if-nez v2, :cond_3

    const/4 v2, 0x1

    .line 210
    :goto_1
    if-eqz v2, :cond_2

    iget v1, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mRefineWidth:I

    .line 211
    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mContentsView:Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;

    sub-int v1, v3, v1

    invoke-static {v1, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-static {v0, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-virtual {v2, v1, v3}, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->measure(II)V

    .line 214
    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mContentsView:Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mContentsView:Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->getMeasuredWidth()I

    move-result v2

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 215
    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mContentsView:Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mContentsView:Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->getMeasuredHeight()I

    move-result v2

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 217
    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mRefineView:Landroid/view/View;

    iget v2, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mRefineWidth:I

    invoke-static {v2, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-static {v0, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-virtual {v1, v2, v0}, Landroid/view/View;->measure(II)V

    .line 220
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mRefineView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mRefineView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 221
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mRefineView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mRefineView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    goto :goto_0

    :cond_3
    move v2, v1

    .line 209
    goto :goto_1
.end method

.method public setSelected(Z)V
    .locals 2

    .prologue
    .line 370
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->setSelected(Z)V

    .line 371
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->isInTouchMode()Z

    move-result v0

    if-nez v0, :cond_0

    .line 372
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mSuggestionDelegate:Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter$OmniboxSuggestionDelegate;

    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mSuggestion:Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;

    invoke-interface {v0, v1}, Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter$OmniboxSuggestionDelegate;->onSetUrlToSuggestion(Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;)V

    .line 374
    :cond_0
    return-void
.end method
