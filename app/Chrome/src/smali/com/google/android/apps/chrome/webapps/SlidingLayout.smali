.class public Lcom/google/android/apps/chrome/webapps/SlidingLayout;
.super Landroid/view/ViewGroup;
.source "SlidingLayout.java"


# static fields
.field public static final ANIMATION_STATE_ANIMATING:I = 0x3

.field public static final ANIMATION_STATE_DONE:I = 0x0

.field public static final ANIMATION_STATE_PREPARING:I = 0x1

.field public static final ANIMATION_STATE_READY:I = 0x2


# instance fields
.field private mAnimation:Landroid/animation/AnimatorSet;

.field private mAnimationState:I

.field private final mContentView:Landroid/view/View;

.field private mListeners:Ljava/util/List;

.field private final mUrlBar:Lcom/google/android/apps/chrome/webapps/WebappUrlBar;

.field private mUrlBarVisibility:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/chrome/webapps/WebappUrlBar;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 93
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 68
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/webapps/SlidingLayout;->mListeners:Ljava/util/List;

    .line 94
    iput-object p3, p0, Lcom/google/android/apps/chrome/webapps/SlidingLayout;->mContentView:Landroid/view/View;

    .line 95
    iput-object p2, p0, Lcom/google/android/apps/chrome/webapps/SlidingLayout;->mUrlBar:Lcom/google/android/apps/chrome/webapps/WebappUrlBar;

    .line 96
    invoke-virtual {p0, p3}, Lcom/google/android/apps/chrome/webapps/SlidingLayout;->addView(Landroid/view/View;)V

    .line 97
    invoke-virtual {p0, p2}, Lcom/google/android/apps/chrome/webapps/SlidingLayout;->addView(Landroid/view/View;)V

    .line 98
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/webapps/SlidingLayout;)I
    .locals 1

    .prologue
    .line 53
    iget v0, p0, Lcom/google/android/apps/chrome/webapps/SlidingLayout;->mAnimationState:I

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/webapps/SlidingLayout;)Landroid/view/View;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/SlidingLayout;->mContentView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/webapps/SlidingLayout;I)V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/webapps/SlidingLayout;->updateAnimationState(I)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/apps/chrome/webapps/SlidingLayout;)V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/google/android/apps/chrome/webapps/SlidingLayout;->startAnimation()V

    return-void
.end method

.method static synthetic access$402(Lcom/google/android/apps/chrome/webapps/SlidingLayout;Landroid/animation/AnimatorSet;)Landroid/animation/AnimatorSet;
    .locals 0

    .prologue
    .line 53
    iput-object p1, p0, Lcom/google/android/apps/chrome/webapps/SlidingLayout;->mAnimation:Landroid/animation/AnimatorSet;

    return-object p1
.end method

.method private animateVisibilityChange()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 166
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/SlidingLayout;->mAnimation:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/SlidingLayout;->mAnimation:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->cancel()V

    .line 167
    :cond_0
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/webapps/SlidingLayout;->mAnimation:Landroid/animation/AnimatorSet;

    .line 168
    invoke-direct {p0, v4}, Lcom/google/android/apps/chrome/webapps/SlidingLayout;->updateAnimationState(I)V

    .line 170
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/SlidingLayout;->mUrlBar:Lcom/google/android/apps/chrome/webapps/WebappUrlBar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/webapps/WebappUrlBar;->getTranslationY()F

    move-result v1

    .line 171
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/webapps/SlidingLayout;->mUrlBarVisibility:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    .line 172
    :goto_0
    const-string/jumbo v2, "translationY"

    new-array v3, v6, [F

    aput v1, v3, v5

    aput v0, v3, v4

    invoke-static {v2, v3}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v0

    .line 175
    iget-object v1, p0, Lcom/google/android/apps/chrome/webapps/SlidingLayout;->mUrlBar:Lcom/google/android/apps/chrome/webapps/WebappUrlBar;

    new-array v2, v4, [Landroid/animation/PropertyValuesHolder;

    aput-object v0, v2, v5

    invoke-static {v1, v2}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 176
    iget-object v2, p0, Lcom/google/android/apps/chrome/webapps/SlidingLayout;->mContentView:Landroid/view/View;

    new-array v3, v4, [Landroid/animation/PropertyValuesHolder;

    aput-object v0, v3, v5

    invoke-static {v2, v3}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 179
    iget-boolean v2, p0, Lcom/google/android/apps/chrome/webapps/SlidingLayout;->mUrlBarVisibility:Z

    if-eqz v2, :cond_2

    .line 180
    iget-object v2, p0, Lcom/google/android/apps/chrome/webapps/SlidingLayout;->mAnimation:Landroid/animation/AnimatorSet;

    invoke-virtual {v2, v0}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet$Builder;->after(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 184
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/SlidingLayout;->mAnimation:Landroid/animation/AnimatorSet;

    const-wide/16 v2, 0xfa

    invoke-virtual {v0, v2, v3}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 185
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/SlidingLayout;->mAnimation:Landroid/animation/AnimatorSet;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/webapps/SlidingLayout;->createAnimatorListener()Landroid/animation/AnimatorListenerAdapter;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 187
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/SlidingLayout;->mContentView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/webapps/SlidingLayout;->getHeight()I

    move-result v1

    if-ne v0, v1, :cond_3

    .line 188
    invoke-direct {p0, v6}, Lcom/google/android/apps/chrome/webapps/SlidingLayout;->updateAnimationState(I)V

    .line 189
    invoke-direct {p0}, Lcom/google/android/apps/chrome/webapps/SlidingLayout;->startAnimation()V

    .line 194
    :goto_2
    return-void

    .line 171
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/SlidingLayout;->mUrlBar:Lcom/google/android/apps/chrome/webapps/WebappUrlBar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/webapps/WebappUrlBar;->getHeight()I

    move-result v0

    neg-int v0, v0

    int-to-float v0, v0

    goto :goto_0

    .line 182
    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/chrome/webapps/SlidingLayout;->mAnimation:Landroid/animation/AnimatorSet;

    invoke-virtual {v2, v0}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet$Builder;->before(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    goto :goto_1

    .line 191
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/SlidingLayout;->mContentView:Landroid/view/View;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/webapps/SlidingLayout;->createLayoutChangeListener()Landroid/view/View$OnLayoutChangeListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 192
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/webapps/SlidingLayout;->requestLayout()V

    goto :goto_2
.end method

.method private createAnimatorListener()Landroid/animation/AnimatorListenerAdapter;
    .locals 1

    .prologue
    .line 217
    new-instance v0, Lcom/google/android/apps/chrome/webapps/SlidingLayout$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/webapps/SlidingLayout$2;-><init>(Lcom/google/android/apps/chrome/webapps/SlidingLayout;)V

    return-object v0
.end method

.method private createLayoutChangeListener()Landroid/view/View$OnLayoutChangeListener;
    .locals 1

    .prologue
    .line 204
    new-instance v0, Lcom/google/android/apps/chrome/webapps/SlidingLayout$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/webapps/SlidingLayout$1;-><init>(Lcom/google/android/apps/chrome/webapps/SlidingLayout;)V

    return-object v0
.end method

.method private startAnimation()V
    .locals 4

    .prologue
    .line 197
    iget v0, p0, Lcom/google/android/apps/chrome/webapps/SlidingLayout;->mAnimationState:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 201
    :goto_0
    return-void

    .line 198
    :cond_0
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/webapps/SlidingLayout;->updateAnimationState(I)V

    .line 199
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/webapps/SlidingLayout;->mUrlBarVisibility:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/SlidingLayout;->mAnimation:Landroid/animation/AnimatorSet;

    const-wide/16 v2, 0xfa

    invoke-virtual {v0, v2, v3}, Landroid/animation/AnimatorSet;->setStartDelay(J)V

    .line 200
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/SlidingLayout;->mAnimation:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    goto :goto_0
.end method

.method private updateAnimationState(I)V
    .locals 2

    .prologue
    .line 129
    iput p1, p0, Lcom/google/android/apps/chrome/webapps/SlidingLayout;->mAnimationState:I

    .line 130
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/SlidingLayout;->mListeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 131
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/SlidingLayout;->mListeners:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/webapps/SlidingLayout$AnimationListener;

    invoke-interface {v0, p1}, Lcom/google/android/apps/chrome/webapps/SlidingLayout$AnimationListener;->onAnimationStateChanged(I)V

    .line 130
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 133
    :cond_0
    return-void
.end method


# virtual methods
.method public addAnimationListener(Lcom/google/android/apps/chrome/webapps/SlidingLayout$AnimationListener;)V
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/SlidingLayout;->mListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 106
    return-void
.end method

.method isUrlBarVisibleForTests(Z)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 229
    iget v2, p0, Lcom/google/android/apps/chrome/webapps/SlidingLayout;->mAnimationState:I

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/chrome/webapps/SlidingLayout;->mUrlBar:Lcom/google/android/apps/chrome/webapps/WebappUrlBar;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/webapps/WebappUrlBar;->getHeight()I

    move-result v2

    int-to-float v2, v2

    cmpl-float v2, v2, v3

    if-nez v2, :cond_2

    :cond_0
    move v0, v1

    .line 234
    :cond_1
    :goto_0
    return v0

    .line 231
    :cond_2
    if-eqz p1, :cond_4

    .line 232
    iget-boolean v2, p0, Lcom/google/android/apps/chrome/webapps/SlidingLayout;->mUrlBarVisibility:Z

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/chrome/webapps/SlidingLayout;->mUrlBar:Lcom/google/android/apps/chrome/webapps/WebappUrlBar;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/webapps/WebappUrlBar;->getTranslationY()F

    move-result v2

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_1

    :cond_3
    move v0, v1

    goto :goto_0

    .line 234
    :cond_4
    iget-boolean v2, p0, Lcom/google/android/apps/chrome/webapps/SlidingLayout;->mUrlBarVisibility:Z

    if-nez v2, :cond_5

    iget-object v2, p0, Lcom/google/android/apps/chrome/webapps/SlidingLayout;->mUrlBar:Lcom/google/android/apps/chrome/webapps/WebappUrlBar;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/webapps/WebappUrlBar;->getTranslationY()F

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/chrome/webapps/SlidingLayout;->mUrlBar:Lcom/google/android/apps/chrome/webapps/WebappUrlBar;

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/webapps/WebappUrlBar;->getHeight()I

    move-result v3

    neg-int v3, v3

    int-to-float v3, v3

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_1

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 154
    iget-object v1, p0, Lcom/google/android/apps/chrome/webapps/SlidingLayout;->mUrlBar:Lcom/google/android/apps/chrome/webapps/WebappUrlBar;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/webapps/WebappUrlBar;->getMeasuredHeight()I

    move-result v1

    .line 155
    iget-object v2, p0, Lcom/google/android/apps/chrome/webapps/SlidingLayout;->mUrlBar:Lcom/google/android/apps/chrome/webapps/WebappUrlBar;

    invoke-virtual {v2, v0, v0, p4, v1}, Lcom/google/android/apps/chrome/webapps/WebappUrlBar;->layout(IIII)V

    .line 156
    iget-object v2, p0, Lcom/google/android/apps/chrome/webapps/SlidingLayout;->mContentView:Landroid/view/View;

    iget-object v3, p0, Lcom/google/android/apps/chrome/webapps/SlidingLayout;->mContentView:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    add-int/2addr v3, v1

    invoke-virtual {v2, v0, v1, p4, v3}, Landroid/view/View;->layout(IIII)V

    .line 158
    iget v2, p0, Lcom/google/android/apps/chrome/webapps/SlidingLayout;->mAnimationState:I

    if-nez v2, :cond_0

    .line 159
    iget-boolean v2, p0, Lcom/google/android/apps/chrome/webapps/SlidingLayout;->mUrlBarVisibility:Z

    if-eqz v2, :cond_1

    .line 160
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/webapps/SlidingLayout;->mUrlBar:Lcom/google/android/apps/chrome/webapps/WebappUrlBar;

    int-to-float v2, v0

    invoke-virtual {v1, v2}, Lcom/google/android/apps/chrome/webapps/WebappUrlBar;->setTranslationY(F)V

    .line 161
    iget-object v1, p0, Lcom/google/android/apps/chrome/webapps/SlidingLayout;->mContentView:Landroid/view/View;

    int-to-float v0, v0

    invoke-virtual {v1, v0}, Landroid/view/View;->setTranslationY(F)V

    .line 163
    :cond_0
    return-void

    .line 159
    :cond_1
    neg-int v0, v1

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 6

    .prologue
    const/high16 v5, 0x40000000    # 2.0f

    const/4 v0, 0x0

    .line 137
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 138
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    .line 139
    invoke-static {v1, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 140
    invoke-virtual {p0, v1, v2}, Lcom/google/android/apps/chrome/webapps/SlidingLayout;->setMeasuredDimension(II)V

    .line 142
    invoke-static {v0, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 143
    iget-object v4, p0, Lcom/google/android/apps/chrome/webapps/SlidingLayout;->mUrlBar:Lcom/google/android/apps/chrome/webapps/WebappUrlBar;

    invoke-virtual {p0, v4, v3, v1}, Lcom/google/android/apps/chrome/webapps/SlidingLayout;->measureChild(Landroid/view/View;II)V

    .line 145
    iget-boolean v1, p0, Lcom/google/android/apps/chrome/webapps/SlidingLayout;->mUrlBarVisibility:Z

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/google/android/apps/chrome/webapps/SlidingLayout;->mAnimationState:I

    if-eqz v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    .line 146
    :goto_0
    if-eqz v1, :cond_2

    :goto_1
    sub-int v0, v2, v0

    .line 147
    invoke-static {v0, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 148
    iget-object v1, p0, Lcom/google/android/apps/chrome/webapps/SlidingLayout;->mContentView:Landroid/view/View;

    invoke-virtual {p0, v1, v3, v0}, Lcom/google/android/apps/chrome/webapps/SlidingLayout;->measureChild(Landroid/view/View;II)V

    .line 149
    return-void

    :cond_1
    move v1, v0

    .line 145
    goto :goto_0

    .line 146
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/SlidingLayout;->mUrlBar:Lcom/google/android/apps/chrome/webapps/WebappUrlBar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/webapps/WebappUrlBar;->getMeasuredHeight()I

    move-result v0

    goto :goto_1
.end method

.method public removeAnimationListener(Lcom/google/android/apps/chrome/webapps/SlidingLayout$AnimationListener;)V
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/SlidingLayout;->mListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 114
    return-void
.end method

.method setUrlBarVisibility(Z)Z
    .locals 1

    .prologue
    .line 122
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/webapps/SlidingLayout;->mUrlBarVisibility:Z

    if-ne v0, p1, :cond_0

    const/4 v0, 0x0

    .line 125
    :goto_0
    return v0

    .line 123
    :cond_0
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/webapps/SlidingLayout;->mUrlBarVisibility:Z

    .line 124
    invoke-direct {p0}, Lcom/google/android/apps/chrome/webapps/SlidingLayout;->animateVisibilityChange()V

    .line 125
    const/4 v0, 0x1

    goto :goto_0
.end method
