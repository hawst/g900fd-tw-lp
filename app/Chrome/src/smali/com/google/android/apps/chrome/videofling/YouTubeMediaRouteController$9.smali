.class Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$9;
.super Ljava/lang/Object;
.source "YouTubeMediaRouteController.java"

# interfaces
.implements Lcom/google/android/gms/cast/k;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;

.field final synthetic val$controller:Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;)V
    .locals 0

    .prologue
    .line 652
    iput-object p1, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$9;->this$0:Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;

    iput-object p2, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$9;->val$controller:Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMessageReceived(Lcom/google/android/gms/cast/CastDevice;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 658
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$9;->this$0:Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;

    # getter for: Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mDebug:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->access$1100(Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "YouTubeMediaRouteController"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "onMessageReceived(): "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 659
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$9;->this$0:Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;

    # getter for: Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mConnectedClient:Lcom/google/android/gms/common/api/i;
    invoke-static {v0}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->access$300(Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;)Lcom/google/android/gms/common/api/i;

    move-result-object v0

    if-nez v0, :cond_2

    .line 677
    :cond_1
    :goto_0
    return-void

    .line 661
    :cond_2
    const-string/jumbo v0, "urn:x-cast:com.google.youtube.mdx"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 663
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p3}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 664
    const-string/jumbo v1, "data"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 665
    const-string/jumbo v1, "screenId"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 666
    new-instance v1, Lcom/google/android/apps/chrome/videofling/YouTubePairingCodeGetter;

    iget-object v2, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$9;->this$0:Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;

    # getter for: Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mCastDevice:Lcom/google/android/gms/cast/CastDevice;
    invoke-static {v2}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->access$1200(Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;)Lcom/google/android/gms/cast/CastDevice;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/cast/CastDevice;->c()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$9;->val$controller:Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;

    invoke-direct {v1, v0, v2, v3}, Lcom/google/android/apps/chrome/videofling/YouTubePairingCodeGetter;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/chrome/videofling/YouTubePairingCodeGetter$Delegate;)V

    .line 671
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Void;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/videofling/YouTubePairingCodeGetter;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 672
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$9;->this$0:Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;

    # invokes: Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->setupRemoteMediaPlayer()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->access$1300(Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 674
    :catch_0
    move-exception v0

    const-string/jumbo v0, "YouTubeMediaRouteController"

    const-string/jumbo v1, "Can\'t parse the message with the screen id"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
