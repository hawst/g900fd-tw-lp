.class public Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory$ParcelUtils;
.super Ljava/lang/Object;
.source "NetworkStatsHistory.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 713
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static readLongArray(Landroid/os/Parcel;)[J
    .locals 4

    .prologue
    .line 715
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 716
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    .line 721
    :goto_0
    return-object v0

    .line 717
    :cond_0
    new-array v1, v0, [J

    .line 718
    const/4 v0, 0x0

    :goto_1
    array-length v2, v1

    if-ge v0, v2, :cond_1

    .line 719
    invoke-virtual {p0}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    aput-wide v2, v1, v0

    .line 718
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    move-object v0, v1

    .line 721
    goto :goto_0
.end method

.method public static writeLongArray(Landroid/os/Parcel;[JI)V
    .locals 4

    .prologue
    .line 725
    if-nez p1, :cond_1

    .line 726
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 736
    :cond_0
    return-void

    .line 729
    :cond_1
    array-length v0, p1

    if-le p2, v0, :cond_2

    .line 730
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "size larger than length"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 732
    :cond_2
    invoke-virtual {p0, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 733
    const/4 v0, 0x0

    :goto_0
    if-ge v0, p2, :cond_0

    .line 734
    aget-wide v2, p1, v0

    invoke-virtual {p0, v2, v3}, Landroid/os/Parcel;->writeLong(J)V

    .line 733
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
