.class final Lcom/google/android/apps/chrome/firstrun/FirstRunUtil$2;
.super Ljava/lang/Object;
.source "FirstRunUtil.java"

# interfaces
.implements Lorg/chromium/chrome/browser/signin/SigninManager$SignInFlowObserver;


# instance fields
.field final synthetic val$account:Landroid/accounts/Account;

.field final synthetic val$activity:Landroid/app/Activity;

.field final synthetic val$observer:Lorg/chromium/chrome/browser/signin/SigninManager$SignInFlowObserver;

.field final synthetic val$printCallback:Ljava/lang/Runnable;

.field final synthetic val$signInSync:I

.field final synthetic val$signInType:I


# direct methods
.method constructor <init>(Landroid/app/Activity;ILandroid/accounts/Account;Lorg/chromium/chrome/browser/signin/SigninManager$SignInFlowObserver;ILjava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 95
    iput-object p1, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunUtil$2;->val$activity:Landroid/app/Activity;

    iput p2, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunUtil$2;->val$signInSync:I

    iput-object p3, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunUtil$2;->val$account:Landroid/accounts/Account;

    iput-object p4, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunUtil$2;->val$observer:Lorg/chromium/chrome/browser/signin/SigninManager$SignInFlowObserver;

    iput p5, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunUtil$2;->val$signInType:I

    iput-object p6, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunUtil$2;->val$printCallback:Ljava/lang/Runnable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onSigninCancelled()V
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunUtil$2;->val$observer:Lorg/chromium/chrome/browser/signin/SigninManager$SignInFlowObserver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunUtil$2;->val$observer:Lorg/chromium/chrome/browser/signin/SigninManager$SignInFlowObserver;

    invoke-interface {v0}, Lorg/chromium/chrome/browser/signin/SigninManager$SignInFlowObserver;->onSigninCancelled()V

    .line 124
    :cond_0
    return-void
.end method

.method public final onSigninComplete()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 100
    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->update()V

    .line 104
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunUtil$2;->val$activity:Landroid/app/Activity;

    invoke-static {v0}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->get(Landroid/content/Context;)Lorg/chromium/chrome/browser/sync/ProfileSyncService;

    move-result-object v3

    iget v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunUtil$2;->val$signInSync:I

    if-nez v0, :cond_3

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->setSetupInProgress(Z)V

    .line 106
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunUtil$2;->val$activity:Landroid/app/Activity;

    invoke-static {v0}, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->get(Landroid/content/Context;)Lcom/google/android/apps/chrome/services/GoogleServicesManager;

    move-result-object v0

    invoke-static {}, Lcom/google/android/apps/chrome/services/GoogleServicesStates;->create()Lcom/google/android/apps/chrome/services/GoogleServicesStates$GoogleServicesStatesBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/google/android/apps/chrome/services/GoogleServicesStates$GoogleServicesStatesBuilder;->sync(Z)Lcom/google/android/apps/chrome/services/GoogleServicesStates$GoogleServicesStatesBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/google/android/apps/chrome/services/GoogleServicesStates$GoogleServicesStatesBuilder;->autoLogin(Z)Lcom/google/android/apps/chrome/services/GoogleServicesStates$GoogleServicesStatesBuilder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/services/GoogleServicesStates$GoogleServicesStatesBuilder;->build()Lcom/google/android/apps/chrome/services/GoogleServicesStates;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunUtil$2;->val$account:Landroid/accounts/Account;

    invoke-virtual {v0, v1, v3}, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->setStates(Lcom/google/android/apps/chrome/services/GoogleServicesStates;Landroid/accounts/Account;)V

    .line 109
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunUtil$2;->val$observer:Lorg/chromium/chrome/browser/signin/SigninManager$SignInFlowObserver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunUtil$2;->val$observer:Lorg/chromium/chrome/browser/signin/SigninManager$SignInFlowObserver;

    invoke-interface {v0}, Lorg/chromium/chrome/browser/signin/SigninManager$SignInFlowObserver;->onSigninComplete()V

    .line 111
    :cond_0
    iget v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunUtil$2;->val$signInType:I

    if-eqz v0, :cond_1

    .line 112
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunUtil$2;->val$activity:Landroid/app/Activity;

    invoke-static {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->setSignOutAllowed(Z)V

    .line 115
    :cond_1
    iget v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunUtil$2;->val$signInType:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 116
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunUtil$2;->val$activity:Landroid/app/Activity;

    invoke-static {v0}, Lcom/google/android/apps/chrome/services/ChildAccountManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/services/ChildAccountManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/services/ChildAccountManager;->onChildAccountSigninComplete()V

    .line 119
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunUtil$2;->val$activity:Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunUtil$2;->val$account:Landroid/accounts/Account;

    iget-object v2, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunUtil$2;->val$printCallback:Ljava/lang/Runnable;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/firstrun/FirstRunUtil;->setupCloudPrint(Landroid/app/Activity;Landroid/accounts/Account;Ljava/lang/Runnable;)V

    .line 120
    return-void

    :cond_3
    move v0, v2

    .line 104
    goto :goto_0
.end method
