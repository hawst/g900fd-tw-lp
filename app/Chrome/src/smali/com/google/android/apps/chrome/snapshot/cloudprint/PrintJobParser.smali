.class public final Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJobParser;
.super Ljava/lang/Object;
.source "PrintJobParser.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 97
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 98
    return-void
.end method

.method private static parseJobData(Lorg/json/JSONArray;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob$JobData;
    .locals 3

    .prologue
    .line 85
    if-eqz p0, :cond_1

    .line 86
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, Lorg/json/JSONArray;->length()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 87
    invoke-virtual {p0, v0}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 88
    const-string/jumbo v2, "__c2dm__job_data="

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 89
    const/16 v0, 0x11

    invoke-virtual {v1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 90
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJobParser;->parsePrintJobJobData(Lorg/json/JSONObject;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob$JobData;

    move-result-object v0

    .line 94
    :goto_1
    return-object v0

    .line 86
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 94
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static parsePrintJob(Lorg/json/JSONObject;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob;
    .locals 8

    .prologue
    .line 51
    :try_start_0
    const-string/jumbo v0, "createTime"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 52
    const-string/jumbo v0, "id"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 53
    const-string/jumbo v0, "title"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 54
    const-string/jumbo v0, "fileUrl"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 55
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    .line 56
    const-string/jumbo v0, "contentType"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 57
    const-string/jumbo v0, "tags"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    .line 58
    invoke-static {v0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJobParser;->parseJobData(Lorg/json/JSONArray;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob$JobData;

    move-result-object v7

    .line 59
    new-instance v0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob;

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob;-><init>(Ljava/lang/String;JLjava/lang/String;Landroid/net/Uri;Ljava/lang/String;Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob$JobData;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 62
    :goto_0
    return-object v0

    .line 60
    :catch_0
    move-exception v0

    .line 61
    const-string/jumbo v1, "PrintJobParser"

    const-string/jumbo v2, "Failed to parse print job"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 62
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parsePrintJobJobData(Lorg/json/JSONObject;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob$JobData;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 68
    if-nez p0, :cond_0

    .line 80
    :goto_0
    return-object v0

    .line 71
    :cond_0
    :try_start_0
    const-string/jumbo v1, "snapID"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string/jumbo v1, "snapID"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object v3, v1

    .line 73
    :goto_1
    const-string/jumbo v1, "url"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string/jumbo v1, "url"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 75
    :goto_2
    const-string/jumbo v2, "type"

    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 76
    if-nez v1, :cond_3

    move-object v2, v0

    .line 77
    :goto_3
    new-instance v1, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob$JobData;

    invoke-direct {v1, v3, v2, v4}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob$JobData;-><init>(Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;)V

    move-object v0, v1

    goto :goto_0

    :cond_1
    move-object v3, v0

    .line 71
    goto :goto_1

    :cond_2
    move-object v1, v0

    .line 73
    goto :goto_2

    .line 76
    :cond_3
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    move-object v2, v1

    goto :goto_3

    .line 78
    :catch_0
    move-exception v1

    .line 79
    const-string/jumbo v2, "PrintJobParser"

    const-string/jumbo v3, "Failed to parse print job"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
