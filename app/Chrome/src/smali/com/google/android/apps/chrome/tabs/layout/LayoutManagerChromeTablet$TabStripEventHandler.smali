.class Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet$TabStripEventHandler;
.super Ljava/lang/Object;
.source "LayoutManagerChromeTablet.java"

# interfaces
.implements Lcom/google/android/apps/chrome/eventfilter/GestureHandler;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet;)V
    .locals 0

    .prologue
    .line 244
    iput-object p1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet$TabStripEventHandler;->this$0:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet;Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet$1;)V
    .locals 0

    .prologue
    .line 244
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet$TabStripEventHandler;-><init>(Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet;)V

    return-void
.end method


# virtual methods
.method public click(FF)V
    .locals 4

    .prologue
    .line 262
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet$TabStripEventHandler;->this$0:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet;

    # getter for: Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet;->mTabStripLayoutHelperManager:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;
    invoke-static {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet;->access$100(Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet;)Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;

    move-result-object v0

    invoke-static {}, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->time()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->click(JFF)V

    .line 263
    return-void
.end method

.method public drag(FFFFFF)V
    .locals 10

    .prologue
    .line 257
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet$TabStripEventHandler;->this$0:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet;

    # getter for: Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet;->mTabStripLayoutHelperManager:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;
    invoke-static {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet;->access$100(Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet;)Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;

    move-result-object v1

    invoke-static {}, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->time()J

    move-result-wide v2

    move v4, p1

    move v5, p2

    move v6, p3

    move v7, p4

    move v8, p5

    move/from16 v9, p6

    invoke-virtual/range {v1 .. v9}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->drag(JFFFFFF)V

    .line 258
    return-void
.end method

.method public fling(FFFF)V
    .locals 8

    .prologue
    .line 267
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet$TabStripEventHandler;->this$0:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet;

    # getter for: Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet;->mTabStripLayoutHelperManager:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;
    invoke-static {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet;->access$100(Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet;)Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;

    move-result-object v1

    invoke-static {}, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->time()J

    move-result-wide v2

    move v4, p1

    move v5, p2

    move v6, p3

    move v7, p4

    invoke-virtual/range {v1 .. v7}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->fling(JFFFF)V

    .line 268
    return-void
.end method

.method public onDown(FF)V
    .locals 4

    .prologue
    .line 247
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet$TabStripEventHandler;->this$0:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet;

    # getter for: Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet;->mTabStripLayoutHelperManager:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;
    invoke-static {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet;->access$100(Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet;)Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;

    move-result-object v0

    invoke-static {}, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->time()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->onDown(JFF)V

    .line 248
    return-void
.end method

.method public onLongPress(FF)V
    .locals 4

    .prologue
    .line 272
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet$TabStripEventHandler;->this$0:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet;

    # getter for: Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet;->mTabStripLayoutHelperManager:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;
    invoke-static {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet;->access$100(Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet;)Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;

    move-result-object v0

    invoke-static {}, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->time()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->onLongPress(JFF)V

    .line 273
    return-void
.end method

.method public onPinch(FFFFZ)V
    .locals 0

    .prologue
    .line 278
    return-void
.end method

.method public onUpOrCancel()V
    .locals 4

    .prologue
    .line 252
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet$TabStripEventHandler;->this$0:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet;

    # getter for: Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet;->mTabStripLayoutHelperManager:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;
    invoke-static {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet;->access$100(Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet;)Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;

    move-result-object v0

    invoke-static {}, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->time()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->onUpOrCancel(J)V

    .line 253
    return-void
.end method
