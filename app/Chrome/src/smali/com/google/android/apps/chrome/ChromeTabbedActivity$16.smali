.class Lcom/google/android/apps/chrome/ChromeTabbedActivity$16;
.super Ljava/lang/Object;
.source "ChromeTabbedActivity.java"

# interfaces
.implements Lorg/chromium/content/browser/ContentReadbackHandler$GetBitmapCallback;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/ChromeTabbedActivity;

.field final synthetic val$helpContextId:Ljava/lang/String;

.field final synthetic val$mainActivity:Landroid/app/Activity;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/ChromeTabbedActivity;Landroid/app/Activity;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1138
    iput-object p1, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity$16;->this$0:Lcom/google/android/apps/chrome/ChromeTabbedActivity;

    iput-object p2, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity$16;->val$mainActivity:Landroid/app/Activity;

    iput-object p3, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity$16;->val$helpContextId:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFinishGetBitmap(Landroid/graphics/Bitmap;)V
    .locals 2

    .prologue
    .line 1141
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity$16;->val$mainActivity:Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity$16;->val$helpContextId:Ljava/lang/String;

    invoke-static {v0, v1, p1}, Lcom/google/android/apps/chrome/help/GoogleHelpUtils;->show(Landroid/app/Activity;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    .line 1142
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->menuFeedback()V

    .line 1143
    return-void
.end method
