.class Lcom/google/android/apps/chrome/videofling/MediaRouteController$7;
.super Ljava/lang/Object;
.source "MediaRouteController.java"

# interfaces
.implements Lcom/google/android/apps/chrome/videofling/MediaRouteController$ResultBundleHandler;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/videofling/MediaRouteController;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/videofling/MediaRouteController;)V
    .locals 0

    .prologue
    .line 667
    iput-object p1, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController$7;->this$0:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 678
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController$7;->this$0:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->release()V

    .line 679
    return-void
.end method

.method public onResult(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 670
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController$7;->this$0:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    # getter for: Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mListeners:Ljava/util/Set;
    invoke-static {v0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->access$800(Lcom/google/android/apps/chrome/videofling/MediaRouteController;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/videofling/MediaRouteController$Listener;

    .line 671
    invoke-interface {v0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController$Listener;->onSeekCompleted()V

    goto :goto_0

    .line 673
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController$7;->this$0:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->processMediaStatusBundle(Landroid/os/Bundle;)V

    .line 674
    return-void
.end method
