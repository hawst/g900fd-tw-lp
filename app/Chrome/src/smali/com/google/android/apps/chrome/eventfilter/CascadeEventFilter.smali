.class public Lcom/google/android/apps/chrome/eventfilter/CascadeEventFilter;
.super Lcom/google/android/apps/chrome/eventfilter/EventFilter;
.source "CascadeEventFilter.java"


# instance fields
.field private mActiveDelegate:Lcom/google/android/apps/chrome/eventfilter/EventFilter;

.field private mDelegates:[Lcom/google/android/apps/chrome/eventfilter/EventFilter;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/chrome/eventfilter/EventFilterHost;[Lcom/google/android/apps/chrome/eventfilter/EventFilter;)V
    .locals 1

    .prologue
    .line 31
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/chrome/eventfilter/EventFilter;-><init>(Lcom/google/android/apps/chrome/eventfilter/EventFilterHost;Z)V

    .line 16
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/CascadeEventFilter;->mActiveDelegate:Lcom/google/android/apps/chrome/eventfilter/EventFilter;

    .line 32
    array-length v0, p2

    invoke-static {p2, v0}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/chrome/eventfilter/EventFilter;

    iput-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/CascadeEventFilter;->mDelegates:[Lcom/google/android/apps/chrome/eventfilter/EventFilter;

    .line 33
    return-void
.end method


# virtual methods
.method public onInterceptTouchEventInternal(Landroid/view/MotionEvent;Z)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 37
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/CascadeEventFilter;->mActiveDelegate:Lcom/google/android/apps/chrome/eventfilter/EventFilter;

    .line 38
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v3

    .line 39
    iget v0, p0, Lcom/google/android/apps/chrome/eventfilter/CascadeEventFilter;->mCurrentTouchOffsetX:F

    iget v2, p0, Lcom/google/android/apps/chrome/eventfilter/CascadeEventFilter;->mCurrentTouchOffsetY:F

    invoke-virtual {v3, v0, v2}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    move v0, v1

    .line 40
    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/chrome/eventfilter/CascadeEventFilter;->mDelegates:[Lcom/google/android/apps/chrome/eventfilter/EventFilter;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 41
    iget-object v2, p0, Lcom/google/android/apps/chrome/eventfilter/CascadeEventFilter;->mDelegates:[Lcom/google/android/apps/chrome/eventfilter/EventFilter;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/eventfilter/EventFilter;->autoOffsetEvents()Z

    move-result v2

    if-eqz v2, :cond_0

    move-object v2, v3

    .line 42
    :goto_1
    iget-object v4, p0, Lcom/google/android/apps/chrome/eventfilter/CascadeEventFilter;->mDelegates:[Lcom/google/android/apps/chrome/eventfilter/EventFilter;

    aget-object v4, v4, v0

    invoke-virtual {v4, v2, p2}, Lcom/google/android/apps/chrome/eventfilter/EventFilter;->onInterceptTouchEventInternal(Landroid/view/MotionEvent;Z)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 43
    iget-object v1, p0, Lcom/google/android/apps/chrome/eventfilter/CascadeEventFilter;->mDelegates:[Lcom/google/android/apps/chrome/eventfilter/EventFilter;

    aget-object v0, v1, v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/CascadeEventFilter;->mActiveDelegate:Lcom/google/android/apps/chrome/eventfilter/EventFilter;

    .line 44
    invoke-virtual {v3}, Landroid/view/MotionEvent;->recycle()V

    .line 45
    const/4 v1, 0x1

    .line 49
    :goto_2
    return v1

    :cond_0
    move-object v2, p1

    .line 41
    goto :goto_1

    .line 40
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 48
    :cond_2
    invoke-virtual {v3}, Landroid/view/MotionEvent;->recycle()V

    goto :goto_2
.end method

.method public onTouchEventInternal(Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/CascadeEventFilter;->mActiveDelegate:Lcom/google/android/apps/chrome/eventfilter/EventFilter;

    if-eqz v0, :cond_1

    .line 55
    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/CascadeEventFilter;->mActiveDelegate:Lcom/google/android/apps/chrome/eventfilter/EventFilter;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/eventfilter/EventFilter;->autoOffsetEvents()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 56
    iget v0, p0, Lcom/google/android/apps/chrome/eventfilter/CascadeEventFilter;->mCurrentTouchOffsetX:F

    iget v1, p0, Lcom/google/android/apps/chrome/eventfilter/CascadeEventFilter;->mCurrentTouchOffsetY:F

    invoke-virtual {p1, v0, v1}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    .line 58
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/CascadeEventFilter;->mActiveDelegate:Lcom/google/android/apps/chrome/eventfilter/EventFilter;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/eventfilter/EventFilter;->onTouchEventInternal(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 60
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
