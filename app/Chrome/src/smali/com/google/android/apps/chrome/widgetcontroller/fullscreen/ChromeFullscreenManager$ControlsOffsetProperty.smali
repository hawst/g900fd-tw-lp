.class Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager$ControlsOffsetProperty;
.super Landroid/util/Property;
.source "ChromeFullscreenManager.java"


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;)V
    .locals 2

    .prologue
    .line 122
    iput-object p1, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager$ControlsOffsetProperty;->this$0:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;

    .line 123
    const-class v0, Ljava/lang/Float;

    const-string/jumbo v1, "controlsOffset"

    invoke-direct {p0, v0, v1}, Landroid/util/Property;-><init>(Ljava/lang/Class;Ljava/lang/String;)V

    .line 124
    return-void
.end method


# virtual methods
.method public get(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;)Ljava/lang/Float;
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager$ControlsOffsetProperty;->this$0:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->getControlOffset()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 121
    check-cast p1, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager$ControlsOffsetProperty;->get(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public set(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;Ljava/lang/Float;)V
    .locals 2

    .prologue
    .line 133
    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager$ControlsOffsetProperty;->this$0:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;

    # getter for: Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mDisableBrowserOverride:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->access$000(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 139
    :cond_0
    :goto_0
    return-void

    .line 134
    :cond_1
    invoke-virtual {p2}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 135
    iget-object v1, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager$ControlsOffsetProperty;->this$0:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;

    # getter for: Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mBrowserControlOffset:F
    invoke-static {v1}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->access$100(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;)F

    move-result v1

    invoke-static {v1, v0}, Ljava/lang/Float;->compare(FF)I

    move-result v1

    if-eqz v1, :cond_0

    .line 136
    iget-object v1, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager$ControlsOffsetProperty;->this$0:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;

    # setter for: Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->mBrowserControlOffset:F
    invoke-static {v1, v0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->access$102(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;F)F

    .line 137
    # invokes: Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->updateControlOffset()V
    invoke-static {p1}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->access$200(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;)V

    .line 138
    # invokes: Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->updateVisuals()V
    invoke-static {p1}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->access$300(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;)V

    goto :goto_0
.end method

.method public bridge synthetic set(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 121
    check-cast p1, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;

    check-cast p2, Ljava/lang/Float;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager$ControlsOffsetProperty;->set(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;Ljava/lang/Float;)V

    return-void
.end method
