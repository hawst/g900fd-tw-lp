.class Lcom/google/android/apps/chrome/icing/IcingController$1;
.super Lcom/google/android/apps/chrome/icing/ConnectedTask;
.source "IcingController.java"


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/icing/IcingController;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/icing/IcingController;Lcom/google/android/apps/chrome/icing/GoogleApiIcingClient;Ljava/util/concurrent/ScheduledThreadPoolExecutor;)V
    .locals 0

    .prologue
    .line 73
    iput-object p1, p0, Lcom/google/android/apps/chrome/icing/IcingController$1;->this$0:Lcom/google/android/apps/chrome/icing/IcingController;

    invoke-direct {p0, p2, p3}, Lcom/google/android/apps/chrome/icing/ConnectedTask;-><init>(Lcom/google/android/apps/chrome/icing/GoogleApiIcingClient;Ljava/util/concurrent/ScheduledThreadPoolExecutor;)V

    return-void
.end method


# virtual methods
.method protected cleanUp()V
    .locals 2

    .prologue
    .line 96
    iget-object v0, p0, Lcom/google/android/apps/chrome/icing/IcingController$1;->this$0:Lcom/google/android/apps/chrome/icing/IcingController;

    iget-object v0, v0, Lcom/google/android/apps/chrome/icing/IcingController;->mIndexingRequested:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 97
    return-void
.end method

.method protected doWhenConnected(Lcom/google/android/apps/chrome/icing/GoogleApiIcingClient;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 81
    iget-object v0, p0, Lcom/google/android/apps/chrome/icing/IcingController$1;->this$0:Lcom/google/android/apps/chrome/icing/IcingController;

    # getter for: Lcom/google/android/apps/chrome/icing/IcingController;->mPrefs:Landroid/content/SharedPreferences;
    invoke-static {v0}, Lcom/google/android/apps/chrome/icing/IcingController;->access$000(Lcom/google/android/apps/chrome/icing/IcingController;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "com.google.android.apps.chrome.icing.IcingController.DATA_CLEARED"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 82
    invoke-interface {p1}, Lcom/google/android/apps/chrome/icing/GoogleApiIcingClient;->clearData()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 83
    iget-object v0, p0, Lcom/google/android/apps/chrome/icing/IcingController$1;->this$0:Lcom/google/android/apps/chrome/icing/IcingController;

    # getter for: Lcom/google/android/apps/chrome/icing/IcingController;->mPrefs:Landroid/content/SharedPreferences;
    invoke-static {v0}, Lcom/google/android/apps/chrome/icing/IcingController;->access$000(Lcom/google/android/apps/chrome/icing/IcingController;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "com.google.android.apps.chrome.icing.IcingController.DATA_CLEARED"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 90
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/icing/IcingController$1;->this$0:Lcom/google/android/apps/chrome/icing/IcingController;

    # getter for: Lcom/google/android/apps/chrome/icing/IcingController;->mJniBridge:Lcom/google/android/apps/chrome/icing/SearchJniBridge;
    invoke-static {v0}, Lcom/google/android/apps/chrome/icing/IcingController;->access$100(Lcom/google/android/apps/chrome/icing/IcingController;)Lcom/google/android/apps/chrome/icing/SearchJniBridge;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/apps/chrome/icing/GoogleApiIcingClient;->getLastCommittedSeqno()J

    move-result-wide v2

    invoke-interface {v0, v2, v3}, Lcom/google/android/apps/chrome/icing/SearchJniBridge;->trimDeltaFile(J)J

    move-result-wide v0

    .line 91
    const-wide/16 v2, -0x1

    cmp-long v2, v0, v2

    if-eqz v2, :cond_1

    invoke-interface {p1, v0, v1}, Lcom/google/android/apps/chrome/icing/GoogleApiIcingClient;->requestIndexing(J)V

    .line 92
    :cond_1
    return-void
.end method

.method protected getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 76
    const-string/jumbo v0, "RequestIndexing"

    return-object v0
.end method
