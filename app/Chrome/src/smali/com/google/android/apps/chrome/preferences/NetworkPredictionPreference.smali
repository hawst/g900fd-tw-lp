.class public Lcom/google/android/apps/chrome/preferences/NetworkPredictionPreference;
.super Lcom/google/android/apps/chrome/preferences/ChromeBaseListPreference;
.source "NetworkPredictionPreference.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    const-class v0, Lcom/google/android/apps/chrome/preferences/NetworkPredictionPreference;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/preferences/NetworkPredictionPreference;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/preferences/ChromeBaseListPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 24
    iput-object p1, p0, Lcom/google/android/apps/chrome/preferences/NetworkPredictionPreference;->mContext:Landroid/content/Context;

    .line 25
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$array;->bandwidth_entries:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    .line 27
    sget-boolean v1, Lcom/google/android/apps/chrome/preferences/NetworkPredictionPreference;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    array-length v0, v0

    invoke-static {}, Lcom/google/android/apps/chrome/preferences/NetworkPredictionOptions;->choiceCount()I

    move-result v1

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 28
    :cond_0
    invoke-virtual {p0, p0}, Lcom/google/android/apps/chrome/preferences/NetworkPredictionPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 29
    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getNetworkPredictionOptions()Lcom/google/android/apps/chrome/preferences/NetworkPredictionOptions;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/NetworkPredictionOptions;->getDisplayTitle()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/NetworkPredictionPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 31
    return-void
.end method


# virtual methods
.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/NetworkPredictionPreference;->mContext:Landroid/content/Context;

    check-cast p2, Ljava/lang/String;

    invoke-static {p2}, Lcom/google/android/apps/chrome/preferences/NetworkPredictionOptions;->stringToEnum(Ljava/lang/String;)Lcom/google/android/apps/chrome/preferences/NetworkPredictionOptions;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/preferences/NetworkPredictionOptions;->getDisplayTitle()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/NetworkPredictionPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 37
    const/4 v0, 0x1

    return v0
.end method
