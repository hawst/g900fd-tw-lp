.class public abstract Lcom/google/android/apps/chrome/preferences/website/WebsitePreferenceBridge;
.super Ljava/lang/Object;
.source "WebsitePreferenceBridge.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    return-void
.end method

.method private static createLocalStorageInfoMap()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 84
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    return-object v0
.end method

.method private static createStorageInfoList()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 73
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    return-object v0
.end method

.method public static fetchLocalStorageInfo(Lcom/google/android/apps/chrome/preferences/website/WebsitePreferenceBridge$LocalStorageInfoReadyCallback;)V
    .locals 0

    .prologue
    .line 150
    invoke-static {p0}, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferenceBridge;->nativeFetchLocalStorageInfo(Ljava/lang/Object;)V

    .line 151
    return-void
.end method

.method public static fetchStorageInfo(Lcom/google/android/apps/chrome/preferences/website/WebsitePreferenceBridge$StorageInfoReadyCallback;)V
    .locals 0

    .prologue
    .line 154
    invoke-static {p0}, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferenceBridge;->nativeFetchStorageInfo(Ljava/lang/Object;)V

    .line 155
    return-void
.end method

.method public static getGeolocationInfo()Ljava/util/List;
    .locals 1

    .prologue
    .line 47
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 48
    invoke-static {v0}, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferenceBridge;->nativeGetGeolocationOrigins(Ljava/lang/Object;)V

    .line 49
    return-object v0
.end method

.method public static getMidiInfo()Ljava/util/List;
    .locals 1

    .prologue
    .line 60
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 61
    invoke-static {v0}, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferenceBridge;->nativeGetMidiOrigins(Ljava/lang/Object;)V

    .line 62
    return-object v0
.end method

.method public static getPopupExceptionInfo()Ljava/util/List;
    .locals 5

    .prologue
    .line 137
    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getPopupExceptions()Ljava/util/ArrayList;

    move-result-object v0

    .line 139
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 140
    if-eqz v0, :cond_0

    .line 141
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences$PopupException;

    .line 142
    new-instance v3, Lcom/google/android/apps/chrome/preferences/website/PopupExceptionInfo;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences$PopupException;->getPattern()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences$PopupException;->getSetting()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v4, v0}, Lcom/google/android/apps/chrome/preferences/website/PopupExceptionInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 146
    :cond_0
    return-object v1
.end method

.method public static getProtectedMediaIdentifierInfo()Ljava/util/List;
    .locals 1

    .prologue
    .line 100
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 102
    invoke-static {v0}, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferenceBridge;->nativeGetProtectedMediaIdentifierOrigins(Ljava/lang/Object;)V

    .line 103
    return-object v0
.end method

.method public static getVoiceAndVideoCaptureInfo()Ljava/util/List;
    .locals 1

    .prologue
    .line 118
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 120
    invoke-static {v0}, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferenceBridge;->nativeGetVoiceAndVideoCaptureOrigins(Ljava/lang/Object;)V

    .line 121
    return-object v0
.end method

.method private static insertGeolocationInfoIntoList(Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 55
    new-instance v0, Lcom/google/android/apps/chrome/preferences/website/GeolocationInfo;

    invoke-direct {v0, p1, p2}, Lcom/google/android/apps/chrome/preferences/website/GeolocationInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 56
    return-void
.end method

.method private static insertLocalStorageInfoIntoMap(Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;J)V
    .locals 1

    .prologue
    .line 91
    new-instance v0, Lcom/google/android/apps/chrome/preferences/website/LocalStorageInfo;

    invoke-direct {v0, p1, p3, p4}, Lcom/google/android/apps/chrome/preferences/website/LocalStorageInfo;-><init>(Ljava/lang/String;J)V

    invoke-virtual {p0, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 92
    return-void
.end method

.method private static insertMidiInfoIntoList(Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 68
    new-instance v0, Lcom/google/android/apps/chrome/preferences/website/MidiInfo;

    invoke-direct {v0, p1, p2}, Lcom/google/android/apps/chrome/preferences/website/MidiInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 69
    return-void
.end method

.method private static insertProtectedMediaIdentifierInfoIntoList(Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 109
    new-instance v0, Lcom/google/android/apps/chrome/preferences/website/ProtectedMediaIdentifierInfo;

    invoke-direct {v0, p1, p2}, Lcom/google/android/apps/chrome/preferences/website/ProtectedMediaIdentifierInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 110
    return-void
.end method

.method private static insertStorageInfoIntoList(Ljava/util/ArrayList;Ljava/lang/String;IJ)V
    .locals 1

    .prologue
    .line 79
    new-instance v0, Lcom/google/android/apps/chrome/preferences/website/StorageInfo;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/google/android/apps/chrome/preferences/website/StorageInfo;-><init>(Ljava/lang/String;IJ)V

    invoke-virtual {p0, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 80
    return-void
.end method

.method private static insertVoiceAndVideoCaptureInfoIntoList(Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 127
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 128
    invoke-virtual {p0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/preferences/website/VoiceAndVideoCaptureInfo;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/website/VoiceAndVideoCaptureInfo;->getOrigin()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/preferences/website/VoiceAndVideoCaptureInfo;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/website/VoiceAndVideoCaptureInfo;->getEmbedder()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 134
    :goto_1
    return-void

    .line 127
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 133
    :cond_1
    new-instance v0, Lcom/google/android/apps/chrome/preferences/website/VoiceAndVideoCaptureInfo;

    invoke-direct {v0, p1, p2}, Lcom/google/android/apps/chrome/preferences/website/VoiceAndVideoCaptureInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method static native nativeClearLocalStorageData(Ljava/lang/String;)V
.end method

.method static native nativeClearStorageData(Ljava/lang/String;ILjava/lang/Object;)V
.end method

.method private static native nativeFetchLocalStorageInfo(Ljava/lang/Object;)V
.end method

.method private static native nativeFetchStorageInfo(Ljava/lang/Object;)V
.end method

.method private static native nativeGetGeolocationOrigins(Ljava/lang/Object;)V
.end method

.method static native nativeGetGeolocationSettingForOrigin(Ljava/lang/String;Ljava/lang/String;)I
.end method

.method private static native nativeGetMidiOrigins(Ljava/lang/Object;)V
.end method

.method static native nativeGetMidiSettingForOrigin(Ljava/lang/String;Ljava/lang/String;)I
.end method

.method private static native nativeGetProtectedMediaIdentifierOrigins(Ljava/lang/Object;)V
.end method

.method static native nativeGetProtectedMediaIdentifierSettingForOrigin(Ljava/lang/String;Ljava/lang/String;)I
.end method

.method static native nativeGetVideoCaptureSettingForOrigin(Ljava/lang/String;Ljava/lang/String;)I
.end method

.method private static native nativeGetVoiceAndVideoCaptureOrigins(Ljava/lang/Object;)V
.end method

.method static native nativeGetVoiceCaptureSettingForOrigin(Ljava/lang/String;Ljava/lang/String;)I
.end method

.method static native nativeSetGeolocationSettingForOrigin(Ljava/lang/String;Ljava/lang/String;I)V
.end method

.method static native nativeSetMidiSettingForOrigin(Ljava/lang/String;Ljava/lang/String;I)V
.end method

.method static native nativeSetProtectedMediaIdentifierSettingForOrigin(Ljava/lang/String;Ljava/lang/String;I)V
.end method

.method static native nativeSetVideoCaptureSettingForOrigin(Ljava/lang/String;Ljava/lang/String;I)V
.end method

.method static native nativeSetVoiceCaptureSettingForOrigin(Ljava/lang/String;Ljava/lang/String;I)V
.end method
