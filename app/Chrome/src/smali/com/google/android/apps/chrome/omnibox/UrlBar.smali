.class public Lcom/google/android/apps/chrome/omnibox/UrlBar;
.super Lcom/google/android/apps/chrome/widget/VerticallyFixedEditText;
.source "UrlBar.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

.field private mAccessibilityTextOverride:Ljava/lang/String;

.field private final mAutocompleteSpan:Lcom/google/android/apps/chrome/omnibox/UrlBar$AutocompleteSpan;

.field private final mDarkDefaultTextColor:I

.field private final mDarkHintColor:Landroid/content/res/ColorStateList;

.field private final mDarkHostColor:I

.field private mDisableTextAccessibilityEvents:Z

.field private mFirstDrawComplete:Z

.field private mFocusTriggerEventDownTime:J

.field private mFocused:Z

.field private mFormattedUrlLocation:Ljava/lang/String;

.field private final mGestureDetector:Landroid/view/GestureDetector;

.field private mHostColor:I

.field mInputConnection:Landroid/view/inputmethod/InputConnectionWrapper;

.field private final mLightDefaultTextColor:I

.field private final mLightHintColor:I

.field private final mLightHostColor:I

.field private mLocationBarTextWatcher:Landroid/text/TextWatcher;

.field private mOriginalUrlLocation:Ljava/lang/String;

.field private final mSchemeToHostColor:I

.field private mShowKeyboardOnWindowFocus:Z

.field private final mStartSchemeEvSecureColor:I

.field private final mStartSchemeSecureColor:I

.field private final mStartSchemeSecurityErrorColor:I

.field private final mStartSchemeSecurityWarningColor:I

.field private final mTrailingUrlColor:I

.field private mUrlBarDelegate:Lcom/google/android/apps/chrome/omnibox/UrlBar$UrlBarDelegate;

.field private mUrlDirection:I

.field private mUrlDirectionListener:Lcom/google/android/apps/chrome/omnibox/UrlBar$UrlDirectionListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 62
    const-class v0, Lcom/google/android/apps/chrome/omnibox/UrlBar;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 174
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/widget/VerticallyFixedEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 782
    new-instance v0, Lcom/google/android/apps/chrome/omnibox/UrlBar$4;

    const/4 v1, 0x1

    invoke-direct {v0, p0, v3, v1}, Lcom/google/android/apps/chrome/omnibox/UrlBar$4;-><init>(Lcom/google/android/apps/chrome/omnibox/UrlBar;Landroid/view/inputmethod/InputConnection;Z)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mInputConnection:Landroid/view/inputmethod/InputConnectionWrapper;

    .line 176
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 178
    sget v1, Lcom/google/android/apps/chrome/R$color;->locationbar_default_text:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mDarkDefaultTextColor:I

    .line 179
    sget v1, Lcom/google/android/apps/chrome/R$color;->locationbar_domain_and_registry:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mDarkHostColor:I

    .line 180
    sget v1, Lcom/google/android/apps/chrome/R$color;->locationbar_light_default_text:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mLightDefaultTextColor:I

    .line 181
    sget v1, Lcom/google/android/apps/chrome/R$color;->locationbar_light_hint_text:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mLightHintColor:I

    .line 182
    sget v1, Lcom/google/android/apps/chrome/R$color;->locationbar_light_domain_and_registry:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mLightHostColor:I

    .line 184
    iget v1, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mDarkDefaultTextColor:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->setTextColor(I)V

    .line 186
    sget v1, Lcom/google/android/apps/chrome/R$color;->locationbar_start_scheme_security_warning:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mStartSchemeSecurityWarningColor:I

    .line 188
    sget v1, Lcom/google/android/apps/chrome/R$color;->locationbar_start_scheme_security_error:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mStartSchemeSecurityErrorColor:I

    .line 190
    sget v1, Lcom/google/android/apps/chrome/R$color;->locationbar_start_scheme_ev_secure:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mStartSchemeEvSecureColor:I

    .line 192
    sget v1, Lcom/google/android/apps/chrome/R$color;->locationbar_start_scheme_secure:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mStartSchemeSecureColor:I

    .line 194
    sget v1, Lcom/google/android/apps/chrome/R$color;->locationbar_scheme_to_domain:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mSchemeToHostColor:I

    .line 196
    iget v1, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mDarkHostColor:I

    iput v1, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mHostColor:I

    .line 197
    sget v1, Lcom/google/android/apps/chrome/R$color;->locationbar_trailing_url:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mTrailingUrlColor:I

    .line 199
    const/4 v0, 0x3

    iput v0, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mUrlDirection:I

    .line 200
    new-instance v0, Lcom/google/android/apps/chrome/omnibox/UrlBar$AutocompleteSpan;

    invoke-direct {v0, p0, v3}, Lcom/google/android/apps/chrome/omnibox/UrlBar$AutocompleteSpan;-><init>(Lcom/google/android/apps/chrome/omnibox/UrlBar;Lcom/google/android/apps/chrome/omnibox/UrlBar$1;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mAutocompleteSpan:Lcom/google/android/apps/chrome/omnibox/UrlBar$AutocompleteSpan;

    .line 201
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->getHintTextColors()Landroid/content/res/ColorStateList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mDarkHintColor:Landroid/content/res/ColorStateList;

    .line 209
    invoke-virtual {p0, v2}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->setFocusable(Z)V

    .line 210
    invoke-virtual {p0, v2}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->setFocusableInTouchMode(Z)V

    .line 212
    new-instance v0, Landroid/view/GestureDetector;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/chrome/omnibox/UrlBar$1;

    invoke-direct {v2, p0}, Lcom/google/android/apps/chrome/omnibox/UrlBar$1;-><init>(Lcom/google/android/apps/chrome/omnibox/UrlBar;)V

    invoke-direct {v0, v1, v2}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mGestureDetector:Landroid/view/GestureDetector;

    .line 220
    const-string/jumbo v0, "accessibility"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    iput-object v0, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    .line 222
    return-void
.end method

.method static synthetic access$300(Lcom/google/android/apps/chrome/omnibox/UrlBar;)Lcom/google/android/apps/chrome/omnibox/UrlBar$AutocompleteSpan;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mAutocompleteSpan:Lcom/google/android/apps/chrome/omnibox/UrlBar$AutocompleteSpan;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/chrome/omnibox/UrlBar;)Landroid/view/accessibility/AccessibilityManager;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/chrome/omnibox/UrlBar;)Landroid/text/TextWatcher;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mLocationBarTextWatcher:Landroid/text/TextWatcher;

    return-object v0
.end method

.method private getEmphasisSpans()[Lcom/google/android/apps/chrome/omnibox/UrlBar$UrlEmphasisSpan;
    .locals 4

    .prologue
    .line 1013
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->getText()Landroid/text/Editable;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->length()I

    move-result v2

    const-class v3, Lcom/google/android/apps/chrome/omnibox/UrlBar$UrlEmphasisSpan;

    invoke-interface {v0, v1, v2, v3}, Landroid/text/Editable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/chrome/omnibox/UrlBar$UrlEmphasisSpan;

    return-object v0
.end method

.method private static getUrlContentsPrePath(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 737
    .line 738
    invoke-virtual {p0, p1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 739
    if-ltz v0, :cond_0

    .line 740
    const/16 v1, 0x2f

    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->indexOf(II)I

    move-result v0

    .line 741
    if-lez v0, :cond_0

    .line 742
    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    .line 747
    :cond_0
    return-object p0
.end method

.method private scrollToTLD()V
    .locals 3

    .prologue
    .line 677
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->getText()Landroid/text/Editable;

    move-result-object v0

    .line 678
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v1

    if-gtz v1, :cond_1

    .line 691
    :cond_0
    :goto_0
    return-void

    .line 679
    :cond_1
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 682
    :try_start_0
    new-instance v1, Ljava/net/URL;

    invoke-direct {v1, v0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 686
    invoke-virtual {v1}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v1

    .line 687
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 688
    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 689
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/2addr v0, v1

    .line 690
    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->setSelection(I)V

    goto :goto_0

    .line 684
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private sendAutocompleteAccessibilityEvent(Ljava/lang/CharSequence;)V
    .locals 2

    .prologue
    .line 773
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-ge v0, v1, :cond_0

    .line 780
    :goto_0
    return-void

    .line 779
    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->announceForAccessibility(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private updateUrlDirection()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 478
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->getLayout()Landroid/text/Layout;

    move-result-object v2

    .line 479
    if-nez v2, :cond_1

    .line 496
    :cond_0
    :goto_0
    return-void

    .line 482
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->length()I

    move-result v3

    if-nez v3, :cond_3

    .line 483
    const/4 v0, 0x3

    .line 490
    :cond_2
    :goto_1
    iget v1, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mUrlDirection:I

    if-eq v0, v1, :cond_0

    .line 491
    iput v0, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mUrlDirection:I

    .line 492
    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mUrlDirectionListener:Lcom/google/android/apps/chrome/omnibox/UrlBar$UrlDirectionListener;

    if-eqz v1, :cond_0

    .line 493
    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mUrlDirectionListener:Lcom/google/android/apps/chrome/omnibox/UrlBar$UrlDirectionListener;

    invoke-interface {v1, v0}, Lcom/google/android/apps/chrome/omnibox/UrlBar$UrlDirectionListener;->onUrlDirectionChanged(I)V

    goto :goto_0

    .line 484
    :cond_3
    invoke-virtual {v2, v0}, Landroid/text/Layout;->getParagraphDirection(I)I

    move-result v2

    if-eq v2, v1, :cond_2

    move v0, v1

    .line 487
    goto :goto_1
.end method


# virtual methods
.method public deEmphasizeUrl()V
    .locals 5

    .prologue
    .line 1005
    invoke-direct {p0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->getEmphasisSpans()[Lcom/google/android/apps/chrome/omnibox/UrlBar$UrlEmphasisSpan;

    move-result-object v1

    .line 1006
    array-length v0, v1

    if-nez v0, :cond_1

    .line 1010
    :cond_0
    return-void

    .line 1007
    :cond_1
    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 1008
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-interface {v4, v3}, Landroid/text/Editable;->removeSpan(Ljava/lang/Object;)V

    .line 1007
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public emphasizeUrl()V
    .locals 11

    .prologue
    const/16 v3, 0x9

    const/4 v1, 0x5

    const/4 v2, 0x0

    const/16 v10, 0x21

    .line 892
    invoke-direct {p0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->getEmphasisSpans()[Lcom/google/android/apps/chrome/omnibox/UrlBar$UrlEmphasisSpan;

    move-result-object v0

    array-length v0, v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 999
    :cond_0
    :goto_0
    return-void

    .line 896
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->getText()Landroid/text/Editable;

    move-result-object v4

    .line 897
    invoke-interface {v4}, Landroid/text/Editable;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 901
    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    .line 903
    const-string/jumbo v0, "chrome://"

    invoke-virtual {v5, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 904
    new-instance v0, Lcom/google/android/apps/chrome/omnibox/UrlBar$UrlEmphasisColorSpan;

    iget v1, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mSchemeToHostColor:I

    invoke-direct {v0, v1}, Lcom/google/android/apps/chrome/omnibox/UrlBar$UrlEmphasisColorSpan;-><init>(I)V

    .line 905
    invoke-interface {v4, v0, v2, v3, v10}, Landroid/text/Editable;->setSpan(Ljava/lang/Object;III)V

    .line 908
    new-instance v0, Lcom/google/android/apps/chrome/omnibox/UrlBar$UrlEmphasisColorSpan;

    iget v1, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mHostColor:I

    invoke-direct {v0, v1}, Lcom/google/android/apps/chrome/omnibox/UrlBar$UrlEmphasisColorSpan;-><init>(I)V

    .line 909
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v1

    invoke-interface {v4, v0, v3, v1, v10}, Landroid/text/Editable;->setSpan(Ljava/lang/Object;III)V

    goto :goto_0

    .line 913
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mUrlBarDelegate:Lcom/google/android/apps/chrome/omnibox/UrlBar$UrlBarDelegate;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/omnibox/UrlBar$UrlBarDelegate;->showingOriginalUrlForPreview()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 916
    new-instance v0, Lcom/google/android/apps/chrome/omnibox/UrlBar$UrlEmphasisColorSpan;

    iget v1, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mTrailingUrlColor:I

    invoke-direct {v0, v1}, Lcom/google/android/apps/chrome/omnibox/UrlBar$UrlEmphasisColorSpan;-><init>(I)V

    .line 917
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v1

    invoke-interface {v4, v0, v2, v1, v10}, Landroid/text/Editable;->setSpan(Ljava/lang/Object;III)V

    goto :goto_0

    .line 923
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mUrlBarDelegate:Lcom/google/android/apps/chrome/omnibox/UrlBar$UrlBarDelegate;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/omnibox/UrlBar$UrlBarDelegate;->getCurrentTab()Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    .line 924
    if-eqz v0, :cond_0

    .line 927
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->isShowingSnapshot()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getBaseUrl()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 928
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getBaseUrl()Ljava/lang/String;

    move-result-object v0

    .line 932
    :goto_1
    :try_start_0
    new-instance v3, Ljava/net/URI;

    invoke-direct {v3, v0}, Ljava/net/URI;-><init>(Ljava/lang/String;)V

    .line 935
    invoke-virtual {v3}, Ljava/net/URI;->getHost()Ljava/lang/String;
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 940
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 942
    const-string/jumbo v0, "https"

    invoke-virtual {v5, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    move v0, v1

    .line 943
    :goto_2
    invoke-virtual {v5, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    .line 944
    const/4 v7, -0x1

    if-eq v6, v7, :cond_0

    .line 950
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int v7, v6, v3

    .line 954
    if-lez v0, :cond_7

    .line 955
    iget v3, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mHostColor:I

    .line 956
    iget-object v8, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mUrlBarDelegate:Lcom/google/android/apps/chrome/omnibox/UrlBar$UrlBarDelegate;

    invoke-interface {v8}, Lcom/google/android/apps/chrome/omnibox/UrlBar$UrlBarDelegate;->getCurrentTab()Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getSecurityLevel()I

    move-result v8

    .line 957
    packed-switch v8, :pswitch_data_0

    .line 976
    :pswitch_0
    sget-boolean v8, Lcom/google/android/apps/chrome/omnibox/UrlBar;->$assertionsDisabled:Z

    if-nez v8, :cond_6

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 930
    :cond_4
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getUrl()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_5
    move v0, v2

    .line 942
    goto :goto_2

    .line 962
    :pswitch_1
    iget v3, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mStartSchemeSecurityWarningColor:I

    .line 978
    :cond_6
    :goto_3
    :pswitch_2
    new-instance v8, Lcom/google/android/apps/chrome/omnibox/UrlBar$UrlEmphasisColorSpan;

    invoke-direct {v8, v3}, Lcom/google/android/apps/chrome/omnibox/UrlBar$UrlEmphasisColorSpan;-><init>(I)V

    .line 979
    invoke-interface {v4, v8, v2, v1, v10}, Landroid/text/Editable;->setSpan(Ljava/lang/Object;III)V

    .line 984
    :cond_7
    if-eqz v6, :cond_8

    .line 985
    new-instance v1, Lcom/google/android/apps/chrome/omnibox/UrlBar$UrlEmphasisColorSpan;

    iget v2, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mSchemeToHostColor:I

    invoke-direct {v1, v2}, Lcom/google/android/apps/chrome/omnibox/UrlBar$UrlEmphasisColorSpan;-><init>(I)V

    .line 986
    invoke-interface {v4, v1, v0, v6, v10}, Landroid/text/Editable;->setSpan(Ljava/lang/Object;III)V

    .line 991
    :cond_8
    new-instance v0, Lcom/google/android/apps/chrome/omnibox/UrlBar$UrlEmphasisColorSpan;

    iget v1, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mHostColor:I

    invoke-direct {v0, v1}, Lcom/google/android/apps/chrome/omnibox/UrlBar$UrlEmphasisColorSpan;-><init>(I)V

    .line 992
    invoke-interface {v4, v0, v6, v7, v10}, Landroid/text/Editable;->setSpan(Ljava/lang/Object;III)V

    .line 995
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v0

    if-ge v7, v0, :cond_0

    .line 996
    new-instance v0, Lcom/google/android/apps/chrome/omnibox/UrlBar$UrlEmphasisColorSpan;

    iget v1, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mTrailingUrlColor:I

    invoke-direct {v0, v1}, Lcom/google/android/apps/chrome/omnibox/UrlBar$UrlEmphasisColorSpan;-><init>(I)V

    .line 997
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v1

    invoke-interface {v4, v0, v7, v1, v10}, Landroid/text/Editable;->setSpan(Ljava/lang/Object;III)V

    goto/16 :goto_0

    .line 965
    :pswitch_3
    iget v3, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mStartSchemeSecurityErrorColor:I

    .line 966
    new-instance v8, Lcom/google/android/apps/chrome/omnibox/UrlBar$UrlEmphasisSecurityErrorSpan;

    const/4 v9, 0x0

    invoke-direct {v8, v9}, Lcom/google/android/apps/chrome/omnibox/UrlBar$UrlEmphasisSecurityErrorSpan;-><init>(Lcom/google/android/apps/chrome/omnibox/UrlBar$1;)V

    .line 967
    invoke-interface {v4, v8, v2, v1, v10}, Landroid/text/Editable;->setSpan(Ljava/lang/Object;III)V

    goto :goto_3

    .line 970
    :pswitch_4
    iget v3, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mStartSchemeEvSecureColor:I

    goto :goto_3

    .line 973
    :pswitch_5
    iget v3, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mStartSchemeSecureColor:I

    goto :goto_3

    .line 938
    :catch_0
    move-exception v0

    goto/16 :goto_0

    .line 957
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_4
        :pswitch_5
        :pswitch_1
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public focusSearch(I)Landroid/view/View;
    .locals 1

    .prologue
    .line 390
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mUrlBarDelegate:Lcom/google/android/apps/chrome/omnibox/UrlBar$UrlBarDelegate;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/omnibox/UrlBar$UrlBarDelegate;->getCurrentTab()Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 392
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mUrlBarDelegate:Lcom/google/android/apps/chrome/omnibox/UrlBar$UrlBarDelegate;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/omnibox/UrlBar$UrlBarDelegate;->getCurrentTab()Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getView()Landroid/view/View;

    move-result-object v0

    .line 394
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/widget/VerticallyFixedEditText;->focusSearch(I)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public getQueryText()Ljava/lang/String;
    .locals 2

    .prologue
    .line 264
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->getEditableText()Landroid/text/Editable;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->getEditableText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 266
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->getQueryTextOffset()I

    move-result v1

    if-lez v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->getQueryTextOffset()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    :cond_0
    return-object v0

    .line 264
    :cond_1
    const-string/jumbo v0, ""

    goto :goto_0
.end method

.method public getQueryTextOffset()I
    .locals 2

    .prologue
    .line 274
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/UrlBar$CorpusChipPlaceholderSpan;->getSpan(Landroid/text/Editable;)Lcom/google/android/apps/chrome/omnibox/UrlBar$CorpusChipPlaceholderSpan;

    move-result-object v0

    .line 275
    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1, v0}, Landroid/text/Editable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getTextWithoutAutocomplete()Ljava/lang/String;
    .locals 4

    .prologue
    .line 282
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->getText()Landroid/text/Editable;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mAutocompleteSpan:Lcom/google/android/apps/chrome/omnibox/UrlBar$AutocompleteSpan;

    invoke-interface {v0, v1}, Landroid/text/Editable;->getSpanStart(Ljava/lang/Object;)I

    move-result v0

    .line 283
    if-gez v0, :cond_0

    .line 284
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->getQueryText()Ljava/lang/String;

    move-result-object v0

    .line 286
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->getQueryText()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->getQueryTextOffset()I

    move-result v3

    sub-int/2addr v0, v3

    invoke-virtual {v1, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getUrlDirection()I
    .locals 1

    .prologue
    .line 502
    iget v0, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mUrlDirection:I

    return v0
.end method

.method protected hasAutocomplete()Z
    .locals 2

    .prologue
    .line 293
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->getText()Landroid/text/Editable;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mAutocompleteSpan:Lcom/google/android/apps/chrome/omnibox/UrlBar$AutocompleteSpan;

    invoke-interface {v0, v1}, Landroid/text/Editable;->getSpanStart(Ljava/lang/Object;)I

    move-result v0

    if-gez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mAutocompleteSpan:Lcom/google/android/apps/chrome/omnibox/UrlBar$AutocompleteSpan;

    # getter for: Lcom/google/android/apps/chrome/omnibox/UrlBar$AutocompleteSpan;->mAutocompleteText:Ljava/lang/CharSequence;
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/UrlBar$AutocompleteSpan;->access$100(Lcom/google/android/apps/chrome/omnibox/UrlBar$AutocompleteSpan;)Ljava/lang/CharSequence;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mAutocompleteSpan:Lcom/google/android/apps/chrome/omnibox/UrlBar$AutocompleteSpan;

    # getter for: Lcom/google/android/apps/chrome/omnibox/UrlBar$AutocompleteSpan;->mUserText:Ljava/lang/CharSequence;
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/UrlBar$AutocompleteSpan;->access$200(Lcom/google/android/apps/chrome/omnibox/UrlBar$AutocompleteSpan;)Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCreateInputConnection(Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;
    .locals 2

    .prologue
    .line 884
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mInputConnection:Landroid/view/inputmethod/InputConnectionWrapper;

    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/widget/VerticallyFixedEditText;->onCreateInputConnection(Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/inputmethod/InputConnectionWrapper;->setTarget(Landroid/view/inputmethod/InputConnection;)V

    .line 885
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mInputConnection:Landroid/view/inputmethod/InputConnectionWrapper;

    return-object v0
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 453
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/widget/VerticallyFixedEditText;->onDraw(Landroid/graphics/Canvas;)V

    .line 455
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mFirstDrawComplete:Z

    if-nez v0, :cond_0

    .line 456
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mFirstDrawComplete:Z

    .line 461
    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->setFocusable(Z)V

    .line 462
    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->setFocusableInTouchMode(Z)V

    .line 465
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->getContext()Landroid/content/Context;

    move-result-object v0

    const/16 v1, 0x41

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastImmediateNotification(Landroid/content/Context;I)V

    .line 470
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->updateUrlDirection()V

    .line 471
    return-void
.end method

.method protected onFocusChanged(ZILandroid/graphics/Rect;)V
    .locals 1

    .prologue
    .line 329
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mFocused:Z

    .line 330
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mUrlBarDelegate:Lcom/google/android/apps/chrome/omnibox/UrlBar$UrlBarDelegate;

    invoke-interface {v0, p1}, Lcom/google/android/apps/chrome/omnibox/UrlBar$UrlBarDelegate;->onUrlPreFocusChanged(Z)V

    .line 331
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mAutocompleteSpan:Lcom/google/android/apps/chrome/omnibox/UrlBar$AutocompleteSpan;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omnibox/UrlBar$AutocompleteSpan;->clearSpan()V

    .line 332
    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/chrome/widget/VerticallyFixedEditText;->onFocusChanged(ZILandroid/graphics/Rect;)V

    .line 333
    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .locals 1

    .prologue
    .line 763
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/widget/VerticallyFixedEditText;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    .line 765
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mAccessibilityTextOverride:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 766
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mAccessibilityTextOverride:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setText(Ljava/lang/CharSequence;)V

    .line 768
    :cond_0
    return-void
.end method

.method public onKeyPreIme(ILandroid/view/KeyEvent;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 400
    const/4 v1, 0x4

    if-ne p1, v1, :cond_1

    .line 401
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v1

    if-nez v1, :cond_0

    .line 404
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->getKeyDispatcherState()Landroid/view/KeyEvent$DispatcherState;

    move-result-object v1

    invoke-virtual {v1, p2, p0}, Landroid/view/KeyEvent$DispatcherState;->startTracking(Landroid/view/KeyEvent;Ljava/lang/Object;)V

    .line 414
    :goto_0
    return v0

    .line 406
    :cond_0
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-ne v1, v0, :cond_1

    .line 407
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->getKeyDispatcherState()Landroid/view/KeyEvent$DispatcherState;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/view/KeyEvent$DispatcherState;->handleUpEvent(Landroid/view/KeyEvent;)V

    .line 408
    invoke-virtual {p2}, Landroid/view/KeyEvent;->isTracking()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p2}, Landroid/view/KeyEvent;->isCanceled()Z

    move-result v1

    if-nez v1, :cond_1

    .line 409
    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mUrlBarDelegate:Lcom/google/android/apps/chrome/omnibox/UrlBar$UrlBarDelegate;

    invoke-interface {v1}, Lcom/google/android/apps/chrome/omnibox/UrlBar$UrlBarDelegate;->backKeyPressed()V

    goto :goto_0

    .line 414
    :cond_1
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/chrome/widget/VerticallyFixedEditText;->onKeyPreIme(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onSelectionChanged(II)V
    .locals 3

    .prologue
    .line 300
    if-ltz p1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->getQueryTextOffset()I

    move-result v0

    if-ge p1, v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->getEditableText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->getQueryTextOffset()I

    move-result v1

    if-le v0, v1, :cond_1

    .line 302
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->getQueryTextOffset()I

    move-result v0

    .line 303
    if-ge p2, v0, :cond_0

    move p2, v0

    .line 304
    :cond_0
    invoke-virtual {p0, v0, p2}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->setSelection(II)V

    .line 325
    :goto_0
    return-void

    .line 308
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->getText()Landroid/text/Editable;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mAutocompleteSpan:Lcom/google/android/apps/chrome/omnibox/UrlBar$AutocompleteSpan;

    invoke-interface {v0, v1}, Landroid/text/Editable;->getSpanStart(Ljava/lang/Object;)I

    move-result v0

    .line 309
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->getText()Landroid/text/Editable;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mAutocompleteSpan:Lcom/google/android/apps/chrome/omnibox/UrlBar$AutocompleteSpan;

    invoke-interface {v1, v2}, Landroid/text/Editable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v1

    .line 310
    if-ltz v0, :cond_3

    if-ne v0, p1, :cond_2

    if-eq v1, p2, :cond_3

    .line 313
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mAutocompleteSpan:Lcom/google/android/apps/chrome/omnibox/UrlBar$AutocompleteSpan;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/omnibox/UrlBar$AutocompleteSpan;->clearSpan()V

    .line 322
    if-gt p2, v0, :cond_3

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->length()I

    move-result v2

    invoke-interface {v1, v0, v2}, Landroid/text/Editable;->delete(II)Landroid/text/Editable;

    .line 324
    :cond_3
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/chrome/widget/VerticallyFixedEditText;->onSelectionChanged(II)V

    goto :goto_0
.end method

.method public onTextContextMenuItem(I)Z
    .locals 7

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 533
    const v0, 0x1020022

    if-ne p1, v0, :cond_2

    .line 534
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v2, "clipboard"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ClipboardManager;

    .line 536
    invoke-virtual {v0}, Landroid/content/ClipboardManager;->getPrimaryClip()Landroid/content/ClipData;

    move-result-object v2

    .line 537
    if-eqz v2, :cond_2

    .line 538
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move v0, v1

    .line 539
    :goto_0
    invoke-virtual {v2}, Landroid/content/ClipData;->getItemCount()I

    move-result v5

    if-ge v0, v5, :cond_0

    .line 540
    invoke-virtual {v2, v0}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    move-result-object v5

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/content/ClipData$Item;->coerceToText(Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 539
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 542
    :cond_0
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->sanitizePastedText(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 545
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    .line 547
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->isFocused()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 548
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->getSelectionStart()I

    move-result v0

    .line 549
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->getSelectionEnd()I

    move-result v5

    .line 551
    invoke-static {v0, v5}, Ljava/lang/Math;->min(II)I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 552
    invoke-static {v0, v5}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    move v1, v2

    .line 555
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-static {v2, v0}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    .line 556
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2, v1, v0, v4}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    move v0, v3

    .line 590
    :goto_1
    return v0

    .line 561
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mOriginalUrlLocation:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mFormattedUrlLocation:Ljava/lang/String;

    if-nez v0, :cond_4

    .line 562
    :cond_3
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/widget/VerticallyFixedEditText;->onTextContextMenuItem(I)Z

    move-result v0

    goto :goto_1

    .line 565
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->getSelectionStart()I

    move-result v0

    .line 566
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->getSelectionEnd()I

    move-result v2

    .line 570
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    .line 571
    if-nez v0, :cond_7

    const v0, 0x1020020

    if-eq p1, v0, :cond_5

    const v0, 0x1020021

    if-ne p1, v0, :cond_7

    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mFormattedUrlLocation:Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mFormattedUrlLocation:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lt v2, v0, :cond_7

    .line 575
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mOriginalUrlLocation:Ljava/lang/String;

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v5, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mFormattedUrlLocation:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 577
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mFormattedUrlLocation:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    sub-int v0, v2, v0

    iget-object v2, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mOriginalUrlLocation:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v0, v2

    .line 579
    iget-object v2, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mUrlBarDelegate:Lcom/google/android/apps/chrome/omnibox/UrlBar$UrlBarDelegate;

    invoke-interface {v2, v3}, Lcom/google/android/apps/chrome/omnibox/UrlBar$UrlBarDelegate;->setIgnoreURLBarModification(Z)V

    .line 580
    invoke-virtual {p0, v5}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->setText(Ljava/lang/CharSequence;)V

    .line 581
    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->setSelection(II)V

    .line 582
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/widget/VerticallyFixedEditText;->onTextContextMenuItem(I)Z

    move-result v0

    .line 583
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 584
    invoke-virtual {p0, v4}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->setText(Ljava/lang/CharSequence;)V

    .line 585
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->length()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->setSelection(I)V

    .line 587
    :cond_6
    iget-object v2, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mUrlBarDelegate:Lcom/google/android/apps/chrome/omnibox/UrlBar$UrlBarDelegate;

    invoke-interface {v2, v1}, Lcom/google/android/apps/chrome/omnibox/UrlBar$UrlBarDelegate;->setIgnoreURLBarModification(Z)V

    goto/16 :goto_1

    .line 590
    :cond_7
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/widget/VerticallyFixedEditText;->onTextContextMenuItem(I)Z

    move-result v0

    goto/16 :goto_1
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 419
    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mUrlBarDelegate:Lcom/google/android/apps/chrome/omnibox/UrlBar$UrlBarDelegate;

    invoke-interface {v1}, Lcom/google/android/apps/chrome/omnibox/UrlBar$UrlBarDelegate;->getCurrentTab()Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v1

    .line 420
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getBackgroundContentViewHelper()Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mUrlBarDelegate:Lcom/google/android/apps/chrome/omnibox/UrlBar$UrlBarDelegate;

    invoke-interface {v2}, Lcom/google/android/apps/chrome/omnibox/UrlBar$UrlBarDelegate;->showingOriginalUrlForPreview()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 425
    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getBackgroundContentViewHelper()Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->forceSwappingContentViews()V

    .line 430
    :cond_0
    iget-boolean v2, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mFocused:Z

    if-nez v2, :cond_2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v2

    if-nez v2, :cond_2

    .line 431
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getDownTime()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mFocusTriggerEventDownTime:J

    .line 432
    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mGestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v1, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 448
    :cond_1
    :goto_0
    return v0

    .line 434
    :cond_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getDownTime()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mFocusTriggerEventDownTime:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_3

    .line 435
    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mGestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v1, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 436
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v1

    if-ne v1, v0, :cond_1

    .line 437
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->requestFocus()Z

    goto :goto_0

    .line 442
    :cond_3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_4

    if-eqz v1, :cond_4

    .line 444
    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    .line 445
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->hideSelectActionBar()V

    .line 448
    :cond_4
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/widget/VerticallyFixedEditText;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onWindowFocusChanged(Z)V
    .locals 1

    .prologue
    .line 371
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/widget/VerticallyFixedEditText;->onWindowFocusChanged(Z)V

    .line 372
    if-eqz p1, :cond_1

    .line 373
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mShowKeyboardOnWindowFocus:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 377
    new-instance v0, Lcom/google/android/apps/chrome/omnibox/UrlBar$3;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/omnibox/UrlBar$3;-><init>(Lcom/google/android/apps/chrome/omnibox/UrlBar;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->post(Ljava/lang/Runnable;)Z

    .line 384
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mShowKeyboardOnWindowFocus:Z

    .line 386
    :cond_1
    return-void
.end method

.method protected onWindowVisibilityChanged(I)V
    .locals 1

    .prologue
    .line 365
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/widget/VerticallyFixedEditText;->onWindowVisibilityChanged(I)V

    .line 366
    const/16 v0, 0x8

    if-ne p1, v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mShowKeyboardOnWindowFocus:Z

    .line 367
    :cond_0
    return-void
.end method

.method protected openKeyboard()V
    .locals 3

    .prologue
    .line 340
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    .line 341
    new-instance v1, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    .line 342
    new-instance v2, Lcom/google/android/apps/chrome/omnibox/UrlBar$2;

    invoke-direct {v2, p0, v1, v0}, Lcom/google/android/apps/chrome/omnibox/UrlBar$2;-><init>(Lcom/google/android/apps/chrome/omnibox/UrlBar;Ljava/util/concurrent/atomic/AtomicInteger;Landroid/os/Handler;)V

    .line 360
    invoke-interface {v2}, Ljava/lang/Runnable;->run()V

    .line 361
    return-void
.end method

.method public sendAccessibilityEventUnchecked(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 2

    .prologue
    .line 752
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mDisableTextAccessibilityEvents:Z

    if-eqz v0, :cond_1

    .line 753
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    move-result v0

    const/16 v1, 0x2000

    if-eq v0, v1, :cond_0

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    move-result v0

    const/16 v1, 0x10

    if-ne v0, v1, :cond_1

    .line 759
    :cond_0
    :goto_0
    return-void

    .line 758
    :cond_1
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/widget/VerticallyFixedEditText;->sendAccessibilityEventUnchecked(Landroid/view/accessibility/AccessibilityEvent;)V

    goto :goto_0
.end method

.method public setAccessibilityTextOverride(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 673
    iput-object p1, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mAccessibilityTextOverride:Ljava/lang/String;

    .line 674
    return-void
.end method

.method public setAutocompleteText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 634
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->getQueryTextOffset()I

    move-result v1

    add-int/2addr v0, v1

    .line 636
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->getQueryText()Ljava/lang/String;

    move-result-object v1

    .line 637
    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/CharSequence;

    aput-object p1, v2, v4

    aput-object p2, v2, v5

    invoke-static {v2}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v2

    .line 639
    iget-object v3, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mUrlBarDelegate:Lcom/google/android/apps/chrome/omnibox/UrlBar$UrlBarDelegate;

    invoke-interface {v3, v5}, Lcom/google/android/apps/chrome/omnibox/UrlBar$UrlBarDelegate;->setIgnoreURLBarModification(Z)V

    .line 640
    iput-boolean v5, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mDisableTextAccessibilityEvents:Z

    .line 642
    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 645
    invoke-static {v2, v1}, Landroid/text/TextUtils;->indexOf(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)I

    move-result v3

    if-nez v3, :cond_3

    .line 646
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    move-result v3

    invoke-interface {v2, v1, v3}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->append(Ljava/lang/CharSequence;)V

    .line 652
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->getSelectionStart()I

    move-result v1

    if-ne v1, v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->getSelectionEnd()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->length()I

    move-result v2

    if-eq v1, v2, :cond_2

    .line 654
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->setSelection(II)V

    .line 656
    invoke-interface {p2}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-eqz v0, :cond_2

    .line 657
    invoke-direct {p0, p2}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->sendAutocompleteAccessibilityEvent(Ljava/lang/CharSequence;)V

    .line 660
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mAutocompleteSpan:Lcom/google/android/apps/chrome/omnibox/UrlBar$AutocompleteSpan;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/chrome/omnibox/UrlBar$AutocompleteSpan;->setSpan(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 662
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mUrlBarDelegate:Lcom/google/android/apps/chrome/omnibox/UrlBar$UrlBarDelegate;

    invoke-interface {v0, v4}, Lcom/google/android/apps/chrome/omnibox/UrlBar$UrlBarDelegate;->setIgnoreURLBarModification(Z)V

    .line 663
    iput-boolean v4, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mDisableTextAccessibilityEvents:Z

    .line 664
    return-void

    .line 648
    :cond_3
    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->setUrl(Ljava/lang/String;Ljava/lang/String;)Z

    goto :goto_0
.end method

.method setDelegate(Lcom/google/android/apps/chrome/omnibox/UrlBar$UrlBarDelegate;)V
    .locals 0

    .prologue
    .line 528
    iput-object p1, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mUrlBarDelegate:Lcom/google/android/apps/chrome/omnibox/UrlBar$UrlBarDelegate;

    .line 529
    return-void
.end method

.method setLocationBarTextWatcher(Landroid/text/TextWatcher;)V
    .locals 0

    .prologue
    .line 520
    iput-object p1, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mLocationBarTextWatcher:Landroid/text/TextWatcher;

    .line 521
    return-void
.end method

.method public setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V
    .locals 5

    .prologue
    .line 700
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->getEditableText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 701
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/chrome/widget/VerticallyFixedEditText;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    .line 702
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mAccessibilityTextOverride:Ljava/lang/String;

    .line 706
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mAutocompleteSpan:Lcom/google/android/apps/chrome/omnibox/UrlBar$AutocompleteSpan;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mAutocompleteSpan:Lcom/google/android/apps/chrome/omnibox/UrlBar$AutocompleteSpan;

    # getter for: Lcom/google/android/apps/chrome/omnibox/UrlBar$AutocompleteSpan;->mUserText:Ljava/lang/CharSequence;
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/UrlBar$AutocompleteSpan;->access$200(Lcom/google/android/apps/chrome/omnibox/UrlBar$AutocompleteSpan;)Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mAutocompleteSpan:Lcom/google/android/apps/chrome/omnibox/UrlBar$AutocompleteSpan;

    # getter for: Lcom/google/android/apps/chrome/omnibox/UrlBar$AutocompleteSpan;->mAutocompleteText:Ljava/lang/CharSequence;
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/UrlBar$AutocompleteSpan;->access$100(Lcom/google/android/apps/chrome/omnibox/UrlBar$AutocompleteSpan;)Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 709
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->getText()Landroid/text/Editable;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mAutocompleteSpan:Lcom/google/android/apps/chrome/omnibox/UrlBar$AutocompleteSpan;

    invoke-interface {v0, v1}, Landroid/text/Editable;->getSpanStart(Ljava/lang/Object;)I

    move-result v0

    if-ltz v0, :cond_1

    .line 710
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->getEditableText()Landroid/text/Editable;

    move-result-object v0

    .line 713
    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mAutocompleteSpan:Lcom/google/android/apps/chrome/omnibox/UrlBar$AutocompleteSpan;

    # getter for: Lcom/google/android/apps/chrome/omnibox/UrlBar$AutocompleteSpan;->mUserText:Ljava/lang/CharSequence;
    invoke-static {v1}, Lcom/google/android/apps/chrome/omnibox/UrlBar$AutocompleteSpan;->access$200(Lcom/google/android/apps/chrome/omnibox/UrlBar$AutocompleteSpan;)Ljava/lang/CharSequence;

    move-result-object v1

    .line 714
    iget-object v2, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mAutocompleteSpan:Lcom/google/android/apps/chrome/omnibox/UrlBar$AutocompleteSpan;

    # getter for: Lcom/google/android/apps/chrome/omnibox/UrlBar$AutocompleteSpan;->mAutocompleteText:Ljava/lang/CharSequence;
    invoke-static {v2}, Lcom/google/android/apps/chrome/omnibox/UrlBar$AutocompleteSpan;->access$100(Lcom/google/android/apps/chrome/omnibox/UrlBar$AutocompleteSpan;)Ljava/lang/CharSequence;

    move-result-object v2

    .line 715
    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->getQueryTextOffset()I

    move-result v3

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    move-result v4

    add-int/2addr v3, v4

    if-lt v0, v3, :cond_1

    .line 718
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0, v1}, Landroid/text/TextUtils;->indexOf(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->getQueryTextOffset()I

    move-result v3

    if-ne v0, v3, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0, v2}, Landroid/text/TextUtils;->indexOf(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->getQueryTextOffset()I

    move-result v2

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    add-int/2addr v1, v2

    if-eq v0, v1, :cond_2

    .line 722
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mAutocompleteSpan:Lcom/google/android/apps/chrome/omnibox/UrlBar$AutocompleteSpan;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omnibox/UrlBar$AutocompleteSpan;->clearSpan()V

    .line 726
    :cond_2
    return-void
.end method

.method public setUrl(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 601
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 603
    :try_start_0
    new-instance v0, Ljava/net/URL;

    invoke-direct {v0, p1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 604
    invoke-virtual {v0}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v1

    invoke-static {p2, v1}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->getUrlContentsPrePath(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mFormattedUrlLocation:Ljava/lang/String;

    .line 606
    invoke-virtual {v0}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->getUrlContentsPrePath(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mOriginalUrlLocation:Ljava/lang/String;
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 618
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->getEditableText()Landroid/text/Editable;

    move-result-object v0

    .line 619
    invoke-virtual {p0, p2}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->setText(Ljava/lang/CharSequence;)V

    .line 620
    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mUrlBarDelegate:Lcom/google/android/apps/chrome/omnibox/UrlBar$UrlBarDelegate;

    invoke-interface {v1}, Lcom/google/android/apps/chrome/omnibox/UrlBar$UrlBarDelegate;->onFinishSetUrl()V

    .line 622
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->isFocused()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->scrollToTLD()V

    .line 624
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->getEditableText()Landroid/text/Editable;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    return v0

    .line 609
    :catch_0
    move-exception v0

    iput-object v2, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mOriginalUrlLocation:Ljava/lang/String;

    .line 610
    iput-object v2, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mFormattedUrlLocation:Ljava/lang/String;

    goto :goto_0

    .line 613
    :cond_1
    iput-object v2, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mOriginalUrlLocation:Ljava/lang/String;

    .line 614
    iput-object v2, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mFormattedUrlLocation:Ljava/lang/String;

    move-object p2, p1

    .line 615
    goto :goto_0

    .line 624
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public setUrlDirectionListener(Lcom/google/android/apps/chrome/omnibox/UrlBar$UrlDirectionListener;)V
    .locals 2

    .prologue
    .line 513
    iput-object p1, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mUrlDirectionListener:Lcom/google/android/apps/chrome/omnibox/UrlBar$UrlDirectionListener;

    .line 514
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mUrlDirectionListener:Lcom/google/android/apps/chrome/omnibox/UrlBar$UrlDirectionListener;

    if-eqz v0, :cond_0

    .line 515
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mUrlDirectionListener:Lcom/google/android/apps/chrome/omnibox/UrlBar$UrlDirectionListener;

    iget v1, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mUrlDirection:I

    invoke-interface {v0, v1}, Lcom/google/android/apps/chrome/omnibox/UrlBar$UrlDirectionListener;->onUrlDirectionChanged(I)V

    .line 517
    :cond_0
    return-void
.end method

.method public setUseDarkTextColors(Z)V
    .locals 3

    .prologue
    .line 230
    if-eqz p1, :cond_3

    .line 231
    iget v0, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mDarkDefaultTextColor:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->setTextColor(I)V

    .line 232
    iget v0, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mDarkHostColor:I

    iput v0, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mHostColor:I

    .line 241
    :goto_0
    const/4 v0, 0x0

    .line 242
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->getText()Landroid/text/Editable;

    move-result-object v1

    .line 243
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 244
    const-string/jumbo v0, ""

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->setText(Ljava/lang/CharSequence;)V

    .line 245
    const/4 v0, 0x1

    .line 247
    :cond_0
    if-eqz p1, :cond_4

    .line 248
    iget-object v2, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mDarkHintColor:Landroid/content/res/ColorStateList;

    invoke-virtual {p0, v2}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->setHintTextColor(Landroid/content/res/ColorStateList;)V

    .line 252
    :goto_1
    if-eqz v0, :cond_1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->setText(Ljava/lang/CharSequence;)V

    .line 254
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->hasFocus()Z

    move-result v0

    if-nez v0, :cond_2

    .line 255
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->deEmphasizeUrl()V

    .line 256
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->emphasizeUrl()V

    .line 258
    :cond_2
    return-void

    .line 234
    :cond_3
    iget v0, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mLightDefaultTextColor:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->setTextColor(I)V

    .line 235
    iget v0, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mLightHostColor:I

    iput v0, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mHostColor:I

    goto :goto_0

    .line 250
    :cond_4
    iget v2, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar;->mLightHintColor:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->setHintTextColor(I)V

    goto :goto_1
.end method
