.class Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$1;
.super Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;
.source "YouTubeMediaRouteController.java"


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;)V
    .locals 0

    .prologue
    .line 110
    iput-object p1, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$1;->this$0:Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 113
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "tabId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 116
    iget-object v1, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$1;->this$0:Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->getOwnerTab()Lorg/chromium/chrome/browser/Tab;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$1;->this$0:Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->getOwnerTab()Lorg/chromium/chrome/browser/Tab;

    move-result-object v1

    invoke-virtual {v1}, Lorg/chromium/chrome/browser/Tab;->getId()I

    move-result v1

    if-eq v1, v0, :cond_1

    .line 152
    :cond_0
    :goto_0
    return-void

    .line 118
    :cond_1
    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    goto :goto_0

    .line 120
    :sswitch_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$1;->this$0:Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;

    invoke-static {v0}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->access$000(Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;)V

    goto :goto_0

    .line 124
    :sswitch_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$1;->this$0:Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;

    # getter for: Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mLoadingPairingCode:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->access$100(Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 126
    invoke-static {v3}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->castYouTubePlayerResult(Z)V

    .line 127
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$1;->this$0:Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;

    # invokes: Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->startUserControls()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->access$200(Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;)V

    .line 128
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$1;->this$0:Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;

    # getter for: Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mConnectedClient:Lcom/google/android/gms/common/api/i;
    invoke-static {v0}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->access$300(Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;)Lcom/google/android/gms/common/api/i;

    move-result-object v0

    if-nez v0, :cond_2

    .line 129
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$1;->this$0:Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;

    # invokes: Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->installMediaStatusBroadcastReceiver()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->access$400(Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;)V

    .line 130
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$1;->this$0:Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;

    # invokes: Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->setupMediaStatusUpdateIntent()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->access$500(Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;)V

    .line 132
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$1;->this$0:Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mLoadingPairingCode:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->access$102(Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;Z)Z

    .line 133
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$1;->this$0:Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->getListeners()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/videofling/MediaRouteController$Listener;

    .line 134
    iget-object v2, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$1;->this$0:Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;

    invoke-interface {v0, v2}, Lcom/google/android/apps/chrome/videofling/MediaRouteController$Listener;->onPrepared(Lcom/google/android/apps/chrome/videofling/MediaRouteController;)V

    goto :goto_1

    .line 136
    :cond_3
    invoke-static {}, Lorg/chromium/base/library_loader/LibraryLoader;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {v3}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->castDefaultPlayerResult(Z)V

    goto :goto_0

    .line 143
    :sswitch_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$1;->this$0:Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->getOwnerTab()Lorg/chromium/chrome/browser/Tab;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/Tab;->getUrl()Ljava/lang/String;

    move-result-object v0

    .line 144
    invoke-static {v0}, Lcom/google/android/apps/chrome/videofling/RemoteMediaUtils;->isYouTubeUrl(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$1;->this$0:Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;

    # getter for: Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mLoadingPairingCode:Z
    invoke-static {v1}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->access$100(Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 145
    iget-object v1, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$1;->this$0:Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;

    # setter for: Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mYouTubeLocalUrl:Ljava/lang/String;
    invoke-static {v1, v0}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->access$602(Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0

    .line 118
    nop

    :sswitch_data_0
    .sparse-switch
        0x5 -> :sswitch_0
        0x9 -> :sswitch_1
        0x19 -> :sswitch_2
    .end sparse-switch
.end method
