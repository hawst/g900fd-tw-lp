.class public Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;
.super Landroid/app/DialogFragment;
.source "EnhancedBookmarkDetailDialog.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnTouchListener;


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private mActionBarLayout:Landroid/widget/RelativeLayout;

.field private mAnimRunner:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog$AnimatorRunner;

.field private mAutoTagsFlowLayout:Lcom/google/android/apps/chrome/widget/FlowLayout;

.field private mAutoTagsLabel:Landroid/widget/TextView;

.field private mBookmarkId:Lorg/chromium/components/bookmarks/BookmarkId;

.field private mBookmarkModelObserver:Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkModelObserver;

.field private mCloseButton:Landroid/widget/ImageButton;

.field private mDeleteButton:Landroid/widget/ImageButton;

.field private mDescriptionEditText:Landroid/widget/EditText;

.field private final mEnhancedBookmarksModel:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;

.field private mFolderTextView:Landroid/widget/TextView;

.field private mImageView:Landroid/widget/ImageView;

.field private mIsEditingMode:Z

.field private mIsInterceptingClick:Z

.field private mMaskView:Landroid/view/View;

.field private mSaveButton:Landroid/widget/ImageButton;

.field private mScrollView:Landroid/widget/ScrollView;

.field private mShadow:Landroid/view/View;

.field private mTitleEditText:Landroid/widget/EditText;

.field private mUrlEditText:Landroid/widget/EditText;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 51
    const-class v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;Lorg/chromium/components/bookmarks/BookmarkId;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 115
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 55
    new-instance v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog$AnimatorRunner;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog$AnimatorRunner;-><init>(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog$1;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mAnimRunner:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog$AnimatorRunner;

    .line 75
    iput-boolean v2, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mIsEditingMode:Z

    .line 76
    iput-boolean v2, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mIsInterceptingClick:Z

    .line 78
    new-instance v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog$1;-><init>(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mBookmarkModelObserver:Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkModelObserver;

    .line 116
    iput-object p1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mEnhancedBookmarksModel:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;

    .line 117
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mEnhancedBookmarksModel:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;

    iget-object v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mBookmarkModelObserver:Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkModelObserver;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;->addModelObserver(Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkModelObserver;)V

    .line 118
    iput-object p2, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mBookmarkId:Lorg/chromium/components/bookmarks/BookmarkId;

    .line 119
    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;)Lorg/chromium/components/bookmarks/BookmarkId;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mBookmarkId:Lorg/chromium/components/bookmarks/BookmarkId;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;)Landroid/view/View;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mShadow:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mTitleEditText:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mUrlEditText:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;)V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->updateViews(Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;)V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->updateViews()V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;)Landroid/widget/ImageButton;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mCloseButton:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mImageView:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;)Landroid/widget/ScrollView;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mScrollView:Landroid/widget/ScrollView;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;)Landroid/widget/RelativeLayout;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mActionBarLayout:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;)Landroid/view/View;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mMaskView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;)Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mEnhancedBookmarksModel:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;

    return-object v0
.end method

.method private enterEditingMode(Landroid/widget/EditText;)V
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 241
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mIsEditingMode:Z

    if-eqz v0, :cond_0

    .line 281
    :goto_0
    return-void

    .line 242
    :cond_0
    iput-boolean v6, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mIsEditingMode:Z

    .line 246
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mMaskView:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 249
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mScrollView:Landroid/widget/ScrollView;

    sget-object v1, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    new-array v2, v7, [F

    iget-object v3, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mScrollView:Landroid/widget/ScrollView;

    invoke-virtual {v3}, Landroid/widget/ScrollView;->getScrollY()I

    move-result v3

    neg-int v3, v3

    int-to-float v3, v3

    aput v3, v2, v5

    iget-object v3, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mActionBarLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mImageView:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->getHeight()I

    move-result v4

    sub-int/2addr v3, v4

    int-to-float v3, v3

    aput v3, v2, v6

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 251
    iget-object v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mMaskView:Landroid/view/View;

    sget-object v2, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v3, v7, [F

    fill-array-data v3, :array_0

    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 253
    iget-object v2, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mAnimRunner:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog$AnimatorRunner;

    new-array v3, v7, [Landroid/animation/Animator;

    aput-object v0, v3, v5

    aput-object v1, v3, v6

    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    const/16 v1, 0x12c

    new-instance v3, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog$4;

    invoke-direct {v3, p0, p1}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog$4;-><init>(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;Landroid/widget/EditText;)V

    invoke-virtual {v2, v0, v1, v3}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog$AnimatorRunner;->run(Ljava/util/List;ILandroid/animation/Animator$AnimatorListener;)V

    goto :goto_0

    .line 251
    nop

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method private leaveEditingMode(Z)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x2

    const/4 v5, 0x0

    .line 284
    sget-boolean v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mIsEditingMode:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 285
    :cond_0
    iput-boolean v5, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mIsEditingMode:Z

    .line 289
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mImageView:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 290
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mMaskView:Landroid/view/View;

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    iget-object v2, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mMaskView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    iget v2, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    iget-object v3, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mImageView:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getHeight()I

    move-result v3

    invoke-direct {v1, v2, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 292
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mShadow:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 293
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mActionBarLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v5}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    .line 296
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mScrollView:Landroid/widget/ScrollView;

    sget-object v1, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    new-array v2, v6, [F

    iget-object v3, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mActionBarLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mImageView:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->getHeight()I

    move-result v4

    sub-int/2addr v3, v4

    int-to-float v3, v3

    aput v3, v2, v5

    const/4 v3, 0x0

    aput v3, v2, v7

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 298
    iget-object v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mMaskView:Landroid/view/View;

    sget-object v2, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v3, v6, [F

    fill-array-data v3, :array_0

    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 300
    iget-object v2, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mAnimRunner:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog$AnimatorRunner;

    new-array v3, v6, [Landroid/animation/Animator;

    aput-object v0, v3, v5

    aput-object v1, v3, v7

    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    const/16 v1, 0x12c

    new-instance v3, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog$5;

    invoke-direct {v3, p0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog$5;-><init>(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;)V

    invoke-virtual {v2, v0, v1, v3}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog$AnimatorRunner;->run(Ljava/util/List;ILandroid/animation/Animator$AnimatorListener;)V

    .line 316
    if-nez p1, :cond_1

    .line 317
    invoke-direct {p0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->updateViews()V

    .line 331
    :goto_0
    return-void

    .line 321
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mEnhancedBookmarksModel:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;

    iget-object v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mBookmarkId:Lorg/chromium/components/bookmarks/BookmarkId;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;->getBookmarkById(Lorg/chromium/components/bookmarks/BookmarkId;)Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;

    move-result-object v0

    .line 322
    invoke-virtual {v0}, Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;->getTitle()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mTitleEditText:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 323
    iget-object v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mEnhancedBookmarksModel:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;

    iget-object v2, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mBookmarkId:Lorg/chromium/components/bookmarks/BookmarkId;

    iget-object v3, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mTitleEditText:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;->setBookmarkTitle(Lorg/chromium/components/bookmarks/BookmarkId;Ljava/lang/String;)V

    .line 326
    :cond_2
    invoke-virtual {v0}, Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;->getUrl()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mUrlEditText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 327
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mEnhancedBookmarksModel:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;

    iget-object v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mBookmarkId:Lorg/chromium/components/bookmarks/BookmarkId;

    iget-object v2, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mUrlEditText:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;->setBookmarkUrl(Lorg/chromium/components/bookmarks/BookmarkId;Ljava/lang/String;)V

    .line 329
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mEnhancedBookmarksModel:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;

    iget-object v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mBookmarkId:Lorg/chromium/components/bookmarks/BookmarkId;

    iget-object v2, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mDescriptionEditText:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;->setBookmarkDescription(Lorg/chromium/components/bookmarks/BookmarkId;Ljava/lang/String;)V

    goto :goto_0

    .line 298
    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method

.method private updateViews()V
    .locals 2

    .prologue
    .line 179
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mEnhancedBookmarksModel:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;

    iget-object v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mBookmarkId:Lorg/chromium/components/bookmarks/BookmarkId;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;->getBookmarkById(Lorg/chromium/components/bookmarks/BookmarkId;)Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;

    move-result-object v0

    .line 180
    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->updateViews(Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;)V

    .line 181
    return-void
.end method

.method private updateViews(Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;)V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 184
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mTitleEditText:Landroid/widget/EditText;

    invoke-virtual {p1}, Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 185
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mUrlEditText:Landroid/widget/EditText;

    invoke-virtual {p1}, Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 186
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mFolderTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mEnhancedBookmarksModel:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;

    invoke-virtual {p1}, Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;->getParentId()Lorg/chromium/components/bookmarks/BookmarkId;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;->getBookmarkById(Lorg/chromium/components/bookmarks/BookmarkId;)Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;

    move-result-object v1

    invoke-virtual {v1}, Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 188
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mDescriptionEditText:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mEnhancedBookmarksModel:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;

    iget-object v2, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mBookmarkId:Lorg/chromium/components/bookmarks/BookmarkId;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;->getBookmarkDescription(Lorg/chromium/components/bookmarks/BookmarkId;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 190
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mImageView:Landroid/widget/ImageView;

    invoke-static {p1}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkColor;->generateBackgroundColor(Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    .line 191
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mEnhancedBookmarksModel:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;

    invoke-virtual {p1}, Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;->getUrl()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog$3;

    invoke-direct {v2, p0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog$3;-><init>(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;->salientImageForUrl(Ljava/lang/String;Lcom/google/android/apps/chrome/enhancedbookmark/ChromeEnhancedBookmarkBridge$SalientImageCallback;)V

    .line 202
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mMaskView:Landroid/view/View;

    invoke-static {p1}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkColor;->generateBackgroundColor(Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 204
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mAutoTagsFlowLayout:Lcom/google/android/apps/chrome/widget/FlowLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/FlowLayout;->removeAllViews()V

    .line 206
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mAutoTagsFlowLayout:Lcom/google/android/apps/chrome/widget/FlowLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/FlowLayout;->getChildCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 207
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mAutoTagsLabel:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 208
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mAutoTagsFlowLayout:Lcom/google/android/apps/chrome/widget/FlowLayout;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/chrome/widget/FlowLayout;->setVisibility(I)V

    .line 213
    :goto_0
    return-void

    .line 210
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mAutoTagsLabel:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 211
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mAutoTagsFlowLayout:Lcom/google/android/apps/chrome/widget/FlowLayout;

    invoke-virtual {v0, v4}, Lcom/google/android/apps/chrome/widget/FlowLayout;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 217
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mFolderTextView:Landroid/widget/TextView;

    if-ne p1, v0, :cond_1

    .line 218
    new-instance v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog;

    iget-object v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mEnhancedBookmarksModel:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;

    iget-object v2, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mBookmarkId:Lorg/chromium/components/bookmarks/BookmarkId;

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog;-><init>(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;Lorg/chromium/components/bookmarks/BookmarkId;)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog;->show(Landroid/app/FragmentManager;)V

    .line 230
    :cond_0
    :goto_0
    return-void

    .line 222
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mDeleteButton:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_2

    .line 223
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mEnhancedBookmarksModel:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;

    iget-object v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mBookmarkId:Lorg/chromium/components/bookmarks/BookmarkId;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;->deleteBookmarkItem(Lorg/chromium/components/bookmarks/BookmarkId;)V

    .line 224
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->dismiss()V

    goto :goto_0

    .line 227
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mCloseButton:Landroid/widget/ImageButton;

    if-eq p1, v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mSaveButton:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_4

    :cond_3
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mIsEditingMode:Z

    if-nez v0, :cond_4

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->dismiss()V

    .line 228
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mCloseButton:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_5

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mIsEditingMode:Z

    if-eqz v0, :cond_5

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->leaveEditingMode(Z)V

    .line 229
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mSaveButton:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mIsEditingMode:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->leaveEditingMode(Z)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 164
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 165
    const/4 v0, 0x2

    sget v1, Lcom/google/android/apps/chrome/R$style;->EnhancedBookmarkDialog:I

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->setStyle(II)V

    .line 166
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    .prologue
    .line 170
    new-instance v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog$2;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->getTheme()I

    move-result v2

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog$2;-><init>(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;Landroid/content/Context;I)V

    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 124
    sget v0, Lcom/google/android/apps/chrome/R$layout;->eb_detail_dialog:I

    invoke-virtual {p1, v0, p2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 126
    sget v0, Lcom/google/android/apps/chrome/R$id;->eb_detail_scroll_view:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    iput-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mScrollView:Landroid/widget/ScrollView;

    .line 127
    sget v0, Lcom/google/android/apps/chrome/R$id;->eb_detail_image_view:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mImageView:Landroid/widget/ImageView;

    .line 128
    sget v0, Lcom/google/android/apps/chrome/R$id;->eb_detail_title:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mTitleEditText:Landroid/widget/EditText;

    .line 129
    sget v0, Lcom/google/android/apps/chrome/R$id;->eb_detail_url:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mUrlEditText:Landroid/widget/EditText;

    .line 130
    sget v0, Lcom/google/android/apps/chrome/R$id;->eb_detail_folder_textview:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mFolderTextView:Landroid/widget/TextView;

    .line 132
    sget v0, Lcom/google/android/apps/chrome/R$id;->eb_detail_autotag_label:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mAutoTagsLabel:Landroid/widget/TextView;

    .line 133
    sget v0, Lcom/google/android/apps/chrome/R$id;->eb_detail_flow_layout:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/widget/FlowLayout;

    iput-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mAutoTagsFlowLayout:Lcom/google/android/apps/chrome/widget/FlowLayout;

    .line 135
    sget v0, Lcom/google/android/apps/chrome/R$id;->eb_detail_description:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mDescriptionEditText:Landroid/widget/EditText;

    .line 137
    sget v0, Lcom/google/android/apps/chrome/R$id;->eb_detail_image_mask:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mMaskView:Landroid/view/View;

    .line 138
    sget v0, Lcom/google/android/apps/chrome/R$id;->eb_detail_action_bar:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mActionBarLayout:Landroid/widget/RelativeLayout;

    .line 140
    sget v0, Lcom/google/android/apps/chrome/R$id;->eb_detail_actionbar_save_button:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mSaveButton:Landroid/widget/ImageButton;

    .line 142
    sget v0, Lcom/google/android/apps/chrome/R$id;->eb_detail_actionbar_delete_button:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mDeleteButton:Landroid/widget/ImageButton;

    .line 144
    sget v0, Lcom/google/android/apps/chrome/R$id;->eb_detail_actionbar_close_button:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mCloseButton:Landroid/widget/ImageButton;

    .line 146
    sget v0, Lcom/google/android/apps/chrome/R$id;->eb_detail_shadow:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mShadow:Landroid/view/View;

    .line 148
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mTitleEditText:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 149
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mUrlEditText:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 150
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mDescriptionEditText:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 152
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mSaveButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 153
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mDeleteButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 154
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mCloseButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 155
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mFolderTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 157
    invoke-direct {p0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->updateViews()V

    .line 158
    return-object v1
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 358
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mEnhancedBookmarksModel:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;

    iget-object v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mBookmarkModelObserver:Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkModelObserver;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;->removeModelObserver(Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkModelObserver;)V

    .line 359
    invoke-super {p0}, Landroid/app/DialogFragment;->onDestroy()V

    .line 360
    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 339
    iget-boolean v2, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mIsEditingMode:Z

    if-eqz v2, :cond_1

    .line 353
    :cond_0
    :goto_0
    return v0

    .line 340
    :cond_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-nez v2, :cond_2

    .line 341
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mIsInterceptingClick:Z

    move v0, v1

    .line 342
    goto :goto_0

    .line 344
    :cond_2
    iget-boolean v2, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mIsInterceptingClick:Z

    if-eqz v2, :cond_3

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_3

    move v0, v1

    .line 345
    goto :goto_0

    .line 347
    :cond_3
    iget-boolean v2, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mIsInterceptingClick:Z

    if-eqz v2, :cond_0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-ne v2, v1, :cond_0

    .line 348
    iput-boolean v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->mIsInterceptingClick:Z

    .line 349
    invoke-virtual {p1}, Landroid/view/View;->performClick()Z

    .line 350
    check-cast p1, Landroid/widget/EditText;

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDetailDialog;->enterEditingMode(Landroid/widget/EditText;)V

    move v0, v1

    .line 351
    goto :goto_0
.end method

.method public show(Landroid/app/FragmentManager;)V
    .locals 1

    .prologue
    .line 366
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-super {p0, p1, v0}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 367
    return-void
.end method
