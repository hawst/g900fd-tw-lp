.class public final enum Lcom/google/android/apps/chrome/tabs/CompositorButton$Property;
.super Ljava/lang/Enum;
.source "CompositorButton.java"


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/chrome/tabs/CompositorButton$Property;

.field public static final enum OPACITY:Lcom/google/android/apps/chrome/tabs/CompositorButton$Property;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 24
    new-instance v0, Lcom/google/android/apps/chrome/tabs/CompositorButton$Property;

    const-string/jumbo v1, "OPACITY"

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/chrome/tabs/CompositorButton$Property;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/tabs/CompositorButton$Property;->OPACITY:Lcom/google/android/apps/chrome/tabs/CompositorButton$Property;

    .line 23
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/google/android/apps/chrome/tabs/CompositorButton$Property;

    sget-object v1, Lcom/google/android/apps/chrome/tabs/CompositorButton$Property;->OPACITY:Lcom/google/android/apps/chrome/tabs/CompositorButton$Property;

    aput-object v1, v0, v2

    sput-object v0, Lcom/google/android/apps/chrome/tabs/CompositorButton$Property;->$VALUES:[Lcom/google/android/apps/chrome/tabs/CompositorButton$Property;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/chrome/tabs/CompositorButton$Property;
    .locals 1

    .prologue
    .line 23
    const-class v0, Lcom/google/android/apps/chrome/tabs/CompositorButton$Property;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/tabs/CompositorButton$Property;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/chrome/tabs/CompositorButton$Property;
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lcom/google/android/apps/chrome/tabs/CompositorButton$Property;->$VALUES:[Lcom/google/android/apps/chrome/tabs/CompositorButton$Property;

    invoke-virtual {v0}, [Lcom/google/android/apps/chrome/tabs/CompositorButton$Property;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/chrome/tabs/CompositorButton$Property;

    return-object v0
.end method
