.class Lcom/google/android/apps/chrome/feedback/LegacyFeedbackReporter$1;
.super Ljava/lang/Object;
.source "LegacyFeedbackReporter.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/feedback/LegacyFeedbackReporter;

.field final synthetic val$screenshot:Landroid/graphics/Bitmap;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/feedback/LegacyFeedbackReporter;Landroid/graphics/Bitmap;)V
    .locals 0

    .prologue
    .line 52
    iput-object p1, p0, Lcom/google/android/apps/chrome/feedback/LegacyFeedbackReporter$1;->this$0:Lcom/google/android/apps/chrome/feedback/LegacyFeedbackReporter;

    iput-object p2, p0, Lcom/google/android/apps/chrome/feedback/LegacyFeedbackReporter$1;->val$screenshot:Landroid/graphics/Bitmap;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 4

    .prologue
    .line 56
    :try_start_0
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    .line 57
    iget-object v1, p0, Lcom/google/android/apps/chrome/feedback/LegacyFeedbackReporter$1;->val$screenshot:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_0

    .line 58
    iget-object v1, p0, Lcom/google/android/apps/chrome/feedback/LegacyFeedbackReporter$1;->val$screenshot:Landroid/graphics/Bitmap;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/graphics/Bitmap;->writeToParcel(Landroid/os/Parcel;I)V

    .line 60
    :cond_0
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-interface {p2, v1, v0, v2, v3}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 65
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/feedback/LegacyFeedbackReporter$1;->this$0:Lcom/google/android/apps/chrome/feedback/LegacyFeedbackReporter;

    # getter for: Lcom/google/android/apps/chrome/feedback/LegacyFeedbackReporter;->mApplicationContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/google/android/apps/chrome/feedback/LegacyFeedbackReporter;->access$000(Lcom/google/android/apps/chrome/feedback/LegacyFeedbackReporter;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 66
    return-void

    .line 61
    :catch_0
    move-exception v0

    .line 62
    const-string/jumbo v1, "LegacyFeedbackReporter"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Failed to send feedback: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 0

    .prologue
    .line 70
    return-void
.end method
