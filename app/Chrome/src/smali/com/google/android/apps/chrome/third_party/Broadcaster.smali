.class public Lcom/google/android/apps/chrome/third_party/Broadcaster;
.super Ljava/lang/Object;
.source "Broadcaster.java"


# instance fields
.field private mReg:Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    return-void
.end method


# virtual methods
.method public broadcast(Landroid/os/Message;Z)V
    .locals 8

    .prologue
    .line 219
    monitor-enter p0

    .line 220
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/Broadcaster;->mReg:Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;

    if-nez v0, :cond_0

    .line 221
    monitor-exit p0

    .line 249
    :goto_0
    return-void

    .line 224
    :cond_0
    iget v2, p1, Landroid/os/Message;->what:I

    .line 225
    iget-object v1, p0, Lcom/google/android/apps/chrome/third_party/Broadcaster;->mReg:Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;

    move-object v0, v1

    .line 228
    :cond_1
    iget v3, v0, Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;->senderWhat:I

    if-ge v3, v2, :cond_2

    .line 229
    iget-object v0, v0, Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;->next:Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;

    .line 232
    if-ne v0, v1, :cond_1

    .line 233
    :cond_2
    iget v1, v0, Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;->senderWhat:I

    if-ne v1, v2, :cond_4

    .line 234
    iget-object v1, v0, Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;->targets:[Landroid/os/Handler;

    .line 235
    iget-object v2, v0, Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;->targetWhats:[I

    .line 236
    array-length v3, v1

    .line 237
    const/4 v0, 0x0

    :goto_1
    if-ge v0, v3, :cond_4

    .line 238
    aget-object v4, v1, v0

    .line 239
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v5

    .line 240
    invoke-virtual {v5, p1}, Landroid/os/Message;->copyFrom(Landroid/os/Message;)V

    .line 241
    aget v6, v2, v0

    iput v6, v5, Landroid/os/Message;->what:I

    .line 242
    if-eqz p2, :cond_3

    invoke-virtual {v4}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v6

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v7

    if-ne v6, v7, :cond_3

    .line 243
    invoke-virtual {v4, v5}, Landroid/os/Handler;->dispatchMessage(Landroid/os/Message;)V

    .line 237
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 245
    :cond_3
    invoke-virtual {v4, v5}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    .line 249
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_4
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public cancelRequest(ILandroid/os/Handler;I)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 115
    monitor-enter p0

    .line 116
    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/chrome/third_party/Broadcaster;->mReg:Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;

    .line 119
    if-nez v2, :cond_0

    .line 120
    monitor-exit p0

    .line 154
    :goto_0
    return-void

    :cond_0
    move-object v1, v2

    .line 124
    :cond_1
    iget v3, v1, Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;->senderWhat:I

    if-ge v3, p1, :cond_2

    .line 125
    iget-object v1, v1, Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;->next:Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;

    .line 128
    if-ne v1, v2, :cond_1

    .line 130
    :cond_2
    iget v2, v1, Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;->senderWhat:I

    if-ne v2, p1, :cond_4

    .line 131
    iget-object v2, v1, Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;->targets:[Landroid/os/Handler;

    .line 132
    iget-object v3, v1, Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;->targetWhats:[I

    .line 133
    array-length v4, v2

    .line 134
    :goto_1
    if-ge v0, v4, :cond_4

    .line 135
    aget-object v5, v2, v0

    if-ne v5, p2, :cond_5

    aget v5, v3, v0

    if-ne v5, p3, :cond_5

    .line 136
    add-int/lit8 v5, v4, -0x1

    new-array v5, v5, [Landroid/os/Handler;

    iput-object v5, v1, Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;->targets:[Landroid/os/Handler;

    .line 137
    add-int/lit8 v5, v4, -0x1

    new-array v5, v5, [I

    iput-object v5, v1, Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;->targetWhats:[I

    .line 138
    if-lez v0, :cond_3

    .line 139
    const/4 v5, 0x0

    iget-object v6, v1, Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;->targets:[Landroid/os/Handler;

    const/4 v7, 0x0

    invoke-static {v2, v5, v6, v7, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 140
    const/4 v5, 0x0

    iget-object v6, v1, Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;->targetWhats:[I

    const/4 v7, 0x0

    invoke-static {v3, v5, v6, v7, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 143
    :cond_3
    sub-int/2addr v4, v0

    add-int/lit8 v4, v4, -0x1

    .line 144
    if-eqz v4, :cond_4

    .line 145
    add-int/lit8 v5, v0, 0x1

    iget-object v6, v1, Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;->targets:[Landroid/os/Handler;

    invoke-static {v2, v5, v6, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 147
    add-int/lit8 v2, v0, 0x1

    iget-object v1, v1, Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;->targetWhats:[I

    invoke-static {v3, v2, v1, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 154
    :cond_4
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 134
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public getRegistrationRepresentations()Ljava/util/List;
    .locals 4

    .prologue
    .line 181
    monitor-enter p0

    .line 182
    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/chrome/third_party/Broadcaster;->mReg:Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;

    .line 183
    if-nez v2, :cond_0

    .line 184
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    monitor-exit p0

    .line 193
    :goto_0
    return-object v0

    .line 186
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    move-object v1, v2

    .line 190
    :cond_1
    new-instance v3, Lcom/google/android/apps/chrome/third_party/Broadcaster$RegistrationRepresentation;

    invoke-direct {v3, p0, v1}, Lcom/google/android/apps/chrome/third_party/Broadcaster$RegistrationRepresentation;-><init>(Lcom/google/android/apps/chrome/third_party/Broadcaster;Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;)V

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 191
    iget-object v1, v1, Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;->next:Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;

    .line 192
    if-ne v1, v2, :cond_1

    .line 193
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 194
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public request(ILandroid/os/Handler;I)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 48
    monitor-enter p0

    .line 49
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/Broadcaster;->mReg:Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;

    if-nez v0, :cond_0

    .line 51
    new-instance v0, Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;-><init>(Lcom/google/android/apps/chrome/third_party/Broadcaster;Lcom/google/android/apps/chrome/third_party/Broadcaster$1;)V

    .line 52
    iput p1, v0, Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;->senderWhat:I

    .line 53
    const/4 v1, 0x1

    new-array v1, v1, [Landroid/os/Handler;

    iput-object v1, v0, Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;->targets:[Landroid/os/Handler;

    .line 54
    const/4 v1, 0x1

    new-array v1, v1, [I

    iput-object v1, v0, Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;->targetWhats:[I

    .line 55
    iget-object v1, v0, Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;->targets:[Landroid/os/Handler;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    .line 56
    iget-object v1, v0, Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;->targetWhats:[I

    const/4 v2, 0x0

    aput p3, v1, v2

    .line 57
    iput-object v0, p0, Lcom/google/android/apps/chrome/third_party/Broadcaster;->mReg:Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;

    .line 58
    iput-object v0, v0, Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;->next:Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;

    .line 59
    iput-object v0, v0, Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;->prev:Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;

    .line 107
    :goto_0
    monitor-exit p0

    :goto_1
    return-void

    .line 62
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/Broadcaster;->mReg:Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;

    move-object v1, v0

    .line 65
    :cond_1
    iget v3, v1, Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;->senderWhat:I

    if-ge v3, p1, :cond_2

    .line 66
    iget-object v1, v1, Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;->next:Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;

    .line 69
    if-ne v1, v0, :cond_1

    .line 71
    :cond_2
    iget v0, v1, Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;->senderWhat:I

    if-eq v0, p1, :cond_4

    .line 74
    new-instance v0, Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;

    const/4 v3, 0x0

    invoke-direct {v0, p0, v3}, Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;-><init>(Lcom/google/android/apps/chrome/third_party/Broadcaster;Lcom/google/android/apps/chrome/third_party/Broadcaster$1;)V

    .line 75
    iput p1, v0, Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;->senderWhat:I

    .line 76
    const/4 v3, 0x1

    new-array v3, v3, [Landroid/os/Handler;

    iput-object v3, v0, Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;->targets:[Landroid/os/Handler;

    .line 77
    const/4 v3, 0x1

    new-array v3, v3, [I

    iput-object v3, v0, Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;->targetWhats:[I

    .line 78
    iput-object v1, v0, Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;->next:Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;

    .line 79
    iget-object v3, v1, Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;->prev:Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;

    iput-object v3, v0, Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;->prev:Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;

    .line 80
    iget-object v3, v1, Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;->prev:Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;

    iput-object v0, v3, Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;->next:Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;

    .line 81
    iput-object v0, v1, Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;->prev:Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;

    .line 83
    iget-object v3, p0, Lcom/google/android/apps/chrome/third_party/Broadcaster;->mReg:Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;

    if-ne v1, v3, :cond_3

    iget v1, v1, Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;->senderWhat:I

    iget v3, v0, Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;->senderWhat:I

    if-le v1, v3, :cond_3

    .line 84
    iput-object v0, p0, Lcom/google/android/apps/chrome/third_party/Broadcaster;->mReg:Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;

    :cond_3
    move v1, v2

    .line 104
    :goto_2
    iget-object v2, v0, Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;->targets:[Landroid/os/Handler;

    aput-object p2, v2, v1

    .line 105
    iget-object v0, v0, Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;->targetWhats:[I

    aput p3, v0, v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 107
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 90
    :cond_4
    :try_start_1
    iget-object v0, v1, Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;->targets:[Landroid/os/Handler;

    array-length v0, v0

    .line 91
    iget-object v3, v1, Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;->targets:[Landroid/os/Handler;

    .line 92
    iget-object v4, v1, Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;->targetWhats:[I

    .line 94
    :goto_3
    if-ge v2, v0, :cond_6

    .line 95
    aget-object v5, v3, v2

    if-ne v5, p2, :cond_5

    aget v5, v4, v2

    if-ne v5, p3, :cond_5

    .line 96
    monitor-exit p0

    goto :goto_1

    .line 94
    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 99
    :cond_6
    add-int/lit8 v2, v0, 0x1

    new-array v2, v2, [Landroid/os/Handler;

    iput-object v2, v1, Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;->targets:[Landroid/os/Handler;

    .line 100
    const/4 v2, 0x0

    iget-object v5, v1, Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;->targets:[Landroid/os/Handler;

    const/4 v6, 0x0

    invoke-static {v3, v2, v5, v6, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 101
    add-int/lit8 v2, v0, 0x1

    new-array v2, v2, [I

    iput-object v2, v1, Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;->targetWhats:[I

    .line 102
    const/4 v2, 0x0

    iget-object v3, v1, Lcom/google/android/apps/chrome/third_party/Broadcaster$Registration;->targetWhats:[I

    const/4 v5, 0x0

    invoke-static {v4, v2, v3, v5, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v7, v1

    move v1, v0

    move-object v0, v7

    goto :goto_2
.end method
