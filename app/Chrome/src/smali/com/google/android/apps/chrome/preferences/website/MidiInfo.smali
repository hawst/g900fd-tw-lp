.class public Lcom/google/android/apps/chrome/preferences/website/MidiInfo;
.super Lcom/google/android/apps/chrome/preferences/website/PermissionInfo;
.source "MidiInfo.java"


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/preferences/website/PermissionInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 13
    return-void
.end method


# virtual methods
.method protected getNativePreferenceValue(Ljava/lang/String;Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 16
    invoke-static {p1, p2}, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferenceBridge;->nativeGetMidiSettingForOrigin(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method protected setNativePreferenceValue(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 22
    invoke-static {p1, p2, p3}, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferenceBridge;->nativeSetMidiSettingForOrigin(Ljava/lang/String;Ljava/lang/String;I)V

    .line 24
    return-void
.end method
