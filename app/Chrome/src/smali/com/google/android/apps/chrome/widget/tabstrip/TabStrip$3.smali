.class Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$3;
.super Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;
.source "TabStrip.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1303
    const-class v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$3;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)V
    .locals 0

    .prologue
    .line 1303
    iput-object p1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$3;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1310
    iget v2, p1, Landroid/os/Message;->what:I

    sparse-switch v2, :sswitch_data_0

    .line 1361
    sget-boolean v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$3;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    const-string/jumbo v1, "Unexpected Chrome Notification Received"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 1313
    :sswitch_0
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    const-string/jumbo v3, "tabId"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 1314
    iget-object v3, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$3;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    # getter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;
    invoke-static {v3}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$1200(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v3

    invoke-static {v3, v2}, Lorg/chromium/chrome/browser/tabmodel/TabModelUtils;->getTabIndexById(Lorg/chromium/chrome/browser/tabmodel/TabList;I)I

    move-result v3

    iget-object v4, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$3;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    # getter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mTabModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;
    invoke-static {v4}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$1200(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v4

    invoke-interface {v4}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->index()I

    move-result v4

    if-ne v3, v4, :cond_1

    .line 1316
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$3;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    # invokes: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->tabCreated(IZ)V
    invoke-static {v1, v2, v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$2100(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;IZ)V

    .line 1364
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v1

    .line 1314
    goto :goto_0

    .line 1319
    :sswitch_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$3;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string/jumbo v2, "tabId"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    # invokes: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->tabClosed(I)V
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$2200(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;I)V

    goto :goto_1

    .line 1322
    :sswitch_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$3;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    const-string/jumbo v3, "tabId"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    # invokes: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->tabSelected(IZ)V
    invoke-static {v0, v2, v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$1400(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;IZ)V

    goto :goto_1

    .line 1325
    :sswitch_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$3;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string/jumbo v2, "tabId"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    # invokes: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->tabLoadStarted(I)V
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$2300(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;I)V

    goto :goto_1

    .line 1328
    :sswitch_4
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$3;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string/jumbo v2, "tabId"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    # invokes: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->tabLoadFinished(I)V
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$2400(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;I)V

    goto :goto_1

    .line 1331
    :sswitch_5
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$3;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string/jumbo v2, "tabId"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    const-string/jumbo v3, "title"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    # invokes: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->tabTitleChanged(ILjava/lang/String;)V
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$2500(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;ILjava/lang/String;)V

    goto :goto_1

    .line 1334
    :sswitch_6
    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$3;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    # invokes: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->setBackgroundTabFading(Z)V
    invoke-static {v1, v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$2600(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;Z)V

    goto :goto_1

    .line 1337
    :sswitch_7
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$3;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    # invokes: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->setBackgroundTabFading(Z)V
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$2600(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;Z)V

    goto :goto_1

    .line 1340
    :sswitch_8
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$3;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    const-string/jumbo v3, "tabId"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v3

    const-string/jumbo v4, "fromPosition"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v4

    const-string/jumbo v5, "toPosition"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    # invokes: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->tabMoved(IIIZ)V
    invoke-static {v0, v2, v3, v4, v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$2700(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;IIIZ)V

    goto/16 :goto_1

    .line 1344
    :sswitch_9
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$3;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string/jumbo v2, "tabId"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    # invokes: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->tabFaviconChanged(I)V
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$2800(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;I)V

    goto/16 :goto_1

    .line 1349
    :sswitch_a
    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$3;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v3

    const-string/jumbo v4, "tabId"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    # invokes: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->tabSelected(IZ)V
    invoke-static {v2, v3, v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$1400(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;IZ)V

    .line 1350
    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$3;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$3;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    iget-object v3, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$3;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v4

    const-string/jumbo v5, "tabId"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    # invokes: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->findTabViewById(I)Lcom/google/android/apps/chrome/widget/tabstrip/TabView;
    invoke-static {v3, v4}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$2900(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;I)Lcom/google/android/apps/chrome/widget/tabstrip/TabView;

    move-result-object v3

    # invokes: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->calculateOffsetToMakeTabVisible(Lcom/google/android/apps/chrome/widget/tabstrip/TabView;ZZZ)F
    invoke-static {v2, v3, v0, v0, v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$3000(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;Lcom/google/android/apps/chrome/widget/tabstrip/TabView;ZZZ)F

    move-result v0

    float-to-int v0, v0

    # invokes: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->startFastExpand(I)V
    invoke-static {v1, v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$3100(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;I)V

    goto/16 :goto_1

    .line 1354
    :sswitch_b
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$3;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string/jumbo v2, "tabId"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    # invokes: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->tabPageLoadStarted(I)V
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$3200(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;I)V

    goto/16 :goto_1

    .line 1358
    :sswitch_c
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$3;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string/jumbo v2, "tabId"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    # invokes: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->tabPageLoadFinished(I)V
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$3300(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;I)V

    goto/16 :goto_1

    .line 1310
    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0x3 -> :sswitch_2
        0x4 -> :sswitch_8
        0x5 -> :sswitch_1
        0x8 -> :sswitch_b
        0x9 -> :sswitch_c
        0xa -> :sswitch_5
        0x11 -> :sswitch_6
        0x12 -> :sswitch_7
        0x14 -> :sswitch_9
        0x15 -> :sswitch_a
        0x1a -> :sswitch_3
        0x1b -> :sswitch_4
        0x1c -> :sswitch_c
        0x45 -> :sswitch_0
    .end sparse-switch
.end method
