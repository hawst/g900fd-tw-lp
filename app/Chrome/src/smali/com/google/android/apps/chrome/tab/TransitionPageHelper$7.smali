.class Lcom/google/android/apps/chrome/tab/TransitionPageHelper$7;
.super Lorg/chromium/content/browser/WebContentsObserverAndroid;
.source "TransitionPageHelper.java"


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/tab/TransitionPageHelper;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/tab/TransitionPageHelper;Lorg/chromium/content_public/browser/WebContents;)V
    .locals 0

    .prologue
    .line 512
    iput-object p1, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper$7;->this$0:Lcom/google/android/apps/chrome/tab/TransitionPageHelper;

    invoke-direct {p0, p2}, Lorg/chromium/content/browser/WebContentsObserverAndroid;-><init>(Lorg/chromium/content_public/browser/WebContents;)V

    return-void
.end method

.method static synthetic access$1600(Lcom/google/android/apps/chrome/tab/TransitionPageHelper$7;)V
    .locals 0

    .prologue
    .line 512
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tab/TransitionPageHelper$7;->signalDestinationMaybeReady()V

    return-void
.end method

.method private signalDestinationMaybeReady()V
    .locals 2

    .prologue
    .line 514
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper$7;->this$0:Lcom/google/android/apps/chrome/tab/TransitionPageHelper;

    # getter for: Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mDestinationDOMContentLoaded:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->access$1200(Lcom/google/android/apps/chrome/tab/TransitionPageHelper;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper$7;->this$0:Lcom/google/android/apps/chrome/tab/TransitionPageHelper;

    # getter for: Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mDestinationPainted:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->access$1300(Lcom/google/android/apps/chrome/tab/TransitionPageHelper;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper$7;->this$0:Lcom/google/android/apps/chrome/tab/TransitionPageHelper;

    # getter for: Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mDestinationReady:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->access$1400(Lcom/google/android/apps/chrome/tab/TransitionPageHelper;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 515
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper$7;->this$0:Lcom/google/android/apps/chrome/tab/TransitionPageHelper;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mDestinationReady:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->access$1402(Lcom/google/android/apps/chrome/tab/TransitionPageHelper;Z)Z

    .line 516
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper$7;->this$0:Lcom/google/android/apps/chrome/tab/TransitionPageHelper;

    # invokes: Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->updateState()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->access$200(Lcom/google/android/apps/chrome/tab/TransitionPageHelper;)V

    .line 518
    :cond_0
    return-void
.end method


# virtual methods
.method public didFailLoad(ZZILjava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 547
    if-eqz p2, :cond_0

    .line 548
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper$7;->this$0:Lcom/google/android/apps/chrome/tab/TransitionPageHelper;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mDestinationReady:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->access$1402(Lcom/google/android/apps/chrome/tab/TransitionPageHelper;Z)Z

    .line 549
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper$7;->this$0:Lcom/google/android/apps/chrome/tab/TransitionPageHelper;

    # invokes: Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->updateState()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->access$200(Lcom/google/android/apps/chrome/tab/TransitionPageHelper;)V

    .line 551
    :cond_0
    return-void
.end method

.method public didFirstVisuallyNonEmptyPaint()V
    .locals 2

    .prologue
    .line 522
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper$7;->this$0:Lcom/google/android/apps/chrome/tab/TransitionPageHelper;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mDestinationPainted:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->access$1302(Lcom/google/android/apps/chrome/tab/TransitionPageHelper;Z)Z

    .line 523
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tab/TransitionPageHelper$7;->signalDestinationMaybeReady()V

    .line 524
    return-void
.end method

.method public documentLoadedInFrame(J)V
    .locals 5

    .prologue
    .line 528
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper$7;->this$0:Lcom/google/android/apps/chrome/tab/TransitionPageHelper;

    # getter for: Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mDestinationMainFrameID:J
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->access$1500(Lcom/google/android/apps/chrome/tab/TransitionPageHelper;)J

    move-result-wide v0

    cmp-long v0, p1, v0

    if-nez v0, :cond_0

    .line 529
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper$7;->this$0:Lcom/google/android/apps/chrome/tab/TransitionPageHelper;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mDestinationDOMContentLoaded:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->access$1202(Lcom/google/android/apps/chrome/tab/TransitionPageHelper;Z)Z

    .line 530
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tab/TransitionPageHelper$7;->signalDestinationMaybeReady()V

    .line 531
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper$7;->this$0:Lcom/google/android/apps/chrome/tab/TransitionPageHelper;

    # getter for: Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->access$1700(Lcom/google/android/apps/chrome/tab/TransitionPageHelper;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/chrome/tab/TransitionPageHelper$7$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/tab/TransitionPageHelper$7$1;-><init>(Lcom/google/android/apps/chrome/tab/TransitionPageHelper$7;)V

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 540
    :cond_0
    return-void
.end method
