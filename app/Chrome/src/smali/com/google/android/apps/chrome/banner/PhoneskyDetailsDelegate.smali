.class public Lcom/google/android/apps/chrome/banner/PhoneskyDetailsDelegate;
.super Lorg/chromium/chrome/browser/banners/AppDetailsDelegate;
.source "PhoneskyDetailsDelegate.java"

# interfaces
.implements Landroid/content/ServiceConnection;
.implements Lorg/chromium/base/ApplicationStatus$ApplicationStateListener;


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final mApplicationContext:Landroid/content/Context;

.field private mDetailsService:Lcom/a/b/a/a;

.field private mIsServiceBinding:Z

.field private final mQueuedRequests:Ljava/util/LinkedList;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    const-class v0, Lcom/google/android/apps/chrome/banner/PhoneskyDetailsDelegate;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/banner/PhoneskyDetailsDelegate;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 61
    invoke-direct {p0}, Lorg/chromium/chrome/browser/banners/AppDetailsDelegate;-><init>()V

    .line 62
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/banner/PhoneskyDetailsDelegate;->mApplicationContext:Landroid/content/Context;

    .line 63
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/banner/PhoneskyDetailsDelegate;->mQueuedRequests:Ljava/util/LinkedList;

    .line 64
    invoke-static {p0}, Lorg/chromium/base/ApplicationStatus;->registerApplicationStateListener(Lorg/chromium/base/ApplicationStatus$ApplicationStateListener;)V

    .line 65
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/banner/PhoneskyDetailsDelegate;)Lcom/a/b/a/a;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/apps/chrome/banner/PhoneskyDetailsDelegate;->mDetailsService:Lcom/a/b/a/a;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/banner/PhoneskyDetailsDelegate;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;I)Lorg/chromium/chrome/browser/banners/AppData;
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/chrome/banner/PhoneskyDetailsDelegate;->parseAndVerifyAppBundle(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;I)Lorg/chromium/chrome/browser/banners/AppData;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/banner/PhoneskyDetailsDelegate;)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/google/android/apps/chrome/banner/PhoneskyDetailsDelegate;->performNextRequest()V

    return-void
.end method

.method private bindService()V
    .locals 3

    .prologue
    .line 201
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/banner/PhoneskyDetailsDelegate;->mIsServiceBinding:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/banner/PhoneskyDetailsDelegate;->mDetailsService:Lcom/a/b/a/a;

    if-eqz v0, :cond_1

    .line 216
    :cond_0
    :goto_0
    return-void

    .line 203
    :cond_1
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "com.android.vending.details.IDetailsService.BIND"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 204
    const-string/jumbo v1, "com.android.vending"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 206
    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/banner/PhoneskyDetailsDelegate;->mApplicationContext:Landroid/content/Context;

    const/4 v2, 0x1

    invoke-virtual {v1, v0, p0, v2}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/banner/PhoneskyDetailsDelegate;->mIsServiceBinding:Z
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 212
    :goto_1
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/banner/PhoneskyDetailsDelegate;->mIsServiceBinding:Z

    if-nez v0, :cond_0

    .line 213
    const-string/jumbo v0, "PhoneskyDetailsDelegate"

    const-string/jumbo v1, "Failed to bind to service. Clearing requests."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 214
    invoke-direct {p0}, Lcom/google/android/apps/chrome/banner/PhoneskyDetailsDelegate;->reset()V

    goto :goto_0

    .line 208
    :catch_0
    move-exception v0

    .line 209
    const-string/jumbo v1, "PhoneskyDetailsDelegate"

    const-string/jumbo v2, "Failed to bind to service: "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method private createRetrievalTask(Lorg/chromium/chrome/browser/banners/AppDetailsDelegate$Observer;Ljava/lang/String;Ljava/lang/String;I)Landroid/os/AsyncTask;
    .locals 6

    .prologue
    .line 83
    new-instance v0, Lcom/google/android/apps/chrome/banner/PhoneskyDetailsDelegate$1;

    move-object v1, p0

    move-object v2, p3

    move-object v3, p2

    move v4, p4

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/chrome/banner/PhoneskyDetailsDelegate$1;-><init>(Lcom/google/android/apps/chrome/banner/PhoneskyDetailsDelegate;Ljava/lang/String;Ljava/lang/String;ILorg/chromium/chrome/browser/banners/AppDetailsDelegate$Observer;)V

    return-object v0
.end method

.method private getImageUrl(Landroid/os/Bundle;I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 156
    const-string/jumbo v0, "fife_url"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 158
    if-eqz v0, :cond_0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/banner/PhoneskyDetailsDelegate;->isValidFifeUrl(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 161
    :cond_0
    const-string/jumbo v0, "image_url"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 165
    :goto_0
    return-object v0

    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "=s"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private isValidFifeUrl(Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 176
    :try_start_0
    new-instance v1, Ljava/net/URI;

    invoke-direct {v1, p1}, Ljava/net/URI;-><init>(Ljava/lang/String;)V

    .line 177
    invoke-virtual {v1}, Ljava/net/URI;->getPath()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Ljava/net/URI;->getPath()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "="

    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    .line 179
    :cond_0
    :goto_0
    return v0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method private parseAndVerifyAppBundle(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;I)Lorg/chromium/chrome/browser/banners/AppData;
    .locals 10

    .prologue
    const/4 v0, 0x0

    .line 123
    if-nez p3, :cond_1

    .line 149
    :cond_0
    :goto_0
    return-object v0

    .line 126
    :cond_1
    const-string/jumbo v1, "title"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 127
    const-string/jumbo v2, "star_rating"

    const/high16 v3, -0x40800000    # -1.0f

    invoke-virtual {p3, v2, v3}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;F)F

    move-result v3

    .line 128
    const-string/jumbo v2, "purchase_button_text"

    invoke-virtual {p3, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 129
    const-string/jumbo v2, "details_intent"

    invoke-virtual {p3, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v5

    check-cast v5, Landroid/app/PendingIntent;

    .line 130
    const-string/jumbo v2, "purchase_intent"

    invoke-virtual {p3, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v6

    check-cast v6, Landroid/app/PendingIntent;

    .line 131
    invoke-direct {p0, p3, p4}, Lcom/google/android/apps/chrome/banner/PhoneskyDetailsDelegate;->getImageUrl(Landroid/os/Bundle;I)Ljava/lang/String;

    move-result-object v2

    .line 134
    if-eqz v1, :cond_0

    const/4 v7, 0x0

    cmpg-float v7, v3, v7

    if-ltz v7, :cond_0

    if-eqz v2, :cond_0

    if-eqz v4, :cond_0

    if-eqz v5, :cond_0

    if-eqz v6, :cond_0

    .line 139
    invoke-static {v5}, Lorg/chromium/base/ApiCompatibilityUtils;->getCreatorPackage(Landroid/app/PendingIntent;)Ljava/lang/String;

    move-result-object v7

    .line 140
    invoke-static {v6}, Lorg/chromium/base/ApiCompatibilityUtils;->getCreatorPackage(Landroid/app/PendingIntent;)Ljava/lang/String;

    move-result-object v8

    .line 141
    const-string/jumbo v9, "com.android.vending"

    invoke-static {v7, v9}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_0

    const-string/jumbo v7, "com.android.vending"

    invoke-static {v8, v7}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 147
    new-instance v0, Lorg/chromium/chrome/browser/banners/AppData;

    invoke-direct {v0, p1, p2}, Lorg/chromium/chrome/browser/banners/AppData;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    invoke-virtual/range {v0 .. v6}, Lorg/chromium/chrome/browser/banners/AppData;->setPackageInfo(Ljava/lang/String;Ljava/lang/String;FLjava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)V

    goto :goto_0
.end method

.method private performNextRequest()V
    .locals 2

    .prologue
    .line 112
    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    .line 114
    iget-object v0, p0, Lcom/google/android/apps/chrome/banner/PhoneskyDetailsDelegate;->mQueuedRequests:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 115
    invoke-direct {p0}, Lcom/google/android/apps/chrome/banner/PhoneskyDetailsDelegate;->reset()V

    .line 119
    :goto_0
    return-void

    .line 117
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/banner/PhoneskyDetailsDelegate;->mQueuedRequests:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/AsyncTask;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method private reset()V
    .locals 1

    .prologue
    .line 236
    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    .line 238
    iget-object v0, p0, Lcom/google/android/apps/chrome/banner/PhoneskyDetailsDelegate;->mDetailsService:Lcom/a/b/a/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/banner/PhoneskyDetailsDelegate;->mApplicationContext:Landroid/content/Context;

    invoke-virtual {v0, p0}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 240
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/banner/PhoneskyDetailsDelegate;->mQueuedRequests:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    .line 241
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/banner/PhoneskyDetailsDelegate;->mDetailsService:Lcom/a/b/a/a;

    .line 242
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/banner/PhoneskyDetailsDelegate;->mIsServiceBinding:Z

    .line 243
    return-void
.end method


# virtual methods
.method public destroy()V
    .locals 0

    .prologue
    .line 185
    invoke-static {p0}, Lorg/chromium/base/ApplicationStatus;->unregisterApplicationStateListener(Lorg/chromium/base/ApplicationStatus$ApplicationStateListener;)V

    .line 186
    invoke-direct {p0}, Lcom/google/android/apps/chrome/banner/PhoneskyDetailsDelegate;->reset()V

    .line 187
    return-void
.end method

.method public getAppDetailsAsynchronously(Lorg/chromium/chrome/browser/banners/AppDetailsDelegate$Observer;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 2

    .prologue
    .line 70
    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    .line 72
    if-eqz p1, :cond_0

    if-nez p4, :cond_1

    .line 78
    :cond_0
    :goto_0
    return-void

    .line 74
    :cond_1
    invoke-static {}, Lorg/chromium/base/ApplicationStatus;->hasVisibleActivities()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 75
    iget-object v0, p0, Lcom/google/android/apps/chrome/banner/PhoneskyDetailsDelegate;->mQueuedRequests:Ljava/util/LinkedList;

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/chrome/banner/PhoneskyDetailsDelegate;->createRetrievalTask(Lorg/chromium/chrome/browser/banners/AppDetailsDelegate$Observer;Ljava/lang/String;Ljava/lang/String;I)Landroid/os/AsyncTask;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 76
    invoke-direct {p0}, Lcom/google/android/apps/chrome/banner/PhoneskyDetailsDelegate;->bindService()V

    goto :goto_0
.end method

.method public onApplicationStateChange(I)V
    .locals 1

    .prologue
    .line 194
    invoke-static {}, Lorg/chromium/base/ApplicationStatus;->hasVisibleActivities()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/banner/PhoneskyDetailsDelegate;->reset()V

    .line 195
    :cond_0
    return-void
.end method

.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 1

    .prologue
    .line 220
    sget-boolean v0, Lcom/google/android/apps/chrome/banner/PhoneskyDetailsDelegate;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/banner/PhoneskyDetailsDelegate;->mIsServiceBinding:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 221
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/banner/PhoneskyDetailsDelegate;->mIsServiceBinding:Z

    .line 222
    invoke-static {p2}, Lcom/a/b/a/b;->a(Landroid/os/IBinder;)Lcom/a/b/a/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/banner/PhoneskyDetailsDelegate;->mDetailsService:Lcom/a/b/a/a;

    .line 225
    invoke-direct {p0}, Lcom/google/android/apps/chrome/banner/PhoneskyDetailsDelegate;->performNextRequest()V

    .line 226
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 1

    .prologue
    .line 231
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/banner/PhoneskyDetailsDelegate;->mDetailsService:Lcom/a/b/a/a;

    .line 232
    invoke-direct {p0}, Lcom/google/android/apps/chrome/banner/PhoneskyDetailsDelegate;->reset()V

    .line 233
    return-void
.end method
