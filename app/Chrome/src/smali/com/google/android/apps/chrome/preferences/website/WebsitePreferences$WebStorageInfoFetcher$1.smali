.class Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$WebStorageInfoFetcher$1;
.super Ljava/lang/Object;
.source "WebsitePreferences.java"

# interfaces
.implements Lcom/google/android/apps/chrome/preferences/website/WebsitePreferenceBridge$StorageInfoReadyCallback;


# instance fields
.field final synthetic this$1:Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$WebStorageInfoFetcher;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$WebStorageInfoFetcher;)V
    .locals 0

    .prologue
    .line 150
    iput-object p1, p0, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$WebStorageInfoFetcher$1;->this$1:Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$WebStorageInfoFetcher;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onStorageInfoReady(Ljava/util/ArrayList;)V
    .locals 4

    .prologue
    .line 154
    .line 155
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/preferences/website/StorageInfo;

    .line 156
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/website/StorageInfo;->getHost()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/chrome/preferences/website/WebsiteAddress;->create(Ljava/lang/String;)Lcom/google/android/apps/chrome/preferences/website/WebsiteAddress;

    move-result-object v1

    .line 157
    if-eqz v1, :cond_0

    .line 158
    iget-object v3, p0, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$WebStorageInfoFetcher$1;->this$1:Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$WebStorageInfoFetcher;

    iget-object v3, v3, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$WebStorageInfoFetcher;->this$0:Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences;

    # invokes: Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences;->findOrCreateSitesByHost(Lcom/google/android/apps/chrome/preferences/website/WebsiteAddress;)Ljava/util/Set;
    invoke-static {v3, v1}, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences;->access$1000(Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences;Lcom/google/android/apps/chrome/preferences/website/WebsiteAddress;)Ljava/util/Set;

    move-result-object v1

    .line 159
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/chrome/preferences/website/Website;

    .line 160
    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/preferences/website/Website;->addStorageInfo(Lcom/google/android/apps/chrome/preferences/website/StorageInfo;)V

    goto :goto_0

    .line 163
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$WebStorageInfoFetcher$1;->this$1:Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$WebStorageInfoFetcher;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferences$WebStorageInfoFetcher;->next()V

    .line 164
    return-void
.end method
