.class Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$SaveListTask;
.super Landroid/os/AsyncTask;
.source "TabPersistentStore.java"


# instance fields
.field mListData:[B

.field mStateSaved:Z

.field final synthetic this$0:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;)V
    .locals 1

    .prologue
    .line 661
    iput-object p1, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$SaveListTask;->this$0:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 663
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$SaveListTask;->mStateSaved:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$1;)V
    .locals 0

    .prologue
    .line 661
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$SaveListTask;-><init>(Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 661
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$SaveListTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 677
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$SaveListTask;->mListData:[B

    if-nez v0, :cond_0

    .line 681
    :goto_0
    return-object v2

    .line 678
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$SaveListTask;->mListData:[B

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$SaveListTask;->this$0:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$SaveListTask;->mListData:[B

    # invokes: Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->saveListToFile([B)V
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->access$800(Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;[B)V

    .line 679
    :cond_1
    iput-object v2, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$SaveListTask;->mListData:[B

    .line 680
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$SaveListTask;->mStateSaved:Z

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 661
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$SaveListTask;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 2

    .prologue
    .line 686
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$SaveListTask;->this$0:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;

    # getter for: Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mDestroyed:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->access$200(Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$SaveListTask;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 688
    :cond_0
    :goto_0
    return-void

    .line 687
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$SaveListTask;->this$0:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mSaveListTask:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$SaveListTask;
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->access$902(Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$SaveListTask;)Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$SaveListTask;

    goto :goto_0
.end method

.method protected onPreExecute()V
    .locals 1

    .prologue
    .line 667
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$SaveListTask;->this$0:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;

    # getter for: Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mDestroyed:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->access$200(Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$SaveListTask;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 673
    :cond_0
    :goto_0
    return-void

    .line 669
    :cond_1
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$SaveListTask;->this$0:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;

    # getter for: Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;
    invoke-static {v0}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->access$700(Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;)Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->serializeTabModelSelector(Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;)[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$SaveListTask;->mListData:[B
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 671
    :catch_0
    move-exception v0

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$SaveListTask;->mListData:[B

    goto :goto_0
.end method
