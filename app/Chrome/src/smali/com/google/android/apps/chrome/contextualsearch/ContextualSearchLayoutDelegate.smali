.class public interface abstract Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchLayoutDelegate;
.super Ljava/lang/Object;
.source "ContextualSearchLayoutDelegate.java"


# virtual methods
.method public abstract animateAfterFirstRunSuccess()V
.end method

.method public abstract closePanel(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;)V
.end method

.method public abstract getContextualSearchState()Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;
.end method

.method public abstract hide()V
.end method

.method public abstract maximizePanelThenPromoteToTab(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;)V
.end method

.method public abstract maximizePanelThenPromoteToTab(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;J)V
.end method

.method public abstract refresh()V
.end method

.method public abstract setFirstRunFlowContentHeight(F)V
.end method

.method public abstract updateBasePageSelectionStartYPx(F)V
.end method
