.class Lcom/google/android/apps/chrome/tab/TransitionPageHelper$6;
.super Lorg/chromium/content/browser/WebContentsObserverAndroid;
.source "TransitionPageHelper.java"


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/tab/TransitionPageHelper;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/tab/TransitionPageHelper;Lorg/chromium/content_public/browser/WebContents;)V
    .locals 0

    .prologue
    .line 488
    iput-object p1, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper$6;->this$0:Lcom/google/android/apps/chrome/tab/TransitionPageHelper;

    invoke-direct {p0, p2}, Lorg/chromium/content/browser/WebContentsObserverAndroid;-><init>(Lorg/chromium/content_public/browser/WebContents;)V

    return-void
.end method


# virtual methods
.method public didFinishLoad(JLjava/lang/String;Z)V
    .locals 2

    .prologue
    .line 499
    if-eqz p4, :cond_0

    .line 500
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper$6;->this$0:Lcom/google/android/apps/chrome/tab/TransitionPageHelper;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mTransitionLoaded:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->access$1102(Lcom/google/android/apps/chrome/tab/TransitionPageHelper;Z)Z

    .line 501
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper$6;->this$0:Lcom/google/android/apps/chrome/tab/TransitionPageHelper;

    # invokes: Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->updateState()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->access$200(Lcom/google/android/apps/chrome/tab/TransitionPageHelper;)V

    .line 503
    :cond_0
    return-void
.end method

.method public didFirstVisuallyNonEmptyPaint()V
    .locals 2

    .prologue
    .line 491
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper$6;->this$0:Lcom/google/android/apps/chrome/tab/TransitionPageHelper;

    # getter for: Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mTransitionPainted:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->access$1000(Lcom/google/android/apps/chrome/tab/TransitionPageHelper;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 492
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper$6;->this$0:Lcom/google/android/apps/chrome/tab/TransitionPageHelper;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mTransitionPainted:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->access$1002(Lcom/google/android/apps/chrome/tab/TransitionPageHelper;Z)Z

    .line 493
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper$6;->this$0:Lcom/google/android/apps/chrome/tab/TransitionPageHelper;

    # invokes: Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->updateState()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->access$200(Lcom/google/android/apps/chrome/tab/TransitionPageHelper;)V

    .line 495
    :cond_0
    return-void
.end method
