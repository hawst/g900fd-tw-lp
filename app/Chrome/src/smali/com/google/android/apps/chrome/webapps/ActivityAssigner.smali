.class public Lcom/google/android/apps/chrome/webapps/ActivityAssigner;
.super Ljava/lang/Object;
.source "ActivityAssigner.java"


# static fields
.field static final INVALID_ACTIVITY_INDEX:I = -0x1

.field static final MAX_WEBAPP_ACTIVITIES_EVER:I = 0x64

.field static final NUM_WEBAPP_ACTIVITIES:I = 0xa

.field static final PREF_ACTIVITY_INDEX:Ljava/lang/String; = "ActivityAssigner.activityIndex"

.field static final PREF_NUM_SAVED_ENTRIES:Ljava/lang/String; = "ActivityAssigner.numSavedEntries"

.field static final PREF_PACKAGE:Ljava/lang/String; = "com.google.android.apps.chrome.webapps"

.field static final PREF_WEBAPP_ID:Ljava/lang/String; = "ActivityAssigner.webappId"

.field private static sInstance:Lcom/google/android/apps/chrome/webapps/ActivityAssigner;


# instance fields
.field private final mActivityList:Ljava/util/List;

.field private final mContext:Landroid/content/Context;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 94
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 95
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/webapps/ActivityAssigner;->mContext:Landroid/content/Context;

    .line 96
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/webapps/ActivityAssigner;->mActivityList:Ljava/util/List;

    .line 98
    invoke-direct {p0}, Lcom/google/android/apps/chrome/webapps/ActivityAssigner;->restoreActivityList()V

    .line 99
    return-void
.end method

.method private findActivityElement(I)I
    .locals 2

    .prologue
    .line 170
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/ActivityAssigner;->mActivityList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 171
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/ActivityAssigner;->mActivityList:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/webapps/ActivityAssigner$ActivityEntry;

    iget v0, v0, Lcom/google/android/apps/chrome/webapps/ActivityAssigner$ActivityEntry;->mActivityIndex:I

    if-ne v0, p1, :cond_0

    .line 175
    :goto_1
    return v1

    .line 170
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 175
    :cond_1
    const/4 v1, -0x1

    goto :goto_1
.end method

.method public static instance(Landroid/content/Context;)Lcom/google/android/apps/chrome/webapps/ActivityAssigner;
    .locals 1

    .prologue
    .line 87
    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    .line 88
    sget-object v0, Lcom/google/android/apps/chrome/webapps/ActivityAssigner;->sInstance:Lcom/google/android/apps/chrome/webapps/ActivityAssigner;

    if-nez v0, :cond_0

    .line 89
    new-instance v0, Lcom/google/android/apps/chrome/webapps/ActivityAssigner;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/webapps/ActivityAssigner;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/apps/chrome/webapps/ActivityAssigner;->sInstance:Lcom/google/android/apps/chrome/webapps/ActivityAssigner;

    .line 91
    :cond_0
    sget-object v0, Lcom/google/android/apps/chrome/webapps/ActivityAssigner;->sInstance:Lcom/google/android/apps/chrome/webapps/ActivityAssigner;

    return-object v0
.end method

.method private restoreActivityList()V
    .locals 12

    .prologue
    const/4 v11, 0x0

    const/16 v10, 0xa

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 192
    .line 193
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/ActivityAssigner;->mActivityList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 198
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    move v0, v2

    .line 199
    :goto_0
    if-ge v0, v10, :cond_0

    .line 200
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v4, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 199
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 205
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/ActivityAssigner;->mContext:Landroid/content/Context;

    const-string/jumbo v3, "com.google.android.apps.chrome.webapps"

    invoke-virtual {v0, v3, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v5

    .line 207
    :try_start_0
    const-string/jumbo v0, "ActivityAssigner.numSavedEntries"

    const/4 v3, 0x0

    invoke-interface {v5, v0, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v6

    .line 208
    const/16 v0, 0x64

    if-gt v6, v0, :cond_2

    move v3, v2

    move v0, v2

    .line 209
    :goto_1
    if-ge v3, v6, :cond_3

    .line 210
    :try_start_1
    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "ActivityAssigner.activityIndex"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 211
    new-instance v8, Ljava/lang/StringBuilder;

    const-string/jumbo v9, "ActivityAssigner.webappId"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 213
    invoke-interface {v5, v7, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v7

    .line 214
    const/4 v9, 0x0

    invoke-interface {v5, v8, v9}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 215
    new-instance v9, Lcom/google/android/apps/chrome/webapps/ActivityAssigner$ActivityEntry;

    invoke-direct {v9, v7, v8}, Lcom/google/android/apps/chrome/webapps/ActivityAssigner$ActivityEntry;-><init>(ILjava/lang/String;)V

    .line 217
    iget v7, v9, Lcom/google/android/apps/chrome/webapps/ActivityAssigner$ActivityEntry;->mActivityIndex:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v4, v7}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 218
    iget-object v7, p0, Lcom/google/android/apps/chrome/webapps/ActivityAssigner;->mActivityList:Ljava/util/List;

    invoke-interface {v7, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/ClassCastException; {:try_start_1 .. :try_end_1} :catch_1

    .line 209
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_1
    move v0, v1

    .line 223
    goto :goto_2

    .line 229
    :catch_0
    move-exception v0

    move v0, v2

    :goto_3
    iget-object v3, p0, Lcom/google/android/apps/chrome/webapps/ActivityAssigner;->mActivityList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->clear()V

    .line 230
    invoke-interface {v4}, Ljava/util/Set;->clear()V

    .line 231
    :goto_4
    if-ge v2, v10, :cond_3

    .line 232
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v4, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 231
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    :cond_2
    move v0, v2

    .line 237
    :cond_3
    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_5
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 238
    new-instance v3, Lcom/google/android/apps/chrome/webapps/ActivityAssigner$ActivityEntry;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {v3, v0, v11}, Lcom/google/android/apps/chrome/webapps/ActivityAssigner$ActivityEntry;-><init>(ILjava/lang/String;)V

    .line 239
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/ActivityAssigner;->mActivityList:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v0, v1

    .line 241
    goto :goto_5

    .line 243
    :cond_4
    if-eqz v0, :cond_5

    .line 244
    invoke-direct {p0}, Lcom/google/android/apps/chrome/webapps/ActivityAssigner;->storeActivityList()V

    .line 246
    :cond_5
    return-void

    .line 229
    :catch_1
    move-exception v3

    goto :goto_3
.end method

.method private storeActivityList()V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 252
    iget-object v1, p0, Lcom/google/android/apps/chrome/webapps/ActivityAssigner;->mContext:Landroid/content/Context;

    const-string/jumbo v2, "com.google.android.apps.chrome.webapps"

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 253
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 254
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    .line 255
    const-string/jumbo v1, "ActivityAssigner.numSavedEntries"

    iget-object v3, p0, Lcom/google/android/apps/chrome/webapps/ActivityAssigner;->mActivityList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-interface {v2, v1, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move v1, v0

    .line 256
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/ActivityAssigner;->mActivityList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 257
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "ActivityAssigner.activityIndex"

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 258
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "ActivityAssigner.webappId"

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 259
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/ActivityAssigner;->mActivityList:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/webapps/ActivityAssigner$ActivityEntry;

    iget v0, v0, Lcom/google/android/apps/chrome/webapps/ActivityAssigner$ActivityEntry;->mActivityIndex:I

    invoke-interface {v2, v3, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 260
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/ActivityAssigner;->mActivityList:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/webapps/ActivityAssigner$ActivityEntry;

    iget-object v0, v0, Lcom/google/android/apps/chrome/webapps/ActivityAssigner$ActivityEntry;->mWebappId:Ljava/lang/String;

    invoke-interface {v2, v4, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 256
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 262
    :cond_0
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 263
    return-void
.end method


# virtual methods
.method assign(Ljava/lang/String;)I
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 110
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/webapps/ActivityAssigner;->checkIfAssigned(Ljava/lang/String;)I

    move-result v0

    .line 113
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 114
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/ActivityAssigner;->mActivityList:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/webapps/ActivityAssigner$ActivityEntry;

    iget v0, v0, Lcom/google/android/apps/chrome/webapps/ActivityAssigner$ActivityEntry;->mActivityIndex:I

    .line 115
    new-instance v1, Lcom/google/android/apps/chrome/webapps/ActivityAssigner$ActivityEntry;

    invoke-direct {v1, v0, p1}, Lcom/google/android/apps/chrome/webapps/ActivityAssigner$ActivityEntry;-><init>(ILjava/lang/String;)V

    .line 116
    iget-object v2, p0, Lcom/google/android/apps/chrome/webapps/ActivityAssigner;->mActivityList:Ljava/util/List;

    invoke-interface {v2, v3, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 119
    :cond_0
    invoke-virtual {p0, v0, p1}, Lcom/google/android/apps/chrome/webapps/ActivityAssigner;->markActivityUsed(ILjava/lang/String;)V

    .line 120
    return v0
.end method

.method checkIfAssigned(Ljava/lang/String;)I
    .locals 3

    .prologue
    const/4 v1, -0x1

    .line 129
    if-nez p1, :cond_0

    move v0, v1

    .line 139
    :goto_0
    return v0

    .line 134
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/ActivityAssigner;->mActivityList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v2, v0

    :goto_1
    if-ltz v2, :cond_2

    .line 135
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/ActivityAssigner;->mActivityList:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/webapps/ActivityAssigner$ActivityEntry;

    iget-object v0, v0, Lcom/google/android/apps/chrome/webapps/ActivityAssigner$ActivityEntry;->mWebappId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 136
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/ActivityAssigner;->mActivityList:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/webapps/ActivityAssigner$ActivityEntry;

    iget v0, v0, Lcom/google/android/apps/chrome/webapps/ActivityAssigner$ActivityEntry;->mActivityIndex:I

    goto :goto_0

    .line 134
    :cond_1
    add-int/lit8 v0, v2, -0x1

    move v2, v0

    goto :goto_1

    :cond_2
    move v0, v1

    .line 139
    goto :goto_0
.end method

.method getEntries()Ljava/util/List;
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/ActivityAssigner;->mActivityList:Ljava/util/List;

    return-object v0
.end method

.method markActivityUsed(ILjava/lang/String;)V
    .locals 3

    .prologue
    .line 150
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/webapps/ActivityAssigner;->findActivityElement(I)I

    move-result v0

    .line 152
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 153
    const-string/jumbo v0, "ActivityAssigner"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Failed to find WebappActivity entry: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 162
    :goto_0
    return-void

    .line 158
    :cond_0
    new-instance v1, Lcom/google/android/apps/chrome/webapps/ActivityAssigner$ActivityEntry;

    invoke-direct {v1, p1, p2}, Lcom/google/android/apps/chrome/webapps/ActivityAssigner$ActivityEntry;-><init>(ILjava/lang/String;)V

    .line 159
    iget-object v2, p0, Lcom/google/android/apps/chrome/webapps/ActivityAssigner;->mActivityList:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 160
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/ActivityAssigner;->mActivityList:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 161
    invoke-direct {p0}, Lcom/google/android/apps/chrome/webapps/ActivityAssigner;->storeActivityList()V

    goto :goto_0
.end method
