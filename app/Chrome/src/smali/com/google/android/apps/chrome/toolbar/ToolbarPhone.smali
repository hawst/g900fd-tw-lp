.class public Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;
.super Landroid/widget/FrameLayout;
.source "ToolbarPhone.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnLongClickListener;
.implements Lcom/google/android/apps/chrome/compositor/Invalidator$Client;
.implements Lcom/google/android/apps/chrome/ntp/NewTabPage$OnSearchBoxScrollListener;
.implements Lcom/google/android/apps/chrome/toolbar/Toolbar;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field static final LOCATION_BAR_TRANSPARENT_BACKGROUND_ALPHA:I = 0x33

.field public static final URL_FOCUS_CHANGE_ANIMATION_DURATION_MS:I = 0xfa


# instance fields
.field private mAnimateNormalToolbar:Z

.field private final mBackgroundOverlayBounds:Landroid/graphics/Rect;

.field private final mBrowsingModeViews:Ljava/util/List;

.field private mClipRect:Landroid/graphics/Rect;

.field private mDelayedOverviewModeAnimation:Landroid/animation/ObjectAnimator;

.field private mDelayingOverviewAnimation:Z

.field private mDisableLocationBarRelayout:Z

.field private mForceDrawLocationBarBackground:Z

.field private mHomeButton:Landroid/widget/ImageButton;

.field private mLayoutLocationBarInFocusedMode:Z

.field private mLocationBarBackground:Landroid/graphics/drawable/Drawable;

.field private final mLocationBarBackgroundOffset:Landroid/graphics/Rect;

.field private mMenuButton:Landroid/widget/ImageView;

.field private mNewTabButton:Lcom/google/android/apps/chrome/widget/newtab/NewTabButton;

.field private mNewTabListener:Landroid/view/View$OnClickListener;

.field private final mNtpSearchBoxBounds:Landroid/graphics/Rect;

.field private mNtpSearchBoxScrollPercent:F

.field private mOverviewAnimationBgOverlay:Landroid/graphics/drawable/Drawable;

.field private mOverviewAnimationMenuDrawable:Landroid/graphics/drawable/Drawable;

.field private mOverviewAnimationTabStackDrawable:Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;

.field private mOverviewListener:Landroid/view/View$OnClickListener;

.field private mOverviewMode:Z

.field private mOverviewModeAnimation:Landroid/animation/ObjectAnimator;

.field private mOverviewModePercent:F

.field private final mOverviewModePercentProperty:Landroid/util/Property;

.field private final mOverviewModeViews:Ljava/util/List;

.field private mPhoneLocationBar:Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;

.field private mProgressBar:Lcom/google/android/apps/chrome/widget/SmoothProgressBar;

.field private mReaderModeButton:Landroid/widget/ImageButton;

.field private mTabSwitcherButtonDrawable:Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;

.field private mTabSwitcherButtonDrawableLight:Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;

.field private mTextureCaptureMode:Z

.field private mToggleTabStackButton:Landroid/widget/ImageView;

.field private mToolbarBackground:Landroid/graphics/drawable/Drawable;

.field private mToolbarButtonsContainer:Landroid/view/View;

.field private mToolbarDelegate:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;

.field private final mToolbarHeightWithoutShadow:I

.field private mToolbarShadow:Landroid/widget/ImageView;

.field private final mToolbarSidePadding:I

.field private mUIAnimatingOverviewTransition:Z

.field private mUnfocusedLocationBarLayoutLeft:I

.field private mUnfocusedLocationBarLayoutWidth:I

.field private mUnfocusedLocationBarUsesTransparentBg:Z

.field private mUrlActionsContainer:Landroid/view/View;

.field private mUrlBackgroundAlpha:I

.field private final mUrlBackgroundPadding:Landroid/graphics/Rect;

.field private mUrlBar:Landroid/widget/TextView;

.field private mUrlContainer:Lcom/google/android/apps/chrome/omnibox/UrlContainer;

.field private mUrlFocusChangeInProgress:Z

.field private mUrlFocusChangePercent:F

.field private final mUrlFocusChangePercentProperty:Landroid/util/Property;

.field private mUrlFocusLayoutAnimator:Landroid/animation/AnimatorSet;

.field private final mUrlViewportBounds:Landroid/graphics/Rect;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 72
    const-class v0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    .line 199
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 110
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mOverviewModeViews:Ljava/util/List;

    .line 111
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mBrowsingModeViews:Ljava/util/List;

    .line 126
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mOverviewModePercent:F

    .line 146
    const/16 v0, 0xff

    iput v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUrlBackgroundAlpha:I

    .line 147
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mNtpSearchBoxScrollPercent:F

    .line 154
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUrlViewportBounds:Landroid/graphics/Rect;

    .line 155
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUrlBackgroundPadding:Landroid/graphics/Rect;

    .line 156
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mBackgroundOverlayBounds:Landroid/graphics/Rect;

    .line 157
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mLocationBarBackgroundOffset:Landroid/graphics/Rect;

    .line 158
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mNtpSearchBoxBounds:Landroid/graphics/Rect;

    .line 166
    new-instance v0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$1;

    const-class v1, Ljava/lang/Float;

    const-string/jumbo v2, ""

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$1;-><init>(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;Ljava/lang/Class;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUrlFocusChangePercentProperty:Landroid/util/Property;

    .line 179
    new-instance v0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$2;

    const-class v1, Ljava/lang/Float;

    const-string/jumbo v2, ""

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$2;-><init>(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;Ljava/lang/Class;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mOverviewModePercentProperty:Landroid/util/Property;

    .line 200
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 201
    sget v1, Lcom/google/android/apps/chrome/R$dimen;->toolbar_edge_padding:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mToolbarSidePadding:I

    .line 203
    sget v1, Lcom/google/android/apps/chrome/R$dimen;->toolbar_height_no_shadow:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mToolbarHeightWithoutShadow:I

    .line 205
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)F
    .locals 1

    .prologue
    .line 72
    iget v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUrlFocusChangePercent:F

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;F)V
    .locals 0

    .prologue
    .line 72
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->setUrlFocusChangePercent(F)V

    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Landroid/animation/ObjectAnimator;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mOverviewModeAnimation:Landroid/animation/ObjectAnimator;

    return-object v0
.end method

.method static synthetic access$1002(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;Landroid/animation/ObjectAnimator;)Landroid/animation/ObjectAnimator;
    .locals 0

    .prologue
    .line 72
    iput-object p1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mOverviewModeAnimation:Landroid/animation/ObjectAnimator;

    return-object p1
.end method

.method static synthetic access$1100(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Lcom/google/android/apps/chrome/omnibox/UrlContainer;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUrlContainer:Lcom/google/android/apps/chrome/omnibox/UrlContainer;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Lcom/google/android/apps/chrome/widget/SmoothProgressBar;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mProgressBar:Lcom/google/android/apps/chrome/widget/SmoothProgressBar;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Landroid/animation/ObjectAnimator;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mDelayedOverviewModeAnimation:Landroid/animation/ObjectAnimator;

    return-object v0
.end method

.method static synthetic access$1302(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;Landroid/animation/ObjectAnimator;)Landroid/animation/ObjectAnimator;
    .locals 0

    .prologue
    .line 72
    iput-object p1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mDelayedOverviewModeAnimation:Landroid/animation/ObjectAnimator;

    return-object p1
.end method

.method static synthetic access$1400(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Landroid/widget/ImageButton;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mReaderModeButton:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Landroid/util/Property;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mOverviewModePercentProperty:Landroid/util/Property;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Z
    .locals 1

    .prologue
    .line 72
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mTextureCaptureMode:Z

    return v0
.end method

.method static synthetic access$1802(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;Z)Z
    .locals 0

    .prologue
    .line 72
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mTextureCaptureMode:Z

    return p1
.end method

.method static synthetic access$1900(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Z
    .locals 1

    .prologue
    .line 72
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUIAnimatingOverviewTransition:Z

    return v0
.end method

.method static synthetic access$1902(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;Z)Z
    .locals 0

    .prologue
    .line 72
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUIAnimatingOverviewTransition:Z

    return p1
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)F
    .locals 1

    .prologue
    .line 72
    iget v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mOverviewModePercent:F

    return v0
.end method

.method static synthetic access$2000(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Ljava/util/List;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mOverviewModeViews:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$202(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;F)F
    .locals 0

    .prologue
    .line 72
    iput p1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mOverviewModePercent:F

    return p1
.end method

.method static synthetic access$2100(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Z
    .locals 1

    .prologue
    .line 72
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mDelayingOverviewAnimation:Z

    return v0
.end method

.method static synthetic access$2102(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;Z)Z
    .locals 0

    .prologue
    .line 72
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mDelayingOverviewAnimation:Z

    return p1
.end method

.method static synthetic access$2200(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Landroid/animation/AnimatorSet;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUrlFocusLayoutAnimator:Landroid/animation/AnimatorSet;

    return-object v0
.end method

.method static synthetic access$2202(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;Landroid/animation/AnimatorSet;)Landroid/animation/AnimatorSet;
    .locals 0

    .prologue
    .line 72
    iput-object p1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUrlFocusLayoutAnimator:Landroid/animation/AnimatorSet;

    return-object p1
.end method

.method static synthetic access$2300(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Lcom/google/android/apps/chrome/widget/newtab/NewTabButton;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mNewTabButton:Lcom/google/android/apps/chrome/widget/newtab/NewTabButton;

    return-object v0
.end method

.method static synthetic access$2400(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Z
    .locals 1

    .prologue
    .line 72
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mAnimateNormalToolbar:Z

    return v0
.end method

.method static synthetic access$2402(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;Z)Z
    .locals 0

    .prologue
    .line 72
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mAnimateNormalToolbar:Z

    return p1
.end method

.method static synthetic access$2502(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;Landroid/graphics/Rect;)Landroid/graphics/Rect;
    .locals 0

    .prologue
    .line 72
    iput-object p1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mClipRect:Landroid/graphics/Rect;

    return-object p1
.end method

.method static synthetic access$2600(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Z
    .locals 1

    .prologue
    .line 72
    invoke-direct {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->isIncognitoMode()Z

    move-result v0

    return v0
.end method

.method static synthetic access$2702(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;
    .locals 0

    .prologue
    .line 72
    iput-object p1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mOverviewAnimationBgOverlay:Landroid/graphics/drawable/Drawable;

    return-object p1
.end method

.method static synthetic access$2800(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mOverviewAnimationMenuDrawable:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method static synthetic access$2802(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;
    .locals 0

    .prologue
    .line 72
    iput-object p1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mOverviewAnimationMenuDrawable:Landroid/graphics/drawable/Drawable;

    return-object p1
.end method

.method static synthetic access$2902(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;Landroid/view/View$OnClickListener;)Landroid/view/View$OnClickListener;
    .locals 0

    .prologue
    .line 72
    iput-object p1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mOverviewListener:Landroid/view/View$OnClickListener;

    return-object p1
.end method

.method static synthetic access$300(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mToolbarDelegate:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;

    return-object v0
.end method

.method static synthetic access$3002(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;Landroid/view/View$OnClickListener;)Landroid/view/View$OnClickListener;
    .locals 0

    .prologue
    .line 72
    iput-object p1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mNewTabListener:Landroid/view/View$OnClickListener;

    return-object p1
.end method

.method static synthetic access$3100(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUrlBar:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$3200(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Landroid/util/Property;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUrlFocusChangePercentProperty:Landroid/util/Property;

    return-object v0
.end method

.method static synthetic access$3300(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mPhoneLocationBar:Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;

    return-object v0
.end method

.method static synthetic access$3400(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)F
    .locals 1

    .prologue
    .line 72
    iget v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mNtpSearchBoxScrollPercent:F

    return v0
.end method

.method static synthetic access$3502(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;Z)Z
    .locals 0

    .prologue
    .line 72
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUrlFocusChangeInProgress:Z

    return p1
.end method

.method static synthetic access$3600(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Landroid/view/View;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mToolbarButtonsContainer:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$3702(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;Z)Z
    .locals 0

    .prologue
    .line 72
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mDisableLocationBarRelayout:Z

    return p1
.end method

.method static synthetic access$3802(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;Z)Z
    .locals 0

    .prologue
    .line 72
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mLayoutLocationBarInFocusedMode:Z

    return p1
.end method

.method static synthetic access$3900(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mToolbarShadow:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mMenuButton:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$4000(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mTabSwitcherButtonDrawableLight:Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;

    return-object v0
.end method

.method static synthetic access$4100(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mTabSwitcherButtonDrawable:Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;

    return-object v0
.end method

.method static synthetic access$4200(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mOverviewAnimationTabStackDrawable:Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;

    return-object v0
.end method

.method static synthetic access$4202(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;)Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;
    .locals 0

    .prologue
    .line 72
    iput-object p1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mOverviewAnimationTabStackDrawable:Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;

    return-object p1
.end method

.method static synthetic access$4300(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)V
    .locals 0

    .prologue
    .line 72
    invoke-direct {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->resetNtpAnimationValues()V

    return-void
.end method

.method static synthetic access$4600(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Z
    .locals 1

    .prologue
    .line 72
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUnfocusedLocationBarUsesTransparentBg:Z

    return v0
.end method

.method static synthetic access$4602(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;Z)Z
    .locals 0

    .prologue
    .line 72
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUnfocusedLocationBarUsesTransparentBg:Z

    return p1
.end method

.method static synthetic access$4700(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 72
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->updateToolbarBackground(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method static synthetic access$4800(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;Lcom/google/android/apps/chrome/ntp/NewTabPage;)V
    .locals 0

    .prologue
    .line 72
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->updateNtpTransitionAnimation(Lcom/google/android/apps/chrome/ntp/NewTabPage;)V

    return-void
.end method

.method static synthetic access$4902(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;I)I
    .locals 0

    .prologue
    .line 72
    iput p1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUrlBackgroundAlpha:I

    return p1
.end method

.method static synthetic access$500(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mToggleTabStackButton:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$5000(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mToolbarBackground:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Landroid/widget/ImageButton;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mHomeButton:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Ljava/util/List;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mBrowsingModeViews:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Z
    .locals 1

    .prologue
    .line 72
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mOverviewMode:Z

    return v0
.end method

.method static synthetic access$902(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;Z)Z
    .locals 0

    .prologue
    .line 72
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mOverviewMode:Z

    return p1
.end method

.method private drawLocationBar(Landroid/graphics/Canvas;J)Z
    .locals 8

    .prologue
    const/high16 v6, 0x3f800000    # 1.0f

    .line 979
    const/4 v0, 0x0

    .line 985
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mLocationBarBackground:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_5

    iget-boolean v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mOverviewMode:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mTextureCaptureMode:Z

    if-eqz v1, :cond_5

    .line 986
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 987
    iget v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUrlBackgroundAlpha:I

    .line 988
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mOverviewModeAnimation:Landroid/animation/ObjectAnimator;

    if-eqz v1, :cond_7

    .line 991
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mPhoneLocationBar:Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->getAlpha()F

    move-result v1

    float-to-double v2, v1

    const-wide/high16 v4, 0x4008000000000000L    # 3.0

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    int-to-double v0, v0

    mul-double/2addr v0, v2

    double-to-int v0, v0

    .line 999
    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mLocationBarBackground:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 1001
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mPhoneLocationBar:Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->getAlpha()F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-gtz v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mForceDrawLocationBarBackground:Z

    if-eqz v0, :cond_3

    .line 1002
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mLocationBarBackground:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUrlViewportBounds:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iget-object v2, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mLocationBarBackgroundOffset:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    add-int/2addr v1, v2

    iget-object v2, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUrlViewportBounds:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    iget-object v3, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mLocationBarBackgroundOffset:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    add-int/2addr v2, v3

    iget-object v3, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUrlViewportBounds:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    iget-object v4, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mLocationBarBackgroundOffset:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->right:I

    add-int/2addr v3, v4

    iget-object v4, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUrlViewportBounds:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    iget-object v5, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mLocationBarBackgroundOffset:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v4, v5

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1007
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mLocationBarBackground:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1010
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUrlViewportBounds:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUrlBackgroundPadding:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mLocationBarBackgroundOffset:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    add-int/2addr v0, v1

    int-to-float v1, v0

    .line 1012
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUrlViewportBounds:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->right:I

    iget-object v2, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUrlBackgroundPadding:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    sub-int/2addr v0, v2

    iget-object v2, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mLocationBarBackgroundOffset:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    add-int/2addr v0, v2

    int-to-float v0, v0

    .line 1019
    iget v2, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUrlFocusChangePercent:F

    cmpl-float v2, v2, v6

    if-eqz v2, :cond_4

    .line 1020
    iget v2, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUnfocusedLocationBarLayoutLeft:I

    invoke-direct {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->getViewBoundsLeftOfLocationBar()I

    move-result v3

    sub-int/2addr v2, v3

    .line 1021
    invoke-direct {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->getViewBoundsRightOfLocationBar()I

    move-result v3

    iget v4, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUnfocusedLocationBarLayoutLeft:I

    sub-int/2addr v3, v4

    iget v4, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUnfocusedLocationBarLayoutWidth:I

    sub-int/2addr v3, v4

    .line 1024
    iget v4, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUrlFocusChangePercent:F

    sub-float v4, v6, v4

    .line 1025
    int-to-float v2, v2

    mul-float/2addr v2, v4

    add-float/2addr v1, v2

    .line 1026
    int-to-float v2, v3

    mul-float/2addr v2, v4

    sub-float/2addr v0, v2

    .line 1029
    :cond_4
    iget-object v2, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUrlViewportBounds:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    iget-object v3, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUrlBackgroundPadding:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    add-int/2addr v2, v3

    iget-object v3, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mLocationBarBackgroundOffset:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    add-int/2addr v2, v3

    int-to-float v2, v2

    .line 1031
    iget-object v3, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUrlViewportBounds:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    iget-object v4, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUrlBackgroundPadding:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v3, v4

    iget-object v4, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mLocationBarBackgroundOffset:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v3, v4

    int-to-float v3, v3

    .line 1034
    invoke-virtual {p1, v1, v2, v0, v3}, Landroid/graphics/Canvas;->clipRect(FFFF)Z

    .line 1037
    const/4 v0, 0x1

    .line 1040
    :cond_5
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mPhoneLocationBar:Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;

    invoke-super {p0, p1, v1, p2, p3}, Landroid/widget/FrameLayout;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    move-result v1

    .line 1042
    if-eqz v0, :cond_6

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 1043
    :cond_6
    return v1

    .line 993
    :cond_7
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->getDelegate()Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->getToolbarDataProvider()Lcom/google/android/apps/chrome/toolbar/ToolbarDataProvider;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarDataProvider;->isUsingBrandColor()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 994
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUnfocusedLocationBarUsesTransparentBg:Z

    if-eqz v0, :cond_8

    const/16 v0, 0x33

    .line 996
    :goto_1
    iget v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUrlFocusChangePercent:F

    rsub-int v2, v0, 0xff

    int-to-float v2, v2

    mul-float/2addr v1, v2

    int-to-float v0, v0

    add-float/2addr v0, v1

    float-to-int v0, v0

    goto/16 :goto_0

    .line 994
    :cond_8
    const/16 v0, 0xff

    goto :goto_1
.end method

.method private drawOverviewAnimationOverlay(Landroid/graphics/Canvas;F)V
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/16 v8, 0x8

    const/4 v2, 0x0

    .line 811
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mToolbarDelegate:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->isNativeLibraryReady()Z

    move-result v0

    if-nez v0, :cond_0

    .line 902
    :goto_0
    return-void

    .line 813
    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    sub-float v3, v0, p2

    .line 814
    const/high16 v0, 0x437f0000    # 255.0f

    mul-float/2addr v0, v3

    float-to-int v4, v0

    .line 815
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 816
    neg-float v0, p2

    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mBackgroundOverlayBounds:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v0, v1

    invoke-virtual {p1, v9, v0}, Landroid/graphics/Canvas;->translate(FF)V

    .line 817
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mBackgroundOverlayBounds:Landroid/graphics/Rect;

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;)Z

    .line 820
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mOverviewAnimationBgOverlay:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->getMeasuredHeight()I

    move-result v5

    invoke-virtual {v0, v2, v2, v1, v5}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 822
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mOverviewAnimationBgOverlay:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v4}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 823
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mOverviewAnimationBgOverlay:Landroid/graphics/drawable/Drawable;

    instance-of v0, v0, Landroid/graphics/drawable/LayerDrawable;

    if-eqz v0, :cond_2

    .line 824
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mOverviewAnimationBgOverlay:Landroid/graphics/drawable/Drawable;

    check-cast v0, Landroid/graphics/drawable/LayerDrawable;

    move v1, v2

    .line 825
    :goto_1
    invoke-virtual {v0}, Landroid/graphics/drawable/LayerDrawable;->getNumberOfLayers()I

    move-result v5

    if-ge v1, v5, :cond_3

    .line 828
    invoke-virtual {v0, v1}, Landroid/graphics/drawable/LayerDrawable;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable;->isVisible()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 829
    invoke-virtual {v0, v1}, Landroid/graphics/drawable/LayerDrawable;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {v5, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 825
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 833
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mOverviewAnimationBgOverlay:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 837
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mHomeButton:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getVisibility()I

    move-result v0

    if-eq v0, v8, :cond_4

    .line 839
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mHomeButton:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getAlpha()F

    move-result v0

    .line 840
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mHomeButton:Landroid/widget/ImageButton;

    mul-float v5, v0, v3

    invoke-virtual {v1, v5}, Landroid/widget/ImageButton;->setAlpha(F)V

    .line 841
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mHomeButton:Landroid/widget/ImageButton;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v6

    invoke-virtual {p0, p1, v1, v6, v7}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    .line 842
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mHomeButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setAlpha(F)V

    .line 846
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mPhoneLocationBar:Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->getAlpha()F

    move-result v0

    .line 847
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mPhoneLocationBar:Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;

    mul-float v5, v0, v3

    invoke-virtual {v1, v5}, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->setAlpha(F)V

    .line 849
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mPhoneLocationBar:Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->getAlpha()F

    move-result v1

    cmpl-float v1, v1, v9

    if-eqz v1, :cond_5

    .line 850
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mPhoneLocationBar:Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v6

    invoke-virtual {p0, p1, v1, v6, v7}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    .line 852
    :cond_5
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mPhoneLocationBar:Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->setAlpha(F)V

    .line 855
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mToolbarButtonsContainer:Landroid/view/View;

    invoke-static {p0, v0, p1}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->translateCanvasToView(Landroid/view/View;Landroid/view/View;Landroid/graphics/Canvas;)V

    .line 857
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mReaderModeButton:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getVisibility()I

    move-result v0

    if-eq v0, v8, :cond_6

    .line 858
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mReaderModeButton:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getVisibility()I

    move-result v0

    .line 859
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mReaderModeButton:Landroid/widget/ImageButton;

    invoke-virtual {v1}, Landroid/widget/ImageButton;->getAlpha()F

    move-result v1

    .line 860
    iget-object v5, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mReaderModeButton:Landroid/widget/ImageButton;

    invoke-virtual {v5, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 861
    iget-object v2, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mReaderModeButton:Landroid/widget/ImageButton;

    mul-float/2addr v3, v1

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setAlpha(F)V

    .line 862
    iget-object v2, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mReaderModeButton:Landroid/widget/ImageButton;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v6

    invoke-virtual {p0, p1, v2, v6, v7}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    .line 863
    iget-object v2, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mReaderModeButton:Landroid/widget/ImageButton;

    invoke-virtual {v2, v1}, Landroid/widget/ImageButton;->setAlpha(F)V

    .line 864
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mReaderModeButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 867
    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mOverviewAnimationTabStackDrawable:Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mToggleTabStackButton:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getVisibility()I

    move-result v0

    if-eq v0, v8, :cond_7

    .line 870
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 871
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mToolbarButtonsContainer:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mToggleTabStackButton:Landroid/widget/ImageView;

    invoke-static {v0, v1, p1}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->translateCanvasToView(Landroid/view/View;Landroid/view/View;Landroid/graphics/Canvas;)V

    .line 873
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mToggleTabStackButton:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    .line 874
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mToggleTabStackButton:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    .line 875
    iget-object v2, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mToggleTabStackButton:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getWidth()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mToggleTabStackButton:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getPaddingLeft()I

    move-result v3

    sub-int/2addr v2, v3

    iget-object v3, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mToggleTabStackButton:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getPaddingRight()I

    move-result v3

    sub-int/2addr v2, v3

    sub-int v0, v2, v0

    div-int/lit8 v0, v0, 0x2

    .line 878
    iget-object v2, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mToggleTabStackButton:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getPaddingLeft()I

    move-result v2

    add-int/2addr v0, v2

    .line 879
    iget-object v2, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mToggleTabStackButton:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getHeight()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mToggleTabStackButton:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getPaddingTop()I

    move-result v3

    sub-int/2addr v2, v3

    iget-object v3, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mToggleTabStackButton:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getPaddingBottom()I

    move-result v3

    sub-int/2addr v2, v3

    sub-int v1, v2, v1

    div-int/lit8 v1, v1, 0x2

    .line 882
    iget-object v2, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mToggleTabStackButton:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getPaddingTop()I

    move-result v2

    add-int/2addr v1, v2

    .line 883
    int-to-float v0, v0

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 885
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mOverviewAnimationTabStackDrawable:Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;

    invoke-virtual {v0, v4}, Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;->setAlpha(I)V

    .line 886
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mOverviewAnimationTabStackDrawable:Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 887
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 891
    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mOverviewAnimationMenuDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_8

    .line 892
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mOverviewAnimationMenuDrawable:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mMenuButton:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getPaddingLeft()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mMenuButton:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getPaddingTop()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mMenuButton:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getWidth()I

    move-result v3

    iget-object v5, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mMenuButton:Landroid/widget/ImageView;

    invoke-virtual {v5}, Landroid/widget/ImageView;->getPaddingRight()I

    move-result v5

    sub-int/2addr v3, v5

    iget-object v5, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mMenuButton:Landroid/widget/ImageView;

    invoke-virtual {v5}, Landroid/widget/ImageView;->getHeight()I

    move-result v5

    iget-object v6, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mMenuButton:Landroid/widget/ImageView;

    invoke-virtual {v6}, Landroid/widget/ImageView;->getPaddingBottom()I

    move-result v6

    sub-int/2addr v5, v6

    invoke-virtual {v0, v1, v2, v3, v5}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 896
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mToolbarButtonsContainer:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mMenuButton:Landroid/widget/ImageView;

    invoke-static {v0, v1, p1}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->translateCanvasToView(Landroid/view/View;Landroid/view/View;Landroid/graphics/Canvas;)V

    .line 897
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mOverviewAnimationMenuDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v4}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 898
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mOverviewAnimationMenuDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 901
    :cond_8
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    goto/16 :goto_0
.end method

.method private drawOverviewFadeAnimation(ZF)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 796
    invoke-virtual {p0, p2}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->setAlpha(F)V

    .line 797
    if-eqz p1, :cond_2

    .line 798
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mClipRect:Landroid/graphics/Rect;

    .line 802
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mClipRect:Landroid/graphics/Rect;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mClipRect:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->getHeight()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v2, p2

    float-to-int v2, v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/Rect;->set(IIII)V

    .line 803
    :cond_1
    return-void

    .line 799
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mClipRect:Landroid/graphics/Rect;

    if-nez v0, :cond_0

    .line 800
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mClipRect:Landroid/graphics/Rect;

    goto :goto_0
.end method

.method private getFrameLayoutParams(Landroid/view/View;)Landroid/widget/FrameLayout$LayoutParams;
    .locals 1

    .prologue
    .line 515
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    return-object v0
.end method

.method private getViewBoundsLeftOfLocationBar()I
    .locals 2

    .prologue
    .line 495
    invoke-static {p0}, Lorg/chromium/base/ApiCompatibilityUtils;->isLayoutRtl(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 496
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mToolbarButtonsContainer:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    .line 498
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mHomeButton:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mHomeButton:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getMeasuredWidth()I

    move-result v0

    goto :goto_0

    :cond_1
    iget v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mToolbarSidePadding:I

    goto :goto_0
.end method

.method private getViewBoundsRightOfLocationBar()I
    .locals 3

    .prologue
    .line 506
    invoke-static {p0}, Lorg/chromium/base/ApiCompatibilityUtils;->isLayoutRtl(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 507
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->getMeasuredWidth()I

    move-result v1

    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mHomeButton:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getVisibility()I

    move-result v0

    const/16 v2, 0x8

    if-eq v0, v2, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mHomeButton:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getMeasuredWidth()I

    move-result v0

    :goto_0
    sub-int v0, v1, v0

    .line 510
    :goto_1
    return v0

    .line 507
    :cond_0
    iget v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mToolbarSidePadding:I

    goto :goto_0

    .line 510
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->getMeasuredWidth()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mToolbarButtonsContainer:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    sub-int/2addr v0, v1

    goto :goto_1
.end method

.method private isIncognitoMode()Z
    .locals 1

    .prologue
    .line 519
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->getDelegate()Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->getToolbarDataProvider()Lcom/google/android/apps/chrome/toolbar/ToolbarDataProvider;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarDataProvider;->isIncognito()Z

    move-result v0

    return v0
.end method

.method private layoutLocationBar(I)Z
    .locals 8

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 448
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->getDelegate()Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->getLocationBar()Lcom/google/android/apps/chrome/omnibox/LocationBar;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->getFrameLayoutParams(Landroid/view/View;)Landroid/widget/FrameLayout$LayoutParams;

    move-result-object v5

    .line 453
    const/16 v0, 0x33

    iput v0, v5, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 455
    invoke-direct {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->updateUnfocusedLocationBarLayoutParams()V

    .line 462
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mLayoutLocationBarInFocusedMode:Z

    if-eqz v0, :cond_3

    move v0, v1

    move v2, v1

    .line 464
    :goto_0
    iget-object v3, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mPhoneLocationBar:Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->getChildCount()I

    move-result v3

    if-ge v2, v3, :cond_1

    .line 465
    iget-object v3, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mPhoneLocationBar:Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;

    invoke-virtual {v3, v2}, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 466
    iget-object v6, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mPhoneLocationBar:Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;

    invoke-virtual {v6}, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->getFirstViewVisibleWhenFocused()Landroid/view/View;

    move-result-object v6

    if-eq v3, v6, :cond_1

    .line 467
    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v6

    const/16 v7, 0x8

    if-eq v6, v7, :cond_0

    .line 468
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    add-int/2addr v0, v3

    .line 464
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 471
    :cond_1
    iget v2, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mToolbarSidePadding:I

    mul-int/lit8 v2, v2, 0x2

    sub-int v2, p1, v2

    add-int/2addr v2, v0

    .line 472
    iget-object v3, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mPhoneLocationBar:Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;

    invoke-static {v3}, Lorg/chromium/base/ApiCompatibilityUtils;->isLayoutRtl(Landroid/view/View;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 473
    iget v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mToolbarSidePadding:I

    .line 482
    :goto_1
    iget v3, v5, Landroid/widget/FrameLayout$LayoutParams;->width:I

    if-eq v2, v3, :cond_4

    move v3, v4

    :goto_2
    or-int/lit8 v3, v3, 0x0

    .line 484
    iput v2, v5, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 486
    iget v2, v5, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    if-eq v0, v2, :cond_5

    :goto_3
    or-int v1, v3, v4

    .line 487
    iput v0, v5, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 489
    return v1

    .line 475
    :cond_2
    neg-int v0, v0

    iget v3, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mToolbarSidePadding:I

    add-int/2addr v0, v3

    .line 477
    goto :goto_1

    .line 478
    :cond_3
    iget v2, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUnfocusedLocationBarLayoutWidth:I

    .line 479
    iget v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUnfocusedLocationBarLayoutLeft:I

    goto :goto_1

    :cond_4
    move v3, v1

    .line 482
    goto :goto_2

    :cond_5
    move v4, v1

    .line 486
    goto :goto_3
.end method

.method private resetNtpAnimationValues()V
    .locals 3

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    .line 690
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mLocationBarBackgroundOffset:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    .line 691
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mPhoneLocationBar:Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->setTranslationY(F)V

    .line 692
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mToolbarShadow:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setTranslationY(F)V

    .line 693
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mToolbarShadow:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 694
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mPhoneLocationBar:Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->setAlpha(F)V

    .line 695
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mForceDrawLocationBarBackground:Z

    .line 696
    invoke-direct {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->isIncognitoMode()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUnfocusedLocationBarUsesTransparentBg:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUrlFocusChangeInProgress:Z

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->getDelegate()Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->getLocationBar()Lcom/google/android/apps/chrome/omnibox/LocationBar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->getUrlBar()Lcom/google/android/apps/chrome/omnibox/UrlBar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->hasFocus()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/16 v0, 0x33

    :goto_0
    iput v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUrlBackgroundAlpha:I

    .line 701
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->setAncestorsShouldClipChildren(Z)V

    .line 702
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mNtpSearchBoxScrollPercent:F

    .line 703
    return-void

    .line 696
    :cond_1
    const/16 v0, 0xff

    goto :goto_0
.end method

.method private setAncestorsShouldClipChildren(Z)V
    .locals 2

    .prologue
    .line 785
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mToolbarDelegate:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;

    # invokes: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->isLocationBarShownInNTP()Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->access$600(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 793
    :cond_0
    return-void

    .line 787
    :cond_1
    :goto_0
    if-eqz p0, :cond_0

    .line 788
    invoke-virtual {p0, p1}, Landroid/view/ViewGroup;->setClipChildren(Z)V

    .line 789
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    instance-of v0, v0, Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    .line 790
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getId()I

    move-result v0

    const v1, 0x1020002

    if-eq v0, v1, :cond_0

    .line 791
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    move-object p0, v0

    goto :goto_0
.end method

.method private setUrlFocusChangePercent(F)V
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 636
    iput p1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUrlFocusChangePercent:F

    .line 637
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mLocationBarBackgroundOffset:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    .line 639
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mPhoneLocationBar:Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->getFrameLayoutParams(Landroid/view/View;)Landroid/widget/FrameLayout$LayoutParams;

    move-result-object v0

    .line 641
    iget v1, v0, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 642
    iget v0, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 644
    iget-object v2, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mToolbarDelegate:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->urlHasFocus()Z

    move-result v2

    if-nez v2, :cond_1

    iget-boolean v2, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mLayoutLocationBarInFocusedMode:Z

    if-nez v2, :cond_1

    .line 645
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mPhoneLocationBar:Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->setTranslationX(F)V

    .line 646
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUrlActionsContainer:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setTranslationX(F)V

    .line 670
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mPhoneLocationBar:Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->invalidate()V

    .line 671
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->invalidate()V

    .line 673
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mToolbarDelegate:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->getToolbarDataProvider()Lcom/google/android/apps/chrome/toolbar/ToolbarDataProvider;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarDataProvider;->getTab()Lorg/chromium/chrome/browser/Tab;

    move-result-object v0

    .line 674
    if-nez v0, :cond_3

    .line 687
    :goto_1
    return-void

    .line 648
    :cond_1
    const/high16 v2, 0x3f800000    # 1.0f

    sub-float/2addr v2, p1

    .line 649
    iget-object v3, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mPhoneLocationBar:Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;

    invoke-static {v3}, Lorg/chromium/base/ApiCompatibilityUtils;->isLayoutRtl(Landroid/view/View;)Z

    move-result v3

    .line 650
    iget-object v4, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mPhoneLocationBar:Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;

    invoke-static {v4}, Lorg/chromium/base/ApiCompatibilityUtils;->isLayoutRtl(Landroid/view/View;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 651
    iget-object v4, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mPhoneLocationBar:Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;

    iget v5, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUnfocusedLocationBarLayoutLeft:I

    iget v6, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUnfocusedLocationBarLayoutWidth:I

    add-int/2addr v5, v6

    add-int/2addr v0, v1

    sub-int v0, v5, v0

    int-to-float v0, v0

    mul-float/2addr v0, v2

    invoke-virtual {v4, v0}, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->setTranslationX(F)V

    .line 664
    :goto_2
    invoke-static {p0}, Lorg/chromium/base/ApiCompatibilityUtils;->isLayoutRtl(Landroid/view/View;)Z

    move-result v0

    if-ne v3, v0, :cond_0

    .line 665
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUrlActionsContainer:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mPhoneLocationBar:Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->getTranslationX()F

    move-result v1

    neg-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationX(F)V

    goto :goto_0

    .line 655
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mPhoneLocationBar:Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;

    iget v4, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUnfocusedLocationBarLayoutLeft:I

    sub-int v1, v4, v1

    int-to-float v1, v1

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->setTranslationX(F)V

    .line 657
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUrlActionsContainer:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mPhoneLocationBar:Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->getTranslationX()F

    move-result v1

    neg-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationX(F)V

    goto :goto_2

    .line 676
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mToolbarDelegate:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->getToolbarDataProvider()Lcom/google/android/apps/chrome/toolbar/ToolbarDataProvider;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarDataProvider;->getNewTabPageForCurrentTab()Lcom/google/android/apps/chrome/ntp/NewTabPage;

    move-result-object v0

    .line 677
    if-eqz v0, :cond_4

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/ntp/NewTabPage;->setUrlFocusChangeAnimationPercent(F)V

    .line 679
    :cond_4
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mToolbarDelegate:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;

    # invokes: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->isLocationBarShownInNTP()Z
    invoke-static {v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->access$600(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 682
    invoke-direct {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->resetNtpAnimationValues()V

    goto :goto_1

    .line 686
    :cond_5
    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->updateNtpTransitionAnimation(Lcom/google/android/apps/chrome/ntp/NewTabPage;)V

    goto :goto_1
.end method

.method private static translateCanvasToView(Landroid/view/View;Landroid/view/View;Landroid/graphics/Canvas;)V
    .locals 2

    .prologue
    .line 920
    sget-boolean v0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 921
    :cond_0
    sget-boolean v0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    if-nez p1, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 927
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    move-object p1, v0

    .line 922
    :cond_2
    if-eq p1, p0, :cond_3

    .line 923
    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 924
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    instance-of v0, v0, Landroid/view/View;

    if-nez v0, :cond_1

    .line 925
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "View \'to\' was not a desendent of \'from\'."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 929
    :cond_3
    return-void
.end method

.method private updateNtpTransitionAnimation(Lcom/google/android/apps/chrome/ntp/NewTabPage;)V
    .locals 14

    .prologue
    const/16 v4, 0xff

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v6, 0x0

    const/high16 v5, 0x3f800000    # 1.0f

    .line 709
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mToolbarBackground:Landroid/graphics/drawable/Drawable;

    instance-of v0, v0, Landroid/graphics/drawable/LayerDrawable;

    if-nez v0, :cond_0

    .line 782
    :goto_0
    return-void

    .line 711
    :cond_0
    iget v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mNtpSearchBoxScrollPercent:F

    iget v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUrlFocusChangePercent:F

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v7

    .line 712
    sget-boolean v0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    cmpl-float v0, v7, v6

    if-gez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 713
    :cond_1
    sget-boolean v0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    cmpg-float v0, v7, v5

    if-lez v0, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 715
    :cond_2
    cmpl-float v0, v7, v5

    if-eqz v0, :cond_3

    cmpl-float v0, v7, v6

    if-nez v0, :cond_7

    :cond_3
    move v0, v3

    :goto_1
    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->setAncestorsShouldClipChildren(Z)V

    .line 718
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mToolbarBackground:Landroid/graphics/drawable/Drawable;

    check-cast v0, Landroid/graphics/drawable/LayerDrawable;

    .line 719
    const/high16 v1, 0x437f0000    # 255.0f

    mul-float/2addr v1, v7

    float-to-int v8, v1

    .line 720
    sget v1, Lcom/google/android/apps/chrome/R$id;->toolbar_bg:I

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/LayerDrawable;->findDrawableByLayerId(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v1, v8}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 722
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mToolbarShadow:Landroid/widget/ImageView;

    invoke-virtual {v1, v7}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 726
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mOverviewAnimationBgOverlay:Landroid/graphics/drawable/Drawable;

    instance-of v1, v1, Landroid/graphics/drawable/LayerDrawable;

    if-eqz v1, :cond_4

    .line 727
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mOverviewAnimationBgOverlay:Landroid/graphics/drawable/Drawable;

    check-cast v1, Landroid/graphics/drawable/LayerDrawable;

    sget v9, Lcom/google/android/apps/chrome/R$id;->toolbar_bg:I

    invoke-virtual {v1, v9}, Landroid/graphics/drawable/LayerDrawable;->findDrawableByLayerId(I)Landroid/graphics/drawable/Drawable;

    move-result-object v9

    .line 729
    invoke-virtual {v9, v8}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 732
    cmpl-float v1, v7, v6

    if-eqz v1, :cond_8

    move v1, v3

    :goto_2
    invoke-virtual {v9, v1, v2}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    .line 735
    :cond_4
    sget v1, Lcom/google/android/apps/chrome/R$id;->toolbar_new_tab_bg:I

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/LayerDrawable;->findDrawableByLayerId(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 736
    cmpl-float v0, v7, v5

    if-nez v0, :cond_9

    move v0, v4

    :goto_3
    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 738
    cmpl-float v0, v7, v6

    if-eqz v0, :cond_5

    cmpl-float v0, v7, v5

    if-nez v0, :cond_a

    .line 740
    :cond_5
    sub-float v0, v5, v7

    .line 748
    :goto_4
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mPhoneLocationBar:Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->getPaddingTop()I

    move-result v8

    .line 749
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mPhoneLocationBar:Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->getPaddingBottom()I

    move-result v9

    .line 751
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mNtpSearchBoxBounds:Landroid/graphics/Rect;

    invoke-virtual {p1, v1}, Lcom/google/android/apps/chrome/ntp/NewTabPage;->getSearchBoxBounds(Landroid/graphics/Rect;)V

    .line 752
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mNtpSearchBoxBounds:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    iget-object v10, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mPhoneLocationBar:Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;

    invoke-virtual {v10}, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->getMeasuredHeight()I

    move-result v10

    sub-int/2addr v10, v8

    sub-int/2addr v10, v9

    sub-int/2addr v1, v10

    int-to-float v1, v1

    const/high16 v10, 0x40000000    # 2.0f

    div-float v10, v1, v10

    .line 754
    iget-object v11, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mPhoneLocationBar:Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;

    cmpl-float v1, v0, v6

    if-nez v1, :cond_c

    move v1, v6

    :goto_5
    invoke-virtual {v11, v1}, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->setTranslationY(F)V

    .line 757
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mLocationBarBackgroundOffset:Landroid/graphics/Rect;

    iget-object v11, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mNtpSearchBoxBounds:Landroid/graphics/Rect;

    iget v11, v11, Landroid/graphics/Rect;->left:I

    iget-object v12, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUrlViewportBounds:Landroid/graphics/Rect;

    iget v12, v12, Landroid/graphics/Rect;->left:I

    sub-int/2addr v11, v12

    iget-object v12, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mPhoneLocationBar:Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;

    invoke-virtual {v12}, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->getPaddingLeft()I

    move-result v12

    sub-int/2addr v11, v12

    int-to-float v11, v11

    mul-float/2addr v11, v0

    float-to-int v11, v11

    neg-float v12, v10

    int-to-float v8, v8

    sub-float v8, v12, v8

    mul-float/2addr v8, v0

    float-to-int v8, v8

    iget-object v12, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mNtpSearchBoxBounds:Landroid/graphics/Rect;

    iget v12, v12, Landroid/graphics/Rect;->right:I

    iget-object v13, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUrlViewportBounds:Landroid/graphics/Rect;

    iget v13, v13, Landroid/graphics/Rect;->right:I

    sub-int/2addr v12, v13

    iget-object v13, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mPhoneLocationBar:Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;

    invoke-virtual {v13}, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->getPaddingRight()I

    move-result v13

    add-int/2addr v12, v13

    int-to-float v12, v12

    mul-float/2addr v12, v0

    float-to-int v12, v12

    int-to-float v9, v9

    sub-float v9, v10, v9

    mul-float/2addr v9, v0

    float-to-int v9, v9

    invoke-virtual {v1, v11, v8, v12, v9}, Landroid/graphics/Rect;->set(IIII)V

    .line 769
    const/high16 v1, 0x3f000000    # 0.5f

    cmpl-float v1, v0, v1

    if-ltz v1, :cond_d

    .line 770
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mPhoneLocationBar:Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;

    invoke-virtual {v0, v6}, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->setAlpha(F)V

    .line 777
    :goto_6
    const v0, 0x3ecccccd    # 0.4f

    cmpl-float v0, v7, v0

    if-ltz v0, :cond_e

    move v0, v4

    :goto_7
    iput v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUrlBackgroundAlpha:I

    .line 779
    cmpl-float v0, v7, v5

    if-nez v0, :cond_6

    iput v4, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUrlBackgroundAlpha:I

    .line 781
    :cond_6
    cmpl-float v0, v7, v6

    if-eqz v0, :cond_f

    :goto_8
    iput-boolean v3, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mForceDrawLocationBarBackground:Z

    goto/16 :goto_0

    :cond_7
    move v0, v2

    .line 715
    goto/16 :goto_1

    :cond_8
    move v1, v2

    .line 732
    goto/16 :goto_2

    :cond_9
    move v0, v2

    .line 736
    goto/16 :goto_3

    .line 744
    :cond_a
    const v0, 0x3ecccccd    # 0.4f

    cmpg-float v0, v7, v0

    if-gtz v0, :cond_b

    move v0, v5

    goto/16 :goto_4

    :cond_b
    sub-float v0, v5, v7

    const v1, 0x3fd55571

    mul-float/2addr v0, v1

    invoke-static {v5, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    goto/16 :goto_4

    .line 754
    :cond_c
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mNtpSearchBoxBounds:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    iget-object v12, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mPhoneLocationBar:Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;

    invoke-virtual {v12}, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->getTop()I

    move-result v12

    sub-int/2addr v1, v12

    int-to-float v1, v1

    add-float/2addr v1, v10

    invoke-static {v6, v1}, Ljava/lang/Math;->max(FF)F

    move-result v1

    goto/16 :goto_5

    .line 772
    :cond_d
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mPhoneLocationBar:Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;

    const/high16 v8, 0x40000000    # 2.0f

    mul-float/2addr v0, v8

    sub-float v0, v5, v0

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->setAlpha(F)V

    goto :goto_6

    .line 777
    :cond_e
    const/high16 v0, 0x40200000    # 2.5f

    mul-float/2addr v0, v7

    const/high16 v1, 0x437f0000    # 255.0f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    goto :goto_7

    :cond_f
    move v3, v2

    .line 781
    goto :goto_8
.end method

.method private updateToolbarBackground(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 523
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mToolbarBackground:Landroid/graphics/drawable/Drawable;

    .line 524
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->invalidate()V

    .line 525
    return-void
.end method

.method private updateUnfocusedLocationBarLayoutParams()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 418
    move v0, v1

    .line 419
    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mPhoneLocationBar:Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->getChildCount()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 420
    iget-object v2, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mPhoneLocationBar:Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 421
    iget-object v3, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUrlContainer:Lcom/google/android/apps/chrome/omnibox/UrlContainer;

    if-eq v2, v3, :cond_0

    .line 422
    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v2

    const/16 v3, 0x8

    if-eq v2, v3, :cond_2

    .line 423
    const/4 v1, 0x1

    .line 428
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->getViewBoundsLeftOfLocationBar()I

    move-result v0

    .line 429
    if-nez v1, :cond_1

    iget v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mToolbarSidePadding:I

    add-int/2addr v0, v1

    .line 430
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->getViewBoundsRightOfLocationBar()I

    move-result v1

    .line 435
    iget-object v2, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mPhoneLocationBar:Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;

    invoke-static {v2}, Lorg/chromium/base/ApiCompatibilityUtils;->isLayoutRtl(Landroid/view/View;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 436
    iget v2, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mToolbarSidePadding:I

    add-int/2addr v0, v2

    move v4, v1

    move v1, v0

    move v0, v4

    .line 441
    :goto_1
    sub-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUnfocusedLocationBarLayoutWidth:I

    .line 442
    iput v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUnfocusedLocationBarLayoutLeft:I

    .line 443
    return-void

    .line 419
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 438
    :cond_3
    iget v2, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mToolbarSidePadding:I

    sub-int/2addr v1, v2

    move v4, v1

    move v1, v0

    move v0, v4

    goto :goto_1
.end method


# virtual methods
.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 9

    .prologue
    const/high16 v8, 0x3f800000    # 1.0f

    const/4 v7, 0x0

    const/4 v2, 0x0

    .line 529
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mTextureCaptureMode:Z

    if-nez v0, :cond_0

    .line 533
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mPhoneLocationBar:Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->getTranslationY()F

    move-result v0

    cmpl-float v0, v0, v7

    if-lez v0, :cond_6

    .line 534
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mToolbarBackground:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->getWidth()I

    move-result v1

    iget-object v3, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mPhoneLocationBar:Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->getTranslationY()F

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->getHeight()I

    move-result v4

    int-to-float v4, v4

    add-float/2addr v3, v4

    iget-object v4, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mLocationBarBackgroundOffset:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    int-to-float v4, v4

    add-float/2addr v3, v4

    float-to-int v3, v3

    invoke-virtual {v0, v2, v2, v1, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 538
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mToolbarShadow:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mToolbarBackground:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->getHeight()I

    move-result v3

    sub-int/2addr v1, v3

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setTranslationY(F)V

    .line 545
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mToolbarBackground:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 548
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mLocationBarBackground:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mPhoneLocationBar:Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mTextureCaptureMode:Z

    if-eqz v0, :cond_4

    .line 552
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->getViewBoundsLeftOfLocationBar()I

    move-result v0

    .line 553
    invoke-direct {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->getViewBoundsRightOfLocationBar()I

    move-result v1

    .line 555
    iget-object v3, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUrlBackgroundPadding:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    sub-int/2addr v0, v3

    .line 556
    iget v3, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUrlFocusChangePercent:F

    cmpl-float v3, v3, v7

    if-eqz v3, :cond_2

    .line 557
    int-to-float v0, v0

    iget v3, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUrlFocusChangePercent:F

    sub-float v3, v8, v3

    mul-float/2addr v0, v3

    float-to-int v0, v0

    .line 558
    int-to-float v0, v0

    iget-object v3, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUrlBackgroundPadding:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    int-to-float v3, v3

    iget v4, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUrlFocusChangePercent:F

    mul-float/2addr v3, v4

    sub-float/2addr v0, v3

    float-to-int v0, v0

    .line 561
    :cond_2
    iget-object v3, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUrlBackgroundPadding:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    add-int/2addr v1, v3

    .line 562
    iget v3, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUrlFocusChangePercent:F

    cmpl-float v3, v3, v7

    if-eqz v3, :cond_3

    .line 563
    int-to-float v3, v1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->getWidth()I

    move-result v4

    sub-int v1, v4, v1

    int-to-float v1, v1

    iget v4, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUrlFocusChangePercent:F

    mul-float/2addr v1, v4

    add-float/2addr v1, v3

    float-to-int v1, v1

    .line 564
    int-to-float v1, v1

    iget-object v3, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUrlBackgroundPadding:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    int-to-float v3, v3

    iget v4, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUrlFocusChangePercent:F

    mul-float/2addr v3, v4

    add-float/2addr v1, v3

    float-to-int v1, v1

    .line 573
    :cond_3
    iget-object v3, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUrlViewportBounds:Landroid/graphics/Rect;

    iget-object v4, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mPhoneLocationBar:Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->getMeasuredHeight()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->getHeight()I

    move-result v5

    iget-object v6, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mPhoneLocationBar:Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;

    invoke-virtual {v6}, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->getMeasuredHeight()I

    move-result v6

    sub-int/2addr v5, v6

    iget-object v6, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUrlBackgroundPadding:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v5, v6

    iget-object v6, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUrlBackgroundPadding:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->top:I

    add-int/2addr v5, v6

    int-to-float v5, v5

    iget v6, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUrlFocusChangePercent:F

    mul-float/2addr v5, v6

    add-float/2addr v4, v5

    float-to-int v4, v4

    invoke-virtual {v3, v0, v2, v1, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 581
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUrlViewportBounds:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mPhoneLocationBar:Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->getY()F

    move-result v1

    iget v3, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUrlFocusChangePercent:F

    sub-float v3, v8, v3

    mul-float/2addr v1, v3

    iget-object v3, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUrlBackgroundPadding:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    int-to-float v3, v3

    iget v4, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUrlFocusChangePercent:F

    mul-float/2addr v3, v4

    sub-float/2addr v1, v3

    float-to-int v1, v1

    invoke-virtual {v0, v2, v1}, Landroid/graphics/Rect;->offset(II)V

    .line 586
    :cond_4
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mTextureCaptureMode:Z

    if-eqz v0, :cond_7

    .line 587
    invoke-direct {p0, p1, v7}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->drawOverviewAnimationOverlay(Landroid/graphics/Canvas;F)V

    .line 613
    :cond_5
    :goto_1
    return-void

    .line 542
    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mToolbarBackground:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->getHeight()I

    move-result v3

    invoke-virtual {v0, v2, v2, v1, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 543
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mToolbarShadow:Landroid/widget/ImageView;

    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setTranslationY(F)V

    goto/16 :goto_0

    .line 590
    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mOverviewModeAnimation:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_b

    .line 591
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mOverviewModeAnimation:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->isRunning()Z

    move-result v0

    if-nez v0, :cond_a

    const/4 v0, 0x1

    .line 595
    :goto_2
    iget-boolean v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mAnimateNormalToolbar:Z

    if-nez v1, :cond_8

    .line 596
    iget v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mOverviewModePercent:F

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->drawOverviewFadeAnimation(ZF)V

    .line 600
    :cond_8
    :goto_3
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 602
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mOverviewModeAnimation:Landroid/animation/ObjectAnimator;

    if-eqz v1, :cond_5

    .line 605
    iget-boolean v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mAnimateNormalToolbar:Z

    if-eqz v1, :cond_9

    .line 606
    iget v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mOverviewModePercent:F

    invoke-direct {p0, p1, v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->drawOverviewAnimationOverlay(Landroid/graphics/Canvas;F)V

    .line 610
    :cond_9
    if-eqz v0, :cond_5

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mOverviewModeAnimation:Landroid/animation/ObjectAnimator;

    goto :goto_1

    :cond_a
    move v0, v2

    .line 591
    goto :goto_2

    :cond_b
    move v0, v2

    goto :goto_3
.end method

.method public doInvalidate()V
    .locals 0

    .prologue
    .line 906
    invoke-static {p0}, Lorg/chromium/base/ApiCompatibilityUtils;->postInvalidateOnAnimation(Landroid/view/View;)V

    .line 907
    return-void
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 2

    .prologue
    .line 1073
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mTextureCaptureMode:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->getAlpha()F

    move-result v0

    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1074
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mTextureCaptureMode:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mClipRect:Landroid/graphics/Rect;

    if-eqz v0, :cond_1

    .line 1075
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 1076
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mClipRect:Landroid/graphics/Rect;

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;)Z

    .line 1078
    :cond_1
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->draw(Landroid/graphics/Canvas;)V

    .line 1079
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mTextureCaptureMode:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mClipRect:Landroid/graphics/Rect;

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 1080
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mToolbarDelegate:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->recordFirstDrawTime()V

    .line 1081
    return-void
.end method

.method protected drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 933
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mPhoneLocationBar:Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;

    if-ne p2, v0, :cond_1

    invoke-direct {p0, p1, p3, p4}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->drawLocationBar(Landroid/graphics/Canvas;J)Z

    move-result v0

    .line 975
    :cond_0
    :goto_0
    return v0

    .line 936
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mLocationBarBackground:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_6

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mOverviewMode:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mOverviewModeViews:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mOverviewMode:Z

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mBrowsingModeViews:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 939
    :cond_3
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 940
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUrlFocusChangeInProgress:Z

    if-eqz v0, :cond_5

    iget v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUrlFocusChangePercent:F

    cmpl-float v0, v0, v6

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUrlViewportBounds:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    invoke-virtual {p2}, Landroid/view/View;->getBottom()I

    move-result v3

    if-ge v0, v3, :cond_5

    .line 948
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mNewTabButton:Lcom/google/android/apps/chrome/widget/newtab/NewTabButton;

    if-eq p2, v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mHomeButton:Landroid/widget/ImageButton;

    if-ne p2, v0, :cond_7

    :cond_4
    move v0, v2

    :goto_1
    invoke-static {}, Lorg/chromium/ui/base/LocalizationUtils;->isLayoutRtl()Z

    move-result v3

    xor-int v5, v0, v3

    .line 951
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUrlViewportBounds:Landroid/graphics/Rect;

    iget v3, v0, Landroid/graphics/Rect;->top:I

    .line 952
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUrlViewportBounds:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    .line 954
    iget-object v4, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mPhoneLocationBar:Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->getTranslationY()F

    move-result v4

    cmpl-float v4, v4, v6

    if-lez v4, :cond_b

    .line 955
    invoke-virtual {p2}, Landroid/view/View;->getTop()I

    move-result v3

    .line 956
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUrlViewportBounds:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move v4, v3

    move v3, v0

    move v0, v2

    .line 960
    :goto_2
    if-eqz v5, :cond_9

    .line 961
    if-eqz v0, :cond_8

    invoke-virtual {p2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    :goto_3
    invoke-virtual {p1, v1, v4, v0, v3}, Landroid/graphics/Canvas;->clipRect(IIII)Z

    :cond_5
    :goto_4
    move v1, v2

    .line 973
    :cond_6
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/FrameLayout;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    move-result v0

    .line 974
    if-eqz v1, :cond_0

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    goto :goto_0

    :cond_7
    move v0, v1

    .line 948
    goto :goto_1

    .line 961
    :cond_8
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUrlViewportBounds:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    goto :goto_3

    .line 966
    :cond_9
    if-eqz v0, :cond_a

    :goto_5
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->getMeasuredWidth()I

    move-result v0

    invoke-virtual {p1, v1, v4, v0, v3}, Landroid/graphics/Canvas;->clipRect(IIII)Z

    goto :goto_4

    :cond_a
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUrlViewportBounds:Landroid/graphics/Rect;

    iget v1, v0, Landroid/graphics/Rect;->right:I

    goto :goto_5

    :cond_b
    move v4, v3

    move v3, v0

    move v0, v1

    goto :goto_2
.end method

.method public getDelegate()Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;
    .locals 1

    .prologue
    .line 1055
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mToolbarDelegate:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;

    return-object v0
.end method

.method public getView()Landroid/view/View;
    .locals 0

    .prologue
    .line 1085
    return-object p0
.end method

.method protected onAttachedToWindow()V
    .locals 2

    .prologue
    .line 1060
    invoke-super {p0}, Landroid/widget/FrameLayout;->onAttachedToWindow()V

    .line 1061
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->getRootView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$id;->toolbar_shadow:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mToolbarShadow:Landroid/widget/ImageView;

    .line 1063
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 1064
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mProgressBar:Lcom/google/android/apps/chrome/widget/SmoothProgressBar;

    invoke-static {v0, v1, p0}, Lorg/chromium/ui/UiUtils;->insertAfter(Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;)I

    move-result v0

    .line 1065
    sget-boolean v1, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    if-gez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1066
    :cond_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 329
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mToggleTabStackButton:Landroid/widget/ImageView;

    if-ne v0, p1, :cond_1

    .line 332
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mToggleTabStackButton:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mToggleTabStackButton:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->isClickable()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mOverviewListener:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_0

    .line 334
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mOverviewListener:Landroid/view/View$OnClickListener;

    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mToggleTabStackButton:Landroid/widget/ImageView;

    invoke-interface {v0, v1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 335
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->toolbarPhoneTabStack()V

    .line 370
    :cond_0
    :goto_0
    return-void

    .line 337
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mNewTabButton:Lcom/google/android/apps/chrome/widget/newtab/NewTabButton;

    if-ne v0, p1, :cond_2

    .line 338
    invoke-virtual {p1, v4}, Landroid/view/View;->setEnabled(Z)V

    .line 340
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mNewTabListener:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_0

    .line 341
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mNewTabListener:Landroid/view/View$OnClickListener;

    invoke-interface {v0, p1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 342
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mNewTabButton:Lcom/google/android/apps/chrome/widget/newtab/NewTabButton;

    if-ne p1, v0, :cond_0

    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->stackViewNewTab()V

    goto :goto_0

    .line 345
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mHomeButton:Landroid/widget/ImageButton;

    if-ne v0, p1, :cond_3

    .line 346
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mToolbarDelegate:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->openHomepage()V

    goto :goto_0

    .line 347
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mReaderModeButton:Landroid/widget/ImageButton;

    if-ne v0, p1, :cond_0

    .line 348
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->getDelegate()Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->getToolbarDataProvider()Lcom/google/android/apps/chrome/toolbar/ToolbarDataProvider;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarDataProvider;->getTab()Lorg/chromium/chrome/browser/Tab;

    move-result-object v0

    .line 349
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/Tab;->getWebContents()Lorg/chromium/content_public/browser/WebContents;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 350
    invoke-virtual {v0}, Lorg/chromium/chrome/browser/Tab;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lorg/chromium/components/dom_distiller/core/DomDistillerUrlUtils;->isDistilledPage(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 351
    invoke-virtual {v0}, Lorg/chromium/chrome/browser/Tab;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lorg/chromium/components/dom_distiller/core/DomDistillerUrlUtils;->getOriginalUrlFromDistillerUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 353
    invoke-virtual {v0, v4, v5}, Lorg/chromium/chrome/browser/Tab;->getDirectedNavigationHistory(ZI)Lorg/chromium/content_public/browser/NavigationHistory;

    move-result-object v2

    .line 354
    invoke-virtual {v2}, Lorg/chromium/content_public/browser/NavigationHistory;->getEntryCount()I

    move-result v3

    if-ne v3, v5, :cond_4

    invoke-virtual {v2, v4}, Lorg/chromium/content_public/browser/NavigationHistory;->getEntryAtIndex(I)Lorg/chromium/content_public/browser/NavigationEntry;

    move-result-object v2

    invoke-virtual {v2}, Lorg/chromium/content_public/browser/NavigationEntry;->getUrl()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 356
    invoke-virtual {v0}, Lorg/chromium/chrome/browser/Tab;->goBack()V

    .line 361
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mReaderModeButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setSelected(Z)V

    goto :goto_0

    .line 358
    :cond_4
    new-instance v2, Lorg/chromium/content_public/browser/LoadUrlParams;

    const/4 v3, 0x6

    invoke-direct {v2, v1, v3}, Lorg/chromium/content_public/browser/LoadUrlParams;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v0, v2}, Lorg/chromium/chrome/browser/Tab;->loadUrl(Lorg/chromium/content_public/browser/LoadUrlParams;)I

    goto :goto_1

    .line 363
    :cond_5
    invoke-virtual {v0}, Lorg/chromium/chrome/browser/Tab;->getWebContents()Lorg/chromium/content_public/browser/WebContents;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/chrome/browser/dom_distiller/DomDistillerTabUtils;->distillCurrentPageAndView(Lorg/chromium/content_public/browser/WebContents;)V

    .line 365
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->readerModeEntered()V

    .line 366
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mReaderModeButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v5}, Landroid/widget/ImageButton;->setSelected(Z)V

    goto/16 :goto_0
.end method

.method public onFinishInflate()V
    .locals 9

    .prologue
    const/16 v1, 0x8

    const/high16 v8, -0x80000000

    const/4 v2, 0x0

    .line 209
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 211
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->getContext()Landroid/content/Context;

    move-result-object v3

    .line 213
    new-instance v0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;-><init>(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mToolbarDelegate:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;

    .line 214
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->getDelegate()Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->getLocationBar()Lcom/google/android/apps/chrome/omnibox/LocationBar;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;

    iput-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mPhoneLocationBar:Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;

    .line 215
    sget v0, Lcom/google/android/apps/chrome/R$id;->toolbar_buttons:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mToolbarButtonsContainer:Landroid/view/View;

    .line 217
    sget v0, Lcom/google/android/apps/chrome/R$id;->tab_switcher_button:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mToggleTabStackButton:Landroid/widget/ImageView;

    .line 218
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mToggleTabStackButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setClickable(Z)V

    .line 219
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mToggleTabStackButton:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 220
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mToggleTabStackButton:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 221
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mToggleTabStackButton:Landroid/widget/ImageView;

    new-instance v4, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$3;

    invoke-direct {v4, p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$3;-><init>(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)V

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 236
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 237
    invoke-static {v0, v2}, Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;->createTabSwitcherDrawable(Landroid/content/res/Resources;Z)Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mTabSwitcherButtonDrawable:Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;

    .line 239
    const/4 v4, 0x1

    invoke-static {v0, v4}, Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;->createTabSwitcherDrawable(Landroid/content/res/Resources;Z)Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mTabSwitcherButtonDrawableLight:Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;

    .line 242
    iget-object v4, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mToggleTabStackButton:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/utilities/FeatureUtilitiesInternal;->isDocumentMode(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {v4, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 244
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mToggleTabStackButton:Landroid/widget/ImageView;

    iget-object v4, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mTabSwitcherButtonDrawable:Lcom/google/android/apps/chrome/toolbar/TabSwitcherDrawable;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 246
    sget v0, Lcom/google/android/apps/chrome/R$id;->new_tab_button:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/widget/newtab/NewTabButton;

    iput-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mNewTabButton:Lcom/google/android/apps/chrome/widget/newtab/NewTabButton;

    .line 247
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mNewTabButton:Lcom/google/android/apps/chrome/widget/newtab/NewTabButton;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/chrome/widget/newtab/NewTabButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 248
    sget v0, Lcom/google/android/apps/chrome/R$id;->home_button:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mHomeButton:Landroid/widget/ImageButton;

    .line 249
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mHomeButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 250
    sget v0, Lcom/google/android/apps/chrome/R$id;->reader_mode_button:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mReaderModeButton:Landroid/widget/ImageButton;

    .line 251
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mReaderModeButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 253
    sget v0, Lcom/google/android/apps/chrome/R$id;->url_bar:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUrlBar:Landroid/widget/TextView;

    .line 254
    sget v0, Lcom/google/android/apps/chrome/R$id;->url_container:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/omnibox/UrlContainer;

    iput-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUrlContainer:Lcom/google/android/apps/chrome/omnibox/UrlContainer;

    .line 256
    sget v0, Lcom/google/android/apps/chrome/R$id;->menu_button:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mMenuButton:Landroid/widget/ImageView;

    .line 257
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mMenuButton:Landroid/widget/ImageView;

    new-instance v4, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$4;

    invoke-direct {v4, p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$4;-><init>(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)V

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 274
    sget v0, Lcom/google/android/apps/chrome/R$id;->url_action_container:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUrlActionsContainer:Landroid/view/View;

    .line 276
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mOverviewModeViews:Ljava/util/List;

    iget-object v4, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mNewTabButton:Lcom/google/android/apps/chrome/widget/newtab/NewTabButton;

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 277
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mBrowsingModeViews:Ljava/util/List;

    iget-object v4, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mPhoneLocationBar:Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 279
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v4, Lcom/google/android/apps/chrome/R$drawable;->toolbar_normal:I

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->updateToolbarBackground(Landroid/graphics/drawable/Drawable;)V

    .line 281
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v4, Lcom/google/android/apps/chrome/R$drawable;->textbox:I

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mLocationBarBackground:Landroid/graphics/drawable/Drawable;

    .line 282
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mLocationBarBackground:Landroid/graphics/drawable/Drawable;

    iget-object v4, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUrlBackgroundPadding:Landroid/graphics/Rect;

    invoke-virtual {v0, v4}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    .line 283
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mPhoneLocationBar:Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;

    iget-object v4, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUrlBackgroundPadding:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->left:I

    iget-object v5, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUrlBackgroundPadding:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->top:I

    iget-object v6, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUrlBackgroundPadding:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->right:I

    iget-object v7, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUrlBackgroundPadding:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v0, v4, v5, v6, v7}, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->setPadding(IIII)V

    .line 287
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->setLayoutTransition(Landroid/animation/LayoutTransition;)V

    .line 289
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mToolbarDelegate:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;

    iget-object v0, v0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->mMenuButton:Landroid/widget/ImageButton;

    iget-object v4, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mToolbarDelegate:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->shouldShowMenuButton()Z

    move-result v4

    if-eqz v4, :cond_0

    move v1, v2

    :cond_0
    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 295
    const-string/jumbo v0, "window"

    invoke-virtual {v3, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 296
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    .line 297
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 299
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mToolbarButtonsContainer:Landroid/view/View;

    iget v3, v1, Landroid/graphics/Point;->x:I

    invoke-static {v3, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    iget v1, v1, Landroid/graphics/Point;->y:I

    invoke-static {v1, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-virtual {v0, v3, v1}, Landroid/view/View;->measure(II)V

    .line 302
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mNewTabButton:Lcom/google/android/apps/chrome/widget/newtab/NewTabButton;

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->getFrameLayoutParams(Landroid/view/View;)Landroid/widget/FrameLayout$LayoutParams;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mToolbarButtonsContainer:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    invoke-static {v0, v1}, Lorg/chromium/base/ApiCompatibilityUtils;->setMarginEnd(Landroid/view/ViewGroup$MarginLayoutParams;I)V

    .line 305
    sget v0, Lcom/google/android/apps/chrome/R$id;->progress:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/widget/SmoothProgressBar;

    iput-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mProgressBar:Lcom/google/android/apps/chrome/widget/SmoothProgressBar;

    .line 306
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mProgressBar:Lcom/google/android/apps/chrome/widget/SmoothProgressBar;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->removeView(Landroid/view/View;)V

    .line 307
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mProgressBar:Lcom/google/android/apps/chrome/widget/SmoothProgressBar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/SmoothProgressBar;->getProgressDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 308
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mProgressBar:Lcom/google/android/apps/chrome/widget/SmoothProgressBar;

    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->getFrameLayoutParams(Landroid/view/View;)Landroid/widget/FrameLayout$LayoutParams;

    move-result-object v1

    iget v3, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mToolbarHeightWithoutShadow:I

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    sub-int v0, v3, v0

    iput v0, v1, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 311
    invoke-virtual {p0, v2}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->setWillNotDraw(Z)V

    .line 312
    return-void

    :cond_1
    move v0, v2

    .line 242
    goto/16 :goto_0
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    .line 319
    iget v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mNtpSearchBoxScrollPercent:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mNtpSearchBoxScrollPercent:F

    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mNtpSearchBoxScrollPercent:F

    const/high16 v1, -0x40800000    # -1.0f

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    .line 322
    const/4 v0, 0x1

    .line 324
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onLongClick(Landroid/view/View;)Z
    .locals 7

    .prologue
    const/4 v2, -0x1

    const/4 v0, 0x0

    .line 374
    .line 375
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mToggleTabStackButton:Landroid/widget/ImageView;

    if-ne p1, v1, :cond_1

    .line 376
    sget v1, Lcom/google/android/apps/chrome/R$string;->open_tabs:I

    .line 382
    :goto_0
    if-eq v1, v2, :cond_0

    .line 383
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 385
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 386
    const/4 v4, 0x2

    new-array v4, v4, [I

    .line 387
    invoke-virtual {p1, v4}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 388
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v5

    .line 390
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    .line 392
    const v2, 0x800035

    aget v0, v4, v0

    sub-int v0, v3, v0

    div-int/lit8 v3, v5, 0x2

    sub-int/2addr v0, v3

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->getHeight()I

    move-result v3

    invoke-virtual {v1, v2, v0, v3}, Landroid/widget/Toast;->setGravity(III)V

    .line 396
    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 397
    const/4 v0, 0x1

    .line 399
    :cond_0
    return v0

    .line 377
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mHomeButton:Landroid/widget/ImageButton;

    if-ne p1, v1, :cond_2

    .line 378
    sget v1, Lcom/google/android/apps/chrome/R$string;->button_home:I

    goto :goto_0

    .line 379
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mReaderModeButton:Landroid/widget/ImageButton;

    if-ne p1, v1, :cond_3

    .line 380
    sget v1, Lcom/google/android/apps/chrome/R$string;->accessibility_toolbar_btn_reader_mode:I

    goto :goto_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 2

    .prologue
    .line 404
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mDisableLocationBarRelayout:Z

    if-nez v0, :cond_0

    .line 405
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onMeasure(II)V

    .line 407
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->layoutLocationBar(I)Z

    move-result v0

    .line 408
    iget v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUrlFocusChangePercent:F

    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->setUrlFocusChangePercent(F)V

    .line 409
    if-nez v0, :cond_1

    .line 415
    :goto_0
    return-void

    .line 411
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->updateUnfocusedLocationBarLayoutParams()V

    .line 414
    :cond_1
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onMeasure(II)V

    goto :goto_0
.end method

.method public onScrollChanged(F)V
    .locals 2

    .prologue
    .line 618
    iget v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mNtpSearchBoxScrollPercent:F

    cmpl-float v0, p1, v0

    if-nez v0, :cond_1

    .line 629
    :cond_0
    :goto_0
    return-void

    .line 620
    :cond_1
    iput p1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mNtpSearchBoxScrollPercent:F

    .line 622
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mToolbarDelegate:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->getToolbarDataProvider()Lcom/google/android/apps/chrome/toolbar/ToolbarDataProvider;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarDataProvider;->getNewTabPageForCurrentTab()Lcom/google/android/apps/chrome/ntp/NewTabPage;

    move-result-object v0

    .line 623
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mToolbarDelegate:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;

    # invokes: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->isLocationBarShownInNTP()Z
    invoke-static {v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->access$600(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 625
    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mLocationBarBackgroundOffset:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->setEmpty()V

    .line 626
    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->updateNtpTransitionAnimation(Lcom/google/android/apps/chrome/ntp/NewTabPage;)V

    .line 628
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->invalidate()V

    goto :goto_0
.end method

.method protected onSizeChanged(IIII)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1048
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mBackgroundOverlayBounds:Landroid/graphics/Rect;

    iget v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mToolbarHeightWithoutShadow:I

    invoke-virtual {v0, v2, v2, p1, v1}, Landroid/graphics/Rect;->set(IIII)V

    .line 1049
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mOverviewAnimationBgOverlay:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mOverviewAnimationBgOverlay:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v2, v2, p1, p2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1050
    :cond_0
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/FrameLayout;->onSizeChanged(IIII)V

    .line 1051
    return-void
.end method
