.class public Lcom/google/android/apps/chrome/document/DocumentActivity;
.super Lcom/google/android/apps/chrome/CompositorChromeActivity;
.source "DocumentActivity.java"

# interfaces
.implements Lcom/google/android/apps/chrome/sync/ui/SyncChooseAccountFragment$Listener;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final APP_ICON_DEFAULT_BACKGROUND_COLOR:I

.field static final PREF_LAST_USED_TAB_ID:Ljava/lang/String; = "pref_last_used_tab_id"

.field public static final TAG:Ljava/lang/String; = "DocumentActivity"

.field static sLastUsedTabId:I

.field private static sNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;


# instance fields
.field private mAppMenuHandler:Lorg/chromium/chrome/browser/appmenu/AppMenuHandler;

.field private mChromeAppMenuPropertiesDelegate:Lcom/google/android/apps/chrome/ChromeAppMenuPropertiesDelegate;

.field private mContextReporter:Lcom/google/android/apps/chrome/icing/ContextReporter;

.field private final mCurrentLocale:Ljava/util/Locale;

.field private mDocumentAppIconGenerator:Lcom/google/android/apps/chrome/widget/TabIconGenerator;

.field private final mDocumentDelegate:Lcom/google/android/apps/chrome/document/DocumentActivity$TitleBarDelegate;

.field protected mDocumentTab:Lcom/google/android/apps/chrome/document/DocumentTab;

.field private mDocumentToolbarHelper:Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;

.field protected mDocumentUtilities:Lcom/google/android/apps/chrome/utilities/DocumentUtilities;

.field private mFindToolbarWrapper:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarWrapper;

.field private mIcon:Landroid/graphics/Bitmap;

.field private mRecordedStartupUma:Z

.field private mSnapshotDownloadBroadcastReceiver:Lcom/google/android/apps/chrome/snapshot/SnapshotStateBroadcastReceiver;

.field private mSsbServiceClient:Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient;

.field private mTabInitializationObserver:Lcom/google/android/apps/chrome/document/DocumentTabList$InitializationObserver;

.field private mTabList:Lcom/google/android/apps/chrome/document/DocumentTabList;

.field private mThemeColor:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/16 v1, 0x32

    .line 103
    const-class v0, Lcom/google/android/apps/chrome/document/DocumentActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/document/DocumentActivity;->$assertionsDisabled:Z

    .line 109
    const/4 v0, -0x1

    sput v0, Lcom/google/android/apps/chrome/document/DocumentActivity;->sLastUsedTabId:I

    .line 114
    invoke-static {v1, v1, v1}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    sput v0, Lcom/google/android/apps/chrome/document/DocumentActivity;->APP_ICON_DEFAULT_BACKGROUND_COLOR:I

    return-void

    .line 103
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 181
    invoke-direct {p0}, Lcom/google/android/apps/chrome/CompositorChromeActivity;-><init>()V

    .line 141
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mCurrentLocale:Ljava/util/Locale;

    .line 182
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->createTitleBarDelegate()Lcom/google/android/apps/chrome/document/DocumentActivity$TitleBarDelegate;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mDocumentDelegate:Lcom/google/android/apps/chrome/document/DocumentActivity$TitleBarDelegate;

    .line 183
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/document/DocumentActivity;)Z
    .locals 1

    .prologue
    .line 104
    invoke-direct {p0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->isNewTabPage()Z

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/document/DocumentActivity;)Lcom/google/android/apps/chrome/document/DocumentTabList;
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mTabList:Lcom/google/android/apps/chrome/document/DocumentTabList;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/document/DocumentActivity;)V
    .locals 0

    .prologue
    .line 104
    invoke-direct {p0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->initializeUI()V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/apps/chrome/document/DocumentActivity;)Lcom/google/android/apps/chrome/ChromeAppMenuPropertiesDelegate;
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mChromeAppMenuPropertiesDelegate:Lcom/google/android/apps/chrome/ChromeAppMenuPropertiesDelegate;

    return-object v0
.end method

.method static synthetic access$402(Lcom/google/android/apps/chrome/document/DocumentActivity;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 0

    .prologue
    .line 104
    iput-object p1, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mIcon:Landroid/graphics/Bitmap;

    return-object p1
.end method

.method static synthetic access$500(Lcom/google/android/apps/chrome/document/DocumentActivity;)V
    .locals 0

    .prologue
    .line 104
    invoke-direct {p0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->updateTaskDescription()V

    return-void
.end method

.method static synthetic access$602(Lcom/google/android/apps/chrome/document/DocumentActivity;Ljava/lang/Integer;)Ljava/lang/Integer;
    .locals 0

    .prologue
    .line 104
    iput-object p1, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mThemeColor:Ljava/lang/Integer;

    return-object p1
.end method

.method private addOrEditBookmark()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 906
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mDocumentTab:Lcom/google/android/apps/chrome/document/DocumentTab;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mDocumentTab:Lcom/google/android/apps/chrome/document/DocumentTab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/document/DocumentTab;->isFrozen()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 925
    :cond_0
    :goto_0
    return-void

    .line 910
    :cond_1
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/chrome/bookmark/ManageBookmarkActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 913
    iget-object v1, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mDocumentTab:Lcom/google/android/apps/chrome/document/DocumentTab;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/document/DocumentTab;->getUserBookmarkId()J

    move-result-wide v2

    .line 915
    const-wide/16 v4, -0x1

    cmp-long v1, v2, v4

    if-nez v1, :cond_2

    .line 916
    const-string/jumbo v1, "folder"

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 917
    const-string/jumbo v1, "title"

    iget-object v2, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mDocumentTab:Lcom/google/android/apps/chrome/document/DocumentTab;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/document/DocumentTab;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 918
    const-string/jumbo v1, "url"

    iget-object v2, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mDocumentTab:Lcom/google/android/apps/chrome/document/DocumentTab;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/document/DocumentTab;->getUrl()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 924
    :goto_1
    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 920
    :cond_2
    const-string/jumbo v1, "folder"

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 921
    const-string/jumbo v1, "_id"

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    goto :goto_1
.end method

.method private connectAndPrepareSsbOverlay()V
    .locals 3

    .prologue
    .line 1024
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mSsbServiceClient:Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/chrome/document/ssb/ChromeSsbServiceCallback;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/document/ssb/ChromeSsbServiceCallback;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/document/ssb/ChromeSsbServiceCallback;->getCallback()Lcom/google/android/apps/chrome/document/ssb/SsbServiceCallback;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient;->connect(Lcom/google/android/apps/chrome/document/ssb/SsbServiceCallback;)Z

    .line 1027
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->getCurrentTab()Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v2

    .line 1028
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getTitle()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 1029
    :goto_0
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getUrl()Ljava/lang/String;

    move-result-object v0

    .line 1031
    :goto_1
    iget-object v2, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mSsbServiceClient:Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient;

    invoke-static {p0, v0, v1}, Lcom/google/android/apps/chrome/icing/SsbContextHelper;->getContextStubForSsb(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/c/f;

    move-result-object v0

    invoke-static {v0}, Lcom/google/protobuf/nano/MessageNano;->toByteArray(Lcom/google/protobuf/nano/MessageNano;)[B

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient;->prepareOverlay([B)V

    .line 1033
    return-void

    .line 1028
    :cond_0
    const-string/jumbo v0, ""

    move-object v1, v0

    goto :goto_0

    .line 1029
    :cond_1
    const-string/jumbo v0, ""

    goto :goto_1
.end method

.method private handleDocumentUma()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 370
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mRecordedStartupUma:Z

    if-eqz v0, :cond_0

    .line 371
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/apps/chrome/document/DocumentUma;->recordStartedBy(I)V

    .line 382
    :goto_0
    invoke-static {v2}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->recordDocumentMode(Z)V

    .line 383
    return-void

    .line 374
    :cond_0
    iput-boolean v2, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mRecordedStartupUma:Z

    .line 376
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mSavedInstanceState:Landroid/os/Bundle;

    if-nez v0, :cond_1

    .line 377
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/document/DocumentUma;->recordStartedBy(Ljava/lang/String;Landroid/content/Intent;)V

    goto :goto_0

    .line 379
    :cond_1
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/apps/chrome/document/DocumentUma;->recordStartedBy(I)V

    goto :goto_0
.end method

.method private initializeDocumentToolbarHelper()V
    .locals 4

    .prologue
    .line 755
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mDocumentToolbarHelper:Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;

    iget-object v1, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mAppMenuHandler:Lorg/chromium/chrome/browser/appmenu/AppMenuHandler;

    iget-object v2, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mDocumentDelegate:Lcom/google/android/apps/chrome/document/DocumentActivity$TitleBarDelegate;

    iget-object v3, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mDocumentDelegate:Lcom/google/android/apps/chrome/document/DocumentActivity$TitleBarDelegate;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;->initializeControls(Lorg/chromium/chrome/browser/appmenu/AppMenuHandler;Lcom/google/android/apps/chrome/document/TitleBar$DocumentDelegate;Lcom/google/android/apps/chrome/document/TitleBar$TitleBarListener;)V

    .line 757
    return-void
.end method

.method private initializeUI()V
    .locals 7

    .prologue
    .line 621
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/IntentHandler;->getTabIdFromIntent(Landroid/content/Intent;)I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/document/DocumentActivityDatabase;->removePendingDocumentData(I)Lcom/google/android/apps/chrome/document/DocumentActivityDatabase$PendingDocumentData;

    move-result-object v6

    .line 623
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->isIncognito()Z

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->getWindowAndroid()Lorg/chromium/ui/base/WindowAndroid;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->determineLastKnownUrl()Ljava/lang/String;

    move-result-object v3

    if-eqz v6, :cond_3

    iget-wide v4, v6, Lcom/google/android/apps/chrome/document/DocumentActivityDatabase$PendingDocumentData;->nativeWebContents:J

    :goto_0
    move-object v0, p0

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/chrome/document/DocumentTab;->create(Lcom/google/android/apps/chrome/document/DocumentActivity;ZLorg/chromium/ui/base/WindowAndroid;Ljava/lang/String;J)Lcom/google/android/apps/chrome/document/DocumentTab;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mDocumentTab:Lcom/google/android/apps/chrome/document/DocumentTab;

    .line 626
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mDocumentTab:Lcom/google/android/apps/chrome/document/DocumentTab;

    iget-object v1, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mTabList:Lcom/google/android/apps/chrome/document/DocumentTabList;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/document/DocumentTabList;->getSessionId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/document/DocumentTab;->setWindowSessionId(I)V

    .line 628
    new-instance v0, Lcom/google/android/apps/chrome/ChromeAppMenuPropertiesDelegate;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/ChromeAppMenuPropertiesDelegate;-><init>(Lcom/google/android/apps/chrome/ChromeActivity;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mChromeAppMenuPropertiesDelegate:Lcom/google/android/apps/chrome/ChromeAppMenuPropertiesDelegate;

    .line 629
    new-instance v0, Lorg/chromium/chrome/browser/appmenu/AppMenuHandler;

    iget-object v1, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mChromeAppMenuPropertiesDelegate:Lcom/google/android/apps/chrome/ChromeAppMenuPropertiesDelegate;

    sget v2, Lcom/google/android/apps/chrome/R$menu;->main_menu:I

    invoke-direct {v0, p0, v1, v2}, Lorg/chromium/chrome/browser/appmenu/AppMenuHandler;-><init>(Landroid/app/Activity;Lorg/chromium/chrome/browser/appmenu/AppMenuPropertiesDelegate;I)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mAppMenuHandler:Lorg/chromium/chrome/browser/appmenu/AppMenuHandler;

    .line 632
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mAppMenuHandler:Lorg/chromium/chrome/browser/appmenu/AppMenuHandler;

    new-instance v1, Lcom/google/android/apps/chrome/document/DocumentActivity$5;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/document/DocumentActivity$5;-><init>(Lcom/google/android/apps/chrome/document/DocumentActivity;)V

    invoke-virtual {v0, v1}, Lorg/chromium/chrome/browser/appmenu/AppMenuHandler;->addObserver(Lorg/chromium/chrome/browser/appmenu/AppMenuObserver;)V

    .line 641
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->getTabModelSelector()Lcom/google/android/apps/chrome/tabmodel/SingleTabModelSelector;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mDocumentTab:Lcom/google/android/apps/chrome/document/DocumentTab;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/tabmodel/SingleTabModelSelector;->setTab(Lorg/chromium/chrome/browser/Tab;)V

    .line 643
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mDocumentTab:Lcom/google/android/apps/chrome/document/DocumentTab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/document/DocumentTab;->didRestoreState()Z

    move-result v0

    if-nez v0, :cond_1

    .line 644
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mDocumentTab:Lcom/google/android/apps/chrome/document/DocumentTab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/document/DocumentTab;->isCreatedWithWebContents()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0, v6}, Lcom/google/android/apps/chrome/document/DocumentActivity;->loadLastKnownUrl(Lcom/google/android/apps/chrome/document/DocumentActivityDatabase$PendingDocumentData;)V

    .line 645
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mDocumentTab:Lcom/google/android/apps/chrome/document/DocumentTab;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "com.android.chrome.preserve_task"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/document/DocumentTab;->setShouldPreserve(Z)V

    .line 649
    :cond_1
    new-instance v1, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->getCompositorViewHolder()Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->getCompositorViewHolder()Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;-><init>(Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;Lcom/google/android/apps/chrome/eventfilter/EventFilterHost;)V

    sget v0, Lcom/google/android/apps/chrome/R$id;->url_bar:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    const v0, 0x1020002

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    sget v0, Lcom/google/android/apps/chrome/R$id;->control_container:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/chrome/widget/ControlContainer;

    sget v5, Lcom/google/android/apps/chrome/R$dimen;->document_titlebar_height:I

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/chrome/document/DocumentActivity;->initializeCompositorContent(Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;Landroid/view/View;Landroid/view/ViewGroup;Lcom/google/android/apps/chrome/widget/ControlContainer;I)V

    .line 655
    invoke-direct {p0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->initializeDocumentToolbarHelper()V

    .line 657
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mDocumentTab:Lcom/google/android/apps/chrome/document/DocumentTab;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->getFullscreenManager()Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/document/DocumentTab;->setFullscreenManager(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;)V

    .line 659
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mDocumentTab:Lcom/google/android/apps/chrome/document/DocumentTab;

    new-instance v1, Lcom/google/android/apps/chrome/document/DocumentActivity$6;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/document/DocumentActivity$6;-><init>(Lcom/google/android/apps/chrome/document/DocumentActivity;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/document/DocumentTab;->addObserver(Lorg/chromium/chrome/browser/TabObserver;)V

    .line 737
    new-instance v0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarWrapper;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->getTabModelSelector()Lcom/google/android/apps/chrome/tabmodel/SingleTabModelSelector;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mDocumentToolbarHelper:Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;->getContextualMenuBar()Lcom/google/android/apps/chrome/ContextualMenuBar;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/ContextualMenuBar;->getCustomSelectionActionModeCallback()Lcom/google/android/apps/chrome/CustomSelectionActionModeCallback;

    move-result-object v2

    sget v4, Lcom/google/android/apps/chrome/R$id;->find_toolbar_stub:I

    sget v5, Lcom/google/android/apps/chrome/R$id;->find_toolbar_tablet_stub:I

    move-object v3, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarWrapper;-><init>(Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;Landroid/view/ActionMode$Callback;Landroid/app/Activity;II)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mFindToolbarWrapper:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarWrapper;

    .line 742
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mFindToolbarWrapper:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarWrapper;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarWrapper;->initialize()V

    .line 744
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->removeWindowBackground()V

    .line 746
    invoke-direct {p0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->initializeWithTab()V

    .line 748
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mDocumentTab:Lcom/google/android/apps/chrome/document/DocumentTab;

    if-eqz v0, :cond_2

    .line 749
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mDocumentTab:Lcom/google/android/apps/chrome/document/DocumentTab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/document/DocumentTab;->getWebContents()Lorg/chromium/content_public/browser/WebContents;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthReductionPreferences;->launchDataReductionSSLInfoBar(Landroid/content/Context;Lorg/chromium/content_public/browser/WebContents;)V

    .line 752
    :cond_2
    return-void

    .line 623
    :cond_3
    const-wide/16 v4, 0x0

    goto/16 :goto_0
.end method

.method private initializeWithTab()V
    .locals 2

    .prologue
    .line 441
    invoke-static {}, Lcom/google/android/apps/chrome/document/DocumentManager;->getInstance()Lcom/google/android/apps/chrome/document/DocumentManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mDocumentTab:Lcom/google/android/apps/chrome/document/DocumentTab;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/document/DocumentTab;->getProfile()Lorg/chromium/chrome/browser/profiles/Profile;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/document/DocumentManager;->setProfile(Lorg/chromium/chrome/browser/profiles/Profile;)V

    .line 443
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mDocumentTab:Lcom/google/android/apps/chrome/document/DocumentTab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/document/DocumentTab;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->registerForContextMenu(Landroid/view/View;)V

    .line 444
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mDocumentToolbarHelper:Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;->inflateControls()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 445
    invoke-direct {p0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->initializeDocumentToolbarHelper()V

    .line 447
    sget v0, Lcom/google/android/apps/chrome/R$id;->control_container:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/widget/ControlContainer;

    .line 449
    sget-boolean v1, Lcom/google/android/apps/chrome/document/DocumentActivity;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 450
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->getFullscreenManager()Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->setControlContainer(Lcom/google/android/apps/chrome/widget/ControlContainer;)V

    .line 451
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->getCompositorViewHolder()Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->setControlContainer(Lcom/google/android/apps/chrome/widget/ControlContainer;)V

    .line 452
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->getCompositorViewHolder()Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$id;->url_bar:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/document/DocumentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->setUrlBar(Landroid/view/View;)V

    .line 455
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->updateLastTabId()V

    .line 457
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "Chrome_SnapshotID"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 458
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mDocumentTab:Lcom/google/android/apps/chrome/document/DocumentTab;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/document/DocumentTab;->setSnapshotId(Ljava/lang/String;)V

    .line 459
    :cond_2
    return-void
.end method

.method private isNewTabPage()Z
    .locals 3

    .prologue
    .line 1037
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mDocumentTab:Lcom/google/android/apps/chrome/document/DocumentTab;

    if-nez v0, :cond_0

    .line 1043
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/IntentHandler;->getTabIdFromIntent(Landroid/content/Intent;)I

    move-result v1

    .line 1044
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/IntentHandler;->getUrlFromIntent(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v0

    .line 1045
    iget-object v2, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mTabList:Lcom/google/android/apps/chrome/document/DocumentTabList;

    invoke-virtual {v2, v1}, Lcom/google/android/apps/chrome/document/DocumentTabList;->hasEntryForTabId(I)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mTabList:Lcom/google/android/apps/chrome/document/DocumentTabList;

    invoke-virtual {v2, v1}, Lcom/google/android/apps/chrome/document/DocumentTabList;->isRetargetable(I)Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v0, 0x0

    .line 1049
    :goto_0
    return v0

    .line 1047
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mDocumentTab:Lcom/google/android/apps/chrome/document/DocumentTab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/document/DocumentTab;->getUrl()Ljava/lang/String;

    move-result-object v0

    .line 1049
    :cond_1
    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/NewTabPage;->isNTPUrl(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method private loadLastKnownUrl(Lcom/google/android/apps/chrome/document/DocumentActivityDatabase$PendingDocumentData;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v5, 0x1

    .line 534
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/IntentHandler;->isIntentChromeOrFirstParty(Landroid/content/Intent;Landroid/content/Context;)Z

    move-result v1

    .line 536
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v3, "com.google.chrome.transition_type"

    invoke-virtual {v0, v3, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 539
    if-eq v0, v5, :cond_0

    if-eqz v0, :cond_0

    if-nez v1, :cond_0

    move v0, v2

    .line 547
    :cond_0
    if-nez v0, :cond_6

    .line 548
    const/high16 v1, 0x8000000

    or-int/2addr v0, v1

    move v1, v0

    .line 551
    :goto_0
    if-eqz p1, :cond_3

    iget-object v0, p1, Lcom/google/android/apps/chrome/document/DocumentActivityDatabase$PendingDocumentData;->url:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p1, Lcom/google/android/apps/chrome/document/DocumentActivityDatabase$PendingDocumentData;->url:Ljava/lang/String;

    .line 553
    :goto_1
    new-instance v3, Lorg/chromium/content_public/browser/LoadUrlParams;

    invoke-direct {v3, v0, v1}, Lorg/chromium/content_public/browser/LoadUrlParams;-><init>(Ljava/lang/String;I)V

    .line 555
    if-eqz p1, :cond_4

    .line 556
    iget-object v0, p1, Lcom/google/android/apps/chrome/document/DocumentActivityDatabase$PendingDocumentData;->postData:[B

    if-eqz v0, :cond_1

    .line 557
    iget-object v0, p1, Lcom/google/android/apps/chrome/document/DocumentActivityDatabase$PendingDocumentData;->postData:[B

    invoke-virtual {v3, v0}, Lorg/chromium/content_public/browser/LoadUrlParams;->setPostData([B)V

    .line 558
    invoke-virtual {v3, v5}, Lorg/chromium/content_public/browser/LoadUrlParams;->setLoadType(I)V

    .line 560
    :cond_1
    iget-object v0, p1, Lcom/google/android/apps/chrome/document/DocumentActivityDatabase$PendingDocumentData;->extraHeaders:Ljava/lang/String;

    invoke-virtual {v3, v0}, Lorg/chromium/content_public/browser/LoadUrlParams;->setVerbatimHeaders(Ljava/lang/String;)V

    .line 561
    iget-object v0, p1, Lcom/google/android/apps/chrome/document/DocumentActivityDatabase$PendingDocumentData;->referrer:Lorg/chromium/content_public/common/Referrer;

    invoke-virtual {v3, v0}, Lorg/chromium/content_public/browser/LoadUrlParams;->setReferrer(Lorg/chromium/content_public/common/Referrer;)V

    .line 562
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mDocumentTab:Lcom/google/android/apps/chrome/document/DocumentTab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/document/DocumentTab;->getTabRedirectHandler()Lcom/google/android/apps/chrome/tab/TabRedirectHandler;

    move-result-object v0

    iget-object v1, p1, Lcom/google/android/apps/chrome/document/DocumentActivityDatabase$PendingDocumentData;->originalIntent:Landroid/content/Intent;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/tab/TabRedirectHandler;->updateIntent(Landroid/content/Intent;)V

    .line 575
    :goto_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mDocumentTab:Lcom/google/android/apps/chrome/document/DocumentTab;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/chrome/document/DocumentTab;->loadUrl(Lorg/chromium/content_public/browser/LoadUrlParams;)I

    .line 577
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "com.android.chrome.use_desktop_user_agent"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 581
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mDocumentTab:Lcom/google/android/apps/chrome/document/DocumentTab;

    invoke-virtual {v0, v5, v5}, Lcom/google/android/apps/chrome/document/DocumentTab;->setUseDesktopUserAgent(ZZ)V

    .line 583
    :cond_2
    return-void

    .line 551
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->determineLastKnownUrl()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 564
    :cond_4
    const/4 v1, 0x0

    .line 565
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 567
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v4, "com.android.chrome.original_intent"

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 572
    :goto_3
    iget-object v1, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mDocumentTab:Lcom/google/android/apps/chrome/document/DocumentTab;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/document/DocumentTab;->getTabRedirectHandler()Lcom/google/android/apps/chrome/tab/TabRedirectHandler;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/tab/TabRedirectHandler;->updateIntent(Landroid/content/Intent;)V

    goto :goto_2

    :catch_0
    move-exception v0

    :cond_5
    move-object v0, v1

    goto :goto_3

    :cond_6
    move v1, v0

    goto :goto_0
.end method

.method private registerIncognitoPolicyChangeListener()V
    .locals 3

    .prologue
    .line 590
    sget-object v0, Lcom/google/android/apps/chrome/document/DocumentActivity;->sNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    if-eqz v0, :cond_0

    .line 618
    :goto_0
    return-void

    .line 592
    :cond_0
    new-instance v0, Lcom/google/android/apps/chrome/document/DocumentActivity$4;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/document/DocumentActivity$4;-><init>(Lcom/google/android/apps/chrome/document/DocumentActivity;)V

    sput-object v0, Lcom/google/android/apps/chrome/document/DocumentActivity;->sNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    .line 614
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [I

    fill-array-data v1, :array_0

    sget-object v2, Lcom/google/android/apps/chrome/document/DocumentActivity;->sNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->registerForNotifications(Landroid/content/Context;[ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    goto :goto_0

    :array_0
    .array-data 4
        0x48
        0x4a
    .end array-data
.end method

.method private shouldUseDefaultStatusBarColor()Z
    .locals 1

    .prologue
    .line 998
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->isIncognito()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mThemeColor:Ljava/lang/Integer;

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private updateLastTabId()V
    .locals 3

    .prologue
    .line 760
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 761
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 762
    const-string/jumbo v1, "pref_last_used_tab_id"

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->getCurrentTab()Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getId()I

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 763
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 764
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->getCurrentTab()Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getId()I

    move-result v0

    sput v0, Lcom/google/android/apps/chrome/document/DocumentActivity;->sLastUsedTabId:I

    .line 765
    return-void
.end method

.method private updateTaskDescription()V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 952
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mDocumentTab:Lcom/google/android/apps/chrome/document/DocumentTab;

    if-nez v0, :cond_0

    .line 953
    invoke-virtual {p0, v2, v2}, Lcom/google/android/apps/chrome/document/DocumentActivity;->updateTaskDescription(Ljava/lang/String;Landroid/graphics/Bitmap;)V

    .line 986
    :goto_0
    return-void

    .line 957
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->isNewTabPage()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->isIncognito()Z

    move-result v0

    if-nez v0, :cond_1

    .line 959
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->getThemeColor()I

    move-result v4

    .line 960
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mDocumentUtilities:Lcom/google/android/apps/chrome/utilities/DocumentUtilities;

    const/4 v5, 0x1

    move-object v1, p0

    move-object v3, v2

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/chrome/utilities/DocumentUtilities;->updateTaskDescription(Landroid/app/Activity;Ljava/lang/String;Landroid/graphics/Bitmap;IZ)V

    goto :goto_0

    .line 964
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mDocumentTab:Lcom/google/android/apps/chrome/document/DocumentTab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/document/DocumentTab;->getTitle()Ljava/lang/String;

    move-result-object v1

    .line 965
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mDocumentTab:Lcom/google/android/apps/chrome/document/DocumentTab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/document/DocumentTab;->getUrl()Ljava/lang/String;

    move-result-object v0

    const/4 v3, 0x0

    invoke-static {v0, v3}, Lorg/chromium/chrome/browser/UrlUtilities;->getDomainAndRegistry(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 966
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_6

    move-object v6, v0

    .line 969
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mIcon:Landroid/graphics/Bitmap;

    if-nez v0, :cond_2

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 970
    invoke-virtual {p0, v2, v2}, Lcom/google/android/apps/chrome/document/DocumentActivity;->updateTaskDescription(Ljava/lang/String;Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 974
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->isIncognito()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 975
    iput-object v2, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mIcon:Landroid/graphics/Bitmap;

    .line 985
    :cond_3
    :goto_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mIcon:Landroid/graphics/Bitmap;

    invoke-virtual {p0, v6, v0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->updateTaskDescription(Ljava/lang/String;Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 976
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mIcon:Landroid/graphics/Bitmap;

    if-nez v0, :cond_3

    .line 977
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mDocumentAppIconGenerator:Lcom/google/android/apps/chrome/widget/TabIconGenerator;

    if-nez v0, :cond_5

    .line 978
    new-instance v0, Lcom/google/android/apps/chrome/widget/TabIconGenerator;

    const/16 v2, 0x40

    const/4 v3, 0x3

    const/16 v4, 0x1e

    sget v5, Lcom/google/android/apps/chrome/document/DocumentActivity;->APP_ICON_DEFAULT_BACKGROUND_COLOR:I

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/chrome/widget/TabIconGenerator;-><init>(Landroid/content/Context;IIII)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mDocumentAppIconGenerator:Lcom/google/android/apps/chrome/widget/TabIconGenerator;

    .line 982
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mDocumentAppIconGenerator:Lcom/google/android/apps/chrome/widget/TabIconGenerator;

    iget-object v1, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mDocumentTab:Lcom/google/android/apps/chrome/document/DocumentTab;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/document/DocumentTab;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/widget/TabIconGenerator;->generateIconForTab(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mIcon:Landroid/graphics/Bitmap;

    goto :goto_2

    :cond_6
    move-object v6, v1

    goto :goto_1
.end method


# virtual methods
.method public createContextualSearchTab(Lorg/chromium/content/browser/ContentViewCore;)Z
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 790
    invoke-virtual {p1}, Lorg/chromium/content/browser/ContentViewCore;->getWebContents()Lorg/chromium/content_public/browser/WebContents;

    move-result-object v0

    invoke-interface {v0}, Lorg/chromium/content_public/browser/WebContents;->getNavigationController()Lorg/chromium/content_public/browser/NavigationController;

    move-result-object v0

    invoke-interface {v0}, Lorg/chromium/content_public/browser/NavigationController;->getPendingEntry()Lorg/chromium/content_public/browser/NavigationEntry;

    move-result-object v0

    .line 792
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/chromium/content_public/browser/NavigationEntry;->getUrl()Ljava/lang/String;

    move-result-object v3

    .line 794
    :goto_0
    new-instance v7, Lcom/google/android/apps/chrome/document/DocumentActivityDatabase$PendingDocumentData;

    invoke-direct {v7}, Lcom/google/android/apps/chrome/document/DocumentActivityDatabase$PendingDocumentData;-><init>()V

    .line 795
    const/16 v4, 0x1f4

    move-object v0, p0

    move v2, v1

    move v5, v1

    move v6, v1

    invoke-static/range {v0 .. v7}, Lcom/google/android/apps/chrome/document/ChromeLauncherActivity;->launchInstance(Landroid/app/Activity;ZILjava/lang/String;IIZLcom/google/android/apps/chrome/document/DocumentActivityDatabase$PendingDocumentData;)V

    .line 799
    return v1

    .line 792
    :cond_0
    invoke-virtual {p1}, Lorg/chromium/content/browser/ContentViewCore;->getWebContents()Lorg/chromium/content_public/browser/WebContents;

    move-result-object v0

    invoke-interface {v0}, Lorg/chromium/content_public/browser/WebContents;->getUrl()Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method

.method protected createDocumentToolbarHelper(Landroid/view/ViewGroup;Z)Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;
    .locals 1

    .prologue
    .line 275
    new-instance v0, Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;-><init>(Lcom/google/android/apps/chrome/document/DocumentActivity;Landroid/view/ViewGroup;Z)V

    return-object v0
.end method

.method protected createTitleBarDelegate()Lcom/google/android/apps/chrome/document/DocumentActivity$TitleBarDelegate;
    .locals 1

    .prologue
    .line 1020
    new-instance v0, Lcom/google/android/apps/chrome/document/DocumentActivity$TitleBarDelegate;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/document/DocumentActivity$TitleBarDelegate;-><init>(Lcom/google/android/apps/chrome/document/DocumentActivity;)V

    return-object v0
.end method

.method protected determineLastKnownUrl()Ljava/lang/String;
    .locals 2

    .prologue
    .line 343
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/IntentHandler;->getTabIdFromIntent(Landroid/content/Intent;)I

    move-result v1

    .line 344
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mTabList:Lcom/google/android/apps/chrome/document/DocumentTabList;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/document/DocumentTabList;->getCurrentUrlForDocument(I)Ljava/lang/String;

    move-result-object v0

    .line 346
    if-nez v0, :cond_0

    .line 347
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mTabList:Lcom/google/android/apps/chrome/document/DocumentTabList;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/document/DocumentTabList;->getInitialUrlForDocument(I)Ljava/lang/String;

    move-result-object v0

    .line 350
    :cond_0
    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/IntentHandler;->getUrlFromIntent(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v0

    :cond_1
    return-object v0
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 929
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mDocumentToolbarHelper:Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;->isInitialized()Z

    move-result v0

    invoke-static {p1, p0, v0}, Lcom/google/android/apps/chrome/KeyboardShortcuts;->dispatchKeyEvent(Landroid/view/KeyEvent;Lcom/google/android/apps/chrome/ChromeActivity;Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 931
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/CompositorChromeActivity;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public finishNativeInitialization()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 280
    invoke-static {p0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v0

    .line 282
    invoke-direct {p0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->isNewTabPage()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getFirstRunFlowComplete()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 284
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getPromosSkippedOnFirstStart()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 287
    invoke-static {p0}, Lcom/google/android/apps/chrome/promoscreen/SigninPromoScreen;->launchSigninPromoIfNeeded(Landroid/app/Activity;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 288
    invoke-static {p0}, Lcom/google/android/apps/chrome/promoscreen/DataReductionPromoScreen;->launchDataReductionPromo(Landroid/app/Activity;)V

    .line 295
    :cond_0
    :goto_0
    const/4 v1, 0x0

    invoke-static {p0, v2, v1}, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;->start(Landroid/app/Activity;ZLorg/chromium/chrome/browser/signin/SigninManager$SignInFlowObserver;)V

    .line 297
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->hasAttemptedMigrationOnUpgrade()Z

    move-result v0

    if-nez v0, :cond_1

    .line 298
    new-instance v0, Lcom/google/android/apps/chrome/document/DocumentActivity$3;

    invoke-static {v2}, Lcom/google/android/apps/chrome/document/DocumentActivityDatabase;->getTabList(Z)Lcom/google/android/apps/chrome/document/DocumentTabList;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/chrome/document/DocumentActivity$3;-><init>(Lcom/google/android/apps/chrome/document/DocumentActivity;Lcom/google/android/apps/chrome/document/DocumentTabList;)V

    .line 318
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/document/DocumentTabList$InitializationObserver;->runWhenReady()V

    .line 321
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x5

    const/4 v2, -0x2

    invoke-virtual {v0, v1, v2}, Landroid/view/Window;->setFeatureInt(II)V

    .line 324
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mTabInitializationObserver:Lcom/google/android/apps/chrome/document/DocumentTabList$InitializationObserver;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/document/DocumentTabList$InitializationObserver;->runWhenReady()V

    .line 326
    invoke-static {p0}, Lcom/google/android/apps/chrome/document/ssb/ChromeSsbServiceCallback;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/document/ssb/ChromeSsbServiceCallback;

    .line 327
    invoke-static {p0}, Lcom/google/android/apps/chrome/document/ssb/SearchLauncher;->isGSAAvailable(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 328
    new-instance v0, Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mSsbServiceClient:Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient;

    .line 329
    invoke-direct {p0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->connectAndPrepareSsbOverlay()V

    .line 333
    :cond_2
    invoke-static {}, Lcom/google/android/apps/chrome/document/DocumentActivityDatabase;->onNativeLibraryReady()V

    .line 335
    invoke-super {p0}, Lcom/google/android/apps/chrome/CompositorChromeActivity;->finishNativeInitialization()V

    .line 336
    return-void

    .line 291
    :cond_3
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->setPromosSkippedOnFirstStart(Z)V

    goto :goto_0
.end method

.method protected getControlContainerHeight(Landroid/content/res/Resources;)F
    .locals 1

    .prologue
    .line 948
    sget v0, Lcom/google/android/apps/chrome/R$dimen;->document_titlebar_height:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    return v0
.end method

.method public getTabModelSelector()Lcom/google/android/apps/chrome/tabmodel/SingleTabModelSelector;
    .locals 1

    .prologue
    .line 518
    invoke-super {p0}, Lcom/google/android/apps/chrome/CompositorChromeActivity;->getTabModelSelector()Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/tabmodel/SingleTabModelSelector;

    return-object v0
.end method

.method public bridge synthetic getTabModelSelector()Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;
    .locals 1

    .prologue
    .line 103
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->getTabModelSelector()Lcom/google/android/apps/chrome/tabmodel/SingleTabModelSelector;

    move-result-object v0

    return-object v0
.end method

.method protected getThemeColor()I
    .locals 2

    .prologue
    .line 989
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->isIncognito()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 990
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$color;->incognito_primary_color:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 992
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mThemeColor:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mThemeColor:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$color;->default_primary_color:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    goto :goto_0
.end method

.method public handleBackPressed()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 769
    invoke-super {p0}, Lcom/google/android/apps/chrome/CompositorChromeActivity;->handleBackPressed()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 785
    :goto_0
    return v0

    .line 770
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mDocumentTab:Lcom/google/android/apps/chrome/document/DocumentTab;

    if-nez v2, :cond_1

    move v0, v1

    goto :goto_0

    .line 772
    :cond_1
    invoke-static {}, Lorg/chromium/content/browser/ContentVideoView;->getContentVideoView()Lorg/chromium/content/browser/ContentVideoView;

    move-result-object v2

    .line 773
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    if-ne v2, p0, :cond_2

    .line 774
    invoke-static {}, Lorg/chromium/content/browser/ContentVideoView;->getContentVideoView()Lorg/chromium/content/browser/ContentVideoView;

    move-result-object v2

    invoke-virtual {v2, v1}, Lorg/chromium/content/browser/ContentVideoView;->exitFullscreen(Z)V

    goto :goto_0

    .line 778
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mDocumentTab:Lcom/google/android/apps/chrome/document/DocumentTab;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/document/DocumentTab;->canGoBack()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 779
    iget-object v1, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mDocumentTab:Lcom/google/android/apps/chrome/document/DocumentTab;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/document/DocumentTab;->goBack()V

    goto :goto_0

    .line 780
    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mDocumentTab:Lcom/google/android/apps/chrome/document/DocumentTab;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/document/DocumentTab;->shouldPreserve()Z

    move-result v1

    if-nez v1, :cond_4

    .line 781
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->finishAndRemoveTask()V

    goto :goto_0

    .line 783
    :cond_4
    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->moveTaskToBack(Z)Z

    goto :goto_0
.end method

.method protected isIncognito()Z
    .locals 1

    .prologue
    .line 1016
    const/4 v0, 0x0

    return v0
.end method

.method public onAccountSelected(Landroid/app/Activity;Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 804
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/sync/signin/AccountManagerHelper;->get(Landroid/content/Context;)Lorg/chromium/sync/signin/AccountManagerHelper;

    move-result-object v0

    invoke-virtual {v0, p2}, Lorg/chromium/sync/signin/AccountManagerHelper;->getAccountFromName(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v1

    .line 806
    if-nez v1, :cond_0

    .line 810
    :goto_0
    return-void

    .line 808
    :cond_0
    const/4 v3, 0x1

    const/4 v5, 0x0

    move-object v0, p0

    move v4, v2

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/chrome/firstrun/FirstRunUtil;->signInToSelectedAccount(Landroid/app/Activity;Landroid/accounts/Account;IIZLorg/chromium/chrome/browser/signin/SigninManager$SignInFlowObserver;)V

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 523
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mAppMenuHandler:Lorg/chromium/chrome/browser/appmenu/AppMenuHandler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mAppMenuHandler:Lorg/chromium/chrome/browser/appmenu/AppMenuHandler;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/appmenu/AppMenuHandler;->hideAppMenu()V

    .line 524
    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/CompositorChromeActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 525
    return-void
.end method

.method public onCreateDescription()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 355
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mDocumentTab:Lcom/google/android/apps/chrome/document/DocumentTab;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mDocumentTab:Lcom/google/android/apps/chrome/document/DocumentTab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/document/DocumentTab;->getTitle()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string/jumbo v0, ""

    goto :goto_0
.end method

.method protected onDestroyInternal()V
    .locals 2

    .prologue
    .line 463
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mDocumentTab:Lcom/google/android/apps/chrome/document/DocumentTab;

    if-eqz v0, :cond_0

    .line 464
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mDocumentTab:Lcom/google/android/apps/chrome/document/DocumentTab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/document/DocumentTab;->destroy()V

    .line 465
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mDocumentTab:Lcom/google/android/apps/chrome/document/DocumentTab;

    .line 467
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mDocumentToolbarHelper:Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mDocumentToolbarHelper:Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;->destroy()V

    .line 469
    :cond_1
    invoke-super {p0}, Lcom/google/android/apps/chrome/CompositorChromeActivity;->onDestroyInternal()V

    .line 471
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mCurrentLocale:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 480
    const-string/jumbo v0, "DocumentActivity"

    const-string/jumbo v1, "Forcefully killing process..."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 481
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v0

    invoke-static {v0}, Landroid/os/Process;->killProcess(I)V

    .line 484
    :cond_2
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 936
    iget-object v2, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mDocumentToolbarHelper:Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;->isInitialized()Z

    move-result v2

    if-nez v2, :cond_1

    .line 937
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-static {p2, p0, v1, v0}, Lcom/google/android/apps/chrome/KeyboardShortcuts;->onKeyDown(Landroid/view/KeyEvent;Lcom/google/android/apps/chrome/ChromeActivity;ZZ)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/chrome/CompositorChromeActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public onMenuOrKeyboardAction(I)Z
    .locals 10

    .prologue
    const-wide/16 v8, 0xaa

    const/4 v4, 0x0

    const/4 v6, 0x1

    const/4 v3, 0x0

    .line 814
    sget v0, Lcom/google/android/apps/chrome/R$id;->new_tab_menu_id:I

    if-ne p1, v0, :cond_2

    .line 815
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/chrome/document/DocumentActivity$7;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/document/DocumentActivity$7;-><init>(Lcom/google/android/apps/chrome/document/DocumentActivity;)V

    invoke-virtual {v0, v1, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_0
    :goto_0
    move v3, v6

    .line 898
    :cond_1
    return v3

    .line 822
    :cond_2
    sget v0, Lcom/google/android/apps/chrome/R$id;->new_incognito_tab_menu_id:I

    if-ne p1, v0, :cond_3

    .line 823
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/chrome/document/DocumentActivity$8;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/document/DocumentActivity$8;-><init>(Lcom/google/android/apps/chrome/document/DocumentActivity;)V

    invoke-virtual {v0, v1, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 830
    :cond_3
    sget v0, Lcom/google/android/apps/chrome/R$id;->all_bookmarks_menu_id:I

    if-ne p1, v0, :cond_4

    .line 831
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mDocumentTab:Lcom/google/android/apps/chrome/document/DocumentTab;

    invoke-static {p0, v0}, Lcom/google/android/apps/chrome/document/home/MostVisitedPage;->launchBookmarksDialog(Lcom/google/android/apps/chrome/document/DocumentActivity;Lorg/chromium/chrome/browser/Tab;)V

    goto :goto_0

    .line 832
    :cond_4
    sget v0, Lcom/google/android/apps/chrome/R$id;->recent_tabs_menu_id:I

    if-ne p1, v0, :cond_5

    .line 833
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mDocumentTab:Lcom/google/android/apps/chrome/document/DocumentTab;

    invoke-static {p0, v0, v3}, Lcom/google/android/apps/chrome/document/home/MostVisitedPage;->launchRecentTabsDialog(Landroid/app/Activity;Lcom/google/android/apps/chrome/tab/ChromeTab;Z)V

    goto :goto_0

    .line 834
    :cond_5
    sget v0, Lcom/google/android/apps/chrome/R$id;->open_history_menu_id:I

    if-ne p1, v0, :cond_6

    .line 835
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mDocumentTab:Lcom/google/android/apps/chrome/document/DocumentTab;

    new-instance v1, Lorg/chromium/content_public/browser/LoadUrlParams;

    const-string/jumbo v2, "chrome://history/"

    const/4 v3, 0x6

    invoke-direct {v1, v2, v3}, Lorg/chromium/content_public/browser/LoadUrlParams;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/document/DocumentTab;->loadUrl(Lorg/chromium/content_public/browser/LoadUrlParams;)I

    goto :goto_0

    .line 837
    :cond_6
    sget v0, Lcom/google/android/apps/chrome/R$id;->add_to_homescreen_id:I

    if-ne p1, v0, :cond_7

    .line 838
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mDocumentTab:Lcom/google/android/apps/chrome/document/DocumentTab;

    invoke-static {p0, v0}, Lcom/google/android/apps/chrome/webapps/WebappManager;->showDialog(Landroid/app/Activity;Lorg/chromium/chrome/browser/Tab;)V

    goto :goto_0

    .line 839
    :cond_7
    sget v0, Lcom/google/android/apps/chrome/R$id;->forward_menu_id:I

    if-ne p1, v0, :cond_8

    .line 840
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mDocumentTab:Lcom/google/android/apps/chrome/document/DocumentTab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/document/DocumentTab;->canGoForward()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mDocumentTab:Lcom/google/android/apps/chrome/document/DocumentTab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/document/DocumentTab;->goForward()V

    goto :goto_0

    .line 841
    :cond_8
    sget v0, Lcom/google/android/apps/chrome/R$id;->find_in_page_id:I

    if-ne p1, v0, :cond_9

    .line 842
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mFindToolbarWrapper:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarWrapper;

    invoke-virtual {v0, v4}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarWrapper;->showToolbar(Ljava/lang/String;)V

    goto :goto_0

    .line 843
    :cond_9
    sget v0, Lcom/google/android/apps/chrome/R$id;->reload_menu_id:I

    if-ne p1, v0, :cond_b

    .line 844
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mDocumentTab:Lcom/google/android/apps/chrome/document/DocumentTab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/document/DocumentTab;->isLoading()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 845
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mDocumentTab:Lcom/google/android/apps/chrome/document/DocumentTab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/document/DocumentTab;->stopLoading()V

    goto :goto_0

    .line 847
    :cond_a
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mDocumentTab:Lcom/google/android/apps/chrome/document/DocumentTab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/document/DocumentTab;->reload()V

    goto :goto_0

    .line 849
    :cond_b
    sget v0, Lcom/google/android/apps/chrome/R$id;->request_desktop_site_id:I

    if-ne p1, v0, :cond_e

    .line 850
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mDocumentTab:Lcom/google/android/apps/chrome/document/DocumentTab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/document/DocumentTab;->isNativePage()Z

    move-result v0

    if-nez v0, :cond_d

    move v0, v6

    .line 851
    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mDocumentTab:Lcom/google/android/apps/chrome/document/DocumentTab;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/document/DocumentTab;->getUseDesktopUserAgent()Z

    move-result v1

    .line 852
    iget-object v2, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mDocumentTab:Lcom/google/android/apps/chrome/document/DocumentTab;

    if-nez v1, :cond_c

    move v3, v6

    :cond_c
    invoke-virtual {v2, v3, v0}, Lcom/google/android/apps/chrome/document/DocumentTab;->setUseDesktopUserAgent(ZZ)V

    .line 853
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->menuRequestDesktopSite()V

    goto/16 :goto_0

    :cond_d
    move v0, v3

    .line 850
    goto :goto_1

    .line 854
    :cond_e
    sget v0, Lcom/google/android/apps/chrome/R$id;->bookmark_this_page_id:I

    if-ne p1, v0, :cond_f

    .line 855
    invoke-direct {p0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->addOrEditBookmark()V

    goto/16 :goto_0

    .line 856
    :cond_f
    sget v0, Lcom/google/android/apps/chrome/R$id;->help_id:I

    if-ne p1, v0, :cond_12

    .line 857
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->getContentReadbackHandler()Lorg/chromium/content/browser/ContentReadbackHandler;

    move-result-object v0

    .line 858
    new-instance v1, Lcom/google/android/apps/chrome/document/DocumentActivity$9;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/document/DocumentActivity$9;-><init>(Lcom/google/android/apps/chrome/document/DocumentActivity;)V

    .line 869
    if-eqz v0, :cond_10

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->getWindowAndroid()Lorg/chromium/ui/base/WindowAndroid;

    move-result-object v2

    if-nez v2, :cond_11

    .line 870
    :cond_10
    invoke-interface {v1, v4}, Lorg/chromium/content/browser/ContentReadbackHandler$GetBitmapCallback;->onFinishGetBitmap(Landroid/graphics/Bitmap;)V

    goto/16 :goto_0

    .line 872
    :cond_11
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->getWindowAndroid()Lorg/chromium/ui/base/WindowAndroid;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, Lorg/chromium/content/browser/ContentReadbackHandler;->getCompositorBitmapAsync(Lorg/chromium/ui/base/WindowAndroid;Lorg/chromium/content/browser/ContentReadbackHandler$GetBitmapCallback;)V

    goto/16 :goto_0

    .line 874
    :cond_12
    sget v0, Lcom/google/android/apps/chrome/R$id;->share_menu_id:I

    if-eq p1, v0, :cond_13

    sget v0, Lcom/google/android/apps/chrome/R$id;->direct_share_menu_id:I

    if-ne p1, v0, :cond_15

    .line 875
    :cond_13
    iget-object v1, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mDocumentTab:Lcom/google/android/apps/chrome/document/DocumentTab;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->getWindowAndroid()Lorg/chromium/ui/base/WindowAndroid;

    move-result-object v2

    sget v0, Lcom/google/android/apps/chrome/R$id;->direct_share_menu_id:I

    if-ne p1, v0, :cond_14

    move v3, v6

    :cond_14
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->isIncognito()Z

    move-result v4

    const/high16 v5, 0x80000

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/chrome/document/DocumentActivity;->onShareMenuItemSelected(Lcom/google/android/apps/chrome/tab/ChromeTab;Lorg/chromium/ui/base/WindowAndroid;ZZI)V

    goto/16 :goto_0

    .line 878
    :cond_15
    sget v0, Lcom/google/android/apps/chrome/R$id;->preferences_id:I

    if-ne p1, v0, :cond_16

    .line 879
    invoke-static {p0}, Lcom/google/android/apps/chrome/document/ChromeLauncherActivity;->launchPreferences(Landroid/app/Activity;)V

    goto/16 :goto_0

    .line 880
    :cond_16
    sget v0, Lcom/google/android/apps/chrome/R$id;->print_id:I

    if-ne p1, v0, :cond_17

    .line 881
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->getChromeApplication()Lcom/google/android/apps/chrome/ChromeMobileApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeMobileApplication;->getPrintingController()Lorg/chromium/printing/PrintingController;

    move-result-object v0

    .line 883
    if-eqz v0, :cond_0

    invoke-interface {v0}, Lorg/chromium/printing/PrintingController;->isBusy()Z

    move-result v1

    if-nez v1, :cond_0

    .line 884
    new-instance v1, Lorg/chromium/chrome/browser/printing/TabPrinter;

    iget-object v2, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mDocumentTab:Lcom/google/android/apps/chrome/document/DocumentTab;

    invoke-direct {v1, v2}, Lorg/chromium/chrome/browser/printing/TabPrinter;-><init>(Lorg/chromium/chrome/browser/Tab;)V

    new-instance v2, Lorg/chromium/printing/PrintManagerDelegateImpl;

    invoke-direct {v2, p0}, Lorg/chromium/printing/PrintManagerDelegateImpl;-><init>(Landroid/content/Context;)V

    invoke-interface {v0, v1, v2}, Lorg/chromium/printing/PrintingController;->startPrint(Lorg/chromium/printing/Printable;Lorg/chromium/printing/PrintManagerDelegate;)V

    .line 886
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->menuPrint()V

    goto/16 :goto_0

    .line 888
    :cond_17
    sget v0, Lcom/google/android/apps/chrome/R$id;->show_menu:I

    if-ne p1, v0, :cond_18

    .line 889
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mDocumentToolbarHelper:Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 890
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mAppMenuHandler:Lorg/chromium/chrome/browser/appmenu/AppMenuHandler;

    iget-object v1, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mDocumentToolbarHelper:Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;->getMenuAnchor()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1, v6, v3}, Lorg/chromium/chrome/browser/appmenu/AppMenuHandler;->showAppMenu(Landroid/view/View;ZZ)Z

    goto/16 :goto_0

    .line 893
    :cond_18
    sget v0, Lcom/google/android/apps/chrome/R$id;->focus_url_bar:I

    if-ne p1, v0, :cond_1

    .line 894
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mDocumentToolbarHelper:Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mDocumentToolbarHelper:Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;->focusUrlBar()V

    goto/16 :goto_0
.end method

.method public onOrientationChange(I)V
    .locals 1

    .prologue
    .line 529
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/CompositorChromeActivity;->onOrientationChange(I)V

    .line 530
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mDocumentToolbarHelper:Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;->onOrientationChange()V

    .line 531
    return-void
.end method

.method public onPauseWithNative()V
    .locals 1

    .prologue
    .line 494
    invoke-super {p0}, Lcom/google/android/apps/chrome/CompositorChromeActivity;->onPauseWithNative()V

    .line 496
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mDocumentTab:Lcom/google/android/apps/chrome/document/DocumentTab;

    if-nez v0, :cond_0

    .line 498
    :goto_0
    return-void

    .line 497
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mDocumentTab:Lcom/google/android/apps/chrome/document/DocumentTab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/document/DocumentTab;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->unregisterForContextMenu(Landroid/view/View;)V

    goto :goto_0
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 401
    invoke-super {p0}, Lcom/google/android/apps/chrome/CompositorChromeActivity;->onResume()V

    .line 402
    invoke-static {p0}, Lcom/google/android/apps/chrome/document/IncognitoDocumentActivity;->dismissIncognitoNotificationIfNecessary(Landroid/content/Context;)V

    .line 403
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mDocumentToolbarHelper:Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;->onResume()V

    .line 405
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mDocumentUtilities:Lcom/google/android/apps/chrome/utilities/DocumentUtilities;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 406
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mDocumentUtilities:Lcom/google/android/apps/chrome/utilities/DocumentUtilities;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->getTaskId()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/utilities/DocumentUtilities;->finishOtherTasksWithData(Landroid/net/Uri;I)Landroid/content/Intent;

    .line 408
    :cond_0
    return-void
.end method

.method public onResumeWithNative()V
    .locals 3

    .prologue
    .line 414
    invoke-static {}, Lorg/chromium/chrome/browser/profiles/Profile;->getLastUsedProfile()Lorg/chromium/chrome/browser/profiles/Profile;

    move-result-object v0

    .line 415
    invoke-static {p0}, Lcom/google/android/apps/chrome/document/IncognitoDocumentActivity;->getNumberOfIncognitoTabs(Landroid/content/Context;)I

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/profiles/Profile;->hasOffTheRecordProfile()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 417
    invoke-virtual {v0}, Lorg/chromium/chrome/browser/profiles/Profile;->getOffTheRecordProfile()Lorg/chromium/chrome/browser/profiles/Profile;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/ChromeMobileApplication;->destroyProfile(Lorg/chromium/chrome/browser/profiles/Profile;)V

    .line 419
    :cond_0
    invoke-super {p0}, Lcom/google/android/apps/chrome/CompositorChromeActivity;->onResumeWithNative()V

    .line 421
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 422
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "default_search_engine_is_google"

    invoke-static {}, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;->getInstance()Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;

    move-result-object v2

    invoke-virtual {v2}, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;->isDefaultSearchEngineGoogle()Z

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 425
    invoke-static {}, Lcom/google/android/apps/chrome/document/DocumentActivityDatabase;->updateRecentlyClosed()V

    .line 427
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mDocumentTab:Lcom/google/android/apps/chrome/document/DocumentTab;

    if-eqz v0, :cond_2

    .line 428
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/IntentHandler;->getTabIdFromIntent(Landroid/content/Intent;)I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/document/DocumentActivityDatabase;->removePendingDocumentData(I)Lcom/google/android/apps/chrome/document/DocumentActivityDatabase$PendingDocumentData;

    move-result-object v0

    .line 430
    if-eqz v0, :cond_1

    iget-object v1, v0, Lcom/google/android/apps/chrome/document/DocumentActivityDatabase$PendingDocumentData;->url:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 431
    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->loadLastKnownUrl(Lcom/google/android/apps/chrome/document/DocumentActivityDatabase$PendingDocumentData;)V

    .line 433
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->initializeWithTab()V

    .line 435
    :cond_2
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 488
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/CompositorChromeActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 489
    invoke-static {}, Lorg/chromium/content/browser/crypto/CipherFactory;->getInstance()Lorg/chromium/content/browser/crypto/CipherFactory;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/chromium/content/browser/crypto/CipherFactory;->saveToBundle(Landroid/os/Bundle;)V

    .line 490
    return-void
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 394
    invoke-super {p0}, Lcom/google/android/apps/chrome/CompositorChromeActivity;->onStart()V

    .line 395
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mContextReporter:Lcom/google/android/apps/chrome/icing/ContextReporter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mContextReporter:Lcom/google/android/apps/chrome/icing/ContextReporter;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/icing/ContextReporter;->enable()V

    .line 396
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mSsbServiceClient:Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient;

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->connectAndPrepareSsbOverlay()V

    .line 397
    :cond_1
    return-void
.end method

.method public onStartWithNative()V
    .locals 3

    .prologue
    .line 360
    invoke-super {p0}, Lcom/google/android/apps/chrome/CompositorChromeActivity;->onStartWithNative()V

    .line 361
    invoke-direct {p0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->handleDocumentUma()V

    .line 363
    new-instance v0, Lcom/google/android/apps/chrome/snapshot/SnapshotStateBroadcastReceiver;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->getTabModelSelector()Lcom/google/android/apps/chrome/tabmodel/SingleTabModelSelector;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/apps/chrome/tabmodel/SingleTabModelSelector;->getModel(Z)Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mHandler:Landroid/os/Handler;

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/chrome/snapshot/SnapshotStateBroadcastReceiver;-><init>(Lorg/chromium/chrome/browser/tabmodel/TabModel;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mSnapshotDownloadBroadcastReceiver:Lcom/google/android/apps/chrome/snapshot/SnapshotStateBroadcastReceiver;

    .line 365
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mSnapshotDownloadBroadcastReceiver:Lcom/google/android/apps/chrome/snapshot/SnapshotStateBroadcastReceiver;

    new-instance v1, Landroid/content/IntentFilter;

    const-string/jumbo v2, "com.google.android.apps.chrome.snapshot.ACTION_SNAPSHOT_STATE_UPDATE"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/chrome/document/DocumentActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 367
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 387
    invoke-super {p0}, Lcom/google/android/apps/chrome/CompositorChromeActivity;->onStop()V

    .line 388
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mContextReporter:Lcom/google/android/apps/chrome/icing/ContextReporter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mContextReporter:Lcom/google/android/apps/chrome/icing/ContextReporter;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/icing/ContextReporter;->disable()V

    .line 389
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mSsbServiceClient:Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mSsbServiceClient:Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient;->disconnect()V

    .line 390
    :cond_1
    return-void
.end method

.method public onStopWithNative()V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 502
    iget-object v1, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mDocumentTab:Lcom/google/android/apps/chrome/document/DocumentTab;

    if-eqz v1, :cond_1

    .line 503
    iget-object v1, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mDocumentTab:Lcom/google/android/apps/chrome/document/DocumentTab;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v3, "com.android.chrome.append_task"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/document/DocumentTab;->setShouldPreserve(Z)V

    .line 505
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mTabList:Lcom/google/android/apps/chrome/document/DocumentTabList;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mDocumentTab:Lcom/google/android/apps/chrome/document/DocumentTab;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/document/DocumentTabList;->updateEntry(Landroid/content/Intent;Lcom/google/android/apps/chrome/tab/ChromeTab;)V

    .line 507
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mAppMenuHandler:Lorg/chromium/chrome/browser/appmenu/AppMenuHandler;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mAppMenuHandler:Lorg/chromium/chrome/browser/appmenu/AppMenuHandler;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/appmenu/AppMenuHandler;->hideAppMenu()V

    .line 509
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mSnapshotDownloadBroadcastReceiver:Lcom/google/android/apps/chrome/snapshot/SnapshotStateBroadcastReceiver;

    if-eqz v0, :cond_3

    .line 510
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mSnapshotDownloadBroadcastReceiver:Lcom/google/android/apps/chrome/snapshot/SnapshotStateBroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 511
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mSnapshotDownloadBroadcastReceiver:Lcom/google/android/apps/chrome/snapshot/SnapshotStateBroadcastReceiver;

    .line 513
    :cond_3
    invoke-super {p0}, Lcom/google/android/apps/chrome/CompositorChromeActivity;->onStopWithNative()V

    .line 514
    return-void
.end method

.method public postInflationStartup()V
    .locals 3

    .prologue
    .line 226
    invoke-super {p0}, Lcom/google/android/apps/chrome/CompositorChromeActivity;->postInflationStartup()V

    .line 228
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/IntentHandler;->getTabIdFromIntent(Landroid/content/Intent;)I

    move-result v0

    .line 229
    new-instance v1, Lcom/google/android/apps/chrome/document/DocumentActivity$2;

    iget-object v2, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mTabList:Lcom/google/android/apps/chrome/document/DocumentTabList;

    invoke-direct {v1, p0, v2, v0}, Lcom/google/android/apps/chrome/document/DocumentActivity$2;-><init>(Lcom/google/android/apps/chrome/document/DocumentActivity;Lcom/google/android/apps/chrome/document/DocumentTabList;I)V

    iput-object v1, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mTabInitializationObserver:Lcom/google/android/apps/chrome/document/DocumentTabList$InitializationObserver;

    .line 246
    return-void
.end method

.method public preInflationStartup()V
    .locals 3

    .prologue
    .line 187
    invoke-direct {p0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->registerIncognitoPolicyChangeListener()V

    .line 190
    invoke-static {}, Lorg/chromium/content/browser/crypto/CipherFactory;->getInstance()Lorg/chromium/content/browser/crypto/CipherFactory;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mSavedInstanceState:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Lorg/chromium/content/browser/crypto/CipherFactory;->restoreFromBundle(Landroid/os/Bundle;)Z

    .line 192
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/IntentHandler;->getTabIdFromIntent(Landroid/content/Intent;)I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/document/DocumentActivityDatabase;->setPrioritizedTabId(I)V

    .line 193
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->isIncognito()Z

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/document/DocumentActivityDatabase;->getTabList(Z)Lcom/google/android/apps/chrome/document/DocumentTabList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mTabList:Lcom/google/android/apps/chrome/document/DocumentTabList;

    .line 195
    invoke-super {p0}, Lcom/google/android/apps/chrome/CompositorChromeActivity;->preInflationStartup()V

    .line 196
    new-instance v0, Lcom/google/android/apps/chrome/utilities/DocumentUtilities;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/utilities/DocumentUtilities;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mDocumentUtilities:Lcom/google/android/apps/chrome/utilities/DocumentUtilities;

    .line 197
    new-instance v0, Lcom/google/android/apps/chrome/document/DocumentActivity$1;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->isIncognito()Z

    move-result v1

    const/4 v2, 0x0

    invoke-direct {v0, p0, p0, v1, v2}, Lcom/google/android/apps/chrome/document/DocumentActivity$1;-><init>(Lcom/google/android/apps/chrome/document/DocumentActivity;Landroid/app/Activity;ZZ)V

    .line 219
    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->setTabModelSelector(Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;)V

    .line 221
    const/16 v0, 0xa

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->requestWindowFeature(I)Z

    .line 222
    return-void
.end method

.method public prepareMenu(Landroid/view/Menu;)V
    .locals 2

    .prologue
    .line 268
    invoke-direct {p0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->isNewTabPage()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->isIncognito()Z

    move-result v0

    if-nez v0, :cond_0

    .line 269
    sget v0, Lcom/google/android/apps/chrome/R$id;->new_tab_menu_id:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 271
    :cond_0
    return-void
.end method

.method protected setContentView()V
    .locals 5

    .prologue
    const/4 v4, -0x1

    .line 250
    invoke-super {p0}, Lcom/google/android/apps/chrome/CompositorChromeActivity;->setContentView()V

    .line 252
    new-instance v1, Landroid/view/View;

    invoke-direct {v1, p0}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 253
    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/document/DocumentActivity;->setContentView(Landroid/view/View;)V

    .line 254
    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 255
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->isIncognito()Z

    move-result v2

    invoke-virtual {p0, v0, v2}, Lcom/google/android/apps/chrome/document/DocumentActivity;->createDocumentToolbarHelper(Landroid/view/ViewGroup;Z)Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mDocumentToolbarHelper:Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;

    .line 257
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 259
    const v0, 0x1020002

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 260
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->getCompositorViewHolder()Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    move-result-object v1

    const/4 v2, 0x0

    new-instance v3, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v3, v4, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 264
    return-void
.end method

.method public shouldShowAppMenu()Z
    .locals 1

    .prologue
    .line 943
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mDocumentTab:Lcom/google/android/apps/chrome/document/DocumentTab;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mDocumentToolbarHelper:Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected updateTaskDescription(Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 6

    .prologue
    .line 1002
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->getThemeColor()I

    move-result v4

    .line 1003
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mDocumentUtilities:Lcom/google/android/apps/chrome/utilities/DocumentUtilities;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->shouldUseDefaultStatusBarColor()Z

    move-result v5

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/chrome/utilities/DocumentUtilities;->updateTaskDescription(Landroid/app/Activity;Ljava/lang/String;Landroid/graphics/Bitmap;IZ)V

    .line 1005
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity;->mDocumentToolbarHelper:Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;

    invoke-virtual {v0, v4}, Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;->setThemeColor(I)V

    .line 1007
    sget v0, Lcom/google/android/apps/chrome/R$id;->control_container:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/widget/ControlContainer;

    .line 1009
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/ControlContainer;->getResourceAdapter()Lcom/google/android/apps/chrome/compositor/resources/dynamics/ViewResourceAdapter;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/compositor/resources/dynamics/ViewResourceAdapter;->invalidate(Landroid/graphics/Rect;)V

    .line 1010
    return-void
.end method
