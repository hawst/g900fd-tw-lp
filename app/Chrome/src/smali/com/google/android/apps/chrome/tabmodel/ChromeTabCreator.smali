.class public Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;
.super Ljava/lang/Object;
.source "ChromeTabCreator.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

.field private final mIncognito:Z

.field private final mNativeWindow:Lorg/chromium/ui/base/WindowAndroid;

.field private final mOrderController:Lorg/chromium/chrome/browser/tabmodel/TabModelOrderController;

.field private mTabContentManager:Lcom/google/android/apps/chrome/compositor/TabContentManager;

.field private mTabModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

.field private final mTabSaver:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const-class v0, Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/google/android/apps/chrome/ChromeActivity;Lorg/chromium/ui/base/WindowAndroid;Lorg/chromium/chrome/browser/tabmodel/TabModelOrderController;Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;Z)V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iput-object p1, p0, Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    .line 47
    iput-object p2, p0, Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;->mNativeWindow:Lorg/chromium/ui/base/WindowAndroid;

    .line 48
    iput-object p3, p0, Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;->mOrderController:Lorg/chromium/chrome/browser/tabmodel/TabModelOrderController;

    .line 49
    iput-object p4, p0, Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;->mTabSaver:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;

    .line 50
    iput-boolean p5, p0, Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;->mIncognito:Z

    .line 51
    return-void
.end method

.method private static getTransitionType(Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;)I
    .locals 2

    .prologue
    .line 310
    sget-object v0, Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator$1;->$SwitchMap$org$chromium$chrome$browser$tabmodel$TabModel$TabLaunchType:[I

    invoke-virtual {p0}, Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 321
    sget-boolean v0, Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 313
    :pswitch_0
    const/high16 v0, 0x8000000

    .line 322
    :goto_0
    return v0

    .line 319
    :pswitch_1
    const/4 v0, 0x6

    goto :goto_0

    .line 322
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 310
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public createFrozenTab(Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;II)V
    .locals 6

    .prologue
    .line 295
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    iget-boolean v2, p1, Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;->isIncognito:Z

    iget-object v3, p0, Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;->mNativeWindow:Lorg/chromium/ui/base/WindowAndroid;

    iget v4, p1, Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;->parentId:I

    move v0, p2

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/chrome/tab/ChromeTab;->createFrozenTabFromState(ILcom/google/android/apps/chrome/ChromeActivity;ZLorg/chromium/ui/base/WindowAndroid;ILcom/google/android/apps/chrome/tab/ChromeTab$TabState;)Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v1

    .line 297
    iget-object v0, p1, Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;->openerAppId:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;->openerAppId:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->associateWithApp(Ljava/lang/String;)V

    .line 298
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;->mOrderController:Lorg/chromium/chrome/browser/tabmodel/TabModelOrderController;

    sget-object v2, Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;->FROM_RESTORE:Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;

    iget-boolean v3, p1, Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;->isIncognito:Z

    invoke-virtual {v0, v2, v3}, Lorg/chromium/chrome/browser/tabmodel/TabModelOrderController;->willOpenInForeground(Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;Z)Z

    move-result v0

    .line 300
    const-wide/16 v2, 0x0

    iget-object v4, p0, Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;->mTabContentManager:Lcom/google/android/apps/chrome/compositor/TabContentManager;

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v2, v3, v4, v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->initialize(JLcom/google/android/apps/chrome/compositor/TabContentManager;Z)V

    .line 301
    sget-boolean v0, Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    iget-boolean v0, p1, Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;->isIncognito:Z

    iget-boolean v2, p0, Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;->mIncognito:Z

    if-eq v0, v2, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 300
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 302
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;->mTabModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    sget-object v2, Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;->FROM_RESTORE:Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;

    invoke-interface {v0, v1, p3, v2}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->addTab(Lorg/chromium/chrome/browser/Tab;ILorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;)V

    .line 303
    return-void
.end method

.method public createNewTab(Lorg/chromium/content_public/browser/LoadUrlParams;Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;Lorg/chromium/chrome/browser/Tab;)Lcom/google/android/apps/chrome/tab/ChromeTab;
    .locals 1

    .prologue
    .line 62
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;->createNewTab(Lorg/chromium/content_public/browser/LoadUrlParams;Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;Lorg/chromium/chrome/browser/Tab;Landroid/content/Intent;)Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    return-object v0
.end method

.method public createNewTab(Lorg/chromium/content_public/browser/LoadUrlParams;Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;Lorg/chromium/chrome/browser/Tab;ILandroid/content/Intent;)Lcom/google/android/apps/chrome/tab/ChromeTab;
    .locals 15

    .prologue
    .line 107
    invoke-static {}, Lorg/chromium/base/TraceEvent;->begin()V

    .line 108
    if-eqz p3, :cond_0

    invoke-virtual/range {p3 .. p3}, Lorg/chromium/chrome/browser/Tab;->getId()I

    move-result v6

    .line 110
    :goto_0
    invoke-virtual/range {p1 .. p1}, Lorg/chromium/content_public/browser/LoadUrlParams;->getUrl()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lorg/chromium/chrome/browser/UrlUtilities;->fixUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/chromium/content_public/browser/LoadUrlParams;->setUrl(Ljava/lang/String;)V

    .line 111
    invoke-static/range {p2 .. p2}, Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;->getTransitionType(Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;)I

    move-result v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/chromium/content_public/browser/LoadUrlParams;->setTransitionType(I)V

    .line 113
    iget-object v2, p0, Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;->mOrderController:Lorg/chromium/chrome/browser/tabmodel/TabModelOrderController;

    iget-boolean v3, p0, Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;->mIncognito:Z

    move-object/from16 v0, p2

    invoke-virtual {v2, v0, v3}, Lorg/chromium/chrome/browser/tabmodel/TabModelOrderController;->willOpenInForeground(Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;Z)Z

    move-result v14

    .line 117
    if-nez v14, :cond_2

    invoke-static {}, Lorg/chromium/base/SysUtils;->isLowEndDevice()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 118
    iget-object v2, p0, Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    iget-boolean v3, p0, Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;->mIncognito:Z

    iget-object v4, p0, Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;->mNativeWindow:Lorg/chromium/ui/base/WindowAndroid;

    move-object/from16 v5, p2

    move-object/from16 v7, p1

    invoke-static/range {v2 .. v7}, Lcom/google/android/apps/chrome/tab/ChromeTab;->createTabForLazyLoad(Lcom/google/android/apps/chrome/ChromeActivity;ZLorg/chromium/ui/base/WindowAndroid;Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;ILorg/chromium/content_public/browser/LoadUrlParams;)Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v3

    .line 120
    const-wide/16 v4, 0x0

    iget-object v6, p0, Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;->mTabContentManager:Lcom/google/android/apps/chrome/compositor/TabContentManager;

    if-nez v14, :cond_1

    const/4 v2, 0x1

    :goto_1
    invoke-virtual {v3, v4, v5, v6, v2}, Lcom/google/android/apps/chrome/tab/ChromeTab;->initialize(JLcom/google/android/apps/chrome/compositor/TabContentManager;Z)V

    .line 121
    iget-object v2, p0, Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;->mTabSaver:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->addTabToSaveQueue(Lcom/google/android/apps/chrome/tab/ChromeTab;)V

    .line 122
    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getTabRedirectHandler()Lcom/google/android/apps/chrome/tab/TabRedirectHandler;

    move-result-object v2

    move-object/from16 v0, p5

    invoke-virtual {v2, v0}, Lcom/google/android/apps/chrome/tab/TabRedirectHandler;->updateIntent(Landroid/content/Intent;)V

    move-object v2, v3

    .line 135
    :goto_2
    iget-object v3, p0, Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;->mTabModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move/from16 v0, p4

    move-object/from16 v1, p2

    invoke-interface {v3, v2, v0, v1}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->addTab(Lorg/chromium/chrome/browser/Tab;ILorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;)V

    .line 137
    invoke-static {}, Lorg/chromium/base/TraceEvent;->end()V

    .line 138
    return-object v2

    .line 108
    :cond_0
    const/4 v6, -0x1

    goto :goto_0

    .line 120
    :cond_1
    const/4 v2, 0x0

    goto :goto_1

    .line 124
    :cond_2
    const/4 v7, -0x1

    iget-object v8, p0, Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    iget-boolean v9, p0, Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;->mIncognito:Z

    iget-object v10, p0, Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;->mNativeWindow:Lorg/chromium/ui/base/WindowAndroid;

    if-nez v14, :cond_3

    const/4 v13, 0x1

    :goto_3
    move-object/from16 v11, p2

    move v12, v6

    invoke-static/range {v7 .. v13}, Lcom/google/android/apps/chrome/tab/ChromeTab;->createLiveTab(ILcom/google/android/apps/chrome/ChromeActivity;ZLorg/chromium/ui/base/WindowAndroid;Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;IZ)Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v5

    .line 127
    invoke-static {}, Lcom/google/android/apps/chrome/WarmupManager;->getInstance()Lcom/google/android/apps/chrome/WarmupManager;

    move-result-object v2

    invoke-virtual/range {p1 .. p1}, Lorg/chromium/content_public/browser/LoadUrlParams;->getUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/chrome/WarmupManager;->hasPrerenderedUrl(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-static {}, Lcom/google/android/apps/chrome/WarmupManager;->getInstance()Lcom/google/android/apps/chrome/WarmupManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/WarmupManager;->takePrerenderedNativeWebContents()J

    move-result-wide v2

    .line 130
    :goto_4
    iget-object v6, p0, Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;->mTabContentManager:Lcom/google/android/apps/chrome/compositor/TabContentManager;

    if-nez v14, :cond_5

    const/4 v4, 0x1

    :goto_5
    invoke-virtual {v5, v2, v3, v6, v4}, Lcom/google/android/apps/chrome/tab/ChromeTab;->initialize(JLcom/google/android/apps/chrome/compositor/TabContentManager;Z)V

    .line 131
    invoke-virtual {v5}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getTabRedirectHandler()Lcom/google/android/apps/chrome/tab/TabRedirectHandler;

    move-result-object v2

    move-object/from16 v0, p5

    invoke-virtual {v2, v0}, Lcom/google/android/apps/chrome/tab/TabRedirectHandler;->updateIntent(Landroid/content/Intent;)V

    .line 132
    move-object/from16 v0, p1

    invoke-virtual {v5, v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->loadUrl(Lorg/chromium/content_public/browser/LoadUrlParams;)I

    move-object v2, v5

    goto :goto_2

    .line 124
    :cond_3
    const/4 v13, 0x0

    goto :goto_3

    .line 127
    :cond_4
    const-wide/16 v2, 0x0

    goto :goto_4

    .line 130
    :cond_5
    const/4 v4, 0x0

    goto :goto_5
.end method

.method public createNewTab(Lorg/chromium/content_public/browser/LoadUrlParams;Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;Lorg/chromium/chrome/browser/Tab;Landroid/content/Intent;)Lcom/google/android/apps/chrome/tab/ChromeTab;
    .locals 6

    .prologue
    const/4 v4, -0x1

    .line 76
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;->mTabModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    invoke-interface {v0, p3}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->indexOf(Lorg/chromium/chrome/browser/Tab;)I

    move-result v0

    .line 77
    if-eq v0, v4, :cond_0

    .line 78
    add-int/lit8 v4, v0, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;->createNewTab(Lorg/chromium/content_public/browser/LoadUrlParams;Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;Lorg/chromium/chrome/browser/Tab;ILandroid/content/Intent;)Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    .line 80
    :goto_0
    return-object v0

    :cond_0
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;->createNewTab(Lorg/chromium/content_public/browser/LoadUrlParams;Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;Lorg/chromium/chrome/browser/Tab;ILandroid/content/Intent;)Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    goto :goto_0
.end method

.method public createTabWithContentViewCore(Lorg/chromium/content/browser/ContentViewCore;ILorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;)Lcom/google/android/apps/chrome/tab/ChromeTab;
    .locals 9

    .prologue
    .line 178
    iget-object v8, p0, Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;->mTabModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    .line 181
    invoke-interface {v8, p2}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->isClosurePending(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 194
    :goto_0
    return-object v0

    .line 183
    :cond_0
    invoke-static {v8, p2}, Lorg/chromium/chrome/browser/tabmodel/TabModelUtils;->getTabIndexById(Lorg/chromium/chrome/browser/tabmodel/TabList;I)I

    move-result v0

    .line 187
    if-ltz v0, :cond_2

    add-int/lit8 v0, v0, 0x1

    move v7, v0

    .line 189
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;->mOrderController:Lorg/chromium/chrome/browser/tabmodel/TabModelOrderController;

    iget-boolean v1, p0, Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;->mIncognito:Z

    invoke-virtual {v0, p3, v1}, Lorg/chromium/chrome/browser/tabmodel/TabModelOrderController;->willOpenInForeground(Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;Z)Z

    move-result v4

    .line 190
    const/4 v0, -0x1

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    iget-boolean v2, p0, Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;->mIncognito:Z

    iget-object v3, p0, Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;->mNativeWindow:Lorg/chromium/ui/base/WindowAndroid;

    if-nez v4, :cond_1

    const/4 v6, 0x1

    :goto_2
    move-object v4, p3

    move v5, p2

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/chrome/tab/ChromeTab;->createLiveTab(ILcom/google/android/apps/chrome/ChromeActivity;ZLorg/chromium/ui/base/WindowAndroid;Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;IZ)Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    .line 192
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;->mTabContentManager:Lcom/google/android/apps/chrome/compositor/TabContentManager;

    invoke-virtual {v0, p1, v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->initialize(Lorg/chromium/content/browser/ContentViewCore;Lcom/google/android/apps/chrome/compositor/TabContentManager;)V

    .line 193
    invoke-interface {v8, v0, v7, p3}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->addTab(Lorg/chromium/chrome/browser/Tab;ILorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;)V

    goto :goto_0

    .line 190
    :cond_1
    const/4 v6, 0x0

    goto :goto_2

    :cond_2
    move v7, v0

    goto :goto_1
.end method

.method public createTabWithNativeContents(JILorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;)Lcom/google/android/apps/chrome/tab/ChromeTab;
    .locals 11

    .prologue
    .line 150
    iget-object v8, p0, Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;->mTabModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    .line 153
    invoke-interface {v8, p3}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->isClosurePending(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 166
    :goto_0
    return-object v0

    .line 155
    :cond_0
    invoke-static {v8, p3}, Lorg/chromium/chrome/browser/tabmodel/TabModelUtils;->getTabIndexById(Lorg/chromium/chrome/browser/tabmodel/TabList;I)I

    move-result v0

    .line 159
    if-ltz v0, :cond_3

    add-int/lit8 v0, v0, 0x1

    move v7, v0

    .line 161
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;->mOrderController:Lorg/chromium/chrome/browser/tabmodel/TabModelOrderController;

    iget-boolean v1, p0, Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;->mIncognito:Z

    invoke-virtual {v0, p4, v1}, Lorg/chromium/chrome/browser/tabmodel/TabModelOrderController;->willOpenInForeground(Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;Z)Z

    move-result v9

    .line 162
    const/4 v0, -0x1

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    iget-boolean v2, p0, Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;->mIncognito:Z

    iget-object v3, p0, Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;->mNativeWindow:Lorg/chromium/ui/base/WindowAndroid;

    if-nez v9, :cond_1

    const/4 v6, 0x1

    :goto_2
    move-object v4, p4

    move v5, p3

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/chrome/tab/ChromeTab;->createLiveTab(ILcom/google/android/apps/chrome/ChromeActivity;ZLorg/chromium/ui/base/WindowAndroid;Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;IZ)Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v1

    .line 164
    iget-object v2, p0, Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;->mTabContentManager:Lcom/google/android/apps/chrome/compositor/TabContentManager;

    if-nez v9, :cond_2

    const/4 v0, 0x1

    :goto_3
    invoke-virtual {v1, p1, p2, v2, v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->initialize(JLcom/google/android/apps/chrome/compositor/TabContentManager;Z)V

    .line 165
    invoke-interface {v8, v1, v7, p4}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->addTab(Lorg/chromium/chrome/browser/Tab;ILorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;)V

    move-object v0, v1

    .line 166
    goto :goto_0

    .line 162
    :cond_1
    const/4 v6, 0x0

    goto :goto_2

    .line 164
    :cond_2
    const/4 v0, 0x0

    goto :goto_3

    :cond_3
    move v7, v0

    goto :goto_1
.end method

.method public launchNTP()Lcom/google/android/apps/chrome/tab/ChromeTab;
    .locals 2

    .prologue
    .line 202
    invoke-static {}, Lorg/chromium/base/TraceEvent;->begin()V

    .line 203
    const-string/jumbo v0, "chrome-native://newtab/"

    sget-object v1, Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;->FROM_MENU_OR_OVERVIEW:Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;->launchUrl(Ljava/lang/String;Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;)Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    .line 205
    invoke-static {}, Lorg/chromium/base/TraceEvent;->end()V

    .line 206
    return-object v0
.end method

.method public launchUrl(Ljava/lang/String;Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;)Lcom/google/android/apps/chrome/tab/ChromeTab;
    .locals 1

    .prologue
    .line 219
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;->launchUrl(Ljava/lang/String;Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;Landroid/content/Intent;)Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    return-object v0
.end method

.method public launchUrl(Ljava/lang/String;Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;Landroid/content/Intent;)Lcom/google/android/apps/chrome/tab/ChromeTab;
    .locals 2

    .prologue
    .line 233
    new-instance v0, Lorg/chromium/content_public/browser/LoadUrlParams;

    invoke-direct {v0, p1}, Lorg/chromium/content_public/browser/LoadUrlParams;-><init>(Ljava/lang/String;)V

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p2, v1, p3}, Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;->createNewTab(Lorg/chromium/content_public/browser/LoadUrlParams;Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;Lorg/chromium/chrome/browser/Tab;Landroid/content/Intent;)Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    return-object v0
.end method

.method public launchUrlFromExternalApp(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLandroid/content/Intent;)Lcom/google/android/apps/chrome/tab/ChromeTab;
    .locals 8

    .prologue
    const/4 v3, 0x0

    const/4 v6, 0x0

    .line 250
    sget-boolean v0, Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;->mIncognito:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 251
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeActivity;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-static {p3, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    .line 252
    if-eqz p4, :cond_1

    if-nez v0, :cond_1

    .line 256
    new-instance v0, Lorg/chromium/content_public/browser/LoadUrlParams;

    invoke-direct {v0, p1}, Lorg/chromium/content_public/browser/LoadUrlParams;-><init>(Ljava/lang/String;)V

    .line 257
    invoke-virtual {v0, p2}, Lorg/chromium/content_public/browser/LoadUrlParams;->setVerbatimHeaders(Ljava/lang/String;)V

    .line 258
    sget-object v1, Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;->FROM_EXTERNAL_APP:Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;

    invoke-virtual {p0, v0, v1, v3, p5}, Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;->createNewTab(Lorg/chromium/content_public/browser/LoadUrlParams;Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;Lorg/chromium/chrome/browser/Tab;Landroid/content/Intent;)Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    .line 284
    :goto_0
    return-object v0

    .line 261
    :cond_1
    if-nez p3, :cond_2

    .line 264
    const-string/jumbo p3, "com.google.android.apps.chrome.unknown_app"

    :cond_2
    move v4, v6

    .line 267
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;->mTabModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->getCount()I

    move-result v0

    if-ge v4, v0, :cond_4

    .line 268
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;->mTabModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    invoke-interface {v0, v4}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->getTabAt(I)Lorg/chromium/chrome/browser/Tab;

    move-result-object v7

    .line 269
    invoke-static {v7}, Lcom/google/android/apps/chrome/tab/ChromeTab;->fromTab(Lorg/chromium/chrome/browser/Tab;)Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getAppAssociatedWith()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 273
    new-instance v1, Lorg/chromium/content_public/browser/LoadUrlParams;

    invoke-direct {v1, p1}, Lorg/chromium/content_public/browser/LoadUrlParams;-><init>(Ljava/lang/String;)V

    sget-object v2, Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;->FROM_EXTERNAL_APP:Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;

    move-object v0, p0

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;->createNewTab(Lorg/chromium/content_public/browser/LoadUrlParams;Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;Lorg/chromium/chrome/browser/Tab;ILandroid/content/Intent;)Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    .line 275
    invoke-virtual {v0, p3}, Lcom/google/android/apps/chrome/tab/ChromeTab;->associateWithApp(Ljava/lang/String;)V

    .line 276
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;->mTabModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    invoke-interface {v1, v7, v6, v6, v6}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->closeTab(Lorg/chromium/chrome/browser/Tab;ZZZ)Z

    goto :goto_0

    .line 267
    :cond_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 282
    :cond_4
    sget-object v0, Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;->FROM_EXTERNAL_APP:Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;

    invoke-virtual {p0, p1, v0, p5}, Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;->launchUrl(Ljava/lang/String;Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;Landroid/content/Intent;)Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    .line 283
    invoke-virtual {v0, p3}, Lcom/google/android/apps/chrome/tab/ChromeTab;->associateWithApp(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setTabModel(Lorg/chromium/chrome/browser/tabmodel/TabModel;Lcom/google/android/apps/chrome/compositor/TabContentManager;)V
    .locals 0

    .prologue
    .line 332
    iput-object p1, p0, Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;->mTabModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    .line 333
    iput-object p2, p0, Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;->mTabContentManager:Lcom/google/android/apps/chrome/compositor/TabContentManager;

    .line 334
    return-void
.end method
