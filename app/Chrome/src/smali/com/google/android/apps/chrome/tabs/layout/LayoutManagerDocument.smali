.class public Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;
.super Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;
.source "LayoutManagerDocument.java"


# static fields
.field private static final NOTIFICATIONS:[I


# instance fields
.field private mContextualSearchDelegate:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManagementDelegate;

.field private final mContextualSearchEdgeSwipeHandler:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;

.field private final mContextualSearchEventFilter:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter;

.field protected final mContextualSearchLayout:Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;

.field private final mContextualSearchState:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;

.field protected final mGestureHandler:Lcom/google/android/apps/chrome/eventfilter/GestureHandler;

.field private final mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

.field private final mStaticEdgeEventFilter:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;

.field protected final mStaticLayout:Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;

.field private final mTabCache:Landroid/util/SparseArray;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 315
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;->NOTIFICATIONS:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x4b
        0x16
        0x3f
    .end array-data
.end method

.method public constructor <init>(Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;Lcom/google/android/apps/chrome/eventfilter/EventFilterHost;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 65
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;-><init>(Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;)V

    .line 55
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;->mTabCache:Landroid/util/SparseArray;

    .line 321
    new-instance v0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument$1;-><init>(Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    .line 66
    invoke-interface {p1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 67
    invoke-interface {p1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;->getLayoutRenderHost()Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;

    move-result-object v3

    .line 69
    new-instance v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;->mContextualSearchState:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;

    .line 72
    new-instance v0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument$ContextualSearchEdgeSwipeHandler;

    invoke-direct {v0, p0, p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument$ContextualSearchEdgeSwipeHandler;-><init>(Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;Lcom/google/android/apps/chrome/tabs/layout/LayoutProvider;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;->mContextualSearchEdgeSwipeHandler:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;

    .line 73
    new-instance v0, Lcom/google/android/apps/chrome/tabs/layout/GestureHandlerLayoutDelegate;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/tabs/layout/GestureHandlerLayoutDelegate;-><init>(Lcom/google/android/apps/chrome/tabs/layout/LayoutProvider;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;->mGestureHandler:Lcom/google/android/apps/chrome/eventfilter/GestureHandler;

    .line 76
    new-instance v0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;

    new-instance v2, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument$StaticEdgeSwipeHandler;

    invoke-direct {v2, p0, v6}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument$StaticEdgeSwipeHandler;-><init>(Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument$1;)V

    invoke-direct {v0, v1, p2, v2}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;-><init>(Landroid/content/Context;Lcom/google/android/apps/chrome/eventfilter/EventFilterHost;Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;->mStaticEdgeEventFilter:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;

    .line 78
    new-instance v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter;

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;->mGestureHandler:Lcom/google/android/apps/chrome/eventfilter/GestureHandler;

    iget-object v4, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;->mContextualSearchState:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;

    invoke-direct {v0, v1, p2, v2, v4}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter;-><init>(Landroid/content/Context;Lcom/google/android/apps/chrome/eventfilter/EventFilterHost;Lcom/google/android/apps/chrome/eventfilter/GestureHandler;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;->mContextualSearchEventFilter:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter;

    .line 82
    new-instance v0, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;->mStaticEdgeEventFilter:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;

    invoke-direct {v0, v1, p0, v3, v2}, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;-><init>(Landroid/content/Context;Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;Lcom/google/android/apps/chrome/eventfilter/EventFilter;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;->mStaticLayout:Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;

    .line 83
    new-instance v0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;

    iget-object v4, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;->mContextualSearchEventFilter:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter;

    iget-object v5, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;->mContextualSearchState:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;-><init>(Landroid/content/Context;Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;Lcom/google/android/apps/chrome/eventfilter/EventFilter;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;->mContextualSearchLayout:Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;

    .line 87
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;->mStaticLayout:Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->setLayoutHandlesTabLifecycles(Z)V

    .line 89
    invoke-virtual {p0, v6}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;->setNextLayout(Lcom/google/android/apps/chrome/tabs/layout/Layout;)V

    .line 90
    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;)Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManagementDelegate;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;->mContextualSearchDelegate:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManagementDelegate;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;Z)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;->showContextualSearchLayout(Z)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;->handleTapContextualSearchControl()V

    return-void
.end method

.method private handleTapContextualSearchControl()V
    .locals 2

    .prologue
    .line 243
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;->mContextualSearchLayout:Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;

    if-ne v0, v1, :cond_1

    .line 257
    :cond_0
    :goto_0
    return-void

    .line 244
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;->mContextualSearchDelegate:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManagementDelegate;

    if-eqz v0, :cond_0

    .line 246
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;->mContextualSearchDelegate:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManagementDelegate;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManagementDelegate;->isRunningInCompatibilityMode()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 247
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;->mContextualSearchDelegate:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManagementDelegate;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManagementDelegate;->openResolvedSearchUrlInNewTab()V

    goto :goto_0

    .line 249
    :cond_2
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;->showContextualSearchLayout(Z)V

    .line 254
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;->mContextualSearchLayout:Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;

    sget-object v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->SEARCH_BAR_TAP:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->expandPanel(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;)V

    goto :goto_0
.end method

.method private showContextualSearchLayout(Z)V
    .locals 1

    .prologue
    .line 260
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;->mContextualSearchDelegate:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManagementDelegate;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManagementDelegate;->preserveBasePageSelectionOnNextLossOfFocus()V

    .line 261
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;->mContextualSearchLayout:Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;

    invoke-virtual {p0, v0, p1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;->startShowing(Lcom/google/android/apps/chrome/tabs/layout/Layout;Z)V

    .line 262
    return-void
.end method


# virtual methods
.method public createLayoutTab(IZZZFF)Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 193
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;->mTabCache:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    .line 194
    if-nez v0, :cond_2

    .line 195
    new-instance v0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    iget v3, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;->mLastContentWidthDp:F

    iget v4, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;->mLastContentHeightDp:F

    move v1, p1

    move v2, p2

    move v5, p3

    move v6, p4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;-><init>(IZFFZZ)V

    .line 197
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;->mTabCache:Landroid/util/SparseArray;

    invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 201
    :goto_0
    cmpl-float v1, p5, v7

    if-lez v1, :cond_0

    invoke-virtual {v0, p5}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setMaxContentWidth(F)V

    .line 202
    :cond_0
    cmpl-float v1, p6, v7

    if-lez v1, :cond_1

    invoke-virtual {v0, p6}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setMaxContentHeight(F)V

    .line 204
    :cond_1
    return-object v0

    .line 199
    :cond_2
    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;->mLastContentWidthDp:F

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;->mLastContentHeightDp:F

    invoke-virtual {v0, v1, v2, p3, p4}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->init(FFZZ)V

    goto :goto_0
.end method

.method public destroy()V
    .locals 3

    .prologue
    .line 122
    invoke-super {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->destroy()V

    .line 124
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;->mHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;->NOTIFICATIONS:[I

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->unregisterForNotifications(Landroid/content/Context;[ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    .line 126
    return-void
.end method

.method protected emptyCachesExcept(I)V
    .locals 2

    .prologue
    .line 142
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;->mTabCache:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    .line 143
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;->mTabCache:Landroid/util/SparseArray;

    invoke-virtual {v1}, Landroid/util/SparseArray;->clear()V

    .line 144
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;->mTabCache:Landroid/util/SparseArray;

    invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 145
    :cond_0
    return-void
.end method

.method public getContextualSearchEdgeSwipeHandler()Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;->mContextualSearchEdgeSwipeHandler:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;

    return-object v0
.end method

.method protected getDefaultLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;->mStaticLayout:Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;

    return-object v0
.end method

.method protected getExistingLayoutTab(I)Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;->mTabCache:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    return-object v0
.end method

.method public init(Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreatorManager;Lcom/google/android/apps/chrome/compositor/TabContentManager;Landroid/view/ViewGroup;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManagementDelegate;)V
    .locals 3

    .prologue
    .line 97
    iput-object p5, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;->mContextualSearchDelegate:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManagementDelegate;

    .line 100
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;->mStaticEdgeEventFilter:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter;->setTabModelSelector(Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;)V

    .line 101
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;->mContextualSearchEventFilter:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter;

    invoke-virtual {v0, p5}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter;->setContextualSearchManagementDelegate(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManagementDelegate;)V

    .line 105
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;->mStaticLayout:Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;

    invoke-virtual {v0, p1, p3}, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->setTabModelSelector(Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;Lcom/google/android/apps/chrome/compositor/TabContentManager;)V

    .line 106
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;->mContextualSearchLayout:Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;

    invoke-virtual {v0, p1, p3}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->setTabModelSelector(Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;Lcom/google/android/apps/chrome/compositor/TabContentManager;)V

    .line 107
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;->mContextualSearchLayout:Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;

    invoke-virtual {v0, p5}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->setContextualSearchManagementDelegate(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManagementDelegate;)V

    .line 110
    if-eqz p5, :cond_0

    .line 111
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;->mContextualSearchLayout:Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;

    invoke-interface {p5, v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManagementDelegate;->setContextualSearchLayoutDelegate(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchLayoutDelegate;)V

    .line 114
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;->mHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;->NOTIFICATIONS:[I

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->registerForNotifications(Landroid/content/Context;[ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    .line 117
    invoke-super/range {p0 .. p5}, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->init(Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreatorManager;Lcom/google/android/apps/chrome/compositor/TabContentManager;Landroid/view/ViewGroup;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManagementDelegate;)V

    .line 118
    return-void
.end method

.method public initLayoutTabFromHost(I)V
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 171
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;->getTabModelSelector()Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    if-nez v0, :cond_1

    .line 188
    :cond_0
    :goto_0
    return-void

    .line 173
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;->getTabModelSelector()Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    move-result-object v0

    .line 174
    invoke-interface {v0, p1}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->getTabById(I)Lorg/chromium/chrome/browser/Tab;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->fromTab(Lorg/chromium/chrome/browser/Tab;)Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v4

    .line 175
    if-eqz v4, :cond_0

    .line 177
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;->mTabCache:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    .line 178
    if-eqz v0, :cond_0

    .line 180
    invoke-virtual {v4}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getUrl()Ljava/lang/String;

    move-result-object v1

    .line 181
    if-eqz v1, :cond_2

    const-string/jumbo v5, "chrome-native://"

    invoke-virtual {v1, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    move v1, v2

    .line 182
    :goto_1
    invoke-virtual {v4}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v5

    if-eqz v5, :cond_3

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/tab/ChromeTab;->isShowingSadTab()Z

    move-result v5

    if-nez v5, :cond_3

    if-nez v1, :cond_3

    .line 184
    :goto_2
    invoke-virtual {v4}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getBackgroundColor()I

    move-result v1

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getFallbackTextureId()I

    move-result v3

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/tab/ChromeTab;->shouldStall()Z

    move-result v4

    invoke-virtual {v0, v1, v3, v4, v2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->initFromHost(IIZZ)V

    .line 187
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;->mHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;->requestRender()V

    goto :goto_0

    :cond_2
    move v1, v3

    .line 181
    goto :goto_1

    :cond_3
    move v2, v3

    .line 182
    goto :goto_2
.end method

.method public onBackPressed()Z
    .locals 1

    .prologue
    .line 239
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->onBackPressed()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onEndGesture()V
    .locals 0

    .prologue
    .line 232
    return-void
.end method

.method public onStartGesture()V
    .locals 1

    .prologue
    .line 225
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->unstallImmediately()V

    .line 226
    return-void
.end method

.method protected onViewportChanged(Landroid/graphics/RectF;)V
    .locals 4

    .prologue
    .line 130
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->onViewportChanged(Landroid/graphics/RectF;)V

    .line 131
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;->mTabCache:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 133
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;->mTabCache:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    invoke-virtual {p1}, Landroid/graphics/RectF;->width()F

    move-result v2

    invoke-virtual {p1}, Landroid/graphics/RectF;->height()F

    move-result v3

    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setContentSize(FF)V

    .line 131
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 135
    :cond_0
    return-void
.end method

.method public releaseTabLayout(I)V
    .locals 1

    .prologue
    .line 209
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;->mTabCache:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->remove(I)V

    .line 210
    return-void
.end method

.method protected setStripLayoutHelperManager(Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;)V
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;->mStaticLayout:Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->setStripLayoutHelperManager(Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;)V

    .line 153
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;->mContextualSearchLayout:Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->setStripLayoutHelperManager(Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;)V

    .line 154
    return-void
.end method
