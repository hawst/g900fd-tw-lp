.class Lcom/google/android/apps/chrome/ntp/NewTabPageView$11;
.super Ljava/lang/Object;
.source "NewTabPageView.java"

# interfaces
.implements Lorg/chromium/chrome/browser/LogoBridge$LogoObserver;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/ntp/NewTabPageView;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/ntp/NewTabPageView;)V
    .locals 0

    .prologue
    .line 420
    iput-object p1, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView$11;->this$0:Lcom/google/android/apps/chrome/ntp/NewTabPageView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLogoAvailable(Lorg/chromium/chrome/browser/LogoBridge$Logo;Z)V
    .locals 2

    .prologue
    .line 423
    if-nez p1, :cond_0

    if-eqz p2, :cond_0

    .line 427
    :goto_0
    return-void

    .line 424
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView$11;->this$0:Lcom/google/android/apps/chrome/ntp/NewTabPageView;

    # getter for: Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mSearchProviderLogoView:Lcom/google/android/apps/chrome/ntp/LogoView;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->access$800(Lcom/google/android/apps/chrome/ntp/NewTabPageView;)Lcom/google/android/apps/chrome/ntp/LogoView;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView$11;->this$0:Lcom/google/android/apps/chrome/ntp/NewTabPageView;

    # getter for: Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mManager:Lcom/google/android/apps/chrome/ntp/NewTabPageView$NewTabPageManager;
    invoke-static {v1}, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->access$000(Lcom/google/android/apps/chrome/ntp/NewTabPageView;)Lcom/google/android/apps/chrome/ntp/NewTabPageView$NewTabPageManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/ntp/LogoView;->setMananger(Lcom/google/android/apps/chrome/ntp/NewTabPageView$NewTabPageManager;)V

    .line 425
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView$11;->this$0:Lcom/google/android/apps/chrome/ntp/NewTabPageView;

    # getter for: Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mSearchProviderLogoView:Lcom/google/android/apps/chrome/ntp/LogoView;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->access$800(Lcom/google/android/apps/chrome/ntp/NewTabPageView;)Lcom/google/android/apps/chrome/ntp/LogoView;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/ntp/LogoView;->updateLogo(Lorg/chromium/chrome/browser/LogoBridge$Logo;)V

    .line 426
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView$11;->this$0:Lcom/google/android/apps/chrome/ntp/NewTabPageView;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mSnapshotMostVisitedChanged:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->access$902(Lcom/google/android/apps/chrome/ntp/NewTabPageView;Z)Z

    goto :goto_0
.end method
