.class public Lcom/google/android/apps/chrome/document/BrandColorUtils;
.super Ljava/lang/Object;
.source "BrandColorUtils.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getLightnessForColor(I)F
    .locals 4

    .prologue
    .line 27
    invoke-static {p0}, Landroid/graphics/Color;->red(I)I

    move-result v0

    .line 28
    invoke-static {p0}, Landroid/graphics/Color;->green(I)I

    move-result v1

    .line 29
    invoke-static {p0}, Landroid/graphics/Color;->blue(I)I

    move-result v2

    .line 30
    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v3

    invoke-static {v0, v3}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 31
    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 32
    add-int/2addr v0, v3

    div-int/lit8 v0, v0, 0x2

    .line 33
    int-to-float v0, v0

    const/high16 v1, 0x437f0000    # 255.0f

    div-float/2addr v0, v1

    return v0
.end method

.method public static isDefaultPrimaryColor(I)Z
    .locals 2

    .prologue
    .line 62
    invoke-static {}, Lorg/chromium/base/ApplicationStatus;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 63
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$color;->default_primary_color:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static shouldUseLightDrawablesForToolbar(F)Z
    .locals 1

    .prologue
    .line 43
    const/high16 v0, 0x3f000000    # 0.5f

    cmpg-float v0, p0, v0

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static shouldUseOpaqueTextboxBackground(F)Z
    .locals 1

    .prologue
    .line 53
    const v0, 0x3f51eb85    # 0.82f

    cmpl-float v0, p0, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
