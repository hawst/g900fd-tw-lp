.class public Lcom/google/android/apps/chrome/ntp/NewTabPageToolbar;
.super Landroid/widget/LinearLayout;
.source "NewTabPageToolbar.java"

# interfaces
.implements Landroid/view/View$OnLongClickListener;


# instance fields
.field private mBookmarksButton:Landroid/view/View;

.field private mRecentTabsButton:Landroid/view/View;

.field private mToast:Landroid/widget/Toast;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 35
    return-void
.end method

.method private initButton(I)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 52
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/ntp/NewTabPageToolbar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 54
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ntp/NewTabPageToolbar;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lorg/chromium/ui/base/DeviceFormFactor;->isTablet(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 56
    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 57
    const-string/jumbo v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 58
    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setCompoundDrawablePadding(I)V

    .line 59
    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 62
    :cond_0
    return-object v0
.end method

.method private showTooltip(I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 81
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageToolbar;->mToast:Landroid/widget/Toast;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageToolbar;->mToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->cancel()V

    .line 82
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ntp/NewTabPageToolbar;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 83
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageToolbar;->mToast:Landroid/widget/Toast;

    .line 84
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageToolbar;->mToast:Landroid/widget/Toast;

    const/16 v1, 0x51

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ntp/NewTabPageToolbar;->getHeight()I

    move-result v2

    invoke-virtual {v0, v1, v3, v2}, Landroid/widget/Toast;->setGravity(III)V

    .line 85
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageToolbar;->mToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 86
    return-void
.end method


# virtual methods
.method public getBookmarksButton()Landroid/view/View;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageToolbar;->mBookmarksButton:Landroid/view/View;

    return-object v0
.end method

.method public getRecentTabsButton()Landroid/view/View;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageToolbar;->mRecentTabsButton:Landroid/view/View;

    return-object v0
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 47
    sget v0, Lcom/google/android/apps/chrome/R$id;->bookmarks_button:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/ntp/NewTabPageToolbar;->initButton(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageToolbar;->mBookmarksButton:Landroid/view/View;

    .line 48
    sget v0, Lcom/google/android/apps/chrome/R$id;->recent_tabs_button:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/ntp/NewTabPageToolbar;->initButton(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageToolbar;->mRecentTabsButton:Landroid/view/View;

    .line 49
    return-void
.end method

.method public onLongClick(Landroid/view/View;)Z
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageToolbar;->mBookmarksButton:Landroid/view/View;

    if-ne p1, v0, :cond_1

    .line 69
    sget v0, Lcom/google/android/apps/chrome/R$string;->ntp_bookmarks:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/ntp/NewTabPageToolbar;->showTooltip(I)V

    .line 73
    :cond_0
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 70
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageToolbar;->mRecentTabsButton:Landroid/view/View;

    if-ne p1, v0, :cond_0

    .line 71
    sget v0, Lcom/google/android/apps/chrome/R$string;->recent_tabs:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/ntp/NewTabPageToolbar;->showTooltip(I)V

    goto :goto_0
.end method
