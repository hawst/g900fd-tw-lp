.class Lcom/google/android/apps/chrome/ntp/SignInPromoView$1;
.super Ljava/lang/Object;
.source "SignInPromoView.java"

# interfaces
.implements Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView$Listener;


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/ntp/SignInPromoView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 77
    const-class v0, Lcom/google/android/apps/chrome/ntp/SignInPromoView;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/ntp/SignInPromoView$1;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lcom/google/android/apps/chrome/ntp/SignInPromoView;)V
    .locals 0

    .prologue
    .line 77
    iput-object p1, p0, Lcom/google/android/apps/chrome/ntp/SignInPromoView$1;->this$0:Lcom/google/android/apps/chrome/ntp/SignInPromoView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAccountSelectionCanceled()V
    .locals 2

    .prologue
    .line 86
    sget-boolean v0, Lcom/google/android/apps/chrome/ntp/SignInPromoView$1;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    const-string/jumbo v1, "Button should be hidden"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 87
    :cond_0
    return-void
.end method

.method public onAccountSelectionConfirmed(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/SignInPromoView$1;->this$0:Lcom/google/android/apps/chrome/ntp/SignInPromoView;

    # getter for: Lcom/google/android/apps/chrome/ntp/SignInPromoView;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/SignInPromoView;->access$000(Lcom/google/android/apps/chrome/ntp/SignInPromoView;)Landroid/app/Activity;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/google/android/apps/chrome/sync/ui/ConfirmAccountChangeFragment;->confirmSyncAccount(Ljava/lang/String;Landroid/app/Activity;)V

    .line 82
    return-void
.end method

.method public onFailedToSetForcedAccount(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 107
    sget-boolean v0, Lcom/google/android/apps/chrome/ntp/SignInPromoView$1;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    const-string/jumbo v1, "No forced accounts in SignInPromoView"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 108
    :cond_0
    return-void
.end method

.method public onNewAccount()V
    .locals 3

    .prologue
    .line 91
    new-instance v0, Lcom/google/android/apps/chrome/services/AccountAdder;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/services/AccountAdder;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/SignInPromoView$1;->this$0:Lcom/google/android/apps/chrome/ntp/SignInPromoView;

    # getter for: Lcom/google/android/apps/chrome/ntp/SignInPromoView;->mActivity:Landroid/app/Activity;
    invoke-static {v1}, Lcom/google/android/apps/chrome/ntp/SignInPromoView;->access$000(Lcom/google/android/apps/chrome/ntp/SignInPromoView;)Landroid/app/Activity;

    move-result-object v1

    const/16 v2, 0x66

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/services/AccountAdder;->addAccount(Landroid/app/Activity;I)V

    .line 92
    return-void
.end method

.method public onSettingsButtonClicked(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 101
    sget-boolean v0, Lcom/google/android/apps/chrome/ntp/SignInPromoView$1;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    const-string/jumbo v1, "Button should be hidden"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 102
    :cond_0
    return-void
.end method

.method public onSigningInCompleted(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 96
    sget-boolean v0, Lcom/google/android/apps/chrome/ntp/SignInPromoView$1;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    const-string/jumbo v1, "Button should be hidden"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 97
    :cond_0
    return-void
.end method
