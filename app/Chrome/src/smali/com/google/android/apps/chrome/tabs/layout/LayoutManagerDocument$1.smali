.class Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument$1;
.super Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;
.source "LayoutManagerDocument.java"


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;)V
    .locals 0

    .prologue
    .line 322
    iput-object p1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument$1;->this$0:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3

    .prologue
    .line 325
    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    .line 335
    :goto_0
    return-void

    .line 327
    :sswitch_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument$1;->this$0:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;

    # invokes: Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;->handleTapContextualSearchControl()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;->access$300(Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;)V

    goto :goto_0

    .line 332
    :sswitch_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument$1;->this$0:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string/jumbo v2, "tabId"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;->initLayoutTabFromHost(I)V

    goto :goto_0

    .line 325
    nop

    :sswitch_data_0
    .sparse-switch
        0x16 -> :sswitch_1
        0x3f -> :sswitch_1
        0x4b -> :sswitch_0
    .end sparse-switch
.end method
