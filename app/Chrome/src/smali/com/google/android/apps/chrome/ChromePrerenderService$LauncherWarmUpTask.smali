.class Lcom/google/android/apps/chrome/ChromePrerenderService$LauncherWarmUpTask;
.super Landroid/os/AsyncTask;
.source "ChromePrerenderService.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/chrome/ChromePrerenderService$1;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/google/android/apps/chrome/ChromePrerenderService$LauncherWarmUpTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 37
    check-cast p1, [Landroid/content/Context;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/ChromePrerenderService$LauncherWarmUpTask;->doInBackground([Landroid/content/Context;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Landroid/content/Context;)Ljava/lang/Void;
    .locals 1

    .prologue
    .line 40
    const/4 v0, 0x0

    aget-object v0, p1, v0

    invoke-static {v0}, Lorg/chromium/content/browser/ChildProcessLauncher;->warmUp(Landroid/content/Context;)V

    .line 41
    const/4 v0, 0x0

    return-object v0
.end method
