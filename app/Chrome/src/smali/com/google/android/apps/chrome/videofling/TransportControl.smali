.class public abstract Lcom/google/android/apps/chrome/videofling/TransportControl;
.super Ljava/lang/Object;
.source "TransportControl.java"


# instance fields
.field private mError:Ljava/lang/String;

.field private final mListeners:Ljava/util/Set;

.field private mPosterBitmap:Landroid/graphics/Bitmap;

.field private mScreenName:Ljava/lang/String;

.field protected mVideoInfo:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/videofling/TransportControl;->mListeners:Ljava/util/Set;

    return-void
.end method

.method private static equal(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 183
    if-nez p0, :cond_1

    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    invoke-virtual {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public addListener(Lcom/google/android/apps/chrome/videofling/TransportControl$Listener;)V
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/TransportControl;->mListeners:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 146
    return-void
.end method

.method public final getError()Ljava/lang/String;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/TransportControl;->mError:Ljava/lang/String;

    return-object v0
.end method

.method protected final getListeners()Ljava/util/Set;
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/TransportControl;->mListeners:Ljava/util/Set;

    return-object v0
.end method

.method public final getPosterBitmap()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/TransportControl;->mPosterBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public final getScreenName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/TransportControl;->mScreenName:Ljava/lang/String;

    return-object v0
.end method

.method public final getVideoInfo()Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/TransportControl;->mVideoInfo:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;

    return-object v0
.end method

.method public final hasError()Z
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/TransportControl;->mError:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onErrorChanged()V
    .locals 0

    .prologue
    .line 178
    return-void
.end method

.method protected onPosterBitmapChanged()V
    .locals 0

    .prologue
    .line 180
    return-void
.end method

.method protected onScreenNameChanged()V
    .locals 0

    .prologue
    .line 177
    return-void
.end method

.method protected onVideoInfoChanged()V
    .locals 0

    .prologue
    .line 179
    return-void
.end method

.method public final setError(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/TransportControl;->mError:Ljava/lang/String;

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 84
    :goto_0
    return-void

    .line 82
    :cond_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 p1, 0x0

    :cond_1
    iput-object p1, p0, Lcom/google/android/apps/chrome/videofling/TransportControl;->mError:Ljava/lang/String;

    .line 83
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/TransportControl;->onErrorChanged()V

    goto :goto_0
.end method

.method public final setPosterBitmap(Landroid/graphics/Bitmap;)V
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/TransportControl;->mPosterBitmap:Landroid/graphics/Bitmap;

    invoke-static {v0, p1}, Lcom/google/android/apps/chrome/videofling/TransportControl;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 139
    :goto_0
    return-void

    .line 137
    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/chrome/videofling/TransportControl;->mPosterBitmap:Landroid/graphics/Bitmap;

    .line 138
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/TransportControl;->onPosterBitmapChanged()V

    goto :goto_0
.end method

.method public abstract setRouteController(Lcom/google/android/apps/chrome/videofling/MediaRouteController;)V
.end method

.method public final setScreenName(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/TransportControl;->mScreenName:Ljava/lang/String;

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 62
    :goto_0
    return-void

    .line 60
    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/chrome/videofling/TransportControl;->mScreenName:Ljava/lang/String;

    .line 61
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/TransportControl;->onScreenNameChanged()V

    goto :goto_0
.end method

.method public final setVideoInfo(Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;)V
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/TransportControl;->mVideoInfo:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;

    invoke-static {v0, p1}, Lcom/google/android/apps/chrome/videofling/TransportControl;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 119
    :goto_0
    return-void

    .line 117
    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/chrome/videofling/TransportControl;->mVideoInfo:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo;

    .line 118
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/TransportControl;->onVideoInfoChanged()V

    goto :goto_0
.end method

.method public abstract show(Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;)V
.end method
