.class public Lcom/google/android/apps/chrome/compositor/CompositorView;
.super Landroid/view/SurfaceView;
.source "CompositorView.java"

# interfaces
.implements Landroid/view/SurfaceHolder$Callback2;
.implements Lcom/google/android/apps/chrome/compositor/ContentOffsetProvider;


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final mCacheAppRect:Landroid/graphics/Rect;

.field private final mCacheViewPosition:[I

.field private final mCacheViewport:Landroid/graphics/Rect;

.field private final mCacheVisibleViewport:Landroid/graphics/Rect;

.field private mContentReadbackHandler:Lorg/chromium/content/browser/ContentReadbackHandler;

.field private mContextualSearchManager:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;

.field private mEnableCompositorTabStrip:Z

.field private mEnableTabletTabStack:Z

.field private mInstantPageContainer:Lcom/google/android/apps/chrome/compositor/InstantPageContainer;

.field private mLastLayerCount:I

.field private mNativeCompositorView:J

.field private mPreviousWindowTop:I

.field private final mRenderHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;

.field private mResourceManager:Lcom/google/android/apps/chrome/compositor/resources/ResourceManager;

.field private mRootActivityView:Landroid/view/View;

.field private mRootView:Landroid/view/View;

.field private mStaticTabLayerContainer:Lcom/google/android/apps/chrome/compositor/StaticTabLayerContainer;

.field private mTabContentManager:Lcom/google/android/apps/chrome/compositor/TabContentManager;

.field private mTabLayerContainer:Lcom/google/android/apps/chrome/compositor/TabLayerContainer;

.field private mTabStripLayerContainer:Lcom/google/android/apps/chrome/compositor/TabStripLayerContainer;

.field private mTakeSurface:Z

.field private mTakeSurfaceRunnable:Lcom/google/android/apps/chrome/compositor/CompositorView$TakeSurfaceRunnable;

.field private mTransitionPageContainer:Lcom/google/android/apps/chrome/compositor/TransitionPageContainer;

.field private mWindowAndroid:Lorg/chromium/ui/base/WindowAndroid;

.field private mWindowDelegate:Lcom/google/android/apps/chrome/WindowDelegate;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 51
    const-class v0, Lcom/google/android/apps/chrome/compositor/CompositorView;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/compositor/CompositorView;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;)V
    .locals 2

    .prologue
    .line 133
    invoke-direct {p0, p1}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;)V

    .line 56
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mCacheViewport:Landroid/graphics/Rect;

    .line 57
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mCacheAppRect:Landroid/graphics/Rect;

    .line 58
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mCacheVisibleViewport:Landroid/graphics/Rect;

    .line 59
    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mCacheViewPosition:[I

    .line 66
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mPreviousWindowTop:I

    .line 134
    iput-object p2, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mRenderHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;

    .line 135
    invoke-static {}, Lorg/chromium/base/CommandLine;->getInstance()Lorg/chromium/base/CommandLine;

    move-result-object v0

    const-string/jumbo v1, "take-surface"

    invoke-virtual {v0, v1}, Lorg/chromium/base/CommandLine;->hasSwitch(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mTakeSurface:Z

    .line 136
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/compositor/CompositorView;->resetFlags()V

    .line 137
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mTakeSurface:Z

    if-nez v0, :cond_0

    .line 138
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/compositor/CompositorView;->setVisibility(I)V

    .line 139
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/compositor/CompositorView;->setZOrderMediaOverlay(Z)V

    .line 141
    :cond_0
    new-instance v0, Lcom/google/android/apps/chrome/compositor/CompositorView$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/compositor/CompositorView$1;-><init>(Lcom/google/android/apps/chrome/compositor/CompositorView;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mContentReadbackHandler:Lorg/chromium/content/browser/ContentReadbackHandler;

    .line 147
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/compositor/CompositorView;)J
    .locals 2

    .prologue
    .line 51
    iget-wide v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mNativeCompositorView:J

    return-wide v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/compositor/CompositorView;J)V
    .locals 1

    .prologue
    .line 51
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/compositor/CompositorView;->nativeSurfaceCreated(J)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/compositor/CompositorView;)Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mRenderHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/chrome/compositor/CompositorView;JIIILandroid/view/Surface;)V
    .locals 1

    .prologue
    .line 51
    invoke-direct/range {p0 .. p6}, Lcom/google/android/apps/chrome/compositor/CompositorView;->nativeSurfaceChanged(JIIILandroid/view/Surface;)V

    return-void
.end method

.method private native nativeDestroy(J)V
.end method

.method private native nativeFinalizeLayers(J)V
.end method

.method private native nativeGetInstantPageContainer(J)Lcom/google/android/apps/chrome/compositor/InstantPageContainer;
.end method

.method private native nativeGetResourceManager(J)Lcom/google/android/apps/chrome/compositor/resources/ResourceManager;
.end method

.method private native nativeGetStaticTabLayerContainer(J)Lcom/google/android/apps/chrome/compositor/StaticTabLayerContainer;
.end method

.method private native nativeGetTabLayerContainer(J)Lcom/google/android/apps/chrome/compositor/TabLayerContainer;
.end method

.method private native nativeGetTabStripLayerContainer(J)Lcom/google/android/apps/chrome/compositor/TabStripLayerContainer;
.end method

.method private native nativeGetTransitionPageContainer(J)Lcom/google/android/apps/chrome/compositor/TransitionPageContainer;
.end method

.method private native nativeInit(ZZIJLcom/google/android/apps/chrome/compositor/LayerTitleCache;Lcom/google/android/apps/chrome/compositor/TabContentManager;)J
.end method

.method private native nativeSetLayoutViewport(JFFFFFFFF)V
.end method

.method private native nativeSetNeedsComposite(J)V
.end method

.method private native nativeSetOverlayVideoMode(JZ)V
.end method

.method private native nativeSetStaticLayerMode(JZ)V
.end method

.method private native nativeSurfaceChanged(JIIILandroid/view/Surface;)V
.end method

.method private native nativeSurfaceCreated(J)V
.end method

.method private native nativeSurfaceDestroyed(J)V
.end method

.method private native nativeUpdateContextualSearchLayer(JZIIIIIILorg/chromium/content/browser/ContentViewCore;FFFFFZFFFFFZFFFI)V
.end method

.method private native nativeUpdateToolbarLayer(JIFZ)V
.end method

.method private onCompositorLayout()V
    .locals 1

    .prologue
    .line 372
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mRenderHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;->onCompositorLayout()V

    .line 373
    return-void
.end method

.method private onSwapBuffersCompleted(I)V
    .locals 1

    .prologue
    .line 382
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/compositor/CompositorView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mTakeSurface:Z

    if-nez v0, :cond_0

    .line 383
    new-instance v0, Lcom/google/android/apps/chrome/compositor/CompositorView$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/compositor/CompositorView$2;-><init>(Lcom/google/android/apps/chrome/compositor/CompositorView;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/compositor/CompositorView;->post(Ljava/lang/Runnable;)Z

    .line 391
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mRenderHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;

    invoke-interface {v0, p1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;->onSwapBuffersCompleted(I)V

    .line 392
    return-void
.end method

.method private updateContextualSearchLayer(Lcom/google/android/apps/chrome/tabs/layout/Layout;)V
    .locals 30

    .prologue
    .line 426
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->isContextualSearchLayoutShowing()Z

    move-result v6

    .line 428
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->getContextualSearchPanelY()F

    move-result v14

    .line 429
    const/4 v10, 0x0

    .line 430
    const/4 v9, 0x0

    .line 431
    const/4 v8, 0x0

    .line 432
    const/high16 v18, 0x3f800000    # 1.0f

    .line 434
    const/16 v19, 0x1

    .line 435
    const/4 v7, 0x0

    .line 436
    const/4 v5, 0x0

    .line 438
    const/16 v22, 0x0

    .line 439
    const/4 v4, 0x0

    .line 440
    const/16 v24, 0x0

    .line 442
    const/16 v25, 0x0

    .line 443
    const/4 v3, 0x0

    .line 444
    const/4 v2, 0x0

    .line 445
    const/16 v28, 0x0

    .line 446
    const/16 v29, 0x0

    .line 448
    if-eqz v6, :cond_0

    .line 453
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mContextualSearchManager:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->shouldShowContextualSearchView()Z

    move-result v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->setContextualSearchViewVisibility(Z)V

    .line 456
    check-cast p1, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->getContextualSearchState()Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;

    move-result-object v11

    .line 459
    invoke-virtual {v11}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->getWidth()F

    move-result v10

    .line 460
    invoke-virtual {v11}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->getSearchBarMarginTop()F

    move-result v9

    .line 461
    invoke-virtual {v11}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->getSearchBarHeight()F

    move-result v8

    .line 462
    invoke-virtual {v11}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->getSearchBarTextOpacity()F

    move-result v18

    .line 464
    invoke-virtual {v11}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->isSearchBarBorderVisible()Z

    move-result v19

    .line 465
    invoke-virtual {v11}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->getSearchBarBorderY()F

    move-result v7

    .line 466
    invoke-virtual {v11}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->getSearchBarBorderHeight()F

    move-result v5

    .line 468
    invoke-virtual {v11}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->getSearchProviderIconOpacity()F

    move-result v22

    .line 469
    invoke-virtual {v11}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->getSearchIconPaddingLeft()F

    move-result v4

    .line 470
    invoke-virtual {v11}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->getSearchIconOpacity()F

    move-result v24

    .line 472
    invoke-virtual {v11}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->isProgressBarVisible()Z

    move-result v25

    .line 473
    invoke-virtual {v11}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->getProgressBarY()F

    move-result v3

    .line 474
    invoke-virtual {v11}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->getProgressBarHeight()F

    move-result v2

    .line 475
    invoke-virtual {v11}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->getProgressBarOpacity()F

    move-result v28

    .line 476
    invoke-virtual {v11}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->getProgressBarCompletion()I

    move-result v29

    move/from16 v23, v4

    move/from16 v21, v5

    move/from16 v20, v7

    move/from16 v17, v8

    move/from16 v16, v9

    move v15, v10

    .line 479
    :goto_0
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/chrome/compositor/CompositorView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    iget v0, v4, Landroid/util/DisplayMetrics;->density:F

    move/from16 v27, v0

    .line 480
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mNativeCompositorView:J

    sget v7, Lcom/google/android/apps/chrome/R$drawable;->contextual_search_bar_background:I

    sget v8, Lcom/google/android/apps/chrome/R$id;->contextual_search_bar_text:I

    sget v9, Lcom/google/android/apps/chrome/R$drawable;->contextual_search_provider_icon:I

    sget v10, Lcom/google/android/apps/chrome/R$drawable;->contextual_search_icon:I

    sget v11, Lcom/google/android/apps/chrome/R$drawable;->progress_bar_background:I

    sget v12, Lcom/google/android/apps/chrome/R$drawable;->progress_bar_foreground:I

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mContextualSearchManager:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;

    invoke-virtual {v13}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->getSearchContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v13

    mul-float v14, v14, v27

    mul-float v15, v15, v27

    mul-float v16, v16, v27

    mul-float v17, v17, v27

    mul-float v20, v20, v27

    mul-float v21, v21, v27

    mul-float v23, v23, v27

    mul-float v26, v3, v27

    mul-float v27, v27, v2

    move-object/from16 v3, p0

    invoke-direct/range {v3 .. v29}, Lcom/google/android/apps/chrome/compositor/CompositorView;->nativeUpdateContextualSearchLayer(JZIIIIIILorg/chromium/content/browser/ContentViewCore;FFFFFZFFFFFZFFFI)V

    .line 505
    return-void

    :cond_0
    move/from16 v23, v4

    move/from16 v21, v5

    move/from16 v20, v7

    move/from16 v17, v8

    move/from16 v16, v9

    move v15, v10

    goto :goto_0
.end method

.method private updateToolbarLayer(Lcom/google/android/apps/chrome/tabs/layout/LayoutProvider;Z)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 395
    if-nez p2, :cond_0

    invoke-static {}, Lcom/google/android/apps/chrome/device/DeviceClassManager;->enableFullscreen()Z

    move-result v0

    if-nez v0, :cond_1

    .line 423
    :cond_0
    :goto_0
    return-void

    .line 397
    :cond_1
    invoke-interface {p1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutProvider;->getFullscreenManager()Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;

    move-result-object v4

    .line 399
    invoke-virtual {v4}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->getControlOffset()F

    move-result v5

    .line 400
    invoke-virtual {v4}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->drawControlsAsTexture()Z

    move-result v0

    if-nez v0, :cond_2

    cmpl-float v0, v5, v8

    if-nez v0, :cond_4

    :cond_2
    move v0, v2

    .line 402
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/compositor/CompositorView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v6, v3, Landroid/util/DisplayMetrics;->density:F

    .line 403
    invoke-interface {p1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutProvider;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v3

    div-float v7, v5, v6

    invoke-virtual {v3, v7}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->getTopControlsOffset(F)F

    move-result v7

    .line 405
    invoke-static {v7}, Ljava/lang/Float;->isNaN(F)Z

    move-result v3

    if-nez v3, :cond_5

    move v3, v2

    .line 407
    :goto_2
    if-eqz v3, :cond_7

    .line 408
    mul-float v5, v7, v6

    move v6, v2

    .line 412
    :goto_3
    if-eqz v3, :cond_6

    cmpl-float v0, v7, v8

    if-eqz v0, :cond_6

    :goto_4
    invoke-virtual {v4, v2}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->setHideTopControlsAndroidView(Z)V

    .line 415
    invoke-interface {p1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutProvider;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->getSizingFlags()I

    move-result v0

    .line 416
    and-int/lit16 v2, v0, 0x1000

    if-eqz v2, :cond_3

    and-int/lit8 v2, v0, 0x1

    if-nez v2, :cond_3

    and-int/lit16 v0, v0, 0x100

    if-nez v0, :cond_3

    move v6, v1

    .line 422
    :cond_3
    iget-wide v2, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mNativeCompositorView:J

    sget v4, Lcom/google/android/apps/chrome/R$id;->control_container:I

    move-object v1, p0

    invoke-direct/range {v1 .. v6}, Lcom/google/android/apps/chrome/compositor/CompositorView;->nativeUpdateToolbarLayer(JIFZ)V

    goto :goto_0

    :cond_4
    move v0, v1

    .line 400
    goto :goto_1

    :cond_5
    move v3, v1

    .line 405
    goto :goto_2

    :cond_6
    move v2, v1

    .line 412
    goto :goto_4

    :cond_7
    move v6, v0

    goto :goto_3
.end method


# virtual methods
.method public finalizeLayers(Lcom/google/android/apps/chrome/tabs/layout/LayoutProvider;Z)V
    .locals 13

    .prologue
    .line 516
    const-string/jumbo v0, "CompositorView:finalizeLayers"

    invoke-static {v0}, Lorg/chromium/base/TraceEvent;->begin(Ljava/lang/String;)V

    .line 517
    invoke-interface {p1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutProvider;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v12

    .line 518
    if-eqz v12, :cond_0

    iget-wide v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mNativeCompositorView:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 519
    :cond_0
    const-string/jumbo v0, "CompositorView:finalizeLayers"

    invoke-static {v0}, Lorg/chromium/base/TraceEvent;->end(Ljava/lang/String;)V

    .line 582
    :goto_0
    return-void

    .line 524
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mResourceManager:Lcom/google/android/apps/chrome/compositor/resources/ResourceManager;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/compositor/CompositorView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/compositor/resources/ResourceManager;->preloadStaticResources(Landroid/content/Context;)V

    .line 530
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/compositor/CompositorView;->updateToolbarLayer(Lcom/google/android/apps/chrome/tabs/layout/LayoutProvider;Z)V

    .line 533
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mRenderHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;

    iget-object v1, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mCacheVisibleViewport:Landroid/graphics/Rect;

    invoke-interface {v0, v1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;->getVisibleViewport(Landroid/graphics/Rect;)Landroid/graphics/Rect;

    .line 534
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mCacheViewport:Landroid/graphics/Rect;

    invoke-interface {p1, v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutProvider;->getViewportPixel(Landroid/graphics/Rect;)Landroid/graphics/Rect;

    .line 535
    iget-wide v2, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mNativeCompositorView:J

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mCacheViewport:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    int-to-float v4, v0

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mCacheViewport:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    int-to-float v5, v0

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mCacheViewport:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    int-to-float v6, v0

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mCacheViewport:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    int-to-float v7, v0

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mCacheVisibleViewport:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    int-to-float v8, v0

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mCacheVisibleViewport:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    int-to-float v9, v0

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mRenderHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;->getCurrentOverdrawBottomHeight()I

    move-result v0

    int-to-float v10, v0

    const/high16 v11, 0x3f800000    # 1.0f

    move-object v1, p0

    invoke-direct/range {v1 .. v11}, Lcom/google/android/apps/chrome/compositor/CompositorView;->nativeSetLayoutViewport(JFFFFFFFF)V

    .line 545
    invoke-virtual {v12}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->getLayoutTabsToRender()[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v1

    .line 546
    if-eqz v1, :cond_5

    array-length v0, v1

    .line 548
    :goto_1
    iget-boolean v2, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mEnableCompositorTabStrip:Z

    if-eqz v2, :cond_2

    .line 550
    iget-object v2, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mTabStripLayerContainer:Lcom/google/android/apps/chrome/compositor/TabStripLayerContainer;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/compositor/CompositorView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v2, v3, p1}, Lcom/google/android/apps/chrome/compositor/TabStripLayerContainer;->pushAndUpdateStrip(Landroid/content/Context;Lcom/google/android/apps/chrome/tabs/layout/LayoutProvider;)V

    .line 554
    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mContextualSearchManager:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;

    if-eqz v2, :cond_3

    .line 555
    invoke-direct {p0, v12}, Lcom/google/android/apps/chrome/compositor/CompositorView;->updateContextualSearchLayer(Lcom/google/android/apps/chrome/tabs/layout/Layout;)V

    .line 558
    :cond_3
    instance-of v2, v12, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;

    if-eqz v2, :cond_7

    const/4 v2, 0x1

    if-ne v0, v2, :cond_7

    iget-object v2, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mStaticTabLayerContainer:Lcom/google/android/apps/chrome/compositor/StaticTabLayerContainer;

    if-eqz v2, :cond_7

    .line 562
    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getId()I

    move-result v2

    .line 563
    iget-wide v4, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mNativeCompositorView:J

    const/4 v1, -0x1

    if-eq v2, v1, :cond_6

    const/4 v1, 0x1

    :goto_2
    invoke-direct {p0, v4, v5, v1}, Lcom/google/android/apps/chrome/compositor/CompositorView;->nativeSetStaticLayerMode(JZ)V

    .line 565
    iget-object v1, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mStaticTabLayerContainer:Lcom/google/android/apps/chrome/compositor/StaticTabLayerContainer;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/compositor/CompositorView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {p1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutProvider;->getFullscreenManager()Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->getContentOffset()F

    move-result v4

    invoke-virtual {v1, v3, v12, v4}, Lcom/google/android/apps/chrome/compositor/StaticTabLayerContainer;->pushLayers(Landroid/content/Context;Lcom/google/android/apps/chrome/tabs/layout/Layout;F)V

    .line 569
    iget-object v1, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mTabContentManager:Lcom/google/android/apps/chrome/compositor/TabContentManager;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mTabContentManager:Lcom/google/android/apps/chrome/compositor/TabContentManager;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/chrome/compositor/TabContentManager;->hasFullCachedThumbnail(I)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 570
    invoke-static {}, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->logPerceivedTabSwitchLatencyMetric()V

    .line 578
    :cond_4
    :goto_3
    iput v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mLastLayerCount:I

    .line 579
    invoke-static {}, Lorg/chromium/chrome/browser/tabmodel/TabModelBase;->flushActualTabSwitchLatencyMetric()V

    .line 580
    iget-wide v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mNativeCompositorView:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/compositor/CompositorView;->nativeFinalizeLayers(J)V

    .line 581
    const-string/jumbo v0, "CompositorView:finalizeLayers"

    invoke-static {v0}, Lorg/chromium/base/TraceEvent;->end(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 546
    :cond_5
    const/4 v0, 0x0

    goto :goto_1

    .line 563
    :cond_6
    const/4 v1, 0x0

    goto :goto_2

    .line 572
    :cond_7
    iget-object v1, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mTabLayerContainer:Lcom/google/android/apps/chrome/compositor/TabLayerContainer;

    if-eqz v1, :cond_4

    .line 574
    iget-wide v2, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mNativeCompositorView:J

    const/4 v1, 0x0

    invoke-direct {p0, v2, v3, v1}, Lcom/google/android/apps/chrome/compositor/CompositorView;->nativeSetStaticLayerMode(JZ)V

    .line 575
    iget-object v1, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mTabLayerContainer:Lcom/google/android/apps/chrome/compositor/TabLayerContainer;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/compositor/CompositorView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2, v12}, Lcom/google/android/apps/chrome/compositor/TabLayerContainer;->pushLayers(Landroid/content/Context;Lcom/google/android/apps/chrome/tabs/layout/Layout;)V

    goto :goto_3
.end method

.method public getContentReadbackHandler()Lorg/chromium/content/browser/ContentReadbackHandler;
    .locals 1

    .prologue
    .line 214
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mContentReadbackHandler:Lorg/chromium/content/browser/ContentReadbackHandler;

    return-object v0
.end method

.method public getInstantPageContainer()Lcom/google/android/apps/chrome/compositor/InstantPageContainer;
    .locals 1

    .prologue
    .line 261
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mInstantPageContainer:Lcom/google/android/apps/chrome/compositor/InstantPageContainer;

    return-object v0
.end method

.method public getLastLayerCount()I
    .locals 1

    .prologue
    .line 589
    iget v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mLastLayerCount:I

    return v0
.end method

.method public getOverdrawBottomHeight()I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 228
    iget-object v1, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mRootActivityView:Landroid/view/View;

    if-nez v1, :cond_0

    .line 229
    iget-object v1, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mRootView:Landroid/view/View;

    const v2, 0x1020002

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mRootActivityView:Landroid/view/View;

    .line 231
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mRootActivityView:Landroid/view/View;

    if-eqz v1, :cond_1

    .line 232
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/compositor/CompositorView;->getHeight()I

    move-result v1

    .line 233
    iget-object v2, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mRootActivityView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    .line 234
    sub-int/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 236
    :cond_1
    return v0
.end method

.method public getOverlayTranslateY()I
    .locals 2

    .prologue
    .line 594
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mRenderHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;->areTopControlsPermanentlyHidden()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mRenderHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;->getTopControlsHeightPixels()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mRenderHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;

    iget-object v1, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mCacheVisibleViewport:Landroid/graphics/Rect;

    invoke-interface {v0, v1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;->getVisibleViewport(Landroid/graphics/Rect;)Landroid/graphics/Rect;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Rect;->top:I

    goto :goto_0
.end method

.method public getResourceManager()Lcom/google/android/apps/chrome/compositor/resources/ResourceManager;
    .locals 1

    .prologue
    .line 221
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mResourceManager:Lcom/google/android/apps/chrome/compositor/resources/ResourceManager;

    return-object v0
.end method

.method public getTransitionPageContainer()Lcom/google/android/apps/chrome/compositor/TransitionPageContainer;
    .locals 1

    .prologue
    .line 268
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mTransitionPageContainer:Lcom/google/android/apps/chrome/compositor/TransitionPageContainer;

    return-object v0
.end method

.method public initNativeCompositor(ZLcom/google/android/apps/chrome/WindowDelegate;Lorg/chromium/ui/base/WindowAndroid;Lcom/google/android/apps/chrome/compositor/LayerTitleCache;Lcom/google/android/apps/chrome/compositor/TabContentManager;)V
    .locals 8

    .prologue
    .line 282
    iput-object p2, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mWindowDelegate:Lcom/google/android/apps/chrome/WindowDelegate;

    .line 283
    iput-object p3, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mWindowAndroid:Lorg/chromium/ui/base/WindowAndroid;

    .line 284
    iput-object p5, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mTabContentManager:Lcom/google/android/apps/chrome/compositor/TabContentManager;

    .line 286
    iget-boolean v1, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mEnableCompositorTabStrip:Z

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/compositor/CompositorView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Lcom/google/android/apps/chrome/R$color;->tab_switcher_background:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {p3}, Lorg/chromium/ui/base/WindowAndroid;->getNativePointer()J

    move-result-wide v4

    move-object v0, p0

    move v2, p1

    move-object v6, p4

    move-object v7, p5

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/chrome/compositor/CompositorView;->nativeInit(ZZIJLcom/google/android/apps/chrome/compositor/LayerTitleCache;Lcom/google/android/apps/chrome/compositor/TabContentManager;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mNativeCompositorView:J

    .line 294
    if-eqz p4, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/compositor/CompositorView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p4, v0}, Lcom/google/android/apps/chrome/compositor/LayerTitleCache;->updateImmutableResources(Landroid/content/Context;)V

    .line 296
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mTakeSurface:Z

    if-nez v0, :cond_2

    .line 298
    sget-boolean v0, Lcom/google/android/apps/chrome/compositor/CompositorView;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/compositor/CompositorView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Surface;->isValid()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    const-string/jumbo v1, "Surface created before native library loaded."

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 299
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/compositor/CompositorView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    invoke-interface {v0, p0}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 302
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/compositor/CompositorView;->setBackgroundColor(I)V

    .line 303
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/compositor/CompositorView;->setVisibility(I)V

    .line 307
    :cond_2
    iget-wide v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mNativeCompositorView:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/compositor/CompositorView;->nativeGetResourceManager(J)Lcom/google/android/apps/chrome/compositor/resources/ResourceManager;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mResourceManager:Lcom/google/android/apps/chrome/compositor/resources/ResourceManager;

    .line 310
    iget-wide v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mNativeCompositorView:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/compositor/CompositorView;->nativeGetInstantPageContainer(J)Lcom/google/android/apps/chrome/compositor/InstantPageContainer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mInstantPageContainer:Lcom/google/android/apps/chrome/compositor/InstantPageContainer;

    .line 313
    iget-wide v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mNativeCompositorView:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/compositor/CompositorView;->nativeGetStaticTabLayerContainer(J)Lcom/google/android/apps/chrome/compositor/StaticTabLayerContainer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mStaticTabLayerContainer:Lcom/google/android/apps/chrome/compositor/StaticTabLayerContainer;

    .line 314
    iget-wide v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mNativeCompositorView:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/compositor/CompositorView;->nativeGetTabLayerContainer(J)Lcom/google/android/apps/chrome/compositor/TabLayerContainer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mTabLayerContainer:Lcom/google/android/apps/chrome/compositor/TabLayerContainer;

    .line 315
    iget-wide v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mNativeCompositorView:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/compositor/CompositorView;->nativeGetTabStripLayerContainer(J)Lcom/google/android/apps/chrome/compositor/TabStripLayerContainer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mTabStripLayerContainer:Lcom/google/android/apps/chrome/compositor/TabStripLayerContainer;

    .line 316
    iget-wide v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mNativeCompositorView:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/compositor/CompositorView;->nativeGetTransitionPageContainer(J)Lcom/google/android/apps/chrome/compositor/TransitionPageContainer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mTransitionPageContainer:Lcom/google/android/apps/chrome/compositor/TransitionPageContainer;

    .line 318
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mTakeSurface:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mTakeSurfaceRunnable:Lcom/google/android/apps/chrome/compositor/CompositorView$TakeSurfaceRunnable;

    if-eqz v0, :cond_3

    .line 319
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mTakeSurfaceRunnable:Lcom/google/android/apps/chrome/compositor/CompositorView$TakeSurfaceRunnable;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/compositor/CompositorView$TakeSurfaceRunnable;->run()V

    .line 320
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mTakeSurfaceRunnable:Lcom/google/android/apps/chrome/compositor/CompositorView$TakeSurfaceRunnable;

    .line 322
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mContentReadbackHandler:Lorg/chromium/content/browser/ContentReadbackHandler;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentReadbackHandler;->initNativeContentReadbackHandler()V

    .line 323
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 206
    invoke-super {p0}, Landroid/view/SurfaceView;->onDetachedFromWindow()V

    .line 207
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mPreviousWindowTop:I

    .line 208
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 2

    .prologue
    .line 200
    invoke-super/range {p0 .. p5}, Landroid/view/SurfaceView;->onLayout(ZIIII)V

    .line 201
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mRenderHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/compositor/CompositorView;->getOverdrawBottomHeight()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;->onOverdrawBottomHeightChanged(I)V

    .line 202
    return-void
.end method

.method protected onMeasure(II)V
    .locals 3

    .prologue
    .line 170
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mRootView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 171
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mRootView:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mCacheAppRect:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 179
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mCacheAppRect:Landroid/graphics/Rect;

    iget v2, v0, Landroid/graphics/Rect;->top:I

    .line 180
    iget v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mPreviousWindowTop:I

    if-eq v2, v0, :cond_1

    const/4 v0, 0x1

    move v1, v0

    .line 181
    :goto_0
    iput v2, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mPreviousWindowTop:I

    .line 183
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mWindowAndroid:Lorg/chromium/ui/base/WindowAndroid;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mWindowAndroid:Lorg/chromium/ui/base/WindowAndroid;

    invoke-virtual {v0}, Lorg/chromium/ui/base/WindowAndroid;->getActivity()Ljava/lang/ref/WeakReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 187
    :goto_1
    if-nez v1, :cond_0

    invoke-static {v0}, Lcom/google/android/apps/chrome/third_party/samsung/MultiWindowUtils;->isMultiWindow(Landroid/app/Activity;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/compositor/CompositorView;->getMeasuredWidth()I

    move-result v0

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/compositor/CompositorView;->getMeasuredHeight()I

    move-result v0

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    if-le v0, v1, :cond_0

    .line 191
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/compositor/CompositorView;->getMeasuredHeight()I

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p2

    .line 195
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/view/SurfaceView;->onMeasure(II)V

    .line 196
    return-void

    .line 180
    :cond_1
    const/4 v0, 0x0

    move v1, v0

    goto :goto_0

    .line 183
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method requestRender()V
    .locals 4

    .prologue
    .line 376
    iget-wide v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mNativeCompositorView:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mNativeCompositorView:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/compositor/CompositorView;->nativeSetNeedsComposite(J)V

    .line 377
    :cond_0
    return-void
.end method

.method public resetFlags()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 161
    invoke-static {}, Lorg/chromium/base/CommandLine;->getInstance()Lorg/chromium/base/CommandLine;

    move-result-object v3

    .line 162
    const-string/jumbo v0, "disable-compositor-tab-strip"

    invoke-virtual {v3, v0}, Lorg/chromium/base/CommandLine;->hasSwitch(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/compositor/CompositorView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/ui/base/DeviceFormFactor;->isTablet(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mEnableCompositorTabStrip:Z

    .line 164
    const-string/jumbo v0, "enable-tablet-tab-stack"

    invoke-virtual {v3, v0}, Lorg/chromium/base/CommandLine;->hasSwitch(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/compositor/CompositorView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/ui/base/DeviceFormFactor;->isTablet(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    :goto_1
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mEnableTabletTabStack:Z

    .line 166
    return-void

    :cond_0
    move v0, v2

    .line 162
    goto :goto_0

    :cond_1
    move v1, v2

    .line 164
    goto :goto_1
.end method

.method public setContextualSearchManager(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;)V
    .locals 0

    .prologue
    .line 254
    iput-object p1, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mContextualSearchManager:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;

    .line 255
    return-void
.end method

.method public setOverlayVideoMode(Z)V
    .locals 2

    .prologue
    .line 330
    if-eqz p1, :cond_0

    const/4 v0, -0x3

    .line 331
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/compositor/CompositorView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v1

    invoke-interface {v1, v0}, Landroid/view/SurfaceHolder;->setFormat(I)V

    .line 332
    iget-wide v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mNativeCompositorView:J

    invoke-direct {p0, v0, v1, p1}, Lcom/google/android/apps/chrome/compositor/CompositorView;->nativeSetOverlayVideoMode(JZ)V

    .line 333
    return-void

    .line 330
    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public setRootView(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 153
    iput-object p1, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mRootView:Landroid/view/View;

    .line 154
    return-void
.end method

.method public shutDown()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 243
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mTakeSurface:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/compositor/CompositorView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    invoke-interface {v0, p0}, Landroid/view/SurfaceHolder;->removeCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 244
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mContentReadbackHandler:Lorg/chromium/content/browser/ContentReadbackHandler;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentReadbackHandler;->destroy()V

    .line 245
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mContentReadbackHandler:Lorg/chromium/content/browser/ContentReadbackHandler;

    .line 246
    iget-wide v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mNativeCompositorView:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    iget-wide v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mNativeCompositorView:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/compositor/CompositorView;->nativeDestroy(J)V

    .line 247
    :cond_1
    iput-wide v2, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mNativeCompositorView:J

    .line 248
    return-void
.end method

.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 8

    .prologue
    const-wide/16 v2, 0x0

    .line 339
    iget-wide v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mNativeCompositorView:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mTakeSurface:Z

    if-eqz v0, :cond_1

    .line 340
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mTakeSurfaceRunnable:Lcom/google/android/apps/chrome/compositor/CompositorView$TakeSurfaceRunnable;

    if-nez v0, :cond_0

    .line 341
    new-instance v0, Lcom/google/android/apps/chrome/compositor/CompositorView$TakeSurfaceRunnable;

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/chrome/compositor/CompositorView$TakeSurfaceRunnable;-><init>(Lcom/google/android/apps/chrome/compositor/CompositorView;Landroid/view/SurfaceHolder;III)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mTakeSurfaceRunnable:Lcom/google/android/apps/chrome/compositor/CompositorView$TakeSurfaceRunnable;

    .line 350
    :cond_0
    :goto_0
    return-void

    .line 347
    :cond_1
    iget-wide v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mNativeCompositorView:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 348
    iget-wide v2, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mNativeCompositorView:J

    invoke-interface {p1}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    move-result-object v7

    move-object v1, p0

    move v4, p2

    move v5, p3

    move v6, p4

    invoke-direct/range {v1 .. v7}, Lcom/google/android/apps/chrome/compositor/CompositorView;->nativeSurfaceChanged(JIIILandroid/view/Surface;)V

    .line 349
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mRenderHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;

    invoke-interface {v0, p3, p4}, Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;->onPhysicalBackingSizeChanged(II)V

    goto :goto_0
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 4

    .prologue
    .line 354
    iget-wide v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mNativeCompositorView:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 357
    :goto_0
    return-void

    .line 355
    :cond_0
    iget-wide v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mNativeCompositorView:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/compositor/CompositorView;->nativeSurfaceCreated(J)V

    .line 356
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mRenderHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;->onSurfaceCreated()V

    goto :goto_0
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 4

    .prologue
    .line 361
    iget-wide v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mNativeCompositorView:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 364
    :goto_0
    return-void

    .line 362
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mTakeSurfaceRunnable:Lcom/google/android/apps/chrome/compositor/CompositorView$TakeSurfaceRunnable;

    .line 363
    iget-wide v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorView;->mNativeCompositorView:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/compositor/CompositorView;->nativeSurfaceDestroyed(J)V

    goto :goto_0
.end method

.method public surfaceRedrawNeeded(Landroid/view/SurfaceHolder;)V
    .locals 0

    .prologue
    .line 368
    return-void
.end method
