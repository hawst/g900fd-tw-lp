.class Lcom/google/android/apps/chrome/snapshot/SnapshotDocumentDataSource$2;
.super Landroid/os/AsyncTask;
.source "SnapshotDocumentDataSource.java"


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/snapshot/SnapshotDocumentDataSource;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/snapshot/SnapshotDocumentDataSource;)V
    .locals 0

    .prologue
    .line 98
    iput-object p1, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocumentDataSource$2;->this$0:Lcom/google/android/apps/chrome/snapshot/SnapshotDocumentDataSource;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 98
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocumentDataSource$2;->doInBackground([Ljava/lang/Void;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/util/Set;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocumentDataSource$2;->this$0:Lcom/google/android/apps/chrome/snapshot/SnapshotDocumentDataSource;

    # getter for: Lcom/google/android/apps/chrome/snapshot/SnapshotDocumentDataSource;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocumentDataSource;->access$200(Lcom/google/android/apps/chrome/snapshot/SnapshotDocumentDataSource;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->getCurrentLocalDocuments(Landroid/content/Context;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 98
    check-cast p1, Ljava/util/Set;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocumentDataSource$2;->onPostExecute(Ljava/util/Set;)V

    return-void
.end method

.method protected onPostExecute(Ljava/util/Set;)V
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocumentDataSource$2;->this$0:Lcom/google/android/apps/chrome/snapshot/SnapshotDocumentDataSource;

    # getter for: Lcom/google/android/apps/chrome/snapshot/SnapshotDocumentDataSource;->mListener:Lcom/google/android/apps/chrome/snapshot/SnapshotDocumentDataSource$SnapshotDocumentDataSourceListener;
    invoke-static {v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocumentDataSource;->access$300(Lcom/google/android/apps/chrome/snapshot/SnapshotDocumentDataSource;)Lcom/google/android/apps/chrome/snapshot/SnapshotDocumentDataSource$SnapshotDocumentDataSourceListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocumentDataSource$SnapshotDocumentDataSourceListener;->onSnapshotDocumentsAvailable(Ljava/util/Set;)V

    .line 107
    return-void
.end method
