.class Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager$3;
.super Ljava/lang/Object;
.source "HomeActivityRecentTabsManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;

.field final synthetic val$tab:Lorg/chromium/chrome/browser/RecentlyClosedBridge$RecentlyClosedTab;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;Lorg/chromium/chrome/browser/RecentlyClosedBridge$RecentlyClosedTab;)V
    .locals 0

    .prologue
    .line 117
    iput-object p1, p0, Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager$3;->this$0:Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;

    iput-object p2, p0, Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager$3;->val$tab:Lorg/chromium/chrome/browser/RecentlyClosedBridge$RecentlyClosedTab;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 120
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager$3;->this$0:Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;

    iget-object v1, p0, Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager$3;->val$tab:Lorg/chromium/chrome/browser/RecentlyClosedBridge$RecentlyClosedTab;

    const/4 v2, 0x4

    # invokes: Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->openRecentlyClosedTab(Lorg/chromium/chrome/browser/RecentlyClosedBridge$RecentlyClosedTab;I)V
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;->access$601(Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;Lorg/chromium/chrome/browser/RecentlyClosedBridge$RecentlyClosedTab;I)V

    .line 122
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager$3;->this$0:Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;

    # getter for: Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;->mDialog:Landroid/app/Dialog;
    invoke-static {v0}, Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;->access$400(Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;)Landroid/app/Dialog;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager$3;->this$0:Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;

    # getter for: Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;->mDialog:Landroid/app/Dialog;
    invoke-static {v0}, Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;->access$400(Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;)Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 123
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager$3;->this$0:Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;

    # getter for: Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;->mFinishActivityOnOpen:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;->access$500(Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager$3;->this$0:Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;

    # getter for: Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;->access$100(Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finishAndRemoveTask()V

    .line 124
    :cond_1
    return-void
.end method
