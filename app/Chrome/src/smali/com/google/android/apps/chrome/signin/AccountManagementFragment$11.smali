.class final Lcom/google/android/apps/chrome/signin/AccountManagementFragment$11;
.super Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;
.source "AccountManagementFragment.java"


# instance fields
.field final synthetic val$activity:Landroid/app/Activity;

.field final synthetic val$preferenceManager:Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

.field final synthetic val$selector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelectorBase;


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 4

    .prologue
    const/16 v3, 0x19

    .line 810
    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    .line 811
    iget v0, p1, Landroid/os/Message;->what:I

    if-eq v0, v3, :cond_1

    .line 827
    :cond_0
    :goto_0
    return-void

    .line 812
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment$11;->val$preferenceManager:Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getDisplayedAccountManagementUpgradeScreen()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 813
    invoke-static {}, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->shouldAlwaysShowUpgradeScreen()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 816
    :cond_2
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "tabId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 817
    iget-object v1, p0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment$11;->val$selector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelectorBase;

    invoke-virtual {v1}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelectorBase;->getCurrentTab()Lorg/chromium/chrome/browser/Tab;

    move-result-object v1

    .line 818
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lorg/chromium/chrome/browser/Tab;->getId()I

    move-result v2

    if-ne v2, v0, :cond_0

    .line 819
    invoke-virtual {v1}, Lorg/chromium/chrome/browser/Tab;->isIncognito()Z

    move-result v0

    if-nez v0, :cond_0

    .line 821
    invoke-virtual {v1}, Lorg/chromium/chrome/browser/Tab;->getUrl()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/chrome/browser/UrlUtilities;->isGooglePropertyUrl(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 823
    iget-object v0, p0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment$11;->val$activity:Landroid/app/Activity;

    invoke-static {v0, v3, p0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->unregisterForNotification(Landroid/content/Context;ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    .line 825
    iget-object v0, p0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment$11;->val$preferenceManager:Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->setDisplayedAccountManagementUpgradeScreen()V

    .line 826
    iget-object v0, p0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment$11;->val$activity:Landroid/app/Activity;

    const/4 v1, 0x0

    # invokes: Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->showUpgradeScreen(Landroid/app/Activity;Landroid/content/DialogInterface$OnDismissListener;)V
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->access$500(Landroid/app/Activity;Landroid/content/DialogInterface$OnDismissListener;)V

    goto :goto_0
.end method
