.class public final enum Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;
.super Ljava/lang/Enum;
.source "RemoteVideoInfo.java"


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

.field public static final enum ERROR:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

.field public static final enum FINISHED:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

.field public static final enum INVALIDATED:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

.field public static final enum LOADING:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

.field public static final enum PAUSED:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

.field public static final enum PLAYING:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

.field public static final enum STOPPED:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 22
    new-instance v0, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    const-string/jumbo v1, "STOPPED"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;->STOPPED:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    .line 25
    new-instance v0, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    const-string/jumbo v1, "LOADING"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;->LOADING:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    .line 28
    new-instance v0, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    const-string/jumbo v1, "PLAYING"

    invoke-direct {v0, v1, v5}, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;->PLAYING:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    .line 31
    new-instance v0, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    const-string/jumbo v1, "PAUSED"

    invoke-direct {v0, v1, v6}, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;->PAUSED:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    .line 34
    new-instance v0, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    const-string/jumbo v1, "ERROR"

    invoke-direct {v0, v1, v7}, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;->ERROR:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    .line 38
    new-instance v0, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    const-string/jumbo v1, "INVALIDATED"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;->INVALIDATED:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    .line 41
    new-instance v0, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    const-string/jumbo v1, "FINISHED"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;->FINISHED:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    .line 20
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    sget-object v1, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;->STOPPED:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;->LOADING:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;->PLAYING:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;->PAUSED:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;->ERROR:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;->INVALIDATED:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;->FINISHED:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;->$VALUES:[Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;
    .locals 1

    .prologue
    .line 20
    const-class v0, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;->$VALUES:[Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    invoke-virtual {v0}, [Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    return-object v0
.end method
