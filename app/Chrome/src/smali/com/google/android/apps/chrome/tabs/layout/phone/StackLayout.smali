.class public Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;
.super Lcom/google/android/apps/chrome/tabs/layout/Layout;
.source "StackLayout.java"

# interfaces
.implements Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final NOTIFICATIONS:[I


# instance fields
.field private mCachedLandscapeViewport:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;

.field private mCachedPortraitViewport:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;

.field private mClicked:Z

.field private mDelayedLayoutTabInitRequired:Z

.field private mFlingFromModelChange:Z

.field private mFlingSpeed:F

.field private mInnerMarginPercent:F

.field private mInputMode:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;

.field private mLastOnDownTimeStamp:J

.field private mLastOnDownX:F

.field private mLastOnDownY:F

.field private final mMinDirectionThreshold:F

.field private final mMinMaxInnerMargin:I

.field private final mMinShortPressThresholdSqr:F

.field private final mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

.field private final mOrderComparator:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$OrderComparator;

.field private mRenderedScrollOffset:F

.field private mScrollIndexOffset:F

.field private mSortedPriorityArray:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

.field private mSortingComparator:Ljava/util/Comparator;

.field private mStackAnimationCount:I

.field private mStackOffsetYPercent:F

.field private final mStackRects:[Landroid/graphics/RectF;

.field private final mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

.field private mTemporarySelectedStack:Ljava/lang/Boolean;

.field private final mViewContainer:Landroid/view/ViewGroup;

.field private final mVisibilityArray:Ljava/util/ArrayList;

.field private final mVisibilityComparator:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$VisibilityComparator;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 44
    const-class v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->$assertionsDisabled:Z

    .line 133
    new-array v0, v1, [I

    const/16 v1, 0xd

    aput v1, v0, v2

    sput-object v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->NOTIFICATIONS:[I

    return-void

    :cond_0
    move v0, v2

    .line 44
    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;Lcom/google/android/apps/chrome/eventfilter/EventFilter;)V
    .locals 10

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 156
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/chrome/tabs/layout/Layout;-><init>(Landroid/content/Context;Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;Lcom/google/android/apps/chrome/eventfilter/EventFilter;Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripStacker;)V

    .line 84
    iput v6, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mFlingSpeed:F

    .line 96
    iput v6, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mRenderedScrollOffset:F

    .line 97
    iput v6, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mScrollIndexOffset:F

    .line 103
    sget-object v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;->NONE:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mInputMode:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;

    .line 115
    iput-object v5, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mSortedPriorityArray:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    .line 117
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mVisibilityArray:Ljava/util/ArrayList;

    .line 118
    new-instance v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$VisibilityComparator;

    invoke-direct {v0, v5}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$VisibilityComparator;-><init>(Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$1;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mVisibilityComparator:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$VisibilityComparator;

    .line 119
    new-instance v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$OrderComparator;

    invoke-direct {v0, v5}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$OrderComparator;-><init>(Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$1;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mOrderComparator:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$OrderComparator;

    .line 120
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mVisibilityComparator:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$VisibilityComparator;

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mSortingComparator:Ljava/util/Comparator;

    .line 123
    iput-boolean v7, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mDelayedLayoutTabInitRequired:Z

    .line 128
    iput-object v5, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mCachedPortraitViewport:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;

    .line 129
    iput-object v5, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mCachedLandscapeViewport:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;

    .line 136
    new-instance v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$1;-><init>(Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    .line 158
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    .line 159
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v1

    int-to-float v1, v1

    iput v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mMinDirectionThreshold:F

    .line 160
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledPagingTouchSlop()I

    move-result v1

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledPagingTouchSlop()I

    move-result v0

    mul-int/2addr v0, v1

    int-to-float v0, v0

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mMinShortPressThresholdSqr:F

    .line 163
    const/16 v0, 0x37

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mMinMaxInnerMargin:I

    .line 164
    const/high16 v0, 0x3fc00000    # 1.5f

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mFlingSpeed:F

    .line 165
    new-array v0, v9, [Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    .line 166
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    new-instance v1, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    invoke-direct {v1, p1, p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;-><init>(Landroid/content/Context;Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;)V

    aput-object v1, v0, v7

    .line 167
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    new-instance v1, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    invoke-direct {v1, p1, p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;-><init>(Landroid/content/Context;Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;)V

    aput-object v1, v0, v8

    .line 168
    new-array v0, v9, [Landroid/graphics/RectF;

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStackRects:[Landroid/graphics/RectF;

    .line 169
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStackRects:[Landroid/graphics/RectF;

    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    aput-object v1, v0, v7

    .line 170
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStackRects:[Landroid/graphics/RectF;

    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    aput-object v1, v0, v8

    .line 172
    new-instance v0, Landroid/widget/FrameLayout;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mViewContainer:Landroid/view/ViewGroup;

    .line 173
    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;)F
    .locals 1

    .prologue
    .line 44
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStackOffsetYPercent:F

    return v0
.end method

.method static synthetic access$202(Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;Ljava/util/Comparator;)Ljava/util/Comparator;
    .locals 0

    .prologue
    .line 44
    iput-object p1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mSortingComparator:Ljava/util/Comparator;

    return-object p1
.end method

.method static synthetic access$300(Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;)Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$VisibilityComparator;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mVisibilityComparator:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$VisibilityComparator;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;)[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;)Z
    .locals 1

    .prologue
    .line 44
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mFlingFromModelChange:Z

    return v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;)F
    .locals 1

    .prologue
    .line 44
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mRenderedScrollOffset:F

    return v0
.end method

.method static synthetic access$700(Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;)F
    .locals 1

    .prologue
    .line 44
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mInnerMarginPercent:F

    return v0
.end method

.method static synthetic access$800(Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;)I
    .locals 1

    .prologue
    .line 44
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mMinMaxInnerMargin:I

    return v0
.end method

.method static synthetic access$900(Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;)F
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->getFullScrollDistance()F

    move-result v0

    return v0
.end method

.method private static addAllTabs(Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;I)I
    .locals 4

    .prologue
    .line 990
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->getTabs()[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    move-result-object v2

    .line 991
    if-eqz v2, :cond_0

    .line 992
    const/4 v0, 0x0

    :goto_0
    array-length v1, v2

    if-ge v0, v1, :cond_0

    .line 993
    add-int/lit8 v1, p2, 0x1

    aget-object v3, v2, v0

    aput-object v3, p1, p2

    .line 992
    add-int/lit8 v0, v0, 0x1

    move p2, v1

    goto :goto_0

    .line 996
    :cond_0
    return p2
.end method

.method private appendVisibleLayoutTabs(JI[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;I)I
    .locals 5

    .prologue
    .line 920
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    aget-object v0, v0, p3

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->getTabs()[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    move-result-object v3

    .line 921
    if-eqz v3, :cond_0

    .line 922
    const/4 v0, 0x0

    move v1, p5

    move v2, v0

    :goto_0
    array-length v0, v3

    if-ge v2, v0, :cond_1

    .line 923
    aget-object v0, v3, v2

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getLayoutTab()Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v4

    .line 924
    invoke-virtual {v4}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_2

    add-int/lit8 v0, v1, 0x1

    aput-object v4, p4, v1

    .line 922
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_0

    :cond_0
    move v1, p5

    .line 927
    :cond_1
    return v1

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method private canScrollLinearly(I)Z
    .locals 3

    .prologue
    .line 954
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    array-length v0, v0

    .line 955
    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mScrollIndexOffset:F

    const/4 v2, 0x0

    cmpg-float v1, v1, v2

    if-gtz v1, :cond_0

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mScrollIndexOffset:F

    neg-float v1, v1

    add-int/lit8 v0, v0, -0x1

    int-to-float v0, v0

    cmpg-float v0, v1, v0

    if-lez v0, :cond_1

    .line 956
    :cond_0
    const/4 v0, 0x0

    .line 960
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    xor-int/lit8 v1, p1, 0x1

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->isDisplayable()Z

    move-result v0

    goto :goto_0
.end method

.method private computeInputMode(JFFFF)Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;
    .locals 9

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 557
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    aget-object v0, v0, v3

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->isDisplayable()Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;->SEND_TO_STACK:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;

    .line 588
    :goto_0
    return-object v0

    .line 558
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->getTabStackIndex()I

    move-result v5

    .line 559
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->getViewportParameters()Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;

    move-result-object v0

    invoke-virtual {v0, p3, p4}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;->getStackIndexAt(FF)I

    move-result v0

    if-eq v5, v0, :cond_1

    .line 560
    sget-object v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;->SWITCH_STACK:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;

    goto :goto_0

    .line 562
    :cond_1
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mLastOnDownX:F

    add-float v1, p3, p5

    sub-float v1, v0, v1

    .line 563
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mLastOnDownY:F

    add-float v2, p4, p6

    sub-float v2, v0, v2

    .line 564
    mul-float v0, p5, p5

    mul-float v6, p6, p6

    add-float/2addr v6, v0

    .line 565
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->getOrientation()I

    move-result v0

    if-ne v0, v3, :cond_2

    move v0, v1

    .line 566
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->getOrientation()I

    move-result v7

    if-ne v7, v3, :cond_3

    .line 568
    :goto_2
    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mMinDirectionThreshold:F

    iget v7, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mMinDirectionThreshold:F

    mul-float/2addr v1, v7

    cmpl-float v1, v6, v1

    if-lez v1, :cond_4

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v2

    cmpl-float v1, v1, v2

    if-lez v1, :cond_4

    .line 570
    sget-object v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;->SEND_TO_STACK:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;

    goto :goto_0

    :cond_2
    move v0, v2

    .line 565
    goto :goto_1

    :cond_3
    move v2, v1

    .line 566
    goto :goto_2

    .line 573
    :cond_4
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v1

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mMinDirectionThreshold:F

    cmpl-float v1, v1, v2

    if-lez v1, :cond_8

    .line 574
    if-nez v5, :cond_5

    move v1, v3

    :goto_3
    const/4 v2, 0x0

    cmpl-float v0, v0, v2

    if-lez v0, :cond_6

    move v0, v3

    :goto_4
    xor-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->getOrientation()I

    move-result v1

    if-ne v1, v3, :cond_7

    invoke-static {}, Lorg/chromium/ui/base/LocalizationUtils;->isLayoutRtl()Z

    move-result v1

    if-eqz v1, :cond_7

    :goto_5
    xor-int/2addr v0, v3

    if-eqz v0, :cond_8

    .line 577
    sget-object v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;->SEND_TO_STACK:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;

    goto :goto_0

    :cond_5
    move v1, v4

    .line 574
    goto :goto_3

    :cond_6
    move v0, v4

    goto :goto_4

    :cond_7
    move v3, v4

    goto :goto_5

    .line 581
    :cond_8
    iget-wide v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mLastOnDownTimeStamp:J

    sub-long v0, p1, v0

    long-to-float v0, v0

    const/high16 v1, 0x43480000    # 200.0f

    cmpl-float v0, v0, v1

    if-lez v0, :cond_9

    .line 582
    sget-object v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;->SEND_TO_STACK:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;

    goto/16 :goto_0

    .line 585
    :cond_9
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mMinShortPressThresholdSqr:F

    cmpl-float v0, v6, v0

    if-lez v0, :cond_a

    .line 586
    sget-object v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;->SWITCH_STACK:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;

    goto/16 :goto_0

    .line 588
    :cond_a
    sget-object v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;->NONE:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;

    goto/16 :goto_0
.end method

.method private finishScrollStacks()V
    .locals 10

    .prologue
    .line 789
    sget-object v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$Property;->STACK_SNAP:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$Property;

    invoke-virtual {p0, p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->cancelAnimation(Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;)V

    .line 790
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->getTabStackIndex()I

    move-result v0

    .line 791
    int-to-float v1, v0

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mRenderedScrollOffset:F

    add-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    .line 792
    neg-int v0, v0

    int-to-float v5, v0

    .line 793
    const/4 v0, 0x0

    cmpl-float v0, v1, v0

    if-eqz v0, :cond_1

    .line 794
    const-wide/16 v2, 0x64

    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->getFullScrollDistance()F

    move-result v0

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mFlingSpeed:F

    div-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    float-to-long v0, v0

    add-long v6, v2, v0

    .line 796
    sget-object v3, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$Property;->STACK_SNAP:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$Property;

    iget v4, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mRenderedScrollOffset:F

    const-wide/16 v8, 0x0

    move-object v1, p0

    move-object v2, p0

    invoke-virtual/range {v1 .. v9}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->addToAnimation(Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJ)V

    .line 804
    :cond_0
    :goto_0
    return-void

    .line 798
    :cond_1
    sget-object v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$Property;->STACK_SNAP:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$Property;

    invoke-virtual {p0, v0, v5}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->setProperty(Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$Property;F)V

    .line 799
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mTemporarySelectedStack:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 800
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mTemporarySelectedStack:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-interface {v0, v1}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->selectModel(Z)V

    .line 801
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mTemporarySelectedStack:Ljava/lang/Boolean;

    goto :goto_0
.end method

.method private flingStacks(Z)V
    .locals 1

    .prologue
    .line 775
    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->canScrollLinearly(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 779
    :goto_1
    return-void

    .line 775
    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    .line 776
    :cond_1
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->setActiveStackState(Z)Z

    .line 777
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->finishScrollStacks()V

    .line 778
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->requestStackUpdate()V

    goto :goto_1
.end method

.method private getFullScrollDistance()F
    .locals 3

    .prologue
    .line 964
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->getOrientation()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->getWidth()F

    move-result v0

    .line 966
    :goto_0
    const/high16 v1, 0x40000000    # 2.0f

    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->getViewportParameters()Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;->getInnerMargin()F

    move-result v2

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    return v0

    .line 964
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->getHeightMinusTopControls()F

    move-result v0

    goto :goto_0
.end method

.method private getTabStackIndex()I
    .locals 1

    .prologue
    .line 204
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->getTabStackIndex(I)I

    move-result v0

    return v0
.end method

.method private getViewportParameters()Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;
    .locals 2

    .prologue
    .line 731
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->getOrientation()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 732
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mCachedPortraitViewport:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;

    if-nez v0, :cond_0

    .line 733
    new-instance v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;-><init>(Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mCachedPortraitViewport:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;

    .line 735
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mCachedPortraitViewport:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;

    .line 740
    :goto_0
    return-object v0

    .line 737
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mCachedLandscapeViewport:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;

    if-nez v0, :cond_2

    .line 738
    new-instance v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$LandscapeViewport;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$LandscapeViewport;-><init>(Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mCachedLandscapeViewport:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;

    .line 740
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mCachedLandscapeViewport:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;

    goto :goto_0
.end method

.method private requestStackUpdate()V
    .locals 2

    .prologue
    .line 511
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->requestUpdate()V

    .line 512
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    const/4 v1, 0x1

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->requestUpdate()V

    .line 513
    return-void
.end method

.method private resetScrollData()V
    .locals 1

    .prologue
    .line 943
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->getTabStackIndex()I

    move-result v0

    neg-int v0, v0

    int-to-float v0, v0

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mScrollIndexOffset:F

    .line 944
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mScrollIndexOffset:F

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mRenderedScrollOffset:F

    .line 945
    return-void
.end method

.method private scrollStacks(F)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 759
    sget-object v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$Property;->STACK_SNAP:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$Property;

    invoke-virtual {p0, p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->cancelAnimation(Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;)V

    .line 760
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->getFullScrollDistance()F

    move-result v0

    .line 761
    iget v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mScrollIndexOffset:F

    div-float v4, p1, v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->getOrientation()I

    move-result v0

    if-ne v0, v1, :cond_0

    invoke-static {}, Lorg/chromium/ui/base/LocalizationUtils;->isLayoutRtl()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v4, v0}, Lcom/google/android/apps/chrome/utilities/ChromeMathUtils;->flipSignIf(FZ)F

    move-result v0

    add-float/2addr v0, v3

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mScrollIndexOffset:F

    .line 764
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->getTabStackIndex()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->canScrollLinearly(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 765
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mScrollIndexOffset:F

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mRenderedScrollOffset:F

    .line 770
    :goto_1
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->requestStackUpdate()V

    .line 771
    return-void

    .line 761
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 767
    :cond_1
    iget v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mScrollIndexOffset:F

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->isDisplayable()Z

    move-result v0

    if-eqz v0, :cond_2

    const/high16 v0, -0x40800000    # -1.0f

    :goto_2
    invoke-static {v3, v2, v0}, Lorg/chromium/chrome/browser/util/MathUtils;->clamp(FFF)F

    move-result v0

    float-to-int v0, v0

    int-to-float v0, v0

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mRenderedScrollOffset:F

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_2
.end method

.method private startMarginAnimation(Z)V
    .locals 2

    .prologue
    .line 424
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    const/4 v1, 0x1

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->isDisplayable()Z

    move-result v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->startMarginAnimation(ZZ)V

    .line 425
    return-void
.end method

.method private startMarginAnimation(ZZ)V
    .locals 10

    .prologue
    .line 428
    iget v4, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mInnerMarginPercent:F

    .line 429
    if-eqz p1, :cond_1

    if-eqz p2, :cond_1

    const/high16 v5, 0x3f800000    # 1.0f

    .line 430
    :goto_0
    cmpl-float v0, v4, v5

    if-eqz v0, :cond_0

    .line 431
    sget-object v3, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$Property;->INNER_MARGIN_PERCENT:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$Property;

    const-wide/16 v6, 0xc8

    const-wide/16 v8, 0x0

    move-object v1, p0

    move-object v2, p0

    invoke-virtual/range {v1 .. v9}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->addToAnimation(Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJ)V

    .line 433
    :cond_0
    return-void

    .line 429
    :cond_1
    const/4 v5, 0x0

    goto :goto_0
.end method

.method private startYOffsetAnimation(Z)V
    .locals 10

    .prologue
    .line 436
    iget v4, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStackOffsetYPercent:F

    .line 437
    if-eqz p1, :cond_1

    const/high16 v5, 0x3f800000    # 1.0f

    .line 438
    :goto_0
    cmpl-float v0, v4, v5

    if-eqz v0, :cond_0

    .line 439
    sget-object v3, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$Property;->STACK_OFFSET_Y_PERCENT:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$Property;

    const-wide/16 v6, 0x12c

    const-wide/16 v8, 0x0

    move-object v1, p0

    move-object v2, p0

    invoke-virtual/range {v1 .. v9}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->addToAnimation(Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJ)V

    .line 441
    :cond_0
    return-void

    .line 437
    :cond_1
    const/4 v5, 0x0

    goto :goto_0
.end method

.method private updateDelayedLayoutTabInit([Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 1072
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mDelayedLayoutTabInitRequired:Z

    if-nez v0, :cond_1

    .line 1086
    :cond_0
    :goto_0
    return-void

    .line 1075
    :cond_1
    array-length v3, p1

    move v1, v2

    move v0, v2

    .line 1076
    :goto_1
    if-ge v1, v3, :cond_3

    .line 1077
    const/4 v4, 0x4

    if-ge v0, v4, :cond_0

    .line 1079
    aget-object v4, p1, v1

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getLayoutTab()Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v4

    .line 1081
    invoke-super {p0, v4}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->initLayoutTabFromHost(Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1082
    add-int/lit8 v0, v0, 0x1

    .line 1076
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1085
    :cond_3
    if-nez v0, :cond_0

    iput-boolean v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mDelayedLayoutTabInitRequired:Z

    goto :goto_0
.end method

.method private updateSortedPriorityArray(Ljava/util/Comparator;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1020
    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->getCount()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    aget-object v3, v3, v1

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->getCount()I

    move-result v3

    add-int/2addr v2, v3

    .line 1021
    if-nez v2, :cond_0

    .line 1030
    :goto_0
    return v0

    .line 1022
    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mSortedPriorityArray:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mSortedPriorityArray:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    array-length v3, v3

    if-eq v3, v2, :cond_2

    .line 1023
    :cond_1
    new-array v2, v2, [Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    iput-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mSortedPriorityArray:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    .line 1025
    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    aget-object v2, v2, v0

    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mSortedPriorityArray:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    invoke-static {v2, v3, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->addAllTabs(Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;I)I

    move-result v0

    .line 1027
    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    aget-object v2, v2, v1

    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mSortedPriorityArray:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    invoke-static {v2, v3, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->addAllTabs(Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;I)I

    move-result v0

    .line 1028
    sget-boolean v2, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->$assertionsDisabled:Z

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mSortedPriorityArray:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    array-length v2, v2

    if-eq v0, v2, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1029
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mSortedPriorityArray:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    invoke-static {v0, p1}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    move v0, v1

    .line 1030
    goto :goto_0
.end method

.method private updateTabPriority()V
    .locals 1

    .prologue
    .line 1042
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mSortingComparator:Ljava/util/Comparator;

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->updateSortedPriorityArray(Ljava/util/Comparator;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1045
    :goto_0
    return-void

    .line 1043
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mSortedPriorityArray:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->updateTabsVisibility([Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;)V

    .line 1044
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mSortedPriorityArray:[Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->updateDelayedLayoutTabInit([Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;)V

    goto :goto_0
.end method

.method private updateTabsVisibility([Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;)V
    .locals 3

    .prologue
    .line 1055
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mVisibilityArray:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1056
    const/4 v0, 0x0

    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_0

    .line 1057
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mVisibilityArray:Ljava/util/ArrayList;

    aget-object v2, p1, v0

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getId()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1056
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1059
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mVisibilityArray:Ljava/util/ArrayList;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->updateCacheVisibleIds(Ljava/util/List;)V

    .line 1060
    return-void
.end method


# virtual methods
.method public attachViews(Landroid/view/ViewGroup;)V
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 296
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mViewContainer:Landroid/view/ViewGroup;

    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v2, v3, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 298
    return-void
.end method

.method public click(JFF)V
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 747
    iput-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mClicked:Z

    .line 748
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->getViewportParameters()Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;

    move-result-object v1

    .line 749
    invoke-virtual {v1, p3, p4}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;->getStackIndexAt(FF)I

    move-result v1

    .line 750
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->getTabStackIndex()I

    move-result v2

    if-ne v1, v2, :cond_0

    .line 751
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->getTabStackIndex()I

    move-result v1

    aget-object v0, v0, v1

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->click(JFF)V

    .line 755
    :goto_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->requestStackUpdate()V

    .line 756
    return-void

    .line 753
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->getTabStackIndex()I

    move-result v1

    if-nez v1, :cond_1

    :goto_1
    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->flingStacks(Z)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public contextChanged(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 527
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->contextChanged(Landroid/content/Context;)V

    .line 528
    invoke-static {p1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->resetDimensionConstants(Landroid/content/Context;)V

    .line 529
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->contextChanged(Landroid/content/Context;)V

    .line 530
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    const/4 v1, 0x1

    aget-object v0, v0, v1

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->contextChanged(Landroid/content/Context;)V

    .line 531
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->requestStackUpdate()V

    .line 532
    return-void
.end method

.method public detachViews()V
    .locals 2

    .prologue
    .line 302
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mViewContainer:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 303
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mViewContainer:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mViewContainer:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 305
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mViewContainer:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 306
    return-void
.end method

.method public doneHiding()V
    .locals 0

    .prologue
    .line 979
    invoke-super {p0}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->doneHiding()V

    .line 980
    return-void
.end method

.method public drag(JFFFF)V
    .locals 9

    .prologue
    .line 536
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mInputMode:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;

    sget-object v1, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;->NONE:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;

    if-ne v0, v1, :cond_0

    .line 537
    invoke-direct/range {p0 .. p6}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->computeInputMode(JFFFF)Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mInputMode:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;

    .line 540
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mInputMode:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;

    sget-object v1, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;->SEND_TO_STACK:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;

    if-ne v0, v1, :cond_2

    .line 541
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->getTabStackIndex()I

    move-result v1

    aget-object v1, v0, v1

    move-wide v2, p1

    move v4, p3

    move v5, p4

    move v6, p5

    move v7, p6

    invoke-virtual/range {v1 .. v7}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->drag(JFFFF)V

    .line 545
    :cond_1
    :goto_0
    return-void

    .line 542
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mInputMode:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;

    sget-object v1, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;->SWITCH_STACK:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;

    if-ne v0, v1, :cond_1

    .line 543
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->getOrientation()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    :goto_1
    invoke-direct {p0, p5}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->scrollStacks(F)V

    goto :goto_0

    :cond_3
    move p5, p6

    goto :goto_1
.end method

.method public fling(JFFFF)V
    .locals 9

    .prologue
    .line 593
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mInputMode:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;

    sget-object v1, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;->NONE:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;

    if-ne v0, v1, :cond_0

    .line 594
    const v0, 0x3d088889

    mul-float v6, p5, v0

    const v0, 0x3d088889

    mul-float v7, p6, v0

    move-object v1, p0

    move-wide v2, p1

    move v4, p3

    move v5, p4

    invoke-direct/range {v1 .. v7}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->computeInputMode(JFFFF)Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mInputMode:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;

    .line 598
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mInputMode:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;

    sget-object v1, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;->SEND_TO_STACK:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;

    if-ne v0, v1, :cond_2

    .line 599
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->getTabStackIndex()I

    move-result v1

    aget-object v1, v0, v1

    move-wide v2, p1

    move v4, p3

    move v5, p4

    move v6, p5

    move v7, p6

    invoke-virtual/range {v1 .. v7}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->fling(JFFFF)V

    .line 608
    :cond_1
    :goto_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->requestStackUpdate()V

    .line 609
    return-void

    .line 600
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mInputMode:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;

    sget-object v1, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;->SWITCH_STACK:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;

    if-ne v0, v1, :cond_1

    .line 601
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->getOrientation()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    .line 602
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->getOrientation()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_4

    .line 603
    :goto_2
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->getOrientation()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_5

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->getWidth()F

    move-result v0

    .line 604
    :goto_3
    const v1, 0x3d088889

    mul-float/2addr v1, p5

    add-float/2addr v1, p3

    .line 605
    const/4 v2, 0x0

    invoke-static {v1, v2, v0}, Lorg/chromium/chrome/browser/util/MathUtils;->clamp(FFF)F

    move-result v0

    sub-float/2addr v0, p3

    .line 606
    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->scrollStacks(F)V

    goto :goto_0

    :cond_3
    move p5, p6

    .line 601
    goto :goto_1

    :cond_4
    move p3, p4

    .line 602
    goto :goto_2

    .line 603
    :cond_5
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->getHeight()F

    move-result v0

    goto :goto_3
.end method

.method public getSizingFlags()I
    .locals 1

    .prologue
    .line 177
    const/16 v0, 0x1010

    return v0
.end method

.method protected getTabStack(I)Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;
    .locals 2

    .prologue
    .line 233
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->getTabStackIndex(I)I

    move-result v1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public getTabStack(Z)Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;
    .locals 2

    .prologue
    .line 196
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    aget-object v0, v1, v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected getTabStackIndex(I)I
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 215
    const/4 v2, -0x1

    if-ne p1, v2, :cond_3

    .line 216
    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mTemporarySelectedStack:Ljava/lang/Boolean;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mTemporarySelectedStack:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    .line 218
    :goto_0
    if-eqz v2, :cond_2

    .line 220
    :cond_0
    :goto_1
    return v0

    .line 216
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    invoke-interface {v2}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->isIncognitoSelected()Z

    move-result v2

    goto :goto_0

    :cond_2
    move v0, v1

    .line 218
    goto :goto_1

    .line 220
    :cond_3
    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    invoke-interface {v2, v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->getModel(Z)Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v2

    invoke-static {v2, p1}, Lorg/chromium/chrome/browser/tabmodel/TabModelUtils;->getTabById(Lorg/chromium/chrome/browser/tabmodel/TabList;I)Lorg/chromium/chrome/browser/Tab;

    move-result-object v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_1
.end method

.method public getViewContainer()Landroid/view/ViewGroup;
    .locals 1

    .prologue
    .line 313
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mViewContainer:Landroid/view/ViewGroup;

    return-object v0
.end method

.method public handlesCloseAll()Z
    .locals 1

    .prologue
    .line 279
    const/4 v0, 0x1

    return v0
.end method

.method public handlesTabClosing()Z
    .locals 1

    .prologue
    .line 289
    const/4 v0, 0x1

    return v0
.end method

.method public handlesTabCreating()Z
    .locals 1

    .prologue
    .line 284
    const/4 v0, 0x1

    return v0
.end method

.method protected initLayoutTabFromHost(Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;)Z
    .locals 1

    .prologue
    .line 1090
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->isInitFromHostNeeded()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mDelayedLayoutTabInitRequired:Z

    .line 1091
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public notifySizeChanged(FFI)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 517
    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mCachedLandscapeViewport:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;

    .line 518
    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mCachedPortraitViewport:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;

    .line 519
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->notifySizeChanged(FFI)V

    .line 520
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    const/4 v1, 0x1

    aget-object v0, v0, v1

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->notifySizeChanged(FFI)V

    .line 521
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->resetScrollData()V

    .line 522
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->requestStackUpdate()V

    .line 523
    return-void
.end method

.method protected onAnimationFinished()V
    .locals 2

    .prologue
    .line 359
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mFlingFromModelChange:Z

    .line 360
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mTemporarySelectedStack:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 361
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mTemporarySelectedStack:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-interface {v0, v1}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->selectModel(Z)V

    .line 362
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mTemporarySelectedStack:Ljava/lang/Boolean;

    .line 364
    :cond_0
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStackAnimationCount:I

    if-nez v0, :cond_1

    invoke-super {p0}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->onAnimationFinished()V

    .line 365
    :cond_1
    return-void
.end method

.method protected onAnimationStarted()V
    .locals 1

    .prologue
    .line 354
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStackAnimationCount:I

    if-nez v0, :cond_0

    invoke-super {p0}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->onAnimationStarted()V

    .line 355
    :cond_0
    return-void
.end method

.method public onDown(JFF)V
    .locals 9

    .prologue
    const/4 v6, 0x0

    .line 808
    iput p3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mLastOnDownX:F

    .line 809
    iput p4, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mLastOnDownY:F

    .line 810
    iput-wide p1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mLastOnDownTimeStamp:J

    move-object v1, p0

    move-wide v2, p1

    move v4, p3

    move v5, p4

    move v7, v6

    .line 811
    invoke-direct/range {v1 .. v7}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->computeInputMode(JFFFF)Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mInputMode:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;

    .line 812
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->getTabStackIndex()I

    move-result v1

    aget-object v0, v0, v1

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->onDown(J)V

    .line 813
    return-void
.end method

.method public onLongPress(JFF)V
    .locals 3

    .prologue
    .line 817
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->getTabStackIndex()I

    move-result v1

    aget-object v0, v0, v1

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->onLongPress(JFF)V

    .line 818
    return-void
.end method

.method public onPinch(JFFFFZ)V
    .locals 9

    .prologue
    .line 857
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->getTabStackIndex()I

    move-result v1

    aget-object v1, v0, v1

    move-wide v2, p1

    move v4, p3

    move v5, p4

    move v6, p5

    move v7, p6

    move/from16 v8, p7

    invoke-virtual/range {v1 .. v8}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->onPinch(JFFFFZ)V

    .line 858
    return-void
.end method

.method public onStackAnimationFinished()V
    .locals 1

    .prologue
    .line 1127
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStackAnimationCount:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStackAnimationCount:I

    .line 1128
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStackAnimationCount:I

    if-nez v0, :cond_0

    invoke-super {p0}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->onAnimationFinished()V

    .line 1129
    :cond_0
    return-void
.end method

.method public onStackAnimationStarted()V
    .locals 1

    .prologue
    .line 1119
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStackAnimationCount:I

    if-nez v0, :cond_0

    invoke-super {p0}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->onAnimationStarted()V

    .line 1120
    :cond_0
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStackAnimationCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStackAnimationCount:I

    .line 1121
    return-void
.end method

.method public onTabClosing(JI)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 250
    invoke-virtual {p0, p3}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->getTabStack(I)Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    move-result-object v0

    .line 251
    if-nez v0, :cond_1

    .line 259
    :cond_0
    :goto_0
    return-void

    .line 252
    :cond_1
    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->tabClosingEffect(JI)V

    .line 255
    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->startMarginAnimation(Z)V

    .line 258
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->isDisplayable()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->onTabModelSwitched(Z)V

    goto :goto_0
.end method

.method public onTabClosureCancelled(JIZ)V
    .locals 1

    .prologue
    .line 273
    invoke-super {p0, p1, p2, p3, p4}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->onTabClosureCancelled(JIZ)V

    .line 274
    invoke-virtual {p0, p4}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->getTabStack(Z)Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->undoClosure(JI)V

    .line 275
    return-void
.end method

.method public onTabCreated(JIIIZZFF)V
    .locals 3

    .prologue
    .line 319
    invoke-super/range {p0 .. p9}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->onTabCreated(JIIIZZFF)V

    .line 321
    invoke-virtual {p0, p3}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->startHiding(I)V

    .line 322
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    invoke-virtual {p0, p3}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->getTabStackIndex(I)I

    move-result v1

    aget-object v0, v0, v1

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->tabCreated(JI)V

    .line 323
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->startMarginAnimation(Z)V

    .line 324
    invoke-virtual {p0, p6}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->onTabModelSwitched(Z)V

    .line 325
    return-void
.end method

.method public onTabModelSwitched(Z)V
    .locals 1

    .prologue
    .line 329
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->flingStacks(Z)V

    .line 330
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mFlingFromModelChange:Z

    .line 331
    return-void
.end method

.method public onTabSelecting(JI)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 238
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    const/4 v1, 0x1

    aget-object v0, v0, v1

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->ensureCleaningUpDyingTabs(J)V

    .line 239
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    aget-object v0, v0, v2

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->ensureCleaningUpDyingTabs(J)V

    .line 240
    const/4 v0, -0x1

    if-ne p3, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->getCurrentTabId()I

    move-result p3

    .line 241
    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->onTabSelecting(JI)V

    .line 242
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->getTabStackIndex()I

    move-result v1

    aget-object v0, v0, v1

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->tabSelectingEffect(JI)V

    .line 243
    invoke-direct {p0, v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->startMarginAnimation(Z)V

    .line 244
    invoke-direct {p0, v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->startYOffsetAnimation(Z)V

    .line 245
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->finishScrollStacks()V

    .line 246
    return-void
.end method

.method public onTabsAllClosing(JZ)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 263
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->onTabsAllClosing(JZ)V

    .line 264
    invoke-virtual {p0, p3}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->getTabStack(Z)Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->tabsAllClosingEffect(J)V

    .line 266
    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->startMarginAnimation(Z)V

    .line 268
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->isDisplayable()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->onTabModelSwitched(Z)V

    .line 269
    :cond_0
    return-void
.end method

.method public onUpOrCancel(J)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 822
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->getTabStackIndex()I

    move-result v2

    .line 823
    rsub-int/lit8 v3, v2, 0x1

    .line 824
    iget-boolean v4, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mClicked:Z

    if-nez v4, :cond_0

    int-to-float v2, v2

    iget v4, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mRenderedScrollOffset:F

    add-float/2addr v2, v4

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    const v4, 0x3ecccccd    # 0.4f

    cmpl-float v2, v2, v4

    if-lez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    aget-object v2, v2, v3

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->isDisplayable()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 827
    if-ne v3, v0, :cond_1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->setActiveStackState(Z)Z

    .line 829
    :cond_0
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mClicked:Z

    .line 830
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->finishScrollStacks()V

    .line 831
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->getTabStackIndex()I

    move-result v1

    aget-object v0, v0, v1

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->onUpOrCancel(J)V

    .line 832
    sget-object v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;->NONE:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mInputMode:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$SwipeMode;

    .line 833
    return-void

    :cond_1
    move v0, v1

    .line 827
    goto :goto_0
.end method

.method public onUpdateAnimation(JZ)Z
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 335
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->onUpdateAnimation(JZ)Z

    move-result v2

    .line 336
    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    aget-object v3, v3, v1

    invoke-virtual {v3, p1, p2, p3}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->onUpdateViewAnimation(JZ)Z

    move-result v3

    .line 337
    iget-object v4, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    aget-object v4, v4, v0

    invoke-virtual {v4, p1, p2, p3}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->onUpdateViewAnimation(JZ)Z

    move-result v4

    .line 338
    iget-object v5, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    aget-object v5, v5, v1

    invoke-virtual {v5, p1, p2, p3}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->onUpdateCompositorAnimations(JZ)Z

    move-result v5

    .line 339
    iget-object v6, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    aget-object v6, v6, v0

    invoke-virtual {v6, p1, p2, p3}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->onUpdateCompositorAnimations(JZ)Z

    move-result v6

    .line 340
    if-eqz v2, :cond_0

    if-eqz v3, :cond_0

    if-eqz v4, :cond_0

    if-eqz v5, :cond_0

    if-eqz v6, :cond_0

    .line 348
    :goto_0
    return v0

    .line 344
    :cond_0
    if-eqz v2, :cond_1

    if-eqz v5, :cond_1

    if-nez v6, :cond_2

    .line 345
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->requestStackUpdate()V

    :cond_2
    move v0, v1

    .line 348
    goto :goto_0
.end method

.method public setActiveStackState(Z)Z
    .locals 1

    .prologue
    .line 937
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->isIncognitoSelected()Z

    move-result v0

    if-ne p1, v0, :cond_0

    const/4 v0, 0x0

    .line 939
    :goto_0
    return v0

    .line 938
    :cond_0
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mTemporarySelectedStack:Ljava/lang/Boolean;

    .line 939
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setProperty(Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$Property;F)V
    .locals 2

    .prologue
    .line 1101
    sget-object v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$2;->$SwitchMap$com$google$android$apps$chrome$tabs$layout$phone$StackLayout$Property:[I

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$Property;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1113
    :goto_0
    return-void

    .line 1103
    :pswitch_0
    iput p2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mRenderedScrollOffset:F

    .line 1104
    iput p2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mScrollIndexOffset:F

    goto :goto_0

    .line 1107
    :pswitch_1
    iput p2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mInnerMarginPercent:F

    goto :goto_0

    .line 1110
    :pswitch_2
    iput p2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStackOffsetYPercent:F

    goto :goto_0

    .line 1101
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public bridge synthetic setProperty(Ljava/lang/Enum;F)V
    .locals 0

    .prologue
    .line 44
    check-cast p1, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$Property;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->setProperty(Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$Property;F)V

    return-void
.end method

.method public setTabModelSelector(Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;Lcom/google/android/apps/chrome/compositor/TabContentManager;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 182
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->setTabModelSelector(Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;Lcom/google/android/apps/chrome/compositor/TabContentManager;)V

    .line 183
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    aget-object v0, v0, v1

    invoke-interface {p1, v1}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->getModel(Z)Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->setTabModel(Lorg/chromium/chrome/browser/tabmodel/TabModel;)V

    .line 184
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    aget-object v0, v0, v2

    invoke-interface {p1, v2}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->getModel(Z)Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->setTabModel(Lorg/chromium/chrome/browser/tabmodel/TabModel;)V

    .line 185
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->resetScrollData()V

    .line 186
    return-void
.end method

.method public show(JZ)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 445
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->show(JZ)V

    .line 449
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mViewContainer:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 451
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v3, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->NOTIFICATIONS:[I

    iget-object v4, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-static {v0, v3, v4}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->registerForNotifications(Landroid/content/Context;[ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    .line 453
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_1

    .line 454
    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    aget-object v3, v3, v0

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->reset()V

    .line 455
    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    aget-object v3, v3, v0

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->isDisplayable()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 456
    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    aget-object v3, v3, v0

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->show()V

    .line 453
    :goto_1
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 458
    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    aget-object v3, v3, v0

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->cleanupTabs()V

    goto :goto_1

    .line 462
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mOrderComparator:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$OrderComparator;

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mSortingComparator:Ljava/util/Comparator;

    .line 463
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->resetScrollData()V

    .line 464
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    move v3, v0

    :goto_2
    if-ltz v3, :cond_5

    .line 465
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    aget-object v0, v0, v3

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->isDisplayable()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 466
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->getTabStackIndex()I

    move-result v0

    if-eq v3, v0, :cond_3

    move v0, v1

    .line 467
    :goto_3
    iget-object v4, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    aget-object v4, v4, v3

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    invoke-virtual {v4, p1, p2, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->stackEntered(JZ)V

    .line 464
    :cond_2
    add-int/lit8 v0, v3, -0x1

    move v3, v0

    goto :goto_2

    :cond_3
    move v0, v2

    .line 466
    goto :goto_3

    :cond_4
    move v0, v2

    .line 467
    goto :goto_4

    .line 470
    :cond_5
    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->startMarginAnimation(Z)V

    .line 471
    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->startYOffsetAnimation(Z)V

    .line 472
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->getTabStackIndex()I

    move-result v0

    if-ne v0, v1, :cond_6

    move v2, v1

    :cond_6
    invoke-direct {p0, v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->flingStacks(Z)V

    .line 474
    if-nez p3, :cond_7

    invoke-virtual {p0, p1, p2, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->onUpdateAnimation(JZ)Z

    .line 478
    :cond_7
    const-wide/16 v0, 0x0

    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->updateLayout(JJ)V

    .line 479
    return-void
.end method

.method public startHiding(I)V
    .locals 3

    .prologue
    .line 971
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->startHiding(I)V

    .line 972
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->commitAllTabClosures()V

    .line 973
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->NOTIFICATIONS:[I

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->unregisterForNotifications(Landroid/content/Context;[ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    .line 975
    return-void
.end method

.method public swipeFinished(J)V
    .locals 3

    .prologue
    .line 493
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->getTabStackIndex()I

    move-result v1

    aget-object v0, v0, v1

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->swipeFinished(J)V

    .line 494
    return-void
.end method

.method public swipeFlingOccurred(JFFFFFF)V
    .locals 11

    .prologue
    .line 504
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->getTabStackIndex()I

    move-result v1

    aget-object v1, v0, v1

    move-wide v2, p1

    move v4, p3

    move v5, p4

    move/from16 v6, p5

    move/from16 v7, p6

    move/from16 v8, p7

    move/from16 v9, p8

    invoke-virtual/range {v1 .. v9}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->swipeFlingOccurred(JFFFFFF)V

    .line 506
    return-void
.end method

.method public swipeStarted(JLcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;FF)V
    .locals 7

    .prologue
    .line 483
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->getTabStackIndex()I

    move-result v1

    aget-object v1, v0, v1

    move-wide v2, p1

    move-object v4, p3

    move v5, p4

    move v6, p5

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->swipeStarted(JLcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;FF)V

    .line 484
    return-void
.end method

.method public swipeUpdated(JFFFFFF)V
    .locals 11

    .prologue
    .line 488
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->getTabStackIndex()I

    move-result v1

    aget-object v1, v0, v1

    move-wide v2, p1

    move v4, p3

    move v5, p4

    move/from16 v6, p5

    move/from16 v7, p6

    move/from16 v8, p7

    move/from16 v9, p8

    invoke-virtual/range {v1 .. v9}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->swipeUpdated(JFFFFFF)V

    .line 489
    return-void
.end method

.method protected uiDoneClosingTab(JIZZ)V
    .locals 1

    .prologue
    .line 409
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    invoke-interface {v0, p5}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->getModel(Z)Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v0

    invoke-static {v0, p3, p4}, Lorg/chromium/chrome/browser/tabmodel/TabModelUtils;->closeTabById(Lorg/chromium/chrome/browser/tabmodel/TabModel;IZ)Z

    .line 410
    return-void
.end method

.method protected uiRequestingCloseTab(JI)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 388
    invoke-virtual {p0, p3}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->getTabStack(I)Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->tabClosingEffect(JI)V

    .line 390
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    invoke-interface {v0, p3}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->getModelForTabId(I)Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v0

    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->isIncognito()Z

    move-result v0

    .line 391
    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    invoke-interface {v3, v1}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->getModel(Z)Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v3

    invoke-interface {v3}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->getCount()I

    move-result v3

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    sub-int v0, v3, v0

    .line 392
    if-lez v0, :cond_2

    move v0, v1

    .line 395
    :goto_1
    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->startMarginAnimation(ZZ)V

    .line 396
    if-nez v0, :cond_0

    invoke-virtual {p0, v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->onTabModelSwitched(Z)V

    .line 397
    :cond_0
    return-void

    :cond_1
    move v0, v2

    .line 391
    goto :goto_0

    :cond_2
    move v0, v2

    .line 392
    goto :goto_1
.end method

.method protected uiSelectingTab(JI)V
    .locals 1

    .prologue
    .line 375
    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->onTabSelecting(JI)V

    .line 376
    return-void
.end method

.method protected updateLayout(JJ)V
    .locals 9

    .prologue
    .line 862
    invoke-super {p0, p1, p2, p3, p4}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->updateLayout(JJ)V

    .line 863
    const/4 v7, 0x0

    .line 865
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->getViewportParameters()Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;

    move-result-object v0

    .line 866
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStackRects:[Landroid/graphics/RectF;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;->getStack0Left()F

    move-result v2

    iput v2, v1, Landroid/graphics/RectF;->left:F

    .line 867
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStackRects:[Landroid/graphics/RectF;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStackRects:[Landroid/graphics/RectF;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    iget v2, v2, Landroid/graphics/RectF;->left:F

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;->getWidth()F

    move-result v3

    add-float/2addr v2, v3

    iput v2, v1, Landroid/graphics/RectF;->right:F

    .line 868
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStackRects:[Landroid/graphics/RectF;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;->getStack0Top()F

    move-result v2

    iput v2, v1, Landroid/graphics/RectF;->top:F

    .line 869
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStackRects:[Landroid/graphics/RectF;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStackRects:[Landroid/graphics/RectF;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    iget v2, v2, Landroid/graphics/RectF;->top:F

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;->getHeight()F

    move-result v3

    add-float/2addr v2, v3

    iput v2, v1, Landroid/graphics/RectF;->bottom:F

    .line 870
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStackRects:[Landroid/graphics/RectF;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStackRects:[Landroid/graphics/RectF;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    iget v2, v2, Landroid/graphics/RectF;->left:F

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;->getStack0ToStack1TranslationX()F

    move-result v3

    add-float/2addr v2, v3

    iput v2, v1, Landroid/graphics/RectF;->left:F

    .line 871
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStackRects:[Landroid/graphics/RectF;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStackRects:[Landroid/graphics/RectF;

    const/4 v3, 0x1

    aget-object v2, v2, v3

    iget v2, v2, Landroid/graphics/RectF;->left:F

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;->getWidth()F

    move-result v3

    add-float/2addr v2, v3

    iput v2, v1, Landroid/graphics/RectF;->right:F

    .line 872
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStackRects:[Landroid/graphics/RectF;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStackRects:[Landroid/graphics/RectF;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    iget v2, v2, Landroid/graphics/RectF;->top:F

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;->getStack0ToStack1TranslationY()F

    move-result v3

    add-float/2addr v2, v3

    iput v2, v1, Landroid/graphics/RectF;->top:F

    .line 873
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStackRects:[Landroid/graphics/RectF;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStackRects:[Landroid/graphics/RectF;

    const/4 v3, 0x1

    aget-object v2, v2, v3

    iget v2, v2, Landroid/graphics/RectF;->top:F

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$PortraitViewport;->getHeight()F

    move-result v0

    add-float/2addr v0, v2

    iput v0, v1, Landroid/graphics/RectF;->bottom:F

    .line 875
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    const/4 v1, 0x0

    aget-object v1, v0, v1

    const/high16 v0, 0x3f800000    # 1.0f

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mRenderedScrollOffset:F

    add-float/2addr v2, v0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mSortingComparator:Ljava/util/Comparator;

    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mOrderComparator:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$OrderComparator;

    if-ne v0, v3, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    const/4 v3, 0x0

    invoke-interface {v0, v3}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->getModel(Z)Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v0

    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->index()I

    move-result v0

    :goto_0
    invoke-virtual {v1, v2, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->setStackFocusInfo(FI)V

    .line 878
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    const/4 v1, 0x1

    aget-object v1, v0, v1

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mRenderedScrollOffset:F

    neg-float v2, v0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mSortingComparator:Ljava/util/Comparator;

    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mOrderComparator:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout$OrderComparator;

    if-ne v0, v3, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    const/4 v3, 0x1

    invoke-interface {v0, v3}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->getModel(Z)Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v0

    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->index()I

    move-result v0

    :goto_1
    invoke-virtual {v1, v2, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->setStackFocusInfo(FI)V

    .line 883
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStackRects:[Landroid/graphics/RectF;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {v0, p1, p2, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->computeTabPosition(JLandroid/graphics/RectF;)V

    .line 884
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    const/4 v1, 0x1

    aget-object v0, v0, v1

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStackRects:[Landroid/graphics/RectF;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-virtual {v0, p1, p2, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->computeTabPosition(JLandroid/graphics/RectF;)V

    .line 888
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->getVisibleCount()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mStacks:[Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/Stack;->getVisibleCount()I

    move-result v1

    add-int v8, v0, v1

    .line 890
    if-nez v8, :cond_3

    .line 891
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    .line 896
    :cond_0
    :goto_2
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->getTabStackIndex()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_5

    .line 898
    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    const/4 v6, 0x0

    move-object v1, p0

    move-wide v2, p1

    invoke-direct/range {v1 .. v6}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->appendVisibleLayoutTabs(JI[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;I)I

    move-result v6

    .line 899
    const/4 v4, 0x1

    iget-object v5, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-object v1, p0

    move-wide v2, p1

    invoke-direct/range {v1 .. v6}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->appendVisibleLayoutTabs(JI[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;I)I

    move-result v0

    .line 904
    :goto_3
    sget-boolean v1, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->$assertionsDisabled:Z

    if-nez v1, :cond_6

    if-eq v0, v8, :cond_6

    new-instance v0, Ljava/lang/AssertionError;

    const-string/jumbo v1, "index should be incremented up to tabVisibleCount"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 875
    :cond_1
    const/4 v0, -0x1

    goto :goto_0

    .line 878
    :cond_2
    const/4 v0, -0x1

    goto :goto_1

    .line 892
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    array-length v0, v0

    if-eq v0, v8, :cond_0

    .line 893
    :cond_4
    new-array v0, v8, [Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    goto :goto_2

    .line 901
    :cond_5
    const/4 v4, 0x1

    iget-object v5, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    const/4 v6, 0x0

    move-object v1, p0

    move-wide v2, p1

    invoke-direct/range {v1 .. v6}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->appendVisibleLayoutTabs(JI[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;I)I

    move-result v6

    .line 902
    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-object v1, p0

    move-wide v2, p1

    invoke-direct/range {v1 .. v6}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->appendVisibleLayoutTabs(JI[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;I)I

    move-result v0

    goto :goto_3

    .line 907
    :cond_6
    const/4 v0, 0x0

    move v1, v0

    move v0, v7

    :goto_4
    if-ge v1, v8, :cond_8

    .line 908
    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    aget-object v2, v2, v1

    invoke-virtual {v2, p3, p4}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->updateSnap(J)Z

    move-result v2

    if-eqz v2, :cond_7

    const/4 v0, 0x1

    .line 907
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 911
    :cond_8
    if-eqz v0, :cond_9

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->requestUpdate()V

    .line 915
    :cond_9
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->updateTabPriority()V

    .line 916
    return-void
.end method
