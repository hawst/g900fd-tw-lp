.class Lcom/google/android/apps/chrome/ntp/BookmarksPage$2;
.super Ljava/lang/Object;
.source "BookmarksPage.java"

# interfaces
.implements Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarksPageManager;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/ntp/BookmarksPage;

.field final synthetic val$listener:Lcom/google/android/apps/chrome/ntp/BookmarksPage$BookmarkSelectedListener;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/ntp/BookmarksPage;Lcom/google/android/apps/chrome/ntp/BookmarksPage$BookmarkSelectedListener;)V
    .locals 0

    .prologue
    .line 389
    iput-object p1, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPage$2;->this$0:Lcom/google/android/apps/chrome/ntp/BookmarksPage;

    iput-object p2, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPage$2;->val$listener:Lcom/google/android/apps/chrome/ntp/BookmarksPage$BookmarkSelectedListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public delete(Lcom/google/android/apps/chrome/ntp/BookmarkItemView;)V
    .locals 0

    .prologue
    .line 439
    return-void
.end method

.method public edit(Lcom/google/android/apps/chrome/ntp/BookmarkItemView;)V
    .locals 0

    .prologue
    .line 447
    return-void
.end method

.method public getFaviconImageForUrl(Ljava/lang/String;ILorg/chromium/chrome/browser/favicon/FaviconHelper$FaviconImageCallback;)V
    .locals 1

    .prologue
    .line 452
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPage$2;->this$0:Lcom/google/android/apps/chrome/ntp/BookmarksPage;

    # invokes: Lcom/google/android/apps/chrome/ntp/BookmarksPage;->getFaviconImageForUrl(Ljava/lang/String;ILorg/chromium/chrome/browser/favicon/FaviconHelper$FaviconImageCallback;)V
    invoke-static {v0, p1, p2, p3}, Lcom/google/android/apps/chrome/ntp/BookmarksPage;->access$300(Lcom/google/android/apps/chrome/ntp/BookmarksPage;Ljava/lang/String;ILorg/chromium/chrome/browser/favicon/FaviconHelper$FaviconImageCallback;)V

    .line 453
    return-void
.end method

.method public isContextMenuEnabled()Z
    .locals 1

    .prologue
    .line 412
    const/4 v0, 0x0

    return v0
.end method

.method public isDestroyed()Z
    .locals 1

    .prologue
    .line 392
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPage$2;->this$0:Lcom/google/android/apps/chrome/ntp/BookmarksPage;

    # getter for: Lcom/google/android/apps/chrome/ntp/BookmarksPage;->mIsDestroyed:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/BookmarksPage;->access$000(Lcom/google/android/apps/chrome/ntp/BookmarksPage;)Z

    move-result v0

    return v0
.end method

.method public isIncognito()Z
    .locals 1

    .prologue
    .line 397
    const/4 v0, 0x0

    return v0
.end method

.method public open(Lcom/google/android/apps/chrome/ntp/BookmarkItemView;)V
    .locals 4

    .prologue
    .line 417
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/ntp/BookmarkItemView;->isFolder()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 418
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPage$2;->this$0:Lcom/google/android/apps/chrome/ntp/BookmarksPage;

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/ntp/BookmarkItemView;->getBookmarkId()Lorg/chromium/components/bookmarks/BookmarkId;

    move-result-object v1

    const/4 v2, 0x0

    # invokes: Lcom/google/android/apps/chrome/ntp/BookmarksPage;->updateBookmarksPageContents(Lorg/chromium/components/bookmarks/BookmarkId;Z)V
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/ntp/BookmarksPage;->access$400(Lcom/google/android/apps/chrome/ntp/BookmarksPage;Lorg/chromium/components/bookmarks/BookmarkId;Z)V

    .line 422
    :goto_0
    return-void

    .line 420
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPage$2;->val$listener:Lcom/google/android/apps/chrome/ntp/BookmarksPage$BookmarkSelectedListener;

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/ntp/BookmarkItemView;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/ntp/BookmarkItemView;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/ntp/BookmarkItemView;->getFavicon()Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/apps/chrome/ntp/BookmarksPage$BookmarkSelectedListener;->onBookmarkSelected(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method public openFolder(Lcom/google/android/apps/chrome/ntp/BookmarkFolderHierarchyItem;)V
    .locals 3

    .prologue
    .line 434
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPage$2;->this$0:Lcom/google/android/apps/chrome/ntp/BookmarksPage;

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/ntp/BookmarkFolderHierarchyItem;->getFolderId()Lorg/chromium/components/bookmarks/BookmarkId;

    move-result-object v1

    const/4 v2, 0x0

    # invokes: Lcom/google/android/apps/chrome/ntp/BookmarksPage;->updateBookmarksPageContents(Lorg/chromium/components/bookmarks/BookmarkId;Z)V
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/ntp/BookmarksPage;->access$400(Lcom/google/android/apps/chrome/ntp/BookmarksPage;Lorg/chromium/components/bookmarks/BookmarkId;Z)V

    .line 435
    return-void
.end method

.method public openInNewIncognitoTab(Lcom/google/android/apps/chrome/ntp/BookmarkItemView;)V
    .locals 0

    .prologue
    .line 430
    return-void
.end method

.method public openInNewTab(Lcom/google/android/apps/chrome/ntp/BookmarkItemView;)V
    .locals 0

    .prologue
    .line 426
    return-void
.end method

.method public shouldShowOpenInNewIncognitoTab()Z
    .locals 1

    .prologue
    .line 407
    const/4 v0, 0x0

    return v0
.end method

.method public shouldShowOpenInNewTab()Z
    .locals 1

    .prologue
    .line 402
    const/4 v0, 0x0

    return v0
.end method
