.class public Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences$AboutVersionStrings;
.super Ljava/lang/Object;
.source "ChromeNativePreferences.java"


# instance fields
.field private final mApplicationVersion:Ljava/lang/String;

.field private final mJavascriptVersion:Ljava/lang/String;

.field private final mOSVersion:Ljava/lang/String;

.field private final mWebkitVersion:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 123
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 124
    iput-object p1, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences$AboutVersionStrings;->mApplicationVersion:Ljava/lang/String;

    .line 125
    iput-object p2, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences$AboutVersionStrings;->mWebkitVersion:Ljava/lang/String;

    .line 126
    iput-object p3, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences$AboutVersionStrings;->mJavascriptVersion:Ljava/lang/String;

    .line 127
    iput-object p4, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences$AboutVersionStrings;->mOSVersion:Ljava/lang/String;

    .line 128
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences$1;)V
    .locals 0

    .prologue
    .line 116
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences$AboutVersionStrings;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public getApplicationVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences$AboutVersionStrings;->mApplicationVersion:Ljava/lang/String;

    return-object v0
.end method

.method public getJavascriptVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences$AboutVersionStrings;->mJavascriptVersion:Ljava/lang/String;

    return-object v0
.end method

.method public getOSVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences$AboutVersionStrings;->mOSVersion:Ljava/lang/String;

    return-object v0
.end method

.method public getWebkitVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences$AboutVersionStrings;->mWebkitVersion:Ljava/lang/String;

    return-object v0
.end method
