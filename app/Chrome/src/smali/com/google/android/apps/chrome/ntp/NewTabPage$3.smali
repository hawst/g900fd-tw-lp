.class Lcom/google/android/apps/chrome/ntp/NewTabPage$3;
.super Lorg/chromium/chrome/browser/EmptyTabObserver;
.source "NewTabPage.java"


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/ntp/NewTabPage;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/ntp/NewTabPage;)V
    .locals 0

    .prologue
    .line 306
    iput-object p1, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage$3;->this$0:Lcom/google/android/apps/chrome/ntp/NewTabPage;

    invoke-direct {p0}, Lorg/chromium/chrome/browser/EmptyTabObserver;-><init>()V

    return-void
.end method


# virtual methods
.method public onDidFailLoad(Lorg/chromium/chrome/browser/Tab;ZZILjava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 317
    if-eqz p2, :cond_0

    if-eqz p3, :cond_0

    .line 318
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage$3;->this$0:Lcom/google/android/apps/chrome/ntp/NewTabPage;

    # getter for: Lcom/google/android/apps/chrome/ntp/NewTabPage;->mNewTabPageView:Lcom/google/android/apps/chrome/ntp/NewTabPageView;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/NewTabPage;->access$200(Lcom/google/android/apps/chrome/ntp/NewTabPage;)Lcom/google/android/apps/chrome/ntp/NewTabPageView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->setUrlFocusAnimationsDisabled(Z)V

    .line 320
    :cond_0
    return-void
.end method

.method public onLoadUrl(Lorg/chromium/chrome/browser/Tab;Ljava/lang/String;I)V
    .locals 2

    .prologue
    .line 309
    invoke-static {p2}, Lcom/google/android/apps/chrome/ntp/NewTabPage;->isNTPUrl(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    if-eqz p3, :cond_0

    .line 310
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPage$3;->this$0:Lcom/google/android/apps/chrome/ntp/NewTabPage;

    # getter for: Lcom/google/android/apps/chrome/ntp/NewTabPage;->mNewTabPageView:Lcom/google/android/apps/chrome/ntp/NewTabPageView;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/NewTabPage;->access$200(Lcom/google/android/apps/chrome/ntp/NewTabPage;)Lcom/google/android/apps/chrome/ntp/NewTabPageView;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->setUrlFocusAnimationsDisabled(Z)V

    .line 312
    :cond_0
    return-void
.end method
