.class Lcom/google/android/apps/chrome/ntp/BookmarksPage$BookmarksPageManagerImpl;
.super Ljava/lang/Object;
.source "BookmarksPage.java"

# interfaces
.implements Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarksPageManager;


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field protected mTab:Lorg/chromium/chrome/browser/Tab;

.field protected mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

.field final synthetic this$0:Lcom/google/android/apps/chrome/ntp/BookmarksPage;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 134
    const-class v0, Lcom/google/android/apps/chrome/ntp/BookmarksPage;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/ntp/BookmarksPage$BookmarksPageManagerImpl;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/google/android/apps/chrome/ntp/BookmarksPage;Lorg/chromium/chrome/browser/Tab;Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;)V
    .locals 0

    .prologue
    .line 138
    iput-object p1, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPage$BookmarksPageManagerImpl;->this$0:Lcom/google/android/apps/chrome/ntp/BookmarksPage;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 139
    iput-object p2, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPage$BookmarksPageManagerImpl;->mTab:Lorg/chromium/chrome/browser/Tab;

    .line 140
    iput-object p3, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPage$BookmarksPageManagerImpl;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    .line 141
    return-void
.end method


# virtual methods
.method public delete(Lcom/google/android/apps/chrome/ntp/BookmarkItemView;)V
    .locals 2

    .prologue
    .line 210
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPage$BookmarksPageManagerImpl;->this$0:Lcom/google/android/apps/chrome/ntp/BookmarksPage;

    # getter for: Lcom/google/android/apps/chrome/ntp/BookmarksPage;->mBookmarksBridge:Lorg/chromium/chrome/browser/BookmarksBridge;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/BookmarksPage;->access$100(Lcom/google/android/apps/chrome/ntp/BookmarksPage;)Lorg/chromium/chrome/browser/BookmarksBridge;

    move-result-object v0

    if-nez v0, :cond_0

    .line 212
    :goto_0
    return-void

    .line 211
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPage$BookmarksPageManagerImpl;->this$0:Lcom/google/android/apps/chrome/ntp/BookmarksPage;

    # getter for: Lcom/google/android/apps/chrome/ntp/BookmarksPage;->mBookmarksBridge:Lorg/chromium/chrome/browser/BookmarksBridge;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/BookmarksPage;->access$100(Lcom/google/android/apps/chrome/ntp/BookmarksPage;)Lorg/chromium/chrome/browser/BookmarksBridge;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/ntp/BookmarkItemView;->getBookmarkId()Lorg/chromium/components/bookmarks/BookmarkId;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/chromium/chrome/browser/BookmarksBridge;->deleteBookmark(Lorg/chromium/components/bookmarks/BookmarkId;)V

    goto :goto_0
.end method

.method public edit(Lcom/google/android/apps/chrome/ntp/BookmarkItemView;)V
    .locals 1

    .prologue
    .line 222
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPage$BookmarksPageManagerImpl;->this$0:Lcom/google/android/apps/chrome/ntp/BookmarksPage;

    # invokes: Lcom/google/android/apps/chrome/ntp/BookmarksPage;->edit(Lcom/google/android/apps/chrome/ntp/BookmarkItemView;)V
    invoke-static {v0, p1}, Lcom/google/android/apps/chrome/ntp/BookmarksPage;->access$200(Lcom/google/android/apps/chrome/ntp/BookmarksPage;Lcom/google/android/apps/chrome/ntp/BookmarkItemView;)V

    .line 223
    return-void
.end method

.method public getFaviconImageForUrl(Ljava/lang/String;ILorg/chromium/chrome/browser/favicon/FaviconHelper$FaviconImageCallback;)V
    .locals 1

    .prologue
    .line 228
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPage$BookmarksPageManagerImpl;->this$0:Lcom/google/android/apps/chrome/ntp/BookmarksPage;

    # invokes: Lcom/google/android/apps/chrome/ntp/BookmarksPage;->getFaviconImageForUrl(Ljava/lang/String;ILorg/chromium/chrome/browser/favicon/FaviconHelper$FaviconImageCallback;)V
    invoke-static {v0, p1, p2, p3}, Lcom/google/android/apps/chrome/ntp/BookmarksPage;->access$300(Lcom/google/android/apps/chrome/ntp/BookmarksPage;Ljava/lang/String;ILorg/chromium/chrome/browser/favicon/FaviconHelper$FaviconImageCallback;)V

    .line 229
    return-void
.end method

.method public isContextMenuEnabled()Z
    .locals 1

    .prologue
    .line 171
    const/4 v0, 0x1

    return v0
.end method

.method public isDestroyed()Z
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPage$BookmarksPageManagerImpl;->this$0:Lcom/google/android/apps/chrome/ntp/BookmarksPage;

    # getter for: Lcom/google/android/apps/chrome/ntp/BookmarksPage;->mIsDestroyed:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/BookmarksPage;->access$000(Lcom/google/android/apps/chrome/ntp/BookmarksPage;)Z

    move-result v0

    return v0
.end method

.method public isIncognito()Z
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPage$BookmarksPageManagerImpl;->mTab:Lorg/chromium/chrome/browser/Tab;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/Tab;->isIncognito()Z

    move-result v0

    return v0
.end method

.method public open(Lcom/google/android/apps/chrome/ntp/BookmarkItemView;)V
    .locals 4

    .prologue
    .line 176
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/ntp/BookmarkItemView;->isFolder()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 177
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPage$BookmarksPageManagerImpl;->mTab:Lorg/chromium/chrome/browser/Tab;

    new-instance v1, Lorg/chromium/content_public/browser/LoadUrlParams;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "chrome-native://bookmarks/#"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/ntp/BookmarkItemView;->getBookmarkId()Lorg/chromium/components/bookmarks/BookmarkId;

    move-result-object v3

    invoke-virtual {v3}, Lorg/chromium/components/bookmarks/BookmarkId;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/chromium/content_public/browser/LoadUrlParams;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lorg/chromium/chrome/browser/Tab;->loadUrl(Lorg/chromium/content_public/browser/LoadUrlParams;)I

    .line 183
    :goto_0
    return-void

    .line 180
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ntp/BookmarksPage$BookmarksPageManagerImpl;->recordOpenedBookmark()V

    .line 181
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPage$BookmarksPageManagerImpl;->mTab:Lorg/chromium/chrome/browser/Tab;

    new-instance v1, Lorg/chromium/content_public/browser/LoadUrlParams;

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/ntp/BookmarkItemView;->getUrl()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/chromium/content_public/browser/LoadUrlParams;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lorg/chromium/chrome/browser/Tab;->loadUrl(Lorg/chromium/content_public/browser/LoadUrlParams;)I

    goto :goto_0
.end method

.method public openFolder(Lcom/google/android/apps/chrome/ntp/BookmarkFolderHierarchyItem;)V
    .locals 4

    .prologue
    .line 204
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPage$BookmarksPageManagerImpl;->mTab:Lorg/chromium/chrome/browser/Tab;

    new-instance v1, Lorg/chromium/content_public/browser/LoadUrlParams;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "chrome-native://bookmarks/#"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/ntp/BookmarkFolderHierarchyItem;->getFolderId()Lorg/chromium/components/bookmarks/BookmarkId;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/chromium/content_public/browser/LoadUrlParams;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lorg/chromium/chrome/browser/Tab;->loadUrl(Lorg/chromium/content_public/browser/LoadUrlParams;)I

    .line 206
    return-void
.end method

.method public openInNewIncognitoTab(Lcom/google/android/apps/chrome/ntp/BookmarkItemView;)V
    .locals 5

    .prologue
    .line 196
    sget-boolean v0, Lcom/google/android/apps/chrome/ntp/BookmarksPage$BookmarksPageManagerImpl;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/ntp/BookmarkItemView;->isFolder()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 197
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ntp/BookmarksPage$BookmarksPageManagerImpl;->recordOpenedBookmark()V

    .line 198
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPage$BookmarksPageManagerImpl;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    new-instance v1, Lorg/chromium/content_public/browser/LoadUrlParams;

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/ntp/BookmarkItemView;->getUrl()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/chromium/content_public/browser/LoadUrlParams;-><init>(Ljava/lang/String;)V

    sget-object v2, Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;->FROM_LONGPRESS_BACKGROUND:Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;

    iget-object v3, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPage$BookmarksPageManagerImpl;->mTab:Lorg/chromium/chrome/browser/Tab;

    const/4 v4, 0x1

    invoke-interface {v0, v1, v2, v3, v4}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->openNewTab(Lorg/chromium/content_public/browser/LoadUrlParams;Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;Lorg/chromium/chrome/browser/Tab;Z)Lorg/chromium/chrome/browser/Tab;

    .line 200
    return-void
.end method

.method public openInNewTab(Lcom/google/android/apps/chrome/ntp/BookmarkItemView;)V
    .locals 5

    .prologue
    .line 187
    sget-boolean v0, Lcom/google/android/apps/chrome/ntp/BookmarksPage$BookmarksPageManagerImpl;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/ntp/BookmarkItemView;->isFolder()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 188
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ntp/BookmarksPage$BookmarksPageManagerImpl;->recordOpenedBookmark()V

    .line 189
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPage$BookmarksPageManagerImpl;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    new-instance v1, Lorg/chromium/content_public/browser/LoadUrlParams;

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/ntp/BookmarkItemView;->getUrl()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/chromium/content_public/browser/LoadUrlParams;-><init>(Ljava/lang/String;)V

    sget-object v2, Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;->FROM_LONGPRESS_BACKGROUND:Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;

    iget-object v3, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPage$BookmarksPageManagerImpl;->mTab:Lorg/chromium/chrome/browser/Tab;

    iget-object v4, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPage$BookmarksPageManagerImpl;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    invoke-interface {v4}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->isIncognitoSelected()Z

    move-result v4

    invoke-interface {v0, v1, v2, v3, v4}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->openNewTab(Lorg/chromium/content_public/browser/LoadUrlParams;Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;Lorg/chromium/chrome/browser/Tab;Z)Lorg/chromium/chrome/browser/Tab;

    .line 192
    return-void
.end method

.method protected recordOpenedBookmark()V
    .locals 1

    .prologue
    .line 144
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ntp/BookmarksPage$BookmarksPageManagerImpl;->isIncognito()Z

    move-result v0

    if-nez v0, :cond_0

    .line 145
    const/4 v0, 0x5

    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/NewTabPageUma;->recordAction(I)V

    .line 147
    :cond_0
    return-void
.end method

.method public shouldShowOpenInNewIncognitoTab()Z
    .locals 1

    .prologue
    .line 166
    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->isIncognitoModeEnabled()Z

    move-result v0

    return v0
.end method

.method public shouldShowOpenInNewTab()Z
    .locals 1

    .prologue
    .line 161
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ntp/BookmarksPage$BookmarksPageManagerImpl;->isIncognito()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
