.class Lcom/google/android/apps/chrome/tab/ChromeTab$7;
.super Lorg/chromium/content/browser/WebContentsObserverAndroid;
.source "ChromeTab.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1295
    const-class v0, Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/tab/ChromeTab$7;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lcom/google/android/apps/chrome/tab/ChromeTab;Lorg/chromium/content_public/browser/WebContents;)V
    .locals 0

    .prologue
    .line 1295
    iput-object p1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$7;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-direct {p0, p2}, Lorg/chromium/content/browser/WebContentsObserverAndroid;-><init>(Lorg/chromium/content_public/browser/WebContents;)V

    return-void
.end method

.method private showSadTab()V
    .locals 5

    .prologue
    const/4 v4, -0x1

    .line 1339
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$7;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1340
    new-instance v0, Lcom/google/android/apps/chrome/tab/ChromeTab$7$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/tab/ChromeTab$7$1;-><init>(Lcom/google/android/apps/chrome/tab/ChromeTab$7;)V

    .line 1346
    new-instance v1, Lcom/google/android/apps/chrome/tab/ChromeTab$7$2;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/tab/ChromeTab$7$2;-><init>(Lcom/google/android/apps/chrome/tab/ChromeTab$7;)V

    .line 1354
    sget-boolean v2, Lcom/google/android/apps/chrome/tab/ChromeTab$7;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$7;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # getter for: Lcom/google/android/apps/chrome/tab/ChromeTab;->mSadTabView:Landroid/view/View;
    invoke-static {v2}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$3900(Lcom/google/android/apps/chrome/tab/ChromeTab;)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1355
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$7;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    iget-object v3, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$7;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # invokes: Lcom/google/android/apps/chrome/tab/ChromeTab;->getApplicationContext()Landroid/content/Context;
    invoke-static {v3}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$4400(Lcom/google/android/apps/chrome/tab/ChromeTab;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v0, v1}, Lcom/google/android/apps/chrome/tab/SadTabViewFactory;->createSadTabView(Landroid/content/Context;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;)Landroid/view/View;

    move-result-object v0

    # setter for: Lcom/google/android/apps/chrome/tab/ChromeTab;->mSadTabView:Landroid/view/View;
    invoke-static {v2, v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$3902(Lcom/google/android/apps/chrome/tab/ChromeTab;Landroid/view/View;)Landroid/view/View;

    .line 1359
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$7;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->getContainerView()Landroid/view/ViewGroup;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$7;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # getter for: Lcom/google/android/apps/chrome/tab/ChromeTab;->mSadTabView:Landroid/view/View;
    invoke-static {v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$3900(Lcom/google/android/apps/chrome/tab/ChromeTab;)Landroid/view/View;

    move-result-object v1

    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v2, v4, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1363
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$7;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # getter for: Lcom/google/android/apps/chrome/tab/ChromeTab;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$2400(Lcom/google/android/apps/chrome/tab/ChromeTab;)Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1364
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$7;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # getter for: Lcom/google/android/apps/chrome/tab/ChromeTab;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$2400(Lcom/google/android/apps/chrome/tab/ChromeTab;)Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->setPositionsForTabToNonFullscreen()V

    .line 1366
    :cond_2
    return-void
.end method


# virtual methods
.method public detachFromWebContents()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1458
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$7;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # invokes: Lcom/google/android/apps/chrome/tab/ChromeTab;->getApplicationContext()Landroid/content/Context;
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$6000(Lcom/google/android/apps/chrome/tab/ChromeTab;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$7;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getId()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$7;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getUrl()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v3, v3, v2}, Lcom/google/android/apps/chrome/MediaCaptureNotificationService;->updateMediaNotificationForTab(Landroid/content/Context;IZZLjava/lang/String;)V

    .line 1460
    invoke-super {p0}, Lorg/chromium/content/browser/WebContentsObserverAndroid;->detachFromWebContents()V

    .line 1461
    return-void
.end method

.method public didAttachInterstitialPage()V
    .locals 4

    .prologue
    .line 1438
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$7;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    iget-object v1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$7;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # getter for: Lcom/google/android/apps/chrome/tab/ChromeTab;->mNativeChromeTab:J
    invoke-static {v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$5700(Lcom/google/android/apps/chrome/tab/ChromeTab;)J

    move-result-wide v2

    # invokes: Lcom/google/android/apps/chrome/tab/ChromeTab;->nativeGetKnoxCertificateFailure(J)I
    invoke-static {v0, v2, v3}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$5800(Lcom/google/android/apps/chrome/tab/ChromeTab;J)I

    move-result v0

    .line 1440
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 1441
    iget-object v1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$7;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # invokes: Lcom/google/android/apps/chrome/tab/ChromeTab;->getApplicationContext()Landroid/content/Context;
    invoke-static {v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$5900(Lcom/google/android/apps/chrome/tab/ChromeTab;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger;->logCertificateFailure(Landroid/content/Context;I)V

    .line 1444
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$7;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->showRenderedPage()V

    .line 1445
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$7;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getInfoBarContainer()Lorg/chromium/chrome/browser/infobar/InfoBarContainer;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->setVisibility(I)V

    .line 1446
    return-void
.end method

.method public didCommitProvisionalLoadForFrame(JZLjava/lang/String;I)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 1418
    if-nez p3, :cond_0

    .line 1434
    :goto_0
    return-void

    .line 1420
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$7;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # setter for: Lcom/google/android/apps/chrome/tab/ChromeTab;->mIsNativePageCommitPending:Z
    invoke-static {v2, v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$5302(Lcom/google/android/apps/chrome/tab/ChromeTab;Z)Z

    .line 1421
    const/16 v2, 0x8

    if-ne p5, v2, :cond_1

    move v0, v1

    .line 1422
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$7;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # invokes: Lcom/google/android/apps/chrome/tab/ChromeTab;->maybeShowNativePage(Ljava/lang/String;Z)Z
    invoke-static {v2, p4, v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$5400(Lcom/google/android/apps/chrome/tab/ChromeTab;Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1423
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$7;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->showRenderedPage()V

    .line 1426
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$7;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # getter for: Lcom/google/android/apps/chrome/tab/ChromeTab;->mBackgroundContentViewHelper:Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$1400(Lcom/google/android/apps/chrome/tab/ChromeTab;)Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->hasPendingBackgroundPage()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1427
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$7;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # getter for: Lcom/google/android/apps/chrome/tab/ChromeTab;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$1500(Lcom/google/android/apps/chrome/tab/ChromeTab;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 1428
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$7;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # getter for: Lcom/google/android/apps/chrome/tab/ChromeTab;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$1500(Lcom/google/android/apps/chrome/tab/ChromeTab;)Landroid/os/Handler;

    move-result-object v0

    const-wide/16 v2, 0xbb8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 1430
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$7;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->updateFullscreenEnabledState()V

    .line 1432
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$7;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # setter for: Lcom/google/android/apps/chrome/tab/ChromeTab;->mIsTabStateDirty:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$5502(Lcom/google/android/apps/chrome/tab/ChromeTab;Z)Z

    .line 1433
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$7;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # invokes: Lcom/google/android/apps/chrome/tab/ChromeTab;->notifyPageUrlChanged()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$5600(Lcom/google/android/apps/chrome/tab/ChromeTab;)V

    goto :goto_0
.end method

.method public didDetachInterstitialPage()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1450
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$7;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getInfoBarContainer()Lorg/chromium/chrome/browser/infobar/InfoBarContainer;

    move-result-object v0

    invoke-virtual {v0, v2}, Lorg/chromium/chrome/browser/infobar/InfoBarContainer;->setVisibility(I)V

    .line 1451
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$7;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    iget-object v1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$7;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getUrl()Ljava/lang/String;

    move-result-object v1

    # invokes: Lcom/google/android/apps/chrome/tab/ChromeTab;->maybeShowNativePage(Ljava/lang/String;Z)Z
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$5400(Lcom/google/android/apps/chrome/tab/ChromeTab;Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1452
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$7;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->showRenderedPage()V

    .line 1454
    :cond_0
    return-void
.end method

.method public didFailLoad(ZZILjava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1379
    if-eqz p2, :cond_1

    .line 1380
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$7;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # getter for: Lcom/google/android/apps/chrome/tab/ChromeTab;->mTabUma:Lcom/google/android/apps/chrome/tab/TabUma;
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$4000(Lcom/google/android/apps/chrome/tab/ChromeTab;)Lcom/google/android/apps/chrome/tab/TabUma;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$7;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # getter for: Lcom/google/android/apps/chrome/tab/ChromeTab;->mTabUma:Lcom/google/android/apps/chrome/tab/TabUma;
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$4000(Lcom/google/android/apps/chrome/tab/ChromeTab;)Lcom/google/android/apps/chrome/tab/TabUma;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/google/android/apps/chrome/tab/TabUma;->onLoadFailed(I)V

    .line 1381
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$7;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # setter for: Lcom/google/android/apps/chrome/tab/ChromeTab;->mIsLoading:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$2302(Lcom/google/android/apps/chrome/tab/ChromeTab;Z)Z

    .line 1382
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$7;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # setter for: Lcom/google/android/apps/chrome/tab/ChromeTab;->mLoadProgressAtViewSwapInTime:I
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$1302(Lcom/google/android/apps/chrome/tab/ChromeTab;I)I

    .line 1383
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$7;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # invokes: Lcom/google/android/apps/chrome/tab/ChromeTab;->cancelEnableFullscreenLoadDelay()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$4700(Lcom/google/android/apps/chrome/tab/ChromeTab;)V

    .line 1384
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$7;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # setter for: Lcom/google/android/apps/chrome/tab/ChromeTab;->mIsBeingRestored:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$4202(Lcom/google/android/apps/chrome/tab/ChromeTab;Z)Z

    .line 1385
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$7;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # invokes: Lcom/google/android/apps/chrome/tab/ChromeTab;->notifyPageLoadFailed(I)V
    invoke-static {v0, p3}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$4800(Lcom/google/android/apps/chrome/tab/ChromeTab;I)V

    .line 1386
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$7;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->updateFullscreenEnabledState()V

    .line 1387
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$7;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # invokes: Lcom/google/android/apps/chrome/tab/ChromeTab;->cancelThumbnailCapture()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$4100(Lcom/google/android/apps/chrome/tab/ChromeTab;)V

    .line 1389
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$7;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # invokes: Lcom/google/android/apps/chrome/tab/ChromeTab;->getApplicationContext()Landroid/content/Context;
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$4900(Lcom/google/android/apps/chrome/tab/ChromeTab;)Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$AuditEvent;->OPEN_URL_FAILURE:Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$AuditEvent;

    const-string/jumbo v2, "ChromeTab"

    invoke-static {v0, v1, v2, p5, p4}, Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger;->logIfNecessary(Landroid/content/Context;Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$AuditEvent;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1392
    const/16 v0, -0x16

    if-ne p3, v0, :cond_2

    .line 1393
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$7;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # invokes: Lcom/google/android/apps/chrome/tab/ChromeTab;->getApplicationContext()Landroid/content/Context;
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$5000(Lcom/google/android/apps/chrome/tab/ChromeTab;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p5}, Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger;->logURLBlockedReportIfNecessary(Landroid/content/Context;Ljava/lang/String;)V

    .line 1396
    :cond_2
    return-void
.end method

.method public didFinishLoad(JLjava/lang/String;Z)V
    .locals 4

    .prologue
    .line 1370
    if-eqz p4, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$7;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # invokes: Lcom/google/android/apps/chrome/tab/ChromeTab;->didFinishPageLoad()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$4500(Lcom/google/android/apps/chrome/tab/ChromeTab;)V

    .line 1371
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$7;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # invokes: Lcom/google/android/apps/chrome/tab/ChromeTab;->getApplicationContext()Landroid/content/Context;
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$4600(Lcom/google/android/apps/chrome/tab/ChromeTab;)Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$AuditEvent;->OPEN_URL_SUCCESS:Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$AuditEvent;

    const-string/jumbo v2, "ChromeTab"

    const-string/jumbo v3, ""

    invoke-static {v0, v1, v2, p3, v3}, Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger;->logIfNecessary(Landroid/content/Context;Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$AuditEvent;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1374
    return-void
.end method

.method public didNavigateMainFrame(Ljava/lang/String;Ljava/lang/String;ZZ)V
    .locals 2

    .prologue
    .line 1401
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$7;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # setter for: Lcom/google/android/apps/chrome/tab/ChromeTab;->mBaseUrl:Ljava/lang/String;
    invoke-static {v0, p2}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$5102(Lcom/google/android/apps/chrome/tab/ChromeTab;Ljava/lang/String;)Ljava/lang/String;

    .line 1403
    if-eqz p3, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$7;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # getter for: Lcom/google/android/apps/chrome/tab/ChromeTab;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$2400(Lcom/google/android/apps/chrome/tab/ChromeTab;)Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1404
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$7;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # getter for: Lcom/google/android/apps/chrome/tab/ChromeTab;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$2400(Lcom/google/android/apps/chrome/tab/ChromeTab;)Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->setPersistentFullscreenMode(Z)V

    .line 1406
    :cond_0
    return-void
.end method

.method public didStartProvisionalLoadForFrame(JJZLjava/lang/String;ZZ)V
    .locals 1

    .prologue
    .line 1412
    if-eqz p5, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$7;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # invokes: Lcom/google/android/apps/chrome/tab/ChromeTab;->didStartPageLoad(Ljava/lang/String;Z)V
    invoke-static {v0, p6, p7}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$5200(Lcom/google/android/apps/chrome/tab/ChromeTab;Ljava/lang/String;Z)V

    .line 1413
    :cond_0
    return-void
.end method

.method public renderProcessGone(Z)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1298
    const-string/jumbo v2, "ChromeTab"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "renderProcessGone() for tab id: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$7;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getId()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ", oom protected: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {p1}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ", already needs reload: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$7;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # getter for: Lcom/google/android/apps/chrome/tab/ChromeTab;->mNeedsReload:Z
    invoke-static {v4}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$3800(Lcom/google/android/apps/chrome/tab/ChromeTab;)Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1308
    iget-object v2, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$7;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # getter for: Lcom/google/android/apps/chrome/tab/ChromeTab;->mNeedsReload:Z
    invoke-static {v2}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$3800(Lcom/google/android/apps/chrome/tab/ChromeTab;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$7;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # getter for: Lcom/google/android/apps/chrome/tab/ChromeTab;->mSadTabView:Landroid/view/View;
    invoke-static {v2}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$3900(Lcom/google/android/apps/chrome/tab/ChromeTab;)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 1333
    :cond_0
    :goto_0
    return-void

    .line 1310
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$7;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # getter for: Lcom/google/android/apps/chrome/tab/ChromeTab;->mTabUma:Lcom/google/android/apps/chrome/tab/TabUma;
    invoke-static {v2}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$4000(Lcom/google/android/apps/chrome/tab/ChromeTab;)Lcom/google/android/apps/chrome/tab/TabUma;

    move-result-object v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$7;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # getter for: Lcom/google/android/apps/chrome/tab/ChromeTab;->mTabUma:Lcom/google/android/apps/chrome/tab/TabUma;
    invoke-static {v2}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$4000(Lcom/google/android/apps/chrome/tab/ChromeTab;)Lcom/google/android/apps/chrome/tab/TabUma;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tab/TabUma;->onRendererCrashed()V

    .line 1311
    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$7;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # getter for: Lcom/google/android/apps/chrome/tab/ChromeTab;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;
    invoke-static {v2}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$900(Lcom/google/android/apps/chrome/tab/ChromeTab;)Lcom/google/android/apps/chrome/ChromeActivity;

    move-result-object v2

    invoke-static {v2}, Lorg/chromium/base/ApplicationStatus;->getStateForActivity(Landroid/app/Activity;)I

    move-result v2

    .line 1312
    if-eqz p1, :cond_3

    const/4 v3, 0x4

    if-eq v2, v3, :cond_3

    const/4 v3, 0x5

    if-eq v2, v3, :cond_3

    const/4 v3, 0x6

    if-ne v2, v3, :cond_4

    .line 1317
    :cond_3
    iget-object v2, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$7;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # setter for: Lcom/google/android/apps/chrome/tab/ChromeTab;->mNeedsReload:Z
    invoke-static {v2, v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$3802(Lcom/google/android/apps/chrome/tab/ChromeTab;Z)Z

    .line 1324
    :goto_1
    iget-object v2, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$7;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # setter for: Lcom/google/android/apps/chrome/tab/ChromeTab;->mIsLoading:Z
    invoke-static {v2, v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$2302(Lcom/google/android/apps/chrome/tab/ChromeTab;Z)Z

    .line 1325
    iget-object v2, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$7;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # setter for: Lcom/google/android/apps/chrome/tab/ChromeTab;->mIsBeingRestored:Z
    invoke-static {v2, v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$4202(Lcom/google/android/apps/chrome/tab/ChromeTab;Z)Z

    .line 1331
    iget-object v2, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$7;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # invokes: Lcom/google/android/apps/chrome/tab/ChromeTab;->handleTabCrash()V
    invoke-static {v2}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$4300(Lcom/google/android/apps/chrome/tab/ChromeTab;)V

    .line 1332
    iget-object v2, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$7;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    iget-object v3, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$7;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # getter for: Lcom/google/android/apps/chrome/tab/ChromeTab;->mSadTabView:Landroid/view/View;
    invoke-static {v3}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$3900(Lcom/google/android/apps/chrome/tab/ChromeTab;)Landroid/view/View;

    move-result-object v3

    if-eqz v3, :cond_5

    :goto_2
    invoke-virtual {v2, v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->notifyTabCrashed(Z)V

    goto :goto_0

    .line 1319
    :cond_4
    iget-object v2, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$7;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # invokes: Lcom/google/android/apps/chrome/tab/ChromeTab;->cancelThumbnailCapture()V
    invoke-static {v2}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$4100(Lcom/google/android/apps/chrome/tab/ChromeTab;)V

    .line 1320
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab$7;->showSadTab()V

    .line 1321
    iget-object v2, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$7;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # getter for: Lcom/google/android/apps/chrome/tab/ChromeTab;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;
    invoke-static {v2}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$900(Lcom/google/android/apps/chrome/tab/ChromeTab;)Lcom/google/android/apps/chrome/ChromeActivity;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/chrome/uma/UmaSessionStats;->logRendererCrash(Landroid/app/Activity;)V

    goto :goto_1

    :cond_5
    move v0, v1

    .line 1332
    goto :goto_2
.end method
