.class Lcom/google/android/apps/chrome/compositor/TabContentManager$1;
.super Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;
.source "TabContentManager.java"


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/compositor/TabContentManager;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/compositor/TabContentManager;)V
    .locals 0

    .prologue
    .line 380
    iput-object p1, p0, Lcom/google/android/apps/chrome/compositor/TabContentManager$1;->this$0:Lcom/google/android/apps/chrome/compositor/TabContentManager;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4

    .prologue
    .line 383
    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    .line 407
    :cond_0
    :goto_0
    return-void

    .line 385
    :sswitch_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/TabContentManager$1;->this$0:Lcom/google/android/apps/chrome/compositor/TabContentManager;

    # invokes: Lcom/google/android/apps/chrome/compositor/TabContentManager;->unregisterNotifications()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/compositor/TabContentManager;->access$000(Lcom/google/android/apps/chrome/compositor/TabContentManager;)V

    goto :goto_0

    .line 389
    :sswitch_1
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "tabId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 390
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string/jumbo v2, "url"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 391
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    const-string/jumbo v3, "incognito"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    .line 392
    invoke-static {v1, v2}, Lcom/google/android/apps/chrome/tab/NativePageFactory;->isNativePageUrl(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 393
    iget-object v2, p0, Lcom/google/android/apps/chrome/compositor/TabContentManager$1;->this$0:Lcom/google/android/apps/chrome/compositor/TabContentManager;

    invoke-virtual {v2, v0, v1}, Lcom/google/android/apps/chrome/compositor/TabContentManager;->invalidateIfChanged(ILjava/lang/String;)V

    goto :goto_0

    .line 395
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/chrome/compositor/TabContentManager$1;->this$0:Lcom/google/android/apps/chrome/compositor/TabContentManager;

    # invokes: Lcom/google/android/apps/chrome/compositor/TabContentManager;->removeTabThumbnail(I)V
    invoke-static {v1, v0}, Lcom/google/android/apps/chrome/compositor/TabContentManager;->access$100(Lcom/google/android/apps/chrome/compositor/TabContentManager;I)V

    goto :goto_0

    .line 400
    :sswitch_2
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "sadTabShown"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 401
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/TabContentManager$1;->this$0:Lcom/google/android/apps/chrome/compositor/TabContentManager;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string/jumbo v2, "tabId"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    # invokes: Lcom/google/android/apps/chrome/compositor/TabContentManager;->removeTabThumbnail(I)V
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/compositor/TabContentManager;->access$100(Lcom/google/android/apps/chrome/compositor/TabContentManager;I)V

    goto :goto_0

    .line 383
    :sswitch_data_0
    .sparse-switch
        0x6 -> :sswitch_2
        0x8 -> :sswitch_1
        0x27 -> :sswitch_0
    .end sparse-switch
.end method
