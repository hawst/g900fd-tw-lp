.class public Lcom/google/android/apps/chrome/third_party/datausagechart/ChartDataUsageView;
.super Lcom/google/android/apps/chrome/third_party/datausagechart/ChartView;
.source "ChartDataUsageView.java"


# static fields
.field public static final DAYS_IN_CHART:I = 0x1e


# instance fields
.field private mCompressedSeries:Lcom/google/android/apps/chrome/third_party/datausagechart/ChartNetworkSeriesView;

.field private mGrid:Lcom/google/android/apps/chrome/third_party/datausagechart/ChartGridView;

.field private mHistory:Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;

.field private mLeft:J

.field private mOriginalSeries:Lcom/google/android/apps/chrome/third_party/datausagechart/ChartNetworkSeriesView;

.field private mRight:J

.field private mVertMax:J


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 63
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartDataUsageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 64
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 70
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartDataUsageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 71
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    .line 78
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 79
    new-instance v0, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartDataUsageView$TimeAxis;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartDataUsageView$TimeAxis;-><init>()V

    new-instance v1, Lcom/google/android/apps/chrome/third_party/datausagechart/InvertedChartAxis;

    new-instance v2, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartDataUsageView$DataAxis;

    invoke-direct {v2}, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartDataUsageView$DataAxis;-><init>()V

    invoke-direct {v1, v2}, Lcom/google/android/apps/chrome/third_party/datausagechart/InvertedChartAxis;-><init>(Lcom/google/android/apps/chrome/third_party/datausagechart/ChartAxis;)V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartDataUsageView;->init(Lcom/google/android/apps/chrome/third_party/datausagechart/ChartAxis;Lcom/google/android/apps/chrome/third_party/datausagechart/ChartAxis;)V

    .line 80
    return-void
.end method

.method static synthetic access$100(J)J
    .locals 2

    .prologue
    .line 41
    invoke-static {p0, p1}, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartDataUsageView;->roundUpToPowerOfTwo(J)J

    move-result-wide v0

    return-wide v0
.end method

.method private static roundUpToPowerOfTwo(J)J
    .locals 6

    .prologue
    const-wide/16 v4, 0x1

    .line 484
    sub-long v0, p0, v4

    .line 487
    const/4 v2, 0x1

    ushr-long v2, v0, v2

    or-long/2addr v0, v2

    .line 488
    const/4 v2, 0x2

    ushr-long v2, v0, v2

    or-long/2addr v0, v2

    .line 489
    const/4 v2, 0x4

    ushr-long v2, v0, v2

    or-long/2addr v0, v2

    .line 490
    const/16 v2, 0x8

    ushr-long v2, v0, v2

    or-long/2addr v0, v2

    .line 491
    const/16 v2, 0x10

    ushr-long v2, v0, v2

    or-long/2addr v0, v2

    .line 492
    const/16 v2, 0x20

    ushr-long v2, v0, v2

    or-long/2addr v0, v2

    .line 494
    add-long/2addr v0, v4

    .line 496
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    :goto_0
    return-wide v0

    :cond_0
    const-wide v0, 0x7fffffffffffffffL

    goto :goto_0
.end method

.method private updateEstimateVisible()V
    .locals 2

    .prologue
    .line 150
    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartDataUsageView;->mOriginalSeries:Lcom/google/android/apps/chrome/third_party/datausagechart/ChartNetworkSeriesView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartNetworkSeriesView;->setEstimateVisible(Z)V

    .line 151
    return-void
.end method

.method private updatePrimaryRange()V
    .locals 5

    .prologue
    .line 218
    iget-wide v0, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartDataUsageView;->mLeft:J

    .line 219
    iget-wide v2, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartDataUsageView;->mRight:J

    .line 222
    iget-object v4, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartDataUsageView;->mCompressedSeries:Lcom/google/android/apps/chrome/third_party/datausagechart/ChartNetworkSeriesView;

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartNetworkSeriesView;->getVisibility()I

    move-result v4

    if-nez v4, :cond_0

    .line 223
    iget-object v4, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartDataUsageView;->mCompressedSeries:Lcom/google/android/apps/chrome/third_party/datausagechart/ChartNetworkSeriesView;

    invoke-virtual {v4, v0, v1, v2, v3}, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartNetworkSeriesView;->setPrimaryRange(JJ)V

    .line 225
    :cond_0
    iget-object v4, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartDataUsageView;->mOriginalSeries:Lcom/google/android/apps/chrome/third_party/datausagechart/ChartNetworkSeriesView;

    invoke-virtual {v4, v0, v1, v2, v3}, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartNetworkSeriesView;->setPrimaryRange(JJ)V

    .line 229
    return-void
.end method

.method private updateVertAxisBounds()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 126
    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartDataUsageView;->mOriginalSeries:Lcom/google/android/apps/chrome/third_party/datausagechart/ChartNetworkSeriesView;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartNetworkSeriesView;->getMaxVisible()J

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartDataUsageView;->mCompressedSeries:Lcom/google/android/apps/chrome/third_party/datausagechart/ChartNetworkSeriesView;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartNetworkSeriesView;->getMaxVisible()J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    .line 131
    invoke-static {v0, v1, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    const-wide/16 v2, 0xc

    mul-long/2addr v0, v2

    const-wide/16 v2, 0xa

    div-long/2addr v0, v2

    .line 132
    const-wide/32 v2, 0x100000

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    .line 133
    invoke-static {v0, v1, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    .line 136
    iget-wide v2, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartDataUsageView;->mVertMax:J

    cmp-long v2, v0, v2

    if-eqz v2, :cond_1

    .line 137
    iput-wide v0, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartDataUsageView;->mVertMax:J

    .line 139
    iget-object v2, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartDataUsageView;->mVert:Lcom/google/android/apps/chrome/third_party/datausagechart/ChartAxis;

    invoke-interface {v2, v4, v5, v0, v1}, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartAxis;->setBounds(JJ)Z

    move-result v0

    .line 141
    if-eqz v0, :cond_0

    .line 142
    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartDataUsageView;->mOriginalSeries:Lcom/google/android/apps/chrome/third_party/datausagechart/ChartNetworkSeriesView;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartNetworkSeriesView;->invalidatePath()V

    .line 143
    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartDataUsageView;->mCompressedSeries:Lcom/google/android/apps/chrome/third_party/datausagechart/ChartNetworkSeriesView;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartNetworkSeriesView;->invalidatePath()V

    .line 145
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartDataUsageView;->mGrid:Lcom/google/android/apps/chrome/third_party/datausagechart/ChartGridView;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartGridView;->invalidate()V

    .line 147
    :cond_1
    return-void
.end method


# virtual methods
.method public bindCompressedNetworkStats(Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;)V
    .locals 6

    .prologue
    .line 108
    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartDataUsageView;->mCompressedSeries:Lcom/google/android/apps/chrome/third_party/datausagechart/ChartNetworkSeriesView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartNetworkSeriesView;->bindNetworkStats(Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;)V

    .line 109
    iget-object v1, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartDataUsageView;->mCompressedSeries:Lcom/google/android/apps/chrome/third_party/datausagechart/ChartNetworkSeriesView;

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartNetworkSeriesView;->setVisibility(I)V

    .line 110
    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartDataUsageView;->mHistory:Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;

    if-eqz v0, :cond_0

    .line 112
    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartDataUsageView;->mOriginalSeries:Lcom/google/android/apps/chrome/third_party/datausagechart/ChartNetworkSeriesView;

    iget-object v1, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartDataUsageView;->mHistory:Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->getEnd()J

    move-result-wide v2

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v1

    iget-object v4, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartDataUsageView;->mHistory:Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->getEnd()J

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, Ljava/util/TimeZone;->getOffset(J)I

    move-result v1

    int-to-long v4, v1

    sub-long/2addr v2, v4

    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartNetworkSeriesView;->setEndTime(J)V

    .line 114
    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartDataUsageView;->mCompressedSeries:Lcom/google/android/apps/chrome/third_party/datausagechart/ChartNetworkSeriesView;

    iget-object v1, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartDataUsageView;->mHistory:Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->getEnd()J

    move-result-wide v2

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v1

    iget-object v4, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartDataUsageView;->mHistory:Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->getEnd()J

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, Ljava/util/TimeZone;->getOffset(J)I

    move-result v1

    int-to-long v4, v1

    sub-long/2addr v2, v4

    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartNetworkSeriesView;->setEndTime(J)V

    .line 117
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartDataUsageView;->updateEstimateVisible()V

    .line 118
    invoke-direct {p0}, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartDataUsageView;->updatePrimaryRange()V

    .line 119
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartDataUsageView;->requestLayout()V

    .line 120
    return-void

    .line 109
    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public bindOriginalNetworkStats(Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;)V
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartDataUsageView;->mOriginalSeries:Lcom/google/android/apps/chrome/third_party/datausagechart/ChartNetworkSeriesView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartNetworkSeriesView;->bindNetworkStats(Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;)V

    .line 100
    iput-object p1, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartDataUsageView;->mHistory:Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;

    .line 101
    invoke-direct {p0}, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartDataUsageView;->updateVertAxisBounds()V

    .line 102
    invoke-direct {p0}, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartDataUsageView;->updateEstimateVisible()V

    .line 103
    invoke-direct {p0}, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartDataUsageView;->updatePrimaryRange()V

    .line 104
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartDataUsageView;->requestLayout()V

    .line 105
    return-void
.end method

.method protected onFinishInflate()V
    .locals 3

    .prologue
    .line 84
    invoke-super {p0}, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartView;->onFinishInflate()V

    .line 86
    sget v0, Lcom/google/android/apps/chrome/R$id;->grid:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartDataUsageView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartGridView;

    iput-object v0, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartDataUsageView;->mGrid:Lcom/google/android/apps/chrome/third_party/datausagechart/ChartGridView;

    .line 87
    sget v0, Lcom/google/android/apps/chrome/R$id;->series:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartDataUsageView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartNetworkSeriesView;

    iput-object v0, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartDataUsageView;->mOriginalSeries:Lcom/google/android/apps/chrome/third_party/datausagechart/ChartNetworkSeriesView;

    .line 88
    sget v0, Lcom/google/android/apps/chrome/R$id;->detail_series:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartDataUsageView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartNetworkSeriesView;

    iput-object v0, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartDataUsageView;->mCompressedSeries:Lcom/google/android/apps/chrome/third_party/datausagechart/ChartNetworkSeriesView;

    .line 91
    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartDataUsageView;->mGrid:Lcom/google/android/apps/chrome/third_party/datausagechart/ChartGridView;

    iget-object v1, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartDataUsageView;->mHoriz:Lcom/google/android/apps/chrome/third_party/datausagechart/ChartAxis;

    iget-object v2, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartDataUsageView;->mVert:Lcom/google/android/apps/chrome/third_party/datausagechart/ChartAxis;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartGridView;->init(Lcom/google/android/apps/chrome/third_party/datausagechart/ChartAxis;Lcom/google/android/apps/chrome/third_party/datausagechart/ChartAxis;)V

    .line 92
    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartDataUsageView;->mOriginalSeries:Lcom/google/android/apps/chrome/third_party/datausagechart/ChartNetworkSeriesView;

    iget-object v1, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartDataUsageView;->mHoriz:Lcom/google/android/apps/chrome/third_party/datausagechart/ChartAxis;

    iget-object v2, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartDataUsageView;->mVert:Lcom/google/android/apps/chrome/third_party/datausagechart/ChartAxis;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartNetworkSeriesView;->init(Lcom/google/android/apps/chrome/third_party/datausagechart/ChartAxis;Lcom/google/android/apps/chrome/third_party/datausagechart/ChartAxis;)V

    .line 93
    iget-object v0, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartDataUsageView;->mCompressedSeries:Lcom/google/android/apps/chrome/third_party/datausagechart/ChartNetworkSeriesView;

    iget-object v1, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartDataUsageView;->mHoriz:Lcom/google/android/apps/chrome/third_party/datausagechart/ChartAxis;

    iget-object v2, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartDataUsageView;->mVert:Lcom/google/android/apps/chrome/third_party/datausagechart/ChartAxis;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartNetworkSeriesView;->init(Lcom/google/android/apps/chrome/third_party/datausagechart/ChartAxis;Lcom/google/android/apps/chrome/third_party/datausagechart/ChartAxis;)V

    .line 94
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartDataUsageView;->setActivated(Z)V

    .line 95
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 155
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartDataUsageView;->isActivated()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 165
    :goto_0
    return v0

    .line 156
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    move v0, v1

    .line 158
    goto :goto_0

    .line 161
    :pswitch_1
    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartDataUsageView;->setActivated(Z)V

    move v0, v1

    .line 162
    goto :goto_0

    .line 156
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public setVisibleRange(JJJJ)V
    .locals 13

    .prologue
    .line 185
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v2

    move-wide/from16 v0, p7

    invoke-virtual {v2, v0, v1}, Ljava/util/TimeZone;->getOffset(J)I

    move-result v2

    int-to-long v4, v2

    .line 186
    iget-object v2, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartDataUsageView;->mHoriz:Lcom/google/android/apps/chrome/third_party/datausagechart/ChartAxis;

    move-wide/from16 v0, p3

    invoke-interface {v2, p1, p2, v0, v1}, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartAxis;->setBounds(JJ)Z

    move-result v6

    .line 187
    iget-object v2, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartDataUsageView;->mOriginalSeries:Lcom/google/android/apps/chrome/third_party/datausagechart/ChartNetworkSeriesView;

    move-wide/from16 v0, p3

    invoke-virtual {v2, p1, p2, v0, v1}, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartNetworkSeriesView;->setBounds(JJ)V

    .line 188
    iget-object v2, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartDataUsageView;->mCompressedSeries:Lcom/google/android/apps/chrome/third_party/datausagechart/ChartNetworkSeriesView;

    move-wide/from16 v0, p3

    invoke-virtual {v2, p1, p2, v0, v1}, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartNetworkSeriesView;->setBounds(JJ)V

    .line 193
    const-wide v2, 0x9a7ec800L

    sub-long v2, p3, v2

    invoke-static {p1, p2, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    .line 195
    const-wide/32 v8, 0x36ee80

    sub-long v8, p3, v8

    const-wide v10, 0x9a7ec800L

    sub-long/2addr v8, v10

    cmp-long v7, v8, p5

    if-nez v7, :cond_0

    add-long v4, v4, p7

    cmp-long v4, p3, v4

    if-eqz v4, :cond_2

    .line 200
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartDataUsageView;->setActivated(Z)V

    move-wide/from16 p3, p7

    .line 203
    :goto_0
    move-wide/from16 v0, p5

    iput-wide v0, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartDataUsageView;->mLeft:J

    .line 204
    move-wide/from16 v0, p3

    iput-wide v0, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartDataUsageView;->mRight:J

    .line 206
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartDataUsageView;->requestLayout()V

    .line 207
    if-eqz v6, :cond_1

    .line 208
    iget-object v2, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartDataUsageView;->mOriginalSeries:Lcom/google/android/apps/chrome/third_party/datausagechart/ChartNetworkSeriesView;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartNetworkSeriesView;->invalidatePath()V

    .line 209
    iget-object v2, p0, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartDataUsageView;->mCompressedSeries:Lcom/google/android/apps/chrome/third_party/datausagechart/ChartNetworkSeriesView;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartNetworkSeriesView;->invalidatePath()V

    .line 212
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartDataUsageView;->updateVertAxisBounds()V

    .line 213
    invoke-direct {p0}, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartDataUsageView;->updateEstimateVisible()V

    .line 214
    invoke-direct {p0}, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartDataUsageView;->updatePrimaryRange()V

    .line 215
    return-void

    :cond_2
    move-wide/from16 p5, v2

    goto :goto_0
.end method
