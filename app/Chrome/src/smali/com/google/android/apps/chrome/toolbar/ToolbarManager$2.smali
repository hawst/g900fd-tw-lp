.class Lcom/google/android/apps/chrome/toolbar/ToolbarManager$2;
.super Lorg/chromium/chrome/browser/EmptyTabObserver;
.source "ToolbarManager.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 122
    const-class v0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$2;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lcom/google/android/apps/chrome/toolbar/ToolbarManager;)V
    .locals 0

    .prologue
    .line 122
    iput-object p1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$2;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarManager;

    invoke-direct {p0}, Lorg/chromium/chrome/browser/EmptyTabObserver;-><init>()V

    return-void
.end method


# virtual methods
.method public onDidNavigateMainFrame(Lorg/chromium/chrome/browser/Tab;Ljava/lang/String;Ljava/lang/String;ZZI)V
    .locals 1

    .prologue
    .line 140
    if-eqz p4, :cond_0

    .line 141
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$2;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarManager;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mToolbarDelegate:Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->access$300(Lcom/google/android/apps/chrome/toolbar/ToolbarManager;)Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->onNavigatedToDifferentPage()V

    .line 143
    :cond_0
    return-void
.end method

.method public onSSLStateUpdated(Lorg/chromium/chrome/browser/Tab;)V
    .locals 2

    .prologue
    .line 125
    invoke-super {p0, p1}, Lorg/chromium/chrome/browser/EmptyTabObserver;->onSSLStateUpdated(Lorg/chromium/chrome/browser/Tab;)V

    .line 127
    sget-boolean v0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$2;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$2;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarManager;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mToolbarModel:Lcom/google/android/apps/chrome/toolbar/ToolbarModelImpl;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->access$100(Lcom/google/android/apps/chrome/toolbar/ToolbarManager;)Lcom/google/android/apps/chrome/toolbar/ToolbarModelImpl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarModelImpl;->getTab()Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    if-eq p1, v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 128
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$2;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarManager;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mLocationBar:Lcom/google/android/apps/chrome/omnibox/LocationBar;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->access$200(Lcom/google/android/apps/chrome/toolbar/ToolbarManager;)Lcom/google/android/apps/chrome/omnibox/LocationBar;

    move-result-object v0

    invoke-virtual {p1}, Lorg/chromium/chrome/browser/Tab;->getSecurityLevel()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->updateSecurityIcon(I)V

    .line 129
    return-void
.end method

.method public onWebContentsInstantSupportDisabled()V
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$2;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarManager;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mLocationBar:Lcom/google/android/apps/chrome/omnibox/LocationBar;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->access$200(Lcom/google/android/apps/chrome/toolbar/ToolbarManager;)Lcom/google/android/apps/chrome/omnibox/LocationBar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->setUrlToPageUrl()V

    .line 134
    return-void
.end method
