.class public Lcom/google/android/apps/chrome/preferences/ChromeBaseFragment;
.super Landroid/app/Fragment;
.source "ChromeBaseFragment.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    return-void
.end method

.method static addSwitchToActionBar(Landroid/app/Activity;Landroid/widget/Switch;)V
    .locals 5

    .prologue
    const/16 v4, 0x10

    const/4 v3, -0x2

    const/4 v2, 0x0

    .line 44
    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$dimen;->action_bar_switch_padding:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 46
    invoke-virtual {p1, v2, v2, v0, v2}, Landroid/widget/Switch;->setPadding(IIII)V

    .line 47
    invoke-virtual {p0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v4, v4}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    .line 49
    invoke-virtual {p0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    new-instance v1, Landroid/app/ActionBar$LayoutParams;

    const/16 v2, 0x15

    invoke-direct {v1, v3, v3, v2}, Landroid/app/ActionBar$LayoutParams;-><init>(III)V

    invoke-virtual {v0, p1, v1}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;Landroid/app/ActionBar$LayoutParams;)V

    .line 53
    return-void
.end method

.method static closeEditor(Landroid/app/Activity;Landroid/view/View;Landroid/app/FragmentManager;)V
    .locals 3

    .prologue
    .line 29
    const-string/jumbo v0, "input_method"

    invoke-virtual {p0, v0}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 31
    invoke-virtual {p1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 32
    invoke-virtual {p2}, Landroid/app/FragmentManager;->getBackStackEntryCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 33
    invoke-virtual {p2}, Landroid/app/FragmentManager;->popBackStack()V

    .line 37
    :goto_0
    return-void

    .line 35
    :cond_0
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto :goto_0
.end method


# virtual methods
.method public addSwitchToActionBar(Landroid/widget/Switch;)V
    .locals 1

    .prologue
    .line 66
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBaseFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/apps/chrome/preferences/ChromeBaseFragment;->addSwitchToActionBar(Landroid/app/Activity;Landroid/widget/Switch;)V

    .line 67
    return-void
.end method

.method public closeEditor()V
    .locals 3

    .prologue
    .line 59
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBaseFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBaseFragment;->getView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBaseFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/preferences/ChromeBaseFragment;->closeEditor(Landroid/app/Activity;Landroid/view/View;Landroid/app/FragmentManager;)V

    .line 60
    return-void
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    .line 71
    invoke-super {p0}, Landroid/app/Fragment;->onDestroy()V

    .line 72
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBaseFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    .line 73
    instance-of v0, v1, Lcom/google/android/apps/chrome/preferences/Preferences;

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 74
    check-cast v0, Lcom/google/android/apps/chrome/preferences/Preferences;

    .line 75
    invoke-virtual {v0, p0}, Lcom/google/android/apps/chrome/preferences/Preferences;->popFragmentList(Landroid/app/Fragment;)V

    .line 76
    invoke-virtual {v1}, Landroid/app/Activity;->invalidateOptionsMenu()V

    .line 77
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/Preferences;->getCurrentFragment()Landroid/app/Fragment;

    move-result-object v0

    .line 78
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-class v2, Lcom/google/android/apps/chrome/preferences/BasicsPreferences;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 80
    check-cast v0, Lcom/google/android/apps/chrome/preferences/BasicsPreferences;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/BasicsPreferences;->updateSummaries()V

    .line 83
    :cond_0
    return-void
.end method
