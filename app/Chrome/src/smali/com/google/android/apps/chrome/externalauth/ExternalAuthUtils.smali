.class public Lcom/google/android/apps/chrome/externalauth/ExternalAuthUtils;
.super Ljava/lang/Object;
.source "ExternalAuthUtils.java"


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final FLAG_SHOULD_BE_GOOGLE_SIGNED:I = 0x1

.field public static final FLAG_SHOULD_BE_SYSTEM:I = 0x2


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-class v0, Lcom/google/android/apps/chrome/externalauth/ExternalAuthUtils;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/externalauth/ExternalAuthUtils;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static canUseFirstPartyGooglePlayServices(Landroid/content/Context;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 85
    invoke-static {p0}, Lcom/google/android/gms/common/f;->a(Landroid/content/Context;)I

    move-result v1

    .line 86
    if-eqz v1, :cond_0

    .line 87
    const-string/jumbo v2, "ExternalAuthUtils"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Failed to use first party Google Play Services: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Lcom/google/android/gms/common/f;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 99
    :goto_0
    return v0

    .line 93
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/gms/common/f;->a(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 95
    const-string/jumbo v1, "ExternalAuthUtils"

    const-string/jumbo v2, "Trying to use first-party Google Play Services while running on a non-Google-signed package."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 99
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static checkCallerIsValid(Landroid/content/Context;I)Z
    .locals 1

    .prologue
    .line 153
    const-string/jumbo v0, ""

    invoke-static {p0, p1, v0}, Lcom/google/android/apps/chrome/externalauth/ExternalAuthUtils;->checkCallerIsValid(Landroid/content/Context;ILjava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static checkCallerIsValid(Landroid/content/Context;ILjava/lang/String;)Z
    .locals 11

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 112
    and-int/lit8 v0, p1, 0x1

    if-eqz v0, :cond_3

    move v5, v1

    .line 113
    :goto_0
    and-int/lit8 v0, p1, 0x2

    if-eqz v0, :cond_4

    move v0, v1

    .line 115
    :goto_1
    invoke-static {p0}, Lcom/google/android/apps/chrome/externalauth/ExternalAuthUtils;->getCallingPackages(Landroid/content/Context;)[Ljava/lang/String;

    move-result-object v6

    .line 116
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v7

    .line 119
    array-length v8, v6

    move v4, v2

    move v3, v2

    :goto_2
    if-ge v4, v8, :cond_7

    aget-object v9, v6, v4

    .line 120
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_0

    invoke-virtual {v9, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 122
    :cond_0
    if-eqz v5, :cond_1

    invoke-static {v7, v9}, Lcom/google/android/apps/chrome/externalauth/ExternalAuthUtils;->checkIsGoogleSigned(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    :cond_1
    if-eqz v0, :cond_5

    invoke-static {v7, v9}, Lcom/google/android/apps/chrome/externalauth/ExternalAuthUtils;->checkIsSystemBuild(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_5

    .line 127
    :cond_2
    :goto_3
    return v2

    :cond_3
    move v5, v2

    .line 112
    goto :goto_0

    :cond_4
    move v0, v2

    .line 113
    goto :goto_1

    :cond_5
    move v3, v1

    .line 119
    :cond_6
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    :cond_7
    move v2, v3

    .line 127
    goto :goto_3
.end method

.method public static checkCallerIsValidForPackage(Landroid/content/Context;ILjava/lang/String;)Z
    .locals 1

    .prologue
    .line 140
    sget-boolean v0, Lcom/google/android/apps/chrome/externalauth/ExternalAuthUtils;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 142
    :cond_0
    invoke-static {p0, p1, p2}, Lcom/google/android/apps/chrome/externalauth/ExternalAuthUtils;->checkCallerIsValid(Landroid/content/Context;ILjava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static checkIsGoogleSigned(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 76
    invoke-static {p0, p1}, Lcom/google/android/gms/common/f;->a(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static checkIsSystemBuild(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 46
    const/4 v2, 0x1

    :try_start_0
    invoke-virtual {p0, p1, v2}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v2

    .line 47
    iget v2, v2, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit8 v2, v2, 0x1

    if-nez v2, :cond_0

    new-instance v0, Ljava/lang/SecurityException;

    invoke-direct {v0}, Ljava/lang/SecurityException;-><init>()V

    throw v0
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1

    .line 49
    :catch_0
    move-exception v0

    const-string/jumbo v0, "ExternalAuthUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Package with name "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " not found"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    .line 56
    :cond_0
    :goto_0
    return v0

    .line 52
    :catch_1
    move-exception v0

    const-string/jumbo v0, "ExternalAuthUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Caller with package name "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " is not in the system build"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    .line 53
    goto :goto_0
.end method

.method public static getCallingPackages(Landroid/content/Context;)[Ljava/lang/String;
    .locals 2

    .prologue
    .line 34
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    .line 35
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 36
    invoke-virtual {v1, v0}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static isChromeGoogleSigned(Landroid/content/Context;)Z
    .locals 2

    .prologue
    .line 66
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/externalauth/ExternalAuthUtils;->checkIsGoogleSigned(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method
