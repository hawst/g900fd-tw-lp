.class public Lcom/google/android/apps/chrome/ntp/BookmarksPageView;
.super Landroid/widget/LinearLayout;
.source "BookmarksPageView.java"

# interfaces
.implements Lorg/chromium/chrome/browser/BookmarksBridge$BookmarksCallback;


# instance fields
.field private final mAdapter:Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarkListAdapter;

.field private mBookmarksList:Lcom/google/android/apps/chrome/ntp/BookmarksListView;

.field private mCurrentFolderId:Lorg/chromium/components/bookmarks/BookmarkId;

.field private mDefaultFavicon:Landroid/graphics/Bitmap;

.field private final mDesiredFaviconSize:I

.field private mDrawingData:Lcom/google/android/apps/chrome/ntp/BookmarkItemView$DrawingData;

.field private mEmptyView:Landroid/widget/TextView;

.field private final mFaviconCache:Landroid/util/LruCache;

.field private mHierarchyContainer:Landroid/widget/HorizontalScrollView;

.field private mHierarchyLayout:Landroid/widget/LinearLayout;

.field private mManager:Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarksPageManager;

.field private mSavedListPosition:I

.field private mSavedListTop:I

.field private mSnapshotBookmarksChanged:Z

.field private mSnapshotBookmarksHierarchyScrollX:I

.field private mSnapshotBookmarksListPosition:I

.field private mSnapshotBookmarksListTop:I

.field private mSnapshotHeight:I

.field private mSnapshotWidth:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 155
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 55
    iput v4, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->mSavedListPosition:I

    .line 56
    iput v4, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->mSavedListTop:I

    .line 157
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$dimen;->ntp_list_item_favicon_size:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->mDesiredFaviconSize:I

    .line 159
    new-instance v0, Landroid/util/LruCache;

    const/16 v1, 0x100

    invoke-direct {v0, v1}, Landroid/util/LruCache;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->mFaviconCache:Landroid/util/LruCache;

    .line 160
    new-instance v0, Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarkListAdapter;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarkListAdapter;-><init>(Lcom/google/android/apps/chrome/ntp/BookmarksPageView;Lcom/google/android/apps/chrome/ntp/BookmarksPageView$1;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->mAdapter:Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarkListAdapter;

    .line 161
    new-instance v0, Lorg/chromium/components/bookmarks/BookmarkId;

    const-wide/16 v2, -0x2

    invoke-direct {v0, v2, v3, v4}, Lorg/chromium/components/bookmarks/BookmarkId;-><init>(JI)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->mCurrentFolderId:Lorg/chromium/components/bookmarks/BookmarkId;

    .line 163
    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/ntp/BookmarksPageView;)Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarksPageManager;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->mManager:Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarksPageManager;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/chrome/ntp/BookmarksPageView;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->mHierarchyLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/chrome/ntp/BookmarksPageView;)Landroid/widget/HorizontalScrollView;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->mHierarchyContainer:Landroid/widget/HorizontalScrollView;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/chrome/ntp/BookmarksPageView;)Lcom/google/android/apps/chrome/ntp/BookmarkItemView$DrawingData;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->mDrawingData:Lcom/google/android/apps/chrome/ntp/BookmarkItemView$DrawingData;

    return-object v0
.end method

.method static synthetic access$502(Lcom/google/android/apps/chrome/ntp/BookmarksPageView;Lcom/google/android/apps/chrome/ntp/BookmarkItemView$DrawingData;)Lcom/google/android/apps/chrome/ntp/BookmarkItemView$DrawingData;
    .locals 0

    .prologue
    .line 40
    iput-object p1, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->mDrawingData:Lcom/google/android/apps/chrome/ntp/BookmarkItemView$DrawingData;

    return-object p1
.end method

.method static synthetic access$600(Lcom/google/android/apps/chrome/ntp/BookmarksPageView;)Landroid/util/LruCache;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->mFaviconCache:Landroid/util/LruCache;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/apps/chrome/ntp/BookmarksPageView;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->mDefaultFavicon:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic access$702(Lcom/google/android/apps/chrome/ntp/BookmarksPageView;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 0

    .prologue
    .line 40
    iput-object p1, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->mDefaultFavicon:Landroid/graphics/Bitmap;

    return-object p1
.end method

.method static synthetic access$802(Lcom/google/android/apps/chrome/ntp/BookmarksPageView;Z)Z
    .locals 0

    .prologue
    .line 40
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->mSnapshotBookmarksChanged:Z

    return p1
.end method

.method static synthetic access$900(Lcom/google/android/apps/chrome/ntp/BookmarksPageView;)I
    .locals 1

    .prologue
    .line 40
    iget v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->mDesiredFaviconSize:I

    return v0
.end method

.method private addItemToHierarchyView(Ljava/lang/String;Lorg/chromium/components/bookmarks/BookmarkId;Z)V
    .locals 6

    .prologue
    .line 304
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 306
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$string;->ntp_bookmarks:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 312
    :goto_0
    new-instance v0, Lcom/google/android/apps/chrome/ntp/BookmarkFolderHierarchyItem;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->mManager:Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarksPageManager;

    move-object v3, p2

    move v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/chrome/ntp/BookmarkFolderHierarchyItem;-><init>(Landroid/content/Context;Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarksPageManager;Lorg/chromium/components/bookmarks/BookmarkId;Ljava/lang/String;Z)V

    .line 314
    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->mHierarchyLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 315
    return-void

    .line 308
    :cond_0
    new-instance v0, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 309
    sget v1, Lcom/google/android/apps/chrome/R$drawable;->breadcrumb_arrow:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 310
    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->mHierarchyLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    move-object v4, p1

    goto :goto_0
.end method


# virtual methods
.method initialize(Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarksPageManager;)V
    .locals 0

    .prologue
    .line 200
    iput-object p1, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->mManager:Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarksPageManager;

    .line 201
    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 3

    .prologue
    .line 213
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 214
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->mBookmarksList:Lcom/google/android/apps/chrome/ntp/BookmarksListView;

    iget v1, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->mSavedListPosition:I

    iget v2, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->mSavedListTop:I

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/ntp/BookmarksListView;->setSelectionFromTop(II)V

    .line 215
    return-void
.end method

.method public onBookmarksAvailable(Lorg/chromium/components/bookmarks/BookmarkId;Ljava/util/List;)V
    .locals 2

    .prologue
    .line 251
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->mEmptyView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->length()I

    move-result v0

    if-nez v0, :cond_0

    .line 255
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->mEmptyView:Landroid/widget/TextView;

    sget v1, Lcom/google/android/apps/chrome/R$string;->bookmarks_folder_empty:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 258
    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->mCurrentFolderId:Lorg/chromium/components/bookmarks/BookmarkId;

    .line 259
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->mAdapter:Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarkListAdapter;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarkListAdapter;->setBookmarksList(Ljava/util/List;)V

    .line 260
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->mAdapter:Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarkListAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarkListAdapter;->notifyDataSetChanged()V

    .line 265
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->mBookmarksList:Lcom/google/android/apps/chrome/ntp/BookmarksListView;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ntp/BookmarksListView;->requestLayout()V

    .line 266
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->mBookmarksList:Lcom/google/android/apps/chrome/ntp/BookmarksListView;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ntp/BookmarksListView;->invalidate()V

    .line 269
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->mBookmarksList:Lcom/google/android/apps/chrome/ntp/BookmarksListView;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ntp/BookmarksListView;->awakenScrollBars()Z

    .line 271
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->mSnapshotBookmarksChanged:Z

    .line 272
    return-void
.end method

.method public onBookmarksFolderHierarchyAvailable(Lorg/chromium/components/bookmarks/BookmarkId;Ljava/util/List;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 277
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->mManager:Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarksPageManager;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarksPageManager;->isDestroyed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 294
    :goto_0
    return-void

    .line 278
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->mHierarchyLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 279
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v2, v0

    :goto_1
    if-ltz v2, :cond_2

    .line 280
    invoke-interface {p2, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;

    .line 281
    invoke-virtual {v0}, Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;->getId()Lorg/chromium/components/bookmarks/BookmarkId;

    move-result-object v4

    if-nez v2, :cond_1

    move v0, v1

    :goto_2
    invoke-direct {p0, v3, v4, v0}, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->addItemToHierarchyView(Ljava/lang/String;Lorg/chromium/components/bookmarks/BookmarkId;Z)V

    .line 279
    add-int/lit8 v0, v2, -0x1

    move v2, v0

    goto :goto_1

    .line 281
    :cond_1
    const/4 v0, 0x0

    goto :goto_2

    .line 283
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->mHierarchyLayout:Landroid/widget/LinearLayout;

    new-instance v2, Lcom/google/android/apps/chrome/ntp/BookmarksPageView$2;

    invoke-direct {v2, p0}, Lcom/google/android/apps/chrome/ntp/BookmarksPageView$2;-><init>(Lcom/google/android/apps/chrome/ntp/BookmarksPageView;)V

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 293
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->mSnapshotBookmarksChanged:Z

    goto :goto_0
.end method

.method public onCreateInputConnection(Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;
    .locals 1

    .prologue
    .line 191
    const/high16 v0, 0x2000000

    iput v0, p1, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    .line 192
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onCreateInputConnection(Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;

    move-result-object v0

    return-object v0
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 205
    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->mBookmarksList:Lcom/google/android/apps/chrome/ntp/BookmarksListView;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/ntp/BookmarksListView;->getFirstVisiblePosition()I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->mSavedListPosition:I

    .line 206
    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->mBookmarksList:Lcom/google/android/apps/chrome/ntp/BookmarksListView;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/ntp/BookmarksListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 207
    if-nez v1, :cond_0

    :goto_0
    iput v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->mSavedListTop:I

    .line 208
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    .line 209
    return-void

    .line 207
    :cond_0
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v0

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 167
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 169
    sget v0, Lcom/google/android/apps/chrome/R$id;->folder_structure_scroll_view:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/HorizontalScrollView;

    iput-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->mHierarchyContainer:Landroid/widget/HorizontalScrollView;

    .line 171
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->mHierarchyContainer:Landroid/widget/HorizontalScrollView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/HorizontalScrollView;->setSmoothScrollingEnabled(Z)V

    .line 172
    sget v0, Lcom/google/android/apps/chrome/R$id;->bookmark_folder_structure:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->mHierarchyLayout:Landroid/widget/LinearLayout;

    .line 174
    sget v0, Lcom/google/android/apps/chrome/R$id;->bookmarks_list_view:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/ntp/BookmarksListView;

    iput-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->mBookmarksList:Lcom/google/android/apps/chrome/ntp/BookmarksListView;

    .line 175
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->mBookmarksList:Lcom/google/android/apps/chrome/ntp/BookmarksListView;

    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->mAdapter:Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarkListAdapter;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/ntp/BookmarksListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 176
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->mBookmarksList:Lcom/google/android/apps/chrome/ntp/BookmarksListView;

    new-instance v1, Lcom/google/android/apps/chrome/ntp/BookmarksPageView$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/ntp/BookmarksPageView$1;-><init>(Lcom/google/android/apps/chrome/ntp/BookmarksPageView;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/ntp/BookmarksListView;->setDropListener(Lcom/google/android/apps/chrome/ntp/BookmarksListView$DropListener;)V

    .line 184
    sget v0, Lcom/google/android/apps/chrome/R$id;->bookmarks_empty_view:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->mEmptyView:Landroid/widget/TextView;

    .line 185
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->mBookmarksList:Lcom/google/android/apps/chrome/ntp/BookmarksListView;

    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->mEmptyView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/ntp/BookmarksListView;->setEmptyView(Landroid/view/View;)V

    .line 186
    return-void
.end method

.method shouldCaptureThumbnail()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 222
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->getWidth()I

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->getHeight()I

    move-result v1

    if-nez v1, :cond_1

    .line 225
    :cond_0
    :goto_0
    return v0

    .line 224
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->mBookmarksList:Lcom/google/android/apps/chrome/ntp/BookmarksListView;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/ntp/BookmarksListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 225
    iget-boolean v2, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->mSnapshotBookmarksChanged:Z

    if-nez v2, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->getWidth()I

    move-result v2

    iget v3, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->mSnapshotWidth:I

    if-ne v2, v3, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->getHeight()I

    move-result v2

    iget v3, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->mSnapshotHeight:I

    if-ne v2, v3, :cond_2

    iget v2, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->mSnapshotBookmarksListPosition:I

    iget-object v3, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->mBookmarksList:Lcom/google/android/apps/chrome/ntp/BookmarksListView;

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/ntp/BookmarksListView;->getFirstVisiblePosition()I

    move-result v3

    if-ne v2, v3, :cond_2

    iget v2, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->mSnapshotBookmarksListTop:I

    if-nez v1, :cond_3

    move v1, v0

    :goto_1
    if-ne v2, v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->mHierarchyContainer:Landroid/widget/HorizontalScrollView;

    invoke-virtual {v1}, Landroid/widget/HorizontalScrollView;->getScrollX()I

    move-result v1

    iget v2, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->mSnapshotBookmarksHierarchyScrollX:I

    if-eq v1, v2, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v1

    goto :goto_1
.end method

.method updateThumbnailState()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 238
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->mSnapshotWidth:I

    .line 239
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->mSnapshotHeight:I

    .line 240
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->mBookmarksList:Lcom/google/android/apps/chrome/ntp/BookmarksListView;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ntp/BookmarksListView;->getFirstVisiblePosition()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->mSnapshotBookmarksListPosition:I

    .line 241
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->mBookmarksList:Lcom/google/android/apps/chrome/ntp/BookmarksListView;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/ntp/BookmarksListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 242
    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    iput v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->mSnapshotBookmarksListTop:I

    .line 243
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->mHierarchyContainer:Landroid/widget/HorizontalScrollView;

    invoke-virtual {v0}, Landroid/widget/HorizontalScrollView;->getScrollX()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->mSnapshotBookmarksHierarchyScrollX:I

    .line 244
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->mSnapshotBookmarksChanged:Z

    .line 245
    return-void

    .line 242
    :cond_0
    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    goto :goto_0
.end method
