.class Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkMainActionBar;
.super Landroid/support/v7/widget/Toolbar;
.source "EnhancedBookmarkMainActionBar.java"

# interfaces
.implements Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkUIObserver;


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private mBookmarkModelObserver:Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkModelObserver;

.field private mCurrentFolder:Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;

.field private mDelegate:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;

.field private mNavigationButton:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const-class v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkMainActionBar;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkMainActionBar;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Landroid/support/v7/widget/Toolbar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 32
    new-instance v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkMainActionBar$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkMainActionBar$1;-><init>(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkMainActionBar;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkMainActionBar;->mBookmarkModelObserver:Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkModelObserver;

    .line 42
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkMainActionBar;->setNavigationButton(I)V

    .line 43
    new-instance v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkMainActionBar$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkMainActionBar$2;-><init>(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkMainActionBar;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkMainActionBar;->setNavigationOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 59
    sget v0, Lcom/google/android/apps/chrome/R$menu;->eb_action_bar_menu:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkMainActionBar;->inflateMenu(I)V

    .line 60
    new-instance v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkMainActionBar$3;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkMainActionBar$3;-><init>(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkMainActionBar;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkMainActionBar;->setOnMenuItemClickListener(Landroid/support/v7/widget/F;)V

    .line 76
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkMainActionBar;)Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkMainActionBar;->mDelegate:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkMainActionBar;)I
    .locals 1

    .prologue
    .line 24
    iget v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkMainActionBar;->mNavigationButton:I

    return v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkMainActionBar;)Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkMainActionBar;->mCurrentFolder:Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;

    return-object v0
.end method

.method private setNavigationButton(I)V
    .locals 2

    .prologue
    .line 83
    iput p1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkMainActionBar;->mNavigationButton:I

    .line 84
    packed-switch p1, :pswitch_data_0

    .line 92
    sget-boolean v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkMainActionBar;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    const-string/jumbo v1, "Incorrect navigationButton argument"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 86
    :pswitch_0
    sget v0, Lcom/google/android/apps/chrome/R$drawable;->eb_menu_normal:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkMainActionBar;->setNavigationIcon(I)V

    .line 94
    :cond_0
    :goto_0
    return-void

    .line 89
    :pswitch_1
    sget v0, Lcom/google/android/apps/chrome/R$drawable;->eb_back_normal:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkMainActionBar;->setNavigationIcon(I)V

    goto :goto_0

    .line 84
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public onAllBookmarksModeSet()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 116
    sget v0, Lcom/google/android/apps/chrome/R$string;->enhanced_bookmark_title_bar_all_items:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkMainActionBar;->setTitle(I)V

    .line 117
    invoke-direct {p0, v2}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkMainActionBar;->setNavigationButton(I)V

    .line 118
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkMainActionBar;->getMenu()Landroid/view/Menu;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$id;->search_menu_id:I

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 119
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkMainActionBar;->getMenu()Landroid/view/Menu;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$id;->edit_menu_id:I

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 120
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 106
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkMainActionBar;->mDelegate:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;->getModel()Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkMainActionBar;->mBookmarkModelObserver:Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkModelObserver;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;->removeModelObserver(Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkModelObserver;)V

    .line 107
    return-void
.end method

.method public onEmptyModeSet()V
    .locals 0

    .prologue
    .line 112
    return-void
.end method

.method public onEnhancedBookmarkDelegateInitialized(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;)V
    .locals 2

    .prologue
    .line 100
    iput-object p1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkMainActionBar;->mDelegate:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;

    .line 101
    invoke-interface {p1}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;->getModel()Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkMainActionBar;->mBookmarkModelObserver:Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkModelObserver;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;->addModelObserver(Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkModelObserver;)V

    .line 102
    return-void
.end method

.method public onFolderModeSet(Lorg/chromium/components/bookmarks/BookmarkId;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 132
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkMainActionBar;->mDelegate:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;->getModel()Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;->getBookmarkById(Lorg/chromium/components/bookmarks/BookmarkId;)Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkMainActionBar;->mCurrentFolder:Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;

    .line 134
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkMainActionBar;->getMenu()Landroid/view/Menu;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$id;->search_menu_id:I

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 135
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkMainActionBar;->getMenu()Landroid/view/Menu;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$id;->edit_menu_id:I

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 138
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkMainActionBar;->mDelegate:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;->getModel()Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;->getTopLevelFolderParentIDs()Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkMainActionBar;->mCurrentFolder:Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;

    invoke-virtual {v1}, Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;->getParentId()Lorg/chromium/components/bookmarks/BookmarkId;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 140
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkMainActionBar;->mCurrentFolder:Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;->getTitle()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 141
    sget v0, Lcom/google/android/apps/chrome/R$string;->enhanced_bookmark_title_bar_all_items:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkMainActionBar;->setTitle(I)V

    .line 145
    :goto_0
    invoke-direct {p0, v2}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkMainActionBar;->setNavigationButton(I)V

    .line 150
    :goto_1
    return-void

    .line 143
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkMainActionBar;->mCurrentFolder:Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;->getTitle()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkMainActionBar;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 147
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkMainActionBar;->mCurrentFolder:Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;->getTitle()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkMainActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 148
    invoke-direct {p0, v3}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkMainActionBar;->setNavigationButton(I)V

    goto :goto_1
.end method

.method public onUncategorizedModeSet()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 124
    sget v0, Lcom/google/android/apps/chrome/R$string;->enhanced_bookmark_title_bar_uncategorized:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkMainActionBar;->setTitle(I)V

    .line 125
    invoke-direct {p0, v2}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkMainActionBar;->setNavigationButton(I)V

    .line 126
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkMainActionBar;->getMenu()Landroid/view/Menu;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$id;->search_menu_id:I

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 127
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkMainActionBar;->getMenu()Landroid/view/Menu;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$id;->edit_menu_id:I

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 128
    return-void
.end method
