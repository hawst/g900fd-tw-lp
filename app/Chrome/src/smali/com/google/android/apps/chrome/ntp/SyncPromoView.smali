.class public Lcom/google/android/apps/chrome/ntp/SyncPromoView;
.super Landroid/widget/FrameLayout;
.source "SyncPromoView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lorg/chromium/sync/notifier/SyncStatusHelper$SyncSettingsChangedObserver;


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private mActivity:Landroid/app/Activity;

.field private mEnableSyncButton:Landroid/view/View;

.field private mSignInPromoView:Lcom/google/android/apps/chrome/ntp/SignInPromoView;

.field private mViewModel:Lcom/google/android/apps/chrome/ntp/SyncPromoView$SyncPromoModel;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const-class v0, Lcom/google/android/apps/chrome/ntp/SyncPromoView;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/ntp/SyncPromoView;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 79
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 80
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 89
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 90
    return-void
.end method

.method private configureForSyncState(Z)V
    .locals 2

    .prologue
    .line 156
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/SyncPromoView;->mSignInPromoView:Lcom/google/android/apps/chrome/ntp/SignInPromoView;

    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/SyncPromoView;->mViewModel:Lcom/google/android/apps/chrome/ntp/SyncPromoView$SyncPromoModel;

    invoke-interface {v1}, Lcom/google/android/apps/chrome/ntp/SyncPromoView$SyncPromoModel;->isSignedIn()Z

    move-result v1

    invoke-virtual {v0, v1, p1}, Lcom/google/android/apps/chrome/ntp/SignInPromoView;->updateSyncPromoState(ZZ)V

    .line 158
    sget v0, Lcom/google/android/apps/chrome/R$id;->text_view:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ntp/SyncPromoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 159
    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/SyncPromoView;->mViewModel:Lcom/google/android/apps/chrome/ntp/SyncPromoView$SyncPromoModel;

    invoke-interface {v1}, Lcom/google/android/apps/chrome/ntp/SyncPromoView$SyncPromoModel;->isSyncEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 160
    sget v1, Lcom/google/android/apps/chrome/R$string;->ntp_recent_tabs_sync_promo_instructions:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 161
    const v1, -0x5f5f60

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 162
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/SyncPromoView;->mEnableSyncButton:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 168
    :goto_0
    return-void

    .line 164
    :cond_0
    sget v1, Lcom/google/android/apps/chrome/R$string;->ntp_recent_tabs_sync_enable_sync:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 165
    const v1, -0xcccccd

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 166
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/SyncPromoView;->mEnableSyncButton:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private registerForUpdates()V
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/SyncPromoView;->mViewModel:Lcom/google/android/apps/chrome/ntp/SyncPromoView$SyncPromoModel;

    invoke-interface {v0, p0}, Lcom/google/android/apps/chrome/ntp/SyncPromoView$SyncPromoModel;->registerForSyncUpdates(Lorg/chromium/sync/notifier/SyncStatusHelper$SyncSettingsChangedObserver;)V

    .line 149
    return-void
.end method

.method private unregisterForUpdates()V
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/SyncPromoView;->mViewModel:Lcom/google/android/apps/chrome/ntp/SyncPromoView$SyncPromoModel;

    invoke-interface {v0, p0}, Lcom/google/android/apps/chrome/ntp/SyncPromoView$SyncPromoModel;->unregisterForSyncUpdates(Lorg/chromium/sync/notifier/SyncStatusHelper$SyncSettingsChangedObserver;)V

    .line 153
    return-void
.end method


# virtual methods
.method public initialize(Lcom/google/android/apps/chrome/ntp/SyncPromoView$SyncPromoModel;Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 99
    iput-object p1, p0, Lcom/google/android/apps/chrome/ntp/SyncPromoView;->mViewModel:Lcom/google/android/apps/chrome/ntp/SyncPromoView$SyncPromoModel;

    .line 100
    iput-object p2, p0, Lcom/google/android/apps/chrome/ntp/SyncPromoView;->mActivity:Landroid/app/Activity;

    .line 101
    sget-boolean v0, Lcom/google/android/apps/chrome/ntp/SyncPromoView;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/SyncPromoView;->mSignInPromoView:Lcom/google/android/apps/chrome/ntp/SignInPromoView;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 102
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/SyncPromoView;->mSignInPromoView:Lcom/google/android/apps/chrome/ntp/SignInPromoView;

    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/SyncPromoView;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/ntp/SignInPromoView;->initialize(Landroid/app/Activity;)V

    .line 103
    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .prologue
    .line 107
    invoke-super {p0}, Landroid/widget/FrameLayout;->onAttachedToWindow()V

    .line 108
    invoke-direct {p0}, Lcom/google/android/apps/chrome/ntp/SyncPromoView;->registerForUpdates()V

    .line 109
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/ntp/SyncPromoView;->configureForSyncState(Z)V

    .line 110
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 143
    sget-boolean v0, Lcom/google/android/apps/chrome/ntp/SyncPromoView;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/SyncPromoView;->mEnableSyncButton:Landroid/view/View;

    if-eq p1, v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 144
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/SyncPromoView;->mViewModel:Lcom/google/android/apps/chrome/ntp/SyncPromoView$SyncPromoModel;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/ntp/SyncPromoView$SyncPromoModel;->enableSync()V

    .line 145
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    .prologue
    .line 114
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    .line 115
    invoke-direct {p0}, Lcom/google/android/apps/chrome/ntp/SyncPromoView;->unregisterForUpdates()V

    .line 116
    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 120
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 121
    sget v0, Lcom/google/android/apps/chrome/R$id;->enable_sync_button:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ntp/SyncPromoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/ntp/SyncPromoView;->mEnableSyncButton:Landroid/view/View;

    .line 122
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/SyncPromoView;->mEnableSyncButton:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 123
    sget v0, Lcom/google/android/apps/chrome/R$id;->signin_promo_view:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ntp/SyncPromoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/ntp/SignInPromoView;

    iput-object v0, p0, Lcom/google/android/apps/chrome/ntp/SyncPromoView;->mSignInPromoView:Lcom/google/android/apps/chrome/ntp/SignInPromoView;

    .line 124
    return-void
.end method

.method public onWindowVisibilityChanged(I)V
    .locals 1

    .prologue
    .line 128
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onWindowVisibilityChanged(I)V

    .line 129
    if-nez p1, :cond_0

    .line 130
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/ntp/SyncPromoView;->configureForSyncState(Z)V

    .line 132
    :cond_0
    return-void
.end method

.method public syncSettingsChanged()V
    .locals 1

    .prologue
    .line 137
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/ntp/SyncPromoView;->configureForSyncState(Z)V

    .line 138
    return-void
.end method
