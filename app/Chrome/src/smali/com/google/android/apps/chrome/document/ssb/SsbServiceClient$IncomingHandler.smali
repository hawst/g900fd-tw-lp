.class Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient$IncomingHandler;
.super Landroid/os/Handler;
.source "SsbServiceClient.java"


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient;)V
    .locals 0

    .prologue
    .line 45
    iput-object p1, p0, Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient$IncomingHandler;->this$0:Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient;Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient$1;)V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient$IncomingHandler;-><init>(Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3

    .prologue
    .line 48
    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    .line 49
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient$IncomingHandler;->this$0:Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient;

    # getter for: Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient;->mCallback:Lcom/google/android/apps/chrome/document/ssb/SsbServiceCallback;
    invoke-static {v0}, Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient;->access$100(Lcom/google/android/apps/chrome/document/ssb/SsbServiceClient;)Lcom/google/android/apps/chrome/document/ssb/SsbServiceCallback;

    move-result-object v1

    .line 50
    if-nez v1, :cond_0

    .line 60
    :goto_0
    return-void

    .line 55
    :cond_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/os/Bundle;

    .line 56
    const-string/jumbo v2, "ssb_service:ssb_state"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    invoke-interface {v1, v0}, Lcom/google/android/apps/chrome/document/ssb/SsbServiceCallback;->updateSsb([B)V

    goto :goto_0

    .line 58
    :cond_1
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    goto :goto_0
.end method
