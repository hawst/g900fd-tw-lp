.class Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab;
.super Lcom/google/android/apps/chrome/tab/ChromeTab;
.source "FullScreenActivityTab.java"


# static fields
.field static final BUNDLE_TAB_ID:Ljava/lang/String; = "tabId"

.field static final BUNDLE_TAB_URL:Ljava/lang/String; = "tabUrl"


# instance fields
.field private mObserver:Lorg/chromium/content/browser/WebContentsObserverAndroid;


# direct methods
.method private constructor <init>(ILcom/google/android/apps/chrome/ChromeActivity;Lorg/chromium/ui/base/WindowAndroid;Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;)V
    .locals 8

    .prologue
    .line 56
    const/4 v3, 0x0

    const/4 v5, -0x1

    const/4 v7, 0x0

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move-object v4, p3

    move-object v6, p4

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/chrome/tab/ChromeTab;-><init>(ILcom/google/android/apps/chrome/ChromeActivity;ZLorg/chromium/ui/base/WindowAndroid;ILcom/google/android/apps/chrome/tab/ChromeTab$TabState;Lcom/google/android/apps/chrome/tab/TabUma$TabCreationState;)V

    .line 57
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab;->initializeFullScreenActivityTab(Z)V

    .line 58
    return-void
.end method

.method private constructor <init>(Lcom/google/android/apps/chrome/ChromeActivity;Lorg/chromium/ui/base/WindowAndroid;)V
    .locals 8

    .prologue
    const/4 v3, 0x0

    const/4 v1, -0x1

    .line 49
    sget-object v5, Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;->FROM_MENU_OR_OVERVIEW:Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;

    const/4 v7, 0x0

    move-object v0, p0

    move-object v2, p1

    move-object v4, p2

    move v6, v1

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/chrome/tab/ChromeTab;-><init>(ILcom/google/android/apps/chrome/ChromeActivity;ZLorg/chromium/ui/base/WindowAndroid;Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;ILcom/google/android/apps/chrome/tab/TabUma$TabCreationState;)V

    .line 51
    invoke-direct {p0, v3}, Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab;->initializeFullScreenActivityTab(Z)V

    .line 52
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab;IIZ)V
    .locals 0

    .prologue
    .line 40
    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab;->updateTopControlsState(IIZ)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 40
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 40
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 40
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method static create(Lcom/google/android/apps/chrome/ChromeActivity;Lorg/chromium/ui/base/WindowAndroid;Ljava/io/File;Landroid/os/Bundle;)Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab;
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v4, -0x1

    .line 149
    .line 153
    if-eqz p3, :cond_2

    .line 154
    const-string/jumbo v0, "tabId"

    invoke-virtual {p3, v0, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 155
    const-string/jumbo v0, "tabUrl"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move v3, v1

    .line 158
    :goto_0
    if-eq v3, v4, :cond_1

    if-eqz v0, :cond_1

    if-eqz p2, :cond_1

    .line 162
    :try_start_0
    new-instance v1, Ljava/io/FileInputStream;

    invoke-static {p2, v3}, Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab;->getTabFile(Ljava/io/File;I)Ljava/io/File;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 163
    const/4 v0, 0x0

    :try_start_1
    invoke-static {v1, v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->readState(Ljava/io/FileInputStream;Z)Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;

    move-result-object v4

    .line 164
    new-instance v0, Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab;

    invoke-direct {v0, v3, p0, p1, v4}, Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab;-><init>(ILcom/google/android/apps/chrome/ChromeActivity;Lorg/chromium/ui/base/WindowAndroid;Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 170
    invoke-static {v1}, Lorg/chromium/chrome/browser/util/StreamUtil;->closeQuietly(Ljava/io/Closeable;)V

    .line 174
    :goto_1
    if-nez v0, :cond_0

    .line 176
    new-instance v0, Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab;-><init>(Lcom/google/android/apps/chrome/ChromeActivity;Lorg/chromium/ui/base/WindowAndroid;)V

    .line 179
    :cond_0
    return-object v0

    .line 165
    :catch_0
    move-exception v0

    move-object v1, v2

    .line 166
    :goto_2
    :try_start_2
    const-string/jumbo v3, "FullScreenActivityTab"

    const-string/jumbo v4, "Failed to restore tab state."

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 170
    invoke-static {v1}, Lorg/chromium/chrome/browser/util/StreamUtil;->closeQuietly(Ljava/io/Closeable;)V

    move-object v0, v2

    .line 171
    goto :goto_1

    .line 167
    :catch_1
    move-exception v0

    move-object v1, v2

    .line 168
    :goto_3
    :try_start_3
    const-string/jumbo v3, "FullScreenActivityTab"

    const-string/jumbo v4, "Failed to restore tab state."

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 170
    invoke-static {v1}, Lorg/chromium/chrome/browser/util/StreamUtil;->closeQuietly(Ljava/io/Closeable;)V

    move-object v0, v2

    .line 171
    goto :goto_1

    .line 170
    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_4
    invoke-static {v1}, Lorg/chromium/chrome/browser/util/StreamUtil;->closeQuietly(Ljava/io/Closeable;)V

    throw v0

    :catchall_1
    move-exception v0

    goto :goto_4

    .line 167
    :catch_2
    move-exception v0

    goto :goto_3

    .line 165
    :catch_3
    move-exception v0

    goto :goto_2

    :cond_1
    move-object v0, v2

    goto :goto_1

    :cond_2
    move-object v0, v2

    move v3, v4

    goto :goto_0
.end method

.method private createWebContentsObserver()Lorg/chromium/content/browser/WebContentsObserverAndroid;
    .locals 2

    .prologue
    .line 78
    new-instance v0, Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab$1;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab;->getWebContents()Lorg/chromium/content_public/browser/WebContents;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab$1;-><init>(Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab;Lorg/chromium/content_public/browser/WebContents;)V

    return-object v0
.end method

.method private static getTabFile(Ljava/io/File;I)Ljava/io/File;
    .locals 2

    .prologue
    .line 134
    new-instance v0, Ljava/io/File;

    const/4 v1, 0x0

    invoke-static {p1, v1}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->getTabStateFilename(IZ)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method private initializeFullScreenActivityTab(Z)V
    .locals 4

    .prologue
    .line 61
    const-wide/16 v0, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab;->initialize(JLcom/google/android/apps/chrome/compositor/TabContentManager;Z)V

    .line 62
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab;->unfreezeContents()Z

    .line 63
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab;->createWebContentsObserver()Lorg/chromium/content/browser/WebContentsObserverAndroid;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab;->mObserver:Lorg/chromium/content/browser/WebContentsObserverAndroid;

    .line 64
    return-void
.end method


# virtual methods
.method protected createContextMenuPopulator()Lorg/chromium/chrome/browser/contextmenu/ContextMenuPopulator;
    .locals 1

    .prologue
    .line 184
    new-instance v0, Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab$2;-><init>(Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab;)V

    return-object v0
.end method

.method protected getTopControlsStateConstraints()I
    .locals 1

    .prologue
    .line 100
    const/4 v0, 0x2

    return v0
.end method

.method protected initContentViewCore(J)V
    .locals 3

    .prologue
    .line 94
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/chrome/tab/ChromeTab;->initContentViewCore(J)V

    .line 95
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab;->getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lorg/chromium/content/browser/ContentViewCore;->setFullscreenRequiredForOrientationLock(Z)V

    .line 96
    return-void
.end method

.method public loadUrl(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 108
    new-instance v0, Lorg/chromium/content_public/browser/LoadUrlParams;

    const/4 v1, 0x6

    invoke-direct {v0, p1, v1}, Lorg/chromium/content_public/browser/LoadUrlParams;-><init>(Ljava/lang/String;I)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab;->loadUrl(Lorg/chromium/content_public/browser/LoadUrlParams;)I

    .line 109
    return-void
.end method

.method saveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 70
    const-string/jumbo v0, "tabId"

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab;->getId()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 71
    const-string/jumbo v0, "tabUrl"

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    return-void
.end method

.method saveState(Ljava/io/File;)V
    .locals 4

    .prologue
    .line 115
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab;->getId()I

    move-result v0

    invoke-static {p1, v0}, Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab;->getTabFile(Ljava/io/File;I)Ljava/io/File;

    move-result-object v0

    .line 117
    const/4 v2, 0x0

    .line 119
    :try_start_0
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 120
    :try_start_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/webapps/FullScreenActivityTab;->getState()Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;

    move-result-object v0

    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Lcom/google/android/apps/chrome/tab/ChromeTab;->saveState(Ljava/io/FileOutputStream;Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;Z)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 126
    invoke-static {v1}, Lorg/chromium/chrome/browser/util/StreamUtil;->closeQuietly(Ljava/io/Closeable;)V

    .line 127
    :goto_0
    return-void

    .line 121
    :catch_0
    move-exception v0

    move-object v1, v2

    .line 122
    :goto_1
    :try_start_2
    const-string/jumbo v2, "FullScreenActivityTab"

    const-string/jumbo v3, "Failed to save out tab state."

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 126
    invoke-static {v1}, Lorg/chromium/chrome/browser/util/StreamUtil;->closeQuietly(Ljava/io/Closeable;)V

    goto :goto_0

    .line 123
    :catch_1
    move-exception v0

    move-object v1, v2

    .line 124
    :goto_2
    :try_start_3
    const-string/jumbo v2, "FullScreenActivityTab"

    const-string/jumbo v3, "Failed to save out tab state."

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 126
    invoke-static {v1}, Lorg/chromium/chrome/browser/util/StreamUtil;->closeQuietly(Ljava/io/Closeable;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_3
    invoke-static {v1}, Lorg/chromium/chrome/browser/util/StreamUtil;->closeQuietly(Ljava/io/Closeable;)V

    throw v0

    :catchall_1
    move-exception v0

    goto :goto_3

    .line 123
    :catch_2
    move-exception v0

    goto :goto_2

    .line 121
    :catch_3
    move-exception v0

    goto :goto_1
.end method
