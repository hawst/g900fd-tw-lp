.class public Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;
.super Lorg/chromium/chrome/browser/EmptyTabObserver;
.source "BackgroundContentViewHelper.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final mBackgroundGestureStateListener:Lorg/chromium/content_public/browser/GestureStateListener;

.field private mBackgroundLoadStartTimeStampMs:J

.field private mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

.field private final mCurrentViewGestureStateListener:Lorg/chromium/content_public/browser/GestureStateListener;

.field private mCurrentViewWebContentsObserver:Lorg/chromium/content/browser/WebContentsObserverAndroid;

.field private final mDelegate:Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper$BackgroundContentViewDelegate;

.field private mDidFinishLoad:Z

.field private mDidStartLoad:Z

.field private final mExperimentGroup:I

.field private final mHandler:Landroid/os/Handler;

.field private mListener:Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper$BackgroundContentViewListener;

.field private mLoadProgress:I

.field private final mMinRendererFramesNeededForSwap:I

.field private mNativeBackgroundContentViewHelperPtr:J

.field private mNumRendererFramesReceived:I

.field private mOriginalPageZoomed:Z

.field private mPaintedNonEmpty:Z

.field private mPreviewLoaded:Z

.field private mRecordUma:Z

.field private mSwapInProgress:Z

.field private final mSwapOnTimeout:Ljava/lang/Runnable;

.field private final mSwapTimeoutMs:I

.field private final mTab:Lorg/chromium/chrome/browser/Tab;

.field private final mWebContentsDelegate:Lorg/chromium/components/web_contents_delegate_android/WebContentsDelegateAndroid;

.field private mWebContentsObserver:Lorg/chromium/content/browser/WebContentsObserverAndroid;

.field private final mWindowAndroid:Lorg/chromium/ui/base/WindowAndroid;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    const-class v0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/chromium/ui/base/WindowAndroid;Lorg/chromium/chrome/browser/Tab;Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper$BackgroundContentViewDelegate;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 218
    invoke-direct {p0}, Lorg/chromium/chrome/browser/EmptyTabObserver;-><init>()V

    .line 103
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mPreviewLoaded:Z

    .line 106
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mPaintedNonEmpty:Z

    .line 108
    iput v1, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mNumRendererFramesReceived:I

    .line 112
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mSwapInProgress:Z

    .line 115
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mDidStartLoad:Z

    .line 118
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mDidFinishLoad:Z

    .line 137
    new-instance v0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper$1;-><init>(Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mSwapOnTimeout:Ljava/lang/Runnable;

    .line 153
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mRecordUma:Z

    .line 166
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mOriginalPageZoomed:Z

    .line 219
    iput-object p1, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mWindowAndroid:Lorg/chromium/ui/base/WindowAndroid;

    .line 220
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->nativeInit()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mNativeBackgroundContentViewHelperPtr:J

    .line 222
    iput-object p2, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mTab:Lorg/chromium/chrome/browser/Tab;

    .line 223
    sget-boolean v0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p3, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 224
    :cond_0
    iput-object p3, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mDelegate:Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper$BackgroundContentViewDelegate;

    .line 225
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mTab:Lorg/chromium/chrome/browser/Tab;

    invoke-virtual {v0, p0}, Lorg/chromium/chrome/browser/Tab;->addObserver(Lorg/chromium/chrome/browser/TabObserver;)V

    .line 226
    new-instance v0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper$2;-><init>(Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mBackgroundGestureStateListener:Lorg/chromium/content_public/browser/GestureStateListener;

    .line 233
    new-instance v0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper$3;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper$3;-><init>(Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mCurrentViewGestureStateListener:Lorg/chromium/content_public/browser/GestureStateListener;

    .line 250
    new-instance v0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper$4;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper$4;-><init>(Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mWebContentsDelegate:Lorg/chromium/components/web_contents_delegate_android/WebContentsDelegateAndroid;

    .line 258
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mHandler:Landroid/os/Handler;

    .line 260
    iget-wide v0, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mNativeBackgroundContentViewHelperPtr:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->nativeGetSwapTimeoutMs(J)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mSwapTimeoutMs:I

    .line 261
    iget-wide v0, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mNativeBackgroundContentViewHelperPtr:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->nativeGetNumFramesNeededForSwap(J)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mMinRendererFramesNeededForSwap:I

    .line 263
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mBackgroundLoadStartTimeStampMs:J

    .line 264
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->getExperimentGroup()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mExperimentGroup:I

    .line 265
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->syncPageStateAndSwapIfReady()V

    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;)Lorg/chromium/content/browser/ContentViewCore;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    return-object v0
.end method

.method static synthetic access$102(Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;I)I
    .locals 0

    .prologue
    .line 39
    iput p1, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mLoadProgress:I

    return p1
.end method

.method static synthetic access$1100(Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->getOriginalUrlForPreviewUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1200(Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->destroyContentViewCore()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;)Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper$BackgroundContentViewDelegate;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mDelegate:Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper$BackgroundContentViewDelegate;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->trySwappingBackgroundView()V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;)Z
    .locals 1

    .prologue
    .line 39
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mPaintedNonEmpty:Z

    return v0
.end method

.method static synthetic access$402(Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;Z)Z
    .locals 0

    .prologue
    .line 39
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mPaintedNonEmpty:Z

    return p1
.end method

.method static synthetic access$502(Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;Z)Z
    .locals 0

    .prologue
    .line 39
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mDidStartLoad:Z

    return p1
.end method

.method static synthetic access$602(Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;Z)Z
    .locals 0

    .prologue
    .line 39
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mDidFinishLoad:Z

    return p1
.end method

.method static synthetic access$700(Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;)Lorg/chromium/chrome/browser/Tab;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mTab:Lorg/chromium/chrome/browser/Tab;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;)Z
    .locals 1

    .prologue
    .line 39
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mSwapInProgress:Z

    return v0
.end method

.method static synthetic access$908(Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;)I
    .locals 2

    .prologue
    .line 39
    iget v0, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mNumRendererFramesReceived:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mNumRendererFramesReceived:I

    return v0
.end method

.method private backgroundViewReady()V
    .locals 1

    .prologue
    .line 542
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mDelegate:Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper$BackgroundContentViewDelegate;

    if-nez v0, :cond_0

    .line 545
    :goto_0
    return-void

    .line 543
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mSwapInProgress:Z

    .line 544
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->swapInBackgroundViewAfterScroll()V

    goto :goto_0
.end method

.method private createContentViewCoreWithUrl(Ljava/lang/String;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 403
    if-eqz p1, :cond_0

    iget-wide v0, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mNativeBackgroundContentViewHelperPtr:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 485
    :cond_0
    :goto_0
    return-void

    .line 405
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->destroyContentViewCore()V

    .line 407
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mWindowAndroid:Lorg/chromium/ui/base/WindowAndroid;

    invoke-virtual {v0}, Lorg/chromium/ui/base/WindowAndroid;->getActivity()Ljava/lang/ref/WeakReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 408
    new-instance v1, Lorg/chromium/content/browser/ContentViewCore;

    invoke-direct {v1, v0}, Lorg/chromium/content/browser/ContentViewCore;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    .line 409
    iget-object v1, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    invoke-static {v0, v1}, Lorg/chromium/content/browser/ContentView;->newInstance(Landroid/content/Context;Lorg/chromium/content/browser/ContentViewCore;)Lorg/chromium/content/browser/ContentView;

    move-result-object v2

    .line 410
    iget-object v1, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mTab:Lorg/chromium/chrome/browser/Tab;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/Tab;->isIncognito()Z

    move-result v0

    invoke-static {v0}, Lorg/chromium/chrome/browser/ContentViewUtil;->createNativeWebContents(Z)J

    move-result-wide v4

    iget-object v6, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mWindowAndroid:Lorg/chromium/ui/base/WindowAndroid;

    move-object v3, v2

    invoke-virtual/range {v1 .. v6}, Lorg/chromium/content/browser/ContentViewCore;->initialize(Landroid/view/ViewGroup;Lorg/chromium/content/browser/ContentViewCore$InternalAccessDelegate;JLorg/chromium/ui/base/WindowAndroid;)V

    .line 413
    new-instance v0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper$5;

    iget-object v1, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    invoke-virtual {v1}, Lorg/chromium/content/browser/ContentViewCore;->getWebContents()Lorg/chromium/content_public/browser/WebContents;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper$5;-><init>(Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;Lorg/chromium/content_public/browser/WebContents;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mWebContentsObserver:Lorg/chromium/content/browser/WebContentsObserverAndroid;

    .line 446
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    new-instance v1, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper$6;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper$6;-><init>(Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;)V

    invoke-virtual {v0, v1}, Lorg/chromium/content/browser/ContentViewCore;->setContentViewClient(Lorg/chromium/content/browser/ContentViewClient;)V

    .line 472
    iget v0, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mSwapTimeoutMs:I

    if-lez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mSwapOnTimeout:Ljava/lang/Runnable;

    iget v2, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mSwapTimeoutMs:I

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 474
    :cond_2
    iget-wide v0, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mNativeBackgroundContentViewHelperPtr:J

    iget-object v2, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    iget-object v3, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mWebContentsDelegate:Lorg/chromium/components/web_contents_delegate_android/WebContentsDelegateAndroid;

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->nativeSetWebContents(JLorg/chromium/content/browser/ContentViewCore;Lorg/chromium/components/web_contents_delegate_android/WebContentsDelegateAndroid;)V

    .line 477
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mBackgroundLoadStartTimeStampMs:J

    .line 479
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->loadUrl(Ljava/lang/String;)V

    .line 481
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    const/4 v1, 0x1

    invoke-virtual {v0, v7, v1, v7}, Lorg/chromium/content/browser/ContentViewCore;->updateTopControlsState(ZZZ)V

    .line 482
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mListener:Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper$BackgroundContentViewListener;

    if-eqz v0, :cond_0

    .line 483
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mListener:Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper$BackgroundContentViewListener;

    invoke-interface {v0, p0}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper$BackgroundContentViewListener;->onBackgroundViewCreated(Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;)V

    goto :goto_0
.end method

.method private destroy()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 386
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mCurrentViewWebContentsObserver:Lorg/chromium/content/browser/WebContentsObserverAndroid;

    if-eqz v0, :cond_0

    .line 387
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mCurrentViewWebContentsObserver:Lorg/chromium/content/browser/WebContentsObserverAndroid;

    invoke-virtual {v0}, Lorg/chromium/content/browser/WebContentsObserverAndroid;->detachFromWebContents()V

    .line 388
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mCurrentViewWebContentsObserver:Lorg/chromium/content/browser/WebContentsObserverAndroid;

    .line 390
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->destroyContentViewCore()V

    .line 392
    iget-wide v0, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mNativeBackgroundContentViewHelperPtr:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 396
    :goto_0
    return-void

    .line 394
    :cond_1
    iget-wide v0, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mNativeBackgroundContentViewHelperPtr:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->nativeDestroy(J)V

    .line 395
    iput-wide v2, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mNativeBackgroundContentViewHelperPtr:J

    goto :goto_0
.end method

.method private destroyContentViewCore()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 491
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    if-eqz v0, :cond_1

    iget-wide v0, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mNativeBackgroundContentViewHelperPtr:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 492
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mBackgroundLoadStartTimeStampMs:J

    sub-long/2addr v0, v2

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->recordInstantSearchClicksTimeInPreview(J)V

    .line 494
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mListener:Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper$BackgroundContentViewListener;

    if-eqz v0, :cond_0

    .line 495
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mListener:Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper$BackgroundContentViewListener;

    invoke-interface {v0, p0}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper$BackgroundContentViewListener;->onBackgroundViewRemoved(Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;)V

    .line 497
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->destroy()V

    .line 498
    iget-wide v0, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mNativeBackgroundContentViewHelperPtr:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->nativeDestroyWebContents(J)V

    .line 500
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->reset()V

    .line 503
    iput-boolean v4, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mSwapInProgress:Z

    .line 504
    iput v4, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mLoadProgress:I

    .line 505
    iput-boolean v4, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mDidStartLoad:Z

    .line 506
    iput-boolean v4, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mDidFinishLoad:Z

    .line 508
    return-void
.end method

.method private getExperimentGroup()I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 271
    invoke-static {}, Lcom/google/android/apps/chrome/device/DeviceClassManager;->enableInstantSearchClicks()Z

    move-result v1

    if-nez v1, :cond_1

    .line 284
    :cond_0
    :goto_0
    return v0

    .line 275
    :cond_1
    const-string/jumbo v1, "InstantSearchClicks"

    invoke-static {v1}, Lorg/chromium/base/FieldTrialList;->findFullName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 277
    const-string/jumbo v2, "Enabled"

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string/jumbo v2, "InstantSearchClicksEnabled"

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 280
    :cond_2
    const/4 v0, 0x1

    goto :goto_0

    .line 281
    :cond_3
    const-string/jumbo v2, "Control"

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 282
    const/4 v0, 0x2

    goto :goto_0
.end method

.method private getNativePtr()J
    .locals 2

    .prologue
    .line 857
    iget-wide v0, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mNativeBackgroundContentViewHelperPtr:J

    return-wide v0
.end method

.method private getOriginalUrlForPreviewUrl(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 563
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->getUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 565
    invoke-virtual {v1}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->isGooglePreviewServerHost(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 567
    :try_start_0
    new-instance v2, Ljava/net/URL;

    const-string/jumbo v3, "url"

    invoke-virtual {v1, v3}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/net/URL;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 572
    :cond_0
    :goto_0
    return-object v0

    .line 569
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method private getUri(Ljava/lang/String;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 556
    if-nez p1, :cond_0

    const-string/jumbo p1, ""

    :cond_0
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method private hasBackgroundPageLoadedMoreThanPreview()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 291
    iget-boolean v1, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mPaintedNonEmpty:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mTab:Lorg/chromium/chrome/browser/Tab;

    invoke-virtual {v1}, Lorg/chromium/chrome/browser/Tab;->getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v1

    if-nez v1, :cond_1

    .line 292
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v1

    invoke-virtual {v1}, Lorg/chromium/content/browser/ContentViewCore;->getContentHeightCss()F

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mTab:Lorg/chromium/chrome/browser/Tab;

    invoke-virtual {v2}, Lorg/chromium/chrome/browser/Tab;->getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v2

    invoke-virtual {v2}, Lorg/chromium/content/browser/ContentViewCore;->getContentHeightCss()F

    move-result v2

    cmpl-float v1, v1, v2

    if-lez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private isBackgroundViewReady()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 518
    iget-boolean v2, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mDidFinishLoad:Z

    if-eqz v2, :cond_1

    .line 528
    :cond_0
    :goto_0
    return v0

    .line 520
    :cond_1
    iget-boolean v2, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mPreviewLoaded:Z

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 522
    :cond_2
    iget-boolean v2, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mPaintedNonEmpty:Z

    if-nez v2, :cond_3

    move v0, v1

    goto :goto_0

    .line 524
    :cond_3
    iget v2, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mNumRendererFramesReceived:I

    iget v3, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mMinRendererFramesNeededForSwap:I

    if-ge v2, v3, :cond_4

    move v0, v1

    goto :goto_0

    .line 526
    :cond_4
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->hasBackgroundPageLoadedMoreThanPreview()Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method private isGooglePreviewServerHost(Ljava/lang/String;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 548
    if-nez p1, :cond_1

    .line 549
    :cond_0
    :goto_0
    return v0

    :cond_1
    const-string/jumbo v1, "icl.googleusercontent.com"

    invoke-static {p1, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-static {}, Lcom/google/android/apps/chrome/utilities/ChromeBuildInfo;->isDevBuild()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string/jumbo v1, ".googleusercontent.com"

    invoke-virtual {p1, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string/jumbo v1, "promise"

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private loadUrl(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 336
    iget-wide v0, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mNativeBackgroundContentViewHelperPtr:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 364
    :cond_0
    :goto_0
    return-void

    .line 338
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    if-eqz v0, :cond_0

    .line 340
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->onShow()V

    .line 342
    new-instance v1, Lorg/chromium/content_public/browser/LoadUrlParams;

    invoke-direct {v1, p1}, Lorg/chromium/content_public/browser/LoadUrlParams;-><init>(Ljava/lang/String;)V

    .line 347
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mTab:Lorg/chromium/chrome/browser/Tab;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/Tab;->getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->getWebContents()Lorg/chromium/content_public/browser/WebContents;

    move-result-object v0

    invoke-interface {v0}, Lorg/chromium/content_public/browser/WebContents;->getNavigationController()Lorg/chromium/content_public/browser/NavigationController;

    move-result-object v0

    invoke-interface {v0}, Lorg/chromium/content_public/browser/NavigationController;->getOriginalUrlForVisibleNavigationEntry()Ljava/lang/String;

    move-result-object v0

    .line 349
    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->getUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 350
    invoke-virtual {v2}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v0

    .line 351
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    invoke-virtual {v2}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "www.google."

    invoke-static {v2, v3}, Landroid/text/TextUtils;->indexOf(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)I

    move-result v2

    if-eqz v2, :cond_3

    .line 353
    :cond_2
    const-string/jumbo v0, "www.google.com"

    .line 360
    :cond_3
    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "http://"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, "/?gcjeid"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, "=19"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 362
    new-instance v2, Lorg/chromium/content_public/common/Referrer;

    const/4 v3, 0x0

    invoke-direct {v2, v0, v3}, Lorg/chromium/content_public/common/Referrer;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v1, v2}, Lorg/chromium/content_public/browser/LoadUrlParams;->setReferrer(Lorg/chromium/content_public/common/Referrer;)V

    .line 363
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->getWebContents()Lorg/chromium/content_public/browser/WebContents;

    move-result-object v0

    invoke-interface {v0}, Lorg/chromium/content_public/browser/WebContents;->getNavigationController()Lorg/chromium/content_public/browser/NavigationController;

    move-result-object v0

    invoke-interface {v0, v1}, Lorg/chromium/content_public/browser/NavigationController;->loadUrl(Lorg/chromium/content_public/browser/LoadUrlParams;)V

    goto :goto_0
.end method

.method private native nativeDestroy(J)V
.end method

.method private native nativeDestroyWebContents(J)V
.end method

.method private native nativeGetNumFramesNeededForSwap(J)I
.end method

.method private native nativeGetSwapTimeoutMs(J)I
.end method

.method private native nativeInit()J
.end method

.method private native nativeMergeHistoryFrom(JLorg/chromium/content_public/browser/WebContents;)V
.end method

.method private native nativeReleaseWebContents(J)V
.end method

.method private native nativeSetWebContents(JLorg/chromium/content/browser/ContentViewCore;Lorg/chromium/components/web_contents_delegate_android/WebContentsDelegateAndroid;)V
.end method

.method private native nativeUnloadAndDeleteWebContents(JLorg/chromium/content_public/browser/WebContents;)V
.end method

.method private releaseContentViewCore()Lorg/chromium/content/browser/ContentViewCore;
    .locals 6

    .prologue
    .line 371
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mListener:Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper$BackgroundContentViewListener;

    if-eqz v0, :cond_0

    .line 372
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mListener:Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper$BackgroundContentViewListener;

    invoke-interface {v0, p0}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper$BackgroundContentViewListener;->onBackgroundViewRemoved(Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;)V

    .line 374
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    .line 375
    iget-object v1, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    if-eqz v1, :cond_1

    iget-wide v2, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mNativeBackgroundContentViewHelperPtr:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_1

    .line 376
    iget-wide v2, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mNativeBackgroundContentViewHelperPtr:J

    invoke-direct {p0, v2, v3}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->nativeReleaseWebContents(J)V

    .line 378
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->reset()V

    .line 379
    return-object v0
.end method

.method private reset()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 778
    iput-object v1, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    .line 779
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mWebContentsObserver:Lorg/chromium/content/browser/WebContentsObserverAndroid;

    if-eqz v0, :cond_0

    .line 780
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mWebContentsObserver:Lorg/chromium/content/browser/WebContentsObserverAndroid;

    invoke-virtual {v0}, Lorg/chromium/content/browser/WebContentsObserverAndroid;->detachFromWebContents()V

    .line 781
    iput-object v1, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mWebContentsObserver:Lorg/chromium/content/browser/WebContentsObserverAndroid;

    .line 784
    :cond_0
    iput-boolean v2, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mPreviewLoaded:Z

    .line 785
    iput-boolean v2, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mPaintedNonEmpty:Z

    .line 786
    iput v2, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mNumRendererFramesReceived:I

    .line 787
    iput-boolean v2, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mRecordUma:Z

    .line 788
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mSwapOnTimeout:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 789
    iput-boolean v2, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mOriginalPageZoomed:Z

    .line 790
    return-void
.end method

.method private setLogUma()V
    .locals 2

    .prologue
    .line 593
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mTab:Lorg/chromium/chrome/browser/Tab;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/Tab;->getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mCurrentViewGestureStateListener:Lorg/chromium/content_public/browser/GestureStateListener;

    invoke-virtual {v0, v1}, Lorg/chromium/content/browser/ContentViewCore;->addGestureStateListener(Lorg/chromium/content_public/browser/GestureStateListener;)V

    .line 594
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mRecordUma:Z

    .line 595
    return-void
.end method

.method private setLogUmaForControlGroupIfNeeded(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 576
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mTab:Lorg/chromium/chrome/browser/Tab;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/Tab;->getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->getWebContents()Lorg/chromium/content_public/browser/WebContents;

    move-result-object v0

    invoke-interface {v0}, Lorg/chromium/content_public/browser/WebContents;->getNavigationController()Lorg/chromium/content_public/browser/NavigationController;

    move-result-object v0

    invoke-interface {v0}, Lorg/chromium/content_public/browser/NavigationController;->getOriginalUrlForVisibleNavigationEntry()Ljava/lang/String;

    move-result-object v0

    .line 578
    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->getUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 579
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->getUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 580
    invoke-virtual {v0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_1

    .line 588
    :cond_0
    :goto_0
    return-void

    .line 583
    :cond_1
    invoke-virtual {v0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "www.google."

    invoke-static {v2, v3}, Landroid/text/TextUtils;->indexOf(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)I

    move-result v2

    if-nez v2, :cond_0

    const-string/jumbo v2, "gcjeid"

    invoke-virtual {v0, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v1}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "www.google."

    invoke-static {v0, v1}, Landroid/text/TextUtils;->indexOf(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 586
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->setLogUma()V

    goto :goto_0
.end method

.method private swapInBackgroundView()V
    .locals 5

    .prologue
    .line 732
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->hasPendingBackgroundPage()Z

    move-result v0

    if-nez v0, :cond_0

    .line 750
    :goto_0
    return-void

    .line 734
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mTab:Lorg/chromium/chrome/browser/Tab;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/Tab;->getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 735
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mTab:Lorg/chromium/chrome/browser/Tab;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/Tab;->getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mCurrentViewGestureStateListener:Lorg/chromium/content_public/browser/GestureStateListener;

    invoke-virtual {v0, v1}, Lorg/chromium/content/browser/ContentViewCore;->removeGestureStateListener(Lorg/chromium/content_public/browser/GestureStateListener;)V

    .line 737
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mBackgroundGestureStateListener:Lorg/chromium/content_public/browser/GestureStateListener;

    invoke-virtual {v0, v1}, Lorg/chromium/content/browser/ContentViewCore;->removeGestureStateListener(Lorg/chromium/content_public/browser/GestureStateListener;)V

    .line 738
    sget-boolean v0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mDelegate:Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper$BackgroundContentViewDelegate;

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 741
    :cond_2
    iget-wide v0, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mNativeBackgroundContentViewHelperPtr:J

    iget-object v2, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mTab:Lorg/chromium/chrome/browser/Tab;

    invoke-virtual {v2}, Lorg/chromium/chrome/browser/Tab;->getWebContents()Lorg/chromium/content_public/browser/WebContents;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->nativeMergeHistoryFrom(JLorg/chromium/content_public/browser/WebContents;)V

    .line 742
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->releaseContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    .line 743
    iget-object v1, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mDelegate:Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper$BackgroundContentViewDelegate;

    iget-boolean v2, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mDidStartLoad:Z

    iget-boolean v3, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mDidFinishLoad:Z

    iget v4, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mLoadProgress:I

    invoke-interface {v1, v0, v2, v3, v4}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper$BackgroundContentViewDelegate;->onBackgroundViewReady(Lorg/chromium/content/browser/ContentViewCore;ZZI)V

    .line 745
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mBackgroundLoadStartTimeStampMs:J

    sub-long/2addr v0, v2

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->recordInstantSearchClicksTimeToSwap(J)V

    .line 749
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->destroyContentViewCore()V

    goto :goto_0
.end method

.method private swapInBackgroundViewAfterScroll()V
    .locals 2

    .prologue
    .line 689
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mBackgroundGestureStateListener:Lorg/chromium/content_public/browser/GestureStateListener;

    invoke-virtual {v0, v1}, Lorg/chromium/content/browser/ContentViewCore;->addGestureStateListener(Lorg/chromium/content_public/browser/GestureStateListener;)V

    .line 690
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->syncPageStateAndSwapIfReady()V

    .line 691
    return-void
.end method

.method private syncPageStateAndSwapIfReady()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x3

    .line 697
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->hasPendingBackgroundPage()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->isPageSwappingInProgress()Z

    move-result v0

    if-nez v0, :cond_1

    .line 727
    :cond_0
    :goto_0
    return-void

    .line 699
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mTab:Lorg/chromium/chrome/browser/Tab;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/Tab;->getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    .line 700
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->isScrollInProgress()Z

    move-result v1

    if-nez v1, :cond_0

    .line 705
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->transferPageScaleFactor()Z

    move-result v1

    if-nez v1, :cond_0

    .line 707
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->transferScrollOffset()Z

    move-result v1

    if-nez v1, :cond_0

    .line 709
    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->getRenderCoordinates()Lorg/chromium/content/browser/RenderCoordinates;

    move-result-object v0

    .line 710
    invoke-virtual {v0}, Lorg/chromium/content/browser/RenderCoordinates;->getScrollYPixInt()I

    move-result v1

    .line 712
    if-nez v1, :cond_2

    .line 713
    invoke-static {v3, v2}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->recordInstantSearchClicksPreviewScrollState(II)V

    .line 722
    :goto_1
    const/4 v0, 0x6

    invoke-static {v3, v0}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->recordInstantSearchClicksSwapReason(II)V

    .line 726
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->swapInBackgroundView()V

    goto :goto_0

    .line 715
    :cond_2
    int-to-float v1, v1

    invoke-virtual {v0}, Lorg/chromium/content/browser/RenderCoordinates;->getMaxVerticalScrollPix()F

    move-result v0

    cmpl-float v0, v1, v0

    if-ltz v0, :cond_3

    .line 716
    const/4 v0, 0x2

    invoke-static {v0, v2}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->recordInstantSearchClicksPreviewScrollState(II)V

    goto :goto_1

    .line 719
    :cond_3
    const/4 v0, 0x1

    invoke-static {v0, v2}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->recordInstantSearchClicksPreviewScrollState(II)V

    goto :goto_1
.end method

.method private transferPageScaleFactor()Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 632
    iget-object v1, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mTab:Lorg/chromium/chrome/browser/Tab;

    invoke-virtual {v1}, Lorg/chromium/chrome/browser/Tab;->getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v1

    invoke-virtual {v1}, Lorg/chromium/content/browser/ContentViewCore;->getRenderCoordinates()Lorg/chromium/content/browser/RenderCoordinates;

    move-result-object v1

    invoke-virtual {v1}, Lorg/chromium/content/browser/RenderCoordinates;->getPageScaleFactor()F

    move-result v1

    .line 635
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v2

    .line 636
    invoke-virtual {v2}, Lorg/chromium/content/browser/ContentViewCore;->getRenderCoordinates()Lorg/chromium/content/browser/RenderCoordinates;

    move-result-object v3

    .line 639
    invoke-virtual {v3}, Lorg/chromium/content/browser/RenderCoordinates;->getMinPageScaleFactor()F

    move-result v4

    invoke-virtual {v3}, Lorg/chromium/content/browser/RenderCoordinates;->getMaxPageScaleFactor()F

    move-result v5

    invoke-static {v1, v5}, Ljava/lang/Math;->min(FF)F

    move-result v1

    invoke-static {v4, v1}, Ljava/lang/Math;->max(FF)F

    move-result v1

    .line 641
    invoke-virtual {v3}, Lorg/chromium/content/browser/RenderCoordinates;->getPageScaleFactor()F

    move-result v4

    cmpl-float v4, v1, v4

    if-nez v4, :cond_0

    const/4 v0, 0x0

    .line 646
    :goto_0
    return v0

    .line 643
    :cond_0
    invoke-virtual {v3}, Lorg/chromium/content/browser/RenderCoordinates;->getPageScaleFactor()F

    move-result v3

    div-float/2addr v1, v3

    .line 644
    invoke-virtual {v2, v1}, Lorg/chromium/content/browser/ContentViewCore;->pinchByDelta(F)Z

    .line 645
    iput-boolean v0, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mOriginalPageZoomed:Z

    goto :goto_0
.end method

.method private transferScrollOffset()Z
    .locals 9

    .prologue
    const/4 v0, 0x1

    .line 655
    iget-object v1, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mTab:Lorg/chromium/chrome/browser/Tab;

    invoke-virtual {v1}, Lorg/chromium/chrome/browser/Tab;->getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v1

    .line 656
    invoke-virtual {v1}, Lorg/chromium/content/browser/ContentViewCore;->getRenderCoordinates()Lorg/chromium/content/browser/RenderCoordinates;

    move-result-object v1

    .line 657
    invoke-virtual {v1}, Lorg/chromium/content/browser/RenderCoordinates;->getScrollXPix()F

    move-result v2

    .line 658
    invoke-virtual {v1}, Lorg/chromium/content/browser/RenderCoordinates;->getScrollYPix()F

    move-result v1

    .line 660
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v3

    .line 661
    invoke-virtual {v3}, Lorg/chromium/content/browser/ContentViewCore;->getRenderCoordinates()Lorg/chromium/content/browser/RenderCoordinates;

    move-result-object v4

    .line 663
    invoke-virtual {v4}, Lorg/chromium/content/browser/RenderCoordinates;->getMaxHorizontalScrollPix()F

    move-result v5

    .line 664
    invoke-virtual {v4}, Lorg/chromium/content/browser/RenderCoordinates;->getMaxVerticalScrollPix()F

    move-result v6

    .line 667
    invoke-static {v2, v5}, Ljava/lang/Math;->min(FF)F

    move-result v2

    .line 668
    invoke-static {v1, v6}, Ljava/lang/Math;->min(FF)F

    move-result v1

    .line 670
    invoke-virtual {v4}, Lorg/chromium/content/browser/RenderCoordinates;->getScrollXPix()F

    move-result v5

    .line 671
    invoke-virtual {v4}, Lorg/chromium/content/browser/RenderCoordinates;->getScrollYPix()F

    move-result v6

    .line 672
    sub-float v7, v2, v5

    invoke-virtual {v4, v7}, Lorg/chromium/content/browser/RenderCoordinates;->fromPixToLocalCss(F)F

    move-result v7

    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    move-result v7

    .line 674
    sub-float v8, v1, v6

    invoke-virtual {v4, v8}, Lorg/chromium/content/browser/RenderCoordinates;->fromPixToLocalCss(F)F

    move-result v4

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    .line 677
    invoke-static {v7}, Ljava/lang/Math;->abs(I)I

    move-result v7

    if-gt v7, v0, :cond_0

    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    move-result v4

    if-gt v4, v0, :cond_0

    const/4 v0, 0x0

    .line 685
    :goto_0
    return v0

    .line 679
    :cond_0
    iget-boolean v4, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mOriginalPageZoomed:Z

    if-nez v4, :cond_1

    .line 680
    sub-float/2addr v2, v5

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    sub-float/2addr v1, v6

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    invoke-virtual {v3, v2, v1}, Lorg/chromium/content/browser/ContentViewCore;->scrollBy(II)V

    goto :goto_0

    .line 683
    :cond_1
    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    invoke-virtual {v3, v2, v1}, Lorg/chromium/content/browser/ContentViewCore;->scrollTo(II)V

    goto :goto_0
.end method

.method private trySwappingBackgroundView()V
    .locals 1

    .prologue
    .line 533
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mSwapInProgress:Z

    if-eqz v0, :cond_1

    .line 538
    :cond_0
    :goto_0
    return-void

    .line 536
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->hasPendingBackgroundPage()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 537
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->isBackgroundViewReady()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->backgroundViewReady()V

    goto :goto_0
.end method


# virtual methods
.method public forceSwappingContentViews()V
    .locals 2

    .prologue
    .line 761
    const/4 v0, 0x5

    const/4 v1, 0x6

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->recordInstantSearchClicksSwapReason(II)V

    .line 763
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->swapInBackgroundView()V

    .line 764
    return-void
.end method

.method public getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;
    .locals 1

    .prologue
    .line 315
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    return-object v0
.end method

.method public getProgress()I
    .locals 1

    .prologue
    .line 320
    iget v0, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mLoadProgress:I

    return v0
.end method

.method public hasPendingBackgroundPage()Z
    .locals 1

    .prologue
    .line 300
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPageSwappingInProgress()Z
    .locals 1

    .prologue
    .line 308
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mSwapInProgress:Z

    return v0
.end method

.method public loadOriginalUrlIfPreview(Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    .line 602
    iget v0, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mExperimentGroup:I

    if-nez v0, :cond_1

    .line 624
    :cond_0
    :goto_0
    return-void

    .line 607
    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->getOriginalUrlForPreviewUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 608
    if-eqz v0, :cond_0

    .line 610
    iget v1, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mExperimentGroup:I

    if-ne v1, v2, :cond_2

    .line 611
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->setLogUmaForControlGroupIfNeeded(Ljava/lang/String;)V

    goto :goto_0

    .line 615
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->hasPendingBackgroundPage()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 616
    const/4 v1, 0x6

    invoke-static {v2, v1}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->recordInstantSearchClicksSwapReason(II)V

    .line 621
    :cond_3
    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->createContentViewCoreWithUrl(Ljava/lang/String;)V

    .line 623
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->setLogUma()V

    goto :goto_0
.end method

.method public onContentChanged(Lorg/chromium/chrome/browser/Tab;)V
    .locals 2

    .prologue
    .line 803
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mCurrentViewWebContentsObserver:Lorg/chromium/content/browser/WebContentsObserverAndroid;

    if-eqz v0, :cond_0

    .line 804
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mCurrentViewWebContentsObserver:Lorg/chromium/content/browser/WebContentsObserverAndroid;

    invoke-virtual {v0}, Lorg/chromium/content/browser/WebContentsObserverAndroid;->detachFromWebContents()V

    .line 806
    :cond_0
    invoke-virtual {p1}, Lorg/chromium/chrome/browser/Tab;->getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    if-nez v0, :cond_1

    .line 848
    :goto_0
    return-void

    .line 807
    :cond_1
    new-instance v0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper$7;

    iget-object v1, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mTab:Lorg/chromium/chrome/browser/Tab;

    invoke-virtual {v1}, Lorg/chromium/chrome/browser/Tab;->getWebContents()Lorg/chromium/content_public/browser/WebContents;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper$7;-><init>(Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;Lorg/chromium/content_public/browser/WebContents;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mCurrentViewWebContentsObserver:Lorg/chromium/content/browser/WebContentsObserverAndroid;

    goto :goto_0
.end method

.method public onDestroyed(Lorg/chromium/chrome/browser/Tab;)V
    .locals 0

    .prologue
    .line 852
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->destroy()V

    .line 853
    return-void
.end method

.method public onLoadProgressChanged(Lorg/chromium/chrome/browser/Tab;I)V
    .locals 1

    .prologue
    .line 794
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->hasPendingBackgroundPage()Z

    move-result v0

    if-nez v0, :cond_1

    .line 799
    :cond_0
    :goto_0
    return-void

    .line 795
    :cond_1
    const/16 v0, 0x64

    if-lt p2, v0, :cond_0

    .line 796
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mPreviewLoaded:Z

    .line 797
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->trySwappingBackgroundView()V

    goto :goto_0
.end method

.method public recordBack()V
    .locals 1

    .prologue
    .line 861
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mRecordUma:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->recordInstantSearchClicksBack()V

    .line 862
    :cond_0
    return-void
.end method

.method public recordReload()V
    .locals 1

    .prologue
    .line 865
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mRecordUma:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->recordInstantSearchClicksReload()V

    .line 866
    :cond_0
    return-void
.end method

.method public recordSingleTap()V
    .locals 1

    .prologue
    .line 873
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mRecordUma:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->recordInstantSearchClicksSingleTap()V

    .line 874
    :cond_0
    return-void
.end method

.method public recordTabClose()V
    .locals 1

    .prologue
    .line 869
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mRecordUma:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->recordInstantSearchClicksTabClose()V

    .line 870
    :cond_0
    return-void
.end method

.method public setBackgroundViewListener(Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper$BackgroundContentViewListener;)V
    .locals 0

    .prologue
    .line 328
    iput-object p1, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mListener:Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper$BackgroundContentViewListener;

    .line 329
    return-void
.end method

.method public stopLoading()V
    .locals 0

    .prologue
    .line 754
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->destroyContentViewCore()V

    .line 755
    return-void
.end method

.method public unloadAndDeleteWebContents(Lorg/chromium/content_public/browser/WebContents;)V
    .locals 2

    .prologue
    .line 771
    iget-wide v0, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mNativeBackgroundContentViewHelperPtr:J

    invoke-direct {p0, v0, v1, p1}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->nativeUnloadAndDeleteWebContents(JLorg/chromium/content_public/browser/WebContents;)V

    .line 772
    return-void
.end method
