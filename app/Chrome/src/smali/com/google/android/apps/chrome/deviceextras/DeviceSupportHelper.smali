.class public Lcom/google/android/apps/chrome/deviceextras/DeviceSupportHelper;
.super Ljava/lang/Object;
.source "DeviceSupportHelper.java"


# static fields
.field private static sSupportPackageInfo:Lcom/google/android/apps/chrome/deviceextras/DeviceSupportHelper$SupportPackageInfo;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getSupportPackageName(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 57
    invoke-static {p0}, Lcom/google/android/apps/chrome/deviceextras/DeviceSupportHelper;->initializeSupportPackageInfo(Landroid/content/Context;)V

    .line 58
    sget-object v0, Lcom/google/android/apps/chrome/deviceextras/DeviceSupportHelper;->sSupportPackageInfo:Lcom/google/android/apps/chrome/deviceextras/DeviceSupportHelper$SupportPackageInfo;

    iget-object v0, v0, Lcom/google/android/apps/chrome/deviceextras/DeviceSupportHelper$SupportPackageInfo;->mSupportPackageName:Ljava/lang/String;

    return-object v0
.end method

.method private static hasSupportApkPermission(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 75
    const-string/jumbo v0, "com.chrome.permission.DEVICE_EXTRAS"

    invoke-virtual {p0, v0}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static initializeSupportPackageInfo(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 28
    sget-object v0, Lcom/google/android/apps/chrome/deviceextras/DeviceSupportHelper;->sSupportPackageInfo:Lcom/google/android/apps/chrome/deviceextras/DeviceSupportHelper$SupportPackageInfo;

    if-eqz v0, :cond_0

    .line 46
    :goto_0
    return-void

    .line 31
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 32
    const-string/jumbo v0, "com.chrome.deviceextras"

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3}, Landroid/content/pm/PackageManager;->resolveContentProvider(Ljava/lang/String;I)Landroid/content/pm/ProviderInfo;

    move-result-object v0

    .line 34
    if-eqz v0, :cond_3

    .line 35
    iget-object v0, v0, Landroid/content/pm/ProviderInfo;->packageName:Ljava/lang/String;

    .line 36
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v0}, Landroid/content/pm/PackageManager;->checkSignatures(Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    .line 38
    if-nez v2, :cond_1

    invoke-static {p0}, Lcom/google/android/apps/chrome/deviceextras/DeviceSupportHelper;->hasSupportApkPermission(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_2

    :cond_1
    move-object v0, v1

    .line 44
    :cond_2
    :goto_1
    new-instance v2, Lcom/google/android/apps/chrome/deviceextras/DeviceSupportHelper$SupportPackageInfo;

    invoke-direct {v2, v1}, Lcom/google/android/apps/chrome/deviceextras/DeviceSupportHelper$SupportPackageInfo;-><init>(Lcom/google/android/apps/chrome/deviceextras/DeviceSupportHelper$1;)V

    .line 45
    sput-object v2, Lcom/google/android/apps/chrome/deviceextras/DeviceSupportHelper;->sSupportPackageInfo:Lcom/google/android/apps/chrome/deviceextras/DeviceSupportHelper$SupportPackageInfo;

    iput-object v0, v2, Lcom/google/android/apps/chrome/deviceextras/DeviceSupportHelper$SupportPackageInfo;->mSupportPackageName:Ljava/lang/String;

    goto :goto_0

    :cond_3
    move-object v0, v1

    goto :goto_1
.end method

.method public static isSupportPackageAvailable(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 69
    invoke-static {p0}, Lcom/google/android/apps/chrome/deviceextras/DeviceSupportHelper;->initializeSupportPackageInfo(Landroid/content/Context;)V

    .line 70
    invoke-static {p0}, Lcom/google/android/apps/chrome/deviceextras/DeviceSupportHelper;->hasSupportApkPermission(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/chrome/deviceextras/DeviceSupportHelper;->sSupportPackageInfo:Lcom/google/android/apps/chrome/deviceextras/DeviceSupportHelper$SupportPackageInfo;

    iget-object v0, v0, Lcom/google/android/apps/chrome/deviceextras/DeviceSupportHelper$SupportPackageInfo;->mSupportPackageName:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
