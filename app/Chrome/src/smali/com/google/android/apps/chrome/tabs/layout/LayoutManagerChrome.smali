.class public Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;
.super Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;
.source "LayoutManagerChrome.java"

# interfaces
.implements Lcom/google/android/apps/chrome/OverviewBehavior;


# static fields
.field private static final NOTIFICATIONS:[I


# instance fields
.field protected mAccessibilityLayout:Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;

.field protected mBlackHoleEventFilter:Lcom/google/android/apps/chrome/eventfilter/EventFilter;

.field private mCreatingNtp:Z

.field protected mEnableAnimations:Z

.field private mGestureEventFilter:Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;

.field protected mIncognitoTitleBitmapFactory:Lcom/google/android/apps/chrome/tabs/TitleBitmapFactory;

.field private final mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

.field protected mStackLayout:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;

.field protected mStandardTitleBitmapFactory:Lcom/google/android/apps/chrome/tabs/TitleBitmapFactory;

.field protected mTitleCache:Lcom/google/android/apps/chrome/tabs/TitleCache;

.field private final mToolbarSwipeHandler:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;

.field protected mToolbarSwipeLayout:Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 357
    const/16 v0, 0x10

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->NOTIFICATIONS:[I

    return-void

    :array_0
    .array-data 4
        0x20
        0x3
        0x2
        0x30
        0x5
        0xc
        0x4
        0x2b
        0x8
        0x9
        0x1c
        0x1a
        0x1b
        0x44
        0x45
        0x49
    .end array-data
.end method

.method public constructor <init>(Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;Lcom/google/android/apps/chrome/eventfilter/EventFilterHost;Z)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 83
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;-><init>(Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;Lcom/google/android/apps/chrome/eventfilter/EventFilterHost;)V

    .line 72
    iput-boolean v4, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->mEnableAnimations:Z

    .line 396
    new-instance v0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome$1;-><init>(Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    .line 84
    invoke-interface {p1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 85
    invoke-interface {p1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;->getLayoutRenderHost()Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;

    move-result-object v1

    .line 88
    new-instance v2, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome$ToolbarSwipeHandler;

    invoke-direct {v2, p0, p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome$ToolbarSwipeHandler;-><init>(Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;Lcom/google/android/apps/chrome/tabs/layout/LayoutProvider;)V

    iput-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->mToolbarSwipeHandler:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;

    .line 91
    new-instance v2, Lcom/google/android/apps/chrome/eventfilter/BlackHoleEventFilter;

    invoke-direct {v2, p2}, Lcom/google/android/apps/chrome/eventfilter/BlackHoleEventFilter;-><init>(Lcom/google/android/apps/chrome/eventfilter/EventFilterHost;)V

    iput-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->mBlackHoleEventFilter:Lcom/google/android/apps/chrome/eventfilter/EventFilter;

    .line 92
    new-instance v2, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;

    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->mGestureHandler:Lcom/google/android/apps/chrome/eventfilter/GestureHandler;

    invoke-direct {v2, v0, p2, v3}, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;-><init>(Landroid/content/Context;Lcom/google/android/apps/chrome/eventfilter/EventFilterHost;Lcom/google/android/apps/chrome/eventfilter/GestureHandler;)V

    iput-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->mGestureEventFilter:Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;

    .line 95
    new-instance v2, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;

    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->mBlackHoleEventFilter:Lcom/google/android/apps/chrome/eventfilter/EventFilter;

    invoke-direct {v2, v0, p0, v1, v3}, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;-><init>(Landroid/content/Context;Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;Lcom/google/android/apps/chrome/eventfilter/EventFilter;)V

    iput-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->mAccessibilityLayout:Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;

    .line 97
    new-instance v2, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;

    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->mBlackHoleEventFilter:Lcom/google/android/apps/chrome/eventfilter/EventFilter;

    invoke-direct {v2, v0, p0, v1, v3}, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;-><init>(Landroid/content/Context;Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;Lcom/google/android/apps/chrome/eventfilter/EventFilter;)V

    iput-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->mToolbarSwipeLayout:Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;

    .line 99
    if-eqz p3, :cond_0

    .line 100
    new-instance v2, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;

    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->mGestureEventFilter:Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;

    invoke-direct {v2, v0, p0, v1, v3}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;-><init>(Landroid/content/Context;Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;Lcom/google/android/apps/chrome/eventfilter/EventFilter;)V

    iput-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->mStackLayout:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;

    .line 104
    :cond_0
    new-instance v1, Lcom/google/android/apps/chrome/tabs/TitleBitmapFactory;

    const/4 v2, 0x0

    invoke-direct {v1, v0, v2}, Lcom/google/android/apps/chrome/tabs/TitleBitmapFactory;-><init>(Landroid/content/Context;Z)V

    iput-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->mStandardTitleBitmapFactory:Lcom/google/android/apps/chrome/tabs/TitleBitmapFactory;

    .line 105
    new-instance v1, Lcom/google/android/apps/chrome/tabs/TitleBitmapFactory;

    invoke-direct {v1, v0, v4}, Lcom/google/android/apps/chrome/tabs/TitleBitmapFactory;-><init>(Landroid/content/Context;Z)V

    iput-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->mIncognitoTitleBitmapFactory:Lcom/google/android/apps/chrome/tabs/TitleBitmapFactory;

    .line 106
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;IIIZ)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->tabMoved(IIIZ)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;Z)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->closeAllTabsRequest(Z)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;IZ)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->tabPageLoadStarted(IZ)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;IZ)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->tabPageLoadFinished(IZ)V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;IZ)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->tabLoadStarted(IZ)V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;IZ)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->tabLoadFinished(IZ)V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;IZ)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->tabClosurePending(IZ)V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;IZ)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->tabClosureCancelled(IZ)V

    return-void
.end method

.method private closeAllTabsRequest(Z)V
    .locals 4

    .prologue
    .line 309
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->handlesCloseAll()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 310
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    invoke-static {}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->time()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3, p1}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->onTabsAllClosing(JZ)V

    .line 315
    :goto_0
    return-void

    .line 313
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->getTabModelSelector()Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    move-result-object v0

    invoke-interface {v0, p1}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->getModel(Z)Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v0

    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->closeAllTabs()V

    goto :goto_0
.end method

.method private overviewNotify(Lcom/google/android/apps/chrome/tabs/layout/Layout;ILandroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 199
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->mStackLayout:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;

    if-eq p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->mAccessibilityLayout:Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;

    if-ne p1, v0, :cond_1

    .line 200
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->mHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p2, p3}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastImmediateNotification(Landroid/content/Context;ILandroid/os/Bundle;)V

    .line 203
    :cond_1
    return-void
.end method

.method private registerNotifications()V
    .locals 3

    .prologue
    .line 377
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->getNotificationsToRegisterFor()[I

    move-result-object v0

    .line 378
    if-eqz v0, :cond_0

    .line 379
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->mHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;

    invoke-interface {v1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-static {v1, v0, v2}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->registerForNotifications(Landroid/content/Context;[ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    .line 382
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->mHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->NOTIFICATIONS:[I

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->registerForNotifications(Landroid/content/Context;[ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    .line 384
    return-void
.end method

.method private tabClosureCancelled(IZ)V
    .locals 4

    .prologue
    .line 352
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 353
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    invoke-static {}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->time()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->onTabClosureCancelled(JIZ)V

    .line 355
    :cond_0
    return-void
.end method

.method private tabClosurePending(IZ)V
    .locals 4

    .prologue
    .line 348
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    invoke-static {}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->time()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->onTabClosurePending(JIZ)V

    .line 349
    :cond_0
    return-void
.end method

.method private tabLoadFinished(IZ)V
    .locals 1

    .prologue
    .line 344
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->onTabLoadFinished(IZ)V

    .line 345
    :cond_0
    return-void
.end method

.method private tabLoadStarted(IZ)V
    .locals 1

    .prologue
    .line 340
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->onTabLoadStarted(IZ)V

    .line 341
    :cond_0
    return-void
.end method

.method private tabMoved(IIIZ)V
    .locals 8

    .prologue
    .line 326
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 327
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v1

    invoke-static {}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->time()J

    move-result-wide v2

    move v4, p1

    move v5, p2

    move v6, p3

    move v7, p4

    invoke-virtual/range {v1 .. v7}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->onTabMoved(JIIIZ)V

    .line 329
    :cond_0
    return-void
.end method

.method private tabPageLoadFinished(IZ)V
    .locals 1

    .prologue
    .line 336
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->onTabPageLoadFinished(IZ)V

    .line 337
    :cond_0
    return-void
.end method

.method private tabPageLoadStarted(IZ)V
    .locals 1

    .prologue
    .line 332
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->onTabPageLoadStarted(IZ)V

    .line 333
    :cond_0
    return-void
.end method

.method private unregisterNotifications()V
    .locals 3

    .prologue
    .line 387
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->getNotificationsToRegisterFor()[I

    move-result-object v0

    .line 388
    if-eqz v0, :cond_0

    .line 389
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->mHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;

    invoke-interface {v1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-static {v1, v0, v2}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->unregisterForNotifications(Landroid/content/Context;[ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    .line 392
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->mHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->NOTIFICATIONS:[I

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->unregisterForNotifications(Landroid/content/Context;[ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    .line 394
    return-void
.end method


# virtual methods
.method public destroy()V
    .locals 0

    .prologue
    .line 148
    invoke-super {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;->destroy()V

    .line 149
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->unregisterNotifications()V

    .line 150
    return-void
.end method

.method public doneHiding()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 234
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v2

    .line 236
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->getNextLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->getDefaultLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v3

    if-ne v0, v3, :cond_0

    .line 237
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->getTabModelSelector()Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->getTabModelSelector()Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    move-result-object v0

    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->getCurrentTab()Lorg/chromium/chrome/browser/Tab;

    move-result-object v0

    .line 238
    :goto_0
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/Tab;->getId()I

    move-result v0

    :goto_1
    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->emptyCachesExcept(I)V

    .line 241
    :cond_0
    invoke-super {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;->doneHiding()V

    .line 243
    const/4 v0, 0x1

    invoke-direct {p0, v2, v0, v1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->overviewNotify(Lcom/google/android/apps/chrome/tabs/layout/Layout;ILandroid/os/Bundle;)V

    .line 245
    return-void

    :cond_1
    move-object v0, v1

    .line 237
    goto :goto_0

    .line 238
    :cond_2
    const/4 v0, -0x1

    goto :goto_1
.end method

.method public getAccessibilityOverviewLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;
    .locals 1

    .prologue
    .line 585
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->mAccessibilityLayout:Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;

    return-object v0
.end method

.method protected getFaviconBitmap(Lorg/chromium/chrome/browser/Tab;)Landroid/graphics/Bitmap;
    .locals 3

    .prologue
    .line 575
    invoke-virtual {p1}, Lorg/chromium/chrome/browser/Tab;->isIncognito()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->mIncognitoTitleBitmapFactory:Lcom/google/android/apps/chrome/tabs/TitleBitmapFactory;

    .line 577
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->mHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;

    invoke-interface {v1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p1}, Lorg/chromium/chrome/browser/Tab;->getFavicon()Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/tabs/TitleBitmapFactory;->getFaviconBitmap(Landroid/content/Context;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0

    .line 575
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->mStandardTitleBitmapFactory:Lcom/google/android/apps/chrome/tabs/TitleBitmapFactory;

    goto :goto_0
.end method

.method protected getNotificationsToRegisterFor()[I
    .locals 1

    .prologue
    .line 112
    const/4 v0, 0x0

    return-object v0
.end method

.method public getStripLayoutHelperManager()Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;
    .locals 1

    .prologue
    .line 593
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getTabById(I)Lcom/google/android/apps/chrome/tab/ChromeTab;
    .locals 1

    .prologue
    .line 702
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->getTabModelSelector()Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    move-result-object v0

    .line 703
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-interface {v0, p1}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->getTabById(I)Lorg/chromium/chrome/browser/Tab;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->fromTab(Lorg/chromium/chrome/browser/Tab;)Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    goto :goto_0
.end method

.method protected getTitleBitmap(Lcom/google/android/apps/chrome/tab/ChromeTab;)Landroid/graphics/Bitmap;
    .locals 3

    .prologue
    .line 550
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->isIncognito()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->mIncognitoTitleBitmapFactory:Lcom/google/android/apps/chrome/tabs/TitleBitmapFactory;

    .line 553
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->mHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;

    invoke-interface {v1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->getTitleForTab(Lcom/google/android/apps/chrome/tab/ChromeTab;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/tabs/TitleBitmapFactory;->getTitleBitmap(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0

    .line 550
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->mStandardTitleBitmapFactory:Lcom/google/android/apps/chrome/tabs/TitleBitmapFactory;

    goto :goto_0
.end method

.method protected getTitleForTab(Lcom/google/android/apps/chrome/tab/ChromeTab;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 562
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getTitle()Ljava/lang/String;

    move-result-object v0

    .line 563
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getUrl()Ljava/lang/String;

    move-result-object v0

    .line 564
    :cond_0
    return-object v0
.end method

.method public getTopSwipeHandler()Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->mToolbarSwipeHandler:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;

    return-object v0
.end method

.method protected handleNotificationMessage(Landroid/os/Message;)V
    .locals 0

    .prologue
    .line 127
    return-void
.end method

.method public hideOverview(Z)V
    .locals 5

    .prologue
    const/4 v4, -0x1

    .line 625
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    .line 626
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->isHiding()Z

    move-result v1

    if-nez v1, :cond_0

    .line 627
    if-eqz p1, :cond_1

    .line 628
    invoke-static {}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->time()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3, v4}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->onTabSelecting(JI)V

    .line 634
    :cond_0
    :goto_0
    return-void

    .line 630
    :cond_1
    invoke-virtual {p0, v4}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->startHiding(I)V

    .line 631
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->doneHiding()V

    goto :goto_0
.end method

.method public init(Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreatorManager;Lcom/google/android/apps/chrome/compositor/TabContentManager;Landroid/view/ViewGroup;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManagementDelegate;)V
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->mHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;->getTitleCache()Lcom/google/android/apps/chrome/tabs/TitleCache;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->mTitleCache:Lcom/google/android/apps/chrome/tabs/TitleCache;

    .line 137
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->mToolbarSwipeLayout:Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;

    invoke-virtual {v0, p1, p3}, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->setTabModelSelector(Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;Lcom/google/android/apps/chrome/compositor/TabContentManager;)V

    .line 138
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->mAccessibilityLayout:Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;

    invoke-virtual {v0, p1, p3}, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->setTabModelSelector(Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;Lcom/google/android/apps/chrome/compositor/TabContentManager;)V

    .line 139
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->mStackLayout:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->mStackLayout:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;

    invoke-virtual {v0, p1, p3}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->setTabModelSelector(Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;Lcom/google/android/apps/chrome/compositor/TabContentManager;)V

    .line 141
    :cond_0
    invoke-super/range {p0 .. p5}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;->init(Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreatorManager;Lcom/google/android/apps/chrome/compositor/TabContentManager;Landroid/view/ViewGroup;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManagementDelegate;)V

    .line 143
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->registerNotifications()V

    .line 144
    return-void
.end method

.method public initLayoutTabFromHost(I)V
    .locals 6

    .prologue
    .line 525
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;->initLayoutTabFromHost(I)V

    .line 527
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->getTabModelSelector()Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    if-nez v0, :cond_1

    .line 540
    :cond_0
    :goto_0
    return-void

    .line 529
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->getTabModelSelector()Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    move-result-object v0

    .line 530
    invoke-interface {v0, p1}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->getTabById(I)Lorg/chromium/chrome/browser/Tab;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->fromTab(Lorg/chromium/chrome/browser/Tab;)Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v1

    .line 531
    if-eqz v1, :cond_0

    .line 533
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->getExistingLayoutTab(I)Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v0

    .line 534
    if-eqz v0, :cond_0

    .line 536
    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->mTitleCache:Lcom/google/android/apps/chrome/tabs/TitleCache;

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->isTitleNeeded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 537
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->mTitleCache:Lcom/google/android/apps/chrome/tabs/TitleCache;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->getTitleBitmap(Lcom/google/android/apps/chrome/tab/ChromeTab;)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->getFaviconBitmap(Lorg/chromium/chrome/browser/Tab;)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->isIncognito()Z

    move-result v4

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->isTitleDirectionRtl()Z

    move-result v5

    move v1, p1

    invoke-interface/range {v0 .. v5}, Lcom/google/android/apps/chrome/tabs/TitleCache;->put(ILandroid/graphics/Bitmap;Landroid/graphics/Bitmap;ZZ)V

    goto :goto_0
.end method

.method public overviewVisible()Z
    .locals 2

    .prologue
    .line 638
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    .line 639
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->mStackLayout:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;

    if-eq v0, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->mAccessibilityLayout:Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;

    if-ne v0, v1, :cond_1

    :cond_0
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->isHiding()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 641
    :goto_0
    return v0

    .line 639
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setEnableAnimations(Z)V
    .locals 0

    .prologue
    .line 651
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->mEnableAnimations:Z

    .line 652
    return-void
.end method

.method public setKeyboardHider(Lcom/google/android/apps/chrome/KeyboardHider;)V
    .locals 0

    .prologue
    .line 647
    return-void
.end method

.method protected setStripLayoutHelperManager(Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;)V
    .locals 1

    .prologue
    .line 154
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;->setStripLayoutHelperManager(Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;)V

    .line 155
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->mAccessibilityLayout:Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;->setStripLayoutHelperManager(Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;)V

    .line 156
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->mToolbarSwipeLayout:Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->setStripLayoutHelperManager(Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;)V

    .line 157
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->mStackLayout:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->mStackLayout:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;->setStripLayoutHelperManager(Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;)V

    .line 158
    :cond_0
    return-void
.end method

.method public showOverview(Z)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 606
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->useAccessibilityLayout()Z

    move-result v3

    .line 608
    if-eqz v3, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->mAccessibilityLayout:Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;

    if-ne v0, v4, :cond_2

    move v0, v1

    .line 610
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->mStackLayout:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;

    if-ne v4, v5, :cond_3

    iget-object v4, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->mStackLayout:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;

    if-eqz v4, :cond_3

    .line 616
    :goto_1
    if-nez v0, :cond_0

    if-eqz v3, :cond_4

    :cond_0
    if-nez v1, :cond_4

    .line 617
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->mAccessibilityLayout:Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;

    invoke-virtual {p0, v0, p1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->startShowing(Lcom/google/android/apps/chrome/tabs/layout/Layout;Z)V

    .line 621
    :cond_1
    :goto_2
    return-void

    :cond_2
    move v0, v2

    .line 608
    goto :goto_0

    :cond_3
    move v1, v2

    .line 610
    goto :goto_1

    .line 618
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->mStackLayout:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;

    if-eqz v0, :cond_1

    .line 619
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->mStackLayout:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;

    invoke-virtual {p0, v0, p1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->startShowing(Lcom/google/android/apps/chrome/tabs/layout/Layout;Z)V

    goto :goto_2
.end method

.method public simulateClick(FF)V
    .locals 4

    .prologue
    .line 179
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    invoke-static {}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->time()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->click(JFF)V

    .line 180
    :cond_0
    return-void
.end method

.method public simulateDrag(FFFF)V
    .locals 8

    .prologue
    const-wide/16 v2, 0x0

    .line 191
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 192
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    invoke-virtual {v0, v2, v3, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->onDown(JFF)V

    .line 193
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v1

    move v4, p1

    move v5, p2

    move v6, p3

    move v7, p4

    invoke-virtual/range {v1 .. v7}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->drag(JFFFF)V

    .line 194
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    invoke-static {}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->time()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->onUpOrCancel(J)V

    .line 196
    :cond_0
    return-void
.end method

.method public startHiding(I)V
    .locals 4

    .prologue
    .line 218
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;->startHiding(I)V

    .line 220
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v2

    .line 221
    const/4 v0, 0x0

    .line 222
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->mStackLayout:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;

    if-ne v2, v1, :cond_0

    .line 223
    invoke-virtual {v2, p1}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->getLayoutTab(I)Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v0

    .line 224
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->showToolbar()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    .line 225
    :goto_0
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 226
    const-string/jumbo v3, "show_toolbar"

    invoke-virtual {v1, v3, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 227
    const-string/jumbo v0, "creating_ntp"

    iget-boolean v3, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->mCreatingNtp:Z

    invoke-virtual {v1, v0, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    move-object v0, v1

    .line 229
    :cond_0
    const/16 v1, 0xf

    invoke-direct {p0, v2, v1, v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->overviewNotify(Lcom/google/android/apps/chrome/tabs/layout/Layout;ILandroid/os/Bundle;)V

    .line 230
    return-void

    .line 224
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected startShowing(Lcom/google/android/apps/chrome/tabs/layout/Layout;Z)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 207
    iput-boolean v2, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->mCreatingNtp:Z

    .line 208
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;->startShowing(Lcom/google/android/apps/chrome/tabs/layout/Layout;Z)V

    .line 210
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 211
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->getTabModelSelector()Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    move-result-object v0

    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->getCurrentModel()Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v0

    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    move v0, v1

    .line 212
    :goto_0
    const-string/jumbo v4, "show_toolbar"

    if-nez v0, :cond_1

    :goto_1
    invoke-virtual {v3, v4, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 213
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    invoke-direct {p0, v0, v2, v3}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->overviewNotify(Lcom/google/android/apps/chrome/tabs/layout/Layout;ILandroid/os/Bundle;)V

    .line 214
    return-void

    :cond_0
    move v0, v2

    .line 211
    goto :goto_0

    :cond_1
    move v1, v2

    .line 212
    goto :goto_1
.end method

.method protected tabClosed(IIZ)V
    .locals 7

    .prologue
    .line 294
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v1

    invoke-static {}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->time()J

    move-result-wide v2

    move v4, p1

    move v5, p2

    move v6, p3

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->onTabClosed(JIIZ)V

    .line 295
    :cond_0
    return-void
.end method

.method protected tabClosureCommitted(IZ)V
    .locals 4

    .prologue
    .line 303
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 304
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    invoke-static {}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->time()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->onTabClosureCommitted(JIZ)V

    .line 306
    :cond_0
    return-void
.end method

.method protected tabCreated(IILorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;ZZFF)V
    .locals 11

    .prologue
    .line 267
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->getTabModelSelector()Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    move-result-object v0

    invoke-interface {v0, p4}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->getModel(Z)Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v0

    invoke-static {v0, p1}, Lorg/chromium/chrome/browser/tabmodel/TabModelUtils;->getTabById(Lorg/chromium/chrome/browser/tabmodel/TabList;I)Lorg/chromium/chrome/browser/Tab;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->fromTab(Lorg/chromium/chrome/browser/Tab;)Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    .line 269
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->isNativePage()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->mCreatingNtp:Z

    .line 271
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->getTabModelSelector()Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    move-result-object v0

    invoke-interface {v0, p4}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->getModel(Z)Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v0

    invoke-static {v0, p1}, Lorg/chromium/chrome/browser/tabmodel/TabModelUtils;->getTabIndexById(Lorg/chromium/chrome/browser/tabmodel/TabList;I)I

    move-result v5

    .line 272
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v1

    invoke-static {}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->time()J

    move-result-wide v2

    if-nez p5, :cond_1

    const/4 v8, 0x1

    :goto_1
    move v4, p1

    move v6, p2

    move v7, p4

    move/from16 v9, p6

    move/from16 v10, p7

    invoke-virtual/range {v1 .. v10}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->onTabCreated(JIIIZZFF)V

    .line 274
    return-void

    .line 269
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 272
    :cond_1
    const/4 v8, 0x0

    goto :goto_1
.end method

.method protected tabCreating(ILjava/lang/String;Z)V
    .locals 1

    .prologue
    .line 284
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->onTabCreating(I)V

    .line 285
    :cond_0
    return-void
.end method

.method protected tabModelSwitched(Z)V
    .locals 1

    .prologue
    .line 322
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->onTabModelSwitched(Z)V

    .line 323
    :cond_0
    return-void
.end method

.method public tabSelected(IIZ)V
    .locals 7

    .prologue
    .line 250
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 251
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v1

    invoke-static {}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->time()J

    move-result-wide v2

    move v4, p1

    move v5, p2

    move v6, p3

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->onTabSelected(JIIZ)V

    .line 253
    :cond_0
    return-void
.end method

.method protected useAccessibilityLayout()Z
    .locals 1

    .prologue
    .line 600
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->mHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/device/DeviceClassManager;->isAccessibilityModeEnabled(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/android/apps/chrome/device/DeviceClassManager;->enableAccessibilityLayout()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
