.class public Lcom/google/android/apps/chrome/widget/bubble/TextBubble;
.super Landroid/widget/PopupWindow;
.source "TextBubble.java"

# interfaces
.implements Landroid/view/View$OnAttachStateChangeListener;
.implements Landroid/view/View$OnLayoutChangeListener;


# static fields
.field public static final ANIM_STYLE_ID:Ljava/lang/String; = "Animation_Style"

.field public static final BACKGROUND_INTRINSIC_PADDING:Ljava/lang/String; = "Background_Intrinsic_Padding"

.field public static final CENTER:Ljava/lang/String; = "Center"

.field public static final TEXT_STYLE_ID:Ljava/lang/String; = "Text_Style_Id"

.field public static final UP_DOWN:Ljava/lang/String; = "Up_Down"


# instance fields
.field private mAnchorBelow:Z

.field private mAnchorView:Landroid/view/View;

.field private final mBubbleTipXMargin:I

.field private final mCachedPaddingRect:Landroid/graphics/Rect;

.field private mCenterView:Z

.field private final mTooltipEdgeMargin:I

.field private final mTooltipText:Landroid/widget/TextView;

.field private final mTooltipTopMargin:I

.field private mXPosition:I

.field private mYPosition:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v2, -0x2

    const/4 v1, 0x1

    .line 77
    invoke-direct {p0}, Landroid/widget/PopupWindow;-><init>()V

    .line 54
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mAnchorBelow:Z

    .line 55
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mCenterView:Z

    .line 59
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mCachedPaddingRect:Landroid/graphics/Rect;

    .line 78
    const-string/jumbo v0, "Up_Down"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string/jumbo v0, "Up_Down"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mAnchorBelow:Z

    .line 79
    const-string/jumbo v0, "Center"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "Center"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    :cond_0
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mCenterView:Z

    .line 80
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$dimen;->tooltip_min_edge_margin:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mTooltipEdgeMargin:I

    .line 82
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$dimen;->tooltip_top_margin:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mTooltipTopMargin:I

    .line 84
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$dimen;->bubble_tip_margin:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mBubbleTipXMargin:I

    .line 87
    new-instance v0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble$BubbleBackgroundDrawable;

    invoke-direct {v0, p1, p2}, Lcom/google/android/apps/chrome/widget/bubble/TextBubble$BubbleBackgroundDrawable;-><init>(Landroid/content/Context;Landroid/os/Bundle;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 88
    const-string/jumbo v0, "Animation_Style"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string/jumbo v0, "Animation_Style"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    :goto_1
    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->setAnimationStyle(I)V

    .line 91
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mTooltipText:Landroid/widget/TextView;

    .line 92
    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mTooltipText:Landroid/widget/TextView;

    const-string/jumbo v0, "Text_Style_Id"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string/jumbo v0, "Text_Style_Id"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    :goto_2
    invoke-virtual {v1, p1, v0}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 95
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mTooltipText:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->setContentView(Landroid/view/View;)V

    .line 96
    invoke-virtual {p0, v2, v2}, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->setWindowLayoutMode(II)V

    .line 98
    return-void

    :cond_1
    move v0, v1

    .line 78
    goto :goto_0

    .line 88
    :cond_2
    const/high16 v0, 0x1030000

    goto :goto_1

    .line 92
    :cond_3
    sget v0, Lcom/google/android/apps/chrome/R$style;->info_bubble:I

    goto :goto_2
.end method

.method private calculateNewPosition()V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 131
    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mAnchorView:Landroid/view/View;

    .line 134
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mAnchorBelow:Z

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mAnchorView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    move-object v3, v2

    .line 136
    :goto_0
    if-eqz v3, :cond_7

    .line 137
    invoke-virtual {v3}, Landroid/view/View;->getLeft()I

    move-result v2

    add-int/2addr v2, v1

    .line 138
    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    move-result v1

    add-int/2addr v1, v0

    .line 139
    invoke-virtual {v3}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    instance-of v0, v0, Landroid/view/View;

    if-eqz v0, :cond_0

    .line 140
    invoke-virtual {v3}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    move-object v3, v0

    move v0, v1

    move v1, v2

    goto :goto_0

    :cond_0
    move v0, v2

    .line 143
    :goto_1
    iget-boolean v2, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mCenterView:Z

    if-eqz v2, :cond_2

    .line 145
    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mAnchorView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v0, v2

    .line 150
    :cond_1
    :goto_2
    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mTooltipText:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v2

    .line 151
    div-int/lit8 v3, v2, 0x2

    sub-int/2addr v0, v3

    .line 154
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mCachedPaddingRect:Landroid/graphics/Rect;

    invoke-virtual {v3, v4}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    .line 155
    iget-object v3, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mCachedPaddingRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    iget-object v4, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mCachedPaddingRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->right:I

    add-int/2addr v3, v4

    add-int/2addr v2, v3

    .line 156
    iget-object v3, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mCachedPaddingRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    sub-int v3, v0, v3

    .line 160
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mAnchorView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v0

    .line 162
    add-int v4, v3, v2

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v5

    if-le v4, v5, :cond_3

    .line 163
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    sub-int/2addr v0, v2

    iget v2, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mTooltipEdgeMargin:I

    sub-int/2addr v0, v2

    move v2, v0

    .line 169
    :goto_3
    sub-int v0, v2, v3

    neg-int v0, v0

    .line 170
    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v3

    iget-object v4, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mTooltipText:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    iget v5, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mBubbleTipXMargin:I

    sub-int/2addr v4, v5

    if-le v3, v4, :cond_5

    .line 171
    iget-object v3, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mTooltipText:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    iget v4, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mBubbleTipXMargin:I

    sub-int/2addr v3, v4

    int-to-float v0, v0

    invoke-static {v0}, Ljava/lang/Math;->signum(F)F

    move-result v0

    float-to-int v0, v0

    mul-int/2addr v0, v3

    move v3, v0

    .line 174
    :goto_4
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble$BubbleBackgroundDrawable;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/chrome/widget/bubble/TextBubble$BubbleBackgroundDrawable;->setBubbleArrowXOffset(I)V

    .line 176
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mAnchorBelow:Z

    if-eqz v0, :cond_4

    .line 177
    iput v2, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mXPosition:I

    .line 178
    iget v0, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mTooltipTopMargin:I

    sub-int v0, v1, v0

    iput v0, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mYPosition:I

    .line 183
    :goto_5
    return-void

    .line 146
    :cond_2
    invoke-static {}, Lorg/chromium/ui/base/LocalizationUtils;->isLayoutRtl()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 147
    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mAnchorView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v2

    add-int/2addr v0, v2

    goto/16 :goto_2

    .line 164
    :cond_3
    if-gez v3, :cond_6

    .line 165
    iget v0, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mTooltipEdgeMargin:I

    move v2, v0

    goto :goto_3

    .line 180
    :cond_4
    iput v2, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mXPosition:I

    .line 181
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mAnchorView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    sub-int/2addr v0, v1

    iget v1, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mTooltipTopMargin:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mYPosition:I

    goto :goto_5

    :cond_5
    move v3, v0

    goto :goto_4

    :cond_6
    move v2, v3

    goto :goto_3

    :cond_7
    move v6, v0

    move v0, v1

    move v1, v6

    goto/16 :goto_1

    :cond_8
    move v0, v1

    move-object v3, v2

    goto/16 :goto_0
.end method

.method private showAtCalculatedPosition()V
    .locals 4

    .prologue
    .line 190
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mAnchorBelow:Z

    if-eqz v0, :cond_0

    .line 191
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mAnchorView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x33

    iget v2, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mXPosition:I

    iget v3, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mYPosition:I

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->showAtLocation(Landroid/view/View;III)V

    .line 203
    :goto_0
    return-void

    .line 197
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mAnchorView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x53

    iget v2, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mXPosition:I

    iget v3, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mYPosition:I

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->showAtLocation(Landroid/view/View;III)V

    goto :goto_0
.end method

.method private updatePosition()Z
    .locals 4

    .prologue
    .line 222
    iget v1, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mXPosition:I

    .line 223
    iget v2, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mYPosition:I

    .line 224
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble$BubbleBackgroundDrawable;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/bubble/TextBubble$BubbleBackgroundDrawable;->getBubbleArrowOffset()I

    move-result v3

    .line 225
    invoke-direct {p0}, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->calculateNewPosition()V

    .line 226
    iget v0, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mXPosition:I

    if-ne v1, v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mYPosition:I

    if-ne v2, v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble$BubbleBackgroundDrawable;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/bubble/TextBubble$BubbleBackgroundDrawable;->getBubbleArrowOffset()I

    move-result v0

    if-eq v3, v0, :cond_1

    .line 228
    :cond_0
    const/4 v0, 0x1

    .line 230
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getBubbleTextView()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mTooltipText:Landroid/widget/TextView;

    return-object v0
.end method

.method public onLayoutChange(Landroid/view/View;IIIIIIII)V
    .locals 2

    .prologue
    .line 236
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mAnchorView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->isShown()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 237
    :goto_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->updatePosition()Z

    move-result v1

    .line 238
    if-eqz v0, :cond_2

    .line 239
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->dismiss()V

    .line 244
    :cond_0
    :goto_1
    return-void

    .line 236
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 240
    :cond_2
    if-eqz v1, :cond_0

    .line 241
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->dismiss()V

    .line 242
    invoke-direct {p0}, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->showAtCalculatedPosition()V

    goto :goto_1
.end method

.method public onViewAttachedToWindow(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 249
    return-void
.end method

.method public onViewDetachedFromWindow(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 253
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->dismiss()V

    .line 254
    return-void
.end method

.method public setOffsetY(I)V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 213
    iget v0, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mXPosition:I

    iget v1, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mYPosition:I

    add-int/2addr v1, p1

    invoke-virtual {p0, v0, v1, v2, v2}, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->update(IIII)V

    .line 214
    return-void
.end method

.method public showTextBubble(Ljava/lang/String;Landroid/view/View;II)V
    .locals 3

    .prologue
    const/high16 v2, -0x80000000

    .line 116
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mTooltipText:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 117
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mTooltipText:Landroid/widget/TextView;

    invoke-static {p3, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-static {p4, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->measure(II)V

    .line 120
    iput-object p2, p0, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->mAnchorView:Landroid/view/View;

    .line 121
    invoke-direct {p0}, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->calculateNewPosition()V

    .line 122
    invoke-direct {p0}, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->showAtCalculatedPosition()V

    .line 123
    return-void
.end method
