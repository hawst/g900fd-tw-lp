.class Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor$2;
.super Ljava/lang/Object;
.source "FirstRunSignInProcessor.java"

# interfaces
.implements Lorg/chromium/chrome/browser/signin/SigninManager$SignInFlowObserver;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;)V
    .locals 0

    .prologue
    .line 174
    iput-object p1, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor$2;->this$0:Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private completeSignIn()V
    .locals 2

    .prologue
    .line 177
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor$2;->this$0:Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;

    # getter for: Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;->mPrefManager:Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;
    invoke-static {v0}, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;->access$400(Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getFirstRunFlowSignInSetupSync()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 178
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor$2;->this$0:Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;

    iget-object v1, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor$2;->this$0:Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;

    # getter for: Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;->mActivity:Landroid/app/Activity;
    invoke-static {v1}, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;->access$500(Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;)Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lorg/chromium/sync/signin/ChromeSigninController;->get(Landroid/content/Context;)Lorg/chromium/sync/signin/ChromeSigninController;

    move-result-object v1

    invoke-virtual {v1}, Lorg/chromium/sync/signin/ChromeSigninController;->getSignedInAccountName()Ljava/lang/String;

    move-result-object v1

    # invokes: Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;->openSyncSettings(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;->access$1000(Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;Ljava/lang/String;)V

    .line 181
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor$2;->this$0:Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;

    # getter for: Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;->mPrefManager:Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;
    invoke-static {v0}, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;->access$400(Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->setFirstRunFlowSignInComplete(Z)V

    .line 182
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor$2;->this$0:Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;

    # getter for: Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;->access$500(Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;)Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/sync/signin/ChromeSigninController;->get(Landroid/content/Context;)Lorg/chromium/sync/signin/ChromeSigninController;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/sync/signin/ChromeSigninController;->isSignedIn()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 183
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->freSignInSuccessful()V

    .line 187
    :goto_0
    return-void

    .line 185
    :cond_1
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->freSignInAttempted()V

    goto :goto_0
.end method


# virtual methods
.method public onSigninCancelled()V
    .locals 1

    .prologue
    .line 197
    invoke-direct {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor$2;->completeSignIn()V

    .line 198
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor$2;->this$0:Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;

    # getter for: Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;->mObserver:Lorg/chromium/chrome/browser/signin/SigninManager$SignInFlowObserver;
    invoke-static {v0}, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;->access$600(Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;)Lorg/chromium/chrome/browser/signin/SigninManager$SignInFlowObserver;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor$2;->this$0:Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;

    # getter for: Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;->mObserver:Lorg/chromium/chrome/browser/signin/SigninManager$SignInFlowObserver;
    invoke-static {v0}, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;->access$600(Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;)Lorg/chromium/chrome/browser/signin/SigninManager$SignInFlowObserver;

    move-result-object v0

    invoke-interface {v0}, Lorg/chromium/chrome/browser/signin/SigninManager$SignInFlowObserver;->onSigninCancelled()V

    .line 199
    :cond_0
    return-void
.end method

.method public onSigninComplete()V
    .locals 1

    .prologue
    .line 191
    invoke-direct {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor$2;->completeSignIn()V

    .line 192
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor$2;->this$0:Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;

    # getter for: Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;->mObserver:Lorg/chromium/chrome/browser/signin/SigninManager$SignInFlowObserver;
    invoke-static {v0}, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;->access$600(Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;)Lorg/chromium/chrome/browser/signin/SigninManager$SignInFlowObserver;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor$2;->this$0:Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;

    # getter for: Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;->mObserver:Lorg/chromium/chrome/browser/signin/SigninManager$SignInFlowObserver;
    invoke-static {v0}, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;->access$600(Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;)Lorg/chromium/chrome/browser/signin/SigninManager$SignInFlowObserver;

    move-result-object v0

    invoke-interface {v0}, Lorg/chromium/chrome/browser/signin/SigninManager$SignInFlowObserver;->onSigninComplete()V

    .line 193
    :cond_0
    return-void
.end method
