.class public Lcom/google/android/apps/chrome/icing/IcingJniBridge;
.super Ljava/lang/Object;
.source "IcingJniBridge.java"

# interfaces
.implements Lcom/google/android/apps/chrome/icing/SearchJniBridge;


# annotations
.annotation runtime Lorg/chromium/base/JNINamespace;
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private mDataChangeObserver:Lcom/google/android/apps/chrome/icing/SearchJniBridge$DataChangeObserver;

.field private mNativeIcingJniBridge:J

.field private final mStarted:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const-class v0, Lcom/google/android/apps/chrome/icing/IcingJniBridge;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/icing/IcingJniBridge;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/icing/IcingJniBridge;->mStarted:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-void
.end method

.method static synthetic access$002(Lcom/google/android/apps/chrome/icing/IcingJniBridge;J)J
    .locals 1

    .prologue
    .line 23
    iput-wide p1, p0, Lcom/google/android/apps/chrome/icing/IcingJniBridge;->mNativeIcingJniBridge:J

    return-wide p1
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/icing/IcingJniBridge;)J
    .locals 2

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/google/android/apps/chrome/icing/IcingJniBridge;->nativeInit()J

    move-result-wide v0

    return-wide v0
.end method

.method private static createDeltaFileEntriesArray(I)[Lcom/google/android/apps/chrome/icing/DeltaFileEntry;
    .locals 1

    .prologue
    .line 127
    new-array v0, p0, [Lcom/google/android/apps/chrome/icing/DeltaFileEntry;

    return-object v0
.end method

.method private static createUsageReportsArray(I)[Lcom/google/android/apps/chrome/icing/UsageReport;
    .locals 1

    .prologue
    .line 138
    new-array v0, p0, [Lcom/google/android/apps/chrome/icing/UsageReport;

    return-object v0
.end method

.method private isInitialized()Z
    .locals 4

    .prologue
    .line 54
    iget-wide v0, p0, Lcom/google/android/apps/chrome/icing/IcingJniBridge;->mNativeIcingJniBridge:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/icing/IcingJniBridge;->mDataChangeObserver:Lcom/google/android/apps/chrome/icing/SearchJniBridge$DataChangeObserver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/icing/IcingJniBridge;->mStarted:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private native nativeAddHistoricVisitsToUsageReportsBuffer(J)Z
.end method

.method private native nativeDump(J)Ljava/lang/String;
.end method

.method private native nativeGetUsageReportsBatch(JI)[Lcom/google/android/apps/chrome/icing/UsageReport;
.end method

.method private native nativeInit()J
.end method

.method private native nativeQuery(JJI)[Lcom/google/android/apps/chrome/icing/DeltaFileEntry;
.end method

.method private native nativeRemoveUsageReports(J[Ljava/lang/String;)V
.end method

.method private native nativeTrimDeltaFile(JJ)J
.end method

.method private onDataChanged()V
    .locals 3

    .prologue
    .line 149
    const-string/jumbo v0, "IcingJniBridge"

    const-string/jumbo v1, "onDataChanged"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/icing/IcingDebugLogger;->debug(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 150
    iget-object v0, p0, Lcom/google/android/apps/chrome/icing/IcingJniBridge;->mDataChangeObserver:Lcom/google/android/apps/chrome/icing/SearchJniBridge$DataChangeObserver;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/icing/SearchJniBridge$DataChangeObserver;->onDataChanged()V

    .line 151
    return-void
.end method

.method private onDataCleared()V
    .locals 3

    .prologue
    .line 155
    const-string/jumbo v0, "IcingJniBridge"

    const-string/jumbo v1, "onDataCleared"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/icing/IcingDebugLogger;->debug(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 156
    iget-object v0, p0, Lcom/google/android/apps/chrome/icing/IcingJniBridge;->mDataChangeObserver:Lcom/google/android/apps/chrome/icing/SearchJniBridge$DataChangeObserver;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/icing/SearchJniBridge$DataChangeObserver;->onDataCleared()V

    .line 157
    return-void
.end method

.method private static setDeltaFileEntry([Lcom/google/android/apps/chrome/icing/DeltaFileEntry;IJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .locals 10

    .prologue
    .line 133
    new-instance v1, Lcom/google/android/apps/chrome/icing/DeltaFileEntry;

    move-wide v2, p2

    move-object v4, p4

    move-object v5, p5

    move-object/from16 v6, p6

    move/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    invoke-direct/range {v1 .. v9}, Lcom/google/android/apps/chrome/icing/DeltaFileEntry;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    aput-object v1, p0, p1

    .line 134
    return-void
.end method

.method private static setUsageReport([Lcom/google/android/apps/chrome/icing/UsageReport;ILjava/lang/String;Ljava/lang/String;JZ)V
    .locals 8

    .prologue
    .line 144
    new-instance v1, Lcom/google/android/apps/chrome/icing/UsageReport;

    move-object v2, p2

    move-object v3, p3

    move-wide v4, p4

    move v6, p6

    invoke-direct/range {v1 .. v6}, Lcom/google/android/apps/chrome/icing/UsageReport;-><init>(Ljava/lang/String;Ljava/lang/String;JZ)V

    aput-object v1, p0, p1

    .line 145
    return-void
.end method

.method private startReportingTask()V
    .locals 3

    .prologue
    .line 161
    const-string/jumbo v0, "IcingJniBridge"

    const-string/jumbo v1, "startReportingTask"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/icing/IcingDebugLogger;->debug(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 162
    iget-object v0, p0, Lcom/google/android/apps/chrome/icing/IcingJniBridge;->mDataChangeObserver:Lcom/google/android/apps/chrome/icing/SearchJniBridge$DataChangeObserver;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/icing/SearchJniBridge$DataChangeObserver;->startReportingTask()V

    .line 163
    return-void
.end method

.method private stopReportingTask()V
    .locals 3

    .prologue
    .line 167
    const-string/jumbo v0, "IcingJniBridge"

    const-string/jumbo v1, "stopReportingTask"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/icing/IcingDebugLogger;->debug(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 168
    iget-object v0, p0, Lcom/google/android/apps/chrome/icing/IcingJniBridge;->mDataChangeObserver:Lcom/google/android/apps/chrome/icing/SearchJniBridge$DataChangeObserver;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/icing/SearchJniBridge$DataChangeObserver;->stopReportingTask()V

    .line 169
    return-void
.end method


# virtual methods
.method public addHistoricVisitsToUsageReportsBuffer()Z
    .locals 2

    .prologue
    .line 111
    invoke-direct {p0}, Lcom/google/android/apps/chrome/icing/IcingJniBridge;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 112
    :goto_0
    return v0

    :cond_0
    iget-wide v0, p0, Lcom/google/android/apps/chrome/icing/IcingJniBridge;->mNativeIcingJniBridge:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/icing/IcingJniBridge;->nativeAddHistoricVisitsToUsageReportsBuffer(J)Z

    move-result v0

    goto :goto_0
.end method

.method public dump(Ljava/io/PrintWriter;)V
    .locals 4

    .prologue
    .line 117
    const-string/jumbo v0, "\nIcingJniBridge ["

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "started: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/apps/chrome/icing/IcingJniBridge;->mStarted:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, ", initialized: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/google/android/apps/chrome/icing/IcingJniBridge;->isInitialized()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    .line 119
    invoke-direct {p0}, Lcom/google/android/apps/chrome/icing/IcingJniBridge;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 120
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, ", "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, Lcom/google/android/apps/chrome/icing/IcingJniBridge;->mNativeIcingJniBridge:J

    invoke-direct {p0, v2, v3}, Lcom/google/android/apps/chrome/icing/IcingJniBridge;->nativeDump(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    .line 122
    :cond_0
    const-string/jumbo v0, "]"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    .line 123
    return-void
.end method

.method public getUsageReportsBatch(I)[Lcom/google/android/apps/chrome/icing/UsageReport;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 87
    invoke-direct {p0}, Lcom/google/android/apps/chrome/icing/IcingJniBridge;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 88
    const-string/jumbo v0, "IcingJniBridge"

    const-string/jumbo v1, "getUsageReportsBatch when not initialized"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 89
    new-array v0, v2, [Lcom/google/android/apps/chrome/icing/UsageReport;

    .line 92
    :goto_0
    return-object v0

    .line 91
    :cond_0
    const-string/jumbo v0, "IcingJniBridge"

    const-string/jumbo v1, "getUsageReportsBatch %d"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/icing/IcingDebugLogger;->debug(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 92
    iget-wide v0, p0, Lcom/google/android/apps/chrome/icing/IcingJniBridge;->mNativeIcingJniBridge:J

    invoke-direct {p0, v0, v1, p1}, Lcom/google/android/apps/chrome/icing/IcingJniBridge;->nativeGetUsageReportsBatch(JI)[Lcom/google/android/apps/chrome/icing/UsageReport;

    move-result-object v0

    goto :goto_0
.end method

.method public init(Lcom/google/android/apps/chrome/icing/SearchJniBridge$DataChangeObserver;)Z
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 33
    sget-boolean v2, Lcom/google/android/apps/chrome/icing/IcingJniBridge;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/chrome/icing/IcingJniBridge;->mDataChangeObserver:Lcom/google/android/apps/chrome/icing/SearchJniBridge$DataChangeObserver;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/chrome/icing/IcingJniBridge;->mDataChangeObserver:Lcom/google/android/apps/chrome/icing/SearchJniBridge$DataChangeObserver;

    if-eq v2, p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 34
    :cond_0
    if-nez p1, :cond_1

    .line 50
    :goto_0
    return v0

    .line 35
    :cond_1
    iget-wide v2, p0, Lcom/google/android/apps/chrome/icing/IcingJniBridge;->mNativeIcingJniBridge:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 36
    :cond_2
    iput-object p1, p0, Lcom/google/android/apps/chrome/icing/IcingJniBridge;->mDataChangeObserver:Lcom/google/android/apps/chrome/icing/SearchJniBridge$DataChangeObserver;

    .line 37
    new-instance v2, Lcom/google/android/apps/chrome/icing/IcingJniBridge$1;

    invoke-direct {v2, p0}, Lcom/google/android/apps/chrome/icing/IcingJniBridge$1;-><init>(Lcom/google/android/apps/chrome/icing/IcingJniBridge;)V

    invoke-static {v2}, Lorg/chromium/base/ThreadUtils;->runOnUiThreadBlocking(Ljava/lang/Runnable;)V

    .line 43
    iget-wide v2, p0, Lcom/google/android/apps/chrome/icing/IcingJniBridge;->mNativeIcingJniBridge:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_3

    .line 44
    const-string/jumbo v1, "IcingJniBridge"

    const-string/jumbo v2, "Initialization unsuccessful."

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 48
    :cond_3
    const-string/jumbo v2, "IcingJniBridge"

    const-string/jumbo v3, "Initialization successful."

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v2, v3, v0}, Lcom/google/android/apps/chrome/icing/IcingDebugLogger;->debug(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 49
    iget-object v0, p0, Lcom/google/android/apps/chrome/icing/IcingJniBridge;->mStarted:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    move v0, v1

    .line 50
    goto :goto_0
.end method

.method public isStartedForTest()Z
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/apps/chrome/icing/IcingJniBridge;->mStarted:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    return v0
.end method

.method public query(JI)[Lcom/google/android/apps/chrome/icing/DeltaFileEntry;
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 65
    invoke-direct {p0}, Lcom/google/android/apps/chrome/icing/IcingJniBridge;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 66
    const-string/jumbo v0, "IcingJniBridge"

    const-string/jumbo v1, "query when not initialized"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 67
    new-array v0, v7, [Lcom/google/android/apps/chrome/icing/DeltaFileEntry;

    .line 72
    :goto_0
    return-object v0

    .line 69
    :cond_0
    const-string/jumbo v0, "IcingJniBridge"

    const-string/jumbo v1, "query %d %d"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v8

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/icing/IcingDebugLogger;->debug(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 70
    iget-wide v2, p0, Lcom/google/android/apps/chrome/icing/IcingJniBridge;->mNativeIcingJniBridge:J

    move-object v1, p0

    move-wide v4, p1

    move v6, p3

    invoke-direct/range {v1 .. v6}, Lcom/google/android/apps/chrome/icing/IcingJniBridge;->nativeQuery(JJI)[Lcom/google/android/apps/chrome/icing/DeltaFileEntry;

    move-result-object v0

    .line 71
    const-string/jumbo v1, "IcingJniBridge"

    const-string/jumbo v2, "query result: %s"

    new-array v3, v8, [Ljava/lang/Object;

    const-string/jumbo v4, "\n"

    invoke-static {v4, v0}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/chrome/icing/IcingDebugLogger;->debug(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public removeUsageReports([Lcom/google/android/apps/chrome/icing/UsageReport;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 97
    invoke-direct {p0}, Lcom/google/android/apps/chrome/icing/IcingJniBridge;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 98
    const-string/jumbo v0, "IcingJniBridge"

    const-string/jumbo v1, "removeUsageReports when not initialized"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 107
    :goto_0
    return-void

    .line 101
    :cond_0
    array-length v0, p1

    new-array v2, v0, [Ljava/lang/String;

    move v0, v1

    .line 102
    :goto_1
    array-length v3, p1

    if-ge v0, v3, :cond_1

    .line 103
    aget-object v3, p1, v0

    iget-object v3, v3, Lcom/google/android/apps/chrome/icing/UsageReport;->reportId:Ljava/lang/String;

    aput-object v3, v2, v0

    .line 102
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 105
    :cond_1
    const-string/jumbo v0, "IcingJniBridge"

    const-string/jumbo v3, "removeUsageReports %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const-string/jumbo v5, ","

    invoke-static {v5, v2}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-static {v0, v3, v4}, Lcom/google/android/apps/chrome/icing/IcingDebugLogger;->debug(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 106
    iget-wide v0, p0, Lcom/google/android/apps/chrome/icing/IcingJniBridge;->mNativeIcingJniBridge:J

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/apps/chrome/icing/IcingJniBridge;->nativeRemoveUsageReports(J[Ljava/lang/String;)V

    goto :goto_0
.end method

.method public trimDeltaFile(J)J
    .locals 5

    .prologue
    .line 77
    invoke-direct {p0}, Lcom/google/android/apps/chrome/icing/IcingJniBridge;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 78
    const-string/jumbo v0, "IcingJniBridge"

    const-string/jumbo v1, "trimDeltaFile when not initialized"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 79
    const-wide/16 v0, -0x1

    .line 82
    :goto_0
    return-wide v0

    .line 81
    :cond_0
    const-string/jumbo v0, "IcingJniBridge"

    const-string/jumbo v1, "trimDeltaFile %d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/icing/IcingDebugLogger;->debug(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 82
    iget-wide v0, p0, Lcom/google/android/apps/chrome/icing/IcingJniBridge;->mNativeIcingJniBridge:J

    invoke-direct {p0, v0, v1, p1, p2}, Lcom/google/android/apps/chrome/icing/IcingJniBridge;->nativeTrimDeltaFile(JJ)J

    move-result-wide v0

    goto :goto_0
.end method
