.class public Lcom/google/android/apps/chrome/ChromeBrowserProvider;
.super Lorg/chromium/chrome/browser/ChromeBrowserProvider;
.source "ChromeBrowserProvider.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;-><init>()V

    return-void
.end method


# virtual methods
.method protected ensureNativeChromeLoadedOnUIThread()Z
    .locals 2

    .prologue
    .line 38
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeBrowserProvider;->isNativeSideInitialized()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 51
    :goto_0
    return v0

    .line 42
    :cond_0
    iget-boolean v1, p0, Lcom/google/android/apps/chrome/ChromeBrowserProvider;->mContentProviderApiCalled:Z

    .line 44
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeBrowserProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/ChromeMobileApplication;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/ChromeMobileApplication;->startChromeBrowserProcessesSync(Z)V
    :try_end_0
    .catch Lorg/chromium/base/library_loader/ProcessInitException; {:try_start_0 .. :try_end_0} :catch_0

    .line 51
    :goto_1
    invoke-super {p0}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->ensureNativeChromeLoadedOnUIThread()Z

    move-result v0

    goto :goto_0

    .line 49
    :catch_0
    move-exception v0

    const/4 v0, -0x1

    invoke-static {v0}, Ljava/lang/System;->exit(I)V

    goto :goto_1
.end method

.method public onCreate()Z
    .locals 2

    .prologue
    .line 19
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeBrowserProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/ChromeMobileApplication;->initCommandLine(Landroid/content/Context;)V

    .line 21
    const/16 v0, 0x25

    new-instance v1, Lcom/google/android/apps/chrome/ChromeBrowserProvider$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/ChromeBrowserProvider$1;-><init>(Lcom/google/android/apps/chrome/ChromeBrowserProvider;)V

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->registerForNotification(ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    .line 33
    invoke-super {p0}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->onCreate()Z

    move-result v0

    return v0
.end method
