.class Lcom/google/android/apps/chrome/services/GoogleServicesManager$4;
.super Ljava/lang/Object;
.source "GoogleServicesManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/services/GoogleServicesManager;

.field final synthetic val$account:Landroid/accounts/Account;

.field final synthetic val$states:Lcom/google/android/apps/chrome/services/GoogleServicesStates;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/services/GoogleServicesManager;Lcom/google/android/apps/chrome/services/GoogleServicesStates;Landroid/accounts/Account;)V
    .locals 0

    .prologue
    .line 425
    iput-object p1, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager$4;->this$0:Lcom/google/android/apps/chrome/services/GoogleServicesManager;

    iput-object p2, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager$4;->val$states:Lcom/google/android/apps/chrome/services/GoogleServicesStates;

    iput-object p3, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager$4;->val$account:Landroid/accounts/Account;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 428
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager$4;->val$states:Lcom/google/android/apps/chrome/services/GoogleServicesStates;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/services/GoogleServicesStates;->hasSync()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 429
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager$4;->this$0:Lcom/google/android/apps/chrome/services/GoogleServicesManager;

    iget-object v1, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager$4;->val$account:Landroid/accounts/Account;

    iget-object v2, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager$4;->val$states:Lcom/google/android/apps/chrome/services/GoogleServicesStates;

    # invokes: Lcom/google/android/apps/chrome/services/GoogleServicesManager;->setSyncState(Landroid/accounts/Account;Lcom/google/android/apps/chrome/services/GoogleServicesStates;)V
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->access$300(Lcom/google/android/apps/chrome/services/GoogleServicesManager;Landroid/accounts/Account;Lcom/google/android/apps/chrome/services/GoogleServicesStates;)V

    .line 431
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager$4;->val$states:Lcom/google/android/apps/chrome/services/GoogleServicesStates;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/services/GoogleServicesStates;->hasChromeToMobile()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 432
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager$4;->this$0:Lcom/google/android/apps/chrome/services/GoogleServicesManager;

    iget-object v1, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager$4;->val$states:Lcom/google/android/apps/chrome/services/GoogleServicesStates;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/services/GoogleServicesStates;->isChromeToMobileEnabled()Z

    move-result v1

    # invokes: Lcom/google/android/apps/chrome/services/GoogleServicesManager;->setChromeToMobileState(Z)V
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->access$400(Lcom/google/android/apps/chrome/services/GoogleServicesManager;Z)V

    .line 434
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager$4;->val$states:Lcom/google/android/apps/chrome/services/GoogleServicesStates;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/services/GoogleServicesStates;->hasAutoLoginSet()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 435
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager$4;->this$0:Lcom/google/android/apps/chrome/services/GoogleServicesManager;

    iget-object v1, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager$4;->val$states:Lcom/google/android/apps/chrome/services/GoogleServicesStates;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/services/GoogleServicesStates;->isAutoLoginEnabled()Z

    move-result v1

    # invokes: Lcom/google/android/apps/chrome/services/GoogleServicesManager;->setAutologinState(Z)V
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->access$500(Lcom/google/android/apps/chrome/services/GoogleServicesManager;Z)V

    .line 437
    :cond_2
    return-void
.end method
