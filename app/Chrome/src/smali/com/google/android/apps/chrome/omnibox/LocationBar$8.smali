.class Lcom/google/android/apps/chrome/omnibox/LocationBar$8;
.super Ljava/lang/Object;
.source "LocationBar.java"

# interfaces
.implements Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter$OmniboxSuggestionDelegate;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/omnibox/LocationBar;)V
    .locals 0

    .prologue
    .line 1570
    iput-object p1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$8;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getMaxMatchContentsWidth()F
    .locals 1

    .prologue
    .line 1629
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$8;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    # getter for: Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSuggestionList:Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->access$700(Lcom/google/android/apps/chrome/omnibox/LocationBar;)Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;->getMaxMatchContentsWidth()F

    move-result v0

    return v0
.end method

.method public getMaxRequiredWidth()F
    .locals 1

    .prologue
    .line 1624
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$8;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    # getter for: Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSuggestionList:Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->access$700(Lcom/google/android/apps/chrome/omnibox/LocationBar;)Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;->getMaxRequiredWidth()F

    move-result v0

    return v0
.end method

.method public onDeleteSuggestion(I)V
    .locals 1

    .prologue
    .line 1599
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$8;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    iget-object v0, v0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mAutocomplete:Lcom/google/android/apps/chrome/omnibox/ChromeAutocompleteController;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$8;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    iget-object v0, v0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mAutocomplete:Lcom/google/android/apps/chrome/omnibox/ChromeAutocompleteController;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/omnibox/ChromeAutocompleteController;->deleteSuggestion(I)V

    .line 1600
    :cond_0
    return-void
.end method

.method public onGestureDown()V
    .locals 2

    .prologue
    .line 1604
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$8;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    const/4 v1, 0x0

    # invokes: Lcom/google/android/apps/chrome/omnibox/LocationBar;->stopAutocomplete(Z)V
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->access$800(Lcom/google/android/apps/chrome/omnibox/LocationBar;Z)V

    .line 1605
    return-void
.end method

.method public onHideModal()V
    .locals 2

    .prologue
    .line 1614
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$8;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSuggestionModalShown:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->access$2602(Lcom/google/android/apps/chrome/omnibox/LocationBar;Z)Z

    .line 1615
    return-void
.end method

.method public onRefineSuggestion(Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;)V
    .locals 3

    .prologue
    .line 1583
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$8;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    const/4 v1, 0x0

    # invokes: Lcom/google/android/apps/chrome/omnibox/LocationBar;->stopAutocomplete(Z)V
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->access$800(Lcom/google/android/apps/chrome/omnibox/LocationBar;Z)V

    .line 1584
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$8;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    iget-object v0, v0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    invoke-virtual {p1}, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;->getFillIntoEdit()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->setUrl(Ljava/lang/String;Ljava/lang/String;)Z

    .line 1585
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$8;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    iget-object v0, v0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$8;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    iget-object v1, v1, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->setSelection(I)V

    .line 1586
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->omniboxRefineSuggestion()V

    .line 1587
    return-void
.end method

.method public onSelection(Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;I)V
    .locals 4

    .prologue
    .line 1573
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$8;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSuggestionSelectionInProgress:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->access$1902(Lcom/google/android/apps/chrome/omnibox/LocationBar;Z)Z

    .line 1574
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$8;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    # invokes: Lcom/google/android/apps/chrome/omnibox/LocationBar;->updateSuggestionUrlIfNeeded(Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;I)Ljava/lang/String;
    invoke-static {v0, p1, p2}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->access$2300(Lcom/google/android/apps/chrome/omnibox/LocationBar;Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;I)Ljava/lang/String;

    move-result-object v0

    .line 1575
    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$8;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    invoke-virtual {p1}, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;->getTransition()I

    move-result v2

    invoke-virtual {p1}, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;->getType()Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;

    move-result-object v3

    # invokes: Lcom/google/android/apps/chrome/omnibox/LocationBar;->loadUrlFromOmniboxMatch(Ljava/lang/String;IILorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;)V
    invoke-static {v1, v0, v2, p2, v3}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->access$2400(Lcom/google/android/apps/chrome/omnibox/LocationBar;Ljava/lang/String;IILorg/chromium/chrome/browser/omnibox/OmniboxSuggestion$Type;)V

    .line 1577
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$8;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->hideSuggestions()V

    .line 1578
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$8;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    iget-object v0, v0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    invoke-static {v0}, Lorg/chromium/ui/UiUtils;->hideKeyboard(Landroid/view/View;)Z

    .line 1579
    return-void
.end method

.method public onSetUrlToSuggestion(Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1591
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$8;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    # getter for: Lcom/google/android/apps/chrome/omnibox/LocationBar;->mIgnoreOmniboxItemSelection:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->access$1700(Lcom/google/android/apps/chrome/omnibox/LocationBar;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1595
    :goto_0
    return-void

    .line 1592
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$8;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    invoke-virtual {p1}, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;->getFillIntoEdit()Ljava/lang/String;

    move-result-object v1

    # invokes: Lcom/google/android/apps/chrome/omnibox/LocationBar;->setUrlBarText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    invoke-static {v0, v1, v2, v2}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->access$1800(Lcom/google/android/apps/chrome/omnibox/LocationBar;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    .line 1593
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$8;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    iget-object v0, v0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$8;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    iget-object v1, v1, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->setSelection(I)V

    .line 1594
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$8;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/apps/chrome/omnibox/LocationBar;->mIgnoreOmniboxItemSelection:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->access$1702(Lcom/google/android/apps/chrome/omnibox/LocationBar;Z)Z

    goto :goto_0
.end method

.method public onShowModal()V
    .locals 2

    .prologue
    .line 1609
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$8;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSuggestionModalShown:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->access$2602(Lcom/google/android/apps/chrome/omnibox/LocationBar;Z)Z

    .line 1610
    return-void
.end method

.method public onTextWidthsUpdated(FF)V
    .locals 1

    .prologue
    .line 1619
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$8;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    # getter for: Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSuggestionList:Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->access$700(Lcom/google/android/apps/chrome/omnibox/LocationBar;)Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;->updateMaxTextWidths(FF)V

    .line 1620
    return-void
.end method
