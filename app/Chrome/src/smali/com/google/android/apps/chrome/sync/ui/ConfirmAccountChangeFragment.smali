.class public Lcom/google/android/apps/chrome/sync/ui/ConfirmAccountChangeFragment;
.super Lcom/google/android/apps/chrome/preferences/ChromeBaseDialogFragment;
.source "ConfirmAccountChangeFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field private mAccountName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBaseDialogFragment;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/sync/ui/ConfirmAccountChangeFragment;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/ui/ConfirmAccountChangeFragment;->showClearSyncDataDialogFragment()V

    return-void
.end method

.method public static confirmSyncAccount(Ljava/lang/String;Landroid/app/Activity;)V
    .locals 3

    .prologue
    .line 38
    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getSyncLastAccountName()Ljava/lang/String;

    move-result-object v0

    .line 39
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 41
    invoke-static {p0, v0}, Lcom/google/android/apps/chrome/sync/ui/ConfirmAccountChangeFragment;->newInstance(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/chrome/sync/ui/ConfirmAccountChangeFragment;

    move-result-object v0

    .line 42
    invoke-virtual {p1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/sync/ui/ConfirmAccountChangeFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 50
    :cond_0
    :goto_0
    return-void

    :cond_1
    move-object v0, p1

    .line 45
    check-cast v0, Lcom/google/android/apps/chrome/sync/ui/SyncChooseAccountFragment$Listener;

    .line 46
    if-eqz v0, :cond_0

    .line 47
    invoke-interface {v0, p1, p0}, Lcom/google/android/apps/chrome/sync/ui/SyncChooseAccountFragment$Listener;->onAccountSelected(Landroid/app/Activity;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static newInstance(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/chrome/sync/ui/ConfirmAccountChangeFragment;
    .locals 3

    .prologue
    .line 54
    new-instance v0, Lcom/google/android/apps/chrome/sync/ui/ConfirmAccountChangeFragment;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/sync/ui/ConfirmAccountChangeFragment;-><init>()V

    .line 55
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 56
    const-string/jumbo v2, "lastAccountName"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    const-string/jumbo v2, "newAccountName"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/sync/ui/ConfirmAccountChangeFragment;->setArguments(Landroid/os/Bundle;)V

    .line 59
    return-object v0
.end method

.method private showClearSyncDataDialogFragment()V
    .locals 3

    .prologue
    .line 105
    new-instance v0, Lcom/google/android/apps/chrome/sync/ui/ClearSyncDataDialogFragment;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/sync/ui/ClearSyncDataDialogFragment;-><init>()V

    .line 106
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/ui/ConfirmAccountChangeFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/sync/ui/ClearSyncDataDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 108
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/ui/ConfirmAccountChangeFragment;->dismiss()V

    .line 109
    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    .prologue
    .line 96
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 97
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/ui/ConfirmAccountChangeFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/sync/ui/SyncChooseAccountFragment$Listener;

    .line 98
    if-eqz v0, :cond_0

    .line 99
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/ui/ConfirmAccountChangeFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/sync/ui/ConfirmAccountChangeFragment;->mAccountName:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/chrome/sync/ui/SyncChooseAccountFragment$Listener;->onAccountSelected(Landroid/app/Activity;Ljava/lang/String;)V

    .line 102
    :cond_0
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 9

    .prologue
    const/4 v6, 0x1

    const/4 v8, 0x0

    .line 66
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/ui/ConfirmAccountChangeFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    .line 67
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/ui/ConfirmAccountChangeFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v2, "lastAccountName"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 68
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/ui/ConfirmAccountChangeFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v3, "newAccountName"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/sync/ui/ConfirmAccountChangeFragment;->mAccountName:Ljava/lang/String;

    .line 70
    invoke-virtual {v1}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    .line 71
    sget v3, Lcom/google/android/apps/chrome/R$layout;->confirm_sync_account_change_account:I

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 72
    sget v0, Lcom/google/android/apps/chrome/R$id;->confirmMessage:I

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 73
    sget v4, Lcom/google/android/apps/chrome/R$string;->confirm_account_change_dialog_message:I

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v2, v5, v8

    iget-object v2, p0, Lcom/google/android/apps/chrome/sync/ui/ConfirmAccountChangeFragment;->mAccountName:Ljava/lang/String;

    aput-object v2, v5, v6

    invoke-virtual {v1, v4, v5}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 77
    new-array v2, v6, [Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;

    new-instance v4, Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;

    const-string/jumbo v5, "<link>"

    const-string/jumbo v6, "</link>"

    new-instance v7, Lcom/google/android/apps/chrome/sync/ui/ConfirmAccountChangeFragment$1;

    invoke-direct {v7, p0}, Lcom/google/android/apps/chrome/sync/ui/ConfirmAccountChangeFragment$1;-><init>(Lcom/google/android/apps/chrome/sync/ui/ConfirmAccountChangeFragment;)V

    invoke-direct {v4, v5, v6, v7}, Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    aput-object v4, v2, v8

    invoke-static {v1, v2}, Lcom/google/android/apps/chrome/utilities/StringLinkifier;->linkify(Ljava/lang/String;[Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;)Landroid/text/SpannableString;

    move-result-object v1

    .line 85
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 86
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 87
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/ui/ConfirmAccountChangeFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v1, Lcom/google/android/apps/chrome/R$string;->confirm_account_change_dialog_title:I

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$string;->confirm_account_change_dialog_signin:I

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$string;->cancel:I

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method
