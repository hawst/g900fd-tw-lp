.class public Lcom/google/android/apps/chrome/EmbedContentViewActivity;
.super Lcom/google/android/apps/chrome/webapps/FullScreenActivity;
.source "EmbedContentViewActivity.java"


# static fields
.field protected static final TITLE_INTENT_EXTRA:Ljava/lang/String; = "title"

.field protected static final URL_INTENT_EXTRA:Ljava/lang/String; = "url"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/google/android/apps/chrome/webapps/FullScreenActivity;-><init>()V

    return-void
.end method

.method public static show(Landroid/content/Context;II)V
    .locals 2

    .prologue
    .line 33
    if-nez p0, :cond_0

    .line 47
    :goto_0
    return-void

    .line 35
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 36
    const-class v1, Lcom/google/android/apps/chrome/EmbedContentViewActivity;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    .line 37
    instance-of v1, p0, Landroid/app/Activity;

    if-eqz v1, :cond_1

    .line 38
    const/high16 v1, 0x20020000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 44
    :goto_1
    const-string/jumbo v1, "title"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 45
    const-string/jumbo v1, "url"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 46
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 42
    :cond_1
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    goto :goto_1
.end method


# virtual methods
.method public finishNativeInitialization()V
    .locals 3

    .prologue
    const/4 v1, 0x4

    const/4 v2, 0x0

    .line 51
    invoke-super {p0}, Lcom/google/android/apps/chrome/webapps/FullScreenActivity;->finishNativeInitialization()V

    .line 52
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/EmbedContentViewActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v1, v1}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    .line 55
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/EmbedContentViewActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "title"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 56
    if-eqz v0, :cond_0

    .line 57
    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/EmbedContentViewActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/EmbedContentViewActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 60
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/EmbedContentViewActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "url"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 61
    if-eqz v0, :cond_1

    .line 62
    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/EmbedContentViewActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/EmbedContentViewActivity;->loadUrl(Ljava/lang/String;)V

    .line 64
    :cond_1
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 68
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 69
    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 70
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/EmbedContentViewActivity;->finish()V

    .line 71
    const/4 v0, 0x1

    .line 73
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/webapps/FullScreenActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method
