.class public Lcom/google/android/apps/chrome/compositor/Invalidator;
.super Ljava/lang/Object;
.source "Invalidator.java"


# instance fields
.field private mHost:Lcom/google/android/apps/chrome/compositor/Invalidator$Host;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    return-void
.end method


# virtual methods
.method public invalidate(Lcom/google/android/apps/chrome/compositor/Invalidator$Client;)V
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/Invalidator;->mHost:Lcom/google/android/apps/chrome/compositor/Invalidator$Host;

    if-eqz v0, :cond_0

    .line 51
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/Invalidator;->mHost:Lcom/google/android/apps/chrome/compositor/Invalidator$Host;

    invoke-interface {v0, p1}, Lcom/google/android/apps/chrome/compositor/Invalidator$Host;->deferInvalidate(Lcom/google/android/apps/chrome/compositor/Invalidator$Client;)V

    .line 55
    :goto_0
    return-void

    .line 53
    :cond_0
    invoke-interface {p1}, Lcom/google/android/apps/chrome/compositor/Invalidator$Client;->doInvalidate()V

    goto :goto_0
.end method

.method public set(Lcom/google/android/apps/chrome/compositor/Invalidator$Host;)V
    .locals 0

    .prologue
    .line 40
    iput-object p1, p0, Lcom/google/android/apps/chrome/compositor/Invalidator;->mHost:Lcom/google/android/apps/chrome/compositor/Invalidator$Host;

    .line 41
    return-void
.end method
