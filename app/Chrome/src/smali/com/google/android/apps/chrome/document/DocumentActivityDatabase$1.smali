.class Lcom/google/android/apps/chrome/document/DocumentActivityDatabase$1;
.super Ljava/lang/Object;
.source "DocumentActivityDatabase.java"

# interfaces
.implements Lcom/google/android/apps/chrome/document/DocumentTabList$Delegate;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/document/DocumentActivityDatabase;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/document/DocumentActivityDatabase;)V
    .locals 0

    .prologue
    .line 229
    iput-object p1, p0, Lcom/google/android/apps/chrome/document/DocumentActivityDatabase$1;->this$0:Lcom/google/android/apps/chrome/document/DocumentActivityDatabase;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private getTask(ZI)Landroid/app/ActivityManager$AppTask;
    .locals 4

    .prologue
    .line 295
    invoke-static {}, Lorg/chromium/base/ApplicationStatus;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 296
    const-string/jumbo v1, "activity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 298
    invoke-virtual {v0}, Landroid/app/ActivityManager;->getAppTasks()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager$AppTask;

    .line 299
    invoke-virtual {v0}, Landroid/app/ActivityManager$AppTask;->getTaskInfo()Landroid/app/ActivityManager$RecentTaskInfo;

    move-result-object v2

    iget-object v2, v2, Landroid/app/ActivityManager$RecentTaskInfo;->baseIntent:Landroid/content/Intent;

    .line 300
    invoke-static {v2}, Lcom/google/android/apps/chrome/IntentHandler;->getTabIdFromIntent(Landroid/content/Intent;)I

    move-result v3

    .line 301
    if-ne v3, p2, :cond_0

    invoke-virtual {p0, p1, v2}, Lcom/google/android/apps/chrome/document/DocumentActivityDatabase$1;->isValidActivity(ZLandroid/content/Intent;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 303
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public bringTabToForeground(ZI)V
    .locals 1

    .prologue
    .line 279
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/document/DocumentActivityDatabase$1;->getTask(ZI)Landroid/app/ActivityManager$AppTask;

    move-result-object v0

    .line 280
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/ActivityManager$AppTask;->moveToFront()V

    .line 281
    :cond_0
    return-void
.end method

.method public closeTab(ZI)V
    .locals 1

    .prologue
    .line 285
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/document/DocumentActivityDatabase$1;->getTask(ZI)Landroid/app/ActivityManager$AppTask;

    move-result-object v0

    .line 286
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/ActivityManager$AppTask;->finishAndRemoveTask()V

    .line 287
    :cond_0
    return-void
.end method

.method public createTab(ZJI)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 269
    new-instance v7, Lcom/google/android/apps/chrome/document/DocumentActivityDatabase$PendingDocumentData;

    invoke-direct {v7}, Lcom/google/android/apps/chrome/document/DocumentActivityDatabase$PendingDocumentData;-><init>()V

    .line 270
    iput-wide p2, v7, Lcom/google/android/apps/chrome/document/DocumentActivityDatabase$PendingDocumentData;->nativeWebContents:J

    .line 271
    const/16 v4, 0x66

    const/16 v5, 0x8

    move v1, p1

    move-object v3, v0

    move v6, v2

    invoke-static/range {v0 .. v7}, Lcom/google/android/apps/chrome/document/ChromeLauncherActivity;->launchInstance(Landroid/app/Activity;ZILjava/lang/String;IIZLcom/google/android/apps/chrome/document/DocumentActivityDatabase$PendingDocumentData;)V

    .line 275
    return-void
.end method

.method public getLastUsedTabId()I
    .locals 1

    .prologue
    .line 291
    sget v0, Lcom/google/android/apps/chrome/document/DocumentActivity;->sLastUsedTabId:I

    return v0
.end method

.method public getTasksFromRecents(Z)Landroid/util/SparseArray;
    .locals 5

    .prologue
    .line 251
    new-instance v1, Landroid/util/SparseArray;

    invoke-direct {v1}, Landroid/util/SparseArray;-><init>()V

    .line 252
    invoke-static {}, Lorg/chromium/base/ApplicationStatus;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 253
    const-string/jumbo v2, "activity"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 255
    invoke-virtual {v0}, Landroid/app/ActivityManager;->getAppTasks()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager$AppTask;

    .line 256
    invoke-virtual {v0}, Landroid/app/ActivityManager$AppTask;->getTaskInfo()Landroid/app/ActivityManager$RecentTaskInfo;

    move-result-object v0

    iget-object v0, v0, Landroid/app/ActivityManager$RecentTaskInfo;->baseIntent:Landroid/content/Intent;

    .line 257
    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/chrome/document/DocumentActivityDatabase$1;->isValidActivity(ZLandroid/content/Intent;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 259
    invoke-static {v0}, Lcom/google/android/apps/chrome/IntentHandler;->getTabIdFromIntent(Landroid/content/Intent;)I

    move-result v3

    .line 260
    invoke-static {v0}, Lcom/google/android/apps/chrome/IntentHandler;->getUrlFromIntent(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v0

    .line 261
    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    if-eqz v0, :cond_0

    .line 262
    new-instance v4, Lcom/google/android/apps/chrome/document/DocumentTabList$Entry;

    invoke-direct {v4, v3, v0}, Lcom/google/android/apps/chrome/document/DocumentTabList$Entry;-><init>(ILjava/lang/String;)V

    invoke-virtual {v1, v3, v4}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_0

    .line 264
    :cond_1
    return-object v1
.end method

.method public isValidActivity(ZLandroid/content/Intent;)Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 232
    if-nez p2, :cond_0

    move v0, v2

    .line 246
    :goto_0
    return v0

    .line 233
    :cond_0
    if-eqz p1, :cond_2

    const-class v0, Lcom/google/android/apps/chrome/document/IncognitoDocumentActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    .line 236
    :goto_1
    const/4 v1, 0x0

    .line 237
    invoke-virtual {p2}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v3

    if-nez v3, :cond_3

    .line 238
    invoke-static {}, Lorg/chromium/base/ApplicationStatus;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    .line 239
    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 240
    invoke-virtual {v3, p2, v2}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v2

    .line 241
    if-eqz v2, :cond_1

    iget-object v1, v2, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v1, v1, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    .line 246
    :cond_1
    :goto_2
    invoke-static {v1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    goto :goto_0

    .line 233
    :cond_2
    const-class v0, Lcom/google/android/apps/chrome/document/DocumentActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 243
    :cond_3
    invoke-virtual {p2}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v1

    goto :goto_2
.end method
