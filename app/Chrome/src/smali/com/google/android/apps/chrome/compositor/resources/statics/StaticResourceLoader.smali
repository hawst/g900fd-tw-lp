.class public Lcom/google/android/apps/chrome/compositor/resources/statics/StaticResourceLoader;
.super Ljava/lang/Object;
.source "StaticResourceLoader.java"


# instance fields
.field private final mCallback:Lcom/google/android/apps/chrome/compositor/resources/ResourceLoaderCallback;

.field private final mOutstandingLoads:Landroid/util/SparseArray;

.field private final mResources:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;Lcom/google/android/apps/chrome/compositor/resources/ResourceLoaderCallback;)V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/compositor/resources/statics/StaticResourceLoader;->mOutstandingLoads:Landroid/util/SparseArray;

    .line 30
    iput-object p1, p0, Lcom/google/android/apps/chrome/compositor/resources/statics/StaticResourceLoader;->mResources:Landroid/content/res/Resources;

    .line 31
    iput-object p2, p0, Lcom/google/android/apps/chrome/compositor/resources/statics/StaticResourceLoader;->mCallback:Lcom/google/android/apps/chrome/compositor/resources/ResourceLoaderCallback;

    .line 32
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/compositor/resources/statics/StaticResourceLoader;)Landroid/content/res/Resources;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/resources/statics/StaticResourceLoader;->mResources:Landroid/content/res/Resources;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/compositor/resources/statics/StaticResourceLoader;Lcom/google/android/apps/chrome/compositor/resources/statics/StaticResource;I)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/compositor/resources/statics/StaticResourceLoader;->registerResource(Lcom/google/android/apps/chrome/compositor/resources/statics/StaticResource;I)V

    return-void
.end method

.method private registerResource(Lcom/google/android/apps/chrome/compositor/resources/statics/StaticResource;I)V
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/resources/statics/StaticResourceLoader;->mCallback:Lcom/google/android/apps/chrome/compositor/resources/ResourceLoaderCallback;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/resources/statics/StaticResourceLoader;->mCallback:Lcom/google/android/apps/chrome/compositor/resources/ResourceLoaderCallback;

    invoke-interface {v0, p2, p1}, Lcom/google/android/apps/chrome/compositor/resources/ResourceLoaderCallback;->onResourceLoaded(ILcom/google/android/apps/chrome/compositor/resources/Resource;)V

    .line 92
    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/compositor/resources/statics/StaticResource;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 93
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/resources/statics/StaticResourceLoader;->mOutstandingLoads:Landroid/util/SparseArray;

    invoke-virtual {v0, p2}, Landroid/util/SparseArray;->remove(I)V

    .line 94
    return-void
.end method


# virtual methods
.method public loadResourceAsync(I)V
    .locals 3

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/resources/statics/StaticResourceLoader;->mOutstandingLoads:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 88
    :goto_0
    return-void

    .line 85
    :cond_0
    new-instance v0, Lcom/google/android/apps/chrome/compositor/resources/statics/StaticResourceLoader$AsyncLoadTask;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/chrome/compositor/resources/statics/StaticResourceLoader$AsyncLoadTask;-><init>(Lcom/google/android/apps/chrome/compositor/resources/statics/StaticResourceLoader;I)V

    .line 86
    sget-object v1, Landroid/os/AsyncTask;->SERIAL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/compositor/resources/statics/StaticResourceLoader$AsyncLoadTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 87
    iget-object v1, p0, Lcom/google/android/apps/chrome/compositor/resources/statics/StaticResourceLoader;->mOutstandingLoads:Landroid/util/SparseArray;

    invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_0
.end method

.method public loadResourceSync(I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 52
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/resources/statics/StaticResourceLoader;->mOutstandingLoads:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/compositor/resources/statics/StaticResourceLoader$AsyncLoadTask;

    .line 54
    if-eqz v0, :cond_0

    .line 55
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/compositor/resources/statics/StaticResourceLoader$AsyncLoadTask;->cancel(Z)Z

    move-result v1

    if-nez v1, :cond_0

    .line 57
    :try_start_0
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/compositor/resources/statics/StaticResourceLoader$AsyncLoadTask;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/compositor/resources/statics/StaticResource;

    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/chrome/compositor/resources/statics/StaticResourceLoader;->registerResource(Lcom/google/android/apps/chrome/compositor/resources/statics/StaticResource;I)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    .line 67
    :goto_0
    return-void

    .line 59
    :catch_0
    move-exception v0

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/resources/statics/StaticResourceLoader;->mCallback:Lcom/google/android/apps/chrome/compositor/resources/ResourceLoaderCallback;

    invoke-interface {v0, p1, v2}, Lcom/google/android/apps/chrome/compositor/resources/ResourceLoaderCallback;->onResourceLoaded(ILcom/google/android/apps/chrome/compositor/resources/Resource;)V

    goto :goto_0

    .line 61
    :catch_1
    move-exception v0

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/resources/statics/StaticResourceLoader;->mCallback:Lcom/google/android/apps/chrome/compositor/resources/ResourceLoaderCallback;

    invoke-interface {v0, p1, v2}, Lcom/google/android/apps/chrome/compositor/resources/ResourceLoaderCallback;->onResourceLoaded(ILcom/google/android/apps/chrome/compositor/resources/Resource;)V

    goto :goto_0

    .line 66
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/resources/statics/StaticResourceLoader;->mResources:Landroid/content/res/Resources;

    invoke-static {v0, p1}, Lcom/google/android/apps/chrome/compositor/resources/statics/StaticResource;->create(Landroid/content/res/Resources;I)Lcom/google/android/apps/chrome/compositor/resources/statics/StaticResource;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/chrome/compositor/resources/statics/StaticResourceLoader;->registerResource(Lcom/google/android/apps/chrome/compositor/resources/statics/StaticResource;I)V

    goto :goto_0
.end method

.method public loadResourcesAsync([I)V
    .locals 2

    .prologue
    .line 75
    const/4 v0, 0x0

    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_0

    aget v1, p1, v0

    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/compositor/resources/statics/StaticResourceLoader;->loadResourceAsync(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 76
    :cond_0
    return-void
.end method

.method public loadResourcesSync([I)V
    .locals 2

    .prologue
    .line 41
    const/4 v0, 0x0

    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_0

    aget v1, p1, v0

    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/compositor/resources/statics/StaticResourceLoader;->loadResourceSync(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 42
    :cond_0
    return-void
.end method
