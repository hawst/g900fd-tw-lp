.class public Lcom/google/android/apps/chrome/webapps/WebappUrlBar;
.super Landroid/widget/FrameLayout;
.source "WebappUrlBar.java"

# interfaces
.implements Landroid/view/View$OnLayoutChangeListener;


# instance fields
.field private mCurrentIconResource:I

.field private mCurrentlyDisplayedUrl:Ljava/lang/String;

.field private final mIconResourceWidths:Landroid/util/SparseIntArray;

.field private final mSeparator:Landroid/view/View;

.field private final mUrlBar:Landroid/widget/TextView;

.field private final mWebappInfo:Lcom/google/android/apps/chrome/webapps/WebappInfo;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/chrome/webapps/WebappInfo;Landroid/content/Context;)V
    .locals 5

    .prologue
    const/4 v3, -0x2

    const/4 v4, 0x1

    .line 59
    invoke-direct {p0, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 60
    iput-object p1, p0, Lcom/google/android/apps/chrome/webapps/WebappUrlBar;->mWebappInfo:Lcom/google/android/apps/chrome/webapps/WebappInfo;

    .line 61
    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/webapps/WebappUrlBar;->mIconResourceWidths:Landroid/util/SparseIntArray;

    .line 63
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/webapps/WebappUrlBar;->mUrlBar:Landroid/widget/TextView;

    .line 64
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/WebappUrlBar;->mUrlBar:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 65
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/WebappUrlBar;->mUrlBar:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/ScrollingMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 66
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/WebappUrlBar;->mUrlBar:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setHorizontalFadingEdgeEnabled(Z)V

    .line 67
    new-instance v0, Landroid/view/View;

    invoke-direct {v0, p2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/webapps/WebappUrlBar;->mSeparator:Landroid/view/View;

    .line 69
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/WebappUrlBar;->mUrlBar:Landroid/widget/TextView;

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    const/16 v2, 0x11

    invoke-direct {v1, v3, v3, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/chrome/webapps/WebappUrlBar;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 73
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/WebappUrlBar;->mSeparator:Landroid/view/View;

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v2, -0x1

    const/16 v3, 0x50

    invoke-direct {v1, v2, v4, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/chrome/webapps/WebappUrlBar;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 78
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/WebappUrlBar;->mSeparator:Landroid/view/View;

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/chrome/R$color;->webapp_url_bar_separator:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 80
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$color;->webapp_url_bar_bg:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/webapps/WebappUrlBar;->setBackgroundColor(I)V

    .line 83
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/WebappUrlBar;->mUrlBar:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 84
    return-void
.end method

.method private static createURI(Ljava/lang/String;)Ljava/net/URI;
    .locals 3

    .prologue
    .line 143
    const-string/jumbo v0, " "

    const-string/jumbo v1, "%20"

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 146
    :try_start_0
    invoke-static {v0}, Ljava/net/URI;->create(Ljava/lang/String;)Ljava/net/URI;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 149
    :goto_0
    return-object v0

    .line 147
    :catch_0
    move-exception v0

    .line 148
    const-string/jumbo v1, "WebappUrlBar"

    const-string/jumbo v2, "Failed to convert URI: "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 149
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private determineVisibility(Ljava/net/URI;I)Z
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 181
    .line 183
    if-eqz p1, :cond_4

    .line 184
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/webapps/WebappUrlBar;->isWebappWebsite(Ljava/net/URI;)Z

    move-result v0

    .line 185
    if-eqz v0, :cond_0

    const/4 v0, 0x5

    if-eq p2, v0, :cond_0

    const/4 v0, 0x3

    if-ne p2, v0, :cond_2

    :cond_0
    move v0, v2

    .line 188
    :goto_0
    if-eqz v0, :cond_3

    move v0, v1

    .line 191
    :goto_1
    if-nez v0, :cond_1

    move v1, v2

    :cond_1
    return v1

    :cond_2
    move v0, v1

    .line 185
    goto :goto_0

    .line 188
    :cond_3
    const/16 v0, 0x8

    goto :goto_1

    :cond_4
    move v0, v1

    goto :goto_1
.end method

.method private isWebappWebsite(Ljava/net/URI;)Z
    .locals 3

    .prologue
    .line 195
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/WebappUrlBar;->mWebappInfo:Lcom/google/android/apps/chrome/webapps/WebappInfo;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/webapps/WebappInfo;->uri()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lorg/chromium/chrome/browser/UrlUtilities;->sameDomainOrHost(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method private updateDisplayedUrl(Ljava/lang/String;Ljava/net/URI;)V
    .locals 2

    .prologue
    .line 167
    iget v0, p0, Lcom/google/android/apps/chrome/webapps/WebappUrlBar;->mCurrentIconResource:I

    if-nez v0, :cond_2

    const/4 v0, 0x1

    .line 169
    :goto_0
    if-eqz p2, :cond_0

    .line 170
    invoke-static {p2, v0}, Lorg/chromium/chrome/browser/UrlUtilities;->getOriginForDisplay(Ljava/net/URI;Z)Ljava/lang/String;

    move-result-object v0

    .line 171
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    move-object p1, v0

    .line 174
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/WebappUrlBar;->mUrlBar:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 175
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/WebappUrlBar;->mCurrentlyDisplayedUrl:Ljava/lang/String;

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 176
    iput-object p1, p0, Lcom/google/android/apps/chrome/webapps/WebappUrlBar;->mCurrentlyDisplayedUrl:Ljava/lang/String;

    .line 178
    :cond_1
    return-void

    .line 167
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private updateSecurityIcon(I)V
    .locals 4

    .prologue
    const/4 v2, -0x1

    const/4 v3, 0x0

    .line 154
    invoke-static {p1}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->getSecurityIconResource(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/webapps/WebappUrlBar;->mCurrentIconResource:I

    .line 156
    iget v0, p0, Lcom/google/android/apps/chrome/webapps/WebappUrlBar;->mCurrentIconResource:I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/WebappUrlBar;->mIconResourceWidths:Landroid/util/SparseIntArray;

    iget v1, p0, Lcom/google/android/apps/chrome/webapps/WebappUrlBar;->mCurrentIconResource:I

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->get(II)I

    move-result v0

    if-ne v0, v2, :cond_0

    .line 157
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/WebappUrlBar;->mUrlBar:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/chrome/webapps/WebappUrlBar;->mCurrentIconResource:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 159
    iget-object v1, p0, Lcom/google/android/apps/chrome/webapps/WebappUrlBar;->mIconResourceWidths:Landroid/util/SparseIntArray;

    iget v2, p0, Lcom/google/android/apps/chrome/webapps/WebappUrlBar;->mCurrentIconResource:I

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    invoke-virtual {v1, v2, v0}, Landroid/util/SparseIntArray;->put(II)V

    .line 162
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/WebappUrlBar;->mUrlBar:Landroid/widget/TextView;

    iget v1, p0, Lcom/google/android/apps/chrome/webapps/WebappUrlBar;->mCurrentIconResource:I

    invoke-static {v0, v1, v3, v3, v3}, Lorg/chromium/base/ApiCompatibilityUtils;->setCompoundDrawablesRelativeWithIntrinsicBounds(Landroid/widget/TextView;IIII)V

    .line 164
    return-void
.end method


# virtual methods
.method protected getCurrentIconResourceForTests()I
    .locals 1

    .prologue
    .line 104
    iget v0, p0, Lcom/google/android/apps/chrome/webapps/WebappUrlBar;->mCurrentIconResource:I

    return v0
.end method

.method protected getDisplayedUrlForTests()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/WebappUrlBar;->mUrlBar:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public onLayoutChange(Landroid/view/View;IIIIIIII)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 121
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/WebappUrlBar;->mUrlBar:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v2

    .line 122
    if-nez v2, :cond_0

    .line 138
    :goto_0
    return-void

    .line 127
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/WebappUrlBar;->mUrlBar:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getWidth()I

    move-result v3

    .line 128
    iget v0, p0, Lcom/google/android/apps/chrome/webapps/WebappUrlBar;->mCurrentIconResource:I

    if-nez v0, :cond_1

    move v0, v1

    .line 130
    :goto_1
    sub-int v0, v3, v0

    .line 131
    invoke-virtual {v2}, Landroid/text/Layout;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2}, Landroid/text/Layout;->getPaint()Landroid/text/TextPaint;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/text/Layout;->getDesiredWidth(Ljava/lang/CharSequence;Landroid/text/TextPaint;)F

    move-result v2

    float-to-int v2, v2

    .line 133
    if-le v2, v0, :cond_2

    .line 134
    iget-object v3, p0, Lcom/google/android/apps/chrome/webapps/WebappUrlBar;->mUrlBar:Landroid/widget/TextView;

    sub-int v0, v2, v0

    invoke-virtual {v3, v0, v1}, Landroid/widget/TextView;->scrollTo(II)V

    goto :goto_0

    .line 128
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/WebappUrlBar;->mIconResourceWidths:Landroid/util/SparseIntArray;

    iget v4, p0, Lcom/google/android/apps/chrome/webapps/WebappUrlBar;->mCurrentIconResource:I

    invoke-virtual {v0, v4}, Landroid/util/SparseIntArray;->get(I)I

    move-result v0

    goto :goto_1

    .line 136
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/WebappUrlBar;->mUrlBar:Landroid/widget/TextView;

    invoke-virtual {v0, v1, v1}, Landroid/widget/TextView;->scrollTo(II)V

    goto :goto_0
.end method

.method update(Ljava/lang/String;I)Z
    .locals 1

    .prologue
    .line 93
    invoke-static {p1}, Lcom/google/android/apps/chrome/webapps/WebappUrlBar;->createURI(Ljava/lang/String;)Ljava/net/URI;

    move-result-object v0

    .line 94
    invoke-direct {p0, p2}, Lcom/google/android/apps/chrome/webapps/WebappUrlBar;->updateSecurityIcon(I)V

    .line 95
    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/chrome/webapps/WebappUrlBar;->updateDisplayedUrl(Ljava/lang/String;Ljava/net/URI;)V

    .line 96
    invoke-direct {p0, v0, p2}, Lcom/google/android/apps/chrome/webapps/WebappUrlBar;->determineVisibility(Ljava/net/URI;I)Z

    move-result v0

    return v0
.end method
