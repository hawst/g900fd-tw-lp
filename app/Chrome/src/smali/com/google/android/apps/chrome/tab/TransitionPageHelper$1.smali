.class Lcom/google/android/apps/chrome/tab/TransitionPageHelper$1;
.super Landroid/animation/AnimatorListenerAdapter;
.source "TransitionPageHelper.java"


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/tab/TransitionPageHelper;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/tab/TransitionPageHelper;)V
    .locals 0

    .prologue
    .line 182
    iput-object p1, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper$1;->this$0:Lcom/google/android/apps/chrome/tab/TransitionPageHelper;

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2

    .prologue
    .line 185
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper$1;->this$0:Lcom/google/android/apps/chrome/tab/TransitionPageHelper;

    # getter for: Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mTransitionContentViewCore:Lorg/chromium/content/browser/ContentViewCore;
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->access$000(Lcom/google/android/apps/chrome/tab/TransitionPageHelper;)Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 186
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper$1;->this$0:Lcom/google/android/apps/chrome/tab/TransitionPageHelper;

    # getter for: Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mTransitionContentViewCore:Lorg/chromium/content/browser/ContentViewCore;
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->access$000(Lcom/google/android/apps/chrome/tab/TransitionPageHelper;)Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->onHide()V

    .line 188
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper$1;->this$0:Lcom/google/android/apps/chrome/tab/TransitionPageHelper;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->mTransitionUnloaded:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->access$102(Lcom/google/android/apps/chrome/tab/TransitionPageHelper;Z)Z

    .line 189
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/TransitionPageHelper$1;->this$0:Lcom/google/android/apps/chrome/tab/TransitionPageHelper;

    # invokes: Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->updateState()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->access$200(Lcom/google/android/apps/chrome/tab/TransitionPageHelper;)V

    .line 190
    return-void
.end method
