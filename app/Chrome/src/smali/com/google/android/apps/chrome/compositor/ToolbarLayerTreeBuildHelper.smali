.class public Lcom/google/android/apps/chrome/compositor/ToolbarLayerTreeBuildHelper;
.super Ljava/lang/Object;
.source "ToolbarLayerTreeBuildHelper.java"


# instance fields
.field private mNativeToolbarLayerTreeBuildHelper:J


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    invoke-direct {p0}, Lcom/google/android/apps/chrome/compositor/ToolbarLayerTreeBuildHelper;->nativeInit()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/chrome/compositor/ToolbarLayerTreeBuildHelper;->mNativeToolbarLayerTreeBuildHelper:J

    .line 22
    return-void
.end method

.method private native nativeInit()J
.end method

.method private native nativeUpdateToolbarLayer(JFFZ)V
.end method

.method private native nativeUpdateToolbarTexture(JLandroid/graphics/Bitmap;F)V
.end method


# virtual methods
.method public destroy()V
    .locals 2

    .prologue
    .line 33
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/apps/chrome/compositor/ToolbarLayerTreeBuildHelper;->mNativeToolbarLayerTreeBuildHelper:J

    .line 34
    return-void
.end method

.method public getNativePointer()J
    .locals 2

    .prologue
    .line 28
    iget-wide v0, p0, Lcom/google/android/apps/chrome/compositor/ToolbarLayerTreeBuildHelper;->mNativeToolbarLayerTreeBuildHelper:J

    return-wide v0
.end method

.method public updateToolbarLayer(FFZ)V
    .locals 7

    .prologue
    .line 43
    iget-wide v2, p0, Lcom/google/android/apps/chrome/compositor/ToolbarLayerTreeBuildHelper;->mNativeToolbarLayerTreeBuildHelper:J

    move-object v1, p0

    move v4, p1

    move v5, p2

    move v6, p3

    invoke-direct/range {v1 .. v6}, Lcom/google/android/apps/chrome/compositor/ToolbarLayerTreeBuildHelper;->nativeUpdateToolbarLayer(JFFZ)V

    .line 45
    return-void
.end method

.method public updateToolbarTexture(Landroid/graphics/Bitmap;F)V
    .locals 2

    .prologue
    .line 53
    iget-wide v0, p0, Lcom/google/android/apps/chrome/compositor/ToolbarLayerTreeBuildHelper;->mNativeToolbarLayerTreeBuildHelper:J

    invoke-direct {p0, v0, v1, p1, p2}, Lcom/google/android/apps/chrome/compositor/ToolbarLayerTreeBuildHelper;->nativeUpdateToolbarTexture(JLandroid/graphics/Bitmap;F)V

    .line 54
    return-void
.end method
