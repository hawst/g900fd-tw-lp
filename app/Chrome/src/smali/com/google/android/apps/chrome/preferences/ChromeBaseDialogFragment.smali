.class public Lcom/google/android/apps/chrome/preferences/ChromeBaseDialogFragment;
.super Landroid/app/DialogFragment;
.source "ChromeBaseDialogFragment.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public onDestroy()V
    .locals 2

    .prologue
    .line 17
    invoke-super {p0}, Landroid/app/DialogFragment;->onDestroy()V

    .line 18
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBaseDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    .line 19
    instance-of v0, v1, Lcom/google/android/apps/chrome/preferences/Preferences;

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 20
    check-cast v0, Lcom/google/android/apps/chrome/preferences/Preferences;

    .line 21
    invoke-virtual {v0, p0}, Lcom/google/android/apps/chrome/preferences/Preferences;->popFragmentList(Landroid/app/Fragment;)V

    .line 22
    invoke-virtual {v1}, Landroid/app/Activity;->invalidateOptionsMenu()V

    .line 24
    :cond_0
    return-void
.end method
