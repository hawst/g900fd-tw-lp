.class public Lcom/google/android/apps/chrome/videofling/ChromeMediaRouteDialogFactory;
.super Landroid/support/v7/app/B;
.source "ChromeMediaRouteDialogFactory.java"


# instance fields
.field private final mDisconnectListener:Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog$DisconnectListener;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog$DisconnectListener;)V
    .locals 0

    .prologue
    .line 62
    invoke-direct {p0}, Landroid/support/v7/app/B;-><init>()V

    .line 63
    iput-object p1, p0, Lcom/google/android/apps/chrome/videofling/ChromeMediaRouteDialogFactory;->mDisconnectListener:Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog$DisconnectListener;

    .line 64
    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/videofling/ChromeMediaRouteDialogFactory;)Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog$DisconnectListener;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/ChromeMediaRouteDialogFactory;->mDisconnectListener:Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog$DisconnectListener;

    return-object v0
.end method


# virtual methods
.method public onCreateChooserDialogFragment()Landroid/support/v7/app/u;
    .locals 1

    .prologue
    .line 91
    new-instance v0, Lcom/google/android/apps/chrome/videofling/ChromeMediaRouteDialogFactory$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/videofling/ChromeMediaRouteDialogFactory$2;-><init>(Lcom/google/android/apps/chrome/videofling/ChromeMediaRouteDialogFactory;)V

    return-object v0
.end method

.method public onCreateControllerDialogFragment()Landroid/support/v7/app/A;
    .locals 1

    .prologue
    .line 68
    new-instance v0, Lcom/google/android/apps/chrome/videofling/ChromeMediaRouteDialogFactory$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/videofling/ChromeMediaRouteDialogFactory$1;-><init>(Lcom/google/android/apps/chrome/videofling/ChromeMediaRouteDialogFactory;)V

    return-object v0
.end method
