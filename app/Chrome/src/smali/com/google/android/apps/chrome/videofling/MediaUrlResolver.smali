.class public Lcom/google/android/apps/chrome/videofling/MediaUrlResolver;
.super Landroid/os/AsyncTask;
.source "MediaUrlResolver.java"


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mDelegate:Lcom/google/android/apps/chrome/videofling/MediaUrlResolver$Delegate;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/chrome/videofling/MediaUrlResolver$Delegate;)V
    .locals 0

    .prologue
    .line 91
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 92
    iput-object p1, p0, Lcom/google/android/apps/chrome/videofling/MediaUrlResolver;->mContext:Landroid/content/Context;

    .line 93
    iput-object p2, p0, Lcom/google/android/apps/chrome/videofling/MediaUrlResolver;->mDelegate:Lcom/google/android/apps/chrome/videofling/MediaUrlResolver$Delegate;

    .line 94
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Lcom/google/android/apps/chrome/videofling/MediaUrlResolver$Result;
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 98
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaUrlResolver;->mDelegate:Lcom/google/android/apps/chrome/videofling/MediaUrlResolver$Delegate;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/videofling/MediaUrlResolver$Delegate;->getUri()Landroid/net/Uri;

    move-result-object v0

    .line 99
    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    .line 101
    iget-object v2, p0, Lcom/google/android/apps/chrome/videofling/MediaUrlResolver;->mDelegate:Lcom/google/android/apps/chrome/videofling/MediaUrlResolver$Delegate;

    invoke-interface {v2}, Lcom/google/android/apps/chrome/videofling/MediaUrlResolver$Delegate;->getCookies()Ljava/lang/String;

    move-result-object v2

    .line 102
    iget-object v3, p0, Lcom/google/android/apps/chrome/videofling/MediaUrlResolver;->mDelegate:Lcom/google/android/apps/chrome/videofling/MediaUrlResolver$Delegate;

    invoke-interface {v3}, Lcom/google/android/apps/chrome/videofling/MediaUrlResolver$Delegate;->getUserAgent()Ljava/lang/String;

    move-result-object v3

    .line 105
    invoke-static {v0}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/apps/chrome/videofling/RemoteMediaUtils;->sanitizeUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 111
    const-string/jumbo v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 114
    :try_start_0
    new-instance v0, Ljava/net/URL;

    invoke-direct {v0, v4}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 115
    invoke-virtual {v0}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    check-cast v0, Ljava/net/HttpURLConnection;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 116
    :try_start_1
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 117
    const-string/jumbo v4, "Cookies"

    invoke-virtual {v0, v4, v2}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    :cond_0
    const-string/jumbo v2, "User-Agent"

    invoke-virtual {v0, v2, v3}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    const-string/jumbo v2, "Range"

    const-string/jumbo v3, "bytes: 0-65536"

    invoke-virtual {v0, v2, v3}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getHeaderFields()Ljava/util/Map;

    .line 125
    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getURL()Ljava/net/URL;

    move-result-object v2

    invoke-virtual {v2}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v2

    .line 126
    const-string/jumbo v3, "Access-Control-Allow-Origin"

    invoke-virtual {v0, v3}, Ljava/net/HttpURLConnection;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 127
    if-eqz v3, :cond_1

    .line 128
    const/4 v4, 0x1

    new-array v1, v4, [Lorg/apache/http/Header;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 129
    const/4 v4, 0x0

    :try_start_2
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string/jumbo v6, "Access-Control-Allow-Origin"

    invoke-direct {v5, v6, v3}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v5, v1, v4
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    :cond_1
    move-object v7, v0

    move-object v0, v2

    move-object v2, v7

    .line 135
    :goto_0
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 137
    :cond_2
    new-instance v2, Lcom/google/android/apps/chrome/videofling/MediaUrlResolver$Result;

    invoke-direct {v2, v0, v1}, Lcom/google/android/apps/chrome/videofling/MediaUrlResolver$Result;-><init>(Ljava/lang/String;[Lorg/apache/http/Header;)V

    return-object v2

    .line 131
    :catch_0
    move-exception v0

    move-object v2, v1

    .line 132
    :goto_1
    const-string/jumbo v3, "MediaUrlResolver"

    const-string/jumbo v4, "Failed to fetch the final URI"

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 133
    const-string/jumbo v0, ""

    move-object v7, v1

    move-object v1, v2

    move-object v2, v7

    goto :goto_0

    .line 131
    :catch_1
    move-exception v2

    move-object v7, v2

    move-object v2, v1

    move-object v1, v0

    move-object v0, v7

    goto :goto_1

    :catch_2
    move-exception v2

    move-object v7, v2

    move-object v2, v1

    move-object v1, v0

    move-object v0, v7

    goto :goto_1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 25
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/videofling/MediaUrlResolver;->doInBackground([Ljava/lang/Void;)Lcom/google/android/apps/chrome/videofling/MediaUrlResolver$Result;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Lcom/google/android/apps/chrome/videofling/MediaUrlResolver$Result;)V
    .locals 3

    .prologue
    .line 142
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/videofling/MediaUrlResolver$Result;->getUri()Ljava/lang/String;

    move-result-object v0

    .line 143
    const-string/jumbo v1, ""

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v0, Landroid/net/Uri;->EMPTY:Landroid/net/Uri;

    .line 144
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/videofling/MediaUrlResolver;->mDelegate:Lcom/google/android/apps/chrome/videofling/MediaUrlResolver$Delegate;

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/videofling/MediaUrlResolver$Result;->getRelevantHeaders()[Lorg/apache/http/Header;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/google/android/apps/chrome/videofling/MediaUrlResolver$Delegate;->setUri(Landroid/net/Uri;[Lorg/apache/http/Header;)V

    .line 145
    return-void

    .line 143
    :cond_0
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 25
    check-cast p1, Lcom/google/android/apps/chrome/videofling/MediaUrlResolver$Result;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/videofling/MediaUrlResolver;->onPostExecute(Lcom/google/android/apps/chrome/videofling/MediaUrlResolver$Result;)V

    return-void
.end method
