.class public Lcom/google/android/apps/chrome/prerender/PrerenderAPITestActivity;
.super Landroid/app/Activity;
.source "PrerenderAPITestActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field static final KEY_PREPRENDERED_URL:Ljava/lang/String; = "url_to_preprender"

.field static final MSG_PRERENDER_URL:I = 0x1


# instance fields
.field private final mConnection:Landroid/content/ServiceConnection;

.field mService:Landroid/os/Messenger;

.field private mUrl:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 36
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/prerender/PrerenderAPITestActivity;->mService:Landroid/os/Messenger;

    .line 38
    new-instance v0, Lcom/google/android/apps/chrome/prerender/PrerenderAPITestActivity$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/prerender/PrerenderAPITestActivity$1;-><init>(Lcom/google/android/apps/chrome/prerender/PrerenderAPITestActivity;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/prerender/PrerenderAPITestActivity;->mConnection:Landroid/content/ServiceConnection;

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 96
    sget v0, Lcom/google/android/apps/chrome/R$id;->url_to_load:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/prerender/PrerenderAPITestActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/prerender/PrerenderAPITestActivity;->mUrl:Ljava/lang/String;

    .line 97
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, Lcom/google/android/apps/chrome/R$id;->preload_button:I

    if-ne v0, v1, :cond_1

    .line 98
    iget-object v0, p0, Lcom/google/android/apps/chrome/prerender/PrerenderAPITestActivity;->mUrl:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/prerender/PrerenderAPITestActivity;->prerenderUrl(Ljava/lang/String;)V

    .line 105
    :cond_0
    :goto_0
    return-void

    .line 99
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, Lcom/google/android/apps/chrome/R$id;->load_button:I

    if-ne v0, v1, :cond_0

    .line 100
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.action.VIEW"

    iget-object v2, p0, Lcom/google/android/apps/chrome/prerender/PrerenderAPITestActivity;->mUrl:Ljava/lang/String;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 101
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/prerender/PrerenderAPITestActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 102
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 103
    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/prerender/PrerenderAPITestActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 52
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 53
    sget v0, Lcom/google/android/apps/chrome/R$layout;->prerender_test_main:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/prerender/PrerenderAPITestActivity;->setContentView(I)V

    .line 54
    sget v0, Lcom/google/android/apps/chrome/R$id;->preload_button:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/prerender/PrerenderAPITestActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 55
    sget v0, Lcom/google/android/apps/chrome/R$id;->load_button:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/prerender/PrerenderAPITestActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 56
    sget v0, Lcom/google/android/apps/chrome/R$id;->url_to_load:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/prerender/PrerenderAPITestActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    const-string/jumbo v1, "http://www.nytimes.com"

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 58
    const-string/jumbo v0, "http://www.nytimes.com"

    iput-object v0, p0, Lcom/google/android/apps/chrome/prerender/PrerenderAPITestActivity;->mUrl:Ljava/lang/String;

    .line 59
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 72
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 73
    iget-object v0, p0, Lcom/google/android/apps/chrome/prerender/PrerenderAPITestActivity;->mService:Landroid/os/Messenger;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/prerender/PrerenderAPITestActivity;->mConnection:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/prerender/PrerenderAPITestActivity;->unbindService(Landroid/content/ServiceConnection;)V

    .line 74
    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 63
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 64
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 65
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/prerender/PrerenderAPITestActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const-class v2, Lcom/google/android/apps/chrome/ChromePrerenderService;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 67
    iget-object v1, p0, Lcom/google/android/apps/chrome/prerender/PrerenderAPITestActivity;->mConnection:Landroid/content/ServiceConnection;

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/apps/chrome/prerender/PrerenderAPITestActivity;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 68
    return-void
.end method

.method public prerenderUrl(Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 81
    iget-object v0, p0, Lcom/google/android/apps/chrome/prerender/PrerenderAPITestActivity;->mService:Landroid/os/Messenger;

    if-nez v0, :cond_0

    .line 92
    :goto_0
    return-void

    .line 82
    :cond_0
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-static {v0, v1, v2, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v0

    .line 84
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 85
    const-string/jumbo v2, "url_to_preprender"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 88
    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/prerender/PrerenderAPITestActivity;->mService:Landroid/os/Messenger;

    invoke-virtual {v1, v0}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 89
    :catch_0
    move-exception v0

    .line 90
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method
