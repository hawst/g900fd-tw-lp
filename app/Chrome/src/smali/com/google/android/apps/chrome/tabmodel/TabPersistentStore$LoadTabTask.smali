.class Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$LoadTabTask;
.super Landroid/os/AsyncTask;
.source "TabPersistentStore.java"


# instance fields
.field public final mTabToRestore:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$TabRestoreDetails;

.field final synthetic this$0:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$TabRestoreDetails;)V
    .locals 0

    .prologue
    .line 766
    iput-object p1, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$LoadTabTask;->this$0:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 767
    iput-object p2, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$LoadTabTask;->mTabToRestore:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$TabRestoreDetails;

    .line 768
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 772
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$LoadTabTask;->this$0:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;

    # getter for: Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mDestroyed:Z
    invoke-static {v1}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->access$200(Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$LoadTabTask;->isCancelled()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 780
    :cond_0
    :goto_0
    return-object v0

    .line 774
    :cond_1
    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$LoadTabTask;->this$0:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$LoadTabTask;->mTabToRestore:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$TabRestoreDetails;

    iget v2, v2, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$TabRestoreDetails;->id:I

    # invokes: Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->readTabState(I)Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;
    invoke-static {v1, v2}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->access$1200(Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;I)Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 778
    :catch_0
    move-exception v1

    .line 779
    const-string/jumbo v2, "TabPersistentStore"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Unable to read state: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 777
    :catch_1
    move-exception v1

    goto :goto_0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 762
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$LoadTabTask;->doInBackground([Ljava/lang/Void;)Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;)V
    .locals 3

    .prologue
    .line 786
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$LoadTabTask;->this$0:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;

    # getter for: Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mDestroyed:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->access$200(Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$LoadTabTask;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 793
    :cond_0
    :goto_0
    return-void

    .line 788
    :cond_1
    if-eqz p1, :cond_4

    iget-boolean v0, p1, Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;->isIncognito:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$LoadTabTask;->this$0:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;

    # getter for: Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mCancelIncognitoTabLoads:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->access$1300(Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    iget-boolean v0, p1, Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;->isIncognito:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$LoadTabTask;->this$0:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;

    # getter for: Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mCancelNormalTabLoads:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->access$1400(Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 790
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$LoadTabTask;->this$0:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$LoadTabTask;->mTabToRestore:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$TabRestoreDetails;

    const/4 v2, 0x0

    # invokes: Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->restoreTab(Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$TabRestoreDetails;Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;Z)V
    invoke-static {v0, v1, p1, v2}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->access$1500(Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$TabRestoreDetails;Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;Z)V

    .line 792
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$LoadTabTask;->this$0:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;

    # invokes: Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->loadNextTab()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->access$1600(Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;)V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 762
    check-cast p1, Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$LoadTabTask;->onPostExecute(Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;)V

    return-void
.end method
