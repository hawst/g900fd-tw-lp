.class Lcom/google/android/apps/chrome/preferences/ProtectedContentPreferences$2;
.super Ljava/lang/Object;
.source "ProtectedContentPreferences.java"

# interfaces
.implements Lorg/chromium/content/browser/MediaDrmCredentialManager$MediaDrmCredentialManagerCallback;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/preferences/ProtectedContentPreferences;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/preferences/ProtectedContentPreferences;)V
    .locals 0

    .prologue
    .line 104
    iput-object p1, p0, Lcom/google/android/apps/chrome/preferences/ProtectedContentPreferences$2;->this$0:Lcom/google/android/apps/chrome/preferences/ProtectedContentPreferences;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCredentialResetFinished(Z)V
    .locals 3

    .prologue
    .line 107
    if-nez p1, :cond_0

    .line 108
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ProtectedContentPreferences$2;->this$0:Lcom/google/android/apps/chrome/preferences/ProtectedContentPreferences;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ProtectedContentPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/ProtectedContentPreferences$2;->this$0:Lcom/google/android/apps/chrome/preferences/ProtectedContentPreferences;

    sget v2, Lcom/google/android/apps/chrome/R$string;->protected_content_prefs_reset_fail_toast_description:I

    invoke-virtual {v1, v2}, Lcom/google/android/apps/chrome/preferences/ProtectedContentPreferences;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 114
    :cond_0
    return-void
.end method
