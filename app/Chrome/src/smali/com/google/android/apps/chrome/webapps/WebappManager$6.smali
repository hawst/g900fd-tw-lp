.class final Lcom/google/android/apps/chrome/webapps/WebappManager$6;
.super Ljava/lang/Object;
.source "WebappManager.java"

# interfaces
.implements Landroid/content/DialogInterface$OnShowListener;


# instance fields
.field final synthetic val$dialog:Landroid/app/AlertDialog;

.field final synthetic val$shortcutHelper:Lorg/chromium/chrome/browser/ShortcutHelper;


# direct methods
.method constructor <init>(Lorg/chromium/chrome/browser/ShortcutHelper;Landroid/app/AlertDialog;)V
    .locals 0

    .prologue
    .line 223
    iput-object p1, p0, Lcom/google/android/apps/chrome/webapps/WebappManager$6;->val$shortcutHelper:Lorg/chromium/chrome/browser/ShortcutHelper;

    iput-object p2, p0, Lcom/google/android/apps/chrome/webapps/WebappManager$6;->val$dialog:Landroid/app/AlertDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onShow(Landroid/content/DialogInterface;)V
    .locals 2

    .prologue
    .line 226
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/WebappManager$6;->val$shortcutHelper:Lorg/chromium/chrome/browser/ShortcutHelper;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/ShortcutHelper;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 227
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/WebappManager$6;->val$dialog:Landroid/app/AlertDialog;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 228
    :cond_0
    return-void
.end method
