.class public Lcom/google/android/apps/chrome/widget/ControlContainer$ControlContainerResourceAdapter;
.super Lcom/google/android/apps/chrome/compositor/resources/dynamics/ViewResourceAdapter;
.source "ControlContainer.java"


# instance fields
.field private final mToolbarActualHeightPx:I

.field final synthetic this$0:Lcom/google/android/apps/chrome/widget/ControlContainer;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/chrome/widget/ControlContainer;)V
    .locals 2

    .prologue
    .line 106
    iput-object p1, p0, Lcom/google/android/apps/chrome/widget/ControlContainer$ControlContainerResourceAdapter;->this$0:Lcom/google/android/apps/chrome/widget/ControlContainer;

    .line 107
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/compositor/resources/dynamics/ViewResourceAdapter;-><init>(Landroid/view/View;)V

    .line 109
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/widget/ControlContainer;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$dimen;->control_container_height:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/widget/ControlContainer$ControlContainerResourceAdapter;->mToolbarActualHeightPx:I

    .line 111
    return-void
.end method


# virtual methods
.method protected computeContentPadding(Landroid/graphics/Rect;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 127
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/ControlContainer$ControlContainerResourceAdapter;->this$0:Lcom/google/android/apps/chrome/widget/ControlContainer;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/ControlContainer;->getWidth()I

    move-result v0

    iget v1, p0, Lcom/google/android/apps/chrome/widget/ControlContainer$ControlContainerResourceAdapter;->mToolbarActualHeightPx:I

    invoke-virtual {p1, v2, v2, v0, v1}, Landroid/graphics/Rect;->set(IIII)V

    .line 128
    return-void
.end method

.method protected onCaptureStart(Landroid/graphics/Canvas;Landroid/graphics/Rect;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 117
    iget v0, p0, Lcom/google/android/apps/chrome/widget/ControlContainer$ControlContainerResourceAdapter;->mToolbarActualHeightPx:I

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/ControlContainer$ControlContainerResourceAdapter;->this$0:Lcom/google/android/apps/chrome/widget/ControlContainer;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/widget/ControlContainer;->getWidth()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/ControlContainer$ControlContainerResourceAdapter;->this$0:Lcom/google/android/apps/chrome/widget/ControlContainer;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/widget/ControlContainer;->getHeight()I

    move-result v2

    invoke-virtual {p2, v3, v0, v1, v2}, Landroid/graphics/Rect;->intersects(IIII)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 118
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 119
    iget v0, p0, Lcom/google/android/apps/chrome/widget/ControlContainer$ControlContainerResourceAdapter;->mToolbarActualHeightPx:I

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/ControlContainer$ControlContainerResourceAdapter;->this$0:Lcom/google/android/apps/chrome/widget/ControlContainer;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/widget/ControlContainer;->getWidth()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/ControlContainer$ControlContainerResourceAdapter;->this$0:Lcom/google/android/apps/chrome/widget/ControlContainer;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/widget/ControlContainer;->getHeight()I

    move-result v2

    invoke-virtual {p1, v3, v0, v1, v2}, Landroid/graphics/Canvas;->clipRect(IIII)Z

    .line 120
    sget-object v0, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {p1, v3, v0}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    .line 121
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 123
    :cond_0
    return-void
.end method
