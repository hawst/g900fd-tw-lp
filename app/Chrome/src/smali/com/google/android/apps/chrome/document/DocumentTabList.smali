.class public Lcom/google/android/apps/chrome/document/DocumentTabList;
.super Ljava/lang/Object;
.source "DocumentTabList.java"

# interfaces
.implements Lorg/chromium/chrome/browser/tabmodel/TabList;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final STATE_CLEAN_UP_OBSOLETE_TABS:I = 0x9

.field public static final STATE_DESERIALIZE_END:I = 0x6

.field public static final STATE_DESERIALIZE_START:I = 0x5

.field public static final STATE_DETERMINE_HISTORICAL_TABS_END:I = 0x8

.field public static final STATE_DETERMINE_HISTORICAL_TABS_START:I = 0x7

.field public static final STATE_FULLY_LOADED:I = 0xa

.field public static final STATE_LOAD_TAB_STATE_END:I = 0x4

.field public static final STATE_LOAD_TAB_STATE_START:I = 0x3

.field public static final STATE_READ_RECENT_TASKS_END:I = 0x2

.field public static final STATE_READ_RECENT_TASKS_START:I = 0x1

.field public static final STATE_UNINITIALIZED:I


# instance fields
.field private mCurrentState:I

.field private final mDelegate:Lcom/google/android/apps/chrome/document/DocumentTabList$Delegate;

.field private final mEntryMap:Landroid/util/SparseArray;

.field private final mFilename:Ljava/lang/String;

.field private final mHistoricalTabs:Ljava/util/List;

.field private final mIsIncognito:Z

.field private mNativePointer:J

.field private final mObservers:Lorg/chromium/base/ObserverList;

.field private final mPrioritizedTabId:I

.field private mSessionId:I

.field private final mTabIdList:Ljava/util/ArrayList;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 48
    const-class v0, Lcom/google/android/apps/chrome/document/DocumentTabList;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/document/DocumentTabList;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lcom/google/android/apps/chrome/document/DocumentTabList$Delegate;ZI)V
    .locals 1

    .prologue
    .line 238
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 239
    iput-object p1, p0, Lcom/google/android/apps/chrome/document/DocumentTabList;->mDelegate:Lcom/google/android/apps/chrome/document/DocumentTabList$Delegate;

    .line 240
    iput-boolean p2, p0, Lcom/google/android/apps/chrome/document/DocumentTabList;->mIsIncognito:Z

    .line 241
    iput p3, p0, Lcom/google/android/apps/chrome/document/DocumentTabList;->mPrioritizedTabId:I

    .line 242
    if-eqz p2, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentTabList;->mFilename:Ljava/lang/String;

    .line 244
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/chrome/document/DocumentTabList;->mCurrentState:I

    .line 245
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentTabList;->mTabIdList:Ljava/util/ArrayList;

    .line 246
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentTabList;->mEntryMap:Landroid/util/SparseArray;

    .line 247
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentTabList;->mHistoricalTabs:Ljava/util/List;

    .line 248
    new-instance v0, Lorg/chromium/base/ObserverList;

    invoke-direct {v0}, Lorg/chromium/base/ObserverList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentTabList;->mObservers:Lorg/chromium/base/ObserverList;

    .line 250
    invoke-direct {p0}, Lcom/google/android/apps/chrome/document/DocumentTabList;->initializeTabList()V

    .line 251
    return-void

    .line 242
    :cond_0
    const-string/jumbo v0, "chrome_document_activity.store"

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/document/DocumentTabList;)Lorg/chromium/base/ObserverList;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentTabList;->mObservers:Lorg/chromium/base/ObserverList;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/document/DocumentTabList;I)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/document/DocumentTabList;->setCurrentState(I)V

    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/apps/chrome/document/DocumentTabList;)Ljava/util/List;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentTabList;->mHistoricalTabs:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/google/android/apps/chrome/document/DocumentTabList;)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/google/android/apps/chrome/document/DocumentTabList;->cleanUpObsoleteTabStatesAsync()V

    return-void
.end method

.method static synthetic access$1200(Lcom/google/android/apps/chrome/document/DocumentTabList;)Lcom/google/android/apps/chrome/document/DocumentTabList$Delegate;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentTabList;->mDelegate:Lcom/google/android/apps/chrome/document/DocumentTabList$Delegate;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/google/android/apps/chrome/document/DocumentTabList;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentTabList;->mFilename:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/google/android/apps/chrome/document/DocumentTabList;ILcom/google/android/apps/chrome/tab/ChromeTab$TabState;)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/document/DocumentTabList;->saveTabState(ILcom/google/android/apps/chrome/tab/ChromeTab$TabState;)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/document/DocumentTabList;)I
    .locals 1

    .prologue
    .line 48
    iget v0, p0, Lcom/google/android/apps/chrome/document/DocumentTabList;->mPrioritizedTabId:I

    return v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/chrome/document/DocumentTabList;I)Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/document/DocumentTabList;->restoreTabState(I)Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/chrome/document/DocumentTabList;)Landroid/util/SparseArray;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentTabList;->mEntryMap:Landroid/util/SparseArray;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/chrome/document/DocumentTabList;)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/google/android/apps/chrome/document/DocumentTabList;->deserializeTabStatesAsync()V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/apps/chrome/document/DocumentTabList;)J
    .locals 2

    .prologue
    .line 48
    iget-wide v0, p0, Lcom/google/android/apps/chrome/document/DocumentTabList;->mNativePointer:J

    return-wide v0
.end method

.method static synthetic access$700(Lcom/google/android/apps/chrome/document/DocumentTabList;J)V
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/document/DocumentTabList;->nativeBroadcastSessionRestoreComplete(J)V

    return-void
.end method

.method static synthetic access$800(Lcom/google/android/apps/chrome/document/DocumentTabList;)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/google/android/apps/chrome/document/DocumentTabList;->loadHistoricalTabsAsync()V

    return-void
.end method

.method static synthetic access$900(Lcom/google/android/apps/chrome/document/DocumentTabList;I)Ljava/io/File;
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/document/DocumentTabList;->getTabFile(I)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method private addTabId(I)V
    .locals 1

    .prologue
    .line 389
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentTabList;->mTabIdList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/chrome/document/DocumentTabList;->addTabId(II)V

    .line 390
    return-void
.end method

.method private addTabId(II)V
    .locals 2

    .prologue
    .line 398
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentTabList;->mTabIdList:Ljava/util/ArrayList;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 401
    :goto_0
    return-void

    .line 400
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentTabList;->mTabIdList:Ljava/util/ArrayList;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    goto :goto_0
.end method

.method private cleanUpObsoleteTabStatesAsync()V
    .locals 3

    .prologue
    .line 808
    new-instance v0, Lcom/google/android/apps/chrome/document/DocumentTabList$4;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/document/DocumentTabList$4;-><init>(Lcom/google/android/apps/chrome/document/DocumentTabList;)V

    sget-object v1, Landroid/os/AsyncTask;->SERIAL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/document/DocumentTabList$4;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 841
    return-void
.end method

.method private createTabWithNativeContents(ZJI)V
    .locals 2

    .prologue
    .line 376
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentTabList;->mDelegate:Lcom/google/android/apps/chrome/document/DocumentTabList$Delegate;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/google/android/apps/chrome/document/DocumentTabList$Delegate;->createTab(ZJI)V

    .line 377
    return-void
.end method

.method private deserializeTabStatesAsync()V
    .locals 3

    .prologue
    .line 708
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentTabList;->shouldStartDeserialization()Z

    move-result v0

    if-nez v0, :cond_0

    .line 748
    :goto_0
    return-void

    .line 710
    :cond_0
    new-instance v0, Lcom/google/android/apps/chrome/document/DocumentTabList$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/document/DocumentTabList$2;-><init>(Lcom/google/android/apps/chrome/document/DocumentTabList;)V

    sget-object v1, Landroid/os/AsyncTask;->SERIAL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/document/DocumentTabList$2;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method private getFrozenTabWithId(I)Lcom/google/android/apps/chrome/tab/ChromeTab;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 530
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentTabList;->mEntryMap:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/google/android/apps/chrome/document/DocumentTabList$Entry;

    .line 531
    if-eqz v6, :cond_0

    iget-object v0, v6, Lcom/google/android/apps/chrome/document/DocumentTabList$Entry;->tabState:Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;

    if-nez v0, :cond_1

    .line 539
    :cond_0
    :goto_0
    return-object v1

    .line 533
    :cond_1
    iget-object v0, v6, Lcom/google/android/apps/chrome/document/DocumentTabList$Entry;->frozenTab:Lcom/google/android/apps/chrome/tab/ChromeTab;

    if-nez v0, :cond_2

    .line 534
    iget v0, v6, Lcom/google/android/apps/chrome/document/DocumentTabList$Entry;->tabId:I

    const/4 v2, 0x0

    const/4 v4, -0x1

    iget-object v5, v6, Lcom/google/android/apps/chrome/document/DocumentTabList$Entry;->tabState:Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;

    move-object v3, v1

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/chrome/tab/ChromeTab;->createFrozenTabFromState(ILcom/google/android/apps/chrome/ChromeActivity;ZLorg/chromium/ui/base/WindowAndroid;ILcom/google/android/apps/chrome/tab/ChromeTab$TabState;)Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    iput-object v0, v6, Lcom/google/android/apps/chrome/document/DocumentTabList$Entry;->frozenTab:Lcom/google/android/apps/chrome/tab/ChromeTab;

    .line 536
    iget-object v0, v6, Lcom/google/android/apps/chrome/document/DocumentTabList$Entry;->frozenTab:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->initialize()V

    .line 539
    :cond_2
    iget-object v1, v6, Lcom/google/android/apps/chrome/document/DocumentTabList$Entry;->frozenTab:Lcom/google/android/apps/chrome/tab/ChromeTab;

    goto :goto_0
.end method

.method public static getStateDirectory()Ljava/io/File;
    .locals 3

    .prologue
    .line 257
    invoke-static {}, Lorg/chromium/base/ApplicationStatus;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v1, "ChromeDocumentActivity"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method private getTabFile(I)Ljava/io/File;
    .locals 3

    .prologue
    .line 981
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentTabList;->isIncognito()Z

    move-result v0

    invoke-static {p1, v0}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->getTabStateFilename(IZ)Ljava/lang/String;

    move-result-object v0

    .line 982
    new-instance v1, Ljava/io/File;

    invoke-static {}, Lcom/google/android/apps/chrome/document/DocumentTabList;->getStateDirectory()Ljava/io/File;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v1
.end method

.method private initializeTabList()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, -0x1

    const/4 v2, 0x0

    .line 603
    invoke-direct {p0, v8}, Lcom/google/android/apps/chrome/document/DocumentTabList;->setCurrentState(I)V

    .line 606
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentTabList;->mDelegate:Lcom/google/android/apps/chrome/document/DocumentTabList$Delegate;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentTabList;->isIncognito()Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/chrome/document/DocumentTabList$Delegate;->getTasksFromRecents(Z)Landroid/util/SparseArray;

    move-result-object v1

    move v0, v2

    .line 607
    :goto_0
    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 608
    iget-object v3, p0, Lcom/google/android/apps/chrome/document/DocumentTabList;->mEntryMap:Landroid/util/SparseArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v4

    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 607
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 612
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentTabList;->isIncognito()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentTabList;->mFilename:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 613
    const/4 v0, 0x0

    .line 615
    :try_start_0
    invoke-static {}, Lorg/chromium/base/ApplicationStatus;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/apps/chrome/document/DocumentTabList;->mFilename:Ljava/lang/String;

    invoke-virtual {v1, v3}, Landroid/content/Context;->openFileInput(Ljava/lang/String;)Ljava/io/FileInputStream;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    .line 616
    :try_start_1
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 619
    const/16 v1, 0x1000

    new-array v1, v1, [B

    .line 621
    :goto_1
    invoke-virtual {v3, v1}, Ljava/io/FileInputStream;->read([B)I

    move-result v4

    if-eq v4, v7, :cond_3

    .line 622
    const/4 v5, 0x0

    invoke-virtual {v0, v1, v5, v4}, Ljava/io/ByteArrayOutputStream;->write([BII)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_1

    .line 641
    :catch_0
    move-exception v0

    move-object v0, v3

    :goto_2
    :try_start_2
    const-string/jumbo v1, "DocumentTabList"

    const-string/jumbo v3, "Database file not found."

    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 645
    invoke-static {v0}, Lorg/chromium/chrome/browser/util/StreamUtil;->closeQuietly(Ljava/io/Closeable;)V

    .line 650
    :cond_1
    :goto_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentTabList;->mEntryMap:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v2, v0, :cond_6

    .line 651
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentTabList;->mEntryMap:Landroid/util/SparseArray;

    invoke-virtual {v0, v2}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v0

    .line 652
    iget-object v1, p0, Lcom/google/android/apps/chrome/document/DocumentTabList;->mTabIdList:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 653
    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/document/DocumentTabList;->addTabId(I)V

    .line 650
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 624
    :cond_3
    :try_start_3
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    .line 626
    new-instance v1, Lcom/google/android/apps/chrome/document/DocumentDatabase$DocumentList;

    invoke-direct {v1}, Lcom/google/android/apps/chrome/document/DocumentDatabase$DocumentList;-><init>()V

    invoke-static {v1, v0}, Lcom/google/protobuf/nano/MessageNano;->mergeFrom(Lcom/google/protobuf/nano/MessageNano;[B)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/document/DocumentDatabase$DocumentList;

    move v4, v2

    .line 628
    :goto_4
    iget-object v1, v0, Lcom/google/android/apps/chrome/document/DocumentDatabase$DocumentList;->entries:[Lcom/google/android/apps/chrome/document/DocumentDatabase$DocumentEntry;

    array-length v1, v1

    if-ge v4, v1, :cond_5

    .line 629
    iget-object v1, v0, Lcom/google/android/apps/chrome/document/DocumentDatabase$DocumentList;->entries:[Lcom/google/android/apps/chrome/document/DocumentDatabase$DocumentEntry;

    aget-object v5, v1, v4

    .line 630
    iget-object v1, v5, Lcom/google/android/apps/chrome/document/DocumentDatabase$DocumentEntry;->tabId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 632
    iget-object v6, p0, Lcom/google/android/apps/chrome/document/DocumentTabList;->mEntryMap:Landroid/util/SparseArray;

    invoke-virtual {v6, v1}, Landroid/util/SparseArray;->indexOfKey(I)I

    move-result v6

    if-gez v6, :cond_4

    .line 633
    iget-object v5, p0, Lcom/google/android/apps/chrome/document/DocumentTabList;->mHistoricalTabs:Ljava/util/List;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v5, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 628
    :goto_5
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_4

    .line 637
    :cond_4
    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/document/DocumentTabList;->addTabId(I)V

    .line 638
    iget-object v6, p0, Lcom/google/android/apps/chrome/document/DocumentTabList;->mEntryMap:Landroid/util/SparseArray;

    invoke-virtual {v6, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/chrome/document/DocumentTabList$Entry;

    iget-object v5, v5, Lcom/google/android/apps/chrome/document/DocumentDatabase$DocumentEntry;->canGoBack:Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    iput-boolean v5, v1, Lcom/google/android/apps/chrome/document/DocumentTabList$Entry;->canGoBack:Z
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_5

    .line 642
    :catch_1
    move-exception v0

    .line 643
    :goto_6
    :try_start_4
    const-string/jumbo v1, "DocumentTabList"

    const-string/jumbo v4, "I/O exception"

    invoke-static {v1, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 645
    invoke-static {v3}, Lorg/chromium/chrome/browser/util/StreamUtil;->closeQuietly(Ljava/io/Closeable;)V

    goto :goto_3

    :cond_5
    invoke-static {v3}, Lorg/chromium/chrome/browser/util/StreamUtil;->closeQuietly(Ljava/io/Closeable;)V

    goto :goto_3

    :catchall_0
    move-exception v1

    move-object v3, v0

    move-object v0, v1

    :goto_7
    invoke-static {v3}, Lorg/chromium/chrome/browser/util/StreamUtil;->closeQuietly(Ljava/io/Closeable;)V

    throw v0

    .line 657
    :cond_6
    iget v0, p0, Lcom/google/android/apps/chrome/document/DocumentTabList;->mPrioritizedTabId:I

    if-eq v0, v7, :cond_7

    .line 658
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentTabList;->mEntryMap:Landroid/util/SparseArray;

    iget v1, p0, Lcom/google/android/apps/chrome/document/DocumentTabList;->mPrioritizedTabId:I

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/document/DocumentTabList$Entry;

    .line 659
    if-eqz v0, :cond_7

    .line 660
    iget v1, p0, Lcom/google/android/apps/chrome/document/DocumentTabList;->mPrioritizedTabId:I

    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/document/DocumentTabList;->restoreTabState(I)Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/chrome/document/DocumentTabList$Entry;->tabState:Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;

    .line 661
    iput-boolean v8, v0, Lcom/google/android/apps/chrome/document/DocumentTabList$Entry;->isTabStateReady:Z

    .line 665
    :cond_7
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/document/DocumentTabList;->setCurrentState(I)V

    .line 666
    invoke-direct {p0}, Lcom/google/android/apps/chrome/document/DocumentTabList;->loadTabStatesAsync()V

    .line 667
    return-void

    .line 645
    :catchall_1
    move-exception v0

    goto :goto_7

    :catchall_2
    move-exception v1

    move-object v3, v0

    move-object v0, v1

    goto :goto_7

    .line 642
    :catch_2
    move-exception v1

    move-object v3, v0

    move-object v0, v1

    goto :goto_6

    .line 641
    :catch_3
    move-exception v1

    goto/16 :goto_2
.end method

.method private isSessionRestoreInProgress()Z
    .locals 2

    .prologue
    .line 381
    iget v0, p0, Lcom/google/android/apps/chrome/document/DocumentTabList;->mCurrentState:I

    const/16 v1, 0xa

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private loadHistoricalTabsAsync()V
    .locals 3

    .prologue
    .line 768
    new-instance v0, Lcom/google/android/apps/chrome/document/DocumentTabList$3;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/document/DocumentTabList$3;-><init>(Lcom/google/android/apps/chrome/document/DocumentTabList;)V

    sget-object v1, Landroid/os/AsyncTask;->SERIAL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/document/DocumentTabList$3;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 802
    return-void
.end method

.method private loadTabStatesAsync()V
    .locals 3

    .prologue
    .line 670
    new-instance v0, Lcom/google/android/apps/chrome/document/DocumentTabList$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/document/DocumentTabList$1;-><init>(Lcom/google/android/apps/chrome/document/DocumentTabList;)V

    sget-object v1, Landroid/os/AsyncTask;->SERIAL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/document/DocumentTabList$1;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 705
    return-void
.end method

.method private native nativeBroadcastSessionRestoreComplete(J)V
.end method

.method private native nativeGetSyncSessionId(J)I
.end method

.method private native nativeInit(Z)J
.end method

.method private restoreTabState(I)Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 951
    .line 954
    :try_start_0
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/document/DocumentTabList;->getTabFile(I)Ljava/io/File;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 955
    :try_start_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentTabList;->isIncognito()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/android/apps/chrome/tab/ChromeTab;->readState(Ljava/io/FileInputStream;Z)Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 961
    invoke-static {v1}, Lorg/chromium/chrome/browser/util/StreamUtil;->closeQuietly(Ljava/io/Closeable;)V

    .line 963
    :goto_0
    return-object v0

    .line 957
    :catch_0
    move-exception v1

    move-object v1, v0

    :goto_1
    :try_start_2
    const-string/jumbo v2, "DocumentTabList"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Failed to restore tab state for tab "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 961
    invoke-static {v1}, Lorg/chromium/chrome/browser/util/StreamUtil;->closeQuietly(Ljava/io/Closeable;)V

    goto :goto_0

    .line 958
    :catch_1
    move-exception v1

    move-object v2, v0

    .line 959
    :goto_2
    :try_start_3
    const-string/jumbo v3, "DocumentTabList"

    const-string/jumbo v4, "Failed to restore tab state."

    invoke-static {v3, v4, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 961
    invoke-static {v2}, Lorg/chromium/chrome/browser/util/StreamUtil;->closeQuietly(Ljava/io/Closeable;)V

    goto :goto_0

    :catchall_0
    move-exception v1

    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    :goto_3
    invoke-static {v1}, Lorg/chromium/chrome/browser/util/StreamUtil;->closeQuietly(Ljava/io/Closeable;)V

    throw v0

    :catchall_1
    move-exception v0

    goto :goto_3

    :catchall_2
    move-exception v0

    move-object v1, v2

    goto :goto_3

    .line 958
    :catch_2
    move-exception v2

    move-object v5, v2

    move-object v2, v1

    move-object v1, v5

    goto :goto_2

    .line 957
    :catch_3
    move-exception v2

    goto :goto_1
.end method

.method private saveTabState(ILcom/google/android/apps/chrome/tab/ChromeTab$TabState;)V
    .locals 5

    .prologue
    .line 967
    const/4 v2, 0x0

    .line 969
    :try_start_0
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/document/DocumentTabList;->getTabFile(I)Ljava/io/File;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 970
    :try_start_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentTabList;->isIncognito()Z

    move-result v0

    invoke-static {v1, p2, v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->saveState(Ljava/io/FileOutputStream;Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;Z)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 976
    invoke-static {v1}, Lorg/chromium/chrome/browser/util/StreamUtil;->closeQuietly(Ljava/io/Closeable;)V

    .line 977
    :goto_0
    return-void

    .line 971
    :catch_0
    move-exception v0

    move-object v1, v2

    .line 972
    :goto_1
    :try_start_2
    const-string/jumbo v2, "DocumentTabList"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Failed to save out tab state for tab "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 976
    invoke-static {v1}, Lorg/chromium/chrome/browser/util/StreamUtil;->closeQuietly(Ljava/io/Closeable;)V

    goto :goto_0

    .line 973
    :catch_1
    move-exception v0

    .line 974
    :goto_2
    :try_start_3
    const-string/jumbo v1, "DocumentTabList"

    const-string/jumbo v3, "Failed to save out tab state."

    invoke-static {v1, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 976
    invoke-static {v2}, Lorg/chromium/chrome/browser/util/StreamUtil;->closeQuietly(Ljava/io/Closeable;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_3
    invoke-static {v1}, Lorg/chromium/chrome/browser/util/StreamUtil;->closeQuietly(Ljava/io/Closeable;)V

    throw v0

    :catchall_1
    move-exception v0

    goto :goto_3

    :catchall_2
    move-exception v0

    move-object v1, v2

    goto :goto_3

    .line 973
    :catch_2
    move-exception v0

    move-object v2, v1

    goto :goto_2

    .line 971
    :catch_3
    move-exception v0

    goto :goto_1
.end method

.method private setCurrentState(I)V
    .locals 5

    .prologue
    .line 928
    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    .line 929
    sget-boolean v0, Lcom/google/android/apps/chrome/document/DocumentTabList;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/chrome/document/DocumentTabList;->mCurrentState:I

    add-int/lit8 v1, p1, -0x1

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 930
    :cond_0
    iput p1, p0, Lcom/google/android/apps/chrome/document/DocumentTabList;->mCurrentState:I

    .line 932
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentTabList;->mObservers:Lorg/chromium/base/ObserverList;

    invoke-virtual {v0}, Lorg/chromium/base/ObserverList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/document/DocumentTabList$InitializationObserver;

    .line 933
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/document/DocumentTabList$InitializationObserver;->isCanceled()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 934
    const-string/jumbo v2, "DocumentTabList"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Observer alerted after canceled: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 935
    iget-object v2, p0, Lcom/google/android/apps/chrome/document/DocumentTabList;->mObservers:Lorg/chromium/base/ObserverList;

    invoke-virtual {v2, v0}, Lorg/chromium/base/ObserverList;->removeObserver(Ljava/lang/Object;)Z

    goto :goto_0

    .line 936
    :cond_2
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/document/DocumentTabList$InitializationObserver;->isSatisfied()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 937
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/document/DocumentTabList$InitializationObserver;->runWhenReady()V

    .line 938
    iget-object v2, p0, Lcom/google/android/apps/chrome/document/DocumentTabList;->mObservers:Lorg/chromium/base/ObserverList;

    invoke-virtual {v2, v0}, Lorg/chromium/base/ObserverList;->removeObserver(Ljava/lang/Object;)Z

    goto :goto_0

    .line 941
    :cond_3
    return-void
.end method

.method private writeGeneralDataToStorageAsync()V
    .locals 3

    .prologue
    .line 847
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentTabList;->isIncognito()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 887
    :goto_0
    return-void

    .line 849
    :cond_0
    new-instance v0, Lcom/google/android/apps/chrome/document/DocumentTabList$5;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/document/DocumentTabList$5;-><init>(Lcom/google/android/apps/chrome/document/DocumentTabList;)V

    sget-object v1, Landroid/os/AsyncTask;->SERIAL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/document/DocumentTabList$5;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method private writeTabStatesToStorageAsync()V
    .locals 3

    .prologue
    .line 893
    new-instance v0, Lcom/google/android/apps/chrome/document/DocumentTabList$6;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/document/DocumentTabList$6;-><init>(Lcom/google/android/apps/chrome/document/DocumentTabList;)V

    sget-object v1, Landroid/os/AsyncTask;->SERIAL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/document/DocumentTabList$6;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 925
    return-void
.end method


# virtual methods
.method public addEntryForMigration(Lcom/google/android/apps/chrome/document/DocumentTabList$Entry;)V
    .locals 2

    .prologue
    .line 597
    iget v0, p1, Lcom/google/android/apps/chrome/document/DocumentTabList$Entry;->tabId:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/document/DocumentTabList;->addTabId(I)V

    .line 598
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentTabList;->mEntryMap:Landroid/util/SparseArray;

    iget v1, p1, Lcom/google/android/apps/chrome/document/DocumentTabList$Entry;->tabId:I

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->indexOfKey(I)I

    move-result v0

    if-ltz v0, :cond_0

    .line 600
    :goto_0
    return-void

    .line 599
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentTabList;->mEntryMap:Landroid/util/SparseArray;

    iget v1, p1, Lcom/google/android/apps/chrome/document/DocumentTabList$Entry;->tabId:I

    invoke-virtual {v0, v1, p1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_0
.end method

.method public closeTab(Lorg/chromium/chrome/browser/Tab;)V
    .locals 3

    .prologue
    .line 368
    invoke-virtual {p1}, Lorg/chromium/chrome/browser/Tab;->getId()I

    move-result v0

    .line 369
    iget-object v1, p0, Lcom/google/android/apps/chrome/document/DocumentTabList;->mDelegate:Lcom/google/android/apps/chrome/document/DocumentTabList$Delegate;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentTabList;->isIncognito()Z

    move-result v2

    invoke-interface {v1, v2, v0}, Lcom/google/android/apps/chrome/document/DocumentTabList$Delegate;->closeTab(ZI)V

    .line 370
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentTabList;->updateRecentlyClosed()V

    .line 371
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 313
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentTabList;->mTabIdList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getCurrentState()I
    .locals 1

    .prologue
    .line 947
    iget v0, p0, Lcom/google/android/apps/chrome/document/DocumentTabList;->mCurrentState:I

    return v0
.end method

.method public getCurrentUrlForDocument(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 453
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentTabList;->mEntryMap:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/document/DocumentTabList$Entry;

    .line 454
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, v0, Lcom/google/android/apps/chrome/document/DocumentTabList$Entry;->currentUrl:Ljava/lang/String;

    goto :goto_0
.end method

.method public getHistoricalTabs()Ljava/util/Set;
    .locals 2

    .prologue
    .line 431
    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    .line 432
    new-instance v0, Ljava/util/HashSet;

    iget-object v1, p0, Lcom/google/android/apps/chrome/document/DocumentTabList;->mHistoricalTabs:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(I)V

    .line 433
    iget-object v1, p0, Lcom/google/android/apps/chrome/document/DocumentTabList;->mHistoricalTabs:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 434
    return-object v0
.end method

.method public getInitialUrlForDocument(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 443
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentTabList;->mEntryMap:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/document/DocumentTabList$Entry;

    .line 444
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, v0, Lcom/google/android/apps/chrome/document/DocumentTabList$Entry;->initialUrl:Ljava/lang/String;

    goto :goto_0
.end method

.method public getSessionId()I
    .locals 1

    .prologue
    .line 283
    iget v0, p0, Lcom/google/android/apps/chrome/document/DocumentTabList;->mSessionId:I

    return v0
.end method

.method public getTabAt(I)Lorg/chromium/chrome/browser/Tab;
    .locals 6

    .prologue
    .line 329
    if-ltz p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentTabList;->getCount()I

    move-result v0

    if-lt p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    .line 348
    :goto_0
    return-object v0

    .line 332
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentTabList;->mTabIdList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 333
    invoke-static {}, Lorg/chromium/base/ApplicationStatus;->getRunningActivities()Ljava/util/List;

    move-result-object v0

    .line 334
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 335
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 336
    instance-of v1, v0, Lcom/google/android/apps/chrome/ChromeActivity;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/chrome/document/DocumentTabList;->mDelegate:Lcom/google/android/apps/chrome/document/DocumentTabList$Delegate;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentTabList;->isIncognito()Z

    move-result v4

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    invoke-interface {v1, v4, v5}, Lcom/google/android/apps/chrome/document/DocumentTabList$Delegate;->isValidActivity(ZLandroid/content/Intent;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 338
    check-cast v0, Lcom/google/android/apps/chrome/ChromeActivity;

    .line 342
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeActivity;->getCurrentTab()Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    .line 343
    if-nez v0, :cond_3

    const/4 v1, -0x1

    .line 344
    :goto_1
    if-ne v1, v2, :cond_2

    goto :goto_0

    .line 343
    :cond_3
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getId()I

    move-result v1

    goto :goto_1

    .line 348
    :cond_4
    invoke-direct {p0, v2}, Lcom/google/android/apps/chrome/document/DocumentTabList;->getFrozenTabWithId(I)Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    goto :goto_0
.end method

.method public getTabIdAtIndex(I)I
    .locals 1

    .prologue
    .line 409
    sget-boolean v0, Lcom/google/android/apps/chrome/document/DocumentTabList;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentTabList;->mTabIdList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 410
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentTabList;->mTabIdList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public getTabStateForDocument(I)Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;
    .locals 1

    .prologue
    .line 474
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentTabList;->mEntryMap:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/document/DocumentTabList$Entry;

    .line 475
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, v0, Lcom/google/android/apps/chrome/document/DocumentTabList$Entry;->tabState:Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;

    goto :goto_0
.end method

.method public hasEntryForTabId(I)Z
    .locals 1

    .prologue
    .line 484
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentTabList;->mEntryMap:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public index()I
    .locals 2

    .prologue
    const/4 v0, -0x1

    .line 299
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentTabList;->getCount()I

    move-result v1

    if-nez v1, :cond_0

    .line 301
    :goto_0
    return v0

    .line 300
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/document/DocumentTabList;->mDelegate:Lcom/google/android/apps/chrome/document/DocumentTabList$Delegate;

    invoke-interface {v1}, Lcom/google/android/apps/chrome/document/DocumentTabList$Delegate;->getLastUsedTabId()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/document/DocumentTabList;->indexOf(I)I

    move-result v1

    .line 301
    if-eq v1, v0, :cond_1

    move v0, v1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public indexOf(I)I
    .locals 2

    .prologue
    .line 292
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentTabList;->mTabIdList:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public indexOf(Lorg/chromium/chrome/browser/Tab;)I
    .locals 1

    .prologue
    .line 306
    if-nez p1, :cond_0

    const/4 v0, -0x1

    .line 307
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Lorg/chromium/chrome/browser/Tab;->getId()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/document/DocumentTabList;->indexOf(I)I

    move-result v0

    goto :goto_0
.end method

.method protected initializeNative()V
    .locals 2

    .prologue
    .line 265
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentTabList;->isNativeCreated()Z

    move-result v0

    if-nez v0, :cond_0

    .line 266
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentTabList;->isIncognito()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/document/DocumentTabList;->nativeInit(Z)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/chrome/document/DocumentTabList;->mNativePointer:J

    .line 267
    iget-wide v0, p0, Lcom/google/android/apps/chrome/document/DocumentTabList;->mNativePointer:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/document/DocumentTabList;->nativeGetSyncSessionId(J)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/document/DocumentTabList;->mSessionId:I

    .line 269
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/document/DocumentTabList;->deserializeTabStatesAsync()V

    .line 270
    return-void
.end method

.method public isClosurePending(I)Z
    .locals 1

    .prologue
    .line 323
    const/4 v0, 0x0

    return v0
.end method

.method public isIncognito()Z
    .locals 1

    .prologue
    .line 318
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/document/DocumentTabList;->mIsIncognito:Z

    return v0
.end method

.method protected isNativeCreated()Z
    .locals 4

    .prologue
    .line 276
    iget-wide v0, p0, Lcom/google/android/apps/chrome/document/DocumentTabList;->mNativePointer:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isRetargetable(I)Z
    .locals 1

    .prologue
    .line 493
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentTabList;->mEntryMap:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/document/DocumentTabList$Entry;

    .line 494
    if-eqz v0, :cond_0

    iget-boolean v0, v0, Lcom/google/android/apps/chrome/document/DocumentTabList$Entry;->canGoBack:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isTabStateReady(I)Z
    .locals 1

    .prologue
    .line 464
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentTabList;->mEntryMap:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/document/DocumentTabList$Entry;

    .line 465
    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    iget-boolean v0, v0, Lcom/google/android/apps/chrome/document/DocumentTabList$Entry;->isTabStateReady:Z

    goto :goto_0
.end method

.method public recordDocumentLaunching(II)V
    .locals 2

    .prologue
    .line 419
    invoke-virtual {p0, p2}, Lcom/google/android/apps/chrome/document/DocumentTabList;->indexOf(I)I

    move-result v0

    .line 420
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 421
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/document/DocumentTabList;->addTabId(I)V

    .line 425
    :goto_0
    return-void

    .line 423
    :cond_0
    add-int/lit8 v0, v0, 0x1

    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/chrome/document/DocumentTabList;->addTabId(II)V

    goto :goto_0
.end method

.method public setIndex(I)V
    .locals 3

    .prologue
    .line 357
    if-ltz p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentTabList;->getCount()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 360
    :cond_0
    :goto_0
    return-void

    .line 358
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentTabList;->mTabIdList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 359
    iget-object v1, p0, Lcom/google/android/apps/chrome/document/DocumentTabList;->mDelegate:Lcom/google/android/apps/chrome/document/DocumentTabList$Delegate;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentTabList;->isIncognito()Z

    move-result v2

    invoke-interface {v1, v2, v0}, Lcom/google/android/apps/chrome/document/DocumentTabList$Delegate;->bringTabToForeground(ZI)V

    goto :goto_0
.end method

.method protected shouldStartDeserialization()Z
    .locals 2

    .prologue
    .line 764
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentTabList;->isNativeCreated()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentTabList;->getCurrentState()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method updateEntry(Landroid/content/Intent;Lcom/google/android/apps/chrome/tab/ChromeTab;)V
    .locals 6

    .prologue
    const/4 v2, -0x1

    .line 550
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentTabList;->mDelegate:Lcom/google/android/apps/chrome/document/DocumentTabList$Delegate;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentTabList;->isIncognito()Z

    move-result v1

    invoke-interface {v0, v1, p1}, Lcom/google/android/apps/chrome/document/DocumentTabList$Delegate;->isValidActivity(ZLandroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 590
    :cond_0
    :goto_0
    return-void

    .line 552
    :cond_1
    invoke-static {p1}, Lcom/google/android/apps/chrome/IntentHandler;->getTabIdFromIntent(Landroid/content/Intent;)I

    move-result v1

    .line 553
    if-eq v1, v2, :cond_0

    .line 555
    sget-boolean v0, Lcom/google/android/apps/chrome/document/DocumentTabList;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/document/DocumentTabList;->indexOf(I)I

    move-result v0

    if-ne v0, v2, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 557
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentTabList;->mEntryMap:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/document/DocumentTabList$Entry;

    .line 558
    invoke-virtual {p2}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getUrl()Ljava/lang/String;

    move-result-object v2

    .line 559
    invoke-virtual {p2}, Lcom/google/android/apps/chrome/tab/ChromeTab;->canGoBack()Z

    move-result v3

    .line 560
    invoke-virtual {p2}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getState()Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;

    move-result-object v4

    .line 561
    if-eqz v0, :cond_3

    iget v5, v0, Lcom/google/android/apps/chrome/document/DocumentTabList$Entry;->tabId:I

    if-ne v5, v1, :cond_3

    iget-object v5, v0, Lcom/google/android/apps/chrome/document/DocumentTabList$Entry;->currentUrl:Ljava/lang/String;

    invoke-static {v5, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_3

    iget-boolean v5, v0, Lcom/google/android/apps/chrome/document/DocumentTabList$Entry;->canGoBack:Z

    if-ne v5, v3, :cond_3

    iget-object v5, v0, Lcom/google/android/apps/chrome/document/DocumentTabList$Entry;->tabState:Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;

    if-ne v5, v4, :cond_3

    invoke-virtual {p2}, Lcom/google/android/apps/chrome/tab/ChromeTab;->shouldTabStateBePersisted()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 570
    :cond_3
    if-nez v0, :cond_4

    .line 571
    new-instance v0, Lcom/google/android/apps/chrome/document/DocumentTabList$Entry;

    invoke-static {p1}, Lcom/google/android/apps/chrome/IntentHandler;->getUrlFromIntent(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v0, v1, v5}, Lcom/google/android/apps/chrome/document/DocumentTabList$Entry;-><init>(ILjava/lang/String;)V

    .line 572
    iget-object v5, p0, Lcom/google/android/apps/chrome/document/DocumentTabList;->mEntryMap:Landroid/util/SparseArray;

    invoke-virtual {v5, v1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 574
    :cond_4
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/apps/chrome/document/DocumentTabList$Entry;->isDirty:Z

    .line 575
    iput-object v2, v0, Lcom/google/android/apps/chrome/document/DocumentTabList$Entry;->currentUrl:Ljava/lang/String;

    .line 576
    iput-boolean v3, v0, Lcom/google/android/apps/chrome/document/DocumentTabList$Entry;->canGoBack:Z

    .line 577
    iput-object v4, v0, Lcom/google/android/apps/chrome/document/DocumentTabList$Entry;->tabState:Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;

    .line 581
    invoke-virtual {p2}, Lcom/google/android/apps/chrome/tab/ChromeTab;->tabStateWasPersisted()V

    .line 583
    iget-object v1, v0, Lcom/google/android/apps/chrome/document/DocumentTabList$Entry;->frozenTab:Lcom/google/android/apps/chrome/tab/ChromeTab;

    if-eqz v1, :cond_5

    .line 584
    iget-object v1, v0, Lcom/google/android/apps/chrome/document/DocumentTabList$Entry;->frozenTab:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->destroy()V

    .line 585
    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/android/apps/chrome/document/DocumentTabList$Entry;->frozenTab:Lcom/google/android/apps/chrome/tab/ChromeTab;

    .line 588
    :cond_5
    invoke-direct {p0}, Lcom/google/android/apps/chrome/document/DocumentTabList;->writeGeneralDataToStorageAsync()V

    .line 589
    invoke-direct {p0}, Lcom/google/android/apps/chrome/document/DocumentTabList;->writeTabStatesToStorageAsync()V

    goto :goto_0
.end method

.method protected updateEntryInfoFromTabState(Lcom/google/android/apps/chrome/document/DocumentTabList$Entry;Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;)V
    .locals 1

    .prologue
    .line 757
    iget-object v0, p2, Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;->contentsState:Lcom/google/android/apps/chrome/tab/ChromeTab$ContentsState;

    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getVirtualUrlFromState(Lcom/google/android/apps/chrome/tab/ChromeTab$ContentsState;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Lcom/google/android/apps/chrome/document/DocumentTabList$Entry;->currentUrl:Ljava/lang/String;

    .line 758
    return-void
.end method

.method public updateRecentlyClosed()V
    .locals 6

    .prologue
    .line 504
    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    .line 505
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentTabList;->mDelegate:Lcom/google/android/apps/chrome/document/DocumentTabList$Delegate;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentTabList;->isIncognito()Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/chrome/document/DocumentTabList$Delegate;->getTasksFromRecents(Z)Landroid/util/SparseArray;

    move-result-object v2

    .line 506
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    .line 507
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentTabList;->mEntryMap:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 508
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentTabList;->mEntryMap:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v4

    .line 509
    invoke-virtual {v2, v4}, Landroid/util/SparseArray;->indexOfKey(I)I

    move-result v0

    if-gez v0, :cond_1

    .line 510
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentTabList;->mEntryMap:Landroid/util/SparseArray;

    invoke-virtual {v0, v4}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/document/DocumentTabList$Entry;

    .line 511
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentTabList;->isIncognito()Z

    move-result v5

    if-nez v5, :cond_0

    iget-object v5, v0, Lcom/google/android/apps/chrome/document/DocumentTabList$Entry;->tabState:Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;

    if-eqz v5, :cond_0

    iget-object v5, v0, Lcom/google/android/apps/chrome/document/DocumentTabList$Entry;->tabState:Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;

    iget-object v5, v5, Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;->contentsState:Lcom/google/android/apps/chrome/tab/ChromeTab$ContentsState;

    if-eqz v5, :cond_0

    .line 513
    iget-object v0, v0, Lcom/google/android/apps/chrome/document/DocumentTabList$Entry;->tabState:Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;

    iget-object v0, v0, Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;->contentsState:Lcom/google/android/apps/chrome/tab/ChromeTab$ContentsState;

    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->createHistoricalTabFromState(Lcom/google/android/apps/chrome/tab/ChromeTab$ContentsState;)V

    .line 515
    :cond_0
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 507
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 519
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentTabList;->mTabIdList:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->removeAll(Ljava/util/Collection;)Z

    .line 520
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 521
    iget-object v2, p0, Lcom/google/android/apps/chrome/document/DocumentTabList;->mEntryMap:Landroid/util/SparseArray;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/util/SparseArray;->remove(I)V

    goto :goto_1

    .line 523
    :cond_3
    return-void
.end method
