.class public Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;
.super Ljava/lang/Object;
.source "FullscreenHtmlApiHandler.java"


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static sFullscreenNotificationShown:Z


# instance fields
.field private mContentViewCoreInFullscreen:Lorg/chromium/content/browser/ContentViewCore;

.field private final mDelegate:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler$FullscreenHtmlApiDelegate;

.field private mFullscreenOnLayoutChangeListener:Landroid/view/View$OnLayoutChangeListener;

.field private final mHandler:Landroid/os/Handler;

.field private mIsPersistentMode:Z

.field private mNotificationBubble:Lcom/google/android/apps/chrome/widget/bubble/TextBubble;

.field private final mNotificationMaxDimension:I

.field private final mPersistentFullscreenSupported:Z

.field private final mWindow:Landroid/view/Window;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const-class v0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/view/Window;Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler$FullscreenHtmlApiDelegate;Z)V
    .locals 2

    .prologue
    .line 117
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 118
    iput-object p1, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;->mWindow:Landroid/view/Window;

    .line 119
    iput-object p2, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;->mDelegate:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler$FullscreenHtmlApiDelegate;

    .line 120
    iput-boolean p3, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;->mPersistentFullscreenSupported:Z

    .line 122
    new-instance v0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler$1;-><init>(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;->mHandler:Landroid/os/Handler;

    .line 187
    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;->mWindow:Landroid/view/Window;

    invoke-virtual {v0}, Landroid/view/Window;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 188
    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    .line 189
    const/high16 v1, 0x44160000    # 600.0f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;->mNotificationMaxDimension:I

    .line 190
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;)Z
    .locals 1

    .prologue
    .line 30
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;->mIsPersistentMode:Z

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;)Lorg/chromium/content/browser/ContentViewCore;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;->mContentViewCoreInFullscreen:Lorg/chromium/content/browser/ContentViewCore;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;)Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler$FullscreenHtmlApiDelegate;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;->mDelegate:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler$FullscreenHtmlApiDelegate;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;)Landroid/view/Window;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;->mWindow:Landroid/view/Window;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;->showNotificationBubble(Ljava/lang/String;)V

    return-void
.end method

.method private exitFullscreen(Lorg/chromium/content/browser/ContentViewCore;)V
    .locals 4

    .prologue
    .line 228
    invoke-virtual {p1}, Lorg/chromium/content/browser/ContentViewCore;->getContainerView()Landroid/view/ViewGroup;

    move-result-object v1

    .line 229
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;->hideNotificationBubble()V

    .line 230
    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 231
    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x3

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 233
    invoke-virtual {v1}, Landroid/view/View;->getSystemUiVisibility()I

    move-result v0

    .line 234
    and-int/lit8 v0, v0, -0x2

    .line 235
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x12

    if-lt v2, v3, :cond_1

    .line 236
    and-int/lit16 v0, v0, -0x401

    .line 237
    and-int/lit8 v0, v0, -0x5

    .line 242
    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 243
    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;->mFullscreenOnLayoutChangeListener:Landroid/view/View$OnLayoutChangeListener;

    if-eqz v0, :cond_0

    .line 244
    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;->mFullscreenOnLayoutChangeListener:Landroid/view/View$OnLayoutChangeListener;

    invoke-virtual {v1, v0}, Landroid/view/View;->removeOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 246
    :cond_0
    new-instance v0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler$2;

    invoke-direct {v0, p0, p1, v1}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler$2;-><init>(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;Lorg/chromium/content/browser/ContentViewCore;Landroid/view/View;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;->mFullscreenOnLayoutChangeListener:Landroid/view/View$OnLayoutChangeListener;

    .line 256
    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;->mFullscreenOnLayoutChangeListener:Landroid/view/View$OnLayoutChangeListener;

    invoke-virtual {v1, v0}, Landroid/view/View;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 257
    invoke-virtual {p1}, Lorg/chromium/content/browser/ContentViewCore;->exitFullscreen()V

    .line 258
    return-void

    .line 239
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;->mWindow:Landroid/view/Window;

    const/16 v3, 0x800

    invoke-virtual {v2, v3}, Landroid/view/Window;->addFlags(I)V

    .line 240
    iget-object v2, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;->mWindow:Landroid/view/Window;

    const/16 v3, 0x400

    invoke-virtual {v2, v3}, Landroid/view/Window;->clearFlags(I)V

    goto :goto_0
.end method

.method private getOrCreateNotificationBubble()Lcom/google/android/apps/chrome/widget/bubble/TextBubble;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 313
    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;->mNotificationBubble:Lcom/google/android/apps/chrome/widget/bubble/TextBubble;

    if-nez v0, :cond_0

    .line 314
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 315
    const-string/jumbo v1, "Background_Intrinsic_Padding"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 316
    const-string/jumbo v1, "Center"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 317
    const-string/jumbo v1, "Up_Down"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 318
    const-string/jumbo v1, "Text_Style_Id"

    const v2, 0x10301b1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 320
    const-string/jumbo v1, "Animation_Style"

    sget v2, Lcom/google/android/apps/chrome/R$style;->FullscreenNotificationBubble:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 321
    new-instance v1, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;

    iget-object v2, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;->mWindow:Landroid/view/Window;

    invoke-virtual {v2}, Landroid/view/Window;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;-><init>(Landroid/content/Context;Landroid/os/Bundle;)V

    iput-object v1, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;->mNotificationBubble:Lcom/google/android/apps/chrome/widget/bubble/TextBubble;

    .line 322
    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;->mNotificationBubble:Lcom/google/android/apps/chrome/widget/bubble/TextBubble;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->getBubbleTextView()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setGravity(I)V

    .line 323
    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;->mNotificationBubble:Lcom/google/android/apps/chrome/widget/bubble/TextBubble;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    const/16 v1, 0xfc

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 324
    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;->mNotificationBubble:Lcom/google/android/apps/chrome/widget/bubble/TextBubble;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->setTouchable(Z)V

    .line 326
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;->mNotificationBubble:Lcom/google/android/apps/chrome/widget/bubble/TextBubble;

    return-object v0
.end method

.method private showNotificationBubble(Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 330
    invoke-direct {p0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;->getOrCreateNotificationBubble()Lcom/google/android/apps/chrome/widget/bubble/TextBubble;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;->mDelegate:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler$FullscreenHtmlApiDelegate;

    invoke-interface {v1}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler$FullscreenHtmlApiDelegate;->getNotificationAnchorView()Landroid/view/View;

    move-result-object v1

    iget v2, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;->mNotificationMaxDimension:I

    iget v3, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;->mNotificationMaxDimension:I

    invoke-virtual {v0, p1, v1, v2, v3}, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->showTextBubble(Ljava/lang/String;Landroid/view/View;II)V

    .line 332
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;->updateBubblePosition()V

    .line 334
    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 336
    const-wide/16 v0, 0xdac

    .line 337
    sget-boolean v2, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;->sFullscreenNotificationShown:Z

    if-eqz v2, :cond_0

    .line 338
    const-wide/16 v0, 0x9c4

    .line 340
    :cond_0
    sput-boolean v4, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;->sFullscreenNotificationShown:Z

    .line 342
    iget-object v2, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v4, v0, v1}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 343
    return-void
.end method


# virtual methods
.method public enterFullscreen(Lorg/chromium/content/browser/ContentViewCore;)V
    .locals 5

    .prologue
    const/16 v4, 0x400

    .line 265
    invoke-virtual {p1}, Lorg/chromium/content/browser/ContentViewCore;->getContainerView()Landroid/view/ViewGroup;

    move-result-object v1

    .line 266
    invoke-virtual {v1}, Landroid/view/View;->getSystemUiVisibility()I

    move-result v0

    .line 267
    or-int/lit8 v0, v0, 0x1

    .line 268
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x12

    if-lt v2, v3, :cond_2

    .line 269
    and-int/lit16 v2, v0, 0x400

    if-ne v2, v4, :cond_1

    .line 271
    or-int/lit8 v0, v0, 0x4

    .line 279
    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;->mFullscreenOnLayoutChangeListener:Landroid/view/View$OnLayoutChangeListener;

    if-eqz v2, :cond_0

    .line 280
    iget-object v2, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;->mFullscreenOnLayoutChangeListener:Landroid/view/View$OnLayoutChangeListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->removeOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 282
    :cond_0
    new-instance v2, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler$3;

    invoke-direct {v2, p0, v1}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler$3;-><init>(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;Landroid/view/View;)V

    iput-object v2, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;->mFullscreenOnLayoutChangeListener:Landroid/view/View$OnLayoutChangeListener;

    .line 304
    iget-object v2, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;->mFullscreenOnLayoutChangeListener:Landroid/view/View$OnLayoutChangeListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 305
    invoke-virtual {v1, v0}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 306
    iput-object p1, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;->mContentViewCoreInFullscreen:Lorg/chromium/content/browser/ContentViewCore;

    .line 307
    return-void

    .line 273
    :cond_1
    or-int/lit16 v0, v0, 0x400

    goto :goto_0

    .line 276
    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;->mWindow:Landroid/view/Window;

    invoke-virtual {v2, v4}, Landroid/view/Window;->addFlags(I)V

    .line 277
    iget-object v2, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;->mWindow:Landroid/view/Window;

    const/16 v3, 0x800

    invoke-virtual {v2, v3}, Landroid/view/Window;->clearFlags(I)V

    goto :goto_0
.end method

.method public getPersistentFullscreenMode()Z
    .locals 1

    .prologue
    .line 224
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;->mIsPersistentMode:Z

    return v0
.end method

.method public hideNotificationBubble()V
    .locals 1

    .prologue
    .line 358
    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;->mNotificationBubble:Lcom/google/android/apps/chrome/widget/bubble/TextBubble;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;->mNotificationBubble:Lcom/google/android/apps/chrome/widget/bubble/TextBubble;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 359
    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;->mNotificationBubble:Lcom/google/android/apps/chrome/widget/bubble/TextBubble;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->dismiss()V

    .line 361
    :cond_0
    return-void
.end method

.method public onContentViewSystemUiVisibilityChange(I)V
    .locals 4

    .prologue
    .line 369
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-ge v0, v1, :cond_1

    .line 374
    :cond_0
    :goto_0
    return-void

    .line 371
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;->mContentViewCoreInFullscreen:Lorg/chromium/content/browser/ContentViewCore;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;->mIsPersistentMode:Z

    if-eqz v0, :cond_0

    .line 372
    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0
.end method

.method public onWindowFocusChanged(Z)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    .line 381
    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;->hideNotificationBubble()V

    .line 382
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-ge v0, v1, :cond_2

    .line 389
    :cond_1
    :goto_0
    return-void

    .line 384
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 385
    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 386
    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;->mContentViewCoreInFullscreen:Lorg/chromium/content/browser/ContentViewCore;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;->mIsPersistentMode:Z

    if-eqz v0, :cond_1

    if-eqz p1, :cond_1

    .line 387
    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;->mHandler:Landroid/os/Handler;

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0
.end method

.method public setPersistentFullscreenMode(Z)V
    .locals 2

    .prologue
    .line 199
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;->mPersistentFullscreenSupported:Z

    if-nez v0, :cond_1

    .line 217
    :cond_0
    :goto_0
    return-void

    .line 201
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;->mIsPersistentMode:Z

    if-eq v0, p1, :cond_0

    .line 203
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;->mIsPersistentMode:Z

    .line 205
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;->mIsPersistentMode:Z

    if-eqz v0, :cond_2

    .line 206
    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;->mDelegate:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler$FullscreenHtmlApiDelegate;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler$FullscreenHtmlApiDelegate;->onEnterFullscreen()V

    goto :goto_0

    .line 208
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;->mContentViewCoreInFullscreen:Lorg/chromium/content/browser/ContentViewCore;

    if-eqz v0, :cond_4

    .line 209
    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;->mContentViewCoreInFullscreen:Lorg/chromium/content/browser/ContentViewCore;

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;->exitFullscreen(Lorg/chromium/content/browser/ContentViewCore;)V

    .line 215
    :cond_3
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;->mContentViewCoreInFullscreen:Lorg/chromium/content/browser/ContentViewCore;

    goto :goto_0

    .line 211
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;->mDelegate:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler$FullscreenHtmlApiDelegate;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler$FullscreenHtmlApiDelegate;->cancelPendingEnterFullscreen()Z

    move-result v0

    if-nez v0, :cond_3

    .line 212
    sget-boolean v0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;->$assertionsDisabled:Z

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    const-string/jumbo v1, "No content view previously set to fullscreen."

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method public updateBubblePosition()V
    .locals 2

    .prologue
    .line 349
    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;->mNotificationBubble:Lcom/google/android/apps/chrome/widget/bubble/TextBubble;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;->mNotificationBubble:Lcom/google/android/apps/chrome/widget/bubble/TextBubble;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 350
    iget-object v0, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;->mNotificationBubble:Lcom/google/android/apps/chrome/widget/bubble/TextBubble;

    iget-object v1, p0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;->mDelegate:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler$FullscreenHtmlApiDelegate;

    invoke-interface {v1}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler$FullscreenHtmlApiDelegate;->getNotificationOffsetY()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/widget/bubble/TextBubble;->setOffsetY(I)V

    .line 352
    :cond_0
    return-void
.end method
