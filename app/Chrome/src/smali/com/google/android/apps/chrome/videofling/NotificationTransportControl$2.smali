.class Lcom/google/android/apps/chrome/videofling/NotificationTransportControl$2;
.super Ljava/lang/Object;
.source "NotificationTransportControl.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;)V
    .locals 0

    .prologue
    .line 561
    iput-object p1, p0, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl$2;->this$0:Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 564
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl$2;->this$0:Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;

    iget-object v1, p0, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl$2;->this$0:Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;

    # getter for: Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->mMediaRouteController:Lcom/google/android/apps/chrome/videofling/MediaRouteController;
    invoke-static {v1}, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->access$600(Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;)Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->getPosition()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->onPositionChanged(I)V

    .line 565
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl$2;->this$0:Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;

    # getter for: Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->mMediaRouteController:Lcom/google/android/apps/chrome/videofling/MediaRouteController;
    invoke-static {v0}, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->access$600(Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;)Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 566
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl$2;->this$0:Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;

    # getter for: Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->access$400(Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl$2;->this$0:Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;

    # getter for: Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->mProgressUpdateInterval:I
    invoke-static {v1}, Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;->access$700(Lcom/google/android/apps/chrome/videofling/NotificationTransportControl;)I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0, p0, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 568
    :cond_0
    return-void
.end method
