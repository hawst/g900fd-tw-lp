.class public interface abstract Lcom/google/android/apps/chrome/eventfilter/GestureHandler;
.super Ljava/lang/Object;
.source "GestureHandler.java"


# virtual methods
.method public abstract click(FF)V
.end method

.method public abstract drag(FFFFFF)V
.end method

.method public abstract fling(FFFF)V
.end method

.method public abstract onDown(FF)V
.end method

.method public abstract onLongPress(FF)V
.end method

.method public abstract onPinch(FFFFZ)V
.end method

.method public abstract onUpOrCancel()V
.end method
