.class public Lcom/google/android/apps/chrome/document/IncognitoDocumentActivity;
.super Lcom/google/android/apps/chrome/document/DocumentActivity;
.source "IncognitoDocumentActivity.java"


# static fields
.field static final INCOGNITO_TABS_OPEN_ID:I = 0x64

.field static final INCOGNITO_TABS_OPEN_TAG:Ljava/lang/String; = "incognito_tabs_open"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/google/android/apps/chrome/document/DocumentActivity;-><init>()V

    return-void
.end method

.method public static dismissIncognitoNotification(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 75
    const-string/jumbo v0, "notification"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 77
    const-string/jumbo v1, "incognito_tabs_open"

    const/16 v2, 0x64

    invoke-virtual {v0, v1, v2}, Landroid/app/NotificationManager;->cancel(Ljava/lang/String;I)V

    .line 78
    return-void
.end method

.method public static dismissIncognitoNotificationIfNecessary(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 67
    invoke-static {p0}, Lcom/google/android/apps/chrome/document/IncognitoDocumentActivity;->getNumberOfIncognitoTabs(Landroid/content/Context;)I

    move-result v0

    if-eqz v0, :cond_0

    .line 69
    :goto_0
    return-void

    .line 68
    :cond_0
    invoke-static {p0}, Lcom/google/android/apps/chrome/document/IncognitoDocumentActivity;->dismissIncognitoNotification(Landroid/content/Context;)V

    goto :goto_0
.end method

.method public static getNumberOfIncognitoTabs(Landroid/content/Context;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 81
    const-string/jumbo v0, "activity"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 82
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 83
    invoke-virtual {v0}, Landroid/app/ActivityManager;->getAppTasks()Ljava/util/List;

    move-result-object v4

    move v2, v1

    .line 85
    :goto_0
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 86
    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager$AppTask;

    invoke-static {v0, v3}, Lcom/google/android/apps/chrome/document/ChromeLauncherActivity;->getTaskClassName(Landroid/app/ActivityManager$AppTask;Landroid/content/pm/PackageManager;)Ljava/lang/String;

    move-result-object v0

    .line 87
    if-eqz v0, :cond_0

    .line 88
    const-class v5, Lcom/google/android/apps/chrome/document/IncognitoDocumentActivity;

    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 89
    if-eqz v0, :cond_0

    add-int/lit8 v2, v2, 0x1

    .line 85
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 91
    :cond_1
    return v2
.end method


# virtual methods
.method protected isIncognito()Z
    .locals 1

    .prologue
    .line 31
    const/4 v0, 0x1

    return v0
.end method

.method public onDestroyInternal()V
    .locals 0

    .prologue
    .line 42
    invoke-super {p0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->onDestroyInternal()V

    .line 43
    invoke-static {p0}, Lcom/google/android/apps/chrome/document/IncognitoDocumentActivity;->dismissIncognitoNotificationIfNecessary(Landroid/content/Context;)V

    .line 44
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 36
    invoke-super {p0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->onResume()V

    .line 37
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/IncognitoDocumentActivity;->updateIncognitoNotification()V

    .line 38
    return-void
.end method

.method public updateIncognitoNotification()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 47
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/IncognitoDocumentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$string;->close_all_incognito:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 48
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/IncognitoDocumentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/chrome/R$string;->app_name:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 50
    new-instance v2, Landroid/app/Notification$Builder;

    invoke-direct {v2, p0}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v1}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v1

    invoke-static {p0}, Lcom/google/android/apps/chrome/document/ChromeLauncherActivity;->getRemoveAllIncognitoTabsIntent(Landroid/content/Context;)Landroid/app/PendingIntent;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/app/Notification$Builder;->setOngoing(Z)Landroid/app/Notification$Builder;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setVisibility(I)Landroid/app/Notification$Builder;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$drawable;->incognito_statusbar:I

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v0

    .line 57
    invoke-static {v0, v3}, Lcom/google/android/apps/chrome/utilities/UnreleasedApiCompatibilityUtils;->setLocalOnly(Landroid/app/Notification$Builder;Z)Landroid/app/Notification$Builder;

    move-result-object v1

    .line 58
    const-string/jumbo v0, "notification"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/document/IncognitoDocumentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 59
    const-string/jumbo v2, "incognito_tabs_open"

    const/16 v3, 0x64

    invoke-static {v1}, Lorg/chromium/base/ApiCompatibilityUtils;->build(Landroid/app/Notification$Builder;)Landroid/app/Notification;

    move-result-object v1

    invoke-virtual {v0, v2, v3, v1}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    .line 61
    return-void
.end method
