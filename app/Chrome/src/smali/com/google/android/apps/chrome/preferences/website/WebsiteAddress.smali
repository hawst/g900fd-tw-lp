.class public Lcom/google/android/apps/chrome/preferences/website/WebsiteAddress;
.super Ljava/lang/Object;
.source "WebsiteAddress.java"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/lang/Comparable;


# instance fields
.field private final mHost:Ljava/lang/String;

.field private final mOmitProtocolAndPort:Z

.field private final mOrigin:Ljava/lang/String;

.field private final mScheme:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    iput-object p1, p0, Lcom/google/android/apps/chrome/preferences/website/WebsiteAddress;->mOrigin:Ljava/lang/String;

    .line 54
    iput-object p2, p0, Lcom/google/android/apps/chrome/preferences/website/WebsiteAddress;->mScheme:Ljava/lang/String;

    .line 55
    iput-object p3, p0, Lcom/google/android/apps/chrome/preferences/website/WebsiteAddress;->mHost:Ljava/lang/String;

    .line 56
    iput-boolean p4, p0, Lcom/google/android/apps/chrome/preferences/website/WebsiteAddress;->mOmitProtocolAndPort:Z

    .line 57
    return-void
.end method

.method public static create(Ljava/lang/String;)Lcom/google/android/apps/chrome/preferences/website/WebsiteAddress;
    .locals 9

    .prologue
    const/4 v8, -0x1

    const/4 v0, 0x1

    const/4 v2, 0x0

    .line 28
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    move-object v0, v2

    .line 46
    :goto_0
    return-object v0

    .line 29
    :cond_1
    const-string/jumbo v1, "[*.]"

    invoke-virtual {p0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 31
    new-instance v1, Lcom/google/android/apps/chrome/preferences/website/WebsiteAddress;

    const/4 v3, 0x4

    invoke-virtual {p0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v2, v3, v0}, Lcom/google/android/apps/chrome/preferences/website/WebsiteAddress;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    move-object v0, v1

    goto :goto_0

    .line 36
    :cond_2
    const-string/jumbo v1, "://"

    invoke-virtual {p0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    if-eq v1, v8, :cond_5

    .line 38
    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 39
    new-instance v1, Lcom/google/android/apps/chrome/preferences/website/WebsiteAddress;

    invoke-static {p0}, Lcom/google/android/apps/chrome/preferences/website/WebsiteAddress;->trimTrailingBackslash(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, "http"

    invoke-virtual {v2}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-virtual {v2}, Landroid/net/Uri;->getPort()I

    move-result v6

    if-eq v6, v8, :cond_3

    invoke-virtual {v2}, Landroid/net/Uri;->getPort()I

    move-result v2

    const/16 v6, 0x50

    if-ne v2, v6, :cond_4

    :cond_3
    :goto_1
    invoke-direct {v1, v3, v4, v5, v0}, Lcom/google/android/apps/chrome/preferences/website/WebsiteAddress;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    move-object v0, v1

    goto :goto_0

    :cond_4
    const/4 v0, 0x0

    goto :goto_1

    .line 46
    :cond_5
    new-instance v1, Lcom/google/android/apps/chrome/preferences/website/WebsiteAddress;

    invoke-direct {v1, v2, v2, p0, v0}, Lcom/google/android/apps/chrome/preferences/website/WebsiteAddress;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    move-object v0, v1

    goto :goto_0
.end method

.method private getDomainAndRegistry()Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 79
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/WebsiteAddress;->mOrigin:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 80
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/WebsiteAddress;->mOrigin:Ljava/lang/String;

    invoke-static {v0, v2}, Lorg/chromium/chrome/browser/UrlUtilities;->getDomainAndRegistry(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 83
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "http://"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/website/WebsiteAddress;->mHost:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v2}, Lorg/chromium/chrome/browser/UrlUtilities;->getDomainAndRegistry(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private getSubdomainsList()[Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 136
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/WebsiteAddress;->mOrigin:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 137
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/WebsiteAddress;->mOrigin:Ljava/lang/String;

    const-string/jumbo v1, "://"

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 138
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 139
    new-array v0, v2, [Ljava/lang/String;

    .line 147
    :goto_0
    return-object v0

    .line 140
    :cond_0
    add-int/lit8 v1, v0, 0x3

    .line 141
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/WebsiteAddress;->mOrigin:Ljava/lang/String;

    .line 146
    :goto_1
    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/website/WebsiteAddress;->getDomainAndRegistry()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    .line 147
    add-int/lit8 v3, v3, -0x1

    if-le v3, v1, :cond_2

    invoke-virtual {v0, v1, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "\\."

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 144
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/WebsiteAddress;->mHost:Ljava/lang/String;

    move v1, v2

    goto :goto_1

    .line 147
    :cond_2
    new-array v0, v2, [Ljava/lang/String;

    goto :goto_0
.end method

.method private static trimTrailingBackslash(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 153
    const-string/jumbo v0, "/"

    invoke-virtual {p0, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    :cond_0
    return-object p0
.end method


# virtual methods
.method public compareTo(Lcom/google/android/apps/chrome/preferences/website/WebsiteAddress;)I
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 106
    if-ne p0, p1, :cond_1

    .line 130
    :cond_0
    :goto_0
    return v2

    .line 107
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/website/WebsiteAddress;->getDomainAndRegistry()Ljava/lang/String;

    move-result-object v0

    .line 108
    invoke-direct {p1}, Lcom/google/android/apps/chrome/preferences/website/WebsiteAddress;->getDomainAndRegistry()Ljava/lang/String;

    move-result-object v3

    .line 109
    invoke-virtual {v0, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    .line 110
    if-eqz v0, :cond_2

    move v2, v0

    .line 111
    goto :goto_0

    .line 113
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/WebsiteAddress;->mScheme:Ljava/lang/String;

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    iget-object v3, p1, Lcom/google/android/apps/chrome/preferences/website/WebsiteAddress;->mScheme:Ljava/lang/String;

    if-nez v3, :cond_3

    move v2, v1

    :cond_3
    if-eq v0, v2, :cond_6

    .line 114
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/WebsiteAddress;->mScheme:Ljava/lang/String;

    if-nez v0, :cond_5

    const/4 v2, -0x1

    goto :goto_0

    :cond_4
    move v0, v2

    .line 113
    goto :goto_1

    :cond_5
    move v2, v1

    .line 114
    goto :goto_0

    .line 115
    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/WebsiteAddress;->mScheme:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 116
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/WebsiteAddress;->mScheme:Ljava/lang/String;

    iget-object v1, p1, Lcom/google/android/apps/chrome/preferences/website/WebsiteAddress;->mScheme:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    .line 117
    if-nez v2, :cond_0

    .line 121
    :cond_7
    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/website/WebsiteAddress;->getSubdomainsList()[Ljava/lang/String;

    move-result-object v4

    .line 122
    invoke-direct {p1}, Lcom/google/android/apps/chrome/preferences/website/WebsiteAddress;->getSubdomainsList()[Ljava/lang/String;

    move-result-object v5

    .line 123
    array-length v0, v4

    add-int/lit8 v1, v0, -0x1

    .line 124
    array-length v0, v5

    add-int/lit8 v0, v0, -0x1

    .line 125
    :goto_2
    if-ltz v1, :cond_8

    if-ltz v0, :cond_8

    .line 126
    add-int/lit8 v3, v1, -0x1

    aget-object v2, v4, v1

    add-int/lit8 v1, v0, -0x1

    aget-object v0, v5, v0

    invoke-virtual {v2, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    .line 127
    if-nez v2, :cond_0

    move v0, v1

    move v1, v3

    .line 129
    goto :goto_2

    .line 130
    :cond_8
    sub-int v2, v1, v0

    goto :goto_0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 17
    check-cast p1, Lcom/google/android/apps/chrome/preferences/website/WebsiteAddress;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/preferences/website/WebsiteAddress;->compareTo(Lcom/google/android/apps/chrome/preferences/website/WebsiteAddress;)I

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 88
    instance-of v1, p1, Lcom/google/android/apps/chrome/preferences/website/WebsiteAddress;

    if-eqz v1, :cond_0

    .line 89
    check-cast p1, Lcom/google/android/apps/chrome/preferences/website/WebsiteAddress;

    .line 90
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/preferences/website/WebsiteAddress;->compareTo(Lcom/google/android/apps/chrome/preferences/website/WebsiteAddress;)I

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 92
    :cond_0
    return v0
.end method

.method public getHost()Ljava/lang/String;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/WebsiteAddress;->mHost:Ljava/lang/String;

    return-object v0
.end method

.method public getOrigin()Ljava/lang/String;
    .locals 2

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/WebsiteAddress;->mOrigin:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/preferences/website/WebsiteAddress;->mOmitProtocolAndPort:Z

    if-eqz v0, :cond_0

    .line 62
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "http://"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/website/WebsiteAddress;->mHost:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 64
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/WebsiteAddress;->mOrigin:Ljava/lang/String;

    goto :goto_0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/WebsiteAddress;->mOrigin:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/preferences/website/WebsiteAddress;->mOmitProtocolAndPort:Z

    if-eqz v0, :cond_1

    .line 73
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/WebsiteAddress;->mHost:Ljava/lang/String;

    .line 75
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/WebsiteAddress;->mOrigin:Ljava/lang/String;

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 97
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/WebsiteAddress;->mOrigin:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 99
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/WebsiteAddress;->mScheme:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 100
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/apps/chrome/preferences/website/WebsiteAddress;->mHost:Ljava/lang/String;

    if-nez v2, :cond_2

    :goto_2
    add-int/2addr v0, v1

    .line 101
    return v0

    .line 97
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/WebsiteAddress;->mOrigin:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 99
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/WebsiteAddress;->mScheme:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    .line 100
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/website/WebsiteAddress;->mHost:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2
.end method
