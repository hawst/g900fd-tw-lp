.class public Lcom/google/android/apps/chrome/utilities/UnreleasedApiCompatibilityUtils;
.super Ljava/lang/Object;
.source "UnreleasedApiCompatibilityUtils.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    return-void
.end method

.method public static finishAndRemoveTask(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 61
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 65
    new-instance v0, Lcom/google/android/apps/chrome/utilities/UnreleasedApiCompatibilityUtils$FinishAndRemoveTaskWithRetry;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/utilities/UnreleasedApiCompatibilityUtils$FinishAndRemoveTaskWithRetry;-><init>(Landroid/app/Activity;)V

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/utilities/UnreleasedApiCompatibilityUtils$FinishAndRemoveTaskWithRetry;->run()V

    .line 69
    :goto_0
    return-void

    .line 67
    :cond_0
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto :goto_0
.end method

.method public static isInteractive(Landroid/os/PowerManager;)Z
    .locals 2

    .prologue
    .line 100
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x14

    if-lt v0, v1, :cond_0

    .line 101
    invoke-virtual {p0}, Landroid/os/PowerManager;->isInteractive()Z

    move-result v0

    .line 103
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v0

    goto :goto_0
.end method

.method public static setLocalOnly(I)I
    .locals 2

    .prologue
    .line 50
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x14

    if-lt v0, v1, :cond_0

    .line 51
    or-int/lit16 p0, p0, 0x100

    .line 53
    :cond_0
    return p0
.end method

.method public static setLocalOnly(Landroid/app/Notification$Builder;Z)Landroid/app/Notification$Builder;
    .locals 2

    .prologue
    .line 27
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x14

    if-lt v0, v1, :cond_0

    .line 28
    invoke-virtual {p0, p1}, Landroid/app/Notification$Builder;->setLocalOnly(Z)Landroid/app/Notification$Builder;

    move-result-object p0

    .line 30
    :cond_0
    return-object p0
.end method

.method public static setLocalOnly(Landroid/support/v4/app/L;Z)Landroid/support/v4/app/L;
    .locals 2

    .prologue
    .line 39
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x14

    if-lt v0, v1, :cond_0

    .line 40
    invoke-virtual {p0, p1}, Landroid/support/v4/app/L;->c(Z)Landroid/support/v4/app/L;

    move-result-object p0

    .line 42
    :cond_0
    return-object p0
.end method
