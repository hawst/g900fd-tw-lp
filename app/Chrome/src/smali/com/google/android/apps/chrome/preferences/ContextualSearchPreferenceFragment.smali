.class public Lcom/google/android/apps/chrome/preferences/ContextualSearchPreferenceFragment;
.super Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;
.source "ContextualSearchPreferenceFragment.java"


# instance fields
.field private mActionBarSwitch:Landroid/widget/Switch;

.field private mContextualSearchEnabler:Lcom/google/android/apps/chrome/preferences/WidgetEnabler;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 24
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    .line 25
    sget v0, Lcom/google/android/apps/chrome/R$xml;->contextual_search_preferences:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/ContextualSearchPreferenceFragment;->addPreferencesFromResource(I)V

    .line 26
    new-instance v0, Landroid/widget/Switch;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/ContextualSearchPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/Switch;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/preferences/ContextualSearchPreferenceFragment;->mActionBarSwitch:Landroid/widget/Switch;

    .line 27
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ContextualSearchPreferenceFragment;->mActionBarSwitch:Landroid/widget/Switch;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/ContextualSearchPreferenceFragment;->addSwitchToActionBar(Landroid/widget/Switch;)V

    .line 28
    new-instance v0, Lcom/google/android/apps/chrome/preferences/ContextualSearchEnabler;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/ContextualSearchPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/preferences/ContextualSearchPreferenceFragment;->mActionBarSwitch:Landroid/widget/Switch;

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/chrome/preferences/ContextualSearchEnabler;-><init>(Landroid/content/Context;Landroid/widget/Switch;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/preferences/ContextualSearchPreferenceFragment;->mContextualSearchEnabler:Lcom/google/android/apps/chrome/preferences/WidgetEnabler;

    .line 29
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ContextualSearchPreferenceFragment;->mContextualSearchEnabler:Lcom/google/android/apps/chrome/preferences/WidgetEnabler;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/preferences/WidgetEnabler;->attach()V

    .line 31
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/ContextualSearchPreferenceFragment;->setHasOptionsMenu(Z)V

    .line 32
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 36
    invoke-super {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;->onDestroy()V

    .line 37
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ContextualSearchPreferenceFragment;->mContextualSearchEnabler:Lcom/google/android/apps/chrome/preferences/WidgetEnabler;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/preferences/WidgetEnabler;->destroy()V

    .line 38
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ContextualSearchPreferenceFragment;->mActionBarSwitch:Landroid/widget/Switch;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/Switch;->setVisibility(I)V

    .line 39
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    .line 43
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sget v1, Lcom/google/android/apps/chrome/R$id;->menu_id_contextual_search_learn:I

    if-eq v0, v1, :cond_0

    .line 44
    const/4 v0, 0x0

    .line 49
    :goto_0
    return v0

    .line 47
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/ContextualSearchPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$string;->learn_more:I

    sget v2, Lcom/google/android/apps/chrome/R$string;->contextual_search_learn_more_url:I

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/EmbedContentViewActivity;->show(Landroid/content/Context;II)V

    .line 49
    const/4 v0, 0x1

    goto :goto_0
.end method
