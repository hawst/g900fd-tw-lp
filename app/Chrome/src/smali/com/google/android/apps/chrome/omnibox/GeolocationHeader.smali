.class public Lcom/google/android/apps/chrome/omnibox/GeolocationHeader;
.super Ljava/lang/Object;
.source "GeolocationHeader.java"


# static fields
.field public static final UMA_DOMAIN_NOT_WHITELISTED:I = 0x7

.field public static final UMA_HEADER_SENT:I = 0x4

.field public static final UMA_LOCATION_DISABLED_FOR_GOOGLE_DOMAIN:I = 0x0

.field public static final UMA_LOCATION_NOT_AVAILABLE:I = 0x2

.field public static final UMA_LOCATION_STALE:I = 0x3

.field public static final UMA_MAX:I = 0x8

.field private static final WHITELISTED_DOMAINS:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 53
    const/16 v0, 0x31

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "com.ar"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string/jumbo v2, "com.au"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string/jumbo v2, "at"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string/jumbo v2, "be"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string/jumbo v2, "com.br"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string/jumbo v2, "bg"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "ca"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "cl"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string/jumbo v2, "cn"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string/jumbo v2, "com"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string/jumbo v2, "cz"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string/jumbo v2, "dk"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string/jumbo v2, "ee"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string/jumbo v2, "fi"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string/jumbo v2, "fr"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string/jumbo v2, "de"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string/jumbo v2, "com.hk"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string/jumbo v2, "hu"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string/jumbo v2, "co.in"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string/jumbo v2, "ie"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string/jumbo v2, "co.il"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string/jumbo v2, "it"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string/jumbo v2, "ci"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string/jumbo v2, "co.jp"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string/jumbo v2, "co.ke"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string/jumbo v2, "co.kr"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string/jumbo v2, "li"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string/jumbo v2, "lu"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string/jumbo v2, "com.my"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string/jumbo v2, "com.mx"

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string/jumbo v2, "nl"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string/jumbo v2, "co.nz"

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string/jumbo v2, "no"

    aput-object v2, v0, v1

    const/16 v1, 0x21

    const-string/jumbo v2, "com.pe"

    aput-object v2, v0, v1

    const/16 v1, 0x22

    const-string/jumbo v2, "pl"

    aput-object v2, v0, v1

    const/16 v1, 0x23

    const-string/jumbo v2, "pt"

    aput-object v2, v0, v1

    const/16 v1, 0x24

    const-string/jumbo v2, "ro"

    aput-object v2, v0, v1

    const/16 v1, 0x25

    const-string/jumbo v2, "ru"

    aput-object v2, v0, v1

    const/16 v1, 0x26

    const-string/jumbo v2, "com.sg"

    aput-object v2, v0, v1

    const/16 v1, 0x27

    const-string/jumbo v2, "co.za"

    aput-object v2, v0, v1

    const/16 v1, 0x28

    const-string/jumbo v2, "es"

    aput-object v2, v0, v1

    const/16 v1, 0x29

    const-string/jumbo v2, "se"

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    const-string/jumbo v2, "ch"

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    const-string/jumbo v2, "com.tw"

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    const-string/jumbo v2, "co.th"

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    const-string/jumbo v2, "com.tr"

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    const-string/jumbo v2, "co.uk"

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    const-string/jumbo v2, "com.ua"

    aput-object v2, v0, v1

    const/16 v1, 0x30

    const-string/jumbo v2, "co.ve"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/chrome/omnibox/GeolocationHeader;->WHITELISTED_DOMAINS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getGeoHeader(Landroid/content/Context;Ljava/lang/String;Z)Ljava/lang/String;
    .locals 12

    .prologue
    const/4 v9, 0x0

    const-wide v10, 0x416312d000000000L    # 1.0E7

    const/4 v8, 0x2

    const/16 v6, 0x8

    const/4 v0, 0x0

    .line 132
    if-eqz p2, :cond_1

    .line 183
    :cond_0
    :goto_0
    return-object v0

    .line 137
    :cond_1
    invoke-static {p1}, Lorg/chromium/chrome/browser/UrlUtilities;->nativeIsGoogleSearchUrl(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 142
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 143
    invoke-static {v1}, Lcom/google/android/apps/chrome/omnibox/GeolocationHeader;->isDomainWhitelisted(Landroid/net/Uri;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 144
    const/4 v1, 0x7

    invoke-static {v1, v6}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->geolocationHeaderHistogram(II)V

    goto :goto_0

    .line 149
    :cond_2
    invoke-static {v1}, Lcom/google/android/apps/chrome/omnibox/GeolocationHeader;->isLocationDisabledForUrl(Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 150
    invoke-static {v9, v6}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->geolocationHeaderHistogram(II)V

    goto :goto_0

    .line 155
    :cond_3
    invoke-static {p0}, Lcom/google/android/apps/chrome/omnibox/GeolocationTracker;->getLastKnownLocation(Landroid/content/Context;)Landroid/location/Location;

    move-result-object v1

    .line 156
    if-nez v1, :cond_4

    .line 157
    invoke-static {v8, v6}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->geolocationHeaderHistogram(II)V

    goto :goto_0

    .line 160
    :cond_4
    invoke-static {v1}, Lcom/google/android/apps/chrome/omnibox/GeolocationTracker;->getLocationAge(Landroid/location/Location;)J

    move-result-wide v2

    const-wide/32 v4, 0x5265c00

    cmp-long v2, v2, v4

    if-lez v2, :cond_5

    .line 161
    const/4 v1, 0x3

    invoke-static {v1, v6}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->geolocationHeaderHistogram(II)V

    goto :goto_0

    .line 165
    :cond_5
    const/4 v0, 0x4

    invoke-static {v0, v6}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->geolocationHeaderHistogram(II)V

    .line 168
    invoke-virtual {v1}, Landroid/location/Location;->getTime()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    .line 170
    invoke-virtual {v1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v4

    mul-double/2addr v4, v10

    double-to-int v0, v4

    .line 172
    invoke-virtual {v1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    mul-double/2addr v4, v10

    double-to-int v4, v4

    .line 174
    invoke-virtual {v1}, Landroid/location/Location;->getAccuracy()F

    move-result v1

    const/high16 v5, 0x447a0000    # 1000.0f

    mul-float/2addr v1, v5

    float-to-int v1, v1

    .line 178
    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string/jumbo v6, "role:1 producer:12 timestamp:%d latlng{latitude_e7:%d longitude_e7:%d} radius:%d"

    const/4 v7, 0x4

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v7, v9

    const/4 v2, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v7, v2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v7, v8

    const/4 v0, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v7, v0

    invoke-static {v5, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 181
    new-instance v1, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-static {v0, v8}, Landroid/util/Base64;->encode([BI)[B

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([B)V

    .line 183
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "X-Geo: a "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0
.end method

.method private static isDomainWhitelisted(Landroid/net/Uri;)Z
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 196
    const-string/jumbo v1, "https"

    invoke-virtual {p0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 203
    :cond_0
    :goto_0
    return v0

    .line 198
    :cond_1
    invoke-virtual {p0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v2

    .line 199
    if-eqz v2, :cond_0

    .line 200
    sget-object v3, Lcom/google/android/apps/chrome/omnibox/GeolocationHeader;->WHITELISTED_DOMAINS:[Ljava/lang/String;

    array-length v4, v3

    move v1, v0

    :goto_1
    if-ge v1, v4, :cond_0

    aget-object v5, v3, v1

    .line 201
    invoke-virtual {v2, v5}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "google."

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    .line 200
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method private static isLocationDisabledForUrl(Landroid/net/Uri;)Z
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 212
    new-instance v0, Lcom/google/android/apps/chrome/preferences/website/GeolocationInfo;

    invoke-virtual {p0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/chrome/preferences/website/GeolocationInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 213
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/website/GeolocationInfo;->getAllowed()Ljava/lang/Boolean;

    move-result-object v0

    .line 217
    if-nez v0, :cond_0

    .line 218
    invoke-virtual {p0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    .line 219
    if-eqz v1, :cond_0

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "https"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Landroid/net/Uri;->getUserInfo()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    .line 221
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "http://"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 222
    new-instance v1, Lcom/google/android/apps/chrome/preferences/website/GeolocationInfo;

    invoke-direct {v1, v0, v3}, Lcom/google/android/apps/chrome/preferences/website/GeolocationInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 223
    invoke-virtual {v1}, Lcom/google/android/apps/chrome/preferences/website/GeolocationInfo;->getAllowed()Ljava/lang/Boolean;

    move-result-object v0

    .line 227
    :cond_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static primeLocationForGeoHeader(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 112
    const-wide/32 v0, 0x493e0

    invoke-static {p0, v0, v1}, Lcom/google/android/apps/chrome/omnibox/GeolocationTracker;->refreshLastKnownLocation(Landroid/content/Context;J)V

    .line 113
    return-void
.end method
