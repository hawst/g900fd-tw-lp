.class public final enum Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;
.super Ljava/lang/Enum;
.source "ContextualSearchState.java"


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

.field public static final enum BACK_PRESS:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

.field public static final enum BASE_PAGE_SCROLL:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

.field public static final enum BASE_PAGE_TAP:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

.field public static final enum CLICK:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

.field public static final enum FLING:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

.field public static final enum INVALID_SELECTION:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

.field public static final enum OPTIN:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

.field public static final enum OPTOUT:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

.field public static final enum RESET:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

.field public static final enum SEARCH_BAR_TAP:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

.field public static final enum SERP_NAVIGATION:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

.field public static final enum SWIPE:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

.field public static final enum TAB_PROMOTION:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

.field public static final enum TEXT_SELECT_LONG_PRESS:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

.field public static final enum TEXT_SELECT_TAP:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

.field public static final enum UNKNOWN:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 33
    new-instance v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    const-string/jumbo v1, "UNKNOWN"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->UNKNOWN:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    .line 34
    new-instance v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    const-string/jumbo v1, "RESET"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->RESET:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    .line 35
    new-instance v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    const-string/jumbo v1, "BACK_PRESS"

    invoke-direct {v0, v1, v5}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->BACK_PRESS:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    .line 36
    new-instance v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    const-string/jumbo v1, "TEXT_SELECT_TAP"

    invoke-direct {v0, v1, v6}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->TEXT_SELECT_TAP:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    .line 37
    new-instance v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    const-string/jumbo v1, "TEXT_SELECT_LONG_PRESS"

    invoke-direct {v0, v1, v7}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->TEXT_SELECT_LONG_PRESS:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    .line 38
    new-instance v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    const-string/jumbo v1, "INVALID_SELECTION"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->INVALID_SELECTION:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    .line 39
    new-instance v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    const-string/jumbo v1, "BASE_PAGE_TAP"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->BASE_PAGE_TAP:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    .line 40
    new-instance v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    const-string/jumbo v1, "BASE_PAGE_SCROLL"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->BASE_PAGE_SCROLL:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    .line 41
    new-instance v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    const-string/jumbo v1, "SEARCH_BAR_TAP"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->SEARCH_BAR_TAP:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    .line 42
    new-instance v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    const-string/jumbo v1, "SERP_NAVIGATION"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->SERP_NAVIGATION:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    .line 43
    new-instance v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    const-string/jumbo v1, "TAB_PROMOTION"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->TAB_PROMOTION:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    .line 44
    new-instance v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    const-string/jumbo v1, "CLICK"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->CLICK:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    .line 45
    new-instance v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    const-string/jumbo v1, "SWIPE"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->SWIPE:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    .line 46
    new-instance v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    const-string/jumbo v1, "FLING"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->FLING:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    .line 47
    new-instance v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    const-string/jumbo v1, "OPTIN"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->OPTIN:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    .line 48
    new-instance v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    const-string/jumbo v1, "OPTOUT"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->OPTOUT:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    .line 32
    const/16 v0, 0x10

    new-array v0, v0, [Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    sget-object v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->UNKNOWN:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->RESET:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->BACK_PRESS:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->TEXT_SELECT_TAP:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->TEXT_SELECT_LONG_PRESS:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->INVALID_SELECTION:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->BASE_PAGE_TAP:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->BASE_PAGE_SCROLL:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->SEARCH_BAR_TAP:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->SERP_NAVIGATION:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->TAB_PROMOTION:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->CLICK:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->SWIPE:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->FLING:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->OPTIN:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->OPTOUT:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->$VALUES:[Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;
    .locals 1

    .prologue
    .line 32
    const-class v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;
    .locals 1

    .prologue
    .line 32
    sget-object v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->$VALUES:[Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    invoke-virtual {v0}, [Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    return-object v0
.end method
