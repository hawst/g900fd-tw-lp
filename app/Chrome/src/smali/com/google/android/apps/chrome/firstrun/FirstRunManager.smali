.class public Lcom/google/android/apps/chrome/firstrun/FirstRunManager;
.super Ljava/lang/Object;
.source "FirstRunManager.java"


# static fields
.field public static final LAUNCH_CHROME_WITH_FRE_EXTRA:Ljava/lang/String; = "Launch Chrome with FRE"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    return-void
.end method


# virtual methods
.method checkIsSyncAllowed(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 43
    invoke-static {p1}, Lorg/chromium/chrome/browser/util/FeatureUtilities;->canAllowSync(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Lorg/chromium/chrome/browser/signin/SigninManager;->get(Landroid/content/Context;)Lorg/chromium/chrome/browser/signin/SigninManager;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/signin/SigninManager;->isSigninDisabledByPolicy()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method checkIsSystemInstall(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 54
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    iget v0, v0, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method testableCheckAnyUserHasSeenToS(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 33
    invoke-static {p1}, Lcom/google/android/apps/chrome/ToSAckedReceiver;->checkAnyUserHasSeenToS(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method
