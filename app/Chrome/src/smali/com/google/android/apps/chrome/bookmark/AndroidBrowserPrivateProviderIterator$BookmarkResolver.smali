.class Lcom/google/android/apps/chrome/bookmark/AndroidBrowserPrivateProviderIterator$BookmarkResolver;
.super Ljava/lang/Object;
.source "AndroidBrowserPrivateProviderIterator.java"


# instance fields
.field private final mContentResolver:Landroid/content/ContentResolver;

.field private final mDatabaseProvider:Lcom/google/android/apps/chrome/bookmark/AndroidBrowserDatabaseProvider;


# direct methods
.method constructor <init>(Landroid/content/ContentResolver;)V
    .locals 1

    .prologue
    .line 103
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 104
    iput-object p1, p0, Lcom/google/android/apps/chrome/bookmark/AndroidBrowserPrivateProviderIterator$BookmarkResolver;->mContentResolver:Landroid/content/ContentResolver;

    .line 105
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AndroidBrowserPrivateProviderIterator$BookmarkResolver;->mDatabaseProvider:Lcom/google/android/apps/chrome/bookmark/AndroidBrowserDatabaseProvider;

    .line 106
    return-void
.end method

.method constructor <init>(Lcom/google/android/apps/chrome/bookmark/AndroidBrowserDatabaseProvider;)V
    .locals 1

    .prologue
    .line 108
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 109
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AndroidBrowserPrivateProviderIterator$BookmarkResolver;->mContentResolver:Landroid/content/ContentResolver;

    .line 110
    iput-object p1, p0, Lcom/google/android/apps/chrome/bookmark/AndroidBrowserPrivateProviderIterator$BookmarkResolver;->mDatabaseProvider:Lcom/google/android/apps/chrome/bookmark/AndroidBrowserDatabaseProvider;

    .line 111
    return-void
.end method


# virtual methods
.method public close()V
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AndroidBrowserPrivateProviderIterator$BookmarkResolver;->mDatabaseProvider:Lcom/google/android/apps/chrome/bookmark/AndroidBrowserDatabaseProvider;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AndroidBrowserPrivateProviderIterator$BookmarkResolver;->mDatabaseProvider:Lcom/google/android/apps/chrome/bookmark/AndroidBrowserDatabaseProvider;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/bookmark/AndroidBrowserDatabaseProvider;->close()V

    .line 122
    :cond_0
    return-void
.end method

.method public getContentResolver()Landroid/content/ContentResolver;
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AndroidBrowserPrivateProviderIterator$BookmarkResolver;->mContentResolver:Landroid/content/ContentResolver;

    return-object v0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 6

    .prologue
    .line 115
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AndroidBrowserPrivateProviderIterator$BookmarkResolver;->mDatabaseProvider:Lcom/google/android/apps/chrome/bookmark/AndroidBrowserDatabaseProvider;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AndroidBrowserPrivateProviderIterator$BookmarkResolver;->mDatabaseProvider:Lcom/google/android/apps/chrome/bookmark/AndroidBrowserDatabaseProvider;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/chrome/bookmark/AndroidBrowserDatabaseProvider;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AndroidBrowserPrivateProviderIterator$BookmarkResolver;->mContentResolver:Landroid/content/ContentResolver;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    goto :goto_0
.end method
