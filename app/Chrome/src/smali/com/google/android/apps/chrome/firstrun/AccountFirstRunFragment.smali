.class public Lcom/google/android/apps/chrome/firstrun/AccountFirstRunFragment;
.super Lcom/google/android/apps/chrome/firstrun/FirstRunPage;
.source "AccountFirstRunFragment.java"


# static fields
.field public static final FORCE_SIGNIN_ACCOUNT_TO:Ljava/lang/String; = "ForceSigninAccountTo"

.field public static final FORCE_SIGNIN_AND_DISABLE_NO_THANKS:Ljava/lang/String; = "ForceSigninAndDisableNoThanks"

.field public static final PRESELECT_BUT_ALLOW_TO_CHANGE:Ljava/lang/String; = "PreselectButAllowToChange"


# instance fields
.field private mView:Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunPage;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/firstrun/AccountFirstRunFragment;)Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunFragment;->mView:Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;

    return-object v0
.end method


# virtual methods
.method protected interceptBackPressed()Z
    .locals 2

    .prologue
    .line 104
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunFragment;->mView:Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->isSignedIn()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunFragment;->mView:Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->isInForcedAccountMode()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunFragment;->getProperties()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "PreselectButAllowToChange"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 107
    :cond_0
    invoke-super {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunPage;->interceptBackPressed()Z

    move-result v0

    .line 120
    :goto_0
    return v0

    .line 110
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunFragment;->mView:Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->isInForcedAccountMode()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunFragment;->getProperties()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "PreselectButAllowToChange"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 114
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunFragment;->getProperties()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "ForceSigninAccountTo"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 119
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunFragment;->getPageDelegate()Lcom/google/android/apps/chrome/firstrun/FirstRunPageDelegate;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/firstrun/FirstRunPageDelegate;->recreateCurrentPage()V

    .line 120
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 30
    sget v0, Lcom/google/android/apps/chrome/R$layout;->fre_choose_account:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;

    iput-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunFragment;->mView:Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;

    .line 32
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunFragment;->mView:Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;

    return-object v0
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 93
    invoke-super {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunPage;->onStart()V

    .line 94
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunFragment;->getView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->setButtonsEnabled(Z)V

    .line 95
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunFragment;->getView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunFragment;->getPageDelegate()Lcom/google/android/apps/chrome/firstrun/FirstRunPageDelegate;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/chrome/firstrun/FirstRunPageDelegate;->getProfileDataCache()Lcom/google/android/apps/chrome/firstrun/ProfileDataCache;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->setProfileDataCache(Lcom/google/android/apps/chrome/firstrun/ProfileDataCache;)V

    .line 97
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunFragment;->getPageDelegate()Lcom/google/android/apps/chrome/firstrun/FirstRunPageDelegate;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/firstrun/FirstRunPageDelegate;->onSigninDialogShown()V

    .line 98
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 37
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/chrome/firstrun/FirstRunPage;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 39
    check-cast p1, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;

    .line 40
    new-instance v0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunFragment$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunFragment$1;-><init>(Lcom/google/android/apps/chrome/firstrun/AccountFirstRunFragment;)V

    invoke-virtual {p1, v0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->setListener(Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView$Listener;)V

    .line 78
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunFragment;->getPageDelegate()Lcom/google/android/apps/chrome/firstrun/FirstRunPageDelegate;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/firstrun/FirstRunPageDelegate;->getProfileDataCache()Lcom/google/android/apps/chrome/firstrun/ProfileDataCache;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->init(Lcom/google/android/apps/chrome/firstrun/ProfileDataCache;)V

    .line 80
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunFragment;->getProperties()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "ForceSigninAndDisableNoThanks"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 81
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->setCanCancel(Z)V

    .line 84
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunFragment;->getProperties()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "ForceSigninAccountTo"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 86
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 87
    invoke-virtual {p1, v0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->switchToForcedAccountMode(Ljava/lang/String;)V

    .line 89
    :cond_1
    return-void
.end method
