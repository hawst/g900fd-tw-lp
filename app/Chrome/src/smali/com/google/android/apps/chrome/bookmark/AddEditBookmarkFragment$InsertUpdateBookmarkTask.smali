.class Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$InsertUpdateBookmarkTask;
.super Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment$FragmentAsyncTask;
.source "AddEditBookmarkFragment.java"


# instance fields
.field private mBookmarkId:Ljava/lang/Long;

.field private final mBookmarkValues:Landroid/content/ContentValues;

.field private final mContext:Landroid/content/Context;

.field final synthetic this$0:Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;Landroid/content/ContentValues;Ljava/lang/Long;)V
    .locals 1

    .prologue
    .line 595
    iput-object p1, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$InsertUpdateBookmarkTask;->this$0:Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment$FragmentAsyncTask;-><init>(Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment;)V

    .line 596
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$InsertUpdateBookmarkTask;->mContext:Landroid/content/Context;

    .line 597
    iput-object p2, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$InsertUpdateBookmarkTask;->mBookmarkValues:Landroid/content/ContentValues;

    .line 598
    iput-object p3, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$InsertUpdateBookmarkTask;->mBookmarkId:Ljava/lang/Long;

    .line 599
    return-void
.end method


# virtual methods
.method protected onTaskFinished()V
    .locals 4

    .prologue
    .line 618
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$InsertUpdateBookmarkTask;->this$0:Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->isFolder()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 619
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$InsertUpdateBookmarkTask;->this$0:Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;

    # getter for: Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mActionListener:Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$OnActionListener;
    invoke-static {v0}, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->access$800(Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;)Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$OnActionListener;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$InsertUpdateBookmarkTask;->mBookmarkId:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iget-object v1, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$InsertUpdateBookmarkTask;->this$0:Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;

    # getter for: Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mTitleInput:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->access$500(Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v2, v3, v1}, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$OnActionListener;->onFolderCreated(JLjava/lang/String;)V

    .line 623
    :goto_0
    return-void

    .line 621
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$InsertUpdateBookmarkTask;->this$0:Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;

    # getter for: Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mActionListener:Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$OnActionListener;
    invoke-static {v0}, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->access$800(Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;)Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$OnActionListener;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$InsertUpdateBookmarkTask;->mBookmarkId:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-interface {v0, v2, v3}, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$OnActionListener;->onNodeEdited(J)V

    goto :goto_0
.end method

.method protected runBackgroundTask()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 603
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$InsertUpdateBookmarkTask;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->getBookmarksUri(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v0

    .line 604
    iget-object v1, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$InsertUpdateBookmarkTask;->mBookmarkId:Ljava/lang/Long;

    if-nez v1, :cond_1

    .line 605
    iget-object v1, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$InsertUpdateBookmarkTask;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$InsertUpdateBookmarkTask;->mBookmarkValues:Landroid/content/ContentValues;

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v0

    .line 607
    if-eqz v0, :cond_0

    invoke-static {v0}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v0

    :goto_0
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$InsertUpdateBookmarkTask;->mBookmarkId:Ljava/lang/Long;

    .line 614
    :goto_1
    return-void

    .line 607
    :cond_0
    const-wide/16 v0, -0x1

    goto :goto_0

    .line 610
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$InsertUpdateBookmarkTask;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$InsertUpdateBookmarkTask;->mBookmarkId:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$InsertUpdateBookmarkTask;->mBookmarkValues:Landroid/content/ContentValues;

    invoke-virtual {v1, v0, v2, v4, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_1
.end method

.method protected setDependentUIEnabled(Z)V
    .locals 1

    .prologue
    .line 627
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$InsertUpdateBookmarkTask;->this$0:Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;

    # getter for: Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mOkButton:Landroid/widget/Button;
    invoke-static {v0}, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->access$200(Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 628
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$InsertUpdateBookmarkTask;->this$0:Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;

    # getter for: Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mRemoveButton:Landroid/widget/Button;
    invoke-static {v0}, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->access$700(Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 629
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$InsertUpdateBookmarkTask;->this$0:Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;

    # getter for: Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mCancelButton:Landroid/widget/Button;
    invoke-static {v0}, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->access$900(Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 630
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$InsertUpdateBookmarkTask;->this$0:Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;

    # getter for: Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mActionListener:Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$OnActionListener;
    invoke-static {v0}, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->access$800(Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;)Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$OnActionListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$OnActionListener;->setBackEnabled(Z)V

    .line 631
    return-void
.end method
