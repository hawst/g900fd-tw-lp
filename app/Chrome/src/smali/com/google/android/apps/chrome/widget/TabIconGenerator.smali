.class public Lcom/google/android/apps/chrome/widget/TabIconGenerator;
.super Ljava/lang/Object;
.source "TabIconGenerator.java"


# instance fields
.field private final mAppIconSizePx:I

.field private final mAppTextPaint:Landroid/text/TextPaint;

.field private final mBackgroundPaint:Landroid/graphics/Paint;

.field private final mBackgroundRect:Landroid/graphics/RectF;

.field private final mCornerRadiusPx:I

.field private final mTextHeight:F

.field private final mTextYOffset:F


# direct methods
.method public constructor <init>(Landroid/content/Context;IIII)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 51
    int-to-float v1, p2

    iget v2, v0, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, p0, Lcom/google/android/apps/chrome/widget/TabIconGenerator;->mAppIconSizePx:I

    .line 52
    int-to-float v1, p3

    iget v2, v0, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, p0, Lcom/google/android/apps/chrome/widget/TabIconGenerator;->mCornerRadiusPx:I

    .line 53
    new-instance v1, Landroid/graphics/RectF;

    iget v2, p0, Lcom/google/android/apps/chrome/widget/TabIconGenerator;->mAppIconSizePx:I

    int-to-float v2, v2

    iget v3, p0, Lcom/google/android/apps/chrome/widget/TabIconGenerator;->mAppIconSizePx:I

    int-to-float v3, v3

    invoke-direct {v1, v4, v4, v2, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v1, p0, Lcom/google/android/apps/chrome/widget/TabIconGenerator;->mBackgroundRect:Landroid/graphics/RectF;

    .line 55
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/chrome/widget/TabIconGenerator;->mBackgroundPaint:Landroid/graphics/Paint;

    .line 56
    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/TabIconGenerator;->mBackgroundPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, p5}, Landroid/graphics/Paint;->setColor(I)V

    .line 58
    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1, v5}, Landroid/text/TextPaint;-><init>(I)V

    iput-object v1, p0, Lcom/google/android/apps/chrome/widget/TabIconGenerator;->mAppTextPaint:Landroid/text/TextPaint;

    .line 59
    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/TabIconGenerator;->mAppTextPaint:Landroid/text/TextPaint;

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    .line 60
    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/TabIconGenerator;->mAppTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v5}, Landroid/text/TextPaint;->setFakeBoldText(Z)V

    .line 61
    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/TabIconGenerator;->mAppTextPaint:Landroid/text/TextPaint;

    const/4 v2, 0x2

    int-to-float v3, p4

    invoke-static {v2, v3, v0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    invoke-virtual {v1, v0}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 64
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/TabIconGenerator;->mAppTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v0}, Landroid/text/TextPaint;->getFontMetrics()Landroid/graphics/Paint$FontMetrics;

    move-result-object v0

    .line 65
    iget v1, v0, Landroid/graphics/Paint$FontMetrics;->bottom:F

    iget v2, v0, Landroid/graphics/Paint$FontMetrics;->top:F

    sub-float/2addr v1, v2

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-float v1, v2

    iput v1, p0, Lcom/google/android/apps/chrome/widget/TabIconGenerator;->mTextHeight:F

    .line 66
    iget v0, v0, Landroid/graphics/Paint$FontMetrics;->top:F

    neg-float v0, v0

    iput v0, p0, Lcom/google/android/apps/chrome/widget/TabIconGenerator;->mTextYOffset:F

    .line 67
    return-void
.end method


# virtual methods
.method public generateIconForTab(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 9

    .prologue
    const/4 v1, 0x0

    const/high16 v8, 0x40000000    # 2.0f

    const/4 v2, 0x0

    .line 76
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 103
    :goto_0
    return-object v0

    .line 77
    :cond_0
    invoke-static {p1, v2}, Lorg/chromium/chrome/browser/UrlUtilities;->getDomainAndRegistry(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 78
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 79
    const-string/jumbo v0, "chrome://"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string/jumbo v0, "chrome-native://"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 81
    :cond_1
    const-string/jumbo v0, "chrome"

    move-object v1, v0

    .line 87
    :goto_1
    iget v0, p0, Lcom/google/android/apps/chrome/widget/TabIconGenerator;->mAppIconSizePx:I

    iget v3, p0, Lcom/google/android/apps/chrome/widget/TabIconGenerator;->mAppIconSizePx:I

    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v3, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v7

    .line 88
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, v7}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 89
    iget-object v3, p0, Lcom/google/android/apps/chrome/widget/TabIconGenerator;->mBackgroundRect:Landroid/graphics/RectF;

    iget v4, p0, Lcom/google/android/apps/chrome/widget/TabIconGenerator;->mCornerRadiusPx:I

    int-to-float v4, v4

    iget v5, p0, Lcom/google/android/apps/chrome/widget/TabIconGenerator;->mCornerRadiusPx:I

    int-to-float v5, v5

    iget-object v6, p0, Lcom/google/android/apps/chrome/widget/TabIconGenerator;->mBackgroundPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v3, v4, v5, v6}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 91
    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    .line 92
    iget-object v3, p0, Lcom/google/android/apps/chrome/widget/TabIconGenerator;->mAppTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v3, v1}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v4

    .line 94
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    iget v5, p0, Lcom/google/android/apps/chrome/widget/TabIconGenerator;->mAppIconSizePx:I

    int-to-float v5, v5

    sub-float v4, v5, v4

    div-float/2addr v4, v8

    iget v5, p0, Lcom/google/android/apps/chrome/widget/TabIconGenerator;->mAppIconSizePx:I

    int-to-float v5, v5

    iget v6, p0, Lcom/google/android/apps/chrome/widget/TabIconGenerator;->mTextHeight:F

    invoke-static {v5, v6}, Ljava/lang/Math;->max(FF)F

    move-result v5

    iget v6, p0, Lcom/google/android/apps/chrome/widget/TabIconGenerator;->mTextHeight:F

    sub-float/2addr v5, v6

    div-float/2addr v5, v8

    iget v6, p0, Lcom/google/android/apps/chrome/widget/TabIconGenerator;->mTextYOffset:F

    add-float/2addr v5, v6

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v5

    int-to-float v5, v5

    iget-object v6, p0, Lcom/google/android/apps/chrome/widget/TabIconGenerator;->mAppTextPaint:Landroid/text/TextPaint;

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;IIFFLandroid/graphics/Paint;)V

    move-object v0, v7

    .line 103
    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 83
    goto :goto_0

    :cond_3
    move-object v1, v0

    goto :goto_1
.end method
