.class public interface abstract Lcom/google/android/apps/chrome/firstrun/FirstRunPageDelegate;
.super Ljava/lang/Object;
.source "FirstRunPageDelegate.java"


# virtual methods
.method public abstract abortFirstRunExperience()V
.end method

.method public abstract acceptSignIn(Ljava/lang/String;)V
.end method

.method public abstract acceptTermsOfService(Z)V
.end method

.method public abstract advanceToNextPage()V
.end method

.method public abstract askToOpenSyncSettings()V
.end method

.method public abstract getProfileDataCache()Lcom/google/android/apps/chrome/firstrun/ProfileDataCache;
.end method

.method public abstract isNeverUploadCrashDump()Z
.end method

.method public abstract onSigninDialogShown()V
.end method

.method public abstract openAccountAdder(Landroid/app/Fragment;)V
.end method

.method public abstract openChromePreferencesPage(Landroid/app/Fragment;Ljava/lang/String;I)V
.end method

.method public abstract recreateCurrentPage()V
.end method

.method public abstract refuseSignIn()V
.end method

.method public abstract skipIntroPages()V
.end method
