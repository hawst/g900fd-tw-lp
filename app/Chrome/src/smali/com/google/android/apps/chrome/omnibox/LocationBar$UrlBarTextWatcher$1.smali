.class Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarTextWatcher$1;
.super Ljava/lang/Object;
.source "LocationBar.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic this$1:Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarTextWatcher;

.field final synthetic val$editableText:Landroid/text/Editable;

.field final synthetic val$textWithoutAutocomplete:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarTextWatcher;Landroid/text/Editable;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 304
    iput-object p1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarTextWatcher$1;->this$1:Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarTextWatcher;

    iput-object p2, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarTextWatcher$1;->val$editableText:Landroid/text/Editable;

    iput-object p3, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarTextWatcher$1;->val$textWithoutAutocomplete:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 307
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarTextWatcher$1;->this$1:Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarTextWatcher;

    iget-object v0, v0, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarTextWatcher;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    # getter for: Lcom/google/android/apps/chrome/omnibox/LocationBar;->mLastUrlEditWasDelete:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->access$1100(Lcom/google/android/apps/chrome/omnibox/LocationBar;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarTextWatcher$1;->this$1:Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarTextWatcher;

    iget-object v0, v0, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarTextWatcher;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    # invokes: Lcom/google/android/apps/chrome/omnibox/LocationBar;->shouldAutocomplete()Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->access$1200(Lcom/google/android/apps/chrome/omnibox/LocationBar;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarTextWatcher$1;->val$editableText:Landroid/text/Editable;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarTextWatcher$1;->val$editableText:Landroid/text/Editable;

    invoke-static {v0}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarTextWatcher$1;->val$editableText:Landroid/text/Editable;

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    if-eq v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 311
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarTextWatcher$1;->this$1:Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarTextWatcher;

    iget-object v1, v1, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarTextWatcher;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    const/4 v2, 0x0

    # setter for: Lcom/google/android/apps/chrome/omnibox/LocationBar;->mRequestSuggestions:Ljava/lang/Runnable;
    invoke-static {v1, v2}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->access$1002(Lcom/google/android/apps/chrome/omnibox/LocationBar;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 312
    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarTextWatcher$1;->this$1:Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarTextWatcher;

    iget-object v1, v1, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarTextWatcher;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->getCurrentTab()Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v1

    if-nez v1, :cond_2

    .line 317
    :goto_1
    return-void

    .line 307
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 313
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarTextWatcher$1;->this$1:Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarTextWatcher;

    iget-object v1, v1, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarTextWatcher;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    iget-object v1, v1, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mAutocomplete:Lcom/google/android/apps/chrome/omnibox/ChromeAutocompleteController;

    iget-object v2, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarTextWatcher$1;->this$1:Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarTextWatcher;

    iget-object v2, v2, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarTextWatcher;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->getCurrentTab()Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getProfile()Lorg/chromium/chrome/browser/profiles/Profile;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarTextWatcher$1;->this$1:Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarTextWatcher;

    iget-object v3, v3, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarTextWatcher;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->getCurrentTab()Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getUrl()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$UrlBarTextWatcher$1;->val$textWithoutAutocomplete:Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v4, v0}, Lcom/google/android/apps/chrome/omnibox/ChromeAutocompleteController;->start(Lorg/chromium/chrome/browser/profiles/Profile;Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_1
.end method
