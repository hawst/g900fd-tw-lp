.class public final Lcom/google/android/apps/chrome/autofill/AutofillDataProvider$AddressColumns;
.super Ljava/lang/Object;
.source "AutofillDataProvider.java"


# static fields
.field public static final ADDRESS_CITY:Ljava/lang/String; = "ADDRESS_CITY"

.field public static final ADDRESS_COUNTRY_CODE:Ljava/lang/String; = "ADDRESS_COUNTRY_CODE"

.field public static final ADDRESS_LINE1:Ljava/lang/String; = "ADDRESS_LINE1"

.field public static final ADDRESS_LINE2:Ljava/lang/String; = "ADDRESS_LINE2"

.field public static final ADDRESS_STATE:Ljava/lang/String; = "ADDRESS_STATE"

.field public static final ADDRESS_ZIP:Ljava/lang/String; = "ADDRESS_ZIP"

.field public static final COMPANY_NAME:Ljava/lang/String; = "COMPANY_NAME"

.field public static final EMAIL_ADDRESS:Ljava/lang/String; = "EMAIL_ADDRESS"

.field public static final GUID:Ljava/lang/String; = "GUID"

.field public static final IS_DEFAULT_BILLING_ADDRESS:Ljava/lang/String; = "IS_DEFAULT_BILLING_ADDRESS"

.field public static final IS_DEFAULT_SHIPPING_ADDRESS:Ljava/lang/String; = "IS_DEFAULT_SHIPPING_ADDRESS"

.field public static final NAME_FULL:Ljava/lang/String; = "NAME_FULL"

.field public static final ORIGIN:Ljava/lang/String; = "ORIGIN"

.field public static final PHONE_WHOLE_NUMBER:Ljava/lang/String; = "PHONE_WHOLE_NUMBER"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
