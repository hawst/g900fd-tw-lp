.class Lcom/google/android/apps/chrome/webapps/WebappActivity$2;
.super Ljava/lang/Object;
.source "WebappActivity.java"

# interfaces
.implements Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/webapps/WebappActivity;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 188
    const-class v0, Lcom/google/android/apps/chrome/webapps/WebappActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/webapps/WebappActivity$2;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lcom/google/android/apps/chrome/webapps/WebappActivity;)V
    .locals 0

    .prologue
    .line 188
    iput-object p1, p0, Lcom/google/android/apps/chrome/webapps/WebappActivity$2;->this$0:Lcom/google/android/apps/chrome/webapps/WebappActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public isSwipeEnabled(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;)Z
    .locals 1

    .prologue
    .line 209
    sget-object v0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;->DOWN:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public swipeFinished()V
    .locals 0

    .prologue
    .line 205
    return-void
.end method

.method public swipeFlingOccurred(FFFFFF)V
    .locals 0

    .prologue
    .line 202
    return-void
.end method

.method public swipeStarted(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;FF)V
    .locals 2

    .prologue
    .line 194
    sget-boolean v0, Lcom/google/android/apps/chrome/webapps/WebappActivity$2;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;->DOWN:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    if-eq v0, p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 195
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/WebappActivity$2;->this$0:Lcom/google/android/apps/chrome/webapps/WebappActivity;

    # getter for: Lcom/google/android/apps/chrome/webapps/WebappActivity;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;
    invoke-static {v0}, Lcom/google/android/apps/chrome/webapps/WebappActivity;->access$100(Lcom/google/android/apps/chrome/webapps/WebappActivity;)Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->isOverlayVideoMode()Z

    move-result v0

    if-nez v0, :cond_1

    .line 196
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/WebappActivity$2;->this$0:Lcom/google/android/apps/chrome/webapps/WebappActivity;

    # getter for: Lcom/google/android/apps/chrome/webapps/WebappActivity;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;
    invoke-static {v0}, Lcom/google/android/apps/chrome/webapps/WebappActivity;->access$100(Lcom/google/android/apps/chrome/webapps/WebappActivity;)Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->setPersistentFullscreenMode(Z)V

    .line 198
    :cond_1
    return-void
.end method

.method public swipeUpdated(FFFFFF)V
    .locals 0

    .prologue
    .line 190
    return-void
.end method
