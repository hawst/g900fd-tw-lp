.class public Lcom/google/android/apps/chrome/autofill/AutofillDataProviderSessionInformation;
.super Ljava/lang/Object;
.source "AutofillDataProviderSessionInformation.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private final mGuidDefaultBilling:Ljava/lang/String;

.field private final mGuidDefaultCard:Ljava/lang/String;

.field private final mGuidDefaultShipping:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput-object p1, p0, Lcom/google/android/apps/chrome/autofill/AutofillDataProviderSessionInformation;->mGuidDefaultBilling:Ljava/lang/String;

    .line 45
    iput-object p2, p0, Lcom/google/android/apps/chrome/autofill/AutofillDataProviderSessionInformation;->mGuidDefaultShipping:Ljava/lang/String;

    .line 46
    iput-object p3, p0, Lcom/google/android/apps/chrome/autofill/AutofillDataProviderSessionInformation;->mGuidDefaultCard:Ljava/lang/String;

    .line 47
    return-void
.end method

.method public static deserialize(Ljava/lang/String;)Lcom/google/android/apps/chrome/autofill/AutofillDataProviderSessionInformation;
    .locals 4

    .prologue
    .line 102
    :try_start_0
    new-instance v0, Ljava/io/ObjectInputStream;

    new-instance v1, Landroid/util/Base64InputStream;

    new-instance v2, Ljava/io/ByteArrayInputStream;

    invoke-virtual {p0}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    const/16 v3, 0x8

    invoke-direct {v1, v2, v3}, Landroid/util/Base64InputStream;-><init>(Ljava/io/InputStream;I)V

    invoke-direct {v0, v1}, Ljava/io/ObjectInputStream;-><init>(Ljava/io/InputStream;)V

    invoke-virtual {v0}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/autofill/AutofillDataProviderSessionInformation;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 108
    :goto_0
    return-object v0

    .line 106
    :catch_0
    move-exception v0

    .line 107
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 108
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getGuidDefaultBilling()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/apps/chrome/autofill/AutofillDataProviderSessionInformation;->mGuidDefaultBilling:Ljava/lang/String;

    return-object v0
.end method

.method public getGuidDefaultCard()Ljava/lang/String;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/android/apps/chrome/autofill/AutofillDataProviderSessionInformation;->mGuidDefaultCard:Ljava/lang/String;

    return-object v0
.end method

.method public getGuidDefaultShipping()Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/apps/chrome/autofill/AutofillDataProviderSessionInformation;->mGuidDefaultShipping:Ljava/lang/String;

    return-object v0
.end method

.method public serialize()Ljava/lang/String;
    .locals 5

    .prologue
    .line 75
    new-instance v3, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v3}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 76
    new-instance v4, Landroid/util/Base64OutputStream;

    const/16 v0, 0x1b

    invoke-direct {v4, v3, v0}, Landroid/util/Base64OutputStream;-><init>(Ljava/io/OutputStream;I)V

    .line 78
    const/4 v2, 0x0

    .line 80
    :try_start_0
    new-instance v1, Ljava/io/ObjectOutputStream;

    invoke-direct {v1, v4}, Ljava/io/ObjectOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 81
    :try_start_1
    invoke-virtual {v1, p0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 86
    :try_start_2
    invoke-virtual {v1}, Ljava/io/ObjectOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    .line 92
    :goto_0
    new-instance v0, Ljava/lang/String;

    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([B)V

    return-object v0

    .line 82
    :catch_0
    move-exception v0

    move-object v1, v2

    .line 83
    :goto_1
    :try_start_3
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 86
    if-eqz v1, :cond_0

    :try_start_4
    invoke-virtual {v1}, Ljava/io/ObjectOutputStream;->close()V

    goto :goto_0

    .line 91
    :catch_1
    move-exception v0

    goto :goto_0

    .line 87
    :cond_0
    invoke-virtual {v4}, Landroid/util/Base64OutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_0

    .line 85
    :catchall_0
    move-exception v0

    move-object v1, v2

    .line 86
    :goto_2
    if-eqz v1, :cond_1

    :try_start_5
    invoke-virtual {v1}, Ljava/io/ObjectOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    .line 90
    :goto_3
    throw v0

    .line 87
    :cond_1
    :try_start_6
    invoke-virtual {v4}, Landroid/util/Base64OutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2

    goto :goto_3

    :catch_2
    move-exception v1

    goto :goto_3

    .line 91
    :catch_3
    move-exception v0

    goto :goto_0

    .line 85
    :catchall_1
    move-exception v0

    goto :goto_2

    .line 82
    :catch_4
    move-exception v0

    goto :goto_1
.end method
