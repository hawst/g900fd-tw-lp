.class Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$SnapshotDocumentsGroup;
.super Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$Group;
.source "RecentTabsRowAdapter.java"


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;)V
    .locals 0

    .prologue
    .line 382
    iput-object p1, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$SnapshotDocumentsGroup;->this$0:Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$Group;-><init>(Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;)V

    return-void
.end method


# virtual methods
.method public configureChildView(ILcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$ViewHolder;)V
    .locals 3

    .prologue
    .line 405
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$SnapshotDocumentsGroup;->getChild(I)Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;

    move-result-object v0

    .line 406
    iget-object v1, p2, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$ViewHolder;->textView:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 407
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->getPageUri()Landroid/net/Uri;

    move-result-object v0

    .line 408
    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$SnapshotDocumentsGroup;->this$0:Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    # invokes: Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->loadLocalFavicon(Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$ViewHolder;Ljava/lang/String;)V
    invoke-static {v1, p2, v0}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->access$400(Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$ViewHolder;Ljava/lang/String;)V

    .line 409
    return-void

    .line 408
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public configureGroupView(Lcom/google/android/apps/chrome/ntp/RecentTabsGroupView;Z)V
    .locals 0

    .prologue
    .line 413
    invoke-virtual {p1, p2}, Lcom/google/android/apps/chrome/ntp/RecentTabsGroupView;->configureForSnapshotDocument(Z)V

    .line 414
    return-void
.end method

.method public getChild(I)Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;
    .locals 1

    .prologue
    .line 400
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$SnapshotDocumentsGroup;->this$0:Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;

    # getter for: Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->mRecentTabsManager:Lcom/google/android/apps/chrome/ntp/RecentTabsManager;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->access$300(Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;)Lcom/google/android/apps/chrome/ntp/RecentTabsManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->getSnapshotDocuments()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;

    return-object v0
.end method

.method public bridge synthetic getChild(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 382
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$SnapshotDocumentsGroup;->getChild(I)Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;

    move-result-object v0

    return-object v0
.end method

.method public getChildType()Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$ChildType;
    .locals 1

    .prologue
    .line 395
    sget-object v0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$ChildType;->DEFAULT_CONTENT:Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$ChildType;

    return-object v0
.end method

.method public getChildrenCount()I
    .locals 1

    .prologue
    .line 390
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$SnapshotDocumentsGroup;->this$0:Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;

    # getter for: Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->mRecentTabsManager:Lcom/google/android/apps/chrome/ntp/RecentTabsManager;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->access$300(Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;)Lcom/google/android/apps/chrome/ntp/RecentTabsManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->getSnapshotDocuments()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getGroupType()Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$GroupType;
    .locals 1

    .prologue
    .line 385
    sget-object v0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$GroupType;->CONTENT:Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$GroupType;

    return-object v0
.end method

.method public isCollapsed()Z
    .locals 1

    .prologue
    .line 423
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$SnapshotDocumentsGroup;->this$0:Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;

    # getter for: Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->mRecentTabsManager:Lcom/google/android/apps/chrome/ntp/RecentTabsManager;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->access$300(Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;)Lcom/google/android/apps/chrome/ntp/RecentTabsManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->isSnapshotDocumentsCollapsed()Z

    move-result v0

    return v0
.end method

.method public onChildClick(I)Z
    .locals 2

    .prologue
    .line 428
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$SnapshotDocumentsGroup;->getChild(I)Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;

    move-result-object v0

    .line 429
    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$SnapshotDocumentsGroup;->this$0:Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;

    # getter for: Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->mRecentTabsManager:Lcom/google/android/apps/chrome/ntp/RecentTabsManager;
    invoke-static {v1}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->access$300(Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;)Lcom/google/android/apps/chrome/ntp/RecentTabsManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->openSnapshotDocument(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)V

    .line 430
    const/4 v0, 0x1

    return v0
.end method

.method public onCreateContextMenuForChild(ILandroid/view/ContextMenu;Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 442
    return-void
.end method

.method public onCreateContextMenuForGroup(Landroid/view/ContextMenu;Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 436
    return-void
.end method

.method public setCollapsed(Z)V
    .locals 1

    .prologue
    .line 418
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$SnapshotDocumentsGroup;->this$0:Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;

    # getter for: Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->mRecentTabsManager:Lcom/google/android/apps/chrome/ntp/RecentTabsManager;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->access$300(Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;)Lcom/google/android/apps/chrome/ntp/RecentTabsManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->setSnapshotDocumentsCollapsed(Z)V

    .line 419
    return-void
.end method
