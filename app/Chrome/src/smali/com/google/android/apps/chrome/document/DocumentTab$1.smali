.class final Lcom/google/android/apps/chrome/document/DocumentTab$1;
.super Lcom/google/android/apps/chrome/tab/NativePageFactory$NativePageBuilder;
.source "DocumentTab.java"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tab/NativePageFactory$NativePageBuilder;-><init>()V

    return-void
.end method


# virtual methods
.method protected final buildBookmarksPage(Landroid/app/Activity;Lorg/chromium/chrome/browser/Tab;Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;)Lorg/chromium/chrome/browser/NativePage;
    .locals 2

    .prologue
    .line 72
    invoke-static {}, Lcom/google/android/apps/chrome/document/DocumentManager;->getInstance()Lcom/google/android/apps/chrome/document/DocumentManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/document/DocumentManager;->getCurrentProfile()Lorg/chromium/chrome/browser/profiles/Profile;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {p1, p2, p3, v0, v1}, Lcom/google/android/apps/chrome/ntp/BookmarksPage;->buildPageHomeActivityMode(Landroid/content/Context;Lorg/chromium/chrome/browser/Tab;Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;Lorg/chromium/chrome/browser/profiles/Profile;Lcom/google/android/apps/chrome/ntp/BookmarksPage$BookmarkSelectedListener;)Lcom/google/android/apps/chrome/ntp/BookmarksPage;

    move-result-object v0

    return-object v0
.end method

.method protected final buildNewTabPage(Landroid/app/Activity;Lcom/google/android/apps/chrome/tab/ChromeTab;Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;)Lorg/chromium/chrome/browser/NativePage;
    .locals 1

    .prologue
    .line 62
    invoke-virtual {p2}, Lcom/google/android/apps/chrome/tab/ChromeTab;->isIncognito()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 63
    new-instance v0, Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPage;

    invoke-direct {v0, p1, p2}, Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPage;-><init>(Landroid/content/Context;Lorg/chromium/chrome/browser/Tab;)V

    .line 65
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/android/apps/chrome/document/home/MostVisitedPage;

    check-cast p1, Lcom/google/android/apps/chrome/document/DocumentActivity;

    invoke-direct {v0, p1, p2}, Lcom/google/android/apps/chrome/document/home/MostVisitedPage;-><init>(Lcom/google/android/apps/chrome/document/DocumentActivity;Lcom/google/android/apps/chrome/tab/ChromeTab;)V

    goto :goto_0
.end method

.method protected final buildRecentTabsPage(Landroid/app/Activity;Lcom/google/android/apps/chrome/tab/ChromeTab;)Lorg/chromium/chrome/browser/NativePage;
    .locals 3

    .prologue
    .line 78
    new-instance v0, Lcom/google/android/apps/chrome/ntp/RecentTabsPage;

    new-instance v1, Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;

    const/4 v2, 0x0

    invoke-direct {v1, p2, p1, v2}, Lcom/google/android/apps/chrome/document/home/HomeActivityRecentTabsManager;-><init>(Lcom/google/android/apps/chrome/tab/ChromeTab;Landroid/app/Activity;Z)V

    invoke-direct {v0, p1, v1}, Lcom/google/android/apps/chrome/ntp/RecentTabsPage;-><init>(Landroid/app/Activity;Lcom/google/android/apps/chrome/ntp/RecentTabsManager;)V

    return-object v0
.end method
