.class public Lcom/google/android/apps/chrome/snapshot/SnapshotStateBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SnapshotStateBroadcastReceiver.java"


# instance fields
.field private final mHandler:Landroid/os/Handler;

.field private final mTabModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;


# direct methods
.method public constructor <init>(Lorg/chromium/chrome/browser/tabmodel/TabModel;Landroid/os/Handler;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotStateBroadcastReceiver;->mTabModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    .line 33
    iput-object p2, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotStateBroadcastReceiver;->mHandler:Landroid/os/Handler;

    .line 34
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotStateBroadcastReceiver;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/chrome/snapshot/SnapshotStateBroadcastReceiver$UpdateSnapshotStateTask;

    iget-object v2, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotStateBroadcastReceiver;->mTabModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    invoke-direct {v1, v2, p2}, Lcom/google/android/apps/chrome/snapshot/SnapshotStateBroadcastReceiver$UpdateSnapshotStateTask;-><init>(Lorg/chromium/chrome/browser/tabmodel/TabModel;Landroid/content/Intent;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 39
    return-void
.end method
