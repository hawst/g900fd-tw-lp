.class Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$FindQuery;
.super Lcom/google/android/apps/chrome/widget/VerticallyFixedEditText;
.source "FindToolbar.java"


# instance fields
.field private mFindToolbar:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/widget/VerticallyFixedEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 69
    return-void
.end method


# virtual methods
.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 96
    const/16 v0, 0x42

    if-ne p1, v0, :cond_1

    .line 97
    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$FindQuery;->mFindToolbar:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;

    invoke-virtual {p2}, Landroid/view/KeyEvent;->isShiftPressed()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    # invokes: Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->hideKeyboardAndStartFinding(Z)V
    invoke-static {v2, v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->access$100(Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;Z)V

    .line 100
    :goto_1
    return v1

    .line 97
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 100
    :cond_1
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/chrome/widget/VerticallyFixedEditText;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v1

    goto :goto_1
.end method

.method public onKeyPreIme(ILandroid/view/KeyEvent;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 77
    const/4 v1, 0x4

    if-ne p1, v1, :cond_1

    .line 78
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v1

    if-nez v1, :cond_0

    .line 81
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$FindQuery;->getKeyDispatcherState()Landroid/view/KeyEvent$DispatcherState;

    move-result-object v1

    invoke-virtual {v1, p2, p0}, Landroid/view/KeyEvent$DispatcherState;->startTracking(Landroid/view/KeyEvent;Ljava/lang/Object;)V

    .line 91
    :goto_0
    return v0

    .line 83
    :cond_0
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-ne v1, v0, :cond_1

    .line 84
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$FindQuery;->getKeyDispatcherState()Landroid/view/KeyEvent$DispatcherState;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/view/KeyEvent$DispatcherState;->handleUpEvent(Landroid/view/KeyEvent;)V

    .line 85
    invoke-virtual {p2}, Landroid/view/KeyEvent;->isTracking()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p2}, Landroid/view/KeyEvent;->isCanceled()Z

    move-result v1

    if-nez v1, :cond_1

    .line 86
    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$FindQuery;->mFindToolbar:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;

    # invokes: Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->sendHideFindToolbarNotification()V
    invoke-static {v1}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;->access$000(Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;)V

    goto :goto_0

    .line 91
    :cond_1
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/chrome/widget/VerticallyFixedEditText;->onKeyPreIme(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method setFindToolbar(Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;)V
    .locals 0

    .prologue
    .line 72
    iput-object p1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar$FindQuery;->mFindToolbar:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbar;

    .line 73
    return-void
.end method
