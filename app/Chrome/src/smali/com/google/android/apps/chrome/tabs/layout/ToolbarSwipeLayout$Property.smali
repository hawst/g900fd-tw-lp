.class public final enum Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout$Property;
.super Ljava/lang/Enum;
.source "ToolbarSwipeLayout.java"


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout$Property;

.field public static final enum OFFSET:Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout$Property;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 38
    new-instance v0, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout$Property;

    const-string/jumbo v1, "OFFSET"

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout$Property;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout$Property;->OFFSET:Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout$Property;

    .line 37
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout$Property;

    sget-object v1, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout$Property;->OFFSET:Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout$Property;

    aput-object v1, v0, v2

    sput-object v0, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout$Property;->$VALUES:[Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout$Property;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout$Property;
    .locals 1

    .prologue
    .line 37
    const-class v0, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout$Property;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout$Property;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout$Property;
    .locals 1

    .prologue
    .line 37
    sget-object v0, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout$Property;->$VALUES:[Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout$Property;

    invoke-virtual {v0}, [Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout$Property;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout$Property;

    return-object v0
.end method
