.class final Lcom/google/android/apps/chrome/document/ChromeLauncherActivity$1;
.super Ljava/lang/Object;
.source "ChromeLauncherActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic val$activity:Landroid/app/Activity;

.field final synthetic val$intentSource:I


# direct methods
.method constructor <init>(Landroid/app/Activity;I)V
    .locals 0

    .prologue
    .line 354
    iput-object p1, p0, Lcom/google/android/apps/chrome/document/ChromeLauncherActivity$1;->val$activity:Landroid/app/Activity;

    iput p2, p0, Lcom/google/android/apps/chrome/document/ChromeLauncherActivity$1;->val$intentSource:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 357
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/ChromeLauncherActivity$1;->val$activity:Landroid/app/Activity;

    invoke-static {v0}, Lcom/google/android/apps/chrome/partnercustomizations/HomepageManager;->getHomepageUri(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    .line 358
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v3, "chrome-native://newtab/"

    .line 359
    :cond_0
    # invokes: Lcom/google/android/apps/chrome/document/ChromeLauncherActivity;->relaunchTask(ZLjava/lang/String;)Z
    invoke-static {v1, v3}, Lcom/google/android/apps/chrome/document/ChromeLauncherActivity;->access$000(ZLjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 362
    :goto_0
    return-void

    .line 360
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/ChromeLauncherActivity$1;->val$activity:Landroid/app/Activity;

    iget v4, p0, Lcom/google/android/apps/chrome/document/ChromeLauncherActivity$1;->val$intentSource:I

    const/4 v5, 0x6

    const/4 v7, 0x0

    move v2, v1

    move v6, v1

    invoke-static/range {v0 .. v7}, Lcom/google/android/apps/chrome/document/ChromeLauncherActivity;->launchInstance(Landroid/app/Activity;ZILjava/lang/String;IIZLcom/google/android/apps/chrome/document/DocumentActivityDatabase$PendingDocumentData;)V

    goto :goto_0
.end method
