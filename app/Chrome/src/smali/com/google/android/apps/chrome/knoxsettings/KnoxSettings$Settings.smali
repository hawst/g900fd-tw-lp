.class public Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;
.super Ljava/lang/Object;
.source "KnoxSettings.java"


# instance fields
.field private final mAuditLogEnabled:Z

.field private final mAutofillEnabled:Z

.field private final mCookiesEnabled:Z

.field private final mHttpProxyHostPort:Ljava/lang/String;

.field private final mJavascriptEnabled:Z

.field private final mPopupsEnabled:Z

.field private final mSmartCardAuthenticationEnabled:Z

.field private final mUrlBlacklist:Ljava/util/List;

.field private final mUrlFilterEnabled:Z

.field private final mUrlFilterReportEnabled:Z

.field final synthetic this$0:Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;)V
    .locals 12

    .prologue
    .line 192
    # getter for: Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;->mContext:Landroid/content/Context;
    invoke-static {p1}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;->access$1200(Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider;->getAuditLogEnabled(Landroid/content/Context;)Z

    move-result v2

    # getter for: Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;->mContext:Landroid/content/Context;
    invoke-static {p1}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;->access$1200(Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider;->getAutofillEnabled(Landroid/content/Context;)Z

    move-result v3

    # getter for: Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;->mContext:Landroid/content/Context;
    invoke-static {p1}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;->access$1200(Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider;->getCookiesEnabled(Landroid/content/Context;)Z

    move-result v4

    # getter for: Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;->mContext:Landroid/content/Context;
    invoke-static {p1}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;->access$1200(Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider;->getHttpProxyHostPort(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    # getter for: Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;->mContext:Landroid/content/Context;
    invoke-static {p1}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;->access$1200(Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider;->getJavascriptEnabled(Landroid/content/Context;)Z

    move-result v6

    # getter for: Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;->mContext:Landroid/content/Context;
    invoke-static {p1}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;->access$1200(Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider;->getPopupsEnabled(Landroid/content/Context;)Z

    move-result v7

    # getter for: Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;->mContext:Landroid/content/Context;
    invoke-static {p1}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;->access$1200(Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider;->getSmartCardAuthenticationEnabled(Landroid/content/Context;)Z

    move-result v8

    # getter for: Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;->mContext:Landroid/content/Context;
    invoke-static {p1}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;->access$1200(Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider;->getUrlBlacklist(Landroid/content/Context;)Ljava/util/List;

    move-result-object v9

    # getter for: Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;->mContext:Landroid/content/Context;
    invoke-static {p1}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;->access$1200(Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider;->getUrlFilterEnabled(Landroid/content/Context;)Z

    move-result v10

    # getter for: Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;->mContext:Landroid/content/Context;
    invoke-static {p1}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;->access$1200(Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider;->getUrlFilterReportEnabled(Landroid/content/Context;)Z

    move-result v11

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v11}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;-><init>(Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;ZZZLjava/lang/String;ZZZLjava/util/List;ZZ)V

    .line 202
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$1;)V
    .locals 0

    .prologue
    .line 179
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;-><init>(Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;)V

    return-void
.end method

.method private constructor <init>(Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;ZZZLjava/lang/String;ZZZLjava/util/List;ZZ)V
    .locals 0

    .prologue
    .line 213
    iput-object p1, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->this$0:Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 214
    iput-boolean p2, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->mAuditLogEnabled:Z

    .line 215
    iput-boolean p3, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->mAutofillEnabled:Z

    .line 216
    iput-boolean p4, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->mCookiesEnabled:Z

    .line 217
    iput-object p5, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->mHttpProxyHostPort:Ljava/lang/String;

    .line 218
    iput-boolean p6, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->mJavascriptEnabled:Z

    .line 219
    iput-boolean p7, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->mPopupsEnabled:Z

    .line 220
    iput-boolean p8, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->mSmartCardAuthenticationEnabled:Z

    .line 221
    iput-object p9, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->mUrlBlacklist:Ljava/util/List;

    .line 222
    iput-boolean p10, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->mUrlFilterEnabled:Z

    .line 223
    iput-boolean p11, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->mUrlFilterReportEnabled:Z

    .line 242
    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;)Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;
    .locals 1

    .prologue
    .line 179
    invoke-direct {p0}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->updateUrlFilterReportEnabled()Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1100(Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;)Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;
    .locals 1

    .prologue
    .line 179
    invoke-direct {p0}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->updateAuditLogEnabled()Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;)Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;
    .locals 1

    .prologue
    .line 179
    invoke-direct {p0}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->updateHttpProxyHostPort()Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;)Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;
    .locals 1

    .prologue
    .line 179
    invoke-direct {p0}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->updateAutofillEnabled()Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;)Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;
    .locals 1

    .prologue
    .line 179
    invoke-direct {p0}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->updateCookiesEnabled()Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;)Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;
    .locals 1

    .prologue
    .line 179
    invoke-direct {p0}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->updateJavascriptEnabled()Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;)Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;
    .locals 1

    .prologue
    .line 179
    invoke-direct {p0}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->updatePopupsEnabled()Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;)Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;
    .locals 1

    .prologue
    .line 179
    invoke-direct {p0}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->updateSmartCardAuthenticationEnabled()Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;)Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;
    .locals 1

    .prologue
    .line 179
    invoke-direct {p0}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->updateUrlBlacklist()Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;)Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;
    .locals 1

    .prologue
    .line 179
    invoke-direct {p0}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->updateUrlFilterEnabled()Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;

    move-result-object v0

    return-object v0
.end method

.method private updateAuditLogEnabled()Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;
    .locals 12

    .prologue
    .line 402
    new-instance v0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;

    iget-object v1, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->this$0:Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;

    iget-object v2, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->this$0:Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;

    # getter for: Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;->access$1200(Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider;->getAuditLogEnabled(Landroid/content/Context;)Z

    move-result v2

    iget-boolean v3, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->mAutofillEnabled:Z

    iget-boolean v4, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->mCookiesEnabled:Z

    iget-object v5, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->mHttpProxyHostPort:Ljava/lang/String;

    iget-boolean v6, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->mJavascriptEnabled:Z

    iget-boolean v7, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->mPopupsEnabled:Z

    iget-boolean v8, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->mSmartCardAuthenticationEnabled:Z

    iget-object v9, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->mUrlBlacklist:Ljava/util/List;

    iget-boolean v10, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->mUrlFilterEnabled:Z

    iget-boolean v11, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->mUrlFilterReportEnabled:Z

    invoke-direct/range {v0 .. v11}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;-><init>(Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;ZZZLjava/lang/String;ZZZLjava/util/List;ZZ)V

    return-object v0
.end method

.method private updateAutofillEnabled()Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;
    .locals 12

    .prologue
    .line 266
    new-instance v0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;

    iget-object v1, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->this$0:Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;

    iget-boolean v2, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->mAuditLogEnabled:Z

    iget-object v3, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->this$0:Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;

    # getter for: Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;->access$1200(Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider;->getAutofillEnabled(Landroid/content/Context;)Z

    move-result v3

    iget-boolean v4, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->mCookiesEnabled:Z

    iget-object v5, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->mHttpProxyHostPort:Ljava/lang/String;

    iget-boolean v6, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->mJavascriptEnabled:Z

    iget-boolean v7, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->mPopupsEnabled:Z

    iget-boolean v8, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->mSmartCardAuthenticationEnabled:Z

    iget-object v9, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->mUrlBlacklist:Ljava/util/List;

    iget-boolean v10, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->mUrlFilterEnabled:Z

    iget-boolean v11, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->mUrlFilterReportEnabled:Z

    invoke-direct/range {v0 .. v11}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;-><init>(Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;ZZZLjava/lang/String;ZZZLjava/util/List;ZZ)V

    return-object v0
.end method

.method private updateCookiesEnabled()Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;
    .locals 12

    .prologue
    .line 283
    new-instance v0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;

    iget-object v1, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->this$0:Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;

    iget-boolean v2, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->mAuditLogEnabled:Z

    iget-boolean v3, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->mAutofillEnabled:Z

    iget-object v4, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->this$0:Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;

    # getter for: Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;->access$1200(Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;)Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider;->getCookiesEnabled(Landroid/content/Context;)Z

    move-result v4

    iget-object v5, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->mHttpProxyHostPort:Ljava/lang/String;

    iget-boolean v6, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->mJavascriptEnabled:Z

    iget-boolean v7, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->mPopupsEnabled:Z

    iget-boolean v8, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->mSmartCardAuthenticationEnabled:Z

    iget-object v9, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->mUrlBlacklist:Ljava/util/List;

    iget-boolean v10, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->mUrlFilterEnabled:Z

    iget-boolean v11, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->mUrlFilterReportEnabled:Z

    invoke-direct/range {v0 .. v11}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;-><init>(Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;ZZZLjava/lang/String;ZZZLjava/util/List;ZZ)V

    return-object v0
.end method

.method private updateHttpProxyHostPort()Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;
    .locals 12

    .prologue
    .line 249
    new-instance v0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;

    iget-object v1, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->this$0:Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;

    iget-boolean v2, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->mAuditLogEnabled:Z

    iget-boolean v3, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->mAutofillEnabled:Z

    iget-boolean v4, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->mCookiesEnabled:Z

    iget-object v5, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->this$0:Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;

    # getter for: Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;->access$1200(Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;)Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider;->getHttpProxyHostPort(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    iget-boolean v6, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->mJavascriptEnabled:Z

    iget-boolean v7, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->mPopupsEnabled:Z

    iget-boolean v8, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->mSmartCardAuthenticationEnabled:Z

    iget-object v9, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->mUrlBlacklist:Ljava/util/List;

    iget-boolean v10, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->mUrlFilterEnabled:Z

    iget-boolean v11, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->mUrlFilterReportEnabled:Z

    invoke-direct/range {v0 .. v11}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;-><init>(Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;ZZZLjava/lang/String;ZZZLjava/util/List;ZZ)V

    return-object v0
.end method

.method private updateJavascriptEnabled()Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;
    .locals 12

    .prologue
    .line 300
    new-instance v0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;

    iget-object v1, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->this$0:Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;

    iget-boolean v2, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->mAuditLogEnabled:Z

    iget-boolean v3, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->mAutofillEnabled:Z

    iget-boolean v4, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->mCookiesEnabled:Z

    iget-object v5, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->mHttpProxyHostPort:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->this$0:Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;

    # getter for: Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;->mContext:Landroid/content/Context;
    invoke-static {v6}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;->access$1200(Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;)Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider;->getJavascriptEnabled(Landroid/content/Context;)Z

    move-result v6

    iget-boolean v7, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->mPopupsEnabled:Z

    iget-boolean v8, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->mSmartCardAuthenticationEnabled:Z

    iget-object v9, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->mUrlBlacklist:Ljava/util/List;

    iget-boolean v10, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->mUrlFilterEnabled:Z

    iget-boolean v11, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->mUrlFilterReportEnabled:Z

    invoke-direct/range {v0 .. v11}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;-><init>(Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;ZZZLjava/lang/String;ZZZLjava/util/List;ZZ)V

    return-object v0
.end method

.method private updatePopupsEnabled()Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;
    .locals 12

    .prologue
    .line 317
    new-instance v0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;

    iget-object v1, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->this$0:Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;

    iget-boolean v2, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->mAuditLogEnabled:Z

    iget-boolean v3, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->mAutofillEnabled:Z

    iget-boolean v4, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->mCookiesEnabled:Z

    iget-object v5, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->mHttpProxyHostPort:Ljava/lang/String;

    iget-boolean v6, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->mJavascriptEnabled:Z

    iget-object v7, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->this$0:Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;

    # getter for: Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;->access$1200(Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;)Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider;->getPopupsEnabled(Landroid/content/Context;)Z

    move-result v7

    iget-boolean v8, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->mSmartCardAuthenticationEnabled:Z

    iget-object v9, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->mUrlBlacklist:Ljava/util/List;

    iget-boolean v10, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->mUrlFilterEnabled:Z

    iget-boolean v11, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->mUrlFilterReportEnabled:Z

    invoke-direct/range {v0 .. v11}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;-><init>(Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;ZZZLjava/lang/String;ZZZLjava/util/List;ZZ)V

    return-object v0
.end method

.method private updateSmartCardAuthenticationEnabled()Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;
    .locals 12

    .prologue
    .line 334
    new-instance v0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;

    iget-object v1, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->this$0:Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;

    iget-boolean v2, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->mAuditLogEnabled:Z

    iget-boolean v3, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->mAutofillEnabled:Z

    iget-boolean v4, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->mCookiesEnabled:Z

    iget-object v5, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->mHttpProxyHostPort:Ljava/lang/String;

    iget-boolean v6, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->mJavascriptEnabled:Z

    iget-boolean v7, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->mPopupsEnabled:Z

    iget-object v8, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->this$0:Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;

    # getter for: Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;->mContext:Landroid/content/Context;
    invoke-static {v8}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;->access$1200(Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;)Landroid/content/Context;

    move-result-object v8

    invoke-static {v8}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider;->getSmartCardAuthenticationEnabled(Landroid/content/Context;)Z

    move-result v8

    iget-object v9, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->mUrlBlacklist:Ljava/util/List;

    iget-boolean v10, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->mUrlFilterEnabled:Z

    iget-boolean v11, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->mUrlFilterReportEnabled:Z

    invoke-direct/range {v0 .. v11}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;-><init>(Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;ZZZLjava/lang/String;ZZZLjava/util/List;ZZ)V

    return-object v0
.end method

.method private updateUrlBlacklist()Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;
    .locals 12

    .prologue
    .line 351
    new-instance v0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;

    iget-object v1, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->this$0:Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;

    iget-boolean v2, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->mAuditLogEnabled:Z

    iget-boolean v3, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->mAutofillEnabled:Z

    iget-boolean v4, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->mCookiesEnabled:Z

    iget-object v5, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->mHttpProxyHostPort:Ljava/lang/String;

    iget-boolean v6, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->mJavascriptEnabled:Z

    iget-boolean v7, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->mPopupsEnabled:Z

    iget-boolean v8, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->mSmartCardAuthenticationEnabled:Z

    iget-object v9, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->this$0:Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;

    # getter for: Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;->mContext:Landroid/content/Context;
    invoke-static {v9}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;->access$1200(Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;)Landroid/content/Context;

    move-result-object v9

    invoke-static {v9}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider;->getUrlBlacklist(Landroid/content/Context;)Ljava/util/List;

    move-result-object v9

    iget-boolean v10, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->mUrlFilterEnabled:Z

    iget-boolean v11, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->mUrlFilterReportEnabled:Z

    invoke-direct/range {v0 .. v11}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;-><init>(Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;ZZZLjava/lang/String;ZZZLjava/util/List;ZZ)V

    return-object v0
.end method

.method private updateUrlFilterEnabled()Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;
    .locals 12

    .prologue
    .line 368
    new-instance v0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;

    iget-object v1, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->this$0:Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;

    iget-boolean v2, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->mAuditLogEnabled:Z

    iget-boolean v3, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->mAutofillEnabled:Z

    iget-boolean v4, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->mCookiesEnabled:Z

    iget-object v5, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->mHttpProxyHostPort:Ljava/lang/String;

    iget-boolean v6, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->mJavascriptEnabled:Z

    iget-boolean v7, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->mPopupsEnabled:Z

    iget-boolean v8, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->mSmartCardAuthenticationEnabled:Z

    iget-object v9, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->mUrlBlacklist:Ljava/util/List;

    iget-object v10, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->this$0:Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;

    # getter for: Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;->mContext:Landroid/content/Context;
    invoke-static {v10}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;->access$1200(Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;)Landroid/content/Context;

    move-result-object v10

    invoke-static {v10}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider;->getUrlFilterEnabled(Landroid/content/Context;)Z

    move-result v10

    iget-boolean v11, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->mUrlFilterReportEnabled:Z

    invoke-direct/range {v0 .. v11}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;-><init>(Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;ZZZLjava/lang/String;ZZZLjava/util/List;ZZ)V

    return-object v0
.end method

.method private updateUrlFilterReportEnabled()Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;
    .locals 12

    .prologue
    .line 385
    new-instance v0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;

    iget-object v1, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->this$0:Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;

    iget-boolean v2, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->mAuditLogEnabled:Z

    iget-boolean v3, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->mAutofillEnabled:Z

    iget-boolean v4, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->mCookiesEnabled:Z

    iget-object v5, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->mHttpProxyHostPort:Ljava/lang/String;

    iget-boolean v6, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->mJavascriptEnabled:Z

    iget-boolean v7, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->mPopupsEnabled:Z

    iget-boolean v8, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->mSmartCardAuthenticationEnabled:Z

    iget-object v9, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->mUrlBlacklist:Ljava/util/List;

    iget-boolean v10, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->mUrlFilterEnabled:Z

    iget-object v11, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->this$0:Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;

    # getter for: Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;->mContext:Landroid/content/Context;
    invoke-static {v11}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;->access$1200(Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;)Landroid/content/Context;

    move-result-object v11

    invoke-static {v11}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider;->getUrlFilterReportEnabled(Landroid/content/Context;)Z

    move-result v11

    invoke-direct/range {v0 .. v11}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;-><init>(Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;ZZZLjava/lang/String;ZZZLjava/util/List;ZZ)V

    return-object v0
.end method


# virtual methods
.method public getAuditLogEnabled()Z
    .locals 1

    .prologue
    .line 398
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->mAuditLogEnabled:Z

    return v0
.end method

.method public getAutofillEnabled()Z
    .locals 1

    .prologue
    .line 262
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->mAutofillEnabled:Z

    return v0
.end method

.method public getCookiesEnabled()Z
    .locals 1

    .prologue
    .line 279
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->mCookiesEnabled:Z

    return v0
.end method

.method public getHttpProxyHostPort()Ljava/lang/String;
    .locals 1

    .prologue
    .line 245
    iget-object v0, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->mHttpProxyHostPort:Ljava/lang/String;

    return-object v0
.end method

.method public getJavascriptEnabled()Z
    .locals 1

    .prologue
    .line 296
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->mJavascriptEnabled:Z

    return v0
.end method

.method public getPopupsEnabled()Z
    .locals 1

    .prologue
    .line 313
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->mPopupsEnabled:Z

    return v0
.end method

.method public getSmartCardAuthenticationEnabled()Z
    .locals 1

    .prologue
    .line 330
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->mSmartCardAuthenticationEnabled:Z

    return v0
.end method

.method public getUrlBlacklist()Ljava/util/List;
    .locals 1

    .prologue
    .line 347
    iget-object v0, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->mUrlBlacklist:Ljava/util/List;

    return-object v0
.end method

.method public getUrlFilterEnabled()Z
    .locals 1

    .prologue
    .line 364
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->mUrlFilterEnabled:Z

    return v0
.end method

.method public getUrlFilterReportEnabled()Z
    .locals 1

    .prologue
    .line 381
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Settings;->mUrlFilterReportEnabled:Z

    return v0
.end method
