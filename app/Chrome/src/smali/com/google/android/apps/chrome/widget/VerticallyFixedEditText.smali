.class public Lcom/google/android/apps/chrome/widget/VerticallyFixedEditText;
.super Landroid/widget/EditText;
.source "VerticallyFixedEditText.java"


# instance fields
.field private mBringingPointIntoView:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0, p1}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 29
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/widget/VerticallyFixedEditText;->mBringingPointIntoView:Z

    .line 19
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0, p1, p2}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 29
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/widget/VerticallyFixedEditText;->mBringingPointIntoView:Z

    .line 23
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 29
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/widget/VerticallyFixedEditText;->mBringingPointIntoView:Z

    .line 27
    return-void
.end method


# virtual methods
.method public bringPointIntoView(I)Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 34
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/apps/chrome/widget/VerticallyFixedEditText;->mBringingPointIntoView:Z

    .line 35
    invoke-super {p0, p1}, Landroid/widget/EditText;->bringPointIntoView(I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 37
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/widget/VerticallyFixedEditText;->mBringingPointIntoView:Z

    return v0

    :catchall_0
    move-exception v0

    iput-boolean v1, p0, Lcom/google/android/apps/chrome/widget/VerticallyFixedEditText;->mBringingPointIntoView:Z

    throw v0
.end method

.method public scrollTo(II)V
    .locals 1

    .prologue
    .line 45
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widget/VerticallyFixedEditText;->mBringingPointIntoView:Z

    if-eqz v0, :cond_0

    :goto_0
    invoke-super {p0, p1, p2}, Landroid/widget/EditText;->scrollTo(II)V

    .line 46
    return-void

    .line 45
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/VerticallyFixedEditText;->getScrollY()I

    move-result p2

    goto :goto_0
.end method
