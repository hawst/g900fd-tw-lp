.class final enum Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$Mode;
.super Ljava/lang/Enum;
.source "AddEditBookmarkFragment.java"


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$Mode;

.field public static final enum ADD_BOOKMARK:Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$Mode;

.field public static final enum ADD_FOLDER:Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$Mode;

.field public static final enum EDIT_BOOKMARK:Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$Mode;

.field public static final enum EDIT_FOLDER:Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$Mode;


# instance fields
.field private final mIsFolder:Z


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 64
    new-instance v0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$Mode;

    const-string/jumbo v1, "ADD_BOOKMARK"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$Mode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$Mode;->ADD_BOOKMARK:Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$Mode;

    .line 65
    new-instance v0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$Mode;

    const-string/jumbo v1, "EDIT_BOOKMARK"

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$Mode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$Mode;->EDIT_BOOKMARK:Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$Mode;

    .line 66
    new-instance v0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$Mode;

    const-string/jumbo v1, "ADD_FOLDER"

    invoke-direct {v0, v1, v4, v2}, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$Mode;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$Mode;->ADD_FOLDER:Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$Mode;

    .line 67
    new-instance v0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$Mode;

    const-string/jumbo v1, "EDIT_FOLDER"

    invoke-direct {v0, v1, v5, v2}, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$Mode;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$Mode;->EDIT_FOLDER:Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$Mode;

    .line 63
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$Mode;

    sget-object v1, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$Mode;->ADD_BOOKMARK:Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$Mode;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$Mode;->EDIT_BOOKMARK:Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$Mode;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$Mode;->ADD_FOLDER:Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$Mode;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$Mode;->EDIT_FOLDER:Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$Mode;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$Mode;->$VALUES:[Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$Mode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 71
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 72
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$Mode;->mIsFolder:Z

    .line 73
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IZ)V
    .locals 0

    .prologue
    .line 75
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 76
    iput-boolean p3, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$Mode;->mIsFolder:Z

    .line 77
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$Mode;
    .locals 1

    .prologue
    .line 63
    const-class v0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$Mode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$Mode;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$Mode;
    .locals 1

    .prologue
    .line 63
    sget-object v0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$Mode;->$VALUES:[Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$Mode;

    invoke-virtual {v0}, [Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$Mode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$Mode;

    return-object v0
.end method


# virtual methods
.method protected final isFolder()Z
    .locals 1

    .prologue
    .line 81
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$Mode;->mIsFolder:Z

    return v0
.end method
