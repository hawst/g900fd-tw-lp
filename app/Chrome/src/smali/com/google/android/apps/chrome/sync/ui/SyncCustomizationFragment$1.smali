.class Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment$1;
.super Ljava/lang/Object;
.source "SyncCustomizationFragment.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 185
    const-class v0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment$1;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;)V
    .locals 0

    .prologue
    .line 185
    iput-object p1, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment$1;->this$0:Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 2

    .prologue
    .line 188
    sget-boolean v0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment$1;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment$1;->this$0:Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;

    # invokes: Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->canDisableSync()Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->access$000(Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 189
    :cond_0
    invoke-static {}, Lcom/google/android/apps/chrome/services/GoogleServicesStates;->create()Lcom/google/android/apps/chrome/services/GoogleServicesStates$GoogleServicesStatesBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/apps/chrome/services/GoogleServicesStates$GoogleServicesStatesBuilder;->sync(Z)Lcom/google/android/apps/chrome/services/GoogleServicesStates$GoogleServicesStatesBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/services/GoogleServicesStates$GoogleServicesStatesBuilder;->build()Lcom/google/android/apps/chrome/services/GoogleServicesStates;

    move-result-object v0

    .line 192
    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment$1;->this$0:Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;

    # getter for: Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mGoogleServicesManager:Lcom/google/android/apps/chrome/services/GoogleServicesManager;
    invoke-static {v1}, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->access$100(Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;)Lcom/google/android/apps/chrome/services/GoogleServicesManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->setStates(Lcom/google/android/apps/chrome/services/GoogleServicesStates;)V

    .line 193
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment$1;->this$0:Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;

    # invokes: Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->updateDataTypeState()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->access$200(Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;)V

    .line 194
    return-void
.end method
