.class public Lcom/google/android/apps/chrome/ntp/RecentTabsManager;
.super Ljava/lang/Object;
.source "RecentTabsManager.java"

# interfaces
.implements Lcom/google/android/apps/chrome/ntp/SyncPromoView$SyncPromoModel;
.implements Lcom/google/android/apps/chrome/snapshot/SnapshotDocumentDataSource$SnapshotDocumentDataSourceListener;
.implements Lorg/chromium/chrome/browser/signin/SigninManager$SignInStateObserver;
.implements Lorg/chromium/sync/notifier/SyncStatusHelper$SyncSettingsChangedObserver;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mFaviconHelper:Lorg/chromium/chrome/browser/favicon/FaviconHelper;

.field private mForeignSessionHelper:Lorg/chromium/chrome/browser/ForeignSessionHelper;

.field private mForeignSessions:Ljava/util/List;

.field private mNewTabPagePrefs:Lorg/chromium/chrome/browser/NewTabPagePrefs;

.field private final mObservers:Lorg/chromium/base/ObserverList;

.field private final mProfile:Lorg/chromium/chrome/browser/profiles/Profile;

.field private mRecentlyClosedBridge:Lorg/chromium/chrome/browser/RecentlyClosedBridge;

.field private mRecentlyClosedTabs:Ljava/util/List;

.field private mSignInManager:Lorg/chromium/chrome/browser/signin/SigninManager;

.field private mSnapshotDocumentDataSource:Lcom/google/android/apps/chrome/snapshot/SnapshotDocumentDataSource;

.field private mSnapshotDocuments:Ljava/util/List;

.field private mSyncStatusHelper:Lorg/chromium/sync/notifier/SyncStatusHelper;

.field private final mTab:Lcom/google/android/apps/chrome/tab/ChromeTab;

.field private mUpdatedCallback:Lcom/google/android/apps/chrome/ntp/RecentTabsManager$UpdatedCallback;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/chrome/tab/ChromeTab;Lorg/chromium/chrome/browser/profiles/Profile;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 92
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    new-instance v0, Lorg/chromium/base/ObserverList;

    invoke-direct {v0}, Lorg/chromium/base/ObserverList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->mObservers:Lorg/chromium/base/ObserverList;

    .line 93
    iput-object p2, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->mProfile:Lorg/chromium/chrome/browser/profiles/Profile;

    .line 94
    iput-object p1, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->mTab:Lcom/google/android/apps/chrome/tab/ChromeTab;

    .line 95
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->mProfile:Lorg/chromium/chrome/browser/profiles/Profile;

    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->buildForeignSessionHelper(Lorg/chromium/chrome/browser/profiles/Profile;)Lorg/chromium/chrome/browser/ForeignSessionHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->mForeignSessionHelper:Lorg/chromium/chrome/browser/ForeignSessionHelper;

    .line 96
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->mProfile:Lorg/chromium/chrome/browser/profiles/Profile;

    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->buildNewTabPagePrefs(Lorg/chromium/chrome/browser/profiles/Profile;)Lorg/chromium/chrome/browser/NewTabPagePrefs;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->mNewTabPagePrefs:Lorg/chromium/chrome/browser/NewTabPagePrefs;

    .line 97
    invoke-static {}, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->buildFaviconHelper()Lorg/chromium/chrome/browser/favicon/FaviconHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->mFaviconHelper:Lorg/chromium/chrome/browser/favicon/FaviconHelper;

    .line 98
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->mProfile:Lorg/chromium/chrome/browser/profiles/Profile;

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->buildRecentlyClosedBridge(Lorg/chromium/chrome/browser/profiles/Profile;)Lorg/chromium/chrome/browser/RecentlyClosedBridge;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->mRecentlyClosedBridge:Lorg/chromium/chrome/browser/RecentlyClosedBridge;

    .line 99
    invoke-static {p3, p0}, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->buildSnapshotDocumentDataSource(Landroid/content/Context;Lcom/google/android/apps/chrome/snapshot/SnapshotDocumentDataSource$SnapshotDocumentDataSourceListener;)Lcom/google/android/apps/chrome/snapshot/SnapshotDocumentDataSource;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->mSnapshotDocumentDataSource:Lcom/google/android/apps/chrome/snapshot/SnapshotDocumentDataSource;

    .line 100
    invoke-static {p3}, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->buildSyncStatusHelper(Landroid/content/Context;)Lorg/chromium/sync/notifier/SyncStatusHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->mSyncStatusHelper:Lorg/chromium/sync/notifier/SyncStatusHelper;

    .line 101
    invoke-static {p3}, Lorg/chromium/chrome/browser/signin/SigninManager;->get(Landroid/content/Context;)Lorg/chromium/chrome/browser/signin/SigninManager;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->mSignInManager:Lorg/chromium/chrome/browser/signin/SigninManager;

    .line 102
    iput-object p3, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->mContext:Landroid/content/Context;

    .line 106
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->mSnapshotDocuments:Ljava/util/List;

    .line 108
    invoke-direct {p0}, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->updateRecentlyClosedTabs()V

    .line 109
    invoke-direct {p0}, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->registerForForeignSessionUpdates()V

    .line 110
    invoke-direct {p0}, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->updateForeignSessions()V

    .line 111
    invoke-direct {p0}, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->registerForSnapshotUpdates()V

    .line 112
    invoke-direct {p0}, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->registerForSignInAndSyncNotifications()V

    .line 113
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/ntp/RecentTabsManager;)V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->updateRecentlyClosedTabs()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/ntp/RecentTabsManager;)V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->updateForeignSessions()V

    return-void
.end method

.method private static buildFaviconHelper()Lorg/chromium/chrome/browser/favicon/FaviconHelper;
    .locals 1

    .prologue
    .line 157
    new-instance v0, Lorg/chromium/chrome/browser/favicon/FaviconHelper;

    invoke-direct {v0}, Lorg/chromium/chrome/browser/favicon/FaviconHelper;-><init>()V

    return-object v0
.end method

.method private static buildForeignSessionHelper(Lorg/chromium/chrome/browser/profiles/Profile;)Lorg/chromium/chrome/browser/ForeignSessionHelper;
    .locals 1

    .prologue
    .line 144
    new-instance v0, Lorg/chromium/chrome/browser/ForeignSessionHelper;

    invoke-direct {v0, p0}, Lorg/chromium/chrome/browser/ForeignSessionHelper;-><init>(Lorg/chromium/chrome/browser/profiles/Profile;)V

    return-object v0
.end method

.method private static buildNewTabPagePrefs(Lorg/chromium/chrome/browser/profiles/Profile;)Lorg/chromium/chrome/browser/NewTabPagePrefs;
    .locals 1

    .prologue
    .line 148
    new-instance v0, Lorg/chromium/chrome/browser/NewTabPagePrefs;

    invoke-direct {v0, p0}, Lorg/chromium/chrome/browser/NewTabPagePrefs;-><init>(Lorg/chromium/chrome/browser/profiles/Profile;)V

    return-object v0
.end method

.method private buildRecentlyClosedBridge(Lorg/chromium/chrome/browser/profiles/Profile;)Lorg/chromium/chrome/browser/RecentlyClosedBridge;
    .locals 2

    .prologue
    .line 165
    new-instance v0, Lorg/chromium/chrome/browser/RecentlyClosedBridge;

    invoke-direct {v0, p1}, Lorg/chromium/chrome/browser/RecentlyClosedBridge;-><init>(Lorg/chromium/chrome/browser/profiles/Profile;)V

    .line 166
    new-instance v1, Lcom/google/android/apps/chrome/ntp/RecentTabsManager$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/ntp/RecentTabsManager$1;-><init>(Lcom/google/android/apps/chrome/ntp/RecentTabsManager;)V

    invoke-virtual {v0, v1}, Lorg/chromium/chrome/browser/RecentlyClosedBridge;->setRecentlyClosedCallback(Lorg/chromium/chrome/browser/RecentlyClosedBridge$RecentlyClosedCallback;)V

    .line 173
    return-object v0
.end method

.method private static buildSnapshotDocumentDataSource(Landroid/content/Context;Lcom/google/android/apps/chrome/snapshot/SnapshotDocumentDataSource$SnapshotDocumentDataSourceListener;)Lcom/google/android/apps/chrome/snapshot/SnapshotDocumentDataSource;
    .locals 1

    .prologue
    .line 153
    new-instance v0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocumentDataSource;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocumentDataSource;-><init>(Landroid/content/Context;Lcom/google/android/apps/chrome/snapshot/SnapshotDocumentDataSource$SnapshotDocumentDataSourceListener;)V

    return-object v0
.end method

.method private static buildSyncStatusHelper(Landroid/content/Context;)Lorg/chromium/sync/notifier/SyncStatusHelper;
    .locals 1

    .prologue
    .line 161
    invoke-static {p0}, Lorg/chromium/sync/notifier/SyncStatusHelper;->get(Landroid/content/Context;)Lorg/chromium/sync/notifier/SyncStatusHelper;

    move-result-object v0

    return-object v0
.end method

.method private registerForForeignSessionUpdates()V
    .locals 2

    .prologue
    .line 177
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->mForeignSessionHelper:Lorg/chromium/chrome/browser/ForeignSessionHelper;

    new-instance v1, Lcom/google/android/apps/chrome/ntp/RecentTabsManager$2;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/ntp/RecentTabsManager$2;-><init>(Lcom/google/android/apps/chrome/ntp/RecentTabsManager;)V

    invoke-virtual {v0, v1}, Lorg/chromium/chrome/browser/ForeignSessionHelper;->setOnForeignSessionCallback(Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSessionCallback;)V

    .line 184
    return-void
.end method

.method private registerForSignInAndSyncNotifications()V
    .locals 1

    .prologue
    .line 187
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->mSyncStatusHelper:Lorg/chromium/sync/notifier/SyncStatusHelper;

    invoke-virtual {v0, p0}, Lorg/chromium/sync/notifier/SyncStatusHelper;->registerSyncSettingsChangedObserver(Lorg/chromium/sync/notifier/SyncStatusHelper$SyncSettingsChangedObserver;)V

    .line 188
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->mSignInManager:Lorg/chromium/chrome/browser/signin/SigninManager;

    invoke-virtual {v0, p0}, Lorg/chromium/chrome/browser/signin/SigninManager;->addSignInStateObserver(Lorg/chromium/chrome/browser/signin/SigninManager$SignInStateObserver;)V

    .line 189
    return-void
.end method

.method private registerForSnapshotUpdates()V
    .locals 2

    .prologue
    .line 192
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->mSnapshotDocumentDataSource:Lcom/google/android/apps/chrome/snapshot/SnapshotDocumentDataSource;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocumentDataSource;->setActive(Z)V

    .line 193
    return-void
.end method

.method private updateForeignSessions()V
    .locals 1

    .prologue
    .line 204
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->mForeignSessionHelper:Lorg/chromium/chrome/browser/ForeignSessionHelper;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/ForeignSessionHelper;->getForeignSessions()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->mForeignSessions:Ljava/util/List;

    .line 205
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->mForeignSessions:Ljava/util/List;

    if-nez v0, :cond_0

    .line 206
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->mForeignSessions:Ljava/util/List;

    .line 208
    :cond_0
    return-void
.end method

.method private updateRecentlyClosedTabs()V
    .locals 2

    .prologue
    .line 199
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->mRecentlyClosedBridge:Lorg/chromium/chrome/browser/RecentlyClosedBridge;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lorg/chromium/chrome/browser/RecentlyClosedBridge;->getRecentlyClosedTabs(I)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->mRecentlyClosedTabs:Ljava/util/List;

    .line 201
    return-void
.end method


# virtual methods
.method public clearRecentlyClosedTabs()V
    .locals 1

    .prologue
    .line 423
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->mRecentlyClosedBridge:Lorg/chromium/chrome/browser/RecentlyClosedBridge;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/RecentlyClosedBridge;->clearRecentlyClosedTabs()V

    .line 424
    return-void
.end method

.method public deleteForeignSession(Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSession;)V
    .locals 1

    .prologue
    .line 416
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->mForeignSessionHelper:Lorg/chromium/chrome/browser/ForeignSessionHelper;

    invoke-virtual {v0, p1}, Lorg/chromium/chrome/browser/ForeignSessionHelper;->deleteForeignSession(Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSession;)V

    .line 417
    return-void
.end method

.method public destroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 119
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->mSnapshotDocumentDataSource:Lcom/google/android/apps/chrome/snapshot/SnapshotDocumentDataSource;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocumentDataSource;->setActive(Z)V

    .line 120
    iput-object v2, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->mSnapshotDocumentDataSource:Lcom/google/android/apps/chrome/snapshot/SnapshotDocumentDataSource;

    .line 122
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->mSyncStatusHelper:Lorg/chromium/sync/notifier/SyncStatusHelper;

    invoke-virtual {v0, p0}, Lorg/chromium/sync/notifier/SyncStatusHelper;->unregisterSyncSettingsChangedObserver(Lorg/chromium/sync/notifier/SyncStatusHelper$SyncSettingsChangedObserver;)V

    .line 123
    iput-object v2, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->mSyncStatusHelper:Lorg/chromium/sync/notifier/SyncStatusHelper;

    .line 125
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->mSignInManager:Lorg/chromium/chrome/browser/signin/SigninManager;

    invoke-virtual {v0, p0}, Lorg/chromium/chrome/browser/signin/SigninManager;->removeSignInStateObserver(Lorg/chromium/chrome/browser/signin/SigninManager$SignInStateObserver;)V

    .line 126
    iput-object v2, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->mSignInManager:Lorg/chromium/chrome/browser/signin/SigninManager;

    .line 128
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->mFaviconHelper:Lorg/chromium/chrome/browser/favicon/FaviconHelper;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/favicon/FaviconHelper;->destroy()V

    .line 129
    iput-object v2, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->mFaviconHelper:Lorg/chromium/chrome/browser/favicon/FaviconHelper;

    .line 131
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->mRecentlyClosedBridge:Lorg/chromium/chrome/browser/RecentlyClosedBridge;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/RecentlyClosedBridge;->destroy()V

    .line 132
    iput-object v2, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->mRecentlyClosedBridge:Lorg/chromium/chrome/browser/RecentlyClosedBridge;

    .line 134
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->mForeignSessionHelper:Lorg/chromium/chrome/browser/ForeignSessionHelper;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/ForeignSessionHelper;->destroy()V

    .line 135
    iput-object v2, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->mForeignSessionHelper:Lorg/chromium/chrome/browser/ForeignSessionHelper;

    .line 137
    iput-object v2, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->mUpdatedCallback:Lcom/google/android/apps/chrome/ntp/RecentTabsManager$UpdatedCallback;

    .line 139
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->mNewTabPagePrefs:Lorg/chromium/chrome/browser/NewTabPagePrefs;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/NewTabPagePrefs;->destroy()V

    .line 140
    iput-object v2, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->mNewTabPagePrefs:Lorg/chromium/chrome/browser/NewTabPagePrefs;

    .line 141
    return-void
.end method

.method public enableSync()V
    .locals 3

    .prologue
    .line 524
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->get(Landroid/content/Context;)Lcom/google/android/apps/chrome/services/GoogleServicesManager;

    move-result-object v0

    .line 525
    invoke-static {}, Lcom/google/android/apps/chrome/services/GoogleServicesStates;->create()Lcom/google/android/apps/chrome/services/GoogleServicesStates$GoogleServicesStatesBuilder;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/android/apps/chrome/services/GoogleServicesStates$GoogleServicesStatesBuilder;->sync(Z)Lcom/google/android/apps/chrome/services/GoogleServicesStates$GoogleServicesStatesBuilder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/services/GoogleServicesStates$GoogleServicesStatesBuilder;->build()Lcom/google/android/apps/chrome/services/GoogleServicesStates;

    move-result-object v1

    .line 528
    iget-object v2, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lorg/chromium/sync/signin/ChromeSigninController;->get(Landroid/content/Context;)Lorg/chromium/sync/signin/ChromeSigninController;

    move-result-object v2

    invoke-virtual {v2}, Lorg/chromium/sync/signin/ChromeSigninController;->getSignedInUser()Landroid/accounts/Account;

    move-result-object v2

    .line 529
    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->setStates(Lcom/google/android/apps/chrome/services/GoogleServicesStates;Landroid/accounts/Account;)V

    .line 530
    return-void
.end method

.method public getCurrentlyOpenTabs()Ljava/util/List;
    .locals 1

    .prologue
    .line 214
    const/4 v0, 0x0

    return-object v0
.end method

.method public getForeignSessionCollapsed(Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSession;)Z
    .locals 1

    .prologue
    .line 369
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->mNewTabPagePrefs:Lorg/chromium/chrome/browser/NewTabPagePrefs;

    invoke-virtual {v0, p1}, Lorg/chromium/chrome/browser/NewTabPagePrefs;->getForeignSessionCollapsed(Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSession;)Z

    move-result v0

    return v0
.end method

.method public getForeignSessions()Ljava/util/List;
    .locals 1

    .prologue
    .line 221
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->mForeignSessions:Ljava/util/List;

    return-object v0
.end method

.method public getLocalFaviconForUrl(Ljava/lang/String;ILorg/chromium/chrome/browser/favicon/FaviconHelper$FaviconImageCallback;)Z
    .locals 6

    .prologue
    .line 300
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->mFaviconHelper:Lorg/chromium/chrome/browser/favicon/FaviconHelper;

    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->mProfile:Lorg/chromium/chrome/browser/profiles/Profile;

    const/4 v3, 0x7

    move-object v2, p1

    move v4, p2

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Lorg/chromium/chrome/browser/favicon/FaviconHelper;->getLocalFaviconImageForURL(Lorg/chromium/chrome/browser/profiles/Profile;Ljava/lang/String;IILorg/chromium/chrome/browser/favicon/FaviconHelper$FaviconImageCallback;)Z

    move-result v0

    return v0
.end method

.method public getRecentlyClosedTabs()Ljava/util/List;
    .locals 1

    .prologue
    .line 235
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->mRecentlyClosedTabs:Ljava/util/List;

    return-object v0
.end method

.method public getSnapshotDocuments()Ljava/util/List;
    .locals 1

    .prologue
    .line 228
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->mSnapshotDocuments:Ljava/util/List;

    return-object v0
.end method

.method public getSyncedFaviconImageForURL(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 2

    .prologue
    .line 286
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->mFaviconHelper:Lorg/chromium/chrome/browser/favicon/FaviconHelper;

    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->mProfile:Lorg/chromium/chrome/browser/profiles/Profile;

    invoke-virtual {v0, v1, p1}, Lorg/chromium/chrome/browser/favicon/FaviconHelper;->getSyncedFaviconImageForURL(Lorg/chromium/chrome/browser/profiles/Profile;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public isCurrentlyOpenTabsCollapsed()Z
    .locals 1

    .prologue
    .line 331
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->mNewTabPagePrefs:Lorg/chromium/chrome/browser/NewTabPagePrefs;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/NewTabPagePrefs;->getCurrentlyOpenTabsCollapsed()Z

    move-result v0

    return v0
.end method

.method public isCurrentlyOpenTabsShowingAll()Z
    .locals 1

    .prologue
    .line 348
    const/4 v0, 0x0

    return v0
.end method

.method public isRecentlyClosedTabsCollapsed()Z
    .locals 1

    .prologue
    .line 387
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->mNewTabPagePrefs:Lorg/chromium/chrome/browser/NewTabPagePrefs;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/NewTabPagePrefs;->getRecentlyClosedTabsCollapsed()Z

    move-result v0

    return v0
.end method

.method public isSignedIn()Z
    .locals 1

    .prologue
    .line 519
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lorg/chromium/sync/signin/ChromeSigninController;->get(Landroid/content/Context;)Lorg/chromium/sync/signin/ChromeSigninController;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/sync/signin/ChromeSigninController;->isSignedIn()Z

    move-result v0

    return v0
.end method

.method public isSnapshotDocumentsCollapsed()Z
    .locals 1

    .prologue
    .line 405
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->mNewTabPagePrefs:Lorg/chromium/chrome/browser/NewTabPagePrefs;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/NewTabPagePrefs;->getSnapshotDocumentCollapsed()Z

    move-result v0

    return v0
.end method

.method public isSyncEnabled()Z
    .locals 1

    .prologue
    .line 514
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lorg/chromium/sync/notifier/SyncStatusHelper;->get(Landroid/content/Context;)Lorg/chromium/sync/notifier/SyncStatusHelper;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/sync/notifier/SyncStatusHelper;->isSyncEnabled()Z

    move-result v0

    return v0
.end method

.method public isSyncPromoCollapsed()Z
    .locals 1

    .prologue
    .line 454
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->mNewTabPagePrefs:Lorg/chromium/chrome/browser/NewTabPagePrefs;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/NewTabPagePrefs;->getSyncPromoCollapsed()Z

    move-result v0

    return v0
.end method

.method public onSignedIn()V
    .locals 0

    .prologue
    .line 466
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->syncSettingsChanged()V

    .line 467
    return-void
.end method

.method public onSignedOut()V
    .locals 0

    .prologue
    .line 471
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->syncSettingsChanged()V

    .line 472
    return-void
.end method

.method public onSnapshotDocumentsAvailable(Ljava/util/Set;)V
    .locals 5

    .prologue
    .line 496
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 497
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;

    .line 499
    invoke-static {v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->isPrintedDocument(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-static {v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->getSnapshotViewableState(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)Lcom/google/android/apps/chrome/snapshot/SnapshotViewableState;

    move-result-object v3

    sget-object v4, Lcom/google/android/apps/chrome/snapshot/SnapshotViewableState;->READY:Lcom/google/android/apps/chrome/snapshot/SnapshotViewableState;

    if-ne v3, v4, :cond_0

    .line 502
    :cond_1
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 506
    :cond_2
    new-instance v0, Lcom/google/android/apps/chrome/ntp/RecentTabsManager$SnapshotDocumentComparator;

    const/4 v2, 0x0

    invoke-direct {v0, v2}, Lcom/google/android/apps/chrome/ntp/RecentTabsManager$SnapshotDocumentComparator;-><init>(Lcom/google/android/apps/chrome/ntp/RecentTabsManager$1;)V

    invoke-static {v1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 507
    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->mSnapshotDocuments:Ljava/util/List;

    .line 508
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->postUpdate()V

    .line 509
    return-void
.end method

.method public openForeignSessionTab(Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSession;Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSessionTab;I)V
    .locals 2

    .prologue
    .line 247
    const/4 v0, 0x6

    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/NewTabPageUma;->recordAction(I)V

    .line 248
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->mForeignSessionHelper:Lorg/chromium/chrome/browser/ForeignSessionHelper;

    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->mTab:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-virtual {v0, v1, p1, p2, p3}, Lorg/chromium/chrome/browser/ForeignSessionHelper;->openForeignSessionTab(Lorg/chromium/chrome/browser/Tab;Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSession;Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSessionTab;I)Z

    .line 249
    return-void
.end method

.method public openHistoryPage()V
    .locals 3

    .prologue
    .line 267
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->mTab:Lcom/google/android/apps/chrome/tab/ChromeTab;

    new-instance v1, Lorg/chromium/content_public/browser/LoadUrlParams;

    const-string/jumbo v2, "chrome://history/"

    invoke-direct {v1, v2}, Lorg/chromium/content_public/browser/LoadUrlParams;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->loadUrl(Lorg/chromium/content_public/browser/LoadUrlParams;)I

    .line 268
    return-void
.end method

.method public openRecentlyClosedTab(Lorg/chromium/chrome/browser/RecentlyClosedBridge$RecentlyClosedTab;I)V
    .locals 2

    .prologue
    .line 259
    const/4 v0, 0x4

    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/NewTabPageUma;->recordAction(I)V

    .line 260
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->mRecentlyClosedBridge:Lorg/chromium/chrome/browser/RecentlyClosedBridge;

    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->mTab:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-virtual {v0, v1, p1, p2}, Lorg/chromium/chrome/browser/RecentlyClosedBridge;->openRecentlyClosedTab(Lorg/chromium/chrome/browser/Tab;Lorg/chromium/chrome/browser/RecentlyClosedBridge$RecentlyClosedTab;I)Z

    .line 261
    return-void
.end method

.method public openSnapshotDocument(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)V
    .locals 1

    .prologue
    .line 276
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->mTab:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->openSnapshotDocument(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)V

    .line 277
    return-void
.end method

.method protected postUpdate()V
    .locals 1

    .prologue
    .line 458
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->mUpdatedCallback:Lcom/google/android/apps/chrome/ntp/RecentTabsManager$UpdatedCallback;

    if-eqz v0, :cond_0

    .line 459
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->mUpdatedCallback:Lcom/google/android/apps/chrome/ntp/RecentTabsManager$UpdatedCallback;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/ntp/RecentTabsManager$UpdatedCallback;->onUpdated()V

    .line 461
    :cond_0
    return-void
.end method

.method public registerForSyncUpdates(Lorg/chromium/sync/notifier/SyncStatusHelper$SyncSettingsChangedObserver;)V
    .locals 1

    .prologue
    .line 534
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->mObservers:Lorg/chromium/base/ObserverList;

    invoke-virtual {v0, p1}, Lorg/chromium/base/ObserverList;->addObserver(Ljava/lang/Object;)Z

    .line 535
    return-void
.end method

.method public setCurrentlyOpenTabsCollapsed(Z)V
    .locals 1

    .prologue
    .line 322
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->mNewTabPagePrefs:Lorg/chromium/chrome/browser/NewTabPagePrefs;

    invoke-virtual {v0, p1}, Lorg/chromium/chrome/browser/NewTabPagePrefs;->setCurrentlyOpenTabsCollapsed(Z)V

    .line 323
    return-void
.end method

.method public setCurrentlyOpenTabsShowAll(Z)V
    .locals 0

    .prologue
    .line 341
    return-void
.end method

.method public setForeignSessionCollapsed(Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSession;Z)V
    .locals 1

    .prologue
    .line 358
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->mNewTabPagePrefs:Lorg/chromium/chrome/browser/NewTabPagePrefs;

    invoke-virtual {v0, p1, p2}, Lorg/chromium/chrome/browser/NewTabPagePrefs;->setForeignSessionCollapsed(Lorg/chromium/chrome/browser/ForeignSessionHelper$ForeignSession;Z)V

    .line 359
    return-void
.end method

.method public setRecentlyClosedTabsCollapsed(Z)V
    .locals 1

    .prologue
    .line 378
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->mNewTabPagePrefs:Lorg/chromium/chrome/browser/NewTabPagePrefs;

    invoke-virtual {v0, p1}, Lorg/chromium/chrome/browser/NewTabPagePrefs;->setRecentlyClosedTabsCollapsed(Z)V

    .line 379
    return-void
.end method

.method public setSnapshotDocumentsCollapsed(Z)V
    .locals 1

    .prologue
    .line 396
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->mNewTabPagePrefs:Lorg/chromium/chrome/browser/NewTabPagePrefs;

    invoke-virtual {v0, p1}, Lorg/chromium/chrome/browser/NewTabPagePrefs;->setSnapshotDocumentCollapsed(Z)V

    .line 397
    return-void
.end method

.method public setSyncPromoCollapsed(Z)V
    .locals 1

    .prologue
    .line 445
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->mNewTabPagePrefs:Lorg/chromium/chrome/browser/NewTabPagePrefs;

    invoke-virtual {v0, p1}, Lorg/chromium/chrome/browser/NewTabPagePrefs;->setSyncPromoCollapsed(Z)V

    .line 446
    return-void
.end method

.method public setUpdatedCallback(Lcom/google/android/apps/chrome/ntp/RecentTabsManager$UpdatedCallback;)V
    .locals 0

    .prologue
    .line 313
    iput-object p1, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->mUpdatedCallback:Lcom/google/android/apps/chrome/ntp/RecentTabsManager$UpdatedCallback;

    .line 314
    return-void
.end method

.method public shouldDisplaySyncPromo()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 432
    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lorg/chromium/chrome/browser/signin/SigninManager;->get(Landroid/content/Context;)Lorg/chromium/chrome/browser/signin/SigninManager;

    move-result-object v1

    invoke-virtual {v1}, Lorg/chromium/chrome/browser/signin/SigninManager;->isSigninDisabledByPolicy()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 436
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->mSyncStatusHelper:Lorg/chromium/sync/notifier/SyncStatusHelper;

    invoke-virtual {v1}, Lorg/chromium/sync/notifier/SyncStatusHelper;->isSyncEnabled()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->mForeignSessions:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public syncSettingsChanged()V
    .locals 2

    .prologue
    .line 477
    invoke-direct {p0}, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->updateForeignSessions()V

    .line 478
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->postUpdate()V

    .line 479
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->mObservers:Lorg/chromium/base/ObserverList;

    invoke-virtual {v0}, Lorg/chromium/base/ObserverList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/sync/notifier/SyncStatusHelper$SyncSettingsChangedObserver;

    .line 480
    invoke-interface {v0}, Lorg/chromium/sync/notifier/SyncStatusHelper$SyncSettingsChangedObserver;->syncSettingsChanged()V

    goto :goto_0

    .line 482
    :cond_0
    return-void
.end method

.method public unregisterForSyncUpdates(Lorg/chromium/sync/notifier/SyncStatusHelper$SyncSettingsChangedObserver;)V
    .locals 1

    .prologue
    .line 539
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->mObservers:Lorg/chromium/base/ObserverList;

    invoke-virtual {v0, p1}, Lorg/chromium/base/ObserverList;->removeObserver(Ljava/lang/Object;)Z

    .line 540
    return-void
.end method

.method protected updateCurrentlyOpenTabs()V
    .locals 0

    .prologue
    .line 196
    return-void
.end method
