.class Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarkListAdapter;
.super Landroid/widget/BaseAdapter;
.source "BookmarksPageView.java"


# instance fields
.field public mBookmarks:Ljava/util/List;

.field final synthetic this$0:Lcom/google/android/apps/chrome/ntp/BookmarksPageView;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/chrome/ntp/BookmarksPageView;)V
    .locals 1

    .prologue
    .line 320
    iput-object p1, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarkListAdapter;->this$0:Lcom/google/android/apps/chrome/ntp/BookmarksPageView;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 322
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarkListAdapter;->mBookmarks:Ljava/util/List;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/chrome/ntp/BookmarksPageView;Lcom/google/android/apps/chrome/ntp/BookmarksPageView$1;)V
    .locals 0

    .prologue
    .line 320
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarkListAdapter;-><init>(Lcom/google/android/apps/chrome/ntp/BookmarksPageView;)V

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 391
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarkListAdapter;->mBookmarks:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 320
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarkListAdapter;->getItem(I)Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;

    move-result-object v0

    return-object v0
.end method

.method public getItem(I)Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;
    .locals 1

    .prologue
    .line 396
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarkListAdapter;->mBookmarks:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 386
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10

    .prologue
    .line 334
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarkListAdapter;->this$0:Lcom/google/android/apps/chrome/ntp/BookmarksPageView;

    # getter for: Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->mDrawingData:Lcom/google/android/apps/chrome/ntp/BookmarkItemView$DrawingData;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->access$500(Lcom/google/android/apps/chrome/ntp/BookmarksPageView;)Lcom/google/android/apps/chrome/ntp/BookmarkItemView$DrawingData;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarkListAdapter;->this$0:Lcom/google/android/apps/chrome/ntp/BookmarksPageView;

    new-instance v1, Lcom/google/android/apps/chrome/ntp/BookmarkItemView$DrawingData;

    iget-object v2, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarkListAdapter;->this$0:Lcom/google/android/apps/chrome/ntp/BookmarksPageView;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/apps/chrome/ntp/BookmarkItemView$DrawingData;-><init>(Landroid/content/Context;)V

    # setter for: Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->mDrawingData:Lcom/google/android/apps/chrome/ntp/BookmarkItemView$DrawingData;
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->access$502(Lcom/google/android/apps/chrome/ntp/BookmarksPageView;Lcom/google/android/apps/chrome/ntp/BookmarkItemView$DrawingData;)Lcom/google/android/apps/chrome/ntp/BookmarkItemView$DrawingData;

    .line 335
    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarkListAdapter;->getItem(I)Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;

    move-result-object v9

    .line 338
    if-eqz p2, :cond_2

    instance-of v0, p2, Lcom/google/android/apps/chrome/ntp/BookmarkItemLayout;

    if-eqz v0, :cond_2

    .line 339
    check-cast p2, Lcom/google/android/apps/chrome/ntp/BookmarkItemLayout;

    .line 340
    invoke-virtual {p2}, Lcom/google/android/apps/chrome/ntp/BookmarkItemLayout;->getBookmarkItemView()Lcom/google/android/apps/chrome/ntp/BookmarkItemView;

    move-result-object v0

    .line 341
    invoke-virtual {v9}, Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;->getId()Lorg/chromium/components/bookmarks/BookmarkId;

    move-result-object v1

    invoke-virtual {v9}, Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v9}, Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;->getUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v9}, Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;->isEditable()Z

    move-result v4

    invoke-virtual {v9}, Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;->isManaged()Z

    move-result v5

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/chrome/ntp/BookmarkItemView;->reset(Lorg/chromium/components/bookmarks/BookmarkId;Ljava/lang/String;Ljava/lang/String;ZZ)Z

    move-result v1

    if-nez v1, :cond_4

    .line 381
    :cond_1
    :goto_0
    return-object p2

    .line 346
    :cond_2
    new-instance v0, Lcom/google/android/apps/chrome/ntp/BookmarkItemView;

    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarkListAdapter;->this$0:Lcom/google/android/apps/chrome/ntp/BookmarksPageView;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarkListAdapter;->this$0:Lcom/google/android/apps/chrome/ntp/BookmarksPageView;

    # getter for: Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->mManager:Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarksPageManager;
    invoke-static {v2}, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->access$200(Lcom/google/android/apps/chrome/ntp/BookmarksPageView;)Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarksPageManager;

    move-result-object v2

    invoke-virtual {v9}, Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;->getId()Lorg/chromium/components/bookmarks/BookmarkId;

    move-result-object v3

    invoke-virtual {v9}, Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;->getTitle()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v9}, Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;->getUrl()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v9}, Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;->isEditable()Z

    move-result v6

    invoke-virtual {v9}, Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;->isManaged()Z

    move-result v7

    iget-object v8, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarkListAdapter;->this$0:Lcom/google/android/apps/chrome/ntp/BookmarksPageView;

    # getter for: Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->mDrawingData:Lcom/google/android/apps/chrome/ntp/BookmarkItemView$DrawingData;
    invoke-static {v8}, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->access$500(Lcom/google/android/apps/chrome/ntp/BookmarksPageView;)Lcom/google/android/apps/chrome/ntp/BookmarkItemView$DrawingData;

    move-result-object v8

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/chrome/ntp/BookmarkItemView;-><init>(Landroid/content/Context;Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarksPageManager;Lorg/chromium/components/bookmarks/BookmarkId;Ljava/lang/String;Ljava/lang/String;ZZLcom/google/android/apps/chrome/ntp/BookmarkItemView$DrawingData;)V

    .line 349
    new-instance p2, Lcom/google/android/apps/chrome/ntp/BookmarkItemLayout;

    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarkListAdapter;->this$0:Lcom/google/android/apps/chrome/ntp/BookmarksPageView;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {p2, v1, v0}, Lcom/google/android/apps/chrome/ntp/BookmarkItemLayout;-><init>(Landroid/content/Context;Lcom/google/android/apps/chrome/ntp/BookmarkItemView;)V

    move-object v1, v0

    .line 352
    :goto_1
    invoke-virtual {v9}, Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;->isFolder()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {v9}, Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;->getUrl()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 353
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarkListAdapter;->this$0:Lcom/google/android/apps/chrome/ntp/BookmarksPageView;

    # getter for: Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->mFaviconCache:Landroid/util/LruCache;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->access$600(Lcom/google/android/apps/chrome/ntp/BookmarksPageView;)Landroid/util/LruCache;

    move-result-object v0

    invoke-virtual {v9}, Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;->getUrl()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 354
    if-eqz v0, :cond_3

    .line 355
    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/ntp/BookmarkItemView;->setFavicon(Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 356
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarkListAdapter;->this$0:Lcom/google/android/apps/chrome/ntp/BookmarksPageView;

    # getter for: Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->mManager:Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarksPageManager;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->access$200(Lcom/google/android/apps/chrome/ntp/BookmarksPageView;)Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarksPageManager;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarksPageManager;->isDestroyed()Z

    move-result v0

    if-nez v0, :cond_1

    .line 357
    new-instance v0, Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarkListAdapter$1;

    invoke-direct {v0, p0, v9, v1}, Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarkListAdapter$1;-><init>(Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarkListAdapter;Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;Lcom/google/android/apps/chrome/ntp/BookmarkItemView;)V

    .line 377
    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarkListAdapter;->this$0:Lcom/google/android/apps/chrome/ntp/BookmarksPageView;

    # getter for: Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->mManager:Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarksPageManager;
    invoke-static {v1}, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->access$200(Lcom/google/android/apps/chrome/ntp/BookmarksPageView;)Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarksPageManager;

    move-result-object v1

    invoke-virtual {v9}, Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;->getUrl()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarkListAdapter;->this$0:Lcom/google/android/apps/chrome/ntp/BookmarksPageView;

    # getter for: Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->mDesiredFaviconSize:I
    invoke-static {v3}, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->access$900(Lcom/google/android/apps/chrome/ntp/BookmarksPageView;)I

    move-result v3

    invoke-interface {v1, v2, v3, v0}, Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarksPageManager;->getFaviconImageForUrl(Ljava/lang/String;ILorg/chromium/chrome/browser/favicon/FaviconHelper$FaviconImageCallback;)V

    goto/16 :goto_0

    :cond_4
    move-object v1, v0

    goto :goto_1
.end method

.method public hasStableIds()Z
    .locals 1

    .prologue
    .line 406
    const/4 v0, 0x0

    return v0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 401
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarkListAdapter;->mBookmarks:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public setBookmarksList(Ljava/util/List;)V
    .locals 0

    .prologue
    .line 329
    iput-object p1, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarkListAdapter;->mBookmarks:Ljava/util/List;

    .line 330
    return-void
.end method
