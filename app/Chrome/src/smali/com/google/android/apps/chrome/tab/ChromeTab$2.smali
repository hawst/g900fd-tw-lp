.class Lcom/google/android/apps/chrome/tab/ChromeTab$2;
.super Ljava/lang/Object;
.source "ChromeTab.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/tab/ChromeTab;)V
    .locals 0

    .prologue
    .line 422
    iput-object p1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$2;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 425
    iget-object v2, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$2;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # getter for: Lcom/google/android/apps/chrome/tab/ChromeTab;->mThumbnailCapturedForLoad:Z
    invoke-static {v2}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$400(Lcom/google/android/apps/chrome/tab/ChromeTab;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 456
    :cond_0
    :goto_0
    return-void

    .line 427
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$2;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # setter for: Lcom/google/android/apps/chrome/tab/ChromeTab;->mThumbnailCapturedForLoad:Z
    invoke-static {v2, v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$402(Lcom/google/android/apps/chrome/tab/ChromeTab;Z)Z

    .line 428
    iget-object v2, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$2;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # invokes: Lcom/google/android/apps/chrome/tab/ChromeTab;->canUpdateHistoryThumbnail()Z
    invoke-static {v2}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$500(Lcom/google/android/apps/chrome/tab/ChromeTab;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 430
    iget-object v2, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$2;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    iget-object v3, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$2;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tab/ChromeTab;->isHidden()Z

    move-result v3

    if-nez v3, :cond_2

    :goto_1
    # setter for: Lcom/google/android/apps/chrome/tab/ChromeTab;->mThumbnailCapturedForLoad:Z
    invoke-static {v2, v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$402(Lcom/google/android/apps/chrome/tab/ChromeTab;Z)Z

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1

    .line 433
    :cond_3
    iget-object v2, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$2;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # invokes: Lcom/google/android/apps/chrome/tab/ChromeTab;->shouldUpdateThumbnail()Z
    invoke-static {v2}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$600(Lcom/google/android/apps/chrome/tab/ChromeTab;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 437
    const/4 v2, 0x2

    new-array v2, v2, [I

    iget-object v3, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$2;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getWidth()I

    move-result v3

    aput v3, v2, v1

    iget-object v3, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$2;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getHeight()I

    move-result v3

    aput v3, v2, v0

    .line 440
    iget-object v3, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$2;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # getter for: Lcom/google/android/apps/chrome/tab/ChromeTab;->mThumbnailWidth:I
    invoke-static {v3}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$700(Lcom/google/android/apps/chrome/tab/ChromeTab;)I

    move-result v3

    iget-object v4, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$2;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # getter for: Lcom/google/android/apps/chrome/tab/ChromeTab;->mThumbnailHeight:I
    invoke-static {v4}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$800(Lcom/google/android/apps/chrome/tab/ChromeTab;)I

    move-result v4

    invoke-static {v2, v3, v4}, Lcom/google/android/apps/chrome/utilities/ChromeMathUtils;->scaleToFitTargetSize([III)F

    .line 442
    iget-object v3, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$2;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # getter for: Lcom/google/android/apps/chrome/tab/ChromeTab;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;
    invoke-static {v3}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$900(Lcom/google/android/apps/chrome/tab/ChromeTab;)Lcom/google/android/apps/chrome/ChromeActivity;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/ChromeActivity;->getContentReadbackHandler()Lorg/chromium/content/browser/ContentReadbackHandler;

    move-result-object v3

    .line 443
    if-eqz v3, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$2;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 444
    iget-object v4, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$2;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getUrl()Ljava/lang/String;

    move-result-object v4

    .line 445
    new-instance v5, Lcom/google/android/apps/chrome/tab/ChromeTab$2$1;

    invoke-direct {v5, p0, v4}, Lcom/google/android/apps/chrome/tab/ChromeTab$2$1;-><init>(Lcom/google/android/apps/chrome/tab/ChromeTab$2;Ljava/lang/String;)V

    .line 454
    const/high16 v4, 0x3f800000    # 1.0f

    new-instance v6, Landroid/graphics/Rect;

    aget v7, v2, v1

    aget v0, v2, v0

    invoke-direct {v6, v1, v1, v7, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$2;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    invoke-virtual {v3, v4, v6, v0, v5}, Lorg/chromium/content/browser/ContentReadbackHandler;->getContentBitmapAsync(FLandroid/graphics/Rect;Lorg/chromium/content/browser/ContentViewCore;Lorg/chromium/content/browser/ContentReadbackHandler$GetBitmapCallback;)V

    goto :goto_0
.end method
