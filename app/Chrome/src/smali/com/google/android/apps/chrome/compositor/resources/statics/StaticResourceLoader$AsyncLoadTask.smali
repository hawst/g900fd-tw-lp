.class Lcom/google/android/apps/chrome/compositor/resources/statics/StaticResourceLoader$AsyncLoadTask;
.super Landroid/os/AsyncTask;
.source "StaticResourceLoader.java"


# instance fields
.field private final mResourceId:I

.field final synthetic this$0:Lcom/google/android/apps/chrome/compositor/resources/statics/StaticResourceLoader;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/chrome/compositor/resources/statics/StaticResourceLoader;I)V
    .locals 0

    .prologue
    .line 99
    iput-object p1, p0, Lcom/google/android/apps/chrome/compositor/resources/statics/StaticResourceLoader$AsyncLoadTask;->this$0:Lcom/google/android/apps/chrome/compositor/resources/statics/StaticResourceLoader;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 100
    iput p2, p0, Lcom/google/android/apps/chrome/compositor/resources/statics/StaticResourceLoader$AsyncLoadTask;->mResourceId:I

    .line 101
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Lcom/google/android/apps/chrome/compositor/resources/statics/StaticResource;
    .locals 2

    .prologue
    .line 105
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/resources/statics/StaticResourceLoader$AsyncLoadTask;->this$0:Lcom/google/android/apps/chrome/compositor/resources/statics/StaticResourceLoader;

    # getter for: Lcom/google/android/apps/chrome/compositor/resources/statics/StaticResourceLoader;->mResources:Landroid/content/res/Resources;
    invoke-static {v0}, Lcom/google/android/apps/chrome/compositor/resources/statics/StaticResourceLoader;->access$000(Lcom/google/android/apps/chrome/compositor/resources/statics/StaticResourceLoader;)Landroid/content/res/Resources;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/chrome/compositor/resources/statics/StaticResourceLoader$AsyncLoadTask;->mResourceId:I

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/compositor/resources/statics/StaticResource;->create(Landroid/content/res/Resources;I)Lcom/google/android/apps/chrome/compositor/resources/statics/StaticResource;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 96
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/compositor/resources/statics/StaticResourceLoader$AsyncLoadTask;->doInBackground([Ljava/lang/Void;)Lcom/google/android/apps/chrome/compositor/resources/statics/StaticResource;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Lcom/google/android/apps/chrome/compositor/resources/statics/StaticResource;)V
    .locals 2

    .prologue
    .line 110
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/resources/statics/StaticResourceLoader$AsyncLoadTask;->this$0:Lcom/google/android/apps/chrome/compositor/resources/statics/StaticResourceLoader;

    iget v1, p0, Lcom/google/android/apps/chrome/compositor/resources/statics/StaticResourceLoader$AsyncLoadTask;->mResourceId:I

    # invokes: Lcom/google/android/apps/chrome/compositor/resources/statics/StaticResourceLoader;->registerResource(Lcom/google/android/apps/chrome/compositor/resources/statics/StaticResource;I)V
    invoke-static {v0, p1, v1}, Lcom/google/android/apps/chrome/compositor/resources/statics/StaticResourceLoader;->access$100(Lcom/google/android/apps/chrome/compositor/resources/statics/StaticResourceLoader;Lcom/google/android/apps/chrome/compositor/resources/statics/StaticResource;I)V

    .line 111
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 96
    check-cast p1, Lcom/google/android/apps/chrome/compositor/resources/statics/StaticResource;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/compositor/resources/statics/StaticResourceLoader$AsyncLoadTask;->onPostExecute(Lcom/google/android/apps/chrome/compositor/resources/statics/StaticResource;)V

    return-void
.end method
