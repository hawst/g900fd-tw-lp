.class Lcom/google/android/apps/chrome/ChromeActivity$2;
.super Ljava/lang/Object;
.source "ChromeActivity.java"

# interfaces
.implements Lorg/chromium/content/browser/ContentReadbackHandler$GetBitmapCallback;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/ChromeActivity;

.field final synthetic val$currentTab:Lcom/google/android/apps/chrome/tab/ChromeTab;

.field final synthetic val$extraIntentFlags:I

.field final synthetic val$mainActivity:Landroid/app/Activity;

.field final synthetic val$shareDirectly:Z


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/ChromeActivity;ZLandroid/app/Activity;Lcom/google/android/apps/chrome/tab/ChromeTab;I)V
    .locals 0

    .prologue
    .line 499
    iput-object p1, p0, Lcom/google/android/apps/chrome/ChromeActivity$2;->this$0:Lcom/google/android/apps/chrome/ChromeActivity;

    iput-boolean p2, p0, Lcom/google/android/apps/chrome/ChromeActivity$2;->val$shareDirectly:Z

    iput-object p3, p0, Lcom/google/android/apps/chrome/ChromeActivity$2;->val$mainActivity:Landroid/app/Activity;

    iput-object p4, p0, Lcom/google/android/apps/chrome/ChromeActivity$2;->val$currentTab:Lcom/google/android/apps/chrome/tab/ChromeTab;

    iput p5, p0, Lcom/google/android/apps/chrome/ChromeActivity$2;->val$extraIntentFlags:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFinishGetBitmap(Landroid/graphics/Bitmap;)V
    .locals 6

    .prologue
    .line 503
    if-nez p1, :cond_1

    const/4 v0, 0x0

    .line 505
    :goto_0
    if-eqz p1, :cond_0

    const/16 v1, 0x12c

    if-gt v0, v1, :cond_2

    :cond_0
    move-object v4, p1

    .line 516
    :goto_1
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/ChromeActivity$2;->val$shareDirectly:Z

    iget-object v1, p0, Lcom/google/android/apps/chrome/ChromeActivity$2;->val$mainActivity:Landroid/app/Activity;

    iget-object v2, p0, Lcom/google/android/apps/chrome/ChromeActivity$2;->val$currentTab:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getTitle()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/chrome/ChromeActivity$2;->val$currentTab:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getUrl()Ljava/lang/String;

    move-result-object v3

    iget v5, p0, Lcom/google/android/apps/chrome/ChromeActivity$2;->val$extraIntentFlags:I

    invoke-static/range {v0 .. v5}, Lorg/chromium/chrome/browser/share/ShareHelper;->share(ZLandroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;I)V

    .line 518
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/ChromeActivity$2;->val$shareDirectly:Z

    if-eqz v0, :cond_3

    .line 519
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->menuDirectShare()V

    .line 523
    :goto_2
    return-void

    .line 503
    :cond_1
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto :goto_0

    .line 509
    :cond_2
    const/high16 v1, 0x43960000    # 300.0f

    int-to-float v0, v0

    div-float v0, v1, v0

    .line 511
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, v0

    float-to-int v1, v1

    .line 512
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v0, v2

    float-to-int v0, v0

    .line 513
    const/4 v2, 0x1

    invoke-static {p1, v1, v0, v2}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v4

    goto :goto_1

    .line 521
    :cond_3
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->menuShare()V

    goto :goto_2
.end method
