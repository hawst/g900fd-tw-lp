.class Lcom/google/android/apps/chrome/snapshot/SnapshotStateBroadcastReceiver$UpdateSnapshotStateTask;
.super Ljava/lang/Object;
.source "SnapshotStateBroadcastReceiver.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private final mIntent:Landroid/content/Intent;

.field private final mTabModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;


# direct methods
.method public constructor <init>(Lorg/chromium/chrome/browser/tabmodel/TabModel;Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    iput-object p2, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotStateBroadcastReceiver$UpdateSnapshotStateTask;->mIntent:Landroid/content/Intent;

    .line 55
    iput-object p1, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotStateBroadcastReceiver$UpdateSnapshotStateTask;->mTabModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    .line 56
    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 60
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotStateBroadcastReceiver$UpdateSnapshotStateTask;->mIntent:Landroid/content/Intent;

    const-string/jumbo v2, "Chrome_SnapshotID"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 61
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotStateBroadcastReceiver$UpdateSnapshotStateTask;->mIntent:Landroid/content/Intent;

    const-string/jumbo v2, "Chrome_SnapshotURI"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 62
    iget-object v2, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotStateBroadcastReceiver$UpdateSnapshotStateTask;->mIntent:Landroid/content/Intent;

    const-string/jumbo v4, "Chrome_SnapshotState"

    invoke-virtual {v2, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 64
    invoke-static {v2}, Lcom/google/android/apps/chrome/snapshot/SnapshotViewableState;->fromValue(Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/SnapshotViewableState;

    move-result-object v4

    .line 66
    if-nez v0, :cond_2

    const/4 v0, 0x0

    move-object v2, v0

    .line 67
    :goto_0
    if-eqz v3, :cond_1

    .line 69
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotStateBroadcastReceiver$UpdateSnapshotStateTask;->mTabModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    if-eqz v0, :cond_0

    move v0, v1

    .line 70
    :goto_1
    iget-object v5, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotStateBroadcastReceiver$UpdateSnapshotStateTask;->mTabModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    invoke-interface {v5}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->getCount()I

    move-result v5

    if-ge v0, v5, :cond_0

    .line 71
    iget-object v5, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotStateBroadcastReceiver$UpdateSnapshotStateTask;->mTabModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    invoke-interface {v5, v0}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->getTabAt(I)Lorg/chromium/chrome/browser/Tab;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/apps/chrome/tab/ChromeTab;->fromTab(Lorg/chromium/chrome/browser/Tab;)Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v5

    invoke-virtual {v5, v3, v2, v4}, Lcom/google/android/apps/chrome/tab/ChromeTab;->snapshotStateQueryResult(Ljava/lang/String;Landroid/net/Uri;Lcom/google/android/apps/chrome/snapshot/SnapshotViewableState;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 73
    const/4 v1, 0x1

    .line 78
    :cond_0
    if-nez v1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotStateBroadcastReceiver$UpdateSnapshotStateTask;->mIntent:Landroid/content/Intent;

    const-string/jumbo v1, "Chrome_ErrorMessage"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 82
    invoke-static {}, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->getInstance()Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;

    move-result-object v0

    .line 86
    iget-object v1, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotStateBroadcastReceiver$UpdateSnapshotStateTask;->mIntent:Landroid/content/Intent;

    const-string/jumbo v2, "Chrome_DocumentID"

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 88
    iget-object v2, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotStateBroadcastReceiver$UpdateSnapshotStateTask;->mIntent:Landroid/content/Intent;

    const-string/jumbo v3, "Chrome_ErrorMessage"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->showOneOffNotification(ILjava/lang/String;)V

    .line 93
    :cond_1
    return-void

    .line 66
    :cond_2
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    move-object v2, v0

    goto :goto_0

    .line 70
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method
