.class public Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;
.super Ljava/lang/Object;
.source "PrinterData.java"


# instance fields
.field private mApplicationVersionTag:Ljava/lang/String;

.field private mC2dmRegistrationIdTag:Ljava/lang/String;

.field private mCapabilities:Ljava/lang/String;

.field private mCapabilitiesHash:Ljava/lang/String;

.field private mDefaultValues:Ljava/lang/String;

.field private mDescription:Ljava/lang/String;

.field private mId:Ljava/lang/String;

.field private mName:Ljava/lang/String;

.field private mProtocolVersionTag:Ljava/lang/String;

.field private mProxy:Ljava/lang/String;

.field private mStatus:Ljava/lang/String;

.field private mType:Ljava/lang/String;


# direct methods
.method private constructor <init>()V
    .locals 13

    .prologue
    const/4 v1, 0x0

    .line 32
    move-object v0, p0

    move-object v2, v1

    move-object v3, v1

    move-object v4, v1

    move-object v5, v1

    move-object v6, v1

    move-object v7, v1

    move-object v8, v1

    move-object v9, v1

    move-object v10, v1

    move-object v11, v1

    move-object v12, v1

    invoke-direct/range {v0 .. v12}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 33
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mId:Ljava/lang/String;

    .line 40
    iput-object p2, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mProxy:Ljava/lang/String;

    .line 41
    iput-object p3, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mName:Ljava/lang/String;

    .line 42
    iput-object p4, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mDescription:Ljava/lang/String;

    .line 43
    iput-object p5, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mCapabilities:Ljava/lang/String;

    .line 44
    iput-object p6, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mDefaultValues:Ljava/lang/String;

    .line 45
    iput-object p7, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mCapabilitiesHash:Ljava/lang/String;

    .line 46
    iput-object p8, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mStatus:Ljava/lang/String;

    .line 47
    iput-object p9, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mType:Ljava/lang/String;

    .line 48
    iput-object p10, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mApplicationVersionTag:Ljava/lang/String;

    .line 49
    iput-object p11, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mProtocolVersionTag:Ljava/lang/String;

    .line 50
    iput-object p12, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mC2dmRegistrationIdTag:Ljava/lang/String;

    .line 51
    return-void
.end method

.method private build()Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;
    .locals 13

    .prologue
    .line 172
    new-instance v0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;

    iget-object v1, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mProxy:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mName:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mDescription:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mCapabilities:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mDefaultValues:Ljava/lang/String;

    iget-object v7, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mCapabilitiesHash:Ljava/lang/String;

    iget-object v8, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mStatus:Ljava/lang/String;

    iget-object v9, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mType:Ljava/lang/String;

    iget-object v10, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mApplicationVersionTag:Ljava/lang/String;

    iget-object v11, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mProtocolVersionTag:Ljava/lang/String;

    iget-object v12, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mC2dmRegistrationIdTag:Ljava/lang/String;

    invoke-direct/range {v0 .. v12}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static create()Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;
    .locals 1

    .prologue
    .line 168
    new-instance v0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;-><init>()V

    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 199
    if-ne p0, p1, :cond_1

    .line 234
    :cond_0
    :goto_0
    return v0

    .line 200
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    goto :goto_0

    .line 202
    :cond_3
    check-cast p1, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;

    .line 204
    iget-object v2, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mApplicationVersionTag:Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mApplicationVersionTag:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mApplicationVersionTag:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 207
    goto :goto_0

    .line 204
    :cond_5
    iget-object v2, p1, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mApplicationVersionTag:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 208
    :cond_6
    iget-object v2, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mC2dmRegistrationIdTag:Ljava/lang/String;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mC2dmRegistrationIdTag:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mC2dmRegistrationIdTag:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 211
    goto :goto_0

    .line 208
    :cond_8
    iget-object v2, p1, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mC2dmRegistrationIdTag:Ljava/lang/String;

    if-nez v2, :cond_7

    .line 212
    :cond_9
    iget-object v2, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mCapabilities:Ljava/lang/String;

    if-eqz v2, :cond_b

    iget-object v2, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mCapabilities:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mCapabilities:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 214
    goto :goto_0

    .line 212
    :cond_b
    iget-object v2, p1, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mCapabilities:Ljava/lang/String;

    if-nez v2, :cond_a

    .line 215
    :cond_c
    iget-object v2, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mCapabilitiesHash:Ljava/lang/String;

    if-eqz v2, :cond_e

    iget-object v2, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mCapabilitiesHash:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mCapabilitiesHash:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    :cond_d
    move v0, v1

    .line 217
    goto :goto_0

    .line 215
    :cond_e
    iget-object v2, p1, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mCapabilitiesHash:Ljava/lang/String;

    if-nez v2, :cond_d

    .line 218
    :cond_f
    iget-object v2, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mDefaultValues:Ljava/lang/String;

    if-eqz v2, :cond_11

    iget-object v2, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mDefaultValues:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mDefaultValues:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_12

    :cond_10
    move v0, v1

    .line 220
    goto :goto_0

    .line 218
    :cond_11
    iget-object v2, p1, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mDefaultValues:Ljava/lang/String;

    if-nez v2, :cond_10

    .line 221
    :cond_12
    iget-object v2, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mDescription:Ljava/lang/String;

    if-eqz v2, :cond_14

    iget-object v2, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mDescription:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mDescription:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_15

    :cond_13
    move v0, v1

    .line 223
    goto/16 :goto_0

    .line 221
    :cond_14
    iget-object v2, p1, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mDescription:Ljava/lang/String;

    if-nez v2, :cond_13

    .line 224
    :cond_15
    iget-object v2, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mId:Ljava/lang/String;

    if-eqz v2, :cond_17

    iget-object v2, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mId:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_18

    :cond_16
    move v0, v1

    goto/16 :goto_0

    :cond_17
    iget-object v2, p1, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mId:Ljava/lang/String;

    if-nez v2, :cond_16

    .line 225
    :cond_18
    iget-object v2, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mName:Ljava/lang/String;

    if-eqz v2, :cond_1a

    iget-object v2, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mName:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1b

    :cond_19
    move v0, v1

    goto/16 :goto_0

    :cond_1a
    iget-object v2, p1, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mName:Ljava/lang/String;

    if-nez v2, :cond_19

    .line 226
    :cond_1b
    iget-object v2, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mProtocolVersionTag:Ljava/lang/String;

    if-eqz v2, :cond_1d

    iget-object v2, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mProtocolVersionTag:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mProtocolVersionTag:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1e

    :cond_1c
    move v0, v1

    .line 229
    goto/16 :goto_0

    .line 226
    :cond_1d
    iget-object v2, p1, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mProtocolVersionTag:Ljava/lang/String;

    if-nez v2, :cond_1c

    .line 230
    :cond_1e
    iget-object v2, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mProxy:Ljava/lang/String;

    if-eqz v2, :cond_20

    iget-object v2, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mProxy:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mProxy:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_21

    :cond_1f
    move v0, v1

    goto/16 :goto_0

    :cond_20
    iget-object v2, p1, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mProxy:Ljava/lang/String;

    if-nez v2, :cond_1f

    .line 231
    :cond_21
    iget-object v2, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mStatus:Ljava/lang/String;

    if-eqz v2, :cond_23

    iget-object v2, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mStatus:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mStatus:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_24

    :cond_22
    move v0, v1

    goto/16 :goto_0

    :cond_23
    iget-object v2, p1, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mStatus:Ljava/lang/String;

    if-nez v2, :cond_22

    .line 232
    :cond_24
    iget-object v2, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mType:Ljava/lang/String;

    if-eqz v2, :cond_25

    iget-object v2, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mType:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mType:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    goto/16 :goto_0

    :cond_25
    iget-object v2, p1, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mType:Ljava/lang/String;

    if-eqz v2, :cond_0

    goto :goto_1
.end method

.method public getApplicationVersionTag()Ljava/lang/String;
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mApplicationVersionTag:Ljava/lang/String;

    return-object v0
.end method

.method public getC2dmRegistrationIdTag()Ljava/lang/String;
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mC2dmRegistrationIdTag:Ljava/lang/String;

    return-object v0
.end method

.method public getCapabilities()Ljava/lang/String;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mCapabilities:Ljava/lang/String;

    return-object v0
.end method

.method public getCapabilitiesHash()Ljava/lang/String;
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mCapabilitiesHash:Ljava/lang/String;

    return-object v0
.end method

.method public getDefaultValues()Ljava/lang/String;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mDefaultValues:Ljava/lang/String;

    return-object v0
.end method

.method public getDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mDescription:Ljava/lang/String;

    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mId:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getProtocolVersionTag()Ljava/lang/String;
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mProtocolVersionTag:Ljava/lang/String;

    return-object v0
.end method

.method public getProxy()Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mProxy:Ljava/lang/String;

    return-object v0
.end method

.method public getStatus()Ljava/lang/String;
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mStatus:Ljava/lang/String;

    return-object v0
.end method

.method public getType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mType:Ljava/lang/String;

    return-object v0
.end method

.method public hasC2dmRegistrationIdTag()Z
    .locals 1

    .prologue
    .line 165
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mC2dmRegistrationIdTag:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasId()Z
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mId:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 179
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mId:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mId:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 180
    :goto_0
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mProxy:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mProxy:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    .line 181
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mType:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mType:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_2
    add-int/2addr v0, v2

    .line 182
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mName:Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mName:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_3
    add-int/2addr v0, v2

    .line 183
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mDescription:Ljava/lang/String;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mDescription:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_4
    add-int/2addr v0, v2

    .line 184
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mCapabilities:Ljava/lang/String;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mCapabilities:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_5
    add-int/2addr v0, v2

    .line 185
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mDefaultValues:Ljava/lang/String;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mDefaultValues:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_6
    add-int/2addr v0, v2

    .line 186
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mCapabilitiesHash:Ljava/lang/String;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mCapabilitiesHash:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_7
    add-int/2addr v0, v2

    .line 187
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mStatus:Ljava/lang/String;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mStatus:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_8
    add-int/2addr v0, v2

    .line 188
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mApplicationVersionTag:Ljava/lang/String;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mApplicationVersionTag:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_9
    add-int/2addr v0, v2

    .line 190
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mProtocolVersionTag:Ljava/lang/String;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mProtocolVersionTag:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_a
    add-int/2addr v0, v2

    .line 192
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mC2dmRegistrationIdTag:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mC2dmRegistrationIdTag:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    .line 194
    return v0

    :cond_1
    move v0, v1

    .line 179
    goto/16 :goto_0

    :cond_2
    move v0, v1

    .line 180
    goto/16 :goto_1

    :cond_3
    move v0, v1

    .line 181
    goto :goto_2

    :cond_4
    move v0, v1

    .line 182
    goto :goto_3

    :cond_5
    move v0, v1

    .line 183
    goto :goto_4

    :cond_6
    move v0, v1

    .line 184
    goto :goto_5

    :cond_7
    move v0, v1

    .line 185
    goto :goto_6

    :cond_8
    move v0, v1

    .line 186
    goto :goto_7

    :cond_9
    move v0, v1

    .line 187
    goto :goto_8

    :cond_a
    move v0, v1

    .line 188
    goto :goto_9

    :cond_b
    move v0, v1

    .line 190
    goto :goto_a
.end method

.method public withApplicationVersionTag(Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;
    .locals 1

    .prologue
    .line 138
    iput-object p1, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mApplicationVersionTag:Ljava/lang/String;

    .line 139
    invoke-direct {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->build()Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;

    move-result-object v0

    return-object v0
.end method

.method public withC2dmRegistrationIdTag(Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;
    .locals 1

    .prologue
    .line 156
    iput-object p1, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mC2dmRegistrationIdTag:Ljava/lang/String;

    .line 157
    invoke-direct {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->build()Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;

    move-result-object v0

    return-object v0
.end method

.method public withCapabilities(Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;
    .locals 1

    .prologue
    .line 102
    iput-object p1, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mCapabilities:Ljava/lang/String;

    .line 103
    invoke-direct {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->build()Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;

    move-result-object v0

    return-object v0
.end method

.method public withCapabilitiesHash(Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;
    .locals 1

    .prologue
    .line 120
    iput-object p1, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mCapabilitiesHash:Ljava/lang/String;

    .line 121
    invoke-direct {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->build()Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;

    move-result-object v0

    return-object v0
.end method

.method public withDefaultValues(Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;
    .locals 1

    .prologue
    .line 111
    iput-object p1, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mDefaultValues:Ljava/lang/String;

    .line 112
    invoke-direct {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->build()Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;

    move-result-object v0

    return-object v0
.end method

.method public withDescription(Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;
    .locals 1

    .prologue
    .line 93
    iput-object p1, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mDescription:Ljava/lang/String;

    .line 94
    invoke-direct {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->build()Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;

    move-result-object v0

    return-object v0
.end method

.method public withId(Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;
    .locals 1

    .prologue
    .line 54
    iput-object p1, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mId:Ljava/lang/String;

    .line 55
    invoke-direct {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->build()Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;

    move-result-object v0

    return-object v0
.end method

.method public withName(Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;
    .locals 1

    .prologue
    .line 84
    iput-object p1, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mName:Ljava/lang/String;

    .line 85
    invoke-direct {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->build()Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;

    move-result-object v0

    return-object v0
.end method

.method public withProtocolVersionTag(Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;
    .locals 1

    .prologue
    .line 147
    iput-object p1, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mProtocolVersionTag:Ljava/lang/String;

    .line 148
    invoke-direct {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->build()Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;

    move-result-object v0

    return-object v0
.end method

.method public withProxy(Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;
    .locals 1

    .prologue
    .line 66
    iput-object p1, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mProxy:Ljava/lang/String;

    .line 67
    invoke-direct {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->build()Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;

    move-result-object v0

    return-object v0
.end method

.method public withStatus(Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;
    .locals 1

    .prologue
    .line 129
    iput-object p1, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mStatus:Ljava/lang/String;

    .line 130
    invoke-direct {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->build()Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;

    move-result-object v0

    return-object v0
.end method

.method public withType(Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;
    .locals 1

    .prologue
    .line 75
    iput-object p1, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->mType:Ljava/lang/String;

    .line 76
    invoke-direct {p0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;->build()Lcom/google/android/apps/chrome/snapshot/cloudprint/PrinterData;

    move-result-object v0

    return-object v0
.end method
