.class public Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPage;
.super Ljava/lang/Object;
.source "IncognitoNewTabPage.java"

# interfaces
.implements Lcom/google/android/apps/chrome/compositor/InvalidationAwareThumbnailProvider;
.implements Lorg/chromium/chrome/browser/NativePage;


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final mBackgroundColor:I

.field private final mIncognitoNewTabPageManager:Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPageView$IncognitoNewTabPageManager;

.field private final mIncognitoNewTabPageView:Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPageView;

.field private final mTab:Lorg/chromium/chrome/browser/Tab;

.field private final mTitle:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const-class v0, Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPage;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPage;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Lorg/chromium/chrome/browser/Tab;)V
    .locals 3

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    new-instance v0, Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPage$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPage$1;-><init>(Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPage;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPage;->mIncognitoNewTabPageManager:Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPageView$IncognitoNewTabPageManager;

    .line 60
    iput-object p2, p0, Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPage;->mTab:Lorg/chromium/chrome/browser/Tab;

    .line 62
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$string;->button_new_tab:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPage;->mTitle:Ljava/lang/String;

    .line 63
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$color;->ntp_bg_incognito:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPage;->mBackgroundColor:I

    .line 65
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 66
    sget v1, Lcom/google/android/apps/chrome/R$layout;->new_tab_page_incognito:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPageView;

    iput-object v0, p0, Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPage;->mIncognitoNewTabPageView:Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPageView;

    .line 68
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPage;->mIncognitoNewTabPageView:Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPageView;

    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPage;->mIncognitoNewTabPageManager:Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPageView$IncognitoNewTabPageManager;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPageView;->initialize(Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPageView$IncognitoNewTabPageManager;)V

    .line 69
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPage;)Lorg/chromium/chrome/browser/Tab;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPage;->mTab:Lorg/chromium/chrome/browser/Tab;

    return-object v0
.end method


# virtual methods
.method public captureThumbnail(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPage;->mIncognitoNewTabPageView:Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPageView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPageView;->captureThumbnail(Landroid/graphics/Canvas;)V

    .line 117
    return-void
.end method

.method public destroy()V
    .locals 2

    .prologue
    .line 75
    sget-boolean v0, Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPage;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPage;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    const-string/jumbo v1, "Destroy called before removed from window"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 76
    :cond_0
    return-void
.end method

.method public getBackgroundColor()I
    .locals 1

    .prologue
    .line 90
    iget v0, p0, Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPage;->mBackgroundColor:I

    return v0
.end method

.method public getHost()Ljava/lang/String;
    .locals 1

    .prologue
    .line 100
    const-string/jumbo v0, "newtab"

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPage;->mTitle:Ljava/lang/String;

    return-object v0
.end method

.method public getUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 80
    const-string/jumbo v0, "chrome-native://newtab/"

    return-object v0
.end method

.method public getView()Landroid/view/View;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPage;->mIncognitoNewTabPageView:Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPageView;

    return-object v0
.end method

.method public shouldCaptureThumbnail()Z
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPage;->mIncognitoNewTabPageView:Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPageView;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPageView;->shouldCaptureThumbnail()Z

    move-result v0

    return v0
.end method

.method public updateForUrl(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 105
    return-void
.end method
