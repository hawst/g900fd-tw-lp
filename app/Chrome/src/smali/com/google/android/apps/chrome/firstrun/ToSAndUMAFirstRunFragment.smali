.class public Lcom/google/android/apps/chrome/firstrun/ToSAndUMAFirstRunFragment;
.super Lcom/google/android/apps/chrome/firstrun/FirstRunPage;
.source "ToSAndUMAFirstRunFragment.java"


# instance fields
.field private mAcceptButton:Landroid/widget/Button;

.field private mSendReportCheckBox:Landroid/widget/CheckBox;

.field private mTosAndPrivacy:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunPage;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/firstrun/ToSAndUMAFirstRunFragment;)Landroid/widget/CheckBox;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/ToSAndUMAFirstRunFragment;->mSendReportCheckBox:Landroid/widget/CheckBox;

    return-object v0
.end method


# virtual methods
.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 38
    sget v0, Lcom/google/android/apps/chrome/R$layout;->fre_tosanduma:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 43
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/chrome/firstrun/FirstRunPage;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 45
    sget v0, Lcom/google/android/apps/chrome/R$id;->terms_accept:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/apps/chrome/firstrun/ToSAndUMAFirstRunFragment;->mAcceptButton:Landroid/widget/Button;

    .line 46
    sget v0, Lcom/google/android/apps/chrome/R$id;->send_report_checkbox:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/google/android/apps/chrome/firstrun/ToSAndUMAFirstRunFragment;->mSendReportCheckBox:Landroid/widget/CheckBox;

    .line 47
    sget v0, Lcom/google/android/apps/chrome/R$id;->tos_and_privacy:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/chrome/firstrun/ToSAndUMAFirstRunFragment;->mTosAndPrivacy:Landroid/widget/TextView;

    .line 49
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/ToSAndUMAFirstRunFragment;->mAcceptButton:Landroid/widget/Button;

    new-instance v3, Lcom/google/android/apps/chrome/firstrun/ToSAndUMAFirstRunFragment$1;

    invoke-direct {v3, p0}, Lcom/google/android/apps/chrome/firstrun/ToSAndUMAFirstRunFragment$1;-><init>(Lcom/google/android/apps/chrome/firstrun/ToSAndUMAFirstRunFragment;)V

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 55
    iget-object v3, p0, Lcom/google/android/apps/chrome/firstrun/ToSAndUMAFirstRunFragment;->mSendReportCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/ToSAndUMAFirstRunFragment;->getPageDelegate()Lcom/google/android/apps/chrome/firstrun/FirstRunPageDelegate;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/firstrun/FirstRunPageDelegate;->isNeverUploadCrashDump()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 57
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/ToSAndUMAFirstRunFragment;->mTosAndPrivacy:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 59
    new-instance v0, Lcom/google/android/apps/chrome/firstrun/ToSAndUMAFirstRunFragment$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/firstrun/ToSAndUMAFirstRunFragment$2;-><init>(Lcom/google/android/apps/chrome/firstrun/ToSAndUMAFirstRunFragment;)V

    .line 67
    new-instance v3, Lcom/google/android/apps/chrome/firstrun/ToSAndUMAFirstRunFragment$3;

    invoke-direct {v3, p0}, Lcom/google/android/apps/chrome/firstrun/ToSAndUMAFirstRunFragment$3;-><init>(Lcom/google/android/apps/chrome/firstrun/ToSAndUMAFirstRunFragment;)V

    .line 74
    iget-object v4, p0, Lcom/google/android/apps/chrome/firstrun/ToSAndUMAFirstRunFragment;->mTosAndPrivacy:Landroid/widget/TextView;

    sget v5, Lcom/google/android/apps/chrome/R$string;->fre_tos_and_privacy:I

    invoke-virtual {p0, v5}, Lcom/google/android/apps/chrome/firstrun/ToSAndUMAFirstRunFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x2

    new-array v6, v6, [Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;

    new-instance v7, Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;

    const-string/jumbo v8, "<LINK1>"

    const-string/jumbo v9, "</LINK1>"

    invoke-direct {v7, v8, v9, v0}, Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    aput-object v7, v6, v2

    new-instance v0, Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;

    const-string/jumbo v2, "<LINK2>"

    const-string/jumbo v7, "</LINK2>"

    invoke-direct {v0, v2, v7, v3}, Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    aput-object v0, v6, v1

    invoke-static {v5, v6}, Lcom/google/android/apps/chrome/utilities/StringLinkifier;->linkify(Ljava/lang/String;[Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;)Landroid/text/SpannableString;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 77
    return-void

    :cond_0
    move v0, v2

    .line 55
    goto :goto_0
.end method

.method public shouldSkipPageOnResume(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 81
    invoke-static {p1}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getFirstRunEulaAccepted()Z

    move-result v0

    return v0
.end method
