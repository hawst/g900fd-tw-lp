.class Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$7;
.super Ljava/lang/Object;
.source "YouTubeMediaRouteController.java"

# interfaces
.implements Lcom/google/android/apps/chrome/videofling/MediaRouteController$ResultBundleHandler;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;)V
    .locals 0

    .prologue
    .line 531
    iput-object p1, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$7;->this$0:Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 539
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$7;->this$0:Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;

    # getter for: Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mDebug:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->access$1100(Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 540
    const-string/jumbo v0, "YouTubeMediaRouteController"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "setupMediaStatusUpdateIntent failed: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " data: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 543
    :cond_0
    return-void
.end method

.method public onResult(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 534
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$7;->this$0:Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;

    # getter for: Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mDebug:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->access$1100(Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "YouTubeMediaRouteController"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "setupMediaStatusUpdateIntent success. data: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 535
    :cond_0
    return-void
.end method
