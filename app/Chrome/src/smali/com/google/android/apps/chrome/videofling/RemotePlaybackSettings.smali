.class public Lcom/google/android/apps/chrome/videofling/RemotePlaybackSettings;
.super Ljava/lang/Object;
.source "RemotePlaybackSettings.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getDeviceId(Landroid/content/Context;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 26
    invoke-static {p0}, Lcom/google/android/apps/chrome/videofling/RemotePlaybackSettings;->getPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "remote_device_id"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getLastPlayedTime(Landroid/content/Context;)J
    .locals 4

    .prologue
    .line 58
    invoke-static {p0}, Lcom/google/android/apps/chrome/videofling/RemotePlaybackSettings;->getPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "last_video_time_played"

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public static getLastVideoState(Landroid/content/Context;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 66
    invoke-static {p0}, Lcom/google/android/apps/chrome/videofling/RemotePlaybackSettings;->getPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "last_video_state"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static getPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;
    .locals 1

    .prologue
    .line 108
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0
.end method

.method public static getRemainingTime(Landroid/content/Context;)J
    .locals 4

    .prologue
    .line 50
    invoke-static {p0}, Lcom/google/android/apps/chrome/videofling/RemotePlaybackSettings;->getPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "last_video_time_remaining"

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public static getSessionId(Landroid/content/Context;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 34
    invoke-static {p0}, Lcom/google/android/apps/chrome/videofling/RemotePlaybackSettings;->getPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "session_id"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getShouldReconnectToRemote(Landroid/content/Context;)Z
    .locals 3

    .prologue
    .line 42
    invoke-static {p0}, Lcom/google/android/apps/chrome/videofling/RemotePlaybackSettings;->getPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "reconnect_remote_device"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static getUriPlaying(Landroid/content/Context;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 82
    invoke-static {p0}, Lcom/google/android/apps/chrome/videofling/RemotePlaybackSettings;->getPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "uri playing"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static putValue(Landroid/content/SharedPreferences;Ljava/lang/String;J)V
    .locals 2

    .prologue
    .line 102
    invoke-interface {p0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 103
    invoke-interface {v0, p1, p2, p3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 104
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 105
    return-void
.end method

.method private static putValue(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 90
    invoke-interface {p0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 91
    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 92
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 93
    return-void
.end method

.method private static putValue(Landroid/content/SharedPreferences;Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 96
    invoke-interface {p0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 97
    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 98
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 99
    return-void
.end method

.method public static setDeviceId(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 30
    invoke-static {p0}, Lcom/google/android/apps/chrome/videofling/RemotePlaybackSettings;->getPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "remote_device_id"

    invoke-static {v0, v1, p1}, Lcom/google/android/apps/chrome/videofling/RemotePlaybackSettings;->putValue(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;)V

    .line 31
    return-void
.end method

.method public static setLastPlayedTime(Landroid/content/Context;J)V
    .locals 3

    .prologue
    .line 62
    invoke-static {p0}, Lcom/google/android/apps/chrome/videofling/RemotePlaybackSettings;->getPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "last_video_time_played"

    invoke-static {v0, v1, p1, p2}, Lcom/google/android/apps/chrome/videofling/RemotePlaybackSettings;->putValue(Landroid/content/SharedPreferences;Ljava/lang/String;J)V

    .line 63
    return-void
.end method

.method public static setLastVideoState(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 70
    invoke-static {p0}, Lcom/google/android/apps/chrome/videofling/RemotePlaybackSettings;->getPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "last_video_state"

    invoke-static {v0, v1, p1}, Lcom/google/android/apps/chrome/videofling/RemotePlaybackSettings;->putValue(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    return-void
.end method

.method public static setPlayerInUse(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 78
    invoke-static {p0}, Lcom/google/android/apps/chrome/videofling/RemotePlaybackSettings;->getPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "player_in_use"

    invoke-static {v0, v1, p1}, Lcom/google/android/apps/chrome/videofling/RemotePlaybackSettings;->putValue(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    return-void
.end method

.method public static setRemainingTime(Landroid/content/Context;J)V
    .locals 3

    .prologue
    .line 54
    invoke-static {p0}, Lcom/google/android/apps/chrome/videofling/RemotePlaybackSettings;->getPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "last_video_time_remaining"

    invoke-static {v0, v1, p1, p2}, Lcom/google/android/apps/chrome/videofling/RemotePlaybackSettings;->putValue(Landroid/content/SharedPreferences;Ljava/lang/String;J)V

    .line 55
    return-void
.end method

.method public static setSessionId(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 38
    invoke-static {p0}, Lcom/google/android/apps/chrome/videofling/RemotePlaybackSettings;->getPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "session_id"

    invoke-static {v0, v1, p1}, Lcom/google/android/apps/chrome/videofling/RemotePlaybackSettings;->putValue(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    return-void
.end method

.method public static setShouldReconnectToRemote(Landroid/content/Context;Z)V
    .locals 2

    .prologue
    .line 46
    invoke-static {p0}, Lcom/google/android/apps/chrome/videofling/RemotePlaybackSettings;->getPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "reconnect_remote_device"

    invoke-static {v0, v1, p1}, Lcom/google/android/apps/chrome/videofling/RemotePlaybackSettings;->putValue(Landroid/content/SharedPreferences;Ljava/lang/String;Z)V

    .line 47
    return-void
.end method

.method public static setUriPlaying(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 86
    invoke-static {p0}, Lcom/google/android/apps/chrome/videofling/RemotePlaybackSettings;->getPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "uri playing"

    invoke-static {v0, v1, p1}, Lcom/google/android/apps/chrome/videofling/RemotePlaybackSettings;->putValue(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    return-void
.end method
