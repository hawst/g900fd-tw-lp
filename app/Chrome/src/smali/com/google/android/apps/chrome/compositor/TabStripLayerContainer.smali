.class public Lcom/google/android/apps/chrome/compositor/TabStripLayerContainer;
.super Ljava/lang/Object;
.source "TabStripLayerContainer.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private mNativeTabStripLayerContainer:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const-class v0, Lcom/google/android/apps/chrome/compositor/TabStripLayerContainer;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/compositor/TabStripLayerContainer;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(J)V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-wide p1, p0, Lcom/google/android/apps/chrome/compositor/TabStripLayerContainer;->mNativeTabStripLayerContainer:J

    .line 28
    return-void
.end method

.method private static create(J)Lcom/google/android/apps/chrome/compositor/TabStripLayerContainer;
    .locals 2

    .prologue
    .line 122
    new-instance v0, Lcom/google/android/apps/chrome/compositor/TabStripLayerContainer;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/chrome/compositor/TabStripLayerContainer;-><init>(J)V

    return-object v0
.end method

.method private destroy()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 127
    sget-boolean v0, Lcom/google/android/apps/chrome/compositor/TabStripLayerContainer;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/apps/chrome/compositor/TabStripLayerContainer;->mNativeTabStripLayerContainer:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 128
    :cond_0
    iput-wide v2, p0, Lcom/google/android/apps/chrome/compositor/TabStripLayerContainer;->mNativeTabStripLayerContainer:J

    .line 129
    return-void
.end method

.method private native nativeBeginBuildingFrame(J)V
.end method

.method private native nativeFinishBuildingFrame(J)V
.end method

.method private native nativePutStripTabLayer(JIIIZZFFFFFFFZFF)V
.end method

.method private native nativeUpdateModelSelectorButton(JIFFFFZZ)V
.end method

.method private native nativeUpdateNewTabButton(JIFFFFZ)V
.end method

.method private native nativeUpdateTabStripLayer(JFFFF)V
.end method

.method private pushButtonsAndBackground(Landroid/content/Context;Lcom/google/android/apps/chrome/tabs/layout/LayoutProvider;Lcom/google/android/apps/chrome/tabs/layout/Layout;)V
    .locals 12

    .prologue
    .line 62
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    .line 64
    invoke-interface {p2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutProvider;->getFullscreenManager()Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;

    move-result-object v1

    .line 65
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->getControlOffset()F

    move-result v6

    .line 67
    :goto_0
    iget-wide v2, p0, Lcom/google/android/apps/chrome/compositor/TabStripLayerContainer;->mNativeTabStripLayerContainer:J

    invoke-virtual {p3}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->getStripWidth()F

    move-result v1

    mul-float v4, v1, v0

    invoke-virtual {p3}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->getStripHeight()F

    move-result v1

    mul-float v5, v1, v0

    invoke-virtual {p3}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->getStripBrightness()F

    move-result v7

    move-object v1, p0

    invoke-direct/range {v1 .. v7}, Lcom/google/android/apps/chrome/compositor/TabStripLayerContainer;->nativeUpdateTabStripLayer(JFFFF)V

    .line 70
    invoke-virtual {p3}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->getNewTabButton()Lcom/google/android/apps/chrome/tabs/CompositorButton;

    move-result-object v1

    .line 71
    invoke-virtual {p3}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->getModelSelectorButton()Lcom/google/android/apps/chrome/tabs/CompositorButton;

    move-result-object v11

    .line 72
    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tabs/CompositorButton;->isVisible()Z

    move-result v9

    .line 73
    invoke-virtual {v11}, Lcom/google/android/apps/chrome/tabs/CompositorButton;->isVisible()Z

    move-result v10

    .line 76
    invoke-static {p1}, Lcom/google/android/apps/chrome/device/DeviceClassManager;->isAccessibilityModeEnabled(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 77
    const/4 v9, 0x0

    .line 78
    const/4 v10, 0x0

    .line 81
    :cond_0
    iget-wide v2, p0, Lcom/google/android/apps/chrome/compositor/TabStripLayerContainer;->mNativeTabStripLayerContainer:J

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tabs/CompositorButton;->getResourceId()I

    move-result v4

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tabs/CompositorButton;->getX()F

    move-result v5

    mul-float/2addr v5, v0

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tabs/CompositorButton;->getY()F

    move-result v6

    mul-float/2addr v6, v0

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tabs/CompositorButton;->getWidth()F

    move-result v7

    mul-float/2addr v7, v0

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tabs/CompositorButton;->getHeight()F

    move-result v1

    mul-float v8, v1, v0

    move-object v1, p0

    invoke-direct/range {v1 .. v9}, Lcom/google/android/apps/chrome/compositor/TabStripLayerContainer;->nativeUpdateNewTabButton(JIFFFFZ)V

    .line 87
    iget-wide v2, p0, Lcom/google/android/apps/chrome/compositor/TabStripLayerContainer;->mNativeTabStripLayerContainer:J

    invoke-virtual {v11}, Lcom/google/android/apps/chrome/tabs/CompositorButton;->getResourceId()I

    move-result v4

    invoke-virtual {v11}, Lcom/google/android/apps/chrome/tabs/CompositorButton;->getX()F

    move-result v1

    mul-float v5, v1, v0

    invoke-virtual {v11}, Lcom/google/android/apps/chrome/tabs/CompositorButton;->getY()F

    move-result v1

    mul-float v6, v1, v0

    invoke-virtual {v11}, Lcom/google/android/apps/chrome/tabs/CompositorButton;->getWidth()F

    move-result v1

    mul-float v7, v1, v0

    invoke-virtual {v11}, Lcom/google/android/apps/chrome/tabs/CompositorButton;->getHeight()F

    move-result v1

    mul-float v8, v1, v0

    invoke-virtual {v11}, Lcom/google/android/apps/chrome/tabs/CompositorButton;->isIncognito()Z

    move-result v9

    move-object v1, p0

    invoke-direct/range {v1 .. v10}, Lcom/google/android/apps/chrome/compositor/TabStripLayerContainer;->nativeUpdateModelSelectorButton(JIFFFFZZ)V

    .line 92
    return-void

    .line 65
    :cond_1
    const/4 v6, 0x0

    goto :goto_0
.end method

.method private pushStripTabs(Landroid/content/Context;Lcom/google/android/apps/chrome/tabs/layout/LayoutProvider;Lcom/google/android/apps/chrome/tabs/layout/Layout;)V
    .locals 24

    .prologue
    .line 96
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v0, v2, Landroid/util/DisplayMetrics;->density:F

    move/from16 v22, v0

    .line 97
    invoke-virtual/range {p3 .. p3}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->getStripLayoutTabsToRender()[Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    move-result-object v23

    .line 98
    if-eqz v23, :cond_0

    move-object/from16 v0, v23

    array-length v2, v0

    .line 100
    :goto_0
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/google/android/apps/chrome/compositor/TabStripLayerContainer;->mNativeTabStripLayerContainer:J

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5}, Lcom/google/android/apps/chrome/compositor/TabStripLayerContainer;->nativeBeginBuildingFrame(J)V

    .line 101
    const/4 v3, 0x0

    move/from16 v21, v3

    :goto_1
    move/from16 v0, v21

    if-ge v0, v2, :cond_3

    .line 102
    aget-object v19, v23, v21

    .line 103
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/google/android/apps/chrome/compositor/TabStripLayerContainer;->mNativeTabStripLayerContainer:J

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->getId()I

    move-result v6

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->getCloseButton()Lcom/google/android/apps/chrome/tabs/CompositorButton;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/CompositorButton;->getResourceId()I

    move-result v7

    add-int/lit8 v3, v2, -0x1

    move/from16 v0, v21

    if-ne v0, v3, :cond_1

    const/4 v3, 0x1

    :goto_2
    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->getResourceId(Z)I

    move-result v8

    add-int/lit8 v3, v2, -0x1

    move/from16 v0, v21

    if-ne v0, v3, :cond_2

    const/4 v9, 0x1

    :goto_3
    invoke-virtual/range {v19 .. v19}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->getClosePressed()Z

    move-result v10

    invoke-virtual/range {p3 .. p3}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->getStripWidth()F

    move-result v3

    mul-float v11, v3, v22

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->getDrawX()F

    move-result v3

    mul-float v12, v3, v22

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->getDrawY()F

    move-result v3

    mul-float v13, v3, v22

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->getWidth()F

    move-result v3

    mul-float v14, v3, v22

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->getHeight()F

    move-result v3

    mul-float v15, v3, v22

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->getContentOffsetX()F

    move-result v3

    mul-float v16, v3, v22

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->getCloseButton()Lcom/google/android/apps/chrome/tabs/CompositorButton;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabs/CompositorButton;->getOpacity()F

    move-result v17

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->isLoading()Z

    move-result v18

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;->getLoadingSpinnerRotation()F

    move-result v19

    invoke-virtual/range {p3 .. p3}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->getStripBorderOpacity()F

    move-result v20

    move-object/from16 v3, p0

    invoke-direct/range {v3 .. v20}, Lcom/google/android/apps/chrome/compositor/TabStripLayerContainer;->nativePutStripTabLayer(JIIIZZFFFFFFFZFF)V

    .line 101
    add-int/lit8 v3, v21, 0x1

    move/from16 v21, v3

    goto :goto_1

    .line 98
    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    .line 103
    :cond_1
    const/4 v3, 0x0

    goto :goto_2

    :cond_2
    const/4 v9, 0x0

    goto :goto_3

    .line 117
    :cond_3
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/google/android/apps/chrome/compositor/TabStripLayerContainer;->mNativeTabStripLayerContainer:J

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3}, Lcom/google/android/apps/chrome/compositor/TabStripLayerContainer;->nativeFinishBuildingFrame(J)V

    .line 118
    return-void
.end method


# virtual methods
.method public pushAndUpdateStrip(Landroid/content/Context;Lcom/google/android/apps/chrome/tabs/layout/LayoutProvider;)V
    .locals 4

    .prologue
    .line 39
    iget-wide v0, p0, Lcom/google/android/apps/chrome/compositor/TabStripLayerContainer;->mNativeTabStripLayerContainer:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 57
    :goto_0
    return-void

    .line 41
    :cond_0
    invoke-interface {p2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutProvider;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    .line 43
    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/chrome/compositor/TabStripLayerContainer;->pushButtonsAndBackground(Landroid/content/Context;Lcom/google/android/apps/chrome/tabs/layout/LayoutProvider;Lcom/google/android/apps/chrome/tabs/layout/Layout;)V

    .line 49
    invoke-static {p1}, Lcom/google/android/apps/chrome/device/DeviceClassManager;->isAccessibilityModeEnabled(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 51
    iget-wide v0, p0, Lcom/google/android/apps/chrome/compositor/TabStripLayerContainer;->mNativeTabStripLayerContainer:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/compositor/TabStripLayerContainer;->nativeBeginBuildingFrame(J)V

    .line 52
    iget-wide v0, p0, Lcom/google/android/apps/chrome/compositor/TabStripLayerContainer;->mNativeTabStripLayerContainer:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/compositor/TabStripLayerContainer;->nativeFinishBuildingFrame(J)V

    goto :goto_0

    .line 56
    :cond_1
    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/chrome/compositor/TabStripLayerContainer;->pushStripTabs(Landroid/content/Context;Lcom/google/android/apps/chrome/tabs/layout/LayoutProvider;Lcom/google/android/apps/chrome/tabs/layout/Layout;)V

    goto :goto_0
.end method
