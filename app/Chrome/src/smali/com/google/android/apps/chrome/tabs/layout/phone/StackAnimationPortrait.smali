.class Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimationPortrait;
.super Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation;
.source "StackAnimationPortrait.java"


# direct methods
.method public constructor <init>(FFFFFF)V
    .locals 0

    .prologue
    .line 35
    invoke-direct/range {p0 .. p6}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation;-><init>(FFFFFF)V

    .line 37
    return-void
.end method


# virtual methods
.method protected addTiltScrollAnimation(Lcom/google/android/apps/chrome/ChromeAnimation;Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;FII)V
    .locals 10

    .prologue
    .line 227
    sget-object v3, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;->TILTX:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

    invoke-virtual {p2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getTiltX()F

    move-result v4

    int-to-long v6, p4

    int-to-long v8, p5

    move-object v1, p1

    move-object v2, p2

    move v5, p3

    invoke-static/range {v1 .. v9}, Lcom/google/android/apps/chrome/ChromeAnimation$AnimatableAnimation;->addAnimation(Lcom/google/android/apps/chrome/ChromeAnimation;Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJ)V

    .line 228
    return-void
.end method

.method protected createEnterStackAnimatorSet([Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;IIF)Lcom/google/android/apps/chrome/ChromeAnimation;
    .locals 17

    .prologue
    .line 42
    new-instance v3, Lcom/google/android/apps/chrome/ChromeAnimation;

    invoke-direct {v3}, Lcom/google/android/apps/chrome/ChromeAnimation;-><init>()V

    .line 43
    const/4 v2, 0x0

    move/from16 v0, p4

    invoke-static {v2, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->screenToScroll(FF)F

    move-result v6

    .line 45
    const/4 v2, 0x0

    .line 46
    if-ltz p2, :cond_0

    move-object/from16 v0, p1

    array-length v4, v0

    add-int/lit8 v4, v4, -0x1

    move/from16 v0, p2

    if-ge v0, v4, :cond_0

    .line 47
    aget-object v2, p1, p2

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getScrollOffset()F

    move-result v4

    .line 48
    add-int/lit8 v2, p2, 0x1

    aget-object v2, p1, v2

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getScrollOffset()F

    move-result v5

    .line 49
    if-nez p2, :cond_1

    move/from16 v0, p3

    int-to-float v2, v0

    .line 50
    :goto_0
    aget-object v7, p1, p2

    invoke-virtual {v7}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getLayoutTab()Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getScaledContentHeight()F

    move-result v7

    const v8, 0x3eb33333    # 0.35f

    mul-float/2addr v7, v8

    .line 52
    sub-float/2addr v4, v5

    add-float/2addr v2, v4

    add-float/2addr v2, v7

    const/4 v4, 0x0

    invoke-static {v2, v4}, Ljava/lang/Math;->max(FF)F

    move-result v2

    .line 55
    :cond_0
    const/4 v4, 0x0

    move/from16 v16, v4

    :goto_1
    move-object/from16 v0, p1

    array-length v4, v0

    move/from16 v0, v16

    if-ge v0, v4, :cond_4

    .line 56
    aget-object v4, p1, v16

    .line 58
    invoke-virtual {v4}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->resetOffset()V

    .line 59
    const v5, 0x3f666666    # 0.9f

    invoke-virtual {v4, v5}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->setScale(F)V

    .line 60
    const/high16 v5, 0x3f800000    # 1.0f

    invoke-virtual {v4, v5}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->setAlpha(F)V

    .line 61
    invoke-virtual {v4}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getLayoutTab()Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v5

    const/4 v7, 0x0

    invoke-virtual {v5, v7}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setToolbarAlpha(F)V

    .line 62
    invoke-virtual {v4}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getLayoutTab()Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v5

    const/high16 v7, 0x3f800000    # 1.0f

    invoke-virtual {v5, v7}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setBorderScale(F)V

    .line 64
    mul-int v5, v16, p3

    int-to-float v5, v5

    move/from16 v0, p4

    invoke-static {v5, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->screenToScroll(FF)F

    move-result v7

    .line 66
    move/from16 v0, v16

    move/from16 v1, p2

    if-ge v0, v1, :cond_2

    .line 67
    invoke-virtual {v4}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getLayoutTab()Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v5

    move-object/from16 v0, p0

    iget v8, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimationPortrait;->mHeightMinusTopControls:F

    invoke-virtual {v5, v8}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setMaxContentHeight(F)V

    .line 68
    sget-object v5, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab$Property;->SCROLL_OFFSET:Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab$Property;

    const-wide/16 v8, 0x12c

    const-wide/16 v10, 0x0

    invoke-static/range {v3 .. v11}, Lcom/google/android/apps/chrome/ChromeAnimation$AnimatableAnimation;->addAnimation(Lcom/google/android/apps/chrome/ChromeAnimation;Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJ)V

    .line 55
    :goto_2
    add-int/lit8 v4, v16, 0x1

    move/from16 v16, v4

    goto :goto_1

    .line 49
    :cond_1
    const/4 v2, 0x0

    goto :goto_0

    .line 70
    :cond_2
    move/from16 v0, v16

    move/from16 v1, p2

    if-le v0, v1, :cond_3

    .line 71
    invoke-virtual {v4}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getLayoutTab()Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v5

    move-object/from16 v0, p0

    iget v8, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimationPortrait;->mHeightMinusTopControls:F

    invoke-virtual {v5, v8}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setMaxContentHeight(F)V

    .line 72
    add-float v5, v7, v2

    invoke-virtual {v4, v5}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->setScrollOffset(F)V

    .line 73
    sget-object v9, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab$Property;->Y_IN_STACK_OFFSET:Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab$Property;

    move-object/from16 v0, p0

    iget v10, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimationPortrait;->mHeight:F

    const/4 v11, 0x0

    const-wide/16 v12, 0x12c

    const-wide/16 v14, 0x0

    move-object v7, v3

    move-object v8, v4

    invoke-static/range {v7 .. v15}, Lcom/google/android/apps/chrome/ChromeAnimation$AnimatableAnimation;->addAnimation(Lcom/google/android/apps/chrome/ChromeAnimation;Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJ)V

    goto :goto_2

    .line 76
    :cond_3
    invoke-virtual {v4, v7}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->setScrollOffset(F)V

    .line 78
    invoke-virtual {v4}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getLayoutTab()Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v8

    sget-object v9, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;->MAX_CONTENT_HEIGHT:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getLayoutTab()Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getUnclampedOriginalContentHeight()F

    move-result v10

    move-object/from16 v0, p0

    iget v11, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimationPortrait;->mHeightMinusTopControls:F

    const-wide/16 v12, 0x12c

    const-wide/16 v14, 0xa

    move-object v7, v3

    invoke-static/range {v7 .. v15}, Lcom/google/android/apps/chrome/ChromeAnimation$AnimatableAnimation;->addAnimation(Lcom/google/android/apps/chrome/ChromeAnimation;Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJ)V

    .line 82
    sget-object v9, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab$Property;->Y_IN_STACK_INFLUENCE:Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab$Property;

    const/4 v10, 0x0

    const/high16 v11, 0x3f800000    # 1.0f

    const-wide/16 v12, 0xc8

    const-wide/16 v14, 0x0

    move-object v7, v3

    move-object v8, v4

    invoke-static/range {v7 .. v15}, Lcom/google/android/apps/chrome/ChromeAnimation$AnimatableAnimation;->addAnimation(Lcom/google/android/apps/chrome/ChromeAnimation;Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJ)V

    .line 84
    sget-object v9, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab$Property;->SCALE:Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab$Property;

    const/high16 v10, 0x3f800000    # 1.0f

    const v11, 0x3f666666    # 0.9f

    const-wide/16 v12, 0xc8

    const-wide/16 v14, 0x0

    move-object v7, v3

    move-object v8, v4

    invoke-static/range {v7 .. v15}, Lcom/google/android/apps/chrome/ChromeAnimation$AnimatableAnimation;->addAnimation(Lcom/google/android/apps/chrome/ChromeAnimation;Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJ)V

    .line 86
    invoke-virtual {v4}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getLayoutTab()Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v8

    sget-object v9, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;->TOOLBAR_ALPHA:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

    const/high16 v10, 0x3f800000    # 1.0f

    const/4 v11, 0x0

    const-wide/16 v12, 0xc8

    const-wide/16 v14, 0x64

    move-object v7, v3

    invoke-static/range {v7 .. v15}, Lcom/google/android/apps/chrome/ChromeAnimation$AnimatableAnimation;->addAnimation(Lcom/google/android/apps/chrome/ChromeAnimation;Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJ)V

    .line 88
    invoke-virtual {v4}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getLayoutTab()Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v8

    sget-object v9, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;->TOOLBAR_Y_OFFSET:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

    const/4 v10, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimationPortrait;->getToolbarOffsetToLineUpWithBorder()F

    move-result v11

    const-wide/16 v12, 0xc8

    const-wide/16 v14, 0x0

    move-object v7, v3

    invoke-static/range {v7 .. v15}, Lcom/google/android/apps/chrome/ChromeAnimation$AnimatableAnimation;->addAnimation(Lcom/google/android/apps/chrome/ChromeAnimation;Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJ)V

    .line 91
    invoke-virtual {v4}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getLayoutTab()Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v8

    sget-object v9, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;->SIDE_BORDER_SCALE:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

    const/4 v10, 0x0

    const/high16 v11, 0x3f800000    # 1.0f

    const-wide/16 v12, 0xc8

    const-wide/16 v14, 0x0

    move-object v7, v3

    invoke-static/range {v7 .. v15}, Lcom/google/android/apps/chrome/ChromeAnimation$AnimatableAnimation;->addAnimation(Lcom/google/android/apps/chrome/ChromeAnimation;Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJ)V

    .line 94
    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimationPortrait;->mHeight:F

    move-object/from16 v0, p0

    iget v7, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimationPortrait;->mHeightMinusTopControls:F

    sub-float/2addr v5, v7

    move-object/from16 v0, p0

    iget v7, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimationPortrait;->mBorderTopHeight:F

    sub-float/2addr v5, v7

    invoke-virtual {v4, v5}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->setYOutOfStack(F)V

    goto/16 :goto_2

    .line 98
    :cond_4
    return-object v3
.end method

.method protected createNewTabOpenedAnimatorSet([Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;IF)Lcom/google/android/apps/chrome/ChromeAnimation;
    .locals 1

    .prologue
    .line 210
    neg-float v0, p3

    invoke-super {p0, p1, p2, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimation;->createNewTabOpenedAnimatorSet([Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;IF)Lcom/google/android/apps/chrome/ChromeAnimation;

    move-result-object v0

    return-object v0
.end method

.method protected createReachTopAnimatorSet([Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;F)Lcom/google/android/apps/chrome/ChromeAnimation;
    .locals 11

    .prologue
    .line 191
    new-instance v1, Lcom/google/android/apps/chrome/ChromeAnimation;

    invoke-direct {v1}, Lcom/google/android/apps/chrome/ChromeAnimation;-><init>()V

    .line 193
    const/4 v2, 0x0

    .line 194
    const/4 v0, 0x0

    move v10, v2

    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_0

    .line 195
    aget-object v2, p1, v0

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getLayoutTab()Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getY()F

    move-result v2

    cmpl-float v2, v10, v2

    if-gez v2, :cond_0

    .line 196
    aget-object v2, p1, v0

    sget-object v3, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab$Property;->SCROLL_OFFSET:Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab$Property;

    aget-object v4, p1, v0

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getScrollOffset()F

    move-result v4

    invoke-static {v10, p2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->screenToScroll(FF)F

    move-result v5

    const-wide/16 v6, 0x190

    const-wide/16 v8, 0x0

    invoke-static/range {v1 .. v9}, Lcom/google/android/apps/chrome/ChromeAnimation$AnimatableAnimation;->addAnimation(Lcom/google/android/apps/chrome/ChromeAnimation;Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJ)V

    .line 201
    aget-object v2, p1, v0

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getLayoutTab()Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getScaledContentHeight()F

    move-result v2

    add-float/2addr v2, v10

    .line 194
    add-int/lit8 v0, v0, 0x1

    move v10, v2

    goto :goto_0

    .line 204
    :cond_0
    return-object v1
.end method

.method protected createTabFocusedAnimatorSet([Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;IIF)Lcom/google/android/apps/chrome/ChromeAnimation;
    .locals 16

    .prologue
    .line 104
    new-instance v3, Lcom/google/android/apps/chrome/ChromeAnimation;

    invoke-direct {v3}, Lcom/google/android/apps/chrome/ChromeAnimation;-><init>()V

    .line 105
    const/4 v2, 0x0

    move v14, v2

    :goto_0
    move-object/from16 v0, p1

    array-length v2, v0

    if-ge v14, v2, :cond_3

    .line 106
    aget-object v15, p1, v14

    .line 107
    invoke-virtual {v15}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getLayoutTab()Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v4

    .line 109
    const/4 v5, 0x0

    const/16 v6, 0x190

    const/4 v7, 0x0

    move-object/from16 v2, p0

    invoke-virtual/range {v2 .. v7}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimationPortrait;->addTiltScrollAnimation(Lcom/google/android/apps/chrome/ChromeAnimation;Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;FII)V

    .line 111
    sget-object v7, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab$Property;->DISCARD_AMOUNT:Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab$Property;

    invoke-virtual {v15}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getDiscardAmount()F

    move-result v8

    const/4 v9, 0x0

    const-wide/16 v10, 0x190

    const-wide/16 v12, 0x0

    move-object v5, v3

    move-object v6, v15

    invoke-static/range {v5 .. v13}, Lcom/google/android/apps/chrome/ChromeAnimation$AnimatableAnimation;->addAnimation(Lcom/google/android/apps/chrome/ChromeAnimation;Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJ)V

    .line 114
    move/from16 v0, p2

    if-ge v14, v0, :cond_0

    .line 116
    sget-object v5, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab$Property;->SCROLL_OFFSET:Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab$Property;

    invoke-virtual {v15}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getScrollOffset()F

    move-result v6

    invoke-virtual {v15}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getScrollOffset()F

    move-result v2

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimationPortrait;->mHeight:F

    sub-float/2addr v2, v4

    move/from16 v0, p3

    int-to-float v4, v0

    sub-float v7, v2, v4

    const-wide/16 v8, 0x190

    const-wide/16 v10, 0x0

    move-object v4, v15

    invoke-static/range {v3 .. v11}, Lcom/google/android/apps/chrome/ChromeAnimation$AnimatableAnimation;->addAnimation(Lcom/google/android/apps/chrome/ChromeAnimation;Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJ)V

    .line 105
    :goto_1
    add-int/lit8 v2, v14, 0x1

    move v14, v2

    goto :goto_0

    .line 119
    :cond_0
    move/from16 v0, p2

    if-le v14, v0, :cond_1

    .line 122
    invoke-virtual {v4}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getY()F

    move-result v2

    .line 123
    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimationPortrait;->mHeight:F

    sub-float v2, v4, v2

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimationPortrait;->mHeight:F

    invoke-static {v2, v4, v5}, Lorg/chromium/chrome/browser/util/MathUtils;->clamp(FFF)F

    move-result v2

    .line 124
    const/high16 v4, 0x42c80000    # 100.0f

    mul-float/2addr v2, v4

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimationPortrait;->mHeight:F

    div-float/2addr v2, v4

    .line 125
    sget-object v5, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab$Property;->Y_IN_STACK_OFFSET:Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab$Property;

    invoke-virtual {v15}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getYInStackOffset()F

    move-result v6

    invoke-virtual {v15}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getYInStackOffset()F

    move-result v4

    move-object/from16 v0, p0

    iget v7, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimationPortrait;->mHeight:F

    add-float/2addr v7, v4

    const-wide/16 v8, 0x190

    float-to-long v10, v2

    sub-long/2addr v8, v10

    float-to-long v10, v2

    move-object v4, v15

    invoke-static/range {v3 .. v11}, Lcom/google/android/apps/chrome/ChromeAnimation$AnimatableAnimation;->addAnimation(Lcom/google/android/apps/chrome/ChromeAnimation;Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJ)V

    goto :goto_1

    .line 134
    :cond_1
    const/4 v2, 0x0

    invoke-virtual {v15, v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->setXOutOfStack(F)V

    .line 135
    const/4 v2, 0x0

    invoke-virtual {v15, v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->setYOutOfStack(F)V

    .line 136
    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v4, v2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setBorderScale(F)V

    .line 138
    sget-object v7, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab$Property;->SCROLL_OFFSET:Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab$Property;

    invoke-virtual {v15}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getScrollOffset()F

    move-result v8

    const/4 v2, 0x0

    invoke-virtual {v15}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getScrollOffset()F

    move-result v5

    move-object/from16 v0, p0

    iget v6, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimationPortrait;->mWidth:F

    sub-float/2addr v5, v6

    move/from16 v0, p3

    int-to-float v6, v0

    sub-float/2addr v5, v6

    invoke-static {v2, v5}, Ljava/lang/Math;->max(FF)F

    move-result v9

    const-wide/16 v10, 0x190

    const-wide/16 v12, 0x0

    move-object v5, v3

    move-object v6, v15

    invoke-static/range {v5 .. v13}, Lcom/google/android/apps/chrome/ChromeAnimation$AnimatableAnimation;->addAnimation(Lcom/google/android/apps/chrome/ChromeAnimation;Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJ)V

    .line 141
    sget-object v7, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab$Property;->SCALE:Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab$Property;

    invoke-virtual {v15}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getScale()F

    move-result v8

    const/high16 v9, 0x3f800000    # 1.0f

    const-wide/16 v10, 0x190

    const-wide/16 v12, 0x0

    move-object v5, v3

    move-object v6, v15

    invoke-static/range {v5 .. v13}, Lcom/google/android/apps/chrome/ChromeAnimation$AnimatableAnimation;->addAnimation(Lcom/google/android/apps/chrome/ChromeAnimation;Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJ)V

    .line 143
    sget-object v7, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab$Property;->Y_IN_STACK_INFLUENCE:Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab$Property;

    invoke-virtual {v15}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getYInStackInfluence()F

    move-result v8

    const/4 v9, 0x0

    const-wide/16 v10, 0xc8

    const-wide/16 v12, 0x0

    move-object v5, v3

    move-object v6, v15

    invoke-static/range {v5 .. v13}, Lcom/google/android/apps/chrome/ChromeAnimation$AnimatableAnimation;->addAnimation(Lcom/google/android/apps/chrome/ChromeAnimation;Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJ)V

    .line 145
    invoke-virtual {v15}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getLayoutTab()Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v6

    sget-object v7, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;->MAX_CONTENT_HEIGHT:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

    invoke-virtual {v15}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getLayoutTab()Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getMaxContentHeight()F

    move-result v8

    invoke-virtual {v15}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getLayoutTab()Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getUnclampedOriginalContentHeight()F

    move-result v9

    const-wide/16 v10, 0x190

    const-wide/16 v12, 0x0

    move-object v5, v3

    invoke-static/range {v5 .. v13}, Lcom/google/android/apps/chrome/ChromeAnimation$AnimatableAnimation;->addAnimation(Lcom/google/android/apps/chrome/ChromeAnimation;Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJ)V

    .line 149
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimationPortrait;->mHeight:F

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimationPortrait;->mHeightMinusTopControls:F

    sub-float/2addr v2, v5

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimationPortrait;->mBorderTopHeight:F

    sub-float/2addr v2, v5

    invoke-virtual {v15, v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->setYOutOfStack(F)V

    .line 151
    invoke-virtual {v4}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->shouldStall()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 152
    sget-object v5, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;->SATURATION:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v7, 0x0

    const-wide/16 v8, 0xc8

    const-wide/16 v10, 0x0

    invoke-static/range {v3 .. v11}, Lcom/google/android/apps/chrome/ChromeAnimation$AnimatableAnimation;->addAnimation(Lcom/google/android/apps/chrome/ChromeAnimation;Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJ)V

    .line 155
    :cond_2
    invoke-virtual {v15}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getLayoutTab()Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v2

    sget-object v5, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;->TOOLBAR_ALPHA:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getToolbarAlpha()F

    move-result v6

    const/high16 v7, 0x3f800000    # 1.0f

    const-wide/16 v8, 0xfa

    const-wide/16 v10, 0x0

    move-object v4, v2

    invoke-static/range {v3 .. v11}, Lcom/google/android/apps/chrome/ChromeAnimation$AnimatableAnimation;->addAnimation(Lcom/google/android/apps/chrome/ChromeAnimation;Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJ)V

    .line 157
    invoke-virtual {v15}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getLayoutTab()Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v4

    sget-object v5, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;->TOOLBAR_Y_OFFSET:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimationPortrait;->getToolbarOffsetToLineUpWithBorder()F

    move-result v6

    const/4 v7, 0x0

    const-wide/16 v8, 0xfa

    const-wide/16 v10, 0x0

    invoke-static/range {v3 .. v11}, Lcom/google/android/apps/chrome/ChromeAnimation$AnimatableAnimation;->addAnimation(Lcom/google/android/apps/chrome/ChromeAnimation;Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJ)V

    .line 160
    invoke-virtual {v15}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getLayoutTab()Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v4

    sget-object v5, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;->SIDE_BORDER_SCALE:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v7, 0x0

    const-wide/16 v8, 0xfa

    const-wide/16 v10, 0x0

    invoke-static/range {v3 .. v11}, Lcom/google/android/apps/chrome/ChromeAnimation$AnimatableAnimation;->addAnimation(Lcom/google/android/apps/chrome/ChromeAnimation;Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJ)V

    goto/16 :goto_1

    .line 165
    :cond_3
    return-object v3
.end method

.method protected createViewMoreAnimatorSet([Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;I)Lcom/google/android/apps/chrome/ChromeAnimation;
    .locals 11

    .prologue
    .line 171
    new-instance v1, Lcom/google/android/apps/chrome/ChromeAnimation;

    invoke-direct {v1}, Lcom/google/android/apps/chrome/ChromeAnimation;-><init>()V

    .line 173
    add-int/lit8 v0, p2, 0x1

    array-length v2, p1

    if-lt v0, v2, :cond_1

    .line 186
    :cond_0
    return-object v1

    .line 175
    :cond_1
    aget-object v0, p1, p2

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getScrollOffset()F

    move-result v0

    add-int/lit8 v2, p2, 0x1

    aget-object v2, p1, v2

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getScrollOffset()F

    move-result v2

    sub-float/2addr v0, v2

    aget-object v2, p1, p2

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getLayoutTab()Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getScaledContentHeight()F

    move-result v2

    const/high16 v3, 0x3f400000    # 0.75f

    mul-float/2addr v2, v3

    add-float/2addr v0, v2

    .line 179
    const/high16 v2, 0x43480000    # 200.0f

    invoke-static {v2, v0}, Ljava/lang/Math;->max(FF)F

    move-result v10

    .line 180
    add-int/lit8 v0, p2, 0x1

    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_0

    .line 181
    aget-object v2, p1, v0

    sget-object v3, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab$Property;->SCROLL_OFFSET:Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab$Property;

    aget-object v4, p1, v0

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getScrollOffset()F

    move-result v4

    aget-object v5, p1, v0

    invoke-virtual {v5}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getScrollOffset()F

    move-result v5

    add-float/2addr v5, v10

    const-wide/16 v6, 0x190

    const-wide/16 v8, 0x0

    invoke-static/range {v1 .. v9}, Lcom/google/android/apps/chrome/ChromeAnimation$AnimatableAnimation;->addAnimation(Lcom/google/android/apps/chrome/ChromeAnimation;Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJ)V

    .line 180
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method protected getScreenPositionInScrollDirection(Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;)F
    .locals 1

    .prologue
    .line 221
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/tabs/layout/phone/StackTab;->getLayoutTab()Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getY()F

    move-result v0

    return v0
.end method

.method protected getScreenSizeInScrollDirection()F
    .locals 1

    .prologue
    .line 232
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/StackAnimationPortrait;->mHeight:F

    return v0
.end method

.method protected getTabCreationDirection()I
    .locals 1

    .prologue
    .line 237
    const/4 v0, -0x1

    return v0
.end method

.method protected isDefaultDiscardDirectionPositive()Z
    .locals 1

    .prologue
    .line 216
    invoke-static {}, Lorg/chromium/ui/base/LocalizationUtils;->isLayoutRtl()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
