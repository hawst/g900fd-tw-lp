.class public final Lcom/google/android/apps/chrome/videofling/MediaUrlResolver$Result;
.super Ljava/lang/Object;
.source "MediaUrlResolver.java"


# instance fields
.field private final mRelevantHeaders:[Lorg/apache/http/Header;

.field private final mUri:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;[Lorg/apache/http/Header;)V
    .locals 1

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    iput-object p1, p0, Lcom/google/android/apps/chrome/videofling/MediaUrlResolver$Result;->mUri:Ljava/lang/String;

    .line 63
    if-eqz p2, :cond_0

    array-length v0, p2

    invoke-static {p2, v0}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lorg/apache/http/Header;

    :goto_0
    iput-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaUrlResolver$Result;->mRelevantHeaders:[Lorg/apache/http/Header;

    .line 67
    return-void

    .line 63
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final getRelevantHeaders()[Lorg/apache/http/Header;
    .locals 2

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaUrlResolver$Result;->mRelevantHeaders:[Lorg/apache/http/Header;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaUrlResolver$Result;->mRelevantHeaders:[Lorg/apache/http/Header;

    iget-object v1, p0, Lcom/google/android/apps/chrome/videofling/MediaUrlResolver$Result;->mRelevantHeaders:[Lorg/apache/http/Header;

    array-length v1, v1

    invoke-static {v0, v1}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lorg/apache/http/Header;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getUri()Ljava/lang/String;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaUrlResolver$Result;->mUri:Ljava/lang/String;

    return-object v0
.end method
