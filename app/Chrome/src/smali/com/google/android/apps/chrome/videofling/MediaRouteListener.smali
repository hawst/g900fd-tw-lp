.class public Lcom/google/android/apps/chrome/videofling/MediaRouteListener;
.super Landroid/support/v7/media/g;
.source "MediaRouteListener.java"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDebug:Z

.field private mListeners:Ljava/util/Set;

.field private mRouter:Landroid/support/v7/media/MediaRouter;

.field private mRoutesAvailable:Z

.field private mSelector:Landroid/support/v7/media/e;


# direct methods
.method constructor <init>(Landroid/support/v7/media/e;Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 37
    invoke-direct {p0}, Landroid/support/v7/media/g;-><init>()V

    .line 31
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteListener;->mListeners:Ljava/util/Set;

    .line 34
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteListener;->mRoutesAvailable:Z

    .line 38
    iput-object p1, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteListener;->mSelector:Landroid/support/v7/media/e;

    .line 39
    iput-object p2, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteListener;->mContext:Landroid/content/Context;

    .line 43
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteListener;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/support/v7/media/MediaRouter;->a(Landroid/content/Context;)Landroid/support/v7/media/MediaRouter;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteListener;->mRouter:Landroid/support/v7/media/MediaRouter;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_0

    .line 49
    :goto_0
    invoke-static {}, Lorg/chromium/base/CommandLine;->getInstance()Lorg/chromium/base/CommandLine;

    move-result-object v0

    const-string/jumbo v1, "enable-cast-debug"

    invoke-virtual {v0, v1}, Lorg/chromium/base/CommandLine;->hasSwitch(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteListener;->mDebug:Z

    .line 50
    return-void

    .line 45
    :catch_0
    move-exception v0

    const-string/jumbo v0, "MediaRouteListener"

    const-string/jumbo v1, "Can\'t get an instance of MediaRouter, casting is not supported. Are you still on JB (JVP15S)?"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 47
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteListener;->mRouter:Landroid/support/v7/media/MediaRouter;

    goto :goto_0
.end method

.method private updateRouteAvailablilty()V
    .locals 6

    .prologue
    .line 136
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteListener;->mRouter:Landroid/support/v7/media/MediaRouter;

    if-nez v0, :cond_1

    .line 151
    :cond_0
    return-void

    .line 138
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteListener;->mRouter:Landroid/support/v7/media/MediaRouter;

    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteListener;->mSelector:Landroid/support/v7/media/e;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Landroid/support/v7/media/MediaRouter;->a(Landroid/support/v7/media/e;I)Z

    move-result v2

    .line 140
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteListener;->mRoutesAvailable:Z

    if-eq v2, v0, :cond_0

    .line 141
    iput-boolean v2, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteListener;->mRoutesAvailable:Z

    .line 142
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteListener;->mListeners:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .line 143
    iget-boolean v1, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteListener;->mDebug:Z

    if-eqz v1, :cond_2

    .line 144
    const-string/jumbo v1, "MediaRouteListener"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "Sending availibitity = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v5, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteListener;->mRoutesAvailable:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " to tab "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " player "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 147
    :cond_2
    const-wide/16 v4, 0x0

    invoke-static {v4, v5}, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->instance(J)Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;

    move-result-object v4

    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v4, v1, v0, v2}, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->onRouteAvailabilityChanged(IIZ)V

    goto :goto_0
.end method


# virtual methods
.method public addListeningNativePlayer(II)V
    .locals 4

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteListener;->mRouter:Landroid/support/v7/media/MediaRouter;

    if-nez v0, :cond_0

    .line 77
    :goto_0
    return-void

    .line 55
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteListener;->mDebug:Z

    if-eqz v0, :cond_1

    .line 56
    const-string/jumbo v0, "MediaRouteListener"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Adding listener player tab = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", player = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 57
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteListener;->mListeners:Ljava/util/Set;

    new-instance v1, Landroid/util/Pair;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 58
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteListener;->mListeners:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 64
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteListener;->mRouter:Landroid/support/v7/media/MediaRouter;

    iget-object v1, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteListener;->mSelector:Landroid/support/v7/media/e;

    const/4 v2, 0x4

    invoke-virtual {v0, v1, p0, v2}, Landroid/support/v7/media/MediaRouter;->a(Landroid/support/v7/media/e;Landroid/support/v7/media/g;I)V
    :try_end_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_0

    .line 71
    :cond_2
    :goto_1
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteListener;->mDebug:Z

    if-eqz v0, :cond_3

    .line 72
    const-string/jumbo v0, "MediaRouteListener"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Sending availibitity = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteListener;->mRoutesAvailable:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " to tab "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " player "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 75
    :cond_3
    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->instance(J)Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteListener;->mRoutesAvailable:Z

    invoke-virtual {v0, p1, p2, v1}, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->onRouteAvailabilityChanged(IIZ)V

    goto/16 :goto_0

    .line 66
    :catch_0
    move-exception v0

    .line 67
    const-string/jumbo v1, "MediaRouteListener"

    invoke-virtual {v0}, Ljava/lang/NoSuchMethodError;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public onProviderAdded(Landroid/support/v7/media/MediaRouter;Landroid/support/v7/media/m;)V
    .locals 0

    .prologue
    .line 122
    invoke-direct {p0}, Lcom/google/android/apps/chrome/videofling/MediaRouteListener;->updateRouteAvailablilty()V

    .line 123
    return-void
.end method

.method public onProviderChanged(Landroid/support/v7/media/MediaRouter;Landroid/support/v7/media/m;)V
    .locals 0

    .prologue
    .line 132
    invoke-direct {p0}, Lcom/google/android/apps/chrome/videofling/MediaRouteListener;->updateRouteAvailablilty()V

    .line 133
    return-void
.end method

.method public onProviderRemoved(Landroid/support/v7/media/MediaRouter;Landroid/support/v7/media/m;)V
    .locals 0

    .prologue
    .line 127
    invoke-direct {p0}, Lcom/google/android/apps/chrome/videofling/MediaRouteListener;->updateRouteAvailablilty()V

    .line 128
    return-void
.end method

.method public onRouteAdded(Landroid/support/v7/media/MediaRouter;Landroid/support/v7/media/MediaRouter$RouteInfo;)V
    .locals 3

    .prologue
    .line 92
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteListener;->mDebug:Z

    if-eqz v0, :cond_0

    .line 93
    const-string/jumbo v0, "MediaRouteListener"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Added route "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/support/v7/media/MediaRouter$RouteInfo;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Landroid/support/v7/media/MediaRouter$RouteInfo;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 94
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/videofling/MediaRouteListener;->updateRouteAvailablilty()V

    .line 95
    return-void
.end method

.method public onRouteChanged(Landroid/support/v7/media/MediaRouter;Landroid/support/v7/media/MediaRouter$RouteInfo;)V
    .locals 0

    .prologue
    .line 107
    invoke-direct {p0}, Lcom/google/android/apps/chrome/videofling/MediaRouteListener;->updateRouteAvailablilty()V

    .line 108
    return-void
.end method

.method public onRouteRemoved(Landroid/support/v7/media/MediaRouter;Landroid/support/v7/media/MediaRouter$RouteInfo;)V
    .locals 3

    .prologue
    .line 99
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteListener;->mDebug:Z

    if-eqz v0, :cond_0

    .line 100
    const-string/jumbo v0, "MediaRouteListener"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Removed route "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/support/v7/media/MediaRouter$RouteInfo;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Landroid/support/v7/media/MediaRouter$RouteInfo;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/videofling/MediaRouteListener;->updateRouteAvailablilty()V

    .line 103
    return-void
.end method

.method public onRouteSelected(Landroid/support/v7/media/MediaRouter;Landroid/support/v7/media/MediaRouter$RouteInfo;)V
    .locals 0

    .prologue
    .line 112
    invoke-direct {p0}, Lcom/google/android/apps/chrome/videofling/MediaRouteListener;->updateRouteAvailablilty()V

    .line 113
    return-void
.end method

.method public onRouteUnselected(Landroid/support/v7/media/MediaRouter;Landroid/support/v7/media/MediaRouter$RouteInfo;)V
    .locals 0

    .prologue
    .line 117
    invoke-direct {p0}, Lcom/google/android/apps/chrome/videofling/MediaRouteListener;->updateRouteAvailablilty()V

    .line 118
    return-void
.end method

.method public removeListeningNativePlayer(II)V
    .locals 4

    .prologue
    .line 80
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteListener;->mRouter:Landroid/support/v7/media/MediaRouter;

    if-nez v0, :cond_1

    .line 88
    :cond_0
    :goto_0
    return-void

    .line 82
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteListener;->mDebug:Z

    if-eqz v0, :cond_2

    const-string/jumbo v0, "MediaRouteListener"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Removing listener tab "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " player "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 83
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteListener;->mListeners:Ljava/util/Set;

    new-instance v1, Landroid/util/Pair;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 84
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteListener;->mListeners:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 86
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteListener;->mRouter:Landroid/support/v7/media/MediaRouter;

    invoke-virtual {v0, p0}, Landroid/support/v7/media/MediaRouter;->a(Landroid/support/v7/media/g;)V

    goto :goto_0
.end method
