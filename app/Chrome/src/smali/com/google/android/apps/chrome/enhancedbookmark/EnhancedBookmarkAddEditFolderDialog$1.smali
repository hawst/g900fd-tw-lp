.class Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkAddEditFolderDialog$1;
.super Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkModelObserver;
.source "EnhancedBookmarkAddEditFolderDialog.java"


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkAddEditFolderDialog;


# virtual methods
.method public bookmarkModelChanged()V
    .locals 0

    .prologue
    .line 99
    return-void
.end method

.method public bookmarkNodeRemoved(Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;ILorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;Z)V
    .locals 3

    .prologue
    .line 104
    invoke-virtual {p3}, Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;->getId()Lorg/chromium/components/bookmarks/BookmarkId;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkAddEditFolderDialog$1;->this$0:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkAddEditFolderDialog;

    # getter for: Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkAddEditFolderDialog;->mFolderId:Lorg/chromium/components/bookmarks/BookmarkId;
    invoke-static {v1}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkAddEditFolderDialog;->access$000(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkAddEditFolderDialog;)Lorg/chromium/components/bookmarks/BookmarkId;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/chromium/components/bookmarks/BookmarkId;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 109
    :goto_0
    return-void

    .line 105
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkAddEditFolderDialog$1;->this$0:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkAddEditFolderDialog;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkAddEditFolderDialog;->dismiss()V

    .line 106
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkAddEditFolderDialog$1;->this$0:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkAddEditFolderDialog;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkAddEditFolderDialog;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkAddEditFolderDialog$1;->this$0:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkAddEditFolderDialog;

    sget v2, Lcom/google/android/apps/chrome/R$string;->enhanced_bookmark_dismiss_on_delete:I

    invoke-virtual {v1, v2}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkAddEditFolderDialog;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method
