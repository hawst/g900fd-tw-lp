.class public Lcom/google/android/apps/chrome/firstrun/FirstRunPage;
.super Landroid/app/Fragment;
.source "FirstRunPage.java"


# instance fields
.field private mProperties:Landroid/os/Bundle;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 15
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunPage;->mProperties:Landroid/os/Bundle;

    return-void
.end method

.method public static addProperties(Landroid/os/Bundle;II)Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 31
    const-string/jumbo v0, "ThisPageNumber"

    invoke-virtual {p0, v0, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 32
    return-object p0
.end method


# virtual methods
.method protected advanceToNextPage()V
    .locals 1

    .prologue
    .line 94
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunPage;->getPageDelegate()Lcom/google/android/apps/chrome/firstrun/FirstRunPageDelegate;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/firstrun/FirstRunPageDelegate;->advanceToNextPage()V

    .line 95
    return-void
.end method

.method protected getPageDelegate()Lcom/google/android/apps/chrome/firstrun/FirstRunPageDelegate;
    .locals 1

    .prologue
    .line 80
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunPage;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/firstrun/FirstRunPageDelegate;

    return-object v0
.end method

.method protected getProperties()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunPage;->mProperties:Landroid/os/Bundle;

    return-object v0
.end method

.method protected interceptBackPressed()Z
    .locals 1

    .prologue
    .line 66
    const/4 v0, 0x0

    return v0
.end method

.method protected isLastPage()Z
    .locals 3

    .prologue
    .line 87
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunPage;->getProperties()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "ThisPageNumber"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunPage;->getProperties()Landroid/os/Bundle;

    move-result-object v1

    const-string/jumbo v2, "LastPageNumber"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 46
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 47
    if-eqz p1, :cond_0

    .line 48
    iput-object p1, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunPage;->mProperties:Landroid/os/Bundle;

    .line 54
    :goto_0
    return-void

    .line 49
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunPage;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 50
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunPage;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunPage;->mProperties:Landroid/os/Bundle;

    goto :goto_0

    .line 52
    :cond_1
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunPage;->mProperties:Landroid/os/Bundle;

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 58
    invoke-super {p0, p1}, Landroid/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 59
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunPage;->mProperties:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 60
    return-void
.end method

.method public shouldSkipPageOnResume(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 40
    const/4 v0, 0x0

    return v0
.end method
