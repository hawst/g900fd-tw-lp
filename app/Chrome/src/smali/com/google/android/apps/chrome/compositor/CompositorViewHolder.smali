.class public Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;
.super Landroid/widget/FrameLayout;
.source "CompositorViewHolder.java"

# interfaces
.implements Lcom/google/android/apps/chrome/KeyboardHider;
.implements Lcom/google/android/apps/chrome/compositor/Invalidator$Host;
.implements Lcom/google/android/apps/chrome/eventfilter/EventFilterHost;
.implements Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;
.implements Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;
.implements Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager$FullscreenListener;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final NOTIFICATIONS:[I


# instance fields
.field private mBackgroundContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

.field private mBackgroundViewListener:Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper$BackgroundContentViewListener;

.field private final mCacheViewport:Landroid/graphics/Rect;

.field private final mCacheVisibleViewport:Landroid/graphics/Rect;

.field private mCompositorView:Lcom/google/android/apps/chrome/compositor/CompositorView;

.field private mContentOverlayVisiblity:Z

.field private mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

.field private mContextualSearchContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

.field private mControlContainer:Lcom/google/android/apps/chrome/widget/ControlContainer;

.field private mEnableCompositorTabStrip:Z

.field private mEventFilter:Lcom/google/android/apps/chrome/eventfilter/EventFilter;

.field private mFindToolbarShowing:Z

.field private mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;

.field private mFullscreenTouchEvent:Z

.field private mHasDrawnOnce:Z

.field private final mInvalidator:Lcom/google/android/apps/chrome/compositor/Invalidator;

.field private mIsKeyboardShowing:Z

.field private mLastContentOffset:F

.field private mLastTapX:I

.field private mLastTapY:I

.field private mLastVisibleContentOffset:F

.field private mLayerTitleCache:Lcom/google/android/apps/chrome/compositor/LayerTitleCache;

.field private mLayoutManager:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;

.field private mNextEventFilter:Lcom/google/android/apps/chrome/eventfilter/EventFilter;

.field private final mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

.field private final mPendingInvalidations:Ljava/util/ArrayList;

.field private mPendingSwapBuffersCount:I

.field private mPostHideKeyboardTask:Ljava/lang/Runnable;

.field private mSkipInvalidation:Z

.field private mSkipNextToolbarTextureUpdate:Z

.field private mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

.field private mTabObserver:Lorg/chromium/chrome/browser/TabObserver;

.field private mTabVisible:Lcom/google/android/apps/chrome/tab/ChromeTab;

.field private mTakeSurface:Z

.field private mTransitionHelperListener:Lcom/google/android/apps/chrome/tab/TransitionPageHelper$TransitionPageHelperListener;

.field private mTransitionPageViewCore:Lorg/chromium/content/browser/ContentViewCore;

.field private mUrlBar:Landroid/view/View;

.field private mView:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 73
    const-class v0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->$assertionsDisabled:Z

    .line 77
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->NOTIFICATIONS:[I

    return-void

    .line 73
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 77
    :array_0
    .array-data 4
        0x2d
        0x3b
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 211
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 82
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mIsKeyboardShowing:Z

    .line 84
    new-instance v0, Lcom/google/android/apps/chrome/compositor/Invalidator;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/compositor/Invalidator;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mInvalidator:Lcom/google/android/apps/chrome/compositor/Invalidator;

    .line 88
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mContentOverlayVisiblity:Z

    .line 92
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mPendingInvalidations:Ljava/util/ArrayList;

    .line 94
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mSkipInvalidation:Z

    .line 99
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mSkipNextToolbarTextureUpdate:Z

    .line 105
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mFindToolbarShowing:Z

    .line 109
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mFullscreenTouchEvent:Z

    .line 110
    iput v2, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mLastContentOffset:F

    .line 111
    iput v2, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mLastVisibleContentOffset:F

    .line 149
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mCacheViewport:Landroid/graphics/Rect;

    .line 150
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mCacheVisibleViewport:Landroid/graphics/Rect;

    .line 159
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mHasDrawnOnce:Z

    .line 1058
    new-instance v0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$8;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$8;-><init>(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    .line 213
    invoke-direct {p0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->internalInit()V

    .line 214
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 222
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 82
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mIsKeyboardShowing:Z

    .line 84
    new-instance v0, Lcom/google/android/apps/chrome/compositor/Invalidator;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/compositor/Invalidator;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mInvalidator:Lcom/google/android/apps/chrome/compositor/Invalidator;

    .line 88
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mContentOverlayVisiblity:Z

    .line 92
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mPendingInvalidations:Ljava/util/ArrayList;

    .line 94
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mSkipInvalidation:Z

    .line 99
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mSkipNextToolbarTextureUpdate:Z

    .line 105
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mFindToolbarShowing:Z

    .line 109
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mFullscreenTouchEvent:Z

    .line 110
    iput v2, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mLastContentOffset:F

    .line 111
    iput v2, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mLastVisibleContentOffset:F

    .line 149
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mCacheViewport:Landroid/graphics/Rect;

    .line 150
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mCacheVisibleViewport:Landroid/graphics/Rect;

    .line 159
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mHasDrawnOnce:Z

    .line 1058
    new-instance v0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$8;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$8;-><init>(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    .line 224
    invoke-direct {p0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->internalInit()V

    .line 225
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;)V
    .locals 0

    .prologue
    .line 73
    invoke-direct {p0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->modelChanged()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;)Lcom/google/android/apps/chrome/tab/ChromeTab;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mTabVisible:Lcom/google/android/apps/chrome/tab/ChromeTab;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;Lorg/chromium/content/browser/ContentViewCore;)V
    .locals 0

    .prologue
    .line 73
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->setContentViewCoreSize(Lorg/chromium/content/browser/ContentViewCore;)V

    return-void
.end method

.method static synthetic access$1102(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;Lorg/chromium/content/browser/ContentViewCore;)Lorg/chromium/content/browser/ContentViewCore;
    .locals 0

    .prologue
    .line 73
    iput-object p1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mContextualSearchContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    return-object p1
.end method

.method static synthetic access$1200(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;)Z
    .locals 1

    .prologue
    .line 73
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mFindToolbarShowing:Z

    return v0
.end method

.method static synthetic access$1202(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;Z)Z
    .locals 0

    .prologue
    .line 73
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mFindToolbarShowing:Z

    return p1
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;)Lorg/chromium/content/browser/ContentViewCore;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mBackgroundContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    return-object v0
.end method

.method static synthetic access$202(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;Lorg/chromium/content/browser/ContentViewCore;)Lorg/chromium/content/browser/ContentViewCore;
    .locals 0

    .prologue
    .line 73
    iput-object p1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mBackgroundContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    return-object p1
.end method

.method static synthetic access$300(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;Lorg/chromium/content/browser/ContentViewCore;Z)V
    .locals 0

    .prologue
    .line 73
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->setupContentView(Lorg/chromium/content/browser/ContentViewCore;Z)V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;)Lcom/google/android/apps/chrome/compositor/CompositorView;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mCompositorView:Lcom/google/android/apps/chrome/compositor/CompositorView;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;)Lorg/chromium/content/browser/ContentViewCore;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mTransitionPageViewCore:Lorg/chromium/content/browser/ContentViewCore;

    return-object v0
.end method

.method static synthetic access$502(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;Lorg/chromium/content/browser/ContentViewCore;)Lorg/chromium/content/browser/ContentViewCore;
    .locals 0

    .prologue
    .line 73
    iput-object p1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mTransitionPageViewCore:Lorg/chromium/content/browser/ContentViewCore;

    return-object p1
.end method

.method static synthetic access$600(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;)V
    .locals 0

    .prologue
    .line 73
    invoke-direct {p0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->setupTransitionPageView()V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;II)V
    .locals 0

    .prologue
    .line 73
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->propagateViewportToLayouts(II)V

    return-void
.end method

.method static synthetic access$800(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mPostHideKeyboardTask:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$802(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;Ljava/lang/Runnable;)Ljava/lang/Runnable;
    .locals 0

    .prologue
    .line 73
    iput-object p1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mPostHideKeyboardTask:Ljava/lang/Runnable;

    return-object p1
.end method

.method static synthetic access$900(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;)Lcom/google/android/apps/chrome/widget/ControlContainer;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mControlContainer:Lcom/google/android/apps/chrome/widget/ControlContainer;

    return-object v0
.end method

.method private flushInvalidation()V
    .locals 2

    .prologue
    .line 1102
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mPendingInvalidations:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1108
    :goto_0
    return-void

    .line 1103
    :cond_0
    const-string/jumbo v0, "CompositorViewHolder.flushInvalidation"

    invoke-static {v0}, Lorg/chromium/base/TraceEvent;->instant(Ljava/lang/String;)V

    .line 1104
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mPendingInvalidations:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1105
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mPendingInvalidations:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/compositor/Invalidator$Client;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/compositor/Invalidator$Client;->doInvalidate()V

    .line 1104
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1107
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mPendingInvalidations:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    goto :goto_0
.end method

.method private getViewToPropagateEventsTo()Landroid/view/View;
    .locals 2

    .prologue
    .line 904
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mContextualSearchContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mLayoutManager:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mLayoutManager:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->isContextualSearchLayoutShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 906
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mContextualSearchContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->getContainerView()Landroid/view/ViewGroup;

    move-result-object v0

    .line 918
    :cond_0
    :goto_0
    return-object v0

    .line 910
    :cond_1
    const/4 v0, 0x2

    .line 911
    iget-boolean v1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mTakeSurface:Z

    if-eqz v1, :cond_2

    .line 912
    const/4 v0, 0x1

    .line 914
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->getChildCount()I

    move-result v1

    if-ge v1, v0, :cond_3

    const/4 v0, 0x0

    goto :goto_0

    .line 916
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 917
    sget-boolean v1, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mContentOverlayVisiblity:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mView:Landroid/view/View;

    if-eq v1, v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method private internalInit()V
    .locals 4

    .prologue
    .line 228
    new-instance v0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$1;-><init>(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mTabObserver:Lorg/chromium/chrome/browser/TabObserver;

    .line 235
    new-instance v0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$2;-><init>(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mBackgroundViewListener:Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper$BackgroundContentViewListener;

    .line 258
    new-instance v0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$3;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$3;-><init>(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mTransitionHelperListener:Lcom/google/android/apps/chrome/tab/TransitionPageHelper$TransitionPageHelperListener;

    .line 285
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/ui/base/DeviceFormFactor;->isTablet(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lorg/chromium/base/CommandLine;->getInstance()Lorg/chromium/base/CommandLine;

    move-result-object v0

    const-string/jumbo v1, "disable-compositor-tab-strip"

    invoke-virtual {v0, v1}, Lorg/chromium/base/CommandLine;->hasSwitch(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mEnableCompositorTabStrip:Z

    .line 289
    new-instance v0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$4;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$4;-><init>(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 308
    invoke-static {}, Lorg/chromium/base/CommandLine;->getInstance()Lorg/chromium/base/CommandLine;

    move-result-object v0

    const-string/jumbo v1, "take-surface"

    invoke-virtual {v0, v1}, Lorg/chromium/base/CommandLine;->hasSwitch(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mTakeSurface:Z

    .line 309
    new-instance v0, Lcom/google/android/apps/chrome/compositor/CompositorView;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lcom/google/android/apps/chrome/compositor/CompositorView;-><init>(Landroid/content/Context;Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mCompositorView:Lcom/google/android/apps/chrome/compositor/CompositorView;

    .line 310
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mTakeSurface:Z

    if-nez v0, :cond_0

    .line 311
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mCompositorView:Lcom/google/android/apps/chrome/compositor/CompositorView;

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-direct {v1, v2, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 314
    :cond_0
    return-void

    .line 285
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private modelChanged()V
    .locals 1

    .prologue
    .line 982
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    if-nez v0, :cond_0

    .line 988
    :goto_0
    return-void

    .line 986
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->getCurrentTab()Lorg/chromium/chrome/browser/Tab;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->fromTab(Lorg/chromium/chrome/browser/Tab;)Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    .line 987
    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->setTab(Lcom/google/android/apps/chrome/tab/ChromeTab;)V

    goto :goto_0
.end method

.method private propagateViewportToLayouts(II)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 604
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->getFullscreenManager()Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->getFullscreenManager()Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->getTopControlsHeight()I

    move-result v0

    :goto_0
    sub-int v0, p2, v0

    .line 607
    iget-object v2, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mCacheViewport:Landroid/graphics/Rect;

    iget v3, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mLastContentOffset:F

    float-to-int v3, v3

    invoke-virtual {v2, v1, v3, p1, p2}, Landroid/graphics/Rect;->set(IIII)V

    .line 608
    iget-object v2, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mCacheVisibleViewport:Landroid/graphics/Rect;

    iget v3, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mLastVisibleContentOffset:F

    float-to-int v3, v3

    invoke-virtual {v2, v1, v3, p1, p2}, Landroid/graphics/Rect;->set(IIII)V

    .line 611
    iget-object v1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    if-eqz v1, :cond_0

    .line 612
    iget-object v1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    iget-object v2, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mCacheViewport:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    neg-int v2, v2

    iget-object v3, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mCacheViewport:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    neg-int v3, v3

    invoke-virtual {v1, v2, v3}, Lorg/chromium/content/browser/ContentViewCore;->setSmartClipOffsets(II)V

    .line 614
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mLayoutManager:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;

    if-eqz v1, :cond_1

    .line 615
    iget-object v1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mLayoutManager:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;

    iget-object v2, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mCacheViewport:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mCacheVisibleViewport:Landroid/graphics/Rect;

    invoke-virtual {v1, v2, v3, v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;->pushNewViewport(Landroid/graphics/Rect;Landroid/graphics/Rect;I)V

    .line 618
    :cond_1
    return-void

    :cond_2
    move v0, v1

    .line 604
    goto :goto_0
.end method

.method private setContentViewCoreSize(Lorg/chromium/content/browser/ContentViewCore;)V
    .locals 4

    .prologue
    .line 1047
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mCompositorView:Lcom/google/android/apps/chrome/compositor/CompositorView;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/compositor/CompositorView;->getWidth()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mCompositorView:Lcom/google/android/apps/chrome/compositor/CompositorView;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/compositor/CompositorView;->getHeight()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lorg/chromium/content/browser/ContentViewCore;->onPhysicalBackingSizeChanged(II)V

    .line 1051
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mView:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mView:Landroid/view/View;

    .line 1052
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->getHeight()I

    move-result v2

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v3

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    invoke-virtual {p1, v1, v2, v3, v0}, Lorg/chromium/content/browser/ContentViewCore;->onSizeChanged(IIII)V

    .line 1054
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mCompositorView:Lcom/google/android/apps/chrome/compositor/CompositorView;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/compositor/CompositorView;->getOverdrawBottomHeight()I

    move-result v0

    invoke-virtual {p1, v0}, Lorg/chromium/content/browser/ContentViewCore;->onOverdrawBottomHeightChanged(I)V

    .line 1056
    return-void

    .line 1051
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mCompositorView:Lcom/google/android/apps/chrome/compositor/CompositorView;

    goto :goto_0
.end method

.method private setContentViewMotionEventOffsets(Landroid/view/MotionEvent;Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 576
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    if-nez v0, :cond_1

    .line 601
    :cond_0
    :goto_0
    return-void

    .line 578
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    .line 580
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lorg/chromium/content/browser/SPenSupport;->isSPenSupported(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 581
    invoke-static {v0}, Lorg/chromium/content/browser/SPenSupport;->convertSPenEventAction(I)I

    move-result v0

    .line 584
    :cond_2
    if-eqz v0, :cond_3

    const/16 v1, 0x9

    if-ne v0, v1, :cond_5

    .line 586
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mLayoutManager:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mLayoutManager:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;

    iget-object v1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mCacheViewport:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;->getViewportPixel(Landroid/graphics/Rect;)Landroid/graphics/Rect;

    .line 587
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    iget-object v1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mCacheViewport:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    neg-int v1, v1

    int-to-float v1, v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mCacheViewport:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    neg-int v2, v2

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Lorg/chromium/content/browser/ContentViewCore;->setCurrentMotionEventOffsets(FF)V

    .line 589
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mEventFilter:Lcom/google/android/apps/chrome/eventfilter/EventFilter;

    if-eqz v0, :cond_0

    .line 590
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mEventFilter:Lcom/google/android/apps/chrome/eventfilter/EventFilter;

    iget-object v1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mCacheViewport:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    neg-int v1, v1

    int-to-float v1, v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mCacheViewport:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    neg-int v2, v2

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/eventfilter/EventFilter;->setCurrentMotionEventOffsets(FF)V

    goto :goto_0

    .line 593
    :cond_5
    if-eqz p2, :cond_0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_6

    const/4 v1, 0x3

    if-eq v0, v1, :cond_6

    const/16 v1, 0xa

    if-ne v0, v1, :cond_0

    .line 596
    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    invoke-virtual {v0, v2, v2}, Lorg/chromium/content/browser/ContentViewCore;->setCurrentMotionEventOffsets(FF)V

    .line 597
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mEventFilter:Lcom/google/android/apps/chrome/eventfilter/EventFilter;

    if-eqz v0, :cond_0

    .line 598
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mEventFilter:Lcom/google/android/apps/chrome/eventfilter/EventFilter;

    invoke-virtual {v0, v2, v2}, Lcom/google/android/apps/chrome/eventfilter/EventFilter;->setCurrentMotionEventOffsets(FF)V

    goto :goto_0
.end method

.method private setTab(Lcom/google/android/apps/chrome/tab/ChromeTab;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x0

    .line 991
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->isFrozen()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->unfreezeContents()Z

    .line 993
    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getView()Landroid/view/View;

    move-result-object v0

    .line 994
    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mView:Landroid/view/View;

    if-ne v2, v0, :cond_2

    .line 1025
    :goto_1
    return-void

    :cond_1
    move-object v0, v1

    .line 993
    goto :goto_0

    .line 996
    :cond_2
    invoke-direct {p0, v4}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->updateContentOverlayVisibility(Z)V

    .line 997
    iget-object v2, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mTabVisible:Lcom/google/android/apps/chrome/tab/ChromeTab;

    if-eq v2, p1, :cond_4

    .line 998
    iget-object v2, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mTabVisible:Lcom/google/android/apps/chrome/tab/ChromeTab;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mTabVisible:Lcom/google/android/apps/chrome/tab/ChromeTab;

    iget-object v3, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mTabObserver:Lorg/chromium/chrome/browser/TabObserver;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/chrome/tab/ChromeTab;->removeObserver(Lorg/chromium/chrome/browser/TabObserver;)V

    .line 999
    :cond_3
    if-eqz p1, :cond_4

    iget-object v2, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mTabObserver:Lorg/chromium/chrome/browser/TabObserver;

    invoke-virtual {p1, v2}, Lcom/google/android/apps/chrome/tab/ChromeTab;->addObserver(Lorg/chromium/chrome/browser/TabObserver;)V

    .line 1001
    :cond_4
    iput-object p1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mTabVisible:Lcom/google/android/apps/chrome/tab/ChromeTab;

    .line 1002
    iput-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mView:Landroid/view/View;

    .line 1003
    if-eqz p1, :cond_8

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    :goto_2
    iput-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    .line 1004
    if-eqz p1, :cond_9

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getBackgroundContentViewHelper()Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;

    move-result-object v0

    .line 1006
    :goto_3
    if-eqz v0, :cond_5

    .line 1007
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mBackgroundContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    .line 1008
    iget-object v2, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mBackgroundViewListener:Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper$BackgroundContentViewListener;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->setBackgroundViewListener(Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper$BackgroundContentViewListener;)V

    .line 1011
    :cond_5
    if-eqz p1, :cond_6

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getTransitionPageHelper()Lcom/google/android/apps/chrome/tab/TransitionPageHelper;

    move-result-object v1

    .line 1013
    :cond_6
    if-eqz v1, :cond_7

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mCompositorView:Lcom/google/android/apps/chrome/compositor/CompositorView;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/compositor/CompositorView;->getTransitionPageContainer()Lcom/google/android/apps/chrome/compositor/TransitionPageContainer;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 1014
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mTransitionHelperListener:Lcom/google/android/apps/chrome/tab/TransitionPageHelper$TransitionPageHelperListener;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->setListener(Lcom/google/android/apps/chrome/tab/TransitionPageHelper$TransitionPageHelperListener;)V

    .line 1015
    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->getTransitionContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    .line 1016
    iget-object v2, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mCompositorView:Lcom/google/android/apps/chrome/compositor/CompositorView;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/compositor/CompositorView;->getTransitionPageContainer()Lcom/google/android/apps/chrome/compositor/TransitionPageContainer;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/android/apps/chrome/compositor/TransitionPageContainer;->setContentViewCore(Lorg/chromium/content/browser/ContentViewCore;)V

    .line 1017
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mCompositorView:Lcom/google/android/apps/chrome/compositor/CompositorView;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/compositor/CompositorView;->getTransitionPageContainer()Lcom/google/android/apps/chrome/compositor/TransitionPageContainer;

    move-result-object v0

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tab/TransitionPageHelper;->isTransitionVisible()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/compositor/TransitionPageContainer;->setVisible(Z)V

    .line 1018
    invoke-direct {p0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->setupTransitionPageView()V

    .line 1021
    :cond_7
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mContentOverlayVisiblity:Z

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->updateContentOverlayVisibility(Z)V

    .line 1023
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    invoke-direct {p0, v0, v4}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->setupContentView(Lorg/chromium/content/browser/ContentViewCore;Z)V

    .line 1024
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mBackgroundContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->setupContentView(Lorg/chromium/content/browser/ContentViewCore;Z)V

    goto :goto_1

    :cond_8
    move-object v0, v1

    .line 1003
    goto :goto_2

    :cond_9
    move-object v0, v1

    .line 1004
    goto :goto_3
.end method

.method private setupContentView(Lorg/chromium/content/browser/ContentViewCore;Z)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1028
    if-nez p1, :cond_1

    .line 1035
    :cond_0
    :goto_0
    return-void

    .line 1029
    :cond_1
    invoke-virtual {p1, v0, v0}, Lorg/chromium/content/browser/ContentViewCore;->setCurrentMotionEventOffsets(FF)V

    .line 1030
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->setContentViewCoreSize(Lorg/chromium/content/browser/ContentViewCore;)V

    .line 1031
    if-eqz p2, :cond_0

    .line 1032
    sget-boolean v0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mCompositorView:Lcom/google/android/apps/chrome/compositor/CompositorView;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/compositor/CompositorView;->getInstantPageContainer()Lcom/google/android/apps/chrome/compositor/InstantPageContainer;

    move-result-object v0

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1033
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mCompositorView:Lcom/google/android/apps/chrome/compositor/CompositorView;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/compositor/CompositorView;->getInstantPageContainer()Lcom/google/android/apps/chrome/compositor/InstantPageContainer;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/compositor/InstantPageContainer;->addChild(Lorg/chromium/content/browser/ContentViewCore;)V

    goto :goto_0
.end method

.method private setupTransitionPageView()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1038
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mTransitionPageViewCore:Lorg/chromium/content/browser/ContentViewCore;

    if-nez v0, :cond_0

    .line 1044
    :goto_0
    return-void

    .line 1040
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mTransitionPageViewCore:Lorg/chromium/content/browser/ContentViewCore;

    invoke-virtual {v0, v1, v1}, Lorg/chromium/content/browser/ContentViewCore;->setCurrentMotionEventOffsets(FF)V

    .line 1041
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mTransitionPageViewCore:Lorg/chromium/content/browser/ContentViewCore;

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->getFullscreenManager()Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->getTopControlsHeight()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/chromium/content/browser/ContentViewCore;->setViewportSizeOffset(II)V

    .line 1043
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mTransitionPageViewCore:Lorg/chromium/content/browser/ContentViewCore;

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->setContentViewCoreSize(Lorg/chromium/content/browser/ContentViewCore;)V

    goto :goto_0
.end method

.method private updateContentOverlayVisibility(Z)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v2, -0x1

    const/4 v1, 0x4

    const/4 v3, 0x0

    .line 922
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mView:Landroid/view/View;

    if-nez v0, :cond_1

    .line 979
    :cond_0
    :goto_0
    return-void

    .line 923
    :cond_1
    if-eqz p1, :cond_9

    .line 924
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eq v0, p0, :cond_0

    .line 926
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    instance-of v0, v0, Landroid/view/ViewGroup;

    if-eqz v0, :cond_2

    .line 927
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 929
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    if-eqz v0, :cond_4

    .line 930
    sget-boolean v0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->$assertionsDisabled:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->isAlive()Z

    move-result v0

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 931
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;

    iget-object v1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->updateContentViewViewportSize(Lorg/chromium/content/browser/ContentViewCore;)V

    .line 933
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mBackgroundContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    if-eqz v0, :cond_6

    .line 934
    sget-boolean v0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->$assertionsDisabled:Z

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mBackgroundContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->isAlive()Z

    move-result v0

    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 935
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;

    iget-object v1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mBackgroundContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->updateContentViewViewportSize(Lorg/chromium/content/browser/ContentViewCore;)V

    .line 937
    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mContextualSearchContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    if-eqz v0, :cond_7

    .line 938
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;

    iget-object v1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mContextualSearchContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->updateContentViewViewportSize(Lorg/chromium/content/browser/ContentViewCore;)V

    .line 941
    :cond_7
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v0, v2, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 943
    iget-object v1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mView:Landroid/view/View;

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 944
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mView:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 945
    invoke-virtual {p0, v3}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->setFocusable(Z)V

    .line 946
    invoke-virtual {p0, v3}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->setFocusableInTouchMode(Z)V

    .line 949
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mUrlBar:Landroid/view/View;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mUrlBar:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->hasFocus()Z

    move-result v0

    if-nez v0, :cond_0

    .line 950
    :cond_8
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    goto/16 :goto_0

    .line 954
    :cond_9
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-ne v0, p0, :cond_0

    .line 955
    invoke-virtual {p0, v4}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->setFocusable(Z)V

    .line 956
    invoke-virtual {p0, v4}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->setFocusableInTouchMode(Z)V

    .line 957
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 958
    :cond_a
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 960
    :cond_b
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mBackgroundContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mBackgroundContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 961
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mBackgroundContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->getContainerView()Landroid/view/ViewGroup;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 963
    :cond_c
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mContextualSearchContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mContextualSearchContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 965
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mContextualSearchContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->getContainerView()Landroid/view/ViewGroup;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 969
    :cond_d
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 970
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 972
    invoke-virtual {v0, p0}, Landroid/view/inputmethod/InputMethodManager;->isActive(Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 973
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v3, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;ILandroid/os/ResultReceiver;)Z

    .line 976
    :cond_e
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mView:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->removeView(Landroid/view/View;)V

    goto/16 :goto_0
.end method


# virtual methods
.method public areTopControlsPermanentlyHidden()Z
    .locals 1

    .prologue
    .line 788
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->areTopControlsPermanentlyHidden()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public clearChildFocus(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 738
    return-void
.end method

.method public deferInvalidate(Lcom/google/android/apps/chrome/compositor/Invalidator$Client;)V
    .locals 1

    .prologue
    .line 1094
    iget v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mPendingSwapBuffersCount:I

    if-gtz v0, :cond_1

    .line 1095
    invoke-interface {p1}, Lcom/google/android/apps/chrome/compositor/Invalidator$Client;->doInvalidate()V

    .line 1099
    :cond_0
    :goto_0
    return-void

    .line 1096
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mPendingInvalidations:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1097
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mPendingInvalidations:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public getContentOffsetProvider()Lcom/google/android/apps/chrome/compositor/ContentOffsetProvider;
    .locals 1

    .prologue
    .line 398
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mCompositorView:Lcom/google/android/apps/chrome/compositor/CompositorView;

    return-object v0
.end method

.method public getContentReadbackHandler()Lorg/chromium/content/browser/ContentReadbackHandler;
    .locals 1

    .prologue
    .line 405
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mCompositorView:Lcom/google/android/apps/chrome/compositor/CompositorView;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 406
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mCompositorView:Lcom/google/android/apps/chrome/compositor/CompositorView;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/compositor/CompositorView;->getContentReadbackHandler()Lorg/chromium/content/browser/ContentReadbackHandler;

    move-result-object v0

    goto :goto_0
.end method

.method public getCurrentOverdrawBottomHeight()I
    .locals 2

    .prologue
    .line 527
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mTabVisible:Lcom/google/android/apps/chrome/tab/ChromeTab;

    if-eqz v0, :cond_0

    .line 528
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mTabVisible:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getFullscreenOverdrawBottomHeightPix()F

    move-result v0

    .line 529
    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v1

    if-nez v1, :cond_0

    .line 530
    float-to-int v0, v0

    .line 533
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mCompositorView:Lcom/google/android/apps/chrome/compositor/CompositorView;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/compositor/CompositorView;->getOverdrawBottomHeight()I

    move-result v0

    goto :goto_0
.end method

.method public getDynamicResourceLoader()Lcom/google/android/apps/chrome/compositor/resources/dynamics/DynamicResourceLoader;
    .locals 1

    .prologue
    .line 413
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mCompositorView:Lcom/google/android/apps/chrome/compositor/CompositorView;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/compositor/CompositorView;->getResourceManager()Lcom/google/android/apps/chrome/compositor/resources/ResourceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/compositor/resources/ResourceManager;->getDynamicResourceLoader()Lcom/google/android/apps/chrome/compositor/resources/dynamics/DynamicResourceLoader;

    move-result-object v0

    return-object v0
.end method

.method public getFullscreenManager()Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;
    .locals 1

    .prologue
    .line 761
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;

    return-object v0
.end method

.method public getInvalidator()Lcom/google/android/apps/chrome/compositor/Invalidator;
    .locals 1

    .prologue
    .line 420
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mInvalidator:Lcom/google/android/apps/chrome/compositor/Invalidator;

    return-object v0
.end method

.method public getLastTapX()I
    .locals 1

    .prologue
    .line 680
    iget v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mLastTapX:I

    return v0
.end method

.method public getLastTapY()I
    .locals 1

    .prologue
    .line 685
    iget v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mLastTapY:I

    return v0
.end method

.method public getLayoutManager()Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;
    .locals 1

    .prologue
    .line 464
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mLayoutManager:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;

    return-object v0
.end method

.method public getLayoutRenderHost()Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;
    .locals 0

    .prologue
    .line 690
    return-object p0
.end method

.method public getLayoutTabsDrawnCount()I
    .locals 1

    .prologue
    .line 695
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mCompositorView:Lcom/google/android/apps/chrome/compositor/CompositorView;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/compositor/CompositorView;->getLastLayerCount()I

    move-result v0

    return v0
.end method

.method public getResourceManager()Lcom/google/android/apps/chrome/compositor/resources/ResourceManager;
    .locals 1

    .prologue
    .line 394
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mCompositorView:Lcom/google/android/apps/chrome/compositor/CompositorView;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/compositor/CompositorView;->getResourceManager()Lcom/google/android/apps/chrome/compositor/resources/ResourceManager;

    move-result-object v0

    return-object v0
.end method

.method public getSurfaceHolderCallback2()Landroid/view/SurfaceHolder$Callback2;
    .locals 1

    .prologue
    .line 351
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mCompositorView:Lcom/google/android/apps/chrome/compositor/CompositorView;

    return-object v0
.end method

.method public getTitleCache()Lcom/google/android/apps/chrome/tabs/TitleCache;
    .locals 1

    .prologue
    .line 1089
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mLayerTitleCache:Lcom/google/android/apps/chrome/compositor/LayerTitleCache;

    return-object v0
.end method

.method public getTopControlsHeightPixels()I
    .locals 1

    .prologue
    .line 793
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->getTopControlsHeight()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getView()Landroid/view/View;
    .locals 0

    .prologue
    .line 469
    return-object p0
.end method

.method public getVisibleViewport(Landroid/graphics/Rect;)Landroid/graphics/Rect;
    .locals 4

    .prologue
    .line 781
    if-nez p1, :cond_0

    new-instance p1, Landroid/graphics/Rect;

    invoke-direct {p1}, Landroid/graphics/Rect;-><init>()V

    .line 782
    :cond_0
    const/4 v0, 0x0

    iget v1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mLastVisibleContentOffset:F

    float-to-int v1, v1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->getWidth()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->getHeight()I

    move-result v3

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 783
    return-object p1
.end method

.method protected handleFindInPage(Z)V
    .locals 0

    .prologue
    .line 1085
    return-void
.end method

.method public hideKeyboard(Ljava/lang/Runnable;)V
    .locals 2

    .prologue
    .line 839
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mUrlBar:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mUrlBar:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->clearFocus()V

    .line 840
    :cond_0
    const/4 v0, 0x0

    .line 841
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->hasFocus()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 842
    invoke-static {p0}, Lorg/chromium/ui/UiUtils;->hideKeyboard(Landroid/view/View;)Z

    move-result v0

    .line 844
    :cond_1
    if-eqz v0, :cond_2

    .line 845
    iput-object p1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mPostHideKeyboardTask:Ljava/lang/Runnable;

    .line 849
    :goto_0
    return-void

    .line 847
    :cond_2
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    goto :goto_0
.end method

.method public isTabInteractive()Z
    .locals 1

    .prologue
    .line 826
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mLayoutManager:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mLayoutManager:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mLayoutManager:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->isTabInteractive()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mContentOverlayVisiblity:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mView:Landroid/view/View;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onAttachedToWindow()V
    .locals 3

    .prologue
    .line 806
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->NOTIFICATIONS:[I

    iget-object v2, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->registerForNotifications(Landroid/content/Context;[ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    .line 808
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mInvalidator:Lcom/google/android/apps/chrome/compositor/Invalidator;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/chrome/compositor/Invalidator;->set(Lcom/google/android/apps/chrome/compositor/Invalidator$Host;)V

    .line 809
    invoke-super {p0}, Landroid/widget/FrameLayout;->onAttachedToWindow()V

    .line 810
    return-void
.end method

.method public onCompositorLayout()V
    .locals 3

    .prologue
    .line 625
    const-string/jumbo v0, "CompositorViewHolder:layout"

    invoke-static {v0}, Lorg/chromium/base/TraceEvent;->begin(Ljava/lang/String;)V

    .line 626
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mLayoutManager:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;

    if-eqz v0, :cond_0

    .line 627
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mLayoutManager:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;->onUpdate()V

    .line 628
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mCompositorView:Lcom/google/android/apps/chrome/compositor/CompositorView;

    iget-object v1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mLayoutManager:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;

    iget-boolean v2, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mSkipNextToolbarTextureUpdate:Z

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/compositor/CompositorView;->finalizeLayers(Lcom/google/android/apps/chrome/tabs/layout/LayoutProvider;Z)V

    .line 631
    :cond_0
    const-string/jumbo v0, "CompositorViewHolder:layout"

    invoke-static {v0}, Lorg/chromium/base/TraceEvent;->end(Ljava/lang/String;)V

    .line 632
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mSkipNextToolbarTextureUpdate:Z

    .line 633
    return-void
.end method

.method public onContentOffsetChanged(F)V
    .locals 2

    .prologue
    .line 557
    iput p1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mLastContentOffset:F

    .line 558
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->getHeight()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->propagateViewportToLayouts(II)V

    .line 559
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 3

    .prologue
    .line 814
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->NOTIFICATIONS:[I

    iget-object v2, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->unregisterForNotifications(Landroid/content/Context;[ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    .line 816
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mLayoutManager:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mLayoutManager:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;->destroy()V

    .line 817
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->flushInvalidation()V

    .line 818
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mInvalidator:Lcom/google/android/apps/chrome/compositor/Invalidator;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/compositor/Invalidator;->set(Lcom/google/android/apps/chrome/compositor/Invalidator$Host;)V

    .line 819
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    .line 820
    return-void
.end method

.method public onEndGesture()V
    .locals 1

    .prologue
    .line 755
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mLayoutManager:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mLayoutManager:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;->onEndGesture()V

    .line 756
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mNextEventFilter:Lcom/google/android/apps/chrome/eventfilter/EventFilter;

    iput-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mEventFilter:Lcom/google/android/apps/chrome/eventfilter/EventFilter;

    .line 757
    return-void
.end method

.method public onFinishNativeInitialization(Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreatorManager;Lcom/google/android/apps/chrome/compositor/TabContentManager;Landroid/view/ViewGroup;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;)V
    .locals 6

    .prologue
    .line 864
    sget-boolean v0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mLayoutManager:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 865
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mLayoutManager:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;->init(Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreatorManager;Lcom/google/android/apps/chrome/compositor/TabContentManager;Landroid/view/ViewGroup;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManagementDelegate;)V

    .line 867
    iput-object p1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    .line 868
    new-instance v0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$6;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$6;-><init>(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;)V

    invoke-interface {p1, v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->registerChangeListener(Lorg/chromium/chrome/browser/tabmodel/TabModelSelector$ChangeListener;)V

    .line 879
    invoke-direct {p0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->modelChanged()V

    .line 881
    if-eqz p5, :cond_1

    .line 882
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mCompositorView:Lcom/google/android/apps/chrome/compositor/CompositorView;

    invoke-virtual {v0, p5}, Lcom/google/android/apps/chrome/compositor/CompositorView;->setContextualSearchManager(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;)V

    .line 884
    new-instance v0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$7;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$7;-><init>(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;)V

    .line 898
    invoke-virtual {p5, v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->setSearchContentViewDelegate(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager$ContextualSearchContentViewDelegate;)V

    .line 900
    :cond_1
    return-void
.end method

.method public onInterceptHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 456
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->setContentViewMotionEventOffsets(Landroid/view/MotionEvent;Z)V

    .line 457
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onInterceptHoverEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 425
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-nez v2, :cond_0

    .line 426
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    iput v2, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mLastTapX:I

    .line 427
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v2, v2

    iput v2, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mLastTapY:I

    .line 429
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mNextEventFilter:Lcom/google/android/apps/chrome/eventfilter/EventFilter;

    iput-object v2, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mEventFilter:Lcom/google/android/apps/chrome/eventfilter/EventFilter;

    .line 430
    iget-object v2, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mEventFilter:Lcom/google/android/apps/chrome/eventfilter/EventFilter;

    if-nez v2, :cond_1

    .line 441
    :goto_0
    return v0

    .line 432
    :cond_1
    iput-boolean v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mFullscreenTouchEvent:Z

    .line 433
    iget-object v2, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->onInterceptMotionEvent(Landroid/view/MotionEvent;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-boolean v2, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mEnableCompositorTabStrip:Z

    if-nez v2, :cond_2

    .line 436
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mFullscreenTouchEvent:Z

    move v0, v1

    .line 437
    goto :goto_0

    .line 440
    :cond_2
    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->setContentViewMotionEventOffsets(Landroid/view/MotionEvent;Z)V

    .line 441
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mEventFilter:Lcom/google/android/apps/chrome/eventfilter/EventFilter;

    iget-boolean v1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mIsKeyboardShowing:Z

    invoke-virtual {v0, p1, v1}, Lcom/google/android/apps/chrome/eventfilter/EventFilter;->onInterceptTouchEvent(Landroid/view/MotionEvent;Z)Z

    move-result v0

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 2

    .prologue
    .line 727
    if-eqz p1, :cond_0

    .line 728
    sub-int v0, p4, p2

    sub-int v1, p5, p3

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->propagateViewportToLayouts(II)V

    .line 730
    :cond_0
    invoke-super/range {p0 .. p5}, Landroid/widget/FrameLayout;->onLayout(ZIIII)V

    .line 731
    return-void
.end method

.method protected onMeasure(II)V
    .locals 1

    .prologue
    .line 721
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onMeasure(II)V

    .line 722
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p0}, Lorg/chromium/ui/UiUtils;->isKeyboardShowing(Landroid/content/Context;Landroid/view/View;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mIsKeyboardShowing:Z

    .line 723
    return-void
.end method

.method public onNativeLibraryReady(Lcom/google/android/apps/chrome/WindowDelegate;Lorg/chromium/ui/base/WindowAndroid;Lcom/google/android/apps/chrome/compositor/TabContentManager;)V
    .locals 6

    .prologue
    .line 376
    sget-boolean v0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mLayerTitleCache:Lcom/google/android/apps/chrome/compositor/LayerTitleCache;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    const-string/jumbo v1, "Should be called once"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 378
    :cond_0
    invoke-static {}, Lcom/google/android/apps/chrome/device/DeviceClassManager;->enableLayerDecorationCache()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 379
    new-instance v0, Lcom/google/android/apps/chrome/compositor/LayerTitleCache;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/chrome/compositor/LayerTitleCache;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mLayerTitleCache:Lcom/google/android/apps/chrome/compositor/LayerTitleCache;

    .line 382
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mCompositorView:Lcom/google/android/apps/chrome/compositor/CompositorView;

    invoke-static {}, Lcom/google/android/apps/chrome/device/DeviceClassManager;->isLowEndDevice()Z

    move-result v1

    iget-object v4, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mLayerTitleCache:Lcom/google/android/apps/chrome/compositor/LayerTitleCache;

    move-object v2, p1

    move-object v3, p2

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/chrome/compositor/CompositorView;->initNativeCompositor(ZLcom/google/android/apps/chrome/WindowDelegate;Lorg/chromium/ui/base/WindowAndroid;Lcom/google/android/apps/chrome/compositor/LayerTitleCache;Lcom/google/android/apps/chrome/compositor/TabContentManager;)V

    .line 385
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mControlContainer:Lcom/google/android/apps/chrome/widget/ControlContainer;

    if-eqz v0, :cond_2

    .line 386
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mCompositorView:Lcom/google/android/apps/chrome/compositor/CompositorView;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/compositor/CompositorView;->getResourceManager()Lcom/google/android/apps/chrome/compositor/resources/ResourceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/compositor/resources/ResourceManager;->getDynamicResourceLoader()Lcom/google/android/apps/chrome/compositor/resources/dynamics/DynamicResourceLoader;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$id;->control_container:I

    iget-object v2, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mControlContainer:Lcom/google/android/apps/chrome/widget/ControlContainer;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/widget/ControlContainer;->getResourceAdapter()Lcom/google/android/apps/chrome/compositor/resources/dynamics/ViewResourceAdapter;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/compositor/resources/dynamics/DynamicResourceLoader;->registerResource(ILcom/google/android/apps/chrome/compositor/resources/dynamics/DynamicResource;)V

    .line 390
    :cond_2
    return-void
.end method

.method public onOverdrawBottomHeightChanged(I)V
    .locals 1

    .prologue
    .line 511
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    if-eqz v0, :cond_0

    .line 512
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    invoke-virtual {v0, p1}, Lorg/chromium/content/browser/ContentViewCore;->onOverdrawBottomHeightChanged(I)V

    .line 514
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mBackgroundContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    if-eqz v0, :cond_1

    .line 515
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mBackgroundContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    invoke-virtual {v0, p1}, Lorg/chromium/content/browser/ContentViewCore;->onOverdrawBottomHeightChanged(I)V

    .line 517
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mContextualSearchContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    if-eqz v0, :cond_2

    .line 518
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mContextualSearchContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    invoke-virtual {v0, p1}, Lorg/chromium/content/browser/ContentViewCore;->onOverdrawBottomHeightChanged(I)V

    .line 521
    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mSkipNextToolbarTextureUpdate:Z

    .line 522
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->requestRender()V

    .line 523
    return-void
.end method

.method public onPhysicalBackingSizeChanged(II)V
    .locals 1

    .prologue
    .line 501
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    if-nez v0, :cond_1

    .line 507
    :cond_0
    :goto_0
    return-void

    .line 502
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    invoke-virtual {v0, p1, p2}, Lorg/chromium/content/browser/ContentViewCore;->onPhysicalBackingSizeChanged(II)V

    .line 503
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mBackgroundContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    if-eqz v0, :cond_0

    .line 504
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mBackgroundContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    invoke-virtual {v0, p1, p2}, Lorg/chromium/content/browser/ContentViewCore;->onPhysicalBackingSizeChanged(II)V

    .line 505
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mContextualSearchContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    if-eqz v0, :cond_0

    .line 506
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mContextualSearchContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    invoke-virtual {v0, p1, p2}, Lorg/chromium/content/browser/ContentViewCore;->onPhysicalBackingSizeChanged(II)V

    goto :goto_0
.end method

.method protected onSizeChanged(IIII)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 474
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/FrameLayout;->onSizeChanged(IIII)V

    .line 480
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->getContainerView()Landroid/view/ViewGroup;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eq v0, p0, :cond_3

    .line 482
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->getContainerView()Landroid/view/ViewGroup;

    move-result-object v0

    invoke-virtual {v0, v2, v2, p1, p2}, Landroid/view/ViewGroup;->layout(IIII)V

    move v0, v1

    .line 485
    :goto_0
    iget-object v3, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mBackgroundContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mBackgroundContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    invoke-virtual {v3}, Lorg/chromium/content/browser/ContentViewCore;->isAlive()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mBackgroundContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    invoke-virtual {v3}, Lorg/chromium/content/browser/ContentViewCore;->getContainerView()Landroid/view/ViewGroup;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    if-eq v3, p0, :cond_0

    .line 487
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mBackgroundContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->getContainerView()Landroid/view/ViewGroup;

    move-result-object v0

    invoke-virtual {v0, v2, v2, p1, p2}, Landroid/view/ViewGroup;->layout(IIII)V

    move v0, v1

    .line 490
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mContextualSearchContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mContextualSearchContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    invoke-virtual {v1}, Lorg/chromium/content/browser/ContentViewCore;->isAlive()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mContextualSearchContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    invoke-virtual {v1}, Lorg/chromium/content/browser/ContentViewCore;->getContainerView()Landroid/view/ViewGroup;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-eq v1, p0, :cond_1

    .line 492
    iget-object v1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mContextualSearchContentViewCore:Lorg/chromium/content/browser/ContentViewCore;

    invoke-virtual {v1}, Lorg/chromium/content/browser/ContentViewCore;->getContainerView()Landroid/view/ViewGroup;

    move-result-object v1

    invoke-virtual {v1, v2, v2, p1, p2}, Landroid/view/ViewGroup;->layout(IIII)V

    .line 493
    or-int/lit8 v0, v0, 0x1

    .line 496
    :cond_1
    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->requestRender()V

    .line 497
    :cond_2
    return-void

    :cond_3
    move v0, v2

    goto :goto_0
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 540
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;

    if-eqz v0, :cond_0

    .line 541
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->getContentOffset()F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mLastContentOffset:F

    .line 542
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->getVisibleContentOffset()F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mLastVisibleContentOffset:F

    .line 543
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->addListener(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager$FullscreenListener;)V

    .line 545
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->requestRender()V

    .line 546
    return-void
.end method

.method public onStartGesture()V
    .locals 1

    .prologue
    .line 750
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mLayoutManager:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mLayoutManager:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;->onStartGesture()V

    .line 751
    :cond_0
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 552
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->removeListener(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager$FullscreenListener;)V

    .line 553
    :cond_0
    return-void
.end method

.method public onSurfaceCreated()V
    .locals 1

    .prologue
    .line 642
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mPendingSwapBuffersCount:I

    .line 643
    invoke-direct {p0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->flushInvalidation()V

    .line 644
    return-void
.end method

.method public onSwapBuffersCompleted(I)V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 648
    const-string/jumbo v1, "onSwapBuffersCompleted"

    invoke-static {v1}, Lorg/chromium/base/TraceEvent;->instant(Ljava/lang/String;)V

    .line 652
    iget-object v1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mControlContainer:Lcom/google/android/apps/chrome/widget/ControlContainer;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mControlContainer:Lcom/google/android/apps/chrome/widget/ControlContainer;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/widget/ControlContainer;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mHasDrawnOnce:Z

    if-eqz v1, :cond_0

    .line 654
    new-instance v1, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$5;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$5;-><init>(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;)V

    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->post(Ljava/lang/Runnable;)Z

    .line 662
    :cond_0
    iput-boolean v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mHasDrawnOnce:Z

    .line 664
    iput p1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mPendingSwapBuffersCount:I

    .line 666
    iget-boolean v1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mSkipInvalidation:Z

    if-eqz v1, :cond_1

    if-nez p1, :cond_2

    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->flushInvalidation()V

    .line 667
    :cond_2
    iget-boolean v1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mSkipInvalidation:Z

    if-nez v1, :cond_3

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mSkipInvalidation:Z

    .line 668
    return-void

    .line 667
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onToggleOverlayVideoMode(Z)V
    .locals 1

    .prologue
    .line 570
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mCompositorView:Lcom/google/android/apps/chrome/compositor/CompositorView;

    if-eqz v0, :cond_0

    .line 571
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mCompositorView:Lcom/google/android/apps/chrome/compositor/CompositorView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/compositor/CompositorView;->setOverlayVideoMode(Z)V

    .line 573
    :cond_0
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 446
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->onMotionEvent(Landroid/view/MotionEvent;)V

    .line 447
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mFullscreenTouchEvent:Z

    if-eqz v0, :cond_1

    .line 451
    :goto_0
    return v1

    .line 449
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mEventFilter:Lcom/google/android/apps/chrome/eventfilter/EventFilter;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mEventFilter:Lcom/google/android/apps/chrome/eventfilter/EventFilter;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/eventfilter/EventFilter;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 450
    :goto_1
    invoke-direct {p0, p1, v1}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->setContentViewMotionEventOffsets(Landroid/view/MotionEvent;Z)V

    move v1, v0

    .line 451
    goto :goto_0

    .line 449
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public onVisibleContentOffsetChanged(F)V
    .locals 2

    .prologue
    .line 563
    iput p1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mLastVisibleContentOffset:F

    .line 564
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->getHeight()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->propagateViewportToLayouts(II)V

    .line 565
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->requestRender()V

    .line 566
    return-void
.end method

.method public propagateEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 713
    invoke-direct {p0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->getViewToPropagateEventsTo()Landroid/view/View;

    move-result-object v0

    .line 714
    if-eqz v0, :cond_0

    if-nez p1, :cond_1

    :cond_0
    const/4 v0, 0x0

    .line 716
    :goto_0
    return v0

    :cond_1
    invoke-virtual {v0, p1}, Landroid/view/View;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public requestRender()V
    .locals 1

    .prologue
    .line 637
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mCompositorView:Lcom/google/android/apps/chrome/compositor/CompositorView;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/compositor/CompositorView;->requestRender()V

    .line 638
    return-void
.end method

.method public resetFlags()V
    .locals 1

    .prologue
    .line 359
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mCompositorView:Lcom/google/android/apps/chrome/compositor/CompositorView;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/compositor/CompositorView;->resetFlags()V

    .line 360
    return-void
.end method

.method public setContentOverlayVisibility(Z)V
    .locals 1

    .prologue
    .line 672
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mContentOverlayVisiblity:Z

    if-eq p1, v0, :cond_0

    .line 673
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mContentOverlayVisiblity:Z

    .line 674
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mContentOverlayVisiblity:Z

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->updateContentOverlayVisibility(Z)V

    .line 676
    :cond_0
    return-void
.end method

.method public setControlContainer(Lcom/google/android/apps/chrome/widget/ControlContainer;)V
    .locals 3

    .prologue
    .line 336
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mCompositorView:Lcom/google/android/apps/chrome/compositor/CompositorView;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/compositor/CompositorView;->getResourceManager()Lcom/google/android/apps/chrome/compositor/resources/ResourceManager;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mControlContainer:Lcom/google/android/apps/chrome/widget/ControlContainer;

    if-eqz v0, :cond_0

    .line 337
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mCompositorView:Lcom/google/android/apps/chrome/compositor/CompositorView;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/compositor/CompositorView;->getResourceManager()Lcom/google/android/apps/chrome/compositor/resources/ResourceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/compositor/resources/ResourceManager;->getDynamicResourceLoader()Lcom/google/android/apps/chrome/compositor/resources/dynamics/DynamicResourceLoader;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$id;->control_container:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/compositor/resources/dynamics/DynamicResourceLoader;->unregisterResource(I)V

    .line 340
    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mControlContainer:Lcom/google/android/apps/chrome/widget/ControlContainer;

    .line 341
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mCompositorView:Lcom/google/android/apps/chrome/compositor/CompositorView;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/compositor/CompositorView;->getResourceManager()Lcom/google/android/apps/chrome/compositor/resources/ResourceManager;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mControlContainer:Lcom/google/android/apps/chrome/widget/ControlContainer;

    if-eqz v0, :cond_1

    .line 342
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mCompositorView:Lcom/google/android/apps/chrome/compositor/CompositorView;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/compositor/CompositorView;->getResourceManager()Lcom/google/android/apps/chrome/compositor/resources/ResourceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/compositor/resources/ResourceManager;->getDynamicResourceLoader()Lcom/google/android/apps/chrome/compositor/resources/dynamics/DynamicResourceLoader;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$id;->control_container:I

    iget-object v2, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mControlContainer:Lcom/google/android/apps/chrome/widget/ControlContainer;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/widget/ControlContainer;->getResourceAdapter()Lcom/google/android/apps/chrome/compositor/resources/dynamics/ViewResourceAdapter;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/compositor/resources/dynamics/DynamicResourceLoader;->registerResource(ILcom/google/android/apps/chrome/compositor/resources/dynamics/DynamicResource;)V

    .line 345
    :cond_1
    return-void
.end method

.method public setEventFilter(Lcom/google/android/apps/chrome/eventfilter/EventFilter;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 742
    iput-object p1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mNextEventFilter:Lcom/google/android/apps/chrome/eventfilter/EventFilter;

    .line 743
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mNextEventFilter:Lcom/google/android/apps/chrome/eventfilter/EventFilter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mNextEventFilter:Lcom/google/android/apps/chrome/eventfilter/EventFilter;

    invoke-virtual {v0, v1, v1}, Lcom/google/android/apps/chrome/eventfilter/EventFilter;->setCurrentMotionEventOffsets(FF)V

    .line 744
    :cond_0
    return-void
.end method

.method public setFullscreenHandler(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;)V
    .locals 2

    .prologue
    .line 769
    iput-object p1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;

    .line 770
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->getContentOffset()F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mLastContentOffset:F

    .line 771
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->getVisibleContentOffset()F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mLastVisibleContentOffset:F

    .line 772
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->addListener(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager$FullscreenListener;)V

    .line 773
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->getHeight()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->propagateViewportToLayouts(II)V

    .line 774
    return-void
.end method

.method public setLayoutManager(Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;)V
    .locals 2

    .prologue
    .line 321
    iput-object p1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mLayoutManager:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;

    .line 322
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->getHeight()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->propagateViewportToLayouts(II)V

    .line 323
    return-void
.end method

.method public setRootView(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 329
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mCompositorView:Lcom/google/android/apps/chrome/compositor/CompositorView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/compositor/CompositorView;->setRootView(Landroid/view/View;)V

    .line 330
    return-void
.end method

.method public setUrlBar(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 801
    iput-object p1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mUrlBar:Landroid/view/View;

    .line 802
    return-void
.end method

.method public shutDown()V
    .locals 1

    .prologue
    .line 366
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->setTab(Lcom/google/android/apps/chrome/tab/ChromeTab;)V

    .line 367
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mLayerTitleCache:Lcom/google/android/apps/chrome/compositor/LayerTitleCache;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mLayerTitleCache:Lcom/google/android/apps/chrome/compositor/LayerTitleCache;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/compositor/LayerTitleCache;->shutDown()V

    .line 368
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mCompositorView:Lcom/google/android/apps/chrome/compositor/CompositorView;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/compositor/CompositorView;->shutDown()V

    .line 369
    return-void
.end method
