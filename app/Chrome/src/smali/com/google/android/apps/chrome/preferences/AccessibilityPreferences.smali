.class public Lcom/google/android/apps/chrome/preferences/AccessibilityPreferences;
.super Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;
.source "AccessibilityPreferences.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# static fields
.field public static final FORCE_ENABLE_ZOOM_THRESHOLD_MULTIPLIER:F = 1.3f


# instance fields
.field private mFontPrefsObserver:Lcom/google/android/apps/chrome/preferences/AccessibilityPreferences$FontPrefsObserver;

.field private mFontSizePrefs:Lorg/chromium/chrome/browser/accessibility/FontSizePrefs;

.field private mFontSizePreview:Lcom/google/android/apps/chrome/preferences/FontSizePreview;

.field private mForceEnableZoomPref:Lcom/google/android/apps/chrome/preferences/SeekBarLinkedCheckBoxPreference;

.field private mFormat:Ljava/text/NumberFormat;

.field private mPreviousFontScaleFactor:F

.field private mTextScalePref:Lcom/google/android/apps/chrome/preferences/SeekBarPreference;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;-><init>()V

    .line 42
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/preferences/AccessibilityPreferences;F)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/preferences/AccessibilityPreferences;->processFontWithForceEnableZoom(F)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/preferences/AccessibilityPreferences;F)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/preferences/AccessibilityPreferences;->updateTextScaleSummary(F)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/preferences/AccessibilityPreferences;)Lcom/google/android/apps/chrome/preferences/SeekBarLinkedCheckBoxPreference;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/AccessibilityPreferences;->mForceEnableZoomPref:Lcom/google/android/apps/chrome/preferences/SeekBarLinkedCheckBoxPreference;

    return-object v0
.end method

.method private processFontWithForceEnableZoom(F)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const v1, 0x3fa66666    # 1.3f

    .line 111
    iget v0, p0, Lcom/google/android/apps/chrome/preferences/AccessibilityPreferences;->mPreviousFontScaleFactor:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_1

    cmpl-float v0, p1, v1

    if-ltz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/AccessibilityPreferences;->mFontSizePrefs:Lorg/chromium/chrome/browser/accessibility/FontSizePrefs;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/accessibility/FontSizePrefs;->getForceEnableZoom()Z

    move-result v0

    if-nez v0, :cond_1

    .line 116
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/AccessibilityPreferences;->mFontSizePrefs:Lorg/chromium/chrome/browser/accessibility/FontSizePrefs;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lorg/chromium/chrome/browser/accessibility/FontSizePrefs;->setForceEnableZoom(Z)V

    .line 117
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/AccessibilityPreferences;->mFontSizePrefs:Lorg/chromium/chrome/browser/accessibility/FontSizePrefs;

    invoke-virtual {v0, v2}, Lorg/chromium/chrome/browser/accessibility/FontSizePrefs;->setUserSetForceEnableZoom(Z)V

    .line 124
    :cond_0
    :goto_0
    return-void

    .line 118
    :cond_1
    iget v0, p0, Lcom/google/android/apps/chrome/preferences/AccessibilityPreferences;->mPreviousFontScaleFactor:F

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_0

    cmpg-float v0, p1, v1

    if-gez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/AccessibilityPreferences;->mFontSizePrefs:Lorg/chromium/chrome/browser/accessibility/FontSizePrefs;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/accessibility/FontSizePrefs;->getUserSetForceEnableZoom()Z

    move-result v0

    if-nez v0, :cond_0

    .line 122
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/AccessibilityPreferences;->mFontSizePrefs:Lorg/chromium/chrome/browser/accessibility/FontSizePrefs;

    invoke-virtual {v0, v2}, Lorg/chromium/chrome/browser/accessibility/FontSizePrefs;->setForceEnableZoom(Z)V

    goto :goto_0
.end method

.method private updateTextScaleSummary(F)V
    .locals 4

    .prologue
    .line 106
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/AccessibilityPreferences;->mTextScalePref:Lcom/google/android/apps/chrome/preferences/SeekBarPreference;

    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/AccessibilityPreferences;->mFormat:Ljava/text/NumberFormat;

    float-to-double v2, p1

    invoke-virtual {v1, v2, v3}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 107
    iput p1, p0, Lcom/google/android/apps/chrome/preferences/AccessibilityPreferences;->mPreviousFontScaleFactor:F

    .line 108
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 60
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    .line 61
    invoke-static {}, Ljava/text/NumberFormat;->getPercentInstance()Ljava/text/NumberFormat;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/preferences/AccessibilityPreferences;->mFormat:Ljava/text/NumberFormat;

    .line 62
    sget v0, Lcom/google/android/apps/chrome/R$xml;->accessibility_preferences:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/AccessibilityPreferences;->addPreferencesFromResource(I)V

    .line 64
    const-string/jumbo v0, "font_size_preview"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/AccessibilityPreferences;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/preferences/FontSizePreview;

    iput-object v0, p0, Lcom/google/android/apps/chrome/preferences/AccessibilityPreferences;->mFontSizePreview:Lcom/google/android/apps/chrome/preferences/FontSizePreview;

    .line 66
    const-string/jumbo v0, "text_scale"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/AccessibilityPreferences;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;

    iput-object v0, p0, Lcom/google/android/apps/chrome/preferences/AccessibilityPreferences;->mTextScalePref:Lcom/google/android/apps/chrome/preferences/SeekBarPreference;

    .line 67
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/AccessibilityPreferences;->mTextScalePref:Lcom/google/android/apps/chrome/preferences/SeekBarPreference;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/chrome/preferences/SeekBarPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 69
    const-string/jumbo v0, "force_enable_zoom"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/AccessibilityPreferences;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/preferences/SeekBarLinkedCheckBoxPreference;

    iput-object v0, p0, Lcom/google/android/apps/chrome/preferences/AccessibilityPreferences;->mForceEnableZoomPref:Lcom/google/android/apps/chrome/preferences/SeekBarLinkedCheckBoxPreference;

    .line 71
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/AccessibilityPreferences;->mForceEnableZoomPref:Lcom/google/android/apps/chrome/preferences/SeekBarLinkedCheckBoxPreference;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/chrome/preferences/SeekBarLinkedCheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 72
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/AccessibilityPreferences;->mForceEnableZoomPref:Lcom/google/android/apps/chrome/preferences/SeekBarLinkedCheckBoxPreference;

    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/AccessibilityPreferences;->mTextScalePref:Lcom/google/android/apps/chrome/preferences/SeekBarPreference;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/preferences/SeekBarLinkedCheckBoxPreference;->setLinkedSeekBarPreference(Lcom/google/android/apps/chrome/preferences/SeekBarPreference;)V

    .line 74
    new-instance v0, Lcom/google/android/apps/chrome/preferences/AccessibilityPreferences$FontPrefsObserver;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/chrome/preferences/AccessibilityPreferences$FontPrefsObserver;-><init>(Lcom/google/android/apps/chrome/preferences/AccessibilityPreferences;Lcom/google/android/apps/chrome/preferences/AccessibilityPreferences$1;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/preferences/AccessibilityPreferences;->mFontPrefsObserver:Lcom/google/android/apps/chrome/preferences/AccessibilityPreferences$FontPrefsObserver;

    .line 75
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/AccessibilityPreferences;->mForceEnableZoomPref:Lcom/google/android/apps/chrome/preferences/SeekBarLinkedCheckBoxPreference;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/SeekBarLinkedCheckBoxPreference;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/chrome/browser/accessibility/FontSizePrefs;->getInstance(Landroid/content/Context;)Lorg/chromium/chrome/browser/accessibility/FontSizePrefs;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/preferences/AccessibilityPreferences;->mFontSizePrefs:Lorg/chromium/chrome/browser/accessibility/FontSizePrefs;

    .line 76
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/AccessibilityPreferences;->mFontSizePrefs:Lorg/chromium/chrome/browser/accessibility/FontSizePrefs;

    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/AccessibilityPreferences;->mFontPrefsObserver:Lcom/google/android/apps/chrome/preferences/AccessibilityPreferences$FontPrefsObserver;

    invoke-virtual {v0, v1}, Lorg/chromium/chrome/browser/accessibility/FontSizePrefs;->addObserver(Lorg/chromium/chrome/browser/accessibility/FontSizePrefs$Observer;)Z

    .line 78
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/AccessibilityPreferences;->mFontSizePrefs:Lorg/chromium/chrome/browser/accessibility/FontSizePrefs;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/accessibility/FontSizePrefs;->getFontScaleFactor()F

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/preferences/AccessibilityPreferences;->updateTextScaleSummary(F)V

    .line 82
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/AccessibilityPreferences;->mForceEnableZoomPref:Lcom/google/android/apps/chrome/preferences/SeekBarLinkedCheckBoxPreference;

    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/AccessibilityPreferences;->mFontSizePrefs:Lorg/chromium/chrome/browser/accessibility/FontSizePrefs;

    invoke-virtual {v1}, Lorg/chromium/chrome/browser/accessibility/FontSizePrefs;->getForceEnableZoom()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/preferences/SeekBarLinkedCheckBoxPreference;->setChecked(Z)V

    .line 83
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 101
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/AccessibilityPreferences;->mFontSizePrefs:Lorg/chromium/chrome/browser/accessibility/FontSizePrefs;

    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/AccessibilityPreferences;->mFontPrefsObserver:Lcom/google/android/apps/chrome/preferences/AccessibilityPreferences$FontPrefsObserver;

    invoke-virtual {v0, v1}, Lorg/chromium/chrome/browser/accessibility/FontSizePrefs;->removeObserver(Lorg/chromium/chrome/browser/accessibility/FontSizePrefs$Observer;)Z

    .line 102
    invoke-super {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;->onDestroy()V

    .line 103
    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 128
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/AccessibilityPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v1

    if-nez v1, :cond_1

    .line 130
    const/4 v0, 0x0

    .line 138
    :cond_0
    :goto_0
    return v0

    .line 132
    :cond_1
    const-string/jumbo v1, "text_scale"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 133
    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/AccessibilityPreferences;->mFontSizePrefs:Lorg/chromium/chrome/browser/accessibility/FontSizePrefs;

    check-cast p2, Ljava/lang/Float;

    invoke-virtual {p2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    invoke-virtual {v1, v2}, Lorg/chromium/chrome/browser/accessibility/FontSizePrefs;->setFontScaleFactor(F)V

    goto :goto_0

    .line 134
    :cond_2
    const-string/jumbo v1, "force_enable_zoom"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 135
    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/AccessibilityPreferences;->mFontSizePrefs:Lorg/chromium/chrome/browser/accessibility/FontSizePrefs;

    invoke-virtual {v1, v0}, Lorg/chromium/chrome/browser/accessibility/FontSizePrefs;->setUserSetForceEnableZoom(Z)V

    .line 136
    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/AccessibilityPreferences;->mFontSizePrefs:Lorg/chromium/chrome/browser/accessibility/FontSizePrefs;

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {v1, v2}, Lorg/chromium/chrome/browser/accessibility/FontSizePrefs;->setForceEnableZoom(Z)V

    goto :goto_0
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 87
    invoke-super {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;->onStart()V

    .line 89
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/AccessibilityPreferences;->mFontSizePreview:Lcom/google/android/apps/chrome/preferences/FontSizePreview;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/FontSizePreview;->startObservingFontPrefs()V

    .line 90
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/AccessibilityPreferences;->mFontSizePreview:Lcom/google/android/apps/chrome/preferences/FontSizePreview;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/FontSizePreview;->stopObservingFontPrefs()V

    .line 96
    invoke-super {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;->onStop()V

    .line 97
    return-void
.end method
