.class Lcom/google/android/apps/chrome/document/DocumentSuggestionProvider$2;
.super Ljava/lang/Object;
.source "DocumentSuggestionProvider.java"

# interfaces
.implements Lorg/chromium/content/browser/BrowserStartupController$StartupCallback;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/document/DocumentSuggestionProvider;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/document/DocumentSuggestionProvider;)V
    .locals 0

    .prologue
    .line 74
    iput-object p1, p0, Lcom/google/android/apps/chrome/document/DocumentSuggestionProvider$2;->this$0:Lcom/google/android/apps/chrome/document/DocumentSuggestionProvider;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure()V
    .locals 2

    .prologue
    .line 83
    const-string/jumbo v0, "DocumentSuggestionProvider"

    const-string/jumbo v1, "Failed to start browser process"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 84
    return-void
.end method

.method public onSuccess(Z)V
    .locals 2

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentSuggestionProvider$2;->this$0:Lcom/google/android/apps/chrome/document/DocumentSuggestionProvider;

    # getter for: Lcom/google/android/apps/chrome/document/DocumentSuggestionProvider;->mController:Lorg/chromium/chrome/browser/omnibox/AutocompleteController;
    invoke-static {v0}, Lcom/google/android/apps/chrome/document/DocumentSuggestionProvider;->access$000(Lcom/google/android/apps/chrome/document/DocumentSuggestionProvider;)Lorg/chromium/chrome/browser/omnibox/AutocompleteController;

    move-result-object v0

    invoke-static {}, Lorg/chromium/chrome/browser/profiles/Profile;->getLastUsedProfile()Lorg/chromium/chrome/browser/profiles/Profile;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/chromium/chrome/browser/omnibox/AutocompleteController;->setProfile(Lorg/chromium/chrome/browser/profiles/Profile;)V

    .line 78
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentSuggestionProvider$2;->this$0:Lcom/google/android/apps/chrome/document/DocumentSuggestionProvider;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/apps/chrome/document/DocumentSuggestionProvider;->mInitialized:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/document/DocumentSuggestionProvider;->access$102(Lcom/google/android/apps/chrome/document/DocumentSuggestionProvider;Z)Z

    .line 79
    return-void
.end method
