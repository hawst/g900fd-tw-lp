.class Lcom/google/android/apps/chrome/webapps/WebappActivity$5;
.super Landroid/os/AsyncTask;
.source "WebappActivity.java"


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/webapps/WebappActivity;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/webapps/WebappActivity;)V
    .locals 0

    .prologue
    .line 326
    iput-object p1, p0, Lcom/google/android/apps/chrome/webapps/WebappActivity$5;->this$0:Lcom/google/android/apps/chrome/webapps/WebappActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 326
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/webapps/WebappActivity$5;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 8

    .prologue
    .line 329
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/WebappActivity$5;->this$0:Lcom/google/android/apps/chrome/webapps/WebappActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/webapps/WebappActivity;->getActivityDirectory()Ljava/io/File;

    move-result-object v1

    .line 332
    invoke-virtual {v1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    .line 333
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/webapps/WebappActivity$5;->isCancelled()Z

    move-result v5

    if-nez v5, :cond_1

    .line 335
    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 336
    iget-object v5, p0, Lcom/google/android/apps/chrome/webapps/WebappActivity$5;->this$0:Lcom/google/android/apps/chrome/webapps/WebappActivity;

    invoke-virtual {v5}, Lcom/google/android/apps/chrome/webapps/WebappActivity;->getLoggingTag()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "Deleted file: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 332
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 338
    :cond_0
    iget-object v5, p0, Lcom/google/android/apps/chrome/webapps/WebappActivity$5;->this$0:Lcom/google/android/apps/chrome/webapps/WebappActivity;

    invoke-virtual {v5}, Lcom/google/android/apps/chrome/webapps/WebappActivity;->getLoggingTag()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "Failed to delete file: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 343
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/webapps/WebappActivity$5;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_2

    .line 344
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 345
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/WebappActivity$5;->this$0:Lcom/google/android/apps/chrome/webapps/WebappActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/webapps/WebappActivity;->getLoggingTag()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Deleted directory: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 351
    :cond_2
    :goto_2
    const/4 v0, 0x0

    return-object v0

    .line 347
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/WebappActivity$5;->this$0:Lcom/google/android/apps/chrome/webapps/WebappActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/webapps/WebappActivity;->getLoggingTag()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Failed to delete directory: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method
