.class public Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;
.super Lcom/google/android/apps/chrome/snapshot/IntentServiceWithWakeLock;
.source "SnapshotArchiveManager.java"


# static fields
.field static final ACCEPTED_MIME_TYPES:Ljava/lang/String; = "application/pdf, multipart/related"

.field static final ACTION_FETCH:Ljava/lang/String; = "com.google.android.apps.chrome.snapshot.ACTION_FETCH"

.field public static final ACTION_SNAPSHOT_STATE_UPDATE:Ljava/lang/String; = "com.google.android.apps.chrome.snapshot.ACTION_SNAPSHOT_STATE_UPDATE"

.field public static final DOWNLOAD_ID_ERROR:J = -0x1L

.field static final DRY_RUN_JOB:Ljava/lang/String; = "dry_run_job"

.field public static final EXTRA_ACCOUNT:Ljava/lang/String; = "account"

.field public static final EXTRA_DOCUMENT_ID:Ljava/lang/String; = "Chrome_DocumentID"

.field public static final EXTRA_PRINT_JOB:Ljava/lang/String; = "printJob"

.field public static final EXTRA_SNAPSHOT_ERROR_MESSAGE:Ljava/lang/String; = "Chrome_ErrorMessage"

.field public static final EXTRA_SNAPSHOT_ID:Ljava/lang/String; = "Chrome_SnapshotID"

.field public static final EXTRA_SNAPSHOT_QUERY:Ljava/lang/String; = "Chrome_Query"

.field public static final EXTRA_SNAPSHOT_URI:Ljava/lang/String; = "Chrome_SnapshotURI"

.field public static final EXTRA_SNAPSHOT_VIEWABLE_STATE:Ljava/lang/String; = "Chrome_SnapshotState"

.field protected static final MIME_TYPE_MHTML:Ljava/lang/String; = "multipart/related"

.field protected static final MIME_TYPE_PDF:Ljava/lang/String; = "application/pdf"

.field private static sProviderDelegate:Lcom/google/android/apps/chrome/snapshot/DefaultSnapshotProviderDelegate;


# instance fields
.field mAuthTokenHelper:Lcom/google/android/apps/chrome/snapshot/cloudprint/AuthTokenHelper;

.field private final mDatabaseChangeCalculator:Lcom/google/android/apps/chrome/snapshot/DatabaseChangeCalculator;

.field private mDownloadDelegate:Lcom/google/android/apps/chrome/snapshot/SnapshotDownloadDelegate;

.field private final mExternalStorageErrorMap:Ljava/util/Map;

.field private mProviderDelegate:Lcom/google/android/apps/chrome/snapshot/SnapshotProviderDelegate;

.field private final mSupportedMimeTypes:Ljava/util/HashSet;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 187
    const-string/jumbo v0, "SnapshotArchiveManager"

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/snapshot/IntentServiceWithWakeLock;-><init>(Ljava/lang/String;)V

    .line 176
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->mExternalStorageErrorMap:Ljava/util/Map;

    .line 188
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->setIntentRedelivery(Z)V

    .line 189
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->mSupportedMimeTypes:Ljava/util/HashSet;

    .line 190
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->mSupportedMimeTypes:Ljava/util/HashSet;

    const-string/jumbo v1, "application/pdf"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 191
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->mSupportedMimeTypes:Ljava/util/HashSet;

    const-string/jumbo v1, "multipart/related"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 192
    invoke-static {}, Lcom/google/android/apps/chrome/snapshot/DatabaseChangeCalculatorFactory;->getInstance()Lcom/google/android/apps/chrome/snapshot/DatabaseChangeCalculator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->mDatabaseChangeCalculator:Lcom/google/android/apps/chrome/snapshot/DatabaseChangeCalculator;

    .line 193
    invoke-direct {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->setupExternalStorageErrorMap()V

    .line 194
    return-void
.end method

.method private static addFileExtension(Ljava/lang/String;Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1137
    const-string/jumbo v0, "application/pdf"

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->getMimeType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1138
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ".pdf"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1140
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ".mht"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private broadcastDocumentState(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;Z)V
    .locals 1

    .prologue
    .line 1050
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->broadcastDocumentState(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;ZLjava/lang/String;)V

    .line 1051
    return-void
.end method

.method private broadcastDocumentState(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;ZLjava/lang/String;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1055
    invoke-static {p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->getSnapshotViewableState(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)Lcom/google/android/apps/chrome/snapshot/SnapshotViewableState;

    move-result-object v2

    .line 1056
    new-instance v3, Landroid/content/Intent;

    const-string/jumbo v0, "com.google.android.apps.chrome.snapshot.ACTION_SNAPSHOT_STATE_UPDATE"

    invoke-direct {v3, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1057
    const-string/jumbo v0, "Chrome_DocumentID"

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->getId()I

    move-result v4

    invoke-virtual {v3, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1058
    const-string/jumbo v0, "Chrome_SnapshotID"

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->getSnapshotId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1059
    const-string/jumbo v4, "Chrome_SnapshotURI"

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->getLocalUri()Landroid/net/Uri;

    move-result-object v0

    if-nez v0, :cond_1

    move-object v0, v1

    :goto_0
    invoke-virtual {v3, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1061
    const-string/jumbo v0, "Chrome_SnapshotState"

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/snapshot/SnapshotViewableState;->toValue()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1062
    const-string/jumbo v0, "Chrome_Query"

    invoke-virtual {v3, v0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1063
    if-eqz p3, :cond_0

    .line 1064
    const-string/jumbo v0, "Chrome_ErrorMessage"

    invoke-virtual {v3, v0, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1066
    :cond_0
    invoke-virtual {p0, v3, v1}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 1067
    return-void

    .line 1059
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->getLocalUri()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private changePrintJobStatus(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJobStatus;)V
    .locals 2

    .prologue
    .line 814
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->getJobId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p2}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->createControlIntent(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJobStatus;)Landroid/content/Intent;

    move-result-object v0

    .line 816
    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->startServiceWithWakeLockInternal(Landroid/content/Intent;)V

    .line 817
    return-void
.end method

.method public static createDownloadFinishedIntent(Landroid/content/Context;J)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 498
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 499
    const-string/jumbo v1, "com.google.android.apps.chrome.snapshot.ACTION_DOWNLOAD_FINISHED"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 500
    const-string/jumbo v1, "downloadId"

    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 501
    return-object v0
.end method

.method static createExecutePendingWorkIntent(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 643
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 644
    const-string/jumbo v1, "com.google.android.apps.chrome.snapshot.ACTION_EXECUTE_PENDING_WORK"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 645
    return-object v0
.end method

.method public static createFetchPrintJobsIntent(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 403
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 404
    const-string/jumbo v1, "com.google.android.apps.chrome.snapshot.ACTION_FETCH"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 405
    return-object v0
.end method

.method public static createInitializeIntent(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 283
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 284
    const-string/jumbo v1, "com.google.android.apps.chrome.snapshot.ACTION_INITIALIZE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 285
    return-object v0
.end method

.method public static createJobDeletedIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 486
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 487
    const-string/jumbo v1, "com.google.android.apps.chrome.snapshot.ACTION_JOB_DELETED"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 488
    const-string/jumbo v1, "jobId"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 489
    return-object v0
.end method

.method public static createPrintJobFetchResultIntent(Landroid/content/Context;Ljava/util/Set;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 417
    new-instance v1, Landroid/content/Intent;

    const-class v0, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;

    invoke-direct {v1, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 418
    const-string/jumbo v0, "com.google.android.apps.chrome.snapshot.ACTION_FETCH_RESULT"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 419
    const-string/jumbo v2, "printJobs"

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 421
    return-object v1

    .line 419
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    goto :goto_0
.end method

.method public static createPrintJobFromC2DMNotificationIntent(Landroid/content/Context;Landroid/accounts/Account;Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 435
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 436
    const-string/jumbo v1, "com.google.android.apps.chrome.snapshot.ACTION_C2DM_NOTIFICATION"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 437
    const-string/jumbo v1, "account"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 438
    const-string/jumbo v1, "printJob"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 439
    return-object v0
.end method

.method public static createPrintJobStatusChangedIntent(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJobStatus;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 455
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 456
    const-string/jumbo v1, "com.google.android.apps.chrome.snapshot.ACTION_JOB_STATUS_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 457
    const-string/jumbo v1, "jobId"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 458
    const-string/jumbo v1, "printJobStatus"

    invoke-virtual {p2}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJobStatus;->getStatus()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 459
    return-object v0
.end method

.method public static createQueryStateIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 582
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 583
    const-string/jumbo v1, "com.google.android.apps.chrome.snapshot.ACTION_QUERY_STATE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 584
    const-string/jumbo v1, "Chrome_SnapshotID"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 585
    return-object v0
.end method

.method public static createSetEnabledIntent(Landroid/content/Context;Z)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 376
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 377
    const-string/jumbo v1, "enabled"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 379
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 380
    const-string/jumbo v2, "com.google.android.apps.chrome.snapshot.ACTION_SET_ENABLED"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 381
    const-string/jumbo v2, "com.google.android.apps.chrome.snapshot.SnapshotArchiveManager"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 382
    return-object v1
.end method

.method protected static createSlugFilename(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 1121
    const/4 v0, 0x0

    .line 1122
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->getTitle()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1123
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->getTitle()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 1125
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->getPageUri()Landroid/net/Uri;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->getPageUri()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->getPageUri()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1128
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->getPageUri()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 1129
    if-nez v0, :cond_2

    move-object v0, v1

    .line 1131
    :cond_1
    :goto_0
    if-nez v0, :cond_3

    const-string/jumbo v0, "ChromeSnapshot"

    .line 1133
    :goto_1
    invoke-static {v0, p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->addFileExtension(Ljava/lang/String;Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1129
    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1131
    :cond_3
    invoke-static {v0}, Lcom/google/android/apps/chrome/snapshot/SlugGenerator;->generateSlug(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method private static createSnapshotJobStateContentValues(Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;Landroid/content/ContentValues;)Landroid/content/ContentValues;
    .locals 3

    .prologue
    .line 891
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 892
    const-string/jumbo v1, "state"

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;->toValue()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 893
    if-eqz p1, :cond_0

    .line 894
    invoke-virtual {v0, p1}, Landroid/content/ContentValues;->putAll(Landroid/content/ContentValues;)V

    .line 896
    :cond_0
    return-object v0
.end method

.method private deletePageUrlJob(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)V
    .locals 2

    .prologue
    .line 808
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->getPageUrlJobId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->createDeleteJobIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 810
    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->startServiceWithWakeLockInternal(Landroid/content/Intent;)V

    .line 811
    return-void
.end method

.method private static displayAndroidNotification(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1223
    invoke-static {}, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->getInstance()Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;

    move-result-object v0

    .line 1225
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->getId()I

    move-result v1

    invoke-virtual {v0, v1, p1}, Lcom/google/android/apps/chrome/services/GoogleServicesNotificationController;->showOneOffNotification(ILjava/lang/String;)V

    .line 1226
    return-void
.end method

.method private displayDownloadError(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1213
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->getSnapshotId()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1215
    invoke-static {p1, p2}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->displayAndroidNotification(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;Ljava/lang/String;)V

    .line 1220
    :goto_0
    return-void

    .line 1218
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->broadcastDocumentState(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;ZLjava/lang/String;)V

    goto :goto_0
.end method

.method private downloadSnapshotJob(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)Z
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v0, 0x1

    .line 830
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->getDownloadUri()Landroid/net/Uri;

    move-result-object v2

    const-string/jumbo v3, "application/pdf, multipart/related"

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->getDownloadRequest(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/DownloadManagerRequestData;

    move-result-object v1

    .line 832
    if-nez v1, :cond_0

    .line 833
    const-string/jumbo v1, "SnapshotArchiveManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Unable to get download request. Not starting download for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 835
    sget-object v1, Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;->UNABLE_TO_DOWNLOAD:Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;

    invoke-direct {p0, p1, v1, v5}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->updateDatabaseWithNewSnapshotJobState(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;Landroid/content/ContentValues;)I

    .line 858
    :goto_0
    return v0

    .line 840
    :cond_0
    const-string/jumbo v2, "SnapshotArchiveManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Starting to download document: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 841
    invoke-direct {p0, v1, p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->updateDownloadRequestFromDocument(Lcom/google/android/apps/chrome/snapshot/DownloadManagerRequestData;Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)V

    .line 844
    :try_start_0
    invoke-direct {p0, v1, p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->setupDownloadEnvironment(Lcom/google/android/apps/chrome/snapshot/DownloadManagerRequestData;Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)V

    .line 845
    iget-object v2, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->mDownloadDelegate:Lcom/google/android/apps/chrome/snapshot/SnapshotDownloadDelegate;

    invoke-interface {v2, v1}, Lcom/google/android/apps/chrome/snapshot/SnapshotDownloadDelegate;->enqueue(Lcom/google/android/apps/chrome/snapshot/DownloadManagerRequestData;)J
    :try_end_0
    .catch Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager$ExternalStorageException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 854
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 855
    const-string/jumbo v3, "downloadId"

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 856
    sget-object v0, Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;->DOWNLOADING:Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;

    invoke-direct {p0, p1, v0, v2}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->updateDatabaseWithNewSnapshotJobState(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;Landroid/content/ContentValues;)I

    .line 858
    const/4 v0, 0x0

    goto :goto_0

    .line 846
    :catch_0
    move-exception v1

    .line 847
    const-string/jumbo v2, "SnapshotArchiveManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "External storage unavailable, so not starting download for "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ". Error: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager$ExternalStorageException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 849
    sget-object v2, Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;->UNABLE_TO_DOWNLOAD:Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;

    invoke-direct {p0, p1, v2, v5}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->updateDatabaseWithNewSnapshotJobState(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;Landroid/content/ContentValues;)I

    .line 851
    invoke-virtual {v1}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager$ExternalStorageException;->getResourceId()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->getDownloadedErrorMessage(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p1, v1}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->displayDownloadError(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private executePendingWorkForDocument(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)Z
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x0

    .line 737
    .line 738
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->getState()Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;

    move-result-object v0

    .line 739
    if-eqz v0, :cond_0

    .line 740
    sget-object v2, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager$1;->$SwitchMap$com$google$android$apps$chrome$snapshot$SnapshotJobState:[I

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;->ordinal()I

    move-result v0

    aget v0, v2, v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    move v0, v1

    .line 755
    :goto_1
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->getPageUrlState()Lcom/google/android/apps/chrome/snapshot/PageUrlJobState;

    move-result-object v2

    .line 761
    if-eqz v2, :cond_1

    .line 762
    sget-object v3, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager$1;->$SwitchMap$com$google$android$apps$chrome$snapshot$PageUrlJobState:[I

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/snapshot/PageUrlJobState;->ordinal()I

    move-result v2

    aget v2, v3, v2

    packed-switch v2, :pswitch_data_1

    .line 770
    :cond_1
    :goto_2
    return v0

    .line 742
    :pswitch_0
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->downloadSnapshotJob(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)Z

    move-result v0

    goto :goto_1

    .line 745
    :pswitch_1
    invoke-direct {p0, p1, v1, v4}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->broadcastDocumentState(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;ZLjava/lang/String;)V

    .line 746
    sget-object v0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJobStatus;->ERROR:Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJobStatus;

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->changePrintJobStatus(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJobStatus;)V

    move v0, v1

    .line 747
    goto :goto_1

    .line 749
    :pswitch_2
    invoke-direct {p0, p1, v1, v4}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->broadcastDocumentState(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;ZLjava/lang/String;)V

    .line 750
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->openDownloadedFile(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)V

    .line 751
    const/4 v0, 0x1

    .line 752
    goto :goto_1

    .line 754
    :pswitch_3
    sget-object v0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJobStatus;->DONE:Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJobStatus;

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->changePrintJobStatus(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJobStatus;)V

    goto :goto_0

    .line 764
    :pswitch_4
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->handleReadyPageUrlJob(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)Z

    move-result v0

    goto :goto_2

    .line 767
    :pswitch_5
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->deletePageUrlJob(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)V

    .line 769
    invoke-direct {p0, p1, v1, v4}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->broadcastDocumentState(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;ZLjava/lang/String;)V

    goto :goto_2

    .line 740
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 762
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private executePendingWorkStoredInDatabase()V
    .locals 6

    .prologue
    .line 706
    invoke-direct {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->getAllDocumentsInStatesRequiringAction()Ljava/util/Set;

    move-result-object v1

    .line 707
    const/4 v0, 0x0

    .line 708
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;

    .line 709
    const-string/jumbo v3, "SnapshotArchiveManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "Handling actions for "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 710
    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->executePendingWorkForDocument(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)Z

    move-result v0

    or-int/2addr v0, v1

    move v1, v0

    .line 711
    goto :goto_0

    .line 712
    :cond_0
    if-eqz v1, :cond_1

    .line 713
    invoke-direct {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->startExecutePendingWorkIntent()V

    .line 715
    :cond_1
    return-void
.end method

.method private static generateContentValuesLog(Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 932
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 933
    const-string/jumbo v0, "{"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 934
    invoke-virtual {p0}, Landroid/content/ContentValues;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 935
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 936
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 937
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0, v0}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 938
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 939
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 940
    const-string/jumbo v3, ", "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 941
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0, v0}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 944
    :cond_0
    const-string/jumbo v0, "} for {"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 945
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 946
    const-string/jumbo v0, " : "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 947
    invoke-static {p2}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 948
    const-string/jumbo v0, "}"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 949
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getAllDocumentsInStatesRequiringAction()Ljava/util/Set;
    .locals 7

    .prologue
    .line 718
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->mProviderDelegate:Lcom/google/android/apps/chrome/snapshot/SnapshotProviderDelegate;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/chrome/snapshot/SnapshotProvider;->getSnapshotsUri(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/chrome/snapshot/SnapshotProvider;->FULL_PROJECTION:[Ljava/lang/String;

    const-string/jumbo v3, "state=? OR state=? OR state=? OR state=? OR pageUrlState=? OR pageUrlState=? "

    const/4 v4, 0x6

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    sget-object v6, Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;->DOWNLOAD_PENDING:Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;

    invoke-virtual {v6}, Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;->toValue()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    sget-object v6, Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;->UNABLE_TO_DOWNLOAD:Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;

    invoke-virtual {v6}, Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;->toValue()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x2

    sget-object v6, Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;->DOWNLOADED:Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;

    invoke-virtual {v6}, Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;->toValue()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x3

    sget-object v6, Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;->DOWNLOADED_AND_OPENED:Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;

    invoke-virtual {v6}, Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;->toValue()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x4

    sget-object v6, Lcom/google/android/apps/chrome/snapshot/PageUrlJobState;->READY:Lcom/google/android/apps/chrome/snapshot/PageUrlJobState;

    invoke-virtual {v6}, Lcom/google/android/apps/chrome/snapshot/PageUrlJobState;->toValue()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x5

    sget-object v6, Lcom/google/android/apps/chrome/snapshot/PageUrlJobState;->DELETION_PENDING:Lcom/google/android/apps/chrome/snapshot/PageUrlJobState;

    invoke-virtual {v6}, Lcom/google/android/apps/chrome/snapshot/PageUrlJobState;->toValue()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x0

    invoke-interface/range {v0 .. v5}, Lcom/google/android/apps/chrome/snapshot/SnapshotProviderDelegate;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public static getCurrentLocalDocuments(Landroid/content/Context;)Ljava/util/Set;
    .locals 1

    .prologue
    .line 953
    invoke-static {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->getProviderDelegateInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/snapshot/SnapshotProviderDelegate;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->getCurrentLocalDocuments(Landroid/content/Context;Lcom/google/android/apps/chrome/snapshot/SnapshotProviderDelegate;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method private static getCurrentLocalDocuments(Landroid/content/Context;Lcom/google/android/apps/chrome/snapshot/SnapshotProviderDelegate;)Ljava/util/Set;
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 958
    invoke-static {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotProvider;->getSnapshotsUri(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/chrome/snapshot/SnapshotProvider;->FULL_PROJECTION:[Ljava/lang/String;

    move-object v0, p1

    move-object v4, v3

    move-object v5, v3

    invoke-interface/range {v0 .. v5}, Lcom/google/android/apps/chrome/snapshot/SnapshotProviderDelegate;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method private static getDocumentsWithSnapshotId(Landroid/content/Context;Lcom/google/android/apps/chrome/snapshot/SnapshotProviderDelegate;Ljava/lang/String;)Ljava/util/Set;
    .locals 6

    .prologue
    .line 607
    invoke-static {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotProvider;->getSnapshotsUri(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/chrome/snapshot/SnapshotProvider;->FULL_PROJECTION:[Ljava/lang/String;

    const-string/jumbo v3, "snapshotId=? "

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object p2, v4, v0

    const/4 v5, 0x0

    move-object v0, p1

    invoke-interface/range {v0 .. v5}, Lcom/google/android/apps/chrome/snapshot/SnapshotProviderDelegate;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method private getDownloadedErrorMessage(I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 862
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget v1, Lcom/google/android/apps/chrome/R$string;->snapshot_cannot_be_downloaded:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getOpenedErrorMessage(I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 866
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget v1, Lcom/google/android/apps/chrome/R$string;->snapshot_cannot_be_opened:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static getProviderDelegateInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/snapshot/SnapshotProviderDelegate;
    .locals 2

    .prologue
    .line 239
    sget-object v0, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->sProviderDelegate:Lcom/google/android/apps/chrome/snapshot/DefaultSnapshotProviderDelegate;

    if-nez v0, :cond_0

    .line 240
    new-instance v0, Lcom/google/android/apps/chrome/snapshot/DefaultSnapshotProviderDelegate;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/chrome/snapshot/DefaultSnapshotProviderDelegate;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->sProviderDelegate:Lcom/google/android/apps/chrome/snapshot/DefaultSnapshotProviderDelegate;

    .line 243
    :cond_0
    sget-object v0, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->sProviderDelegate:Lcom/google/android/apps/chrome/snapshot/DefaultSnapshotProviderDelegate;

    return-object v0
.end method

.method public static getSnapshotViewableState(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)Lcom/google/android/apps/chrome/snapshot/SnapshotViewableState;
    .locals 2

    .prologue
    .line 1075
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->getState()Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;

    move-result-object v0

    .line 1076
    if-nez v0, :cond_0

    .line 1080
    sget-object v0, Lcom/google/android/apps/chrome/snapshot/SnapshotViewableState;->DOWNLOADING:Lcom/google/android/apps/chrome/snapshot/SnapshotViewableState;

    .line 1095
    :goto_0
    return-object v0

    .line 1082
    :cond_0
    sget-object v1, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager$1;->$SwitchMap$com$google$android$apps$chrome$snapshot$SnapshotJobState:[I

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 1095
    sget-object v0, Lcom/google/android/apps/chrome/snapshot/SnapshotViewableState;->UNKNOWN:Lcom/google/android/apps/chrome/snapshot/SnapshotViewableState;

    goto :goto_0

    .line 1085
    :pswitch_0
    sget-object v0, Lcom/google/android/apps/chrome/snapshot/SnapshotViewableState;->DOWNLOADING:Lcom/google/android/apps/chrome/snapshot/SnapshotViewableState;

    goto :goto_0

    .line 1088
    :pswitch_1
    sget-object v0, Lcom/google/android/apps/chrome/snapshot/SnapshotViewableState;->ERROR:Lcom/google/android/apps/chrome/snapshot/SnapshotViewableState;

    goto :goto_0

    .line 1092
    :pswitch_2
    sget-object v0, Lcom/google/android/apps/chrome/snapshot/SnapshotViewableState;->READY:Lcom/google/android/apps/chrome/snapshot/SnapshotViewableState;

    goto :goto_0

    .line 1082
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private handleDownloadFinished(Landroid/content/Intent;)V
    .locals 5

    .prologue
    const-wide/16 v2, -0x1

    .line 505
    const-string/jumbo v0, "downloadId"

    invoke-virtual {p1, v0, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    .line 506
    cmp-long v2, v0, v2

    if-nez v2, :cond_0

    .line 507
    const-string/jumbo v0, "SnapshotArchiveManager"

    const-string/jumbo v1, "Could not find download id from download finished intent"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 539
    :goto_0
    return-void

    .line 512
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->mDownloadDelegate:Lcom/google/android/apps/chrome/snapshot/SnapshotDownloadDelegate;

    invoke-interface {v2, v0, v1}, Lcom/google/android/apps/chrome/snapshot/SnapshotDownloadDelegate;->queryByDownloadId(J)Lcom/google/android/apps/chrome/snapshot/DownloadInfo;

    move-result-object v2

    .line 513
    if-nez v2, :cond_1

    .line 514
    const-string/jumbo v2, "SnapshotArchiveManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Failed to download file with downloadId = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ". Reason: Failed to query DownloadManager"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 516
    sget-object v2, Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;->UNABLE_TO_DOWNLOAD:Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;

    const/4 v3, 0x0

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->updateDatabaseWithNewSnapshotJobState(JLcom/google/android/apps/chrome/snapshot/SnapshotJobState;Landroid/content/ContentValues;)I

    goto :goto_0

    .line 520
    :cond_1
    invoke-virtual {v2}, Lcom/google/android/apps/chrome/snapshot/DownloadInfo;->getStatus()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 537
    invoke-direct {p0, v2}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->handleUnknownDownloadStatus(Lcom/google/android/apps/chrome/snapshot/DownloadInfo;)V

    goto :goto_0

    .line 522
    :sswitch_0
    invoke-direct {p0, v2}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->handleSuccessfulDownload(Lcom/google/android/apps/chrome/snapshot/DownloadInfo;)V

    goto :goto_0

    .line 525
    :sswitch_1
    invoke-direct {p0, v2}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->handleFailedDownload(Lcom/google/android/apps/chrome/snapshot/DownloadInfo;)V

    goto :goto_0

    .line 528
    :sswitch_2
    const-string/jumbo v0, "STATUS_PENDING"

    invoke-direct {p0, v2, v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->handleNonFinishedDownload(Lcom/google/android/apps/chrome/snapshot/DownloadInfo;Ljava/lang/String;)V

    goto :goto_0

    .line 531
    :sswitch_3
    const-string/jumbo v0, "STATUS_RUNNING"

    invoke-direct {p0, v2, v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->handleNonFinishedDownload(Lcom/google/android/apps/chrome/snapshot/DownloadInfo;Ljava/lang/String;)V

    goto :goto_0

    .line 534
    :sswitch_4
    const-string/jumbo v0, "STATUS_PAUSED"

    invoke-direct {p0, v2, v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->handleNonFinishedDownload(Lcom/google/android/apps/chrome/snapshot/DownloadInfo;Ljava/lang/String;)V

    goto :goto_0

    .line 520
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_2
        0x2 -> :sswitch_3
        0x4 -> :sswitch_4
        0x8 -> :sswitch_0
        0x10 -> :sswitch_1
    .end sparse-switch
.end method

.method private handleFailedDownload(Lcom/google/android/apps/chrome/snapshot/DownloadInfo;)V
    .locals 4

    .prologue
    .line 558
    const-string/jumbo v0, "SnapshotArchiveManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Failed to download file with downloadId = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/snapshot/DownloadInfo;->getDownloadId()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ". Reason: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/snapshot/DownloadInfo;->getReason()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ". placing job in error state."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 560
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/snapshot/DownloadInfo;->getDownloadId()J

    move-result-wide v0

    sget-object v2, Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;->UNABLE_TO_DOWNLOAD:Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;

    const/4 v3, 0x0

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->updateDatabaseWithNewSnapshotJobState(JLcom/google/android/apps/chrome/snapshot/SnapshotJobState;Landroid/content/ContentValues;)I

    .line 562
    return-void
.end method

.method private handleFetchPrintJobs()V
    .locals 1

    .prologue
    .line 409
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/snapshot/ChromeToMobileFeature;->isEnabled(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 410
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->createFetchIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 412
    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->startServiceWithWakeLockInternal(Landroid/content/Intent;)V

    .line 414
    :cond_0
    return-void
.end method

.method private handleInitialization()V
    .locals 3

    .prologue
    .line 291
    invoke-direct {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->migrateToVersion2IfNeeded()V

    .line 294
    invoke-direct {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->updateProtocolVersionFieldIfNeeded()V

    .line 300
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/snapshot/ChromeToMobileFeature;->updateCachedNewProfileManagement(Landroid/content/Context;)V

    .line 303
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/sync/signin/ChromeSigninController;->get(Landroid/content/Context;)Lorg/chromium/sync/signin/ChromeSigninController;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/sync/signin/ChromeSigninController;->getSignedInUser()Landroid/accounts/Account;

    move-result-object v0

    .line 304
    if-nez v0, :cond_1

    .line 322
    :cond_0
    :goto_0
    return-void

    .line 307
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->mAuthTokenHelper:Lcom/google/android/apps/chrome/snapshot/cloudprint/AuthTokenHelper;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->createInitializeIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/chrome/snapshot/cloudprint/AuthTokenHelper;->acquireAuthToken(Landroid/accounts/Account;Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 314
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->isUpdatedLastWeek()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->isStoredApplicationVersionUpToDate()Z

    move-result v0

    if-nez v0, :cond_3

    .line 315
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->setNeedsUpdating(Landroid/content/Context;Z)V

    .line 318
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->needsUpdating(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 319
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/snapshot/ChromeToMobileFeature;->isEnabled(Landroid/content/Context;)Z

    move-result v0

    .line 320
    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->refreshCloudPrintRegistrationState(Z)V

    goto :goto_0
.end method

.method private handleJobDeleted(Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 493
    const-string/jumbo v0, "jobId"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 494
    sget-object v1, Lcom/google/android/apps/chrome/snapshot/PageUrlJobState;->FINISHED:Lcom/google/android/apps/chrome/snapshot/PageUrlJobState;

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->updateDatabaseWithNewPageUrlJobState(Ljava/lang/String;Lcom/google/android/apps/chrome/snapshot/PageUrlJobState;Landroid/content/ContentValues;)I

    .line 495
    return-void
.end method

.method private handleNonFinishedDownload(Lcom/google/android/apps/chrome/snapshot/DownloadInfo;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 565
    const-string/jumbo v0, "SnapshotArchiveManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Got event for download finished for downloadId = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/snapshot/DownloadInfo;->getDownloadId()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", but status is currently: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ". Assuming an error occurred."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 568
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/snapshot/DownloadInfo;->getDownloadId()J

    move-result-wide v0

    sget-object v2, Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;->UNABLE_TO_DOWNLOAD:Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;

    const/4 v3, 0x0

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->updateDatabaseWithNewSnapshotJobState(JLcom/google/android/apps/chrome/snapshot/SnapshotJobState;Landroid/content/ContentValues;)I

    .line 570
    return-void
.end method

.method private handlePrintJobFetchResult(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 425
    const-string/jumbo v0, "printJobs"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 426
    if-nez v0, :cond_0

    .line 427
    const-string/jumbo v0, "SnapshotArchiveManager"

    const-string/jumbo v1, "Failed find printJobs in update intent"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 431
    :goto_0
    return-void

    .line 430
    :cond_0
    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->updateLocalDatabaseBasedOnPrintJobs(Ljava/util/Collection;)V

    goto :goto_0
.end method

.method private handlePrintJobFromC2DMNotification(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 445
    invoke-direct {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->migrateToVersion2IfNeeded()V

    .line 446
    const-string/jumbo v0, "printJob"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob;

    .line 447
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 448
    invoke-virtual {v1, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 449
    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->updateLocalDatabaseBasedOnPrintJobs(Ljava/util/Collection;)V

    .line 450
    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->schedulePrintJobsFetchIfNeeded(Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob;)V

    .line 451
    return-void
.end method

.method private handlePrintJobStatusChanged(Landroid/content/Intent;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 463
    const-string/jumbo v0, "jobId"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 464
    const-string/jumbo v0, "printJobStatus"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 465
    invoke-static {v2}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJobStatus;->parsePrintJobStatus(Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJobStatus;

    move-result-object v0

    .line 467
    if-nez v0, :cond_0

    .line 468
    const-string/jumbo v0, "SnapshotArchiveManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Unable to parse print job status from "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ", assuming DONE"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 470
    sget-object v0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJobStatus;->DONE:Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJobStatus;

    .line 472
    :cond_0
    sget-object v2, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager$1;->$SwitchMap$com$google$android$apps$chrome$snapshot$cloudprint$PrintJobStatus:[I

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJobStatus;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 480
    const-string/jumbo v2, "SnapshotArchiveManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Do not know what do with with print job status = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v3, " for jobid = "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 483
    :goto_0
    return-void

    .line 474
    :pswitch_0
    sget-object v0, Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;->FINISHED:Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;

    invoke-direct {p0, v1, v0, v5}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->updateDatabaseWithNewSnapshotJobState(Ljava/lang/String;Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;Landroid/content/ContentValues;)I

    goto :goto_0

    .line 477
    :pswitch_1
    sget-object v0, Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;->ERROR:Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;

    invoke-direct {p0, v1, v0, v5}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->updateDatabaseWithNewSnapshotJobState(Ljava/lang/String;Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;Landroid/content/ContentValues;)I

    goto :goto_0

    .line 472
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private handleQueryState(Landroid/content/Intent;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 589
    const-string/jumbo v0, "Chrome_SnapshotID"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 591
    if-nez v0, :cond_1

    .line 603
    :cond_0
    :goto_0
    return-void

    .line 592
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->mProviderDelegate:Lcom/google/android/apps/chrome/snapshot/SnapshotProviderDelegate;

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->getDocumentsWithSnapshotId(Landroid/content/Context;Lcom/google/android/apps/chrome/snapshot/SnapshotProviderDelegate;Ljava/lang/String;)Ljava/util/Set;

    move-result-object v1

    .line 594
    invoke-interface {v1}, Ljava/util/Set;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 595
    const-string/jumbo v1, "SnapshotArchiveManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Document with SnapshotID = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, " not found"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 597
    :cond_2
    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v2

    if-le v2, v5, :cond_3

    .line 598
    const-string/jumbo v2, "SnapshotArchiveManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Found "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " documents with SnapshotID = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 600
    :cond_3
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;

    .line 601
    invoke-direct {p0, v0, v5}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->broadcastDocumentState(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;Z)V

    goto :goto_1
.end method

.method private handleReadyPageUrlJob(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 788
    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v2, "android.intent.action.VIEW"

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->getPageUri()Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 789
    const-string/jumbo v2, "create_new_tab"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 790
    const/high16 v2, 0x10000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 791
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->getSnapshotId()Ljava/lang/String;

    move-result-object v2

    .line 792
    const-string/jumbo v3, "SnapshotArchiveManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "Opening URL "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->getPageUri()Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " for snapshot id "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 794
    if-eqz v2, :cond_0

    .line 795
    const-string/jumbo v3, "Chrome_SnapshotID"

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 797
    :cond_0
    sget-object v2, Lcom/google/android/apps/chrome/snapshot/PageUrlJobState;->DELETION_PENDING:Lcom/google/android/apps/chrome/snapshot/PageUrlJobState;

    const/4 v3, 0x0

    invoke-direct {p0, p1, v2, v3}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->updateDatabaseWithNewPageUrlJobState(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;Lcom/google/android/apps/chrome/snapshot/PageUrlJobState;Landroid/content/ContentValues;)I

    move-result v2

    .line 800
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/google/android/apps/chrome/IntentHandler;->startActivityForTrustedIntent(Landroid/content/Intent;Landroid/content/Context;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 804
    :goto_0
    if-ne v2, v0, :cond_1

    :goto_1
    return v0

    .line 802
    :catch_0
    move-exception v1

    const-string/jumbo v1, "SnapshotArchiveManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Unable to launch activity for uri: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->getPageUri()Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 804
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private handleSetEnabled(Landroid/content/Intent;)V
    .locals 4

    .prologue
    .line 386
    const-string/jumbo v0, "com.google.android.apps.chrome.snapshot.SnapshotArchiveManager"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 387
    const-string/jumbo v1, "enabled"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    .line 390
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/apps/chrome/snapshot/ChromeToMobileFeature;->setEnabled(Landroid/content/Context;Z)Z

    move-result v0

    .line 391
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-wide/16 v2, 0x3

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->setVersion(Landroid/content/Context;J)V

    .line 392
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->setNeedsUpdating(Landroid/content/Context;Z)V

    .line 393
    if-nez v0, :cond_0

    .line 394
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->mAuthTokenHelper:Lcom/google/android/apps/chrome/snapshot/cloudprint/AuthTokenHelper;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->createInitializeIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/snapshot/cloudprint/AuthTokenHelper;->stopAcquiringAuthToken(Landroid/content/Intent;)V

    .line 397
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->createInitializeIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 399
    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->startServiceWithWakeLockInternal(Landroid/content/Intent;)V

    .line 400
    return-void
.end method

.method private handleSuccessfulDownload(Lcom/google/android/apps/chrome/snapshot/DownloadInfo;)V
    .locals 4

    .prologue
    .line 542
    const-string/jumbo v0, "SnapshotArchiveManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Successfully downloaded file with downloadId = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/snapshot/DownloadInfo;->getDownloadId()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 544
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->mDownloadDelegate:Lcom/google/android/apps/chrome/snapshot/SnapshotDownloadDelegate;

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/snapshot/DownloadInfo;->getDownloadId()J

    move-result-wide v2

    invoke-interface {v0, v2, v3}, Lcom/google/android/apps/chrome/snapshot/SnapshotDownloadDelegate;->getUriForDownloadedFile(J)Landroid/net/Uri;

    move-result-object v0

    .line 545
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 546
    const-string/jumbo v2, "localUri"

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 547
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/snapshot/DownloadInfo;->getMediaType()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 548
    const-string/jumbo v0, "SnapshotArchiveManager"

    const-string/jumbo v2, "Unable to get media type from successful download."

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 553
    :goto_1
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/snapshot/DownloadInfo;->getDownloadId()J

    move-result-wide v2

    sget-object v0, Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;->DOWNLOADED:Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;

    invoke-direct {p0, v2, v3, v0, v1}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->updateDatabaseWithNewSnapshotJobState(JLcom/google/android/apps/chrome/snapshot/SnapshotJobState;Landroid/content/ContentValues;)I

    .line 555
    return-void

    .line 546
    :cond_0
    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 550
    :cond_1
    const-string/jumbo v0, "SnapshotArchiveManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Updating DB with downloaded media type = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/snapshot/DownloadInfo;->getMediaType()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 551
    const-string/jumbo v0, "mimeType"

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/snapshot/DownloadInfo;->getMediaType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private handleUnknownDownloadStatus(Lcom/google/android/apps/chrome/snapshot/DownloadInfo;)V
    .locals 4

    .prologue
    .line 573
    const-string/jumbo v0, "SnapshotArchiveManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Got event for download finished for downloadId = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/snapshot/DownloadInfo;->getDownloadId()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", but status is unknown: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/snapshot/DownloadInfo;->getStatus()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ". Assuming an error occurred."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 577
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/snapshot/DownloadInfo;->getDownloadId()J

    move-result-wide v0

    sget-object v2, Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;->UNABLE_TO_DOWNLOAD:Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;

    const/4 v3, 0x0

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->updateDatabaseWithNewSnapshotJobState(JLcom/google/android/apps/chrome/snapshot/SnapshotJobState;Landroid/content/ContentValues;)I

    .line 579
    return-void
.end method

.method public static isPrintedDocument(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)Z
    .locals 1

    .prologue
    .line 1254
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->getSnapshotId()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->getJobId()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isValidMediaState(ZLjava/lang/String;I)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v2, -0x1

    .line 1204
    if-eqz p1, :cond_2

    .line 1205
    if-ne p3, v2, :cond_1

    .line 1207
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 1205
    goto :goto_0

    .line 1207
    :cond_2
    if-eq p3, v2, :cond_0

    const-string/jumbo v2, "mounted_ro"

    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method private static logChanges(Ljava/util/Map;)V
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 660
    .line 662
    invoke-interface {p0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    move v2, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/snapshot/SnapshotState;

    .line 663
    sget-object v4, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager$1;->$SwitchMap$com$google$android$apps$chrome$snapshot$SnapshotState:[I

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotState;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 671
    const-string/jumbo v4, "SnapshotArchiveManager"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "Unknown SnapshotState: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 665
    :pswitch_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    .line 666
    goto :goto_0

    .line 668
    :pswitch_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 669
    goto :goto_0

    .line 674
    :cond_0
    const-string/jumbo v0, "SnapshotArchiveManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Updating database with changes: created = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ", updated = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 676
    return-void

    .line 663
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private makeSurePathIsCreated()V
    .locals 4

    .prologue
    .line 1144
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Landroid/os/Environment;->DIRECTORY_DOWNLOADS:Ljava/lang/String;

    invoke-static {v2}, Landroid/os/Environment;->getExternalStoragePublicDirectory(Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/google/android/apps/chrome/R$string;->snapshot_external_public_directory_sub_path:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1147
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1148
    const-string/jumbo v1, "SnapshotArchiveManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Trying to create folder for downloads: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1149
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1150
    new-instance v1, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager$ExternalStorageException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Unable to create folder for offline copies: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget v2, Lcom/google/android/apps/chrome/R$string;->snapshot_external_storage_unable_to_create_folder:I

    invoke-direct {v1, v0, v2}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager$ExternalStorageException;-><init>(Ljava/lang/String;I)V

    throw v1

    .line 1155
    :cond_0
    return-void
.end method

.method private migrateToVersion2IfNeeded()V
    .locals 2

    .prologue
    .line 615
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettingsMigrator;->needsMigration(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 617
    new-instance v0, Lcom/google/android/apps/chrome/snapshot/SnapshotSettingsMigrator;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettingsMigrator;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettingsMigrator;->migrate()V

    .line 621
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->setNeedsUpdating(Landroid/content/Context;Z)V

    .line 623
    :cond_0
    return-void
.end method

.method private openDownloadedFile(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)V
    .locals 7

    .prologue
    const/4 v3, 0x0

    const/4 v6, 0x0

    .line 987
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->mSupportedMimeTypes:Ljava/util/HashSet;

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->getMimeType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 988
    const-string/jumbo v0, "SnapshotArchiveManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Not opening document of non supported mime type ("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->getMimeType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "). Setting to error state."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 990
    sget-object v0, Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;->UNABLE_TO_DOWNLOAD:Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;

    invoke-direct {p0, p1, v0, v3}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->updateDatabaseWithNewSnapshotJobState(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;Landroid/content/ContentValues;)I

    .line 1025
    :cond_0
    :goto_0
    return-void

    .line 995
    :cond_1
    sget-object v0, Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;->DOWNLOADED_AND_OPENED:Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;

    invoke-direct {p0, p1, v0, v3}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->updateDatabaseWithNewSnapshotJobState(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;Landroid/content/ContentValues;)I

    .line 998
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->getLocalUri()Landroid/net/Uri;

    move-result-object v0

    if-nez v0, :cond_3

    .line 999
    const-string/jumbo v0, "SnapshotArchiveManager"

    const-string/jumbo v1, "Unable to find local URI for downloaded file. Trying to fetch URI again from DownloadManager"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1001
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->mDownloadDelegate:Lcom/google/android/apps/chrome/snapshot/SnapshotDownloadDelegate;

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->getDownloadId()J

    move-result-wide v2

    invoke-interface {v0, v2, v3}, Lcom/google/android/apps/chrome/snapshot/SnapshotDownloadDelegate;->getUriForDownloadedFile(J)Landroid/net/Uri;

    move-result-object v1

    .line 1003
    if-eqz v1, :cond_2

    .line 1004
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->builder()Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->withLocalUri(Landroid/net/Uri;)Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument$Builder;->build()Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;

    move-result-object v0

    .line 1005
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 1006
    const-string/jumbo v3, "localUri"

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1007
    const-string/jumbo v1, "downloadId=?"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->getDownloadId()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-direct {p0, v2, v1, v3}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->updateDatabase(Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1014
    :goto_1
    const-string/jumbo v1, "multipart/related"

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->getMimeType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1019
    const/4 v1, 0x0

    :try_start_0
    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->verifyMediaState(Z)V
    :try_end_0
    .catch Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager$ExternalStorageException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1024
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->openDownloadedFileAsNewTaskActivity(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;Landroid/content/Context;)V

    goto :goto_0

    .line 1010
    :cond_2
    const-string/jumbo v0, "SnapshotArchiveManager"

    const-string/jumbo v1, "Unable to find local URI for downloaded file. Giving up."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1020
    :catch_0
    move-exception v0

    .line 1021
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager$ExternalStorageException;->getResourceId()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->getOpenedErrorMessage(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->displayAndroidNotification(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    move-object v0, p1

    goto :goto_1
.end method

.method public static openDownloadedFileAsNewTaskActivity(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1029
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1030
    const-string/jumbo v1, "android.intent.action.VIEW"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1031
    const-string/jumbo v1, "create_new_tab"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1032
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->getLocalUri()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->getMimeType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 1033
    invoke-static {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->isPrintedDocument(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1035
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1038
    :cond_0
    :try_start_0
    invoke-static {v0, p1}, Lcom/google/android/apps/chrome/IntentHandler;->startActivityForTrustedIntent(Landroid/content/Intent;Landroid/content/Context;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1047
    :goto_0
    return-void

    .line 1040
    :catch_0
    move-exception v0

    const-string/jumbo v0, "SnapshotArchiveManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Unable to open activity for document { mime_type = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->getMimeType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", uri = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->getLocalUri()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "}"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1042
    sget v0, Lcom/google/android/apps/chrome/R$string;->snapshot_error_opening_mime_type:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-array v1, v3, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->getMimeType()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1045
    invoke-static {p0, v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->displayAndroidNotification(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private schedulePrintJobsFetchIfNeeded(Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob;)V
    .locals 2

    .prologue
    .line 679
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob;->hasJobData()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 680
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob;->getJobData()Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob$JobData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob$JobData;->getType()Ljava/lang/String;

    move-result-object v0

    .line 681
    const-string/jumbo v1, "url_with_delayed_snapshot"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 692
    :goto_0
    return-void

    .line 687
    :cond_0
    const-string/jumbo v0, "dry_run_job"

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob;->getJobId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 688
    const-string/jumbo v0, "SnapshotArchiveManager"

    const-string/jumbo v1, "Got ping from Cloud Print servers. Scheduling fetch request."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 690
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->createFetchPrintJobsIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 691
    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->startServiceWithWakeLockInternal(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private setupDownloadEnvironment(Lcom/google/android/apps/chrome/snapshot/DownloadManagerRequestData;Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)V
    .locals 3

    .prologue
    .line 1111
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->verifyMediaState(Z)V

    .line 1112
    invoke-direct {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->makeSurePathIsCreated()V

    .line 1113
    sget-object v0, Landroid/os/Environment;->DIRECTORY_DOWNLOADS:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget v2, Lcom/google/android/apps/chrome/R$string;->snapshot_external_public_directory_sub_path:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p2}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->createSlugFilename(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/android/apps/chrome/snapshot/DownloadManagerRequestData;->setDestinationInExternalPublicDir(Ljava/lang/String;Ljava/lang/String;)V

    .line 1116
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/snapshot/DownloadManagerRequestData;->allowScanningByMediaScanner()V

    .line 1117
    return-void
.end method

.method private setupExternalStorageErrorMap()V
    .locals 3

    .prologue
    .line 202
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->mExternalStorageErrorMap:Ljava/util/Map;

    const-string/jumbo v1, "mounted"

    const/4 v2, -0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 203
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->mExternalStorageErrorMap:Ljava/util/Map;

    const-string/jumbo v1, "mounted_ro"

    sget v2, Lcom/google/android/apps/chrome/R$string;->snapshot_external_storage_read_only:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 205
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->mExternalStorageErrorMap:Ljava/util/Map;

    const-string/jumbo v1, "bad_removal"

    sget v2, Lcom/google/android/apps/chrome/R$string;->snapshot_external_storage_bad_removal:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 207
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->mExternalStorageErrorMap:Ljava/util/Map;

    const-string/jumbo v1, "checking"

    sget v2, Lcom/google/android/apps/chrome/R$string;->snapshot_external_storage_checking:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 209
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->mExternalStorageErrorMap:Ljava/util/Map;

    const-string/jumbo v1, "nofs"

    sget v2, Lcom/google/android/apps/chrome/R$string;->snapshot_external_storage_nofs:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 211
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->mExternalStorageErrorMap:Ljava/util/Map;

    const-string/jumbo v1, "removed"

    sget v2, Lcom/google/android/apps/chrome/R$string;->snapshot_external_storage_removed:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 213
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->mExternalStorageErrorMap:Ljava/util/Map;

    const-string/jumbo v1, "shared"

    sget v2, Lcom/google/android/apps/chrome/R$string;->snapshot_external_storage_shared:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 215
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->mExternalStorageErrorMap:Ljava/util/Map;

    const-string/jumbo v1, "unmountable"

    sget v2, Lcom/google/android/apps/chrome/R$string;->snapshot_external_storage_unmountable:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 217
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->mExternalStorageErrorMap:Ljava/util/Map;

    const-string/jumbo v1, "unmounted"

    sget v2, Lcom/google/android/apps/chrome/R$string;->snapshot_external_storage_unmounted:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 219
    return-void
.end method

.method private startExecutePendingWorkIntent()V
    .locals 1

    .prologue
    .line 637
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->createExecutePendingWorkIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 638
    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->startServiceWithWakeLockInternal(Landroid/content/Intent;)V

    .line 639
    return-void
.end method

.method private updateDatabase(Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 4

    .prologue
    .line 917
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->mProviderDelegate:Lcom/google/android/apps/chrome/snapshot/SnapshotProviderDelegate;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/chrome/snapshot/SnapshotProvider;->getSnapshotsUri(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v1

    invoke-interface {v0, v1, p1, p2, p3}, Lcom/google/android/apps/chrome/snapshot/SnapshotProviderDelegate;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 920
    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 921
    const-string/jumbo v1, "SnapshotArchiveManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Set "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1, p2, p3}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->generateContentValuesLog(Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 927
    :goto_0
    return v0

    .line 923
    :cond_0
    const-string/jumbo v1, "SnapshotArchiveManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Error setting "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1, p2, p3}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->generateContentValuesLog(Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ": Changed rows = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private updateDatabaseWithChanges(Ljava/util/Map;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 963
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 964
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;

    .line 965
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/snapshot/SnapshotState;

    .line 966
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/chrome/snapshot/SnapshotProvider;->getSnapshotsIdBaseUri(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->getId()I

    move-result v4

    int-to-long v4, v4

    invoke-static {v3, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    .line 969
    invoke-static {v1}, Lcom/google/android/apps/chrome/snapshot/SnapshotProvider;->createContentValues(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)Landroid/content/ContentValues;

    move-result-object v4

    .line 970
    sget-object v5, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager$1;->$SwitchMap$com$google$android$apps$chrome$snapshot$SnapshotState:[I

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotState;->ordinal()I

    move-result v0

    aget v0, v5, v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 972
    :pswitch_0
    const-string/jumbo v0, "SnapshotArchiveManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "Got created document: "

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 973
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->mProviderDelegate:Lcom/google/android/apps/chrome/snapshot/SnapshotProviderDelegate;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/chrome/snapshot/SnapshotProvider;->getSnapshotsUri(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v3

    invoke-interface {v0, v3, v4}, Lcom/google/android/apps/chrome/snapshot/SnapshotProviderDelegate;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v0

    .line 976
    invoke-static {v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotProvider;->getDatabaseId(Landroid/net/Uri;)I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->setId(I)V

    goto :goto_0

    .line 979
    :pswitch_1
    const-string/jumbo v0, "SnapshotArchiveManager"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "Got updated document: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 980
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->mProviderDelegate:Lcom/google/android/apps/chrome/snapshot/SnapshotProviderDelegate;

    invoke-interface {v0, v3, v4, v7, v7}, Lcom/google/android/apps/chrome/snapshot/SnapshotProviderDelegate;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_0

    .line 984
    :cond_0
    return-void

    .line 970
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private updateDatabaseWithNewPageUrlJobState(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;Lcom/google/android/apps/chrome/snapshot/PageUrlJobState;Landroid/content/ContentValues;)I
    .locals 1

    .prologue
    .line 901
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->getPageUrlJobId()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p2, p3}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->updateDatabaseWithNewPageUrlJobState(Ljava/lang/String;Lcom/google/android/apps/chrome/snapshot/PageUrlJobState;Landroid/content/ContentValues;)I

    move-result v0

    return v0
.end method

.method private updateDatabaseWithNewPageUrlJobState(Ljava/lang/String;Lcom/google/android/apps/chrome/snapshot/PageUrlJobState;Landroid/content/ContentValues;)I
    .locals 4

    .prologue
    .line 907
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 908
    const-string/jumbo v1, "pageUrlState"

    invoke-virtual {p2}, Lcom/google/android/apps/chrome/snapshot/PageUrlJobState;->toValue()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 909
    if-eqz p3, :cond_0

    .line 910
    invoke-virtual {v0, p3}, Landroid/content/ContentValues;->putAll(Landroid/content/ContentValues;)V

    .line 912
    :cond_0
    const-string/jumbo v1, "pageUrlJobId=?"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->updateDatabase(Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method private updateDatabaseWithNewSnapshotJobState(JLcom/google/android/apps/chrome/snapshot/SnapshotJobState;Landroid/content/ContentValues;)I
    .locals 5

    .prologue
    .line 884
    invoke-static {p3, p4}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->createSnapshotJobStateContentValues(Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;Landroid/content/ContentValues;)Landroid/content/ContentValues;

    move-result-object v0

    .line 885
    const-string/jumbo v1, "downloadId=?"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->updateDatabase(Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method private updateDatabaseWithNewSnapshotJobState(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;Landroid/content/ContentValues;)I
    .locals 1

    .prologue
    .line 871
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->getJobId()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p2, p3}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->updateDatabaseWithNewSnapshotJobState(Ljava/lang/String;Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;Landroid/content/ContentValues;)I

    move-result v0

    return v0
.end method

.method private updateDatabaseWithNewSnapshotJobState(Ljava/lang/String;Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;Landroid/content/ContentValues;)I
    .locals 4

    .prologue
    .line 877
    invoke-static {p2, p3}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->createSnapshotJobStateContentValues(Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;Landroid/content/ContentValues;)Landroid/content/ContentValues;

    move-result-object v0

    .line 878
    const-string/jumbo v1, "jobId=?"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->updateDatabase(Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method private updateDownloadRequestFromDocument(Lcom/google/android/apps/chrome/snapshot/DownloadManagerRequestData;Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)V
    .locals 2

    .prologue
    .line 1166
    if-nez p2, :cond_0

    .line 1180
    :goto_0
    return-void

    .line 1169
    :cond_0
    invoke-virtual {p2}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->getTitle()Ljava/lang/String;

    move-result-object v0

    .line 1170
    if-nez v0, :cond_1

    .line 1171
    sget v0, Lcom/google/android/apps/chrome/R$string;->snapshot_download_default_title:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/apps/chrome/snapshot/DownloadManagerRequestData;->setTitle(Ljava/lang/String;)V

    .line 1175
    :goto_1
    const-string/jumbo v0, "application/pdf"

    invoke-virtual {p2}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->getMimeType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1176
    sget v0, Lcom/google/android/apps/chrome/R$string;->snapshot_download_description_print:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/apps/chrome/snapshot/DownloadManagerRequestData;->setDescription(Ljava/lang/String;)V

    goto :goto_0

    .line 1173
    :cond_1
    invoke-virtual {p1, v0}, Lcom/google/android/apps/chrome/snapshot/DownloadManagerRequestData;->setTitle(Ljava/lang/String;)V

    goto :goto_1

    .line 1178
    :cond_2
    sget v0, Lcom/google/android/apps/chrome/R$string;->snapshot_download_description_default:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/apps/chrome/snapshot/DownloadManagerRequestData;->setDescription(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private updateLocalDatabaseBasedOnPrintJobs(Ljava/util/Collection;)V
    .locals 2

    .prologue
    .line 649
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->mProviderDelegate:Lcom/google/android/apps/chrome/snapshot/SnapshotProviderDelegate;

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->getCurrentLocalDocuments(Landroid/content/Context;Lcom/google/android/apps/chrome/snapshot/SnapshotProviderDelegate;)Ljava/util/Set;

    move-result-object v0

    .line 651
    iget-object v1, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->mDatabaseChangeCalculator:Lcom/google/android/apps/chrome/snapshot/DatabaseChangeCalculator;

    invoke-virtual {v1, v0, p1}, Lcom/google/android/apps/chrome/snapshot/DatabaseChangeCalculator;->calculateChanges(Ljava/util/Collection;Ljava/util/Collection;)Ljava/util/Map;

    move-result-object v0

    .line 653
    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 654
    invoke-static {v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->logChanges(Ljava/util/Map;)V

    .line 656
    :cond_0
    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->updateDatabaseWithChanges(Ljava/util/Map;)V

    .line 657
    return-void
.end method

.method private updateProtocolVersionFieldIfNeeded()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x3

    .line 626
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->getVersion(Landroid/content/Context;)J

    move-result-wide v0

    .line 627
    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 628
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v2, v3}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->setVersion(Landroid/content/Context;J)V

    .line 630
    :cond_0
    return-void
.end method

.method private verifyMediaState(Z)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1189
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v1

    .line 1190
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->mExternalStorageErrorMap:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1191
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->mExternalStorageErrorMap:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1192
    invoke-direct {p0, p1, v1, v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->isValidMediaState(ZLjava/lang/String;I)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1193
    new-instance v2, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager$ExternalStorageUnavailableException;

    invoke-direct {v2, v1, v0, v3}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager$ExternalStorageUnavailableException;-><init>(Ljava/lang/String;ILcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager$1;)V

    throw v2

    .line 1197
    :cond_0
    new-instance v0, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager$ExternalStorageUnavailableException;

    sget v2, Lcom/google/android/apps/chrome/R$string;->snapshot_external_storage_generic_error:I

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager$ExternalStorageUnavailableException;-><init>(Ljava/lang/String;ILcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager$1;)V

    throw v0

    .line 1195
    :cond_1
    return-void
.end method


# virtual methods
.method createAuthTokenHelper(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 248
    new-instance v0, Lcom/google/android/apps/chrome/snapshot/cloudprint/AuthTokenHelper;

    invoke-direct {v0, p1}, Lcom/google/android/apps/chrome/snapshot/cloudprint/AuthTokenHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->mAuthTokenHelper:Lcom/google/android/apps/chrome/snapshot/cloudprint/AuthTokenHelper;

    .line 249
    return-void
.end method

.method createProviderDelegate()Lcom/google/android/apps/chrome/snapshot/SnapshotProviderDelegate;
    .locals 2

    .prologue
    .line 231
    new-instance v0, Lcom/google/android/apps/chrome/snapshot/DefaultSnapshotProviderDelegate;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/chrome/snapshot/DefaultSnapshotProviderDelegate;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method protected getDownloadService()Lcom/google/android/apps/chrome/snapshot/SnapshotDownloadDelegate;
    .locals 2

    .prologue
    .line 1230
    new-instance v0, Lcom/google/android/apps/chrome/snapshot/DefaultSnapshotDownloadDelegate;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/chrome/snapshot/DefaultSnapshotDownloadDelegate;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public isStoredApplicationVersionUpToDate()Z
    .locals 2

    .prologue
    .line 352
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/base/BuildInfo;->getPackageVersionName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 353
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->getApplicationVersion(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 354
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isUpdatedLastWeek()Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 326
    new-instance v1, Landroid/text/format/Time;

    const-string/jumbo v2, "UTC"

    invoke-direct {v1, v2}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 327
    invoke-virtual {v1}, Landroid/text/format/Time;->setToNow()V

    .line 328
    invoke-virtual {v1, v0}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v2

    .line 331
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->getLastUpdatedTimestamp(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 332
    if-nez v1, :cond_1

    .line 348
    :cond_0
    :goto_0
    return v0

    .line 336
    :cond_1
    new-instance v4, Landroid/text/format/Time;

    const-string/jumbo v5, "UTC"

    invoke-direct {v4, v5}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 338
    :try_start_0
    invoke-virtual {v4, v1}, Landroid/text/format/Time;->parse3339(Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/util/TimeFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 344
    invoke-virtual {v4, v0}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v4

    .line 347
    sub-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(J)J

    move-result-wide v2

    .line 348
    const-wide/32 v4, 0x240c8400

    cmp-long v1, v2, v4

    if-gez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    .line 340
    :catch_0
    move-exception v1

    const-string/jumbo v1, "SnapshotArchiveManager"

    const-string/jumbo v2, "Failed to read snapshot settings mtime, defaulting to null"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 341
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->setLastUpdatedTimestamp(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 223
    invoke-super {p0}, Lcom/google/android/apps/chrome/snapshot/IntentServiceWithWakeLock;->onCreate()V

    .line 224
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->getDownloadService()Lcom/google/android/apps/chrome/snapshot/SnapshotDownloadDelegate;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->mDownloadDelegate:Lcom/google/android/apps/chrome/snapshot/SnapshotDownloadDelegate;

    .line 225
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->createProviderDelegate()Lcom/google/android/apps/chrome/snapshot/SnapshotProviderDelegate;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->mProviderDelegate:Lcom/google/android/apps/chrome/snapshot/SnapshotProviderDelegate;

    .line 226
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->createAuthTokenHelper(Landroid/content/Context;)V

    .line 227
    return-void
.end method

.method protected onHandleIntentWithWakeLock(Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 254
    const-string/jumbo v0, "com.google.android.apps.chrome.snapshot.ACTION_INITIALIZE"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 255
    invoke-direct {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->handleInitialization()V

    .line 279
    :goto_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->executePendingWorkStoredInDatabase()V

    .line 280
    :goto_1
    return-void

    .line 256
    :cond_0
    const-string/jumbo v0, "com.google.android.apps.chrome.snapshot.ACTION_SET_ENABLED"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 257
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->handleSetEnabled(Landroid/content/Intent;)V

    goto :goto_0

    .line 258
    :cond_1
    const-string/jumbo v0, "com.google.android.apps.chrome.snapshot.ACTION_FETCH"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 259
    invoke-direct {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->handleFetchPrintJobs()V

    goto :goto_0

    .line 260
    :cond_2
    const-string/jumbo v0, "com.google.android.apps.chrome.snapshot.ACTION_FETCH_RESULT"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 261
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->handlePrintJobFetchResult(Landroid/content/Intent;)V

    goto :goto_0

    .line 262
    :cond_3
    const-string/jumbo v0, "com.google.android.apps.chrome.snapshot.ACTION_C2DM_NOTIFICATION"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 263
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->handlePrintJobFromC2DMNotification(Landroid/content/Intent;)V

    goto :goto_0

    .line 264
    :cond_4
    const-string/jumbo v0, "com.google.android.apps.chrome.snapshot.ACTION_JOB_STATUS_CHANGED"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 265
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->handlePrintJobStatusChanged(Landroid/content/Intent;)V

    goto :goto_0

    .line 266
    :cond_5
    const-string/jumbo v0, "com.google.android.apps.chrome.snapshot.ACTION_JOB_DELETED"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 267
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->handleJobDeleted(Landroid/content/Intent;)V

    goto :goto_0

    .line 268
    :cond_6
    const-string/jumbo v0, "com.google.android.apps.chrome.snapshot.ACTION_DOWNLOAD_FINISHED"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 269
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->handleDownloadFinished(Landroid/content/Intent;)V

    goto :goto_0

    .line 270
    :cond_7
    const-string/jumbo v0, "com.google.android.apps.chrome.snapshot.ACTION_QUERY_STATE"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 271
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->handleQueryState(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 272
    :cond_8
    const-string/jumbo v0, "com.google.android.apps.chrome.snapshot.ACTION_EXECUTE_PENDING_WORK"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 273
    const-string/jumbo v0, "SnapshotArchiveManager"

    const-string/jumbo v1, "Executing pending work stored in database"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 275
    :cond_9
    const-string/jumbo v0, "SnapshotArchiveManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Dropping unknown action from intent: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1
.end method

.method refreshCloudPrintRegistrationState(Z)V
    .locals 3

    .prologue
    .line 364
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->getAuthToken(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 365
    const-string/jumbo v1, "SnapshotArchiveManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v0, "Unable to change Cloud Print registration state for Chrome to Mobile since no auth token was found. Not "

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-eqz p1, :cond_0

    const-string/jumbo v0, "registering"

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, " with Cloud Print servers."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 373
    :goto_1
    return-void

    .line 365
    :cond_0
    const-string/jumbo v0, "unregistering"

    goto :goto_0

    .line 369
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintService;->createSyncStateWithServerIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 371
    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->startServiceWithWakeLockInternal(Landroid/content/Intent;)V

    goto :goto_1
.end method

.method protected startServiceWithWakeLockInternal(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 1071
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager;->startServiceWithWakeLock(Landroid/content/Context;Landroid/content/Intent;)V

    .line 1072
    return-void
.end method
