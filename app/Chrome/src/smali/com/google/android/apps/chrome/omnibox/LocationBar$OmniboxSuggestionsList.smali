.class public Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;
.super Landroid/widget/ListView;
.source "LocationBar.java"


# instance fields
.field private final mAnchorView:Landroid/view/View;

.field private final mBackgroundVerticalPadding:I

.field private mMaxMatchContentsWidth:F

.field private mMaxRequiredWidth:F

.field private final mSuggestionAnswerHeight:I

.field private final mSuggestionHeight:I

.field private final mTempPosition:[I

.field private final mTempRect:Landroid/graphics/Rect;

.field final synthetic this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/chrome/omnibox/LocationBar;Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 583
    iput-object p1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    .line 584
    const v0, 0x101006d

    invoke-direct {p0, p2, v2, v0}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 571
    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;->mTempPosition:[I

    .line 572
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;->mTempRect:Landroid/graphics/Rect;

    .line 585
    invoke-virtual {p0, v2}, Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 586
    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;->setFocusable(Z)V

    .line 587
    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;->setFocusableInTouchMode(Z)V

    .line 589
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$dimen;->omnibox_suggestion_height:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;->mSuggestionHeight:I

    .line 591
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$dimen;->omnibox_suggestion_answer_height:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;->mSuggestionAnswerHeight:I

    .line 594
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$dimen;->omnibox_suggestion_list_padding_top:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    .line 596
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/chrome/R$dimen;->omnibox_suggestion_list_padding_bottom:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    .line 598
    invoke-static {p0, v3, v0, v3, v1}, Lorg/chromium/base/ApiCompatibilityUtils;->setPaddingRelative(Landroid/view/View;IIII)V

    .line 600
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->getSuggestionPopupBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 601
    invoke-static {p0, v0}, Lorg/chromium/base/ApiCompatibilityUtils;->setBackgroundForView(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 602
    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;->mTempRect:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    .line 604
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;->mTempRect:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;->mTempRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;->getPaddingTop()I

    move-result v1

    add-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;->getPaddingBottom()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;->mBackgroundVerticalPadding:I

    .line 607
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->getRootView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$id;->toolbar:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;->mAnchorView:Landroid/view/View;

    .line 608
    return-void
.end method

.method static synthetic access$3200(Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;)V
    .locals 0

    .prologue
    .line 566
    invoke-direct {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;->updateLayoutParams()V

    return-void
.end method

.method static synthetic access$3300(Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;)V
    .locals 0

    .prologue
    .line 566
    invoke-direct {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;->show()V

    return-void
.end method

.method private getDesiredWidth()I
    .locals 1

    .prologue
    .line 735
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;->mAnchorView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    return v0
.end method

.method private getIdealHeight()I
    .locals 3

    .prologue
    .line 722
    iget v1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;->mBackgroundVerticalPadding:I

    .line 723
    const/4 v0, 0x0

    move v2, v1

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    # getter for: Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSuggestionItems:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->access$2100(Lcom/google/android/apps/chrome/omnibox/LocationBar;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 724
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    # getter for: Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSuggestionItems:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->access$2100(Lcom/google/android/apps/chrome/omnibox/LocationBar;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter$OmniboxResultItem;

    .line 725
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter$OmniboxResultItem;->getSuggestion()Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/omnibox/OmniboxSuggestion;->getAnswerContents()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 726
    iget v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;->mSuggestionAnswerHeight:I

    add-int/2addr v2, v0

    .line 723
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 728
    :cond_0
    iget v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;->mSuggestionHeight:I

    add-int/2addr v2, v0

    goto :goto_1

    .line 731
    :cond_1
    return v2
.end method

.method private show()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 611
    invoke-direct {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;->updateLayoutParams()V

    .line 612
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    .line 613
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/apps/chrome/omnibox/LocationBar;->mIgnoreOmniboxItemSelection:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->access$1702(Lcom/google/android/apps/chrome/omnibox/LocationBar;Z)Z

    .line 614
    invoke-virtual {p0, v2}, Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;->setVisibility(I)V

    .line 615
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;->getSelectedItemPosition()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, v2}, Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;->setSelection(I)V

    .line 617
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    iget-object v1, v1, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mUrlBar:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->getUrlDirection()I

    move-result v1

    # invokes: Lcom/google/android/apps/chrome/omnibox/LocationBar;->updateSuggestionsLayoutDirection(I)V
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->access$2500(Lcom/google/android/apps/chrome/omnibox/LocationBar;I)V

    .line 618
    return-void
.end method

.method private updateLayoutParams()V
    .locals 8

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 670
    .line 671
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 672
    if-nez v0, :cond_5

    .line 673
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v0, v3, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 674
    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    move-object v1, v0

    .line 680
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->getRootView()Landroid/view/View;

    move-result-object v0

    const v4, 0x1020002

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 681
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;->mAnchorView:Landroid/view/View;

    iget-object v5, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;->mTempPosition:[I

    invoke-static {v4, v0, v5}, Lcom/google/android/apps/chrome/utilities/ViewUtils;->getRelativeLayoutPosition(Landroid/view/View;Landroid/view/View;[I)V

    .line 682
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;->mTempPosition:[I

    aget v5, v0, v3

    .line 683
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;->mTempPosition:[I

    aget v6, v0, v2

    .line 685
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iget-object v7, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;->mTempPosition:[I

    invoke-static {v4, v0, v7}, Lcom/google/android/apps/chrome/utilities/ViewUtils;->getRelativeLayoutPosition(Landroid/view/View;Landroid/view/View;[I)V

    .line 686
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;->mTempPosition:[I

    aget v0, v0, v2

    .line 688
    iget-object v7, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;->mAnchorView:Landroid/view/View;

    invoke-virtual {v7}, Landroid/view/View;->getMeasuredHeight()I

    move-result v7

    add-int/2addr v6, v7

    .line 689
    sub-int v0, v6, v0

    .line 690
    iget v7, v1, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    if-eq v7, v0, :cond_4

    .line 691
    iput v0, v1, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    move v0, v2

    .line 695
    :goto_1
    invoke-virtual {v4}, Landroid/view/View;->getLeft()I

    move-result v3

    .line 696
    sub-int v3, v5, v3

    .line 697
    iget v4, v1, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    if-eq v4, v3, :cond_0

    .line 698
    iput v3, v1, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    move v0, v2

    .line 702
    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->getWindowDelegate()Lcom/google/android/apps/chrome/WindowDelegate;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;->mTempRect:Landroid/graphics/Rect;

    invoke-virtual {v3, v4}, Lcom/google/android/apps/chrome/WindowDelegate;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 703
    iget-object v3, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->getWindowDelegate()Lcom/google/android/apps/chrome/WindowDelegate;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/WindowDelegate;->getDecorViewHeight()I

    move-result v3

    .line 704
    iget-object v4, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;->mTempRect:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    invoke-static {v4, v3}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 705
    sub-int/2addr v3, v6

    .line 706
    invoke-direct {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;->getIdealHeight()I

    move-result v4

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 707
    iget v4, v1, Landroid/widget/FrameLayout$LayoutParams;->height:I

    if-eq v4, v3, :cond_1

    .line 708
    iput v3, v1, Landroid/widget/FrameLayout$LayoutParams;->height:I

    move v0, v2

    .line 712
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;->getDesiredWidth()I

    move-result v3

    .line 713
    iget v4, v1, Landroid/widget/FrameLayout$LayoutParams;->width:I

    if-eq v4, v3, :cond_3

    .line 714
    iput v3, v1, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 718
    :goto_2
    if-eqz v2, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;->requestLayout()V

    .line 719
    :cond_2
    return-void

    :cond_3
    move v2, v0

    goto :goto_2

    :cond_4
    move v0, v3

    goto :goto_1

    :cond_5
    move-object v1, v0

    goto/16 :goto_0
.end method


# virtual methods
.method public getMaxMatchContentsWidth()F
    .locals 1

    .prologue
    .line 666
    iget v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;->mMaxMatchContentsWidth:F

    return v0
.end method

.method public getMaxRequiredWidth()F
    .locals 1

    .prologue
    .line 659
    iget v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;->mMaxRequiredWidth:F

    return v0
.end method

.method public invalidateSuggestionViews()V
    .locals 3

    .prologue
    .line 625
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;->isShown()Z

    move-result v0

    if-nez v0, :cond_1

    .line 633
    :cond_0
    return-void

    .line 626
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    # getter for: Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSuggestionList:Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->access$700(Lcom/google/android/apps/chrome/omnibox/LocationBar;)Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;

    move-result-object v1

    .line 627
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1}, Landroid/widget/ListView;->getChildCount()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 628
    invoke-virtual {v1, v0}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    instance-of v2, v2, Lcom/google/android/apps/chrome/omnibox/SuggestionView;

    if-eqz v2, :cond_2

    .line 629
    invoke-virtual {v1, v0}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-static {v2}, Lorg/chromium/base/ApiCompatibilityUtils;->postInvalidateOnAnimation(Landroid/view/View;)V

    .line 627
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method protected layoutChildren()V
    .locals 2

    .prologue
    .line 746
    invoke-super {p0}, Landroid/widget/ListView;->layoutChildren()V

    .line 749
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;->isInTouchMode()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;->getSelectedView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 750
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;->getSelectedView()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setSelected(Z)V

    .line 752
    :cond_0
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 1

    .prologue
    .line 740
    invoke-super {p0, p1}, Landroid/widget/ListView;->onWindowFocusChanged(Z)V

    .line 741
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    # getter for: Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSuggestionModalShown:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->access$2600(Lcom/google/android/apps/chrome/omnibox/LocationBar;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->hideSuggestions()V

    .line 742
    :cond_0
    return-void
.end method

.method public resetMaxTextWidths()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 641
    iput v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;->mMaxRequiredWidth:F

    .line 642
    iput v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;->mMaxMatchContentsWidth:F

    .line 643
    return-void
.end method

.method public updateMaxTextWidths(FF)V
    .locals 1

    .prologue
    .line 651
    iget v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;->mMaxRequiredWidth:F

    invoke-static {v0, p1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;->mMaxRequiredWidth:F

    .line 652
    iget v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;->mMaxMatchContentsWidth:F

    invoke-static {v0, p2}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$OmniboxSuggestionsList;->mMaxMatchContentsWidth:F

    .line 653
    return-void
.end method
