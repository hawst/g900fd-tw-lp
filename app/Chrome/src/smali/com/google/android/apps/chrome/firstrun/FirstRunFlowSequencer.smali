.class public abstract Lcom/google/android/apps/chrome/firstrun/FirstRunFlowSequencer;
.super Ljava/lang/Object;
.source "FirstRunFlowSequencer.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final mActivity:Landroid/app/Activity;

.field private mHasChildAccount:Z

.field private mIsAndroidEduDevice:Z

.field private final mLaunchProperties:Landroid/os/Bundle;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const-class v0, Lcom/google/android/apps/chrome/firstrun/FirstRunFlowSequencer;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/firstrun/FirstRunFlowSequencer;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    iput-object p1, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunFlowSequencer;->mActivity:Landroid/app/Activity;

    .line 52
    iput-object p2, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunFlowSequencer;->mLaunchProperties:Landroid/os/Bundle;

    .line 53
    return-void
.end method

.method static synthetic access$002(Lcom/google/android/apps/chrome/firstrun/FirstRunFlowSequencer;Z)Z
    .locals 0

    .prologue
    .line 34
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunFlowSequencer;->mIsAndroidEduDevice:Z

    return p1
.end method

.method static synthetic access$102(Lcom/google/android/apps/chrome/firstrun/FirstRunFlowSequencer;Z)Z
    .locals 0

    .prologue
    .line 34
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunFlowSequencer;->mHasChildAccount:Z

    return p1
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/firstrun/FirstRunFlowSequencer;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunFlowSequencer;->processFreEnvironment()V

    return-void
.end method

.method public static checkIfFirstRunIsNecessary(Landroid/app/Activity;Landroid/content/Intent;Z)Landroid/content/Intent;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 213
    invoke-static {}, Lorg/chromium/base/CommandLine;->getInstance()Lorg/chromium/base/CommandLine;

    move-result-object v1

    const-string/jumbo v2, "disable-fre"

    invoke-virtual {v1, v2}, Lorg/chromium/base/CommandLine;->hasSwitch(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 238
    :cond_0
    :goto_0
    return-object v0

    .line 220
    :cond_1
    if-nez p2, :cond_2

    invoke-static {p0}, Lcom/google/android/apps/chrome/ToSAckedReceiver;->checkAnyUserHasSeenToS(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 223
    :cond_2
    invoke-static {p0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v1

    .line 224
    invoke-virtual {v1}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getFirstRunFlowComplete()Z

    move-result v1

    .line 225
    if-nez v1, :cond_3

    .line 226
    invoke-static {p0, p1, p2}, Lcom/google/android/apps/chrome/firstrun/FirstRunFlowSequencer;->createGenericFirstRunIntent(Landroid/app/Activity;Landroid/content/Intent;Z)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0

    .line 231
    :cond_3
    if-eqz p2, :cond_0

    invoke-static {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunIntroPage;->wereAllNecessaryPagesShown(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 236
    invoke-static {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunIntroPage;->getAllPresentablePages(Landroid/content/Context;)J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 238
    invoke-static {p0, p1, p2}, Lcom/google/android/apps/chrome/firstrun/FirstRunFlowSequencer;->createGenericFirstRunIntent(Landroid/app/Activity;Landroid/content/Intent;Z)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0
.end method

.method public static createGenericFirstRunIntent(Landroid/app/Activity;Landroid/content/Intent;Z)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 249
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "ComingFromChromeIcon"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "UseFreFlowSequencer"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static markFlowAsCompleted(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 179
    invoke-static {p0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v0

    .line 184
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getFirstRunEulaAccepted()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->setFirstRunEulaAccepted()V

    .line 187
    :cond_0
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getFirstRunFlowComplete()Z

    move-result v1

    if-nez v1, :cond_1

    .line 188
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->setFirstRunFlowComplete(Z)V

    .line 192
    :cond_1
    const-string/jumbo v1, "ShowIntroBitmap"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {p0, v2, v3}, Lcom/google/android/apps/chrome/firstrun/FirstRunIntroPage;->markAsShown(Landroid/content/Context;J)V

    .line 195
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getFirstRunFlowSignInComplete()Z

    move-result v1

    if-nez v1, :cond_2

    .line 196
    const-string/jumbo v1, "ResultSignInTo"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->setFirstRunFlowSignInAccountName(Ljava/lang/String;)V

    .line 198
    const-string/jumbo v1, "ResultShowSyncSettings"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->setFirstRunFlowSignInSetupSync(Z)V

    .line 201
    :cond_2
    return-void
.end method

.method private processFreEnvironment()V
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 107
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunFlowSequencer;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    .line 108
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunFlowSequencer;->mActivity:Landroid/app/Activity;

    invoke-static {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v5

    .line 110
    invoke-virtual {v5}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getFirstRunFlowComplete()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunFlowSequencer;->mActivity:Landroid/app/Activity;

    invoke-static {v0}, Lcom/google/android/apps/chrome/firstrun/FirstRunIntroPage;->wereAllNecessaryPagesShown(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 112
    sget-boolean v0, Lcom/google/android/apps/chrome/firstrun/FirstRunFlowSequencer;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunFlowSequencer;->mActivity:Landroid/app/Activity;

    invoke-static {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getFirstRunEulaAccepted()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 114
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunFlowSequencer;->mActivity:Landroid/app/Activity;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/chrome/firstrun/FirstRunFlowSequencer;->onFlowIsKnown(Landroid/app/Activity;Landroid/os/Bundle;)V

    .line 171
    :goto_0
    return-void

    .line 118
    :cond_1
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 119
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunFlowSequencer;->mLaunchProperties:Landroid/os/Bundle;

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 120
    const-string/jumbo v0, "UseFreFlowSequencer"

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 122
    invoke-static {v3}, Lorg/chromium/sync/signin/AccountManagerHelper;->get(Landroid/content/Context;)Lorg/chromium/sync/signin/AccountManagerHelper;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/sync/signin/AccountManagerHelper;->getGoogleAccounts()[Landroid/accounts/Account;

    move-result-object v7

    .line 123
    array-length v0, v7

    if-ne v0, v1, :cond_7

    move v0, v1

    .line 127
    :goto_1
    iget-boolean v4, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunFlowSequencer;->mIsAndroidEduDevice:Z

    if-eqz v4, :cond_8

    if-eqz v0, :cond_8

    invoke-static {v3}, Lorg/chromium/sync/signin/ChromeSigninController;->get(Landroid/content/Context;)Lorg/chromium/sync/signin/ChromeSigninController;

    move-result-object v3

    invoke-virtual {v3}, Lorg/chromium/sync/signin/ChromeSigninController;->isSignedIn()Z

    move-result v3

    if-nez v3, :cond_8

    move v3, v1

    .line 131
    :goto_2
    invoke-virtual {v5}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getFirstRunFlowComplete()Z

    move-result v4

    if-nez v4, :cond_c

    .line 133
    if-nez v3, :cond_9

    move v4, v1

    .line 134
    :goto_3
    const-string/jumbo v8, "ShowWelcome"

    invoke-virtual {v6, v8, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 138
    invoke-static {}, Lcom/google/android/apps/chrome/utilities/ChromeBuildInfo;->isStableBuild()Z

    move-result v4

    if-nez v4, :cond_2

    .line 139
    iget-object v4, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunFlowSequencer;->mActivity:Landroid/app/Activity;

    invoke-static {v4, v1}, Lcom/google/android/apps/chrome/firstrun/FirstRunUtil;->setCrashUploadPreference(Landroid/content/Context;Z)V

    .line 143
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunFlowSequencer;->isSyncAllowed()Z

    move-result v8

    .line 144
    const-string/jumbo v9, "ShowSignIn"

    if-eqz v8, :cond_a

    if-nez v3, :cond_a

    move v4, v1

    :goto_4
    invoke-virtual {v6, v9, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 145
    if-nez v8, :cond_3

    if-eqz v3, :cond_6

    .line 150
    :cond_3
    iget-object v4, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunFlowSequencer;->mActivity:Landroid/app/Activity;

    invoke-static {v4}, Lcom/google/android/apps/chrome/ToSAckedReceiver;->checkAnyUserHasSeenToS(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_4

    if-nez v0, :cond_5

    :cond_4
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunFlowSequencer;->mHasChildAccount:Z

    if-nez v0, :cond_5

    if-eqz v3, :cond_6

    .line 153
    :cond_5
    const-string/jumbo v0, "ForceSigninAccountTo"

    aget-object v4, v7, v2

    iget-object v4, v4, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v6, v0, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    const-string/jumbo v0, "PreselectButAllowToChange"

    if-nez v3, :cond_b

    iget-boolean v3, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunFlowSequencer;->mHasChildAccount:Z

    if-nez v3, :cond_b

    :goto_5
    invoke-virtual {v6, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 166
    :cond_6
    :goto_6
    const-string/jumbo v0, "ShowIntroBitmap"

    iget-object v1, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunFlowSequencer;->mActivity:Landroid/app/Activity;

    invoke-static {v1}, Lcom/google/android/apps/chrome/firstrun/FirstRunIntroPage;->getAllPresentablePages(Landroid/content/Context;)J

    move-result-wide v2

    invoke-virtual {v5}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getFirstRunIntroPageVisitedFlags()J

    move-result-wide v4

    const-wide/16 v8, -0x1

    xor-long/2addr v4, v8

    and-long/2addr v2, v4

    invoke-virtual {v6, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 170
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunFlowSequencer;->mActivity:Landroid/app/Activity;

    invoke-virtual {p0, v0, v6}, Lcom/google/android/apps/chrome/firstrun/FirstRunFlowSequencer;->onFlowIsKnown(Landroid/app/Activity;Landroid/os/Bundle;)V

    goto/16 :goto_0

    :cond_7
    move v0, v2

    .line 123
    goto :goto_1

    :cond_8
    move v3, v2

    .line 127
    goto :goto_2

    :cond_9
    move v4, v2

    .line 133
    goto :goto_3

    :cond_a
    move v4, v2

    .line 144
    goto :goto_4

    :cond_b
    move v1, v2

    .line 155
    goto :goto_5

    .line 161
    :cond_c
    const-string/jumbo v0, "ShowWelcome"

    invoke-virtual {v6, v0, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 162
    const-string/jumbo v0, "ShowSignIn"

    invoke-virtual {v6, v0, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_6
.end method


# virtual methods
.method didAcceptToS()Z
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunFlowSequencer;->mActivity:Landroid/app/Activity;

    invoke-static {v0}, Lcom/google/android/apps/chrome/ToSAckedReceiver;->checkAnyUserHasSeenToS(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunFlowSequencer;->mActivity:Landroid/app/Activity;

    invoke-static {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getFirstRunEulaAccepted()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method isSyncAllowed()Z
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunFlowSequencer;->mActivity:Landroid/app/Activity;

    invoke-static {v0}, Lorg/chromium/chrome/browser/util/FeatureUtilities;->canAllowSync(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunFlowSequencer;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/chrome/browser/signin/SigninManager;->get(Landroid/content/Context;)Lorg/chromium/chrome/browser/signin/SigninManager;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/signin/SigninManager;->isSigninDisabledByPolicy()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method isSystemInstall()Z
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunFlowSequencer;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    iget v0, v0, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract onFlowIsKnown(Landroid/app/Activity;Landroid/os/Bundle;)V
.end method

.method public start()V
    .locals 2

    .prologue
    .line 60
    invoke-static {}, Lorg/chromium/base/CommandLine;->getInstance()Lorg/chromium/base/CommandLine;

    move-result-object v0

    const-string/jumbo v1, "disable-fre"

    invoke-virtual {v0, v1}, Lorg/chromium/base/CommandLine;->hasSwitch(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 61
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunFlowSequencer;->mActivity:Landroid/app/Activity;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/chrome/firstrun/FirstRunFlowSequencer;->onFlowIsKnown(Landroid/app/Activity;Landroid/os/Bundle;)V

    .line 78
    :goto_0
    return-void

    .line 65
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunFlowSequencer;->mLaunchProperties:Landroid/os/Bundle;

    const-string/jumbo v1, "UseFreFlowSequencer"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 66
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunFlowSequencer;->mActivity:Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunFlowSequencer;->mLaunchProperties:Landroid/os/Bundle;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/chrome/firstrun/FirstRunFlowSequencer;->onFlowIsKnown(Landroid/app/Activity;Landroid/os/Bundle;)V

    goto :goto_0

    .line 70
    :cond_1
    new-instance v0, Lcom/google/android/apps/chrome/firstrun/FirstRunFlowSequencer$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunFlowSequencer$1;-><init>(Lcom/google/android/apps/chrome/firstrun/FirstRunFlowSequencer;)V

    iget-object v1, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunFlowSequencer;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/firstrun/FirstRunFlowSequencer$1;->start(Landroid/app/Activity;)V

    goto :goto_0
.end method
