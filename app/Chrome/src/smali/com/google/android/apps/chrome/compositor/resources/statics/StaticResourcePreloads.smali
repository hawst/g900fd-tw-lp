.class public Lcom/google/android/apps/chrome/compositor/resources/statics/StaticResourcePreloads;
.super Ljava/lang/Object;
.source "StaticResourcePreloads.java"


# instance fields
.field private mAsynchronousResources:[I

.field private mSynchronousResources:[I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getAsynchronousResources()[I
    .locals 3

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/resources/statics/StaticResourcePreloads;->mAsynchronousResources:[I

    if-nez v0, :cond_0

    .line 47
    const/4 v0, 0x3

    new-array v0, v0, [I

    const/4 v1, 0x0

    sget v2, Lcom/google/android/apps/chrome/R$drawable;->logo_card_back:I

    aput v2, v0, v1

    const/4 v1, 0x1

    sget v2, Lcom/google/android/apps/chrome/R$drawable;->btn_tabstrip_switch_normal:I

    aput v2, v0, v1

    const/4 v1, 0x2

    sget v2, Lcom/google/android/apps/chrome/R$drawable;->btn_tabstrip_switch_incognito:I

    aput v2, v0, v1

    iput-object v0, p0, Lcom/google/android/apps/chrome/compositor/resources/statics/StaticResourcePreloads;->mAsynchronousResources:[I

    .line 53
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/resources/statics/StaticResourcePreloads;->mAsynchronousResources:[I

    return-object v0
.end method

.method public getSynchronousResources(Landroid/content/Context;)[I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 26
    invoke-static {p1}, Lorg/chromium/ui/base/DeviceFormFactor;->isTablet(Landroid/content/Context;)Z

    move-result v0

    .line 28
    iget-object v1, p0, Lcom/google/android/apps/chrome/compositor/resources/statics/StaticResourcePreloads;->mSynchronousResources:[I

    if-nez v1, :cond_0

    if-eqz v0, :cond_0

    .line 29
    const/16 v0, 0x8

    new-array v0, v0, [I

    sget v1, Lcom/google/android/apps/chrome/R$drawable;->bg_tabstrip_tab:I

    aput v1, v0, v2

    const/4 v1, 0x1

    sget v2, Lcom/google/android/apps/chrome/R$drawable;->bg_tabstrip_background_tab:I

    aput v2, v0, v1

    const/4 v1, 0x2

    sget v2, Lcom/google/android/apps/chrome/R$drawable;->btn_tab_close_normal:I

    aput v2, v0, v1

    const/4 v1, 0x3

    sget v2, Lcom/google/android/apps/chrome/R$drawable;->btn_tab_close_white_normal:I

    aput v2, v0, v1

    const/4 v1, 0x4

    sget v2, Lcom/google/android/apps/chrome/R$drawable;->btn_tab_close_pressed:I

    aput v2, v0, v1

    const/4 v1, 0x5

    sget v2, Lcom/google/android/apps/chrome/R$drawable;->btn_tabstrip_new_tab_normal:I

    aput v2, v0, v1

    const/4 v1, 0x6

    sget v2, Lcom/google/android/apps/chrome/R$drawable;->btn_tabstrip_new_incognito_tab_normal:I

    aput v2, v0, v1

    const/4 v1, 0x7

    sget v2, Lcom/google/android/apps/chrome/R$drawable;->btn_tabstrip_new_tab_pressed:I

    aput v2, v0, v1

    iput-object v0, p0, Lcom/google/android/apps/chrome/compositor/resources/statics/StaticResourcePreloads;->mSynchronousResources:[I

    .line 42
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/resources/statics/StaticResourcePreloads;->mSynchronousResources:[I

    return-object v0

    .line 40
    :cond_0
    new-array v0, v2, [I

    iput-object v0, p0, Lcom/google/android/apps/chrome/compositor/resources/statics/StaticResourcePreloads;->mSynchronousResources:[I

    goto :goto_0
.end method
