.class public Lcom/google/android/apps/chrome/autofill/AutofillDataProvider;
.super Landroid/content/ContentProvider;
.source "AutofillDataProvider.java"

# interfaces
.implements Lorg/chromium/chrome/browser/autofill/PersonalDataManager$PersonalDataManagerObserver;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field static final ADDRESSES_DEFAULT_PROJECTION:[Ljava/lang/String;

.field public static final ADDRESSES_PATH:Ljava/lang/String; = "addresses"

.field public static final API_AUTHORITY_SUFFIX:Ljava/lang/String; = ".AutofillDataProvider"

.field static final CREDIT_CARDS_DEFAULT_PROJECTION:[Ljava/lang/String;

.field public static final CREDIT_CARDS_PATH:Ljava/lang/String; = "creditcards"


# instance fields
.field private final mInitializeUriMatcherLock:Ljava/lang/Object;

.field private mUriMatcher:Landroid/content/UriMatcher;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 47
    const-class v0, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider;->$assertionsDisabled:Z

    .line 98
    const/16 v0, 0xe

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v3, "GUID"

    aput-object v3, v0, v2

    const-string/jumbo v3, "ORIGIN"

    aput-object v3, v0, v1

    const-string/jumbo v3, "NAME_FULL"

    aput-object v3, v0, v5

    const-string/jumbo v3, "COMPANY_NAME"

    aput-object v3, v0, v6

    const-string/jumbo v3, "ADDRESS_LINE1"

    aput-object v3, v0, v7

    const/4 v3, 0x5

    const-string/jumbo v4, "ADDRESS_LINE2"

    aput-object v4, v0, v3

    const/4 v3, 0x6

    const-string/jumbo v4, "ADDRESS_CITY"

    aput-object v4, v0, v3

    const/4 v3, 0x7

    const-string/jumbo v4, "ADDRESS_STATE"

    aput-object v4, v0, v3

    const/16 v3, 0x8

    const-string/jumbo v4, "ADDRESS_ZIP"

    aput-object v4, v0, v3

    const/16 v3, 0x9

    const-string/jumbo v4, "ADDRESS_COUNTRY_CODE"

    aput-object v4, v0, v3

    const/16 v3, 0xa

    const-string/jumbo v4, "PHONE_WHOLE_NUMBER"

    aput-object v4, v0, v3

    const/16 v3, 0xb

    const-string/jumbo v4, "EMAIL_ADDRESS"

    aput-object v4, v0, v3

    const/16 v3, 0xc

    const-string/jumbo v4, "IS_DEFAULT_BILLING_ADDRESS"

    aput-object v4, v0, v3

    const/16 v3, 0xd

    const-string/jumbo v4, "IS_DEFAULT_SHIPPING_ADDRESS"

    aput-object v4, v0, v3

    sput-object v0, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider;->ADDRESSES_DEFAULT_PROJECTION:[Ljava/lang/String;

    .line 131
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v3, "GUID"

    aput-object v3, v0, v2

    const-string/jumbo v2, "ORIGIN"

    aput-object v2, v0, v1

    const-string/jumbo v1, "NAME_FULL"

    aput-object v1, v0, v5

    const-string/jumbo v1, "CREDIT_CARD_NUMBER"

    aput-object v1, v0, v6

    const-string/jumbo v1, "EXPIRATION_MONTH"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string/jumbo v2, "EXPIRATION_YEAR"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "IS_DEFAULT_CREDIT_CARD"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider;->CREDIT_CARDS_DEFAULT_PROJECTION:[Ljava/lang/String;

    return-void

    :cond_0
    move v0, v2

    .line 47
    goto/16 :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    .line 57
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider;->mInitializeUriMatcherLock:Ljava/lang/Object;

    .line 118
    return-void
.end method

.method private addAddress(Landroid/content/ContentValues;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 302
    sget-boolean v0, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    const-string/jumbo v0, "GUID"

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 303
    :cond_0
    const-string/jumbo v0, "GUID"

    const-string/jumbo v1, ""

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 304
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider;->addOrUpdateAddress(Landroid/content/ContentValues;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private addCreditCard(Landroid/content/ContentValues;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 296
    sget-boolean v0, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    const-string/jumbo v0, "GUID"

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 297
    :cond_0
    const-string/jumbo v0, "GUID"

    const-string/jumbo v1, ""

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 298
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider;->addOrUpdateCreditCard(Landroid/content/ContentValues;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private addOrUpdateAddress(Landroid/content/ContentValues;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 176
    const-string/jumbo v0, "ORIGIN"

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 177
    const-string/jumbo v0, "ORIGIN"

    const-string/jumbo v1, "Chrome Autofill dialog"

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    :cond_0
    new-instance v0, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider$1;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider$1;-><init>(Lcom/google/android/apps/chrome/autofill/AutofillDataProvider;Landroid/content/ContentValues;)V

    invoke-static {v0}, Lorg/chromium/base/ThreadUtils;->runOnUiThreadBlockingNoException(Ljava/util/concurrent/Callable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method private addOrUpdateCreditCard(Landroid/content/ContentValues;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 208
    const-string/jumbo v0, "ORIGIN"

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 209
    const-string/jumbo v0, "ORIGIN"

    const-string/jumbo v1, "Chrome Autofill dialog"

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    :cond_0
    new-instance v0, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider$2;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider$2;-><init>(Lcom/google/android/apps/chrome/autofill/AutofillDataProvider;Landroid/content/ContentValues;)V

    invoke-static {v0}, Lorg/chromium/base/ThreadUtils;->runOnUiThreadBlockingNoException(Ljava/util/concurrent/Callable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method private static buildAPIContentUri(Landroid/content/Context;Ljava/lang/String;)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 142
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "content://"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ".AutofillDataProvider/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method private createAddressesCursor(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 4

    .prologue
    .line 361
    if-nez p1, :cond_0

    const/4 v0, 0x0

    .line 375
    :goto_0
    return-object v0

    .line 363
    :cond_0
    new-instance v2, Landroid/database/MatrixCursor;

    sget-object v0, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider;->ADDRESSES_DEFAULT_PROJECTION:[Ljava/lang/String;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v2, v0, v1}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;I)V

    .line 364
    sget-object v0, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider;->ADDRESSES_DEFAULT_PROJECTION:[Ljava/lang/String;

    array-length v0, v0

    new-array v3, v0, [Ljava/lang/Object;

    .line 366
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 367
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/autofill/PersonalDataManager$AutofillProfile;

    .line 368
    invoke-direct {p0, v3, v0, p2, p3}, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider;->fillColumnValuesWithAddressDefaultProjection([Ljava/lang/Object;Lorg/chromium/chrome/browser/autofill/PersonalDataManager$AutofillProfile;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 370
    invoke-virtual {v2, v3}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 366
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_2
    move-object v0, v2

    .line 375
    goto :goto_0
.end method

.method private createCreditCardsCursor(Ljava/util/List;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 4

    .prologue
    .line 380
    if-nez p1, :cond_0

    const/4 v0, 0x0

    .line 394
    :goto_0
    return-object v0

    .line 382
    :cond_0
    new-instance v2, Landroid/database/MatrixCursor;

    sget-object v0, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider;->CREDIT_CARDS_DEFAULT_PROJECTION:[Ljava/lang/String;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v2, v0, v1}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;I)V

    .line 383
    sget-object v0, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider;->CREDIT_CARDS_DEFAULT_PROJECTION:[Ljava/lang/String;

    array-length v0, v0

    new-array v3, v0, [Ljava/lang/Object;

    .line 385
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 386
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/autofill/PersonalDataManager$CreditCard;

    .line 387
    invoke-direct {p0, v3, v0, p2}, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider;->fillColumnValuesWithCreditCardDefaultProjection([Ljava/lang/Object;Lorg/chromium/chrome/browser/autofill/PersonalDataManager$CreditCard;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 389
    invoke-virtual {v2, v3}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 385
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_2
    move-object v0, v2

    .line 394
    goto :goto_0
.end method

.method private ensureTheCallOriginatesFromGoogle()V
    .locals 3

    .prologue
    .line 168
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x1

    const-string/jumbo v2, "com.google.android.gms"

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/externalauth/ExternalAuthUtils;->checkCallerIsValidForPackage(Landroid/content/Context;ILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 171
    new-instance v0, Ljava/lang/SecurityException;

    const-string/jumbo v1, "Invalid call from non-GmsCore client"

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 173
    :cond_0
    return-void
.end method

.method private ensureUriMatcherInitialized()V
    .locals 5

    .prologue
    .line 147
    iget-object v1, p0, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider;->mInitializeUriMatcherLock:Ljava/lang/Object;

    monitor-enter v1

    .line 148
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider;->mUriMatcher:Landroid/content/UriMatcher;

    if-eqz v0, :cond_0

    monitor-exit v1

    .line 160
    :goto_0
    return-void

    .line 150
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, ".AutofillDataProvider"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 151
    new-instance v2, Landroid/content/UriMatcher;

    const/4 v3, -0x1

    invoke-direct {v2, v3}, Landroid/content/UriMatcher;-><init>(I)V

    iput-object v2, p0, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider;->mUriMatcher:Landroid/content/UriMatcher;

    .line 154
    iget-object v2, p0, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider;->mUriMatcher:Landroid/content/UriMatcher;

    const-string/jumbo v3, "addresses/*"

    const/4 v4, 0x0

    invoke-virtual {v2, v0, v3, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 155
    iget-object v2, p0, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider;->mUriMatcher:Landroid/content/UriMatcher;

    const-string/jumbo v3, "addresses/*/*"

    const/4 v4, 0x1

    invoke-virtual {v2, v0, v3, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 158
    iget-object v2, p0, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider;->mUriMatcher:Landroid/content/UriMatcher;

    const-string/jumbo v3, "creditcards/*"

    const/4 v4, 0x2

    invoke-virtual {v2, v0, v3, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 159
    iget-object v2, p0, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider;->mUriMatcher:Landroid/content/UriMatcher;

    const-string/jumbo v3, "creditcards/*/*"

    const/4 v4, 0x3

    invoke-virtual {v2, v0, v3, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 160
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private fetchAddresses(Ljava/lang/String;)Ljava/util/List;
    .locals 1

    .prologue
    .line 318
    new-instance v0, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider$3;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider$3;-><init>(Lcom/google/android/apps/chrome/autofill/AutofillDataProvider;Ljava/lang/String;)V

    invoke-static {v0}, Lorg/chromium/base/ThreadUtils;->runOnUiThreadBlockingNoException(Ljava/util/concurrent/Callable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method private fetchCreditCards(Ljava/lang/String;)Ljava/util/List;
    .locals 1

    .prologue
    .line 339
    new-instance v0, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider$4;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider$4;-><init>(Lcom/google/android/apps/chrome/autofill/AutofillDataProvider;Ljava/lang/String;)V

    invoke-static {v0}, Lorg/chromium/base/ThreadUtils;->runOnUiThreadBlockingNoException(Ljava/util/concurrent/Callable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method private fillColumnValuesWithAddressDefaultProjection([Ljava/lang/Object;Lorg/chromium/chrome/browser/autofill/PersonalDataManager$AutofillProfile;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 231
    if-nez p2, :cond_1

    .line 267
    :cond_0
    :goto_0
    return v3

    .line 232
    :cond_1
    if-eqz p1, :cond_0

    .line 234
    sget-boolean v0, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    sget-object v0, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider;->ADDRESSES_DEFAULT_PROJECTION:[Ljava/lang/String;

    array-length v0, v0

    const/16 v1, 0xe

    if-eq v0, v1, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 235
    :cond_2
    array-length v0, p1

    sget-object v1, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider;->ADDRESSES_DEFAULT_PROJECTION:[Ljava/lang/String;

    array-length v1, v1

    if-ne v0, v1, :cond_0

    .line 237
    invoke-virtual {p2}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager$AutofillProfile;->getStreetAddress()Ljava/lang/String;

    move-result-object v0

    .line 239
    const-string/jumbo v1, ""

    .line 240
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    const-string/jumbo v4, "\n"

    invoke-virtual {v0, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 242
    const-string/jumbo v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 243
    array-length v0, v1

    if-lez v0, :cond_4

    aget-object v0, v1, v3

    .line 244
    :goto_1
    array-length v4, v1

    if-le v4, v2, :cond_5

    aget-object v1, v1, v2

    .line 247
    :cond_3
    :goto_2
    invoke-virtual {p2}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager$AutofillProfile;->getGUID()Ljava/lang/String;

    move-result-object v4

    aput-object v4, p1, v3

    .line 248
    invoke-virtual {p2}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager$AutofillProfile;->getOrigin()Ljava/lang/String;

    move-result-object v4

    aput-object v4, p1, v2

    .line 249
    const/4 v4, 0x2

    invoke-virtual {p2}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager$AutofillProfile;->getFullName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, p1, v4

    .line 250
    const/4 v4, 0x3

    invoke-virtual {p2}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager$AutofillProfile;->getCompanyName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, p1, v4

    .line 251
    const/4 v4, 0x4

    aput-object v0, p1, v4

    .line 252
    const/4 v0, 0x5

    aput-object v1, p1, v0

    .line 253
    const/4 v0, 0x6

    invoke-virtual {p2}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager$AutofillProfile;->getLocality()Ljava/lang/String;

    move-result-object v1

    aput-object v1, p1, v0

    .line 254
    const/4 v0, 0x7

    invoke-virtual {p2}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager$AutofillProfile;->getRegion()Ljava/lang/String;

    move-result-object v1

    aput-object v1, p1, v0

    .line 255
    const/16 v0, 0x8

    invoke-virtual {p2}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager$AutofillProfile;->getPostalCode()Ljava/lang/String;

    move-result-object v1

    aput-object v1, p1, v0

    .line 256
    const/16 v0, 0x9

    invoke-virtual {p2}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager$AutofillProfile;->getCountryCode()Ljava/lang/String;

    move-result-object v1

    aput-object v1, p1, v0

    .line 257
    const/16 v0, 0xa

    invoke-virtual {p2}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager$AutofillProfile;->getPhoneNumber()Ljava/lang/String;

    move-result-object v1

    aput-object v1, p1, v0

    .line 258
    const/16 v0, 0xb

    invoke-virtual {p2}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager$AutofillProfile;->getEmailAddress()Ljava/lang/String;

    move-result-object v1

    aput-object v1, p1, v0

    .line 259
    const/16 v1, 0xc

    invoke-virtual {p2}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager$AutofillProfile;->getGUID()Ljava/lang/String;

    move-result-object v0

    invoke-static {p3, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_6

    move v0, v2

    :goto_3
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, p1, v1

    .line 260
    const/16 v1, 0xd

    invoke-virtual {p2}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager$AutofillProfile;->getGUID()Ljava/lang/String;

    move-result-object v0

    invoke-static {p4, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_7

    move v0, v2

    :goto_4
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, p1, v1

    move v0, v3

    .line 262
    :goto_5
    array-length v1, p1

    if-ge v0, v1, :cond_9

    .line 263
    sget-boolean v1, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider;->$assertionsDisabled:Z

    if-nez v1, :cond_8

    aget-object v1, p1, v0

    if-nez v1, :cond_8

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 243
    :cond_4
    const-string/jumbo v0, ""

    goto/16 :goto_1

    .line 244
    :cond_5
    const-string/jumbo v1, ""

    goto/16 :goto_2

    :cond_6
    move v0, v3

    .line 259
    goto :goto_3

    :cond_7
    move v0, v3

    .line 260
    goto :goto_4

    .line 264
    :cond_8
    aget-object v1, p1, v0

    if-eqz v1, :cond_0

    .line 262
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    :cond_9
    move v3, v2

    .line 267
    goto/16 :goto_0
.end method

.method private fillColumnValuesWithCreditCardDefaultProjection([Ljava/lang/Object;Lorg/chromium/chrome/browser/autofill/PersonalDataManager$CreditCard;Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 273
    if-nez p2, :cond_1

    .line 292
    :cond_0
    :goto_0
    return v2

    .line 274
    :cond_1
    if-eqz p1, :cond_0

    .line 276
    sget-boolean v0, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    sget-object v0, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider;->CREDIT_CARDS_DEFAULT_PROJECTION:[Ljava/lang/String;

    array-length v0, v0

    const/4 v3, 0x7

    if-eq v0, v3, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 277
    :cond_2
    array-length v0, p1

    sget-object v3, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider;->CREDIT_CARDS_DEFAULT_PROJECTION:[Ljava/lang/String;

    array-length v3, v3

    if-ne v0, v3, :cond_0

    .line 279
    invoke-virtual {p2}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager$CreditCard;->getGUID()Ljava/lang/String;

    move-result-object v0

    aput-object v0, p1, v2

    .line 280
    invoke-virtual {p2}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager$CreditCard;->getOrigin()Ljava/lang/String;

    move-result-object v0

    aput-object v0, p1, v1

    .line 281
    const/4 v0, 0x2

    invoke-virtual {p2}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager$CreditCard;->getName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, p1, v0

    .line 282
    const/4 v0, 0x3

    invoke-virtual {p2}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager$CreditCard;->getNumber()Ljava/lang/String;

    move-result-object v3

    aput-object v3, p1, v0

    .line 283
    const/4 v0, 0x4

    invoke-virtual {p2}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager$CreditCard;->getMonth()Ljava/lang/String;

    move-result-object v3

    aput-object v3, p1, v0

    .line 284
    const/4 v0, 0x5

    invoke-virtual {p2}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager$CreditCard;->getYear()Ljava/lang/String;

    move-result-object v3

    aput-object v3, p1, v0

    .line 285
    const/4 v3, 0x6

    invoke-virtual {p2}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager$CreditCard;->getGUID()Ljava/lang/String;

    move-result-object v0

    invoke-static {p3, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    :goto_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, p1, v3

    move v0, v2

    .line 287
    :goto_2
    array-length v3, p1

    if-ge v0, v3, :cond_5

    .line 288
    sget-boolean v3, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider;->$assertionsDisabled:Z

    if-nez v3, :cond_4

    aget-object v3, p1, v0

    if-nez v3, :cond_4

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_3
    move v0, v2

    .line 285
    goto :goto_1

    .line 289
    :cond_4
    aget-object v3, p1, v0

    if-eqz v3, :cond_0

    .line 287
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_5
    move v2, v1

    .line 292
    goto :goto_0
.end method

.method private getAddressByGuid(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 399
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 402
    :goto_0
    return-object v0

    .line 401
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider;->fetchAddresses(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 402
    invoke-direct {p0, v0, p2, p3}, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider;->createAddressesCursor(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    goto :goto_0
.end method

.method private getAddresses(Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 413
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider;->fetchAddresses(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 414
    invoke-direct {p0, v0, p1, p2}, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider;->createAddressesCursor(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method private getCreditCardByGuid(Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 406
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 409
    :goto_0
    return-object v0

    .line 408
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider;->fetchCreditCards(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 409
    invoke-direct {p0, v0, p2}, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider;->createCreditCardsCursor(Ljava/util/List;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    goto :goto_0
.end method

.method private getCreditCards(Ljava/lang/String;)Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 418
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider;->fetchCreditCards(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 419
    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider;->createCreditCardsCursor(Ljava/util/List;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method private updateAddress(Landroid/content/ContentValues;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 313
    sget-boolean v0, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    const-string/jumbo v0, "GUID"

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 314
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider;->addOrUpdateAddress(Landroid/content/ContentValues;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private updateCreditCard(Landroid/content/ContentValues;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 308
    sget-boolean v0, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    const-string/jumbo v0, "GUID"

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 309
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider;->addOrUpdateCreditCard(Landroid/content/ContentValues;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 3

    .prologue
    .line 521
    invoke-direct {p0}, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider;->ensureTheCallOriginatesFromGoogle()V

    .line 522
    invoke-direct {p0}, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider;->ensureUriMatcherInitialized()V

    .line 524
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "AutofillDataProvider: DELETE is not supported for URI "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 529
    invoke-direct {p0}, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider;->ensureTheCallOriginatesFromGoogle()V

    .line 530
    invoke-direct {p0}, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider;->ensureUriMatcherInitialized()V

    .line 532
    iget-object v0, p0, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider;->mUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    .line 533
    packed-switch v0, :pswitch_data_0

    .line 543
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "AutofillDataProvider: getType - unknown URL "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 535
    :pswitch_0
    const-string/jumbo v0, "vnd.android.gpswsci.cursor.dir/address"

    .line 541
    :goto_0
    return-object v0

    .line 537
    :pswitch_1
    const-string/jumbo v0, "vnd.android.gpswsci.cursor.dir/creditcard"

    goto :goto_0

    .line 539
    :pswitch_2
    const-string/jumbo v0, "vnd.android.gpswsci.cursor.item/address"

    goto :goto_0

    .line 541
    :pswitch_3
    const-string/jumbo v0, "vnd.android.gpswsci.cursor.item/creditcard"

    goto :goto_0

    .line 533
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 474
    invoke-direct {p0}, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider;->ensureTheCallOriginatesFromGoogle()V

    .line 475
    invoke-direct {p0}, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider;->ensureUriMatcherInitialized()V

    .line 477
    iget-object v0, p0, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider;->mUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    .line 479
    packed-switch v0, :pswitch_data_0

    .line 488
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "AutofillDataProvider: insert - unknown URL "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 482
    :pswitch_1
    invoke-direct {p0, p2}, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider;->addAddress(Landroid/content/ContentValues;)Ljava/lang/String;

    move-result-object v0

    .line 491
    :goto_0
    if-eqz v0, :cond_1

    invoke-static {p1, v0}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 492
    :goto_1
    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 494
    :cond_0
    return-object v0

    .line 485
    :pswitch_2
    invoke-direct {p0, p2}, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider;->addCreditCard(Landroid/content/ContentValues;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    move-object v0, v1

    goto :goto_1

    .line 479
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public onCreate()Z
    .locals 1

    .prologue
    .line 426
    const/4 v0, 0x1

    return v0
.end method

.method public onPersonalDataChanged()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 551
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string/jumbo v2, "addresses"

    invoke-static {v1, v2}, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider;->buildAPIContentUri(Landroid/content/Context;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 553
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string/jumbo v2, "creditcards"

    invoke-static {v1, v2}, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider;->buildAPIContentUri(Landroid/content/Context;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 555
    return-void
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 432
    invoke-direct {p0}, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider;->ensureTheCallOriginatesFromGoogle()V

    .line 433
    invoke-direct {p0}, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider;->ensureUriMatcherInitialized()V

    .line 435
    iget-object v0, p0, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider;->mUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v2

    .line 439
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    .line 440
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    if-le v3, v4, :cond_2

    .line 441
    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/chrome/autofill/AutofillDataProviderSessionInformation;->deserialize(Ljava/lang/String;)Lcom/google/android/apps/chrome/autofill/AutofillDataProviderSessionInformation;

    move-result-object v0

    .line 443
    :goto_0
    if-nez v0, :cond_0

    .line 444
    new-instance v0, Lcom/google/android/apps/chrome/autofill/AutofillDataProviderSessionInformation;

    invoke-direct {v0, v1, v1, v1}, Lcom/google/android/apps/chrome/autofill/AutofillDataProviderSessionInformation;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 447
    :cond_0
    packed-switch v2, :pswitch_data_0

    .line 464
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "AutofillDataProvider: query - unknown URL uri = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 449
    :pswitch_0
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/autofill/AutofillDataProviderSessionInformation;->getGuidDefaultBilling()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/autofill/AutofillDataProviderSessionInformation;->getGuidDefaultShipping()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider;->getAddresses(Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 467
    :goto_1
    if-nez v0, :cond_1

    new-instance v0, Landroid/database/MatrixCursor;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 468
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    .line 469
    return-object v0

    .line 453
    :pswitch_1
    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/autofill/AutofillDataProviderSessionInformation;->getGuidDefaultBilling()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/autofill/AutofillDataProviderSessionInformation;->getGuidDefaultShipping()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v1, v2, v0}, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider;->getAddressByGuid(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    goto :goto_1

    .line 457
    :pswitch_2
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/autofill/AutofillDataProviderSessionInformation;->getGuidDefaultCard()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider;->getCreditCards(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    goto :goto_1

    .line 460
    :pswitch_3
    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/autofill/AutofillDataProviderSessionInformation;->getGuidDefaultCard()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider;->getCreditCardByGuid(Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    goto :goto_1

    :cond_2
    move-object v0, v1

    goto :goto_0

    .line 447
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 499
    invoke-direct {p0}, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider;->ensureTheCallOriginatesFromGoogle()V

    .line 500
    invoke-direct {p0}, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider;->ensureUriMatcherInitialized()V

    .line 502
    iget-object v1, p0, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider;->mUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v1, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v2

    .line 503
    const/4 v1, 0x0

    .line 504
    packed-switch v2, :pswitch_data_0

    .line 512
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "AutofillDataProvider: update - unknown URL "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 506
    :pswitch_1
    invoke-direct {p0, p2}, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider;->updateAddress(Landroid/content/ContentValues;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 515
    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 516
    :cond_1
    return v0

    .line 509
    :pswitch_2
    invoke-direct {p0, p2}, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider;->updateCreditCard(Landroid/content/ContentValues;)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    .line 504
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
