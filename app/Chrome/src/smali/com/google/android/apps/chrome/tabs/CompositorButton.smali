.class public Lcom/google/android/apps/chrome/tabs/CompositorButton;
.super Ljava/lang/Object;
.source "CompositorButton.java"

# interfaces
.implements Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;


# instance fields
.field private final mCacheBounds:Landroid/graphics/RectF;

.field private mClickSlop:F

.field private mHeight:F

.field private mIncognitoPressedResource:I

.field private mIncognitoResource:I

.field private mIsEnabled:Z

.field private mIsIncognito:Z

.field private mIsPressed:Z

.field private mIsVisible:Z

.field private mOpacity:F

.field private mPressedResource:I

.field private mResource:I

.field private mWidth:F

.field private mX:F

.field private mY:F


# direct methods
.method public constructor <init>(Landroid/content/Context;FF)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/CompositorButton;->mCacheBounds:Landroid/graphics/RectF;

    .line 52
    iput p2, p0, Lcom/google/android/apps/chrome/tabs/CompositorButton;->mWidth:F

    .line 53
    iput p3, p0, Lcom/google/android/apps/chrome/tabs/CompositorButton;->mHeight:F

    .line 54
    iput v1, p0, Lcom/google/android/apps/chrome/tabs/CompositorButton;->mX:F

    .line 55
    iput v1, p0, Lcom/google/android/apps/chrome/tabs/CompositorButton;->mY:F

    .line 56
    iput v2, p0, Lcom/google/android/apps/chrome/tabs/CompositorButton;->mOpacity:F

    .line 57
    iput-boolean v3, p0, Lcom/google/android/apps/chrome/tabs/CompositorButton;->mIsPressed:Z

    .line 58
    iput-boolean v4, p0, Lcom/google/android/apps/chrome/tabs/CompositorButton;->mIsVisible:Z

    .line 59
    iput-boolean v3, p0, Lcom/google/android/apps/chrome/tabs/CompositorButton;->mIsIncognito:Z

    .line 60
    iput-boolean v4, p0, Lcom/google/android/apps/chrome/tabs/CompositorButton;->mIsEnabled:Z

    .line 62
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 63
    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    div-float v1, v2, v1

    .line 64
    sget v2, Lcom/google/android/apps/chrome/R$dimen;->close_button_slop:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/CompositorButton;->mClickSlop:F

    .line 65
    return-void
.end method


# virtual methods
.method public checkClicked(FF)Z
    .locals 6

    .prologue
    .line 253
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/CompositorButton;->mOpacity:F

    const/high16 v1, 0x3f800000    # 1.0f

    cmpg-float v0, v0, v1

    if-ltz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/CompositorButton;->mIsVisible:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/CompositorButton;->mIsEnabled:Z

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    .line 257
    :goto_0
    return v0

    .line 255
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/CompositorButton;->mCacheBounds:Landroid/graphics/RectF;

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/CompositorButton;->mX:F

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/CompositorButton;->mY:F

    iget v3, p0, Lcom/google/android/apps/chrome/tabs/CompositorButton;->mX:F

    iget v4, p0, Lcom/google/android/apps/chrome/tabs/CompositorButton;->mWidth:F

    add-float/2addr v3, v4

    iget v4, p0, Lcom/google/android/apps/chrome/tabs/CompositorButton;->mY:F

    iget v5, p0, Lcom/google/android/apps/chrome/tabs/CompositorButton;->mHeight:F

    add-float/2addr v4, v5

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;->set(FFFF)V

    .line 256
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/CompositorButton;->mCacheBounds:Landroid/graphics/RectF;

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/CompositorButton;->mClickSlop:F

    neg-float v1, v1

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/CompositorButton;->mClickSlop:F

    neg-float v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/RectF;->inset(FF)V

    .line 257
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/CompositorButton;->mCacheBounds:Landroid/graphics/RectF;

    invoke-virtual {v0, p1, p2}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v0

    goto :goto_0
.end method

.method public click(FF)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 294
    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/chrome/tabs/CompositorButton;->checkClicked(FF)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 295
    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/tabs/CompositorButton;->setPressed(Z)V

    .line 296
    const/4 v0, 0x1

    .line 298
    :cond_0
    return v0
.end method

.method public drag(FF)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 267
    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/chrome/tabs/CompositorButton;->checkClicked(FF)Z

    move-result v1

    if-nez v1, :cond_0

    .line 268
    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/tabs/CompositorButton;->setPressed(Z)V

    .line 271
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/CompositorButton;->isPressed()Z

    move-result v0

    goto :goto_0
.end method

.method public getHeight()F
    .locals 1

    .prologue
    .line 139
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/CompositorButton;->mHeight:F

    return v0
.end method

.method public getOpacity()F
    .locals 1

    .prologue
    .line 163
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/CompositorButton;->mOpacity:F

    return v0
.end method

.method public getResourceId()I
    .locals 1

    .prologue
    .line 241
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/CompositorButton;->isPressed()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 242
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/CompositorButton;->isIncognito()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/CompositorButton;->mIncognitoPressedResource:I

    .line 244
    :goto_0
    return v0

    .line 242
    :cond_0
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/CompositorButton;->mPressedResource:I

    goto :goto_0

    .line 244
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/CompositorButton;->isIncognito()Z

    move-result v0

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/CompositorButton;->mIncognitoResource:I

    goto :goto_0

    :cond_2
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/CompositorButton;->mResource:I

    goto :goto_0
.end method

.method public getWidth()F
    .locals 1

    .prologue
    .line 125
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/CompositorButton;->mWidth:F

    return v0
.end method

.method public getX()F
    .locals 1

    .prologue
    .line 97
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/CompositorButton;->mX:F

    return v0
.end method

.method public getY()F
    .locals 1

    .prologue
    .line 111
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/CompositorButton;->mY:F

    return v0
.end method

.method public isIncognito()Z
    .locals 1

    .prologue
    .line 205
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/CompositorButton;->mIsIncognito:Z

    return v0
.end method

.method public isPressed()Z
    .locals 1

    .prologue
    .line 177
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/CompositorButton;->mIsPressed:Z

    return v0
.end method

.method public isVisible()Z
    .locals 1

    .prologue
    .line 191
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/CompositorButton;->mIsVisible:Z

    return v0
.end method

.method public onDown(FF)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 281
    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/chrome/tabs/CompositorButton;->checkClicked(FF)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 282
    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/tabs/CompositorButton;->setPressed(Z)V

    .line 285
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onUpOrCancel()Z
    .locals 2

    .prologue
    .line 306
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/CompositorButton;->isPressed()Z

    move-result v0

    .line 307
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/tabs/CompositorButton;->setPressed(Z)V

    .line 308
    return v0
.end method

.method public setBounds(Landroid/graphics/RectF;)V
    .locals 1

    .prologue
    .line 153
    invoke-virtual {p1}, Landroid/graphics/RectF;->width()F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/CompositorButton;->mWidth:F

    .line 154
    invoke-virtual {p1}, Landroid/graphics/RectF;->height()F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/CompositorButton;->mHeight:F

    .line 155
    iget v0, p1, Landroid/graphics/RectF;->left:F

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/CompositorButton;->mX:F

    .line 156
    iget v0, p1, Landroid/graphics/RectF;->top:F

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/CompositorButton;->mY:F

    .line 157
    return-void
.end method

.method public setClickSlop(F)V
    .locals 0

    .prologue
    .line 234
    iput p1, p0, Lcom/google/android/apps/chrome/tabs/CompositorButton;->mClickSlop:F

    .line 235
    return-void
.end method

.method public setHeight(F)V
    .locals 0

    .prologue
    .line 146
    iput p1, p0, Lcom/google/android/apps/chrome/tabs/CompositorButton;->mHeight:F

    .line 147
    return-void
.end method

.method public setIncognito(Z)V
    .locals 0

    .prologue
    .line 212
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/tabs/CompositorButton;->mIsIncognito:Z

    .line 213
    return-void
.end method

.method public setOpacity(F)V
    .locals 0

    .prologue
    .line 170
    iput p1, p0, Lcom/google/android/apps/chrome/tabs/CompositorButton;->mOpacity:F

    .line 171
    return-void
.end method

.method public setPressed(Z)V
    .locals 0

    .prologue
    .line 184
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/tabs/CompositorButton;->mIsPressed:Z

    .line 185
    return-void
.end method

.method public setProperty(Lcom/google/android/apps/chrome/tabs/CompositorButton$Property;F)V
    .locals 2

    .prologue
    .line 319
    sget-object v0, Lcom/google/android/apps/chrome/tabs/CompositorButton$1;->$SwitchMap$com$google$android$apps$chrome$tabs$CompositorButton$Property:[I

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/tabs/CompositorButton$Property;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 322
    :goto_0
    return-void

    .line 321
    :pswitch_0
    invoke-virtual {p0, p2}, Lcom/google/android/apps/chrome/tabs/CompositorButton;->setOpacity(F)V

    goto :goto_0

    .line 319
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic setProperty(Ljava/lang/Enum;F)V
    .locals 0

    .prologue
    .line 18
    check-cast p1, Lcom/google/android/apps/chrome/tabs/CompositorButton$Property;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/chrome/tabs/CompositorButton;->setProperty(Lcom/google/android/apps/chrome/tabs/CompositorButton$Property;F)V

    return-void
.end method

.method public setResources(IIII)V
    .locals 0

    .prologue
    .line 87
    iput p1, p0, Lcom/google/android/apps/chrome/tabs/CompositorButton;->mResource:I

    .line 88
    iput p2, p0, Lcom/google/android/apps/chrome/tabs/CompositorButton;->mPressedResource:I

    .line 89
    iput p3, p0, Lcom/google/android/apps/chrome/tabs/CompositorButton;->mIncognitoResource:I

    .line 90
    iput p4, p0, Lcom/google/android/apps/chrome/tabs/CompositorButton;->mIncognitoPressedResource:I

    .line 91
    return-void
.end method

.method public setVisible(Z)V
    .locals 0

    .prologue
    .line 198
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/tabs/CompositorButton;->mIsVisible:Z

    .line 199
    return-void
.end method

.method public setWidth(F)V
    .locals 0

    .prologue
    .line 132
    iput p1, p0, Lcom/google/android/apps/chrome/tabs/CompositorButton;->mWidth:F

    .line 133
    return-void
.end method

.method public setX(F)V
    .locals 0

    .prologue
    .line 104
    iput p1, p0, Lcom/google/android/apps/chrome/tabs/CompositorButton;->mX:F

    .line 105
    return-void
.end method

.method public setY(F)V
    .locals 0

    .prologue
    .line 118
    iput p1, p0, Lcom/google/android/apps/chrome/tabs/CompositorButton;->mY:F

    .line 119
    return-void
.end method
