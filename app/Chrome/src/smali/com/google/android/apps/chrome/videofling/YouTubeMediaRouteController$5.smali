.class Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$5;
.super Landroid/content/BroadcastReceiver;
.source "YouTubeMediaRouteController.java"


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;)V
    .locals 0

    .prologue
    .line 334
    iput-object p1, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$5;->this$0:Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 337
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$5;->this$0:Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;

    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string/jumbo v2, "pairingCode"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mPairingCode:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->access$802(Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;Ljava/lang/String;)Ljava/lang/String;

    .line 338
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$5;->this$0:Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;

    # getter for: Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mPairingCode:Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->access$800(Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$5;->this$0:Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;

    # getter for: Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mPairingCode:Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->access$800(Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 339
    :cond_0
    const-string/jumbo v0, "YouTubeMediaRouteController"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Received an empty pairing code for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$5;->this$0:Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;

    # getter for: Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mYouTubeLocalUrl:Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->access$600(Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 340
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->castYouTubePlayerResult(Z)V

    .line 347
    :goto_0
    return-void

    .line 343
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$5;->this$0:Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;

    # invokes: Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->applyPairingCode()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->access$900(Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;)V

    .line 345
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$5;->this$0:Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$5;->this$0:Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;

    # getter for: Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mYTPairingBroadcastReceiver:Landroid/content/BroadcastReceiver;
    invoke-static {v1}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->access$1000(Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;)Landroid/content/BroadcastReceiver;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 346
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$5;->this$0:Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mYTPairingBroadcastReceiver:Landroid/content/BroadcastReceiver;
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->access$1002(Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;Landroid/content/BroadcastReceiver;)Landroid/content/BroadcastReceiver;

    goto :goto_0
.end method
