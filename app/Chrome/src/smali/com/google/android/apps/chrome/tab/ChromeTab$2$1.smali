.class Lcom/google/android/apps/chrome/tab/ChromeTab$2$1;
.super Ljava/lang/Object;
.source "ChromeTab.java"

# interfaces
.implements Lorg/chromium/content/browser/ContentReadbackHandler$GetBitmapCallback;


# instance fields
.field final synthetic this$1:Lcom/google/android/apps/chrome/tab/ChromeTab$2;

.field final synthetic val$requestedUrl:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/tab/ChromeTab$2;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 446
    iput-object p1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$2$1;->this$1:Lcom/google/android/apps/chrome/tab/ChromeTab$2;

    iput-object p2, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$2$1;->val$requestedUrl:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFinishGetBitmap(Landroid/graphics/Bitmap;)V
    .locals 2

    .prologue
    .line 449
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$2$1;->val$requestedUrl:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$2$1;->this$1:Lcom/google/android/apps/chrome/tab/ChromeTab$2;

    iget-object v1, v1, Lcom/google/android/apps/chrome/tab/ChromeTab$2;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 452
    :cond_0
    :goto_0
    return-void

    .line 450
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$2$1;->this$1:Lcom/google/android/apps/chrome/tab/ChromeTab$2;

    iget-object v0, v0, Lcom/google/android/apps/chrome/tab/ChromeTab$2;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # invokes: Lcom/google/android/apps/chrome/tab/ChromeTab;->updateHistoryThumbnail(Landroid/graphics/Bitmap;)V
    invoke-static {v0, p1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$1000(Lcom/google/android/apps/chrome/tab/ChromeTab;Landroid/graphics/Bitmap;)V

    .line 451
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_0
.end method
