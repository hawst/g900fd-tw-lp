.class public final Lcom/google/android/apps/chrome/autofill/AutofillDataProvider$CreditCardColumns;
.super Ljava/lang/Object;
.source "AutofillDataProvider.java"


# static fields
.field public static final CREDIT_CARD_NUMBER:Ljava/lang/String; = "CREDIT_CARD_NUMBER"

.field public static final EXPIRATION_MONTH:Ljava/lang/String; = "EXPIRATION_MONTH"

.field public static final EXPIRATION_YEAR:Ljava/lang/String; = "EXPIRATION_YEAR"

.field public static final GUID:Ljava/lang/String; = "GUID"

.field public static final IS_DEFAULT_CREDIT_CARD:Ljava/lang/String; = "IS_DEFAULT_CREDIT_CARD"

.field public static final NAME_FULL:Ljava/lang/String; = "NAME_FULL"

.field public static final ORIGIN:Ljava/lang/String; = "ORIGIN"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 118
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
