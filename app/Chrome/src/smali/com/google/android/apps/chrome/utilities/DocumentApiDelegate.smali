.class public abstract Lcom/google/android/apps/chrome/utilities/DocumentApiDelegate;
.super Ljava/lang/Object;
.source "DocumentApiDelegate.java"


# static fields
.field static final DELEGATE_IMPL_CLASSNAME:Ljava/lang/String; = "com.google.android.apps.chrome.utilities.DocumentUtilities"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create()Lcom/google/android/apps/chrome/utilities/DocumentApiDelegate;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 29
    :try_start_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x15

    if-ge v0, v2, :cond_0

    move-object v0, v1

    .line 40
    :goto_0
    return-object v0

    .line 30
    :cond_0
    const-string/jumbo v0, "com.google.android.apps.chrome.utilities.DocumentUtilities"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 31
    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/utilities/DocumentApiDelegate;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_0

    .line 32
    :catch_0
    move-exception v0

    .line 33
    invoke-virtual {v0}, Ljava/lang/ClassNotFoundException;->printStackTrace()V

    :goto_1
    move-object v0, v1

    .line 40
    goto :goto_0

    .line 34
    :catch_1
    move-exception v0

    .line 35
    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_1

    .line 36
    :catch_2
    move-exception v0

    .line 37
    invoke-virtual {v0}, Ljava/lang/InstantiationException;->printStackTrace()V

    goto :goto_1
.end method


# virtual methods
.method public abstract finishAndRemoveTask(Landroid/app/Activity;)V
.end method

.method public abstract finishOtherTasksWithData(Landroid/net/Uri;I)Landroid/content/Intent;
.end method

.method public abstract migrateTabs(ZLandroid/app/Activity;Z)V
.end method

.method public abstract updateTaskDescription(Landroid/app/Activity;Ljava/lang/String;Landroid/graphics/Bitmap;IZ)V
.end method
