.class Lcom/google/android/apps/chrome/omnibox/GeolocationTracker$SelfCancelingListener;
.super Ljava/lang/Object;
.source "GeolocationTracker.java"

# interfaces
.implements Landroid/location/LocationListener;


# instance fields
.field private final mCancelRunnable:Ljava/lang/Runnable;

.field private final mHandler:Landroid/os/Handler;

.field private final mLocationManager:Landroid/location/LocationManager;


# direct methods
.method private constructor <init>(Landroid/location/LocationManager;)V
    .locals 4

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/google/android/apps/chrome/omnibox/GeolocationTracker$SelfCancelingListener;->mLocationManager:Landroid/location/LocationManager;

    .line 39
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/omnibox/GeolocationTracker$SelfCancelingListener;->mHandler:Landroid/os/Handler;

    .line 40
    new-instance v0, Lcom/google/android/apps/chrome/omnibox/GeolocationTracker$SelfCancelingListener$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/omnibox/GeolocationTracker$SelfCancelingListener$1;-><init>(Lcom/google/android/apps/chrome/omnibox/GeolocationTracker$SelfCancelingListener;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/omnibox/GeolocationTracker$SelfCancelingListener;->mCancelRunnable:Ljava/lang/Runnable;

    .line 47
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/GeolocationTracker$SelfCancelingListener;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/GeolocationTracker$SelfCancelingListener;->mCancelRunnable:Ljava/lang/Runnable;

    const-wide/32 v2, 0xea60

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 48
    return-void
.end method

.method synthetic constructor <init>(Landroid/location/LocationManager;Lcom/google/android/apps/chrome/omnibox/GeolocationTracker$1;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/omnibox/GeolocationTracker$SelfCancelingListener;-><init>(Landroid/location/LocationManager;)V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/omnibox/GeolocationTracker$SelfCancelingListener;)Landroid/location/LocationManager;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/GeolocationTracker$SelfCancelingListener;->mLocationManager:Landroid/location/LocationManager;

    return-object v0
.end method


# virtual methods
.method public onLocationChanged(Landroid/location/Location;)V
    .locals 2

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/GeolocationTracker$SelfCancelingListener;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/GeolocationTracker$SelfCancelingListener;->mCancelRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 53
    const/4 v0, 0x0

    # setter for: Lcom/google/android/apps/chrome/omnibox/GeolocationTracker;->sListener:Lcom/google/android/apps/chrome/omnibox/GeolocationTracker$SelfCancelingListener;
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/GeolocationTracker;->access$102(Lcom/google/android/apps/chrome/omnibox/GeolocationTracker$SelfCancelingListener;)Lcom/google/android/apps/chrome/omnibox/GeolocationTracker$SelfCancelingListener;

    .line 54
    return-void
.end method

.method public onProviderDisabled(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 57
    return-void
.end method

.method public onProviderEnabled(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 60
    return-void
.end method

.method public onStatusChanged(Ljava/lang/String;ILandroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 63
    return-void
.end method
