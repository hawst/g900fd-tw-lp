.class Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListView$2;
.super Ljava/lang/Object;
.source "EnhancedBookmarkDrawerListView.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    const-class v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListView;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListView$2;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListView;)V
    .locals 0

    .prologue
    .line 38
    iput-object p1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListView$2;->this$0:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListView$2;->this$0:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListView;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v0}, Landroid/support/v4/widget/DrawerLayout;->a()V

    .line 43
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListView$2;->this$0:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListView;

    # getter for: Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListView;->mAdapter:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListViewAdapter;
    invoke-static {v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListView;->access$100(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListView;)Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListViewAdapter;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListViewAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListViewAdapter$Item;

    .line 46
    iget v1, v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListViewAdapter$Item;->mType:I

    packed-switch v1, :pswitch_data_0

    .line 57
    sget-boolean v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListView$2;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 48
    :pswitch_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListView$2;->this$0:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListView;

    # getter for: Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListView;->mDelegate:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;
    invoke-static {v1}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListView;->access$000(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListView;)Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;

    move-result-object v1

    iget-object v0, v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListViewAdapter$Item;->mFolderId:Lorg/chromium/components/bookmarks/BookmarkId;

    invoke-interface {v1, v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;->setFolderMode(Lorg/chromium/components/bookmarks/BookmarkId;)V

    .line 59
    :cond_0
    :goto_0
    return-void

    .line 51
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListView$2;->this$0:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListView;

    # getter for: Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListView;->mDelegate:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;
    invoke-static {v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListView;->access$000(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListView;)Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;->setAllBookmarksMode()V

    goto :goto_0

    .line 54
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListView$2;->this$0:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListView;

    # getter for: Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListView;->mDelegate:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;
    invoke-static {v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListView;->access$000(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDrawerListView;)Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;->setUncategorizedMode()V

    goto :goto_0

    .line 46
    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
