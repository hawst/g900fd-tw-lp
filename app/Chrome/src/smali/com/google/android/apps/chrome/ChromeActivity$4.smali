.class Lcom/google/android/apps/chrome/ChromeActivity$4;
.super Ljava/lang/Object;
.source "ChromeActivity.java"

# interfaces
.implements Lcom/google/android/apps/chrome/nfc/BeamProvider;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/ChromeActivity;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/ChromeActivity;)V
    .locals 0

    .prologue
    .line 711
    iput-object p1, p0, Lcom/google/android/apps/chrome/ChromeActivity$4;->this$0:Lcom/google/android/apps/chrome/ChromeActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getTabUrlForBeam()Ljava/lang/String;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 714
    iget-object v1, p0, Lcom/google/android/apps/chrome/ChromeActivity$4;->this$0:Lcom/google/android/apps/chrome/ChromeActivity;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/ChromeActivity;->isOverlayVisible()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 716
    :cond_0
    :goto_0
    return-object v0

    .line 715
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/chrome/ChromeActivity$4;->this$0:Lcom/google/android/apps/chrome/ChromeActivity;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/ChromeActivity;->getCurrentTab()Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 716
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeActivity$4;->this$0:Lcom/google/android/apps/chrome/ChromeActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeActivity;->getCurrentTab()Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getUrl()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
