.class Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;
.super Landroid/view/ViewGroup;
.source "SuggestionView.java"

# interfaces
.implements Landroid/view/View$OnLayoutChangeListener;


# instance fields
.field private final mAnswerImage:Landroid/widget/ImageView;

.field private mAnswerImageMaxSize:I

.field private mForceIsFocused:Z

.field private mMatchContentsWidth:F

.field private final mRelayoutRunnable:Ljava/lang/Runnable;

.field private mRequiredWidth:F

.field private mSuggestionIcon:Landroid/graphics/drawable/Drawable;

.field private mSuggestionIconLeft:I

.field private mSuggestionIconType:Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionIconType;

.field private mTextLeft:I

.field private final mTextLine1:Landroid/widget/TextView;

.field private final mTextLine2:Landroid/widget/TextView;

.field private mTextRight:I

.field final synthetic this$0:Lcom/google/android/apps/chrome/omnibox/SuggestionView;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/omnibox/SuggestionView;Landroid/content/Context;Landroid/graphics/drawable/Drawable;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v4, -0x2

    const/4 v3, 0x0

    const/high16 v0, -0x80000000

    .line 625
    iput-object p1, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->this$0:Lcom/google/android/apps/chrome/omnibox/SuggestionView;

    .line 626
    invoke-direct {p0, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 603
    iput v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mSuggestionIconLeft:I

    .line 604
    iput v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mTextLeft:I

    .line 605
    iput v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mTextRight:I

    .line 618
    new-instance v0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer$1;-><init>(Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mRelayoutRunnable:Ljava/lang/Runnable;

    .line 628
    const/4 v0, 0x2

    invoke-static {p0, v0}, Lorg/chromium/base/ApiCompatibilityUtils;->setLayoutDirection(Landroid/view/View;I)V

    .line 630
    invoke-static {p0, p3}, Lorg/chromium/base/ApiCompatibilityUtils;->setBackgroundForView(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 631
    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->setClickable(Z)V

    .line 632
    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->setFocusable(Z)V

    .line 633
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    const/4 v1, -0x1

    # getter for: Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mSuggestionHeight:I
    invoke-static {p1}, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->access$1400(Lcom/google/android/apps/chrome/omnibox/SuggestionView;)I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 634
    new-instance v0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer$2;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer$2;-><init>(Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;Lcom/google/android/apps/chrome/omnibox/SuggestionView;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 643
    new-instance v0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer$3;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer$3;-><init>(Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;Lcom/google/android/apps/chrome/omnibox/SuggestionView;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 684
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mTextLine1:Landroid/widget/TextView;

    .line 685
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mTextLine1:Landroid/widget/TextView;

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    # getter for: Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mSuggestionHeight:I
    invoke-static {p1}, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->access$1400(Lcom/google/android/apps/chrome/omnibox/SuggestionView;)I

    move-result v2

    invoke-direct {v1, v4, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 687
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mTextLine1:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->setSingleLine()V

    .line 688
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mTextLine1:Landroid/widget/TextView;

    # invokes: Lcom/google/android/apps/chrome/omnibox/SuggestionView;->getStandardFontColor()I
    invoke-static {p1}, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->access$1600(Lcom/google/android/apps/chrome/omnibox/SuggestionView;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 689
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mTextLine1:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->addView(Landroid/view/View;)V

    .line 691
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mTextLine2:Landroid/widget/TextView;

    .line 692
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mTextLine2:Landroid/widget/TextView;

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    # getter for: Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mSuggestionHeight:I
    invoke-static {p1}, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->access$1400(Lcom/google/android/apps/chrome/omnibox/SuggestionView;)I

    move-result v2

    invoke-direct {v1, v4, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 694
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mTextLine2:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->setSingleLine()V

    .line 695
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mTextLine2:Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 696
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mTextLine2:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->addView(Landroid/view/View;)V

    .line 698
    new-instance v0, Landroid/widget/ImageView;

    invoke-direct {v0, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mAnswerImage:Landroid/widget/ImageView;

    .line 699
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mAnswerImage:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 700
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mAnswerImage:Landroid/widget/ImageView;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->FIT_CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 701
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mAnswerImage:Landroid/widget/ImageView;

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v1, v3, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 702
    iput v3, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mAnswerImageMaxSize:I

    .line 703
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mAnswerImage:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->addView(Landroid/view/View;)V

    .line 704
    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;)V
    .locals 0

    .prologue
    .line 602
    invoke-direct {p0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->resetTextWidths()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 602
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mTextLine1:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 602
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mAnswerImage:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$402(Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;I)I
    .locals 0

    .prologue
    .line 602
    iput p1, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mAnswerImageMaxSize:I

    return p1
.end method

.method static synthetic access$500(Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 602
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mTextLine2:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionIconType;Z)V
    .locals 0

    .prologue
    .line 602
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->setSuggestionIcon(Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionIconType;Z)V

    return-void
.end method

.method static synthetic access$800(Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;)F
    .locals 1

    .prologue
    .line 602
    iget v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mRequiredWidth:F

    return v0
.end method

.method static synthetic access$802(Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;F)F
    .locals 0

    .prologue
    .line 602
    iput p1, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mRequiredWidth:F

    return p1
.end method

.method static synthetic access$900(Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;)F
    .locals 1

    .prologue
    .line 602
    iget v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mMatchContentsWidth:F

    return v0
.end method

.method static synthetic access$902(Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;F)F
    .locals 0

    .prologue
    .line 602
    iput p1, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mMatchContentsWidth:F

    return p1
.end method

.method private getSuggestionIconLeftPosition()I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 866
    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->this$0:Lcom/google/android/apps/chrome/omnibox/SuggestionView;

    # getter for: Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mNavigationButton:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->access$1700(Lcom/google/android/apps/chrome/omnibox/SuggestionView;)Landroid/widget/ImageView;

    move-result-object v1

    if-nez v1, :cond_0

    .line 875
    :goto_0
    return v0

    .line 870
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->this$0:Lcom/google/android/apps/chrome/omnibox/SuggestionView;

    # getter for: Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mNavigationButton:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->access$1700(Lcom/google/android/apps/chrome/omnibox/SuggestionView;)Landroid/widget/ImageView;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->this$0:Lcom/google/android/apps/chrome/omnibox/SuggestionView;

    # getter for: Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mViewPositionHolder:[I
    invoke-static {v2}, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->access$2300(Lcom/google/android/apps/chrome/omnibox/SuggestionView;)[I

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->getLocationOnScreen([I)V

    .line 871
    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->this$0:Lcom/google/android/apps/chrome/omnibox/SuggestionView;

    # getter for: Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mViewPositionHolder:[I
    invoke-static {v1}, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->access$2300(Lcom/google/android/apps/chrome/omnibox/SuggestionView;)[I

    move-result-object v1

    aget v1, v1, v0

    iget-object v2, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->this$0:Lcom/google/android/apps/chrome/omnibox/SuggestionView;

    # getter for: Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mNavigationButton:Landroid/widget/ImageView;
    invoke-static {v2}, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->access$1700(Lcom/google/android/apps/chrome/omnibox/SuggestionView;)Landroid/widget/ImageView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/ImageView;->getPaddingLeft()I

    move-result v2

    add-int/2addr v1, v2

    .line 873
    iget-object v2, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->this$0:Lcom/google/android/apps/chrome/omnibox/SuggestionView;

    # getter for: Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mViewPositionHolder:[I
    invoke-static {v2}, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->access$2300(Lcom/google/android/apps/chrome/omnibox/SuggestionView;)[I

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->getLocationOnScreen([I)V

    .line 875
    iget-object v2, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->this$0:Lcom/google/android/apps/chrome/omnibox/SuggestionView;

    # getter for: Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mViewPositionHolder:[I
    invoke-static {v2}, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->access$2300(Lcom/google/android/apps/chrome/omnibox/SuggestionView;)[I

    move-result-object v2

    aget v0, v2, v0

    sub-int v0, v1, v0

    goto :goto_0
.end method

.method private getSuggestionTextLeftPosition()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 834
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->this$0:Lcom/google/android/apps/chrome/omnibox/SuggestionView;

    # getter for: Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mLocationBar:Lcom/google/android/apps/chrome/omnibox/LocationBar;
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->access$1900(Lcom/google/android/apps/chrome/omnibox/SuggestionView;)Lcom/google/android/apps/chrome/omnibox/LocationBar;

    move-result-object v0

    if-nez v0, :cond_0

    move v0, v1

    .line 843
    :goto_0
    return v0

    .line 836
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->this$0:Lcom/google/android/apps/chrome/omnibox/SuggestionView;

    # getter for: Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mCorpusChipTextView:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->access$2000(Lcom/google/android/apps/chrome/omnibox/SuggestionView;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/TextView;->isShown()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 838
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->this$0:Lcom/google/android/apps/chrome/omnibox/SuggestionView;

    # invokes: Lcom/google/android/apps/chrome/omnibox/SuggestionView;->getCorpusChipTextViewLeftPosition()I
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->access$2100(Lcom/google/android/apps/chrome/omnibox/SuggestionView;)I

    move-result v0

    .line 842
    :goto_1
    iget-object v2, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->this$0:Lcom/google/android/apps/chrome/omnibox/SuggestionView;

    # getter for: Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mViewPositionHolder:[I
    invoke-static {v2}, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->access$2300(Lcom/google/android/apps/chrome/omnibox/SuggestionView;)[I

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->getLocationOnScreen([I)V

    .line 843
    iget-object v2, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->this$0:Lcom/google/android/apps/chrome/omnibox/SuggestionView;

    # getter for: Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mViewPositionHolder:[I
    invoke-static {v2}, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->access$2300(Lcom/google/android/apps/chrome/omnibox/SuggestionView;)[I

    move-result-object v2

    aget v1, v2, v1

    sub-int/2addr v0, v1

    goto :goto_0

    .line 840
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->this$0:Lcom/google/android/apps/chrome/omnibox/SuggestionView;

    # invokes: Lcom/google/android/apps/chrome/omnibox/SuggestionView;->getUrlBarTextLeftPosition()I
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->access$2200(Lcom/google/android/apps/chrome/omnibox/SuggestionView;)I

    move-result v0

    goto :goto_1
.end method

.method private getSuggestionTextRightPosition()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 850
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->this$0:Lcom/google/android/apps/chrome/omnibox/SuggestionView;

    # getter for: Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mLocationBar:Lcom/google/android/apps/chrome/omnibox/LocationBar;
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->access$1900(Lcom/google/android/apps/chrome/omnibox/SuggestionView;)Lcom/google/android/apps/chrome/omnibox/LocationBar;

    move-result-object v0

    if-nez v0, :cond_0

    move v0, v1

    .line 859
    :goto_0
    return v0

    .line 852
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->this$0:Lcom/google/android/apps/chrome/omnibox/SuggestionView;

    # getter for: Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mCorpusChipTextView:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->access$2000(Lcom/google/android/apps/chrome/omnibox/SuggestionView;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/TextView;->isShown()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 854
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->this$0:Lcom/google/android/apps/chrome/omnibox/SuggestionView;

    # invokes: Lcom/google/android/apps/chrome/omnibox/SuggestionView;->getCorpusChipTextViewRightPosition()I
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->access$2400(Lcom/google/android/apps/chrome/omnibox/SuggestionView;)I

    move-result v0

    .line 858
    :goto_1
    iget-object v2, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->this$0:Lcom/google/android/apps/chrome/omnibox/SuggestionView;

    # getter for: Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mViewPositionHolder:[I
    invoke-static {v2}, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->access$2300(Lcom/google/android/apps/chrome/omnibox/SuggestionView;)[I

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->getLocationOnScreen([I)V

    .line 859
    iget-object v2, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->this$0:Lcom/google/android/apps/chrome/omnibox/SuggestionView;

    # getter for: Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mViewPositionHolder:[I
    invoke-static {v2}, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->access$2300(Lcom/google/android/apps/chrome/omnibox/SuggestionView;)[I

    move-result-object v2

    aget v1, v2, v1

    sub-int/2addr v0, v1

    goto :goto_0

    .line 856
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->this$0:Lcom/google/android/apps/chrome/omnibox/SuggestionView;

    # invokes: Lcom/google/android/apps/chrome/omnibox/SuggestionView;->getUrlBarTextRightPosition()I
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->access$2500(Lcom/google/android/apps/chrome/omnibox/SuggestionView;)I

    move-result v0

    goto :goto_1
.end method

.method private resetTextWidths()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 707
    iput v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mRequiredWidth:F

    .line 708
    iput v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mMatchContentsWidth:F

    .line 709
    return-void
.end method

.method private setSuggestionIcon(Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionIconType;Z)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 934
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mSuggestionIconType:Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionIconType;

    if-ne v0, p1, :cond_0

    if-nez p2, :cond_0

    .line 965
    :goto_0
    return-void

    .line 936
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->this$0:Lcom/google/android/apps/chrome/omnibox/SuggestionView;

    # getter for: Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mUseDarkColors:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->access$2600(Lcom/google/android/apps/chrome/omnibox/SuggestionView;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget v0, Lcom/google/android/apps/chrome/R$drawable;->ic_suggestion_page:I

    .line 938
    :goto_1
    sget-object v1, Lcom/google/android/apps/chrome/omnibox/SuggestionView$4;->$SwitchMap$com$google$android$apps$chrome$omnibox$SuggestionView$SuggestionIconType:[I

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionIconType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 954
    :goto_2
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mSuggestionIcon:Landroid/graphics/drawable/Drawable;

    .line 959
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mSuggestionIcon:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mSuggestionIcon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mSuggestionIcon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 963
    iput-object p1, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mSuggestionIconType:Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionIconType;

    .line 964
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->invalidate()V

    goto :goto_0

    .line 936
    :cond_1
    sget v0, Lcom/google/android/apps/chrome/R$drawable;->ic_suggestion_page_white:I

    goto :goto_1

    .line 940
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->this$0:Lcom/google/android/apps/chrome/omnibox/SuggestionView;

    # getter for: Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mUseDarkColors:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->access$2600(Lcom/google/android/apps/chrome/omnibox/SuggestionView;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget v0, Lcom/google/android/apps/chrome/R$drawable;->ic_suggestion_bookmark:I

    goto :goto_2

    :cond_2
    sget v0, Lcom/google/android/apps/chrome/R$drawable;->ic_suggestion_bookmark_white:I

    goto :goto_2

    .line 944
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->this$0:Lcom/google/android/apps/chrome/omnibox/SuggestionView;

    # getter for: Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mUseDarkColors:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->access$2600(Lcom/google/android/apps/chrome/omnibox/SuggestionView;)Z

    move-result v0

    if-eqz v0, :cond_3

    sget v0, Lcom/google/android/apps/chrome/R$drawable;->ic_suggestion_magnifier:I

    goto :goto_2

    :cond_3
    sget v0, Lcom/google/android/apps/chrome/R$drawable;->ic_suggestion_magnifier_white:I

    goto :goto_2

    .line 948
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->this$0:Lcom/google/android/apps/chrome/omnibox/SuggestionView;

    # getter for: Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mUseDarkColors:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->access$2600(Lcom/google/android/apps/chrome/omnibox/SuggestionView;)Z

    move-result v0

    if-eqz v0, :cond_4

    sget v0, Lcom/google/android/apps/chrome/R$drawable;->ic_suggestion_history:I

    goto :goto_2

    :cond_4
    sget v0, Lcom/google/android/apps/chrome/R$drawable;->ic_suggestion_history_white:I

    goto :goto_2

    .line 952
    :pswitch_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->this$0:Lcom/google/android/apps/chrome/omnibox/SuggestionView;

    # getter for: Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mUseDarkColors:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->access$2600(Lcom/google/android/apps/chrome/omnibox/SuggestionView;)Z

    move-result v0

    if-eqz v0, :cond_5

    sget v0, Lcom/google/android/apps/chrome/R$drawable;->btn_omnibox_mic_normal:I

    goto :goto_2

    :cond_5
    sget v0, Lcom/google/android/apps/chrome/R$drawable;->btn_omnibox_mic_white_normal:I

    goto :goto_2

    .line 938
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method protected drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 732
    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mTextLine1:Landroid/widget/TextView;

    if-eq p2, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mTextLine2:Landroid/widget/TextView;

    if-eq p2, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mAnswerImage:Landroid/widget/ImageView;

    if-eq p2, v1, :cond_0

    .line 733
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/ViewGroup;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    move-result v0

    .line 775
    :goto_0
    return v0

    .line 736
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->getMeasuredHeight()I

    move-result v2

    .line 737
    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mTextLine1:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v3

    .line 738
    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mTextLine2:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getVisibility()I

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mTextLine2:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v1

    .line 741
    :goto_1
    add-int v4, v3, v1

    if-le v4, v2, :cond_4

    .line 745
    iget-object v3, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mTextLine1:Landroid/widget/TextView;

    if-ne p2, v3, :cond_3

    .line 771
    :cond_1
    :goto_2
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 772
    const/4 v1, 0x0

    int-to-float v0, v0

    invoke-virtual {p1, v1, v0}, Landroid/graphics/Canvas;->translate(FF)V

    .line 773
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/ViewGroup;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    move-result v0

    .line 774
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    goto :goto_0

    :cond_2
    move v1, v0

    .line 738
    goto :goto_1

    .line 748
    :cond_3
    sub-int v0, v2, v1

    goto :goto_2

    .line 752
    :cond_4
    sub-int v0, v2, v3

    sub-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    .line 753
    iget-object v2, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mTextLine2:Landroid/widget/TextView;

    if-ne p2, v2, :cond_5

    add-int/2addr v0, v3

    .line 758
    :cond_5
    if-eq v3, v1, :cond_6

    .line 759
    sub-int/2addr v1, v3

    div-int/lit8 v1, v1, 0xa

    add-int/2addr v0, v1

    .line 765
    :cond_6
    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mAnswerImage:Landroid/widget/ImageView;

    if-ne p2, v1, :cond_1

    .line 766
    int-to-float v0, v0

    const/high16 v1, 0x40a00000    # 5.0f

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    float-to-int v0, v0

    goto :goto_2
.end method

.method public invalidate()V
    .locals 4

    .prologue
    .line 902
    invoke-direct {p0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->getSuggestionTextLeftPosition()I

    move-result v0

    iget v1, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mTextLeft:I

    if-ne v0, v1, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->getSuggestionTextRightPosition()I

    move-result v0

    iget v1, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mTextRight:I

    if-eq v0, v1, :cond_1

    .line 909
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mRelayoutRunnable:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 910
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mRelayoutRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x14

    invoke-virtual {p0, v0, v2, v3}, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 914
    :goto_0
    return-void

    .line 912
    :cond_1
    invoke-super {p0}, Landroid/view/ViewGroup;->invalidate()V

    goto :goto_0
.end method

.method public isFocused()Z
    .locals 1

    .prologue
    .line 918
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mForceIsFocused:Z

    if-nez v0, :cond_0

    invoke-super {p0}, Landroid/view/ViewGroup;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .prologue
    .line 995
    invoke-super {p0}, Landroid/view/ViewGroup;->onAttachedToWindow()V

    .line 996
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->this$0:Lcom/google/android/apps/chrome/omnibox/SuggestionView;

    # getter for: Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mNavigationButton:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->access$1700(Lcom/google/android/apps/chrome/omnibox/SuggestionView;)Landroid/widget/ImageView;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->this$0:Lcom/google/android/apps/chrome/omnibox/SuggestionView;

    # getter for: Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mNavigationButton:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->access$1700(Lcom/google/android/apps/chrome/omnibox/SuggestionView;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 997
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->this$0:Lcom/google/android/apps/chrome/omnibox/SuggestionView;

    # getter for: Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mUrlBar:Lcom/google/android/apps/chrome/omnibox/UrlBar;
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->access$1800(Lcom/google/android/apps/chrome/omnibox/SuggestionView;)Lcom/google/android/apps/chrome/omnibox/UrlBar;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->this$0:Lcom/google/android/apps/chrome/omnibox/SuggestionView;

    # getter for: Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mUrlBar:Lcom/google/android/apps/chrome/omnibox/UrlBar;
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->access$1800(Lcom/google/android/apps/chrome/omnibox/SuggestionView;)Lcom/google/android/apps/chrome/omnibox/UrlBar;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 998
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->this$0:Lcom/google/android/apps/chrome/omnibox/SuggestionView;

    # getter for: Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mLocationBar:Lcom/google/android/apps/chrome/omnibox/LocationBar;
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->access$1900(Lcom/google/android/apps/chrome/omnibox/SuggestionView;)Lcom/google/android/apps/chrome/omnibox/LocationBar;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->this$0:Lcom/google/android/apps/chrome/omnibox/SuggestionView;

    # getter for: Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mLocationBar:Lcom/google/android/apps/chrome/omnibox/LocationBar;
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->access$1900(Lcom/google/android/apps/chrome/omnibox/SuggestionView;)Lcom/google/android/apps/chrome/omnibox/LocationBar;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 999
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->getRootView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 1000
    return-void
.end method

.method protected onCreateDrawableState(I)[I
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 927
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->isSelected()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->isInTouchMode()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mForceIsFocused:Z

    .line 928
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onCreateDrawableState(I)[I

    move-result-object v0

    .line 929
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mForceIsFocused:Z

    .line 930
    return-object v0

    :cond_0
    move v0, v1

    .line 927
    goto :goto_0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 1004
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->this$0:Lcom/google/android/apps/chrome/omnibox/SuggestionView;

    # getter for: Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mNavigationButton:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->access$1700(Lcom/google/android/apps/chrome/omnibox/SuggestionView;)Landroid/widget/ImageView;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->this$0:Lcom/google/android/apps/chrome/omnibox/SuggestionView;

    # getter for: Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mNavigationButton:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->access$1700(Lcom/google/android/apps/chrome/omnibox/SuggestionView;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->removeOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 1005
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->this$0:Lcom/google/android/apps/chrome/omnibox/SuggestionView;

    # getter for: Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mUrlBar:Lcom/google/android/apps/chrome/omnibox/UrlBar;
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->access$1800(Lcom/google/android/apps/chrome/omnibox/SuggestionView;)Lcom/google/android/apps/chrome/omnibox/UrlBar;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->this$0:Lcom/google/android/apps/chrome/omnibox/SuggestionView;

    # getter for: Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mUrlBar:Lcom/google/android/apps/chrome/omnibox/UrlBar;
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->access$1800(Lcom/google/android/apps/chrome/omnibox/SuggestionView;)Lcom/google/android/apps/chrome/omnibox/UrlBar;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->removeOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 1006
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->this$0:Lcom/google/android/apps/chrome/omnibox/SuggestionView;

    # getter for: Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mLocationBar:Lcom/google/android/apps/chrome/omnibox/LocationBar;
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->access$1900(Lcom/google/android/apps/chrome/omnibox/SuggestionView;)Lcom/google/android/apps/chrome/omnibox/LocationBar;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->this$0:Lcom/google/android/apps/chrome/omnibox/SuggestionView;

    # getter for: Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mLocationBar:Lcom/google/android/apps/chrome/omnibox/LocationBar;
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->access$1900(Lcom/google/android/apps/chrome/omnibox/SuggestionView;)Lcom/google/android/apps/chrome/omnibox/LocationBar;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->removeOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 1007
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->getRootView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->removeOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 1009
    invoke-super {p0}, Landroid/view/ViewGroup;->onDetachedFromWindow()V

    .line 1010
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 3

    .prologue
    .line 713
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onDraw(Landroid/graphics/Canvas;)V

    .line 715
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/ui/base/DeviceFormFactor;->isTablet(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 719
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 720
    invoke-direct {p0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->getSuggestionIconLeftPosition()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mSuggestionIconLeft:I

    .line 721
    iget v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mSuggestionIconLeft:I

    int-to-float v0, v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->getMeasuredHeight()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->this$0:Lcom/google/android/apps/chrome/omnibox/SuggestionView;

    # getter for: Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mNavigationButton:Landroid/widget/ImageView;
    invoke-static {v2}, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->access$1700(Lcom/google/android/apps/chrome/omnibox/SuggestionView;)Landroid/widget/ImageView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v2

    sub-int/2addr v1, v2

    int-to-float v1, v1

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 724
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->this$0:Lcom/google/android/apps/chrome/omnibox/SuggestionView;

    # getter for: Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mNavigationButton:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->access$1700(Lcom/google/android/apps/chrome/omnibox/SuggestionView;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ImageView;->getImageMatrix()Landroid/graphics/Matrix;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->concat(Landroid/graphics/Matrix;)V

    .line 725
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mSuggestionIcon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 726
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 728
    :cond_0
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 780
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->this$0:Lcom/google/android/apps/chrome/omnibox/SuggestionView;

    # getter for: Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mUrlBar:Lcom/google/android/apps/chrome/omnibox/UrlBar;
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->access$1800(Lcom/google/android/apps/chrome/omnibox/SuggestionView;)Lcom/google/android/apps/chrome/omnibox/UrlBar;

    move-result-object v0

    if-nez v0, :cond_0

    .line 781
    iget-object v2, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->this$0:Lcom/google/android/apps/chrome/omnibox/SuggestionView;

    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->this$0:Lcom/google/android/apps/chrome/omnibox/SuggestionView;

    # getter for: Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mLocationBar:Lcom/google/android/apps/chrome/omnibox/LocationBar;
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->access$1900(Lcom/google/android/apps/chrome/omnibox/SuggestionView;)Lcom/google/android/apps/chrome/omnibox/LocationBar;

    move-result-object v0

    sget v3, Lcom/google/android/apps/chrome/R$id;->url_bar:I

    invoke-virtual {v0, v3}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/omnibox/UrlBar;

    # setter for: Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mUrlBar:Lcom/google/android/apps/chrome/omnibox/UrlBar;
    invoke-static {v2, v0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->access$1802(Lcom/google/android/apps/chrome/omnibox/SuggestionView;Lcom/google/android/apps/chrome/omnibox/UrlBar;)Lcom/google/android/apps/chrome/omnibox/UrlBar;

    .line 782
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->this$0:Lcom/google/android/apps/chrome/omnibox/SuggestionView;

    # getter for: Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mUrlBar:Lcom/google/android/apps/chrome/omnibox/UrlBar;
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->access$1800(Lcom/google/android/apps/chrome/omnibox/SuggestionView;)Lcom/google/android/apps/chrome/omnibox/UrlBar;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 784
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->this$0:Lcom/google/android/apps/chrome/omnibox/SuggestionView;

    # getter for: Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mNavigationButton:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->access$1700(Lcom/google/android/apps/chrome/omnibox/SuggestionView;)Landroid/widget/ImageView;

    move-result-object v0

    if-nez v0, :cond_1

    .line 785
    iget-object v2, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->this$0:Lcom/google/android/apps/chrome/omnibox/SuggestionView;

    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->this$0:Lcom/google/android/apps/chrome/omnibox/SuggestionView;

    # getter for: Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mLocationBar:Lcom/google/android/apps/chrome/omnibox/LocationBar;
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->access$1900(Lcom/google/android/apps/chrome/omnibox/SuggestionView;)Lcom/google/android/apps/chrome/omnibox/LocationBar;

    move-result-object v0

    sget v3, Lcom/google/android/apps/chrome/R$id;->navigation_button:I

    invoke-virtual {v0, v3}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    # setter for: Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mNavigationButton:Landroid/widget/ImageView;
    invoke-static {v2, v0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->access$1702(Lcom/google/android/apps/chrome/omnibox/SuggestionView;Landroid/widget/ImageView;)Landroid/widget/ImageView;

    .line 786
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->this$0:Lcom/google/android/apps/chrome/omnibox/SuggestionView;

    # getter for: Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mNavigationButton:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->access$1700(Lcom/google/android/apps/chrome/omnibox/SuggestionView;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 790
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->getSuggestionTextLeftPosition()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mTextLeft:I

    .line 791
    invoke-direct {p0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->getSuggestionTextRightPosition()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mTextRight:I

    .line 792
    invoke-static {p0}, Lorg/chromium/base/ApiCompatibilityUtils;->isLayoutRtl(Landroid/view/View;)Z

    move-result v2

    .line 793
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/ui/base/DeviceFormFactor;->isTablet(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 794
    if-eqz v2, :cond_4

    iget v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mTextRight:I

    .line 795
    :goto_0
    iget-object v3, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->this$0:Lcom/google/android/apps/chrome/omnibox/SuggestionView;

    # getter for: Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mSuggestionDelegate:Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter$OmniboxSuggestionDelegate;
    invoke-static {v3}, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->access$1300(Lcom/google/android/apps/chrome/omnibox/SuggestionView;)Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter$OmniboxSuggestionDelegate;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter$OmniboxSuggestionDelegate;->getMaxRequiredWidth()F

    move-result v3

    .line 796
    iget-object v4, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->this$0:Lcom/google/android/apps/chrome/omnibox/SuggestionView;

    # getter for: Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mSuggestionDelegate:Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter$OmniboxSuggestionDelegate;
    invoke-static {v4}, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->access$1300(Lcom/google/android/apps/chrome/omnibox/SuggestionView;)Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter$OmniboxSuggestionDelegate;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter$OmniboxSuggestionDelegate;->getMaxMatchContentsWidth()F

    move-result v4

    .line 797
    int-to-float v5, v0

    cmpl-float v3, v5, v3

    if-lez v3, :cond_5

    iget v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mRequiredWidth:F

    iget v3, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mMatchContentsWidth:F

    sub-float/2addr v0, v3

    .line 800
    :goto_1
    iget-object v3, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mTextLine1:Landroid/widget/TextView;

    float-to-int v0, v0

    iget-object v4, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mTextLine1:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getPaddingTop()I

    move-result v4

    iget-object v5, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mTextLine1:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getPaddingBottom()I

    move-result v5

    invoke-static {v3, v0, v4, v1, v5}, Lorg/chromium/base/ApiCompatibilityUtils;->setPaddingRelative(Landroid/view/View;IIII)V

    .line 806
    :cond_2
    iget v3, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mAnswerImageMaxSize:I

    .line 808
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mAnswerImage:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_7

    if-lez v3, :cond_7

    .line 809
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    .line 810
    const/high16 v4, 0x40800000    # 4.0f

    mul-float/2addr v0, v4

    float-to-int v0, v0

    .line 812
    :goto_2
    if-eqz v2, :cond_6

    .line 813
    iget-object v2, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mTextLine1:Landroid/widget/TextView;

    iget v4, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mTextRight:I

    invoke-virtual {v2, v1, p3, v4, p5}, Landroid/widget/TextView;->layout(IIII)V

    .line 814
    iget-object v2, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mAnswerImage:Landroid/widget/ImageView;

    iget v4, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mTextRight:I

    add-int/2addr v4, v3

    iget v5, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mTextRight:I

    invoke-virtual {v2, v4, p3, v5, p5}, Landroid/widget/ImageView;->layout(IIII)V

    .line 815
    iget-object v2, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mTextLine2:Landroid/widget/TextView;

    add-int/2addr v0, v3

    iget v3, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mTextRight:I

    add-int/2addr v0, v3

    invoke-virtual {v2, v1, p3, v0, p5}, Landroid/widget/TextView;->layout(IIII)V

    .line 822
    :goto_3
    invoke-direct {p0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->getSuggestionIconLeftPosition()I

    move-result v0

    .line 823
    iget v1, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mSuggestionIconLeft:I

    if-eq v1, v0, :cond_3

    iget v1, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mSuggestionIconLeft:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_3

    .line 825
    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->this$0:Lcom/google/android/apps/chrome/omnibox/SuggestionView;

    # getter for: Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mContentsView:Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;
    invoke-static {v1}, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->access$1000(Lcom/google/android/apps/chrome/omnibox/SuggestionView;)Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;

    move-result-object v1

    invoke-static {v1}, Lorg/chromium/base/ApiCompatibilityUtils;->postInvalidateOnAnimation(Landroid/view/View;)V

    .line 827
    :cond_3
    iput v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mSuggestionIconLeft:I

    .line 828
    return-void

    .line 794
    :cond_4
    sub-int v0, p4, p2

    iget v3, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mTextLeft:I

    sub-int/2addr v0, v3

    goto/16 :goto_0

    .line 797
    :cond_5
    int-to-float v0, v0

    sub-float/2addr v0, v4

    const/4 v3, 0x0

    invoke-static {v0, v3}, Ljava/lang/Math;->max(FF)F

    move-result v0

    goto :goto_1

    .line 817
    :cond_6
    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mTextLine1:Landroid/widget/TextView;

    iget v2, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mTextLeft:I

    sub-int v4, p4, p2

    invoke-virtual {v1, v2, p3, v4, p5}, Landroid/widget/TextView;->layout(IIII)V

    .line 818
    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mAnswerImage:Landroid/widget/ImageView;

    iget v2, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mTextLeft:I

    iget v4, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mTextLeft:I

    add-int/2addr v4, v3

    invoke-virtual {v1, v2, p3, v4, p5}, Landroid/widget/ImageView;->layout(IIII)V

    .line 819
    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mTextLine2:Landroid/widget/TextView;

    iget v2, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mTextLeft:I

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    sub-int v2, p4, p2

    invoke-virtual {v1, v0, p3, v2, p5}, Landroid/widget/TextView;->layout(IIII)V

    goto :goto_3

    :cond_7
    move v0, v1

    goto :goto_2
.end method

.method public onLayoutChange(Landroid/view/View;IIIIIIII)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/high16 v4, -0x80000000

    .line 971
    const/4 v0, 0x0

    .line 972
    iget-object v2, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->this$0:Lcom/google/android/apps/chrome/omnibox/SuggestionView;

    # getter for: Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mNavigationButton:Landroid/widget/ImageView;
    invoke-static {v2}, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->access$1700(Lcom/google/android/apps/chrome/omnibox/SuggestionView;)Landroid/widget/ImageView;

    move-result-object v2

    if-ne p1, v2, :cond_2

    .line 973
    iget v2, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mSuggestionIconLeft:I

    invoke-direct {p0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->getSuggestionIconLeftPosition()I

    move-result v3

    if-eq v2, v3, :cond_0

    iget v2, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mSuggestionIconLeft:I

    if-eq v2, v4, :cond_0

    move v0, v1

    .line 987
    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    .line 988
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mRelayoutRunnable:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 989
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mRelayoutRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x14

    invoke-virtual {p0, v0, v2, v3}, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 991
    :cond_1
    return-void

    .line 978
    :cond_2
    iget v2, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mTextLeft:I

    invoke-direct {p0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->getSuggestionTextLeftPosition()I

    move-result v3

    if-eq v2, v3, :cond_3

    iget v2, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mTextLeft:I

    if-eq v2, v4, :cond_3

    move v0, v1

    .line 982
    :cond_3
    iget v2, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mTextRight:I

    invoke-direct {p0}, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->getSuggestionTextRightPosition()I

    move-result v3

    if-eq v2, v3, :cond_0

    iget v2, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mTextRight:I

    if-eq v2, v4, :cond_0

    move v0, v1

    .line 984
    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 6

    .prologue
    const/high16 v5, -0x80000000

    .line 880
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->onMeasure(II)V

    .line 882
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 883
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 885
    iget-object v2, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mTextLine1:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v2

    if-ne v2, v0, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mTextLine1:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v2

    if-eq v2, v1, :cond_1

    .line 887
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mTextLine1:Landroid/widget/TextView;

    invoke-static {p1, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    iget-object v4, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->this$0:Lcom/google/android/apps/chrome/omnibox/SuggestionView;

    # getter for: Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mSuggestionHeight:I
    invoke-static {v4}, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->access$1400(Lcom/google/android/apps/chrome/omnibox/SuggestionView;)I

    move-result v4

    invoke-static {v4, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-virtual {v2, v3, v4}, Landroid/widget/TextView;->measure(II)V

    .line 892
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mTextLine2:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v2

    if-ne v2, v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mTextLine2:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v0

    if-eq v0, v1, :cond_3

    .line 894
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->mTextLine2:Landroid/widget/TextView;

    invoke-static {p1, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/omnibox/SuggestionView$SuggestionContentsContainer;->this$0:Lcom/google/android/apps/chrome/omnibox/SuggestionView;

    # getter for: Lcom/google/android/apps/chrome/omnibox/SuggestionView;->mSuggestionHeight:I
    invoke-static {v2}, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->access$1400(Lcom/google/android/apps/chrome/omnibox/SuggestionView;)I

    move-result v2

    invoke-static {v2, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->measure(II)V

    .line 898
    :cond_3
    return-void
.end method
