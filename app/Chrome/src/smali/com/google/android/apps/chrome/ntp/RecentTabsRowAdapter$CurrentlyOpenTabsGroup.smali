.class Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$CurrentlyOpenTabsGroup;
.super Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$Group;
.source "RecentTabsRowAdapter.java"


# instance fields
.field private final mCurrentlyOpenTabs:Ljava/util/List;

.field private final mShowingAll:Z

.field final synthetic this$0:Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;Ljava/util/List;)V
    .locals 1

    .prologue
    .line 202
    iput-object p1, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$CurrentlyOpenTabsGroup;->this$0:Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$Group;-><init>(Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;)V

    .line 203
    iput-object p2, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$CurrentlyOpenTabsGroup;->mCurrentlyOpenTabs:Ljava/util/List;

    .line 204
    # getter for: Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->mRecentTabsManager:Lcom/google/android/apps/chrome/ntp/RecentTabsManager;
    invoke-static {p1}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->access$300(Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;)Lcom/google/android/apps/chrome/ntp/RecentTabsManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->isCurrentlyOpenTabsShowingAll()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$CurrentlyOpenTabsGroup;->mShowingAll:Z

    .line 205
    return-void
.end method

.method private isMoreButton(I)Z
    .locals 2

    .prologue
    .line 208
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$CurrentlyOpenTabsGroup;->mShowingAll:Z

    if-nez v0, :cond_0

    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$CurrentlyOpenTabsGroup;->mCurrentlyOpenTabs:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method configureChildView(ILcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$ViewHolder;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 237
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$CurrentlyOpenTabsGroup;->isMoreButton(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 238
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$CurrentlyOpenTabsGroup;->this$0:Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;

    # getter for: Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->access$000(Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 239
    sget v1, Lcom/google/android/apps/chrome/R$string;->recent_tabs_show_more:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 240
    iget-object v2, p2, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$ViewHolder;->textView:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 241
    sget v1, Lcom/google/android/apps/chrome/R$drawable;->more_horiz:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 242
    iget-object v1, p2, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$ViewHolder;->textView:Landroid/widget/TextView;

    invoke-static {v1, v0, v3, v3, v3}, Lorg/chromium/base/ApiCompatibilityUtils;->setCompoundDrawablesRelativeWithIntrinsicBounds(Landroid/widget/TextView;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 250
    :goto_0
    return-void

    .line 245
    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$CurrentlyOpenTabsGroup;->getChild(I)Lcom/google/android/apps/chrome/ntp/CurrentlyOpenTab;

    move-result-object v1

    .line 246
    iget-object v2, p2, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$ViewHolder;->textView:Landroid/widget/TextView;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/ntp/CurrentlyOpenTab;->getTitle()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/ntp/CurrentlyOpenTab;->getUrl()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 248
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$CurrentlyOpenTabsGroup;->this$0:Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/ntp/CurrentlyOpenTab;->getUrl()Ljava/lang/String;

    move-result-object v1

    # invokes: Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->loadLocalFavicon(Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$ViewHolder;Ljava/lang/String;)V
    invoke-static {v0, p2, v1}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->access$400(Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$ViewHolder;Ljava/lang/String;)V

    goto :goto_0

    .line 246
    :cond_1
    invoke-virtual {v1}, Lcom/google/android/apps/chrome/ntp/CurrentlyOpenTab;->getTitle()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method configureGroupView(Lcom/google/android/apps/chrome/ntp/RecentTabsGroupView;Z)V
    .locals 0

    .prologue
    .line 254
    invoke-virtual {p1, p2}, Lcom/google/android/apps/chrome/ntp/RecentTabsGroupView;->configureForCurrentlyOpenTabs(Z)V

    .line 255
    return-void
.end method

.method getChild(I)Lcom/google/android/apps/chrome/ntp/CurrentlyOpenTab;
    .locals 1

    .prologue
    .line 230
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$CurrentlyOpenTabsGroup;->isMoreButton(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 232
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$CurrentlyOpenTabsGroup;->mCurrentlyOpenTabs:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/ntp/CurrentlyOpenTab;

    goto :goto_0
.end method

.method bridge synthetic getChild(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 196
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$CurrentlyOpenTabsGroup;->getChild(I)Lcom/google/android/apps/chrome/ntp/CurrentlyOpenTab;

    move-result-object v0

    return-object v0
.end method

.method getChildType()Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$ChildType;
    .locals 1

    .prologue
    .line 225
    sget-object v0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$ChildType;->DEFAULT_CONTENT:Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$ChildType;

    return-object v0
.end method

.method getChildrenCount()I
    .locals 2

    .prologue
    .line 219
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$CurrentlyOpenTabsGroup;->mShowingAll:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$CurrentlyOpenTabsGroup;->mCurrentlyOpenTabs:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 220
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$CurrentlyOpenTabsGroup;->mCurrentlyOpenTabs:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method getGroupType()Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$GroupType;
    .locals 1

    .prologue
    .line 214
    sget-object v0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$GroupType;->CONTENT:Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$GroupType;

    return-object v0
.end method

.method isCollapsed()Z
    .locals 1

    .prologue
    .line 264
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$CurrentlyOpenTabsGroup;->this$0:Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;

    # getter for: Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->mRecentTabsManager:Lcom/google/android/apps/chrome/ntp/RecentTabsManager;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->access$300(Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;)Lcom/google/android/apps/chrome/ntp/RecentTabsManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->isCurrentlyOpenTabsCollapsed()Z

    move-result v0

    return v0
.end method

.method onChildClick(I)Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 269
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$CurrentlyOpenTabsGroup;->isMoreButton(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 270
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$CurrentlyOpenTabsGroup;->this$0:Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;

    # getter for: Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->mRecentTabsManager:Lcom/google/android/apps/chrome/ntp/RecentTabsManager;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->access$300(Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;)Lcom/google/android/apps/chrome/ntp/RecentTabsManager;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->setCurrentlyOpenTabsShowAll(Z)V

    .line 274
    :goto_0
    return v1

    .line 272
    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$CurrentlyOpenTabsGroup;->getChild(I)Lcom/google/android/apps/chrome/ntp/CurrentlyOpenTab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ntp/CurrentlyOpenTab;->getRunnable()Ljava/lang/Runnable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto :goto_0
.end method

.method setCollapsed(Z)V
    .locals 1

    .prologue
    .line 259
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$CurrentlyOpenTabsGroup;->this$0:Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;

    # getter for: Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->mRecentTabsManager:Lcom/google/android/apps/chrome/ntp/RecentTabsManager;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->access$300(Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;)Lcom/google/android/apps/chrome/ntp/RecentTabsManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->setCurrentlyOpenTabsCollapsed(Z)V

    .line 260
    return-void
.end method
