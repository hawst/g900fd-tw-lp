.class public final enum Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthType;
.super Ljava/lang/Enum;
.source "BandwidthType.java"


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthType;

.field static final synthetic $assertionsDisabled:Z

.field public static final enum ALWAYS_PRERENDER:Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthType;

.field public static final DEFAULT:Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthType;

.field public static final enum NEVER_PRERENDER:Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthType;

.field public static final enum PRERENDER_ON_WIFI:Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthType;


# instance fields
.field private final mTitle:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 13
    const-class v0, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthType;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthType;->$assertionsDisabled:Z

    .line 17
    new-instance v0, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthType;

    const-string/jumbo v3, "NEVER_PRERENDER"

    const-string/jumbo v4, "never_prefetch"

    invoke-direct {v0, v3, v2, v4}, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthType;->NEVER_PRERENDER:Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthType;

    .line 18
    new-instance v0, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthType;

    const-string/jumbo v3, "PRERENDER_ON_WIFI"

    const-string/jumbo v4, "prefetch_on_wifi"

    invoke-direct {v0, v3, v1, v4}, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthType;->PRERENDER_ON_WIFI:Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthType;

    .line 19
    new-instance v0, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthType;

    const-string/jumbo v3, "ALWAYS_PRERENDER"

    const-string/jumbo v4, "always_prefetch"

    invoke-direct {v0, v3, v5, v4}, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthType;->ALWAYS_PRERENDER:Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthType;

    .line 13
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthType;

    sget-object v3, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthType;->NEVER_PRERENDER:Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthType;

    aput-object v3, v0, v2

    sget-object v2, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthType;->PRERENDER_ON_WIFI:Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthType;

    aput-object v2, v0, v1

    sget-object v1, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthType;->ALWAYS_PRERENDER:Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthType;->$VALUES:[Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthType;

    .line 21
    sget-object v0, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthType;->PRERENDER_ON_WIFI:Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthType;

    sput-object v0, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthType;->DEFAULT:Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthType;

    return-void

    :cond_0
    move v0, v2

    .line 13
    goto :goto_0
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 26
    iput-object p3, p0, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthType;->mTitle:Ljava/lang/String;

    .line 27
    return-void
.end method

.method public static getBandwidthFromTitle(Ljava/lang/String;)Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthType;
    .locals 5

    .prologue
    .line 60
    invoke-static {}, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthType;->values()[Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthType;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 61
    iget-object v4, v0, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthType;->mTitle:Ljava/lang/String;

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 65
    :goto_1
    return-object v0

    .line 60
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 64
    :cond_1
    sget-boolean v0, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthType;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 65
    :cond_2
    sget-object v0, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthType;->DEFAULT:Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthType;

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthType;
    .locals 1

    .prologue
    .line 13
    const-class v0, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthType;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthType;
    .locals 1

    .prologue
    .line 13
    sget-object v0, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthType;->$VALUES:[Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthType;

    invoke-virtual {v0}, [Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthType;

    return-object v0
.end method


# virtual methods
.method public final title()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthType;->mTitle:Ljava/lang/String;

    return-object v0
.end method
