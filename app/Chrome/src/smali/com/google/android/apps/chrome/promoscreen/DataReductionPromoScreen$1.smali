.class Lcom/google/android/apps/chrome/promoscreen/DataReductionPromoScreen$1;
.super Ljava/lang/Object;
.source "DataReductionPromoScreen.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/promoscreen/DataReductionPromoScreen;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$delegate:Landroid/view/View;

.field final synthetic val$parent:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/promoscreen/DataReductionPromoScreen;Landroid/content/Context;Landroid/view/View;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 121
    iput-object p1, p0, Lcom/google/android/apps/chrome/promoscreen/DataReductionPromoScreen$1;->this$0:Lcom/google/android/apps/chrome/promoscreen/DataReductionPromoScreen;

    iput-object p2, p0, Lcom/google/android/apps/chrome/promoscreen/DataReductionPromoScreen$1;->val$context:Landroid/content/Context;

    iput-object p3, p0, Lcom/google/android/apps/chrome/promoscreen/DataReductionPromoScreen$1;->val$delegate:Landroid/view/View;

    iput-object p4, p0, Lcom/google/android/apps/chrome/promoscreen/DataReductionPromoScreen$1;->val$parent:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 124
    const/high16 v0, 0x42960000    # 75.0f

    iget-object v1, p0, Lcom/google/android/apps/chrome/promoscreen/DataReductionPromoScreen$1;->val$context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    .line 127
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 128
    iget-object v2, p0, Lcom/google/android/apps/chrome/promoscreen/DataReductionPromoScreen$1;->val$delegate:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    .line 129
    iget v2, v1, Landroid/graphics/Rect;->top:I

    sub-int/2addr v2, v0

    iput v2, v1, Landroid/graphics/Rect;->top:I

    .line 130
    iget v2, v1, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v2, v0

    iput v2, v1, Landroid/graphics/Rect;->bottom:I

    .line 131
    iget v2, v1, Landroid/graphics/Rect;->left:I

    sub-int/2addr v2, v0

    iput v2, v1, Landroid/graphics/Rect;->left:I

    .line 132
    iget v2, v1, Landroid/graphics/Rect;->right:I

    add-int/2addr v0, v2

    iput v0, v1, Landroid/graphics/Rect;->right:I

    .line 133
    iget-object v0, p0, Lcom/google/android/apps/chrome/promoscreen/DataReductionPromoScreen$1;->val$parent:Landroid/view/View;

    new-instance v2, Landroid/view/TouchDelegate;

    iget-object v3, p0, Lcom/google/android/apps/chrome/promoscreen/DataReductionPromoScreen$1;->val$delegate:Landroid/view/View;

    invoke-direct {v2, v1, v3}, Landroid/view/TouchDelegate;-><init>(Landroid/graphics/Rect;Landroid/view/View;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setTouchDelegate(Landroid/view/TouchDelegate;)V

    .line 134
    return-void
.end method
