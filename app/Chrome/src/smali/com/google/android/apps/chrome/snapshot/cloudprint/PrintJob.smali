.class public Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob;
.super Ljava/lang/Object;
.source "PrintJob.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static CREATOR:Landroid/os/Parcelable$Creator; = null

.field public static final NO_CREATE_TIME:J = -0x1L


# instance fields
.field private mCreateTime:J

.field private final mDownloadUri:Landroid/net/Uri;

.field private final mJobData:Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob$JobData;

.field private final mJobId:Ljava/lang/String;

.field private final mMimeType:Ljava/lang/String;

.field private final mTitle:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    new-instance v0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob$1;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob$1;-><init>()V

    sput-object v0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob;->mJobId:Ljava/lang/String;

    .line 47
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob;->mCreateTime:J

    .line 48
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob;->mTitle:Ljava/lang/String;

    .line 49
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 50
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob;->mDownloadUri:Landroid/net/Uri;

    .line 51
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob;->mMimeType:Ljava/lang/String;

    .line 52
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob$JobData;

    iput-object v0, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob;->mJobData:Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob$JobData;

    .line 53
    return-void

    .line 50
    :cond_0
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob$1;)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;JLjava/lang/String;Landroid/net/Uri;Ljava/lang/String;Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob$JobData;)V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    iput-object p1, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob;->mJobId:Ljava/lang/String;

    .line 58
    iput-wide p2, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob;->mCreateTime:J

    .line 59
    iput-object p4, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob;->mTitle:Ljava/lang/String;

    .line 60
    iput-object p5, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob;->mDownloadUri:Landroid/net/Uri;

    .line 61
    iput-object p6, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob;->mMimeType:Ljava/lang/String;

    .line 62
    iput-object p7, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob;->mJobData:Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob$JobData;

    .line 63
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 107
    const/4 v0, 0x0

    return v0
.end method

.method public getCreateTime()J
    .locals 2

    .prologue
    .line 70
    iget-wide v0, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob;->mCreateTime:J

    return-wide v0
.end method

.method public getDownloadUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob;->mDownloadUri:Landroid/net/Uri;

    return-object v0
.end method

.method public getJobData()Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob$JobData;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob;->mJobData:Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob$JobData;

    return-object v0
.end method

.method public getJobId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob;->mJobId:Ljava/lang/String;

    return-object v0
.end method

.method public getMimeType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob;->mMimeType:Ljava/lang/String;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob;->mTitle:Ljava/lang/String;

    return-object v0
.end method

.method public hasJobData()Z
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob;->mJobData:Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob$JobData;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    const/16 v4, 0x27

    .line 95
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "PrintJob{mJobId=\'"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob;->mJobId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", mCreateTime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob;->mCreateTime:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", mTitle=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob;->mTitle:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", mDownloadUri="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob;->mDownloadUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", mMimeType=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob;->mMimeType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", mJobData="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob;->mJobData:Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob$JobData;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 112
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob;->mJobId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 113
    iget-wide v0, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob;->mCreateTime:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 114
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob;->mTitle:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 115
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob;->mDownloadUri:Landroid/net/Uri;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 116
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob;->mMimeType:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 117
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob;->mJobData:Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob$JobData;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 118
    return-void

    .line 115
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/cloudprint/PrintJob;->mDownloadUri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
