.class Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager$2;
.super Ljava/lang/Object;
.source "DelegatedPKCS11Manager.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;)V
    .locals 0

    .prologue
    .line 88
    iput-object p1, p0, Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager$2;->this$0:Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 3

    .prologue
    .line 91
    iget-object v0, p0, Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager$2;->this$0:Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;

    invoke-static {p2}, Lorg/chromium/net/IRemoteAndroidKeyStore$Stub;->asInterface(Landroid/os/IBinder;)Lorg/chromium/net/IRemoteAndroidKeyStore;

    move-result-object v1

    # setter for: Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;->mRemoteManager:Lorg/chromium/net/IRemoteAndroidKeyStore;
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;->access$302(Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;Lorg/chromium/net/IRemoteAndroidKeyStore;)Lorg/chromium/net/IRemoteAndroidKeyStore;

    .line 93
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager$2;->this$0:Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;

    # getter for: Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;->mRemoteManager:Lorg/chromium/net/IRemoteAndroidKeyStore;
    invoke-static {v0}, Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;->access$300(Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;)Lorg/chromium/net/IRemoteAndroidKeyStore;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager$2;->this$0:Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;

    # getter for: Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;->mCallbacks:Lorg/chromium/net/IRemoteAndroidKeyStoreCallbacks;
    invoke-static {v1}, Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;->access$400(Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;)Lorg/chromium/net/IRemoteAndroidKeyStoreCallbacks;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/chromium/net/IRemoteAndroidKeyStore;->setClientCallbacks(Lorg/chromium/net/IRemoteAndroidKeyStoreCallbacks;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 102
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager$2;->this$0:Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;

    new-instance v1, Lorg/chromium/net/RemoteAndroidKeyStore;

    iget-object v2, p0, Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager$2;->this$0:Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;

    # getter for: Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;->mRemoteManager:Lorg/chromium/net/IRemoteAndroidKeyStore;
    invoke-static {v2}, Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;->access$300(Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;)Lorg/chromium/net/IRemoteAndroidKeyStore;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/chromium/net/RemoteAndroidKeyStore;-><init>(Lorg/chromium/net/IRemoteAndroidKeyStore;)V

    # setter for: Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;->mKeyStoreRemote:Lorg/chromium/net/RemoteAndroidKeyStore;
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;->access$502(Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;Lorg/chromium/net/RemoteAndroidKeyStore;)Lorg/chromium/net/RemoteAndroidKeyStore;

    .line 103
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager$2;->this$0:Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;

    # invokes: Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;->clearState()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;->access$600(Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;)V

    .line 108
    return-void
.end method
