.class Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment$FolderListEntry;
.super Ljava/lang/Object;
.source "SelectBookmarkFolderFragment.java"


# instance fields
.field final mDepth:I

.field final mFolder:Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

.field final mIsSelectedFolder:Z


# direct methods
.method constructor <init>(Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;IZ)V
    .locals 0

    .prologue
    .line 236
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 237
    iput-object p1, p0, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment$FolderListEntry;->mFolder:Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    .line 238
    iput p2, p0, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment$FolderListEntry;->mDepth:I

    .line 239
    iput-boolean p3, p0, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment$FolderListEntry;->mIsSelectedFolder:Z

    .line 240
    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 244
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment$FolderListEntry;->mFolder:Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->name()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
