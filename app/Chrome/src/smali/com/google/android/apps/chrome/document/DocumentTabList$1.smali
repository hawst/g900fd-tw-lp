.class Lcom/google/android/apps/chrome/document/DocumentTabList$1;
.super Landroid/os/AsyncTask;
.source "DocumentTabList.java"


# instance fields
.field private final mEntries:Ljava/util/List;

.field final synthetic this$0:Lcom/google/android/apps/chrome/document/DocumentTabList;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/document/DocumentTabList;)V
    .locals 2

    .prologue
    .line 670
    iput-object p1, p0, Lcom/google/android/apps/chrome/document/DocumentTabList$1;->this$0:Lcom/google/android/apps/chrome/document/DocumentTabList;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 671
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/apps/chrome/document/DocumentTabList$1;->this$0:Lcom/google/android/apps/chrome/document/DocumentTabList;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/document/DocumentTabList;->getCount()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentTabList$1;->mEntries:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 670
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/document/DocumentTabList$1;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method public varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 4

    .prologue
    .line 683
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentTabList$1;->mEntries:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/document/DocumentTabList$Entry;

    .line 684
    iget-object v2, p0, Lcom/google/android/apps/chrome/document/DocumentTabList$1;->this$0:Lcom/google/android/apps/chrome/document/DocumentTabList;

    # getter for: Lcom/google/android/apps/chrome/document/DocumentTabList;->mPrioritizedTabId:I
    invoke-static {v2}, Lcom/google/android/apps/chrome/document/DocumentTabList;->access$200(Lcom/google/android/apps/chrome/document/DocumentTabList;)I

    move-result v2

    iget v3, v0, Lcom/google/android/apps/chrome/document/DocumentTabList$Entry;->tabId:I

    if-eq v2, v3, :cond_0

    .line 685
    iget-object v2, p0, Lcom/google/android/apps/chrome/document/DocumentTabList$1;->this$0:Lcom/google/android/apps/chrome/document/DocumentTabList;

    iget v3, v0, Lcom/google/android/apps/chrome/document/DocumentTabList$Entry;->tabId:I

    # invokes: Lcom/google/android/apps/chrome/document/DocumentTabList;->restoreTabState(I)Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;
    invoke-static {v2, v3}, Lcom/google/android/apps/chrome/document/DocumentTabList;->access$300(Lcom/google/android/apps/chrome/document/DocumentTabList;I)Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;

    move-result-object v2

    iput-object v2, v0, Lcom/google/android/apps/chrome/document/DocumentTabList$Entry;->tabState:Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;

    goto :goto_0

    .line 688
    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method

.method public bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 670
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/document/DocumentTabList$1;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method public onPostExecute(Ljava/lang/Void;)V
    .locals 4

    .prologue
    .line 693
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentTabList$1;->mEntries:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/document/DocumentTabList$Entry;

    .line 694
    iget-object v1, p0, Lcom/google/android/apps/chrome/document/DocumentTabList$1;->this$0:Lcom/google/android/apps/chrome/document/DocumentTabList;

    # getter for: Lcom/google/android/apps/chrome/document/DocumentTabList;->mEntryMap:Landroid/util/SparseArray;
    invoke-static {v1}, Lcom/google/android/apps/chrome/document/DocumentTabList;->access$400(Lcom/google/android/apps/chrome/document/DocumentTabList;)Landroid/util/SparseArray;

    move-result-object v1

    iget v3, v0, Lcom/google/android/apps/chrome/document/DocumentTabList$Entry;->tabId:I

    invoke-virtual {v1, v3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/chrome/document/DocumentTabList$Entry;

    .line 695
    if-eqz v1, :cond_0

    .line 697
    iget-object v3, v1, Lcom/google/android/apps/chrome/document/DocumentTabList$Entry;->tabState:Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;

    if-nez v3, :cond_1

    iget-object v0, v0, Lcom/google/android/apps/chrome/document/DocumentTabList$Entry;->tabState:Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;

    iput-object v0, v1, Lcom/google/android/apps/chrome/document/DocumentTabList$Entry;->tabState:Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;

    .line 698
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, v1, Lcom/google/android/apps/chrome/document/DocumentTabList$Entry;->isTabStateReady:Z

    goto :goto_0

    .line 701
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentTabList$1;->this$0:Lcom/google/android/apps/chrome/document/DocumentTabList;

    const/4 v1, 0x4

    # invokes: Lcom/google/android/apps/chrome/document/DocumentTabList;->setCurrentState(I)V
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/document/DocumentTabList;->access$100(Lcom/google/android/apps/chrome/document/DocumentTabList;I)V

    .line 702
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentTabList$1;->this$0:Lcom/google/android/apps/chrome/document/DocumentTabList;

    # invokes: Lcom/google/android/apps/chrome/document/DocumentTabList;->deserializeTabStatesAsync()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/document/DocumentTabList;->access$500(Lcom/google/android/apps/chrome/document/DocumentTabList;)V

    .line 703
    return-void
.end method

.method public onPreExecute()V
    .locals 4

    .prologue
    .line 675
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentTabList$1;->this$0:Lcom/google/android/apps/chrome/document/DocumentTabList;

    const/4 v1, 0x3

    # invokes: Lcom/google/android/apps/chrome/document/DocumentTabList;->setCurrentState(I)V
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/document/DocumentTabList;->access$100(Lcom/google/android/apps/chrome/document/DocumentTabList;I)V

    .line 676
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/document/DocumentTabList$1;->this$0:Lcom/google/android/apps/chrome/document/DocumentTabList;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/document/DocumentTabList;->getCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 677
    iget-object v1, p0, Lcom/google/android/apps/chrome/document/DocumentTabList$1;->mEntries:Ljava/util/List;

    new-instance v2, Lcom/google/android/apps/chrome/document/DocumentTabList$Entry;

    iget-object v3, p0, Lcom/google/android/apps/chrome/document/DocumentTabList$1;->this$0:Lcom/google/android/apps/chrome/document/DocumentTabList;

    invoke-virtual {v3, v0}, Lcom/google/android/apps/chrome/document/DocumentTabList;->getTabIdAtIndex(I)I

    move-result v3

    invoke-direct {v2, v3}, Lcom/google/android/apps/chrome/document/DocumentTabList$Entry;-><init>(I)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 676
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 679
    :cond_0
    return-void
.end method
