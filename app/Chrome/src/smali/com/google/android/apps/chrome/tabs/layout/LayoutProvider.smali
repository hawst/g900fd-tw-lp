.class public interface abstract Lcom/google/android/apps/chrome/tabs/layout/LayoutProvider;
.super Ljava/lang/Object;
.source "LayoutProvider.java"


# virtual methods
.method public abstract getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;
.end method

.method public abstract getFullscreenManager()Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;
.end method

.method public abstract getViewportPixel(Landroid/graphics/Rect;)Landroid/graphics/Rect;
.end method
