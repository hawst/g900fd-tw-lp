.class public Lcom/google/android/apps/chrome/promoscreen/SigninPromoScreen;
.super Landroid/app/Dialog;
.source "SigninPromoScreen.java"

# interfaces
.implements Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView$Listener;


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private mAccountFirstRunView:Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    const-class v0, Lcom/google/android/apps/chrome/promoscreen/SigninPromoScreen;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/promoscreen/SigninPromoScreen;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Landroid/app/Activity;)V
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 76
    sget v0, Lcom/google/android/apps/chrome/R$style;->FirstRunTheme:I

    invoke-direct {p0, p1, v0}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 77
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/promoscreen/SigninPromoScreen;->setOwnerActivity(Landroid/app/Activity;)V

    .line 79
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 80
    sget v1, Lcom/google/android/apps/chrome/R$layout;->fre_choose_account:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 81
    new-instance v2, Lcom/google/android/apps/chrome/firstrun/ProfileDataCache;

    invoke-static {}, Lorg/chromium/chrome/browser/profiles/Profile;->getLastUsedProfile()Lorg/chromium/chrome/browser/profiles/Profile;

    move-result-object v0

    invoke-direct {v2, p1, v0}, Lcom/google/android/apps/chrome/firstrun/ProfileDataCache;-><init>(Landroid/app/Activity;Lorg/chromium/chrome/browser/profiles/Profile;)V

    .line 82
    sget v0, Lcom/google/android/apps/chrome/R$id;->fre_account_layout:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;

    iput-object v0, p0, Lcom/google/android/apps/chrome/promoscreen/SigninPromoScreen;->mAccountFirstRunView:Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;

    .line 83
    iget-object v0, p0, Lcom/google/android/apps/chrome/promoscreen/SigninPromoScreen;->mAccountFirstRunView:Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->init(Lcom/google/android/apps/chrome/firstrun/ProfileDataCache;)V

    .line 84
    iget-object v0, p0, Lcom/google/android/apps/chrome/promoscreen/SigninPromoScreen;->mAccountFirstRunView:Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->configureForAddAccountPromo()V

    .line 85
    iget-object v0, p0, Lcom/google/android/apps/chrome/promoscreen/SigninPromoScreen;->mAccountFirstRunView:Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->setListener(Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView$Listener;)V

    .line 87
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v3, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/chrome/promoscreen/SigninPromoScreen;->setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 89
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/promoscreen/SigninPromoScreen;)Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/apps/chrome/promoscreen/SigninPromoScreen;->mAccountFirstRunView:Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;

    return-object v0
.end method

.method public static launchSigninPromoIfNeeded(Landroid/app/Activity;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 52
    invoke-static {p0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v1

    .line 53
    invoke-static {p0}, Lcom/google/android/apps/chrome/third_party/samsung/MultiWindowUtils;->isMultiWindow(Landroid/app/Activity;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 67
    :cond_0
    :goto_0
    return v0

    .line 54
    :cond_1
    invoke-virtual {v1}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getShowSigninPromo()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 55
    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->setShowSigninPromo(Z)V

    .line 57
    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getSyncLastAccountName()Ljava/lang/String;

    move-result-object v2

    .line 58
    invoke-static {}, Lorg/chromium/chrome/browser/signin/SigninManager;->getAndroidSigninPromoExperimentGroup()I

    move-result v3

    if-ltz v3, :cond_0

    invoke-static {p0}, Lorg/chromium/sync/signin/ChromeSigninController;->get(Landroid/content/Context;)Lorg/chromium/sync/signin/ChromeSigninController;

    move-result-object v3

    invoke-virtual {v3}, Lorg/chromium/sync/signin/ChromeSigninController;->isSignedIn()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 64
    new-instance v0, Lcom/google/android/apps/chrome/promoscreen/SigninPromoScreen;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/promoscreen/SigninPromoScreen;-><init>(Landroid/app/Activity;)V

    .line 65
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/promoscreen/SigninPromoScreen;->show()V

    .line 66
    invoke-virtual {v1}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->setSigninPromoShown()V

    .line 67
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public onAccountSelectionCanceled()V
    .locals 0

    .prologue
    .line 122
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->recordAndroidSigninPromoDeclined()V

    .line 123
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/promoscreen/SigninPromoScreen;->dismiss()V

    .line 124
    return-void
.end method

.method public onAccountSelectionConfirmed(Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 99
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/promoscreen/SigninPromoScreen;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/sync/signin/AccountManagerHelper;->get(Landroid/content/Context;)Lorg/chromium/sync/signin/AccountManagerHelper;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/chromium/sync/signin/AccountManagerHelper;->getAccountFromName(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v1

    .line 101
    new-instance v5, Lcom/google/android/apps/chrome/promoscreen/SigninPromoScreen$1;

    invoke-direct {v5, p0}, Lcom/google/android/apps/chrome/promoscreen/SigninPromoScreen$1;-><init>(Lcom/google/android/apps/chrome/promoscreen/SigninPromoScreen;)V

    .line 115
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/promoscreen/SigninPromoScreen;->getOwnerActivity()Landroid/app/Activity;

    move-result-object v0

    const/4 v3, 0x1

    move v4, v2

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/chrome/firstrun/FirstRunUtil;->signInToSelectedAccount(Landroid/app/Activity;Landroid/accounts/Account;IIZLorg/chromium/chrome/browser/signin/SigninManager$SignInFlowObserver;)V

    .line 118
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 93
    invoke-super {p0, p1}, Landroid/app/Dialog;->onCreate(Landroid/os/Bundle;)V

    .line 94
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->recordAndroidSigninPromoShown()V

    .line 95
    return-void
.end method

.method public onFailedToSetForcedAccount(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 157
    sget-boolean v0, Lcom/google/android/apps/chrome/promoscreen/SigninPromoScreen;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    const-string/jumbo v1, "No forced accounts in SigninPromoScreen"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 158
    :cond_0
    return-void
.end method

.method public onNewAccount()V
    .locals 3

    .prologue
    .line 128
    new-instance v0, Lcom/google/android/apps/chrome/services/AccountAdder;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/services/AccountAdder;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/promoscreen/SigninPromoScreen;->getOwnerActivity()Landroid/app/Activity;

    move-result-object v1

    const/16 v2, 0x66

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/services/AccountAdder;->addAccount(Landroid/app/Activity;I)V

    .line 129
    return-void
.end method

.method public onSettingsButtonClicked(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 138
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 139
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/promoscreen/SigninPromoScreen;->getContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/google/android/apps/chrome/preferences/Preferences;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    .line 140
    const/high16 v1, 0x20020000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 142
    const-string/jumbo v1, ":android:show_fragment"

    const-class v2, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 144
    const-string/jumbo v1, ":android:show_fragment_title"

    sget v2, Lcom/google/android/apps/chrome/R$string;->sign_in_sync:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 145
    const-string/jumbo v1, "display_home_as_up"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 146
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 147
    const-string/jumbo v2, "account"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    const-string/jumbo v2, ":android:show_fragment_args"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 149
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/promoscreen/SigninPromoScreen;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 151
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->recordAndroidSigninPromoAcceptedAdvanced()V

    .line 152
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/promoscreen/SigninPromoScreen;->dismiss()V

    .line 153
    return-void
.end method

.method public onSigningInCompleted(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 133
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/promoscreen/SigninPromoScreen;->dismiss()V

    .line 134
    return-void
.end method
