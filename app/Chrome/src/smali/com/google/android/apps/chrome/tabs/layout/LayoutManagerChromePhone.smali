.class public Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromePhone;
.super Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;
.source "LayoutManagerChromePhone.java"


# static fields
.field private static final NOTIFICATIONS:[I


# instance fields
.field private final mSimpleAnimationLayout:Lcom/google/android/apps/chrome/tabs/layout/Layout;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 145
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const/16 v2, 0x24

    aput v2, v0, v1

    sput-object v0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromePhone;->NOTIFICATIONS:[I

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;Lcom/google/android/apps/chrome/eventfilter/EventFilterHost;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 40
    invoke-direct {p0, p1, p2, v4}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;-><init>(Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;Lcom/google/android/apps/chrome/eventfilter/EventFilterHost;Z)V

    .line 41
    invoke-interface {p1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 42
    invoke-interface {p1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;->getLayoutRenderHost()Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;

    move-result-object v1

    .line 45
    new-instance v2, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;

    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromePhone;->mBlackHoleEventFilter:Lcom/google/android/apps/chrome/eventfilter/EventFilter;

    invoke-direct {v2, v0, p0, v1, v3}, Lcom/google/android/apps/chrome/tabs/layout/phone/SimpleAnimationLayout;-><init>(Landroid/content/Context;Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;Lcom/google/android/apps/chrome/eventfilter/EventFilter;)V

    iput-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromePhone;->mSimpleAnimationLayout:Lcom/google/android/apps/chrome/tabs/layout/Layout;

    .line 49
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromePhone;->mStaticLayout:Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/tabs/layout/StaticLayout;->setLayoutHandlesTabLifecycles(Z)V

    .line 50
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromePhone;->mToolbarSwipeLayout:Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;

    invoke-virtual {v0, v4}, Lcom/google/android/apps/chrome/tabs/layout/ToolbarSwipeLayout;->setMovesToolbar(Z)V

    .line 51
    return-void
.end method

.method private tabClosing(I)V
    .locals 4

    .prologue
    .line 84
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromePhone;->getTabById(I)Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    .line 85
    if-nez v0, :cond_1

    .line 95
    :cond_0
    :goto_0
    return-void

    .line 87
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromePhone;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->handlesTabClosing()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 90
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromePhone;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    invoke-static {}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromePhone;->time()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3, p1}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->onTabClosing(JI)V

    goto :goto_0

    .line 91
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromePhone;->mEnableAnimations:Z

    if-eqz v0, :cond_0

    .line 92
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromePhone;->mSimpleAnimationLayout:Lcom/google/android/apps/chrome/tabs/layout/Layout;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromePhone;->startShowing(Lcom/google/android/apps/chrome/tabs/layout/Layout;Z)V

    .line 93
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromePhone;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    invoke-static {}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromePhone;->time()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3, p1}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->onTabClosing(JI)V

    goto :goto_0
.end method


# virtual methods
.method protected emptyCachesExcept(I)V
    .locals 1

    .prologue
    .line 79
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->emptyCachesExcept(I)V

    .line 80
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromePhone;->mTitleCache:Lcom/google/android/apps/chrome/tabs/TitleCache;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromePhone;->mTitleCache:Lcom/google/android/apps/chrome/tabs/TitleCache;

    invoke-interface {v0, p1}, Lcom/google/android/apps/chrome/tabs/TitleCache;->clearExcept(I)V

    .line 81
    :cond_0
    return-void
.end method

.method protected getNotificationsToRegisterFor()[I
    .locals 1

    .prologue
    .line 142
    sget-object v0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromePhone;->NOTIFICATIONS:[I

    return-object v0
.end method

.method public getStackLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromePhone;->mStackLayout:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;

    return-object v0
.end method

.method protected handleNotificationMessage(Landroid/os/Message;)V
    .locals 2

    .prologue
    .line 151
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 160
    :cond_0
    :goto_0
    return-void

    .line 153
    :pswitch_0
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "animate"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 154
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "tabId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromePhone;->tabClosing(I)V

    goto :goto_0

    .line 151
    :pswitch_data_0
    .packed-switch 0x24
        :pswitch_0
    .end packed-switch
.end method

.method public init(Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreatorManager;Lcom/google/android/apps/chrome/compositor/TabContentManager;Landroid/view/ViewGroup;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManagementDelegate;)V
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromePhone;->mSimpleAnimationLayout:Lcom/google/android/apps/chrome/tabs/layout/Layout;

    invoke-virtual {v0, p1, p3}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->setTabModelSelector(Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;Lcom/google/android/apps/chrome/compositor/TabContentManager;)V

    .line 68
    invoke-super/range {p0 .. p5}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->init(Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreatorManager;Lcom/google/android/apps/chrome/compositor/TabContentManager;Landroid/view/ViewGroup;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManagementDelegate;)V

    .line 69
    return-void
.end method

.method public releaseTabLayout(I)V
    .locals 1

    .prologue
    .line 164
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromePhone;->mTitleCache:Lcom/google/android/apps/chrome/tabs/TitleCache;

    invoke-interface {v0, p1}, Lcom/google/android/apps/chrome/tabs/TitleCache;->remove(I)V

    .line 165
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->releaseTabLayout(I)V

    .line 166
    return-void
.end method

.method protected setStripLayoutHelperManager(Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;)V
    .locals 1

    .prologue
    .line 73
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->setStripLayoutHelperManager(Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;)V

    .line 74
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromePhone;->mSimpleAnimationLayout:Lcom/google/android/apps/chrome/tabs/layout/Layout;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->setStripLayoutHelperManager(Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;)V

    .line 75
    return-void
.end method

.method protected tabClosed(IIZ)V
    .locals 9

    .prologue
    const/4 v7, 0x0

    .line 99
    const/4 v0, -0x1

    if-ne p2, v0, :cond_3

    const/4 v0, 0x1

    .line 100
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromePhone;->useAccessibilityLayout()Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromePhone;->mAccessibilityLayout:Lcom/google/android/apps/chrome/widget/accessibility/AccessibilityOverviewLayout;

    move-object v8, v1

    .line 102
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromePhone;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v1

    if-eq v1, v8, :cond_0

    if-eqz v0, :cond_0

    .line 105
    invoke-virtual {p0, v8}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromePhone;->setNextLayout(Lcom/google/android/apps/chrome/tabs/layout/Layout;)V

    .line 107
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromePhone;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v1

    invoke-static {}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromePhone;->time()J

    move-result-wide v2

    move v4, p1

    move v5, p2

    move v6, p3

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->onTabClosed(JIIZ)V

    .line 108
    invoke-virtual {p0, p2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromePhone;->getTabById(I)Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v1

    .line 109
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->requestFocus()V

    .line 110
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromePhone;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v1

    if-eq v1, v8, :cond_2

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromePhone;->mEnableAnimations:Z

    if-nez v0, :cond_2

    .line 111
    invoke-virtual {p0, v8, v7}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromePhone;->startShowing(Lcom/google/android/apps/chrome/tabs/layout/Layout;Z)V

    .line 113
    :cond_2
    return-void

    :cond_3
    move v0, v7

    .line 99
    goto :goto_0

    .line 100
    :cond_4
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromePhone;->mStackLayout:Lcom/google/android/apps/chrome/tabs/layout/phone/StackLayout;

    move-object v8, v1

    goto :goto_1
.end method

.method protected tabCreated(IILorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;ZZFF)V
    .locals 1

    .prologue
    .line 131
    invoke-super/range {p0 .. p7}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->tabCreated(IILorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;ZZFF)V

    .line 133
    if-eqz p5, :cond_0

    .line 134
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromePhone;->getTabModelSelector()Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    move-result-object v0

    invoke-interface {v0, p4}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->getModel(Z)Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v0

    invoke-static {v0, p1}, Lorg/chromium/chrome/browser/tabmodel/TabModelUtils;->getTabById(Lorg/chromium/chrome/browser/tabmodel/TabList;I)Lorg/chromium/chrome/browser/Tab;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->fromTab(Lorg/chromium/chrome/browser/Tab;)Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    .line 136
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->requestFocus()V

    .line 138
    :cond_0
    return-void
.end method

.method protected tabCreating(ILjava/lang/String;Z)V
    .locals 2

    .prologue
    .line 117
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromePhone;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->isHiding()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromePhone;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->handlesTabCreating()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 121
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromePhone;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->onTabCreating(I)V

    .line 126
    :cond_0
    :goto_0
    return-void

    .line 122
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromePhone;->mEnableAnimations:Z

    if-eqz v0, :cond_0

    .line 123
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromePhone;->mSimpleAnimationLayout:Lcom/google/android/apps/chrome/tabs/layout/Layout;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromePhone;->startShowing(Lcom/google/android/apps/chrome/tabs/layout/Layout;Z)V

    .line 124
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromePhone;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->onTabCreating(I)V

    goto :goto_0
.end method
