.class Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;
.super Landroid/view/View;
.source "FindResultBar.java"


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static sComparator:Ljava/util/Comparator;


# instance fields
.field private final mActiveBorderColor:I

.field private final mActiveColor:I

.field private mActiveMatch:Landroid/graphics/RectF;

.field private final mActiveMinHeight:I

.field private final mBackgroundBorderColor:I

.field private final mBackgroundColor:I

.field private final mBarDrawWidth:I

.field private mBarHeightForWhichTickmarksWereCached:I

.field private final mBarTouchWidth:I

.field private final mBarVerticalPadding:I

.field private final mFillPaint:Landroid/graphics/Paint;

.field private mMatches:[Landroid/graphics/RectF;

.field private final mMinGapBetweenStacks:I

.field mRectsVersion:I

.field private final mResultBorderColor:I

.field private final mResultColor:I

.field private final mResultMinHeight:I

.field private final mStackedResultHeight:I

.field private final mStrokePaint:Landroid/graphics/Paint;

.field private final mTab:Lcom/google/android/apps/chrome/tab/ChromeTab;

.field private mTickmarks:Ljava/util/ArrayList;

.field private mVisibilityAnimation:Landroid/animation/Animator;

.field mWaitingForActivateAck:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    const-class v0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->$assertionsDisabled:Z

    .line 74
    new-instance v0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar$1;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar$1;-><init>()V

    sput-object v0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->sComparator:Ljava/util/Comparator;

    return-void

    .line 40
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/chrome/tab/ChromeTab;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, -0x1

    const/4 v4, 0x0

    .line 89
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 60
    iput v5, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mRectsVersion:I

    .line 61
    new-array v0, v4, [Landroid/graphics/RectF;

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mMatches:[Landroid/graphics/RectF;

    .line 64
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v4}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mTickmarks:Ljava/util/ArrayList;

    .line 65
    iput v5, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mBarHeightForWhichTickmarksWereCached:I

    .line 72
    iput-boolean v4, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mWaitingForActivateAck:Z

    .line 91
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 92
    sget v1, Lcom/google/android/apps/chrome/R$color;->find_result_bar_background_color:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mBackgroundColor:I

    .line 94
    sget v1, Lcom/google/android/apps/chrome/R$color;->find_result_bar_background_border_color:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mBackgroundBorderColor:I

    .line 96
    sget v1, Lcom/google/android/apps/chrome/R$color;->find_result_bar_result_color:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mResultColor:I

    .line 98
    sget v1, Lcom/google/android/apps/chrome/R$color;->find_result_bar_result_border_color:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mResultBorderColor:I

    .line 100
    sget v1, Lcom/google/android/apps/chrome/R$color;->find_result_bar_active_color:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mActiveColor:I

    .line 102
    sget v1, Lcom/google/android/apps/chrome/R$color;->find_result_bar_active_border_color:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mActiveBorderColor:I

    .line 104
    sget v1, Lcom/google/android/apps/chrome/R$dimen;->find_result_bar_touch_width:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mBarTouchWidth:I

    .line 106
    sget v1, Lcom/google/android/apps/chrome/R$dimen;->find_result_bar_draw_width:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sget v2, Lcom/google/android/apps/chrome/R$dimen;->find_in_page_separator_width:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    add-int/2addr v1, v2

    iput v1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mBarDrawWidth:I

    .line 109
    sget v1, Lcom/google/android/apps/chrome/R$dimen;->find_result_bar_result_min_height:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mResultMinHeight:I

    .line 111
    sget v1, Lcom/google/android/apps/chrome/R$dimen;->find_result_bar_active_min_height:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mActiveMinHeight:I

    .line 113
    sget v1, Lcom/google/android/apps/chrome/R$dimen;->find_result_bar_vertical_padding:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mBarVerticalPadding:I

    .line 115
    sget v1, Lcom/google/android/apps/chrome/R$dimen;->find_result_bar_min_gap_between_stacks:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mMinGapBetweenStacks:I

    .line 117
    sget v1, Lcom/google/android/apps/chrome/R$dimen;->find_result_bar_stacked_result_height:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mStackedResultHeight:I

    .line 120
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mFillPaint:Landroid/graphics/Paint;

    .line 121
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mStrokePaint:Landroid/graphics/Paint;

    .line 122
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mFillPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v6}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 123
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mStrokePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v6}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 124
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mFillPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 125
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mStrokePaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 126
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mStrokePaint:Landroid/graphics/Paint;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 128
    iput-object p2, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mTab:Lcom/google/android/apps/chrome/tab/ChromeTab;

    .line 129
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mTab:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->getContainerView()Landroid/view/ViewGroup;

    move-result-object v0

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    iget v2, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mBarTouchWidth:I

    const v3, 0x800005

    invoke-direct {v1, v2, v5, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    invoke-virtual {v0, p0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 132
    iget v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mBarTouchWidth:I

    invoke-static {}, Lorg/chromium/ui/base/LocalizationUtils;->isLayoutRtl()Z

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/utilities/ChromeMathUtils;->flipSignIf(IZ)I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->setTranslationX(F)V

    .line 135
    sget-object v0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->TRANSLATION_X:Landroid/util/Property;

    new-array v1, v6, [F

    const/4 v2, 0x0

    aput v2, v1, v4

    invoke-static {p0, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mVisibilityAnimation:Landroid/animation/Animator;

    .line 136
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mVisibilityAnimation:Landroid/animation/Animator;

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 137
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mVisibilityAnimation:Landroid/animation/Animator;

    sget-object v1, Lorg/chromium/ui/interpolators/BakedBezierInterpolator;->FADE_IN_CURVE:Lorg/chromium/ui/interpolators/BakedBezierInterpolator;

    invoke-virtual {v0, v1}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 138
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mVisibilityAnimation:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    .line 139
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;)I
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->getLeftMargin()I

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;)I
    .locals 1

    .prologue
    .line 40
    iget v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mBarDrawWidth:I

    return v0
.end method

.method private calculateTickmarks()V
    .locals 14

    .prologue
    const/4 v3, 0x0

    .line 300
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mBarHeightForWhichTickmarksWereCached:I

    .line 303
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mMatches:[Landroid/graphics/RectF;

    array-length v1, v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mTickmarks:Ljava/util/ArrayList;

    .line 305
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mMatches:[Landroid/graphics/RectF;

    aget-object v0, v0, v3

    invoke-direct {p0, v0, v3}, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->tickmarkForRect(Landroid/graphics/RectF;Z)Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar$Tickmark;

    move-result-object v1

    .line 306
    iget v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mMinGapBetweenStacks:I

    neg-int v0, v0

    int-to-float v0, v0

    move v2, v3

    move-object v13, v1

    move v1, v0

    move-object v0, v13

    .line 307
    :goto_0
    iget-object v4, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mMatches:[Landroid/graphics/RectF;

    array-length v4, v4

    if-ge v2, v4, :cond_6

    .line 309
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 310
    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 311
    add-int/lit8 v4, v2, 0x1

    .line 312
    :goto_1
    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mMatches:[Landroid/graphics/RectF;

    array-length v2, v2

    if-ge v4, v2, :cond_0

    .line 313
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mMatches:[Landroid/graphics/RectF;

    aget-object v0, v0, v4

    invoke-direct {p0, v0, v3}, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->tickmarkForRect(Landroid/graphics/RectF;Z)Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar$Tickmark;

    move-result-object v2

    .line 314
    iget v5, v2, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar$Tickmark;->mTop:F

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {v8, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar$Tickmark;

    iget v0, v0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar$Tickmark;->mBottom:F

    iget v6, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mMinGapBetweenStacks:I

    int-to-float v6, v6

    add-float/2addr v0, v6

    cmpg-float v0, v5, v0

    if-gtz v0, :cond_1

    .line 316
    invoke-interface {v8, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 317
    add-int/lit8 v4, v4, 0x1

    move-object v0, v2

    goto :goto_1

    :cond_0
    move-object v2, v0

    .line 324
    :cond_1
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v9

    .line 325
    iget v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mMinGapBetweenStacks:I

    int-to-float v0, v0

    add-float v5, v1, v0

    .line 326
    add-int/lit8 v0, v9, -0x1

    invoke-interface {v8, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar$Tickmark;

    iget v1, v0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar$Tickmark;->mBottom:F

    .line 327
    add-int/lit8 v0, v9, -0x1

    iget v6, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mStackedResultHeight:I

    mul-int/2addr v0, v6

    int-to-float v0, v0

    sub-float v0, v1, v0

    iget v6, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mResultMinHeight:I

    int-to-float v6, v6

    sub-float v6, v0, v6

    .line 330
    invoke-interface {v8, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar$Tickmark;

    iget v0, v0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar$Tickmark;->mTop:F

    .line 331
    invoke-static {v6, v5, v0}, Lorg/chromium/chrome/browser/util/MathUtils;->clamp(FFF)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    int-to-float v10, v0

    .line 332
    cmpl-float v0, v10, v6

    if-ltz v0, :cond_3

    const/high16 v0, 0x3f800000    # 1.0f

    move v7, v0

    .line 334
    :goto_2
    const/4 v0, 0x1

    if-ne v9, v0, :cond_4

    const/4 v0, 0x0

    move v5, v0

    :goto_3
    move v6, v3

    .line 336
    :goto_4
    if-ge v6, v9, :cond_5

    .line 337
    invoke-interface {v8, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar$Tickmark;

    .line 338
    int-to-float v11, v6

    mul-float/2addr v11, v5

    add-float/2addr v11, v10

    iput v11, v0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar$Tickmark;->mTop:F

    .line 339
    add-int/lit8 v11, v9, -0x1

    if-eq v6, v11, :cond_2

    .line 340
    iget v11, v0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar$Tickmark;->mTop:F

    iget v12, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mResultMinHeight:I

    int-to-float v12, v12

    mul-float/2addr v12, v7

    add-float/2addr v11, v12

    iput v11, v0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar$Tickmark;->mBottom:F

    .line 342
    :cond_2
    iget-object v11, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mTickmarks:Ljava/util/ArrayList;

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 336
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_4

    .line 332
    :cond_3
    sub-float v0, v1, v10

    sub-float v5, v1, v6

    div-float/2addr v0, v5

    move v7, v0

    goto :goto_2

    .line 334
    :cond_4
    sub-float v0, v1, v10

    iget v5, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mResultMinHeight:I

    int-to-float v5, v5

    mul-float/2addr v5, v7

    sub-float/2addr v0, v5

    add-int/lit8 v5, v9, -0x1

    int-to-float v5, v5

    div-float/2addr v0, v5

    move v5, v0

    goto :goto_3

    :cond_5
    move-object v0, v2

    move v2, v4

    .line 344
    goto/16 :goto_0

    .line 345
    :cond_6
    return-void
.end method

.method private expandTickmarkToMinHeight(Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar$Tickmark;Z)Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar$Tickmark;
    .locals 5

    .prologue
    const/high16 v4, 0x40000000    # 2.0f

    .line 359
    if-eqz p2, :cond_1

    iget v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mActiveMinHeight:I

    .line 360
    :goto_0
    int-to-float v0, v0

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar$Tickmark;->height()F

    move-result v1

    sub-float v1, v0, v1

    .line 361
    const/4 v0, 0x0

    cmpl-float v0, v1, v0

    if-lez v0, :cond_0

    .line 362
    new-instance v0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar$Tickmark;

    iget v2, p1, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar$Tickmark;->mTop:F

    div-float v3, v1, v4

    sub-float/2addr v2, v3

    iget v3, p1, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar$Tickmark;->mBottom:F

    div-float/2addr v1, v4

    add-float/2addr v1, v3

    invoke-direct {v0, p0, v2, v1}, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar$Tickmark;-><init>(Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;FF)V

    move-object p1, v0

    .line 365
    :cond_0
    return-object p1

    .line 359
    :cond_1
    iget v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mResultMinHeight:I

    goto :goto_0
.end method

.method private getLeftMargin()I
    .locals 2

    .prologue
    .line 289
    invoke-static {}, Lorg/chromium/ui/base/LocalizationUtils;->isLayoutRtl()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->getWidth()I

    move-result v0

    iget v1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mBarDrawWidth:I

    sub-int/2addr v0, v1

    goto :goto_0
.end method

.method private tickmarkForRect(Landroid/graphics/RectF;Z)Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar$Tickmark;
    .locals 4

    .prologue
    .line 349
    iget v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mBarHeightForWhichTickmarksWereCached:I

    iget v1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mBarVerticalPadding:I

    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    int-to-float v0, v0

    .line 351
    new-instance v1, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar$Tickmark;

    iget v2, p1, Landroid/graphics/RectF;->top:F

    mul-float/2addr v2, v0

    iget v3, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mBarVerticalPadding:I

    int-to-float v3, v3

    add-float/2addr v2, v3

    iget v3, p1, Landroid/graphics/RectF;->bottom:F

    mul-float/2addr v0, v3

    iget v3, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mBarVerticalPadding:I

    int-to-float v3, v3

    add-float/2addr v0, v3

    invoke-direct {v1, p0, v2, v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar$Tickmark;-><init>(Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;FF)V

    .line 354
    invoke-direct {p0, v1, p2}, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->expandTickmarkToMinHeight(Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar$Tickmark;Z)Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar$Tickmark;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public clearMatchRects()V
    .locals 3

    .prologue
    .line 178
    const/4 v0, -0x1

    const/4 v1, 0x0

    new-array v1, v1, [Landroid/graphics/RectF;

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->setMatchRects(I[Landroid/graphics/RectF;Landroid/graphics/RectF;)V

    .line 179
    return-void
.end method

.method public dismiss()V
    .locals 5

    .prologue
    .line 143
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mVisibilityAnimation:Landroid/animation/Animator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mVisibilityAnimation:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 144
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mVisibilityAnimation:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->cancel()V

    .line 147
    :cond_0
    sget-object v0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->TRANSLATION_X:Landroid/util/Property;

    const/4 v1, 0x1

    new-array v1, v1, [F

    const/4 v2, 0x0

    iget v3, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mBarTouchWidth:I

    invoke-static {}, Lorg/chromium/ui/base/LocalizationUtils;->isLayoutRtl()Z

    move-result v4

    invoke-static {v3, v4}, Lcom/google/android/apps/chrome/utilities/ChromeMathUtils;->flipSignIf(IZ)I

    move-result v3

    int-to-float v3, v3

    aput v3, v1, v2

    invoke-static {p0, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mVisibilityAnimation:Landroid/animation/Animator;

    .line 149
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mVisibilityAnimation:Landroid/animation/Animator;

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 150
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mVisibilityAnimation:Landroid/animation/Animator;

    sget-object v1, Lorg/chromium/ui/interpolators/BakedBezierInterpolator;->FADE_OUT_CURVE:Lorg/chromium/ui/interpolators/BakedBezierInterpolator;

    invoke-virtual {v0, v1}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 151
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mVisibilityAnimation:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    .line 152
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mVisibilityAnimation:Landroid/animation/Animator;

    new-instance v1, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar$2;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar$2;-><init>(Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;)V

    invoke-virtual {v0, v1}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 160
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/high16 v8, 0x3f000000    # 0.5f

    const/4 v2, 0x0

    const/high16 v7, 0x40000000    # 2.0f

    .line 234
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 236
    invoke-direct {p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->getLeftMargin()I

    move-result v6

    .line 237
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mFillPaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mBackgroundColor:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 238
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mStrokePaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mBackgroundBorderColor:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 239
    int-to-float v1, v6

    iget v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mBarDrawWidth:I

    add-int/2addr v0, v6

    int-to-float v3, v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->getHeight()I

    move-result v0

    int-to-float v4, v0

    iget-object v5, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mFillPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 241
    invoke-static {}, Lorg/chromium/ui/base/LocalizationUtils;->isLayoutRtl()Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mBarDrawWidth:I

    add-int/2addr v0, v6

    int-to-float v0, v0

    sub-float v1, v0, v8

    .line 244
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->getHeight()I

    move-result v0

    int-to-float v4, v0

    iget-object v5, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mStrokePaint:Landroid/graphics/Paint;

    move-object v0, p1

    move v3, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 246
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mMatches:[Landroid/graphics/RectF;

    array-length v0, v0

    if-nez v0, :cond_2

    .line 286
    :cond_0
    :goto_1
    return-void

    .line 241
    :cond_1
    int-to-float v0, v6

    add-float v1, v0, v8

    goto :goto_0

    .line 250
    :cond_2
    iget v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mBarHeightForWhichTickmarksWereCached:I

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->getHeight()I

    move-result v1

    if-eq v0, v1, :cond_3

    .line 251
    invoke-direct {p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->calculateTickmarks()V

    .line 256
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mFillPaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mResultColor:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 257
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mStrokePaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mResultBorderColor:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 258
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mTickmarks:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar$Tickmark;

    .line 259
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar$Tickmark;->toRectF()Landroid/graphics/RectF;

    move-result-object v0

    .line 260
    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mFillPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v7, v7, v2}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 261
    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mStrokePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v7, v7, v2}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    goto :goto_2

    .line 266
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mActiveMatch:Landroid/graphics/RectF;

    if-eqz v0, :cond_0

    .line 268
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mMatches:[Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mActiveMatch:Landroid/graphics/RectF;

    sget-object v2, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->sComparator:Ljava/util/Comparator;

    invoke-static {v0, v1, v2}, Ljava/util/Arrays;->binarySearch([Ljava/lang/Object;Ljava/lang/Object;Ljava/util/Comparator;)I

    move-result v0

    .line 269
    if-ltz v0, :cond_5

    .line 274
    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mTickmarks:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar$Tickmark;

    invoke-direct {p0, v0, v9}, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->expandTickmarkToMinHeight(Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar$Tickmark;Z)Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar$Tickmark;

    move-result-object v0

    .line 280
    :goto_3
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar$Tickmark;->toRectF()Landroid/graphics/RectF;

    move-result-object v0

    .line 281
    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mFillPaint:Landroid/graphics/Paint;

    iget v2, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mActiveColor:I

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 282
    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mStrokePaint:Landroid/graphics/Paint;

    iget v2, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mActiveBorderColor:I

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 283
    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mFillPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v7, v7, v1}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 284
    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mStrokePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v7, v7, v1}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    goto :goto_1

    .line 278
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mActiveMatch:Landroid/graphics/RectF;

    invoke-direct {p0, v0, v9}, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->tickmarkForRect(Landroid/graphics/RectF;Z)Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar$Tickmark;

    move-result-object v0

    goto :goto_3
.end method

.method protected onSizeChanged(IIII)V
    .locals 2

    .prologue
    .line 227
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/View;->onSizeChanged(IIII)V

    .line 229
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mMatches:[Landroid/graphics/RectF;

    array-length v0, v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mTab:Lcom/google/android/apps/chrome/tab/ChromeTab;

    iget v1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mRectsVersion:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->requestFindMatchRects(I)V

    .line 230
    :cond_0
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 183
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mTickmarks:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mTickmarks:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget-object v3, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mMatches:[Landroid/graphics/RectF;

    array-length v3, v3

    if-ne v0, v3, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mWaitingForActivateAck:Z

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v3, 0x3

    if-eq v0, v3, :cond_0

    .line 190
    invoke-static {p0}, Lorg/chromium/ui/UiUtils;->hideKeyboard(Landroid/view/View;)Z

    .line 193
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mTickmarks:Ljava/util/ArrayList;

    new-instance v3, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar$Tickmark;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    invoke-direct {v3, p0, v4, v5}, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar$Tickmark;-><init>(Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;FF)V

    invoke-static {v0, v3}, Ljava/util/Collections;->binarySearch(Ljava/util/List;Ljava/lang/Object;)I

    move-result v0

    .line 195
    if-gez v0, :cond_4

    .line 197
    rsub-int/lit8 v3, v0, -0x1

    .line 198
    if-nez v3, :cond_1

    .line 217
    :goto_0
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mWaitingForActivateAck:Z

    .line 218
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mTab:Lcom/google/android/apps/chrome/tab/ChromeTab;

    iget-object v3, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mMatches:[Landroid/graphics/RectF;

    aget-object v3, v3, v2

    invoke-virtual {v3}, Landroid/graphics/RectF;->centerX()F

    move-result v3

    iget-object v4, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mMatches:[Landroid/graphics/RectF;

    aget-object v2, v4, v2

    invoke-virtual {v2}, Landroid/graphics/RectF;->centerY()F

    move-result v2

    invoke-virtual {v0, v3, v2}, Lcom/google/android/apps/chrome/tab/ChromeTab;->activateNearestFindResult(FF)V

    .line 222
    :cond_0
    return v1

    .line 200
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mTickmarks:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ne v3, v0, :cond_2

    .line 201
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mTickmarks:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    move v2, v0

    move v0, v1

    .line 207
    :goto_1
    sub-int/2addr v2, v0

    goto :goto_0

    .line 203
    :cond_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mTickmarks:Ljava/util/ArrayList;

    add-int/lit8 v5, v3, -0x1

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar$Tickmark;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar$Tickmark;->centerY()F

    move-result v0

    sub-float v0, v4, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v4

    .line 205
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mTickmarks:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar$Tickmark;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar$Tickmark;->centerY()F

    move-result v0

    sub-float v0, v5, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    .line 207
    cmpg-float v0, v4, v0

    if-gtz v0, :cond_3

    move v0, v1

    move v2, v3

    goto :goto_1

    :cond_3
    move v0, v2

    move v2, v3

    goto :goto_1

    :cond_4
    move v2, v0

    goto :goto_0
.end method

.method public setMatchRects(I[Landroid/graphics/RectF;Landroid/graphics/RectF;)V
    .locals 2

    .prologue
    .line 164
    iget v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mRectsVersion:I

    if-eq v0, p1, :cond_1

    .line 165
    iput p1, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mRectsVersion:I

    .line 166
    sget-boolean v0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 167
    :cond_0
    iput-object p2, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mMatches:[Landroid/graphics/RectF;

    .line 168
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mTickmarks:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 169
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mMatches:[Landroid/graphics/RectF;

    sget-object v1, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->sComparator:Ljava/util/Comparator;

    invoke-static {v0, v1}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    .line 170
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mBarHeightForWhichTickmarksWereCached:I

    .line 172
    :cond_1
    iput-object p3, p0, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->mActiveMatch:Landroid/graphics/RectF;

    .line 173
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/findinpage/FindResultBar;->invalidate()V

    .line 174
    return-void
.end method
