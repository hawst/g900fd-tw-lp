.class public Lcom/google/android/apps/chrome/preferences/DocumentModeEnabler;
.super Lcom/google/android/apps/chrome/preferences/ChromeBaseSwitchEnabler;
.source "DocumentModeEnabler.java"


# instance fields
.field private final mActivity:Landroid/app/Activity;

.field private final mPrefManager:Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

.field private final mSwitch:Landroid/widget/Switch;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Landroid/widget/Switch;)V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/preferences/ChromeBaseSwitchEnabler;-><init>(Landroid/content/Context;Landroid/widget/Switch;)V

    .line 36
    iput-object p1, p0, Lcom/google/android/apps/chrome/preferences/DocumentModeEnabler;->mActivity:Landroid/app/Activity;

    .line 37
    iput-object p2, p0, Lcom/google/android/apps/chrome/preferences/DocumentModeEnabler;->mSwitch:Landroid/widget/Switch;

    .line 38
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/DocumentModeEnabler;->mActivity:Landroid/app/Activity;

    invoke-static {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/preferences/DocumentModeEnabler;->mPrefManager:Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    .line 39
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/preferences/DocumentModeEnabler;)Landroid/widget/Switch;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/DocumentModeEnabler;->mSwitch:Landroid/widget/Switch;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/preferences/DocumentModeEnabler;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/DocumentModeEnabler;->mActivity:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/preferences/DocumentModeEnabler;Z)Z
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/preferences/DocumentModeEnabler;->isRestartNeeded(Z)Z

    move-result v0

    return v0
.end method

.method private isRestartNeeded(Z)Z
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 93
    if-eqz p1, :cond_1

    move v2, v3

    .line 108
    :cond_0
    :goto_0
    return v2

    .line 94
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/DocumentModeEnabler;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/DocumentModeEnabler;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "com.android.chrome.invoked_from_fre"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v3

    .line 96
    :goto_1
    if-nez v0, :cond_3

    move v2, v3

    goto :goto_0

    :cond_2
    move v0, v2

    .line 94
    goto :goto_1

    .line 98
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/DocumentModeEnabler;->mActivity:Landroid/app/Activity;

    const-string/jumbo v1, "activity"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 100
    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/DocumentModeEnabler;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    .line 101
    invoke-virtual {v0}, Landroid/app/ActivityManager;->getAppTasks()Ljava/util/List;

    move-result-object v5

    move v1, v2

    .line 103
    :goto_2
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 104
    invoke-interface {v5, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager$AppTask;

    invoke-static {v0, v4}, Lcom/google/android/apps/chrome/document/ChromeLauncherActivity;->getTaskClassName(Landroid/app/ActivityManager$AppTask;Landroid/content/pm/PackageManager;)Ljava/lang/String;

    move-result-object v0

    .line 105
    if-eqz v0, :cond_4

    .line 106
    invoke-static {v0}, Lcom/google/android/apps/chrome/document/ChromeLauncherActivity;->isADocumentActivity(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    move v2, v3

    goto :goto_0

    .line 103
    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2
.end method


# virtual methods
.method protected isSwitchEnabled()Z
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/DocumentModeEnabler;->mPrefManager:Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->isOptedOutOfDocumentMode()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onValueChanged(Z)V
    .locals 3

    .prologue
    .line 43
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/DocumentModeEnabler;->isSwitchEnabled()Z

    move-result v0

    if-ne p1, v0, :cond_0

    .line 82
    :goto_0
    return-void

    .line 44
    :cond_0
    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/DocumentModeEnabler;->mActivity:Landroid/app/Activity;

    invoke-direct {v1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    if-eqz p1, :cond_1

    sget v0, Lcom/google/android/apps/chrome/R$string;->tabs_and_apps_turn_on_title:I

    :goto_1
    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    if-eqz p1, :cond_2

    sget v0, Lcom/google/android/apps/chrome/R$string;->tabs_and_apps_opt_in_confirmation:I

    :goto_2
    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$string;->cancel:I

    new-instance v2, Lcom/google/android/apps/chrome/preferences/DocumentModeEnabler$3;

    invoke-direct {v2, p0}, Lcom/google/android/apps/chrome/preferences/DocumentModeEnabler$3;-><init>(Lcom/google/android/apps/chrome/preferences/DocumentModeEnabler;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$string;->ok:I

    new-instance v2, Lcom/google/android/apps/chrome/preferences/DocumentModeEnabler$2;

    invoke-direct {v2, p0, p1}, Lcom/google/android/apps/chrome/preferences/DocumentModeEnabler$2;-><init>(Lcom/google/android/apps/chrome/preferences/DocumentModeEnabler;Z)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/chrome/preferences/DocumentModeEnabler$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/preferences/DocumentModeEnabler$1;-><init>(Lcom/google/android/apps/chrome/preferences/DocumentModeEnabler;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 81
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto :goto_0

    .line 44
    :cond_1
    sget v0, Lcom/google/android/apps/chrome/R$string;->tabs_and_apps_turn_off_title:I

    goto :goto_1

    :cond_2
    sget v0, Lcom/google/android/apps/chrome/R$string;->tabs_and_apps_opt_out_confirmation:I

    goto :goto_2
.end method
