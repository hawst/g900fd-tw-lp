.class Lcom/google/android/apps/chrome/ntp/BookmarksPage$3;
.super Ljava/lang/Object;
.source "BookmarksPage.java"

# interfaces
.implements Lorg/chromium/chrome/browser/BookmarksBridge$BookmarksCallback;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/ntp/BookmarksPage;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/ntp/BookmarksPage;)V
    .locals 0

    .prologue
    .line 484
    iput-object p1, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPage$3;->this$0:Lcom/google/android/apps/chrome/ntp/BookmarksPage;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onBookmarksAvailable(Lorg/chromium/components/bookmarks/BookmarkId;Ljava/util/List;)V
    .locals 1

    .prologue
    .line 496
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPage$3;->this$0:Lcom/google/android/apps/chrome/ntp/BookmarksPage;

    # setter for: Lcom/google/android/apps/chrome/ntp/BookmarksPage;->mCurrentFolderId:Lorg/chromium/components/bookmarks/BookmarkId;
    invoke-static {v0, p1}, Lcom/google/android/apps/chrome/ntp/BookmarksPage;->access$502(Lcom/google/android/apps/chrome/ntp/BookmarksPage;Lorg/chromium/components/bookmarks/BookmarkId;)Lorg/chromium/components/bookmarks/BookmarkId;

    .line 497
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPage$3;->this$0:Lcom/google/android/apps/chrome/ntp/BookmarksPage;

    # invokes: Lcom/google/android/apps/chrome/ntp/BookmarksPage;->updateLastUsedFolderId()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/BookmarksPage;->access$700(Lcom/google/android/apps/chrome/ntp/BookmarksPage;)V

    .line 498
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPage$3;->this$0:Lcom/google/android/apps/chrome/ntp/BookmarksPage;

    # getter for: Lcom/google/android/apps/chrome/ntp/BookmarksPage;->mPageView:Lcom/google/android/apps/chrome/ntp/BookmarksPageView;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/BookmarksPage;->access$600(Lcom/google/android/apps/chrome/ntp/BookmarksPage;)Lcom/google/android/apps/chrome/ntp/BookmarksPageView;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->onBookmarksAvailable(Lorg/chromium/components/bookmarks/BookmarkId;Ljava/util/List;)V

    .line 499
    return-void
.end method

.method public onBookmarksFolderHierarchyAvailable(Lorg/chromium/components/bookmarks/BookmarkId;Ljava/util/List;)V
    .locals 1

    .prologue
    .line 488
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPage$3;->this$0:Lcom/google/android/apps/chrome/ntp/BookmarksPage;

    # getter for: Lcom/google/android/apps/chrome/ntp/BookmarksPage;->mPageView:Lcom/google/android/apps/chrome/ntp/BookmarksPageView;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/BookmarksPage;->access$600(Lcom/google/android/apps/chrome/ntp/BookmarksPage;)Lcom/google/android/apps/chrome/ntp/BookmarksPageView;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->onBookmarksFolderHierarchyAvailable(Lorg/chromium/components/bookmarks/BookmarkId;Ljava/util/List;)V

    .line 489
    return-void
.end method
