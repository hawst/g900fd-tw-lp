.class Lcom/google/android/apps/chrome/ChromeTabbedActivity$13;
.super Ljava/lang/Object;
.source "ChromeTabbedActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/ChromeTabbedActivity;

.field final synthetic val$currentTab:Lcom/google/android/apps/chrome/tab/ChromeTab;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/ChromeTabbedActivity;Lcom/google/android/apps/chrome/tab/ChromeTab;)V
    .locals 0

    .prologue
    .line 1062
    iput-object p1, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity$13;->this$0:Lcom/google/android/apps/chrome/ChromeTabbedActivity;

    iput-object p2, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity$13;->val$currentTab:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 1065
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity$13;->this$0:Lcom/google/android/apps/chrome/ChromeTabbedActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getTabModelSelector()Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->getModel(Z)Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v0

    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->getProfile()Lorg/chromium/chrome/browser/profiles/Profile;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;->isEnhancedBookmarkEnabled(Lorg/chromium/chrome/browser/profiles/Profile;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1067
    new-instance v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;

    iget-object v1, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity$13;->this$0:Lcom/google/android/apps/chrome/ChromeTabbedActivity;

    iget-object v2, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity$13;->this$0:Lcom/google/android/apps/chrome/ChromeTabbedActivity;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getTabModelSelector()Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;-><init>(Landroid/app/Activity;Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;)V

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDialog;->show()V

    .line 1074
    :goto_0
    return-void

    .line 1070
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity$13;->val$currentTab:Lcom/google/android/apps/chrome/tab/ChromeTab;

    new-instance v1, Lorg/chromium/content_public/browser/LoadUrlParams;

    const-string/jumbo v2, "chrome-native://bookmarks/"

    const/4 v3, 0x2

    invoke-direct {v1, v2, v3}, Lorg/chromium/content_public/browser/LoadUrlParams;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->loadUrl(Lorg/chromium/content_public/browser/LoadUrlParams;)I

    goto :goto_0
.end method
