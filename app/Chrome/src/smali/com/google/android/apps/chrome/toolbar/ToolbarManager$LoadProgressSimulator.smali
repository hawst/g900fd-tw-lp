.class Lcom/google/android/apps/chrome/toolbar/ToolbarManager$LoadProgressSimulator;
.super Ljava/lang/Object;
.source "ToolbarManager.java"


# instance fields
.field private final mHandler:Landroid/os/Handler;

.field private mProgress:I

.field private final mToolbar:Lcom/google/android/apps/chrome/toolbar/ToolbarManager;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/chrome/toolbar/ToolbarManager;)V
    .locals 2

    .prologue
    .line 703
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 704
    iput-object p1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$LoadProgressSimulator;->mToolbar:Lcom/google/android/apps/chrome/toolbar/ToolbarManager;

    .line 705
    new-instance v0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$LoadProgressSimulator$1;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$LoadProgressSimulator$1;-><init>(Lcom/google/android/apps/chrome/toolbar/ToolbarManager$LoadProgressSimulator;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$LoadProgressSimulator;->mHandler:Landroid/os/Handler;

    .line 716
    return-void
.end method

.method static synthetic access$2500(Lcom/google/android/apps/chrome/toolbar/ToolbarManager$LoadProgressSimulator;)I
    .locals 1

    .prologue
    .line 692
    iget v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$LoadProgressSimulator;->mProgress:I

    return v0
.end method

.method static synthetic access$2502(Lcom/google/android/apps/chrome/toolbar/ToolbarManager$LoadProgressSimulator;I)I
    .locals 0

    .prologue
    .line 692
    iput p1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$LoadProgressSimulator;->mProgress:I

    return p1
.end method

.method static synthetic access$2512(Lcom/google/android/apps/chrome/toolbar/ToolbarManager$LoadProgressSimulator;I)I
    .locals 1

    .prologue
    .line 692
    iget v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$LoadProgressSimulator;->mProgress:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$LoadProgressSimulator;->mProgress:I

    return v0
.end method

.method static synthetic access$2600(Lcom/google/android/apps/chrome/toolbar/ToolbarManager$LoadProgressSimulator;)Lcom/google/android/apps/chrome/toolbar/ToolbarManager;
    .locals 1

    .prologue
    .line 692
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$LoadProgressSimulator;->mToolbar:Lcom/google/android/apps/chrome/toolbar/ToolbarManager;

    return-object v0
.end method


# virtual methods
.method public cancel()V
    .locals 2

    .prologue
    .line 730
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$LoadProgressSimulator;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 731
    return-void
.end method

.method public start()V
    .locals 2

    .prologue
    .line 722
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$LoadProgressSimulator;->mProgress:I

    .line 723
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$LoadProgressSimulator;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 724
    return-void
.end method
