.class Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkMainActionBar$2;
.super Ljava/lang/Object;
.source "EnhancedBookmarkMainActionBar.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkMainActionBar;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    const-class v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkMainActionBar;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkMainActionBar$2;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkMainActionBar;)V
    .locals 0

    .prologue
    .line 43
    iput-object p1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkMainActionBar$2;->this$0:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkMainActionBar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkMainActionBar$2;->this$0:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkMainActionBar;

    # getter for: Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkMainActionBar;->mNavigationButton:I
    invoke-static {v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkMainActionBar;->access$100(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkMainActionBar;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 54
    sget-boolean v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkMainActionBar$2;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    const-string/jumbo v1, "Incorrect navigation button state"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 48
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkMainActionBar$2;->this$0:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkMainActionBar;

    # getter for: Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkMainActionBar;->mDelegate:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;
    invoke-static {v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkMainActionBar;->access$000(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkMainActionBar;)Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;->toggleDrawer()V

    .line 56
    :cond_0
    :goto_0
    return-void

    .line 51
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkMainActionBar$2;->this$0:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkMainActionBar;

    # getter for: Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkMainActionBar;->mDelegate:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;
    invoke-static {v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkMainActionBar;->access$000(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkMainActionBar;)Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkMainActionBar$2;->this$0:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkMainActionBar;

    # getter for: Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkMainActionBar;->mCurrentFolder:Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;
    invoke-static {v1}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkMainActionBar;->access$200(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkMainActionBar;)Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;

    move-result-object v1

    invoke-virtual {v1}, Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;->getParentId()Lorg/chromium/components/bookmarks/BookmarkId;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;->setFolderMode(Lorg/chromium/components/bookmarks/BookmarkId;)V

    goto :goto_0

    .line 46
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
