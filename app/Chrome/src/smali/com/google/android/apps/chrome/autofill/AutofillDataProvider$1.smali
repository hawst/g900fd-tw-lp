.class Lcom/google/android/apps/chrome/autofill/AutofillDataProvider$1;
.super Ljava/lang/Object;
.source "AutofillDataProvider.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/autofill/AutofillDataProvider;

.field final synthetic val$values:Landroid/content/ContentValues;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/autofill/AutofillDataProvider;Landroid/content/ContentValues;)V
    .locals 0

    .prologue
    .line 180
    iput-object p1, p0, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider$1;->this$0:Lcom/google/android/apps/chrome/autofill/AutofillDataProvider;

    iput-object p2, p0, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider$1;->val$values:Landroid/content/ContentValues;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 180
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider$1;->call()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public call()Ljava/lang/String;
    .locals 15

    .prologue
    .line 183
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider$1;->val$values:Landroid/content/ContentValues;

    const-string/jumbo v2, "ADDRESS_LINE1"

    invoke-virtual {v1, v2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider$1;->val$values:Landroid/content/ContentValues;

    const-string/jumbo v2, "ADDRESS_LINE2"

    invoke-virtual {v1, v2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 187
    new-instance v0, Lorg/chromium/chrome/browser/autofill/PersonalDataManager$AutofillProfile;

    iget-object v1, p0, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider$1;->val$values:Landroid/content/ContentValues;

    const-string/jumbo v2, "GUID"

    invoke-virtual {v1, v2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider$1;->val$values:Landroid/content/ContentValues;

    const-string/jumbo v3, "ORIGIN"

    invoke-virtual {v2, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider$1;->val$values:Landroid/content/ContentValues;

    const-string/jumbo v4, "NAME_FULL"

    invoke-virtual {v3, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider$1;->val$values:Landroid/content/ContentValues;

    const-string/jumbo v6, "COMPANY_NAME"

    invoke-virtual {v4, v6}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iget-object v6, p0, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider$1;->val$values:Landroid/content/ContentValues;

    const-string/jumbo v7, "ADDRESS_STATE"

    invoke-virtual {v6, v7}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider$1;->val$values:Landroid/content/ContentValues;

    const-string/jumbo v8, "ADDRESS_CITY"

    invoke-virtual {v7, v8}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string/jumbo v8, ""

    iget-object v9, p0, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider$1;->val$values:Landroid/content/ContentValues;

    const-string/jumbo v10, "ADDRESS_ZIP"

    invoke-virtual {v9, v10}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    const-string/jumbo v10, ""

    iget-object v11, p0, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider$1;->val$values:Landroid/content/ContentValues;

    const-string/jumbo v12, "ADDRESS_COUNTRY_CODE"

    invoke-virtual {v11, v12}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    iget-object v12, p0, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider$1;->val$values:Landroid/content/ContentValues;

    const-string/jumbo v13, "PHONE_WHOLE_NUMBER"

    invoke-virtual {v12, v13}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    iget-object v13, p0, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider$1;->val$values:Landroid/content/ContentValues;

    const-string/jumbo v14, "EMAIL_ADDRESS"

    invoke-virtual {v13, v14}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    const-string/jumbo v14, ""

    invoke-direct/range {v0 .. v14}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager$AutofillProfile;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 202
    invoke-static {}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager;->getInstance()Lorg/chromium/chrome/browser/autofill/PersonalDataManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager;->setProfile(Lorg/chromium/chrome/browser/autofill/PersonalDataManager$AutofillProfile;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
