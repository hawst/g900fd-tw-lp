.class Lcom/google/android/apps/chrome/omnibox/UrlBar$4;
.super Landroid/view/inputmethod/InputConnectionWrapper;
.source "UrlBar.java"


# instance fields
.field private final mTempSelectionChar:[C

.field final synthetic this$0:Lcom/google/android/apps/chrome/omnibox/UrlBar;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/omnibox/UrlBar;Landroid/view/inputmethod/InputConnection;Z)V
    .locals 1

    .prologue
    .line 782
    iput-object p1, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar$4;->this$0:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    invoke-direct {p0, p2, p3}, Landroid/view/inputmethod/InputConnectionWrapper;-><init>(Landroid/view/inputmethod/InputConnection;Z)V

    .line 783
    const/4 v0, 0x1

    new-array v0, v0, [C

    iput-object v0, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar$4;->mTempSelectionChar:[C

    return-void
.end method


# virtual methods
.method public commitText(Ljava/lang/CharSequence;I)Z
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v6, 0x0

    .line 787
    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar$4;->this$0:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->getText()Landroid/text/Editable;

    move-result-object v1

    .line 788
    if-nez v1, :cond_1

    invoke-super {p0, p1, p2}, Landroid/view/inputmethod/InputConnectionWrapper;->commitText(Ljava/lang/CharSequence;I)Z

    move-result v0

    .line 832
    :cond_0
    :goto_0
    return v0

    .line 790
    :cond_1
    invoke-static {v1}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v2

    .line 791
    invoke-static {v1}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v3

    .line 792
    iget-object v4, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar$4;->this$0:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    # getter for: Lcom/google/android/apps/chrome/omnibox/UrlBar;->mAutocompleteSpan:Lcom/google/android/apps/chrome/omnibox/UrlBar$AutocompleteSpan;
    invoke-static {v4}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->access$300(Lcom/google/android/apps/chrome/omnibox/UrlBar;)Lcom/google/android/apps/chrome/omnibox/UrlBar$AutocompleteSpan;

    move-result-object v4

    invoke-interface {v1, v4}, Landroid/text/Editable;->getSpanStart(Ljava/lang/Object;)I

    move-result v4

    .line 801
    if-ne p2, v0, :cond_4

    if-lez v2, :cond_4

    if-eq v2, v3, :cond_4

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v5

    if-lt v3, v5, :cond_4

    if-ne v4, v2, :cond_4

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v4

    if-ne v4, v0, :cond_4

    .line 805
    add-int/lit8 v4, v2, 0x1

    iget-object v5, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar$4;->mTempSelectionChar:[C

    invoke-interface {v1, v2, v4, v5, v6}, Landroid/text/Editable;->getChars(II[CI)V

    .line 806
    iget-object v4, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar$4;->mTempSelectionChar:[C

    aget-char v4, v4, v6

    invoke-interface {p1, v6}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v5

    if-ne v4, v5, :cond_4

    .line 810
    iget-object v4, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar$4;->this$0:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    # getter for: Lcom/google/android/apps/chrome/omnibox/UrlBar;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;
    invoke-static {v4}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->access$400(Lcom/google/android/apps/chrome/omnibox/UrlBar;)Landroid/view/accessibility/AccessibilityManager;

    move-result-object v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar$4;->this$0:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    # getter for: Lcom/google/android/apps/chrome/omnibox/UrlBar;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;
    invoke-static {v4}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->access$400(Lcom/google/android/apps/chrome/omnibox/UrlBar;)Landroid/view/accessibility/AccessibilityManager;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 811
    const/16 v4, 0x10

    invoke-static {v4}, Landroid/view/accessibility/AccessibilityEvent;->obtain(I)Landroid/view/accessibility/AccessibilityEvent;

    move-result-object v4

    .line 813
    invoke-virtual {v4, v2}, Landroid/view/accessibility/AccessibilityEvent;->setFromIndex(I)V

    .line 814
    invoke-virtual {v4, v6}, Landroid/view/accessibility/AccessibilityEvent;->setRemovedCount(I)V

    .line 815
    invoke-virtual {v4, v0}, Landroid/view/accessibility/AccessibilityEvent;->setAddedCount(I)V

    .line 816
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, v6, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/view/accessibility/AccessibilityEvent;->setBeforeText(Ljava/lang/CharSequence;)V

    .line 817
    iget-object v5, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar$4;->this$0:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    invoke-virtual {v5, v4}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->sendAccessibilityEventUnchecked(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 820
    :cond_2
    iget-object v4, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar$4;->this$0:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    # getter for: Lcom/google/android/apps/chrome/omnibox/UrlBar;->mLocationBarTextWatcher:Landroid/text/TextWatcher;
    invoke-static {v4}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->access$500(Lcom/google/android/apps/chrome/omnibox/UrlBar;)Landroid/text/TextWatcher;

    move-result-object v4

    if-eqz v4, :cond_3

    .line 821
    iget-object v4, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar$4;->this$0:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    # getter for: Lcom/google/android/apps/chrome/omnibox/UrlBar;->mLocationBarTextWatcher:Landroid/text/TextWatcher;
    invoke-static {v4}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->access$500(Lcom/google/android/apps/chrome/omnibox/UrlBar;)Landroid/text/TextWatcher;

    move-result-object v4

    invoke-interface {v4, v1, v6, v6, v6}, Landroid/text/TextWatcher;->beforeTextChanged(Ljava/lang/CharSequence;III)V

    .line 823
    :cond_3
    iget-object v4, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar$4;->this$0:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    iget-object v5, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar$4;->this$0:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    invoke-virtual {v5}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->getQueryTextOffset()I

    move-result v5

    add-int/lit8 v6, v2, 0x1

    invoke-interface {v1, v5, v6}, Landroid/text/Editable;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v5

    add-int/lit8 v2, v2, 0x1

    invoke-interface {v1, v2, v3}, Landroid/text/Editable;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v4, v5, v2}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->setAutocompleteText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 826
    iget-object v2, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar$4;->this$0:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    # getter for: Lcom/google/android/apps/chrome/omnibox/UrlBar;->mLocationBarTextWatcher:Landroid/text/TextWatcher;
    invoke-static {v2}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->access$500(Lcom/google/android/apps/chrome/omnibox/UrlBar;)Landroid/text/TextWatcher;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 827
    iget-object v2, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar$4;->this$0:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    # getter for: Lcom/google/android/apps/chrome/omnibox/UrlBar;->mLocationBarTextWatcher:Landroid/text/TextWatcher;
    invoke-static {v2}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->access$500(Lcom/google/android/apps/chrome/omnibox/UrlBar;)Landroid/text/TextWatcher;

    move-result-object v2

    invoke-interface {v2, v1}, Landroid/text/TextWatcher;->afterTextChanged(Landroid/text/Editable;)V

    goto/16 :goto_0

    .line 832
    :cond_4
    invoke-super {p0, p1, p2}, Landroid/view/inputmethod/InputConnectionWrapper;->commitText(Ljava/lang/CharSequence;I)Z

    move-result v0

    goto/16 :goto_0
.end method

.method public setComposingText(Ljava/lang/CharSequence;I)Z
    .locals 4

    .prologue
    .line 837
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar$4;->this$0:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->getText()Landroid/text/Editable;

    move-result-object v0

    .line 838
    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar$4;->this$0:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    # getter for: Lcom/google/android/apps/chrome/omnibox/UrlBar;->mAutocompleteSpan:Lcom/google/android/apps/chrome/omnibox/UrlBar$AutocompleteSpan;
    invoke-static {v1}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->access$300(Lcom/google/android/apps/chrome/omnibox/UrlBar;)Lcom/google/android/apps/chrome/omnibox/UrlBar$AutocompleteSpan;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/text/Editable;->getSpanStart(Ljava/lang/Object;)I

    move-result v1

    .line 839
    if-ltz v1, :cond_1

    .line 840
    invoke-static {v0}, Landroid/view/inputmethod/BaseInputConnection;->getComposingSpanEnd(Landroid/text/Spannable;)I

    move-result v2

    .line 861
    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v3

    if-ne v2, v3, :cond_0

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v2

    if-lt v1, v2, :cond_0

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v2

    sub-int v2, v1, v2

    invoke-interface {v0, v2, v1}, Landroid/text/Editable;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {v2, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 868
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v2

    sub-int v2, v1, v2

    invoke-virtual {p0, v2, v1}, Lcom/google/android/apps/chrome/omnibox/UrlBar$4;->setComposingRegion(II)Z

    .line 874
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/chrome/omnibox/UrlBar$4;->this$0:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    # getter for: Lcom/google/android/apps/chrome/omnibox/UrlBar;->mAutocompleteSpan:Lcom/google/android/apps/chrome/omnibox/UrlBar$AutocompleteSpan;
    invoke-static {v2}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->access$300(Lcom/google/android/apps/chrome/omnibox/UrlBar;)Lcom/google/android/apps/chrome/omnibox/UrlBar$AutocompleteSpan;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/omnibox/UrlBar$AutocompleteSpan;->clearSpan()V

    .line 875
    invoke-static {v0, v1}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    .line 876
    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/text/Editable;->delete(II)Landroid/text/Editable;

    .line 878
    :cond_1
    invoke-super {p0, p1, p2}, Landroid/view/inputmethod/InputConnectionWrapper;->setComposingText(Ljava/lang/CharSequence;I)Z

    move-result v0

    return v0
.end method
