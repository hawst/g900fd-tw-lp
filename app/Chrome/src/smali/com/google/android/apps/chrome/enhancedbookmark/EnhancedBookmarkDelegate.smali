.class interface abstract Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkDelegate;
.super Ljava/lang/Object;
.source "EnhancedBookmarkDelegate.java"


# static fields
.field public static final MODE_ALL_BOOKMARKS:I = 0x1

.field public static final MODE_EMPTY:I = 0x0

.field public static final MODE_FOLDER:I = 0x3

.field public static final MODE_UNCATEGORIZED:I = 0x2


# virtual methods
.method public abstract closeDialog()V
.end method

.method public abstract getFragmentManager()Landroid/app/FragmentManager;
.end method

.method public abstract getModel()Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;
.end method

.method public abstract notifyCurrentModeSet(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkUIObserver;)V
.end method

.method public abstract openBookmark(Lorg/chromium/components/bookmarks/BookmarkId;)V
.end method

.method public abstract setAllBookmarksMode()V
.end method

.method public abstract setFolderMode(Lorg/chromium/components/bookmarks/BookmarkId;)V
.end method

.method public abstract setUncategorizedMode()V
.end method

.method public abstract toggleDrawer()V
.end method
