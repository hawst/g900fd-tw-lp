.class public Lcom/google/android/apps/chrome/preferences/HyperlinkPreference;
.super Landroid/preference/Preference;
.source "HyperlinkPreference.java"


# instance fields
.field private final mTitleResId:I

.field private final mUrlResId:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 25
    invoke-direct {p0, p1, p2}, Landroid/preference/Preference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 26
    sget-object v0, Lcom/google/android/apps/chrome/R$styleable;->HyperlinkPreference:[I

    invoke-virtual {p1, p2, v0, v2, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 28
    sget v1, Lcom/google/android/apps/chrome/R$styleable;->HyperlinkPreference_url:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/chrome/preferences/HyperlinkPreference;->mUrlResId:I

    .line 29
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 30
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/HyperlinkPreference;->getTitleRes()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/preferences/HyperlinkPreference;->mTitleResId:I

    .line 31
    return-void
.end method


# virtual methods
.method protected onBindView(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 40
    invoke-super {p0, p1}, Landroid/preference/Preference;->onBindView(Landroid/view/View;)V

    .line 41
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/HyperlinkPreference;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->changeViewStyle(Landroid/view/View;Landroid/content/Context;)V

    .line 42
    return-void
.end method

.method protected onClick()V
    .locals 3

    .prologue
    .line 35
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/HyperlinkPreference;->getContext()Landroid/content/Context;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/chrome/preferences/HyperlinkPreference;->mTitleResId:I

    iget v2, p0, Lcom/google/android/apps/chrome/preferences/HyperlinkPreference;->mUrlResId:I

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/EmbedContentViewActivity;->show(Landroid/content/Context;II)V

    .line 36
    return-void
.end method
