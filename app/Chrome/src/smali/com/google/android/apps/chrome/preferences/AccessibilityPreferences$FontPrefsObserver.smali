.class Lcom/google/android/apps/chrome/preferences/AccessibilityPreferences$FontPrefsObserver;
.super Ljava/lang/Object;
.source "AccessibilityPreferences.java"

# interfaces
.implements Lorg/chromium/chrome/browser/accessibility/FontSizePrefs$Observer;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/preferences/AccessibilityPreferences;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/chrome/preferences/AccessibilityPreferences;)V
    .locals 0

    .prologue
    .line 42
    iput-object p1, p0, Lcom/google/android/apps/chrome/preferences/AccessibilityPreferences$FontPrefsObserver;->this$0:Lcom/google/android/apps/chrome/preferences/AccessibilityPreferences;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/chrome/preferences/AccessibilityPreferences;Lcom/google/android/apps/chrome/preferences/AccessibilityPreferences$1;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/preferences/AccessibilityPreferences$FontPrefsObserver;-><init>(Lcom/google/android/apps/chrome/preferences/AccessibilityPreferences;)V

    return-void
.end method


# virtual methods
.method public onChangeFontSize(F)V
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/AccessibilityPreferences$FontPrefsObserver;->this$0:Lcom/google/android/apps/chrome/preferences/AccessibilityPreferences;

    # invokes: Lcom/google/android/apps/chrome/preferences/AccessibilityPreferences;->processFontWithForceEnableZoom(F)V
    invoke-static {v0, p1}, Lcom/google/android/apps/chrome/preferences/AccessibilityPreferences;->access$000(Lcom/google/android/apps/chrome/preferences/AccessibilityPreferences;F)V

    .line 46
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/AccessibilityPreferences$FontPrefsObserver;->this$0:Lcom/google/android/apps/chrome/preferences/AccessibilityPreferences;

    # invokes: Lcom/google/android/apps/chrome/preferences/AccessibilityPreferences;->updateTextScaleSummary(F)V
    invoke-static {v0, p1}, Lcom/google/android/apps/chrome/preferences/AccessibilityPreferences;->access$100(Lcom/google/android/apps/chrome/preferences/AccessibilityPreferences;F)V

    .line 47
    return-void
.end method

.method public onChangeForceEnableZoom(Z)V
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/AccessibilityPreferences$FontPrefsObserver;->this$0:Lcom/google/android/apps/chrome/preferences/AccessibilityPreferences;

    # getter for: Lcom/google/android/apps/chrome/preferences/AccessibilityPreferences;->mForceEnableZoomPref:Lcom/google/android/apps/chrome/preferences/SeekBarLinkedCheckBoxPreference;
    invoke-static {v0}, Lcom/google/android/apps/chrome/preferences/AccessibilityPreferences;->access$200(Lcom/google/android/apps/chrome/preferences/AccessibilityPreferences;)Lcom/google/android/apps/chrome/preferences/SeekBarLinkedCheckBoxPreference;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/preferences/SeekBarLinkedCheckBoxPreference;->setChecked(Z)V

    .line 52
    return-void
.end method

.method public onChangeUserSetForceEnableZoom(Z)V
    .locals 0

    .prologue
    .line 55
    return-void
.end method
