.class public Lcom/google/android/apps/chrome/omnibox/LocationBarTablet;
.super Lcom/google/android/apps/chrome/omnibox/LocationBar;
.source "LocationBarTablet.java"


# instance fields
.field private mBookmarkButton:Landroid/view/View;

.field private final mKeyboardResizeModeTask:Ljava/lang/Runnable;

.field private mUrlFocusChangeAnimator:Landroid/animation/Animator;

.field private mUrlFocusChangePercent:F

.field private final mUrlFocusChangePercentProperty:Landroid/util/Property;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    .line 61
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/omnibox/LocationBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 32
    new-instance v0, Lcom/google/android/apps/chrome/omnibox/LocationBarTablet$1;

    const-class v1, Ljava/lang/Float;

    const-string/jumbo v2, ""

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/apps/chrome/omnibox/LocationBarTablet$1;-><init>(Lcom/google/android/apps/chrome/omnibox/LocationBarTablet;Ljava/lang/Class;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBarTablet;->mUrlFocusChangePercentProperty:Landroid/util/Property;

    .line 45
    new-instance v0, Lcom/google/android/apps/chrome/omnibox/LocationBarTablet$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/omnibox/LocationBarTablet$2;-><init>(Lcom/google/android/apps/chrome/omnibox/LocationBarTablet;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBarTablet;->mKeyboardResizeModeTask:Ljava/lang/Runnable;

    .line 62
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/omnibox/LocationBarTablet;)F
    .locals 1

    .prologue
    .line 27
    iget v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBarTablet;->mUrlFocusChangePercent:F

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/omnibox/LocationBarTablet;F)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/omnibox/LocationBarTablet;->setUrlFocusChangePercent(F)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/omnibox/LocationBarTablet;Z)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/omnibox/LocationBarTablet;->finishUrlFocusChange(Z)V

    return-void
.end method

.method private finishUrlFocusChange(Z)V
    .locals 4

    .prologue
    const/16 v3, 0x20

    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 119
    if-eqz p1, :cond_3

    .line 120
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBarTablet;->mSecurityButton:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBarTablet;->mSecurityButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 121
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBarTablet;->getWindowDelegate()Lcom/google/android/apps/chrome/WindowDelegate;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/WindowDelegate;->getWindowSoftInputMode()I

    move-result v0

    if-eq v0, v3, :cond_1

    .line 123
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBarTablet;->getWindowDelegate()Lcom/google/android/apps/chrome/WindowDelegate;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/google/android/apps/chrome/WindowDelegate;->setWindowSoftInputMode(I)V

    .line 126
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBarTablet;->mUrlBar:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->openKeyboard()V

    .line 144
    :cond_2
    :goto_0
    return-void

    .line 128
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBarTablet;->mSecurityButton:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getVisibility()I

    move-result v0

    if-ne v0, v2, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBarTablet;->mSecurityButton:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBarTablet;->mSecurityButton:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    if-lez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBarTablet;->mSecurityButton:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    if-lez v0, :cond_4

    .line 132
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBarTablet;->mSecurityButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 134
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBarTablet;->mUrlBar:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    invoke-static {v0}, Lorg/chromium/ui/UiUtils;->hideKeyboard(Landroid/view/View;)Z

    .line 135
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBarTablet;->mUrlBar:Lcom/google/android/apps/chrome/omnibox/UrlBar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0, v1}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    .line 139
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBarTablet;->getWindowDelegate()Lcom/google/android/apps/chrome/WindowDelegate;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/WindowDelegate;->getWindowSoftInputMode()I

    move-result v0

    const/16 v1, 0x10

    if-eq v0, v1, :cond_2

    .line 141
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBarTablet;->mKeyboardResizeModeTask:Ljava/lang/Runnable;

    const-wide/16 v2, 0x12c

    invoke-virtual {p0, v0, v2, v3}, Lcom/google/android/apps/chrome/omnibox/LocationBarTablet;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method private setUrlFocusChangePercent(F)V
    .locals 1

    .prologue
    .line 151
    iput p1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBarTablet;->mUrlFocusChangePercent:F

    .line 153
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBarTablet;->getToolbarDataProvider()Lcom/google/android/apps/chrome/toolbar/ToolbarDataProvider;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarDataProvider;->getNewTabPageForCurrentTab()Lcom/google/android/apps/chrome/ntp/NewTabPage;

    move-result-object v0

    .line 154
    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/ntp/NewTabPage;->setUrlFocusChangeAnimationPercent(F)V

    .line 155
    :cond_0
    return-void
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 66
    invoke-super {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->onFinishInflate()V

    .line 68
    sget v0, Lcom/google/android/apps/chrome/R$id;->bookmark_button:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/omnibox/LocationBarTablet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBarTablet;->mBookmarkButton:Landroid/view/View;

    .line 69
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBarTablet;->getUrlBar()Lcom/google/android/apps/chrome/omnibox/UrlBar;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/chrome/omnibox/LocationBarTablet$3;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/omnibox/LocationBarTablet$3;-><init>(Lcom/google/android/apps/chrome/omnibox/LocationBarTablet;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/omnibox/UrlBar;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 75
    return-void
.end method

.method public onUrlFocusChange(Z)V
    .locals 5

    .prologue
    .line 79
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->onUrlFocusChange(Z)V

    .line 81
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBarTablet;->mKeyboardResizeModeTask:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/omnibox/LocationBarTablet;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 83
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBarTablet;->mUrlFocusChangeAnimator:Landroid/animation/Animator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBarTablet;->mUrlFocusChangeAnimator:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 84
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBarTablet;->mUrlFocusChangeAnimator:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->cancel()V

    .line 85
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBarTablet;->mUrlFocusChangeAnimator:Landroid/animation/Animator;

    .line 88
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBarTablet;->getToolbarDataProvider()Lcom/google/android/apps/chrome/toolbar/ToolbarDataProvider;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarDataProvider;->getNewTabPageForCurrentTab()Lcom/google/android/apps/chrome/ntp/NewTabPage;

    move-result-object v0

    if-nez v0, :cond_1

    .line 89
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/omnibox/LocationBarTablet;->finishUrlFocusChange(Z)V

    .line 116
    :goto_0
    return-void

    .line 93
    :cond_1
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 94
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBarTablet;->getRootView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->getLocalVisibleRect(Landroid/graphics/Rect;)Z

    .line 95
    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v2

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v1, v0

    .line 97
    iget-object v2, p0, Lcom/google/android/apps/chrome/omnibox/LocationBarTablet;->mUrlFocusChangePercentProperty:Landroid/util/Property;

    const/4 v0, 0x1

    new-array v3, v0, [F

    const/4 v4, 0x0

    if-eqz p1, :cond_2

    const/high16 v0, 0x3f800000    # 1.0f

    :goto_1
    aput v0, v3, v4

    invoke-static {p0, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBarTablet;->mUrlFocusChangeAnimator:Landroid/animation/Animator;

    .line 99
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBarTablet;->mUrlFocusChangeAnimator:Landroid/animation/Animator;

    const/high16 v2, 0x43480000    # 200.0f

    mul-float/2addr v1, v2

    float-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 101
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBarTablet;->mUrlFocusChangeAnimator:Landroid/animation/Animator;

    new-instance v1, Lcom/google/android/apps/chrome/omnibox/LocationBarTablet$4;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/chrome/omnibox/LocationBarTablet$4;-><init>(Lcom/google/android/apps/chrome/omnibox/LocationBarTablet;Z)V

    invoke-virtual {v0, v1}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 115
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBarTablet;->mUrlFocusChangeAnimator:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    goto :goto_0

    .line 97
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method protected updateDeleteButton(Z)V
    .locals 2

    .prologue
    .line 159
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->updateDeleteButton(Z)V

    .line 160
    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBarTablet;->mBookmarkButton:Landroid/view/View;

    if-eqz p1, :cond_0

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 161
    return-void

    .line 160
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected updateLayoutParams()V
    .locals 3

    .prologue
    .line 166
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBarTablet;->mMicButton:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 168
    invoke-static {v0}, Lorg/chromium/base/ApiCompatibilityUtils;->getMarginEnd(Landroid/view/ViewGroup$MarginLayoutParams;)I

    move-result v0

    .line 169
    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBarTablet;->mMicButton:Landroid/widget/ImageButton;

    invoke-virtual {v1}, Landroid/widget/ImageButton;->getVisibility()I

    move-result v1

    const/16 v2, 0x8

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBarTablet;->mMicButton:Landroid/widget/ImageButton;

    invoke-virtual {v1}, Landroid/widget/ImageButton;->getWidth()I

    move-result v1

    add-int/2addr v0, v1

    move v2, v0

    .line 171
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBarTablet;->mDeleteButton:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 173
    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBarTablet;->mBookmarkButton:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 176
    invoke-static {v0, v2}, Lorg/chromium/base/ApiCompatibilityUtils;->setMarginEnd(Landroid/view/ViewGroup$MarginLayoutParams;I)V

    .line 177
    invoke-static {v1, v2}, Lorg/chromium/base/ApiCompatibilityUtils;->setMarginEnd(Landroid/view/ViewGroup$MarginLayoutParams;I)V

    .line 179
    iget-object v2, p0, Lcom/google/android/apps/chrome/omnibox/LocationBarTablet;->mDeleteButton:Landroid/widget/ImageButton;

    invoke-virtual {v2, v0}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 180
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBarTablet;->mBookmarkButton:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 182
    invoke-super {p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->updateLayoutParams()V

    .line 183
    return-void

    :cond_0
    move v2, v0

    goto :goto_0
.end method
