.class public interface abstract Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripStacker;
.super Ljava/lang/Object;
.source "StripStacker.java"


# virtual methods
.method public abstract canShowCloseButton()Z
.end method

.method public abstract canSlideTitleText()Z
.end method

.method public abstract createVisualOrdering(I[Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;[Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;)V
.end method

.method public abstract performOcclusionPass(I[Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;)V
.end method
