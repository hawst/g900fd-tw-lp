.class public Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;
.super Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;
.source "SyncCustomizationFragment.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;
.implements Landroid/preference/Preference$OnPreferenceClickListener;
.implements Lcom/google/android/apps/chrome/sync/ui/PassphraseDialogFragment$Listener;
.implements Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment$Listener;
.implements Lorg/chromium/chrome/browser/sync/ProfileSyncService$SyncStateChangedListener;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final ARGUMENT_ACCOUNT:Ljava/lang/String; = "account"

.field public static final FRAGMENT_CUSTOM_PASSWORD:Ljava/lang/String; = "custom_password"

.field public static final FRAGMENT_ENTER_PASSWORD:Ljava/lang/String; = "enter_password"

.field public static final FRAGMENT_PASSWORD_TYPE:Ljava/lang/String; = "password_type"

.field public static final PREFERENCE_ENCRYPTION:Ljava/lang/String; = "encryption"

.field public static final PREFERENCE_SYNC_AUTOFILL:Ljava/lang/String; = "sync_autofill"

.field public static final PREFERENCE_SYNC_BOOKMARKS:Ljava/lang/String; = "sync_bookmarks"

.field public static final PREFERENCE_SYNC_EVERYTHING:Ljava/lang/String; = "sync_everything"

.field public static final PREFERENCE_SYNC_OMNIBOX:Ljava/lang/String; = "sync_omnibox"

.field public static final PREFERENCE_SYNC_PASSWORDS:Ljava/lang/String; = "sync_passwords"

.field public static final PREFERENCE_SYNC_RECENT_TABS:Ljava/lang/String; = "sync_recent_tabs"

.field public static final PREFS_TO_SAVE:[Ljava/lang/String;


# instance fields
.field private mActionBarSwitch:Landroid/widget/Switch;

.field private mAllTypes:[Landroid/preference/CheckBoxPreference;

.field private mAndroidSyncSettingsObserver:Lorg/chromium/sync/notifier/SyncStatusHelper$SyncSettingsChangedObserver;

.field private mCheckboxesInitialized:Z

.field private mDoCustomAfterGaia:Z

.field private mGoogleServicesManager:Lcom/google/android/apps/chrome/services/GoogleServicesManager;

.field private mPasswordSyncConfigurable:Z

.field private mProfileSyncService:Lorg/chromium/chrome/browser/sync/ProfileSyncService;

.field private mSyncAutofill:Landroid/preference/CheckBoxPreference;

.field private mSyncBookmarks:Landroid/preference/CheckBoxPreference;

.field private mSyncEncryption:Landroid/preference/Preference;

.field private mSyncEverything:Landroid/preference/CheckBoxPreference;

.field private mSyncOmnibox:Landroid/preference/CheckBoxPreference;

.field private mSyncPasswords:Landroid/preference/CheckBoxPreference;

.field private mSyncRecentTabs:Landroid/preference/CheckBoxPreference;

.field private mSyncStartupHelper:Lcom/google/android/apps/chrome/sync/SyncStartupHelper;

.field private mSyncStatusHelper:Lorg/chromium/sync/notifier/SyncStatusHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 62
    const-class v0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->$assertionsDisabled:Z

    .line 102
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v3, "sync_everything"

    aput-object v3, v0, v2

    const-string/jumbo v2, "sync_autofill"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string/jumbo v2, "sync_bookmarks"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string/jumbo v2, "sync_omnibox"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string/jumbo v2, "sync_passwords"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string/jumbo v2, "sync_recent_tabs"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->PREFS_TO_SAVE:[Ljava/lang/String;

    return-void

    :cond_0
    move v0, v2

    .line 62
    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;-><init>()V

    .line 688
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;)Z
    .locals 1

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->canDisableSync()Z

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;)Lcom/google/android/apps/chrome/services/GoogleServicesManager;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mGoogleServicesManager:Lcom/google/android/apps/chrome/services/GoogleServicesManager;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;)V
    .locals 0

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->notifyInvalidPassphrase()V

    return-void
.end method

.method static synthetic access$1100(Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;)Lorg/chromium/sync/notifier/SyncStatusHelper;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mSyncStatusHelper:Lorg/chromium/sync/notifier/SyncStatusHelper;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;)Landroid/widget/Switch;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mActionBarSwitch:Landroid/widget/Switch;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;)V
    .locals 0

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->updateDataTypeState()V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;)Z
    .locals 1

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->maybeDisableSync()Z

    move-result v0

    return v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;)V
    .locals 0

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->destroySyncStartupHelper()V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 62
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->closeDialogIfOpen(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;)V
    .locals 0

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->closePage()V

    return-void
.end method

.method static synthetic access$900(Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 62
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->configureEncryption(Ljava/lang/String;Z)V

    return-void
.end method

.method private canDisableSync()Z
    .locals 1

    .prologue
    .line 231
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/services/ChildAccountManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/services/ChildAccountManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/services/ChildAccountManager;->hasChildAccount()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private closeDialogIfOpen(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 434
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Landroid/app/DialogFragment;

    .line 435
    if-eqz v0, :cond_0

    .line 436
    invoke-virtual {v0}, Landroid/app/DialogFragment;->dismiss()V

    .line 438
    :cond_0
    return-void
.end method

.method private closePage()V
    .locals 2

    .prologue
    .line 610
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceActivity;

    .line 611
    invoke-virtual {v0}, Landroid/preference/PreferenceActivity;->onIsMultiPane()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 612
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->popBackStack()V

    .line 617
    :goto_0
    return-void

    .line 614
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceActivity;->setResult(I)V

    .line 615
    invoke-virtual {v0}, Landroid/preference/PreferenceActivity;->finish()V

    goto :goto_0
.end method

.method private configureEncryption(Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 441
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mProfileSyncService:Lorg/chromium/chrome/browser/sync/ProfileSyncService;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->isSyncInitialized()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 442
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mProfileSyncService:Lorg/chromium/chrome/browser/sync/ProfileSyncService;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->enableEncryptEverything()V

    .line 443
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mProfileSyncService:Lorg/chromium/chrome/browser/sync/ProfileSyncService;

    invoke-virtual {v0, p1, p2}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->setEncryptionPassphrase(Ljava/lang/String;Z)V

    .line 446
    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->configureSyncDataTypes()V

    .line 448
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->showConfigure()V

    .line 450
    :cond_0
    return-void
.end method

.method private configureSyncDataTypes()V
    .locals 4

    .prologue
    .line 372
    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->maybeDisableSync()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 381
    :goto_0
    return-void

    .line 374
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mSyncEverything:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    .line 375
    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mProfileSyncService:Lorg/chromium/chrome/browser/sync/ProfileSyncService;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->getSelectedModelTypes()Ljava/util/Set;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->setPreferredDataTypes(ZLjava/util/Set;)V

    .line 377
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lorg/chromium/chrome/browser/invalidation/InvalidationController;->get(Landroid/content/Context;)Lorg/chromium/chrome/browser/invalidation/InvalidationController;

    move-result-object v1

    .line 379
    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->getAccountArgument()Landroid/accounts/Account;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mProfileSyncService:Lorg/chromium/chrome/browser/sync/ProfileSyncService;

    invoke-virtual {v3}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->getPreferredDataTypes()Ljava/util/Set;

    move-result-object v3

    invoke-virtual {v1, v2, v0, v3}, Lorg/chromium/chrome/browser/invalidation/InvalidationController;->setRegisteredTypes(Landroid/accounts/Account;ZLjava/util/Set;)V

    goto :goto_0
.end method

.method private destroySyncStartupHelper()V
    .locals 1

    .prologue
    .line 255
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mSyncStartupHelper:Lcom/google/android/apps/chrome/sync/SyncStartupHelper;

    if-eqz v0, :cond_0

    .line 256
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mSyncStartupHelper:Lcom/google/android/apps/chrome/sync/SyncStartupHelper;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/sync/SyncStartupHelper;->destroy()V

    .line 257
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mSyncStartupHelper:Lcom/google/android/apps/chrome/sync/SyncStartupHelper;

    .line 259
    :cond_0
    return-void
.end method

.method private displayCustomPasswordDialog()V
    .locals 3

    .prologue
    .line 420
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 421
    new-instance v1, Lcom/google/android/apps/chrome/sync/ui/CustomPassphraseDialogFragment;

    invoke-direct {v1}, Lcom/google/android/apps/chrome/sync/ui/CustomPassphraseDialogFragment;-><init>()V

    .line 422
    const/4 v2, -0x1

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/chrome/sync/ui/CustomPassphraseDialogFragment;->setTargetFragment(Landroid/app/Fragment;I)V

    .line 423
    const-string/jumbo v2, "custom_password"

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/chrome/sync/ui/CustomPassphraseDialogFragment;->show(Landroid/app/FragmentTransaction;Ljava/lang/String;)I

    .line 424
    return-void
.end method

.method private displayPasswordDialog(ZZ)V
    .locals 3

    .prologue
    .line 414
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 415
    invoke-static {p0, p1, p2}, Lcom/google/android/apps/chrome/sync/ui/PassphraseDialogFragment;->newInstance(Landroid/app/Fragment;ZZ)Lcom/google/android/apps/chrome/sync/ui/PassphraseDialogFragment;

    move-result-object v1

    const-string/jumbo v2, "enter_password"

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/chrome/sync/ui/PassphraseDialogFragment;->show(Landroid/app/FragmentTransaction;Ljava/lang/String;)I

    .line 417
    return-void
.end method

.method private displayPasswordTypeDialog()V
    .locals 5

    .prologue
    .line 404
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 405
    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->getCurrentlySelectedEncryptionType()Lorg/chromium/sync/internal_api/pub/SyncDecryptionPassphraseType;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mProfileSyncService:Lorg/chromium/chrome/browser/sync/ProfileSyncService;

    invoke-virtual {v2}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->getExplicitPassphraseTime()J

    move-result-wide v2

    iget-object v4, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mProfileSyncService:Lorg/chromium/chrome/browser/sync/ProfileSyncService;

    invoke-virtual {v4}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->isEncryptEverythingAllowed()Z

    move-result v4

    invoke-static {v1, v2, v3, v4}, Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment;->create(Lorg/chromium/sync/internal_api/pub/SyncDecryptionPassphraseType;JZ)Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment;

    move-result-object v1

    .line 409
    const-string/jumbo v2, "password_type"

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment;->show(Landroid/app/FragmentTransaction;Ljava/lang/String;)I

    .line 410
    const/4 v0, -0x1

    invoke-virtual {v1, p0, v0}, Lcom/google/android/apps/chrome/sync/ui/PassphraseTypeDialogFragment;->setTargetFragment(Landroid/app/Fragment;I)V

    .line 411
    return-void
.end method

.method private displaySpinnerDialog()V
    .locals 3

    .prologue
    .line 427
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 428
    new-instance v1, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment$SpinnerDialogFragment;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment$SpinnerDialogFragment;-><init>(Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment$1;)V

    .line 429
    const/4 v2, 0x0

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment$SpinnerDialogFragment;->setTargetFragment(Landroid/app/Fragment;I)V

    .line 430
    const-string/jumbo v2, "spinner"

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment$SpinnerDialogFragment;->show(Landroid/app/FragmentTransaction;Ljava/lang/String;)I

    .line 431
    return-void
.end method

.method private getAccountArgument()Landroid/accounts/Account;
    .locals 2

    .prologue
    .line 620
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "account"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/sync/signin/AccountManagerHelper;->createAccountFromName(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v0

    return-object v0
.end method

.method private getCurrentlySelectedEncryptionType()Lorg/chromium/sync/internal_api/pub/SyncDecryptionPassphraseType;
    .locals 1

    .prologue
    .line 400
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mProfileSyncService:Lorg/chromium/chrome/browser/sync/ProfileSyncService;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->getSyncDecryptionPassphraseType()Lorg/chromium/sync/internal_api/pub/SyncDecryptionPassphraseType;

    move-result-object v0

    return-object v0
.end method

.method private getSelectedModelTypes()Ljava/util/Set;
    .locals 2

    .prologue
    .line 384
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 385
    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mSyncAutofill:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lorg/chromium/sync/internal_api/pub/base/ModelType;->AUTOFILL:Lorg/chromium/sync/internal_api/pub/base/ModelType;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 386
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mSyncBookmarks:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_1

    sget-object v1, Lorg/chromium/sync/internal_api/pub/base/ModelType;->BOOKMARK:Lorg/chromium/sync/internal_api/pub/base/ModelType;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 387
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mSyncOmnibox:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_2

    sget-object v1, Lorg/chromium/sync/internal_api/pub/base/ModelType;->TYPED_URL:Lorg/chromium/sync/internal_api/pub/base/ModelType;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 388
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mSyncPasswords:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 389
    sget-boolean v1, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->$assertionsDisabled:Z

    if-nez v1, :cond_3

    iget-boolean v1, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mPasswordSyncConfigurable:Z

    if-nez v1, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 390
    :cond_3
    sget-object v1, Lorg/chromium/sync/internal_api/pub/base/ModelType;->PASSWORD:Lorg/chromium/sync/internal_api/pub/base/ModelType;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 392
    :cond_4
    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mSyncRecentTabs:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_5

    sget-object v1, Lorg/chromium/sync/internal_api/pub/base/ModelType;->PROXY_TABS:Lorg/chromium/sync/internal_api/pub/base/ModelType;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 393
    :cond_5
    return-object v0
.end method

.method private handleDecryption(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 492
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mProfileSyncService:Lorg/chromium/chrome/browser/sync/ProfileSyncService;

    invoke-virtual {v0, p1}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->setDecryptionPassphrase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 494
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->showConfigure()V

    .line 499
    :goto_0
    return-void

    .line 497
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->notifyInvalidPassphrase()V

    goto :goto_0
.end method

.method private handleEncryptWithGaia(Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 453
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string/jumbo v1, "account"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/accounts/AccountManager;

    .line 455
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string/jumbo v2, "account"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 456
    new-instance v4, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment$5;

    invoke-direct {v4, p0, p1}, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment$5;-><init>(Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;Ljava/lang/String;)V

    .line 481
    invoke-static {v1}, Lorg/chromium/sync/signin/AccountManagerHelper;->createAccountFromName(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v1

    .line 482
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 483
    const-string/jumbo v5, "password"

    invoke-virtual {v2, v5, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move-object v5, v3

    .line 484
    invoke-virtual/range {v0 .. v5}, Landroid/accounts/AccountManager;->confirmCredentials(Landroid/accounts/Account;Landroid/os/Bundle;Landroid/app/Activity;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    .line 485
    return-void
.end method

.method private isSyncTypePreference(Landroid/preference/Preference;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 235
    iget-object v2, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mAllTypes:[Landroid/preference/CheckBoxPreference;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    .line 236
    if-ne v4, p1, :cond_1

    const/4 v0, 0x1

    .line 238
    :cond_0
    return v0

    .line 235
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private maybeDisableSync()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 679
    iget-object v2, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mSyncEverything:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v2}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->getSelectedModelTypes()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->canDisableSync()Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    move v0, v1

    .line 685
    :goto_0
    return v0

    .line 682
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mActionBarSwitch:Landroid/widget/Switch;

    invoke-virtual {v2, v1}, Landroid/widget/Switch;->setChecked(Z)V

    .line 683
    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mSyncEverything:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1, v0}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 684
    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->updateDataTypeState()V

    goto :goto_0
.end method

.method private notifyInvalidPassphrase()V
    .locals 2

    .prologue
    .line 518
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const-string/jumbo v1, "enter_password"

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/sync/ui/PassphraseDialogFragment;

    .line 520
    if-eqz v0, :cond_0

    .line 521
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/sync/ui/PassphraseDialogFragment;->invalidPassphrase()V

    .line 525
    :goto_0
    return-void

    .line 523
    :cond_0
    const-string/jumbo v0, "SyncCustomizationFragment"

    const-string/jumbo v1, "invalid passphrase but no dialog to notify"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private openDashboardTabInNewActivityStack()V
    .locals 3

    .prologue
    .line 600
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.action.VIEW"

    const-string/jumbo v2, "https://www.google.com/settings/chrome/sync"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 601
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 602
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 603
    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->startActivity(Landroid/content/Intent;)V

    .line 604
    return-void
.end method

.method private updateDataTypeState()V
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 653
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mSyncStatusHelper:Lorg/chromium/sync/notifier/SyncStatusHelper;

    invoke-virtual {v0}, Lorg/chromium/sync/notifier/SyncStatusHelper;->isSyncEnabled()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mProfileSyncService:Lorg/chromium/chrome/browser/sync/ProfileSyncService;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->isSyncInitialized()Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 655
    :goto_0
    iget-object v3, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mSyncEverything:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v3, v0}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    .line 656
    iget-object v3, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mSyncEncryption:Landroid/preference/Preference;

    invoke-virtual {v3, v0}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 658
    iget-object v3, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mSyncEverything:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v3}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v5

    .line 659
    iget-object v6, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mAllTypes:[Landroid/preference/CheckBoxPreference;

    array-length v7, v6

    move v4, v2

    :goto_1
    if-ge v4, v7, :cond_5

    aget-object v8, v6, v4

    .line 660
    iget-object v3, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mSyncPasswords:Landroid/preference/CheckBoxPreference;

    if-ne v8, v3, :cond_0

    iget-boolean v3, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mPasswordSyncConfigurable:Z

    if-eqz v3, :cond_3

    :cond_0
    move v3, v1

    .line 662
    :goto_2
    if-eqz v5, :cond_1

    invoke-virtual {v8, v3}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 664
    :cond_1
    if-eqz v0, :cond_4

    if-nez v5, :cond_4

    if-eqz v3, :cond_4

    move v3, v1

    :goto_3
    invoke-virtual {v8, v3}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    .line 659
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_1

    :cond_2
    move v0, v2

    .line 653
    goto :goto_0

    :cond_3
    move v3, v2

    .line 660
    goto :goto_2

    :cond_4
    move v3, v2

    .line 664
    goto :goto_3

    .line 666
    :cond_5
    return-void
.end method

.method private updateEncryptionState()V
    .locals 2

    .prologue
    .line 352
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mSyncEncryption:Landroid/preference/Preference;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 353
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mProfileSyncService:Lorg/chromium/chrome/browser/sync/ProfileSyncService;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->isSyncInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 369
    :goto_0
    return-void

    .line 359
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mProfileSyncService:Lorg/chromium/chrome/browser/sync/ProfileSyncService;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->isPassphraseRequiredForDecryption()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 360
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mProfileSyncService:Lorg/chromium/chrome/browser/sync/ProfileSyncService;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->isUsingSecondaryPassphrase()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 361
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mSyncEncryption:Landroid/preference/Preference;

    sget v1, Lcom/google/android/apps/chrome/R$string;->sync_need_passphrase:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->errorSummary(Ljava/lang/String;)Landroid/text/Spannable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 368
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mProfileSyncService:Lorg/chromium/chrome/browser/sync/ProfileSyncService;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->isCryptographerReady()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mPasswordSyncConfigurable:Z

    goto :goto_0

    .line 364
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mSyncEncryption:Landroid/preference/Preference;

    sget v1, Lcom/google/android/apps/chrome/R$string;->sync_need_password:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/chrome/services/ui/GoogleServicesFragment;->errorSummary(Ljava/lang/String;)Landroid/text/Spannable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method


# virtual methods
.method public getSyncActionBarSwitch()Landroid/widget/Switch;
    .locals 1

    .prologue
    .line 248
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mActionBarSwitch:Landroid/widget/Switch;

    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 149
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/sync/notifier/SyncStatusHelper;->get(Landroid/content/Context;)Lorg/chromium/sync/notifier/SyncStatusHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mSyncStatusHelper:Lorg/chromium/sync/notifier/SyncStatusHelper;

    .line 150
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->get(Landroid/content/Context;)Lcom/google/android/apps/chrome/services/GoogleServicesManager;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mGoogleServicesManager:Lcom/google/android/apps/chrome/services/GoogleServicesManager;

    .line 151
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->get(Landroid/content/Context;)Lorg/chromium/chrome/browser/sync/ProfileSyncService;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mProfileSyncService:Lorg/chromium/chrome/browser/sync/ProfileSyncService;

    .line 153
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v3

    .line 154
    sget v0, Lcom/google/android/apps/chrome/R$xml;->sync_customization_preferences:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->addPreferencesFromResource(I)V

    .line 155
    const-string/jumbo v0, "sync_everything"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mSyncEverything:Landroid/preference/CheckBoxPreference;

    .line 156
    const-string/jumbo v0, "sync_autofill"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mSyncAutofill:Landroid/preference/CheckBoxPreference;

    .line 157
    const-string/jumbo v0, "sync_bookmarks"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mSyncBookmarks:Landroid/preference/CheckBoxPreference;

    .line 158
    const-string/jumbo v0, "sync_omnibox"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mSyncOmnibox:Landroid/preference/CheckBoxPreference;

    .line 159
    const-string/jumbo v0, "sync_passwords"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mSyncPasswords:Landroid/preference/CheckBoxPreference;

    .line 160
    const-string/jumbo v0, "sync_recent_tabs"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mSyncRecentTabs:Landroid/preference/CheckBoxPreference;

    .line 161
    const-string/jumbo v0, "encryption"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mSyncEncryption:Landroid/preference/Preference;

    .line 162
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mSyncEncryption:Landroid/preference/Preference;

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 164
    const/4 v0, 0x5

    new-array v0, v0, [Landroid/preference/CheckBoxPreference;

    iget-object v4, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mSyncAutofill:Landroid/preference/CheckBoxPreference;

    aput-object v4, v0, v2

    iget-object v4, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mSyncBookmarks:Landroid/preference/CheckBoxPreference;

    aput-object v4, v0, v1

    const/4 v4, 0x2

    iget-object v5, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mSyncOmnibox:Landroid/preference/CheckBoxPreference;

    aput-object v5, v0, v4

    const/4 v4, 0x3

    iget-object v5, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mSyncPasswords:Landroid/preference/CheckBoxPreference;

    aput-object v5, v0, v4

    const/4 v4, 0x4

    iget-object v5, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mSyncRecentTabs:Landroid/preference/CheckBoxPreference;

    aput-object v5, v0, v4

    iput-object v0, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mAllTypes:[Landroid/preference/CheckBoxPreference;

    .line 168
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mSyncEverything:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 169
    iget-object v4, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mAllTypes:[Landroid/preference/CheckBoxPreference;

    array-length v5, v4

    move v0, v2

    :goto_0
    if-ge v0, v5, :cond_0

    aget-object v6, v4, v0

    .line 170
    invoke-virtual {v6, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 169
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 174
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mProfileSyncService:Lorg/chromium/chrome/browser/sync/ProfileSyncService;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->isSyncInitialized()Z

    move-result v0

    if-nez v0, :cond_1

    .line 176
    if-nez p3, :cond_1

    .line 177
    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->displaySpinnerDialog()V

    .line 180
    :cond_1
    new-instance v0, Landroid/widget/Switch;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-direct {v0, v4}, Landroid/widget/Switch;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mActionBarSwitch:Landroid/widget/Switch;

    .line 181
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mActionBarSwitch:Landroid/widget/Switch;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    sget v5, Lcom/google/android/apps/chrome/R$style;->PreferenceHeaderSwitchText:I

    invoke-virtual {v0, v4, v5}, Landroid/widget/Switch;->setSwitchTextAppearance(Landroid/content/Context;I)V

    .line 182
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mActionBarSwitch:Landroid/widget/Switch;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->addSwitchToActionBar(Landroid/widget/Switch;)V

    .line 183
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mActionBarSwitch:Landroid/widget/Switch;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->canDisableSync()Z

    move-result v4

    invoke-virtual {v0, v4}, Landroid/widget/Switch;->setEnabled(Z)V

    .line 184
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mActionBarSwitch:Landroid/widget/Switch;

    iget-object v4, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mSyncStatusHelper:Lorg/chromium/sync/notifier/SyncStatusHelper;

    invoke-virtual {v4}, Lorg/chromium/sync/notifier/SyncStatusHelper;->isSyncEnabled()Z

    move-result v4

    invoke-virtual {v0, v4}, Landroid/widget/Switch;->setChecked(Z)V

    .line 185
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mActionBarSwitch:Landroid/widget/Switch;

    new-instance v4, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment$1;

    invoke-direct {v4, p0}, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment$1;-><init>(Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;)V

    invoke-virtual {v0, v4}, Landroid/widget/Switch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 196
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mProfileSyncService:Lorg/chromium/chrome/browser/sync/ProfileSyncService;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->isSyncInitialized()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mProfileSyncService:Lorg/chromium/chrome/browser/sync/ProfileSyncService;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->isCryptographerReady()Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mPasswordSyncConfigurable:Z

    .line 198
    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->updateDataTypeState()V

    .line 200
    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->setHasOptionsMenu(Z)V

    .line 201
    return-object v3

    :cond_2
    move v0, v2

    .line 196
    goto :goto_1
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 648
    invoke-super {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;->onDestroy()V

    .line 649
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mActionBarSwitch:Landroid/widget/Switch;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/Switch;->setVisibility(I)V

    .line 650
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 641
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sget v1, Lcom/google/android/apps/chrome/R$id;->menu_id_sync_manage_data:I

    if-eq v0, v1, :cond_0

    const/4 v0, 0x0

    .line 643
    :goto_0
    return v0

    .line 642
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->openDashboardTabInNewActivityStack()V

    .line 643
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onPassphraseCanceled(ZZ)V
    .locals 1

    .prologue
    .line 532
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mDoCustomAfterGaia:Z

    .line 533
    return-void
.end method

.method public onPassphraseEntered(Ljava/lang/String;ZZ)V
    .locals 1

    .prologue
    .line 506
    if-eqz p3, :cond_1

    .line 507
    if-eqz p2, :cond_0

    .line 508
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->handleEncryptWithGaia(Ljava/lang/String;)V

    .line 515
    :goto_0
    return-void

    .line 510
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->configureEncryption(Ljava/lang/String;Z)V

    goto :goto_0

    .line 513
    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->handleDecryption(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onPassphraseTypeSelected(Lorg/chromium/sync/internal_api/pub/SyncDecryptionPassphraseType;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 540
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mProfileSyncService:Lorg/chromium/chrome/browser/sync/ProfileSyncService;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->isEncryptEverythingEnabled()Z

    move-result v3

    .line 541
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mProfileSyncService:Lorg/chromium/chrome/browser/sync/ProfileSyncService;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->isUsingSecondaryPassphrase()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 543
    :goto_0
    sget-object v4, Lorg/chromium/sync/internal_api/pub/SyncDecryptionPassphraseType;->IMPLICIT_PASSPHRASE:Lorg/chromium/sync/internal_api/pub/SyncDecryptionPassphraseType;

    if-ne p1, v4, :cond_4

    .line 545
    sget-boolean v4, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->$assertionsDisabled:Z

    if-nez v4, :cond_1

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    move v0, v2

    .line 541
    goto :goto_0

    .line 546
    :cond_1
    if-nez v3, :cond_2

    move v2, v1

    .line 548
    :cond_2
    invoke-direct {p0, v1, v2}, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->displayPasswordDialog(ZZ)V

    .line 573
    :cond_3
    :goto_1
    return-void

    .line 549
    :cond_4
    sget-object v3, Lorg/chromium/sync/internal_api/pub/SyncDecryptionPassphraseType;->CUSTOM_PASSPHRASE:Lorg/chromium/sync/internal_api/pub/SyncDecryptionPassphraseType;

    if-ne p1, v3, :cond_3

    .line 550
    if-eqz v0, :cond_6

    .line 552
    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->getCurrentlySelectedEncryptionType()Lorg/chromium/sync/internal_api/pub/SyncDecryptionPassphraseType;

    move-result-object v0

    sget-object v2, Lorg/chromium/sync/internal_api/pub/SyncDecryptionPassphraseType;->NONE:Lorg/chromium/sync/internal_api/pub/SyncDecryptionPassphraseType;

    if-ne v0, v2, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mProfileSyncService:Lorg/chromium/chrome/browser/sync/ProfileSyncService;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->isPassphraseRequiredForExternalType()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 557
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mDoCustomAfterGaia:Z

    .line 558
    invoke-direct {p0, v1, v1}, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->displayPasswordDialog(ZZ)V

    goto :goto_1

    .line 563
    :cond_5
    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->displayCustomPasswordDialog()V

    goto :goto_1

    .line 568
    :cond_6
    invoke-direct {p0, v2, v2}, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->displayPasswordDialog(ZZ)V

    goto :goto_1
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 293
    invoke-super {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;->onPause()V

    .line 294
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mSyncStartupHelper:Lcom/google/android/apps/chrome/sync/SyncStartupHelper;

    if-eqz v0, :cond_2

    .line 296
    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->destroySyncStartupHelper()V

    .line 310
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mAndroidSyncSettingsObserver:Lorg/chromium/sync/notifier/SyncStatusHelper$SyncSettingsChangedObserver;

    if-eqz v0, :cond_1

    .line 311
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mSyncStatusHelper:Lorg/chromium/sync/notifier/SyncStatusHelper;

    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mAndroidSyncSettingsObserver:Lorg/chromium/sync/notifier/SyncStatusHelper$SyncSettingsChangedObserver;

    invoke-virtual {v0, v1}, Lorg/chromium/sync/notifier/SyncStatusHelper;->unregisterSyncSettingsChangedObserver(Lorg/chromium/sync/notifier/SyncStatusHelper$SyncSettingsChangedObserver;)V

    .line 313
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mProfileSyncService:Lorg/chromium/chrome/browser/sync/ProfileSyncService;

    invoke-virtual {v0, p0}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->removeSyncStateChangedListener(Lorg/chromium/chrome/browser/sync/ProfileSyncService$SyncStateChangedListener;)V

    .line 314
    return-void

    .line 300
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mProfileSyncService:Lorg/chromium/chrome/browser/sync/ProfileSyncService;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->isSyncInitialized()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 303
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->isChangingConfigurations()Z

    move-result v0

    if-nez v0, :cond_0

    .line 304
    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->configureSyncDataTypes()V

    .line 305
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mProfileSyncService:Lorg/chromium/chrome/browser/sync/ProfileSyncService;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->setSyncSetupCompleted()V

    .line 306
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mProfileSyncService:Lorg/chromium/chrome/browser/sync/ProfileSyncService;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->setSetupInProgress(Z)V

    goto :goto_0
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 206
    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mSyncEverything:Landroid/preference/CheckBoxPreference;

    if-ne p1, v1, :cond_0

    .line 207
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    new-instance v2, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment$2;

    invoke-direct {v2, p0}, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment$2;-><init>(Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 224
    :goto_0
    return v0

    .line 215
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->isSyncTypePreference(Landroid/preference/Preference;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 216
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    new-instance v2, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment$3;

    invoke-direct {v2, p0}, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment$3;-><init>(Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 224
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 580
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->isResumed()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mProfileSyncService:Lorg/chromium/chrome/browser/sync/ProfileSyncService;

    invoke-virtual {v2}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->isSyncInitialized()Z

    move-result v2

    if-nez v2, :cond_1

    .line 593
    :cond_0
    :goto_0
    return v1

    .line 585
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mSyncEncryption:Landroid/preference/Preference;

    if-ne p1, v2, :cond_0

    .line 586
    iget-object v2, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mProfileSyncService:Lorg/chromium/chrome/browser/sync/ProfileSyncService;

    invoke-virtual {v2}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->isPassphraseRequiredForDecryption()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 587
    iget-object v2, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mProfileSyncService:Lorg/chromium/chrome/browser/sync/ProfileSyncService;

    invoke-virtual {v2}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->isUsingSecondaryPassphrase()Z

    move-result v2

    if-nez v2, :cond_2

    :goto_1
    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->displayPasswordDialog(ZZ)V

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1

    .line 589
    :cond_3
    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->displayPasswordTypeDialog()V

    move v1, v0

    .line 590
    goto :goto_0
.end method

.method public onResume()V
    .locals 4

    .prologue
    .line 263
    invoke-super {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;->onResume()V

    .line 266
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 267
    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mProfileSyncService:Lorg/chromium/chrome/browser/sync/ProfileSyncService;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->setSetupInProgress(Z)V

    .line 268
    new-instance v1, Lcom/google/android/apps/chrome/sync/SyncStartupHelper;

    iget-object v2, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mProfileSyncService:Lorg/chromium/chrome/browser/sync/ProfileSyncService;

    new-instance v3, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment$4;

    invoke-direct {v3, p0}, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment$4;-><init>(Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;)V

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/chrome/sync/SyncStartupHelper;-><init>(Lorg/chromium/chrome/browser/sync/ProfileSyncService;Lcom/google/android/apps/chrome/sync/SyncStartupHelper$Delegate;)V

    iput-object v1, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mSyncStartupHelper:Lcom/google/android/apps/chrome/sync/SyncStartupHelper;

    .line 284
    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mSyncStartupHelper:Lcom/google/android/apps/chrome/sync/SyncStartupHelper;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->getAccountArgument()Landroid/accounts/Account;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/chrome/sync/SyncStartupHelper;->startSync(Landroid/app/Activity;Landroid/accounts/Account;)V

    .line 285
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mProfileSyncService:Lorg/chromium/chrome/browser/sync/ProfileSyncService;

    invoke-virtual {v0, p0}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->addSyncStateChangedListener(Lorg/chromium/chrome/browser/sync/ProfileSyncService$SyncStateChangedListener;)V

    .line 287
    new-instance v0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment$AndroidSyncSettingsObserver;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment$AndroidSyncSettingsObserver;-><init>(Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment$1;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mAndroidSyncSettingsObserver:Lorg/chromium/sync/notifier/SyncStatusHelper$SyncSettingsChangedObserver;

    .line 288
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mSyncStatusHelper:Lorg/chromium/sync/notifier/SyncStatusHelper;

    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mAndroidSyncSettingsObserver:Lorg/chromium/sync/notifier/SyncStatusHelper$SyncSettingsChangedObserver;

    invoke-virtual {v0, v1}, Lorg/chromium/sync/notifier/SyncStatusHelper;->registerSyncSettingsChangedObserver(Lorg/chromium/sync/notifier/SyncStatusHelper$SyncSettingsChangedObserver;)V

    .line 289
    return-void
.end method

.method public showConfigure()V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 317
    const-string/jumbo v2, "spinner"

    invoke-direct {p0, v2}, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->closeDialogIfOpen(Ljava/lang/String;)V

    .line 318
    const-string/jumbo v2, "custom_password"

    invoke-direct {p0, v2}, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->closeDialogIfOpen(Ljava/lang/String;)V

    .line 319
    const-string/jumbo v2, "enter_password"

    invoke-direct {p0, v2}, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->closeDialogIfOpen(Ljava/lang/String;)V

    .line 321
    sget-boolean v2, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mProfileSyncService:Lorg/chromium/chrome/browser/sync/ProfileSyncService;

    invoke-virtual {v2}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->isSyncInitialized()Z

    move-result v2

    if-nez v2, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 322
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mProfileSyncService:Lorg/chromium/chrome/browser/sync/ProfileSyncService;

    invoke-virtual {v2}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->isSyncInitialized()Z

    move-result v2

    if-nez v2, :cond_1

    .line 349
    :goto_0
    return-void

    .line 328
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->updateEncryptionState()V

    .line 329
    iget-object v2, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mProfileSyncService:Lorg/chromium/chrome/browser/sync/ProfileSyncService;

    invoke-virtual {v2}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->isPassphraseRequiredForDecryption()Z

    move-result v2

    if-nez v2, :cond_2

    iget-boolean v2, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mDoCustomAfterGaia:Z

    if-eqz v2, :cond_2

    .line 330
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mDoCustomAfterGaia:Z

    .line 331
    sget-object v2, Lorg/chromium/sync/internal_api/pub/SyncDecryptionPassphraseType;->CUSTOM_PASSPHRASE:Lorg/chromium/sync/internal_api/pub/SyncDecryptionPassphraseType;

    invoke-virtual {p0, v2}, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->onPassphraseTypeSelected(Lorg/chromium/sync/internal_api/pub/SyncDecryptionPassphraseType;)V

    .line 337
    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mProfileSyncService:Lorg/chromium/chrome/browser/sync/ProfileSyncService;

    invoke-virtual {v2}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->getPreferredDataTypes()Ljava/util/Set;

    move-result-object v2

    .line 338
    iget-boolean v3, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mCheckboxesInitialized:Z

    if-nez v3, :cond_3

    .line 339
    iput-boolean v0, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mCheckboxesInitialized:Z

    .line 340
    iget-object v3, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mSyncEverything:Landroid/preference/CheckBoxPreference;

    iget-object v4, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mProfileSyncService:Lorg/chromium/chrome/browser/sync/ProfileSyncService;

    invoke-virtual {v4}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->hasKeepEverythingSynced()Z

    move-result v4

    invoke-virtual {v3, v4}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 341
    iget-object v3, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mSyncAutofill:Landroid/preference/CheckBoxPreference;

    sget-object v4, Lorg/chromium/sync/internal_api/pub/base/ModelType;->AUTOFILL:Lorg/chromium/sync/internal_api/pub/base/ModelType;

    invoke-interface {v2, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    invoke-virtual {v3, v4}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 342
    iget-object v3, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mSyncBookmarks:Landroid/preference/CheckBoxPreference;

    sget-object v4, Lorg/chromium/sync/internal_api/pub/base/ModelType;->BOOKMARK:Lorg/chromium/sync/internal_api/pub/base/ModelType;

    invoke-interface {v2, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    invoke-virtual {v3, v4}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 343
    iget-object v3, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mSyncOmnibox:Landroid/preference/CheckBoxPreference;

    sget-object v4, Lorg/chromium/sync/internal_api/pub/base/ModelType;->TYPED_URL:Lorg/chromium/sync/internal_api/pub/base/ModelType;

    invoke-interface {v2, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    invoke-virtual {v3, v4}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 344
    iget-object v3, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mSyncPasswords:Landroid/preference/CheckBoxPreference;

    iget-boolean v4, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mPasswordSyncConfigurable:Z

    if-eqz v4, :cond_4

    sget-object v4, Lorg/chromium/sync/internal_api/pub/base/ModelType;->PASSWORD:Lorg/chromium/sync/internal_api/pub/base/ModelType;

    invoke-interface {v2, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    :goto_1
    invoke-virtual {v3, v0}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 346
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->mSyncRecentTabs:Landroid/preference/CheckBoxPreference;

    sget-object v1, Lorg/chromium/sync/internal_api/pub/base/ModelType;->PROXY_TABS:Lorg/chromium/sync/internal_api/pub/base/ModelType;

    invoke-interface {v2, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 348
    :cond_3
    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->updateDataTypeState()V

    goto :goto_0

    :cond_4
    move v0, v1

    .line 344
    goto :goto_1
.end method

.method public syncStateChanged()V
    .locals 0

    .prologue
    .line 670
    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->updateEncryptionState()V

    .line 671
    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;->updateDataTypeState()V

    .line 672
    return-void
.end method
