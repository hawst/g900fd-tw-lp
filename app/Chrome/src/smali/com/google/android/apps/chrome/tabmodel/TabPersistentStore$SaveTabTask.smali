.class Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$SaveTabTask;
.super Landroid/os/AsyncTask;
.source "TabPersistentStore.java"


# instance fields
.field mEncrypted:Z

.field mId:I

.field mState:Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;

.field mStateSaved:Z

.field mTab:Lcom/google/android/apps/chrome/tab/ChromeTab;

.field final synthetic this$0:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;Lcom/google/android/apps/chrome/tab/ChromeTab;)V
    .locals 1

    .prologue
    .line 625
    iput-object p1, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$SaveTabTask;->this$0:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 623
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$SaveTabTask;->mStateSaved:Z

    .line 626
    iput-object p2, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$SaveTabTask;->mTab:Lcom/google/android/apps/chrome/tab/ChromeTab;

    .line 627
    invoke-virtual {p2}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getId()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$SaveTabTask;->mId:I

    .line 628
    invoke-virtual {p2}, Lcom/google/android/apps/chrome/tab/ChromeTab;->isIncognito()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$SaveTabTask;->mEncrypted:Z

    .line 629
    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 618
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$SaveTabTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 639
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$SaveTabTask;->mState:Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;

    if-nez v0, :cond_0

    .line 649
    :goto_0
    return-object v3

    .line 641
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$SaveTabTask;->this$0:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;

    iget v1, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$SaveTabTask;->mId:I

    iget-boolean v2, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$SaveTabTask;->mEncrypted:Z

    # invokes: Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->openTabStateOutputStream(IZ)Ljava/io/FileOutputStream;
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->access$300(Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;IZ)Ljava/io/FileOutputStream;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$SaveTabTask;->mState:Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;

    iget-boolean v2, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$SaveTabTask;->mEncrypted:Z

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/tab/ChromeTab;->saveState(Ljava/io/FileOutputStream;Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;Z)V

    .line 642
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$SaveTabTask;->mStateSaved:Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 644
    :catch_0
    move-exception v0

    const-string/jumbo v0, "TabPersistentStore"

    const-string/jumbo v1, "IO Exception while attempting to save tab state."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 646
    :catch_1
    move-exception v0

    const-string/jumbo v0, "TabPersistentStore"

    const-string/jumbo v1, "Out of memory error while attempting to save tab state.  Erasing."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 647
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$SaveTabTask;->this$0:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;

    iget v1, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$SaveTabTask;->mId:I

    iget-boolean v2, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$SaveTabTask;->mEncrypted:Z

    # invokes: Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->deleteTabStateFile(IZ)V
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->access$400(Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;IZ)V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 618
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$SaveTabTask;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 2

    .prologue
    .line 654
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$SaveTabTask;->this$0:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;

    # getter for: Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mDestroyed:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->access$200(Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$SaveTabTask;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 658
    :cond_0
    :goto_0
    return-void

    .line 655
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$SaveTabTask;->mStateSaved:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$SaveTabTask;->mTab:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->tabStateWasPersisted()V

    .line 656
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$SaveTabTask;->this$0:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mSaveTabTask:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$SaveTabTask;
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->access$502(Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$SaveTabTask;)Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$SaveTabTask;

    .line 657
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$SaveTabTask;->this$0:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;

    # invokes: Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->saveNextTab()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->access$600(Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;)V

    goto :goto_0
.end method

.method protected onPreExecute()V
    .locals 1

    .prologue
    .line 633
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$SaveTabTask;->this$0:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;

    # getter for: Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mDestroyed:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->access$200(Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$SaveTabTask;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 635
    :cond_0
    :goto_0
    return-void

    .line 634
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$SaveTabTask;->mTab:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getState()Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$SaveTabTask;->mState:Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;

    goto :goto_0
.end method
