.class Lcom/google/android/apps/chrome/icing/HistoryUsageMigrator;
.super Ljava/lang/Object;
.source "HistoryUsageMigrator.java"


# static fields
.field private static sInProgress:Z

.field private static final sLock:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/apps/chrome/icing/HistoryUsageMigrator;->sLock:Ljava/lang/Object;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/google/android/apps/chrome/icing/HistoryUsageMigrator;->sLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$102(Z)Z
    .locals 0

    .prologue
    .line 15
    sput-boolean p0, Lcom/google/android/apps/chrome/icing/HistoryUsageMigrator;->sInProgress:Z

    return p0
.end method

.method static startMigrationAndEnableReporting(Landroid/content/SharedPreferences;Lcom/google/android/apps/chrome/icing/SearchJniBridge;Lcom/google/android/apps/chrome/icing/IcingController;Z)V
    .locals 4

    .prologue
    .line 30
    sget-object v1, Lcom/google/android/apps/chrome/icing/HistoryUsageMigrator;->sLock:Ljava/lang/Object;

    monitor-enter v1

    .line 31
    if-eqz p3, :cond_2

    .line 32
    :try_start_0
    invoke-interface {p0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v2, "com.google.android.apps.chrome.icing.HistoryUsageMigrator.HISTORY_MIGRATION_REQUESTED"

    const/4 v3, 0x1

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 39
    :cond_0
    sget-boolean v0, Lcom/google/android/apps/chrome/icing/HistoryUsageMigrator;->sInProgress:Z

    if-nez v0, :cond_1

    .line 40
    const/4 v0, 0x1

    sput-boolean v0, Lcom/google/android/apps/chrome/icing/HistoryUsageMigrator;->sInProgress:Z

    .line 42
    new-instance v0, Lcom/google/android/apps/chrome/icing/HistoryUsageMigrator$1;

    invoke-direct {v0, p1, p2, p0}, Lcom/google/android/apps/chrome/icing/HistoryUsageMigrator$1;-><init>(Lcom/google/android/apps/chrome/icing/SearchJniBridge;Lcom/google/android/apps/chrome/icing/IcingController;Landroid/content/SharedPreferences;)V

    sget-object v2, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Void;

    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/chrome/icing/HistoryUsageMigrator$1;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 65
    :cond_1
    monitor-exit v1

    :goto_0
    return-void

    .line 33
    :cond_2
    const-string/jumbo v0, "com.google.android.apps.chrome.icing.HistoryUsageMigrator.HISTORY_MIGRATION_REQUESTED"

    const/4 v2, 0x0

    invoke-interface {p0, v0, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 35
    invoke-virtual {p2}, Lcom/google/android/apps/chrome/icing/IcingController;->enableUsageReporting()V

    .line 36
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 65
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
