.class Lcom/google/android/apps/chrome/tab/ChromeTab$6$1;
.super Lorg/chromium/content/browser/ActivityContentVideoViewClient;
.source "ChromeTab.java"


# instance fields
.field final synthetic this$1:Lcom/google/android/apps/chrome/tab/ChromeTab$6;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/tab/ChromeTab$6;Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 1264
    iput-object p1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$6$1;->this$1:Lcom/google/android/apps/chrome/tab/ChromeTab$6;

    invoke-direct {p0, p2}, Lorg/chromium/content/browser/ActivityContentVideoViewClient;-><init>(Landroid/app/Activity;)V

    return-void
.end method


# virtual methods
.method public onDestroyContentVideoView()V
    .locals 2

    .prologue
    .line 1279
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$6$1;->this$1:Lcom/google/android/apps/chrome/tab/ChromeTab$6;

    iget-object v0, v0, Lcom/google/android/apps/chrome/tab/ChromeTab$6;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # getter for: Lcom/google/android/apps/chrome/tab/ChromeTab;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$2400(Lcom/google/android/apps/chrome/tab/ChromeTab;)Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1280
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$6$1;->this$1:Lcom/google/android/apps/chrome/tab/ChromeTab$6;

    iget-object v0, v0, Lcom/google/android/apps/chrome/tab/ChromeTab$6;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # getter for: Lcom/google/android/apps/chrome/tab/ChromeTab;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$2400(Lcom/google/android/apps/chrome/tab/ChromeTab;)Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->setOverlayVideoMode(Z)V

    .line 1282
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$6$1;->this$1:Lcom/google/android/apps/chrome/tab/ChromeTab$6;

    iget-object v0, v0, Lcom/google/android/apps/chrome/tab/ChromeTab$6;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1283
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$6$1;->this$1:Lcom/google/android/apps/chrome/tab/ChromeTab$6;

    iget-object v0, v0, Lcom/google/android/apps/chrome/tab/ChromeTab$6;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lorg/chromium/content/browser/ContentViewCore;->updateDoubleTapSupport(Z)V

    .line 1286
    :cond_0
    invoke-super {p0}, Lorg/chromium/content/browser/ActivityContentVideoViewClient;->onDestroyContentVideoView()V

    .line 1287
    return-void
.end method

.method public onShowCustomView(Landroid/view/View;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1267
    iget-object v1, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$6$1;->this$1:Lcom/google/android/apps/chrome/tab/ChromeTab$6;

    iget-object v1, v1, Lcom/google/android/apps/chrome/tab/ChromeTab$6;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # getter for: Lcom/google/android/apps/chrome/tab/ChromeTab;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;
    invoke-static {v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$2400(Lcom/google/android/apps/chrome/tab/ChromeTab;)Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    move-result-object v1

    if-nez v1, :cond_0

    .line 1274
    :goto_0
    return v0

    .line 1268
    :cond_0
    invoke-super {p0, p1}, Lorg/chromium/content/browser/ActivityContentVideoViewClient;->onShowCustomView(Landroid/view/View;)Z

    move-result v1

    .line 1269
    iget-object v2, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$6$1;->this$1:Lcom/google/android/apps/chrome/tab/ChromeTab$6;

    iget-object v2, v2, Lcom/google/android/apps/chrome/tab/ChromeTab$6;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    # getter for: Lcom/google/android/apps/chrome/tab/ChromeTab;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;
    invoke-static {v2}, Lcom/google/android/apps/chrome/tab/ChromeTab;->access$2400(Lcom/google/android/apps/chrome/tab/ChromeTab;)Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->setOverlayVideoMode(Z)V

    .line 1271
    iget-object v2, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$6$1;->this$1:Lcom/google/android/apps/chrome/tab/ChromeTab$6;

    iget-object v2, v2, Lcom/google/android/apps/chrome/tab/ChromeTab$6;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 1272
    iget-object v2, p0, Lcom/google/android/apps/chrome/tab/ChromeTab$6$1;->this$1:Lcom/google/android/apps/chrome/tab/ChromeTab$6;

    iget-object v2, v2, Lcom/google/android/apps/chrome/tab/ChromeTab$6;->this$0:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v2

    invoke-virtual {v2, v0}, Lorg/chromium/content/browser/ContentViewCore;->updateDoubleTapSupport(Z)V

    :cond_1
    move v0, v1

    .line 1274
    goto :goto_0
.end method
