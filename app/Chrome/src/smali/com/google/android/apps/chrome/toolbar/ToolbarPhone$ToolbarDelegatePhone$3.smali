.class Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone$3;
.super Landroid/animation/AnimatorListenerAdapter;
.source "ToolbarPhone.java"


# instance fields
.field final synthetic this$1:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;)V
    .locals 0

    .prologue
    .line 1265
    iput-object p1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone$3;->this$1:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2

    .prologue
    .line 1278
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone$3;->this$1:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;

    iget-object v0, v0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mDelayedOverviewModeAnimation:Landroid/animation/ObjectAnimator;
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$1302(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;Landroid/animation/ObjectAnimator;)Landroid/animation/ObjectAnimator;

    .line 1279
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone$3;->this$1:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;

    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone$3;->this$1:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;

    iget-object v1, v1, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mOverviewMode:Z
    invoke-static {v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$900(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Z

    move-result v1

    # invokes: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->updateShadowVisibility(Z)V
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->access$1700(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;Z)V

    .line 1280
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone$3;->this$1:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;

    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone$3;->this$1:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;

    iget-object v1, v1, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mOverviewMode:Z
    invoke-static {v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$900(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Z

    move-result v1

    # invokes: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->updateViewsForOverviewMode(Z)V
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->access$1600(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;Z)V

    .line 1281
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 2

    .prologue
    .line 1268
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone$3;->this$1:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;

    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone$3;->this$1:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;

    iget-object v1, v1, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mOverviewMode:Z
    invoke-static {v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$900(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Z

    move-result v1

    # invokes: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->updateViewsForOverviewMode(Z)V
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->access$1600(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;Z)V

    .line 1271
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-gt v0, v1, :cond_0

    .line 1272
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone$3;->this$1:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->requestLayout()V

    .line 1274
    :cond_0
    return-void
.end method
