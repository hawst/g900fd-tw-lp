.class public Lcom/google/android/apps/chrome/tabmodel/SingleTabModel;
.super Ljava/lang/Object;
.source "SingleTabModel.java"

# interfaces
.implements Lorg/chromium/chrome/browser/tabmodel/TabModel;


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final mActivity:Landroid/app/Activity;

.field private mBlockNewWindows:Z

.field private mIsIncognito:Z

.field private mTab:Lorg/chromium/chrome/browser/Tab;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const-class v0, Lcom/google/android/apps/chrome/tabmodel/SingleTabModel;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/tabmodel/SingleTabModel;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Landroid/app/Activity;ZZ)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/google/android/apps/chrome/tabmodel/SingleTabModel;->mActivity:Landroid/app/Activity;

    .line 28
    iput-boolean p2, p0, Lcom/google/android/apps/chrome/tabmodel/SingleTabModel;->mIsIncognito:Z

    .line 29
    iput-boolean p3, p0, Lcom/google/android/apps/chrome/tabmodel/SingleTabModel;->mBlockNewWindows:Z

    .line 30
    return-void
.end method

.method private completeActivity()V
    .locals 2

    .prologue
    .line 88
    invoke-static {}, Lcom/google/android/apps/chrome/utilities/DocumentApiDelegate;->create()Lcom/google/android/apps/chrome/utilities/DocumentApiDelegate;

    move-result-object v0

    .line 89
    if-eqz v0, :cond_0

    iget-boolean v1, p0, Lcom/google/android/apps/chrome/tabmodel/SingleTabModel;->mBlockNewWindows:Z

    if-eqz v1, :cond_1

    .line 90
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/SingleTabModel;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 94
    :goto_0
    return-void

    .line 92
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabmodel/SingleTabModel;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/utilities/DocumentApiDelegate;->finishAndRemoveTask(Landroid/app/Activity;)V

    goto :goto_0
.end method

.method private static native nativePermanentlyBlockAllNewWindows(Lorg/chromium/chrome/browser/Tab;)V
.end method


# virtual methods
.method public addTab(Lorg/chromium/chrome/browser/Tab;ILorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;)V
    .locals 0

    .prologue
    .line 151
    return-void
.end method

.method public cancelTabClosure(I)V
    .locals 0

    .prologue
    .line 142
    return-void
.end method

.method public closeAllTabs()V
    .locals 0

    .prologue
    .line 98
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabmodel/SingleTabModel;->completeActivity()V

    .line 99
    return-void
.end method

.method public closeTab(Lorg/chromium/chrome/browser/Tab;)Z
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 70
    invoke-virtual {p0, p1, v0, v0, v0}, Lcom/google/android/apps/chrome/tabmodel/SingleTabModel;->closeTab(Lorg/chromium/chrome/browser/Tab;ZZZ)Z

    move-result v0

    return v0
.end method

.method public closeTab(Lorg/chromium/chrome/browser/Tab;ZZZ)Z
    .locals 2

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/SingleTabModel;->mTab:Lorg/chromium/chrome/browser/Tab;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/SingleTabModel;->mTab:Lorg/chromium/chrome/browser/Tab;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/Tab;->getId()I

    move-result v0

    invoke-virtual {p1}, Lorg/chromium/chrome/browser/Tab;->getId()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 76
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabmodel/SingleTabModel;->completeActivity()V

    .line 77
    const/4 v0, 0x1

    .line 79
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public commitAllTabClosures()V
    .locals 0

    .prologue
    .line 134
    return-void
.end method

.method public commitTabClosure(I)V
    .locals 0

    .prologue
    .line 138
    return-void
.end method

.method public destroy()V
    .locals 0

    .prologue
    .line 164
    return-void
.end method

.method public getComprehensiveModel()Lorg/chromium/chrome/browser/tabmodel/TabList;
    .locals 1

    .prologue
    .line 129
    const/4 v0, 0x0

    return-object v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 55
    const/4 v0, 0x1

    return v0
.end method

.method public getNextTabIfClosed(I)Lorg/chromium/chrome/browser/Tab;
    .locals 1

    .prologue
    .line 119
    const/4 v0, 0x0

    return-object v0
.end method

.method public getProfile()Lorg/chromium/chrome/browser/profiles/Profile;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/SingleTabModel;->mTab:Lorg/chromium/chrome/browser/Tab;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/Tab;->getProfile()Lorg/chromium/chrome/browser/profiles/Profile;

    move-result-object v0

    return-object v0
.end method

.method public getTabAt(I)Lorg/chromium/chrome/browser/Tab;
    .locals 1

    .prologue
    .line 104
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/SingleTabModel;->mTab:Lorg/chromium/chrome/browser/Tab;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public index()I
    .locals 1

    .prologue
    .line 65
    const/4 v0, 0x0

    return v0
.end method

.method public indexOf(Lorg/chromium/chrome/browser/Tab;)I
    .locals 2

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/SingleTabModel;->mTab:Lorg/chromium/chrome/browser/Tab;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/SingleTabModel;->mTab:Lorg/chromium/chrome/browser/Tab;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/Tab;->getId()I

    move-result v0

    invoke-virtual {p1}, Lorg/chromium/chrome/browser/Tab;->getId()I

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public isClosurePending(I)Z
    .locals 1

    .prologue
    .line 124
    const/4 v0, 0x0

    return v0
.end method

.method public isIncognito()Z
    .locals 1

    .prologue
    .line 50
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tabmodel/SingleTabModel;->mIsIncognito:Z

    return v0
.end method

.method public moveTab(II)V
    .locals 1

    .prologue
    .line 114
    sget-boolean v0, Lcom/google/android/apps/chrome/tabmodel/SingleTabModel;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 115
    :cond_0
    return-void
.end method

.method public setIndex(ILorg/chromium/chrome/browser/tabmodel/TabModel$TabSelectionType;)V
    .locals 1

    .prologue
    .line 109
    sget-boolean v0, Lcom/google/android/apps/chrome/tabmodel/SingleTabModel;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 110
    :cond_0
    return-void
.end method

.method setTab(Lorg/chromium/chrome/browser/Tab;)V
    .locals 2

    .prologue
    .line 38
    iput-object p1, p0, Lcom/google/android/apps/chrome/tabmodel/SingleTabModel;->mTab:Lorg/chromium/chrome/browser/Tab;

    .line 39
    sget-boolean v0, Lcom/google/android/apps/chrome/tabmodel/SingleTabModel;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/SingleTabModel;->mTab:Lorg/chromium/chrome/browser/Tab;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/Tab;->isIncognito()Z

    move-result v0

    iget-boolean v1, p0, Lcom/google/android/apps/chrome/tabmodel/SingleTabModel;->mIsIncognito:Z

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 40
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tabmodel/SingleTabModel;->mBlockNewWindows:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/SingleTabModel;->mTab:Lorg/chromium/chrome/browser/Tab;

    invoke-static {v0}, Lcom/google/android/apps/chrome/tabmodel/SingleTabModel;->nativePermanentlyBlockAllNewWindows(Lorg/chromium/chrome/browser/Tab;)V

    .line 41
    :cond_1
    return-void
.end method

.method public supportsPendingClosures()Z
    .locals 1

    .prologue
    .line 146
    const/4 v0, 0x0

    return v0
.end method
