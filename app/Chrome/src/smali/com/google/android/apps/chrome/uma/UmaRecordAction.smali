.class public Lcom/google/android/apps/chrome/uma/UmaRecordAction;
.super Ljava/lang/Object;
.source "UmaRecordAction.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-class v0, Lcom/google/android/apps/chrome/uma/UmaRecordAction;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static back(Z)V
    .locals 0

    .prologue
    .line 30
    invoke-static {p0}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordBack(Z)V

    .line 31
    return-void
.end method

.method public static beamCallbackSuccess()V
    .locals 0

    .prologue
    .line 405
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordBeamCallbackSuccess()V

    .line 406
    return-void
.end method

.method public static beamInvalidAppState()V
    .locals 0

    .prologue
    .line 409
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordBeamInvalidAppState()V

    .line 410
    return-void
.end method

.method public static castButtonShown(Z)V
    .locals 0

    .prologue
    .line 555
    invoke-static {p0}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordCastButtonShown(Z)V

    .line 556
    return-void
.end method

.method public static castButtonShownOnInitialFullscreen(Z)V
    .locals 0

    .prologue
    .line 568
    invoke-static {p0}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordCastButtonShownOnInitialFullscreen(Z)V

    .line 569
    return-void
.end method

.method public static castDefaultPlayerResult(Z)V
    .locals 0

    .prologue
    .line 607
    invoke-static {p0}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordCastPlayResult(Z)V

    .line 608
    invoke-static {p0}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordCastDefaultPlayerResult(Z)V

    .line 609
    return-void
.end method

.method public static castEndedTimeRemaining(II)V
    .locals 0

    .prologue
    .line 621
    invoke-static {p0, p1}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordCastEndedTimeRemaining(II)V

    .line 622
    return-void
.end method

.method public static castMediaType(I)V
    .locals 0

    .prologue
    .line 630
    invoke-static {p0}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordCastMediaType(I)V

    .line 631
    return-void
.end method

.method public static castPlayRequested()V
    .locals 0

    .prologue
    .line 597
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordCastPlayRequested()V

    .line 598
    return-void
.end method

.method public static castYouTubePlayerResult(Z)V
    .locals 0

    .prologue
    .line 612
    invoke-static {p0}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordCastYouTubePlayerResult(Z)V

    .line 613
    return-void
.end method

.method public static coldStartup(Z)V
    .locals 0

    .prologue
    .line 456
    invoke-static {p0}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordColdStartup(Z)V

    .line 457
    return-void
.end method

.method public static crashUploadAttempt()V
    .locals 0

    .prologue
    .line 431
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordCrashUploadAttempt()V

    .line 432
    return-void
.end method

.method public static crashUploadAttemptFail()V
    .locals 0

    .prologue
    .line 439
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordCrashUploadFail()V

    .line 440
    return-void
.end method

.method public static crashUploadAttemptSuccess()V
    .locals 0

    .prologue
    .line 435
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordCrashUploadSuccess()V

    .line 436
    return-void
.end method

.method public static dataReductionProxyPromoAction(I)V
    .locals 2

    .prologue
    const/4 v1, 0x4

    .line 507
    sget-boolean v0, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    if-ltz p0, :cond_0

    if-lt p0, v1, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 508
    :cond_1
    invoke-static {p0, v1}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordDataReductionProxyPromoAction(II)V

    .line 510
    return-void
.end method

.method public static dataReductionProxyPromoDisplayed()V
    .locals 0

    .prologue
    .line 516
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordDataReductionProxyPromoDisplayed()V

    .line 517
    return-void
.end method

.method public static dataReductionProxySettings(I)V
    .locals 2

    .prologue
    const/4 v1, 0x4

    .line 524
    sget-boolean v0, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    if-ltz p0, :cond_0

    if-lt p0, v1, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 526
    :cond_1
    invoke-static {p0, v1}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordDataReductionProxySettings(II)V

    .line 528
    return-void
.end method

.method public static dataReductionProxyTurnedOff()V
    .locals 0

    .prologue
    .line 491
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordDataReductionProxyTurnedOff()V

    .line 492
    return-void
.end method

.method public static dataReductionProxyTurnedOn()V
    .locals 0

    .prologue
    .line 484
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordDataReductionProxyTurnedOn()V

    .line 485
    return-void
.end method

.method public static dataReductionProxyTurnedOnFromPromo()V
    .locals 0

    .prologue
    .line 499
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordDataReductionProxyTurnedOnFromPromo()V

    .line 500
    return-void
.end method

.method public static deviceMemoryClass(I)V
    .locals 0

    .prologue
    .line 461
    invoke-static {p0}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordDeviceMemoryClass(I)V

    .line 462
    return-void
.end method

.method public static firstEditInOmnibox()V
    .locals 0

    .prologue
    .line 310
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordFirstEditInOmnibox()V

    .line 311
    return-void
.end method

.method public static focusedFakeboxOnNtp()V
    .locals 0

    .prologue
    .line 298
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordFocusedFakeboxOnNtp()V

    .line 299
    return-void
.end method

.method public static focusedOmniboxNotOnNtp()V
    .locals 0

    .prologue
    .line 306
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordFocusedOmniboxNotOnNtp()V

    .line 307
    return-void
.end method

.method public static focusedOmniboxOnNtp()V
    .locals 0

    .prologue
    .line 302
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordFocusedOmniboxOnNtp()V

    .line 303
    return-void
.end method

.method public static foregroundTabAgeAtStartup(J)V
    .locals 0

    .prologue
    .line 227
    invoke-static {p0, p1}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordForegroundTabAgeAtStartup(J)V

    .line 228
    return-void
.end method

.method public static forward(Z)V
    .locals 0

    .prologue
    .line 41
    invoke-static {p0}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordForward(Z)V

    .line 42
    return-void
.end method

.method public static freSignInAccepted(ZZ)V
    .locals 0

    .prologue
    .line 391
    invoke-static {p0, p1}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordFreSignInAccepted(ZZ)V

    .line 392
    return-void
.end method

.method public static freSignInAttempted()V
    .locals 0

    .prologue
    .line 374
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordFreSignInAttempted()V

    .line 375
    return-void
.end method

.method public static freSignInDeclined()V
    .locals 0

    .prologue
    .line 395
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordFreSignInDeclined()V

    .line 396
    return-void
.end method

.method public static freSignInShown()V
    .locals 0

    .prologue
    .line 387
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordFreSignInShown()V

    .line 388
    return-void
.end method

.method public static freSignInSuccessful()V
    .locals 0

    .prologue
    .line 378
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordFreSignInSuccessful()V

    .line 379
    return-void
.end method

.method public static freSkipSignIn()V
    .locals 0

    .prologue
    .line 382
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordFreSkipSignIn()V

    .line 383
    return-void
.end method

.method public static geolocationHeaderHistogram(II)V
    .locals 0

    .prologue
    .line 179
    invoke-static {p0, p1}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordGeolocationHeaderHistogram(II)V

    .line 180
    return-void
.end method

.method public static getHistogramValueCount(Ljava/lang/String;I)I
    .locals 1

    .prologue
    .line 922
    invoke-static {p0, p1}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeGetHistogramValueCount(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public static lowMemoryTimeBetween(J)V
    .locals 0

    .prologue
    .line 465
    invoke-static {p0, p1}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordLowMemoryTimeBetween(J)V

    .line 466
    return-void
.end method

.method public static memoryNotificationBackground(I)V
    .locals 2

    .prologue
    const/4 v1, 0x4

    .line 469
    sget-boolean v0, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    if-ltz p0, :cond_0

    if-lt p0, v1, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 470
    :cond_1
    invoke-static {p0, v1}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordMemoryNotificationBackground(II)V

    .line 471
    return-void
.end method

.method public static memoryNotificationForeground(I)V
    .locals 3

    .prologue
    const/4 v2, 0x4

    .line 474
    sget-boolean v0, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    if-ltz p0, :cond_0

    if-lt p0, v2, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 476
    :cond_1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-ge v0, v1, :cond_2

    .line 478
    :goto_0
    return-void

    .line 477
    :cond_2
    invoke-static {p0, v2}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordMemoryNotificationForeground(II)V

    goto :goto_0
.end method

.method public static menuAddToBookmarks()V
    .locals 0

    .prologue
    .line 74
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordMenuAddToBookmarks()V

    .line 75
    return-void
.end method

.method public static menuAddToHomescreen()V
    .locals 0

    .prologue
    .line 62
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordMenuAddToHomescreen()V

    .line 63
    return-void
.end method

.method public static menuAllBookmarks()V
    .locals 0

    .prologue
    .line 66
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordMenuAllBookmarks()V

    .line 67
    return-void
.end method

.method public static menuCloseAllTabs()V
    .locals 0

    .prologue
    .line 54
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordMenuCloseAllTabs()V

    .line 55
    return-void
.end method

.method public static menuDirectShare()V
    .locals 0

    .prologue
    .line 82
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordMenuDirectShare()V

    .line 83
    return-void
.end method

.method public static menuFeedback()V
    .locals 0

    .prologue
    .line 98
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordMenuFeedback()V

    .line 99
    return-void
.end method

.method public static menuFindInPage()V
    .locals 0

    .prologue
    .line 90
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordMenuFindInPage()V

    .line 91
    return-void
.end method

.method public static menuHistory()V
    .locals 0

    .prologue
    .line 86
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordMenuHistory()V

    .line 87
    return-void
.end method

.method public static menuNewIncognitoTab()V
    .locals 0

    .prologue
    .line 58
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordMenuNewIncognitoTab()V

    .line 59
    return-void
.end method

.method public static menuNewTab()V
    .locals 0

    .prologue
    .line 50
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordMenuNewTab()V

    .line 51
    return-void
.end method

.method public static menuOpenTabs()V
    .locals 0

    .prologue
    .line 70
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordMenuOpenTabs()V

    .line 71
    return-void
.end method

.method public static menuPrint()V
    .locals 0

    .prologue
    .line 102
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordMenuPrint()V

    .line 103
    return-void
.end method

.method public static menuRequestDesktopSite()V
    .locals 0

    .prologue
    .line 106
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordMenuRequestDesktopSite()V

    .line 107
    return-void
.end method

.method public static menuSettings()V
    .locals 0

    .prologue
    .line 94
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordMenuSettings()V

    .line 95
    return-void
.end method

.method public static menuShare()V
    .locals 0

    .prologue
    .line 78
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordMenuShare()V

    .line 79
    return-void
.end method

.method public static multiWindowSession(II)V
    .locals 0

    .prologue
    .line 132
    invoke-static {p0, p1}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordMultiWindowSession(II)V

    .line 133
    return-void
.end method

.method private static native nativeGetHistogramValueCount(Ljava/lang/String;I)I
.end method

.method private static native nativeRecordAccountConsistencyActionAcceptedAdvanced()V
.end method

.method private static native nativeRecordAccountConsistencyActionAcceptedDefaults()V
.end method

.method private static native nativeRecordAccountConsistencyActionShown()V
.end method

.method private static native nativeRecordActionBarShown()V
.end method

.method private static native nativeRecordAndroidSigninPromoAccepted()V
.end method

.method private static native nativeRecordAndroidSigninPromoAcceptedAdvanced()V
.end method

.method private static native nativeRecordAndroidSigninPromoDeclined()V
.end method

.method private static native nativeRecordAndroidSigninPromoEnabled()V
.end method

.method private static native nativeRecordAndroidSigninPromoShown()V
.end method

.method private static native nativeRecordBack(Z)V
.end method

.method private static native nativeRecordBeamCallbackSuccess()V
.end method

.method private static native nativeRecordBeamInvalidAppState()V
.end method

.method private static native nativeRecordCastButtonShown(Z)V
.end method

.method private static native nativeRecordCastButtonShownOnInitialFullscreen(Z)V
.end method

.method private static native nativeRecordCastDefaultPlayerResult(Z)V
.end method

.method private static native nativeRecordCastDeviceSelected()V
.end method

.method private static native nativeRecordCastEndedTimeRemaining(II)V
.end method

.method private static native nativeRecordCastEnterFullscreen()V
.end method

.method private static native nativeRecordCastMediaType(I)V
.end method

.method private static native nativeRecordCastPlayRequested()V
.end method

.method private static native nativeRecordCastPlayResult(Z)V
.end method

.method private static native nativeRecordCastYouTubePlayerResult(Z)V
.end method

.method private static native nativeRecordColdStartup(Z)V
.end method

.method private static native nativeRecordContextMenuCopyLinkAddress()V
.end method

.method private static native nativeRecordContextMenuCopyLinkText()V
.end method

.method private static native nativeRecordContextMenuDownloadImage()V
.end method

.method private static native nativeRecordContextMenuDownloadLink()V
.end method

.method private static native nativeRecordContextMenuDownloadVideo()V
.end method

.method private static native nativeRecordContextMenuImage()V
.end method

.method private static native nativeRecordContextMenuLink()V
.end method

.method private static native nativeRecordContextMenuOpenImageInNewTab()V
.end method

.method private static native nativeRecordContextMenuOpenNewIncognitoTab()V
.end method

.method private static native nativeRecordContextMenuOpenNewTab()V
.end method

.method private static native nativeRecordContextMenuOpenOriginalImageInNewTab()V
.end method

.method private static native nativeRecordContextMenuSaveImage()V
.end method

.method private static native nativeRecordContextMenuSearchByImage()V
.end method

.method private static native nativeRecordContextMenuText()V
.end method

.method private static native nativeRecordContextMenuVideo()V
.end method

.method private static native nativeRecordContextMenuViewImage()V
.end method

.method private static native nativeRecordContextualSearchDurationSeen(J)V
.end method

.method private static native nativeRecordContextualSearchDurationUnseen(J)V
.end method

.method private static native nativeRecordContextualSearchDurationUnseenChained(J)V
.end method

.method private static native nativeRecordContextualSearchEnterClosed(II)V
.end method

.method private static native nativeRecordContextualSearchEnterExpanded(II)V
.end method

.method private static native nativeRecordContextualSearchEnterMaximized(II)V
.end method

.method private static native nativeRecordContextualSearchEnterPeeked(II)V
.end method

.method private static native nativeRecordContextualSearchExitClosed(II)V
.end method

.method private static native nativeRecordContextualSearchExitExpanded(II)V
.end method

.method private static native nativeRecordContextualSearchExitMaximized(II)V
.end method

.method private static native nativeRecordContextualSearchExitPeeked(II)V
.end method

.method private static native nativeRecordContextualSearchFirstRunFlowOutcome(II)V
.end method

.method private static native nativeRecordContextualSearchFirstRunPanelSeen(II)V
.end method

.method private static native nativeRecordContextualSearchPreferenceChange(II)V
.end method

.method private static native nativeRecordContextualSearchPreferenceState(II)V
.end method

.method private static native nativeRecordContextualSearchResultsSeen(II)V
.end method

.method private static native nativeRecordContextualSearchSelectionIsValid(II)V
.end method

.method private static native nativeRecordContextualSearchTimeToSearch(J)V
.end method

.method private static native nativeRecordCrashUploadAttempt()V
.end method

.method private static native nativeRecordCrashUploadFail()V
.end method

.method private static native nativeRecordCrashUploadSuccess()V
.end method

.method private static native nativeRecordDataReductionProxyPromoAction(II)V
.end method

.method private static native nativeRecordDataReductionProxyPromoDisplayed()V
.end method

.method private static native nativeRecordDataReductionProxySettings(II)V
.end method

.method private static native nativeRecordDataReductionProxyTurnedOff()V
.end method

.method private static native nativeRecordDataReductionProxyTurnedOn()V
.end method

.method private static native nativeRecordDataReductionProxyTurnedOnFromPromo()V
.end method

.method private static native nativeRecordDeviceMemoryClass(I)V
.end method

.method private static native nativeRecordDocumentActivityClickAction(I)V
.end method

.method private static native nativeRecordDocumentActivityEditUrlAction(I)V
.end method

.method private static native nativeRecordDocumentActivityFiredSearchIntent()V
.end method

.method private static native nativeRecordDocumentActivityHomeExitAction(I)V
.end method

.method private static native nativeRecordDocumentActivityStartedBy(I)V
.end method

.method private static native nativeRecordDocumentMode(Z)V
.end method

.method private static native nativeRecordDocumentOptOutPromoClick(I)V
.end method

.method private static native nativeRecordDocumentOptOutShown()V
.end method

.method private static native nativeRecordDocumentUserOptOut(Z)V
.end method

.method private static native nativeRecordExternalIntentSource(II)V
.end method

.method private static native nativeRecordFirstEditInOmnibox()V
.end method

.method private static native nativeRecordFocusedFakeboxOnNtp()V
.end method

.method private static native nativeRecordFocusedOmniboxNotOnNtp()V
.end method

.method private static native nativeRecordFocusedOmniboxOnNtp()V
.end method

.method private static native nativeRecordForegroundTabAgeAtStartup(J)V
.end method

.method private static native nativeRecordForward(Z)V
.end method

.method private static native nativeRecordFreSignInAccepted(ZZ)V
.end method

.method private static native nativeRecordFreSignInAttempted()V
.end method

.method private static native nativeRecordFreSignInDeclined()V
.end method

.method private static native nativeRecordFreSignInShown()V
.end method

.method private static native nativeRecordFreSignInSuccessful()V
.end method

.method private static native nativeRecordFreSkipSignIn()V
.end method

.method private static native nativeRecordGeolocationHeaderHistogram(II)V
.end method

.method private static native nativeRecordInstantSearchClicksBack()V
.end method

.method private static native nativeRecordInstantSearchClicksPreviewScrollState(II)V
.end method

.method private static native nativeRecordInstantSearchClicksReload()V
.end method

.method private static native nativeRecordInstantSearchClicksSingleTap()V
.end method

.method private static native nativeRecordInstantSearchClicksSwapReason(II)V
.end method

.method private static native nativeRecordInstantSearchClicksSwapTime(J)V
.end method

.method private static native nativeRecordInstantSearchClicksTabClose()V
.end method

.method private static native nativeRecordInstantSearchClicksTimeInPreview(J)V
.end method

.method private static native nativeRecordLowMemoryTimeBetween(J)V
.end method

.method private static native nativeRecordMemoryNotificationBackground(II)V
.end method

.method private static native nativeRecordMemoryNotificationForeground(II)V
.end method

.method private static native nativeRecordMenuAddToBookmarks()V
.end method

.method private static native nativeRecordMenuAddToHomescreen()V
.end method

.method private static native nativeRecordMenuAllBookmarks()V
.end method

.method private static native nativeRecordMenuCloseAllTabs()V
.end method

.method private static native nativeRecordMenuDirectShare()V
.end method

.method private static native nativeRecordMenuFeedback()V
.end method

.method private static native nativeRecordMenuFindInPage()V
.end method

.method private static native nativeRecordMenuHistory()V
.end method

.method private static native nativeRecordMenuNewIncognitoTab()V
.end method

.method private static native nativeRecordMenuNewTab()V
.end method

.method private static native nativeRecordMenuOpenTabs()V
.end method

.method private static native nativeRecordMenuPrint()V
.end method

.method private static native nativeRecordMenuRequestDesktopSite()V
.end method

.method private static native nativeRecordMenuSettings()V
.end method

.method private static native nativeRecordMenuShare()V
.end method

.method private static native nativeRecordMultiWindowSession(II)V
.end method

.method private static native nativeRecordNtpAction(II)V
.end method

.method private static native nativeRecordNtpBookmark()V
.end method

.method private static native nativeRecordNtpForeignSession()V
.end method

.method private static native nativeRecordNtpLoadTime(J)V
.end method

.method private static native nativeRecordNtpMostVisited()V
.end method

.method private static native nativeRecordNtpMostVisitedIndex(II)V
.end method

.method private static native nativeRecordNtpRecentlyClosed()V
.end method

.method private static native nativeRecordNtpSectionBookmarks()V
.end method

.method private static native nativeRecordNtpSectionOpenTabs()V
.end method

.method private static native nativeRecordOmniboxDeleteGesture()V
.end method

.method private static native nativeRecordOmniboxDeleteRequested()V
.end method

.method private static native nativeRecordOmniboxFirstFocusTime(J)V
.end method

.method private static native nativeRecordOmniboxRefineSuggestion()V
.end method

.method private static native nativeRecordOmniboxSearch()V
.end method

.method private static native nativeRecordOmniboxSuggestionsDismissed(I)V
.end method

.method private static native nativeRecordOmniboxVoiceSearch()V
.end method

.method private static native nativeRecordPageLoaded(Z)V
.end method

.method private static native nativeRecordPageLoadedWithKeyboard()V
.end method

.method private static native nativeRecordReaderModeEligible(Z)V
.end method

.method private static native nativeRecordReaderModeEntered()V
.end method

.method private static native nativeRecordReaderModePrefsEntered()V
.end method

.method private static native nativeRecordReceivedExternalIntent()V
.end method

.method private static native nativeRecordReload()V
.end method

.method private static native nativeRecordRemotePlaybackDeviceSelected(I)V
.end method

.method private static native nativeRecordShortcutAllBookmarks()V
.end method

.method private static native nativeRecordShortcutFindInPage()V
.end method

.method private static native nativeRecordShortcutNewIncognitoTab()V
.end method

.method private static native nativeRecordShortcutNewTab()V
.end method

.method private static native nativeRecordSideSwipeFinished()V
.end method

.method private static native nativeRecordStackViewCloseTab()V
.end method

.method private static native nativeRecordStackViewNewTab()V
.end method

.method private static native nativeRecordStackViewSwipeCloseTab()V
.end method

.method private static native nativeRecordSystemBack()V
.end method

.method private static native nativeRecordSystemBackForNavigation()V
.end method

.method private static native nativeRecordTabAgeUponRestoreFromColdStart(J)V
.end method

.method private static native nativeRecordTabBackgroundLoadStatus(II)V
.end method

.method private static native nativeRecordTabClobbered()V
.end method

.method private static native nativeRecordTabCountAtStartup(I)V
.end method

.method private static native nativeRecordTabCountPerLoad(II)V
.end method

.method private static native nativeRecordTabRestoreResult(ZJJI)V
.end method

.method private static native nativeRecordTabShown(JI)V
.end method

.method private static native nativeRecordTabStatusWhenSwitchedBackToForeground(II)V
.end method

.method private static native nativeRecordTabStripCloseTab()V
.end method

.method private static native nativeRecordTabStripNewTab()V
.end method

.method private static native nativeRecordTabSwitched()V
.end method

.method private static native nativeRecordTabUndoBarIgnored(Z)V
.end method

.method private static native nativeRecordTabUndoBarPressed()V
.end method

.method private static native nativeRecordTabUndoBarShown(Z)V
.end method

.method private static native nativeRecordToolbarFirstDrawTime(J)V
.end method

.method private static native nativeRecordToolbarInflationTime(J)V
.end method

.method private static native nativeRecordToolbarPhoneTabStack()V
.end method

.method private static native nativeRecordToolbarShowMenu()V
.end method

.method private static native nativeRecordToolbarToggleBookmark()V
.end method

.method private static native nativeRecordUserActionDuringTabRestore(II)V
.end method

.method private static native nativeRecordYouTubeDeviceSelected()V
.end method

.method public static ntpAction(II)V
    .locals 0

    .prologue
    .line 261
    invoke-static {p0, p1}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordNtpAction(II)V

    .line 262
    return-void
.end method

.method public static ntpBookmark()V
    .locals 0

    .prologue
    .line 277
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordNtpBookmark()V

    .line 278
    return-void
.end method

.method public static ntpForeignSession()V
    .locals 0

    .prologue
    .line 285
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordNtpForeignSession()V

    .line 286
    return-void
.end method

.method public static ntpLoadTime(J)V
    .locals 0

    .prologue
    .line 265
    invoke-static {p0, p1}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordNtpLoadTime(J)V

    .line 266
    return-void
.end method

.method public static ntpMostVisited()V
    .locals 0

    .prologue
    .line 273
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordNtpMostVisited()V

    .line 274
    return-void
.end method

.method public static ntpMostVisitedIndex(II)V
    .locals 0

    .prologue
    .line 269
    invoke-static {p0, p1}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordNtpMostVisitedIndex(II)V

    .line 270
    return-void
.end method

.method public static ntpRecentlyClosed()V
    .locals 0

    .prologue
    .line 281
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordNtpRecentlyClosed()V

    .line 282
    return-void
.end method

.method public static ntpSectionBookmarks()V
    .locals 0

    .prologue
    .line 289
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordNtpSectionBookmarks()V

    .line 290
    return-void
.end method

.method public static ntpSectionOpenTabs()V
    .locals 0

    .prologue
    .line 293
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordNtpSectionOpenTabs()V

    .line 294
    return-void
.end method

.method public static omniboxDeleteGesture()V
    .locals 0

    .prologue
    .line 171
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordOmniboxDeleteGesture()V

    .line 172
    return-void
.end method

.method public static omniboxDeleteRequested()V
    .locals 0

    .prologue
    .line 175
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordOmniboxDeleteRequested()V

    .line 176
    return-void
.end method

.method public static omniboxFirstFocusTime(J)V
    .locals 0

    .prologue
    .line 452
    invoke-static {p0, p1}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordOmniboxFirstFocusTime(J)V

    .line 453
    return-void
.end method

.method public static omniboxRefineSuggestion()V
    .locals 0

    .prologue
    .line 156
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordOmniboxRefineSuggestion()V

    .line 157
    return-void
.end method

.method public static omniboxSearch()V
    .locals 0

    .prologue
    .line 148
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordOmniboxSearch()V

    .line 149
    return-void
.end method

.method public static omniboxSuggestionsDismissed(I)V
    .locals 0

    .prologue
    .line 167
    invoke-static {p0}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordOmniboxSuggestionsDismissed(I)V

    .line 168
    return-void
.end method

.method public static omniboxVoiceSearch()V
    .locals 0

    .prologue
    .line 152
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordOmniboxVoiceSearch()V

    .line 153
    return-void
.end method

.method public static pageLoaded(ZZ)V
    .locals 0

    .prologue
    .line 207
    invoke-static {p1}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordPageLoaded(Z)V

    .line 208
    if-eqz p0, :cond_0

    .line 209
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordPageLoadedWithKeyboard()V

    .line 211
    :cond_0
    return-void
.end method

.method public static readerModeEligible(Z)V
    .locals 0

    .prologue
    .line 882
    invoke-static {p0}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordReaderModeEligible(Z)V

    .line 883
    return-void
.end method

.method public static readerModeEntered()V
    .locals 0

    .prologue
    .line 886
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordReaderModeEntered()V

    .line 887
    return-void
.end method

.method public static readerModePrefsEntered()V
    .locals 0

    .prologue
    .line 890
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordReaderModePrefsEntered()V

    .line 891
    return-void
.end method

.method public static receivedExternalIntent()V
    .locals 0

    .prologue
    .line 218
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordReceivedExternalIntent()V

    .line 219
    return-void
.end method

.method public static recordAccountConsistencyActionAcceptedAdvanced()V
    .locals 0

    .prologue
    .line 877
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordAccountConsistencyActionAcceptedAdvanced()V

    .line 878
    return-void
.end method

.method public static recordAccountConsistencyActionAcceptedDefaults()V
    .locals 0

    .prologue
    .line 873
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordAccountConsistencyActionAcceptedDefaults()V

    .line 874
    return-void
.end method

.method public static recordAccountConsistencyActionShown()V
    .locals 0

    .prologue
    .line 869
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordAccountConsistencyActionShown()V

    .line 870
    return-void
.end method

.method public static recordActionBarShown()V
    .locals 0

    .prologue
    .line 400
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordActionBarShown()V

    .line 401
    return-void
.end method

.method public static recordAndroidSigninPromoAccepted()V
    .locals 0

    .prologue
    .line 907
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordAndroidSigninPromoAccepted()V

    .line 908
    return-void
.end method

.method public static recordAndroidSigninPromoAcceptedAdvanced()V
    .locals 0

    .prologue
    .line 911
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordAndroidSigninPromoAcceptedAdvanced()V

    .line 912
    return-void
.end method

.method public static recordAndroidSigninPromoDeclined()V
    .locals 0

    .prologue
    .line 903
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordAndroidSigninPromoDeclined()V

    .line 904
    return-void
.end method

.method public static recordAndroidSigninPromoEnabled()V
    .locals 0

    .prologue
    .line 895
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordAndroidSigninPromoEnabled()V

    .line 896
    return-void
.end method

.method public static recordAndroidSigninPromoShown()V
    .locals 0

    .prologue
    .line 899
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordAndroidSigninPromoShown()V

    .line 900
    return-void
.end method

.method public static recordContextMenuCopyLinkAddress()V
    .locals 0

    .prologue
    .line 340
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordContextMenuCopyLinkAddress()V

    .line 341
    return-void
.end method

.method public static recordContextMenuCopyLinkText()V
    .locals 0

    .prologue
    .line 344
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordContextMenuCopyLinkText()V

    .line 345
    return-void
.end method

.method public static recordContextMenuDownloadImage()V
    .locals 0

    .prologue
    .line 365
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordContextMenuDownloadImage()V

    .line 366
    return-void
.end method

.method public static recordContextMenuDownloadLink()V
    .locals 0

    .prologue
    .line 361
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordContextMenuDownloadLink()V

    .line 362
    return-void
.end method

.method public static recordContextMenuDownloadVideo()V
    .locals 0

    .prologue
    .line 369
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordContextMenuDownloadVideo()V

    .line 370
    return-void
.end method

.method public static recordContextMenuOpenImageInNewTab(Z)V
    .locals 0

    .prologue
    .line 352
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordContextMenuOpenImageInNewTab()V

    .line 353
    if-eqz p0, :cond_0

    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordContextMenuOpenOriginalImageInNewTab()V

    .line 354
    :cond_0
    return-void
.end method

.method public static recordContextMenuOpenImageUrl()V
    .locals 0

    .prologue
    .line 336
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordContextMenuViewImage()V

    .line 337
    return-void
.end method

.method public static recordContextMenuOpenInNewIncognitoTab()V
    .locals 0

    .prologue
    .line 332
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordContextMenuOpenNewIncognitoTab()V

    .line 333
    return-void
.end method

.method public static recordContextMenuOpenInNewTab()V
    .locals 0

    .prologue
    .line 328
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordContextMenuOpenNewTab()V

    .line 329
    return-void
.end method

.method public static recordContextMenuSaveImage()V
    .locals 0

    .prologue
    .line 348
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordContextMenuSaveImage()V

    .line 349
    return-void
.end method

.method public static recordContextMenuSearchByImage()V
    .locals 0

    .prologue
    .line 357
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordContextMenuSearchByImage()V

    .line 358
    return-void
.end method

.method public static recordContextualSearchDurationSeen(J)V
    .locals 0

    .prologue
    .line 668
    invoke-static {p0, p1}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordContextualSearchDurationSeen(J)V

    .line 669
    return-void
.end method

.method public static recordContextualSearchDurationUnseen(J)V
    .locals 0

    .prologue
    .line 686
    invoke-static {p0, p1}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordContextualSearchDurationUnseen(J)V

    .line 687
    return-void
.end method

.method public static recordContextualSearchDurationUnseenChained(J)V
    .locals 0

    .prologue
    .line 677
    invoke-static {p0, p1}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordContextualSearchDurationUnseenChained(J)V

    .line 678
    return-void
.end method

.method public static recordContextualSearchEnterClosed(II)V
    .locals 0

    .prologue
    .line 722
    invoke-static {p0, p1}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordContextualSearchEnterClosed(II)V

    .line 723
    return-void
.end method

.method public static recordContextualSearchEnterExpanded(II)V
    .locals 0

    .prologue
    .line 740
    invoke-static {p0, p1}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordContextualSearchEnterExpanded(II)V

    .line 741
    return-void
.end method

.method public static recordContextualSearchEnterMaximized(II)V
    .locals 0

    .prologue
    .line 749
    invoke-static {p0, p1}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordContextualSearchEnterMaximized(II)V

    .line 750
    return-void
.end method

.method public static recordContextualSearchEnterPeeked(II)V
    .locals 0

    .prologue
    .line 731
    invoke-static {p0, p1}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordContextualSearchEnterPeeked(II)V

    .line 732
    return-void
.end method

.method public static recordContextualSearchExitClosed(II)V
    .locals 0

    .prologue
    .line 758
    invoke-static {p0, p1}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordContextualSearchExitClosed(II)V

    .line 759
    return-void
.end method

.method public static recordContextualSearchExitExpanded(II)V
    .locals 0

    .prologue
    .line 776
    invoke-static {p0, p1}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordContextualSearchExitExpanded(II)V

    .line 777
    return-void
.end method

.method public static recordContextualSearchExitMaximized(II)V
    .locals 0

    .prologue
    .line 785
    invoke-static {p0, p1}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordContextualSearchExitMaximized(II)V

    .line 786
    return-void
.end method

.method public static recordContextualSearchExitPeeked(II)V
    .locals 0

    .prologue
    .line 767
    invoke-static {p0, p1}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordContextualSearchExitPeeked(II)V

    .line 768
    return-void
.end method

.method public static recordContextualSearchFirstRunFlowOutcome(II)V
    .locals 0

    .prologue
    .line 660
    invoke-static {p0, p1}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordContextualSearchFirstRunFlowOutcome(II)V

    .line 661
    return-void
.end method

.method public static recordContextualSearchFirstRunPanelSeen(II)V
    .locals 0

    .prologue
    .line 695
    invoke-static {p0, p1}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordContextualSearchFirstRunPanelSeen(II)V

    .line 696
    return-void
.end method

.method public static recordContextualSearchPreferenceChange(II)V
    .locals 0

    .prologue
    .line 650
    invoke-static {p0, p1}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordContextualSearchPreferenceChange(II)V

    .line 651
    return-void
.end method

.method public static recordContextualSearchPreferenceState(II)V
    .locals 0

    .prologue
    .line 641
    invoke-static {p0, p1}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordContextualSearchPreferenceState(II)V

    .line 642
    return-void
.end method

.method public static recordContextualSearchResultsSeen(II)V
    .locals 0

    .prologue
    .line 704
    invoke-static {p0, p1}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordContextualSearchResultsSeen(II)V

    .line 705
    return-void
.end method

.method public static recordContextualSearchSelectionIsValid(II)V
    .locals 0

    .prologue
    .line 713
    invoke-static {p0, p1}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordContextualSearchSelectionIsValid(II)V

    .line 714
    return-void
.end method

.method public static recordDocumentActivityClickAction(I)V
    .locals 0

    .prologue
    .line 835
    invoke-static {p0}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordDocumentActivityClickAction(I)V

    .line 836
    return-void
.end method

.method public static recordDocumentActivityFiredSearchIntent()V
    .locals 0

    .prologue
    .line 831
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordDocumentActivityFiredSearchIntent()V

    .line 832
    return-void
.end method

.method public static recordDocumentActivityHomeExitAction(I)V
    .locals 0

    .prologue
    .line 839
    invoke-static {p0}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordDocumentActivityHomeExitAction(I)V

    .line 840
    return-void
.end method

.method public static recordDocumentActivityStartedBy(I)V
    .locals 0

    .prologue
    .line 843
    invoke-static {p0}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordDocumentActivityStartedBy(I)V

    .line 844
    return-void
.end method

.method public static recordDocumentMode(Z)V
    .locals 0

    .prologue
    .line 863
    invoke-static {p0}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordDocumentMode(Z)V

    .line 864
    return-void
.end method

.method public static recordDocumentOptOutPromoClick(I)V
    .locals 0

    .prologue
    .line 851
    invoke-static {p0}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordDocumentOptOutPromoClick(I)V

    .line 852
    return-void
.end method

.method public static recordDocumentOptOutShown()V
    .locals 0

    .prologue
    .line 855
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordDocumentOptOutShown()V

    .line 856
    return-void
.end method

.method public static recordDocumentUserOptOut(Z)V
    .locals 0

    .prologue
    .line 859
    invoke-static {p0}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordDocumentUserOptOut(Z)V

    .line 860
    return-void
.end method

.method public static recordExternalIntentSource(II)V
    .locals 0

    .prologue
    .line 536
    invoke-static {p0, p1}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordExternalIntentSource(II)V

    .line 537
    return-void
.end method

.method public static recordInstantSearchClicksBack()V
    .locals 0

    .prologue
    .line 815
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordInstantSearchClicksBack()V

    .line 816
    return-void
.end method

.method public static recordInstantSearchClicksPreviewScrollState(II)V
    .locals 0

    .prologue
    .line 811
    invoke-static {p0, p1}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordInstantSearchClicksPreviewScrollState(II)V

    .line 812
    return-void
.end method

.method public static recordInstantSearchClicksReload()V
    .locals 0

    .prologue
    .line 819
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordInstantSearchClicksReload()V

    .line 820
    return-void
.end method

.method public static recordInstantSearchClicksSingleTap()V
    .locals 0

    .prologue
    .line 827
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordInstantSearchClicksSingleTap()V

    .line 828
    return-void
.end method

.method public static recordInstantSearchClicksSwapReason(II)V
    .locals 0

    .prologue
    .line 806
    invoke-static {p0, p1}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordInstantSearchClicksSwapReason(II)V

    .line 807
    return-void
.end method

.method public static recordInstantSearchClicksTabClose()V
    .locals 0

    .prologue
    .line 823
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordInstantSearchClicksTabClose()V

    .line 824
    return-void
.end method

.method public static recordInstantSearchClicksTimeInPreview(J)V
    .locals 0

    .prologue
    .line 802
    invoke-static {p0, p1}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordInstantSearchClicksTimeInPreview(J)V

    .line 803
    return-void
.end method

.method public static recordInstantSearchClicksTimeToSwap(J)V
    .locals 0

    .prologue
    .line 798
    invoke-static {p0, p1}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordInstantSearchClicksSwapTime(J)V

    .line 799
    return-void
.end method

.method public static recordShowContextMenu(ZZZZ)V
    .locals 0

    .prologue
    .line 316
    if-eqz p1, :cond_1

    .line 317
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordContextMenuLink()V

    .line 325
    :cond_0
    :goto_0
    return-void

    .line 318
    :cond_1
    if-eqz p0, :cond_2

    .line 319
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordContextMenuImage()V

    goto :goto_0

    .line 320
    :cond_2
    if-eqz p2, :cond_3

    .line 321
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordContextMenuText()V

    goto :goto_0

    .line 322
    :cond_3
    if-eqz p3, :cond_0

    .line 323
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordContextMenuVideo()V

    goto :goto_0
.end method

.method public static reload()V
    .locals 0

    .prologue
    .line 45
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordReload()V

    .line 46
    return-void
.end method

.method public static remotePlaybackDeviceSelected(I)V
    .locals 1

    .prologue
    .line 586
    sget-boolean v0, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    if-ltz p0, :cond_0

    const/4 v0, 0x3

    if-lt p0, v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 588
    :cond_1
    invoke-static {p0}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordRemotePlaybackDeviceSelected(I)V

    .line 589
    return-void
.end method

.method public static shortcutFindInPage()V
    .locals 0

    .prologue
    .line 422
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordShortcutFindInPage()V

    .line 423
    return-void
.end method

.method public static stackViewCloseTab()V
    .locals 0

    .prologue
    .line 140
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordStackViewCloseTab()V

    .line 141
    return-void
.end method

.method public static stackViewNewTab()V
    .locals 0

    .prologue
    .line 136
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordStackViewNewTab()V

    .line 137
    return-void
.end method

.method public static stackViewSwipeCloseTab()V
    .locals 0

    .prologue
    .line 144
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordStackViewSwipeCloseTab()V

    .line 145
    return-void
.end method

.method public static systemBack()V
    .locals 0

    .prologue
    .line 34
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordSystemBack()V

    .line 35
    return-void
.end method

.method public static systemBackForNavigation()V
    .locals 0

    .prologue
    .line 38
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordSystemBackForNavigation()V

    .line 39
    return-void
.end method

.method public static tabAgeUponRestoreFromColdStart(J)V
    .locals 0

    .prologue
    .line 231
    invoke-static {p0, p1}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordTabAgeUponRestoreFromColdStart(J)V

    .line 232
    return-void
.end method

.method public static tabBackgroundLoadStatus(I)V
    .locals 2

    .prologue
    const/4 v1, 0x3

    .line 240
    sget-boolean v0, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    if-ltz p0, :cond_0

    if-lt p0, v1, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 241
    :cond_1
    invoke-static {p0, v1}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordTabBackgroundLoadStatus(II)V

    .line 242
    return-void
.end method

.method public static tabClobbered()V
    .locals 0

    .prologue
    .line 183
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordTabClobbered()V

    .line 184
    return-void
.end method

.method public static tabCountAtStartup(I)V
    .locals 0

    .prologue
    .line 223
    invoke-static {p0}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordTabCountAtStartup(I)V

    .line 224
    return-void
.end method

.method public static tabCountPerLoad(II)V
    .locals 0

    .prologue
    .line 214
    invoke-static {p0, p1}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordTabCountPerLoad(II)V

    .line 215
    return-void
.end method

.method public static tabRestoreResult(ZJJI)V
    .locals 1

    .prologue
    .line 256
    invoke-static/range {p0 .. p5}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordTabRestoreResult(ZJJI)V

    .line 257
    return-void
.end method

.method public static tabSideSwipeFinished()V
    .locals 0

    .prologue
    .line 191
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordSideSwipeFinished()V

    .line 192
    return-void
.end method

.method public static tabStatusWhenSwitchedBackToForeground(I)V
    .locals 2

    .prologue
    const/16 v1, 0x9

    .line 235
    sget-boolean v0, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    if-ltz p0, :cond_0

    if-lt p0, v1, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 236
    :cond_1
    invoke-static {p0, v1}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordTabStatusWhenSwitchedBackToForeground(II)V

    .line 237
    return-void
.end method

.method public static tabStripCloseTab()V
    .locals 0

    .prologue
    .line 119
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordTabStripCloseTab()V

    .line 120
    return-void
.end method

.method public static tabStripNewTab()V
    .locals 0

    .prologue
    .line 115
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordTabStripNewTab()V

    .line 116
    return-void
.end method

.method public static tabSwitched()V
    .locals 0

    .prologue
    .line 187
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordTabSwitched()V

    .line 188
    return-void
.end method

.method public static tabSwitchedToForeground(JI)V
    .locals 0

    .prologue
    .line 245
    invoke-static {p0, p1, p2}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordTabShown(JI)V

    .line 246
    return-void
.end method

.method public static tabUndoBarDismissed(Z)V
    .locals 0

    .prologue
    .line 203
    invoke-static {p0}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordTabUndoBarIgnored(Z)V

    .line 204
    return-void
.end method

.method public static tabUndoBarPressed()V
    .locals 0

    .prologue
    .line 199
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordTabUndoBarPressed()V

    .line 200
    return-void
.end method

.method public static tabUndoBarShown(Z)V
    .locals 0

    .prologue
    .line 195
    invoke-static {p0}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordTabUndoBarShown(Z)V

    .line 196
    return-void
.end method

.method public static toolbarFirstDrawTime(J)V
    .locals 0

    .prologue
    .line 448
    invoke-static {p0, p1}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordToolbarFirstDrawTime(J)V

    .line 449
    return-void
.end method

.method public static toolbarInflationTime(J)V
    .locals 0

    .prologue
    .line 444
    invoke-static {p0, p1}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordToolbarInflationTime(J)V

    .line 445
    return-void
.end method

.method public static toolbarPhoneTabStack()V
    .locals 0

    .prologue
    .line 111
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordToolbarPhoneTabStack()V

    .line 112
    return-void
.end method

.method public static toolbarShowMenu()V
    .locals 0

    .prologue
    .line 127
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordToolbarShowMenu()V

    .line 128
    return-void
.end method

.method public static toolbarToggleBookmark()V
    .locals 0

    .prologue
    .line 123
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordToolbarToggleBookmark()V

    .line 124
    return-void
.end method

.method public static userActionDuringTabRestore(I)V
    .locals 2

    .prologue
    const/4 v1, 0x3

    .line 249
    sget-boolean v0, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    if-ltz p0, :cond_0

    if-lt p0, v1, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 250
    :cond_1
    invoke-static {p0, v1}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->nativeRecordUserActionDuringTabRestore(II)V

    .line 252
    return-void
.end method
