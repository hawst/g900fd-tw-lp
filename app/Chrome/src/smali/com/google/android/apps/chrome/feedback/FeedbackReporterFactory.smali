.class final Lcom/google/android/apps/chrome/feedback/FeedbackReporterFactory;
.super Ljava/lang/Object;
.source "FeedbackReporterFactory.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    return-void
.end method

.method static createFeedbackReporter(Landroid/content/Context;)Lcom/google/android/apps/chrome/feedback/FeedbackReporter;
    .locals 1

    .prologue
    .line 18
    invoke-static {p0}, Lcom/google/android/apps/chrome/feedback/FeedbackReporterFactory;->useNewFeedbackEngine(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 19
    new-instance v0, Lcom/google/android/apps/chrome/feedback/PlayServicesFeedbackReporter;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/feedback/PlayServicesFeedbackReporter;-><init>(Landroid/content/Context;)V

    .line 21
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/android/apps/chrome/feedback/LegacyFeedbackReporter;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/feedback/LegacyFeedbackReporter;-><init>(Landroid/content/Context;)V

    goto :goto_0
.end method

.method private static useNewFeedbackEngine(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 26
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/externalauth/ExternalAuthUtils;->isChromeGoogleSigned(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method
