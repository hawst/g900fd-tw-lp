.class Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$LoadBookmarkNodeTask;
.super Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment$FragmentAsyncTask;
.source "AddEditBookmarkFragment.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final mBookmarkId:Ljava/lang/Long;

.field private final mContext:Landroid/content/Context;

.field private mResult:Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

.field final synthetic this$0:Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 522
    const-class v0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$LoadBookmarkNodeTask;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;Ljava/lang/Long;)V
    .locals 1

    .prologue
    .line 531
    iput-object p1, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$LoadBookmarkNodeTask;->this$0:Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment$FragmentAsyncTask;-><init>(Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment;)V

    .line 534
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$LoadBookmarkNodeTask;->mContext:Landroid/content/Context;

    .line 535
    iput-object p2, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$LoadBookmarkNodeTask;->mBookmarkId:Ljava/lang/Long;

    .line 536
    return-void
.end method


# virtual methods
.method protected onTaskFinished()V
    .locals 2

    .prologue
    .line 548
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$LoadBookmarkNodeTask;->mBookmarkId:Ljava/lang/Long;

    if-nez v0, :cond_0

    .line 549
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$LoadBookmarkNodeTask;->this$0:Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;

    iget-object v1, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$LoadBookmarkNodeTask;->mResult:Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    # invokes: Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->handleDefaultBookmarkNode(Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;)V
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->access$000(Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;)V

    .line 553
    :goto_0
    return-void

    .line 551
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$LoadBookmarkNodeTask;->this$0:Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;

    iget-object v1, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$LoadBookmarkNodeTask;->mResult:Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    # invokes: Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->handleGetBookmarkNode(Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;)V
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->access$100(Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;)V

    goto :goto_0
.end method

.method protected runBackgroundTask()V
    .locals 4

    .prologue
    .line 540
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$LoadBookmarkNodeTask;->mBookmarkId:Ljava/lang/Long;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$LoadBookmarkNodeTask;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lorg/chromium/chrome/browser/ChromeBrowserProviderClient;->getDefaultBookmarkFolder(Landroid/content/Context;)Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$LoadBookmarkNodeTask;->mResult:Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    .line 544
    return-void

    .line 540
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$LoadBookmarkNodeTask;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$LoadBookmarkNodeTask;->mBookmarkId:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const/4 v1, 0x1

    invoke-static {v0, v2, v3, v1}, Lorg/chromium/chrome/browser/ChromeBrowserProviderClient;->getBookmarkNode(Landroid/content/Context;JI)Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    move-result-object v0

    goto :goto_0
.end method

.method protected setDependentUIEnabled(Z)V
    .locals 2

    .prologue
    .line 557
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$LoadBookmarkNodeTask;->this$0:Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;

    # getter for: Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mOkButton:Landroid/widget/Button;
    invoke-static {v0}, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->access$200(Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 558
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$LoadBookmarkNodeTask;->this$0:Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;

    # getter for: Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mFolderInput:Landroid/widget/Button;
    invoke-static {v0}, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->access$300(Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 560
    sget-object v0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$4;->$SwitchMap$com$google$android$apps$chrome$bookmark$AddEditBookmarkFragment$Mode:[I

    iget-object v1, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$LoadBookmarkNodeTask;->this$0:Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;

    # getter for: Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mActivityMode:Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$Mode;
    invoke-static {v1}, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->access$400(Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;)Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$Mode;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$Mode;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 576
    sget-boolean v0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$LoadBookmarkNodeTask;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 566
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$LoadBookmarkNodeTask;->this$0:Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;

    # getter for: Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mTitleInput:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->access$500(Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 567
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$LoadBookmarkNodeTask;->this$0:Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;

    # getter for: Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mUrlInput:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->access$600(Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 568
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$LoadBookmarkNodeTask;->this$0:Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;

    # getter for: Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mRemoveButton:Landroid/widget/Button;
    invoke-static {v0}, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->access$700(Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 570
    if-nez p1, :cond_0

    .line 571
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$LoadBookmarkNodeTask;->this$0:Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;

    # getter for: Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mUrlInput:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->access$600(Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;)Landroid/widget/EditText;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$string;->loading_bookmark:I

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(I)V

    .line 572
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$LoadBookmarkNodeTask;->this$0:Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;

    # getter for: Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->mTitleInput:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;->access$500(Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment;)Landroid/widget/EditText;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$string;->loading_bookmark:I

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(I)V

    .line 578
    :cond_0
    :pswitch_1
    return-void

    .line 560
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
