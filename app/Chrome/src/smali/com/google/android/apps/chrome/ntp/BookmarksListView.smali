.class public Lcom/google/android/apps/chrome/ntp/BookmarksListView;
.super Landroid/widget/ListView;
.source "BookmarksListView.java"


# instance fields
.field private mDragPointBottomY:I

.field private mDragPointTopY:I

.field private mDropListener:Lcom/google/android/apps/chrome/ntp/BookmarksListView$DropListener;

.field private mExpandedItem:I

.field private mFadingShadow:Lcom/google/android/apps/chrome/widget/FadingShadow;

.field private mIsScrolling:Z

.field private mItemHeightExpanded:I

.field private mItemHeightNormal:I

.field private mLowerBound:I

.field private mSrcDragPos:I

.field private mTempRect:Landroid/graphics/Rect;

.field private mTimerRunnable:Ljava/lang/Runnable;

.field private final mTouchSlop:I

.field private mTouchY:I

.field private mUpperBound:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 75
    invoke-direct {p0, p1, p2}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 58
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksListView;->mTempRect:Landroid/graphics/Rect;

    .line 67
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksListView;->mExpandedItem:I

    .line 69
    iput-boolean v2, p0, Lcom/google/android/apps/chrome/ntp/BookmarksListView;->mIsScrolling:Z

    .line 76
    new-instance v0, Lcom/google/android/apps/chrome/widget/FadingShadow;

    const/high16 v1, 0x11000000

    invoke-direct {v0, v1}, Lcom/google/android/apps/chrome/widget/FadingShadow;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksListView;->mFadingShadow:Lcom/google/android/apps/chrome/widget/FadingShadow;

    .line 77
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ntp/BookmarksListView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$dimen;->ntp_shadow_height:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ntp/BookmarksListView;->setFadingEdgeLength(I)V

    .line 78
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksListView;->mTouchSlop:I

    .line 79
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ntp/BookmarksListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 80
    invoke-virtual {p0, v2}, Lcom/google/android/apps/chrome/ntp/BookmarksListView;->setDividerHeight(I)V

    .line 81
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/ntp/BookmarksListView;)Z
    .locals 1

    .prologue
    .line 32
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksListView;->mIsScrolling:Z

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/ntp/BookmarksListView;)I
    .locals 1

    .prologue
    .line 32
    iget v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksListView;->mTouchY:I

    return v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/ntp/BookmarksListView;)I
    .locals 1

    .prologue
    .line 32
    iget v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksListView;->mLowerBound:I

    return v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/chrome/ntp/BookmarksListView;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/google/android/apps/chrome/ntp/BookmarksListView;->stopScrolling()V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/apps/chrome/ntp/BookmarksListView;)I
    .locals 1

    .prologue
    .line 32
    iget v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksListView;->mUpperBound:I

    return v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/chrome/ntp/BookmarksListView;)I
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/google/android/apps/chrome/ntp/BookmarksListView;->getItemToExpand()I

    move-result v0

    return v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/chrome/ntp/BookmarksListView;I)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/ntp/BookmarksListView;->expandView(I)V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/apps/chrome/ntp/BookmarksListView;I)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/ntp/BookmarksListView;->translateView(I)V

    return-void
.end method

.method private expandView(I)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 184
    iput p1, p0, Lcom/google/android/apps/chrome/ntp/BookmarksListView;->mExpandedItem:I

    move v1, v2

    .line 188
    :goto_0
    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/ntp/BookmarksListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/ntp/BookmarkItemLayout;

    .line 189
    if-eqz v0, :cond_1

    .line 190
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ntp/BookmarkItemLayout;->getBookmarkItemView()Lcom/google/android/apps/chrome/ntp/BookmarkItemView;

    move-result-object v5

    .line 193
    const/4 v3, -0x2

    .line 195
    iget v4, p0, Lcom/google/android/apps/chrome/ntp/BookmarksListView;->mSrcDragPos:I

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ntp/BookmarksListView;->getFirstVisiblePosition()I

    move-result v6

    sub-int/2addr v4, v6

    if-ne v1, v4, :cond_0

    .line 200
    const/4 v4, 0x1

    .line 201
    const/4 v3, 0x4

    .line 204
    :goto_1
    invoke-virtual {v0, v4}, Lcom/google/android/apps/chrome/ntp/BookmarkItemLayout;->setHeight(I)V

    .line 209
    const/4 v0, 0x0

    invoke-virtual {v5, v0}, Lcom/google/android/apps/chrome/ntp/BookmarkItemView;->setTranslationY(F)V

    .line 210
    invoke-virtual {v5, v3}, Lcom/google/android/apps/chrome/ntp/BookmarkItemView;->setVisibility(I)V

    .line 187
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 202
    :cond_0
    if-ne v1, p1, :cond_2

    .line 203
    iget v3, p0, Lcom/google/android/apps/chrome/ntp/BookmarksListView;->mItemHeightExpanded:I

    move v4, v3

    move v3, v2

    goto :goto_1

    .line 212
    :cond_1
    return-void

    :cond_2
    move v4, v3

    move v3, v2

    goto :goto_1
.end method

.method private getDragAmount(I)F
    .locals 3

    .prologue
    .line 219
    iget v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksListView;->mExpandedItem:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ntp/BookmarksListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/ntp/BookmarkItemLayout;

    .line 220
    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 221
    :goto_0
    return v0

    :cond_0
    int-to-float v1, p1

    iget v2, p0, Lcom/google/android/apps/chrome/ntp/BookmarksListView;->mDragPointTopY:I

    int-to-float v2, v2

    sub-float/2addr v1, v2

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ntp/BookmarkItemLayout;->getTop()I

    move-result v0

    int-to-float v0, v0

    sub-float v0, v1, v0

    iget v1, p0, Lcom/google/android/apps/chrome/ntp/BookmarksListView;->mItemHeightNormal:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    goto :goto_0
.end method

.method private getItemToExpand()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 311
    iget v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksListView;->mTouchY:I

    iget v2, p0, Lcom/google/android/apps/chrome/ntp/BookmarksListView;->mDragPointTopY:I

    sub-int/2addr v0, v2

    .line 312
    iget v2, p0, Lcom/google/android/apps/chrome/ntp/BookmarksListView;->mTouchY:I

    iget v3, p0, Lcom/google/android/apps/chrome/ntp/BookmarksListView;->mDragPointBottomY:I

    add-int/2addr v2, v3

    .line 313
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ntp/BookmarksListView;->getHeight()I

    move-result v3

    if-lt v2, v3, :cond_0

    .line 314
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ntp/BookmarksListView;->getHeight()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    .line 316
    :cond_0
    if-gez v0, :cond_1

    move v0, v1

    .line 319
    :cond_1
    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/chrome/ntp/BookmarksListView;->pointToPosition(II)I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ntp/BookmarksListView;->getFirstVisiblePosition()I

    move-result v3

    sub-int/2addr v0, v3

    .line 320
    invoke-virtual {p0, v1, v2}, Lcom/google/android/apps/chrome/ntp/BookmarksListView;->pointToPosition(II)I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ntp/BookmarksListView;->getFirstVisiblePosition()I

    move-result v2

    sub-int/2addr v1, v2

    .line 322
    iget v2, p0, Lcom/google/android/apps/chrome/ntp/BookmarksListView;->mSrcDragPos:I

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ntp/BookmarksListView;->getFirstVisiblePosition()I

    move-result v3

    sub-int/2addr v2, v3

    if-ne v0, v2, :cond_2

    .line 323
    add-int/lit8 v0, v0, 0x1

    .line 325
    :cond_2
    iget v2, p0, Lcom/google/android/apps/chrome/ntp/BookmarksListView;->mSrcDragPos:I

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ntp/BookmarksListView;->getFirstVisiblePosition()I

    move-result v3

    sub-int/2addr v2, v3

    if-ne v1, v2, :cond_3

    .line 326
    add-int/lit8 v1, v1, -0x1

    .line 328
    :cond_3
    iget v2, p0, Lcom/google/android/apps/chrome/ntp/BookmarksListView;->mExpandedItem:I

    if-ge v2, v1, :cond_4

    .line 331
    :goto_0
    return v1

    :cond_4
    move v1, v0

    goto :goto_0
.end method

.method private stopScrolling()V
    .locals 1

    .prologue
    .line 339
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksListView;->mIsScrolling:Z

    .line 340
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksListView;->mTimerRunnable:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ntp/BookmarksListView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 341
    return-void
.end method

.method private translateView(I)V
    .locals 10

    .prologue
    const/4 v1, 0x0

    const-wide/high16 v8, 0x3fd0000000000000L    # 0.25

    const-wide/high16 v6, 0x3fe8000000000000L    # 0.75

    .line 231
    iget v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksListView;->mExpandedItem:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ntp/BookmarksListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/ntp/BookmarkItemLayout;

    .line 232
    if-nez v0, :cond_0

    .line 244
    :goto_0
    return-void

    .line 233
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/ntp/BookmarksListView;->getDragAmount(I)F

    move-result v2

    .line 235
    float-to-double v4, v2

    cmpl-double v3, v4, v8

    if-lez v3, :cond_2

    float-to-double v4, v2

    cmpg-double v3, v4, v6

    if-gez v3, :cond_2

    .line 236
    float-to-double v2, v2

    sub-double v2, v6, v2

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    mul-double/2addr v2, v4

    double-to-float v1, v2

    .line 242
    :cond_1
    :goto_1
    iget v2, p0, Lcom/google/android/apps/chrome/ntp/BookmarksListView;->mItemHeightNormal:I

    int-to-float v2, v2

    mul-float/2addr v1, v2

    .line 243
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ntp/BookmarkItemLayout;->getBookmarkItemView()Lcom/google/android/apps/chrome/ntp/BookmarkItemView;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/ntp/BookmarkItemView;->setTranslationY(F)V

    goto :goto_0

    .line 237
    :cond_2
    float-to-double v4, v2

    cmpg-double v3, v4, v8

    if-gtz v3, :cond_3

    .line 238
    const/high16 v1, 0x3f800000    # 1.0f

    goto :goto_1

    .line 239
    :cond_3
    float-to-double v2, v2

    cmpl-double v2, v2, v6

    if-ltz v2, :cond_1

    goto :goto_1
.end method


# virtual methods
.method public awakenScrollBars()Z
    .locals 1

    .prologue
    .line 97
    invoke-super {p0}, Landroid/widget/ListView;->awakenScrollBars()Z

    move-result v0

    return v0
.end method

.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 85
    invoke-super {p0, p1}, Landroid/widget/ListView;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 86
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ntp/BookmarksListView;->setVerticalFadingEdgeEnabled(Z)V

    .line 87
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ntp/BookmarksListView;->getTopFadingEdgeStrength()F

    move-result v5

    .line 88
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ntp/BookmarksListView;->getVerticalFadingEdgeLength()I

    move-result v0

    int-to-float v4, v0

    .line 89
    invoke-virtual {p0, v3}, Lcom/google/android/apps/chrome/ntp/BookmarksListView;->setVerticalFadingEdgeEnabled(Z)V

    .line 90
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksListView;->mFadingShadow:Lcom/google/android/apps/chrome/widget/FadingShadow;

    move-object v1, p0

    move-object v2, p1

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/chrome/widget/FadingShadow;->drawShadow(Landroid/view/View;Landroid/graphics/Canvas;IFF)V

    .line 92
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 103
    invoke-super {p0, p1}, Landroid/widget/ListView;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 249
    invoke-super {p0, p1}, Landroid/widget/ListView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public setDropListener(Lcom/google/android/apps/chrome/ntp/BookmarksListView$DropListener;)V
    .locals 0

    .prologue
    .line 469
    iput-object p1, p0, Lcom/google/android/apps/chrome/ntp/BookmarksListView;->mDropListener:Lcom/google/android/apps/chrome/ntp/BookmarksListView$DropListener;

    .line 470
    return-void
.end method
