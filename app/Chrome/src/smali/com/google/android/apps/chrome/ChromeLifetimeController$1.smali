.class Lcom/google/android/apps/chrome/ChromeLifetimeController$1;
.super Ljava/lang/Object;
.source "ChromeLifetimeController.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/ChromeLifetimeController;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/ChromeLifetimeController;)V
    .locals 0

    .prologue
    .line 82
    iput-object p1, p0, Lcom/google/android/apps/chrome/ChromeLifetimeController$1;->this$0:Lcom/google/android/apps/chrome/ChromeLifetimeController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 85
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeLifetimeController$1;->this$0:Lcom/google/android/apps/chrome/ChromeLifetimeController;

    # getter for: Lcom/google/android/apps/chrome/ChromeLifetimeController;->mRestartChromeOnDestroy:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/ChromeLifetimeController;->access$000(Lcom/google/android/apps/chrome/ChromeLifetimeController;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeLifetimeController$1;->this$0:Lcom/google/android/apps/chrome/ChromeLifetimeController;

    # getter for: Lcom/google/android/apps/chrome/ChromeLifetimeController;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ChromeLifetimeController;->access$100(Lcom/google/android/apps/chrome/ChromeLifetimeController;)Landroid/content/Context;

    move-result-object v0

    # invokes: Lcom/google/android/apps/chrome/ChromeLifetimeController;->scheduleRestart(Landroid/content/Context;)V
    invoke-static {v0}, Lcom/google/android/apps/chrome/ChromeLifetimeController;->access$200(Landroid/content/Context;)V

    .line 87
    :cond_0
    const-string/jumbo v0, "ChromeLifetimeController"

    const-string/jumbo v1, "Forcefully killing process..."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 88
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v0

    invoke-static {v0}, Landroid/os/Process;->killProcess(I)V

    .line 90
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeLifetimeController$1;->this$0:Lcom/google/android/apps/chrome/ChromeLifetimeController;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/apps/chrome/ChromeLifetimeController;->mRestartChromeOnDestroy:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/ChromeLifetimeController;->access$002(Lcom/google/android/apps/chrome/ChromeLifetimeController;Z)Z

    .line 91
    return-void
.end method
