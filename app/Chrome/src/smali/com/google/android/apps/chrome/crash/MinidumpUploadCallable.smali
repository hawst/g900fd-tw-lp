.class public Lcom/google/android/apps/chrome/crash/MinidumpUploadCallable;
.super Ljava/lang/Object;
.source "MinidumpUploadCallable.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# static fields
.field protected static final CONTENT_TYPE_TMPL:Ljava/lang/String; = "multipart/form-data; boundary=%s"

.field protected static final CRASH_URL_STRING:Ljava/lang/String; = "https://clients2.google.com/cr/report"


# instance fields
.field private final mClientFactory:Lcom/google/android/apps/chrome/utilities/HttpClientFactory;

.field private final mFileToUpload:Ljava/io/File;

.field private final mLogfile:Ljava/io/File;

.field private final mPermManager:Lcom/google/android/apps/chrome/crash/ReportingPermissionManager;


# direct methods
.method public constructor <init>(Ljava/io/File;Ljava/io/File;Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 51
    new-instance v0, Lcom/google/android/apps/chrome/utilities/HttpClientFactoryImpl;

    invoke-direct {v0, p3}, Lcom/google/android/apps/chrome/utilities/HttpClientFactoryImpl;-><init>(Landroid/content/Context;)V

    new-instance v1, Lcom/google/android/apps/chrome/crash/ReportingPermissionManagerImpl;

    invoke-direct {v1, p3}, Lcom/google/android/apps/chrome/crash/ReportingPermissionManagerImpl;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/google/android/apps/chrome/crash/MinidumpUploadCallable;-><init>(Ljava/io/File;Ljava/io/File;Lcom/google/android/apps/chrome/utilities/HttpClientFactory;Lcom/google/android/apps/chrome/crash/ReportingPermissionManager;)V

    .line 55
    return-void
.end method

.method public constructor <init>(Ljava/io/File;Ljava/io/File;Lcom/google/android/apps/chrome/utilities/HttpClientFactory;Lcom/google/android/apps/chrome/crash/ReportingPermissionManager;)V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    iput-object p1, p0, Lcom/google/android/apps/chrome/crash/MinidumpUploadCallable;->mFileToUpload:Ljava/io/File;

    .line 63
    iput-object p2, p0, Lcom/google/android/apps/chrome/crash/MinidumpUploadCallable;->mLogfile:Ljava/io/File;

    .line 64
    iput-object p3, p0, Lcom/google/android/apps/chrome/crash/MinidumpUploadCallable;->mClientFactory:Lcom/google/android/apps/chrome/utilities/HttpClientFactory;

    .line 65
    iput-object p4, p0, Lcom/google/android/apps/chrome/crash/MinidumpUploadCallable;->mPermManager:Lcom/google/android/apps/chrome/crash/ReportingPermissionManager;

    .line 66
    return-void
.end method

.method private appendUploadedEntryToLog(Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 159
    new-instance v1, Ljava/io/FileWriter;

    iget-object v0, p0, Lcom/google/android/apps/chrome/crash/MinidumpUploadCallable;->mLogfile:Ljava/io/File;

    const/4 v2, 0x1

    invoke-direct {v1, v0, v2}, Ljava/io/FileWriter;-><init>(Ljava/io/File;Z)V

    .line 163
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 164
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 165
    const-string/jumbo v2, ","

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 166
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 167
    const/16 v2, 0xa

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 171
    :try_start_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 173
    invoke-virtual {v1}, Ljava/io/FileWriter;->close()V

    .line 174
    return-void

    .line 173
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Ljava/io/FileWriter;->close()V

    throw v0
.end method

.method private cleanupMinidumpFile()V
    .locals 3

    .prologue
    .line 207
    iget-object v0, p0, Lcom/google/android/apps/chrome/crash/MinidumpUploadCallable;->mFileToUpload:Ljava/io/File;

    invoke-static {v0}, Lcom/google/android/apps/chrome/crash/CrashFileManager;->tryMarkAsUploaded(Ljava/io/File;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 208
    const-string/jumbo v0, "MinidumpUploadCallable"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Unable to mark "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/apps/chrome/crash/MinidumpUploadCallable;->mFileToUpload:Ljava/io/File;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " as uploaded."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 209
    iget-object v0, p0, Lcom/google/android/apps/chrome/crash/MinidumpUploadCallable;->mFileToUpload:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v0

    if-nez v0, :cond_0

    .line 210
    const-string/jumbo v0, "MinidumpUploadCallable"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Cannot delete "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/apps/chrome/crash/MinidumpUploadCallable;->mFileToUpload:Ljava/io/File;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 213
    :cond_0
    return-void
.end method

.method private createHttpPostRequest()Lorg/apache/http/client/methods/HttpPost;
    .locals 7

    .prologue
    .line 101
    invoke-direct {p0}, Lcom/google/android/apps/chrome/crash/MinidumpUploadCallable;->readBoundary()Ljava/lang/String;

    move-result-object v1

    .line 102
    if-nez v1, :cond_0

    .line 103
    const/4 v0, 0x0

    .line 109
    :goto_0
    return-object v0

    .line 107
    :cond_0
    new-instance v0, Lorg/apache/http/client/methods/HttpPost;

    const-string/jumbo v2, "https://clients2.google.com/cr/report"

    invoke-direct {v0, v2}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 108
    new-instance v2, Lorg/apache/http/entity/FileEntity;

    iget-object v3, p0, Lcom/google/android/apps/chrome/crash/MinidumpUploadCallable;->mFileToUpload:Ljava/io/File;

    const-string/jumbo v4, "multipart/form-data; boundary=%s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v1, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v3, v1}, Lorg/apache/http/entity/FileEntity;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    goto :goto_0
.end method

.method private static getResponseContentAsString(Lorg/apache/http/HttpResponse;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 235
    const/4 v0, 0x0

    .line 236
    invoke-interface {p0}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v1

    .line 237
    if-eqz v1, :cond_0

    .line 238
    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 239
    invoke-interface {v1, v2}, Lorg/apache/http/HttpEntity;->writeTo(Ljava/io/OutputStream;)V

    .line 240
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 241
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toString()Ljava/lang/String;

    move-result-object v0

    .line 244
    :cond_0
    return-object v0
.end method

.method private handleExecutionResponse(Lorg/apache/http/HttpResponse;)Ljava/lang/Boolean;
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 120
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v0

    .line 121
    invoke-static {v0}, Lcom/google/android/apps/chrome/crash/MinidumpUploadCallable;->isSuccessful(Lorg/apache/http/StatusLine;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 122
    invoke-static {p1}, Lcom/google/android/apps/chrome/crash/MinidumpUploadCallable;->getResponseContentAsString(Lorg/apache/http/HttpResponse;)Ljava/lang/String;

    move-result-object v0

    .line 124
    if-eqz v0, :cond_0

    .line 125
    :goto_0
    const-string/jumbo v1, "MinidumpUploadCallable"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Minidump "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/apps/chrome/crash/MinidumpUploadCallable;->mFileToUpload:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " uploaded successfully, id: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 130
    invoke-direct {p0}, Lcom/google/android/apps/chrome/crash/MinidumpUploadCallable;->cleanupMinidumpFile()V

    .line 133
    :try_start_0
    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/crash/MinidumpUploadCallable;->appendUploadedEntryToLog(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 137
    :goto_1
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 148
    :goto_2
    return-object v0

    .line 124
    :cond_0
    const-string/jumbo v0, "unknown"

    goto :goto_0

    .line 135
    :catch_0
    move-exception v0

    const-string/jumbo v0, "MinidumpUploadCallable"

    const-string/jumbo v1, "Fail to write uploaded entry to log file"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 141
    :cond_1
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string/jumbo v2, "Failed to upload %s with code: %d (%s)."

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/android/apps/chrome/crash/MinidumpUploadCallable;->mFileToUpload:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-interface {v0}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    const/4 v4, 0x2

    invoke-interface {v0}, Lorg/apache/http/StatusLine;->getReasonPhrase()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 144
    const-string/jumbo v1, "MinidumpUploadCallable"

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 148
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_2
.end method

.method private static isSuccessful(Lorg/apache/http/StatusLine;)Z
    .locals 2

    .prologue
    .line 223
    invoke-interface {p0}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v0

    .line 224
    const/16 v1, 0xc8

    if-eq v0, v1, :cond_0

    const/16 v1, 0xc9

    if-eq v0, v1, :cond_0

    const/16 v1, 0xca

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private readBoundary()Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 184
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v2, Ljava/io/FileReader;

    iget-object v3, p0, Lcom/google/android/apps/chrome/crash/MinidumpUploadCallable;->mFileToUpload:Ljava/io/File;

    invoke-direct {v2, v3}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V

    invoke-direct {v1, v2}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 185
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v2

    .line 186
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V

    .line 187
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 188
    :cond_0
    const-string/jumbo v1, "MinidumpUploadCallable"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Ignoring invalid crash dump: \'"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/apps/chrome/crash/MinidumpUploadCallable;->mFileToUpload:Ljava/io/File;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 197
    :goto_0
    return-object v0

    .line 191
    :cond_1
    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 192
    const-string/jumbo v2, "--"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    const/16 v3, 0xa

    if-ge v2, v3, :cond_3

    .line 193
    :cond_2
    const-string/jumbo v1, "MinidumpUploadCallable"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Ignoring invalidly bound crash dump: \'"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/apps/chrome/crash/MinidumpUploadCallable;->mFileToUpload:Ljava/io/File;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 196
    :cond_3
    const/4 v0, 0x2

    invoke-virtual {v1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public call()Ljava/lang/Boolean;
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 70
    iget-object v0, p0, Lcom/google/android/apps/chrome/crash/MinidumpUploadCallable;->mPermManager:Lcom/google/android/apps/chrome/crash/ReportingPermissionManager;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/crash/ReportingPermissionManager;->isUploadPermitted()Z

    move-result v0

    if-nez v0, :cond_1

    .line 71
    const-string/jumbo v0, "MinidumpUploadCallable"

    const-string/jumbo v1, "Minidump upload is not permitted"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 72
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 87
    :cond_0
    :goto_0
    return-object v0

    .line 74
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/crash/MinidumpUploadCallable;->mClientFactory:Lcom/google/android/apps/chrome/utilities/HttpClientFactory;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/utilities/HttpClientFactory;->newInstance()Lcom/google/android/apps/chrome/utilities/HttpClientWrapper;

    move-result-object v1

    .line 76
    :try_start_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/crash/MinidumpUploadCallable;->createHttpPostRequest()Lorg/apache/http/client/methods/HttpPost;

    move-result-object v0

    .line 77
    if-nez v0, :cond_2

    .line 78
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 87
    if-eqz v1, :cond_0

    invoke-interface {v1}, Lcom/google/android/apps/chrome/utilities/HttpClientWrapper;->close()V

    goto :goto_0

    .line 80
    :cond_2
    :try_start_1
    invoke-interface {v1, v0}, Lcom/google/android/apps/chrome/utilities/HttpClientWrapper;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 81
    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/crash/MinidumpUploadCallable;->handleExecutionResponse(Lorg/apache/http/HttpResponse;)Ljava/lang/Boolean;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 87
    if-eqz v1, :cond_0

    invoke-interface {v1}, Lcom/google/android/apps/chrome/utilities/HttpClientWrapper;->close()V

    goto :goto_0

    .line 82
    :catch_0
    move-exception v0

    .line 84
    :try_start_2
    const-string/jumbo v2, "MinidumpUploadCallable"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Error while uploading "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/apps/chrome/crash/MinidumpUploadCallable;->mFileToUpload:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 85
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    .line 87
    if-eqz v1, :cond_0

    invoke-interface {v1}, Lcom/google/android/apps/chrome/utilities/HttpClientWrapper;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    if-eqz v1, :cond_3

    invoke-interface {v1}, Lcom/google/android/apps/chrome/utilities/HttpClientWrapper;->close()V

    :cond_3
    throw v0
.end method

.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 36
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/crash/MinidumpUploadCallable;->call()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
