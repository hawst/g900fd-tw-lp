.class Lcom/google/android/apps/chrome/webapps/WebappFullscreenManager;
.super Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;
.source "WebappFullscreenManager.java"

# interfaces
.implements Lcom/google/android/apps/chrome/webapps/SlidingLayout$AnimationListener;


# instance fields
.field private final mDelegate:Lcom/google/android/apps/chrome/webapps/WebappFullscreenManager$WebappFullscreenDelegate;

.field private final mSlidingLayout:Lcom/google/android/apps/chrome/webapps/SlidingLayout;

.field private mTriggerFullscreenOnAnimationDone:Z

.field private final mUrlBarView:Landroid/view/View;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/chrome/webapps/WebappFullscreenManager$WebappFullscreenDelegate;Landroid/view/Window;Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;Lcom/google/android/apps/chrome/webapps/SlidingLayout;Landroid/view/View;)V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 56
    invoke-direct {p0, p2, p3, v0, v0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;-><init>(Landroid/view/Window;Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;ZZ)V

    .line 57
    iput-object p1, p0, Lcom/google/android/apps/chrome/webapps/WebappFullscreenManager;->mDelegate:Lcom/google/android/apps/chrome/webapps/WebappFullscreenManager$WebappFullscreenDelegate;

    .line 58
    iput-object p4, p0, Lcom/google/android/apps/chrome/webapps/WebappFullscreenManager;->mSlidingLayout:Lcom/google/android/apps/chrome/webapps/SlidingLayout;

    .line 59
    iput-object p5, p0, Lcom/google/android/apps/chrome/webapps/WebappFullscreenManager;->mUrlBarView:Landroid/view/View;

    .line 60
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/webapps/WebappFullscreenManager;)Lcom/google/android/apps/chrome/webapps/WebappFullscreenManager$WebappFullscreenDelegate;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/WebappFullscreenManager;->mDelegate:Lcom/google/android/apps/chrome/webapps/WebappFullscreenManager$WebappFullscreenDelegate;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/webapps/WebappFullscreenManager;)Lcom/google/android/apps/chrome/webapps/SlidingLayout;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/WebappFullscreenManager;->mSlidingLayout:Lcom/google/android/apps/chrome/webapps/SlidingLayout;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/webapps/WebappFullscreenManager;)Z
    .locals 1

    .prologue
    .line 19
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/webapps/WebappFullscreenManager;->mTriggerFullscreenOnAnimationDone:Z

    return v0
.end method

.method static synthetic access$202(Lcom/google/android/apps/chrome/webapps/WebappFullscreenManager;Z)Z
    .locals 0

    .prologue
    .line 19
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/webapps/WebappFullscreenManager;->mTriggerFullscreenOnAnimationDone:Z

    return p1
.end method

.method static synthetic access$300(Lcom/google/android/apps/chrome/webapps/WebappFullscreenManager;)Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;
    .locals 1

    .prologue
    .line 19
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/webapps/WebappFullscreenManager;->getTabModelSelector()Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/chrome/webapps/WebappFullscreenManager;)Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;
    .locals 1

    .prologue
    .line 19
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/webapps/WebappFullscreenManager;->getHtmlApiHandler()Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/chrome/webapps/WebappFullscreenManager;)Landroid/view/View;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/WebappFullscreenManager;->mUrlBarView:Landroid/view/View;

    return-object v0
.end method


# virtual methods
.method protected createApiDelegate()Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler$FullscreenHtmlApiDelegate;
    .locals 1

    .prologue
    .line 64
    new-instance v0, Lcom/google/android/apps/chrome/webapps/WebappFullscreenManager$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/webapps/WebappFullscreenManager$1;-><init>(Lcom/google/android/apps/chrome/webapps/WebappFullscreenManager;)V

    return-object v0
.end method

.method public hideControlsPersistent(I)V
    .locals 0

    .prologue
    .line 120
    return-void
.end method

.method public onAnimationStateChanged(I)V
    .locals 2

    .prologue
    .line 144
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/webapps/WebappFullscreenManager;->mTriggerFullscreenOnAnimationDone:Z

    if-eqz v0, :cond_0

    if-nez p1, :cond_0

    .line 146
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/webapps/WebappFullscreenManager;->getHtmlApiHandler()Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/webapps/WebappFullscreenManager;->getTabModelSelector()Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    move-result-object v1

    invoke-interface {v1}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->getCurrentTab()Lorg/chromium/chrome/browser/Tab;

    move-result-object v1

    invoke-virtual {v1}, Lorg/chromium/chrome/browser/Tab;->getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler;->enterFullscreen(Lorg/chromium/content/browser/ContentViewCore;)V

    .line 148
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/webapps/WebappFullscreenManager;->mTriggerFullscreenOnAnimationDone:Z

    .line 149
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/WebappFullscreenManager;->mSlidingLayout:Lcom/google/android/apps/chrome/webapps/SlidingLayout;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/chrome/webapps/SlidingLayout;->removeAnimationListener(Lcom/google/android/apps/chrome/webapps/SlidingLayout$AnimationListener;)V

    .line 151
    :cond_0
    return-void
.end method

.method public setOverlayVideoMode(Z)V
    .locals 1

    .prologue
    .line 129
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;->setOverlayVideoMode(Z)V

    .line 130
    iget-object v0, p0, Lcom/google/android/apps/chrome/webapps/WebappFullscreenManager;->mDelegate:Lcom/google/android/apps/chrome/webapps/WebappFullscreenManager$WebappFullscreenDelegate;

    invoke-interface {v0, p1}, Lcom/google/android/apps/chrome/webapps/WebappFullscreenManager$WebappFullscreenDelegate;->onToggleOverlayVideoMode(Z)V

    .line 131
    return-void
.end method

.method public setPositionsForTab(FF)V
    .locals 0

    .prologue
    .line 137
    return-void
.end method

.method public setPositionsForTabToNonFullscreen()V
    .locals 0

    .prologue
    .line 134
    return-void
.end method

.method public showControlsPersistentAndClearOldToken(I)I
    .locals 1

    .prologue
    .line 116
    const/4 v0, -0x1

    return v0
.end method

.method public showControlsTransient()V
    .locals 0

    .prologue
    .line 107
    return-void
.end method

.method public updateContentViewChildrenState()V
    .locals 0

    .prologue
    .line 140
    return-void
.end method
