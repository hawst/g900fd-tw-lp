.class Lcom/google/android/apps/chrome/omnibox/LocationBar$1;
.super Landroid/animation/AnimatorListenerAdapter;
.source "LocationBar.java"


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/omnibox/LocationBar;)V
    .locals 0

    .prologue
    .line 811
    iput-object p1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$1;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2

    .prologue
    const/4 v1, 0x4

    .line 814
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$1;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    # getter for: Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSecurityButtonShowAnimator:Landroid/animation/AnimatorSet;
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->access$2700(Lcom/google/android/apps/chrome/omnibox/LocationBar;)Landroid/animation/AnimatorSet;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 815
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$1;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    iget-object v0, v0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mNavigationButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 819
    :cond_0
    :goto_0
    return-void

    .line 816
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$1;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    # getter for: Lcom/google/android/apps/chrome/omnibox/LocationBar;->mNavigationIconShowAnimator:Landroid/animation/AnimatorSet;
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->access$2800(Lcom/google/android/apps/chrome/omnibox/LocationBar;)Landroid/animation/AnimatorSet;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 817
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$1;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    iget-object v0, v0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSecurityButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_0
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 823
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$1;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    # getter for: Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSecurityButtonShowAnimator:Landroid/animation/AnimatorSet;
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->access$2700(Lcom/google/android/apps/chrome/omnibox/LocationBar;)Landroid/animation/AnimatorSet;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 824
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$1;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    iget-object v0, v0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mSecurityButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 828
    :cond_0
    :goto_0
    return-void

    .line 825
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$1;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    # getter for: Lcom/google/android/apps/chrome/omnibox/LocationBar;->mNavigationIconShowAnimator:Landroid/animation/AnimatorSet;
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->access$2800(Lcom/google/android/apps/chrome/omnibox/LocationBar;)Landroid/animation/AnimatorSet;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 826
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$1;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    iget-object v0, v0, Lcom/google/android/apps/chrome/omnibox/LocationBar;->mNavigationButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method
