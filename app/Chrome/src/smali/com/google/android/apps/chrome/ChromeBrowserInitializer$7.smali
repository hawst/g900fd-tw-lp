.class Lcom/google/android/apps/chrome/ChromeBrowserInitializer$7;
.super Ljava/lang/Object;
.source "ChromeBrowserInitializer.java"

# interfaces
.implements Lorg/chromium/content/browser/BrowserStartupController$StartupCallback;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/ChromeBrowserInitializer;

.field final synthetic val$delegate:Lcom/google/android/apps/chrome/ChromeBrowserInitializer$BrowserParts;

.field final synthetic val$initQueue:Ljava/util/LinkedList;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/ChromeBrowserInitializer;Lcom/google/android/apps/chrome/ChromeBrowserInitializer$BrowserParts;Ljava/util/LinkedList;)V
    .locals 0

    .prologue
    .line 280
    iput-object p1, p0, Lcom/google/android/apps/chrome/ChromeBrowserInitializer$7;->this$0:Lcom/google/android/apps/chrome/ChromeBrowserInitializer;

    iput-object p2, p0, Lcom/google/android/apps/chrome/ChromeBrowserInitializer$7;->val$delegate:Lcom/google/android/apps/chrome/ChromeBrowserInitializer$BrowserParts;

    iput-object p3, p0, Lcom/google/android/apps/chrome/ChromeBrowserInitializer$7;->val$initQueue:Ljava/util/LinkedList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure()V
    .locals 1

    .prologue
    .line 283
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeBrowserInitializer$7;->val$delegate:Lcom/google/android/apps/chrome/ChromeBrowserInitializer$BrowserParts;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/ChromeBrowserInitializer$BrowserParts;->onStartupFailure()V

    .line 284
    return-void
.end method

.method public onSuccess(Z)V
    .locals 2

    .prologue
    .line 288
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeBrowserInitializer$7;->this$0:Lcom/google/android/apps/chrome/ChromeBrowserInitializer;

    # getter for: Lcom/google/android/apps/chrome/ChromeBrowserInitializer;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ChromeBrowserInitializer;->access$000(Lcom/google/android/apps/chrome/ChromeBrowserInitializer;)Landroid/os/Handler;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeBrowserInitializer$7;->val$initQueue:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 289
    return-void
.end method
