.class Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$1;
.super Ljava/lang/Object;
.source "TabStrip.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)V
    .locals 0

    .prologue
    .line 208
    iput-object p1, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$1;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 211
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$1;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    # getter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mNewTabButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$100(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getAlpha()F

    move-result v0

    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    .line 212
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$1;->this$0:Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;

    # getter for: Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->mKeyboardHider:Lcom/google/android/apps/chrome/KeyboardHider;
    invoke-static {v0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;->access$300(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip;)Lcom/google/android/apps/chrome/KeyboardHider;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$1$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$1$1;-><init>(Lcom/google/android/apps/chrome/widget/tabstrip/TabStrip$1;)V

    invoke-interface {v0, v1}, Lcom/google/android/apps/chrome/KeyboardHider;->hideKeyboard(Ljava/lang/Runnable;)V

    .line 218
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->tabStripNewTab()V

    .line 220
    :cond_0
    return-void
.end method
