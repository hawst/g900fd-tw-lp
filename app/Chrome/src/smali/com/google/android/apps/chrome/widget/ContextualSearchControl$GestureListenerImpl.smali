.class Lcom/google/android/apps/chrome/widget/ContextualSearchControl$GestureListenerImpl;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source "ContextualSearchControl.java"


# instance fields
.field private final mMotionStartPoint:Landroid/graphics/PointF;

.field final synthetic this$0:Lcom/google/android/apps/chrome/widget/ContextualSearchControl;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/chrome/widget/ContextualSearchControl;)V
    .locals 1

    .prologue
    .line 93
    iput-object p1, p0, Lcom/google/android/apps/chrome/widget/ContextualSearchControl$GestureListenerImpl;->this$0:Lcom/google/android/apps/chrome/widget/ContextualSearchControl;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    .line 94
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/ContextualSearchControl$GestureListenerImpl;->mMotionStartPoint:Landroid/graphics/PointF;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/chrome/widget/ContextualSearchControl;Lcom/google/android/apps/chrome/widget/ContextualSearchControl$1;)V
    .locals 0

    .prologue
    .line 93
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/widget/ContextualSearchControl$GestureListenerImpl;-><init>(Lcom/google/android/apps/chrome/widget/ContextualSearchControl;)V

    return-void
.end method


# virtual methods
.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 134
    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/ContextualSearchControl$GestureListenerImpl;->this$0:Lcom/google/android/apps/chrome/widget/ContextualSearchControl;

    # getter for: Lcom/google/android/apps/chrome/widget/ContextualSearchControl;->mSwipeHandler:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;
    invoke-static {v1}, Lcom/google/android/apps/chrome/widget/ContextualSearchControl;->access$100(Lcom/google/android/apps/chrome/widget/ContextualSearchControl;)Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;

    move-result-object v1

    if-nez v1, :cond_1

    .line 146
    :cond_0
    :goto_0
    return v0

    .line 135
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/ContextualSearchControl$GestureListenerImpl;->this$0:Lcom/google/android/apps/chrome/widget/ContextualSearchControl;

    # getter for: Lcom/google/android/apps/chrome/widget/ContextualSearchControl;->mForwardingDirection:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;
    invoke-static {v1}, Lcom/google/android/apps/chrome/widget/ContextualSearchControl;->access$300(Lcom/google/android/apps/chrome/widget/ContextualSearchControl;)Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;->UNKNOWN:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    if-eq v1, v2, :cond_0

    .line 136
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/ContextualSearchControl$GestureListenerImpl;->this$0:Lcom/google/android/apps/chrome/widget/ContextualSearchControl;

    # getter for: Lcom/google/android/apps/chrome/widget/ContextualSearchControl;->mPxToDp:F
    invoke-static {v1}, Lcom/google/android/apps/chrome/widget/ContextualSearchControl;->access$200(Lcom/google/android/apps/chrome/widget/ContextualSearchControl;)F

    move-result v1

    mul-float/2addr v1, v0

    .line 137
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v0

    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/ContextualSearchControl$GestureListenerImpl;->this$0:Lcom/google/android/apps/chrome/widget/ContextualSearchControl;

    # getter for: Lcom/google/android/apps/chrome/widget/ContextualSearchControl;->mPxToDp:F
    invoke-static {v2}, Lcom/google/android/apps/chrome/widget/ContextualSearchControl;->access$200(Lcom/google/android/apps/chrome/widget/ContextualSearchControl;)F

    move-result v2

    mul-float/2addr v2, v0

    .line 138
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    iget-object v3, p0, Lcom/google/android/apps/chrome/widget/ContextualSearchControl$GestureListenerImpl;->mMotionStartPoint:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->x:F

    sub-float/2addr v0, v3

    iget-object v3, p0, Lcom/google/android/apps/chrome/widget/ContextualSearchControl$GestureListenerImpl;->this$0:Lcom/google/android/apps/chrome/widget/ContextualSearchControl;

    # getter for: Lcom/google/android/apps/chrome/widget/ContextualSearchControl;->mPxToDp:F
    invoke-static {v3}, Lcom/google/android/apps/chrome/widget/ContextualSearchControl;->access$200(Lcom/google/android/apps/chrome/widget/ContextualSearchControl;)F

    move-result v3

    mul-float/2addr v3, v0

    .line 139
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v0

    iget-object v4, p0, Lcom/google/android/apps/chrome/widget/ContextualSearchControl$GestureListenerImpl;->mMotionStartPoint:Landroid/graphics/PointF;

    iget v4, v4, Landroid/graphics/PointF;->y:F

    sub-float/2addr v0, v4

    iget-object v4, p0, Lcom/google/android/apps/chrome/widget/ContextualSearchControl$GestureListenerImpl;->this$0:Lcom/google/android/apps/chrome/widget/ContextualSearchControl;

    # getter for: Lcom/google/android/apps/chrome/widget/ContextualSearchControl;->mPxToDp:F
    invoke-static {v4}, Lcom/google/android/apps/chrome/widget/ContextualSearchControl;->access$200(Lcom/google/android/apps/chrome/widget/ContextualSearchControl;)F

    move-result v4

    mul-float/2addr v4, v0

    .line 140
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/ContextualSearchControl$GestureListenerImpl;->this$0:Lcom/google/android/apps/chrome/widget/ContextualSearchControl;

    # getter for: Lcom/google/android/apps/chrome/widget/ContextualSearchControl;->mPxToDp:F
    invoke-static {v0}, Lcom/google/android/apps/chrome/widget/ContextualSearchControl;->access$200(Lcom/google/android/apps/chrome/widget/ContextualSearchControl;)F

    move-result v0

    mul-float v5, p3, v0

    .line 141
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/ContextualSearchControl$GestureListenerImpl;->this$0:Lcom/google/android/apps/chrome/widget/ContextualSearchControl;

    # getter for: Lcom/google/android/apps/chrome/widget/ContextualSearchControl;->mPxToDp:F
    invoke-static {v0}, Lcom/google/android/apps/chrome/widget/ContextualSearchControl;->access$200(Lcom/google/android/apps/chrome/widget/ContextualSearchControl;)F

    move-result v0

    mul-float v6, p4, v0

    .line 143
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/ContextualSearchControl$GestureListenerImpl;->this$0:Lcom/google/android/apps/chrome/widget/ContextualSearchControl;

    # getter for: Lcom/google/android/apps/chrome/widget/ContextualSearchControl;->mSwipeHandler:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;
    invoke-static {v0}, Lcom/google/android/apps/chrome/widget/ContextualSearchControl;->access$100(Lcom/google/android/apps/chrome/widget/ContextualSearchControl;)Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;

    move-result-object v0

    invoke-interface/range {v0 .. v6}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;->swipeFlingOccurred(FFFFFF)V

    .line 144
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 98
    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/ContextualSearchControl$GestureListenerImpl;->this$0:Lcom/google/android/apps/chrome/widget/ContextualSearchControl;

    # getter for: Lcom/google/android/apps/chrome/widget/ContextualSearchControl;->mSwipeHandler:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;
    invoke-static {v1}, Lcom/google/android/apps/chrome/widget/ContextualSearchControl;->access$100(Lcom/google/android/apps/chrome/widget/ContextualSearchControl;)Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;

    move-result-object v1

    if-eqz v1, :cond_0

    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 129
    :cond_0
    :goto_0
    return v0

    .line 100
    :cond_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/ContextualSearchControl$GestureListenerImpl;->this$0:Lcom/google/android/apps/chrome/widget/ContextualSearchControl;

    # getter for: Lcom/google/android/apps/chrome/widget/ContextualSearchControl;->mPxToDp:F
    invoke-static {v2}, Lcom/google/android/apps/chrome/widget/ContextualSearchControl;->access$200(Lcom/google/android/apps/chrome/widget/ContextualSearchControl;)F

    move-result v2

    mul-float/2addr v1, v2

    .line 101
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/chrome/widget/ContextualSearchControl$GestureListenerImpl;->this$0:Lcom/google/android/apps/chrome/widget/ContextualSearchControl;

    # getter for: Lcom/google/android/apps/chrome/widget/ContextualSearchControl;->mPxToDp:F
    invoke-static {v3}, Lcom/google/android/apps/chrome/widget/ContextualSearchControl;->access$200(Lcom/google/android/apps/chrome/widget/ContextualSearchControl;)F

    move-result v3

    mul-float/2addr v2, v3

    .line 103
    iget-object v3, p0, Lcom/google/android/apps/chrome/widget/ContextualSearchControl$GestureListenerImpl;->this$0:Lcom/google/android/apps/chrome/widget/ContextualSearchControl;

    # getter for: Lcom/google/android/apps/chrome/widget/ContextualSearchControl;->mForwardingDirection:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;
    invoke-static {v3}, Lcom/google/android/apps/chrome/widget/ContextualSearchControl;->access$300(Lcom/google/android/apps/chrome/widget/ContextualSearchControl;)Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    move-result-object v3

    sget-object v4, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;->UNKNOWN:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    if-ne v3, v4, :cond_2

    iget-object v3, p0, Lcom/google/android/apps/chrome/widget/ContextualSearchControl$GestureListenerImpl;->this$0:Lcom/google/android/apps/chrome/widget/ContextualSearchControl;

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/widget/ContextualSearchControl;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/chrome/widget/ContextualSearchControl$GestureListenerImpl;->this$0:Lcom/google/android/apps/chrome/widget/ContextualSearchControl;

    invoke-static {v3, v4}, Lorg/chromium/ui/UiUtils;->isKeyboardShowing(Landroid/content/Context;Landroid/view/View;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 105
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v4

    sub-float/2addr v3, v4

    iget-object v4, p0, Lcom/google/android/apps/chrome/widget/ContextualSearchControl$GestureListenerImpl;->this$0:Lcom/google/android/apps/chrome/widget/ContextualSearchControl;

    # getter for: Lcom/google/android/apps/chrome/widget/ContextualSearchControl;->mPxToDp:F
    invoke-static {v4}, Lcom/google/android/apps/chrome/widget/ContextualSearchControl;->access$200(Lcom/google/android/apps/chrome/widget/ContextualSearchControl;)F

    move-result v4

    mul-float/2addr v3, v4

    .line 106
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v5

    sub-float/2addr v4, v5

    iget-object v5, p0, Lcom/google/android/apps/chrome/widget/ContextualSearchControl$GestureListenerImpl;->this$0:Lcom/google/android/apps/chrome/widget/ContextualSearchControl;

    # getter for: Lcom/google/android/apps/chrome/widget/ContextualSearchControl;->mPxToDp:F
    invoke-static {v5}, Lcom/google/android/apps/chrome/widget/ContextualSearchControl;->access$200(Lcom/google/android/apps/chrome/widget/ContextualSearchControl;)F

    move-result v5

    mul-float/2addr v4, v5

    .line 108
    const/high16 v5, 0x40a00000    # 5.0f

    cmpg-float v4, v4, v5

    if-gez v4, :cond_2

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    const/high16 v4, 0x41200000    # 10.0f

    cmpg-float v3, v3, v4

    if-gez v3, :cond_2

    const/4 v3, 0x0

    cmpl-float v3, p4, v3

    if-lez v3, :cond_2

    .line 111
    iget-object v3, p0, Lcom/google/android/apps/chrome/widget/ContextualSearchControl$GestureListenerImpl;->this$0:Lcom/google/android/apps/chrome/widget/ContextualSearchControl;

    # getter for: Lcom/google/android/apps/chrome/widget/ContextualSearchControl;->mSwipeHandler:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;
    invoke-static {v3}, Lcom/google/android/apps/chrome/widget/ContextualSearchControl;->access$100(Lcom/google/android/apps/chrome/widget/ContextualSearchControl;)Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;

    move-result-object v3

    sget-object v4, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;->UP:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    invoke-interface {v3, v4}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;->isSwipeEnabled(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 112
    iget-object v3, p0, Lcom/google/android/apps/chrome/widget/ContextualSearchControl$GestureListenerImpl;->this$0:Lcom/google/android/apps/chrome/widget/ContextualSearchControl;

    sget-object v4, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;->UP:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    # setter for: Lcom/google/android/apps/chrome/widget/ContextualSearchControl;->mForwardingDirection:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;
    invoke-static {v3, v4}, Lcom/google/android/apps/chrome/widget/ContextualSearchControl;->access$302(Lcom/google/android/apps/chrome/widget/ContextualSearchControl;Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;)Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    .line 113
    iget-object v3, p0, Lcom/google/android/apps/chrome/widget/ContextualSearchControl$GestureListenerImpl;->this$0:Lcom/google/android/apps/chrome/widget/ContextualSearchControl;

    # getter for: Lcom/google/android/apps/chrome/widget/ContextualSearchControl;->mSwipeHandler:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;
    invoke-static {v3}, Lcom/google/android/apps/chrome/widget/ContextualSearchControl;->access$100(Lcom/google/android/apps/chrome/widget/ContextualSearchControl;)Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;

    move-result-object v3

    sget-object v4, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;->UP:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    invoke-interface {v3, v4, v1, v2}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;->swipeStarted(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;FF)V

    .line 114
    iget-object v3, p0, Lcom/google/android/apps/chrome/widget/ContextualSearchControl$GestureListenerImpl;->mMotionStartPoint:Landroid/graphics/PointF;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v4

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v5

    invoke-virtual {v3, v4, v5}, Landroid/graphics/PointF;->set(FF)V

    .line 119
    :cond_2
    iget-object v3, p0, Lcom/google/android/apps/chrome/widget/ContextualSearchControl$GestureListenerImpl;->this$0:Lcom/google/android/apps/chrome/widget/ContextualSearchControl;

    # getter for: Lcom/google/android/apps/chrome/widget/ContextualSearchControl;->mForwardingDirection:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;
    invoke-static {v3}, Lcom/google/android/apps/chrome/widget/ContextualSearchControl;->access$300(Lcom/google/android/apps/chrome/widget/ContextualSearchControl;)Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    move-result-object v3

    sget-object v4, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;->UNKNOWN:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    if-eq v3, v4, :cond_0

    .line 120
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    iget-object v3, p0, Lcom/google/android/apps/chrome/widget/ContextualSearchControl$GestureListenerImpl;->mMotionStartPoint:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->x:F

    sub-float/2addr v0, v3

    iget-object v3, p0, Lcom/google/android/apps/chrome/widget/ContextualSearchControl$GestureListenerImpl;->this$0:Lcom/google/android/apps/chrome/widget/ContextualSearchControl;

    # getter for: Lcom/google/android/apps/chrome/widget/ContextualSearchControl;->mPxToDp:F
    invoke-static {v3}, Lcom/google/android/apps/chrome/widget/ContextualSearchControl;->access$200(Lcom/google/android/apps/chrome/widget/ContextualSearchControl;)F

    move-result v3

    mul-float v5, v0, v3

    .line 121
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v0

    iget-object v3, p0, Lcom/google/android/apps/chrome/widget/ContextualSearchControl$GestureListenerImpl;->mMotionStartPoint:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    sub-float/2addr v0, v3

    iget-object v3, p0, Lcom/google/android/apps/chrome/widget/ContextualSearchControl$GestureListenerImpl;->this$0:Lcom/google/android/apps/chrome/widget/ContextualSearchControl;

    # getter for: Lcom/google/android/apps/chrome/widget/ContextualSearchControl;->mPxToDp:F
    invoke-static {v3}, Lcom/google/android/apps/chrome/widget/ContextualSearchControl;->access$200(Lcom/google/android/apps/chrome/widget/ContextualSearchControl;)F

    move-result v3

    mul-float v6, v0, v3

    .line 122
    neg-float v0, p3

    iget-object v3, p0, Lcom/google/android/apps/chrome/widget/ContextualSearchControl$GestureListenerImpl;->this$0:Lcom/google/android/apps/chrome/widget/ContextualSearchControl;

    # getter for: Lcom/google/android/apps/chrome/widget/ContextualSearchControl;->mPxToDp:F
    invoke-static {v3}, Lcom/google/android/apps/chrome/widget/ContextualSearchControl;->access$200(Lcom/google/android/apps/chrome/widget/ContextualSearchControl;)F

    move-result v3

    mul-float/2addr v3, v0

    .line 123
    neg-float v0, p4

    iget-object v4, p0, Lcom/google/android/apps/chrome/widget/ContextualSearchControl$GestureListenerImpl;->this$0:Lcom/google/android/apps/chrome/widget/ContextualSearchControl;

    # getter for: Lcom/google/android/apps/chrome/widget/ContextualSearchControl;->mPxToDp:F
    invoke-static {v4}, Lcom/google/android/apps/chrome/widget/ContextualSearchControl;->access$200(Lcom/google/android/apps/chrome/widget/ContextualSearchControl;)F

    move-result v4

    mul-float/2addr v4, v0

    .line 125
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/ContextualSearchControl$GestureListenerImpl;->this$0:Lcom/google/android/apps/chrome/widget/ContextualSearchControl;

    # getter for: Lcom/google/android/apps/chrome/widget/ContextualSearchControl;->mSwipeHandler:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;
    invoke-static {v0}, Lcom/google/android/apps/chrome/widget/ContextualSearchControl;->access$100(Lcom/google/android/apps/chrome/widget/ContextualSearchControl;)Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;

    move-result-object v0

    invoke-interface/range {v0 .. v6}, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;->swipeUpdated(FFFFFF)V

    .line 126
    const/4 v0, 0x1

    goto/16 :goto_0
.end method

.method public onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    .line 151
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/ContextualSearchControl$GestureListenerImpl;->this$0:Lcom/google/android/apps/chrome/widget/ContextualSearchControl;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/ContextualSearchControl;->getContext()Landroid/content/Context;

    move-result-object v0

    const/16 v1, 0x4b

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastImmediateNotification(Landroid/content/Context;I)V

    .line 153
    const/4 v0, 0x1

    return v0
.end method
