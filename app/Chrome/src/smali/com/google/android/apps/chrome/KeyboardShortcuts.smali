.class public Lcom/google/android/apps/chrome/KeyboardShortcuts;
.super Ljava/lang/Object;
.source "KeyboardShortcuts.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static dispatchKeyEvent(Landroid/view/KeyEvent;Lcom/google/android/apps/chrome/ChromeActivity;Z)Ljava/lang/Boolean;
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v4, 0x1

    .line 53
    invoke-virtual {p0}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    .line 54
    if-nez p2, :cond_2

    .line 55
    const/16 v2, 0x54

    if-eq v1, v2, :cond_0

    const/16 v2, 0x52

    if-ne v1, v2, :cond_1

    :cond_0
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 92
    :cond_1
    :goto_0
    return-object v0

    .line 59
    :cond_2
    sparse-switch v1, :sswitch_data_0

    goto :goto_0

    .line 87
    :sswitch_0
    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->instance(J)Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->handleVolumeKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 88
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 61
    :sswitch_1
    invoke-virtual {p0}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {p0}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v0

    if-nez v0, :cond_3

    .line 62
    sget v0, Lcom/google/android/apps/chrome/R$id;->focus_url_bar:I

    invoke-virtual {p1, v0}, Lcom/google/android/apps/chrome/ChromeActivity;->onMenuOrKeyboardAction(I)Z

    .line 66
    :cond_3
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 68
    :sswitch_2
    invoke-virtual {p0}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_4

    invoke-virtual {p0}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v0

    if-nez v0, :cond_4

    .line 69
    sget v0, Lcom/google/android/apps/chrome/R$id;->show_menu:I

    invoke-virtual {p1, v0}, Lcom/google/android/apps/chrome/ChromeActivity;->onMenuOrKeyboardAction(I)Z

    .line 71
    :cond_4
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 84
    :sswitch_3
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 59
    :sswitch_data_0
    .sparse-switch
        0x18 -> :sswitch_0
        0x19 -> :sswitch_0
        0x52 -> :sswitch_2
        0x54 -> :sswitch_1
        0xaa -> :sswitch_3
        0xab -> :sswitch_3
        0xac -> :sswitch_3
        0xad -> :sswitch_3
        0xb1 -> :sswitch_3
        0xb2 -> :sswitch_3
        0xb3 -> :sswitch_3
        0xb4 -> :sswitch_3
        0xb5 -> :sswitch_3
        0xb6 -> :sswitch_3
    .end sparse-switch
.end method

.method private static getMetaState(Landroid/view/KeyEvent;)I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 33
    invoke-virtual {p0}, Landroid/view/KeyEvent;->isCtrlPressed()Z

    move-result v0

    if-eqz v0, :cond_1

    const/high16 v0, -0x80000000

    :goto_0
    invoke-virtual {p0}, Landroid/view/KeyEvent;->isAltPressed()Z

    move-result v2

    if-eqz v2, :cond_2

    const/high16 v2, 0x40000000    # 2.0f

    :goto_1
    or-int/2addr v0, v2

    invoke-virtual {p0}, Landroid/view/KeyEvent;->isShiftPressed()Z

    move-result v2

    if-eqz v2, :cond_0

    const/high16 v1, 0x20000000

    :cond_0
    or-int/2addr v0, v1

    return v0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    move v2, v1

    goto :goto_1
.end method

.method public static onKeyDown(Landroid/view/KeyEvent;Lcom/google/android/apps/chrome/ChromeActivity;ZZ)Z
    .locals 8

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 111
    invoke-virtual {p0}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v2

    .line 112
    invoke-virtual {p0}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v3

    if-nez v3, :cond_0

    invoke-static {v2}, Landroid/view/KeyEvent;->isModifierKey(I)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 240
    :cond_0
    :goto_0
    return v0

    .line 113
    :cond_1
    invoke-virtual {p0}, Landroid/view/KeyEvent;->isCtrlPressed()Z

    move-result v3

    if-nez v3, :cond_2

    invoke-virtual {p0}, Landroid/view/KeyEvent;->isAltPressed()Z

    move-result v3

    if-nez v3, :cond_2

    const/16 v3, 0x87

    if-eq v2, v3, :cond_2

    const/16 v3, 0x6f

    if-ne v2, v3, :cond_0

    .line 119
    :cond_2
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/ChromeActivity;->getCurrentTabModel()Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v3

    .line 120
    invoke-interface {v3}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->getCount()I

    move-result v4

    .line 122
    invoke-static {p0}, Lcom/google/android/apps/chrome/KeyboardShortcuts;->getMetaState(Landroid/view/KeyEvent;)I

    move-result v5

    .line 123
    or-int v6, v2, v5

    .line 125
    sparse-switch v6, :sswitch_data_0

    .line 161
    if-eqz p2, :cond_0

    .line 162
    if-eqz p3, :cond_8

    const/high16 v7, -0x80000000

    if-ne v5, v7, :cond_8

    .line 163
    add-int/lit8 v2, v2, -0x7

    .line 164
    if-lez v2, :cond_7

    const/16 v5, 0x8

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v5

    if-gt v2, v5, :cond_7

    .line 166
    add-int/lit8 v0, v2, -0x1

    invoke-static {v3, v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelUtils;->setIndex(Lorg/chromium/chrome/browser/tabmodel/TabModel;I)V

    move v0, v1

    .line 167
    goto :goto_0

    .line 127
    :sswitch_0
    invoke-interface {v3}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->isIncognito()Z

    move-result v0

    if-eqz v0, :cond_3

    sget v0, Lcom/google/android/apps/chrome/R$id;->new_incognito_tab_menu_id:I

    :goto_1
    invoke-virtual {p1, v0}, Lcom/google/android/apps/chrome/ChromeActivity;->onMenuOrKeyboardAction(I)Z

    move v0, v1

    .line 130
    goto :goto_0

    .line 127
    :cond_3
    sget v0, Lcom/google/android/apps/chrome/R$id;->new_tab_menu_id:I

    goto :goto_1

    .line 132
    :sswitch_1
    sget v0, Lcom/google/android/apps/chrome/R$id;->new_tab_menu_id:I

    invoke-virtual {p1, v0}, Lcom/google/android/apps/chrome/ChromeActivity;->onMenuOrKeyboardAction(I)Z

    move v0, v1

    .line 133
    goto :goto_0

    .line 135
    :sswitch_2
    sget v0, Lcom/google/android/apps/chrome/R$id;->new_incognito_tab_menu_id:I

    invoke-virtual {p1, v0}, Lcom/google/android/apps/chrome/ChromeActivity;->onMenuOrKeyboardAction(I)Z

    move v0, v1

    .line 136
    goto :goto_0

    .line 139
    :sswitch_3
    const/16 v0, 0x1e

    if-ne v2, v0, :cond_5

    const-string/jumbo v0, "chrome-native://bookmarks/"

    .line 142
    :goto_2
    invoke-static {v3}, Lorg/chromium/chrome/browser/tabmodel/TabModelUtils;->getCurrentTab(Lorg/chromium/chrome/browser/tabmodel/TabList;)Lorg/chromium/chrome/browser/Tab;

    move-result-object v2

    .line 143
    if-eqz v2, :cond_6

    if-eqz p2, :cond_6

    .line 144
    new-instance v3, Lorg/chromium/content_public/browser/LoadUrlParams;

    const/4 v4, 0x2

    invoke-direct {v3, v0, v4}, Lorg/chromium/content_public/browser/LoadUrlParams;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v2, v3}, Lorg/chromium/chrome/browser/Tab;->loadUrl(Lorg/chromium/content_public/browser/LoadUrlParams;)I

    :cond_4
    :goto_3
    move v0, v1

    .line 152
    goto :goto_0

    .line 139
    :cond_5
    const-string/jumbo v0, "chrome://history/"

    goto :goto_2

    .line 147
    :cond_6
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/ChromeActivity;->getCurrentTabCreator()Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;

    move-result-object v2

    .line 148
    if-eqz v2, :cond_4

    .line 149
    sget-object v3, Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;->FROM_KEYBOARD:Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;

    invoke-virtual {v2, v0, v3}, Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;->launchUrl(Ljava/lang/String;Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;)Lcom/google/android/apps/chrome/tab/ChromeTab;

    goto :goto_3

    .line 154
    :sswitch_4
    sget v0, Lcom/google/android/apps/chrome/R$id;->show_menu:I

    invoke-virtual {p1, v0}, Lcom/google/android/apps/chrome/ChromeActivity;->onMenuOrKeyboardAction(I)Z

    move v0, v1

    .line 155
    goto/16 :goto_0

    .line 157
    :sswitch_5
    sget v0, Lcom/google/android/apps/chrome/R$id;->help_id:I

    invoke-virtual {p1, v0}, Lcom/google/android/apps/chrome/ChromeActivity;->onMenuOrKeyboardAction(I)Z

    move v0, v1

    .line 158
    goto/16 :goto_0

    .line 168
    :cond_7
    const/16 v5, 0x9

    if-ne v2, v5, :cond_8

    if-eqz v4, :cond_8

    .line 170
    add-int/lit8 v0, v4, -0x1

    invoke-static {v3, v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelUtils;->setIndex(Lorg/chromium/chrome/browser/tabmodel/TabModel;I)V

    move v0, v1

    .line 171
    goto/16 :goto_0

    .line 175
    :cond_8
    sparse-switch v6, :sswitch_data_1

    goto/16 :goto_0

    .line 221
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/ChromeActivity;->getCurrentContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    .line 222
    if-eqz v0, :cond_9

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->zoomReset()Z

    :cond_9
    move v0, v1

    .line 223
    goto/16 :goto_0

    .line 178
    :sswitch_7
    if-eqz p3, :cond_a

    if-le v4, v1, :cond_a

    .line 179
    invoke-interface {v3}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->index()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    rem-int/2addr v0, v4

    invoke-static {v3, v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelUtils;->setIndex(Lorg/chromium/chrome/browser/tabmodel/TabModel;I)V

    :cond_a
    move v0, v1

    .line 181
    goto/16 :goto_0

    .line 184
    :sswitch_8
    if-eqz p3, :cond_b

    if-le v4, v1, :cond_b

    .line 185
    invoke-interface {v3}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->index()I

    move-result v0

    add-int/2addr v0, v4

    add-int/lit8 v0, v0, -0x1

    rem-int/2addr v0, v4

    invoke-static {v3, v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelUtils;->setIndex(Lorg/chromium/chrome/browser/tabmodel/TabModel;I)V

    :cond_b
    move v0, v1

    .line 187
    goto/16 :goto_0

    .line 189
    :sswitch_9
    invoke-static {v3}, Lorg/chromium/chrome/browser/tabmodel/TabModelUtils;->closeCurrentTab(Lorg/chromium/chrome/browser/tabmodel/TabModel;)Z

    move v0, v1

    .line 190
    goto/16 :goto_0

    .line 192
    :sswitch_a
    const/16 v0, 0x2c

    invoke-static {p1, v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastImmediateNotification(Landroid/content/Context;I)V

    .line 194
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->shortcutFindInPage()V

    move v0, v1

    .line 195
    goto/16 :goto_0

    .line 198
    :sswitch_b
    sget v0, Lcom/google/android/apps/chrome/R$id;->focus_url_bar:I

    invoke-virtual {p1, v0}, Lcom/google/android/apps/chrome/ChromeActivity;->onMenuOrKeyboardAction(I)Z

    move v0, v1

    .line 199
    goto/16 :goto_0

    .line 202
    :sswitch_c
    sget v0, Lcom/google/android/apps/chrome/R$id;->bookmark_this_page_id:I

    invoke-virtual {p1, v0}, Lcom/google/android/apps/chrome/ChromeActivity;->onMenuOrKeyboardAction(I)Z

    move v0, v1

    .line 203
    goto/16 :goto_0

    .line 205
    :sswitch_d
    sget v0, Lcom/google/android/apps/chrome/R$id;->print_id:I

    invoke-virtual {p1, v0}, Lcom/google/android/apps/chrome/ChromeActivity;->onMenuOrKeyboardAction(I)Z

    move v0, v1

    .line 206
    goto/16 :goto_0

    .line 212
    :sswitch_e
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/ChromeActivity;->getCurrentContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    .line 213
    if-eqz v0, :cond_c

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->zoomIn()Z

    :cond_c
    move v0, v1

    .line 214
    goto/16 :goto_0

    .line 217
    :sswitch_f
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/ChromeActivity;->getCurrentContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    .line 218
    if-eqz v0, :cond_d

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->zoomOut()Z

    :cond_d
    move v0, v1

    .line 219
    goto/16 :goto_0

    .line 226
    :sswitch_10
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/ChromeActivity;->getCurrentTab()Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    .line 227
    if-eqz v0, :cond_e

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/Tab;->reload()V

    :cond_e
    move v0, v1

    .line 228
    goto/16 :goto_0

    .line 230
    :sswitch_11
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/ChromeActivity;->getCurrentTab()Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    .line 231
    if-eqz v0, :cond_f

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/Tab;->canGoBack()Z

    move-result v2

    if-eqz v2, :cond_f

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/Tab;->goBack()V

    :cond_f
    move v0, v1

    .line 232
    goto/16 :goto_0

    .line 234
    :sswitch_12
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/ChromeActivity;->getCurrentTab()Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    .line 235
    if-eqz v0, :cond_10

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/Tab;->canGoForward()Z

    move-result v2

    if-eqz v2, :cond_10

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/Tab;->goForward()V

    :cond_10
    move v0, v1

    .line 236
    goto/16 :goto_0

    .line 125
    :sswitch_data_0
    .sparse-switch
        -0x7fffffdc -> :sswitch_3
        -0x7fffffd6 -> :sswitch_1
        -0x7fffffd0 -> :sswitch_0
        -0x5fffffe2 -> :sswitch_3
        -0x5fffffd6 -> :sswitch_2
        -0x5fffffb4 -> :sswitch_5
        0x40000022 -> :sswitch_4
    .end sparse-switch

    .line 175
    :sswitch_data_1
    .sparse-switch
        -0x7ffffff9 -> :sswitch_6
        -0x7fffffe0 -> :sswitch_c
        -0x7fffffde -> :sswitch_a
        -0x7fffffd8 -> :sswitch_b
        -0x7fffffd4 -> :sswitch_d
        -0x7fffffd2 -> :sswitch_10
        -0x7fffffcd -> :sswitch_9
        -0x7fffffc3 -> :sswitch_7
        -0x7fffffbb -> :sswitch_f
        -0x7fffffba -> :sswitch_e
        -0x7fffffaf -> :sswitch_e
        -0x7fffffa4 -> :sswitch_8
        -0x7fffffa3 -> :sswitch_7
        -0x5fffffc3 -> :sswitch_8
        -0x5fffffba -> :sswitch_e
        -0x5fffffaf -> :sswitch_e
        0x87 -> :sswitch_10
        0xa8 -> :sswitch_e
        0xa9 -> :sswitch_f
        0xae -> :sswitch_c
        0x40000015 -> :sswitch_11
        0x40000016 -> :sswitch_12
        0x40000020 -> :sswitch_b
    .end sparse-switch
.end method
