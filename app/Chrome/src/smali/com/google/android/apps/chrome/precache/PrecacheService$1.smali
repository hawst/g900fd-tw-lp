.class Lcom/google/android/apps/chrome/precache/PrecacheService$1;
.super Landroid/content/BroadcastReceiver;
.source "PrecacheService.java"


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/precache/PrecacheService;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/precache/PrecacheService;)V
    .locals 0

    .prologue
    .line 56
    iput-object p1, p0, Lcom/google/android/apps/chrome/precache/PrecacheService$1;->this$0:Lcom/google/android/apps/chrome/precache/PrecacheService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/apps/chrome/precache/PrecacheService$1;->this$0:Lcom/google/android/apps/chrome/precache/PrecacheService;

    # getter for: Lcom/google/android/apps/chrome/precache/PrecacheService;->mIsPrecaching:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/precache/PrecacheService;->access$000(Lcom/google/android/apps/chrome/precache/PrecacheService;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/precache/PrecacheService$1;->this$0:Lcom/google/android/apps/chrome/precache/PrecacheService;

    # getter for: Lcom/google/android/apps/chrome/precache/PrecacheService;->mDeviceState:Lcom/google/android/apps/chrome/precache/DeviceState;
    invoke-static {v0}, Lcom/google/android/apps/chrome/precache/PrecacheService;->access$100(Lcom/google/android/apps/chrome/precache/PrecacheService;)Lcom/google/android/apps/chrome/precache/DeviceState;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/precache/DeviceState;->isInteractive(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/precache/PrecacheService$1;->this$0:Lcom/google/android/apps/chrome/precache/PrecacheService;

    # getter for: Lcom/google/android/apps/chrome/precache/PrecacheService;->mDeviceState:Lcom/google/android/apps/chrome/precache/DeviceState;
    invoke-static {v0}, Lcom/google/android/apps/chrome/precache/PrecacheService;->access$100(Lcom/google/android/apps/chrome/precache/PrecacheService;)Lcom/google/android/apps/chrome/precache/DeviceState;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/precache/DeviceState;->isPowerConnected(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/precache/PrecacheService$1;->this$0:Lcom/google/android/apps/chrome/precache/PrecacheService;

    # getter for: Lcom/google/android/apps/chrome/precache/PrecacheService;->mDeviceState:Lcom/google/android/apps/chrome/precache/DeviceState;
    invoke-static {v0}, Lcom/google/android/apps/chrome/precache/PrecacheService;->access$100(Lcom/google/android/apps/chrome/precache/PrecacheService;)Lcom/google/android/apps/chrome/precache/DeviceState;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/precache/DeviceState;->isWifiAvailable(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 63
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/precache/PrecacheService$1;->this$0:Lcom/google/android/apps/chrome/precache/PrecacheService;

    # invokes: Lcom/google/android/apps/chrome/precache/PrecacheService;->cancelPrecaching()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/precache/PrecacheService;->access$200(Lcom/google/android/apps/chrome/precache/PrecacheService;)V

    .line 65
    :cond_1
    return-void
.end method
