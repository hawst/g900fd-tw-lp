.class Lcom/google/android/apps/chrome/crash/MinidumpUploadRetry$Scheduler;
.super Ljava/lang/Object;
.source "MinidumpUploadRetry.java"

# interfaces
.implements Ljava/lang/Runnable;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static sThreadCheck:Lcom/google/android/apps/chrome/utilities/NonThreadSafe;


# instance fields
.field private final mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const-class v0, Lcom/google/android/apps/chrome/crash/MinidumpUploadRetry;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/crash/MinidumpUploadRetry$Scheduler;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/google/android/apps/chrome/crash/MinidumpUploadRetry$Scheduler;->mContext:Landroid/content/Context;

    .line 28
    return-void
.end method

.method synthetic constructor <init>(Landroid/content/Context;Lcom/google/android/apps/chrome/crash/MinidumpUploadRetry$1;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/crash/MinidumpUploadRetry$Scheduler;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 32
    sget-object v0, Lcom/google/android/apps/chrome/crash/MinidumpUploadRetry$Scheduler;->sThreadCheck:Lcom/google/android/apps/chrome/utilities/NonThreadSafe;

    if-nez v0, :cond_0

    .line 33
    new-instance v0, Lcom/google/android/apps/chrome/utilities/NonThreadSafe;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/utilities/NonThreadSafe;-><init>()V

    sput-object v0, Lcom/google/android/apps/chrome/crash/MinidumpUploadRetry$Scheduler;->sThreadCheck:Lcom/google/android/apps/chrome/utilities/NonThreadSafe;

    .line 36
    :cond_0
    sget-boolean v0, Lcom/google/android/apps/chrome/crash/MinidumpUploadRetry$Scheduler;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    sget-object v0, Lcom/google/android/apps/chrome/crash/MinidumpUploadRetry$Scheduler;->sThreadCheck:Lcom/google/android/apps/chrome/utilities/NonThreadSafe;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/utilities/NonThreadSafe;->calledOnValidThread()Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 37
    :cond_1
    invoke-static {}, Lorg/chromium/net/NetworkChangeNotifier;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_3

    .line 43
    :cond_2
    :goto_0
    return-void

    .line 40
    :cond_3
    # getter for: Lcom/google/android/apps/chrome/crash/MinidumpUploadRetry;->sSingleton:Lcom/google/android/apps/chrome/crash/MinidumpUploadRetry;
    invoke-static {}, Lcom/google/android/apps/chrome/crash/MinidumpUploadRetry;->access$000()Lcom/google/android/apps/chrome/crash/MinidumpUploadRetry;

    move-result-object v0

    if-nez v0, :cond_2

    .line 41
    new-instance v0, Lcom/google/android/apps/chrome/crash/MinidumpUploadRetry;

    iget-object v1, p0, Lcom/google/android/apps/chrome/crash/MinidumpUploadRetry$Scheduler;->mContext:Landroid/content/Context;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/chrome/crash/MinidumpUploadRetry;-><init>(Landroid/content/Context;Lcom/google/android/apps/chrome/crash/MinidumpUploadRetry$1;)V

    # setter for: Lcom/google/android/apps/chrome/crash/MinidumpUploadRetry;->sSingleton:Lcom/google/android/apps/chrome/crash/MinidumpUploadRetry;
    invoke-static {v0}, Lcom/google/android/apps/chrome/crash/MinidumpUploadRetry;->access$002(Lcom/google/android/apps/chrome/crash/MinidumpUploadRetry;)Lcom/google/android/apps/chrome/crash/MinidumpUploadRetry;

    goto :goto_0
.end method
