.class Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager$3;
.super Lorg/chromium/content/browser/WebContentsObserverAndroid;
.source "ContextualSearchManager.java"


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;Lorg/chromium/content_public/browser/WebContents;)V
    .locals 0

    .prologue
    .line 841
    iput-object p1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager$3;->this$0:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;

    invoke-direct {p0, p2}, Lorg/chromium/content/browser/WebContentsObserverAndroid;-><init>(Lorg/chromium/content_public/browser/WebContents;)V

    return-void
.end method


# virtual methods
.method public didFinishLoad(JLjava/lang/String;Z)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 878
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager$3;->this$0:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;

    # getter for: Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mSearchRequest:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchRequest;
    invoke-static {v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->access$200(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;)Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchRequest;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager$3;->this$0:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;

    # getter for: Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mSearchRequest:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchRequest;
    invoke-static {v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->access$200(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;)Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchRequest;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchRequest;->getHasFailed()Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 880
    :goto_0
    const-string/jumbo v3, "chrome://contextual-search-promo/promo.html"

    invoke-virtual {p3, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 881
    iget-object v1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager$3;->this$0:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;

    # invokes: Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->extractFirstRunFlowContentHeight()V
    invoke-static {v1}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->access$700(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;)V

    .line 895
    :cond_0
    :goto_1
    if-eqz v0, :cond_1

    .line 896
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager$3;->this$0:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;

    # getter for: Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mSearchContentViewCore:Lorg/chromium/content/browser/ContentViewCore;
    invoke-static {v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->access$300(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;)Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->getWebContents()Lorg/chromium/content_public/browser/WebContents;

    move-result-object v0

    invoke-interface {v0}, Lorg/chromium/content_public/browser/WebContents;->getNavigationController()Lorg/chromium/content_public/browser/NavigationController;

    move-result-object v0

    invoke-interface {v0}, Lorg/chromium/content_public/browser/NavigationController;->clearHistory()V

    .line 899
    :cond_1
    return-void

    :cond_2
    move v0, v2

    .line 878
    goto :goto_0

    .line 882
    :cond_3
    iget-object v3, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager$3;->this$0:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;

    # getter for: Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mIsShowingFirstRunFlow:Z
    invoke-static {v3}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->access$800(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string/jumbo v3, "chrome://contextual-search-promo/"

    invoke-virtual {p3, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 884
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager$3;->this$0:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;

    # setter for: Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mIsShowingFirstRunFlow:Z
    invoke-static {v0, v2}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->access$802(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;Z)Z

    move v0, v1

    .line 885
    goto :goto_1
.end method

.method public didNavigateMainFrame(Ljava/lang/String;Ljava/lang/String;ZZI)V
    .locals 2

    .prologue
    .line 846
    const-string/jumbo v0, "chrome://contextual-search-promo/"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 847
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager$3;->this$0:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;

    # invokes: Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->onFirstRunNavigation(Ljava/lang/String;)V
    invoke-static {v0, p1}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->access$100(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;Ljava/lang/String;)V

    .line 873
    :cond_0
    :goto_0
    return-void

    .line 848
    :cond_1
    if-lez p5, :cond_2

    const/16 v0, 0x190

    if-lt p5, v0, :cond_4

    .line 850
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager$3;->this$0:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;

    # getter for: Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mSearchRequest:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchRequest;
    invoke-static {v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->access$200(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;)Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchRequest;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchRequest;->isUsingLowPriority()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 852
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager$3;->this$0:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;

    # getter for: Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mSearchContentViewCore:Lorg/chromium/content/browser/ContentViewCore;
    invoke-static {v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->access$300(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;)Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->getWebContents()Lorg/chromium/content_public/browser/WebContents;

    move-result-object v0

    invoke-interface {v0}, Lorg/chromium/content_public/browser/WebContents;->stop()V

    .line 853
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager$3;->this$0:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;

    # getter for: Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mSearchRequest:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchRequest;
    invoke-static {v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->access$200(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;)Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchRequest;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchRequest;->setHasFailed()V

    .line 854
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager$3;->this$0:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;

    # getter for: Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mSearchRequest:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchRequest;
    invoke-static {v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->access$200(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;)Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchRequest;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchRequest;->setNormalPriority()V

    .line 856
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager$3;->this$0:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;

    # getter for: Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mIsSearchContentViewShowing:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->access$400(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 857
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager$3;->this$0:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;

    iget-object v1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager$3;->this$0:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;

    # getter for: Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mSearchRequest:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchRequest;
    invoke-static {v1}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->access$200(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;)Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchRequest;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchRequest;->getSearchUrl()Ljava/lang/String;

    move-result-object v1

    # invokes: Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->loadSearchUrl(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->access$500(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;Ljava/lang/String;)V

    goto :goto_0

    .line 859
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager$3;->this$0:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mDidLoadResolvedSearchRequest:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->access$602(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;Z)Z

    goto :goto_0

    .line 862
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager$3;->this$0:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;

    # getter for: Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mSearchRequest:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchRequest;
    invoke-static {v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->access$200(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;)Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchRequest;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchRequest;->isSearchUrl(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "about:blank"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "intent:"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 870
    iget-object v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager$3;->this$0:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;

    # getter for: Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->mLayoutDelegate:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchLayoutDelegate;
    invoke-static {v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;->access$000(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager;)Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchLayoutDelegate;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->SERP_NAVIGATION:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    invoke-interface {v0, v1}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchLayoutDelegate;->maximizePanelThenPromoteToTab(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;)V

    goto :goto_0
.end method
