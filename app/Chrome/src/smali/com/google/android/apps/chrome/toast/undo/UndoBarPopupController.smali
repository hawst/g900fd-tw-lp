.class public Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupController;
.super Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;
.source "UndoBarPopupController.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final NOTIFICATIONS:[I

.field private static sUndoBarShowDurationMs:I


# instance fields
.field private final mHideRunnable:Ljava/lang/Runnable;

.field private mIsTablet:Z

.field private final mParent:Landroid/view/View;

.field private mPopup:Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupWindow;

.field private final mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

.field private final mUndoStack:Ljava/util/Stack;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 48
    const-class v0, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupController;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupController;->$assertionsDisabled:Z

    .line 51
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupController;->NOTIFICATIONS:[I

    .line 58
    const/16 v0, 0x1388

    sput v0, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupController;->sUndoBarShowDurationMs:I

    return-void

    .line 48
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 51
    :array_0
    .array-data 4
        0x44
        0x45
        0x49
    .end array-data
.end method

.method public constructor <init>(Landroid/view/View;Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;)V
    .locals 2

    .prologue
    .line 79
    invoke-direct {p0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;-><init>()V

    .line 63
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupController;->mUndoStack:Ljava/util/Stack;

    .line 65
    new-instance v0, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupController$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupController$1;-><init>(Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupController;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupController;->mHideRunnable:Ljava/lang/Runnable;

    .line 80
    iput-object p1, p0, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupController;->mParent:Landroid/view/View;

    .line 81
    iput-object p2, p0, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupController;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    .line 82
    iget-object v0, p0, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupController;->mParent:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupController;->NOTIFICATIONS:[I

    invoke-static {v0, v1, p0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->registerForNotifications(Landroid/content/Context;[ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    .line 84
    invoke-virtual {p1}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 85
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/ui/base/DeviceFormFactor;->isTablet(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupController;->mIsTablet:Z

    .line 86
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupController;)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupController;->hideUndoBar()V

    return-void
.end method

.method private hideUndoBar()V
    .locals 2

    .prologue
    .line 168
    iget-object v0, p0, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupController;->mParent:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHandler()Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupController;->mHideRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 170
    iget-object v0, p0, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupController;->mPopup:Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupWindow;

    if-eqz v0, :cond_0

    .line 171
    iget-object v0, p0, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupController;->mPopup:Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupWindow;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupWindow;->dismiss()V

    .line 172
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupController;->mPopup:Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupWindow;

    .line 175
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupController;->mUndoStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->empty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 176
    iget-object v0, p0, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupController;->mUndoStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 177
    iget-object v1, p0, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupController;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    invoke-interface {v1, v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->getModelForTabId(I)Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v1

    .line 178
    if-eqz v1, :cond_0

    invoke-interface {v1, v0}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->commitTabClosure(I)V

    goto :goto_0

    .line 180
    :cond_1
    return-void
.end method

.method private removeUndo(I)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 143
    .line 144
    iget-object v0, p0, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupController;->mUndoStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 145
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, p1, :cond_0

    .line 146
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    .line 147
    const/4 v0, 0x1

    .line 148
    :goto_0
    if-nez v0, :cond_1

    .line 161
    :goto_1
    return-void

    .line 154
    :cond_1
    invoke-static {v1}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->tabUndoBarDismissed(Z)V

    .line 156
    iget-object v0, p0, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupController;->mUndoStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->empty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 157
    invoke-direct {p0}, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupController;->hideUndoBar()V

    goto :goto_1

    .line 159
    :cond_2
    invoke-direct {p0}, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupController;->showNextUndoBar()V

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public static setTimeoutForTesting(I)V
    .locals 0

    .prologue
    .line 251
    sput p0, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupController;->sUndoBarShowDurationMs:I

    .line 252
    return-void
.end method

.method private showNextUndoBar()V
    .locals 7

    .prologue
    const v6, 0x800033

    const/4 v2, 0x0

    .line 183
    sget-boolean v0, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupController;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupController;->mUndoStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->empty()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 185
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupController;->mParent:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHandler()Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupController;->mHideRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 186
    iget-object v0, p0, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupController;->mParent:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHandler()Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupController;->mHideRunnable:Ljava/lang/Runnable;

    sget v3, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupController;->sUndoBarShowDurationMs:I

    int-to-long v4, v3

    invoke-virtual {v0, v1, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 188
    iget-object v0, p0, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupController;->mPopup:Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupWindow;

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    move v1, v0

    .line 190
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupController;->mPopup:Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupWindow;

    if-nez v0, :cond_1

    .line 191
    new-instance v0, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupWindow;

    iget-object v3, p0, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupController;->mParent:Landroid/view/View;

    iget-object v4, p0, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupController;->mParent:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    sget v5, Lcom/google/android/apps/chrome/R$string;->undo_bar_close_message:I

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v3, p0, v4}, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupWindow;-><init>(Landroid/view/View;Landroid/view/View$OnClickListener;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupController;->mPopup:Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupWindow;

    .line 193
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupController;->mIsTablet:Z

    if-nez v0, :cond_3

    .line 195
    iget-object v0, p0, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupController;->mPopup:Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupWindow;

    iget-object v3, p0, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupController;->mParent:Landroid/view/View;

    iget-object v4, p0, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupController;->mParent:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getBottom()I

    move-result v4

    iget-object v5, p0, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupController;->mPopup:Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupWindow;

    invoke-virtual {v5}, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupWindow;->getHeight()I

    move-result v5

    sub-int/2addr v4, v5

    invoke-virtual {v0, v3, v6, v2, v4}, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupWindow;->showAtLocation(Landroid/view/View;III)V

    .line 204
    :cond_1
    :goto_1
    iget-object v2, p0, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupController;->mPopup:Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupWindow;

    iget-object v0, p0, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupController;->mUndoStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupWindow;->setUndoText(Ljava/lang/String;Z)V

    .line 205
    return-void

    :cond_2
    move v1, v2

    .line 188
    goto :goto_0

    .line 198
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupController;->mParent:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Lcom/google/android/apps/chrome/R$dimen;->undo_bar_tablet_margin:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 200
    iget-object v2, p0, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupController;->mPopup:Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupWindow;

    iget-object v3, p0, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupController;->mParent:Landroid/view/View;

    iget-object v4, p0, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupController;->mParent:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getBottom()I

    move-result v4

    iget-object v5, p0, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupController;->mPopup:Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupWindow;

    invoke-virtual {v5}, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupWindow;->getHeight()I

    move-result v5

    sub-int/2addr v4, v5

    sub-int/2addr v4, v0

    invoke-virtual {v2, v3, v6, v0, v4}, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupWindow;->showAtLocation(Landroid/view/View;III)V

    goto :goto_1
.end method

.method private showUndoBar(ILjava/lang/String;)V
    .locals 2

    .prologue
    .line 126
    iget-object v0, p0, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupController;->mPopup:Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupWindow;

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->tabUndoBarShown(Z)V

    .line 127
    iget-object v0, p0, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupController;->mUndoStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 128
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, p1, :cond_0

    .line 129
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    .line 130
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupController;->mUndoStack:Ljava/util/Stack;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v1, p2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 134
    invoke-direct {p0}, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupController;->showNextUndoBar()V

    .line 135
    return-void

    .line 126
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public destroy()V
    .locals 2

    .prologue
    .line 113
    iget-object v0, p0, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupController;->mParent:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupController;->NOTIFICATIONS:[I

    invoke-static {v0, v1, p0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->unregisterForNotifications(Landroid/content/Context;[ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    .line 115
    iget-object v0, p0, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupController;->mParent:Landroid/view/View;

    invoke-static {v0, p0}, Lorg/chromium/base/ApiCompatibilityUtils;->removeOnGlobalLayoutListener(Landroid/view/View;Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 116
    return-void
.end method

.method public handleMessage(Landroid/os/Message;)V
    .locals 3

    .prologue
    .line 225
    iget-object v0, p0, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupController;->mParent:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/device/DeviceClassManager;->enableUndo(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupController;->mParent:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/device/DeviceClassManager;->isAccessibilityModeEnabled(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/android/apps/chrome/device/DeviceClassManager;->enableAccessibilityLayout()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 242
    :cond_0
    :goto_0
    return-void

    .line 231
    :cond_1
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 240
    :pswitch_0
    sget-boolean v0, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupController;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 233
    :pswitch_1
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "tabId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string/jumbo v2, "title"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupController;->showUndoBar(ILjava/lang/String;)V

    goto :goto_0

    .line 237
    :pswitch_2
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "tabId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupController;->removeUndo(I)V

    goto :goto_0

    .line 231
    nop

    :pswitch_data_0
    .packed-switch 0x44
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 209
    iget-object v0, p0, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupController;->mUndoStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->empty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 221
    :goto_0
    return-void

    .line 211
    :cond_0
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->tabUndoBarPressed()V

    .line 212
    iget-object v0, p0, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupController;->mUndoStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 213
    iget-object v1, p0, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupController;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    invoke-interface {v1, v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->getModelForTabId(I)Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v1

    .line 214
    if-eqz v1, :cond_1

    invoke-interface {v1, v0}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->cancelTabClosure(I)V

    .line 216
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupController;->mUndoStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->empty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 217
    invoke-direct {p0}, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupController;->hideUndoBar()V

    goto :goto_0

    .line 219
    :cond_2
    invoke-direct {p0}, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupController;->showNextUndoBar()V

    goto :goto_0
.end method

.method public onGlobalLayout()V
    .locals 7

    .prologue
    const/4 v6, -0x1

    .line 90
    iget-object v0, p0, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupController;->mPopup:Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupWindow;

    if-nez v0, :cond_0

    .line 106
    :goto_0
    return-void

    .line 91
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupController;->mIsTablet:Z

    if-eqz v0, :cond_2

    .line 92
    iget-object v0, p0, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupController;->mParent:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$dimen;->undo_bar_tablet_margin:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 94
    iget-object v0, p0, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupController;->mParent:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Lcom/google/android/apps/chrome/R$dimen;->undo_bar_tablet_width:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 96
    iget-object v0, p0, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupController;->mParent:Landroid/view/View;

    invoke-static {v0}, Lorg/chromium/base/ApiCompatibilityUtils;->isLayoutRtl(Landroid/view/View;)Z

    move-result v0

    .line 97
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupController;->mParent:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v0

    sub-int/2addr v0, v2

    sub-int/2addr v0, v1

    .line 99
    :goto_1
    iget-object v3, p0, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupController;->mPopup:Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupWindow;

    iget-object v4, p0, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupController;->mParent:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getBottom()I

    move-result v4

    iget-object v5, p0, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupController;->mPopup:Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupWindow;

    invoke-virtual {v5}, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupWindow;->getHeight()I

    move-result v5

    sub-int/2addr v4, v5

    sub-int v1, v4, v1

    invoke-virtual {v3, v0, v1, v2, v6}, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupWindow;->update(IIII)V

    goto :goto_0

    .line 97
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupController;->mParent:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v0

    add-int/2addr v0, v1

    goto :goto_1

    .line 103
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupController;->mPopup:Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupWindow;

    iget-object v1, p0, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupController;->mParent:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupController;->mParent:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getBottom()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupController;->mPopup:Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupWindow;

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupWindow;->getHeight()I

    move-result v3

    sub-int/2addr v2, v3

    iget-object v3, p0, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupController;->mParent:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getWidth()I

    move-result v3

    invoke-virtual {v0, v1, v2, v3, v6}, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupWindow;->update(IIII)V

    goto :goto_0
.end method
