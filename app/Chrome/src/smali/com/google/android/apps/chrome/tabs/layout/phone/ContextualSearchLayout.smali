.class public Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;
.super Lcom/google/android/apps/chrome/tabs/layout/Layout;
.source "ContextualSearchLayout.java"

# interfaces
.implements Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;
.implements Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchLayoutDelegate;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final MINIMUM_ANIMATION_DURATION_MS:J


# instance fields
.field private mBasePageOffsetY:F

.field private mBaseTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

.field private mContextualSearchState:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;

.field private mInitialPanelHeight:F

.field private mInitialPanelTouchY:F

.field private mIsAnimatingPanel:Z

.field private mLastPanelHeight:F

.field private mLastSelectionYPx:F

.field private mManagementDelegate:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManagementDelegate;

.field private mPromoBasePageOffsetY:F

.field private final mPxToDp:F

.field private final mSearchBarHeightExpanded:F

.field private final mSearchBarHeightMaximized:F

.field private final mSearchBarHeightPeeking:F

.field private mSearchBarHeightPromo:F

.field private mShouldPromoteToTabAfterMaximizing:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 34
    const-class v0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->$assertionsDisabled:Z

    .line 59
    const/high16 v0, 0x43260000    # 166.0f

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    int-to-long v0, v0

    sput-wide v0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->MINIMUM_ANIMATION_DURATION_MS:J

    return-void

    .line 34
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;Lcom/google/android/apps/chrome/eventfilter/EventFilter;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 256
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/chrome/tabs/layout/Layout;-><init>(Landroid/content/Context;Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;Lcom/google/android/apps/chrome/eventfilter/EventFilter;Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripStacker;)V

    .line 219
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mLastSelectionYPx:F

    .line 225
    iput v6, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mBasePageOffsetY:F

    .line 231
    iput v6, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mPromoBasePageOffsetY:F

    .line 258
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    div-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mPxToDp:F

    .line 259
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$dimen;->contextual_search_bar_height:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mPxToDp:F

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mSearchBarHeightPeeking:F

    .line 261
    const/high16 v0, 0x42900000    # 72.0f

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mSearchBarHeightMaximized:F

    .line 262
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mSearchBarHeightPeeking:F

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mSearchBarHeightMaximized:F

    add-float/2addr v0, v1

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mSearchBarHeightExpanded:F

    .line 264
    const/high16 v0, 0x42200000    # 40.0f

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mSearchBarHeightPromo:F

    .line 266
    iput-object p5, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mContextualSearchState:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;

    .line 267
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->initializeUiState()V

    .line 268
    return-void
.end method

.method private animatePanelTo(FJ)V
    .locals 6

    .prologue
    .line 631
    sget-object v1, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout$Property;->PANEL_HEIGHT:Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout$Property;

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mLastPanelHeight:F

    move-object v0, p0

    move v3, p1

    move-wide v4, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->animateProperty(Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout$Property;FFJ)V

    .line 632
    return-void
.end method

.method private animatePanelToState(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;)V
    .locals 2

    .prologue
    .line 609
    const-wide/16 v0, 0xda

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->animatePanelToState(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;J)V

    .line 610
    return-void
.end method

.method private animatePanelToState(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;J)V
    .locals 1

    .prologue
    .line 620
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->getPanelHeightFromState(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;)F

    move-result v0

    .line 621
    invoke-direct {p0, v0, p3, p4}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->animatePanelTo(FJ)V

    .line 622
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->setPanelState(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;)V

    .line 623
    return-void
.end method

.method private animateProperty(Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout$Property;FFJ)V
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    .line 642
    cmp-long v0, p4, v8

    if-lez v0, :cond_1

    .line 643
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->animationIsRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 644
    invoke-virtual {p0, p0, p1}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->cancelAnimation(Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;)V

    .line 646
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mIsAnimatingPanel:Z

    move-object v1, p0

    move-object v2, p0

    move-object v3, p1

    move v4, p2

    move v5, p3

    move-wide v6, p4

    .line 647
    invoke-virtual/range {v1 .. v9}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->addToAnimation(Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJ)V

    .line 649
    :cond_1
    return-void
.end method

.method private calculateAnimationDisplacement(FF)F
    .locals 2

    .prologue
    .line 664
    mul-float v0, p1, p2

    const/high16 v1, 0x44fa0000    # 2000.0f

    div-float/2addr v0, v1

    return v0
.end method

.method private calculateAnimationDuration(FF)J
    .locals 6

    .prologue
    .line 680
    const/high16 v0, 0x44fa0000    # 2000.0f

    mul-float/2addr v0, p2

    div-float/2addr v0, p1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    int-to-long v0, v0

    sget-wide v2, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->MINIMUM_ANIMATION_DURATION_MS:J

    const-wide/16 v4, 0x15e

    invoke-static/range {v0 .. v5}, Lorg/chromium/chrome/browser/util/MathUtils;->clamp(JJJ)J

    move-result-wide v0

    return-wide v0
.end method

.method private calculateBasePageOffsetY(FLcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;)F
    .locals 4

    .prologue
    .line 1203
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mPxToDp:F

    mul-float/2addr v0, p1

    .line 1208
    invoke-direct {p0, p2}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->getPanelHeightFromState(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;)F

    move-result v1

    const/high16 v2, 0x41800000    # 16.0f

    sub-float/2addr v1, v2

    .line 1212
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->getContentHeight()F

    move-result v2

    .line 1213
    sub-float/2addr v2, v1

    .line 1214
    neg-float v0, v0

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    add-float/2addr v0, v2

    .line 1218
    const/4 v2, 0x0

    invoke-static {v0, v2}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 1221
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->isToolbarShowing()Z

    move-result v2

    if-eqz v2, :cond_0

    const/high16 v2, 0x42600000    # 56.0f

    sub-float/2addr v0, v2

    .line 1224
    :cond_0
    neg-float v1, v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 1226
    return v0
.end method

.method private createBaseLayoutTab(Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;)V
    .locals 4

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    .line 347
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    if-nez v0, :cond_1

    .line 361
    :cond_0
    :goto_0
    return-void

    .line 349
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->getCurrentTabId()I

    move-result v0

    .line 350
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 352
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    invoke-interface {v1}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->isIncognitoSelected()Z

    move-result v1

    invoke-virtual {p0, v0, v1, v2, v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->createLayoutTab(IZZZ)Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mBaseTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    .line 355
    sget-boolean v0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mBaseTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 356
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mBaseTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setScale(F)V

    .line 357
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mBaseTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setBorderScale(F)V

    .line 358
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mBaseTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setBorderAlpha(F)V

    .line 360
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mBaseTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    aput-object v1, v0, v2

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    goto :goto_0
.end method

.method private findLargestPanelStateFromHeight(F)Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;
    .locals 6

    .prologue
    .line 1145
    sget-object v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->CLOSED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    .line 1148
    invoke-static {}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->values()[Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    move-result-object v3

    array-length v4, v3

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v4, :cond_1

    aget-object v0, v3, v2

    .line 1149
    iget-object v5, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mContextualSearchState:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;

    invoke-virtual {v5, v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->isValidState(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1150
    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->getPanelHeightFromState(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;)F

    move-result v5

    cmpg-float v5, p1, v5

    if-gtz v5, :cond_0

    .line 1158
    :goto_1
    return-object v0

    .line 1148
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    move-object v0, v1

    goto :goto_1
.end method

.method private findNearestPanelStateFromHeight(F)Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;
    .locals 8

    .prologue
    .line 1167
    sget-object v3, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->CLOSED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    .line 1168
    const/high16 v1, 0x7f800000    # Float.POSITIVE_INFINITY

    .line 1171
    invoke-static {}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->values()[Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    move-result-object v5

    array-length v6, v5

    const/4 v0, 0x0

    move v4, v0

    :goto_0
    if-ge v4, v6, :cond_0

    aget-object v2, v5, v4

    .line 1172
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mContextualSearchState:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->isValidState(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1173
    invoke-direct {p0, v2}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->getPanelHeightFromState(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;)F

    move-result v0

    .line 1177
    sub-float v0, p1, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    .line 1178
    cmpg-float v7, v0, v1

    if-gez v7, :cond_1

    move-object v1, v2

    .line 1171
    :goto_1
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move-object v3, v1

    move v1, v0

    goto :goto_0

    .line 1184
    :cond_0
    return-object v3

    :cond_1
    move v0, v1

    move-object v1, v3

    goto :goto_1
.end method

.method private getBasePageOffsetY()F
    .locals 1

    .prologue
    .line 1234
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mBasePageOffsetY:F

    return v0
.end method

.method private getContentHeight()F
    .locals 2

    .prologue
    .line 1040
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->getHeight()F

    move-result v0

    .line 1046
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->isToolbarShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1047
    const/high16 v1, 0x42600000    # 56.0f

    add-float/2addr v0, v1

    .line 1049
    :cond_0
    return v0
.end method

.method private getPanelHeightFromState(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;)F
    .locals 4

    .prologue
    const/high16 v3, 0x41800000    # 16.0f

    const/4 v0, 0x0

    .line 1113
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->getContentHeight()F

    move-result v1

    .line 1116
    sget-object v2, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->UNDEFINED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    if-ne p1, v2, :cond_1

    .line 1132
    :cond_0
    :goto_0
    return v0

    .line 1118
    :cond_1
    sget-object v2, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->CLOSED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    if-eq p1, v2, :cond_0

    .line 1120
    sget-object v2, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->PEEKED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    if-ne p1, v2, :cond_2

    .line 1121
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mSearchBarHeightPeeking:F

    goto :goto_0

    .line 1122
    :cond_2
    sget-object v2, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->PROMO:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    if-ne p1, v2, :cond_3

    .line 1123
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mContextualSearchState:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->getFirstRunFlowContentHeight()F

    move-result v0

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mSearchBarHeightPromo:F

    add-float/2addr v0, v1

    goto :goto_0

    .line 1125
    :cond_3
    sget-object v2, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->EXPANDED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    if-ne p1, v2, :cond_4

    .line 1126
    const v0, 0x3f333333    # 0.7f

    mul-float/2addr v0, v1

    add-float/2addr v0, v3

    goto :goto_0

    .line 1128
    :cond_4
    sget-object v2, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->MAXIMIZED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    if-ne p1, v2, :cond_0

    .line 1129
    add-float v0, v1, v3

    goto :goto_0
.end method

.method private getPanelState()Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;
    .locals 1

    .prologue
    .line 1063
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mContextualSearchState:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->getPanelState()Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    move-result-object v0

    return-object v0
.end method

.method private getPromoBasePageOffsetY()F
    .locals 1

    .prologue
    .line 1242
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mPromoBasePageOffsetY:F

    return v0
.end method

.method private getStateCompletion(FLcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;)F
    .locals 3

    .prologue
    .line 849
    invoke-direct {p0, p2}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->getPanelHeightFromState(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;)F

    move-result v0

    .line 850
    invoke-direct {p0, p3}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->getPanelHeightFromState(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;)F

    move-result v1

    .line 851
    sub-float v2, p1, v0

    sub-float v0, v1, v0

    div-float v0, v2, v0

    .line 852
    return v0
.end method

.method private handleFling(F)V
    .locals 4

    .prologue
    .line 541
    const/high16 v0, 0x435a0000    # 218.0f

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->calculateAnimationDisplacement(FF)F

    move-result v0

    .line 542
    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mLastPanelHeight:F

    sub-float v0, v1, v0

    .line 546
    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->findNearestPanelStateFromHeight(F)Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    move-result-object v0

    .line 547
    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->getPanelHeightFromState(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;)F

    move-result v1

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mLastPanelHeight:F

    sub-float/2addr v1, v2

    .line 548
    invoke-direct {p0, p1, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->calculateAnimationDuration(FF)J

    move-result-wide v2

    .line 550
    sget-object v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->FLING:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->animatePanelToState(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;J)V

    .line 551
    return-void
.end method

.method private handleSwipeEnd()V
    .locals 4

    .prologue
    .line 523
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mIsAnimatingPanel:Z

    if-eqz v0, :cond_0

    .line 534
    :goto_0
    return-void

    .line 528
    :cond_0
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mLastPanelHeight:F

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->findNearestPanelStateFromHeight(F)Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    move-result-object v0

    .line 529
    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->getPanelHeightFromState(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;)F

    move-result v1

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mLastPanelHeight:F

    sub-float/2addr v1, v2

    .line 530
    const v2, 0x44dac000    # 1750.0f

    invoke-direct {p0, v2, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->calculateAnimationDuration(FF)J

    move-result-wide v2

    .line 533
    sget-object v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->SWIPE:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->animatePanelToState(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;J)V

    goto :goto_0
.end method

.method private handleSwipeMove(F)V
    .locals 1

    .prologue
    .line 515
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mInitialPanelHeight:F

    sub-float/2addr v0, p1

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->setClampedPanelHeight(F)V

    .line 516
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->requestUpdate()V

    .line 517
    return-void
.end method

.method private handleSwipeStart()V
    .locals 1

    .prologue
    .line 505
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mInitialPanelHeight:F

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->setClampedPanelHeight(F)V

    .line 506
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->requestUpdate()V

    .line 507
    return-void
.end method

.method private initializeUiState()V
    .locals 2

    .prologue
    const/high16 v1, 0x41800000    # 16.0f

    .line 801
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mContextualSearchState:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->setSearchBarMarginTop(F)V

    .line 802
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mContextualSearchState:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->setSearchIconPaddingLeft(F)V

    .line 803
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mContextualSearchState:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;

    const/high16 v1, 0x40000000    # 2.0f

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->setProgressBarHeight(F)V

    .line 804
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mContextualSearchState:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->setSearchBarBorderHeight(F)V

    .line 807
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mContextualSearchState:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mSearchBarHeightPeeking:F

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->setSearchBarHeight(F)V

    .line 808
    return-void
.end method

.method private isToolbarShowing()Z
    .locals 2

    .prologue
    .line 1056
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->getHeight()F

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->getHeightMinusTopControls()F

    move-result v1

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private peekPanel(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;)V
    .locals 1

    .prologue
    .line 590
    sget-object v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->PEEKED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->animatePanelToState(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;)V

    .line 591
    return-void
.end method

.method private resetLayout()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 332
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->animationIsRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 333
    sget-object v0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout$Property;->PANEL_HEIGHT:Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout$Property;

    invoke-virtual {p0, p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->cancelAnimation(Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;)V

    .line 336
    :cond_0
    iput-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    .line 337
    iput-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mBaseTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    .line 338
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mShouldPromoteToTabAfterMaximizing:Z

    .line 339
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mBasePageOffsetY:F

    .line 340
    return-void
.end method

.method private resizePanelToState(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;)V
    .locals 1

    .prologue
    .line 1091
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->getPanelHeightFromState(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;)F

    move-result v0

    .line 1092
    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->setPanelHeight(F)V

    .line 1093
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->setPanelState(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;)V

    .line 1094
    return-void
.end method

.method private setClampedPanelHeight(F)V
    .locals 2

    .prologue
    .line 1101
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mContextualSearchState:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->getMaximumState()Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->getPanelHeightFromState(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;)F

    move-result v0

    sget-object v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->PEEKED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->getPanelHeightFromState(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;)F

    move-result v1

    invoke-static {p1, v0, v1}, Lorg/chromium/chrome/browser/util/MathUtils;->clamp(FFF)F

    move-result v0

    .line 1104
    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->setPanelHeight(F)V

    .line 1105
    return-void
.end method

.method private setPanelHeight(F)V
    .locals 0

    .prologue
    .line 1081
    iput p1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mLastPanelHeight:F

    .line 1082
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->updateUiStateForHeight(F)V

    .line 1083
    return-void
.end method

.method private setPanelHeightForPromoOptInAnimation(F)V
    .locals 3

    .prologue
    .line 732
    iput p1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mLastPanelHeight:F

    .line 734
    sget-object v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->EXPANDED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    .line 735
    sget-object v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->PROMO:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    .line 737
    invoke-direct {p0, p1, v1, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->getStateCompletion(FLcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;)F

    move-result v1

    .line 738
    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->updateUiStateForPromoOptInAnimation(F)V

    .line 740
    sget-object v2, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->MAXIMIZED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    if-ne v0, v2, :cond_0

    const/high16 v0, 0x3f800000    # 1.0f

    cmpl-float v0, v1, v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 741
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mContextualSearchState:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->setMaximized(Z)V

    .line 742
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mContextualSearchState:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->getWidth()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->setWidth(F)V

    .line 743
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mContextualSearchState:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->setHeight(F)V

    .line 744
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mContextualSearchState:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->getContextualSearchPanelY()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->setOffsetY(F)V

    .line 745
    return-void

    .line 740
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setPanelState(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;)V
    .locals 1

    .prologue
    .line 1072
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mContextualSearchState:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->setPanelState(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;)V

    .line 1073
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->getPanelHeightFromState(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;)F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mInitialPanelHeight:F

    .line 1074
    return-void
.end method

.method private shouldHideContextualSearchLayout()Z
    .locals 2

    .prologue
    .line 302
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->shouldShowContextualSearchView()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->getPanelState()Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->CLOSED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private updateUiStateForAppearance(F)V
    .locals 4

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    .line 862
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mBaseTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setY(F)V

    .line 865
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mBaseTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setBrightness(F)V

    .line 868
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mContextualSearchState:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mSearchBarHeightPeeking:F

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->setSearchBarHeight(F)V

    .line 871
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mContextualSearchState:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->setSearchBarTextOpacity(F)V

    .line 874
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mContextualSearchState:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->setSearchProviderIconOpacity(F)V

    .line 878
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mContextualSearchState:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->setSearchIconOpacity(F)V

    .line 881
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mContextualSearchState:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->setProgressBarOpacity(F)V

    .line 882
    return-void
.end method

.method private updateUiStateForExpansion(F)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    .line 891
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->getBasePageOffsetY()F

    move-result v0

    invoke-static {v5, v0, p1}, Lcom/google/android/apps/chrome/utilities/ChromeMathUtils;->interpolate(FFF)F

    move-result v0

    .line 895
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mBaseTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setY(F)V

    .line 898
    const v0, 0x3f333333    # 0.7f

    invoke-static {v4, v0, p1}, Lcom/google/android/apps/chrome/utilities/ChromeMathUtils;->interpolate(FFF)F

    move-result v0

    .line 902
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mBaseTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setBrightness(F)V

    .line 905
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mSearchBarHeightPeeking:F

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mSearchBarHeightExpanded:F

    invoke-static {v0, v1, p1}, Lcom/google/android/apps/chrome/utilities/ChromeMathUtils;->interpolate(FFF)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    int-to-float v0, v0

    .line 909
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mContextualSearchState:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->setSearchBarHeight(F)V

    .line 912
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mContextualSearchState:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;

    invoke-virtual {v1, v4}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->setSearchBarTextOpacity(F)V

    .line 915
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mContextualSearchState:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->setSearchBarBorderVisible(Z)V

    .line 916
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mContextualSearchState:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;

    sub-float v2, v0, v4

    add-float/2addr v2, v4

    invoke-virtual {v1, v2}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->setSearchBarBorderY(F)V

    .line 920
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mContextualSearchState:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;

    invoke-virtual {v1, v4}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->setSearchProviderIconOpacity(F)V

    .line 924
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mContextualSearchState:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;

    invoke-virtual {v1, v5}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->setSearchIconOpacity(F)V

    .line 927
    sget-object v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->PEEKED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->getPanelHeightFromState(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;)F

    move-result v1

    .line 928
    const/high16 v2, 0x41200000    # 10.0f

    iget v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mPxToDp:F

    div-float/2addr v2, v3

    .line 929
    iget v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mLastPanelHeight:F

    sub-float v1, v3, v1

    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v1

    .line 931
    div-float/2addr v1, v2

    invoke-static {v5, v4, v1}, Lcom/google/android/apps/chrome/utilities/ChromeMathUtils;->interpolate(FFF)F

    move-result v1

    .line 932
    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mContextualSearchState:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;

    invoke-virtual {v2, v1}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->setProgressBarOpacity(F)V

    .line 933
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mContextualSearchState:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;

    const/high16 v2, 0x40000000    # 2.0f

    sub-float/2addr v0, v2

    add-float/2addr v0, v4

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->setProgressBarY(F)V

    .line 934
    return-void
.end method

.method private updateUiStateForHeight(F)V
    .locals 3

    .prologue
    .line 818
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->findLargestPanelStateFromHeight(F)Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    move-result-object v0

    .line 819
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mContextualSearchState:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->getPreviousPanelState(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;)Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    move-result-object v1

    .line 821
    invoke-direct {p0, p1, v1, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->getStateCompletion(FLcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;)F

    move-result v1

    .line 822
    sget-object v2, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->PEEKED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    if-ne v0, v2, :cond_1

    .line 823
    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->updateUiStateForAppearance(F)V

    .line 832
    :cond_0
    :goto_0
    sget-object v2, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->MAXIMIZED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    if-ne v0, v2, :cond_4

    const/high16 v0, 0x3f800000    # 1.0f

    cmpl-float v0, v1, v0

    if-nez v0, :cond_4

    const/4 v0, 0x1

    .line 833
    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mContextualSearchState:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->setMaximized(Z)V

    .line 834
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mContextualSearchState:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->getWidth()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->setWidth(F)V

    .line 835
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mContextualSearchState:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->setHeight(F)V

    .line 836
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mContextualSearchState:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->getContextualSearchPanelY()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->setOffsetY(F)V

    .line 837
    return-void

    .line 824
    :cond_1
    sget-object v2, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->EXPANDED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    if-ne v0, v2, :cond_2

    .line 825
    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->updateUiStateForExpansion(F)V

    goto :goto_0

    .line 826
    :cond_2
    sget-object v2, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->MAXIMIZED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    if-ne v0, v2, :cond_3

    .line 827
    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->updateUiStateForMaximization(F)V

    goto :goto_0

    .line 828
    :cond_3
    sget-object v2, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->PROMO:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    if-ne v0, v2, :cond_0

    .line 829
    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->updateUiStateForOpeningPromo(F)V

    goto :goto_0

    .line 832
    :cond_4
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private updateUiStateForMaximization(F)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/high16 v3, 0x3f800000    # 1.0f

    .line 943
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mBaseTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->getBasePageOffsetY()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setY(F)V

    .line 946
    const v0, 0x3f333333    # 0.7f

    const v1, 0x3e99999a    # 0.3f

    invoke-static {v0, v1, p1}, Lcom/google/android/apps/chrome/utilities/ChromeMathUtils;->interpolate(FFF)F

    move-result v0

    .line 950
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mBaseTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setBrightness(F)V

    .line 953
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mSearchBarHeightExpanded:F

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mSearchBarHeightMaximized:F

    invoke-static {v0, v1, p1}, Lcom/google/android/apps/chrome/utilities/ChromeMathUtils;->interpolate(FFF)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    int-to-float v0, v0

    .line 957
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mContextualSearchState:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->setSearchBarHeight(F)V

    .line 960
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mContextualSearchState:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->setSearchBarBorderVisible(Z)V

    .line 961
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mContextualSearchState:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;

    sub-float v2, v0, v3

    add-float/2addr v2, v3

    invoke-virtual {v1, v2}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->setSearchBarBorderY(F)V

    .line 965
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mContextualSearchState:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;

    invoke-virtual {v1, v3}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->setSearchBarTextOpacity(F)V

    .line 968
    invoke-static {v3, v4, p1}, Lcom/google/android/apps/chrome/utilities/ChromeMathUtils;->interpolate(FFF)F

    move-result v1

    .line 972
    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mContextualSearchState:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;

    invoke-virtual {v2, v1}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->setSearchProviderIconOpacity(F)V

    .line 975
    invoke-static {v4, v3, p1}, Lcom/google/android/apps/chrome/utilities/ChromeMathUtils;->interpolate(FFF)F

    move-result v1

    .line 979
    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mContextualSearchState:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;

    invoke-virtual {v2, v1}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->setSearchIconOpacity(F)V

    .line 982
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mContextualSearchState:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;

    invoke-virtual {v1, v3}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->setProgressBarOpacity(F)V

    .line 983
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mContextualSearchState:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;

    const/high16 v2, 0x40000000    # 2.0f

    sub-float/2addr v0, v2

    add-float/2addr v0, v3

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->setProgressBarY(F)V

    .line 984
    return-void
.end method

.method private updateUiStateForOpeningPromo(F)V
    .locals 6

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v5, 0x0

    .line 993
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->getPromoBasePageOffsetY()F

    move-result v0

    invoke-static {v5, v0, p1}, Lcom/google/android/apps/chrome/utilities/ChromeMathUtils;->interpolate(FFF)F

    move-result v0

    .line 997
    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mBaseTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setY(F)V

    .line 1000
    const v0, 0x3f333333    # 0.7f

    invoke-static {v1, v0, p1}, Lcom/google/android/apps/chrome/utilities/ChromeMathUtils;->interpolate(FFF)F

    move-result v0

    .line 1004
    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mBaseTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setBrightness(F)V

    .line 1007
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mSearchBarHeightPeeking:F

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mSearchBarHeightPromo:F

    invoke-static {v0, v2, p1}, Lcom/google/android/apps/chrome/utilities/ChromeMathUtils;->interpolate(FFF)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    int-to-float v2, v0

    .line 1011
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mContextualSearchState:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->setSearchBarHeight(F)V

    .line 1014
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mContextualSearchState:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->setSearchBarBorderVisible(Z)V

    .line 1017
    sget-object v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->PEEKED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->getPanelHeightFromState(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;)F

    move-result v0

    .line 1018
    const/high16 v3, 0x41200000    # 10.0f

    iget v4, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mPxToDp:F

    div-float/2addr v3, v4

    .line 1019
    iget v4, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mLastPanelHeight:F

    sub-float v0, v4, v0

    invoke-static {v0, v3}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 1020
    div-float/2addr v0, v3

    invoke-static {v1, v5, v0}, Lcom/google/android/apps/chrome/utilities/ChromeMathUtils;->interpolate(FFF)F

    move-result v0

    .line 1021
    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mContextualSearchState:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;

    invoke-virtual {v3, v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->setSearchBarTextOpacity(F)V

    .line 1024
    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mContextualSearchState:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->shouldHidePromoHeader()Z

    move-result v3

    .line 1025
    iget-object v4, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mContextualSearchState:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;

    if-eqz v3, :cond_0

    move v0, v1

    :cond_0
    invoke-virtual {v4, v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->setSearchProviderIconOpacity(F)V

    .line 1029
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mContextualSearchState:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;

    invoke-virtual {v0, v5}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->setSearchIconOpacity(F)V

    .line 1032
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mContextualSearchState:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;

    invoke-virtual {v0, v5}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->setProgressBarOpacity(F)V

    .line 1033
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mContextualSearchState:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;

    const/high16 v3, 0x40000000    # 2.0f

    sub-float/2addr v2, v3

    add-float/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->setProgressBarY(F)V

    .line 1034
    return-void
.end method

.method private updateUiStateForPromoOptInAnimation(F)V
    .locals 6

    .prologue
    const/high16 v5, 0x3f000000    # 0.5f

    const/4 v2, 0x0

    const/high16 v1, 0x3f800000    # 1.0f

    .line 754
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->getPromoBasePageOffsetY()F

    move-result v0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->getBasePageOffsetY()F

    move-result v3

    invoke-static {v0, v3, p1}, Lcom/google/android/apps/chrome/utilities/ChromeMathUtils;->interpolate(FFF)F

    move-result v0

    .line 758
    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mBaseTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    invoke-virtual {v3, v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setY(F)V

    .line 761
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mBaseTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    const v3, 0x3f333333    # 0.7f

    invoke-virtual {v0, v3}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setBrightness(F)V

    .line 764
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mSearchBarHeightPromo:F

    iget v3, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mSearchBarHeightExpanded:F

    invoke-static {v0, v3, p1}, Lcom/google/android/apps/chrome/utilities/ChromeMathUtils;->interpolate(FFF)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    int-to-float v3, v0

    .line 768
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mContextualSearchState:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->setSearchBarHeight(F)V

    .line 771
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mContextualSearchState:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;

    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->setSearchBarBorderVisible(Z)V

    .line 774
    sub-float v0, p1, v5

    invoke-static {v0, v2}, Ljava/lang/Math;->max(FF)F

    move-result v0

    div-float/2addr v0, v5

    invoke-static {v2, v1, v0}, Lcom/google/android/apps/chrome/utilities/ChromeMathUtils;->interpolate(FFF)F

    move-result v0

    .line 777
    iget-object v4, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mContextualSearchState:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;

    invoke-virtual {v4, v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->setSearchBarTextOpacity(F)V

    .line 780
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mContextualSearchState:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->shouldHidePromoHeader()Z

    move-result v0

    .line 781
    iget-object v4, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mContextualSearchState:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v4, v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->setSearchProviderIconOpacity(F)V

    .line 785
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mContextualSearchState:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->setSearchIconOpacity(F)V

    .line 788
    iget-object v4, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mContextualSearchState:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;

    cmpl-float v0, p1, v1

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {v4, v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->setProgressBarOpacity(F)V

    .line 789
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mContextualSearchState:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;

    const/high16 v2, 0x40000000    # 2.0f

    sub-float v2, v3, v2

    add-float/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->setProgressBarY(F)V

    .line 790
    return-void

    .line 781
    :cond_0
    invoke-static {v2, v1, p1}, Lcom/google/android/apps/chrome/utilities/ChromeMathUtils;->interpolate(FFF)F

    move-result v0

    goto :goto_0

    :cond_1
    move v0, v2

    .line 788
    goto :goto_1
.end method


# virtual methods
.method public animateAfterFirstRunSuccess()V
    .locals 7

    .prologue
    .line 724
    sget-object v6, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->EXPANDED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    .line 725
    invoke-direct {p0, v6}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->getPanelHeightFromState(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;)F

    move-result v3

    .line 726
    sget-object v1, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout$Property;->FIRST_RUN_PANEL_HEIGHT:Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout$Property;

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mLastPanelHeight:F

    const-wide/16 v4, 0xda

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->animateProperty(Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout$Property;FFJ)V

    .line 728
    sget-object v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->OPTIN:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    invoke-direct {p0, v6, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->setPanelState(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;)V

    .line 729
    return-void
.end method

.method public click(JFF)V
    .locals 2

    .prologue
    .line 445
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mContextualSearchState:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;

    invoke-virtual {v0, p4}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->isYCoordinateInsideBasePage(F)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 446
    sget-object v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->BASE_PAGE_TAP:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->closePanel(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;)V

    .line 459
    :goto_0
    return-void

    .line 447
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mContextualSearchState:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;

    invoke-virtual {v0, p4}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->isYCoordinateInsideSearchBar(F)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 448
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->getPanelState()Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->MAXIMIZED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    if-ne v0, v1, :cond_1

    .line 449
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mManagementDelegate:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManagementDelegate;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManagementDelegate;->promoteToTab()V

    goto :goto_0

    .line 451
    :cond_1
    sget-object v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->SEARCH_BAR_TAP:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->peekPanel(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;)V

    goto :goto_0

    .line 457
    :cond_2
    sget-object v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->UNKNOWN:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->maximizePanel(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;)V

    goto :goto_0
.end method

.method public closePanel(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;)V
    .locals 1

    .prologue
    .line 599
    sget-object v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->CLOSED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->animatePanelToState(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;)V

    .line 600
    return-void
.end method

.method public drag(JFFFF)V
    .locals 3

    .prologue
    .line 420
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mInitialPanelTouchY:F

    sub-float v0, p4, v0

    .line 422
    const/4 v1, 0x0

    cmpl-float v1, v0, v1

    if-lez v1, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->getPanelState()Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->MAXIMIZED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    if-ne v1, v2, :cond_0

    .line 427
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mManagementDelegate:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManagementDelegate;

    invoke-interface {v1}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManagementDelegate;->resetSearchContentViewScroll()V

    .line 430
    :cond_0
    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->handleSwipeMove(F)V

    .line 431
    return-void
.end method

.method public expandPanel(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;)V
    .locals 1

    .prologue
    .line 582
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mContextualSearchState:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->getIntermediaryState()Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->animatePanelToState(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;)V

    .line 583
    return-void
.end method

.method public fling(JFFFF)V
    .locals 0

    .prologue
    .line 440
    invoke-direct {p0, p6}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->handleFling(F)V

    .line 441
    return-void
.end method

.method public getContextualSearchPanelY()F
    .locals 2

    .prologue
    .line 272
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->getContentHeight()F

    move-result v0

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mLastPanelHeight:F

    sub-float/2addr v0, v1

    return v0
.end method

.method public getContextualSearchState()Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;
    .locals 1

    .prologue
    .line 397
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mContextualSearchState:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;

    return-object v0
.end method

.method public getSizingFlags()I
    .locals 1

    .prologue
    .line 307
    const/16 v0, 0x111

    return v0
.end method

.method public getTopControlsOffset(F)F
    .locals 3

    .prologue
    .line 289
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mBaseTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getY()F

    move-result v0

    const/high16 v1, -0x3da00000    # -56.0f

    const/4 v2, 0x0

    invoke-static {p1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v2

    invoke-static {v0, v1, v2}, Lorg/chromium/chrome/browser/util/MathUtils;->clamp(FFF)F

    move-result v0

    return v0
.end method

.method public handlesTabCreating()Z
    .locals 3

    .prologue
    .line 376
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mBaseTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getId()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->startHiding(I)V

    .line 377
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->doneHiding()V

    .line 383
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mManagementDelegate:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManagementDelegate;

    const/4 v1, 0x3

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManagementDelegate;->updateTopControlsState(IZ)V

    .line 384
    const/4 v0, 0x1

    return v0
.end method

.method public hide()V
    .locals 1

    .prologue
    .line 325
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->isActive()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mBaseTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    if-eqz v0, :cond_0

    .line 326
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mBaseTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getId()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->startHiding(I)V

    .line 327
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->doneHiding()V

    .line 329
    :cond_0
    return-void
.end method

.method public isContextualSearchLayoutShowing()Z
    .locals 1

    .prologue
    .line 277
    const/4 v0, 0x1

    return v0
.end method

.method public maximizePanel(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;)V
    .locals 1

    .prologue
    .line 574
    sget-object v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->MAXIMIZED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->animatePanelToState(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;)V

    .line 575
    return-void
.end method

.method public maximizePanelThenPromoteToTab(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;)V
    .locals 1

    .prologue
    .line 559
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mShouldPromoteToTabAfterMaximizing:Z

    .line 560
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->maximizePanel(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;)V

    .line 561
    return-void
.end method

.method public maximizePanelThenPromoteToTab(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;J)V
    .locals 2

    .prologue
    .line 565
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mShouldPromoteToTabAfterMaximizing:Z

    .line 566
    sget-object v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->MAXIMIZED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    invoke-direct {p0, v0, p1, p2, p3}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->animatePanelToState(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;J)V

    .line 567
    return-void
.end method

.method protected onAnimationFinished()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 700
    invoke-super {p0}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->onAnimationFinished()V

    .line 702
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->shouldHideContextualSearchLayout()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 703
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mBaseTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getId()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->startHiding(I)V

    .line 704
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->getPanelState()Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->CLOSED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    if-ne v0, v1, :cond_0

    .line 705
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    const/16 v1, 0x4c

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastImmediateNotification(Landroid/content/Context;I)V

    .line 710
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mShouldPromoteToTabAfterMaximizing:Z

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->getPanelState()Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->MAXIMIZED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    if-ne v0, v1, :cond_1

    .line 711
    iput-boolean v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mShouldPromoteToTabAfterMaximizing:Z

    .line 712
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mManagementDelegate:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManagementDelegate;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManagementDelegate;->promoteToTab()V

    .line 715
    :cond_1
    iput-boolean v2, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mIsAnimatingPanel:Z

    .line 716
    return-void
.end method

.method public onBackPressed()Z
    .locals 1

    .prologue
    .line 493
    sget-object v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->BACK_PRESS:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->closePanel(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;)V

    .line 494
    const/4 v0, 0x1

    return v0
.end method

.method public onDown(JFF)V
    .locals 0

    .prologue
    .line 414
    iput p4, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mInitialPanelTouchY:F

    .line 415
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->handleSwipeStart()V

    .line 416
    return-void
.end method

.method public onUpOrCancel(J)V
    .locals 0

    .prologue
    .line 435
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->handleSwipeEnd()V

    .line 436
    return-void
.end method

.method public refresh()V
    .locals 1

    .prologue
    .line 389
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->isActive()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->requestUpdate()V

    .line 390
    :cond_0
    return-void
.end method

.method public setContextualSearchManagementDelegate(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManagementDelegate;)V
    .locals 0

    .prologue
    .line 405
    iput-object p1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mManagementDelegate:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManagementDelegate;

    .line 406
    return-void
.end method

.method public setFirstRunFlowContentHeight(F)V
    .locals 2

    .prologue
    .line 1189
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->getContextualSearchState()Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->setFirstRunFlowContentHeight(F)V

    .line 1190
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mLastSelectionYPx:F

    sget-object v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->PROMO:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->calculateBasePageOffsetY(FLcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;)F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mPromoBasePageOffsetY:F

    .line 1191
    return-void
.end method

.method public setProperty(Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout$Property;F)V
    .locals 1

    .prologue
    .line 691
    sget-object v0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout$Property;->PANEL_HEIGHT:Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout$Property;

    if-ne p1, v0, :cond_1

    .line 692
    invoke-direct {p0, p2}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->setPanelHeight(F)V

    .line 696
    :cond_0
    :goto_0
    return-void

    .line 693
    :cond_1
    sget-object v0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout$Property;->FIRST_RUN_PANEL_HEIGHT:Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout$Property;

    if-ne p1, v0, :cond_0

    .line 694
    invoke-direct {p0, p2}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->setPanelHeightForPromoOptInAnimation(F)V

    goto :goto_0
.end method

.method public bridge synthetic setProperty(Ljava/lang/Enum;F)V
    .locals 0

    .prologue
    .line 34
    check-cast p1, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout$Property;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->setProperty(Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout$Property;F)V

    return-void
.end method

.method public shouldShowContextualSearchView()Z
    .locals 2

    .prologue
    .line 283
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->getPanelState()Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->PEEKED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mLastPanelHeight:F

    sget-object v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->PEEKED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->getPanelHeightFromState(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;)F

    move-result v1

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public show(JZ)V
    .locals 3

    .prologue
    .line 312
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->show(JZ)V

    .line 314
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->resetLayout()V

    .line 315
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mBaseTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->createBaseLayoutTab(Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;)V

    .line 317
    sget-object v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->PEEKED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    sget-object v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->RESET:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->resizePanelToState(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;)V

    .line 319
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mLastSelectionYPx:F

    sget-object v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->EXPANDED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->calculateBasePageOffsetY(FLcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;)F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mBasePageOffsetY:F

    .line 320
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mLastSelectionYPx:F

    sget-object v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->PROMO:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->calculateBasePageOffsetY(FLcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;)F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mPromoBasePageOffsetY:F

    .line 321
    return-void
.end method

.method public swipeFinished(J)V
    .locals 0

    .prologue
    .line 483
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->handleSwipeEnd()V

    .line 484
    return-void
.end method

.method public swipeFlingOccurred(JFFFFFF)V
    .locals 0

    .prologue
    .line 478
    invoke-direct {p0, p8}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->handleFling(F)V

    .line 479
    return-void
.end method

.method public swipeStarted(JLcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;FF)V
    .locals 0

    .prologue
    .line 467
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->handleSwipeStart()V

    .line 468
    return-void
.end method

.method public swipeUpdated(JFFFFFF)V
    .locals 0

    .prologue
    .line 472
    invoke-direct {p0, p8}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->handleSwipeMove(F)V

    .line 473
    return-void
.end method

.method public updateBasePageSelectionStartYPx(F)V
    .locals 1

    .prologue
    .line 1195
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->getContextualSearchState()Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState;->setBasePageSelectionStartYPx(F)V

    .line 1196
    sget-object v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->EXPANDED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->calculateBasePageOffsetY(FLcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;)F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mBasePageOffsetY:F

    .line 1197
    sget-object v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->PROMO:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->calculateBasePageOffsetY(FLcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;)F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mPromoBasePageOffsetY:F

    .line 1198
    iput p1, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mLastSelectionYPx:F

    .line 1199
    return-void
.end method

.method protected updateLayout(JJ)V
    .locals 1

    .prologue
    .line 365
    invoke-super {p0, p1, p2, p3, p4}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->updateLayout(JJ)V

    .line 366
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mBaseTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    if-nez v0, :cond_1

    .line 371
    :cond_0
    :goto_0
    return-void

    .line 369
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->mBaseTab:Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    invoke-virtual {v0, p3, p4}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->updateSnap(J)Z

    move-result v0

    .line 370
    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;->requestUpdate()V

    goto :goto_0
.end method
