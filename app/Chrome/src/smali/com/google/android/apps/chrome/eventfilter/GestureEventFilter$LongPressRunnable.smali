.class Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter$LongPressRunnable;
.super Ljava/lang/Object;
.source "GestureEventFilter.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private mInitialEvent:Landroid/view/MotionEvent;

.field private mIsPending:Z

.field final synthetic this$0:Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;)V
    .locals 0

    .prologue
    .line 30
    iput-object p1, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter$LongPressRunnable;->this$0:Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter$1;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter$LongPressRunnable;-><init>(Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;)V

    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 1

    .prologue
    .line 49
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter$LongPressRunnable;->mIsPending:Z

    .line 50
    return-void
.end method

.method public getInitialEvent()Landroid/view/MotionEvent;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter$LongPressRunnable;->mInitialEvent:Landroid/view/MotionEvent;

    return-object v0
.end method

.method public init(Landroid/view/MotionEvent;)V
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter$LongPressRunnable;->mInitialEvent:Landroid/view/MotionEvent;

    if-eqz v0, :cond_0

    .line 36
    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter$LongPressRunnable;->mInitialEvent:Landroid/view/MotionEvent;

    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    .line 38
    :cond_0
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter$LongPressRunnable;->mInitialEvent:Landroid/view/MotionEvent;

    .line 39
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter$LongPressRunnable;->mIsPending:Z

    .line 40
    return-void
.end method

.method public isPending()Z
    .locals 1

    .prologue
    .line 53
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter$LongPressRunnable;->mIsPending:Z

    return v0
.end method

.method public run()V
    .locals 2

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter$LongPressRunnable;->this$0:Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;

    iget-object v1, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter$LongPressRunnable;->mInitialEvent:Landroid/view/MotionEvent;

    # invokes: Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->longPress(Landroid/view/MotionEvent;)V
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->access$100(Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;Landroid/view/MotionEvent;)V

    .line 45
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter$LongPressRunnable;->mIsPending:Z

    .line 46
    return-void
.end method
