.class public interface abstract Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenHtmlApiHandler$FullscreenHtmlApiDelegate;
.super Ljava/lang/Object;
.source "FullscreenHtmlApiHandler.java"


# virtual methods
.method public abstract cancelPendingEnterFullscreen()Z
.end method

.method public abstract getNotificationAnchorView()Landroid/view/View;
.end method

.method public abstract getNotificationOffsetY()I
.end method

.method public abstract onEnterFullscreen()V
.end method

.method public abstract onFullscreenExited(Lorg/chromium/content/browser/ContentViewCore;)V
.end method

.method public abstract shouldShowNotificationBubble()Z
.end method
