.class public Lcom/google/android/apps/chrome/preferences/AdvancedPreferences;
.super Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;
.source "AdvancedPreferences.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 24
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    .line 25
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/AdvancedPreferences;->updatePreferences()V

    .line 26
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 61
    invoke-super {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;->onResume()V

    .line 62
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/AdvancedPreferences;->updateSummaries()V

    .line 63
    return-void
.end method

.method public updatePreferences()V
    .locals 2

    .prologue
    .line 32
    sget v0, Lcom/google/android/apps/chrome/R$xml;->advanced_preferences:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/AdvancedPreferences;->addPreferencesFromResource(I)V

    .line 34
    invoke-static {}, Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings;->getInstance()Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings;->isDataReductionProxyAllowed()Z

    move-result v0

    .line 36
    if-nez v0, :cond_0

    .line 37
    const-string/jumbo v0, "reduce_data_usage_settings"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/AdvancedPreferences;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceScreen;

    .line 39
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/AdvancedPreferences;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 42
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/AdvancedPreferences;->updateSummaries()V

    .line 43
    return-void
.end method

.method public updateSummaries()V
    .locals 4

    .prologue
    .line 49
    const-string/jumbo v0, "reduce_data_usage_settings"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/AdvancedPreferences;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceScreen;

    .line 51
    if-eqz v0, :cond_0

    .line 52
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/AdvancedPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthReductionPreferences;->generateSummary(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->setSummary(Ljava/lang/CharSequence;)V

    .line 55
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/AdvancedPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v1

    sget v2, Lcom/google/android/apps/chrome/R$id;->reduce_data_usage_settings:I

    int-to-long v2, v2

    invoke-static {v1, v2, v3, v0}, Lcom/google/android/apps/chrome/preferences/ManagedPreferencesUtils;->disableIfManagedByPolicy(Landroid/content/Context;JLandroid/preference/PreferenceScreen;)V

    .line 57
    return-void
.end method
