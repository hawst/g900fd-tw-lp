.class public Lcom/google/android/apps/chrome/preferences/ProtectedContentPreferences;
.super Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;
.source "ProtectedContentPreferences.java"

# interfaces
.implements Lcom/google/android/apps/chrome/preferences/ProtectedContentResetCredentialConfirmDialogFragment$Listener;


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private mActionBarSwitch:Landroid/widget/Switch;

.field private mProtectedContentEnabler:Lcom/google/android/apps/chrome/preferences/WidgetEnabler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const-class v0, Lcom/google/android/apps/chrome/preferences/ProtectedContentPreferences;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/preferences/ProtectedContentPreferences;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;-><init>()V

    .line 29
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 54
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    .line 55
    sget v0, Lcom/google/android/apps/chrome/R$xml;->protected_content_preferences:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/ProtectedContentPreferences;->addPreferencesFromResource(I)V

    .line 58
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/ProtectedContentPreferences;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    const-string/jumbo v1, "protected_content_learn_more"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 60
    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-virtual {v0}, Landroid/preference/Preference;->getTitle()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 62
    new-instance v2, Lcom/google/android/apps/chrome/preferences/ProtectedContentPreferences$1;

    invoke-direct {v2, p0}, Lcom/google/android/apps/chrome/preferences/ProtectedContentPreferences$1;-><init>(Lcom/google/android/apps/chrome/preferences/ProtectedContentPreferences;)V

    .line 69
    const/4 v3, 0x0

    invoke-virtual {v1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v4

    const/16 v5, 0x21

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 71
    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 73
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/ProtectedContentPreferences;->setHasOptionsMenu(Z)V

    .line 74
    new-instance v0, Landroid/widget/Switch;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/ProtectedContentPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/Switch;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/preferences/ProtectedContentPreferences;->mActionBarSwitch:Landroid/widget/Switch;

    .line 75
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ProtectedContentPreferences;->mActionBarSwitch:Landroid/widget/Switch;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/ProtectedContentPreferences;->addSwitchToActionBar(Landroid/widget/Switch;)V

    .line 76
    new-instance v0, Lcom/google/android/apps/chrome/preferences/ProtectedContentPreferences$Enabler;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/ProtectedContentPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/preferences/ProtectedContentPreferences;->mActionBarSwitch:Landroid/widget/Switch;

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/chrome/preferences/ProtectedContentPreferences$Enabler;-><init>(Landroid/content/Context;Landroid/widget/Switch;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/preferences/ProtectedContentPreferences;->mProtectedContentEnabler:Lcom/google/android/apps/chrome/preferences/WidgetEnabler;

    .line 77
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ProtectedContentPreferences;->mProtectedContentEnabler:Lcom/google/android/apps/chrome/preferences/WidgetEnabler;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/preferences/WidgetEnabler;->attach()V

    .line 78
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 82
    invoke-super {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;->onDestroy()V

    .line 83
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ProtectedContentPreferences;->mProtectedContentEnabler:Lcom/google/android/apps/chrome/preferences/WidgetEnabler;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/preferences/WidgetEnabler;->destroy()V

    .line 84
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ProtectedContentPreferences;->mActionBarSwitch:Landroid/widget/Switch;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/Switch;->setVisibility(I)V

    .line 85
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    .line 89
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 90
    sget v1, Lcom/google/android/apps/chrome/R$id;->menu_id_reset_device_credential:I

    if-ne v0, v1, :cond_0

    .line 91
    new-instance v0, Lcom/google/android/apps/chrome/preferences/ProtectedContentResetCredentialConfirmDialogFragment;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/preferences/ProtectedContentResetCredentialConfirmDialogFragment;-><init>(Lcom/google/android/apps/chrome/preferences/ProtectedContentResetCredentialConfirmDialogFragment$Listener;)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/ProtectedContentPreferences;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/preferences/ProtectedContentResetCredentialConfirmDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 94
    const/4 v0, 0x1

    .line 98
    :goto_0
    return v0

    .line 96
    :cond_0
    sget-boolean v0, Lcom/google/android/apps/chrome/preferences/ProtectedContentPreferences;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 98
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public resetDeviceCredential()V
    .locals 1

    .prologue
    .line 103
    new-instance v0, Lcom/google/android/apps/chrome/preferences/ProtectedContentPreferences$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/preferences/ProtectedContentPreferences$2;-><init>(Lcom/google/android/apps/chrome/preferences/ProtectedContentPreferences;)V

    invoke-static {v0}, Lorg/chromium/content/browser/MediaDrmCredentialManager;->resetCredentials(Lorg/chromium/content/browser/MediaDrmCredentialManager$MediaDrmCredentialManagerCallback;)V

    .line 116
    return-void
.end method
