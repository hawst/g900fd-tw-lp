.class public Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewWrapper;
.super Lcom/google/android/apps/chrome/widget/lazyloading/LazyViewLoader;
.source "EmptyBackgroundViewWrapper.java"


# static fields
.field private static final NOTIFICATIONS:[I


# instance fields
.field private mBackgroundView:Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewTablet;

.field private mMenuHandler:Lorg/chromium/chrome/browser/appmenu/AppMenuHandler;

.field private final mTabCreator:Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;

.field private final mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 96
    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewWrapper;->NOTIFICATIONS:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x5
        0x2
        0x45
        0xc
    .end array-data
.end method

.method public constructor <init>(Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;Landroid/app/Activity;ILorg/chromium/chrome/browser/appmenu/AppMenuHandler;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0, p3, p4}, Lcom/google/android/apps/chrome/widget/lazyloading/LazyViewLoader;-><init>(Landroid/app/Activity;I)V

    .line 39
    iput-object p5, p0, Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewWrapper;->mMenuHandler:Lorg/chromium/chrome/browser/appmenu/AppMenuHandler;

    .line 40
    iput-object p1, p0, Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewWrapper;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    .line 41
    iput-object p2, p0, Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewWrapper;->mTabCreator:Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;

    .line 42
    return-void
.end method

.method private checkEmptyContainerState()V
    .locals 2

    .prologue
    .line 77
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewWrapper;->inflateIfNecessary()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 78
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewWrapper;->mBackgroundView:Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewTablet;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewWrapper;->shouldShowEmptyContainer()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewTablet;->setEmptyContainerState(Z)V

    .line 80
    :cond_0
    return-void
.end method

.method private shouldShowEmptyContainer()Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 83
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewWrapper;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    invoke-interface {v0, v2}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->getModel(Z)Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v3

    .line 84
    if-nez v3, :cond_1

    .line 91
    :cond_0
    :goto_0
    return v2

    .line 87
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewWrapper;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    invoke-interface {v0, v1}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->getModel(Z)Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v0

    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->getCount()I

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    .line 88
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewWrapper;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/apps/chrome/device/DeviceClassManager;->isAccessibilityModeEnabled(Landroid/content/Context;)Z

    move-result v4

    .line 90
    iget-object v5, p0, Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewWrapper;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    invoke-interface {v5}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->isIncognitoSelected()Z

    move-result v5

    .line 91
    invoke-interface {v3}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->getCount()I

    move-result v3

    if-nez v3, :cond_0

    if-nez v4, :cond_2

    if-eqz v5, :cond_3

    :cond_2
    if-eqz v0, :cond_0

    if-eqz v4, :cond_0

    :cond_3
    move v2, v1

    goto :goto_0

    :cond_4
    move v0, v2

    .line 87
    goto :goto_1
.end method


# virtual methods
.method protected getNotificationsToRegister()[I
    .locals 1

    .prologue
    .line 46
    sget-object v0, Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewWrapper;->NOTIFICATIONS:[I

    return-object v0
.end method

.method protected handleNotificationMessage(Landroid/os/Message;)V
    .locals 1

    .prologue
    .line 51
    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    .line 61
    :goto_0
    return-void

    .line 55
    :sswitch_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewWrapper;->checkEmptyContainerState()V

    goto :goto_0

    .line 58
    :sswitch_1
    invoke-direct {p0}, Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewWrapper;->checkEmptyContainerState()V

    goto :goto_0

    .line 51
    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0x5 -> :sswitch_0
        0xc -> :sswitch_1
        0x45 -> :sswitch_0
    .end sparse-switch
.end method

.method protected shouldInflate()Z
    .locals 1

    .prologue
    .line 65
    invoke-direct {p0}, Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewWrapper;->shouldShowEmptyContainer()Z

    move-result v0

    return v0
.end method

.method protected bridge synthetic viewInflated(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 22
    check-cast p1, Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewTablet;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewWrapper;->viewInflated(Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewTablet;)V

    return-void
.end method

.method protected viewInflated(Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewTablet;)V
    .locals 2

    .prologue
    .line 70
    iput-object p1, p0, Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewWrapper;->mBackgroundView:Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewTablet;

    .line 71
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewWrapper;->mBackgroundView:Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewTablet;

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewWrapper;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewTablet;->setTabModelSelector(Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;)V

    .line 72
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewWrapper;->mBackgroundView:Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewTablet;

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewWrapper;->mTabCreator:Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewTablet;->setTabCreator(Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;)V

    .line 73
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewWrapper;->mBackgroundView:Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewTablet;

    iget-object v1, p0, Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewWrapper;->mMenuHandler:Lorg/chromium/chrome/browser/appmenu/AppMenuHandler;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewTablet;->setMenuOnTouchListener(Lorg/chromium/chrome/browser/appmenu/AppMenuHandler;)V

    .line 74
    return-void
.end method
