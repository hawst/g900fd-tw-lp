.class Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone$5;
.super Landroid/animation/AnimatorListenerAdapter;
.source "ToolbarPhone.java"


# instance fields
.field private mCanceled:Z

.field final synthetic this$1:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;

.field final synthetic val$hasFocus:Z


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;Z)V
    .locals 0

    .prologue
    .line 1611
    iput-object p1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone$5;->this$1:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;

    iput-boolean p2, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone$5;->val$hasFocus:Z

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 1

    .prologue
    .line 1633
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone$5;->mCanceled:Z

    .line 1634
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v1, 0x4

    const/4 v2, 0x0

    .line 1638
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone$5;->mCanceled:Z

    if-eqz v0, :cond_0

    .line 1656
    :goto_0
    return-void

    .line 1640
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone$5;->val$hasFocus:Z

    if-nez v0, :cond_2

    .line 1641
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone$5;->this$1:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;

    iget-object v0, v0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # setter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mDisableLocationBarRelayout:Z
    invoke-static {v0, v2}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$3702(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;Z)Z

    .line 1642
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone$5;->this$1:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;

    iget-object v0, v0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # setter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mLayoutLocationBarInFocusedMode:Z
    invoke-static {v0, v2}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$3802(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;Z)Z

    .line 1643
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone$5;->this$1:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->requestLayout()V

    .line 1654
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone$5;->this$1:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;

    iget-object v0, v0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mPhoneLocationBar:Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$3300(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone$5;->val$hasFocus:Z

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/omnibox/LocationBarPhone;->finishUrlFocusChange(Z)V

    .line 1655
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone$5;->this$1:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;

    iget-object v0, v0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # setter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUrlFocusChangeInProgress:Z
    invoke-static {v0, v2}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$3502(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;Z)Z

    goto :goto_0

    .line 1646
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone$5;->this$1:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;

    iget-object v0, v0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mToolbarButtonsContainer:Landroid/view/View;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$3600(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1647
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone$5;->this$1:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;

    iget-object v0, v0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mHomeButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$700(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getVisibility()I

    move-result v0

    if-eq v0, v3, :cond_3

    .line 1648
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone$5;->this$1:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;

    iget-object v0, v0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mHomeButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$700(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1650
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone$5;->this$1:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;

    iget-object v0, v0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mReaderModeButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$1400(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getVisibility()I

    move-result v0

    if-eq v0, v3, :cond_1

    .line 1651
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone$5;->this$1:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;

    iget-object v0, v0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mReaderModeButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$1400(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_1
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1616
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone$5;->val$hasFocus:Z

    if-nez v0, :cond_2

    .line 1617
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone$5;->this$1:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;

    iget-object v0, v0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mToolbarButtonsContainer:Landroid/view/View;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$3600(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1618
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone$5;->this$1:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;

    iget-object v0, v0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mHomeButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$700(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getVisibility()I

    move-result v0

    if-eq v0, v3, :cond_0

    .line 1619
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone$5;->this$1:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;

    iget-object v0, v0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mHomeButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$700(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1621
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone$5;->this$1:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;

    iget-object v0, v0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mReaderModeButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$1400(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getVisibility()I

    move-result v0

    if-eq v0, v3, :cond_1

    .line 1622
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone$5;->this$1:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;

    iget-object v0, v0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mReaderModeButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$1400(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1624
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone$5;->this$1:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;

    iget-object v0, v0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # setter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mDisableLocationBarRelayout:Z
    invoke-static {v0, v2}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$3702(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;Z)Z

    .line 1629
    :goto_0
    return-void

    .line 1626
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone$5;->this$1:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;

    iget-object v0, v0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # setter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mLayoutLocationBarInFocusedMode:Z
    invoke-static {v0, v2}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$3802(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;Z)Z

    .line 1627
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone$5;->this$1:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->requestLayout()V

    goto :goto_0
.end method
