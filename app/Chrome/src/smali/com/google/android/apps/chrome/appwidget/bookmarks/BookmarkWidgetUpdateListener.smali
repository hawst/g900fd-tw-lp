.class public Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener;
.super Ljava/lang/Object;
.source "BookmarkWidgetUpdateListener.java"


# instance fields
.field private mBookmarkUpdateObserver:Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener$BookmarkUpdateObserver;

.field private final mContext:Landroid/content/Context;

.field private mListener:Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener$UpdateListener;

.field private mSyncObserver:Lorg/chromium/sync/notifier/SyncStatusHelper$SyncSettingsChangedObserver;

.field private final mThumbnailUpdated:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener$UpdateListener;)V
    .locals 4

    .prologue
    .line 104
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 85
    new-instance v0, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener$1;-><init>(Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener;->mThumbnailUpdated:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    .line 105
    iput-object p1, p0, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener;->mContext:Landroid/content/Context;

    .line 107
    if-nez p2, :cond_0

    .line 118
    :goto_0
    return-void

    .line 108
    :cond_0
    iput-object p2, p0, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener;->mListener:Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener$UpdateListener;

    .line 111
    iget-object v0, p0, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 112
    new-instance v1, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener$BookmarkUpdateObserver;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener$BookmarkUpdateObserver;-><init>(Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener;)V

    iput-object v1, p0, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener;->mBookmarkUpdateObserver:Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener$BookmarkUpdateObserver;

    .line 113
    iget-object v1, p0, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lorg/chromium/chrome/browser/ChromeBrowserProvider;->getBookmarksApiUri(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener;->mBookmarkUpdateObserver:Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener$BookmarkUpdateObserver;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 117
    new-instance v0, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener$SyncUpdateObserver;

    iget-object v1, p0, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lorg/chromium/sync/notifier/SyncStatusHelper;->get(Landroid/content/Context;)Lorg/chromium/sync/notifier/SyncStatusHelper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener$SyncUpdateObserver;-><init>(Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener;Lorg/chromium/sync/notifier/SyncStatusHelper;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener;->mSyncObserver:Lorg/chromium/sync/notifier/SyncStatusHelper$SyncSettingsChangedObserver;

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener;)Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener$UpdateListener;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener;->mListener:Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener$UpdateListener;

    return-object v0
.end method


# virtual methods
.method public destroy()V
    .locals 2

    .prologue
    .line 121
    iget-object v0, p0, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener;->mListener:Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener$UpdateListener;

    if-nez v0, :cond_0

    .line 130
    :goto_0
    return-void

    .line 124
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 125
    iget-object v1, p0, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener;->mBookmarkUpdateObserver:Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener$BookmarkUpdateObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 126
    iget-object v0, p0, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lorg/chromium/sync/notifier/SyncStatusHelper;->get(Landroid/content/Context;)Lorg/chromium/sync/notifier/SyncStatusHelper;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener;->mSyncObserver:Lorg/chromium/sync/notifier/SyncStatusHelper$SyncSettingsChangedObserver;

    invoke-virtual {v0, v1}, Lorg/chromium/sync/notifier/SyncStatusHelper;->unregisterSyncSettingsChangedObserver(Lorg/chromium/sync/notifier/SyncStatusHelper$SyncSettingsChangedObserver;)V

    .line 127
    const/16 v0, 0x3d

    iget-object v1, p0, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener;->mThumbnailUpdated:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->unregisterForNotification(ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    .line 129
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener;->mListener:Lcom/google/android/apps/chrome/appwidget/bookmarks/BookmarkWidgetUpdateListener$UpdateListener;

    goto :goto_0
.end method
