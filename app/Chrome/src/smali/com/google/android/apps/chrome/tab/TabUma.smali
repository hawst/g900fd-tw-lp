.class public Lcom/google/android/apps/chrome/tab/TabUma;
.super Ljava/lang/Object;
.source "TabUma.java"


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final TAB_BACKGROUND_LOAD_LIM:I = 0x3

.field static final TAB_BACKGROUND_LOAD_LOST:I = 0x1

.field static final TAB_BACKGROUND_LOAD_SHOWN:I = 0x0

.field static final TAB_BACKGROUND_LOAD_SKIPPED:I = 0x2

.field static final TAB_STATUS_LAZY_LOAD_FOR_BG_TAB:I = 0x8

.field public static final TAB_STATUS_LIM:I = 0x9

.field static final TAB_STATUS_MEMORY_RESIDENT:I = 0x0

.field static final TAB_STATUS_RELOAD_COLD_START_BG:I = 0x7

.field static final TAB_STATUS_RELOAD_COLD_START_FG:I = 0x6

.field static final TAB_STATUS_RELOAD_EVICTED:I = 0x1

.field private static sAllTabsShowCount:J


# instance fields
.field private mLastShowMillis:J

.field private mRestoreStartedAtMillis:J

.field private final mTab:Lcom/google/android/apps/chrome/tab/ChromeTab;

.field private final mTabCreationState:Lcom/google/android/apps/chrome/tab/TabUma$TabCreationState;

.field private final mTabModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 20
    const-class v0, Lcom/google/android/apps/chrome/tab/TabUma;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/tab/TabUma;->$assertionsDisabled:Z

    .line 36
    const-wide/16 v0, 0x0

    sput-wide v0, Lcom/google/android/apps/chrome/tab/TabUma;->sAllTabsShowCount:J

    return-void

    .line 20
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lcom/google/android/apps/chrome/tab/ChromeTab;Lcom/google/android/apps/chrome/tab/TabUma$TabCreationState;Lorg/chromium/chrome/browser/tabmodel/TabModel;)V
    .locals 2

    .prologue
    const-wide/16 v0, -0x1

    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    iput-wide v0, p0, Lcom/google/android/apps/chrome/tab/TabUma;->mLastShowMillis:J

    .line 58
    iput-wide v0, p0, Lcom/google/android/apps/chrome/tab/TabUma;->mRestoreStartedAtMillis:J

    .line 67
    iput-object p1, p0, Lcom/google/android/apps/chrome/tab/TabUma;->mTab:Lcom/google/android/apps/chrome/tab/ChromeTab;

    .line 68
    iput-object p2, p0, Lcom/google/android/apps/chrome/tab/TabUma;->mTabCreationState:Lcom/google/android/apps/chrome/tab/TabUma$TabCreationState;

    .line 69
    iput-object p3, p0, Lcom/google/android/apps/chrome/tab/TabUma;->mTabModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    .line 70
    return-void
.end method

.method private static computeMRURank(Lcom/google/android/apps/chrome/tab/ChromeTab;Lorg/chromium/chrome/browser/tabmodel/TabModel;)I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 201
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getTabUma()Lcom/google/android/apps/chrome/tab/TabUma;

    move-result-object v1

    iget-wide v2, v1, Lcom/google/android/apps/chrome/tab/TabUma;->mLastShowMillis:J

    move v1, v0

    .line 203
    :goto_0
    invoke-interface {p1}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->getCount()I

    move-result v4

    if-ge v0, v4, :cond_1

    .line 204
    invoke-interface {p1, v0}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->getTabAt(I)Lorg/chromium/chrome/browser/Tab;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/apps/chrome/tab/ChromeTab;->fromTab(Lorg/chromium/chrome/browser/Tab;)Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v4

    .line 205
    if-eq v4, p0, :cond_0

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getTabUma()Lcom/google/android/apps/chrome/tab/TabUma;

    move-result-object v4

    iget-wide v4, v4, Lcom/google/android/apps/chrome/tab/TabUma;->mLastShowMillis:J

    cmp-long v4, v4, v2

    if-lez v4, :cond_0

    .line 206
    add-int/lit8 v1, v1, 0x1

    .line 203
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 209
    :cond_1
    return v1
.end method

.method private static increaseTabShowCount()V
    .locals 4

    .prologue
    .line 190
    sget-wide v0, Lcom/google/android/apps/chrome/tab/TabUma;->sAllTabsShowCount:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    sput-wide v0, Lcom/google/android/apps/chrome/tab/TabUma;->sAllTabsShowCount:J

    .line 191
    return-void
.end method

.method private static millisecondsToMinutes(J)J
    .locals 4

    .prologue
    .line 194
    const-wide/16 v0, 0x3e8

    div-long v0, p0, v0

    const-wide/16 v2, 0x3c

    div-long/2addr v0, v2

    return-wide v0
.end method


# virtual methods
.method onLoadFailed(I)V
    .locals 7

    .prologue
    const-wide/16 v2, -0x1

    .line 173
    iget-wide v0, p0, Lcom/google/android/apps/chrome/tab/TabUma;->mRestoreStartedAtMillis:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/apps/chrome/tab/TabUma;->mLastShowMillis:J

    iget-wide v4, p0, Lcom/google/android/apps/chrome/tab/TabUma;->mRestoreStartedAtMillis:J

    cmp-long v0, v0, v4

    if-ltz v0, :cond_0

    .line 175
    const/4 v1, 0x0

    move-wide v4, v2

    move v6, p1

    invoke-static/range {v1 .. v6}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->tabRestoreResult(ZJJI)V

    .line 177
    :cond_0
    iput-wide v2, p0, Lcom/google/android/apps/chrome/tab/TabUma;->mRestoreStartedAtMillis:J

    .line 178
    return-void
.end method

.method onLoadFinished()V
    .locals 10

    .prologue
    const-wide/16 v8, -0x1

    .line 162
    iget-wide v0, p0, Lcom/google/android/apps/chrome/tab/TabUma;->mRestoreStartedAtMillis:J

    cmp-long v0, v0, v8

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/apps/chrome/tab/TabUma;->mLastShowMillis:J

    iget-wide v2, p0, Lcom/google/android/apps/chrome/tab/TabUma;->mRestoreStartedAtMillis:J

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    .line 163
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 164
    iget-wide v2, p0, Lcom/google/android/apps/chrome/tab/TabUma;->mRestoreStartedAtMillis:J

    sub-long v2, v0, v2

    .line 165
    iget-wide v4, p0, Lcom/google/android/apps/chrome/tab/TabUma;->mLastShowMillis:J

    sub-long v4, v0, v4

    .line 166
    const/4 v1, 0x1

    const/4 v6, -0x1

    invoke-static/range {v1 .. v6}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->tabRestoreResult(ZJJI)V

    .line 168
    :cond_0
    iput-wide v8, p0, Lcom/google/android/apps/chrome/tab/TabUma;->mRestoreStartedAtMillis:J

    .line 169
    return-void
.end method

.method onRendererCrashed()V
    .locals 4

    .prologue
    const-wide/16 v2, -0x1

    .line 182
    iget-wide v0, p0, Lcom/google/android/apps/chrome/tab/TabUma;->mRestoreStartedAtMillis:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 185
    iput-wide v2, p0, Lcom/google/android/apps/chrome/tab/TabUma;->mRestoreStartedAtMillis:J

    .line 187
    :cond_0
    return-void
.end method

.method onRestoreStarted()V
    .locals 2

    .prologue
    .line 154
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/chrome/tab/TabUma;->mRestoreStartedAtMillis:J

    .line 155
    return-void
.end method

.method onShow(Lorg/chromium/chrome/browser/tabmodel/TabModel$TabSelectionType;J)V
    .locals 12

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    const-wide/16 v10, -0x1

    .line 79
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    .line 83
    iget-wide v6, p0, Lcom/google/android/apps/chrome/tab/TabUma;->mLastShowMillis:J

    cmp-long v0, v6, v10

    if-eqz v0, :cond_0

    sget-object v0, Lorg/chromium/chrome/browser/tabmodel/TabModel$TabSelectionType;->FROM_USER:Lorg/chromium/chrome/browser/tabmodel/TabModel$TabSelectionType;

    if-ne p1, v0, :cond_0

    .line 84
    iget-wide v6, p0, Lcom/google/android/apps/chrome/tab/TabUma;->mLastShowMillis:J

    sub-long v6, v4, v6

    .line 85
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/TabUma;->mTab:Lcom/google/android/apps/chrome/tab/ChromeTab;

    iget-object v3, p0, Lcom/google/android/apps/chrome/tab/TabUma;->mTabModel:Lorg/chromium/chrome/browser/tabmodel/TabModel;

    invoke-static {v0, v3}, Lcom/google/android/apps/chrome/tab/TabUma;->computeMRURank(Lcom/google/android/apps/chrome/tab/ChromeTab;Lorg/chromium/chrome/browser/tabmodel/TabModel;)I

    move-result v0

    .line 86
    invoke-static {v6, v7, v0}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->tabSwitchedToForeground(JI)V

    .line 89
    :cond_0
    invoke-static {}, Lcom/google/android/apps/chrome/tab/TabUma;->increaseTabShowCount()V

    .line 90
    sget-wide v6, Lcom/google/android/apps/chrome/tab/TabUma;->sAllTabsShowCount:J

    const-wide/16 v8, 0x1

    cmp-long v0, v6, v8

    if-nez v0, :cond_4

    move v0, v1

    .line 91
    :goto_0
    iget-object v3, p0, Lcom/google/android/apps/chrome/tab/TabUma;->mTabCreationState:Lcom/google/android/apps/chrome/tab/TabUma$TabCreationState;

    sget-object v6, Lcom/google/android/apps/chrome/tab/TabUma$TabCreationState;->FROZEN_FOR_LAZY_LOAD:Lcom/google/android/apps/chrome/tab/TabUma$TabCreationState;

    if-ne v3, v6, :cond_5

    iget-wide v6, p0, Lcom/google/android/apps/chrome/tab/TabUma;->mLastShowMillis:J

    cmp-long v3, v6, v10

    if-nez v3, :cond_5

    move v3, v1

    .line 95
    :goto_1
    iget-wide v6, p0, Lcom/google/android/apps/chrome/tab/TabUma;->mRestoreStartedAtMillis:J

    cmp-long v6, v6, v10

    if-nez v6, :cond_6

    if-nez v3, :cond_6

    move v3, v2

    .line 119
    :goto_2
    sget-object v6, Lorg/chromium/chrome/browser/tabmodel/TabModel$TabSelectionType;->FROM_USER:Lorg/chromium/chrome/browser/tabmodel/TabModel$TabSelectionType;

    if-ne p1, v6, :cond_1

    .line 120
    invoke-static {v3}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->tabStatusWhenSwitchedBackToForeground(I)V

    .line 124
    :cond_1
    iget-wide v6, p0, Lcom/google/android/apps/chrome/tab/TabUma;->mLastShowMillis:J

    cmp-long v3, v6, v10

    if-nez v3, :cond_2

    .line 125
    iget-object v3, p0, Lcom/google/android/apps/chrome/tab/TabUma;->mTabCreationState:Lcom/google/android/apps/chrome/tab/TabUma$TabCreationState;

    sget-object v6, Lcom/google/android/apps/chrome/tab/TabUma$TabCreationState;->LIVE_IN_BACKGROUND:Lcom/google/android/apps/chrome/tab/TabUma$TabCreationState;

    if-ne v3, v6, :cond_d

    .line 126
    iget-wide v6, p0, Lcom/google/android/apps/chrome/tab/TabUma;->mRestoreStartedAtMillis:J

    cmp-long v3, v6, v10

    if-nez v3, :cond_c

    .line 127
    invoke-static {v2}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->tabBackgroundLoadStatus(I)V

    .line 139
    :cond_2
    :goto_3
    iget-wide v2, p0, Lcom/google/android/apps/chrome/tab/TabUma;->mLastShowMillis:J

    cmp-long v1, v2, v10

    if-nez v1, :cond_3

    const-wide/16 v2, 0x0

    cmp-long v1, p2, v2

    if-lez v1, :cond_3

    .line 140
    if-eqz v0, :cond_f

    .line 141
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sub-long/2addr v0, p2

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/tab/TabUma;->millisecondsToMinutes(J)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->foregroundTabAgeAtStartup(J)V

    .line 149
    :cond_3
    :goto_4
    iput-wide v4, p0, Lcom/google/android/apps/chrome/tab/TabUma;->mLastShowMillis:J

    .line 150
    return-void

    :cond_4
    move v0, v2

    .line 90
    goto :goto_0

    :cond_5
    move v3, v2

    .line 91
    goto :goto_1

    .line 98
    :cond_6
    iget-wide v6, p0, Lcom/google/android/apps/chrome/tab/TabUma;->mLastShowMillis:J

    cmp-long v3, v6, v10

    if-nez v3, :cond_b

    .line 100
    if-eqz v0, :cond_7

    .line 101
    const/4 v3, 0x6

    goto :goto_2

    .line 102
    :cond_7
    iget-object v3, p0, Lcom/google/android/apps/chrome/tab/TabUma;->mTabCreationState:Lcom/google/android/apps/chrome/tab/TabUma$TabCreationState;

    sget-object v6, Lcom/google/android/apps/chrome/tab/TabUma$TabCreationState;->FROZEN_ON_RESTORE:Lcom/google/android/apps/chrome/tab/TabUma$TabCreationState;

    if-ne v3, v6, :cond_8

    .line 103
    const/4 v3, 0x7

    goto :goto_2

    .line 104
    :cond_8
    iget-object v3, p0, Lcom/google/android/apps/chrome/tab/TabUma;->mTabCreationState:Lcom/google/android/apps/chrome/tab/TabUma$TabCreationState;

    sget-object v6, Lcom/google/android/apps/chrome/tab/TabUma$TabCreationState;->FROZEN_FOR_LAZY_LOAD:Lcom/google/android/apps/chrome/tab/TabUma$TabCreationState;

    if-ne v3, v6, :cond_9

    .line 105
    const/16 v3, 0x8

    goto :goto_2

    .line 107
    :cond_9
    sget-boolean v3, Lcom/google/android/apps/chrome/tab/TabUma;->$assertionsDisabled:Z

    if-nez v3, :cond_a

    iget-object v3, p0, Lcom/google/android/apps/chrome/tab/TabUma;->mTabCreationState:Lcom/google/android/apps/chrome/tab/TabUma$TabCreationState;

    sget-object v6, Lcom/google/android/apps/chrome/tab/TabUma$TabCreationState;->LIVE_IN_FOREGROUND:Lcom/google/android/apps/chrome/tab/TabUma$TabCreationState;

    if-eq v3, v6, :cond_a

    iget-object v3, p0, Lcom/google/android/apps/chrome/tab/TabUma;->mTabCreationState:Lcom/google/android/apps/chrome/tab/TabUma$TabCreationState;

    sget-object v6, Lcom/google/android/apps/chrome/tab/TabUma$TabCreationState;->LIVE_IN_BACKGROUND:Lcom/google/android/apps/chrome/tab/TabUma$TabCreationState;

    if-eq v3, v6, :cond_a

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_a
    move v3, v1

    .line 109
    goto :goto_2

    :cond_b
    move v3, v1

    .line 113
    goto :goto_2

    .line 129
    :cond_c
    invoke-static {v1}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->tabBackgroundLoadStatus(I)V

    goto :goto_3

    .line 131
    :cond_d
    iget-object v1, p0, Lcom/google/android/apps/chrome/tab/TabUma;->mTabCreationState:Lcom/google/android/apps/chrome/tab/TabUma$TabCreationState;

    sget-object v2, Lcom/google/android/apps/chrome/tab/TabUma$TabCreationState;->FROZEN_FOR_LAZY_LOAD:Lcom/google/android/apps/chrome/tab/TabUma$TabCreationState;

    if-ne v1, v2, :cond_2

    .line 132
    sget-boolean v1, Lcom/google/android/apps/chrome/tab/TabUma;->$assertionsDisabled:Z

    if-nez v1, :cond_e

    iget-wide v2, p0, Lcom/google/android/apps/chrome/tab/TabUma;->mRestoreStartedAtMillis:J

    cmp-long v1, v2, v10

    if-eqz v1, :cond_e

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 133
    :cond_e
    const/4 v1, 0x2

    invoke-static {v1}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->tabBackgroundLoadStatus(I)V

    goto :goto_3

    .line 143
    :cond_f
    sget-object v0, Lorg/chromium/chrome/browser/tabmodel/TabModel$TabSelectionType;->FROM_USER:Lorg/chromium/chrome/browser/tabmodel/TabModel$TabSelectionType;

    if-ne p1, v0, :cond_3

    .line 144
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sub-long/2addr v0, p2

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/tab/TabUma;->millisecondsToMinutes(J)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->tabAgeUponRestoreFromColdStart(J)V

    goto :goto_4
.end method
