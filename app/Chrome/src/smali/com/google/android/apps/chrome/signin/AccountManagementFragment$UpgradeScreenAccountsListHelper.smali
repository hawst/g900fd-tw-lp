.class Lcom/google/android/apps/chrome/signin/AccountManagementFragment$UpgradeScreenAccountsListHelper;
.super Ljava/lang/Object;
.source "AccountManagementFragment.java"

# interfaces
.implements Lorg/chromium/chrome/browser/profiles/ProfileDownloader$Observer;


# instance fields
.field private final mAccountsList:Landroid/widget/LinearLayout;

.field private final mActivity:Landroid/app/Activity;

.field private final mDefaultAccountBitmap:Landroid/graphics/Bitmap;

.field private final mProfile:Lorg/chromium/chrome/browser/profiles/Profile;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lorg/chromium/chrome/browser/profiles/Profile;Landroid/widget/LinearLayout;)V
    .locals 2

    .prologue
    .line 678
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 679
    iput-object p1, p0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment$UpgradeScreenAccountsListHelper;->mActivity:Landroid/app/Activity;

    .line 680
    iput-object p2, p0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment$UpgradeScreenAccountsListHelper;->mProfile:Lorg/chromium/chrome/browser/profiles/Profile;

    .line 681
    iput-object p3, p0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment$UpgradeScreenAccountsListHelper;->mAccountsList:Landroid/widget/LinearLayout;

    .line 682
    invoke-static {p0}, Lorg/chromium/chrome/browser/profiles/ProfileDownloader;->addObserver(Lorg/chromium/chrome/browser/profiles/ProfileDownloader$Observer;)V

    .line 684
    iget-object v0, p0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment$UpgradeScreenAccountsListHelper;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$drawable;->account_management_no_picture:I

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->makeRoundUserPicture(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment$UpgradeScreenAccountsListHelper;->mDefaultAccountBitmap:Landroid/graphics/Bitmap;

    .line 686
    iget-object v0, p0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment$UpgradeScreenAccountsListHelper;->mActivity:Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment$UpgradeScreenAccountsListHelper;->mProfile:Lorg/chromium/chrome/browser/profiles/Profile;

    # invokes: Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->startFetchingAccountsInformation(Landroid/content/Context;Lorg/chromium/chrome/browser/profiles/Profile;)V
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->access$300(Landroid/content/Context;Lorg/chromium/chrome/browser/profiles/Profile;)V

    .line 688
    invoke-direct {p0}, Lcom/google/android/apps/chrome/signin/AccountManagementFragment$UpgradeScreenAccountsListHelper;->updateAccountsList()V

    .line 689
    return-void
.end method

.method private updateAccountsList()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 696
    iget-object v0, p0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment$UpgradeScreenAccountsListHelper;->mAccountsList:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 697
    iget-object v0, p0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment$UpgradeScreenAccountsListHelper;->mAccountsList:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setClickable(Z)V

    .line 698
    iget-object v0, p0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment$UpgradeScreenAccountsListHelper;->mActivity:Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment$UpgradeScreenAccountsListHelper;->mAccountsList:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment$UpgradeScreenAccountsListHelper;->mDefaultAccountBitmap:Landroid/graphics/Bitmap;

    const/4 v3, 0x0

    # invokes: Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->fillAccountsList(Landroid/app/Activity;Landroid/widget/LinearLayout;Landroid/graphics/Bitmap;Ljava/lang/String;I)V
    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->access$400(Landroid/app/Activity;Landroid/widget/LinearLayout;Landroid/graphics/Bitmap;Ljava/lang/String;I)V

    .line 700
    return-void
.end method


# virtual methods
.method public destroy()V
    .locals 0

    .prologue
    .line 692
    invoke-static {p0}, Lorg/chromium/chrome/browser/profiles/ProfileDownloader;->removeObserver(Lorg/chromium/chrome/browser/profiles/ProfileDownloader$Observer;)V

    .line 693
    return-void
.end method

.method public onProfileDownloaded(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 0

    .prologue
    .line 704
    invoke-static {p1, p2, p3}, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->updateUserNamePictureCache(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    .line 705
    invoke-direct {p0}, Lcom/google/android/apps/chrome/signin/AccountManagementFragment$UpgradeScreenAccountsListHelper;->updateAccountsList()V

    .line 706
    return-void
.end method
