.class public Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;
.super Ljava/lang/Object;
.source "DelegatedPKCS11Manager.java"

# interfaces
.implements Lorg/chromium/chrome/browser/PKCS11AuthenticationManager;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final CONNECTION_LOCK:Ljava/lang/Object;


# instance fields
.field private final mCallbacks:Lorg/chromium/net/IRemoteAndroidKeyStoreCallbacks;

.field private final mConnection:Landroid/content/ServiceConnection;

.field private mContext:Landroid/content/Context;

.field private mIsBound:Z

.field private mIsEnabled:Z

.field private mKeyStoreRemote:Lorg/chromium/net/RemoteAndroidKeyStore;

.field private mRemoteIsReady:Z

.field private mRemoteManager:Lorg/chromium/net/IRemoteAndroidKeyStore;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    const-class v0, Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;->$assertionsDisabled:Z

    .line 62
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;->CONNECTION_LOCK:Ljava/lang/Object;

    return-void

    .line 37
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    new-instance v0, Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager$1;-><init>(Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;->mCallbacks:Lorg/chromium/net/IRemoteAndroidKeyStoreCallbacks;

    .line 88
    new-instance v0, Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager$2;-><init>(Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;->mConnection:Landroid/content/ServiceConnection;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;->disconnectFromRemote()V

    return-void
.end method

.method static synthetic access$100()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 37
    sget-object v0, Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;->CONNECTION_LOCK:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$202(Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;Z)Z
    .locals 0

    .prologue
    .line 37
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;->mRemoteIsReady:Z

    return p1
.end method

.method static synthetic access$300(Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;)Lorg/chromium/net/IRemoteAndroidKeyStore;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;->mRemoteManager:Lorg/chromium/net/IRemoteAndroidKeyStore;

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;Lorg/chromium/net/IRemoteAndroidKeyStore;)Lorg/chromium/net/IRemoteAndroidKeyStore;
    .locals 0

    .prologue
    .line 37
    iput-object p1, p0, Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;->mRemoteManager:Lorg/chromium/net/IRemoteAndroidKeyStore;

    return-object p1
.end method

.method static synthetic access$400(Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;)Lorg/chromium/net/IRemoteAndroidKeyStoreCallbacks;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;->mCallbacks:Lorg/chromium/net/IRemoteAndroidKeyStoreCallbacks;

    return-object v0
.end method

.method static synthetic access$502(Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;Lorg/chromium/net/RemoteAndroidKeyStore;)Lorg/chromium/net/RemoteAndroidKeyStore;
    .locals 0

    .prologue
    .line 37
    iput-object p1, p0, Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;->mKeyStoreRemote:Lorg/chromium/net/RemoteAndroidKeyStore;

    return-object p1
.end method

.method static synthetic access$600(Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;->clearState()V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;)Z
    .locals 1

    .prologue
    .line 37
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;->mIsEnabled:Z

    return v0
.end method

.method static synthetic access$702(Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;Z)Z
    .locals 0

    .prologue
    .line 37
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;->mIsEnabled:Z

    return p1
.end method

.method static synthetic access$800(Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;->bindToService()V

    return-void
.end method

.method private bindToService()V
    .locals 4

    .prologue
    .line 209
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 210
    iget-object v1, p0, Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/apps/chrome/deviceextras/DeviceSupportHelper;->getSupportPackageName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 211
    const-string/jumbo v1, "com.chrome.deviceextras.KnoxPKCS11Service"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 212
    iget-object v1, p0, Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;->mConnection:Landroid/content/ServiceConnection;

    const/4 v3, 0x1

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;->mIsBound:Z

    .line 213
    return-void
.end method

.method private clearState()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 224
    iput-boolean v0, p0, Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;->mIsBound:Z

    .line 225
    iput-object v1, p0, Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;->mRemoteManager:Lorg/chromium/net/IRemoteAndroidKeyStore;

    .line 226
    iput-object v1, p0, Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;->mKeyStoreRemote:Lorg/chromium/net/RemoteAndroidKeyStore;

    .line 227
    iput-boolean v0, p0, Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;->mRemoteIsReady:Z

    .line 228
    invoke-static {}, Lorg/chromium/chrome/browser/SSLClientCertificateRequest;->notifyClientCertificatesChangedOnIOThread()V

    .line 229
    return-void
.end method

.method private disconnectFromRemote()V
    .locals 2

    .prologue
    .line 216
    const-string/jumbo v0, "DelegatedPKCS11Manager"

    const-string/jumbo v1, "Disconnect from remote"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 217
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;->mIsBound:Z

    if-eqz v0, :cond_0

    .line 218
    iget-object v0, p0, Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;->mConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 220
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;->clearState()V

    .line 221
    return-void
.end method

.method private maybeWaitForRemoteServiceToInitialize()V
    .locals 4

    .prologue
    .line 143
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;->mIsEnabled:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;->mRemoteIsReady:Z

    if-nez v0, :cond_1

    .line 147
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;->mIsBound:Z

    if-nez v0, :cond_0

    .line 148
    invoke-direct {p0}, Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;->bindToService()V

    .line 150
    :cond_0
    sget-object v1, Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;->CONNECTION_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 151
    :try_start_0
    const-string/jumbo v0, "DelegatedPKCS11Manager"

    const-string/jumbo v2, "Waiting for Remote service to initialize."

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 153
    :try_start_1
    sget-object v0, Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;->CONNECTION_LOCK:Ljava/lang/Object;

    const-wide/16 v2, 0x4e20

    invoke-virtual {v0, v2, v3}, Ljava/lang/Object;->wait(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 157
    :goto_0
    :try_start_2
    monitor-exit v1

    .line 159
    :cond_1
    return-void

    .line 154
    :catch_0
    move-exception v0

    .line 155
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 157
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public getCertificateChain(Ljava/lang/String;)[Ljava/security/cert/X509Certificate;
    .locals 2

    .prologue
    .line 163
    sget-boolean v0, Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-static {}, Lorg/chromium/base/ThreadUtils;->runningOnUiThread()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 165
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;->mRemoteManager:Lorg/chromium/net/IRemoteAndroidKeyStore;

    invoke-interface {v0, p1}, Lorg/chromium/net/IRemoteAndroidKeyStore;->getEncodedCertificateChain(Ljava/lang/String;)[B

    move-result-object v0

    .line 166
    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-direct {v1, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 167
    new-instance v0, Ljava/io/ObjectInputStream;

    invoke-direct {v0, v1}, Ljava/io/ObjectInputStream;-><init>(Ljava/io/InputStream;)V

    .line 168
    invoke-virtual {v0}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/security/cert/X509Certificate;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 171
    :goto_0
    return-object v0

    .line 169
    :catch_0
    move-exception v0

    .line 170
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 171
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getClientCertificateAlias(Ljava/lang/String;I)Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 118
    sget-boolean v0, Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-static {}, Lorg/chromium/base/ThreadUtils;->runningOnUiThread()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 121
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;->maybeWaitForRemoteServiceToInitialize()V

    .line 123
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;->mContext:Landroid/content/Context;

    invoke-static {v0, p1, p2}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider;->getClientCertificateAlias(Landroid/content/Context;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 125
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 126
    iget-object v0, p0, Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;->mRemoteManager:Lorg/chromium/net/IRemoteAndroidKeyStore;

    if-nez v0, :cond_2

    .line 127
    const-string/jumbo v0, "DelegatedPKCS11Manager"

    const-string/jumbo v2, "Cannot get client certificate alias as remote is unavailable"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    .line 138
    :cond_1
    :goto_0
    return-object v0

    .line 133
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;->mRemoteManager:Lorg/chromium/net/IRemoteAndroidKeyStore;

    invoke-interface {v0}, Lorg/chromium/net/IRemoteAndroidKeyStore;->getClientCertificateAlias()Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 136
    :catch_0
    move-exception v0

    .line 137
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    move-object v0, v1

    .line 138
    goto :goto_0
.end method

.method public getPrivateKey(Ljava/lang/String;)Lorg/chromium/net/AndroidPrivateKey;
    .locals 2

    .prologue
    .line 177
    iget-object v0, p0, Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;->mKeyStoreRemote:Lorg/chromium/net/RemoteAndroidKeyStore;

    if-nez v0, :cond_0

    .line 178
    const-string/jumbo v0, "DelegatedPKCS11Manager"

    const-string/jumbo v1, "Cannot get private key as remote is unavailable"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 179
    const/4 v0, 0x0

    .line 181
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;->mKeyStoreRemote:Lorg/chromium/net/RemoteAndroidKeyStore;

    invoke-virtual {v0, p1}, Lorg/chromium/net/RemoteAndroidKeyStore;->createKey(Ljava/lang/String;)Lorg/chromium/net/AndroidPrivateKey;

    move-result-object v0

    goto :goto_0
.end method

.method public initialize(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 186
    sget-boolean v0, Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-static {}, Lorg/chromium/base/ThreadUtils;->runningOnUiThread()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 187
    :cond_0
    const-string/jumbo v0, "DelegatedPKCS11Manager"

    const-string/jumbo v1, "initialize"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 188
    iput-object p1, p0, Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;->mContext:Landroid/content/Context;

    .line 189
    invoke-static {p1}, Lcom/google/android/apps/chrome/deviceextras/DeviceSupportHelper;->isSupportPackageAvailable(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 190
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;->mIsEnabled:Z

    .line 206
    :goto_0
    return-void

    .line 193
    :cond_1
    invoke-static {p1}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager$3;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager$3;-><init>(Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings;->addObserver(Lcom/google/android/apps/chrome/knoxsettings/KnoxSettings$Observer;)V

    goto :goto_0
.end method

.method public isPKCS11AuthEnabled()Z
    .locals 1

    .prologue
    .line 113
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/oemsupport/DelegatedPKCS11Manager;->mIsEnabled:Z

    return v0
.end method
