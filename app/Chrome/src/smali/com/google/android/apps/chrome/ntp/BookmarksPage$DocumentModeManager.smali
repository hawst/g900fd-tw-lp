.class Lcom/google/android/apps/chrome/ntp/BookmarksPage$DocumentModeManager;
.super Lcom/google/android/apps/chrome/ntp/BookmarksPage$BookmarksPageManagerImpl;
.source "BookmarksPage.java"


# instance fields
.field private final mListener:Lcom/google/android/apps/chrome/ntp/BookmarksPage$BookmarkSelectedListener;

.field final synthetic this$0:Lcom/google/android/apps/chrome/ntp/BookmarksPage;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/chrome/ntp/BookmarksPage;Lorg/chromium/chrome/browser/Tab;Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;Lcom/google/android/apps/chrome/ntp/BookmarksPage$BookmarkSelectedListener;)V
    .locals 0

    .prologue
    .line 236
    iput-object p1, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPage$DocumentModeManager;->this$0:Lcom/google/android/apps/chrome/ntp/BookmarksPage;

    .line 237
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/chrome/ntp/BookmarksPage$BookmarksPageManagerImpl;-><init>(Lcom/google/android/apps/chrome/ntp/BookmarksPage;Lorg/chromium/chrome/browser/Tab;Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;)V

    .line 238
    iput-object p4, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPage$DocumentModeManager;->mListener:Lcom/google/android/apps/chrome/ntp/BookmarksPage$BookmarkSelectedListener;

    .line 239
    return-void
.end method


# virtual methods
.method public open(Lcom/google/android/apps/chrome/ntp/BookmarkItemView;)V
    .locals 4

    .prologue
    .line 255
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/ntp/BookmarkItemView;->isFolder()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 256
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPage$DocumentModeManager;->this$0:Lcom/google/android/apps/chrome/ntp/BookmarksPage;

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/ntp/BookmarkItemView;->getBookmarkId()Lorg/chromium/components/bookmarks/BookmarkId;

    move-result-object v1

    const/4 v2, 0x0

    # invokes: Lcom/google/android/apps/chrome/ntp/BookmarksPage;->updateBookmarksPageContents(Lorg/chromium/components/bookmarks/BookmarkId;Z)V
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/ntp/BookmarksPage;->access$400(Lcom/google/android/apps/chrome/ntp/BookmarksPage;Lorg/chromium/components/bookmarks/BookmarkId;Z)V

    .line 261
    :goto_0
    return-void

    .line 258
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ntp/BookmarksPage$DocumentModeManager;->recordOpenedBookmark()V

    .line 259
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPage$DocumentModeManager;->mListener:Lcom/google/android/apps/chrome/ntp/BookmarksPage$BookmarkSelectedListener;

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/ntp/BookmarkItemView;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/ntp/BookmarkItemView;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/ntp/BookmarkItemView;->getFavicon()Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/apps/chrome/ntp/BookmarksPage$BookmarkSelectedListener;->onBookmarkSelected(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method public openFolder(Lcom/google/android/apps/chrome/ntp/BookmarkFolderHierarchyItem;)V
    .locals 3

    .prologue
    .line 265
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPage$DocumentModeManager;->this$0:Lcom/google/android/apps/chrome/ntp/BookmarksPage;

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/ntp/BookmarkFolderHierarchyItem;->getFolderId()Lorg/chromium/components/bookmarks/BookmarkId;

    move-result-object v1

    const/4 v2, 0x0

    # invokes: Lcom/google/android/apps/chrome/ntp/BookmarksPage;->updateBookmarksPageContents(Lorg/chromium/components/bookmarks/BookmarkId;Z)V
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/ntp/BookmarksPage;->access$400(Lcom/google/android/apps/chrome/ntp/BookmarksPage;Lorg/chromium/components/bookmarks/BookmarkId;Z)V

    .line 266
    return-void
.end method

.method public openInNewIncognitoTab(Lcom/google/android/apps/chrome/ntp/BookmarkItemView;)V
    .locals 1

    .prologue
    .line 249
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/ntp/BookmarksPage$BookmarksPageManagerImpl;->openInNewIncognitoTab(Lcom/google/android/apps/chrome/ntp/BookmarkItemView;)V

    .line 250
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPage$DocumentModeManager;->mListener:Lcom/google/android/apps/chrome/ntp/BookmarksPage$BookmarkSelectedListener;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/ntp/BookmarksPage$BookmarkSelectedListener;->onNewTabOpened()V

    .line 251
    return-void
.end method

.method public openInNewTab(Lcom/google/android/apps/chrome/ntp/BookmarkItemView;)V
    .locals 1

    .prologue
    .line 243
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/ntp/BookmarksPage$BookmarksPageManagerImpl;->openInNewTab(Lcom/google/android/apps/chrome/ntp/BookmarkItemView;)V

    .line 244
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPage$DocumentModeManager;->mListener:Lcom/google/android/apps/chrome/ntp/BookmarksPage$BookmarkSelectedListener;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/ntp/BookmarksPage$BookmarkSelectedListener;->onNewTabOpened()V

    .line 245
    return-void
.end method
