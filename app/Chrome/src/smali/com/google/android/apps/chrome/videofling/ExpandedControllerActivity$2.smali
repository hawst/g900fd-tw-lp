.class Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity$2;
.super Ljava/lang/Object;
.source "ExpandedControllerActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;)V
    .locals 0

    .prologue
    .line 107
    iput-object p1, p0, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity$2;->this$0:Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 110
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity$2;->this$0:Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;

    # getter for: Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;->mRemoteMediaPlayerController:Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;
    invoke-static {v0}, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;->access$000(Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;)Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 111
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity$2;->this$0:Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;

    # getter for: Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;->mMediaController:Lcom/google/android/apps/chrome/third_party/MediaController;
    invoke-static {v0}, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;->access$200(Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;)Lcom/google/android/apps/chrome/third_party/MediaController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/third_party/MediaController;->updateProgress()J

    .line 112
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity$2;->this$0:Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;

    # getter for: Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;->access$300(Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;)Landroid/os/Handler;

    move-result-object v0

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, p0, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 116
    :goto_0
    return-void

    .line 114
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity$2;->this$0:Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;

    # getter for: Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;->access$300(Lcom/google/android/apps/chrome/videofling/ExpandedControllerActivity;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    goto :goto_0
.end method
