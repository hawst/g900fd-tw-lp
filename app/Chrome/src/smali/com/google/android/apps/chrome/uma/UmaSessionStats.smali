.class public Lcom/google/android/apps/chrome/uma/UmaSessionStats;
.super Ljava/lang/Object;
.source "UmaSessionStats.java"

# interfaces
.implements Lorg/chromium/net/NetworkChangeNotifier$ConnectionTypeObserver;


# static fields
.field private static final NOTIFICATIONS:[I

.field private static sNativeUmaSessionStats:J


# instance fields
.field private mComponentCallbacks:Landroid/content/ComponentCallbacks;

.field private final mContext:Landroid/content/Context;

.field private final mIsMultiWindowCapable:Z

.field private mKeyboardConnected:Z

.field private final mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

.field private final mReportingPermissionManager:Lcom/google/android/apps/chrome/crash/ReportingPermissionManagerImpl;

.field private mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 55
    const-wide/16 v0, 0x0

    sput-wide v0, Lcom/google/android/apps/chrome/uma/UmaSessionStats;->sNativeUmaSessionStats:J

    .line 70
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const/16 v2, 0x9

    aput v2, v0, v1

    sput-object v0, Lcom/google/android/apps/chrome/uma/UmaSessionStats;->NOTIFICATIONS:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    iput-object v2, p0, Lcom/google/android/apps/chrome/uma/UmaSessionStats;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    .line 65
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/uma/UmaSessionStats;->mKeyboardConnected:Z

    .line 75
    iput-object p1, p0, Lcom/google/android/apps/chrome/uma/UmaSessionStats;->mContext:Landroid/content/Context;

    .line 76
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string/jumbo v1, "com.sec.feature.multiwindow"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/uma/UmaSessionStats;->mIsMultiWindowCapable:Z

    .line 78
    new-instance v0, Lcom/google/android/apps/chrome/uma/UmaSessionStats$NotificationHandler;

    invoke-direct {v0, p0, v2}, Lcom/google/android/apps/chrome/uma/UmaSessionStats$NotificationHandler;-><init>(Lcom/google/android/apps/chrome/uma/UmaSessionStats;Lcom/google/android/apps/chrome/uma/UmaSessionStats$1;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/uma/UmaSessionStats;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    .line 79
    new-instance v0, Lcom/google/android/apps/chrome/crash/ReportingPermissionManagerImpl;

    invoke-direct {v0, p1}, Lcom/google/android/apps/chrome/crash/ReportingPermissionManagerImpl;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/uma/UmaSessionStats;->mReportingPermissionManager:Lcom/google/android/apps/chrome/crash/ReportingPermissionManagerImpl;

    .line 80
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/uma/UmaSessionStats;)Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/android/apps/chrome/uma/UmaSessionStats;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/uma/UmaSessionStats;I)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/uma/UmaSessionStats;->recordPageLoadStats(I)V

    return-void
.end method

.method static synthetic access$302(Lcom/google/android/apps/chrome/uma/UmaSessionStats;Z)Z
    .locals 0

    .prologue
    .line 32
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/uma/UmaSessionStats;->mKeyboardConnected:Z

    return p1
.end method

.method private getTabCountFromModel(Lorg/chromium/chrome/browser/tabmodel/TabModel;)I
    .locals 1

    .prologue
    .line 97
    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-interface {p1}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->getCount()I

    move-result v0

    goto :goto_0
.end method

.method public static logRendererCrash(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 166
    invoke-static {p0}, Lorg/chromium/base/ApplicationStatus;->getStateForActivity(Landroid/app/Activity;)I

    move-result v0

    .line 167
    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    const/4 v1, 0x5

    if-eq v0, v1, :cond_0

    const/4 v1, 0x6

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/apps/chrome/uma/UmaSessionStats;->nativeLogRendererCrash(Z)V

    .line 171
    return-void

    .line 167
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private native nativeInit()J
.end method

.method private static native nativeLogRendererCrash(Z)V
.end method

.method private static native nativeRegisterSyntheticFieldTrial(ILjava/lang/String;)V
.end method

.method private native nativeUmaEndSession(J)V
.end method

.method private native nativeUmaResumeSession(J)V
.end method

.method private native nativeUpdateMetricsServiceState(ZZ)V
.end method

.method private recordPageLoadStats(I)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 83
    iget-object v0, p0, Lcom/google/android/apps/chrome/uma/UmaSessionStats;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    invoke-interface {v0, p1}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->getTabById(I)Lorg/chromium/chrome/browser/Tab;

    move-result-object v0

    .line 84
    if-nez v0, :cond_0

    .line 94
    :goto_0
    return-void

    .line 85
    :cond_0
    invoke-virtual {v0}, Lorg/chromium/chrome/browser/Tab;->getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    .line 86
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->getWebContents()Lorg/chromium/content_public/browser/WebContents;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->getWebContents()Lorg/chromium/content_public/browser/WebContents;

    move-result-object v0

    invoke-interface {v0}, Lorg/chromium/content_public/browser/WebContents;->getNavigationController()Lorg/chromium/content_public/browser/NavigationController;

    move-result-object v0

    invoke-interface {v0}, Lorg/chromium/content_public/browser/NavigationController;->getUseDesktopUserAgent()Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    .line 90
    :goto_1
    iget-boolean v3, p0, Lcom/google/android/apps/chrome/uma/UmaSessionStats;->mKeyboardConnected:Z

    invoke-static {v3, v0}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->pageLoaded(ZZ)V

    .line 91
    iget-object v0, p0, Lcom/google/android/apps/chrome/uma/UmaSessionStats;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    invoke-interface {v0, v2}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->getModel(Z)Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/uma/UmaSessionStats;->getTabCountFromModel(Lorg/chromium/chrome/browser/tabmodel/TabModel;)I

    move-result v0

    iget-object v2, p0, Lcom/google/android/apps/chrome/uma/UmaSessionStats;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    invoke-interface {v2, v1}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->getModel(Z)Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/uma/UmaSessionStats;->getTabCountFromModel(Lorg/chromium/chrome/browser/tabmodel/TabModel;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->tabCountPerLoad(II)V

    goto :goto_0

    :cond_1
    move v0, v2

    .line 86
    goto :goto_1
.end method

.method public static registerSyntheticFieldTrial(ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 191
    invoke-static {p0, p1}, Lcom/google/android/apps/chrome/uma/UmaSessionStats;->nativeRegisterSyntheticFieldTrial(ILjava/lang/String;)V

    .line 192
    return-void
.end method


# virtual methods
.method public logAndEndSession()V
    .locals 3

    .prologue
    .line 153
    iget-object v0, p0, Lcom/google/android/apps/chrome/uma/UmaSessionStats;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    if-eqz v0, :cond_0

    .line 155
    iget-object v0, p0, Lcom/google/android/apps/chrome/uma/UmaSessionStats;->mContext:Landroid/content/Context;

    sget-object v1, Lcom/google/android/apps/chrome/uma/UmaSessionStats;->NOTIFICATIONS:[I

    iget-object v2, p0, Lcom/google/android/apps/chrome/uma/UmaSessionStats;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->unregisterForNotifications(Landroid/content/Context;[ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    .line 157
    iget-object v0, p0, Lcom/google/android/apps/chrome/uma/UmaSessionStats;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/chrome/uma/UmaSessionStats;->mComponentCallbacks:Landroid/content/ComponentCallbacks;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterComponentCallbacks(Landroid/content/ComponentCallbacks;)V

    .line 158
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/uma/UmaSessionStats;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    .line 161
    :cond_0
    sget-wide v0, Lcom/google/android/apps/chrome/uma/UmaSessionStats;->sNativeUmaSessionStats:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/uma/UmaSessionStats;->nativeUmaEndSession(J)V

    .line 162
    invoke-static {p0}, Lorg/chromium/net/NetworkChangeNotifier;->removeConnectionTypeObserver(Lorg/chromium/net/NetworkChangeNotifier$ConnectionTypeObserver;)V

    .line 163
    return-void
.end method

.method public logMultiWindowStats(III)V
    .locals 1

    .prologue
    .line 141
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/uma/UmaSessionStats;->mIsMultiWindowCapable:Z

    if-eqz v0, :cond_0

    .line 142
    if-nez p2, :cond_1

    .line 147
    :cond_0
    :goto_0
    return-void

    .line 143
    :cond_1
    mul-int/lit8 v0, p1, 0x64

    div-int/2addr v0, p2

    .line 144
    if-lez v0, :cond_2

    .line 145
    :goto_1
    invoke-static {v0, p3}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->multiWindowSession(II)V

    goto :goto_0

    .line 144
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public onConnectionTypeChanged(I)V
    .locals 0

    .prologue
    .line 187
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/uma/UmaSessionStats;->updateMetricsServiceState()V

    .line 188
    return-void
.end method

.method public startNewSession(Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;)V
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 108
    sget-wide v2, Lcom/google/android/apps/chrome/uma/UmaSessionStats;->sNativeUmaSessionStats:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 109
    invoke-direct {p0}, Lcom/google/android/apps/chrome/uma/UmaSessionStats;->nativeInit()J

    move-result-wide v2

    sput-wide v2, Lcom/google/android/apps/chrome/uma/UmaSessionStats;->sNativeUmaSessionStats:J

    .line 112
    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/chrome/uma/UmaSessionStats;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    .line 113
    iget-object v1, p0, Lcom/google/android/apps/chrome/uma/UmaSessionStats;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    if-eqz v1, :cond_1

    .line 114
    new-instance v1, Lcom/google/android/apps/chrome/uma/UmaSessionStats$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/uma/UmaSessionStats$1;-><init>(Lcom/google/android/apps/chrome/uma/UmaSessionStats;)V

    iput-object v1, p0, Lcom/google/android/apps/chrome/uma/UmaSessionStats;->mComponentCallbacks:Landroid/content/ComponentCallbacks;

    .line 125
    iget-object v1, p0, Lcom/google/android/apps/chrome/uma/UmaSessionStats;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/chrome/uma/UmaSessionStats;->mComponentCallbacks:Landroid/content/ComponentCallbacks;

    invoke-virtual {v1, v2}, Landroid/content/Context;->registerComponentCallbacks(Landroid/content/ComponentCallbacks;)V

    .line 126
    iget-object v1, p0, Lcom/google/android/apps/chrome/uma/UmaSessionStats;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->keyboard:I

    if-eq v1, v0, :cond_2

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/chrome/uma/UmaSessionStats;->mKeyboardConnected:Z

    .line 130
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/uma/UmaSessionStats;->mContext:Landroid/content/Context;

    sget-object v1, Lcom/google/android/apps/chrome/uma/UmaSessionStats;->NOTIFICATIONS:[I

    iget-object v2, p0, Lcom/google/android/apps/chrome/uma/UmaSessionStats;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->registerForNotifications(Landroid/content/Context;[ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    .line 132
    sget-wide v0, Lcom/google/android/apps/chrome/uma/UmaSessionStats;->sNativeUmaSessionStats:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/uma/UmaSessionStats;->nativeUmaResumeSession(J)V

    .line 133
    invoke-static {p0}, Lorg/chromium/net/NetworkChangeNotifier;->addConnectionTypeObserver(Lorg/chromium/net/NetworkChangeNotifier$ConnectionTypeObserver;)V

    .line 134
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/uma/UmaSessionStats;->updateMetricsServiceState()V

    .line 135
    return-void

    .line 126
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public updateMetricsServiceState()V
    .locals 2

    .prologue
    .line 177
    iget-object v0, p0, Lcom/google/android/apps/chrome/uma/UmaSessionStats;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->isNeverUploadCrashDump()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 179
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/uma/UmaSessionStats;->mReportingPermissionManager:Lcom/google/android/apps/chrome/crash/ReportingPermissionManagerImpl;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/crash/ReportingPermissionManagerImpl;->isUploadPermitted()Z

    move-result v1

    .line 182
    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/uma/UmaSessionStats;->nativeUpdateMetricsServiceState(ZZ)V

    .line 183
    return-void

    .line 177
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
