.class public Lcom/google/android/apps/chrome/videofling/RemoteMediaUtils;
.super Ljava/lang/Object;
.source "RemoteMediaUtils.java"


# static fields
.field public static final DASH_MEDIA:I = 0x26

.field public static final DEVICE_TYPE_CAST_GENERIC:I = 0x0

.field public static final DEVICE_TYPE_CAST_YOUTUBE:I = 0x1

.field public static final DEVICE_TYPE_COUNT:I = 0x3

.field public static final DEVICE_TYPE_NON_CAST_YOUTUBE:I = 0x2

.field public static final HLS_MEDIA:I = 0x16

.field public static final MPEG4_MEDIA:I = 0x1d

.field public static final SMOOTHSTREAM_MEDIA:I = 0x27

.field public static final UNKNOWN_MEDIA:I = 0x0

.field public static final YOUTUBE_APP_ID:Ljava/lang/String; = "233637DE"

.field public static final YT_MEDIA_ROUTE_CONTROL_CATEGORY:Ljava/lang/String; = "EXTERNAL_MDX_MEDIA_ROUTE_CONTROL_CATEGORY"

.field private static sTreatAllVideosAsYouTube:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 59
    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/apps/chrome/videofling/RemoteMediaUtils;->sTreatAllVideosAsYouTube:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getCurrentActivity()Lcom/google/android/apps/chrome/ChromeActivity;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 132
    invoke-static {}, Lorg/chromium/base/ApplicationStatus;->getLastTrackedFocusedActivity()Landroid/app/Activity;

    move-result-object v0

    .line 134
    if-nez v0, :cond_0

    .line 141
    :goto_0
    return-object v1

    .line 137
    :cond_0
    instance-of v2, v0, Lcom/google/android/apps/chrome/ChromeActivity;

    if-eqz v2, :cond_1

    .line 138
    check-cast v0, Lcom/google/android/apps/chrome/ChromeActivity;

    :goto_1
    move-object v1, v0

    .line 141
    goto :goto_0

    :cond_1
    move-object v0, v1

    goto :goto_1
.end method

.method public static getMediaType(Landroid/net/Uri;)I
    .locals 2

    .prologue
    .line 68
    invoke-virtual {p0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    .line 69
    const-string/jumbo v1, ".m3u8"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 70
    const/16 v0, 0x16

    .line 81
    :goto_0
    return v0

    .line 72
    :cond_0
    const-string/jumbo v1, ".mp4"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 73
    const/16 v0, 0x1d

    goto :goto_0

    .line 75
    :cond_1
    const-string/jumbo v1, ".mpd"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 76
    const/16 v0, 0x26

    goto :goto_0

    .line 78
    :cond_2
    const-string/jumbo v1, ".ism"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 79
    const/16 v0, 0x27

    goto :goto_0

    .line 81
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getTabById(I)Lorg/chromium/chrome/browser/Tab;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 163
    invoke-static {}, Lcom/google/android/apps/chrome/videofling/RemoteMediaUtils;->getCurrentActivity()Lcom/google/android/apps/chrome/ChromeActivity;

    move-result-object v1

    .line 164
    if-nez v1, :cond_1

    .line 172
    :cond_0
    :goto_0
    return-object v0

    .line 166
    :cond_1
    invoke-virtual {v1}, Lcom/google/android/apps/chrome/ChromeActivity;->getTabModelSelector()Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    move-result-object v1

    .line 167
    if-eqz v1, :cond_0

    .line 169
    invoke-interface {v1, p0}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->getTabById(I)Lorg/chromium/chrome/browser/Tab;

    move-result-object v1

    .line 170
    if-eqz v1, :cond_0

    move-object v0, v1

    .line 172
    goto :goto_0
.end method

.method public static handleVolumeKeyEvent(Lcom/google/android/apps/chrome/videofling/MediaRouteController;Landroid/view/KeyEvent;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 93
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->isBeingCast()Z

    move-result v2

    if-nez v2, :cond_0

    .line 106
    :goto_0
    return v0

    .line 95
    :cond_0
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    .line 96
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v3

    .line 98
    packed-switch v3, :pswitch_data_0

    goto :goto_0

    .line 103
    :pswitch_0
    if-nez v2, :cond_1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->setRemoteVolume(I)V

    :cond_1
    move v0, v1

    .line 104
    goto :goto_0

    .line 100
    :pswitch_1
    if-nez v2, :cond_2

    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->setRemoteVolume(I)V

    :cond_2
    move v0, v1

    .line 101
    goto :goto_0

    .line 98
    :pswitch_data_0
    .packed-switch 0x18
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static isEnhancedMedia(Landroid/net/Uri;)Z
    .locals 2

    .prologue
    .line 62
    invoke-static {p0}, Lcom/google/android/apps/chrome/videofling/RemoteMediaUtils;->getMediaType(Landroid/net/Uri;)I

    move-result v0

    .line 63
    const/16 v1, 0x16

    if-eq v0, v1, :cond_0

    const/16 v1, 0x26

    if-eq v0, v1, :cond_0

    const/16 v1, 0x27

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isYouTubeUrl(Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 117
    sget-boolean v2, Lcom/google/android/apps/chrome/videofling/RemoteMediaUtils;->sTreatAllVideosAsYouTube:Z

    if-eqz v2, :cond_1

    .line 118
    :cond_0
    :goto_0
    return v0

    :cond_1
    if-eqz p0, :cond_2

    const-string/jumbo v2, "http://www.youtube.com"

    invoke-static {p0, v2, v1}, Lorg/chromium/chrome/browser/UrlUtilities;->sameDomainOrHost(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_0

    const-string/jumbo v2, "http://www.youtube-nocookie.com"

    invoke-static {p0, v2, v1}, Lorg/chromium/chrome/browser/UrlUtilities;->sameDomainOrHost(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public static sanitizeUrl(Ljava/lang/String;)Ljava/lang/String;
    .locals 8

    .prologue
    .line 179
    :try_start_0
    new-instance v7, Ljava/net/URL;

    invoke-direct {v7, p0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 180
    new-instance v0, Ljava/net/URI;

    invoke-virtual {v7}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7}, Ljava/net/URL;->getUserInfo()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v7}, Ljava/net/URL;->getPort()I

    move-result v4

    invoke-virtual {v7}, Ljava/net/URL;->getPath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v7}, Ljava/net/URL;->getQuery()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7}, Ljava/net/URL;->getRef()Ljava/lang/String;

    move-result-object v7

    invoke-direct/range {v0 .. v7}, Ljava/net/URI;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    invoke-virtual {v0}, Ljava/net/URI;->toURL()Ljava/net/URL;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/URL;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 188
    :goto_0
    return-object v0

    .line 183
    :catch_0
    move-exception v0

    .line 184
    const-string/jumbo v1, "VideoFling"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "URISyntaxException "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 188
    :goto_1
    const-string/jumbo v0, ""

    goto :goto_0

    .line 185
    :catch_1
    move-exception v0

    .line 186
    const-string/jumbo v1, "VideoFling"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "MalformedURLException "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method static setTreatAllAsYouTube(Z)V
    .locals 0

    .prologue
    .line 197
    sput-boolean p0, Lcom/google/android/apps/chrome/videofling/RemoteMediaUtils;->sTreatAllVideosAsYouTube:Z

    .line 198
    return-void
.end method
