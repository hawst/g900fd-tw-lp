.class public Lcom/google/android/apps/chrome/ChromeAppMenuPropertiesDelegate;
.super Ljava/lang/Object;
.source "ChromeAppMenuPropertiesDelegate.java"

# interfaces
.implements Lorg/chromium/chrome/browser/appmenu/AppMenuPropertiesDelegate;


# instance fields
.field private final mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

.field private mReloadMenuItem:Landroid/view/MenuItem;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/chrome/ChromeActivity;)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p1, p0, Lcom/google/android/apps/chrome/ChromeAppMenuPropertiesDelegate;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    .line 43
    return-void
.end method

.method private disableEnableMenuItem(Landroid/view/Menu;IZZZ)V
    .locals 3

    .prologue
    .line 216
    const/4 v0, 0x0

    :goto_0
    invoke-interface {p1}, Landroid/view/Menu;->size()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 217
    invoke-interface {p1, v0}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 218
    invoke-interface {v1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    if-ne v2, p2, :cond_0

    invoke-interface {v1}, Landroid/view/MenuItem;->isVisible()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 219
    invoke-interface {v1, p3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 220
    invoke-interface {v1, p4}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 221
    if-eqz p5, :cond_1

    .line 222
    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ManagedPreferencesUtils;->getManagedByEnterpriseIconId()I

    move-result v2

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 216
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 224
    :cond_1
    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    goto :goto_1

    .line 228
    :cond_2
    return-void
.end method


# virtual methods
.method public getMenuThemeResourceId()I
    .locals 1

    .prologue
    .line 191
    sget v0, Lcom/google/android/apps/chrome/R$style;->OverflowMenuTheme:I

    return v0
.end method

.method public loadingStateChanged(Z)V
    .locals 2

    .prologue
    .line 199
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeAppMenuPropertiesDelegate;->mReloadMenuItem:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    .line 200
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeAppMenuPropertiesDelegate;->mReloadMenuItem:Landroid/view/MenuItem;

    invoke-interface {v0}, Landroid/view/MenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-eqz p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->setLevel(I)Z

    .line 203
    :cond_0
    return-void

    .line 200
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onMenuDismissed()V
    .locals 1

    .prologue
    .line 209
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/ChromeAppMenuPropertiesDelegate;->mReloadMenuItem:Landroid/view/MenuItem;

    .line 210
    return-void
.end method

.method public prepareMenu(Landroid/view/Menu;)V
    .locals 12

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 61
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeAppMenuPropertiesDelegate;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeActivity;->isInOverviewMode()Z

    move-result v3

    .line 62
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeAppMenuPropertiesDelegate;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeActivity;->getCurrentTabModel()Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v0

    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->isIncognito()Z

    move-result v5

    .line 63
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeAppMenuPropertiesDelegate;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeActivity;->getCurrentTab()Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v6

    .line 66
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeAppMenuPropertiesDelegate;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeActivity;->isTablet()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 67
    if-nez v3, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeAppMenuPropertiesDelegate;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeActivity;->getCurrentTabModel()Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v0

    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->getCount()I

    move-result v0

    if-eqz v0, :cond_6

    move v4, v1

    .line 68
    :goto_0
    if-eqz v3, :cond_7

    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeAppMenuPropertiesDelegate;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeActivity;->getCurrentTabModel()Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v0

    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->getCount()I

    move-result v0

    if-eqz v0, :cond_7

    move v3, v1

    .line 69
    :goto_1
    if-nez v4, :cond_8

    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeAppMenuPropertiesDelegate;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeActivity;->getCurrentTabModel()Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v0

    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->getCount()I

    move-result v0

    if-nez v0, :cond_8

    move v0, v1

    .line 76
    :goto_2
    sget v7, Lcom/google/android/apps/chrome/R$id;->PAGE_MENU:I

    invoke-interface {p1, v7, v4}, Landroid/view/Menu;->setGroupVisible(IZ)V

    .line 77
    sget v7, Lcom/google/android/apps/chrome/R$id;->OVERVIEW_MODE_MENU:I

    invoke-interface {p1, v7, v3}, Landroid/view/Menu;->setGroupVisible(IZ)V

    .line 78
    sget v7, Lcom/google/android/apps/chrome/R$id;->TABLET_EMPTY_MODE_MENU:I

    invoke-interface {p1, v7, v0}, Landroid/view/Menu;->setGroupVisible(IZ)V

    .line 80
    if-eqz v4, :cond_3

    if-eqz v6, :cond_3

    .line 81
    invoke-virtual {v6}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getUrl()Ljava/lang/String;

    move-result-object v0

    .line 82
    const-string/jumbo v4, "chrome://"

    invoke-virtual {v0, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string/jumbo v4, "chrome-native://"

    invoke-virtual {v0, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    :cond_0
    move v0, v1

    .line 86
    :goto_3
    sget v4, Lcom/google/android/apps/chrome/R$id;->icon_row_menu_id:I

    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v7

    iget-object v4, p0, Lcom/google/android/apps/chrome/ChromeAppMenuPropertiesDelegate;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/ChromeActivity;->isTablet()Z

    move-result v4

    if-nez v4, :cond_c

    move v4, v1

    :goto_4
    invoke-interface {v7, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 87
    iget-object v4, p0, Lcom/google/android/apps/chrome/ChromeAppMenuPropertiesDelegate;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/ChromeActivity;->isTablet()Z

    move-result v4

    if-nez v4, :cond_1

    .line 89
    sget v4, Lcom/google/android/apps/chrome/R$id;->forward_menu_id:I

    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    .line 90
    invoke-virtual {v6}, Lcom/google/android/apps/chrome/tab/ChromeTab;->canGoForward()Z

    move-result v7

    invoke-interface {v4, v7}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 92
    sget v4, Lcom/google/android/apps/chrome/R$id;->reload_menu_id:I

    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/chrome/ChromeAppMenuPropertiesDelegate;->mReloadMenuItem:Landroid/view/MenuItem;

    .line 93
    iget-object v4, p0, Lcom/google/android/apps/chrome/ChromeAppMenuPropertiesDelegate;->mReloadMenuItem:Landroid/view/MenuItem;

    sget v7, Lcom/google/android/apps/chrome/R$drawable;->btn_reload_stop:I

    invoke-interface {v4, v7}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 94
    iget-object v4, p0, Lcom/google/android/apps/chrome/ChromeAppMenuPropertiesDelegate;->mReloadMenuItem:Landroid/view/MenuItem;

    invoke-interface {v4}, Landroid/view/MenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v7

    invoke-virtual {v6}, Lcom/google/android/apps/chrome/tab/ChromeTab;->isLoading()Z

    move-result v4

    if-eqz v4, :cond_d

    move v4, v1

    :goto_5
    invoke-virtual {v7, v4}, Landroid/graphics/drawable/Drawable;->setLevel(I)Z

    .line 97
    sget v4, Lcom/google/android/apps/chrome/R$id;->bookmark_this_page_id:I

    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    .line 98
    invoke-static {}, Lorg/chromium/chrome/browser/BookmarksBridge;->isEditBookmarksEnabled()Z

    move-result v7

    invoke-interface {v4, v7}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 99
    invoke-virtual {v6}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getBookmarkId()J

    move-result-wide v8

    const-wide/16 v10, -0x1

    cmp-long v7, v8, v10

    if-eqz v7, :cond_e

    .line 100
    sget v7, Lcom/google/android/apps/chrome/R$drawable;->btn_star_filled:I

    invoke-interface {v4, v7}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 107
    :cond_1
    :goto_6
    sget v4, Lcom/google/android/apps/chrome/R$id;->recent_tabs_menu_id:I

    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v7

    .line 108
    if-nez v5, :cond_f

    iget-object v4, p0, Lcom/google/android/apps/chrome/ChromeAppMenuPropertiesDelegate;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    invoke-static {v4}, Lorg/chromium/chrome/browser/util/FeatureUtilities;->canAllowSync(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_f

    move v4, v1

    :goto_7
    invoke-interface {v7, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 109
    sget v4, Lcom/google/android/apps/chrome/R$string;->menu_recent_tabs:I

    invoke-interface {v7, v4}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    .line 112
    sget v4, Lcom/google/android/apps/chrome/R$id;->share_row_menu_id:I

    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v7

    if-nez v0, :cond_10

    move v4, v1

    :goto_8
    invoke-interface {v7, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 114
    iget-object v4, p0, Lcom/google/android/apps/chrome/ChromeAppMenuPropertiesDelegate;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    sget v7, Lcom/google/android/apps/chrome/R$id;->direct_share_menu_id:I

    invoke-interface {p1, v7}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v7

    invoke-static {v4, v7}, Lorg/chromium/chrome/browser/share/ShareHelper;->configureDirectShareMenuItem(Landroid/app/Activity;Landroid/view/MenuItem;)V

    .line 118
    sget v4, Lcom/google/android/apps/chrome/R$id;->find_in_page_id:I

    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    invoke-virtual {v6}, Lcom/google/android/apps/chrome/tab/ChromeTab;->supportsFinding()Z

    move-result v7

    invoke-interface {v4, v7}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 124
    sget v4, Lcom/google/android/apps/chrome/R$id;->add_to_homescreen_id:I

    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v7

    .line 125
    iget-object v4, p0, Lcom/google/android/apps/chrome/ChromeAppMenuPropertiesDelegate;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    invoke-static {v4}, Lorg/chromium/chrome/browser/BookmarkUtils;->isAddToHomeIntentSupported(Landroid/content/Context;)Z

    move-result v4

    .line 127
    if-eqz v4, :cond_11

    if-nez v0, :cond_11

    if-nez v5, :cond_11

    move v4, v1

    :goto_9
    invoke-interface {v7, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 132
    sget v4, Lcom/google/android/apps/chrome/R$id;->request_desktop_site_id:I

    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    .line 133
    if-eqz v0, :cond_2

    invoke-virtual {v6}, Lcom/google/android/apps/chrome/tab/ChromeTab;->isNativePage()Z

    move-result v0

    if-eqz v0, :cond_12

    :cond_2
    move v0, v1

    :goto_a
    invoke-interface {v4, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 134
    invoke-virtual {v6}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getUseDesktopUserAgent()Z

    move-result v0

    invoke-interface {v4, v0}, Landroid/view/MenuItem;->setChecked(Z)Landroid/view/MenuItem;

    .line 136
    sget v0, Lcom/google/android/apps/chrome/R$id;->print_id:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    .line 137
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeAppMenuPropertiesDelegate;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeActivity;->getChromeApplication()Lcom/google/android/apps/chrome/ChromeMobileApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeMobileApplication;->getPrintingController()Lorg/chromium/printing/PrintingController;

    move-result-object v7

    .line 139
    if-eqz v7, :cond_13

    invoke-virtual {v6}, Lcom/google/android/apps/chrome/tab/ChromeTab;->isNativePage()Z

    move-result v0

    if-nez v0, :cond_13

    move v0, v1

    :goto_b
    invoke-interface {v4, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 140
    if-eqz v7, :cond_14

    invoke-interface {v7}, Lorg/chromium/printing/PrintingController;->isBusy()Z

    move-result v0

    if-nez v0, :cond_14

    move v0, v1

    :goto_c
    invoke-interface {v4, v0}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 144
    sget v0, Lcom/google/android/apps/chrome/R$id;->reader_mode_id:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v7

    .line 145
    if-eqz v7, :cond_3

    .line 146
    invoke-static {}, Lorg/chromium/base/CommandLine;->getInstance()Lorg/chromium/base/CommandLine;

    move-result-object v0

    const-string/jumbo v4, "enable-dom-distiller"

    invoke-virtual {v0, v4}, Lorg/chromium/base/CommandLine;->hasSwitch(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_15

    invoke-virtual {v6}, Lcom/google/android/apps/chrome/tab/ChromeTab;->isNativePage()Z

    move-result v0

    if-nez v0, :cond_15

    move v4, v1

    .line 148
    :goto_d
    if-eqz v4, :cond_16

    invoke-static {}, Lcom/google/android/apps/chrome/utilities/ChromeBuildInfo;->isBetaBuild()Z

    move-result v0

    if-nez v0, :cond_16

    invoke-static {}, Lcom/google/android/apps/chrome/utilities/ChromeBuildInfo;->isStableBuild()Z

    move-result v0

    if-nez v0, :cond_16

    invoke-virtual {v6}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getUrl()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/components/dom_distiller/core/DomDistillerUrlUtils;->isUrlDistillable(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_16

    invoke-virtual {v6}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getUrl()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/components/dom_distiller/core/DomDistillerUrlUtils;->isDistilledPage(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_16

    move v0, v1

    :goto_e
    invoke-interface {v7, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 153
    sget v0, Lcom/google/android/apps/chrome/R$id;->reader_mode_prefs_id:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v7

    if-eqz v4, :cond_17

    invoke-virtual {v6}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getUrl()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/components/dom_distiller/core/DomDistillerUrlUtils;->isDistilledPage(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_17

    move v0, v1

    :goto_f
    invoke-interface {v7, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 158
    :cond_3
    if-eqz v3, :cond_4

    .line 159
    if-eqz v5, :cond_18

    .line 161
    sget v0, Lcom/google/android/apps/chrome/R$id;->close_all_tabs_menu_id:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 163
    sget v0, Lcom/google/android/apps/chrome/R$id;->close_all_incognito_tabs_menu_id:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 174
    :cond_4
    :goto_10
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeAppMenuPropertiesDelegate;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    invoke-static {v0}, Lcom/google/android/apps/chrome/utilities/FeatureUtilitiesInternal;->isDocumentMode(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_5

    if-eqz v6, :cond_5

    invoke-virtual {v6}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getUrl()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v3, "chrome-native://newtab/"

    invoke-static {v0, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    if-nez v5, :cond_1a

    :cond_5
    move v3, v1

    .line 182
    :goto_11
    sget v2, Lcom/google/android/apps/chrome/R$id;->new_incognito_tab_menu_id:I

    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->isIncognitoModeEnabled()Z

    move-result v4

    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->isIncognitoModeManaged()Z

    move-result v5

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/chrome/ChromeAppMenuPropertiesDelegate;->disableEnableMenuItem(Landroid/view/Menu;IZZZ)V

    .line 186
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeAppMenuPropertiesDelegate;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/ChromeActivity;->prepareMenu(Landroid/view/Menu;)V

    .line 187
    return-void

    :cond_6
    move v4, v2

    .line 67
    goto/16 :goto_0

    :cond_7
    move v3, v2

    .line 68
    goto/16 :goto_1

    :cond_8
    move v0, v2

    .line 69
    goto/16 :goto_2

    .line 71
    :cond_9
    if-nez v3, :cond_a

    move v0, v1

    :goto_12
    move v4, v0

    move v0, v2

    .line 73
    goto/16 :goto_2

    :cond_a
    move v0, v2

    .line 71
    goto :goto_12

    :cond_b
    move v0, v2

    .line 82
    goto/16 :goto_3

    :cond_c
    move v4, v2

    .line 86
    goto/16 :goto_4

    :cond_d
    move v4, v2

    .line 94
    goto/16 :goto_5

    .line 102
    :cond_e
    sget v7, Lcom/google/android/apps/chrome/R$drawable;->btn_star:I

    invoke-interface {v4, v7}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    goto/16 :goto_6

    :cond_f
    move v4, v2

    .line 108
    goto/16 :goto_7

    :cond_10
    move v4, v2

    .line 112
    goto/16 :goto_8

    :cond_11
    move v4, v2

    .line 127
    goto/16 :goto_9

    :cond_12
    move v0, v2

    .line 133
    goto/16 :goto_a

    :cond_13
    move v0, v2

    .line 139
    goto/16 :goto_b

    :cond_14
    move v0, v2

    .line 140
    goto/16 :goto_c

    :cond_15
    move v4, v2

    .line 146
    goto/16 :goto_d

    :cond_16
    move v0, v2

    .line 148
    goto/16 :goto_e

    :cond_17
    move v0, v2

    .line 153
    goto/16 :goto_f

    .line 166
    :cond_18
    sget v0, Lcom/google/android/apps/chrome/R$id;->close_all_incognito_tabs_menu_id:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 168
    sget v0, Lcom/google/android/apps/chrome/R$id;->close_all_tabs_menu_id:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeAppMenuPropertiesDelegate;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeActivity;->getTabModelSelector()Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    move-result-object v0

    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->getTotalTabCount()I

    move-result v0

    if-lez v0, :cond_19

    move v0, v1

    :goto_13
    invoke-interface {v3, v0}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto/16 :goto_10

    :cond_19
    move v0, v2

    goto :goto_13

    :cond_1a
    move v3, v2

    .line 174
    goto :goto_11
.end method

.method public shouldShowAppMenu()Z
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeAppMenuPropertiesDelegate;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeActivity;->shouldShowAppMenu()Z

    move-result v0

    return v0
.end method
