.class public abstract Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;
.super Ljava/lang/Object;
.source "LayoutDriver.java"

# interfaces
.implements Lcom/google/android/apps/chrome/tabs/layout/LayoutProvider;
.implements Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private mActiveLayout:Lcom/google/android/apps/chrome/tabs/layout/Layout;

.field private final mCachedRectF:Landroid/graphics/RectF;

.field private mContentContainer:Landroid/view/ViewGroup;

.field private mFullscreenToken:I

.field protected final mHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;

.field protected mLastContentHeightDp:F

.field protected mLastContentWidthDp:F

.field protected final mLastFullscreenViewportDp:Landroid/graphics/RectF;

.field private final mLastFullscreenViewportPx:Landroid/graphics/Rect;

.field protected mLastHeightMinusTopControlsDp:F

.field protected final mLastViewportDp:Landroid/graphics/RectF;

.field private final mLastViewportPx:Landroid/graphics/Rect;

.field protected final mLastVisibleViewportDp:Landroid/graphics/RectF;

.field private final mLastVisibleViewportPx:Landroid/graphics/Rect;

.field private mNextActiveLayout:Lcom/google/android/apps/chrome/tabs/layout/Layout;

.field protected final mPxToDp:F

.field private mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

.field private mUpdateRequested:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const-class v0, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->mFullscreenToken:I

    .line 52
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->mLastViewportPx:Landroid/graphics/Rect;

    .line 53
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->mLastVisibleViewportPx:Landroid/graphics/Rect;

    .line 54
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->mLastFullscreenViewportPx:Landroid/graphics/Rect;

    .line 55
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->mLastViewportDp:Landroid/graphics/RectF;

    .line 56
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->mLastVisibleViewportDp:Landroid/graphics/RectF;

    .line 57
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->mLastFullscreenViewportDp:Landroid/graphics/RectF;

    .line 63
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->mCachedRectF:Landroid/graphics/RectF;

    .line 70
    iput-object p1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->mHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;

    .line 71
    const/high16 v0, 0x3f800000    # 1.0f

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->mHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;

    invoke-interface {v1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    div-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->mPxToDp:F

    .line 73
    invoke-interface {p1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;->getWidth()I

    move-result v0

    .line 74
    invoke-interface {p1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;->getHeight()I

    move-result v1

    .line 75
    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->mLastViewportPx:Landroid/graphics/Rect;

    invoke-virtual {v2, v4, v4, v0, v1}, Landroid/graphics/Rect;->set(IIII)V

    .line 76
    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->mLastVisibleViewportPx:Landroid/graphics/Rect;

    invoke-virtual {v2, v4, v4, v0, v1}, Landroid/graphics/Rect;->set(IIII)V

    .line 77
    iget-object v2, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->mLastFullscreenViewportPx:Landroid/graphics/Rect;

    invoke-virtual {v2, v4, v4, v0, v1}, Landroid/graphics/Rect;->set(IIII)V

    .line 79
    int-to-float v0, v0

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->mPxToDp:F

    mul-float/2addr v0, v2

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->mLastContentWidthDp:F

    .line 80
    int-to-float v0, v1

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->mPxToDp:F

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->mLastContentHeightDp:F

    .line 81
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->mLastViewportDp:Landroid/graphics/RectF;

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->mLastContentWidthDp:F

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->mLastContentHeightDp:F

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/RectF;->set(FFFF)V

    .line 82
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->mLastVisibleViewportDp:Landroid/graphics/RectF;

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->mLastContentWidthDp:F

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->mLastContentHeightDp:F

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/RectF;->set(FFFF)V

    .line 83
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->mLastFullscreenViewportDp:Landroid/graphics/RectF;

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->mLastContentWidthDp:F

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->mLastContentHeightDp:F

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/RectF;->set(FFFF)V

    .line 85
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->mLastContentHeightDp:F

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->mLastHeightMinusTopControlsDp:F

    .line 86
    return-void
.end method

.method private getOrientation()I
    .locals 2

    .prologue
    .line 363
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->mLastContentWidthDp:F

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->mLastContentHeightDp:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 364
    const/4 v0, 0x2

    .line 366
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private propagateViewportToActiveLayout(Z)V
    .locals 3

    .prologue
    .line 351
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->mCachedRectF:Landroid/graphics/RectF;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->getViewportDp(Landroid/graphics/RectF;)Landroid/graphics/RectF;

    .line 353
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->mCachedRectF:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v0

    .line 354
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->mCachedRectF:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v1

    .line 355
    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->mLastContentWidthDp:F

    cmpl-float v2, v2, v0

    if-nez v2, :cond_0

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->mLastContentHeightDp:F

    cmpl-float v2, v2, v1

    if-nez v2, :cond_0

    if-eqz p1, :cond_1

    .line 356
    :cond_0
    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->mLastContentWidthDp:F

    .line 357
    iput v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->mLastContentHeightDp:F

    .line 358
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->mCachedRectF:Landroid/graphics/RectF;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->onViewportChanged(Landroid/graphics/RectF;)V

    .line 360
    :cond_1
    return-void
.end method

.method public static time()J
    .locals 2

    .prologue
    .line 92
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    return-wide v0
.end method


# virtual methods
.method public destroy()V
    .locals 0

    .prologue
    .line 143
    return-void
.end method

.method public doneHiding()V
    .locals 2

    .prologue
    .line 279
    sget-boolean v0, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->mNextActiveLayout:Lcom/google/android/apps/chrome/tabs/layout/Layout;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    const-string/jumbo v1, "Need to have a next active layout."

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 280
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->mNextActiveLayout:Lcom/google/android/apps/chrome/tabs/layout/Layout;

    if-eqz v0, :cond_1

    .line 281
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->mNextActiveLayout:Lcom/google/android/apps/chrome/tabs/layout/Layout;

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->startShowing(Lcom/google/android/apps/chrome/tabs/layout/Layout;Z)V

    .line 283
    :cond_1
    return-void
.end method

.method public getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;
    .locals 1

    .prologue
    .line 215
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->mActiveLayout:Lcom/google/android/apps/chrome/tabs/layout/Layout;

    return-object v0
.end method

.method protected abstract getDefaultLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;
.end method

.method public getFullscreenManager()Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;
    .locals 1

    .prologue
    .line 261
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->mHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->mHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;->getFullscreenManager()Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected getNextLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;
    .locals 1

    .prologue
    .line 210
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->mNextActiveLayout:Lcom/google/android/apps/chrome/tabs/layout/Layout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->mNextActiveLayout:Lcom/google/android/apps/chrome/tabs/layout/Layout;

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->getDefaultLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    goto :goto_0
.end method

.method protected getTabModelSelector()Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;
    .locals 1

    .prologue
    .line 201
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    return-object v0
.end method

.method public getViewportDp(Landroid/graphics/RectF;)Landroid/graphics/RectF;
    .locals 2

    .prologue
    .line 220
    if-nez p1, :cond_0

    new-instance p1, Landroid/graphics/RectF;

    invoke-direct {p1}, Landroid/graphics/RectF;-><init>()V

    .line 222
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    if-nez v0, :cond_1

    .line 223
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->mLastViewportDp:Landroid/graphics/RectF;

    invoke-virtual {p1, v0}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 236
    :goto_0
    return-object p1

    .line 227
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->getSizingFlags()I

    move-result v0

    .line 228
    and-int/lit16 v1, v0, 0x1000

    if-eqz v1, :cond_2

    .line 229
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->mLastFullscreenViewportDp:Landroid/graphics/RectF;

    invoke-virtual {p1, v0}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    goto :goto_0

    .line 230
    :cond_2
    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_3

    .line 231
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->mLastViewportDp:Landroid/graphics/RectF;

    invoke-virtual {p1, v0}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    goto :goto_0

    .line 233
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->mLastVisibleViewportDp:Landroid/graphics/RectF;

    invoke-virtual {p1, v0}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    goto :goto_0
.end method

.method public getViewportPixel(Landroid/graphics/Rect;)Landroid/graphics/Rect;
    .locals 2

    .prologue
    .line 241
    if-nez p1, :cond_0

    new-instance p1, Landroid/graphics/Rect;

    invoke-direct {p1}, Landroid/graphics/Rect;-><init>()V

    .line 243
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    if-nez v0, :cond_1

    .line 244
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->mLastViewportPx:Landroid/graphics/Rect;

    invoke-virtual {p1, v0}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 256
    :goto_0
    return-object p1

    .line 248
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->getSizingFlags()I

    move-result v0

    .line 249
    and-int/lit16 v1, v0, 0x1000

    if-eqz v1, :cond_2

    .line 250
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->mLastFullscreenViewportPx:Landroid/graphics/Rect;

    invoke-virtual {p1, v0}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    goto :goto_0

    .line 251
    :cond_2
    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_3

    .line 252
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->mLastViewportPx:Landroid/graphics/Rect;

    invoke-virtual {p1, v0}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    goto :goto_0

    .line 254
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->mLastVisibleViewportPx:Landroid/graphics/Rect;

    invoke-virtual {p1, v0}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    goto :goto_0
.end method

.method public init(Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreatorManager;Lcom/google/android/apps/chrome/compositor/TabContentManager;Landroid/view/ViewGroup;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManagementDelegate;)V
    .locals 2

    .prologue
    .line 133
    iput-object p1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    .line 134
    iput-object p4, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->mContentContainer:Landroid/view/ViewGroup;

    .line 136
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->mNextActiveLayout:Lcom/google/android/apps/chrome/tabs/layout/Layout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->mNextActiveLayout:Lcom/google/android/apps/chrome/tabs/layout/Layout;

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->startShowing(Lcom/google/android/apps/chrome/tabs/layout/Layout;Z)V

    .line 137
    :cond_0
    return-void
.end method

.method public isActiveLayout(Lcom/google/android/apps/chrome/tabs/layout/Layout;)Z
    .locals 1

    .prologue
    .line 347
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->mActiveLayout:Lcom/google/android/apps/chrome/tabs/layout/Layout;

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onUpdate()V
    .locals 4

    .prologue
    .line 100
    const-string/jumbo v0, "LayoutDriver:onUpdate"

    invoke-static {v0}, Lorg/chromium/base/TraceEvent;->begin(Ljava/lang/String;)V

    .line 101
    invoke-static {}, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->time()J

    move-result-wide v0

    const-wide/16 v2, 0x10

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->onUpdate(JJ)Z

    .line 102
    const-string/jumbo v0, "LayoutDriver:onUpdate"

    invoke-static {v0}, Lorg/chromium/base/TraceEvent;->end(Ljava/lang/String;)V

    .line 103
    return-void
.end method

.method public onUpdate(JJ)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 113
    iget-boolean v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->mUpdateRequested:Z

    if-nez v1, :cond_0

    .line 119
    :goto_0
    return v0

    .line 114
    :cond_0
    iput-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->mUpdateRequested:Z

    .line 115
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    .line 116
    if-eqz v0, :cond_1

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->onUpdate(JJ)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->isHiding()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 117
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->doneHiding()V

    .line 119
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->mUpdateRequested:Z

    goto :goto_0
.end method

.method protected onViewportChanged(Landroid/graphics/RectF;)V
    .locals 5

    .prologue
    .line 151
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 152
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    invoke-virtual {p1}, Landroid/graphics/RectF;->width()F

    move-result v1

    invoke-virtual {p1}, Landroid/graphics/RectF;->height()F

    move-result v2

    iget v3, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->mLastHeightMinusTopControlsDp:F

    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->getOrientation()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->sizeChanged(FFFI)V

    .line 155
    :cond_0
    return-void
.end method

.method public final pushNewViewport(Landroid/graphics/Rect;Landroid/graphics/Rect;I)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 167
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->mLastViewportPx:Landroid/graphics/Rect;

    invoke-virtual {v0, p1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 168
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->mLastVisibleViewportPx:Landroid/graphics/Rect;

    invoke-virtual {v0, p2}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 170
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->mLastViewportDp:Landroid/graphics/RectF;

    iget v1, p1, Landroid/graphics/Rect;->left:I

    int-to-float v1, v1

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->mPxToDp:F

    mul-float/2addr v1, v2

    iget v2, p1, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    iget v3, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->mPxToDp:F

    mul-float/2addr v2, v3

    iget v3, p1, Landroid/graphics/Rect;->right:I

    int-to-float v3, v3

    iget v4, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->mPxToDp:F

    mul-float/2addr v3, v4

    iget v4, p1, Landroid/graphics/Rect;->bottom:I

    int-to-float v4, v4

    iget v5, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->mPxToDp:F

    mul-float/2addr v4, v5

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;->set(FFFF)V

    .line 172
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->mLastVisibleViewportDp:Landroid/graphics/RectF;

    iget v1, p2, Landroid/graphics/Rect;->left:I

    int-to-float v1, v1

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->mPxToDp:F

    mul-float/2addr v1, v2

    iget v2, p2, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    iget v3, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->mPxToDp:F

    mul-float/2addr v2, v3

    iget v3, p2, Landroid/graphics/Rect;->right:I

    int-to-float v3, v3

    iget v4, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->mPxToDp:F

    mul-float/2addr v3, v4

    iget v4, p2, Landroid/graphics/Rect;->bottom:I

    int-to-float v4, v4

    iget v5, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->mPxToDp:F

    mul-float/2addr v4, v5

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;->set(FFFF)V

    .line 174
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->mLastFullscreenViewportDp:Landroid/graphics/RectF;

    iget v1, p1, Landroid/graphics/Rect;->right:I

    int-to-float v1, v1

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->mPxToDp:F

    mul-float/2addr v1, v2

    iget v2, p1, Landroid/graphics/Rect;->bottom:I

    int-to-float v2, v2

    iget v3, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->mPxToDp:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v6, v6, v1, v2}, Landroid/graphics/RectF;->set(FFFF)V

    .line 175
    int-to-float v0, p3

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->mPxToDp:F

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->mLastHeightMinusTopControlsDp:F

    .line 177
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->propagateViewportToActiveLayout(Z)V

    .line 178
    return-void
.end method

.method public requestUpdate()V
    .locals 1

    .prologue
    .line 266
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->mUpdateRequested:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->mHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;->requestRender()V

    .line 267
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->mUpdateRequested:Z

    .line 268
    return-void
.end method

.method public setNextLayout(Lcom/google/android/apps/chrome/tabs/layout/Layout;)V
    .locals 0

    .prologue
    .line 342
    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->getDefaultLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object p1

    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->mNextActiveLayout:Lcom/google/android/apps/chrome/tabs/layout/Layout;

    .line 343
    return-void
.end method

.method public startHiding(I)V
    .locals 0

    .prologue
    .line 272
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->requestUpdate()V

    .line 273
    return-void
.end method

.method protected startShowing(Lcom/google/android/apps/chrome/tabs/layout/Layout;Z)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 295
    sget-boolean v0, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    const-string/jumbo v1, "init() must be called first."

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 296
    :cond_0
    sget-boolean v0, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    if-nez p1, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    const-string/jumbo v1, "Can\'t show a null layout."

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 299
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->setNextLayout(Lcom/google/android/apps/chrome/tabs/layout/Layout;)V

    .line 300
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    .line 301
    if-eq v0, p1, :cond_3

    .line 302
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->detachViews()V

    .line 303
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->mHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->contextChanged(Landroid/content/Context;)V

    .line 304
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->mContentContainer:Landroid/view/ViewGroup;

    invoke-virtual {p1, v0}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->attachViews(Landroid/view/ViewGroup;)V

    .line 305
    iput-object p1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->mActiveLayout:Lcom/google/android/apps/chrome/tabs/layout/Layout;

    .line 308
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->mHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;->getFullscreenManager()Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;

    move-result-object v2

    .line 309
    if-eqz v2, :cond_5

    .line 311
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->mFullscreenToken:I

    invoke-virtual {v2, v0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->hideControlsPersistent(I)V

    .line 312
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->mFullscreenToken:I

    .line 315
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->getSizingFlags()I

    move-result v0

    .line 316
    and-int/lit8 v3, v0, 0x1

    if-nez v3, :cond_4

    .line 317
    invoke-virtual {v2}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->showControlsPersistent()I

    move-result v3

    iput v3, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->mFullscreenToken:I

    .line 321
    :cond_4
    if-ne v0, v1, :cond_6

    move v0, v1

    :goto_0
    invoke-virtual {v2, v0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->setTopControlsPermamentlyHidden(Z)V

    .line 325
    :cond_5
    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->propagateViewportToActiveLayout(Z)V

    .line 326
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    invoke-static {}, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->time()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3, p2}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->show(JZ)V

    .line 327
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->mHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->getEventFilter()Lcom/google/android/apps/chrome/eventfilter/EventFilter;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;->setEventFilter(Lcom/google/android/apps/chrome/eventfilter/EventFilter;)V

    .line 328
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->mHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->shouldDisplayContentOverlay()Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;->setContentOverlayVisibility(Z)V

    .line 329
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->mHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;->requestRender()V

    .line 330
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->mHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutDriver;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->shouldDisplayContentOverlay()Z

    move-result v0

    if-eqz v0, :cond_7

    const/16 v0, 0x35

    :goto_1
    invoke-static {v1, v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastNotification(Landroid/content/Context;I)V

    .line 334
    return-void

    .line 321
    :cond_6
    const/4 v0, 0x0

    goto :goto_0

    .line 330
    :cond_7
    const/16 v0, 0x34

    goto :goto_1
.end method
