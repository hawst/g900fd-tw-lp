.class Lcom/google/android/apps/chrome/preferences/HomepagePreferences$2;
.super Ljava/lang/Object;
.source "HomepagePreferences.java"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/preferences/HomepagePreferences;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/preferences/HomepagePreferences;)V
    .locals 0

    .prologue
    .line 62
    iput-object p1, p0, Lcom/google/android/apps/chrome/preferences/HomepagePreferences$2;->this$0:Lcom/google/android/apps/chrome/preferences/HomepagePreferences;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 2

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/HomepagePreferences$2;->this$0:Lcom/google/android/apps/chrome/preferences/HomepagePreferences;

    # getter for: Lcom/google/android/apps/chrome/preferences/HomepagePreferences;->mCustomUriEditText:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/google/android/apps/chrome/preferences/HomepagePreferences;->access$100(Lcom/google/android/apps/chrome/preferences/HomepagePreferences;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 74
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/HomepagePreferences$2;->this$0:Lcom/google/android/apps/chrome/preferences/HomepagePreferences;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/google/android/apps/chrome/preferences/HomepagePreferences;->mCustomUriCache:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/preferences/HomepagePreferences;->access$202(Lcom/google/android/apps/chrome/preferences/HomepagePreferences;Ljava/lang/String;)Ljava/lang/String;

    .line 76
    :cond_0
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 69
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 65
    return-void
.end method
