.class Lcom/google/android/apps/chrome/videofling/ChromeMediaRouteDialogFactory$2;
.super Landroid/support/v7/app/u;
.source "ChromeMediaRouteDialogFactory.java"


# instance fields
.field final mVisibilitySaver:Lcom/google/android/apps/chrome/videofling/ChromeMediaRouteDialogFactory$SystemVisibilitySaver;

.field final synthetic this$0:Lcom/google/android/apps/chrome/videofling/ChromeMediaRouteDialogFactory;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/videofling/ChromeMediaRouteDialogFactory;)V
    .locals 2

    .prologue
    .line 91
    iput-object p1, p0, Lcom/google/android/apps/chrome/videofling/ChromeMediaRouteDialogFactory$2;->this$0:Lcom/google/android/apps/chrome/videofling/ChromeMediaRouteDialogFactory;

    invoke-direct {p0}, Landroid/support/v7/app/u;-><init>()V

    .line 92
    new-instance v0, Lcom/google/android/apps/chrome/videofling/ChromeMediaRouteDialogFactory$SystemVisibilitySaver;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/apps/chrome/videofling/ChromeMediaRouteDialogFactory$SystemVisibilitySaver;-><init>(Lcom/google/android/apps/chrome/videofling/ChromeMediaRouteDialogFactory$1;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/videofling/ChromeMediaRouteDialogFactory$2;->mVisibilitySaver:Lcom/google/android/apps/chrome/videofling/ChromeMediaRouteDialogFactory$SystemVisibilitySaver;

    return-void
.end method


# virtual methods
.method public onCreateChooserDialog(Landroid/content/Context;Landroid/os/Bundle;)Landroid/support/v7/app/q;
    .locals 2

    .prologue
    .line 97
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    .line 98
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/ChromeMediaRouteDialogFactory$2;->mVisibilitySaver:Lcom/google/android/apps/chrome/videofling/ChromeMediaRouteDialogFactory$SystemVisibilitySaver;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/ChromeMediaRouteDialogFactory$2;->getActivity()Landroid/support/v4/app/k;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/videofling/ChromeMediaRouteDialogFactory$SystemVisibilitySaver;->saveSystemVisibility(Landroid/app/Activity;)V

    .line 100
    :cond_0
    new-instance v0, Lcom/google/android/apps/chrome/videofling/ChromeMediaRouteChooserDialog;

    invoke-direct {v0, p1}, Lcom/google/android/apps/chrome/videofling/ChromeMediaRouteChooserDialog;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 105
    invoke-super {p0}, Landroid/support/v7/app/u;->onStop()V

    .line 106
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    .line 107
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/ChromeMediaRouteDialogFactory$2;->mVisibilitySaver:Lcom/google/android/apps/chrome/videofling/ChromeMediaRouteDialogFactory$SystemVisibilitySaver;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/ChromeMediaRouteDialogFactory$2;->getActivity()Landroid/support/v4/app/k;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/videofling/ChromeMediaRouteDialogFactory$SystemVisibilitySaver;->restoreSystemVisibility(Landroid/app/Activity;)V

    .line 109
    :cond_0
    return-void
.end method
