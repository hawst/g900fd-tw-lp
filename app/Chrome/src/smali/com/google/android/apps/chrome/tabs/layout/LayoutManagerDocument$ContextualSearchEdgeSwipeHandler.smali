.class Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument$ContextualSearchEdgeSwipeHandler;
.super Lcom/google/android/apps/chrome/tabs/layout/EdgeSwipeHandlerLayoutDelegate;
.source "LayoutManagerDocument.java"


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;Lcom/google/android/apps/chrome/tabs/layout/LayoutProvider;)V
    .locals 0

    .prologue
    .line 286
    iput-object p1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument$ContextualSearchEdgeSwipeHandler;->this$0:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;

    .line 287
    invoke-direct {p0, p2}, Lcom/google/android/apps/chrome/tabs/layout/EdgeSwipeHandlerLayoutDelegate;-><init>(Lcom/google/android/apps/chrome/tabs/layout/LayoutProvider;)V

    .line 288
    return-void
.end method

.method private isCompatabilityMode()Z
    .locals 1

    .prologue
    .line 310
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument$ContextualSearchEdgeSwipeHandler;->this$0:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;

    # getter for: Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;->mContextualSearchDelegate:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManagementDelegate;
    invoke-static {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;->access$100(Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;)Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManagementDelegate;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument$ContextualSearchEdgeSwipeHandler;->this$0:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;

    # getter for: Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;->mContextualSearchDelegate:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManagementDelegate;
    invoke-static {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;->access$100(Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;)Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManagementDelegate;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManagementDelegate;->isRunningInCompatibilityMode()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public isSwipeEnabled(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;)Z
    .locals 1

    .prologue
    .line 306
    sget-object v0, Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;->UP:Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument$ContextualSearchEdgeSwipeHandler;->this$0:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;

    # getter for: Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;->mContextualSearchDelegate:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManagementDelegate;
    invoke-static {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;->access$100(Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;)Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManagementDelegate;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public swipeStarted(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;FF)V
    .locals 2

    .prologue
    .line 292
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument$ContextualSearchEdgeSwipeHandler;->isCompatabilityMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 293
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument$ContextualSearchEdgeSwipeHandler;->this$0:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;

    # getter for: Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;->mContextualSearchDelegate:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManagementDelegate;
    invoke-static {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;->access$100(Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;)Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManagementDelegate;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManagementDelegate;->openResolvedSearchUrlInNewTab()V

    .line 302
    :goto_0
    return-void

    .line 297
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument$ContextualSearchEdgeSwipeHandler;->this$0:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;->getActiveLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument$ContextualSearchEdgeSwipeHandler;->this$0:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;

    iget-object v1, v1, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;->mContextualSearchLayout:Lcom/google/android/apps/chrome/tabs/layout/phone/ContextualSearchLayout;

    if-eq v0, v1, :cond_1

    .line 298
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument$ContextualSearchEdgeSwipeHandler;->this$0:Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;

    const/4 v1, 0x0

    # invokes: Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;->showContextualSearchLayout(Z)V
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;->access$200(Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;Z)V

    .line 301
    :cond_1
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/chrome/tabs/layout/EdgeSwipeHandlerLayoutDelegate;->swipeStarted(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;FF)V

    goto :goto_0
.end method
