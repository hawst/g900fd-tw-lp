.class public Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma;
.super Ljava/lang/Object;
.source "ContextualSearchUma.java"


# static fields
.field private static final ENTER_CLOSED_STATE_CHANGE_CODES:Ljava/util/Map;

.field private static final ENTER_EXPANDED_STATE_CHANGE_CODES:Ljava/util/Map;

.field private static final ENTER_MAXIMIZED_STATE_CHANGE_CODES:Ljava/util/Map;

.field private static final ENTER_PEEKED_STATE_CHANGE_CODES:Ljava/util/Map;

.field private static final EXIT_CLOSED_TO_STATE_CHANGE_CODES:Ljava/util/Map;

.field private static final EXIT_EXPANDED_TO_STATE_CHANGE_CODES:Ljava/util/Map;

.field private static final EXIT_MAXIMIZED_TO_STATE_CHANGE_CODES:Ljava/util/Map;

.field private static final EXIT_PEEKED_TO_STATE_CHANGE_CODES:Ljava/util/Map;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x5

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 160
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 161
    new-instance v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;

    sget-object v2, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->PEEKED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    sget-object v3, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->BACK_PRESS:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;-><init>(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;)V

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 163
    new-instance v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;

    sget-object v2, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->PEEKED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    sget-object v3, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->BASE_PAGE_SCROLL:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;-><init>(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;)V

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 165
    new-instance v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;

    sget-object v2, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->PEEKED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    sget-object v3, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->TEXT_SELECT_TAP:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;-><init>(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;)V

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 167
    new-instance v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;

    sget-object v2, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->EXPANDED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    sget-object v3, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->BACK_PRESS:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;-><init>(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;)V

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 169
    new-instance v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;

    sget-object v2, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->EXPANDED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    sget-object v3, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->BASE_PAGE_TAP:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;-><init>(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;)V

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 171
    new-instance v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;

    sget-object v2, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->EXPANDED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    sget-object v3, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->FLING:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;-><init>(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;)V

    const/4 v2, 0x6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 173
    new-instance v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;

    sget-object v2, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->MAXIMIZED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    sget-object v3, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->BACK_PRESS:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;-><init>(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;)V

    const/4 v2, 0x7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 175
    new-instance v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;

    sget-object v2, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->MAXIMIZED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    sget-object v3, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->FLING:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;-><init>(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;)V

    const/16 v2, 0x8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 177
    new-instance v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;

    sget-object v2, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->MAXIMIZED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    sget-object v3, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->TAB_PROMOTION:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;-><init>(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;)V

    const/16 v2, 0x9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 179
    new-instance v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;

    sget-object v2, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->MAXIMIZED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    sget-object v3, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->SERP_NAVIGATION:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;-><init>(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;)V

    const/16 v2, 0xa

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 181
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma;->ENTER_CLOSED_STATE_CHANGE_CODES:Ljava/util/Map;

    .line 187
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 189
    new-instance v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;

    sget-object v2, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->UNDEFINED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    sget-object v3, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->TEXT_SELECT_TAP:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;-><init>(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;)V

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 191
    new-instance v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;

    sget-object v2, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->UNDEFINED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    sget-object v3, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->TEXT_SELECT_LONG_PRESS:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;-><init>(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;)V

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 194
    new-instance v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;

    sget-object v2, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->CLOSED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    sget-object v3, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->TEXT_SELECT_TAP:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;-><init>(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;)V

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 196
    new-instance v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;

    sget-object v2, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->CLOSED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    sget-object v3, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->TEXT_SELECT_LONG_PRESS:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;-><init>(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;)V

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 198
    new-instance v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;

    sget-object v2, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->PEEKED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    sget-object v3, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->TEXT_SELECT_TAP:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;-><init>(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;)V

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 200
    new-instance v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;

    sget-object v2, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->PEEKED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    sget-object v3, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->TEXT_SELECT_LONG_PRESS:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;-><init>(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;)V

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 202
    new-instance v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;

    sget-object v2, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->EXPANDED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    sget-object v3, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->SEARCH_BAR_TAP:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;-><init>(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;)V

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 204
    new-instance v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;

    sget-object v2, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->EXPANDED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    sget-object v3, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->SWIPE:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;-><init>(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;)V

    const/4 v2, 0x6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 206
    new-instance v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;

    sget-object v2, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->EXPANDED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    sget-object v3, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->FLING:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;-><init>(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;)V

    const/4 v2, 0x7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208
    new-instance v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;

    sget-object v2, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->MAXIMIZED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    sget-object v3, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->SWIPE:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;-><init>(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;)V

    const/16 v2, 0x8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 210
    new-instance v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;

    sget-object v2, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->MAXIMIZED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    sget-object v3, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->FLING:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;-><init>(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;)V

    const/16 v2, 0x9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 212
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma;->ENTER_PEEKED_STATE_CHANGE_CODES:Ljava/util/Map;

    .line 218
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 219
    new-instance v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;

    sget-object v2, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->PEEKED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    sget-object v3, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->SEARCH_BAR_TAP:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;-><init>(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;)V

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 221
    new-instance v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;

    sget-object v2, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->PEEKED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    sget-object v3, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->SWIPE:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;-><init>(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;)V

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 223
    new-instance v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;

    sget-object v2, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->PEEKED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    sget-object v3, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->FLING:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;-><init>(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;)V

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 225
    new-instance v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;

    sget-object v2, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->MAXIMIZED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    sget-object v3, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->SWIPE:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;-><init>(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;)V

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 227
    new-instance v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;

    sget-object v2, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->MAXIMIZED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    sget-object v3, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->FLING:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;-><init>(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;)V

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 229
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma;->ENTER_EXPANDED_STATE_CHANGE_CODES:Ljava/util/Map;

    .line 235
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 236
    new-instance v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;

    sget-object v2, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->PEEKED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    sget-object v3, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->SWIPE:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;-><init>(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;)V

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 238
    new-instance v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;

    sget-object v2, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->PEEKED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    sget-object v3, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->FLING:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;-><init>(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;)V

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 240
    new-instance v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;

    sget-object v2, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->EXPANDED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    sget-object v3, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->SWIPE:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;-><init>(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;)V

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 242
    new-instance v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;

    sget-object v2, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->EXPANDED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    sget-object v3, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->FLING:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;-><init>(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;)V

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 244
    new-instance v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;

    sget-object v2, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->EXPANDED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    sget-object v3, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->SERP_NAVIGATION:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;-><init>(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;)V

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 246
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma;->ENTER_MAXIMIZED_STATE_CHANGE_CODES:Ljava/util/Map;

    .line 252
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 253
    new-instance v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;

    sget-object v2, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->PEEKED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    sget-object v3, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->TEXT_SELECT_TAP:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;-><init>(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;)V

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 255
    new-instance v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;

    sget-object v2, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->PEEKED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    sget-object v3, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->TEXT_SELECT_LONG_PRESS:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;-><init>(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;)V

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 257
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma;->EXIT_CLOSED_TO_STATE_CHANGE_CODES:Ljava/util/Map;

    .line 263
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 264
    new-instance v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;

    sget-object v2, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->CLOSED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    sget-object v3, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->BACK_PRESS:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;-><init>(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;)V

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 266
    new-instance v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;

    sget-object v2, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->CLOSED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    sget-object v3, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->BASE_PAGE_SCROLL:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;-><init>(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;)V

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 268
    new-instance v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;

    sget-object v2, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->CLOSED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    sget-object v3, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->BASE_PAGE_TAP:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;-><init>(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;)V

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 270
    new-instance v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;

    sget-object v2, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->PEEKED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    sget-object v3, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->TEXT_SELECT_TAP:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;-><init>(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;)V

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 272
    new-instance v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;

    sget-object v2, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->PEEKED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    sget-object v3, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->TEXT_SELECT_LONG_PRESS:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;-><init>(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;)V

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 274
    new-instance v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;

    sget-object v2, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->EXPANDED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    sget-object v3, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->SEARCH_BAR_TAP:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;-><init>(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;)V

    const/4 v2, 0x6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 276
    new-instance v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;

    sget-object v2, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->EXPANDED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    sget-object v3, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->SWIPE:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;-><init>(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;)V

    const/4 v2, 0x7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 278
    new-instance v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;

    sget-object v2, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->EXPANDED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    sget-object v3, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->FLING:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;-><init>(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;)V

    const/16 v2, 0x8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 280
    new-instance v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;

    sget-object v2, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->MAXIMIZED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    sget-object v3, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->SWIPE:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;-><init>(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;)V

    const/16 v2, 0x9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 282
    new-instance v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;

    sget-object v2, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->MAXIMIZED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    sget-object v3, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->FLING:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;-><init>(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;)V

    const/16 v2, 0xa

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 284
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma;->EXIT_PEEKED_TO_STATE_CHANGE_CODES:Ljava/util/Map;

    .line 290
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 291
    new-instance v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;

    sget-object v2, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->CLOSED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    sget-object v3, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->BACK_PRESS:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;-><init>(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;)V

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 293
    new-instance v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;

    sget-object v2, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->CLOSED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    sget-object v3, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->BASE_PAGE_TAP:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;-><init>(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;)V

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 295
    new-instance v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;

    sget-object v2, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->CLOSED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    sget-object v3, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->FLING:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;-><init>(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;)V

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 297
    new-instance v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;

    sget-object v2, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->PEEKED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    sget-object v3, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->SEARCH_BAR_TAP:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;-><init>(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;)V

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 299
    new-instance v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;

    sget-object v2, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->PEEKED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    sget-object v3, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->SWIPE:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;-><init>(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;)V

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 301
    new-instance v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;

    sget-object v2, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->PEEKED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    sget-object v3, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->FLING:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;-><init>(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;)V

    const/4 v2, 0x6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 303
    new-instance v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;

    sget-object v2, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->MAXIMIZED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    sget-object v3, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->SWIPE:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;-><init>(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;)V

    const/4 v2, 0x7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 305
    new-instance v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;

    sget-object v2, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->MAXIMIZED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    sget-object v3, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->FLING:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;-><init>(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;)V

    const/16 v2, 0x8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 307
    new-instance v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;

    sget-object v2, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->MAXIMIZED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    sget-object v3, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->SERP_NAVIGATION:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;-><init>(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;)V

    const/16 v2, 0x9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 309
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma;->EXIT_EXPANDED_TO_STATE_CHANGE_CODES:Ljava/util/Map;

    .line 315
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 316
    new-instance v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;

    sget-object v2, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->CLOSED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    sget-object v3, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->BACK_PRESS:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;-><init>(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;)V

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 318
    new-instance v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;

    sget-object v2, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->CLOSED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    sget-object v3, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->FLING:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;-><init>(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;)V

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 320
    new-instance v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;

    sget-object v2, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->CLOSED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    sget-object v3, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->TAB_PROMOTION:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;-><init>(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;)V

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 322
    new-instance v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;

    sget-object v2, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->CLOSED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    sget-object v3, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->SERP_NAVIGATION:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;-><init>(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;)V

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 324
    new-instance v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;

    sget-object v2, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->PEEKED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    sget-object v3, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->SWIPE:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;-><init>(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;)V

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 326
    new-instance v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;

    sget-object v2, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->PEEKED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    sget-object v3, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->FLING:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;-><init>(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;)V

    const/4 v2, 0x6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 328
    new-instance v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;

    sget-object v2, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->EXPANDED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    sget-object v3, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->SWIPE:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;-><init>(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;)V

    const/4 v2, 0x7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 330
    new-instance v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;

    sget-object v2, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->EXPANDED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    sget-object v3, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->FLING:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;-><init>(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;)V

    const/16 v2, 0x8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 332
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma;->EXIT_MAXIMIZED_TO_STATE_CHANGE_CODES:Ljava/util/Map;

    .line 333
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 130
    return-void
.end method

.method private static getPreferenceValue()I
    .locals 2

    .prologue
    .line 493
    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v0

    .line 494
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->isContextualSearchUninitialized()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 495
    const/4 v0, 0x0

    .line 499
    :goto_0
    return v0

    .line 496
    :cond_0
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->isContextualSearchDisabled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 497
    const/4 v0, 0x2

    goto :goto_0

    .line 499
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static getStateChangeCode(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;Ljava/util/Map;I)I
    .locals 1

    .prologue
    .line 485
    new-instance v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;-><init>(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;)V

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 486
    if-eqz v0, :cond_0

    .line 487
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result p3

    .line 489
    :cond_0
    return p3
.end method

.method public static logDuration(ZZJ)V
    .locals 0

    .prologue
    .line 370
    if-eqz p0, :cond_0

    .line 371
    invoke-static {p2, p3}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->recordContextualSearchDurationSeen(J)V

    .line 377
    :goto_0
    return-void

    .line 372
    :cond_0
    if-eqz p1, :cond_1

    .line 373
    invoke-static {p2, p3}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->recordContextualSearchDurationUnseenChained(J)V

    goto :goto_0

    .line 375
    :cond_1
    invoke-static {p2, p3}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->recordContextualSearchDurationUnseen(J)V

    goto :goto_0
.end method

.method public static logFirstRunFlowOutcome()V
    .locals 2

    .prologue
    .line 359
    invoke-static {}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma;->getPreferenceValue()I

    move-result v0

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->recordContextualSearchFirstRunFlowOutcome(II)V

    .line 361
    return-void
.end method

.method public static logFirstRunPanelSeen(Z)V
    .locals 2

    .prologue
    .line 385
    if-eqz p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->recordContextualSearchFirstRunPanelSeen(II)V

    .line 387
    return-void

    .line 385
    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static logFirstStateEntry(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;)V
    .locals 4

    .prologue
    const/4 v3, 0x6

    const/4 v2, 0x0

    .line 416
    sget-object v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$1;->$SwitchMap$com$google$android$apps$chrome$contextualsearch$ContextualSearchState$PanelState:[I

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 440
    :goto_0
    return-void

    .line 418
    :pswitch_0
    sget-object v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma;->ENTER_CLOSED_STATE_CHANGE_CODES:Ljava/util/Map;

    invoke-static {p0, p2, v0, v2}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma;->getStateChangeCode(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;Ljava/util/Map;I)I

    move-result v0

    .line 420
    const/16 v1, 0xb

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->recordContextualSearchEnterClosed(II)V

    goto :goto_0

    .line 424
    :pswitch_1
    sget-object v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma;->ENTER_PEEKED_STATE_CHANGE_CODES:Ljava/util/Map;

    invoke-static {p0, p2, v0, v2}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma;->getStateChangeCode(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;Ljava/util/Map;I)I

    move-result v0

    .line 426
    const/16 v1, 0xa

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->recordContextualSearchEnterPeeked(II)V

    goto :goto_0

    .line 430
    :pswitch_2
    sget-object v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma;->ENTER_EXPANDED_STATE_CHANGE_CODES:Ljava/util/Map;

    invoke-static {p0, p2, v0, v2}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma;->getStateChangeCode(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;Ljava/util/Map;I)I

    move-result v0

    .line 432
    invoke-static {v0, v3}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->recordContextualSearchEnterExpanded(II)V

    goto :goto_0

    .line 436
    :pswitch_3
    sget-object v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma;->ENTER_MAXIMIZED_STATE_CHANGE_CODES:Ljava/util/Map;

    invoke-static {p0, p2, v0, v2}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma;->getStateChangeCode(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;Ljava/util/Map;I)I

    move-result v0

    .line 438
    invoke-static {v0, v3}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->recordContextualSearchEnterMaximized(II)V

    goto :goto_0

    .line 416
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static logFirstStateExit(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 455
    sget-object v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$1;->$SwitchMap$com$google$android$apps$chrome$contextualsearch$ContextualSearchState$PanelState:[I

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 477
    :goto_0
    return-void

    .line 458
    :pswitch_0
    sget-object v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma;->EXIT_CLOSED_TO_STATE_CHANGE_CODES:Ljava/util/Map;

    invoke-static {p1, p2, v0, v2}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma;->getStateChangeCode(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;Ljava/util/Map;I)I

    move-result v0

    .line 460
    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->recordContextualSearchExitClosed(II)V

    goto :goto_0

    .line 463
    :pswitch_1
    sget-object v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma;->EXIT_PEEKED_TO_STATE_CHANGE_CODES:Ljava/util/Map;

    invoke-static {p1, p2, v0, v2}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma;->getStateChangeCode(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;Ljava/util/Map;I)I

    move-result v0

    .line 465
    const/16 v1, 0xb

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->recordContextualSearchExitPeeked(II)V

    goto :goto_0

    .line 468
    :pswitch_2
    sget-object v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma;->EXIT_EXPANDED_TO_STATE_CHANGE_CODES:Ljava/util/Map;

    invoke-static {p1, p2, v0, v2}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma;->getStateChangeCode(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;Ljava/util/Map;I)I

    move-result v0

    .line 470
    const/16 v1, 0xa

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->recordContextualSearchExitExpanded(II)V

    goto :goto_0

    .line 473
    :pswitch_3
    sget-object v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma;->EXIT_MAXIMIZED_TO_STATE_CHANGE_CODES:Ljava/util/Map;

    invoke-static {p1, p2, v0, v2}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma;->getStateChangeCode(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;Ljava/util/Map;I)I

    move-result v0

    .line 475
    const/16 v1, 0x9

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->recordContextualSearchExitMaximized(II)V

    goto :goto_0

    .line 455
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
    .end packed-switch
.end method

.method public static logPreferenceChange(Z)V
    .locals 2

    .prologue
    .line 351
    if-eqz p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->recordContextualSearchPreferenceChange(II)V

    .line 353
    return-void

    .line 351
    :cond_0
    const/4 v0, 0x2

    goto :goto_0
.end method

.method public static logPreferenceState()V
    .locals 2

    .prologue
    .line 341
    invoke-static {}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma;->getPreferenceValue()I

    move-result v0

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->recordContextualSearchPreferenceState(II)V

    .line 343
    return-void
.end method

.method public static logResultsSeen(Z)V
    .locals 2

    .prologue
    .line 394
    if-eqz p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->recordContextualSearchResultsSeen(II)V

    .line 396
    return-void

    .line 394
    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static logSelectionIsValid(Z)V
    .locals 2

    .prologue
    .line 403
    if-eqz p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->recordContextualSearchSelectionIsValid(II)V

    .line 405
    return-void

    .line 403
    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method
