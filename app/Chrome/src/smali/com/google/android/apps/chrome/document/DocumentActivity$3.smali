.class Lcom/google/android/apps/chrome/document/DocumentActivity$3;
.super Lcom/google/android/apps/chrome/document/DocumentTabList$InitializationObserver;
.source "DocumentActivity.java"


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/document/DocumentActivity;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/document/DocumentActivity;Lcom/google/android/apps/chrome/document/DocumentTabList;)V
    .locals 0

    .prologue
    .line 299
    iput-object p1, p0, Lcom/google/android/apps/chrome/document/DocumentActivity$3;->this$0:Lcom/google/android/apps/chrome/document/DocumentActivity;

    invoke-direct {p0, p2}, Lcom/google/android/apps/chrome/document/DocumentTabList$InitializationObserver;-><init>(Lcom/google/android/apps/chrome/document/DocumentTabList;)V

    return-void
.end method


# virtual methods
.method public isCanceled()Z
    .locals 1

    .prologue
    .line 314
    const/4 v0, 0x0

    return v0
.end method

.method public isSatisfied()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 308
    invoke-static {v0}, Lcom/google/android/apps/chrome/document/DocumentActivityDatabase;->getTabList(Z)Lcom/google/android/apps/chrome/document/DocumentTabList;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/document/DocumentTabList;->getCurrentState()I

    move-result v1

    const/16 v2, 0xa

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method protected runImmediately()V
    .locals 2

    .prologue
    .line 302
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity$3;->this$0:Lcom/google/android/apps/chrome/document/DocumentActivity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/document/DocumentMigrationHelper;->migrateTabsToDocumentForUpgrade(Landroid/app/Activity;I)Z

    .line 304
    return-void
.end method
