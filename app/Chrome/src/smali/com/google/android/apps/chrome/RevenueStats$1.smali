.class Lcom/google/android/apps/chrome/RevenueStats$1;
.super Landroid/os/AsyncTask;
.source "RevenueStats.java"


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/RevenueStats;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/RevenueStats;)V
    .locals 0

    .prologue
    .line 105
    iput-object p1, p0, Lcom/google/android/apps/chrome/RevenueStats$1;->this$0:Lcom/google/android/apps/chrome/RevenueStats;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 105
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/RevenueStats$1;->doInBackground([Ljava/lang/Void;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/google/android/apps/chrome/RevenueStats$1;->this$0:Lcom/google/android/apps/chrome/RevenueStats;

    # getter for: Lcom/google/android/apps/chrome/RevenueStats;->mApplicationContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/google/android/apps/chrome/RevenueStats;->access$000(Lcom/google/android/apps/chrome/RevenueStats;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/RevenueStats;->retrieveClientId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 105
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/RevenueStats$1;->onPostExecute(Ljava/lang/String;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 116
    iget-object v0, p0, Lcom/google/android/apps/chrome/RevenueStats$1;->this$0:Lcom/google/android/apps/chrome/RevenueStats;

    # setter for: Lcom/google/android/apps/chrome/RevenueStats;->mClientId:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/android/apps/chrome/RevenueStats;->access$102(Lcom/google/android/apps/chrome/RevenueStats;Ljava/lang/String;)Ljava/lang/String;

    .line 117
    iget-object v0, p0, Lcom/google/android/apps/chrome/RevenueStats$1;->this$0:Lcom/google/android/apps/chrome/RevenueStats;

    # getter for: Lcom/google/android/apps/chrome/RevenueStats;->mRlz:Ljava/util/concurrent/atomic/AtomicReference;
    invoke-static {v0}, Lcom/google/android/apps/chrome/RevenueStats;->access$200(Lcom/google/android/apps/chrome/RevenueStats;)Ljava/util/concurrent/atomic/AtomicReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 118
    iget-object v0, p0, Lcom/google/android/apps/chrome/RevenueStats$1;->this$0:Lcom/google/android/apps/chrome/RevenueStats;

    iget-object v1, p0, Lcom/google/android/apps/chrome/RevenueStats$1;->this$0:Lcom/google/android/apps/chrome/RevenueStats;

    # getter for: Lcom/google/android/apps/chrome/RevenueStats;->mClientId:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/apps/chrome/RevenueStats;->access$100(Lcom/google/android/apps/chrome/RevenueStats;)Ljava/lang/String;

    move-result-object v1

    # invokes: Lcom/google/android/apps/chrome/RevenueStats;->setSearchClient(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/RevenueStats;->access$300(Lcom/google/android/apps/chrome/RevenueStats;Ljava/lang/String;)V

    .line 120
    :cond_0
    return-void
.end method
