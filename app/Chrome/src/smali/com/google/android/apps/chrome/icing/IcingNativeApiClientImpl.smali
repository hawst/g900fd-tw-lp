.class public Lcom/google/android/apps/chrome/icing/IcingNativeApiClientImpl;
.super Ljava/lang/Object;
.source "IcingNativeApiClientImpl.java"

# interfaces
.implements Lcom/google/android/apps/chrome/icing/IcingNativeApiClient;


# static fields
.field private static sCachedInfo:Ljava/util/concurrent/atomic/AtomicReference;


# instance fields
.field private final mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    sput-object v0, Lcom/google/android/apps/chrome/icing/IcingNativeApiClientImpl;->sCachedInfo:Ljava/util/concurrent/atomic/AtomicReference;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iput-object p1, p0, Lcom/google/android/apps/chrome/icing/IcingNativeApiClientImpl;->mContext:Landroid/content/Context;

    .line 47
    return-void
.end method

.method private callService(J)Lcom/google/android/apps/chrome/icing/IcingNativeApiClientInfo;
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v5, 0x0

    .line 141
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/icing/IcingNativeApiClientImpl;->isSupported()Z

    move-result v0

    if-nez v0, :cond_0

    .line 142
    const-string/jumbo v0, "IcingNativeApiClientImpl"

    const-string/jumbo v2, "Google Play Services too old or unavailable."

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lcom/google/android/apps/chrome/icing/IcingDebugLogger;->debug(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 178
    :goto_0
    return-object v1

    .line 146
    :cond_0
    const-string/jumbo v0, "IcingNativeApiClientImpl"

    const-string/jumbo v2, "Connecting..."

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lcom/google/android/apps/chrome/icing/IcingDebugLogger;->debug(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 147
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/icing/IcingNativeApiClientImpl;->getAppDataSearchClient()Lcom/google/android/gms/appdatasearch/AppDataSearchClient;

    move-result-object v0

    .line 148
    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->connectWithTimeout(J)Lcom/google/android/gms/common/ConnectionResult;

    move-result-object v2

    .line 149
    invoke-virtual {v2}, Lcom/google/android/gms/common/ConnectionResult;->isSuccess()Z

    move-result v3

    if-nez v3, :cond_1

    .line 150
    invoke-virtual {v2}, Lcom/google/android/gms/common/ConnectionResult;->getErrorCode()I

    move-result v0

    .line 151
    const-string/jumbo v2, "IcingNativeApiClientImpl"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Connection failed, code="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {v2, v0, v3}, Lcom/google/android/apps/chrome/icing/IcingDebugLogger;->debug(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 155
    :cond_1
    invoke-virtual {v0}, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->getNativeApiInfo()Lcom/google/android/gms/appdatasearch/NativeApiInfo;

    move-result-object v0

    .line 156
    if-nez v0, :cond_2

    .line 158
    const-string/jumbo v0, "IcingNativeApiClientImpl"

    const-string/jumbo v2, "Unable to get native API info."

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lcom/google/android/apps/chrome/icing/IcingDebugLogger;->debug(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 162
    :cond_2
    iget-object v2, v0, Lcom/google/android/gms/appdatasearch/NativeApiInfo;->sharedLibAbsoluteFilename:Ljava/lang/String;

    .line 163
    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 165
    :cond_3
    const-string/jumbo v0, "IcingNativeApiClientImpl"

    const-string/jumbo v2, "Main shared library unavailable."

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lcom/google/android/apps/chrome/icing/IcingDebugLogger;->debug(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 169
    :cond_4
    iget-object v0, v0, Lcom/google/android/gms/appdatasearch/NativeApiInfo;->sharedLibExtensionAbsoluteFilename:Ljava/lang/String;

    .line 170
    if-eqz v0, :cond_5

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 174
    :cond_5
    const-string/jumbo v0, "IcingNativeApiClientImpl"

    const-string/jumbo v3, "Extension shared library unavailable."

    new-array v4, v5, [Ljava/lang/Object;

    invoke-static {v0, v3, v4}, Lcom/google/android/apps/chrome/icing/IcingDebugLogger;->debug(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move-object v0, v1

    .line 178
    :cond_6
    new-instance v1, Lcom/google/android/apps/chrome/icing/IcingNativeApiClientInfo;

    invoke-direct {v1, v2, v0}, Lcom/google/android/apps/chrome/icing/IcingNativeApiClientInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static getNativeApiClientInfoArray(Landroid/content/Context;ZJ)[Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v2, 0x2

    .line 125
    new-instance v0, Lcom/google/android/apps/chrome/icing/IcingNativeApiClientImpl;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/icing/IcingNativeApiClientImpl;-><init>(Landroid/content/Context;)V

    .line 126
    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/apps/chrome/icing/IcingNativeApiClientImpl;->getNativeApiClientInfo(ZJ)Lcom/google/android/apps/chrome/icing/IcingNativeApiClientInfo;

    move-result-object v1

    .line 128
    if-nez v1, :cond_0

    new-array v0, v2, [Ljava/lang/String;

    .line 129
    :goto_0
    return-object v0

    :cond_0
    new-array v0, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, v1, Lcom/google/android/apps/chrome/icing/IcingNativeApiClientInfo;->sharedLibAbsoluteFilename:Ljava/lang/String;

    aput-object v3, v0, v2

    const/4 v2, 0x1

    iget-object v1, v1, Lcom/google/android/apps/chrome/icing/IcingNativeApiClientInfo;->sharedLibExtensionAbsoluteFilename:Ljava/lang/String;

    aput-object v1, v0, v2

    goto :goto_0
.end method


# virtual methods
.method clearCachedNativeApiClientInfo_ForTest()V
    .locals 2

    .prologue
    .line 193
    sget-object v0, Lcom/google/android/apps/chrome/icing/IcingNativeApiClientImpl;->sCachedInfo:Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 194
    return-void
.end method

.method getAppDataSearchClient()Lcom/google/android/gms/appdatasearch/AppDataSearchClient;
    .locals 2

    .prologue
    .line 188
    new-instance v0, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;

    iget-object v1, p0, Lcom/google/android/apps/chrome/icing/IcingNativeApiClientImpl;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public getCachedNativeApiClientInfo()Lcom/google/android/apps/chrome/icing/IcingNativeApiClientInfo;
    .locals 1

    .prologue
    .line 59
    sget-object v0, Lcom/google/android/apps/chrome/icing/IcingNativeApiClientImpl;->sCachedInfo:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/icing/IcingNativeApiClientInfo;

    return-object v0
.end method

.method getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 201
    iget-object v0, p0, Lcom/google/android/apps/chrome/icing/IcingNativeApiClientImpl;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method public getNativeApiClientInfo(ZJ)Lcom/google/android/apps/chrome/icing/IcingNativeApiClientInfo;
    .locals 6

    .prologue
    .line 65
    sget-object v0, Lcom/google/android/apps/chrome/icing/IcingNativeApiClientImpl;->sCachedInfo:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/icing/IcingNativeApiClientInfo;

    .line 66
    if-eqz v0, :cond_2

    .line 67
    if-nez p1, :cond_1

    .line 102
    :cond_0
    :goto_0
    return-object v0

    .line 72
    :cond_1
    iget-object v1, v0, Lcom/google/android/apps/chrome/icing/IcingNativeApiClientInfo;->sharedLibExtensionAbsoluteFilename:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 80
    :cond_2
    invoke-direct {p0, p2, p3}, Lcom/google/android/apps/chrome/icing/IcingNativeApiClientImpl;->callService(J)Lcom/google/android/apps/chrome/icing/IcingNativeApiClientInfo;

    move-result-object v1

    .line 96
    if-eqz v1, :cond_3

    .line 97
    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/icing/IcingNativeApiClientInfo;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 98
    const-string/jumbo v2, "IcingNativeApiClientImpl"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Library paths updated: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lcom/google/android/apps/chrome/icing/IcingDebugLogger;->debug(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 99
    sget-object v2, Lcom/google/android/apps/chrome/icing/IcingNativeApiClientImpl;->sCachedInfo:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v2, v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 102
    :cond_3
    sget-object v0, Lcom/google/android/apps/chrome/icing/IcingNativeApiClientImpl;->sCachedInfo:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/icing/IcingNativeApiClientInfo;

    goto :goto_0
.end method

.method public isSupported()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 51
    sget-object v1, Lcom/google/android/apps/chrome/icing/IcingNativeApiClientImpl;->sCachedInfo:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 53
    :cond_0
    :goto_0
    return v0

    .line 52
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/chrome/icing/IcingNativeApiClientImpl;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/gms/common/f;->a(Landroid/content/Context;)I

    move-result v1

    .line 53
    if-eqz v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method
