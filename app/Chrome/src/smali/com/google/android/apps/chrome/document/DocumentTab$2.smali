.class Lcom/google/android/apps/chrome/document/DocumentTab$2;
.super Lorg/chromium/content/browser/WebContentsObserverAndroid;
.source "DocumentTab.java"


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/document/DocumentTab;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/document/DocumentTab;Lorg/chromium/content_public/browser/WebContents;)V
    .locals 0

    .prologue
    .line 174
    iput-object p1, p0, Lcom/google/android/apps/chrome/document/DocumentTab$2;->this$0:Lcom/google/android/apps/chrome/document/DocumentTab;

    invoke-direct {p0, p2}, Lorg/chromium/content/browser/WebContentsObserverAndroid;-><init>(Lorg/chromium/content_public/browser/WebContents;)V

    return-void
.end method


# virtual methods
.method public didFinishLoad(JLjava/lang/String;Z)V
    .locals 7

    .prologue
    .line 177
    invoke-super {p0, p1, p2, p3, p4}, Lorg/chromium/content/browser/WebContentsObserverAndroid;->didFinishLoad(JLjava/lang/String;Z)V

    .line 178
    if-nez p4, :cond_0

    .line 182
    :goto_0
    return-void

    .line 179
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentTab$2;->this$0:Lcom/google/android/apps/chrome/document/DocumentTab;

    # getter for: Lcom/google/android/apps/chrome/document/DocumentTab;->mFaviconHelper:Lorg/chromium/chrome/browser/favicon/FaviconHelper;
    invoke-static {v0}, Lcom/google/android/apps/chrome/document/DocumentTab;->access$200(Lcom/google/android/apps/chrome/document/DocumentTab;)Lorg/chromium/chrome/browser/favicon/FaviconHelper;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/document/DocumentTab$2;->this$0:Lcom/google/android/apps/chrome/document/DocumentTab;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/document/DocumentTab;->getProfile()Lorg/chromium/chrome/browser/profiles/Profile;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/document/DocumentTab$2;->this$0:Lcom/google/android/apps/chrome/document/DocumentTab;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/document/DocumentTab;->getUrl()Ljava/lang/String;

    move-result-object v2

    # getter for: Lcom/google/android/apps/chrome/document/DocumentTab;->ICON_TYPES:[I
    invoke-static {}, Lcom/google/android/apps/chrome/document/DocumentTab;->access$000()[I

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/chrome/document/DocumentTab$2;->this$0:Lcom/google/android/apps/chrome/document/DocumentTab;

    # getter for: Lcom/google/android/apps/chrome/document/DocumentTab;->mDesiredIconSizePx:I
    invoke-static {v4}, Lcom/google/android/apps/chrome/document/DocumentTab;->access$100(Lcom/google/android/apps/chrome/document/DocumentTab;)I

    move-result v4

    iget-object v5, p0, Lcom/google/android/apps/chrome/document/DocumentTab$2;->this$0:Lcom/google/android/apps/chrome/document/DocumentTab;

    invoke-virtual/range {v0 .. v5}, Lorg/chromium/chrome/browser/favicon/FaviconHelper;->getLargestRawFaviconForUrl(Lorg/chromium/chrome/browser/profiles/Profile;Ljava/lang/String;[IILorg/chromium/chrome/browser/favicon/FaviconHelper$FaviconImageCallback;)V

    goto :goto_0
.end method
