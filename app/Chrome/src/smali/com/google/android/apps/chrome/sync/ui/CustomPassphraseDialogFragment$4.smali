.class Lcom/google/android/apps/chrome/sync/ui/CustomPassphraseDialogFragment$4;
.super Ljava/lang/Object;
.source "CustomPassphraseDialogFragment.java"

# interfaces
.implements Landroid/widget/TextView$OnEditorActionListener;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/sync/ui/CustomPassphraseDialogFragment;

.field final synthetic val$passphrase:Landroid/widget/EditText;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/sync/ui/CustomPassphraseDialogFragment;Landroid/widget/EditText;)V
    .locals 0

    .prologue
    .line 81
    iput-object p1, p0, Lcom/google/android/apps/chrome/sync/ui/CustomPassphraseDialogFragment$4;->this$0:Lcom/google/android/apps/chrome/sync/ui/CustomPassphraseDialogFragment;

    iput-object p2, p0, Lcom/google/android/apps/chrome/sync/ui/CustomPassphraseDialogFragment$4;->val$passphrase:Landroid/widget/EditText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 84
    const/4 v0, 0x6

    if-ne p2, v0, :cond_0

    .line 85
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ui/CustomPassphraseDialogFragment$4;->this$0:Lcom/google/android/apps/chrome/sync/ui/CustomPassphraseDialogFragment;

    iget-object v2, p0, Lcom/google/android/apps/chrome/sync/ui/CustomPassphraseDialogFragment$4;->val$passphrase:Landroid/widget/EditText;

    iget-object v3, p0, Lcom/google/android/apps/chrome/sync/ui/CustomPassphraseDialogFragment$4;->this$0:Lcom/google/android/apps/chrome/sync/ui/CustomPassphraseDialogFragment;

    # getter for: Lcom/google/android/apps/chrome/sync/ui/CustomPassphraseDialogFragment;->mConfirmPassphrase:Landroid/widget/EditText;
    invoke-static {v3}, Lcom/google/android/apps/chrome/sync/ui/CustomPassphraseDialogFragment;->access$000(Lcom/google/android/apps/chrome/sync/ui/CustomPassphraseDialogFragment;)Landroid/widget/EditText;

    move-result-object v3

    # invokes: Lcom/google/android/apps/chrome/sync/ui/CustomPassphraseDialogFragment;->validatePasswordText(Landroid/widget/EditText;Landroid/widget/EditText;)V
    invoke-static {v0, v2, v3}, Lcom/google/android/apps/chrome/sync/ui/CustomPassphraseDialogFragment;->access$100(Lcom/google/android/apps/chrome/sync/ui/CustomPassphraseDialogFragment;Landroid/widget/EditText;Landroid/widget/EditText;)V

    .line 87
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ui/CustomPassphraseDialogFragment$4;->this$0:Lcom/google/android/apps/chrome/sync/ui/CustomPassphraseDialogFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/sync/ui/CustomPassphraseDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Landroid/app/AlertDialog;

    .line 88
    if-eqz v0, :cond_0

    .line 89
    const/4 v2, -0x1

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ui/CustomPassphraseDialogFragment$4;->this$0:Lcom/google/android/apps/chrome/sync/ui/CustomPassphraseDialogFragment;

    # getter for: Lcom/google/android/apps/chrome/sync/ui/CustomPassphraseDialogFragment;->mConfirmPassphrase:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/google/android/apps/chrome/sync/ui/CustomPassphraseDialogFragment;->access$000(Lcom/google/android/apps/chrome/sync/ui/CustomPassphraseDialogFragment;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getError()Ljava/lang/CharSequence;

    move-result-object v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v2, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 93
    :cond_0
    return v1

    :cond_1
    move v0, v1

    .line 89
    goto :goto_0
.end method
