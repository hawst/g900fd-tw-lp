.class public Lcom/google/android/apps/chrome/identity/GservicesAndPackageNameBasedIdentificationGenerator;
.super Ljava/lang/Object;
.source "GservicesAndPackageNameBasedIdentificationGenerator.java"

# interfaces
.implements Lorg/chromium/chrome/browser/identity/UniqueIdentificationGenerator;


# static fields
.field public static final GENERATOR_ID:Ljava/lang/String; = "GSERVICES_ANDROID_ID"

.field static final NOT_FOUND:I = -0x1


# instance fields
.field private final mApplicationContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/identity/GservicesAndPackageNameBasedIdentificationGenerator;->mApplicationContext:Landroid/content/Context;

    .line 49
    return-void
.end method

.method private getAndroidIdWithPackageName()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 78
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/identity/GservicesAndPackageNameBasedIdentificationGenerator;->getAndroidIdFromGservices()J
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 84
    const-wide/16 v4, -0x1

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 88
    :goto_0
    return-object v0

    .line 79
    :catch_0
    move-exception v1

    .line 80
    const-string/jumbo v2, "GservicesAndPackageNameBasedIdentificationGenerator"

    const-string/jumbo v3, "Couldn\'t get Android ID."

    invoke-static {v2, v3, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 88
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/identity/GservicesAndPackageNameBasedIdentificationGenerator;->mApplicationContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method getAndroidIdFromGservices()J
    .locals 4

    .prologue
    .line 98
    iget-object v0, p0, Lcom/google/android/apps/chrome/identity/GservicesAndPackageNameBasedIdentificationGenerator;->mApplicationContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "android_id"

    const-wide/16 v2, -0x1

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/gsf/b;->a(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method getUUID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 93
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getUniqueId(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/apps/chrome/identity/GservicesAndPackageNameBasedIdentificationGenerator;->mApplicationContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 56
    const-string/jumbo v0, "com.google.android.apps.chrome.identity.UNIQUE_ID"

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 57
    if-eqz v0, :cond_0

    .line 72
    :goto_0
    return-object v0

    .line 62
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/identity/GservicesAndPackageNameBasedIdentificationGenerator;->getAndroidIdWithPackageName()Ljava/lang/String;

    move-result-object v0

    .line 63
    if-nez v0, :cond_1

    .line 65
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/identity/GservicesAndPackageNameBasedIdentificationGenerator;->getUUID()Ljava/lang/String;

    move-result-object v0

    .line 69
    :cond_1
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 70
    const-string/jumbo v2, "com.google.android.apps.chrome.identity.UNIQUE_ID"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 71
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_0
.end method
