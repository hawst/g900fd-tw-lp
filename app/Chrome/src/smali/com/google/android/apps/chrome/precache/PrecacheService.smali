.class public Lcom/google/android/apps/chrome/precache/PrecacheService;
.super Landroid/app/Service;
.source "PrecacheService.java"


# static fields
.field public static final ACTION_START_PRECACHE:Ljava/lang/String; = "com.google.android.apps.chrome.precache.PrecacheService.START_PRECACHE"


# instance fields
.field private mDeviceState:Lcom/google/android/apps/chrome/precache/DeviceState;

.field private final mDeviceStateReceiver:Landroid/content/BroadcastReceiver;

.field private mIsPrecaching:Z

.field private mPrecacheLauncher:Lcom/google/android/apps/chrome/precache/PrecacheLauncher;

.field private mPrecachingWakeLock:Landroid/os/PowerManager$WakeLock;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 40
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/precache/PrecacheService;->mIsPrecaching:Z

    .line 47
    invoke-static {}, Lcom/google/android/apps/chrome/precache/DeviceState;->getInstance()Lcom/google/android/apps/chrome/precache/DeviceState;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/precache/PrecacheService;->mDeviceState:Lcom/google/android/apps/chrome/precache/DeviceState;

    .line 56
    new-instance v0, Lcom/google/android/apps/chrome/precache/PrecacheService$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/precache/PrecacheService$1;-><init>(Lcom/google/android/apps/chrome/precache/PrecacheService;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/precache/PrecacheService;->mDeviceStateReceiver:Landroid/content/BroadcastReceiver;

    .line 79
    new-instance v0, Lcom/google/android/apps/chrome/precache/PrecacheService$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/precache/PrecacheService$2;-><init>(Lcom/google/android/apps/chrome/precache/PrecacheService;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/precache/PrecacheService;->mPrecacheLauncher:Lcom/google/android/apps/chrome/precache/PrecacheLauncher;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/precache/PrecacheService;)Z
    .locals 1

    .prologue
    .line 30
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/precache/PrecacheService;->mIsPrecaching:Z

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/precache/PrecacheService;)Lcom/google/android/apps/chrome/precache/DeviceState;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/apps/chrome/precache/PrecacheService;->mDeviceState:Lcom/google/android/apps/chrome/precache/DeviceState;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/precache/PrecacheService;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/google/android/apps/chrome/precache/PrecacheService;->cancelPrecaching()V

    return-void
.end method

.method private acquirePrecachingWakeLock()V
    .locals 3

    .prologue
    .line 195
    iget-object v0, p0, Lcom/google/android/apps/chrome/precache/PrecacheService;->mPrecachingWakeLock:Landroid/os/PowerManager$WakeLock;

    if-nez v0, :cond_0

    .line 196
    const-string/jumbo v0, "power"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/precache/PrecacheService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 197
    const/4 v1, 0x1

    const-string/jumbo v2, "PrecacheService"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/precache/PrecacheService;->mPrecachingWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 199
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/precache/PrecacheService;->mPrecachingWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 200
    return-void
.end method

.method private cancelPrecaching()V
    .locals 2

    .prologue
    .line 168
    const-string/jumbo v0, "PrecacheService"

    const-string/jumbo v1, "Cancel precaching"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 169
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/precache/PrecacheService;->prepareNativeLibraries()V

    .line 170
    iget-object v0, p0, Lcom/google/android/apps/chrome/precache/PrecacheService;->mPrecacheLauncher:Lcom/google/android/apps/chrome/precache/PrecacheLauncher;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/precache/PrecacheLauncher;->cancel()V

    .line 172
    invoke-direct {p0}, Lcom/google/android/apps/chrome/precache/PrecacheService;->shutdownPrecaching()V

    .line 173
    return-void
.end method

.method private finishPrecaching()V
    .locals 2

    .prologue
    .line 162
    const-string/jumbo v0, "PrecacheService"

    const-string/jumbo v1, "Finish precaching"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 163
    invoke-direct {p0}, Lcom/google/android/apps/chrome/precache/PrecacheService;->shutdownPrecaching()V

    .line 164
    return-void
.end method

.method private registerDeviceStateReceiver()V
    .locals 2

    .prologue
    .line 186
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 187
    const-string/jumbo v1, "android.intent.action.ACTION_POWER_DISCONNECTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 188
    const-string/jumbo v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 189
    const-string/jumbo v1, "android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 190
    iget-object v1, p0, Lcom/google/android/apps/chrome/precache/PrecacheService;->mDeviceStateReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/chrome/precache/PrecacheService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 191
    return-void
.end method

.method private releasePrecachingWakeLock()V
    .locals 1

    .prologue
    .line 204
    iget-object v0, p0, Lcom/google/android/apps/chrome/precache/PrecacheService;->mPrecachingWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/precache/PrecacheService;->mPrecachingWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 205
    iget-object v0, p0, Lcom/google/android/apps/chrome/precache/PrecacheService;->mPrecachingWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 207
    :cond_0
    return-void
.end method

.method private shutdownPrecaching()V
    .locals 1

    .prologue
    .line 179
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/precache/PrecacheService;->mIsPrecaching:Z

    .line 180
    invoke-direct {p0}, Lcom/google/android/apps/chrome/precache/PrecacheService;->releasePrecachingWakeLock()V

    .line 181
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/precache/PrecacheService;->stopSelf()V

    .line 182
    return-void
.end method

.method private startPrecaching()V
    .locals 2

    .prologue
    .line 148
    const-string/jumbo v0, "PrecacheService"

    const-string/jumbo v1, "Start precaching"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 149
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/precache/PrecacheService;->prepareNativeLibraries()V

    .line 150
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/precache/PrecacheService;->mIsPrecaching:Z

    .line 151
    invoke-direct {p0}, Lcom/google/android/apps/chrome/precache/PrecacheService;->acquirePrecachingWakeLock()V

    .line 157
    iget-object v0, p0, Lcom/google/android/apps/chrome/precache/PrecacheService;->mPrecacheLauncher:Lcom/google/android/apps/chrome/precache/PrecacheLauncher;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/precache/PrecacheLauncher;->start()V

    .line 158
    return-void
.end method


# virtual methods
.method getDeviceStateReceiver()Landroid/content/BroadcastReceiver;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/android/apps/chrome/precache/PrecacheService;->mDeviceStateReceiver:Landroid/content/BroadcastReceiver;

    return-object v0
.end method

.method handlePrecacheCompleted()V
    .locals 1

    .prologue
    .line 75
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/precache/PrecacheService;->mIsPrecaching:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/precache/PrecacheService;->finishPrecaching()V

    .line 76
    :cond_0
    return-void
.end method

.method isPrecaching()Z
    .locals 1

    .prologue
    .line 44
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/precache/PrecacheService;->mIsPrecaching:Z

    return v0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 129
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 0

    .prologue
    .line 93
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 94
    invoke-direct {p0}, Lcom/google/android/apps/chrome/precache/PrecacheService;->registerDeviceStateReceiver()V

    .line 95
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 99
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/precache/PrecacheService;->mIsPrecaching:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/precache/PrecacheService;->cancelPrecaching()V

    .line 101
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/precache/PrecacheService;->mDeviceStateReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/precache/PrecacheService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 102
    iget-object v0, p0, Lcom/google/android/apps/chrome/precache/PrecacheService;->mPrecacheLauncher:Lcom/google/android/apps/chrome/precache/PrecacheLauncher;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/precache/PrecacheLauncher;->destroy()V

    .line 103
    invoke-direct {p0}, Lcom/google/android/apps/chrome/precache/PrecacheService;->releasePrecachingWakeLock()V

    .line 105
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 106
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 2

    .prologue
    .line 111
    if-eqz p1, :cond_0

    :try_start_0
    const-string/jumbo v0, "com.google.android.apps.chrome.precache.PrecacheService.START_PRECACHE"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/precache/PrecacheService;->mIsPrecaching:Z

    if-nez v0, :cond_0

    .line 114
    invoke-direct {p0}, Lcom/google/android/apps/chrome/precache/PrecacheService;->startPrecaching()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 117
    :cond_0
    invoke-static {}, Lcom/google/android/apps/chrome/precache/PrecacheServiceLauncher;->releaseWakeLock()V

    .line 123
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/precache/PrecacheService;->mIsPrecaching:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    .line 117
    :catchall_0
    move-exception v0

    invoke-static {}, Lcom/google/android/apps/chrome/precache/PrecacheServiceLauncher;->releaseWakeLock()V

    throw v0

    .line 123
    :cond_1
    const/4 v0, 0x2

    goto :goto_0
.end method

.method prepareNativeLibraries()V
    .locals 3

    .prologue
    .line 136
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/precache/PrecacheService;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/ChromeMobileApplication;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/precache/PrecacheService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/ChromeMobileApplication;->startBrowserProcessesAndLoadLibrariesSync(Landroid/content/Context;Z)V
    :try_end_0
    .catch Lorg/chromium/base/library_loader/ProcessInitException; {:try_start_0 .. :try_end_0} :catch_0

    .line 144
    :goto_0
    return-void

    .line 139
    :catch_0
    move-exception v0

    const-string/jumbo v0, "PrecacheService"

    const-string/jumbo v1, "ProcessInitException while starting the browser process"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 142
    const/4 v0, -0x1

    invoke-static {v0}, Ljava/lang/System;->exit(I)V

    goto :goto_0
.end method

.method setDeviceState(Lcom/google/android/apps/chrome/precache/DeviceState;)V
    .locals 0

    .prologue
    .line 51
    iput-object p1, p0, Lcom/google/android/apps/chrome/precache/PrecacheService;->mDeviceState:Lcom/google/android/apps/chrome/precache/DeviceState;

    .line 52
    return-void
.end method

.method setPrecacheLauncher(Lcom/google/android/apps/chrome/precache/PrecacheLauncher;)V
    .locals 0

    .prologue
    .line 88
    iput-object p1, p0, Lcom/google/android/apps/chrome/precache/PrecacheService;->mPrecacheLauncher:Lcom/google/android/apps/chrome/precache/PrecacheLauncher;

    .line 89
    return-void
.end method
