.class public interface abstract Lcom/google/android/apps/chrome/ntp/NewTabPageView$NewTabPageManager;
.super Ljava/lang/Object;
.source "NewTabPageView.java"

# interfaces
.implements Lcom/google/android/apps/chrome/ntp/MostVisitedItem$MostVisitedItemManager;


# virtual methods
.method public abstract focusSearchBox(ZLjava/lang/String;)V
.end method

.method public abstract getLocalFaviconImageForURL(Ljava/lang/String;ILorg/chromium/chrome/browser/favicon/FaviconHelper$FaviconImageCallback;)V
.end method

.method public abstract getSearchProviderLogo(Lorg/chromium/chrome/browser/LogoBridge$LogoObserver;)V
.end method

.method public abstract getURLThumbnail(Ljava/lang/String;Lorg/chromium/chrome/browser/profiles/MostVisitedSites$ThumbnailCallback;)V
.end method

.method public abstract isLocationBarShownInNTP()Z
.end method

.method public abstract navigateToBookmarks()V
.end method

.method public abstract navigateToRecentTabs()V
.end method

.method public abstract notifyLoadingComplete()V
.end method

.method public abstract openLogoLink()V
.end method

.method public abstract optOutPromoSettingsSelected()V
.end method

.method public abstract setMostVisitedURLsObserver(Lorg/chromium/chrome/browser/profiles/MostVisitedSites$MostVisitedURLsObserver;I)V
.end method

.method public abstract shouldShowOptOutPromo()Z
.end method
