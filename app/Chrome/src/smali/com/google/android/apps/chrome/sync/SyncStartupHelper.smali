.class public Lcom/google/android/apps/chrome/sync/SyncStartupHelper;
.super Ljava/lang/Object;
.source "SyncStartupHelper.java"

# interfaces
.implements Lorg/chromium/chrome/browser/sync/ProfileSyncService$SyncStateChangedListener;


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final mDelegate:Lcom/google/android/apps/chrome/sync/SyncStartupHelper$Delegate;

.field private mDestroyed:Z

.field private final mProfileSyncService:Lorg/chromium/chrome/browser/sync/ProfileSyncService;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const-class v0, Lcom/google/android/apps/chrome/sync/SyncStartupHelper;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/sync/SyncStartupHelper;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/chromium/chrome/browser/sync/ProfileSyncService;Lcom/google/android/apps/chrome/sync/SyncStartupHelper$Delegate;)V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    .line 38
    iput-object p1, p0, Lcom/google/android/apps/chrome/sync/SyncStartupHelper;->mProfileSyncService:Lorg/chromium/chrome/browser/sync/ProfileSyncService;

    .line 39
    iput-object p2, p0, Lcom/google/android/apps/chrome/sync/SyncStartupHelper;->mDelegate:Lcom/google/android/apps/chrome/sync/SyncStartupHelper$Delegate;

    .line 40
    sget-boolean v0, Lcom/google/android/apps/chrome/sync/SyncStartupHelper;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncStartupHelper;->mDelegate:Lcom/google/android/apps/chrome/sync/SyncStartupHelper$Delegate;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 41
    :cond_0
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/sync/SyncStartupHelper;)Z
    .locals 1

    .prologue
    .line 22
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/sync/SyncStartupHelper;->mDestroyed:Z

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/sync/SyncStartupHelper;)Lcom/google/android/apps/chrome/sync/SyncStartupHelper$Delegate;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncStartupHelper;->mDelegate:Lcom/google/android/apps/chrome/sync/SyncStartupHelper$Delegate;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/sync/SyncStartupHelper;)Lorg/chromium/chrome/browser/sync/ProfileSyncService;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncStartupHelper;->mProfileSyncService:Lorg/chromium/chrome/browser/sync/ProfileSyncService;

    return-object v0
.end method


# virtual methods
.method public destroy()V
    .locals 1

    .prologue
    .line 83
    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    .line 84
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncStartupHelper;->mProfileSyncService:Lorg/chromium/chrome/browser/sync/ProfileSyncService;

    invoke-virtual {v0, p0}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->removeSyncStateChangedListener(Lorg/chromium/chrome/browser/sync/ProfileSyncService$SyncStateChangedListener;)V

    .line 85
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/sync/SyncStartupHelper;->mDestroyed:Z

    .line 86
    return-void
.end method

.method public startSync(Landroid/app/Activity;Landroid/accounts/Account;)V
    .locals 2

    .prologue
    .line 44
    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    .line 45
    sget-boolean v0, Lcom/google/android/apps/chrome/sync/SyncStartupHelper;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/sync/SyncStartupHelper;->mDestroyed:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 46
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncStartupHelper;->mProfileSyncService:Lorg/chromium/chrome/browser/sync/ProfileSyncService;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->isSyncInitialized()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 48
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncStartupHelper;->mDelegate:Lcom/google/android/apps/chrome/sync/SyncStartupHelper$Delegate;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/sync/SyncStartupHelper$Delegate;->startupComplete()V

    .line 80
    :goto_0
    return-void

    .line 51
    :cond_1
    new-instance v0, Lcom/google/android/apps/chrome/sync/SyncStartupHelper$1;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/apps/chrome/sync/SyncStartupHelper$1;-><init>(Lcom/google/android/apps/chrome/sync/SyncStartupHelper;Landroid/app/Activity;Landroid/accounts/Account;)V

    .line 77
    const-string/jumbo v1, "https://www.googleapis.com/auth/chromesync"

    invoke-static {p1, p1, p2, v1, v0}, Lorg/chromium/chrome/browser/signin/OAuth2TokenService;->getOAuth2AccessToken(Landroid/content/Context;Landroid/app/Activity;Landroid/accounts/Account;Ljava/lang/String;Lorg/chromium/sync/signin/AccountManagerHelper$GetAuthTokenCallback;)V

    goto :goto_0
.end method

.method public syncStateChanged()V
    .locals 2

    .prologue
    .line 90
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncStartupHelper;->mProfileSyncService:Lorg/chromium/chrome/browser/sync/ProfileSyncService;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->isSyncInitialized()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 91
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncStartupHelper;->mDelegate:Lcom/google/android/apps/chrome/sync/SyncStartupHelper$Delegate;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/sync/SyncStartupHelper$Delegate;->startupComplete()V

    .line 96
    :cond_0
    :goto_0
    return-void

    .line 92
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncStartupHelper;->mProfileSyncService:Lorg/chromium/chrome/browser/sync/ProfileSyncService;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->getAuthError()Lorg/chromium/chrome/browser/sync/GoogleServiceAuthError$State;

    move-result-object v0

    sget-object v1, Lorg/chromium/chrome/browser/sync/GoogleServiceAuthError$State;->NONE:Lorg/chromium/chrome/browser/sync/GoogleServiceAuthError$State;

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncStartupHelper;->mProfileSyncService:Lorg/chromium/chrome/browser/sync/ProfileSyncService;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->hasUnrecoverableError()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 94
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/SyncStartupHelper;->mDelegate:Lcom/google/android/apps/chrome/sync/SyncStartupHelper$Delegate;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/sync/SyncStartupHelper$Delegate;->startupFailed()V

    goto :goto_0
.end method
