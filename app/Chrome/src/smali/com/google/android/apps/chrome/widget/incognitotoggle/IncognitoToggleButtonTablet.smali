.class public Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet;
.super Landroid/widget/ImageButton;
.source "IncognitoToggleButtonTablet.java"


# static fields
.field private static final NOTIFICATIONS:[I


# instance fields
.field private mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

.field private mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 105
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet;->NOTIFICATIONS:[I

    return-void

    nop

    :array_0
    .array-data 4
        0xc
        0x5
        0x2
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0, p1, p2}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 111
    new-instance v0, Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet$3;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet$3;-><init>(Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    .line 34
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet;)Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet;->updateButtonResource()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet;->updateButtonVisibility()V

    return-void
.end method

.method private updateButtonResource()V
    .locals 2

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->getCurrentModel()Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 74
    :cond_0
    :goto_0
    return-void

    .line 69
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->isIncognitoSelected()Z

    move-result v0

    if-eqz v0, :cond_2

    sget v0, Lcom/google/android/apps/chrome/R$string;->accessibility_tabstrip_btn_incognito_toggle_incognito:I

    :goto_1
    invoke-virtual {v1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 72
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->isIncognitoSelected()Z

    move-result v0

    if-eqz v0, :cond_3

    sget v0, Lcom/google/android/apps/chrome/R$drawable;->btn_tabstrip_switch_incognito:I

    :goto_2
    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet;->setImageResource(I)V

    goto :goto_0

    .line 69
    :cond_2
    sget v0, Lcom/google/android/apps/chrome/R$string;->accessibility_tabstrip_btn_incognito_toggle_standard:I

    goto :goto_1

    .line 72
    :cond_3
    sget v0, Lcom/google/android/apps/chrome/R$drawable;->btn_tabstrip_switch_normal:I

    goto :goto_2
.end method

.method private updateButtonVisibility()V
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->getCurrentModel()Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 78
    :cond_0
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet;->setVisibility(I)V

    .line 89
    :goto_0
    return-void

    .line 82
    :cond_1
    new-instance v0, Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet$2;-><init>(Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method


# virtual methods
.method protected onAttachedToWindow()V
    .locals 3

    .prologue
    .line 93
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet;->NOTIFICATIONS:[I

    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->registerForNotifications(Landroid/content/Context;[ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    .line 95
    invoke-super {p0}, Landroid/widget/ImageButton;->onAttachedToWindow()V

    .line 96
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 3

    .prologue
    .line 100
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet;->NOTIFICATIONS:[I

    iget-object v2, p0, Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->unregisterForNotifications(Landroid/content/Context;[ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    .line 102
    invoke-super {p0}, Landroid/widget/ImageButton;->onDetachedFromWindow()V

    .line 103
    return-void
.end method

.method public onFinishInflate()V
    .locals 1

    .prologue
    .line 38
    invoke-super {p0}, Landroid/widget/ImageButton;->onFinishInflate()V

    .line 40
    sget-object v0, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 41
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet;->setVisibility(I)V

    .line 43
    new-instance v0, Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet$1;-><init>(Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 51
    return-void
.end method

.method public setTabModelSelector(Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;)V
    .locals 0

    .prologue
    .line 59
    iput-object p1, p0, Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    .line 60
    if-eqz p1, :cond_0

    .line 61
    invoke-direct {p0}, Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet;->updateButtonResource()V

    .line 62
    invoke-direct {p0}, Lcom/google/android/apps/chrome/widget/incognitotoggle/IncognitoToggleButtonTablet;->updateButtonVisibility()V

    .line 64
    :cond_0
    return-void
.end method
