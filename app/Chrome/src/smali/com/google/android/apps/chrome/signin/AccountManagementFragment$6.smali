.class Lcom/google/android/apps/chrome/signin/AccountManagementFragment$6;
.super Ljava/lang/Object;
.source "AccountManagementFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/signin/AccountManagementFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/signin/AccountManagementFragment;)V
    .locals 0

    .prologue
    .line 517
    iput-object p1, p0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment$6;->this$0:Lcom/google/android/apps/chrome/signin/AccountManagementFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 521
    iget-object v0, p0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment$6;->this$0:Lcom/google/android/apps/chrome/signin/AccountManagementFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/preferences/Preferences;

    .line 522
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/Preferences;->updatePreferencesHeadersAndMenu()V

    .line 523
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/Preferences;->getCurrentFragment()Landroid/app/Fragment;

    move-result-object v0

    .line 524
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-class v2, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 526
    check-cast v0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->updateAccountsList()V

    .line 530
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment$6;->this$0:Lcom/google/android/apps/chrome/signin/AccountManagementFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment$6;->this$0:Lcom/google/android/apps/chrome/signin/AccountManagementFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->closeEditor()V

    .line 531
    :cond_1
    return-void
.end method
