.class Lcom/google/android/apps/chrome/services/AndroidEdu$1$1;
.super Landroid/os/AsyncTask;
.source "AndroidEdu.java"


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/services/AndroidEdu$1;

.field final synthetic val$connection:Landroid/content/ServiceConnection;

.field final synthetic val$ownedService:Lcom/google/android/b/a;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/services/AndroidEdu$1;Lcom/google/android/b/a;Landroid/content/ServiceConnection;)V
    .locals 0

    .prologue
    .line 96
    iput-object p1, p0, Lcom/google/android/apps/chrome/services/AndroidEdu$1$1;->this$0:Lcom/google/android/apps/chrome/services/AndroidEdu$1;

    iput-object p2, p0, Lcom/google/android/apps/chrome/services/AndroidEdu$1$1;->val$ownedService:Lcom/google/android/b/a;

    iput-object p3, p0, Lcom/google/android/apps/chrome/services/AndroidEdu$1$1;->val$connection:Landroid/content/ServiceConnection;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method public varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;
    .locals 3

    .prologue
    .line 100
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/AndroidEdu$1$1;->val$ownedService:Lcom/google/android/b/a;

    invoke-interface {v0}, Lcom/google/android/b/a;->a()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 103
    :goto_0
    return-object v0

    .line 101
    :catch_0
    move-exception v0

    .line 102
    const-string/jumbo v1, "AndroidEdu"

    const-string/jumbo v2, "Error calling service."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 103
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0
.end method

.method public bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 96
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/services/AndroidEdu$1$1;->doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public onPostExecute(Ljava/lang/Boolean;)V
    .locals 2

    .prologue
    .line 109
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/AndroidEdu$1$1;->this$0:Lcom/google/android/apps/chrome/services/AndroidEdu$1;

    iget-object v0, v0, Lcom/google/android/apps/chrome/services/AndroidEdu$1;->val$appContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/chrome/services/AndroidEdu$1$1;->val$connection:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 110
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/AndroidEdu$1$1;->this$0:Lcom/google/android/apps/chrome/services/AndroidEdu$1;

    iget-object v0, v0, Lcom/google/android/apps/chrome/services/AndroidEdu$1;->val$callback:Lcom/google/android/apps/chrome/services/AndroidEdu$OwnerCheckCallback;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/chrome/services/AndroidEdu$OwnerCheckCallback;->onSchoolCheckDone(Z)V

    .line 111
    return-void
.end method

.method public bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 96
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/services/AndroidEdu$1$1;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method
