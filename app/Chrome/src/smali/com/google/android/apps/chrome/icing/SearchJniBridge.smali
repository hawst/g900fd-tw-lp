.class public interface abstract Lcom/google/android/apps/chrome/icing/SearchJniBridge;
.super Ljava/lang/Object;
.source "SearchJniBridge.java"


# virtual methods
.method public abstract addHistoricVisitsToUsageReportsBuffer()Z
.end method

.method public abstract dump(Ljava/io/PrintWriter;)V
.end method

.method public abstract getUsageReportsBatch(I)[Lcom/google/android/apps/chrome/icing/UsageReport;
.end method

.method public abstract init(Lcom/google/android/apps/chrome/icing/SearchJniBridge$DataChangeObserver;)Z
.end method

.method public abstract isStartedForTest()Z
.end method

.method public abstract query(JI)[Lcom/google/android/apps/chrome/icing/DeltaFileEntry;
.end method

.method public abstract removeUsageReports([Lcom/google/android/apps/chrome/icing/UsageReport;)V
.end method

.method public abstract trimDeltaFile(J)J
.end method
