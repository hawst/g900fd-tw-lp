.class public abstract Lcom/google/android/apps/chrome/precache/PrecacheLauncher;
.super Ljava/lang/Object;
.source "PrecacheLauncher.java"


# instance fields
.field private mNativePrecacheLauncher:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static isPrecachingEnabled(Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;)Z
    .locals 1

    .prologue
    .line 67
    invoke-static {}, Lcom/google/android/apps/chrome/precache/PrecacheLauncher;->nativeIsPrecachingEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 69
    :goto_0
    return v0

    :cond_0
    new-instance v0, Lcom/google/android/apps/chrome/precache/PrecacheLauncher$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/precache/PrecacheLauncher$1;-><init>(Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;)V

    invoke-static {v0}, Lorg/chromium/base/ThreadUtils;->runOnUiThreadBlockingNoException(Ljava/util/concurrent/Callable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_0
.end method

.method private native nativeCancel(J)V
.end method

.method private native nativeDestroy(J)V
.end method

.method private native nativeInit()J
.end method

.method private static native nativeIsPrecachingEnabled()Z
.end method

.method private native nativeStart(J)V
.end method

.method private onPrecacheCompletedCallback()V
    .locals 0

    .prologue
    .line 56
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/precache/PrecacheLauncher;->onPrecacheCompleted()V

    .line 57
    return-void
.end method

.method public static updatePrecachingEnabled(Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 87
    invoke-static {p0}, Lcom/google/android/apps/chrome/precache/PrecacheLauncher;->isPrecachingEnabled(Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;)Z

    move-result v0

    invoke-static {p1, v0}, Lcom/google/android/apps/chrome/precache/PrecacheServiceLauncher;->setIsPrecachingEnabled(Landroid/content/Context;Z)V

    .line 89
    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 4

    .prologue
    .line 41
    iget-wide v0, p0, Lcom/google/android/apps/chrome/precache/PrecacheLauncher;->mNativePrecacheLauncher:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 42
    invoke-direct {p0}, Lcom/google/android/apps/chrome/precache/PrecacheLauncher;->nativeInit()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/chrome/precache/PrecacheLauncher;->mNativePrecacheLauncher:J

    .line 44
    :cond_0
    iget-wide v0, p0, Lcom/google/android/apps/chrome/precache/PrecacheLauncher;->mNativePrecacheLauncher:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/precache/PrecacheLauncher;->nativeCancel(J)V

    .line 45
    return-void
.end method

.method public destroy()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 23
    iget-wide v0, p0, Lcom/google/android/apps/chrome/precache/PrecacheLauncher;->mNativePrecacheLauncher:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 24
    iget-wide v0, p0, Lcom/google/android/apps/chrome/precache/PrecacheLauncher;->mNativePrecacheLauncher:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/precache/PrecacheLauncher;->nativeDestroy(J)V

    .line 25
    iput-wide v2, p0, Lcom/google/android/apps/chrome/precache/PrecacheLauncher;->mNativePrecacheLauncher:J

    .line 27
    :cond_0
    return-void
.end method

.method protected abstract onPrecacheCompleted()V
.end method

.method public start()V
    .locals 4

    .prologue
    .line 32
    iget-wide v0, p0, Lcom/google/android/apps/chrome/precache/PrecacheLauncher;->mNativePrecacheLauncher:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 33
    invoke-direct {p0}, Lcom/google/android/apps/chrome/precache/PrecacheLauncher;->nativeInit()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/chrome/precache/PrecacheLauncher;->mNativePrecacheLauncher:J

    .line 35
    :cond_0
    iget-wide v0, p0, Lcom/google/android/apps/chrome/precache/PrecacheLauncher;->mNativePrecacheLauncher:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/precache/PrecacheLauncher;->nativeStart(J)V

    .line 36
    return-void
.end method
