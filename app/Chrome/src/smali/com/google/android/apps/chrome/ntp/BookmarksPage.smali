.class public Lcom/google/android/apps/chrome/ntp/BookmarksPage;
.super Ljava/lang/Object;
.source "BookmarksPage.java"

# interfaces
.implements Lcom/google/android/apps/chrome/compositor/InvalidationAwareThumbnailProvider;
.implements Lorg/chromium/chrome/browser/NativePage;


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final mBackgroundColor:I

.field private mBookmarksBridge:Lorg/chromium/chrome/browser/BookmarksBridge;

.field private mCurrentFolderId:Lorg/chromium/components/bookmarks/BookmarkId;

.field private mFaviconHelper:Lorg/chromium/chrome/browser/favicon/FaviconHelper;

.field private mIsDestroyed:Z

.field private final mPageView:Lcom/google/android/apps/chrome/ntp/BookmarksPageView;

.field private final mProfile:Lorg/chromium/chrome/browser/profiles/Profile;

.field private final mSharedPreferences:Landroid/content/SharedPreferences;

.field private final mTitle:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 47
    const-class v0, Lcom/google/android/apps/chrome/ntp/BookmarksPage;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/ntp/BookmarksPage;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Landroid/content/Context;Lorg/chromium/chrome/browser/profiles/Profile;Lorg/chromium/chrome/browser/Tab;Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;Lcom/google/android/apps/chrome/ntp/BookmarksPage$BookmarkSelectedListener;I)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v7, 0x0

    .line 271
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 272
    iput-object p2, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPage;->mProfile:Lorg/chromium/chrome/browser/profiles/Profile;

    .line 273
    new-instance v0, Lorg/chromium/chrome/browser/favicon/FaviconHelper;

    invoke-direct {v0}, Lorg/chromium/chrome/browser/favicon/FaviconHelper;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPage;->mFaviconHelper:Lorg/chromium/chrome/browser/favicon/FaviconHelper;

    .line 274
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Lcom/google/android/apps/chrome/R$string;->ntp_bookmarks:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPage;->mTitle:Ljava/lang/String;

    .line 275
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Lcom/google/android/apps/chrome/R$color;->ntp_bg:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPage;->mBackgroundColor:I

    .line 276
    new-instance v0, Lorg/chromium/components/bookmarks/BookmarkId;

    const-wide/16 v2, -0x2

    invoke-direct {v0, v2, v3, v7}, Lorg/chromium/components/bookmarks/BookmarkId;-><init>(JI)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPage;->mCurrentFolderId:Lorg/chromium/components/bookmarks/BookmarkId;

    .line 278
    invoke-static {p1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPage;->mSharedPreferences:Landroid/content/SharedPreferences;

    .line 280
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 281
    sget v2, Lcom/google/android/apps/chrome/R$layout;->bookmarks_page:I

    invoke-virtual {v0, v2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;

    iput-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPage;->mPageView:Lcom/google/android/apps/chrome/ntp/BookmarksPageView;

    .line 283
    if-eqz p6, :cond_0

    const/4 v0, 0x2

    if-ne p6, v0, :cond_1

    .line 293
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 294
    sget v2, Lcom/google/android/apps/chrome/R$dimen;->tab_strip_height:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    .line 295
    sget v3, Lcom/google/android/apps/chrome/R$dimen;->toolbar_height_no_shadow:I

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    .line 297
    iget-object v3, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPage;->mPageView:Lcom/google/android/apps/chrome/ntp/BookmarksPageView;

    iget-object v4, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPage;->mPageView:Lcom/google/android/apps/chrome/ntp/BookmarksPageView;

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->getPaddingLeft()I

    move-result v4

    iget-object v5, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPage;->mPageView:Lcom/google/android/apps/chrome/ntp/BookmarksPageView;

    invoke-virtual {v5}, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->getPaddingRight()I

    move-result v5

    iget-object v6, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPage;->mPageView:Lcom/google/android/apps/chrome/ntp/BookmarksPageView;

    invoke-virtual {v6}, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->getPaddingBottom()I

    move-result v6

    invoke-virtual {v3, v4, v2, v5, v6}, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->setPadding(IIII)V

    .line 299
    iget-object v2, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPage;->mPageView:Lcom/google/android/apps/chrome/ntp/BookmarksPageView;

    invoke-virtual {v2, v7}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 300
    invoke-virtual {v2}, Landroid/view/View;->getPaddingLeft()I

    move-result v3

    invoke-virtual {v2}, Landroid/view/View;->getPaddingRight()I

    move-result v4

    invoke-virtual {v2}, Landroid/view/View;->getPaddingBottom()I

    move-result v5

    invoke-virtual {v2, v3, v0, v4, v5}, Landroid/view/View;->setPadding(IIII)V

    .line 305
    :cond_1
    packed-switch p6, :pswitch_data_0

    .line 316
    sget-boolean v0, Lcom/google/android/apps/chrome/ntp/BookmarksPage;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 307
    :pswitch_0
    invoke-direct {p0, p3, p4}, Lcom/google/android/apps/chrome/ntp/BookmarksPage;->buildManager(Lorg/chromium/chrome/browser/Tab;Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;)Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarksPageManager;

    move-result-object v0

    .line 320
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPage;->mPageView:Lcom/google/android/apps/chrome/ntp/BookmarksPageView;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->initialize(Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarksPageManager;)V

    .line 322
    new-instance v0, Lorg/chromium/chrome/browser/BookmarksBridge;

    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPage;->mProfile:Lorg/chromium/chrome/browser/profiles/Profile;

    invoke-direct {v0, v1}, Lorg/chromium/chrome/browser/BookmarksBridge;-><init>(Lorg/chromium/chrome/browser/profiles/Profile;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPage;->mBookmarksBridge:Lorg/chromium/chrome/browser/BookmarksBridge;

    .line 323
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPage;->mBookmarksBridge:Lorg/chromium/chrome/browser/BookmarksBridge;

    new-instance v1, Lcom/google/android/apps/chrome/ntp/BookmarksPage$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/ntp/BookmarksPage$1;-><init>(Lcom/google/android/apps/chrome/ntp/BookmarksPage;)V

    invoke-virtual {v0, v1}, Lorg/chromium/chrome/browser/BookmarksBridge;->addObserver(Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkModelObserver;)V

    .line 376
    return-void

    .line 310
    :pswitch_1
    invoke-direct {p0, p5}, Lcom/google/android/apps/chrome/ntp/BookmarksPage;->buildManagerForSelectBookmarkMode(Lcom/google/android/apps/chrome/ntp/BookmarksPage$BookmarkSelectedListener;)Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarksPageManager;

    move-result-object v0

    goto :goto_0

    .line 313
    :pswitch_2
    invoke-direct {p0, p3, p4, p5}, Lcom/google/android/apps/chrome/ntp/BookmarksPage;->buildManagerForDocumentMode(Lorg/chromium/chrome/browser/Tab;Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;Lcom/google/android/apps/chrome/ntp/BookmarksPage$BookmarkSelectedListener;)Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarksPageManager;

    move-result-object v0

    goto :goto_0

    :cond_2
    move-object v0, v1

    goto :goto_0

    .line 305
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/ntp/BookmarksPage;)Z
    .locals 1

    .prologue
    .line 47
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPage;->mIsDestroyed:Z

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/ntp/BookmarksPage;)Lorg/chromium/chrome/browser/BookmarksBridge;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPage;->mBookmarksBridge:Lorg/chromium/chrome/browser/BookmarksBridge;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/ntp/BookmarksPage;Lcom/google/android/apps/chrome/ntp/BookmarkItemView;)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/ntp/BookmarksPage;->edit(Lcom/google/android/apps/chrome/ntp/BookmarkItemView;)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/apps/chrome/ntp/BookmarksPage;Ljava/lang/String;ILorg/chromium/chrome/browser/favicon/FaviconHelper$FaviconImageCallback;)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/chrome/ntp/BookmarksPage;->getFaviconImageForUrl(Ljava/lang/String;ILorg/chromium/chrome/browser/favicon/FaviconHelper$FaviconImageCallback;)V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/apps/chrome/ntp/BookmarksPage;Lorg/chromium/components/bookmarks/BookmarkId;Z)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/ntp/BookmarksPage;->updateBookmarksPageContents(Lorg/chromium/components/bookmarks/BookmarkId;Z)V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/apps/chrome/ntp/BookmarksPage;)Lorg/chromium/components/bookmarks/BookmarkId;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPage;->mCurrentFolderId:Lorg/chromium/components/bookmarks/BookmarkId;

    return-object v0
.end method

.method static synthetic access$502(Lcom/google/android/apps/chrome/ntp/BookmarksPage;Lorg/chromium/components/bookmarks/BookmarkId;)Lorg/chromium/components/bookmarks/BookmarkId;
    .locals 0

    .prologue
    .line 47
    iput-object p1, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPage;->mCurrentFolderId:Lorg/chromium/components/bookmarks/BookmarkId;

    return-object p1
.end method

.method static synthetic access$600(Lcom/google/android/apps/chrome/ntp/BookmarksPage;)Lcom/google/android/apps/chrome/ntp/BookmarksPageView;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPage;->mPageView:Lcom/google/android/apps/chrome/ntp/BookmarksPageView;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/apps/chrome/ntp/BookmarksPage;)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/google/android/apps/chrome/ntp/BookmarksPage;->updateLastUsedFolderId()V

    return-void
.end method

.method private buildManager(Lorg/chromium/chrome/browser/Tab;Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;)Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarksPageManager;
    .locals 1

    .prologue
    .line 379
    new-instance v0, Lcom/google/android/apps/chrome/ntp/BookmarksPage$BookmarksPageManagerImpl;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/apps/chrome/ntp/BookmarksPage$BookmarksPageManagerImpl;-><init>(Lcom/google/android/apps/chrome/ntp/BookmarksPage;Lorg/chromium/chrome/browser/Tab;Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;)V

    return-object v0
.end method

.method private buildManagerForDocumentMode(Lorg/chromium/chrome/browser/Tab;Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;Lcom/google/android/apps/chrome/ntp/BookmarksPage$BookmarkSelectedListener;)Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarksPageManager;
    .locals 1

    .prologue
    .line 384
    new-instance v0, Lcom/google/android/apps/chrome/ntp/BookmarksPage$DocumentModeManager;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/android/apps/chrome/ntp/BookmarksPage$DocumentModeManager;-><init>(Lcom/google/android/apps/chrome/ntp/BookmarksPage;Lorg/chromium/chrome/browser/Tab;Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;Lcom/google/android/apps/chrome/ntp/BookmarksPage$BookmarkSelectedListener;)V

    return-object v0
.end method

.method private buildManagerForSelectBookmarkMode(Lcom/google/android/apps/chrome/ntp/BookmarksPage$BookmarkSelectedListener;)Lcom/google/android/apps/chrome/ntp/BookmarksPageView$BookmarksPageManager;
    .locals 1

    .prologue
    .line 389
    new-instance v0, Lcom/google/android/apps/chrome/ntp/BookmarksPage$2;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/chrome/ntp/BookmarksPage$2;-><init>(Lcom/google/android/apps/chrome/ntp/BookmarksPage;Lcom/google/android/apps/chrome/ntp/BookmarksPage$BookmarkSelectedListener;)V

    return-object v0
.end method

.method public static buildPage(Landroid/content/Context;Lorg/chromium/chrome/browser/Tab;Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;)Lcom/google/android/apps/chrome/ntp/BookmarksPage;
    .locals 7

    .prologue
    .line 99
    new-instance v0, Lcom/google/android/apps/chrome/ntp/BookmarksPage;

    invoke-virtual {p1}, Lorg/chromium/chrome/browser/Tab;->getProfile()Lorg/chromium/chrome/browser/profiles/Profile;

    move-result-object v2

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/chrome/ntp/BookmarksPage;-><init>(Landroid/content/Context;Lorg/chromium/chrome/browser/profiles/Profile;Lorg/chromium/chrome/browser/Tab;Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;Lcom/google/android/apps/chrome/ntp/BookmarksPage$BookmarkSelectedListener;I)V

    return-object v0
.end method

.method public static buildPageHomeActivityMode(Landroid/content/Context;Lorg/chromium/chrome/browser/Tab;Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;Lorg/chromium/chrome/browser/profiles/Profile;Lcom/google/android/apps/chrome/ntp/BookmarksPage$BookmarkSelectedListener;)Lcom/google/android/apps/chrome/ntp/BookmarksPage;
    .locals 7

    .prologue
    .line 127
    new-instance v0, Lcom/google/android/apps/chrome/ntp/BookmarksPage;

    const/4 v6, 0x2

    move-object v1, p0

    move-object v2, p3

    move-object v3, p1

    move-object v4, p2

    move-object v5, p4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/chrome/ntp/BookmarksPage;-><init>(Landroid/content/Context;Lorg/chromium/chrome/browser/profiles/Profile;Lorg/chromium/chrome/browser/Tab;Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;Lcom/google/android/apps/chrome/ntp/BookmarksPage$BookmarkSelectedListener;I)V

    return-object v0
.end method

.method public static buildPageInSelectBookmarkMode(Landroid/content/Context;Lorg/chromium/chrome/browser/profiles/Profile;Lcom/google/android/apps/chrome/ntp/BookmarksPage$BookmarkSelectedListener;)Lcom/google/android/apps/chrome/ntp/BookmarksPage;
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 112
    new-instance v0, Lcom/google/android/apps/chrome/ntp/BookmarksPage;

    const/4 v6, 0x1

    move-object v1, p0

    move-object v2, p1

    move-object v4, v3

    move-object v5, p2

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/chrome/ntp/BookmarksPage;-><init>(Landroid/content/Context;Lorg/chromium/chrome/browser/profiles/Profile;Lorg/chromium/chrome/browser/Tab;Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;Lcom/google/android/apps/chrome/ntp/BookmarksPage$BookmarkSelectedListener;I)V

    return-object v0
.end method

.method private edit(Lcom/google/android/apps/chrome/ntp/BookmarkItemView;)V
    .locals 6

    .prologue
    .line 466
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/ntp/BookmarkItemView;->getBookmarkId()Lorg/chromium/components/bookmarks/BookmarkId;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/components/bookmarks/BookmarkId;->getType()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 467
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPage;->mPageView:Lcom/google/android/apps/chrome/ntp/BookmarksPageView;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPage;->mProfile:Lorg/chromium/chrome/browser/profiles/Profile;

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/ntp/BookmarkItemView;->getBookmarkId()Lorg/chromium/components/bookmarks/BookmarkId;

    move-result-object v2

    invoke-virtual {v2}, Lorg/chromium/components/bookmarks/BookmarkId;->getId()J

    move-result-wide v2

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/ntp/BookmarkItemView;->getTitle()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/ntp/BookmarkItemView;->isFolder()Z

    move-result v5

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/chrome/bookmark/EditBookmarkHelper;->editPartnerBookmark(Landroid/content/Context;Lorg/chromium/chrome/browser/profiles/Profile;JLjava/lang/String;Z)V

    .line 474
    :goto_0
    return-void

    .line 471
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPage;->mPageView:Lcom/google/android/apps/chrome/ntp/BookmarksPageView;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/ntp/BookmarkItemView;->getBookmarkId()Lorg/chromium/components/bookmarks/BookmarkId;

    move-result-object v1

    invoke-virtual {v1}, Lorg/chromium/components/bookmarks/BookmarkId;->getId()J

    move-result-wide v2

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/ntp/BookmarkItemView;->isFolder()Z

    move-result v1

    invoke-static {v0, v2, v3, v1}, Lcom/google/android/apps/chrome/bookmark/EditBookmarkHelper;->editBookmark(Landroid/content/Context;JZ)V

    goto :goto_0
.end method

.method private getFaviconImageForUrl(Ljava/lang/String;ILorg/chromium/chrome/browser/favicon/FaviconHelper$FaviconImageCallback;)V
    .locals 6

    .prologue
    .line 458
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPage;->mFaviconHelper:Lorg/chromium/chrome/browser/favicon/FaviconHelper;

    if-nez v0, :cond_0

    .line 463
    :goto_0
    return-void

    .line 459
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPage;->mFaviconHelper:Lorg/chromium/chrome/browser/favicon/FaviconHelper;

    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPage;->mProfile:Lorg/chromium/chrome/browser/profiles/Profile;

    const/4 v3, 0x7

    move-object v2, p1

    move v4, p2

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Lorg/chromium/chrome/browser/favicon/FaviconHelper;->getLocalFaviconImageForURL(Lorg/chromium/chrome/browser/profiles/Profile;Ljava/lang/String;IILorg/chromium/chrome/browser/favicon/FaviconHelper$FaviconImageCallback;)Z

    goto :goto_0
.end method

.method private getLastUsedFolderId()Lorg/chromium/components/bookmarks/BookmarkId;
    .locals 3

    .prologue
    .line 515
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPage;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "last_used_folder_id"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/components/bookmarks/BookmarkId;->getBookmarkIdFromString(Ljava/lang/String;)Lorg/chromium/components/bookmarks/BookmarkId;

    move-result-object v0

    return-object v0
.end method

.method private updateBookmarksPageContents(Lorg/chromium/components/bookmarks/BookmarkId;Z)V
    .locals 2

    .prologue
    .line 482
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPage;->mBookmarksBridge:Lorg/chromium/chrome/browser/BookmarksBridge;

    if-nez v0, :cond_1

    .line 503
    :cond_0
    :goto_0
    return-void

    .line 483
    :cond_1
    if-nez p2, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPage;->mCurrentFolderId:Lorg/chromium/components/bookmarks/BookmarkId;

    invoke-virtual {v0, p1}, Lorg/chromium/components/bookmarks/BookmarkId;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 484
    :cond_2
    new-instance v0, Lcom/google/android/apps/chrome/ntp/BookmarksPage$3;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/ntp/BookmarksPage$3;-><init>(Lcom/google/android/apps/chrome/ntp/BookmarksPage;)V

    .line 501
    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPage;->mBookmarksBridge:Lorg/chromium/chrome/browser/BookmarksBridge;

    invoke-virtual {v1, p1, v0}, Lorg/chromium/chrome/browser/BookmarksBridge;->getBookmarksForFolder(Lorg/chromium/components/bookmarks/BookmarkId;Lorg/chromium/chrome/browser/BookmarksBridge$BookmarksCallback;)V

    .line 502
    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPage;->mBookmarksBridge:Lorg/chromium/chrome/browser/BookmarksBridge;

    invoke-virtual {v1, p1, v0}, Lorg/chromium/chrome/browser/BookmarksBridge;->getCurrentFolderHierarchy(Lorg/chromium/components/bookmarks/BookmarkId;Lorg/chromium/chrome/browser/BookmarksBridge$BookmarksCallback;)V

    goto :goto_0
.end method

.method private updateLastUsedFolderId()V
    .locals 3

    .prologue
    .line 506
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPage;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "last_used_folder_id"

    iget-object v2, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPage;->mCurrentFolderId:Lorg/chromium/components/bookmarks/BookmarkId;

    invoke-virtual {v2}, Lorg/chromium/components/bookmarks/BookmarkId;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 508
    return-void
.end method


# virtual methods
.method public captureThumbnail(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 589
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPage;->mPageView:Lcom/google/android/apps/chrome/ntp/BookmarksPageView;

    invoke-static {v0, p1}, Lcom/google/android/apps/chrome/utilities/ViewUtils;->captureBitmap(Landroid/view/View;Landroid/graphics/Canvas;)V

    .line 590
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPage;->mPageView:Lcom/google/android/apps/chrome/ntp/BookmarksPageView;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->updateThumbnailState()V

    .line 591
    return-void
.end method

.method public destroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 551
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPage;->mFaviconHelper:Lorg/chromium/chrome/browser/favicon/FaviconHelper;

    if-eqz v0, :cond_0

    .line 552
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPage;->mFaviconHelper:Lorg/chromium/chrome/browser/favicon/FaviconHelper;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/favicon/FaviconHelper;->destroy()V

    .line 553
    iput-object v1, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPage;->mFaviconHelper:Lorg/chromium/chrome/browser/favicon/FaviconHelper;

    .line 555
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPage;->mBookmarksBridge:Lorg/chromium/chrome/browser/BookmarksBridge;

    if-eqz v0, :cond_1

    .line 556
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPage;->mBookmarksBridge:Lorg/chromium/chrome/browser/BookmarksBridge;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/BookmarksBridge;->destroy()V

    .line 557
    iput-object v1, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPage;->mBookmarksBridge:Lorg/chromium/chrome/browser/BookmarksBridge;

    .line 559
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPage;->mIsDestroyed:Z

    .line 560
    return-void
.end method

.method public getBackgroundColor()I
    .locals 1

    .prologue
    .line 537
    iget v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPage;->mBackgroundColor:I

    return v0
.end method

.method public getHost()Ljava/lang/String;
    .locals 1

    .prologue
    .line 564
    const-string/jumbo v0, "bookmarks"

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 532
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPage;->mTitle:Ljava/lang/String;

    return-object v0
.end method

.method public getUrl()Ljava/lang/String;
    .locals 4

    .prologue
    .line 523
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPage;->mCurrentFolderId:Lorg/chromium/components/bookmarks/BookmarkId;

    invoke-virtual {v0}, Lorg/chromium/components/bookmarks/BookmarkId;->getId()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    .line 524
    const-string/jumbo v0, "chrome-native://bookmarks/"

    .line 526
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "chrome-native://bookmarks/#"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPage;->mCurrentFolderId:Lorg/chromium/components/bookmarks/BookmarkId;

    invoke-virtual {v1}, Lorg/chromium/components/bookmarks/BookmarkId;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getView()Landroid/view/View;
    .locals 1

    .prologue
    .line 542
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPage;->mPageView:Lcom/google/android/apps/chrome/ntp/BookmarksPageView;

    return-object v0
.end method

.method public shouldCaptureThumbnail()Z
    .locals 1

    .prologue
    .line 584
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksPage;->mPageView:Lcom/google/android/apps/chrome/ntp/BookmarksPageView;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ntp/BookmarksPageView;->shouldCaptureThumbnail()Z

    move-result v0

    return v0
.end method

.method public updateForUrl(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 569
    const/4 v0, 0x0

    .line 570
    if-eqz p1, :cond_0

    const-string/jumbo v1, "chrome-native://bookmarks/#"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 571
    const/16 v1, 0x1b

    invoke-virtual {p1, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 572
    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 573
    invoke-static {v1}, Lorg/chromium/components/bookmarks/BookmarkId;->getBookmarkIdFromString(Ljava/lang/String;)Lorg/chromium/components/bookmarks/BookmarkId;

    move-result-object v0

    .line 576
    :cond_0
    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/chrome/ntp/BookmarksPage;->getLastUsedFolderId()Lorg/chromium/components/bookmarks/BookmarkId;

    move-result-object v0

    .line 577
    :cond_1
    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/ntp/BookmarksPage;->updateBookmarksPageContents(Lorg/chromium/components/bookmarks/BookmarkId;Z)V

    .line 578
    return-void
.end method
