.class public Lcom/google/android/apps/chrome/preferences/website/PopupExceptionInfo;
.super Ljava/lang/Object;
.source "PopupExceptionInfo.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private final mPattern:Ljava/lang/String;

.field private final mSetting:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/google/android/apps/chrome/preferences/website/PopupExceptionInfo;->mPattern:Ljava/lang/String;

    .line 20
    iput-object p2, p0, Lcom/google/android/apps/chrome/preferences/website/PopupExceptionInfo;->mSetting:Ljava/lang/String;

    .line 21
    return-void
.end method


# virtual methods
.method public getAllowed()Ljava/lang/Boolean;
    .locals 3

    .prologue
    .line 28
    const/4 v0, 0x0

    .line 29
    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/website/PopupExceptionInfo;->mSetting:Ljava/lang/String;

    const-string/jumbo v2, "allow"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 30
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 33
    :cond_0
    :goto_0
    return-object v0

    .line 31
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/website/PopupExceptionInfo;->mSetting:Ljava/lang/String;

    const-string/jumbo v2, "block"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 32
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0
.end method

.method public getPattern()Ljava/lang/String;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/PopupExceptionInfo;->mPattern:Ljava/lang/String;

    return-object v0
.end method

.method public setAllowed(Ljava/lang/Boolean;)V
    .locals 3

    .prologue
    .line 37
    if-eqz p1, :cond_0

    .line 38
    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/website/PopupExceptionInfo;->mPattern:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->setPopupException(Ljava/lang/String;Z)V

    .line 41
    :goto_0
    return-void

    .line 40
    :cond_0
    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/website/PopupExceptionInfo;->mPattern:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->removePopupException(Ljava/lang/String;)V

    goto :goto_0
.end method
