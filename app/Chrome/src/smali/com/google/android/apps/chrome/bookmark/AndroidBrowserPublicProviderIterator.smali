.class public Lcom/google/android/apps/chrome/bookmark/AndroidBrowserPublicProviderIterator;
.super Ljava/lang/Object;
.source "AndroidBrowserPublicProviderIterator.java"

# interfaces
.implements Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$BookmarkIterator;


# instance fields
.field private final mCursor:Landroid/database/Cursor;

.field private mNextId:J


# direct methods
.method private constructor <init>(Landroid/database/Cursor;)V
    .locals 2

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    const-wide/16 v0, 0x1

    iput-wide v0, p0, Lcom/google/android/apps/chrome/bookmark/AndroidBrowserPublicProviderIterator;->mNextId:J

    .line 42
    iput-object p1, p0, Lcom/google/android/apps/chrome/bookmark/AndroidBrowserPublicProviderIterator;->mCursor:Landroid/database/Cursor;

    .line 43
    return-void
.end method

.method public static createIfAvailable(Landroid/content/ContentResolver;)Lcom/google/android/apps/chrome/bookmark/AndroidBrowserPublicProviderIterator;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 36
    sget-object v1, Landroid/provider/Browser;->BOOKMARKS_URI:Landroid/net/Uri;

    const-string/jumbo v3, "bookmark=1"

    move-object v0, p0

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 38
    if-nez v0, :cond_0

    :goto_0
    return-object v2

    :cond_0
    new-instance v2, Lcom/google/android/apps/chrome/bookmark/AndroidBrowserPublicProviderIterator;

    invoke-direct {v2, v0}, Lcom/google/android/apps/chrome/bookmark/AndroidBrowserPublicProviderIterator;-><init>(Landroid/database/Cursor;)V

    goto :goto_0
.end method

.method public static isProviderAvailable(Landroid/content/ContentResolver;)Z
    .locals 1

    .prologue
    .line 25
    sget-object v0, Landroid/provider/Browser;->BOOKMARKS_URI:Landroid/net/Uri;

    invoke-virtual {p0, v0}, Landroid/content/ContentResolver;->acquireContentProviderClient(Landroid/net/Uri;)Landroid/content/ContentProviderClient;

    move-result-object v0

    .line 27
    if-eqz v0, :cond_0

    .line 28
    invoke-virtual {v0}, Landroid/content/ContentProviderClient;->release()Z

    .line 29
    const/4 v0, 0x1

    .line 31
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public close()V
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AndroidBrowserPublicProviderIterator;->mCursor:Landroid/database/Cursor;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 53
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AndroidBrowserPublicProviderIterator;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 54
    return-void
.end method

.method public hasNext()Z
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AndroidBrowserPublicProviderIterator;->mCursor:Landroid/database/Cursor;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 59
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AndroidBrowserPublicProviderIterator;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AndroidBrowserPublicProviderIterator;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isLast()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AndroidBrowserPublicProviderIterator;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public next()Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v5, -0x1

    .line 64
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AndroidBrowserPublicProviderIterator;->mCursor:Landroid/database/Cursor;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 65
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AndroidBrowserPublicProviderIterator;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 67
    :cond_1
    new-instance v0, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;-><init>()V

    .line 69
    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/chrome/bookmark/AndroidBrowserPublicProviderIterator;->mCursor:Landroid/database/Cursor;

    iget-object v3, p0, Lcom/google/android/apps/chrome/bookmark/AndroidBrowserPublicProviderIterator;->mCursor:Landroid/database/Cursor;

    const-string/jumbo v4, "url"

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;->url:Ljava/lang/String;

    .line 70
    iget-object v2, p0, Lcom/google/android/apps/chrome/bookmark/AndroidBrowserPublicProviderIterator;->mCursor:Landroid/database/Cursor;

    iget-object v3, p0, Lcom/google/android/apps/chrome/bookmark/AndroidBrowserPublicProviderIterator;->mCursor:Landroid/database/Cursor;

    const-string/jumbo v4, "title"

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;->title:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 76
    iget-object v2, v0, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;->url:Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v2, v0, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;->title:Ljava/lang/String;

    if-nez v2, :cond_3

    :cond_2
    move-object v0, v1

    .line 97
    :goto_0
    return-object v0

    .line 73
    :catch_0
    move-exception v0

    move-object v0, v1

    goto :goto_0

    .line 80
    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/chrome/bookmark/AndroidBrowserPublicProviderIterator;->mCursor:Landroid/database/Cursor;

    const-string/jumbo v2, "created"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 81
    if-eq v1, v5, :cond_4

    iget-object v2, p0, Lcom/google/android/apps/chrome/bookmark/AndroidBrowserPublicProviderIterator;->mCursor:Landroid/database/Cursor;

    invoke-interface {v2, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-nez v2, :cond_4

    iget-object v2, p0, Lcom/google/android/apps/chrome/bookmark/AndroidBrowserPublicProviderIterator;->mCursor:Landroid/database/Cursor;

    invoke-interface {v2, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;->created:Ljava/lang/Long;

    .line 83
    :cond_4
    iget-object v1, p0, Lcom/google/android/apps/chrome/bookmark/AndroidBrowserPublicProviderIterator;->mCursor:Landroid/database/Cursor;

    const-string/jumbo v2, "date"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 84
    if-eq v1, v5, :cond_5

    iget-object v2, p0, Lcom/google/android/apps/chrome/bookmark/AndroidBrowserPublicProviderIterator;->mCursor:Landroid/database/Cursor;

    invoke-interface {v2, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-nez v2, :cond_5

    iget-object v2, p0, Lcom/google/android/apps/chrome/bookmark/AndroidBrowserPublicProviderIterator;->mCursor:Landroid/database/Cursor;

    invoke-interface {v2, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;->lastVisit:Ljava/lang/Long;

    .line 86
    :cond_5
    iget-object v1, p0, Lcom/google/android/apps/chrome/bookmark/AndroidBrowserPublicProviderIterator;->mCursor:Landroid/database/Cursor;

    const-string/jumbo v2, "visits"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 87
    if-eq v1, v5, :cond_6

    iget-object v2, p0, Lcom/google/android/apps/chrome/bookmark/AndroidBrowserPublicProviderIterator;->mCursor:Landroid/database/Cursor;

    invoke-interface {v2, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-nez v2, :cond_6

    iget-object v2, p0, Lcom/google/android/apps/chrome/bookmark/AndroidBrowserPublicProviderIterator;->mCursor:Landroid/database/Cursor;

    invoke-interface {v2, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;->visits:Ljava/lang/Long;

    .line 89
    :cond_6
    iget-object v1, p0, Lcom/google/android/apps/chrome/bookmark/AndroidBrowserPublicProviderIterator;->mCursor:Landroid/database/Cursor;

    const-string/jumbo v2, "favicon"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 90
    if-eq v1, v5, :cond_7

    iget-object v2, p0, Lcom/google/android/apps/chrome/bookmark/AndroidBrowserPublicProviderIterator;->mCursor:Landroid/database/Cursor;

    invoke-interface {v2, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-nez v2, :cond_7

    iget-object v2, p0, Lcom/google/android/apps/chrome/bookmark/AndroidBrowserPublicProviderIterator;->mCursor:Landroid/database/Cursor;

    invoke-interface {v2, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;->favicon:[B

    .line 93
    :cond_7
    iget-wide v2, p0, Lcom/google/android/apps/chrome/bookmark/AndroidBrowserPublicProviderIterator;->mNextId:J

    const-wide/16 v4, 0x1

    add-long/2addr v4, v2

    iput-wide v4, p0, Lcom/google/android/apps/chrome/bookmark/AndroidBrowserPublicProviderIterator;->mNextId:J

    iput-wide v2, v0, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;->id:J

    .line 94
    const-wide/16 v2, 0x0

    iput-wide v2, v0, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;->parentId:J

    .line 95
    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;->isFolder:Z

    goto/16 :goto_0
.end method

.method public bridge synthetic next()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 18
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/bookmark/AndroidBrowserPublicProviderIterator;->next()Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$Bookmark;

    move-result-object v0

    return-object v0
.end method

.method public remove()V
    .locals 1

    .prologue
    .line 47
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
