.class final enum Lcom/google/android/apps/chrome/DownloadManagerService$DownloadStatus;
.super Ljava/lang/Enum;
.source "DownloadManagerService.java"


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/chrome/DownloadManagerService$DownloadStatus;

.field public static final enum COMPLETE:Lcom/google/android/apps/chrome/DownloadManagerService$DownloadStatus;

.field public static final enum FAILED:Lcom/google/android/apps/chrome/DownloadManagerService$DownloadStatus;

.field public static final enum IN_PROGRESS:Lcom/google/android/apps/chrome/DownloadManagerService$DownloadStatus;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 57
    new-instance v0, Lcom/google/android/apps/chrome/DownloadManagerService$DownloadStatus;

    const-string/jumbo v1, "IN_PROGRESS"

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/chrome/DownloadManagerService$DownloadStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/DownloadManagerService$DownloadStatus;->IN_PROGRESS:Lcom/google/android/apps/chrome/DownloadManagerService$DownloadStatus;

    .line 58
    new-instance v0, Lcom/google/android/apps/chrome/DownloadManagerService$DownloadStatus;

    const-string/jumbo v1, "COMPLETE"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/chrome/DownloadManagerService$DownloadStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/DownloadManagerService$DownloadStatus;->COMPLETE:Lcom/google/android/apps/chrome/DownloadManagerService$DownloadStatus;

    .line 59
    new-instance v0, Lcom/google/android/apps/chrome/DownloadManagerService$DownloadStatus;

    const-string/jumbo v1, "FAILED"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/chrome/DownloadManagerService$DownloadStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/DownloadManagerService$DownloadStatus;->FAILED:Lcom/google/android/apps/chrome/DownloadManagerService$DownloadStatus;

    .line 56
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/apps/chrome/DownloadManagerService$DownloadStatus;

    sget-object v1, Lcom/google/android/apps/chrome/DownloadManagerService$DownloadStatus;->IN_PROGRESS:Lcom/google/android/apps/chrome/DownloadManagerService$DownloadStatus;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/chrome/DownloadManagerService$DownloadStatus;->COMPLETE:Lcom/google/android/apps/chrome/DownloadManagerService$DownloadStatus;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/chrome/DownloadManagerService$DownloadStatus;->FAILED:Lcom/google/android/apps/chrome/DownloadManagerService$DownloadStatus;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/apps/chrome/DownloadManagerService$DownloadStatus;->$VALUES:[Lcom/google/android/apps/chrome/DownloadManagerService$DownloadStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/chrome/DownloadManagerService$DownloadStatus;
    .locals 1

    .prologue
    .line 56
    const-class v0, Lcom/google/android/apps/chrome/DownloadManagerService$DownloadStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/DownloadManagerService$DownloadStatus;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/chrome/DownloadManagerService$DownloadStatus;
    .locals 1

    .prologue
    .line 56
    sget-object v0, Lcom/google/android/apps/chrome/DownloadManagerService$DownloadStatus;->$VALUES:[Lcom/google/android/apps/chrome/DownloadManagerService$DownloadStatus;

    invoke-virtual {v0}, [Lcom/google/android/apps/chrome/DownloadManagerService$DownloadStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/chrome/DownloadManagerService$DownloadStatus;

    return-object v0
.end method
