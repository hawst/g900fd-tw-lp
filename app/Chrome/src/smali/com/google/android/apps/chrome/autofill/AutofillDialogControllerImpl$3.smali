.class Lcom/google/android/apps/chrome/autofill/AutofillDialogControllerImpl$3;
.super Ljava/lang/Object;
.source "AutofillDialogControllerImpl.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/autofill/AutofillDialogControllerImpl;

.field final synthetic val$proxyCard:Lcom/google/android/gms/wallet/ProxyCard;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/autofill/AutofillDialogControllerImpl;Lcom/google/android/gms/wallet/ProxyCard;)V
    .locals 0

    .prologue
    .line 315
    iput-object p1, p0, Lcom/google/android/apps/chrome/autofill/AutofillDialogControllerImpl$3;->this$0:Lcom/google/android/apps/chrome/autofill/AutofillDialogControllerImpl;

    iput-object p2, p0, Lcom/google/android/apps/chrome/autofill/AutofillDialogControllerImpl$3;->val$proxyCard:Lcom/google/android/gms/wallet/ProxyCard;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 315
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/autofill/AutofillDialogControllerImpl$3;->call()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public call()Ljava/lang/String;
    .locals 4

    .prologue
    .line 318
    invoke-static {}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager;->getInstance()Lorg/chromium/chrome/browser/autofill/PersonalDataManager;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager;->getCreditCards()Ljava/util/List;

    move-result-object v2

    .line 320
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 321
    iget-object v3, p0, Lcom/google/android/apps/chrome/autofill/AutofillDialogControllerImpl$3;->val$proxyCard:Lcom/google/android/gms/wallet/ProxyCard;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/autofill/PersonalDataManager$CreditCard;

    # invokes: Lcom/google/android/apps/chrome/autofill/AutofillDialogControllerImpl;->areEqual(Lcom/google/android/gms/wallet/ProxyCard;Lorg/chromium/chrome/browser/autofill/PersonalDataManager$CreditCard;)Z
    invoke-static {v3, v0}, Lcom/google/android/apps/chrome/autofill/AutofillDialogControllerImpl;->access$100(Lcom/google/android/gms/wallet/ProxyCard;Lorg/chromium/chrome/browser/autofill/PersonalDataManager$CreditCard;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 322
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/chrome/browser/autofill/PersonalDataManager$CreditCard;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager$CreditCard;->getGUID()Ljava/lang/String;

    move-result-object v0

    .line 325
    :goto_1
    return-object v0

    .line 320
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 325
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method
