.class public Lcom/google/android/apps/chrome/autofill/AutofillDialogControllerImpl;
.super Ljava/lang/Object;
.source "AutofillDialogControllerImpl.java"

# interfaces
.implements Lorg/chromium/chrome/browser/autofill/AutofillDialogControllerAndroid$AutofillDialog;
.implements Lorg/chromium/ui/base/WindowAndroid$IntentCallback;


# instance fields
.field private mDelegate:Lorg/chromium/chrome/browser/autofill/AutofillDialogControllerAndroid$AutofillDialogDelegate;

.field private mDialogActivityRequestCode:I

.field private mWindowAndroid:Lorg/chromium/ui/base/WindowAndroid;


# direct methods
.method private constructor <init>(Lorg/chromium/chrome/browser/autofill/AutofillDialogControllerAndroid$AutofillDialogDelegate;Lorg/chromium/ui/base/WindowAndroid;ZZZZZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;)V
    .locals 9

    .prologue
    .line 108
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    const/4 v3, -0x1

    iput v3, p0, Lcom/google/android/apps/chrome/autofill/AutofillDialogControllerImpl;->mDialogActivityRequestCode:I

    .line 109
    iput-object p1, p0, Lcom/google/android/apps/chrome/autofill/AutofillDialogControllerImpl;->mDelegate:Lorg/chromium/chrome/browser/autofill/AutofillDialogControllerAndroid$AutofillDialogDelegate;

    .line 110
    iput-object p2, p0, Lcom/google/android/apps/chrome/autofill/AutofillDialogControllerImpl;->mWindowAndroid:Lorg/chromium/ui/base/WindowAndroid;

    .line 112
    iget-object v3, p0, Lcom/google/android/apps/chrome/autofill/AutofillDialogControllerImpl;->mWindowAndroid:Lorg/chromium/ui/base/WindowAndroid;

    invoke-static {v3}, Lcom/google/android/apps/chrome/autofill/AutofillDialogControllerImpl;->isApiSupported(Lorg/chromium/ui/base/WindowAndroid;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 113
    iget-object v3, p0, Lcom/google/android/apps/chrome/autofill/AutofillDialogControllerImpl;->mDelegate:Lorg/chromium/chrome/browser/autofill/AutofillDialogControllerAndroid$AutofillDialogDelegate;

    invoke-interface {v3}, Lorg/chromium/chrome/browser/autofill/AutofillDialogControllerAndroid$AutofillDialogDelegate;->dialogCancel()V

    .line 155
    :cond_0
    :goto_0
    return-void

    .line 117
    :cond_1
    new-instance v5, Landroid/content/Intent;

    const-string/jumbo v3, "com.google.android.gms.wallet.payform.ACTION_REQUEST_AUTO_COMPLETE"

    invoke-direct {v5, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 118
    const-string/jumbo v3, "com.google.android.gms"

    invoke-virtual {v5, v3}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 120
    new-instance v6, Lcom/google/android/apps/chrome/autofill/AutofillDataProviderSessionInformation;

    move-object/from16 v0, p9

    move-object/from16 v1, p10

    move-object/from16 v2, p11

    invoke-direct {v6, v0, v1, v2}, Lcom/google/android/apps/chrome/autofill/AutofillDataProviderSessionInformation;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    iget-object v3, p0, Lcom/google/android/apps/chrome/autofill/AutofillDialogControllerImpl;->mWindowAndroid:Lorg/chromium/ui/base/WindowAndroid;

    invoke-virtual {v3}, Lorg/chromium/ui/base/WindowAndroid;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    move/from16 v0, p7

    move-object/from16 v1, p8

    invoke-direct {p0, v3, v0, v1}, Lcom/google/android/apps/chrome/autofill/AutofillDialogControllerImpl;->pickUserAccountToUse(Landroid/content/Context;ZLjava/lang/String;)Landroid/accounts/Account;

    move-result-object v7

    .line 128
    if-nez p6, :cond_2

    invoke-static {}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager;->isAutofillEnabled()Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v3, 0x1

    move v4, v3

    .line 131
    :goto_1
    if-nez p3, :cond_3

    if-nez p4, :cond_3

    const/4 v3, 0x1

    .line 137
    :goto_2
    invoke-static {}, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->a()Lcom/google/android/gms/wallet/a;

    move-result-object v8

    invoke-virtual {v8, v7}, Lcom/google/android/gms/wallet/a;->a(Landroid/accounts/Account;)Lcom/google/android/gms/wallet/a;

    move-result-object v7

    invoke-direct {p0}, Lcom/google/android/apps/chrome/autofill/AutofillDialogControllerImpl;->getWalletEnvironment()I

    move-result v8

    invoke-virtual {v7, v8}, Lcom/google/android/gms/wallet/a;->a(I)Lcom/google/android/gms/wallet/a;

    move-result-object v7

    move-object/from16 v0, p12

    invoke-virtual {v7, v0}, Lcom/google/android/gms/wallet/a;->a(Ljava/lang/String;)Lcom/google/android/gms/wallet/a;

    move-result-object v7

    invoke-virtual {v7, p5}, Lcom/google/android/gms/wallet/a;->a(Z)Lcom/google/android/gms/wallet/a;

    move-result-object v7

    invoke-virtual {v7, p4}, Lcom/google/android/gms/wallet/a;->b(Z)Lcom/google/android/gms/wallet/a;

    move-result-object v7

    invoke-virtual {v7, v3}, Lcom/google/android/gms/wallet/a;->c(Z)Lcom/google/android/gms/wallet/a;

    move-result-object v3

    invoke-virtual {v6}, Lcom/google/android/apps/chrome/autofill/AutofillDataProviderSessionInformation;->serialize()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Lcom/google/android/gms/wallet/a;->b(Ljava/lang/String;)Lcom/google/android/gms/wallet/a;

    move-result-object v3

    invoke-virtual {v3, v4}, Lcom/google/android/gms/wallet/a;->d(Z)Lcom/google/android/gms/wallet/a;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/wallet/a;->a()Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    move-result-object v3

    .line 147
    const-string/jumbo v4, "com.google.android.gms.wallet.EXTRA_IMMEDIATE_FULL_WALLET_REQUEST"

    invoke-virtual {v5, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 149
    iget-object v3, p0, Lcom/google/android/apps/chrome/autofill/AutofillDialogControllerImpl;->mWindowAndroid:Lorg/chromium/ui/base/WindowAndroid;

    sget v4, Lcom/google/android/apps/chrome/R$string;->low_memory_error:I

    invoke-virtual {v3, v5, p0, v4}, Lorg/chromium/ui/base/WindowAndroid;->showCancelableIntent(Landroid/content/Intent;Lorg/chromium/ui/base/WindowAndroid$IntentCallback;I)I

    move-result v3

    iput v3, p0, Lcom/google/android/apps/chrome/autofill/AutofillDialogControllerImpl;->mDialogActivityRequestCode:I

    .line 151
    iget v3, p0, Lcom/google/android/apps/chrome/autofill/AutofillDialogControllerImpl;->mDialogActivityRequestCode:I

    if-gez v3, :cond_0

    .line 152
    iget-object v3, p0, Lcom/google/android/apps/chrome/autofill/AutofillDialogControllerImpl;->mDelegate:Lorg/chromium/chrome/browser/autofill/AutofillDialogControllerAndroid$AutofillDialogDelegate;

    invoke-interface {v3}, Lorg/chromium/chrome/browser/autofill/AutofillDialogControllerAndroid$AutofillDialogDelegate;->dialogCancel()V

    goto/16 :goto_0

    .line 128
    :cond_2
    const/4 v3, 0x0

    move v4, v3

    goto :goto_1

    .line 131
    :cond_3
    const/4 v3, 0x0

    goto :goto_2
.end method

.method synthetic constructor <init>(Lorg/chromium/chrome/browser/autofill/AutofillDialogControllerAndroid$AutofillDialogDelegate;Lorg/chromium/ui/base/WindowAndroid;ZZZZZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;Lcom/google/android/apps/chrome/autofill/AutofillDialogControllerImpl$1;)V
    .locals 0

    .prologue
    .line 46
    invoke-direct/range {p0 .. p14}, Lcom/google/android/apps/chrome/autofill/AutofillDialogControllerImpl;-><init>(Lorg/chromium/chrome/browser/autofill/AutofillDialogControllerAndroid$AutofillDialogDelegate;Lorg/chromium/ui/base/WindowAndroid;ZZZZZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/gms/wallet/ProxyCard;Lorg/chromium/chrome/browser/autofill/PersonalDataManager$CreditCard;)Z
    .locals 1

    .prologue
    .line 46
    invoke-static {p0, p1}, Lcom/google/android/apps/chrome/autofill/AutofillDialogControllerImpl;->areEqual(Lcom/google/android/gms/wallet/ProxyCard;Lorg/chromium/chrome/browser/autofill/PersonalDataManager$CreditCard;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/google/android/gms/wallet/Address;Lorg/chromium/chrome/browser/autofill/PersonalDataManager$AutofillProfile;)Z
    .locals 1

    .prologue
    .line 46
    invoke-static {p0, p1}, Lcom/google/android/apps/chrome/autofill/AutofillDialogControllerImpl;->areEqual(Lcom/google/android/gms/wallet/Address;Lorg/chromium/chrome/browser/autofill/PersonalDataManager$AutofillProfile;)Z

    move-result v0

    return v0
.end method

.method private static areEqual(Lcom/google/android/gms/wallet/Address;Lorg/chromium/chrome/browser/autofill/PersonalDataManager$AutofillProfile;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 299
    if-eqz p1, :cond_0

    if-nez p0, :cond_4

    :cond_0
    if-nez p1, :cond_1

    move v3, v1

    :goto_0
    if-nez p0, :cond_2

    move v0, v1

    :goto_1
    if-ne v3, v0, :cond_3

    move v0, v1

    .line 304
    :goto_2
    return v0

    :cond_1
    move v3, v2

    .line 299
    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1

    :cond_3
    move v0, v2

    goto :goto_2

    .line 301
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/Address;->c()Ljava/lang/String;

    move-result-object v0

    .line 302
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/Address;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_5

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v3, "\n"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/Address;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 304
    :cond_5
    invoke-virtual {p1}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager$AutofillProfile;->getStreetAddress()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {p1}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager$AutofillProfile;->getLocality()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/Address;->f()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {p1}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager$AutofillProfile;->getCountryCode()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/Address;->e()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {p1}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager$AutofillProfile;->getRegion()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/Address;->g()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {p1}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager$AutofillProfile;->getPostalCode()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/Address;->h()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {p1}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager$AutofillProfile;->getFullName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/Address;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_6

    move v0, v1

    goto/16 :goto_2

    :cond_6
    move v0, v2

    goto/16 :goto_2
.end method

.method private static areEqual(Lcom/google/android/gms/wallet/ProxyCard;Lorg/chromium/chrome/browser/autofill/PersonalDataManager$CreditCard;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 291
    if-eqz p1, :cond_0

    if-nez p0, :cond_5

    :cond_0
    if-nez p1, :cond_2

    move v3, v0

    :goto_0
    if-nez p0, :cond_3

    move v2, v0

    :goto_1
    if-ne v3, v2, :cond_4

    .line 293
    :cond_1
    :goto_2
    return v0

    :cond_2
    move v3, v1

    .line 291
    goto :goto_0

    :cond_3
    move v2, v1

    goto :goto_1

    :cond_4
    move v0, v1

    goto :goto_2

    .line 293
    :cond_5
    invoke-virtual {p1}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager$CreditCard;->getNumber()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ProxyCard;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-virtual {p1}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager$CreditCard;->getMonth()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ProxyCard;->d()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-virtual {p1}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager$CreditCard;->getYear()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ProxyCard;->e()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    :cond_6
    move v0, v1

    goto :goto_2
.end method

.method private convertToResultAddress(Lcom/google/android/gms/wallet/Address;)Lorg/chromium/chrome/browser/autofill/AutofillDialogResult$ResultAddress;
    .locals 11

    .prologue
    .line 366
    if-nez p1, :cond_0

    const/4 v0, 0x0

    .line 371
    :goto_0
    return-object v0

    .line 368
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/wallet/Address;->c()Ljava/lang/String;

    move-result-object v3

    .line 369
    invoke-virtual {p1}, Lcom/google/android/gms/wallet/Address;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/Address;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 371
    :cond_1
    new-instance v0, Lorg/chromium/chrome/browser/autofill/AutofillDialogResult$ResultAddress;

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/Address;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/Address;->i()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/Address;->f()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, ""

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/Address;->g()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/Address;->h()Ljava/lang/String;

    move-result-object v7

    const-string/jumbo v8, ""

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/Address;->e()Ljava/lang/String;

    move-result-object v9

    const-string/jumbo v10, ""

    invoke-direct/range {v0 .. v10}, Lorg/chromium/chrome/browser/autofill/AutofillDialogResult$ResultAddress;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private convertToResultCard(Lcom/google/android/gms/wallet/ProxyCard;)Lorg/chromium/chrome/browser/autofill/AutofillDialogResult$ResultCard;
    .locals 5

    .prologue
    .line 356
    if-nez p1, :cond_0

    const/4 v0, 0x0

    .line 358
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lorg/chromium/chrome/browser/autofill/AutofillDialogResult$ResultCard;

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/ProxyCard;->d()I

    move-result v1

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/ProxyCard;->e()I

    move-result v2

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/ProxyCard;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/ProxyCard;->c()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lorg/chromium/chrome/browser/autofill/AutofillDialogResult$ResultCard;-><init>(IILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private convertToResultWallet(Lcom/google/android/gms/wallet/FullWallet;)Lorg/chromium/chrome/browser/autofill/AutofillDialogResult$ResultWallet;
    .locals 6

    .prologue
    .line 347
    new-instance v0, Lorg/chromium/chrome/browser/autofill/AutofillDialogResult$ResultWallet;

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/FullWallet;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/FullWallet;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/FullWallet;->c()Lcom/google/android/gms/wallet/ProxyCard;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/google/android/apps/chrome/autofill/AutofillDialogControllerImpl;->convertToResultCard(Lcom/google/android/gms/wallet/ProxyCard;)Lorg/chromium/chrome/browser/autofill/AutofillDialogResult$ResultCard;

    move-result-object v3

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/FullWallet;->e()Lcom/google/android/gms/wallet/Address;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/google/android/apps/chrome/autofill/AutofillDialogControllerImpl;->convertToResultAddress(Lcom/google/android/gms/wallet/Address;)Lorg/chromium/chrome/browser/autofill/AutofillDialogResult$ResultAddress;

    move-result-object v4

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/FullWallet;->f()Lcom/google/android/gms/wallet/Address;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/google/android/apps/chrome/autofill/AutofillDialogControllerImpl;->convertToResultAddress(Lcom/google/android/gms/wallet/Address;)Lorg/chromium/chrome/browser/autofill/AutofillDialogResult$ResultAddress;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lorg/chromium/chrome/browser/autofill/AutofillDialogResult$ResultWallet;-><init>(Ljava/lang/String;Ljava/lang/String;Lorg/chromium/chrome/browser/autofill/AutofillDialogResult$ResultCard;Lorg/chromium/chrome/browser/autofill/AutofillDialogResult$ResultAddress;Lorg/chromium/chrome/browser/autofill/AutofillDialogResult$ResultAddress;)V

    return-object v0
.end method

.method private findMatchingAddress(Lcom/google/android/gms/wallet/Address;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 331
    if-nez p1, :cond_0

    const/4 v0, 0x0

    .line 333
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/android/apps/chrome/autofill/AutofillDialogControllerImpl$4;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/chrome/autofill/AutofillDialogControllerImpl$4;-><init>(Lcom/google/android/apps/chrome/autofill/AutofillDialogControllerImpl;Lcom/google/android/gms/wallet/Address;)V

    invoke-static {v0}, Lorg/chromium/base/ThreadUtils;->runOnUiThreadBlockingNoException(Ljava/util/concurrent/Callable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method private findMatchingCreditCard(Lcom/google/android/gms/wallet/ProxyCard;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 313
    if-nez p1, :cond_0

    const/4 v0, 0x0

    .line 315
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/android/apps/chrome/autofill/AutofillDialogControllerImpl$3;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/chrome/autofill/AutofillDialogControllerImpl$3;-><init>(Lcom/google/android/apps/chrome/autofill/AutofillDialogControllerImpl;Lcom/google/android/gms/wallet/ProxyCard;)V

    invoke-static {v0}, Lorg/chromium/base/ThreadUtils;->runOnUiThreadBlockingNoException(Ljava/util/concurrent/Callable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method private getProxyCardDescription(Lcom/google/android/gms/wallet/ProxyCard;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 386
    if-nez p1, :cond_0

    const-string/jumbo v0, ""

    .line 391
    :goto_0
    return-object v0

    .line 387
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/wallet/ProxyCard;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x4

    if-ge v0, v1, :cond_1

    const-string/jumbo v0, ""

    goto :goto_0

    .line 389
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/gms/wallet/ProxyCard;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/ProxyCard;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x4

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 391
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "MasterCard-"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private getWalletEnvironment()I
    .locals 2

    .prologue
    .line 89
    invoke-static {}, Lorg/chromium/base/CommandLine;->getInstance()Lorg/chromium/base/CommandLine;

    move-result-object v0

    const-string/jumbo v1, "wallet-service-use-sandbox"

    invoke-virtual {v0, v1}, Lorg/chromium/base/CommandLine;->hasSwitch(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 91
    const/4 v0, 0x0

    .line 94
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static isApiSupported(Lorg/chromium/ui/base/WindowAndroid;)Z
    .locals 2

    .prologue
    .line 239
    invoke-virtual {p0}, Lorg/chromium/ui/base/WindowAndroid;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/f;->a(Landroid/content/Context;)I

    move-result v0

    .line 241
    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 247
    :goto_0
    return v0

    .line 243
    :cond_0
    invoke-static {v0}, Lcom/google/android/gms/common/f;->b(I)Z

    move-result v1

    if-eqz v1, :cond_1

    instance-of v1, p0, Lcom/google/android/apps/chrome/ChromeWindow;

    if-eqz v1, :cond_1

    .line 245
    check-cast p0, Lcom/google/android/apps/chrome/ChromeWindow;

    const/4 v1, -0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/chrome/ChromeWindow;->getGooglePlayServicesErrorDialog(II)Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 247
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private pickUserAccountToUse(Landroid/content/Context;ZLjava/lang/String;)Landroid/accounts/Account;
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 260
    if-eqz p2, :cond_1

    sget-object v0, Lcom/google/android/gms/wallet/b;->a:Landroid/accounts/Account;

    .line 287
    :cond_0
    :goto_0
    return-object v0

    .line 263
    :cond_1
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 264
    invoke-static {p1}, Lorg/chromium/sync/signin/ChromeSigninController;->get(Landroid/content/Context;)Lorg/chromium/sync/signin/ChromeSigninController;

    move-result-object v0

    .line 265
    if-eqz v0, :cond_2

    .line 266
    invoke-virtual {v0}, Lorg/chromium/sync/signin/ChromeSigninController;->getSignedInUser()Landroid/accounts/Account;

    move-result-object v0

    .line 267
    if-nez v0, :cond_0

    .line 272
    :cond_2
    invoke-static {p1}, Lorg/chromium/sync/signin/AccountManagerHelper;->get(Landroid/content/Context;)Lorg/chromium/sync/signin/AccountManagerHelper;

    move-result-object v0

    .line 273
    if-nez v0, :cond_3

    move-object v0, v2

    goto :goto_0

    .line 276
    :cond_3
    invoke-virtual {v0}, Lorg/chromium/sync/signin/AccountManagerHelper;->getGoogleAccounts()[Landroid/accounts/Account;

    move-result-object v3

    .line 277
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    move v0, v1

    .line 278
    :goto_1
    array-length v4, v3

    if-ge v0, v4, :cond_5

    .line 279
    aget-object v4, v3, v0

    iget-object v4, v4, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v4, p3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_4

    aget-object v0, v3, v0

    goto :goto_0

    .line 278
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 284
    :cond_5
    array-length v0, v3

    if-lez v0, :cond_6

    aget-object v0, v3, v1

    goto :goto_0

    :cond_6
    move-object v0, v2

    .line 287
    goto :goto_0
.end method

.method public static setAsDialogFactory()V
    .locals 1

    .prologue
    .line 57
    new-instance v0, Lcom/google/android/apps/chrome/autofill/AutofillDialogControllerImpl$1;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/autofill/AutofillDialogControllerImpl$1;-><init>()V

    invoke-static {v0}, Lorg/chromium/chrome/browser/autofill/AutofillDialogControllerAndroid;->setDialogFactory(Lorg/chromium/chrome/browser/autofill/AutofillDialogControllerAndroid$AutofillDialogFactory;)V

    .line 86
    return-void
.end method


# virtual methods
.method public onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 161
    iget v0, p0, Lcom/google/android/apps/chrome/autofill/AutofillDialogControllerImpl;->mDialogActivityRequestCode:I

    if-ltz v0, :cond_1

    .line 162
    iget-object v0, p0, Lcom/google/android/apps/chrome/autofill/AutofillDialogControllerImpl;->mWindowAndroid:Lorg/chromium/ui/base/WindowAndroid;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/autofill/AutofillDialogControllerImpl;->mWindowAndroid:Lorg/chromium/ui/base/WindowAndroid;

    iget v1, p0, Lcom/google/android/apps/chrome/autofill/AutofillDialogControllerImpl;->mDialogActivityRequestCode:I

    invoke-virtual {v0, v1}, Lorg/chromium/ui/base/WindowAndroid;->cancelIntent(I)V

    .line 163
    :cond_0
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/chrome/autofill/AutofillDialogControllerImpl;->mDialogActivityRequestCode:I

    .line 165
    :cond_1
    iput-object v2, p0, Lcom/google/android/apps/chrome/autofill/AutofillDialogControllerImpl;->mDelegate:Lorg/chromium/chrome/browser/autofill/AutofillDialogControllerAndroid$AutofillDialogDelegate;

    .line 166
    iput-object v2, p0, Lcom/google/android/apps/chrome/autofill/AutofillDialogControllerImpl;->mWindowAndroid:Lorg/chromium/ui/base/WindowAndroid;

    .line 167
    return-void
.end method

.method public onIntentCompleted(Lorg/chromium/ui/base/WindowAndroid;ILandroid/content/ContentResolver;Landroid/content/Intent;)V
    .locals 10

    .prologue
    const/4 v1, -0x1

    const/4 v6, 0x0

    .line 175
    iput v1, p0, Lcom/google/android/apps/chrome/autofill/AutofillDialogControllerImpl;->mDialogActivityRequestCode:I

    .line 176
    iget-object v0, p0, Lcom/google/android/apps/chrome/autofill/AutofillDialogControllerImpl;->mDelegate:Lorg/chromium/chrome/browser/autofill/AutofillDialogControllerAndroid$AutofillDialogDelegate;

    if-nez v0, :cond_0

    .line 236
    :goto_0
    return-void

    .line 178
    :cond_0
    if-ne p2, v1, :cond_1

    if-nez p4, :cond_2

    .line 179
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/autofill/AutofillDialogControllerImpl;->mDelegate:Lorg/chromium/chrome/browser/autofill/AutofillDialogControllerAndroid$AutofillDialogDelegate;

    invoke-interface {v0}, Lorg/chromium/chrome/browser/autofill/AutofillDialogControllerAndroid$AutofillDialogDelegate;->dialogCancel()V

    goto :goto_0

    .line 183
    :cond_2
    const-string/jumbo v0, "com.google.android.gms.wallet.EXTRA_FULL_WALLET"

    invoke-virtual {p4, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/google/android/gms/wallet/FullWallet;

    .line 184
    if-nez v1, :cond_3

    .line 185
    iget-object v0, p0, Lcom/google/android/apps/chrome/autofill/AutofillDialogControllerImpl;->mDelegate:Lorg/chromium/chrome/browser/autofill/AutofillDialogControllerAndroid$AutofillDialogDelegate;

    invoke-interface {v0}, Lorg/chromium/chrome/browser/autofill/AutofillDialogControllerAndroid$AutofillDialogDelegate;->dialogCancel()V

    goto :goto_0

    .line 189
    :cond_3
    const-string/jumbo v0, "com.google.android.gms.wallet.EXTRA_BUYER_ACCOUNT"

    invoke-virtual {p4, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    .line 191
    sget-object v2, Lcom/google/android/gms/wallet/b;->a:Landroid/accounts/Account;

    invoke-virtual {v2, v0}, Landroid/accounts/Account;->equals(Ljava/lang/Object;)Z

    move-result v2

    .line 193
    if-eqz v0, :cond_4

    iget-object v3, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    .line 194
    :goto_1
    invoke-virtual {v1}, Lcom/google/android/gms/wallet/FullWallet;->c()Lcom/google/android/gms/wallet/ProxyCard;

    move-result-object v0

    .line 200
    if-eqz v2, :cond_5

    .line 201
    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/autofill/AutofillDialogControllerImpl;->findMatchingCreditCard(Lcom/google/android/gms/wallet/ProxyCard;)Ljava/lang/String;

    move-result-object v6

    .line 202
    invoke-virtual {v1}, Lcom/google/android/gms/wallet/FullWallet;->e()Lcom/google/android/gms/wallet/Address;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/autofill/AutofillDialogControllerImpl;->findMatchingAddress(Lcom/google/android/gms/wallet/Address;)Ljava/lang/String;

    move-result-object v4

    .line 203
    invoke-virtual {v1}, Lcom/google/android/gms/wallet/FullWallet;->f()Lcom/google/android/gms/wallet/Address;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/autofill/AutofillDialogControllerImpl;->findMatchingAddress(Lcom/google/android/gms/wallet/Address;)Ljava/lang/String;

    move-result-object v5

    .line 232
    :goto_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/autofill/AutofillDialogControllerImpl;->mDelegate:Lorg/chromium/chrome/browser/autofill/AutofillDialogControllerAndroid$AutofillDialogDelegate;

    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/autofill/AutofillDialogControllerImpl;->convertToResultWallet(Lcom/google/android/gms/wallet/FullWallet;)Lorg/chromium/chrome/browser/autofill/AutofillDialogResult$ResultWallet;

    move-result-object v1

    invoke-interface/range {v0 .. v6}, Lorg/chromium/chrome/browser/autofill/AutofillDialogControllerAndroid$AutofillDialogDelegate;->dialogContinue(Lorg/chromium/chrome/browser/autofill/AutofillDialogResult$ResultWallet;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 193
    :cond_4
    const-string/jumbo v3, ""

    goto :goto_1

    .line 205
    :cond_5
    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/autofill/AutofillDialogControllerImpl;->getProxyCardDescription(Lcom/google/android/gms/wallet/ProxyCard;)Ljava/lang/String;

    move-result-object v4

    .line 206
    const-string/jumbo v0, ""

    .line 207
    invoke-virtual {v1}, Lcom/google/android/gms/wallet/FullWallet;->g()[Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_6

    .line 209
    const-string/jumbo v0, " "

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/FullWallet;->g()[Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 212
    :cond_6
    invoke-virtual {p1}, Lorg/chromium/ui/base/WindowAndroid;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    sget v7, Lcom/google/android/apps/chrome/R$string;->autofill_credit_card_generated_text:I

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v4, v8, v9

    const/4 v4, 0x1

    aput-object v0, v8, v4

    invoke-virtual {v5, v7, v8}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 216
    new-instance v4, Lcom/google/android/apps/chrome/autofill/AutofillDialogControllerImpl$2;

    invoke-direct {v4, p0, p1, v0}, Lcom/google/android/apps/chrome/autofill/AutofillDialogControllerImpl$2;-><init>(Lcom/google/android/apps/chrome/autofill/AutofillDialogControllerImpl;Lorg/chromium/ui/base/WindowAndroid;Ljava/lang/String;)V

    const-wide/16 v8, 0x7d0

    invoke-static {v4, v8, v9}, Lorg/chromium/base/ThreadUtils;->postOnUiThreadDelayed(Ljava/lang/Runnable;J)V

    move-object v5, v6

    move-object v4, v6

    goto :goto_2
.end method
