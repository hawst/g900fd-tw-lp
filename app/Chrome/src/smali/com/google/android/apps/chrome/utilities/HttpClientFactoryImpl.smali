.class public Lcom/google/android/apps/chrome/utilities/HttpClientFactoryImpl;
.super Ljava/lang/Object;
.source "HttpClientFactoryImpl.java"

# interfaces
.implements Lcom/google/android/apps/chrome/utilities/HttpClientFactory;


# static fields
.field private static sAppAndVersion:Ljava/lang/String;


# instance fields
.field private final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/google/android/apps/chrome/utilities/HttpClientFactoryImpl;->mContext:Landroid/content/Context;

    .line 20
    return-void
.end method

.method private static getAppAndVersion(Landroid/content/Context;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 27
    sget-object v0, Lcom/google/android/apps/chrome/utilities/HttpClientFactoryImpl;->sAppAndVersion:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 28
    sget-object v0, Lcom/google/android/apps/chrome/utilities/HttpClientFactoryImpl;->sAppAndVersion:Ljava/lang/String;

    .line 38
    :goto_0
    return-object v0

    .line 31
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    iget-object v0, v0, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    .line 33
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Chrome/"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/chrome/utilities/HttpClientFactoryImpl;->sAppAndVersion:Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 38
    :goto_1
    sget-object v0, Lcom/google/android/apps/chrome/utilities/HttpClientFactoryImpl;->sAppAndVersion:Ljava/lang/String;

    goto :goto_0

    .line 34
    :catch_0
    move-exception v0

    .line 35
    const-string/jumbo v1, "HttpClientFactoryImpl"

    const-string/jumbo v2, "Unable to find package when generating version name"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 36
    const-string/jumbo v0, "Chrome/unknown"

    sput-object v0, Lcom/google/android/apps/chrome/utilities/HttpClientFactoryImpl;->sAppAndVersion:Ljava/lang/String;

    goto :goto_1
.end method


# virtual methods
.method public newInstance()Lcom/google/android/apps/chrome/utilities/HttpClientWrapper;
    .locals 2

    .prologue
    .line 23
    iget-object v0, p0, Lcom/google/android/apps/chrome/utilities/HttpClientFactoryImpl;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/apps/chrome/utilities/HttpClientFactoryImpl;->getAppAndVersion(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/utilities/HttpClientFactoryImpl;->mContext:Landroid/content/Context;

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/utilities/HttpClientWrapperImpl;->newInstance(Ljava/lang/String;Landroid/content/Context;)Lcom/google/android/apps/chrome/utilities/HttpClientWrapperImpl;

    move-result-object v0

    return-object v0
.end method
