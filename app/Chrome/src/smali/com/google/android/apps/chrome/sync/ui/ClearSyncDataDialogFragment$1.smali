.class Lcom/google/android/apps/chrome/sync/ui/ClearSyncDataDialogFragment$1;
.super Landroid/os/AsyncTask;
.source "ClearSyncDataDialogFragment.java"


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/sync/ui/ClearSyncDataDialogFragment;

.field final synthetic val$optionsSelected:Ljava/util/EnumSet;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/sync/ui/ClearSyncDataDialogFragment;Ljava/util/EnumSet;)V
    .locals 0

    .prologue
    .line 40
    iput-object p1, p0, Lcom/google/android/apps/chrome/sync/ui/ClearSyncDataDialogFragment$1;->this$0:Lcom/google/android/apps/chrome/sync/ui/ClearSyncDataDialogFragment;

    iput-object p2, p0, Lcom/google/android/apps/chrome/sync/ui/ClearSyncDataDialogFragment$1;->val$optionsSelected:Ljava/util/EnumSet;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 40
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/sync/ui/ClearSyncDataDialogFragment$1;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 2

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ui/ClearSyncDataDialogFragment$1;->val$optionsSelected:Ljava/util/EnumSet;

    sget-object v1, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment$DialogOption;->CLEAR_BOOKMARKS_DATA:Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment$DialogOption;

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 44
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ui/ClearSyncDataDialogFragment$1;->this$0:Lcom/google/android/apps/chrome/sync/ui/ClearSyncDataDialogFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/sync/ui/ClearSyncDataDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/chrome/browser/ChromeBrowserProviderClient;->removeAllUserBookmarks(Landroid/content/Context;)V

    .line 46
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 40
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/sync/ui/ClearSyncDataDialogFragment$1;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 2

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ui/ClearSyncDataDialogFragment$1;->this$0:Lcom/google/android/apps/chrome/sync/ui/ClearSyncDataDialogFragment;

    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/ui/ClearSyncDataDialogFragment$1;->val$optionsSelected:Ljava/util/EnumSet;

    # invokes: Lcom/google/android/apps/chrome/sync/ui/ClearSyncDataDialogFragment;->clearBrowsingData(Ljava/util/EnumSet;)V
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/sync/ui/ClearSyncDataDialogFragment;->access$000(Lcom/google/android/apps/chrome/sync/ui/ClearSyncDataDialogFragment;Ljava/util/EnumSet;)V

    .line 53
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ui/ClearSyncDataDialogFragment$1;->val$optionsSelected:Ljava/util/EnumSet;

    sget-object v1, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment$DialogOption;->CLEAR_BOOKMARKS_DATA:Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment$DialogOption;

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 55
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/ui/ClearSyncDataDialogFragment$1;->this$0:Lcom/google/android/apps/chrome/sync/ui/ClearSyncDataDialogFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/sync/ui/ClearSyncDataDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/chrome/browser/signin/SigninManager;->get(Landroid/content/Context;)Lorg/chromium/chrome/browser/signin/SigninManager;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/signin/SigninManager;->clearLastSignedInUser()V

    .line 57
    :cond_0
    return-void
.end method
