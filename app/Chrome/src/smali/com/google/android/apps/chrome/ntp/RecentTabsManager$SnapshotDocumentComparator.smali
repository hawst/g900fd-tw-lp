.class Lcom/google/android/apps/chrome/ntp/RecentTabsManager$SnapshotDocumentComparator;
.super Ljava/lang/Object;
.source "RecentTabsManager.java"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/util/Comparator;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 484
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/chrome/ntp/RecentTabsManager$1;)V
    .locals 0

    .prologue
    .line 484
    invoke-direct {p0}, Lcom/google/android/apps/chrome/ntp/RecentTabsManager$SnapshotDocumentComparator;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)I
    .locals 4

    .prologue
    .line 488
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->getCreateTime()J

    move-result-wide v0

    invoke-virtual {p2}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->getCreateTime()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, -0x1

    .line 490
    :goto_0
    return v0

    .line 489
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->getCreateTime()J

    move-result-wide v0

    invoke-virtual {p2}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->getCreateTime()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-gez v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    .line 490
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 484
    check-cast p1, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;

    check-cast p2, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/chrome/ntp/RecentTabsManager$SnapshotDocumentComparator;->compare(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)I

    move-result v0

    return v0
.end method
