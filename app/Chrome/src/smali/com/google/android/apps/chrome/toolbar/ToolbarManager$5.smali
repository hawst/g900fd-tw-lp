.class Lcom/google/android/apps/chrome/toolbar/ToolbarManager$5;
.super Ljava/lang/Object;
.source "ToolbarManager.java"

# interfaces
.implements Lorg/chromium/chrome/browser/appmenu/AppMenuObserver;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarManager;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/toolbar/ToolbarManager;)V
    .locals 0

    .prologue
    .line 245
    iput-object p1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$5;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMenuVisibilityChanged(Z)V
    .locals 3

    .prologue
    .line 248
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$5;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarManager;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->access$1200(Lcom/google/android/apps/chrome/toolbar/ToolbarManager;)Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;

    move-result-object v0

    if-nez v0, :cond_0

    .line 257
    :goto_0
    return-void

    .line 249
    :cond_0
    if-eqz p1, :cond_1

    .line 250
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$5;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarManager;

    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$5;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarManager;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;
    invoke-static {v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->access$1200(Lcom/google/android/apps/chrome/toolbar/ToolbarManager;)Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$5;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarManager;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mFullscreenMenuToken:I
    invoke-static {v2}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->access$1300(Lcom/google/android/apps/chrome/toolbar/ToolbarManager;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->showControlsPersistentAndClearOldToken(I)I

    move-result v1

    # setter for: Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mFullscreenMenuToken:I
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->access$1302(Lcom/google/android/apps/chrome/toolbar/ToolbarManager;I)I

    goto :goto_0

    .line 254
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$5;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarManager;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mFullscreenManager:Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;
    invoke-static {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->access$1200(Lcom/google/android/apps/chrome/toolbar/ToolbarManager;)Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$5;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarManager;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mFullscreenMenuToken:I
    invoke-static {v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->access$1300(Lcom/google/android/apps/chrome/toolbar/ToolbarManager;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->hideControlsPersistent(I)V

    .line 255
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager$5;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarManager;

    const/4 v1, -0x1

    # setter for: Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->mFullscreenMenuToken:I
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->access$1302(Lcom/google/android/apps/chrome/toolbar/ToolbarManager;I)I

    goto :goto_0
.end method
