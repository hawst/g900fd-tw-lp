.class public interface abstract Lcom/google/android/apps/chrome/OverviewBehavior;
.super Ljava/lang/Object;
.source "OverviewBehavior.java"


# virtual methods
.method public abstract hideOverview(Z)V
.end method

.method public abstract overviewVisible()Z
.end method

.method public abstract setEnableAnimations(Z)V
.end method

.method public abstract setKeyboardHider(Lcom/google/android/apps/chrome/KeyboardHider;)V
.end method

.method public abstract showOverview(Z)V
.end method
