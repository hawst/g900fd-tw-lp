.class public Lcom/google/android/apps/chrome/preferences/TranslatePreferences;
.super Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;
.source "TranslatePreferences.java"


# instance fields
.field private mActionBarSwitch:Landroid/widget/Switch;

.field private mTranslateEnabler:Lcom/google/android/apps/chrome/preferences/WidgetEnabler;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;-><init>()V

    .line 22
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 46
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    .line 47
    sget v0, Lcom/google/android/apps/chrome/R$xml;->translate_preferences:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/TranslatePreferences;->addPreferencesFromResource(I)V

    .line 48
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/TranslatePreferences;->setHasOptionsMenu(Z)V

    .line 49
    new-instance v0, Lcom/google/android/apps/chrome/preferences/ManagedSwitch;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/TranslatePreferences;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->isTranslateManaged()Z

    move-result v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/chrome/preferences/ManagedSwitch;-><init>(Landroid/content/Context;Z)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/preferences/TranslatePreferences;->mActionBarSwitch:Landroid/widget/Switch;

    .line 51
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/TranslatePreferences;->mActionBarSwitch:Landroid/widget/Switch;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/TranslatePreferences;->addSwitchToActionBar(Landroid/widget/Switch;)V

    .line 52
    new-instance v0, Lcom/google/android/apps/chrome/preferences/TranslatePreferences$TranslateEnabler;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/TranslatePreferences;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/preferences/TranslatePreferences;->mActionBarSwitch:Landroid/widget/Switch;

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/chrome/preferences/TranslatePreferences$TranslateEnabler;-><init>(Landroid/content/Context;Landroid/widget/Switch;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/preferences/TranslatePreferences;->mTranslateEnabler:Lcom/google/android/apps/chrome/preferences/WidgetEnabler;

    .line 53
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/TranslatePreferences;->mTranslateEnabler:Lcom/google/android/apps/chrome/preferences/WidgetEnabler;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/preferences/WidgetEnabler;->attach()V

    .line 54
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 58
    invoke-super {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;->onDestroy()V

    .line 59
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/TranslatePreferences;->mTranslateEnabler:Lcom/google/android/apps/chrome/preferences/WidgetEnabler;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/preferences/WidgetEnabler;->destroy()V

    .line 60
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/TranslatePreferences;->mActionBarSwitch:Landroid/widget/Switch;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/Switch;->setVisibility(I)V

    .line 61
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 65
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    .line 66
    sget v3, Lcom/google/android/apps/chrome/R$id;->menu_id_clear_translate_data:I

    if-ne v2, v3, :cond_0

    .line 67
    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->resetTranslateDefaults()V

    .line 68
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/TranslatePreferences;->getActivity()Landroid/app/Activity;

    move-result-object v2

    sget v3, Lcom/google/android/apps/chrome/R$string;->translate_prefs_toast_description:I

    invoke-virtual {p0, v3}, Lcom/google/android/apps/chrome/preferences/TranslatePreferences;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 76
    :goto_0
    return v0

    .line 72
    :cond_0
    sget v3, Lcom/google/android/apps/chrome/R$id;->menu_id_translate_help:I

    if-ne v2, v3, :cond_1

    .line 73
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/TranslatePreferences;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-string/jumbo v2, "mobile_translate"

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/chrome/help/GoogleHelpUtils;->show(Landroid/app/Activity;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    goto :goto_0

    :cond_1
    move v0, v1

    .line 76
    goto :goto_0
.end method
