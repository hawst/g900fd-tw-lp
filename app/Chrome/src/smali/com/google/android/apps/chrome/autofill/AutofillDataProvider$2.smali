.class Lcom/google/android/apps/chrome/autofill/AutofillDataProvider$2;
.super Ljava/lang/Object;
.source "AutofillDataProvider.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/autofill/AutofillDataProvider;

.field final synthetic val$values:Landroid/content/ContentValues;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/autofill/AutofillDataProvider;Landroid/content/ContentValues;)V
    .locals 0

    .prologue
    .line 212
    iput-object p1, p0, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider$2;->this$0:Lcom/google/android/apps/chrome/autofill/AutofillDataProvider;

    iput-object p2, p0, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider$2;->val$values:Landroid/content/ContentValues;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 212
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider$2;->call()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public call()Ljava/lang/String;
    .locals 9

    .prologue
    .line 215
    new-instance v0, Lorg/chromium/chrome/browser/autofill/PersonalDataManager$CreditCard;

    iget-object v1, p0, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider$2;->val$values:Landroid/content/ContentValues;

    const-string/jumbo v2, "GUID"

    invoke-virtual {v1, v2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider$2;->val$values:Landroid/content/ContentValues;

    const-string/jumbo v3, "ORIGIN"

    invoke-virtual {v2, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider$2;->val$values:Landroid/content/ContentValues;

    const-string/jumbo v4, "NAME_FULL"

    invoke-virtual {v3, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider$2;->val$values:Landroid/content/ContentValues;

    const-string/jumbo v5, "CREDIT_CARD_NUMBER"

    invoke-virtual {v4, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider$2;->val$values:Landroid/content/ContentValues;

    const-string/jumbo v6, "CREDIT_CARD_NUMBER"

    invoke-virtual {v5, v6}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider$2;->val$values:Landroid/content/ContentValues;

    const-string/jumbo v7, "EXPIRATION_MONTH"

    invoke-virtual {v6, v7}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/apps/chrome/autofill/AutofillDataProvider$2;->val$values:Landroid/content/ContentValues;

    const-string/jumbo v8, "EXPIRATION_YEAR"

    invoke-virtual {v7, v8}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-direct/range {v0 .. v7}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager$CreditCard;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 223
    invoke-static {}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager;->getInstance()Lorg/chromium/chrome/browser/autofill/PersonalDataManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager;->setCreditCard(Lorg/chromium/chrome/browser/autofill/PersonalDataManager$CreditCard;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
