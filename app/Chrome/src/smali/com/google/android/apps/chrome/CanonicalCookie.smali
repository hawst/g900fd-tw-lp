.class public Lcom/google/android/apps/chrome/CanonicalCookie;
.super Ljava/lang/Object;
.source "CanonicalCookie.java"


# instance fields
.field private final mCreation:J

.field private final mDomain:Ljava/lang/String;

.field private final mExpiration:J

.field private final mHttpOnly:Z

.field private final mLastAccess:J

.field private final mName:Ljava/lang/String;

.field private final mPath:Ljava/lang/String;

.field private final mPriority:I

.field private final mSecure:Z

.field private final mUrl:Ljava/lang/String;

.field private final mValue:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJJZZI)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object p1, p0, Lcom/google/android/apps/chrome/CanonicalCookie;->mUrl:Ljava/lang/String;

    .line 42
    iput-object p2, p0, Lcom/google/android/apps/chrome/CanonicalCookie;->mName:Ljava/lang/String;

    .line 43
    iput-object p3, p0, Lcom/google/android/apps/chrome/CanonicalCookie;->mValue:Ljava/lang/String;

    .line 44
    iput-object p4, p0, Lcom/google/android/apps/chrome/CanonicalCookie;->mDomain:Ljava/lang/String;

    .line 45
    iput-object p5, p0, Lcom/google/android/apps/chrome/CanonicalCookie;->mPath:Ljava/lang/String;

    .line 46
    iput-wide p6, p0, Lcom/google/android/apps/chrome/CanonicalCookie;->mCreation:J

    .line 47
    iput-wide p8, p0, Lcom/google/android/apps/chrome/CanonicalCookie;->mExpiration:J

    .line 48
    iput-wide p10, p0, Lcom/google/android/apps/chrome/CanonicalCookie;->mLastAccess:J

    .line 49
    iput-boolean p12, p0, Lcom/google/android/apps/chrome/CanonicalCookie;->mSecure:Z

    .line 50
    iput-boolean p13, p0, Lcom/google/android/apps/chrome/CanonicalCookie;->mHttpOnly:Z

    .line 51
    iput p14, p0, Lcom/google/android/apps/chrome/CanonicalCookie;->mPriority:I

    .line 52
    return-void
.end method

.method public static createFromStream(Ljava/io/DataInputStream;)Lcom/google/android/apps/chrome/CanonicalCookie;
    .locals 15

    .prologue
    .line 137
    new-instance v0, Lcom/google/android/apps/chrome/CanonicalCookie;

    invoke-virtual {p0}, Ljava/io/DataInputStream;->readUTF()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/io/DataInputStream;->readUTF()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Ljava/io/DataInputStream;->readUTF()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Ljava/io/DataInputStream;->readUTF()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Ljava/io/DataInputStream;->readUTF()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0}, Ljava/io/DataInputStream;->readLong()J

    move-result-wide v6

    invoke-virtual {p0}, Ljava/io/DataInputStream;->readLong()J

    move-result-wide v8

    invoke-virtual {p0}, Ljava/io/DataInputStream;->readLong()J

    move-result-wide v10

    invoke-virtual {p0}, Ljava/io/DataInputStream;->readBoolean()Z

    move-result v12

    invoke-virtual {p0}, Ljava/io/DataInputStream;->readBoolean()Z

    move-result v13

    invoke-virtual {p0}, Ljava/io/DataInputStream;->readInt()I

    move-result v14

    invoke-direct/range {v0 .. v14}, Lcom/google/android/apps/chrome/CanonicalCookie;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJJZZI)V

    return-object v0
.end method


# virtual methods
.method public getCreationDate()J
    .locals 2

    .prologue
    .line 81
    iget-wide v0, p0, Lcom/google/android/apps/chrome/CanonicalCookie;->mCreation:J

    return-wide v0
.end method

.method public getDomain()Ljava/lang/String;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/google/android/apps/chrome/CanonicalCookie;->mDomain:Ljava/lang/String;

    return-object v0
.end method

.method public getExpirationDate()J
    .locals 2

    .prologue
    .line 76
    iget-wide v0, p0, Lcom/google/android/apps/chrome/CanonicalCookie;->mExpiration:J

    return-wide v0
.end method

.method public getLastAccessDate()J
    .locals 2

    .prologue
    .line 71
    iget-wide v0, p0, Lcom/google/android/apps/chrome/CanonicalCookie;->mLastAccess:J

    return-wide v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/google/android/apps/chrome/CanonicalCookie;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/google/android/apps/chrome/CanonicalCookie;->mPath:Ljava/lang/String;

    return-object v0
.end method

.method public getPriority()I
    .locals 1

    .prologue
    .line 56
    iget v0, p0, Lcom/google/android/apps/chrome/CanonicalCookie;->mPriority:I

    return v0
.end method

.method public getUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/google/android/apps/chrome/CanonicalCookie;->mUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/google/android/apps/chrome/CanonicalCookie;->mValue:Ljava/lang/String;

    return-object v0
.end method

.method public isHttpOnly()Z
    .locals 1

    .prologue
    .line 61
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/CanonicalCookie;->mHttpOnly:Z

    return v0
.end method

.method public isSecure()Z
    .locals 1

    .prologue
    .line 66
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/CanonicalCookie;->mSecure:Z

    return v0
.end method

.method public saveToStream(Ljava/io/DataOutputStream;)V
    .locals 2

    .prologue
    .line 116
    iget-object v0, p0, Lcom/google/android/apps/chrome/CanonicalCookie;->mUrl:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    .line 117
    iget-object v0, p0, Lcom/google/android/apps/chrome/CanonicalCookie;->mName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    .line 118
    iget-object v0, p0, Lcom/google/android/apps/chrome/CanonicalCookie;->mValue:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    .line 119
    iget-object v0, p0, Lcom/google/android/apps/chrome/CanonicalCookie;->mDomain:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    .line 120
    iget-object v0, p0, Lcom/google/android/apps/chrome/CanonicalCookie;->mPath:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    .line 121
    iget-wide v0, p0, Lcom/google/android/apps/chrome/CanonicalCookie;->mCreation:J

    invoke-virtual {p1, v0, v1}, Ljava/io/DataOutputStream;->writeLong(J)V

    .line 122
    iget-wide v0, p0, Lcom/google/android/apps/chrome/CanonicalCookie;->mExpiration:J

    invoke-virtual {p1, v0, v1}, Ljava/io/DataOutputStream;->writeLong(J)V

    .line 123
    iget-wide v0, p0, Lcom/google/android/apps/chrome/CanonicalCookie;->mLastAccess:J

    invoke-virtual {p1, v0, v1}, Ljava/io/DataOutputStream;->writeLong(J)V

    .line 124
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/CanonicalCookie;->mSecure:Z

    invoke-virtual {p1, v0}, Ljava/io/DataOutputStream;->writeBoolean(Z)V

    .line 125
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/CanonicalCookie;->mHttpOnly:Z

    invoke-virtual {p1, v0}, Ljava/io/DataOutputStream;->writeBoolean(Z)V

    .line 126
    iget v0, p0, Lcom/google/android/apps/chrome/CanonicalCookie;->mPriority:I

    invoke-virtual {p1, v0}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 127
    return-void
.end method
