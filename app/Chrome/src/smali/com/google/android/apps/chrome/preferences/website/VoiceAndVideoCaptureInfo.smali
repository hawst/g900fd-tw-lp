.class public Lcom/google/android/apps/chrome/preferences/website/VoiceAndVideoCaptureInfo;
.super Ljava/lang/Object;
.source "VoiceAndVideoCaptureInfo.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private final mEmbedder:Ljava/lang/String;

.field private final mOrigin:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/google/android/apps/chrome/preferences/website/VoiceAndVideoCaptureInfo;->mOrigin:Ljava/lang/String;

    .line 25
    iput-object p2, p0, Lcom/google/android/apps/chrome/preferences/website/VoiceAndVideoCaptureInfo;->mEmbedder:Ljava/lang/String;

    .line 26
    return-void
.end method

.method private getEmbedderSafe()Ljava/lang/String;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/VoiceAndVideoCaptureInfo;->mEmbedder:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/VoiceAndVideoCaptureInfo;->mEmbedder:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/VoiceAndVideoCaptureInfo;->mOrigin:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public getEmbedder()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/VoiceAndVideoCaptureInfo;->mEmbedder:Ljava/lang/String;

    return-object v0
.end method

.method public getOrigin()Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/VoiceAndVideoCaptureInfo;->mOrigin:Ljava/lang/String;

    return-object v0
.end method

.method public isVideoCaptureAllowed()Ljava/lang/Boolean;
    .locals 2

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/VoiceAndVideoCaptureInfo;->mOrigin:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/website/VoiceAndVideoCaptureInfo;->getEmbedderSafe()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferenceBridge;->nativeGetVideoCaptureSettingForOrigin(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 68
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 64
    :pswitch_0
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    goto :goto_0

    .line 66
    :pswitch_1
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    goto :goto_0

    .line 61
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public isVoiceCaptureAllowed()Ljava/lang/Boolean;
    .locals 2

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/website/VoiceAndVideoCaptureInfo;->mOrigin:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/website/VoiceAndVideoCaptureInfo;->getEmbedderSafe()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferenceBridge;->nativeGetVoiceCaptureSettingForOrigin(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 53
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 49
    :pswitch_0
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    goto :goto_0

    .line 51
    :pswitch_1
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    goto :goto_0

    .line 46
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public setVideoCaptureAllowed(Ljava/lang/Boolean;)V
    .locals 3

    .prologue
    .line 97
    const/4 v0, -0x1

    .line 98
    if-eqz p1, :cond_0

    .line 99
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 101
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/website/VoiceAndVideoCaptureInfo;->mOrigin:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/website/VoiceAndVideoCaptureInfo;->getEmbedderSafe()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferenceBridge;->nativeSetVideoCaptureSettingForOrigin(Ljava/lang/String;Ljava/lang/String;I)V

    .line 103
    return-void

    .line 99
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setVoiceCaptureAllowed(Ljava/lang/Boolean;)V
    .locals 3

    .prologue
    .line 84
    const/4 v0, -0x1

    .line 85
    if-eqz p1, :cond_0

    .line 86
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 88
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/website/VoiceAndVideoCaptureInfo;->mOrigin:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/website/VoiceAndVideoCaptureInfo;->getEmbedderSafe()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/chrome/preferences/website/WebsitePreferenceBridge;->nativeSetVoiceCaptureSettingForOrigin(Ljava/lang/String;Ljava/lang/String;I)V

    .line 90
    return-void

    .line 86
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
