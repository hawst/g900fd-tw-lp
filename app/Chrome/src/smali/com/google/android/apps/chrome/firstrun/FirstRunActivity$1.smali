.class Lcom/google/android/apps/chrome/firstrun/FirstRunActivity$1;
.super Lcom/google/android/apps/chrome/firstrun/FirstRunFlowSequencer;
.source "FirstRunActivity.java"


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 143
    iput-object p1, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity$1;->this$0:Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;

    invoke-direct {p0, p2, p3}, Lcom/google/android/apps/chrome/firstrun/FirstRunFlowSequencer;-><init>(Landroid/app/Activity;Landroid/os/Bundle;)V

    return-void
.end method


# virtual methods
.method public onFlowIsKnown(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 146
    if-nez p2, :cond_0

    .line 147
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity$1;->this$0:Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->completeFirstRunExperience()V

    .line 175
    :goto_0
    return-void

    .line 151
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity$1;->this$0:Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;

    # setter for: Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->mFreProperties:Landroid/os/Bundle;
    invoke-static {v0, p2}, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->access$002(Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;Landroid/os/Bundle;)Landroid/os/Bundle;

    .line 152
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity$1;->this$0:Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;

    iget-object v1, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity$1;->this$0:Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;

    # getter for: Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->mFreProperties:Landroid/os/Bundle;
    invoke-static {v1}, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->access$000(Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;)Landroid/os/Bundle;

    move-result-object v1

    const-string/jumbo v2, "ShowWelcome"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    # setter for: Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->mShowWelcomePage:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->access$102(Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;Z)Z

    .line 153
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity$1;->this$0:Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;

    # getter for: Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->mShowWelcomePage:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->access$100(Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity$1;->this$0:Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;

    # getter for: Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->mFreProperties:Landroid/os/Bundle;
    invoke-static {v0}, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->access$000(Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;)Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "SkipWelcomePageIfAcceptedToS"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 155
    iget-object v1, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity$1;->this$0:Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;

    sget-object v0, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->sGlue:Lcom/google/android/apps/chrome/firstrun/FirstRunGlue;

    iget-object v2, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity$1;->this$0:Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v0, v2}, Lcom/google/android/apps/chrome/firstrun/FirstRunGlue;->didAcceptTermsOfService(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x1

    :goto_1
    # setter for: Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->mShowWelcomePage:Z
    invoke-static {v1, v0}, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->access$102(Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;Z)Z

    .line 157
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity$1;->this$0:Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;

    # invokes: Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->createPageSequence()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->access$200(Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;)V

    .line 159
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity$1;->this$0:Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;

    # getter for: Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->mResultSignInAccountName:Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->access$300(Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 160
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity$1;->this$0:Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;

    iget-object v1, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity$1;->this$0:Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;

    # getter for: Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->mFreProperties:Landroid/os/Bundle;
    invoke-static {v1}, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->access$000(Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;)Landroid/os/Bundle;

    move-result-object v1

    const-string/jumbo v2, "ForceSigninAccountTo"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->mResultSignInAccountName:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->access$302(Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 164
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity$1;->this$0:Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;

    # getter for: Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->mPages:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->access$400(Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_4

    .line 165
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity$1;->this$0:Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->completeFirstRunExperience()V

    goto :goto_0

    .line 155
    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    .line 169
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity$1;->this$0:Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;

    new-instance v1, Lcom/google/android/apps/chrome/firstrun/FirstRunPagerAdapter;

    iget-object v2, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity$1;->this$0:Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity$1;->this$0:Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;

    # getter for: Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->mPages:Ljava/util/List;
    invoke-static {v3}, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->access$400(Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;)Ljava/util/List;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity$1;->this$0:Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;

    # getter for: Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->mFreProperties:Landroid/os/Bundle;
    invoke-static {v4}, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->access$000(Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;)Landroid/os/Bundle;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/google/android/apps/chrome/firstrun/FirstRunPagerAdapter;-><init>(Landroid/app/FragmentManager;Ljava/util/List;Landroid/os/Bundle;)V

    # setter for: Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->mPagerAdapter:Lcom/google/android/apps/chrome/firstrun/FirstRunPagerAdapter;
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->access$502(Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;Lcom/google/android/apps/chrome/firstrun/FirstRunPagerAdapter;)Lcom/google/android/apps/chrome/firstrun/FirstRunPagerAdapter;

    .line 171
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity$1;->this$0:Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;

    # invokes: Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->stopProgressionIfNotAcceptedTermsOfService()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->access$600(Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;)V

    .line 172
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity$1;->this$0:Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;

    # getter for: Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->mPager:Landroid/support/v4/view/ViewPager;
    invoke-static {v0}, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->access$700(Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;)Landroid/support/v4/view/ViewPager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity$1;->this$0:Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;

    # getter for: Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->mPagerAdapter:Lcom/google/android/apps/chrome/firstrun/FirstRunPagerAdapter;
    invoke-static {v1}, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->access$500(Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;)Lcom/google/android/apps/chrome/firstrun/FirstRunPagerAdapter;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->a(Landroid/support/v4/view/H;)V

    .line 174
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity$1;->this$0:Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;

    # invokes: Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->skipPagesIfNecessary()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;->access$800(Lcom/google/android/apps/chrome/firstrun/FirstRunActivity;)V

    goto/16 :goto_0
.end method
