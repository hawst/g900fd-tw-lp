.class public Lcom/google/android/apps/chrome/snapshot/SnapshotProvider;
.super Landroid/content/ContentProvider;
.source "SnapshotProvider.java"


# static fields
.field static final FULL_PROJECTION:[Ljava/lang/String;

.field private static final URI_MATCHER:Landroid/content/UriMatcher;


# instance fields
.field private mOpenHelper:Lcom/google/android/apps/chrome/snapshot/SnapshotProvider$DatabaseHelper;

.field private final mSnapshotProjectionMap:Ljava/util/Map;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 50
    new-instance v0, Landroid/content/UriMatcher;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    sput-object v0, Lcom/google/android/apps/chrome/snapshot/SnapshotProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    .line 56
    const/16 v0, 0x11

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string/jumbo v2, "createTime"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string/jumbo v2, "snapshotId"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string/jumbo v2, "printerId"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string/jumbo v2, "jobId"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string/jumbo v2, "pageUrlJobId"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "title"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "uri"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string/jumbo v2, "downloadUri"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string/jumbo v2, "pageUrlDownloadUri"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string/jumbo v2, "mimeType"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string/jumbo v2, "pageUrlMimeType"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string/jumbo v2, "localUri"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string/jumbo v2, "downloadId"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string/jumbo v2, "pageUrlDownloadId"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string/jumbo v2, "state"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string/jumbo v2, "pageUrlState"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/chrome/snapshot/SnapshotProvider;->FULL_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    .line 78
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotProvider;->mSnapshotProjectionMap:Ljava/util/Map;

    .line 115
    return-void
.end method

.method private static buildContentUri(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 348
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "content://"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static createContentValues(Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;)Landroid/content/ContentValues;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 311
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 312
    const-string/jumbo v0, "snapshotId"

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->getSnapshotId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 313
    const-string/jumbo v0, "createTime"

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->getCreateTime()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 314
    const-string/jumbo v0, "printerId"

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->getPrinterId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 315
    const-string/jumbo v0, "jobId"

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->getJobId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 316
    const-string/jumbo v0, "pageUrlJobId"

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->getPageUrlJobId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 317
    const-string/jumbo v0, "title"

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 318
    const-string/jumbo v3, "uri"

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->getPageUri()Landroid/net/Uri;

    move-result-object v0

    if-nez v0, :cond_0

    move-object v0, v1

    :goto_0
    invoke-virtual {v2, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 320
    const-string/jumbo v3, "downloadUri"

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->getDownloadUri()Landroid/net/Uri;

    move-result-object v0

    if-nez v0, :cond_1

    move-object v0, v1

    :goto_1
    invoke-virtual {v2, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 322
    const-string/jumbo v3, "pageUrlDownloadUri"

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->getPageUrlDownloadUri()Landroid/net/Uri;

    move-result-object v0

    if-nez v0, :cond_2

    move-object v0, v1

    :goto_2
    invoke-virtual {v2, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 324
    const-string/jumbo v0, "mimeType"

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->getMimeType()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 325
    const-string/jumbo v0, "pageUrlMimeType"

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->getPageUrlMimeType()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 326
    const-string/jumbo v3, "localUri"

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->getLocalUri()Landroid/net/Uri;

    move-result-object v0

    if-nez v0, :cond_3

    move-object v0, v1

    :goto_3
    invoke-virtual {v2, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 328
    const-string/jumbo v0, "downloadId"

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->getDownloadId()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 329
    const-string/jumbo v0, "pageUrlDownloadId"

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->getPageUrlDownloadId()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 330
    const-string/jumbo v3, "state"

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->getState()Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;

    move-result-object v0

    if-nez v0, :cond_4

    move-object v0, v1

    :goto_4
    invoke-virtual {v2, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 332
    const-string/jumbo v0, "pageUrlState"

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->getPageUrlState()Lcom/google/android/apps/chrome/snapshot/PageUrlJobState;

    move-result-object v3

    if-nez v3, :cond_5

    :goto_5
    invoke-virtual {v2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 334
    return-object v2

    .line 318
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->getPageUri()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 320
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->getDownloadUri()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 322
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->getPageUrlDownloadUri()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 326
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->getLocalUri()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 330
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->getState()Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;->toValue()Ljava/lang/String;

    move-result-object v0

    goto :goto_4

    .line 332
    :cond_5
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;->getPageUrlState()Lcom/google/android/apps/chrome/snapshot/PageUrlJobState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/snapshot/PageUrlJobState;->toValue()Ljava/lang/String;

    move-result-object v1

    goto :goto_5
.end method

.method static getDatabaseId(Landroid/net/Uri;)I
    .locals 2

    .prologue
    .line 344
    invoke-virtual {p0}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method static getNextDocument(Landroid/database/Cursor;)Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;
    .locals 24

    .prologue
    .line 270
    const-string/jumbo v2, "_id"

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    .line 271
    const-string/jumbo v2, "createTime"

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v5

    .line 272
    const-string/jumbo v2, "snapshotId"

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 273
    const-string/jumbo v2, "printerId"

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 274
    const-string/jumbo v2, "jobId"

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 275
    const-string/jumbo v2, "pageUrlJobId"

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 276
    const-string/jumbo v2, "title"

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 277
    const-string/jumbo v2, "uri"

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 278
    if-nez v2, :cond_0

    const/4 v12, 0x0

    .line 279
    :goto_0
    const-string/jumbo v2, "downloadUri"

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 280
    if-nez v2, :cond_1

    const/4 v13, 0x0

    .line 281
    :goto_1
    const-string/jumbo v2, "pageUrlDownloadUri"

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 283
    if-nez v2, :cond_2

    const/4 v14, 0x0

    .line 285
    :goto_2
    const-string/jumbo v2, "mimeType"

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 286
    const-string/jumbo v2, "pageUrlMimeType"

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 287
    const-string/jumbo v2, "localUri"

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 288
    if-nez v2, :cond_3

    const/16 v17, 0x0

    .line 289
    :goto_3
    const-string/jumbo v2, "downloadId"

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v18

    .line 290
    const-string/jumbo v2, "pageUrlDownloadId"

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v20

    .line 291
    const-string/jumbo v2, "state"

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 292
    if-nez v2, :cond_4

    const/16 v22, 0x0

    .line 294
    :goto_4
    const-string/jumbo v2, "pageUrlState"

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 295
    if-nez v2, :cond_5

    const/16 v23, 0x0

    .line 297
    :goto_5
    new-instance v3, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;

    invoke-direct/range {v3 .. v23}, Lcom/google/android/apps/chrome/snapshot/SnapshotDocument;-><init>(IJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;JJLcom/google/android/apps/chrome/snapshot/SnapshotJobState;Lcom/google/android/apps/chrome/snapshot/PageUrlJobState;)V

    return-object v3

    .line 278
    :cond_0
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v12

    goto/16 :goto_0

    .line 280
    :cond_1
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v13

    goto/16 :goto_1

    .line 283
    :cond_2
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v14

    goto/16 :goto_2

    .line 288
    :cond_3
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v17

    goto :goto_3

    .line 292
    :cond_4
    invoke-static {v2}, Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;->fromValue(Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/SnapshotJobState;

    move-result-object v22

    goto :goto_4

    .line 295
    :cond_5
    invoke-static {v2}, Lcom/google/android/apps/chrome/snapshot/PageUrlJobState;->fromValue(Ljava/lang/String;)Lcom/google/android/apps/chrome/snapshot/PageUrlJobState;

    move-result-object v23

    goto :goto_5
.end method

.method public static getSnapshotsIdBaseUri(Landroid/content/Context;)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 155
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ".snapshot"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "snapshots/"

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/snapshot/SnapshotProvider;->buildContentUri(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static getSnapshotsUri(Landroid/content/Context;)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 151
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ".snapshot"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "snapshots"

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/snapshot/SnapshotProvider;->buildContentUri(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method private initializeProjectionMap()V
    .locals 4

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotProvider;->mSnapshotProjectionMap:Ljava/util/Map;

    const-string/jumbo v1, "_id"

    const-string/jumbo v2, "_id"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 89
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotProvider;->mSnapshotProjectionMap:Ljava/util/Map;

    const-string/jumbo v1, "createTime"

    const-string/jumbo v2, "createTime"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 90
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotProvider;->mSnapshotProjectionMap:Ljava/util/Map;

    const-string/jumbo v1, "snapshotId"

    const-string/jumbo v2, "snapshotId"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 91
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotProvider;->mSnapshotProjectionMap:Ljava/util/Map;

    const-string/jumbo v1, "printerId"

    const-string/jumbo v2, "printerId"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 92
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotProvider;->mSnapshotProjectionMap:Ljava/util/Map;

    const-string/jumbo v1, "jobId"

    const-string/jumbo v2, "jobId"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 93
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotProvider;->mSnapshotProjectionMap:Ljava/util/Map;

    const-string/jumbo v1, "pageUrlJobId"

    const-string/jumbo v2, "pageUrlJobId"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 95
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotProvider;->mSnapshotProjectionMap:Ljava/util/Map;

    const-string/jumbo v1, "title"

    const-string/jumbo v2, "title"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 96
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotProvider;->mSnapshotProjectionMap:Ljava/util/Map;

    const-string/jumbo v1, "uri"

    const-string/jumbo v2, "uri"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 97
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotProvider;->mSnapshotProjectionMap:Ljava/util/Map;

    const-string/jumbo v1, "downloadUri"

    const-string/jumbo v2, "downloadUri"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 98
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotProvider;->mSnapshotProjectionMap:Ljava/util/Map;

    const-string/jumbo v1, "pageUrlDownloadUri"

    const-string/jumbo v2, "pageUrlDownloadUri"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 100
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotProvider;->mSnapshotProjectionMap:Ljava/util/Map;

    const-string/jumbo v1, "mimeType"

    const-string/jumbo v2, "mimeType"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 101
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotProvider;->mSnapshotProjectionMap:Ljava/util/Map;

    const-string/jumbo v1, "pageUrlMimeType"

    const-string/jumbo v2, "pageUrlMimeType"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 103
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotProvider;->mSnapshotProjectionMap:Ljava/util/Map;

    const-string/jumbo v1, "localUri"

    const-string/jumbo v2, "localUri"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 104
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotProvider;->mSnapshotProjectionMap:Ljava/util/Map;

    const-string/jumbo v1, "downloadId"

    const-string/jumbo v2, "downloadId"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 105
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotProvider;->mSnapshotProjectionMap:Ljava/util/Map;

    const-string/jumbo v1, "pageUrlDownloadId"

    const-string/jumbo v2, "pageUrlDownloadId"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 107
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotProvider;->mSnapshotProjectionMap:Ljava/util/Map;

    const-string/jumbo v1, "state"

    const-string/jumbo v2, "state"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 108
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotProvider;->mSnapshotProjectionMap:Ljava/util/Map;

    const-string/jumbo v1, "pageUrlState"

    const-string/jumbo v2, "pageUrlState"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 110
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ".snapshot"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 111
    sget-object v1, Lcom/google/android/apps/chrome/snapshot/SnapshotProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string/jumbo v2, "snapshots"

    const/4 v3, 0x1

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 112
    sget-object v1, Lcom/google/android/apps/chrome/snapshot/SnapshotProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string/jumbo v2, "snapshots/#"

    const/4 v3, 0x2

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 113
    return-void
.end method


# virtual methods
.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 4

    .prologue
    .line 217
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotProvider;->mOpenHelper:Lcom/google/android/apps/chrome/snapshot/SnapshotProvider$DatabaseHelper;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotProvider$DatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 219
    sget-object v0, Lcom/google/android/apps/chrome/snapshot/SnapshotProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 233
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Unknown URI for delete "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 221
    :pswitch_0
    const-string/jumbo v0, "snapshots"

    invoke-virtual {v1, v0, p2, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 235
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 236
    return v0

    .line 224
    :pswitch_1
    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v0, "_id = "

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    const/4 v3, 0x1

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 227
    if-eqz p2, :cond_0

    .line 228
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, " AND "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 230
    :cond_0
    const-string/jumbo v2, "snapshots"

    invoke-virtual {v1, v2, v0, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto :goto_0

    .line 219
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 189
    sget-object v0, Lcom/google/android/apps/chrome/snapshot/SnapshotProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 194
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Unknown URI for getType "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 192
    :pswitch_0
    const-string/jumbo v0, "vnd.android.cursor.item/vnd.android.chrome.snapshot"

    return-object v0

    .line 189
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 4

    .prologue
    .line 200
    sget-object v0, Lcom/google/android/apps/chrome/snapshot/SnapshotProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 201
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Unknown URI for insert "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 203
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotProvider;->mOpenHelper:Lcom/google/android/apps/chrome/snapshot/SnapshotProvider$DatabaseHelper;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotProvider$DatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 204
    const-string/jumbo v1, "snapshots"

    const-string/jumbo v2, "uri"

    invoke-virtual {v0, v1, v2, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v0

    .line 205
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_1

    .line 206
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/chrome/snapshot/SnapshotProvider;->getSnapshotsIdBaseUri(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v2, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    .line 209
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 210
    return-object v0

    .line 212
    :cond_1
    new-instance v0, Landroid/database/SQLException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Failed to insert row into "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onCreate()Z
    .locals 2

    .prologue
    .line 82
    invoke-direct {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotProvider;->initializeProjectionMap()V

    .line 83
    new-instance v0, Lcom/google/android/apps/chrome/snapshot/SnapshotProvider$DatabaseHelper;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/chrome/snapshot/SnapshotProvider$DatabaseHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotProvider;->mOpenHelper:Lcom/google/android/apps/chrome/snapshot/SnapshotProvider$DatabaseHelper;

    .line 84
    const/4 v0, 0x1

    return v0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 161
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 162
    const-string/jumbo v1, "snapshots"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 163
    sget-object v1, Lcom/google/android/apps/chrome/snapshot/SnapshotProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    invoke-virtual {v1, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 173
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Unknown URI for query "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 165
    :pswitch_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotProvider;->mSnapshotProjectionMap:Ljava/util/Map;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    .line 176
    :goto_0
    invoke-static {p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 177
    const-string/jumbo v7, "createTime DESC"

    .line 181
    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotProvider;->mOpenHelper:Lcom/google/android/apps/chrome/snapshot/SnapshotProvider$DatabaseHelper;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/snapshot/SnapshotProvider$DatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v6, v5

    .line 182
    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 183
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    .line 184
    return-object v0

    .line 168
    :pswitch_1
    iget-object v1, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotProvider;->mSnapshotProjectionMap:Ljava/util/Map;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    .line 169
    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "_id = "

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v1

    const/4 v3, 0x1

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_0
    move-object v7, p5

    .line 179
    goto :goto_1

    .line 163
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 4

    .prologue
    .line 241
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/SnapshotProvider;->mOpenHelper:Lcom/google/android/apps/chrome/snapshot/SnapshotProvider$DatabaseHelper;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotProvider$DatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 243
    sget-object v0, Lcom/google/android/apps/chrome/snapshot/SnapshotProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 256
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Unknown URI for update "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 245
    :pswitch_0
    const-string/jumbo v0, "snapshots"

    invoke-virtual {v1, v0, p2, p3, p4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 258
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 259
    return v0

    .line 248
    :pswitch_1
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    const/4 v2, 0x1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 249
    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "_id = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 250
    if-eqz p3, :cond_0

    .line 251
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, " AND "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 253
    :cond_0
    const-string/jumbo v2, "snapshots"

    invoke-virtual {v1, v2, p2, v0, p4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto :goto_0

    .line 243
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
