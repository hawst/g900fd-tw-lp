.class Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPage$1;
.super Ljava/lang/Object;
.source "IncognitoNewTabPage.java"

# interfaces
.implements Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPageView$IncognitoNewTabPageManager;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPage;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPage;)V
    .locals 0

    .prologue
    .line 38
    iput-object p1, p0, Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPage$1;->this$0:Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPage;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public loadIncognitoLearnMore()V
    .locals 3

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPage$1;->this$0:Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPage;

    # getter for: Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPage;->mTab:Lorg/chromium/chrome/browser/Tab;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPage;->access$000(Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPage;)Lorg/chromium/chrome/browser/Tab;

    move-result-object v0

    new-instance v1, Lorg/chromium/content_public/browser/LoadUrlParams;

    const-string/jumbo v2, "https://www.google.com/support/chrome/bin/answer.py?answer=95464"

    invoke-direct {v1, v2}, Lorg/chromium/content_public/browser/LoadUrlParams;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lorg/chromium/chrome/browser/Tab;->loadUrl(Lorg/chromium/content_public/browser/LoadUrlParams;)I

    .line 42
    return-void
.end method

.method public notifyLoadingComplete()V
    .locals 3

    .prologue
    .line 46
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 47
    const-string/jumbo v1, "tabId"

    iget-object v2, p0, Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPage$1;->this$0:Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPage;

    # getter for: Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPage;->mTab:Lorg/chromium/chrome/browser/Tab;
    invoke-static {v2}, Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPage;->access$000(Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPage;)Lorg/chromium/chrome/browser/Tab;

    move-result-object v2

    invoke-virtual {v2}, Lorg/chromium/chrome/browser/Tab;->getId()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 48
    const-string/jumbo v1, "incognito"

    iget-object v2, p0, Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPage$1;->this$0:Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPage;

    # getter for: Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPage;->mTab:Lorg/chromium/chrome/browser/Tab;
    invoke-static {v2}, Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPage;->access$000(Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPage;)Lorg/chromium/chrome/browser/Tab;

    move-result-object v2

    invoke-virtual {v2}, Lorg/chromium/chrome/browser/Tab;->isIncognito()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 49
    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPage$1;->this$0:Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPage;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/ntp/IncognitoNewTabPage;->getView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    const/16 v2, 0x20

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastNotification(Landroid/content/Context;ILandroid/os/Bundle;)V

    .line 51
    return-void
.end method
