.class public Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;
.super Lcom/google/android/apps/chrome/videofling/MediaRouteController;
.source "YouTubeMediaRouteController.java"

# interfaces
.implements Lcom/google/android/apps/chrome/videofling/YouTubePairingCodeGetter$Delegate;


# static fields
.field private static final NOTIFICATIONS:[I


# instance fields
.field private mCastDevice:Lcom/google/android/gms/cast/CastDevice;

.field private mConnectedClient:Lcom/google/android/gms/common/api/i;

.field private mConnectedRemoteMediaPlayer:Lcom/google/android/gms/cast/u;

.field private final mDebug:Z

.field private mLoadingPairingCode:Z

.field final mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

.field private mPairingCode:Ljava/lang/String;

.field private mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

.field private mYTPairingBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private mYouTubeLocalUrl:Ljava/lang/String;

.field private mYouTubeSessionId:Ljava/lang/String;

.field private mYtMediaStatusBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private mYtMediaStatusUpdateIntent:Landroid/app/PendingIntent;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 103
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->NOTIFICATIONS:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x5
        0x9
        0x19
    .end array-data
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 179
    invoke-direct {p0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;-><init>()V

    .line 92
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mLoadingPairingCode:Z

    .line 109
    new-instance v0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$1;-><init>(Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    .line 181
    invoke-static {}, Lorg/chromium/base/CommandLine;->getInstance()Lorg/chromium/base/CommandLine;

    move-result-object v0

    const-string/jumbo v1, "enable-cast-debug"

    invoke-virtual {v0, v1}, Lorg/chromium/base/CommandLine;->hasSwitch(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mDebug:Z

    .line 182
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;)V
    .locals 1

    .prologue
    .line 58
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->setOwnerTab(Lorg/chromium/chrome/browser/Tab;)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;)Z
    .locals 1

    .prologue
    .line 58
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mLoadingPairingCode:Z

    return v0
.end method

.method static synthetic access$1000(Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;)Landroid/content/BroadcastReceiver;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mYTPairingBroadcastReceiver:Landroid/content/BroadcastReceiver;

    return-object v0
.end method

.method static synthetic access$1002(Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;Landroid/content/BroadcastReceiver;)Landroid/content/BroadcastReceiver;
    .locals 0

    .prologue
    .line 58
    iput-object p1, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mYTPairingBroadcastReceiver:Landroid/content/BroadcastReceiver;

    return-object p1
.end method

.method static synthetic access$102(Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;Z)Z
    .locals 0

    .prologue
    .line 58
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mLoadingPairingCode:Z

    return p1
.end method

.method static synthetic access$1100(Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;)Z
    .locals 1

    .prologue
    .line 58
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mDebug:Z

    return v0
.end method

.method static synthetic access$1200(Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;)Lcom/google/android/gms/cast/CastDevice;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mCastDevice:Lcom/google/android/gms/cast/CastDevice;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;)V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->setupRemoteMediaPlayer()V

    return-void
.end method

.method static synthetic access$1402(Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 58
    iput-object p1, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mYouTubeSessionId:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1500(Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->getVideoId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1600(Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;Ljava/lang/String;J)Ljava/lang/String;
    .locals 2

    .prologue
    .line 58
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->createSendMessage(Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1700(Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;)Lcom/google/android/gms/cast/k;
    .locals 1

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->createMessageReceivedCallback()Lcom/google/android/gms/cast/k;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1800(Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;)Lcom/google/android/gms/common/api/o;
    .locals 1

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->createLaunchApplicationCallback()Lcom/google/android/gms/common/api/o;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1900(Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;)Lcom/google/android/gms/cast/u;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mConnectedRemoteMediaPlayer:Lcom/google/android/gms/cast/u;

    return-object v0
.end method

.method static synthetic access$1902(Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;Lcom/google/android/gms/cast/u;)Lcom/google/android/gms/cast/u;
    .locals 0

    .prologue
    .line 58
    iput-object p1, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mConnectedRemoteMediaPlayer:Lcom/google/android/gms/cast/u;

    return-object p1
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;)V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->startUserControls()V

    return-void
.end method

.method static synthetic access$2000(Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;II)I
    .locals 1

    .prologue
    .line 58
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->playerStateToPlaybackState(II)I

    move-result v0

    return v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;)Lcom/google/android/gms/common/api/i;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mConnectedClient:Lcom/google/android/gms/common/api/i;

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;Lcom/google/android/gms/common/api/i;)Lcom/google/android/gms/common/api/i;
    .locals 0

    .prologue
    .line 58
    iput-object p1, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mConnectedClient:Lcom/google/android/gms/common/api/i;

    return-object p1
.end method

.method static synthetic access$400(Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;)V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->installMediaStatusBroadcastReceiver()V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;)V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->setupMediaStatusUpdateIntent()V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mYouTubeLocalUrl:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$602(Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 58
    iput-object p1, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mYouTubeLocalUrl:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$700(Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;)V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->doStartUserControls()V

    return-void
.end method

.method static synthetic access$800(Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mPairingCode:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$802(Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 58
    iput-object p1, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mPairingCode:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$900(Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;)V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->applyPairingCode()V

    return-void
.end method

.method private applyPairingCode()V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 580
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mPairingCode:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mPairingCode:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mYouTubeLocalUrl:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 581
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mDebug:Z

    if-eqz v0, :cond_0

    .line 582
    const-string/jumbo v0, "YouTubeMediaRouteController"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Applying pairing code of "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mPairingCode:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " to "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mYouTubeLocalUrl:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 585
    :cond_0
    const-string/jumbo v0, "306d8b5f-b273-4c3a-815c-c96726e499d9"

    iput-object v0, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mCurrentItemId:Ljava/lang/String;

    .line 586
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mYouTubeLocalUrl:Ljava/lang/String;

    const-string/jumbo v3, "/embed/"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 587
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mYouTubeLocalUrl:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->createWatchPageUrlFromEmbed(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mYouTubeLocalUrl:Ljava/lang/String;

    move v0, v1

    .line 590
    :goto_0
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mLoadingPairingCode:Z

    .line 591
    const-string/jumbo v3, "%s&pairingCode=%s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mYouTubeLocalUrl:Ljava/lang/String;

    aput-object v5, v4, v2

    iget-object v2, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mPairingCode:Ljava/lang/String;

    aput-object v2, v4, v1

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 592
    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->loadPairingUrl(Ljava/lang/String;Z)V

    .line 594
    :cond_1
    return-void

    :cond_2
    move v0, v2

    goto :goto_0
.end method

.method public static buildYouTubeMediaRouteSelector()Landroid/support/v7/media/e;
    .locals 2

    .prologue
    .line 170
    new-instance v0, Landroid/support/v7/media/f;

    invoke-direct {v0}, Landroid/support/v7/media/f;-><init>()V

    .line 171
    const-string/jumbo v1, "233637DE"

    invoke-static {v1}, Lcom/google/android/gms/cast/CastMediaControlIntent;->categoryForRemotePlayback(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/media/f;->a(Ljava/lang/String;)Landroid/support/v7/media/f;

    .line 173
    const-string/jumbo v1, "EXTERNAL_MDX_MEDIA_ROUTE_CONTROL_CATEGORY"

    invoke-virtual {v0, v1}, Landroid/support/v7/media/f;->a(Ljava/lang/String;)Landroid/support/v7/media/f;

    .line 174
    invoke-virtual {v0}, Landroid/support/v7/media/f;->a()Landroid/support/v7/media/e;

    move-result-object v0

    .line 175
    return-object v0
.end method

.method private createCastListener()Lcom/google/android/gms/cast/j;
    .locals 1

    .prologue
    .line 619
    new-instance v0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$8;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$8;-><init>(Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;)V

    return-object v0
.end method

.method private createConnectionCallback(Lcom/google/android/gms/common/api/i;Landroid/support/v7/media/MediaRouter;Landroid/support/v7/media/MediaRouter$RouteInfo;)Lcom/google/android/gms/common/api/k;
    .locals 1

    .prologue
    .line 747
    new-instance v0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$11;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$11;-><init>(Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;Lcom/google/android/gms/common/api/i;)V

    return-object v0
.end method

.method private createLaunchApplicationCallback()Lcom/google/android/gms/common/api/o;
    .locals 1

    .prologue
    .line 682
    new-instance v0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$10;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$10;-><init>(Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;)V

    return-object v0
.end method

.method private createMessageReceivedCallback()Lcom/google/android/gms/cast/k;
    .locals 1

    .prologue
    .line 651
    .line 652
    new-instance v0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$9;

    invoke-direct {v0, p0, p0}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$9;-><init>(Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;)V

    return-object v0
.end method

.method private createOnStatusUpdatedListener(Lcom/google/android/gms/cast/u;)Lcom/google/android/gms/cast/z;
    .locals 1

    .prologue
    .line 831
    new-instance v0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$12;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$12;-><init>(Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;Lcom/google/android/gms/cast/u;)V

    return-object v0
.end method

.method private createSendMessage(Ljava/lang/String;J)Ljava/lang/String;
    .locals 8

    .prologue
    .line 641
    sget-object v0, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const-string/jumbo v1, "{\"type\":\"flingVideo\",\"data\":{\"videoId\":\"%s\",\"currentTime\":%.2f}}"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    long-to-double v4, p2

    const-wide v6, 0x408f400000000000L    # 1000.0

    div-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 646
    iget-boolean v1, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mDebug:Z

    if-eqz v1, :cond_0

    const-string/jumbo v1, "YouTubeMediaRouteController"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "createSendMessage(): "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 647
    :cond_0
    return-object v0
.end method

.method private createWatchPageUrlFromEmbed(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 549
    const-string/jumbo v0, "/embed/"

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    add-int/lit8 v1, v0, 0x7

    .line 552
    const/16 v0, 0x3f

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->indexOf(II)I

    move-result v0

    .line 553
    if-gez v0, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    :cond_0
    invoke-virtual {p1, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 556
    const-string/jumbo v1, "http://www.youtube.com/watch?v=%s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 558
    return-object v0
.end method

.method private doStartUserControls()V
    .locals 2

    .prologue
    .line 260
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->updateState(I)V

    .line 263
    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->instance(J)Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;->PLAYING:Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;

    invoke-virtual {v0, v1, p0}, Lcom/google/android/apps/chrome/videofling/RemoteMediaPlayerController;->startNotificationAndLockScreen(Lcom/google/android/apps/chrome/videofling/RemoteVideoInfo$PlayerState;Lcom/google/android/apps/chrome/videofling/MediaRouteController;)V

    .line 266
    const-string/jumbo v0, "YouTube"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->updateTitle(Ljava/lang/String;)V

    .line 267
    return-void
.end method

.method private getVideoId(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 600
    const-string/jumbo v0, "/embed/"

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    if-ltz v0, :cond_1

    const/4 v0, 0x1

    .line 601
    :goto_0
    if-eqz v0, :cond_2

    .line 602
    const-string/jumbo v0, "/embed/"

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    add-int/lit8 v1, v0, 0x7

    .line 605
    const/16 v0, 0x3f

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->indexOf(II)I

    move-result v0

    .line 606
    if-gez v0, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    :cond_0
    invoke-virtual {p1, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 615
    :goto_1
    return-object v0

    .line 600
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 608
    :cond_2
    const-string/jumbo v0, "v="

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    if-ltz v0, :cond_4

    .line 609
    const-string/jumbo v0, "v="

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    add-int/lit8 v1, v0, 0x2

    .line 611
    const/16 v0, 0x26

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->indexOf(II)I

    move-result v0

    .line 612
    if-gez v0, :cond_3

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    :cond_3
    invoke-virtual {p1, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 615
    :cond_4
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private installMediaStatusBroadcastReceiver()V
    .locals 3

    .prologue
    .line 494
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mYtMediaStatusBroadcastReceiver:Landroid/content/BroadcastReceiver;

    if-nez v0, :cond_0

    .line 495
    new-instance v0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$6;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$6;-><init>(Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mYtMediaStatusBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 503
    new-instance v0, Landroid/content/IntentFilter;

    const-string/jumbo v1, "com.google.android.apps.youtube.app.remote.action.WATCH_STATUS"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 505
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mYtMediaStatusBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 508
    :cond_0
    return-void
.end method

.method private installPairCodeReceiver()V
    .locals 3

    .prologue
    .line 332
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mYTPairingBroadcastReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    .line 353
    :goto_0
    return-void

    .line 334
    :cond_0
    new-instance v0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$5;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$5;-><init>(Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mYTPairingBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 350
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 351
    const-string/jumbo v1, "com.google.android.youtube.action.mrp_pairing_code_registered"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 352
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mYTPairingBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    goto :goto_0
.end method

.method private loadPairingUrl(Ljava/lang/String;Z)V
    .locals 5

    .prologue
    .line 479
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mDebug:Z

    if-eqz v0, :cond_0

    const-string/jumbo v0, "YouTubeMediaRouteController"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Loading YouTube URL: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 481
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->getOwnerTab()Lorg/chromium/chrome/browser/Tab;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->getOwnerTab()Lorg/chromium/chrome/browser/Tab;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/Tab;->isClosing()Z

    move-result v0

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    .line 482
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->getOwnerTab()Lorg/chromium/chrome/browser/Tab;

    move-result-object v0

    new-instance v1, Lorg/chromium/content_public/browser/LoadUrlParams;

    const/16 v2, 0x8

    invoke-direct {v1, p1, v2}, Lorg/chromium/content_public/browser/LoadUrlParams;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v0, v1}, Lorg/chromium/chrome/browser/Tab;->loadUrl(Lorg/chromium/content_public/browser/LoadUrlParams;)I

    .line 491
    :goto_0
    return-void

    .line 486
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->resetCurrentActivity()V

    .line 487
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    new-instance v1, Lorg/chromium/content_public/browser/LoadUrlParams;

    invoke-direct {v1, p1}, Lorg/chromium/content_public/browser/LoadUrlParams;-><init>(Ljava/lang/String;)V

    sget-object v2, Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;->FROM_EXTERNAL_APP:Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    invoke-interface {v4}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->isIncognitoSelected()Z

    move-result v4

    invoke-interface {v0, v1, v2, v3, v4}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->openNewTab(Lorg/chromium/content_public/browser/LoadUrlParams;Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;Lorg/chromium/chrome/browser/Tab;Z)Lorg/chromium/chrome/browser/Tab;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->setOwnerTab(Lorg/chromium/chrome/browser/Tab;)V

    goto :goto_0
.end method

.method private playerStateToPlaybackState(II)I
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 799
    packed-switch p1, :pswitch_data_0

    .line 822
    :goto_0
    return v0

    .line 801
    :pswitch_0
    const/4 v0, 0x3

    goto :goto_0

    .line 803
    :pswitch_1
    packed-switch p2, :pswitch_data_1

    goto :goto_0

    .line 809
    :pswitch_2
    const/4 v0, 0x4

    goto :goto_0

    .line 805
    :pswitch_3
    const/4 v0, 0x5

    goto :goto_0

    .line 807
    :pswitch_4
    const/4 v0, 0x7

    goto :goto_0

    .line 811
    :pswitch_5
    const/4 v0, 0x6

    goto :goto_0

    .line 817
    :pswitch_6
    const/4 v0, 0x2

    goto :goto_0

    .line 819
    :pswitch_7
    const/4 v0, 0x1

    goto :goto_0

    .line 799
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_7
        :pswitch_6
        :pswitch_0
    .end packed-switch

    .line 803
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_5
        :pswitch_4
    .end packed-switch
.end method

.method private resetCurrentActivity()V
    .locals 1

    .prologue
    .line 301
    invoke-static {}, Lcom/google/android/apps/chrome/videofling/RemoteMediaUtils;->getCurrentActivity()Lcom/google/android/apps/chrome/ChromeActivity;

    move-result-object v0

    .line 303
    if-eqz v0, :cond_0

    .line 304
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeActivity;->getTabModelSelector()Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    .line 305
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mLoadingPairingCode:Z

    .line 307
    :cond_0
    return-void
.end method

.method private setupMediaStatusUpdateIntent()V
    .locals 4

    .prologue
    .line 524
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "com.google.android.apps.youtube.app.remote.action.WATCH_STATUS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 525
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    const/high16 v3, 0x8000000

    invoke-static {v1, v2, v0, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mYtMediaStatusUpdateIntent:Landroid/app/PendingIntent;

    .line 528
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "com.google.android.apps.youtube.app.remote.action.WATCH_STATUS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 529
    const-string/jumbo v1, "android.media.intent.extra.ITEM_STATUS_UPDATE_RECEIVER"

    iget-object v2, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mYtMediaStatusUpdateIntent:Landroid/app/PendingIntent;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 531
    new-instance v1, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$7;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$7;-><init>(Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;)V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->sendIntentToRoute(Landroid/content/Intent;Lcom/google/android/apps/chrome/videofling/MediaRouteController$ResultBundleHandler;)V

    .line 545
    return-void
.end method

.method private setupRemoteMediaPlayer()V
    .locals 4

    .prologue
    .line 781
    new-instance v1, Lcom/google/android/gms/cast/u;

    invoke-direct {v1}, Lcom/google/android/gms/cast/u;-><init>()V

    .line 783
    :try_start_0
    sget-object v0, Lcom/google/android/gms/cast/a;->c:Lcom/google/android/gms/cast/d;

    iget-object v2, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mConnectedClient:Lcom/google/android/gms/common/api/i;

    invoke-virtual {v1}, Lcom/google/android/gms/cast/u;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3, v1}, Lcom/google/android/gms/cast/d;->a(Lcom/google/android/gms/common/api/i;Ljava/lang/String;Lcom/google/android/gms/cast/k;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 790
    :goto_0
    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->createOnStatusUpdatedListener(Lcom/google/android/gms/cast/u;)Lcom/google/android/gms/cast/z;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/cast/u;->a(Lcom/google/android/gms/cast/z;)V

    .line 792
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mConnectedClient:Lcom/google/android/gms/common/api/i;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/cast/u;->a(Lcom/google/android/gms/common/api/i;)Lcom/google/android/gms/common/api/l;

    .line 793
    return-void

    .line 785
    :catch_0
    move-exception v0

    .line 786
    const-string/jumbo v2, "YouTubeMediaRouteController"

    const-string/jumbo v3, "setupRemoteMediaPlayer state error: "

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 787
    :catch_1
    move-exception v0

    .line 788
    const-string/jumbo v2, "YouTubeMediaRouteController"

    const-string/jumbo v3, "setupRemoteMediaPlayer IO error: "

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private startUserControls()V
    .locals 2

    .prologue
    .line 273
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mConnectedClient:Lcom/google/android/gms/common/api/i;

    if-eqz v0, :cond_0

    .line 274
    invoke-direct {p0}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->doStartUserControls()V

    .line 292
    :goto_0
    return-void

    .line 278
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.media.intent.action.GET_STATUS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 279
    new-instance v1, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$4;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$4;-><init>(Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;)V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->sendIntentToRoute(Landroid/content/Intent;Lcom/google/android/apps/chrome/videofling/MediaRouteController$ResultBundleHandler;)V

    goto :goto_0
.end method


# virtual methods
.method public buildMediaRouteSelector()Landroid/support/v7/media/e;
    .locals 1

    .prologue
    .line 163
    invoke-static {}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->buildYouTubeMediaRouteSelector()Landroid/support/v7/media/e;

    move-result-object v0

    return-object v0
.end method

.method createClient(Lcom/google/android/gms/cast/CastDevice;Landroid/content/Context;)Lcom/google/android/gms/common/api/i;
    .locals 3

    .prologue
    .line 633
    invoke-direct {p0}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->createCastListener()Lcom/google/android/gms/cast/j;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/google/android/gms/cast/h;->a(Lcom/google/android/gms/cast/CastDevice;Lcom/google/android/gms/cast/j;)Lcom/google/android/gms/cast/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/cast/i;->a()Lcom/google/android/gms/cast/h;

    move-result-object v0

    .line 635
    new-instance v1, Lcom/google/android/gms/common/api/j;

    invoke-direct {v1, p2}, Lcom/google/android/gms/common/api/j;-><init>(Landroid/content/Context;)V

    .line 636
    sget-object v2, Lcom/google/android/gms/cast/a;->b:Lcom/google/android/gms/common/api/a;

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/common/api/j;->a(Lcom/google/android/gms/common/api/a;Landroid/support/v4/view/i;)Lcom/google/android/gms/common/api/j;

    .line 637
    invoke-virtual {v1}, Lcom/google/android/gms/common/api/j;->a()Lcom/google/android/gms/common/api/i;

    move-result-object v0

    return-object v0
.end method

.method public currentRouteSupportsDomain(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 407
    invoke-static {p1}, Lcom/google/android/apps/chrome/videofling/RemoteMediaUtils;->isYouTubeUrl(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public currentRouteSupportsRemotePlayback()Z
    .locals 1

    .prologue
    .line 468
    const/4 v0, 0x1

    return v0
.end method

.method public initialize(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 296
    invoke-direct {p0}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->resetCurrentActivity()V

    .line 297
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->initialize(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method protected onActivitiesDestroyed()V
    .locals 2

    .prologue
    .line 320
    invoke-super {p0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->onActivitiesDestroyed()V

    .line 322
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mYTPairingBroadcastReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    .line 323
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mYTPairingBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 324
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mYTPairingBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 326
    :cond_0
    return-void
.end method

.method public onPairingCodeReady(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 250
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 251
    :cond_0
    const-string/jumbo v0, "YouTubeMediaRouteController"

    const-string/jumbo v1, "Haven\'t received a valid pairing code"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 252
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->release()V

    .line 257
    :goto_0
    return-void

    .line 255
    :cond_1
    iput-object p1, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mPairingCode:Ljava/lang/String;

    .line 256
    invoke-direct {p0}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->applyPairingCode()V

    goto :goto_0
.end method

.method public onRouteSelected(Landroid/support/v7/media/MediaRouter;Landroid/support/v7/media/MediaRouter$RouteInfo;)V
    .locals 5

    .prologue
    .line 357
    const-string/jumbo v0, "233637DE"

    invoke-static {v0}, Lcom/google/android/gms/cast/CastMediaControlIntent;->categoryForRemotePlayback(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/support/v7/media/MediaRouter$RouteInfo;->a(Ljava/lang/String;)Z

    move-result v0

    .line 359
    const-string/jumbo v1, "EXTERNAL_MDX_MEDIA_ROUTE_CONTROL_CATEGORY"

    invoke-virtual {p2, v1}, Landroid/support/v7/media/MediaRouter$RouteInfo;->a(Ljava/lang/String;)Z

    move-result v1

    .line 361
    if-eqz v0, :cond_0

    .line 362
    const/4 v1, 0x1

    invoke-static {v1}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->remotePlaybackDeviceSelected(I)V

    .line 373
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->getListeners()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 374
    invoke-virtual {p2}, Landroid/support/v7/media/MediaRouter$RouteInfo;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->showCastError(Ljava/lang/String;)V

    .line 375
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->release()V

    .line 403
    :goto_1
    return-void

    .line 363
    :cond_0
    if-eqz v1, :cond_1

    .line 364
    const/4 v1, 0x2

    invoke-static {v1}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->remotePlaybackDeviceSelected(I)V

    goto :goto_0

    .line 367
    :cond_1
    const-string/jumbo v0, "YouTubeMediaRouteController"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Unknown category of the media route: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 368
    invoke-virtual {p2}, Landroid/support/v7/media/MediaRouter$RouteInfo;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->showCastError(Ljava/lang/String;)V

    .line 369
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->release()V

    goto :goto_1

    .line 379
    :cond_2
    if-eqz v0, :cond_4

    .line 380
    invoke-virtual {p2}, Landroid/support/v7/media/MediaRouter$RouteInfo;->m()Landroid/os/Bundle;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/cast/CastDevice;->b(Landroid/os/Bundle;)Lcom/google/android/gms/cast/CastDevice;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mCastDevice:Lcom/google/android/gms/cast/CastDevice;

    .line 381
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mCastDevice:Lcom/google/android/gms/cast/CastDevice;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->createClient(Lcom/google/android/gms/cast/CastDevice;Landroid/content/Context;)Lcom/google/android/gms/common/api/i;

    move-result-object v0

    .line 382
    invoke-direct {p0, v0, p1, p2}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->createConnectionCallback(Lcom/google/android/gms/common/api/i;Landroid/support/v7/media/MediaRouter;Landroid/support/v7/media/MediaRouter$RouteInfo;)Lcom/google/android/gms/common/api/k;

    move-result-object v1

    .line 384
    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/i;->a(Lcom/google/android/gms/common/api/k;)V

    .line 385
    invoke-interface {v0}, Lcom/google/android/gms/common/api/i;->a()V

    .line 390
    :goto_2
    invoke-virtual {p0, p2}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->registerRoute(Landroid/support/v7/media/MediaRouter$RouteInfo;)V

    .line 394
    invoke-static {}, Lorg/chromium/content/browser/ContentVideoView;->getContentVideoView()Lorg/chromium/content/browser/ContentVideoView;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 395
    invoke-static {}, Lorg/chromium/content/browser/ContentVideoView;->getContentVideoView()Lorg/chromium/content/browser/ContentVideoView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lorg/chromium/content/browser/ContentVideoView;->exitFullscreen(Z)V

    .line 398
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->getListeners()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/videofling/MediaRouteController$Listener;

    .line 399
    invoke-virtual {p2}, Landroid/support/v7/media/MediaRouter$RouteInfo;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->getOwnerTab()Lorg/chromium/chrome/browser/Tab;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->getNativePlayerId()I

    move-result v4

    invoke-interface {v0, v2, v3, v4, p0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController$Listener;->onRouteSelected(Ljava/lang/String;Lorg/chromium/chrome/browser/Tab;ILcom/google/android/apps/chrome/videofling/MediaRouteController;)V

    goto :goto_3

    .line 387
    :cond_4
    invoke-direct {p0}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->installPairCodeReceiver()V

    goto :goto_2

    .line 402
    :cond_5
    sget-object v0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->NOTIFICATIONS:[I

    iget-object v1, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->registerForNotifications([ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    goto/16 :goto_1
.end method

.method public pause()V
    .locals 3

    .prologue
    .line 208
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mConnectedRemoteMediaPlayer:Lcom/google/android/gms/cast/u;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mConnectedClient:Lcom/google/android/gms/common/api/i;

    if-nez v0, :cond_1

    .line 209
    :cond_0
    invoke-super {p0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->pause()V

    .line 226
    :goto_0
    return-void

    .line 213
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mConnectedRemoteMediaPlayer:Lcom/google/android/gms/cast/u;

    iget-object v1, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mConnectedClient:Lcom/google/android/gms/common/api/i;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/u;->a(Lcom/google/android/gms/common/api/i;Lorg/json/JSONObject;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$3;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$3;-><init>(Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/l;->a(Lcom/google/android/gms/common/api/o;)V

    .line 225
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->updateState(I)V

    goto :goto_0
.end method

.method public prepareAsync(Ljava/lang/String;J)V
    .locals 1

    .prologue
    .line 569
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->castPlayRequested()V

    .line 570
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mYouTubeLocalUrl:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 573
    :goto_0
    return-void

    .line 571
    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mYouTubeLocalUrl:Ljava/lang/String;

    .line 572
    invoke-direct {p0}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->applyPairingCode()V

    goto :goto_0
.end method

.method public prepareMediaRoute()V
    .locals 3

    .prologue
    .line 311
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v7/media/MediaRouter;->a(Landroid/content/Context;)Landroid/support/v7/media/MediaRouter;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->buildMediaRouteSelector()Landroid/support/v7/media/e;

    move-result-object v1

    const/4 v2, 0x5

    invoke-virtual {v0, v1, p0, v2}, Landroid/support/v7/media/MediaRouter;->a(Landroid/support/v7/media/e;Landroid/support/v7/media/g;I)V

    .line 316
    return-void
.end method

.method protected processMediaStatusBundle(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 512
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->processMediaStatusBundle(Landroid/os/Bundle;)V

    .line 516
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mConnectedClient:Lcom/google/android/gms/common/api/i;

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->setupMediaStatusUpdateIntent()V

    .line 517
    :cond_0
    return-void
.end method

.method public reconnectAnyExistingRoute()V
    .locals 0

    .prologue
    .line 159
    return-void
.end method

.method public release()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 423
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mDebug:Z

    if-eqz v0, :cond_0

    const-string/jumbo v0, "YouTubeMediaRouteController"

    const-string/jumbo v1, "In release"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 425
    :cond_0
    iput-object v4, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mConnectedRemoteMediaPlayer:Lcom/google/android/gms/cast/u;

    .line 426
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mConnectedClient:Lcom/google/android/gms/common/api/i;

    if-eqz v0, :cond_1

    .line 427
    sget-object v0, Lcom/google/android/gms/cast/a;->c:Lcom/google/android/gms/cast/d;

    iget-object v1, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mConnectedClient:Lcom/google/android/gms/common/api/i;

    iget-object v2, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mYouTubeSessionId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/d;->a(Lcom/google/android/gms/common/api/i;Ljava/lang/String;)Lcom/google/android/gms/common/api/l;

    .line 428
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mConnectedClient:Lcom/google/android/gms/common/api/i;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/i;->b()V

    .line 430
    :cond_1
    iput-object v4, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mConnectedClient:Lcom/google/android/gms/common/api/i;

    .line 431
    iput-object v4, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mCastDevice:Lcom/google/android/gms/cast/CastDevice;

    .line 433
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    if-eqz v0, :cond_2

    .line 434
    sget-object v0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->NOTIFICATIONS:[I

    iget-object v1, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->unregisterForNotifications([ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    .line 437
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mYTPairingBroadcastReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_3

    .line 438
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mYTPairingBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 439
    iput-object v4, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mYTPairingBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 442
    :cond_3
    iput-object v4, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mPairingCode:Ljava/lang/String;

    .line 446
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->getOwnerTab()Lorg/chromium/chrome/browser/Tab;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->getOwnerTab()Lorg/chromium/chrome/browser/Tab;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/Tab;->isClosing()Z

    move-result v0

    if-nez v0, :cond_4

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->getOwnerTab()Lorg/chromium/chrome/browser/Tab;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/Tab;->getUrl()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "pairingCode="

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 448
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->getOwnerTab()Lorg/chromium/chrome/browser/Tab;

    move-result-object v0

    new-instance v1, Lorg/chromium/content_public/browser/LoadUrlParams;

    iget-object v2, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mYouTubeLocalUrl:Ljava/lang/String;

    const/16 v3, 0x8

    invoke-direct {v1, v2, v3}, Lorg/chromium/content_public/browser/LoadUrlParams;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v0, v1}, Lorg/chromium/chrome/browser/Tab;->loadUrl(Lorg/chromium/content_public/browser/LoadUrlParams;)I

    .line 451
    :cond_4
    iput-object v4, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    .line 453
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->getListeners()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/videofling/MediaRouteController$Listener;

    .line 454
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->getOwnerTab()Lorg/chromium/chrome/browser/Tab;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->getNativePlayerId()I

    move-result v3

    invoke-interface {v0, v2, v3, p0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController$Listener;->onRouteUnselected(Lorg/chromium/chrome/browser/Tab;ILcom/google/android/apps/chrome/videofling/MediaRouteController;)V

    goto :goto_0

    .line 456
    :cond_5
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->removeAllListeners()V

    .line 457
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->clearItemState()V

    .line 458
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->clearMediaRoute()V

    .line 459
    invoke-virtual {p0, v4}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->setOwnerTab(Lorg/chromium/chrome/browser/Tab;)V

    .line 461
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v7/media/MediaRouter;->a(Landroid/content/Context;)Landroid/support/v7/media/MediaRouter;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/support/v7/media/MediaRouter;->a(Landroid/support/v7/media/g;)V

    .line 462
    return-void
.end method

.method public resume()V
    .locals 3

    .prologue
    .line 186
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mConnectedRemoteMediaPlayer:Lcom/google/android/gms/cast/u;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mConnectedClient:Lcom/google/android/gms/common/api/i;

    if-nez v0, :cond_1

    .line 187
    :cond_0
    invoke-super {p0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->resume()V

    .line 204
    :goto_0
    return-void

    .line 191
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mConnectedRemoteMediaPlayer:Lcom/google/android/gms/cast/u;

    iget-object v1, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mConnectedClient:Lcom/google/android/gms/common/api/i;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/u;->b(Lcom/google/android/gms/common/api/i;Lorg/json/JSONObject;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$2;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$2;-><init>(Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/l;->a(Lcom/google/android/gms/common/api/o;)V

    .line 203
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->updateState(I)V

    goto :goto_0
.end method

.method protected sendIntentToRoute(Landroid/content/Intent;Lcom/google/android/apps/chrome/videofling/MediaRouteController$ResultBundleHandler;)V
    .locals 0

    .prologue
    .line 413
    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->sendControlIntent(Landroid/content/Intent;Lcom/google/android/apps/chrome/videofling/MediaRouteController$ResultBundleHandler;)V

    .line 414
    return-void
.end method

.method public setRemoteVolume(I)V
    .locals 6

    .prologue
    .line 230
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mConnectedRemoteMediaPlayer:Lcom/google/android/gms/cast/u;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mConnectedClient:Lcom/google/android/gms/common/api/i;

    if-nez v0, :cond_1

    .line 231
    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->setRemoteVolume(I)V

    .line 246
    :goto_0
    return-void

    .line 235
    :cond_1
    sget-object v0, Lcom/google/android/gms/cast/a;->c:Lcom/google/android/gms/cast/d;

    iget-object v1, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mConnectedClient:Lcom/google/android/gms/common/api/i;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/cast/d;->a(Lcom/google/android/gms/common/api/i;)D

    move-result-wide v0

    .line 236
    int-to-double v2, p1

    const-wide/high16 v4, 0x4024000000000000L    # 10.0

    div-double/2addr v2, v4

    add-double/2addr v0, v2

    .line 238
    :try_start_0
    sget-object v2, Lcom/google/android/gms/cast/a;->c:Lcom/google/android/gms/cast/d;

    iget-object v3, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mConnectedClient:Lcom/google/android/gms/common/api/i;

    invoke-virtual {v2, v3, v0, v1}, Lcom/google/android/gms/cast/d;->a(Lcom/google/android/gms/common/api/i;D)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_0

    .line 239
    :catch_0
    move-exception v0

    .line 240
    const-string/jumbo v1, "YouTubeMediaRouteController"

    const-string/jumbo v2, "Failed to set the volume"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 241
    :catch_1
    move-exception v0

    .line 242
    const-string/jumbo v1, "YouTubeMediaRouteController"

    const-string/jumbo v2, "Failed to set the volume"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 243
    :catch_2
    move-exception v0

    .line 244
    const-string/jumbo v1, "YouTubeMediaRouteController"

    const-string/jumbo v2, "Failed to set the volume"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
