.class public Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;
.super Ljava/lang/Object;
.source "LayoutTab.java"

# interfaces
.implements Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;


# static fields
.field public static final ALPHA_THRESHOLD:F = 0.003921569f

.field public static final CLOSE_BUTTON_WIDTH_DP:F = 36.0f

.field private static sCloseButtonSlop:F

.field private static sDpToPx:F

.field private static sPxToDp:F


# instance fields
.field private mAlpha:F

.field private mAnonymizeToolbar:Z

.field private mBackgroundColor:I

.field private mBorderAlpha:F

.field private mBorderCloseButtonAlpha:F

.field private mBorderScale:F

.field private final mBounds:Landroid/graphics/RectF;

.field private mBrightness:F

.field private mCanUseLiveTexture:Z

.field private mClippedHeight:F

.field private mClippedWidth:F

.field private mClippedX:F

.field private mClippedY:F

.field private final mClosePlacement:Landroid/graphics/RectF;

.field private mDecorationAlpha:F

.field private mFallbackThumbnailId:I

.field private final mId:I

.field private mInitFromHostCalled:Z

.field private mInsetBorderVertical:Z

.field private final mIsIncognito:Z

.field private mIsTitleNeeded:Z

.field private mMaxContentHeight:F

.field private mMaxContentWidth:F

.field private mOriginalContentHeight:F

.field private mOriginalContentWidth:F

.field private mRenderX:F

.field private mRenderY:F

.field private mSaturation:F

.field private mScale:F

.field private mShouldStall:Z

.field private mShowToolbar:Z

.field private mSideBorderScale:F

.field private mStaticToViewBlend:F

.field private mTiltX:F

.field private mTiltXPivotOffset:F

.field private mTiltY:F

.field private mTiltYPivotOffset:F

.field private mToolbarAlpha:F

.field private mToolbarYOffset:F

.field private mVisible:Z

.field private mX:F

.field private mY:F


# direct methods
.method public constructor <init>(IZFFZZ)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, -0x1

    .line 145
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 103
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mBounds:Landroid/graphics/RectF;

    .line 104
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mClosePlacement:Landroid/graphics/RectF;

    .line 113
    iput-boolean v2, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mIsTitleNeeded:Z

    .line 118
    iput-boolean v2, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mInitFromHostCalled:Z

    .line 125
    iput v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mBackgroundColor:I

    .line 127
    iput v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mFallbackThumbnailId:I

    .line 146
    iput p1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mId:I

    .line 147
    iput-boolean p2, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mIsIncognito:Z

    .line 148
    invoke-virtual {p0, p3, p4, p5, p6}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->init(FFZZ)V

    .line 149
    return-void
.end method

.method public static getTouchSlop()F
    .locals 1

    .prologue
    .line 242
    sget v0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->sCloseButtonSlop:F

    return v0
.end method

.method public static resetDimensionConstants(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 232
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 233
    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    sput v1, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->sDpToPx:F

    .line 234
    const/high16 v1, 0x3f800000    # 1.0f

    sget v2, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->sDpToPx:F

    div-float/2addr v1, v2

    sput v1, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->sPxToDp:F

    .line 235
    sget v1, Lcom/google/android/apps/chrome/R$dimen;->close_button_slop:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    sget v1, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->sPxToDp:F

    mul-float/2addr v0, v1

    sput v0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->sCloseButtonSlop:F

    .line 236
    return-void
.end method

.method private updateSnap(FFF)F
    .locals 2

    .prologue
    .line 726
    sub-float v0, p2, p3

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    sget v1, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->sPxToDp:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 735
    :goto_0
    return p3

    .line 727
    :cond_0
    sget v0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->sDpToPx:F

    mul-float/2addr v0, p3

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    int-to-float v0, v0

    sget v1, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->sPxToDp:F

    mul-float/2addr v0, v1

    .line 728
    cmpg-float v1, v0, p3

    if-gez v1, :cond_1

    .line 729
    sub-float v1, p2, p1

    .line 730
    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    :goto_1
    move p3, v0

    .line 735
    goto :goto_0

    .line 732
    :cond_1
    add-float v1, p2, p1

    .line 733
    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    goto :goto_1
.end method


# virtual methods
.method public anonymizeToolbar()Z
    .locals 1

    .prologue
    .line 823
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mAnonymizeToolbar:Z

    return v0
.end method

.method public canUseLiveTexture()Z
    .locals 1

    .prologue
    .line 791
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mCanUseLiveTexture:Z

    return v0
.end method

.method public checkCloseHitTest(FFZ)Z
    .locals 1

    .prologue
    .line 683
    invoke-virtual {p0, p3}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getCloseBounds(Z)Landroid/graphics/RectF;

    move-result-object v0

    .line 684
    if-eqz v0, :cond_0

    invoke-virtual {v0, p1, p2}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public computeDistanceTo(FF)F
    .locals 3

    .prologue
    .line 656
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getClickTargetBounds()Landroid/graphics/RectF;

    move-result-object v0

    .line 657
    iget v1, v0, Landroid/graphics/RectF;->left:F

    sub-float/2addr v1, p1

    iget v2, v0, Landroid/graphics/RectF;->right:F

    sub-float v2, p1, v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(FF)F

    move-result v1

    .line 658
    iget v2, v0, Landroid/graphics/RectF;->top:F

    sub-float/2addr v2, p2

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    sub-float v0, p2, v0

    invoke-static {v2, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 659
    const/4 v2, 0x0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    invoke-static {v2, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    return v0
.end method

.method public computeVisibleArea()F
    .locals 2

    .prologue
    .line 858
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mVisible:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mAlpha:F

    const v1, 0x3b808081

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    const/high16 v0, 0x3f800000    # 1.0f

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getFinalContentWidth()F

    move-result v1

    mul-float/2addr v0, v1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getFinalContentHeight()F

    move-result v1

    mul-float/2addr v0, v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getAlpha()F
    .locals 1

    .prologue
    .line 501
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mAlpha:F

    return v0
.end method

.method public getBackgroundColor()I
    .locals 1

    .prologue
    .line 866
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mBackgroundColor:I

    return v0
.end method

.method public getBorderAlpha()F
    .locals 1

    .prologue
    .line 538
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mBorderAlpha:F

    return v0
.end method

.method public getBorderCloseButtonAlpha()F
    .locals 1

    .prologue
    .line 552
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mBorderCloseButtonAlpha:F

    return v0
.end method

.method public getBorderScale()F
    .locals 1

    .prologue
    .line 567
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mBorderScale:F

    return v0
.end method

.method public getBrightness()F
    .locals 1

    .prologue
    .line 623
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mBrightness:F

    return v0
.end method

.method public getClickTargetBounds()Landroid/graphics/RectF;
    .locals 4

    .prologue
    .line 666
    const/high16 v0, 0x40800000    # 4.0f

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mBorderScale:F

    mul-float/2addr v0, v1

    .line 667
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mBounds:Landroid/graphics/RectF;

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mY:F

    iget v3, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mClippedY:F

    add-float/2addr v2, v3

    sub-float/2addr v2, v0

    iput v2, v1, Landroid/graphics/RectF;->top:F

    .line 668
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mBounds:Landroid/graphics/RectF;

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mY:F

    iget v3, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mClippedY:F

    add-float/2addr v2, v3

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getFinalContentHeight()F

    move-result v3

    add-float/2addr v2, v3

    add-float/2addr v2, v0

    iput v2, v1, Landroid/graphics/RectF;->bottom:F

    .line 669
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mBounds:Landroid/graphics/RectF;

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mX:F

    iget v3, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mClippedX:F

    add-float/2addr v2, v3

    sub-float/2addr v2, v0

    iput v2, v1, Landroid/graphics/RectF;->left:F

    .line 670
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mBounds:Landroid/graphics/RectF;

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mX:F

    iget v3, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mClippedX:F

    add-float/2addr v2, v3

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getFinalContentWidth()F

    move-result v3

    add-float/2addr v2, v3

    add-float/2addr v0, v2

    iput v0, v1, Landroid/graphics/RectF;->right:F

    .line 671
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mBounds:Landroid/graphics/RectF;

    return-object v0
.end method

.method public getClippedHeight()F
    .locals 1

    .prologue
    .line 350
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mClippedHeight:F

    return v0
.end method

.method public getClippedWidth()F
    .locals 1

    .prologue
    .line 343
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mClippedWidth:F

    return v0
.end method

.method public getClippedX()F
    .locals 1

    .prologue
    .line 329
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mClippedX:F

    return v0
.end method

.method public getClippedY()F
    .locals 1

    .prologue
    .line 336
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mClippedY:F

    return v0
.end method

.method public getCloseBounds(Z)Landroid/graphics/RectF;
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/high16 v5, 0x42100000    # 36.0f

    const/high16 v3, 0x3f000000    # 0.5f

    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v4, 0x0

    .line 693
    iget-boolean v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mIsTitleNeeded:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mVisible:Z

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mBorderCloseButtonAlpha:F

    cmpg-float v1, v1, v3

    if-ltz v1, :cond_0

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mBorderAlpha:F

    cmpg-float v1, v1, v3

    if-ltz v1, :cond_0

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mBorderScale:F

    cmpl-float v1, v1, v2

    if-nez v1, :cond_0

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mTiltX:F

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    cmpl-float v1, v1, v2

    if-gtz v1, :cond_0

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mTiltY:F

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    cmpl-float v1, v1, v2

    if-lez v1, :cond_1

    .line 706
    :cond_0
    :goto_0
    return-object v0

    .line 696
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mClosePlacement:Landroid/graphics/RectF;

    invoke-virtual {v1, v4, v4, v5, v5}, Landroid/graphics/RectF;->set(FFFF)V

    .line 697
    if-nez p1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mClosePlacement:Landroid/graphics/RectF;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getFinalContentWidth()F

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mClosePlacement:Landroid/graphics/RectF;

    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    move-result v3

    sub-float/2addr v2, v3

    invoke-virtual {v1, v2, v4}, Landroid/graphics/RectF;->offset(FF)V

    .line 699
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mClosePlacement:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getFinalContentHeight()F

    move-result v2

    cmpl-float v1, v1, v2

    if-gtz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mClosePlacement:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->right:F

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getFinalContentWidth()F

    move-result v2

    cmpl-float v1, v1, v2

    if-gtz v1, :cond_0

    .line 703
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mClosePlacement:Landroid/graphics/RectF;

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mX:F

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mClippedX:F

    add-float/2addr v1, v2

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mY:F

    iget v3, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mClippedY:F

    add-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/RectF;->offset(FF)V

    .line 704
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mClosePlacement:Landroid/graphics/RectF;

    sget v1, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->sCloseButtonSlop:F

    neg-float v1, v1

    sget v2, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->sCloseButtonSlop:F

    neg-float v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/RectF;->inset(FF)V

    .line 706
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mClosePlacement:Landroid/graphics/RectF;

    goto :goto_0
.end method

.method public getDecorationAlpha()F
    .locals 1

    .prologue
    .line 581
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mDecorationAlpha:F

    return v0
.end method

.method public getFallbackThumbnailId()I
    .locals 1

    .prologue
    .line 874
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mFallbackThumbnailId:I

    return v0
.end method

.method public getFinalContentHeight()F
    .locals 2

    .prologue
    .line 399
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mClippedHeight:F

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getScaledContentHeight()F

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    return v0
.end method

.method public getFinalContentWidth()F
    .locals 2

    .prologue
    .line 392
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mClippedWidth:F

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getScaledContentWidth()F

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    return v0
.end method

.method public getId()I
    .locals 1

    .prologue
    .line 434
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mId:I

    return v0
.end method

.method public getMaxContentHeight()F
    .locals 1

    .prologue
    .line 413
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mMaxContentHeight:F

    return v0
.end method

.method public getMaxContentWidth()F
    .locals 1

    .prologue
    .line 406
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mMaxContentWidth:F

    return v0
.end method

.method public getOriginalContentHeight()F
    .locals 2

    .prologue
    .line 378
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mOriginalContentHeight:F

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mMaxContentHeight:F

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    return v0
.end method

.method public getOriginalContentWidth()F
    .locals 2

    .prologue
    .line 371
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mOriginalContentWidth:F

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mMaxContentWidth:F

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    return v0
.end method

.method public getRenderX()F
    .locals 1

    .prologue
    .line 483
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mRenderX:F

    return v0
.end method

.method public getRenderY()F
    .locals 1

    .prologue
    .line 462
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mRenderY:F

    return v0
.end method

.method public getSaturation()F
    .locals 1

    .prologue
    .line 524
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mSaturation:F

    return v0
.end method

.method public getScale()F
    .locals 1

    .prologue
    .line 249
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mScale:F

    return v0
.end method

.method public getScaledContentHeight()F
    .locals 2

    .prologue
    .line 364
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getOriginalContentHeight()F

    move-result v0

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mScale:F

    mul-float/2addr v0, v1

    return v0
.end method

.method public getScaledContentWidth()F
    .locals 2

    .prologue
    .line 357
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getOriginalContentWidth()F

    move-result v0

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mScale:F

    mul-float/2addr v0, v1

    return v0
.end method

.method public getShadowOpacity()F
    .locals 1

    .prologue
    .line 508
    const/high16 v0, 0x3f800000    # 1.0f

    return v0
.end method

.method public getSideBorderScale()F
    .locals 1

    .prologue
    .line 609
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mSideBorderScale:F

    return v0
.end method

.method public getStaticToViewBlend()F
    .locals 1

    .prologue
    .line 644
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mStaticToViewBlend:F

    return v0
.end method

.method public getTiltX()F
    .locals 1

    .prologue
    .line 272
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mTiltX:F

    return v0
.end method

.method public getTiltXPivotOffset()F
    .locals 1

    .prologue
    .line 279
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mTiltXPivotOffset:F

    return v0
.end method

.method public getTiltY()F
    .locals 1

    .prologue
    .line 295
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mTiltY:F

    return v0
.end method

.method public getTiltYPivotOffset()F
    .locals 1

    .prologue
    .line 302
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mTiltYPivotOffset:F

    return v0
.end method

.method public getToolbarAlpha()F
    .locals 1

    .prologue
    .line 837
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mToolbarAlpha:F

    return v0
.end method

.method public getToolbarYOffset()F
    .locals 1

    .prologue
    .line 595
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mToolbarYOffset:F

    return v0
.end method

.method public getUnclampedOriginalContentHeight()F
    .locals 1

    .prologue
    .line 385
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mOriginalContentHeight:F

    return v0
.end method

.method public getX()F
    .locals 1

    .prologue
    .line 476
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mX:F

    return v0
.end method

.method public getY()F
    .locals 1

    .prologue
    .line 455
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mY:F

    return v0
.end method

.method public init(FFZZ)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const v4, 0x7f7fffff    # Float.MAX_VALUE

    const/4 v3, 0x0

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    .line 163
    iput v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mAlpha:F

    .line 164
    iput v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mSaturation:F

    .line 165
    iput v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mBrightness:F

    .line 166
    iput v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mBorderAlpha:F

    .line 167
    if-eqz p3, :cond_0

    move v0, v1

    :goto_0
    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mBorderCloseButtonAlpha:F

    .line 168
    iput v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mBorderScale:F

    .line 169
    iput v2, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mClippedX:F

    .line 170
    iput v2, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mClippedY:F

    .line 171
    iput v4, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mClippedWidth:F

    .line 172
    iput v4, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mClippedHeight:F

    .line 173
    iput v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mScale:F

    .line 174
    iput v2, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mTiltX:F

    .line 175
    iput v2, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mTiltY:F

    .line 176
    iput-boolean v5, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mVisible:Z

    .line 177
    iput v2, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mX:F

    .line 178
    iput v2, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mY:F

    .line 179
    iput v2, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mRenderX:F

    .line 180
    iput v2, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mRenderY:F

    .line 181
    iput v2, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mStaticToViewBlend:F

    .line 182
    iput v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mDecorationAlpha:F

    .line 183
    iput-boolean p4, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mIsTitleNeeded:Z

    .line 184
    iput-boolean v5, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mCanUseLiveTexture:Z

    .line 185
    iput-boolean v3, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mShowToolbar:Z

    .line 186
    iput-boolean v3, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mAnonymizeToolbar:Z

    .line 187
    iput v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mToolbarAlpha:F

    .line 188
    iput-boolean v3, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mInsetBorderVertical:Z

    .line 189
    iput v2, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mToolbarYOffset:F

    .line 190
    iput v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mSideBorderScale:F

    .line 191
    iput p1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mOriginalContentWidth:F

    .line 192
    iput p2, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mOriginalContentHeight:F

    .line 193
    iput p1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mMaxContentWidth:F

    .line 194
    iput p2, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mMaxContentHeight:F

    .line 196
    iput-boolean v3, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mInitFromHostCalled:Z

    .line 197
    return-void

    :cond_0
    move v0, v2

    .line 167
    goto :goto_0
.end method

.method public initFromHost(IIZZ)V
    .locals 1

    .prologue
    .line 213
    iput p1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mBackgroundColor:I

    .line 214
    iput p2, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mFallbackThumbnailId:I

    .line 215
    iput-boolean p3, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mShouldStall:Z

    .line 216
    iput-boolean p4, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mCanUseLiveTexture:Z

    .line 217
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mInitFromHostCalled:Z

    .line 218
    return-void
.end method

.method public insetBorderVertical()Z
    .locals 1

    .prologue
    .line 851
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mInsetBorderVertical:Z

    return v0
.end method

.method public isIncognito()Z
    .locals 1

    .prologue
    .line 441
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mIsIncognito:Z

    return v0
.end method

.method public isInitFromHostNeeded()Z
    .locals 1

    .prologue
    .line 224
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mInitFromHostCalled:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isTitleNeeded()Z
    .locals 1

    .prologue
    .line 756
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mIsTitleNeeded:Z

    return v0
.end method

.method public isVisible()Z
    .locals 1

    .prologue
    .line 770
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mVisible:Z

    return v0
.end method

.method public setAlpha(F)V
    .locals 0

    .prologue
    .line 494
    iput p1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mAlpha:F

    .line 495
    return-void
.end method

.method public setAnonymizeToolbar(Z)V
    .locals 0

    .prologue
    .line 814
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mAnonymizeToolbar:Z

    .line 815
    return-void
.end method

.method public setBorderAlpha(F)V
    .locals 0

    .prologue
    .line 531
    iput p1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mBorderAlpha:F

    .line 532
    return-void
.end method

.method public setBorderCloseButtonAlpha(F)V
    .locals 0

    .prologue
    .line 545
    iput p1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mBorderCloseButtonAlpha:F

    .line 546
    return-void
.end method

.method public setBorderScale(F)V
    .locals 0

    .prologue
    .line 560
    iput p1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mBorderScale:F

    .line 561
    return-void
.end method

.method public setBrightness(F)V
    .locals 0

    .prologue
    .line 616
    iput p1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mBrightness:F

    .line 617
    return-void
.end method

.method public setClipOffset(FF)V
    .locals 0

    .prologue
    .line 311
    iput p1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mClippedX:F

    .line 312
    iput p2, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mClippedY:F

    .line 313
    return-void
.end method

.method public setClipSize(FF)V
    .locals 0

    .prologue
    .line 321
    iput p1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mClippedWidth:F

    .line 322
    iput p2, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mClippedHeight:F

    .line 323
    return-void
.end method

.method public setContentSize(FF)V
    .locals 0

    .prologue
    .line 748
    iput p1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mOriginalContentWidth:F

    .line 749
    iput p2, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mOriginalContentHeight:F

    .line 750
    return-void
.end method

.method public setDecorationAlpha(F)V
    .locals 0

    .prologue
    .line 574
    iput p1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mDecorationAlpha:F

    .line 575
    return-void
.end method

.method public setDrawDecoration(Z)V
    .locals 1

    .prologue
    .line 630
    if-eqz p1, :cond_0

    const/high16 v0, 0x3f800000    # 1.0f

    :goto_0
    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mDecorationAlpha:F

    .line 631
    return-void

    .line 630
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setInsetBorderVertical(Z)V
    .locals 0

    .prologue
    .line 844
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mInsetBorderVertical:Z

    .line 845
    return-void
.end method

.method public setMaxContentHeight(F)V
    .locals 0

    .prologue
    .line 427
    iput p1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mMaxContentHeight:F

    .line 428
    return-void
.end method

.method public setMaxContentWidth(F)V
    .locals 0

    .prologue
    .line 420
    iput p1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mMaxContentWidth:F

    .line 421
    return-void
.end method

.method public setProperty(Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;F)V
    .locals 2

    .prologue
    .line 885
    sget-object v0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$1;->$SwitchMap$com$google$android$apps$chrome$tabs$layout$LayoutTab$Property:[I

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 935
    :goto_0
    return-void

    .line 887
    :pswitch_0
    invoke-virtual {p0, p2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setBorderAlpha(F)V

    goto :goto_0

    .line 890
    :pswitch_1
    invoke-virtual {p0, p2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setBorderScale(F)V

    goto :goto_0

    .line 893
    :pswitch_2
    invoke-virtual {p0, p2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setAlpha(F)V

    goto :goto_0

    .line 896
    :pswitch_3
    invoke-virtual {p0, p2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setSaturation(F)V

    goto :goto_0

    .line 899
    :pswitch_4
    invoke-virtual {p0, p2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setStaticToViewBlend(F)V

    goto :goto_0

    .line 902
    :pswitch_5
    invoke-virtual {p0, p2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setScale(F)V

    goto :goto_0

    .line 905
    :pswitch_6
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mTiltXPivotOffset:F

    invoke-virtual {p0, p2, v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setTiltX(FF)V

    goto :goto_0

    .line 908
    :pswitch_7
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mTiltYPivotOffset:F

    invoke-virtual {p0, p2, v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setTiltY(FF)V

    goto :goto_0

    .line 911
    :pswitch_8
    invoke-virtual {p0, p2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setX(F)V

    goto :goto_0

    .line 914
    :pswitch_9
    invoke-virtual {p0, p2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setY(F)V

    goto :goto_0

    .line 917
    :pswitch_a
    invoke-virtual {p0, p2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setMaxContentWidth(F)V

    goto :goto_0

    .line 920
    :pswitch_b
    invoke-virtual {p0, p2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setMaxContentHeight(F)V

    goto :goto_0

    .line 923
    :pswitch_c
    invoke-virtual {p0, p2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setToolbarAlpha(F)V

    goto :goto_0

    .line 926
    :pswitch_d
    invoke-virtual {p0, p2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setDecorationAlpha(F)V

    goto :goto_0

    .line 929
    :pswitch_e
    invoke-virtual {p0, p2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setToolbarYOffset(F)V

    goto :goto_0

    .line 932
    :pswitch_f
    invoke-virtual {p0, p2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setSideBorderScale(F)V

    goto :goto_0

    .line 885
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
    .end packed-switch
.end method

.method public bridge synthetic setProperty(Ljava/lang/Enum;F)V
    .locals 0

    .prologue
    .line 23
    check-cast p1, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->setProperty(Lcom/google/android/apps/chrome/tabs/layout/LayoutTab$Property;F)V

    return-void
.end method

.method public setSaturation(F)V
    .locals 0

    .prologue
    .line 517
    iput p1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mSaturation:F

    .line 518
    return-void
.end method

.method public setScale(F)V
    .locals 0

    .prologue
    .line 256
    iput p1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mScale:F

    .line 257
    return-void
.end method

.method public setShouldStall(Z)V
    .locals 0

    .prologue
    .line 777
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mShouldStall:Z

    .line 778
    return-void
.end method

.method public setShowToolbar(Z)V
    .locals 0

    .prologue
    .line 798
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mShowToolbar:Z

    .line 799
    return-void
.end method

.method public setSideBorderScale(F)V
    .locals 2

    .prologue
    .line 602
    const/4 v0, 0x0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-static {p1, v0, v1}, Lorg/chromium/chrome/browser/util/MathUtils;->clamp(FFF)F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mSideBorderScale:F

    .line 603
    return-void
.end method

.method public setStaticToViewBlend(F)V
    .locals 0

    .prologue
    .line 637
    iput p1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mStaticToViewBlend:F

    .line 638
    return-void
.end method

.method public setTiltX(FF)V
    .locals 0

    .prologue
    .line 264
    iput p1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mTiltX:F

    .line 265
    iput p2, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mTiltXPivotOffset:F

    .line 266
    return-void
.end method

.method public setTiltY(FF)V
    .locals 0

    .prologue
    .line 287
    iput p1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mTiltY:F

    .line 288
    iput p2, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mTiltYPivotOffset:F

    .line 289
    return-void
.end method

.method public setToolbarAlpha(F)V
    .locals 0

    .prologue
    .line 830
    iput p1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mToolbarAlpha:F

    .line 831
    return-void
.end method

.method public setToolbarYOffset(F)V
    .locals 0

    .prologue
    .line 588
    iput p1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mToolbarYOffset:F

    .line 589
    return-void
.end method

.method public setVisible(Z)V
    .locals 0

    .prologue
    .line 763
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mVisible:Z

    .line 764
    return-void
.end method

.method public setX(F)V
    .locals 0

    .prologue
    .line 469
    iput p1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mX:F

    .line 470
    return-void
.end method

.method public setY(F)V
    .locals 0

    .prologue
    .line 448
    iput p1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mY:F

    .line 449
    return-void
.end method

.method public shouldStall()Z
    .locals 1

    .prologue
    .line 784
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mShouldStall:Z

    return v0
.end method

.method public showToolbar()Z
    .locals 1

    .prologue
    .line 805
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mShowToolbar:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 740
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getId()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public updateSnap(J)Z
    .locals 5

    .prologue
    .line 716
    long-to-float v0, p1

    const/high16 v1, 0x447a0000    # 1000.0f

    div-float/2addr v0, v1

    .line 717
    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mRenderX:F

    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mX:F

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->updateSnap(FFF)F

    move-result v1

    .line 718
    iget v2, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mRenderY:F

    iget v3, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mY:F

    invoke-direct {p0, v0, v2, v3}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->updateSnap(FFF)F

    move-result v2

    .line 719
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mRenderX:F

    cmpl-float v0, v1, v0

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mRenderY:F

    cmpl-float v0, v2, v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 720
    :goto_0
    iput v1, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mRenderX:F

    .line 721
    iput v2, p0, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->mRenderY:F

    .line 722
    return v0

    .line 719
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
