.class public Lcom/google/android/apps/chrome/bookmark/ImportBookmarksProgressDialog;
.super Lcom/google/android/apps/chrome/preferences/ChromeBaseDialogFragment;
.source "ImportBookmarksProgressDialog.java"

# interfaces
.implements Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$OnBookmarksImportedListener;


# instance fields
.field private mActivity:Landroid/app/Activity;

.field private mImporter:Lcom/google/android/apps/chrome/bookmark/BookmarkImporter;

.field private mProgressDialog:Landroid/app/ProgressDialog;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBaseDialogFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public onBookmarksImported(Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$ImportResults;)V
    .locals 4

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/ImportBookmarksProgressDialog;->mProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/ImportBookmarksProgressDialog;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/ImportBookmarksProgressDialog;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 43
    :cond_0
    if-eqz p1, :cond_1

    iget v0, p1, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$ImportResults;->numImported:I

    iget v1, p1, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$ImportResults;->newBookmarks:I

    if-ne v0, v1, :cond_1

    .line 46
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 47
    const/high16 v1, 0x1000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 48
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 49
    const/high16 v1, 0x20000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 50
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "chrome-native://bookmarks/#"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p1, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$ImportResults;->rootFolderId:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 51
    iget-object v1, p0, Lcom/google/android/apps/chrome/bookmark/ImportBookmarksProgressDialog;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 54
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/bookmark/ImportBookmarksProgressDialog;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const-class v2, Lcom/google/android/apps/chrome/ChromeTabbedActivity;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 56
    iget-object v1, p0, Lcom/google/android/apps/chrome/bookmark/ImportBookmarksProgressDialog;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 61
    :goto_0
    return-void

    .line 58
    :cond_1
    new-instance v0, Lcom/google/android/apps/chrome/bookmark/ImportBookmarksRetryDialog;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/bookmark/ImportBookmarksRetryDialog;-><init>()V

    .line 59
    iget-object v1, p0, Lcom/google/android/apps/chrome/bookmark/ImportBookmarksProgressDialog;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/bookmark/ImportBookmarksRetryDialog;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 30
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/bookmark/ImportBookmarksProgressDialog;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/bookmark/ImportBookmarksProgressDialog;->mActivity:Landroid/app/Activity;

    .line 31
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/ImportBookmarksProgressDialog;->mActivity:Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/android/apps/chrome/bookmark/ImportBookmarksProgressDialog;->mActivity:Landroid/app/Activity;

    sget v2, Lcom/google/android/apps/chrome/R$string;->import_bookmarks_progress_header:I

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/bookmark/ImportBookmarksProgressDialog;->mActivity:Landroid/app/Activity;

    sget v3, Lcom/google/android/apps/chrome/R$string;->import_bookmarks_progress_message:I

    invoke-virtual {v2, v3}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2, v4, v4}, Landroid/app/ProgressDialog;->show(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZZ)Landroid/app/ProgressDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/bookmark/ImportBookmarksProgressDialog;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 35
    new-instance v0, Lcom/google/android/apps/chrome/bookmark/AndroidBrowserImporter;

    iget-object v1, p0, Lcom/google/android/apps/chrome/bookmark/ImportBookmarksProgressDialog;->mActivity:Landroid/app/Activity;

    invoke-direct {v0, v1}, Lcom/google/android/apps/chrome/bookmark/AndroidBrowserImporter;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/bookmark/ImportBookmarksProgressDialog;->mImporter:Lcom/google/android/apps/chrome/bookmark/BookmarkImporter;

    .line 36
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/ImportBookmarksProgressDialog;->mImporter:Lcom/google/android/apps/chrome/bookmark/BookmarkImporter;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter;->importBookmarks(Lcom/google/android/apps/chrome/bookmark/BookmarkImporter$OnBookmarksImportedListener;)V

    .line 37
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/ImportBookmarksProgressDialog;->mProgressDialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 65
    invoke-super {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBaseDialogFragment;->onStop()V

    .line 67
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/ImportBookmarksProgressDialog;->mImporter:Lcom/google/android/apps/chrome/bookmark/BookmarkImporter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/ImportBookmarksProgressDialog;->mImporter:Lcom/google/android/apps/chrome/bookmark/BookmarkImporter;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/bookmark/BookmarkImporter;->cancel()V

    .line 68
    :cond_0
    return-void
.end method
