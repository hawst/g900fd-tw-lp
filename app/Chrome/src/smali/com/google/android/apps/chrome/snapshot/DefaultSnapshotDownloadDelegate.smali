.class public Lcom/google/android/apps/chrome/snapshot/DefaultSnapshotDownloadDelegate;
.super Ljava/lang/Object;
.source "DefaultSnapshotDownloadDelegate.java"

# interfaces
.implements Lcom/google/android/apps/chrome/snapshot/SnapshotDownloadDelegate;


# instance fields
.field private final mDownloadService:Landroid/app/DownloadManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    const-string/jumbo v0, "download"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/DownloadManager;

    iput-object v0, p0, Lcom/google/android/apps/chrome/snapshot/DefaultSnapshotDownloadDelegate;->mDownloadService:Landroid/app/DownloadManager;

    .line 26
    return-void
.end method

.method private static getDownloadError(Landroid/database/Cursor;)I
    .locals 1

    .prologue
    .line 85
    const-string/jumbo v0, "reason"

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 86
    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    return v0
.end method

.method private getDownloadInfoCursor(J)Landroid/database/Cursor;
    .locals 5

    .prologue
    const/4 v3, 0x1

    .line 67
    new-instance v0, Landroid/app/DownloadManager$Query;

    invoke-direct {v0}, Landroid/app/DownloadManager$Query;-><init>()V

    .line 68
    new-array v1, v3, [J

    const/4 v2, 0x0

    aput-wide p1, v1, v2

    invoke-virtual {v0, v1}, Landroid/app/DownloadManager$Query;->setFilterById([J)Landroid/app/DownloadManager$Query;

    .line 69
    iget-object v1, p0, Lcom/google/android/apps/chrome/snapshot/DefaultSnapshotDownloadDelegate;->mDownloadService:Landroid/app/DownloadManager;

    invoke-virtual {v1, v0}, Landroid/app/DownloadManager;->query(Landroid/app/DownloadManager$Query;)Landroid/database/Cursor;

    move-result-object v0

    .line 70
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-ne v1, v3, :cond_0

    .line 71
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 75
    :goto_0
    return-object v0

    .line 74
    :cond_0
    const-string/jumbo v0, "DefaultSnapshotDownloadDelegate"

    const-string/jumbo v1, "Failed to find download in DownloadManager."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 75
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static getDownloadMimeType(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 90
    const-string/jumbo v0, "media_type"

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 91
    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static getDownloadStatus(Landroid/database/Cursor;)I
    .locals 1

    .prologue
    .line 80
    const-string/jumbo v0, "status"

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 81
    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    return v0
.end method


# virtual methods
.method public enqueue(Lcom/google/android/apps/chrome/snapshot/DownloadManagerRequestData;)J
    .locals 3

    .prologue
    .line 49
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/snapshot/DownloadManagerRequestData;->asServiceRequest()Landroid/app/DownloadManager$Request;

    move-result-object v0

    .line 51
    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/snapshot/DefaultSnapshotDownloadDelegate;->mDownloadService:Landroid/app/DownloadManager;

    invoke-virtual {v1, v0}, Landroid/app/DownloadManager;->enqueue(Landroid/app/DownloadManager$Request;)J
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    return-wide v0

    .line 52
    :catch_0
    move-exception v0

    .line 53
    new-instance v1, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager$ExternalStorageException;

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v0

    sget v2, Lcom/google/android/apps/chrome/R$string;->snapshot_download_failed_infobar:I

    invoke-direct {v1, v0, v2}, Lcom/google/android/apps/chrome/snapshot/SnapshotArchiveManager$ExternalStorageException;-><init>(Ljava/lang/String;I)V

    throw v1
.end method

.method public getUriForDownloadedFile(J)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/apps/chrome/snapshot/DefaultSnapshotDownloadDelegate;->mDownloadService:Landroid/app/DownloadManager;

    invoke-virtual {v0, p1, p2}, Landroid/app/DownloadManager;->getUriForDownloadedFile(J)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public queryByDownloadId(J)Lcom/google/android/apps/chrome/snapshot/DownloadInfo;
    .locals 7

    .prologue
    .line 35
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/snapshot/DefaultSnapshotDownloadDelegate;->getDownloadInfoCursor(J)Landroid/database/Cursor;

    move-result-object v0

    .line 36
    if-nez v0, :cond_0

    .line 37
    const/4 v1, 0x0

    .line 43
    :goto_0
    return-object v1

    .line 39
    :cond_0
    invoke-static {v0}, Lcom/google/android/apps/chrome/snapshot/DefaultSnapshotDownloadDelegate;->getDownloadStatus(Landroid/database/Cursor;)I

    move-result v4

    .line 40
    invoke-static {v0}, Lcom/google/android/apps/chrome/snapshot/DefaultSnapshotDownloadDelegate;->getDownloadError(Landroid/database/Cursor;)I

    move-result v5

    .line 41
    invoke-static {v0}, Lcom/google/android/apps/chrome/snapshot/DefaultSnapshotDownloadDelegate;->getDownloadMimeType(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v6

    .line 42
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 43
    new-instance v1, Lcom/google/android/apps/chrome/snapshot/DownloadInfo;

    move-wide v2, p1

    invoke-direct/range {v1 .. v6}, Lcom/google/android/apps/chrome/snapshot/DownloadInfo;-><init>(JIILjava/lang/String;)V

    goto :goto_0
.end method
