.class public Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthStatisticsPreferenceCategory;
.super Landroid/preference/PreferenceCategory;
.source "BandwidthStatisticsPreferenceCategory.java"


# instance fields
.field private mChartDataUsageView:Lcom/google/android/apps/chrome/third_party/datausagechart/ChartDataUsageView;

.field private mCurrentTime:Ljava/lang/Long;

.field private mDateRangePhrase:Ljava/lang/String;

.field private mDateRangeTextView:Landroid/widget/TextView;

.field private mLeftPosition:J

.field private mOriginalNetworkStatsHistory:Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;

.field private mOriginalSizeTextView:Landroid/widget/TextView;

.field private mOriginalTotalPhrase:Ljava/lang/String;

.field private mPercentReductionPhrase:Ljava/lang/String;

.field private mPercentReductionTextView:Landroid/widget/TextView;

.field private mReceivedNetworkStatsHistory:Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;

.field private mReceivedSizeTextView:Landroid/widget/TextView;

.field private mReceivedTotalPhrase:Ljava/lang/String;

.field private mRightPosition:J


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0, p1}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 62
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0, p1, p2}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 58
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0, p1, p2, p3}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 54
    return-void
.end method

.method private static formatDateRange(Landroid/content/Context;JJ)Ljava/lang/String;
    .locals 9

    .prologue
    .line 173
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x32

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 175
    new-instance v1, Ljava/util/Formatter;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Ljava/util/Formatter;-><init>(Ljava/lang/Appendable;Ljava/util/Locale;)V

    .line 177
    const/16 v6, 0x18

    const/4 v7, 0x0

    move-object v0, p0

    move-wide v2, p1

    move-wide v4, p3

    invoke-static/range {v0 .. v7}, Landroid/text/format/DateUtils;->formatDateRange(Landroid/content/Context;Ljava/util/Formatter;JJILjava/lang/String;)Ljava/util/Formatter;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Formatter;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private updateDetailData()V
    .locals 19

    .prologue
    .line 136
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthStatisticsPreferenceCategory;->mLeftPosition:J

    .line 138
    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthStatisticsPreferenceCategory;->mRightPosition:J

    .line 139
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthStatisticsPreferenceCategory;->mCurrentTime:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    .line 140
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthStatisticsPreferenceCategory;->getContext()Landroid/content/Context;

    move-result-object v11

    .line 142
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthStatisticsPreferenceCategory;->mOriginalNetworkStatsHistory:Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;

    const/4 v10, 0x0

    invoke-virtual/range {v3 .. v10}, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->getValues(JJJLcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory$Entry;)Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory$Entry;

    move-result-object v2

    .line 145
    iget-wide v12, v2, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory$Entry;->rxBytes:J

    .line 146
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthStatisticsPreferenceCategory;->getContext()Landroid/content/Context;

    move-result-object v2

    sget v3, Lcom/google/android/apps/chrome/R$string;->data_reduction_statistics_original_size_suffix:I

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v14, 0x0

    const-string/jumbo v15, "<b>%s</b>"

    const/16 v16, 0x1

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    invoke-static {v11, v12, v13}, Landroid/text/format/Formatter;->formatFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v18

    aput-object v18, v16, v17

    invoke-static/range {v15 .. v16}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v15

    aput-object v15, v10, v14

    invoke-virtual {v2, v3, v10}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 150
    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthStatisticsPreferenceCategory;->mOriginalTotalPhrase:Ljava/lang/String;

    .line 152
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthStatisticsPreferenceCategory;->mReceivedNetworkStatsHistory:Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;

    const/4 v10, 0x0

    invoke-virtual/range {v3 .. v10}, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;->getValues(JJJLcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory$Entry;)Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory$Entry;

    move-result-object v2

    .line 155
    iget-wide v8, v2, Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory$Entry;->rxBytes:J

    .line 156
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthStatisticsPreferenceCategory;->getContext()Landroid/content/Context;

    move-result-object v2

    sget v3, Lcom/google/android/apps/chrome/R$string;->data_reduction_statistics_compressed_size_suffix:I

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v14, 0x0

    const-string/jumbo v15, "<b>%s</b>"

    const/16 v16, 0x1

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    invoke-static {v11, v8, v9}, Landroid/text/format/Formatter;->formatFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v18

    aput-object v18, v16, v17

    invoke-static/range {v15 .. v16}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v15

    aput-object v15, v10, v14

    invoke-virtual {v2, v3, v10}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 160
    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthStatisticsPreferenceCategory;->mReceivedTotalPhrase:Ljava/lang/String;

    .line 162
    const/4 v2, 0x0

    .line 163
    const-wide/16 v14, 0x0

    cmp-long v3, v12, v14

    if-lez v3, :cond_0

    cmp-long v3, v12, v8

    if-lez v3, :cond_0

    .line 164
    sub-long v2, v12, v8

    long-to-float v2, v2

    long-to-float v3, v12

    div-float/2addr v2, v3

    .line 166
    :cond_0
    const-string/jumbo v3, "%.0f%%"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    const-wide/high16 v12, 0x4059000000000000L    # 100.0

    float-to-double v14, v2

    mul-double/2addr v12, v14

    invoke-static {v12, v13}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    aput-object v2, v8, v9

    invoke-static {v3, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthStatisticsPreferenceCategory;->mPercentReductionPhrase:Ljava/lang/String;

    .line 168
    invoke-static {v11, v4, v5, v6, v7}, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthStatisticsPreferenceCategory;->formatDateRange(Landroid/content/Context;JJ)Ljava/lang/String;

    move-result-object v2

    .line 169
    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthStatisticsPreferenceCategory;->mDateRangePhrase:Ljava/lang/String;

    .line 170
    return-void
.end method


# virtual methods
.method public isEnabled()Z
    .locals 1

    .prologue
    .line 92
    invoke-super {p0}, Landroid/preference/PreferenceCategory;->isEnabled()Z

    move-result v0

    return v0
.end method

.method protected onBindView(Landroid/view/View;)V
    .locals 10

    .prologue
    .line 101
    invoke-super {p0, p1}, Landroid/preference/PreferenceCategory;->onBindView(Landroid/view/View;)V

    .line 102
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthStatisticsPreferenceCategory;->mOriginalTotalPhrase:Ljava/lang/String;

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthStatisticsPreferenceCategory;->updateDetailData()V

    .line 103
    :cond_0
    sget v0, Lcom/google/android/apps/chrome/R$id;->data_reduction_original_size:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthStatisticsPreferenceCategory;->mOriginalSizeTextView:Landroid/widget/TextView;

    .line 104
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthStatisticsPreferenceCategory;->mOriginalSizeTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthStatisticsPreferenceCategory;->mOriginalTotalPhrase:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 105
    sget v0, Lcom/google/android/apps/chrome/R$id;->data_reduction_compressed_size:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthStatisticsPreferenceCategory;->mReceivedSizeTextView:Landroid/widget/TextView;

    .line 106
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthStatisticsPreferenceCategory;->mReceivedSizeTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthStatisticsPreferenceCategory;->mReceivedTotalPhrase:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 107
    sget v0, Lcom/google/android/apps/chrome/R$id;->data_reduction_percent:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthStatisticsPreferenceCategory;->mPercentReductionTextView:Landroid/widget/TextView;

    .line 108
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthStatisticsPreferenceCategory;->mPercentReductionTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthStatisticsPreferenceCategory;->mPercentReductionPhrase:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 109
    sget v0, Lcom/google/android/apps/chrome/R$id;->data_reduction_date_range:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthStatisticsPreferenceCategory;->mDateRangeTextView:Landroid/widget/TextView;

    .line 110
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthStatisticsPreferenceCategory;->mDateRangeTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthStatisticsPreferenceCategory;->mDateRangePhrase:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 112
    sget v0, Lcom/google/android/apps/chrome/R$id;->chart:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartDataUsageView;

    iput-object v0, p0, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthStatisticsPreferenceCategory;->mChartDataUsageView:Lcom/google/android/apps/chrome/third_party/datausagechart/ChartDataUsageView;

    .line 113
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthStatisticsPreferenceCategory;->mChartDataUsageView:Lcom/google/android/apps/chrome/third_party/datausagechart/ChartDataUsageView;

    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthStatisticsPreferenceCategory;->mOriginalNetworkStatsHistory:Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartDataUsageView;->bindOriginalNetworkStats(Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;)V

    .line 114
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthStatisticsPreferenceCategory;->mChartDataUsageView:Lcom/google/android/apps/chrome/third_party/datausagechart/ChartDataUsageView;

    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthStatisticsPreferenceCategory;->mReceivedNetworkStatsHistory:Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartDataUsageView;->bindCompressedNetworkStats(Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;)V

    .line 115
    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthStatisticsPreferenceCategory;->mChartDataUsageView:Lcom/google/android/apps/chrome/third_party/datausagechart/ChartDataUsageView;

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthStatisticsPreferenceCategory;->mCurrentTime:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide v4, 0x9a7ec800L

    sub-long/2addr v2, v4

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthStatisticsPreferenceCategory;->mCurrentTime:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const-wide/32 v6, 0x36ee80

    add-long/2addr v4, v6

    iget-wide v6, p0, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthStatisticsPreferenceCategory;->mLeftPosition:J

    iget-wide v8, p0, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthStatisticsPreferenceCategory;->mRightPosition:J

    invoke-virtual/range {v1 .. v9}, Lcom/google/android/apps/chrome/third_party/datausagechart/ChartDataUsageView;->setVisibleRange(JJJJ)V

    .line 119
    sget v0, Lcom/google/android/apps/chrome/R$id;->data_reduction_proxy_unreachable:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 121
    invoke-static {}, Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings;->getInstance()Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings;

    move-result-object v1

    invoke-virtual {v1}, Lorg/chromium/chrome/browser/net/spdyproxy/DataReductionProxySettings;->isDataReductionProxyUnreachable()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 122
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 126
    :goto_0
    return-void

    .line 124
    :cond_1
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method protected onPrepareAddPreference(Landroid/preference/Preference;)Z
    .locals 1

    .prologue
    .line 87
    invoke-super {p0, p1}, Landroid/preference/PreferenceCategory;->onPrepareAddPreference(Landroid/preference/Preference;)Z

    move-result v0

    return v0
.end method

.method public setReductionStats(JLcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;)V
    .locals 7

    .prologue
    .line 77
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthStatisticsPreferenceCategory;->mCurrentTime:Ljava/lang/Long;

    .line 78
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthStatisticsPreferenceCategory;->mCurrentTime:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/32 v2, 0x36ee80

    add-long/2addr v0, v2

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthStatisticsPreferenceCategory;->mCurrentTime:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/util/TimeZone;->getOffset(J)I

    move-result v2

    int-to-long v2, v2

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthStatisticsPreferenceCategory;->mRightPosition:J

    .line 80
    const-wide v0, 0x9a7ec800L

    sub-long v0, p1, v0

    iput-wide v0, p0, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthStatisticsPreferenceCategory;->mLeftPosition:J

    .line 81
    iput-object p3, p0, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthStatisticsPreferenceCategory;->mOriginalNetworkStatsHistory:Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;

    .line 82
    iput-object p4, p0, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthStatisticsPreferenceCategory;->mReceivedNetworkStatsHistory:Lcom/google/android/apps/chrome/third_party/datausagechart/NetworkStatsHistory;

    .line 83
    return-void
.end method
