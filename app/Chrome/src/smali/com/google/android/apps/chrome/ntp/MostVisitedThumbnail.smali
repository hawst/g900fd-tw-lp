.class public Lcom/google/android/apps/chrome/ntp/MostVisitedThumbnail;
.super Landroid/widget/ImageView;
.source "MostVisitedThumbnail.java"


# instance fields
.field private final mDesiredHeight:I

.field private final mDesiredWidth:I

.field private mImageMatrix:Landroid/graphics/Matrix;

.field private mThumbnail:Landroid/graphics/Bitmap;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 32
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ntp/MostVisitedThumbnail;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 33
    sget v1, Lcom/google/android/apps/chrome/R$dimen;->most_visited_thumbnail_width:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/chrome/ntp/MostVisitedThumbnail;->mDesiredWidth:I

    .line 34
    sget v1, Lcom/google/android/apps/chrome/R$dimen;->most_visited_thumbnail_height:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/ntp/MostVisitedThumbnail;->mDesiredHeight:I

    .line 35
    return-void
.end method

.method private updateThumbnailMatrix()V
    .locals 3

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/MostVisitedThumbnail;->mThumbnail:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    .line 70
    :goto_0
    return-void

    .line 64
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/MostVisitedThumbnail;->mImageMatrix:Landroid/graphics/Matrix;

    if-nez v0, :cond_1

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/ntp/MostVisitedThumbnail;->mImageMatrix:Landroid/graphics/Matrix;

    .line 65
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ntp/MostVisitedThumbnail;->getMeasuredWidth()I

    move-result v0

    int-to-float v0, v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/MostVisitedThumbnail;->mThumbnail:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    .line 66
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ntp/MostVisitedThumbnail;->getMeasuredHeight()I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/ntp/MostVisitedThumbnail;->mThumbnail:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    .line 67
    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 68
    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/MostVisitedThumbnail;->mImageMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v1, v0, v0}, Landroid/graphics/Matrix;->setScale(FF)V

    .line 69
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/MostVisitedThumbnail;->mImageMatrix:Landroid/graphics/Matrix;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ntp/MostVisitedThumbnail;->setImageMatrix(Landroid/graphics/Matrix;)V

    goto :goto_0
.end method


# virtual methods
.method protected onMeasure(II)V
    .locals 4

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    .line 85
    iget v0, p0, Lcom/google/android/apps/chrome/ntp/MostVisitedThumbnail;->mDesiredWidth:I

    invoke-static {v0, p1}, Lcom/google/android/apps/chrome/ntp/MostVisitedThumbnail;->resolveSize(II)I

    move-result v1

    .line 87
    iget v0, p0, Lcom/google/android/apps/chrome/ntp/MostVisitedThumbnail;->mDesiredWidth:I

    if-ne v1, v0, :cond_0

    .line 88
    iget v0, p0, Lcom/google/android/apps/chrome/ntp/MostVisitedThumbnail;->mDesiredHeight:I

    .line 94
    :goto_0
    invoke-static {v1, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-static {v0, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-super {p0, v1, v0}, Landroid/widget/ImageView;->onMeasure(II)V

    .line 96
    return-void

    .line 91
    :cond_0
    iget v0, p0, Lcom/google/android/apps/chrome/ntp/MostVisitedThumbnail;->mDesiredHeight:I

    int-to-float v0, v0

    iget v2, p0, Lcom/google/android/apps/chrome/ntp/MostVisitedThumbnail;->mDesiredWidth:I

    int-to-float v2, v2

    div-float/2addr v0, v2

    int-to-float v2, v1

    mul-float/2addr v0, v2

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 92
    invoke-static {v0, p2}, Lcom/google/android/apps/chrome/ntp/MostVisitedThumbnail;->resolveSize(II)I

    move-result v0

    goto :goto_0
.end method

.method protected onSizeChanged(IIII)V
    .locals 0

    .prologue
    .line 74
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/ImageView;->onSizeChanged(IIII)V

    .line 75
    invoke-direct {p0}, Lcom/google/android/apps/chrome/ntp/MostVisitedThumbnail;->updateThumbnailMatrix()V

    .line 76
    return-void
.end method

.method setThumbnail(Landroid/graphics/Bitmap;)V
    .locals 1

    .prologue
    .line 41
    iput-object p1, p0, Lcom/google/android/apps/chrome/ntp/MostVisitedThumbnail;->mThumbnail:Landroid/graphics/Bitmap;

    .line 42
    if-eqz p1, :cond_0

    .line 43
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/ntp/MostVisitedThumbnail;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 44
    sget-object v0, Landroid/widget/ImageView$ScaleType;->MATRIX:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ntp/MostVisitedThumbnail;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 45
    invoke-direct {p0}, Lcom/google/android/apps/chrome/ntp/MostVisitedThumbnail;->updateThumbnailMatrix()V

    .line 51
    :goto_0
    return-void

    .line 47
    :cond_0
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ntp/MostVisitedThumbnail;->setBackgroundColor(I)V

    .line 48
    sget v0, Lcom/google/android/apps/chrome/R$drawable;->most_visited_thumbnail_placeholder:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ntp/MostVisitedThumbnail;->setImageResource(I)V

    .line 49
    sget-object v0, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ntp/MostVisitedThumbnail;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    goto :goto_0
.end method
