.class public Lcom/google/android/apps/chrome/compositor/TabLayerContainer;
.super Ljava/lang/Object;
.source "TabLayerContainer.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private mNativeTabLayerContainer:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const-class v0, Lcom/google/android/apps/chrome/compositor/TabLayerContainer;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/compositor/TabLayerContainer;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(J)V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-wide p1, p0, Lcom/google/android/apps/chrome/compositor/TabLayerContainer;->mNativeTabLayerContainer:J

    .line 28
    return-void
.end method

.method private static create(J)Lcom/google/android/apps/chrome/compositor/TabLayerContainer;
    .locals 2

    .prologue
    .line 94
    new-instance v0, Lcom/google/android/apps/chrome/compositor/TabLayerContainer;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/chrome/compositor/TabLayerContainer;-><init>(J)V

    return-object v0
.end method

.method private destroy()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 99
    sget-boolean v0, Lcom/google/android/apps/chrome/compositor/TabLayerContainer;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/apps/chrome/compositor/TabLayerContainer;->mNativeTabLayerContainer:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 100
    :cond_0
    iput-wide v2, p0, Lcom/google/android/apps/chrome/compositor/TabLayerContainer;->mNativeTabLayerContainer:J

    .line 101
    return-void
.end method

.method private native nativeBeginBuildingFrame(J)V
.end method

.method private native nativeFinishBuildingFrame(J)V
.end method

.method private native nativePutLayer(JIIIIIIIZZIIZZFFFFFFFFFFFFFFFFFFFFFFFFZZFFFZZ)V
.end method


# virtual methods
.method public pushLayers(Landroid/content/Context;Lcom/google/android/apps/chrome/tabs/layout/Layout;)V
    .locals 54

    .prologue
    .line 39
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/google/android/apps/chrome/compositor/TabLayerContainer;->mNativeTabLayerContainer:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    .line 90
    :goto_0
    return-void

    .line 41
    :cond_0
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v51

    .line 42
    invoke-virtual/range {v51 .. v51}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v0, v2, Landroid/util/DisplayMetrics;->density:F

    move/from16 v52, v0

    .line 44
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->getLayoutTabsToRender()[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v53

    .line 45
    if-eqz v53, :cond_1

    move-object/from16 v0, v53

    array-length v2, v0

    .line 47
    :goto_1
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/google/android/apps/chrome/compositor/TabLayerContainer;->mNativeTabLayerContainer:J

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5}, Lcom/google/android/apps/chrome/compositor/TabLayerContainer;->nativeBeginBuildingFrame(J)V

    .line 48
    const/4 v3, 0x0

    move/from16 v50, v3

    :goto_2
    move/from16 v0, v50

    if-ge v0, v2, :cond_8

    .line 49
    aget-object v49, v53, v50

    .line 50
    sget-boolean v3, Lcom/google/android/apps/chrome/compositor/TabLayerContainer;->$assertionsDisabled:Z

    if-nez v3, :cond_2

    invoke-virtual/range {v49 .. v49}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->isVisible()Z

    move-result v3

    if-nez v3, :cond_2

    new-instance v2, Ljava/lang/AssertionError;

    const-string/jumbo v3, "LayoutTab in that list should be visible"

    invoke-direct {v2, v3}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2

    .line 45
    :cond_1
    const/4 v2, 0x0

    goto :goto_1

    .line 51
    :cond_2
    invoke-virtual/range {v49 .. v49}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getDecorationAlpha()F

    move-result v35

    .line 53
    invoke-virtual/range {v49 .. v49}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->isIncognito()Z

    move-result v3

    if-eqz v3, :cond_3

    sget v12, Lcom/google/android/apps/chrome/R$drawable;->tabswitcher_border_frame_incognito:I

    .line 55
    :goto_3
    invoke-virtual/range {v49 .. v49}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->isIncognito()Z

    move-result v3

    if-eqz v3, :cond_4

    sget v8, Lcom/google/android/apps/chrome/R$drawable;->btn_tab_close_white_normal:I

    .line 57
    :goto_4
    invoke-virtual/range {v49 .. v49}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->isIncognito()Z

    move-result v3

    if-eqz v3, :cond_5

    sget v3, Lcom/google/android/apps/chrome/R$color;->tab_back_incognito:I

    .line 60
    :goto_5
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/google/android/apps/chrome/compositor/TabLayerContainer;->mNativeTabLayerContainer:J

    invoke-virtual/range {v49 .. v49}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getId()I

    move-result v6

    sget v7, Lcom/google/android/apps/chrome/R$id;->control_container:I

    sget v9, Lcom/google/android/apps/chrome/R$drawable;->tabswitcher_border_frame_shadow:I

    sget v10, Lcom/google/android/apps/chrome/R$drawable;->tabswitcher_border_frame_decoration:I

    sget v11, Lcom/google/android/apps/chrome/R$drawable;->logo_card_back:I

    invoke-virtual/range {v49 .. v49}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->canUseLiveTexture()Z

    move-result v13

    invoke-virtual/range {v49 .. v49}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getFallbackThumbnailId()I

    move-result v14

    const/4 v15, -0x2

    if-ne v14, v15, :cond_6

    const/4 v14, 0x1

    :goto_6
    invoke-virtual/range {v49 .. v49}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getBackgroundColor()I

    move-result v15

    move-object/from16 v0, v51

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v16

    invoke-virtual/range {v49 .. v49}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->isIncognito()Z

    move-result v17

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->getOrientation()I

    move-result v3

    const/16 v18, 0x1

    move/from16 v0, v18

    if-ne v3, v0, :cond_7

    const/16 v18, 0x1

    :goto_7
    invoke-virtual/range {v49 .. v49}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getRenderX()F

    move-result v3

    mul-float v19, v3, v52

    invoke-virtual/range {v49 .. v49}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getRenderY()F

    move-result v3

    mul-float v20, v3, v52

    invoke-virtual/range {v49 .. v49}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getScaledContentWidth()F

    move-result v3

    mul-float v21, v3, v52

    invoke-virtual/range {v49 .. v49}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getScaledContentHeight()F

    move-result v3

    mul-float v22, v3, v52

    invoke-virtual/range {v49 .. v49}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getOriginalContentWidth()F

    move-result v3

    mul-float v23, v3, v52

    invoke-virtual/range {v49 .. v49}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getOriginalContentHeight()F

    move-result v3

    mul-float v24, v3, v52

    invoke-virtual/range {v49 .. v49}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getClippedX()F

    move-result v3

    mul-float v25, v3, v52

    invoke-virtual/range {v49 .. v49}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getClippedY()F

    move-result v3

    mul-float v26, v3, v52

    invoke-virtual/range {v49 .. v49}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getClippedWidth()F

    move-result v3

    invoke-virtual/range {v49 .. v49}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getScaledContentWidth()F

    move-result v27

    move/from16 v0, v27

    invoke-static {v3, v0}, Ljava/lang/Math;->min(FF)F

    move-result v3

    mul-float v27, v3, v52

    invoke-virtual/range {v49 .. v49}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getClippedHeight()F

    move-result v3

    invoke-virtual/range {v49 .. v49}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getScaledContentHeight()F

    move-result v28

    move/from16 v0, v28

    invoke-static {v3, v0}, Ljava/lang/Math;->min(FF)F

    move-result v3

    mul-float v28, v3, v52

    invoke-virtual/range {v49 .. v49}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getTiltXPivotOffset()F

    move-result v3

    mul-float v29, v3, v52

    invoke-virtual/range {v49 .. v49}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getTiltYPivotOffset()F

    move-result v3

    mul-float v30, v3, v52

    invoke-virtual/range {v49 .. v49}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getTiltX()F

    move-result v31

    invoke-virtual/range {v49 .. v49}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getTiltY()F

    move-result v32

    invoke-virtual/range {v49 .. v49}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getAlpha()F

    move-result v33

    invoke-virtual/range {v49 .. v49}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getBorderAlpha()F

    move-result v3

    mul-float v34, v3, v35

    invoke-virtual/range {v49 .. v49}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getShadowOpacity()F

    move-result v3

    mul-float v36, v3, v35

    invoke-virtual/range {v49 .. v49}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getBorderCloseButtonAlpha()F

    move-result v3

    mul-float v37, v3, v35

    const/high16 v3, 0x42100000    # 36.0f

    mul-float v38, v3, v52

    invoke-virtual/range {v49 .. v49}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getStaticToViewBlend()F

    move-result v39

    invoke-virtual/range {v49 .. v49}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getBorderScale()F

    move-result v40

    invoke-virtual/range {v49 .. v49}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getSaturation()F

    move-result v41

    invoke-virtual/range {v49 .. v49}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getBrightness()F

    move-result v42

    invoke-virtual/range {v49 .. v49}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->showToolbar()Z

    move-result v43

    invoke-virtual/range {v49 .. v49}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->anonymizeToolbar()Z

    move-result v44

    invoke-virtual/range {v49 .. v49}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getToolbarAlpha()F

    move-result v45

    invoke-virtual/range {v49 .. v49}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getToolbarYOffset()F

    move-result v3

    mul-float v46, v3, v52

    invoke-virtual/range {v49 .. v49}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getSideBorderScale()F

    move-result v47

    const/16 v48, 0x1

    invoke-virtual/range {v49 .. v49}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->insetBorderVertical()Z

    move-result v49

    move-object/from16 v3, p0

    invoke-direct/range {v3 .. v49}, Lcom/google/android/apps/chrome/compositor/TabLayerContainer;->nativePutLayer(JIIIIIIIZZIIZZFFFFFFFFFFFFFFFFFFFFFFFFZZFFFZZ)V

    .line 48
    add-int/lit8 v3, v50, 0x1

    move/from16 v50, v3

    goto/16 :goto_2

    .line 53
    :cond_3
    sget v12, Lcom/google/android/apps/chrome/R$drawable;->tabswitcher_border_frame:I

    goto/16 :goto_3

    .line 55
    :cond_4
    sget v8, Lcom/google/android/apps/chrome/R$drawable;->btn_tab_close_normal:I

    goto/16 :goto_4

    .line 57
    :cond_5
    sget v3, Lcom/google/android/apps/chrome/R$color;->tab_back:I

    goto/16 :goto_5

    .line 60
    :cond_6
    const/4 v14, 0x0

    goto/16 :goto_6

    :cond_7
    const/16 v18, 0x0

    goto/16 :goto_7

    .line 89
    :cond_8
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/google/android/apps/chrome/compositor/TabLayerContainer;->mNativeTabLayerContainer:J

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3}, Lcom/google/android/apps/chrome/compositor/TabLayerContainer;->nativeFinishBuildingFrame(J)V

    goto/16 :goto_0
.end method
