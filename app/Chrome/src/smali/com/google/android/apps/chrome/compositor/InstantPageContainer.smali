.class public Lcom/google/android/apps/chrome/compositor/InstantPageContainer;
.super Ljava/lang/Object;
.source "InstantPageContainer.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private mNativeInstantPageContainerPtr:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const-class v0, Lcom/google/android/apps/chrome/compositor/InstantPageContainer;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/compositor/InstantPageContainer;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(J)V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-wide p1, p0, Lcom/google/android/apps/chrome/compositor/InstantPageContainer;->mNativeInstantPageContainerPtr:J

    .line 26
    return-void
.end method

.method private static create(J)Lcom/google/android/apps/chrome/compositor/InstantPageContainer;
    .locals 2

    .prologue
    .line 50
    new-instance v0, Lcom/google/android/apps/chrome/compositor/InstantPageContainer;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/chrome/compositor/InstantPageContainer;-><init>(J)V

    return-object v0
.end method

.method private destroy()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 55
    sget-boolean v0, Lcom/google/android/apps/chrome/compositor/InstantPageContainer;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/apps/chrome/compositor/InstantPageContainer;->mNativeInstantPageContainerPtr:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 56
    :cond_0
    iput-wide v2, p0, Lcom/google/android/apps/chrome/compositor/InstantPageContainer;->mNativeInstantPageContainerPtr:J

    .line 57
    return-void
.end method

.method private native nativeAddChild(JLorg/chromium/content/browser/ContentViewCore;)V
.end method

.method private native nativeRemoveChild(JLorg/chromium/content/browser/ContentViewCore;)V
.end method


# virtual methods
.method public addChild(Lorg/chromium/content/browser/ContentViewCore;)V
    .locals 4

    .prologue
    .line 33
    iget-wide v0, p0, Lcom/google/android/apps/chrome/compositor/InstantPageContainer;->mNativeInstantPageContainerPtr:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 36
    :goto_0
    return-void

    .line 35
    :cond_0
    iget-wide v0, p0, Lcom/google/android/apps/chrome/compositor/InstantPageContainer;->mNativeInstantPageContainerPtr:J

    invoke-direct {p0, v0, v1, p1}, Lcom/google/android/apps/chrome/compositor/InstantPageContainer;->nativeAddChild(JLorg/chromium/content/browser/ContentViewCore;)V

    goto :goto_0
.end method

.method public removeChild(Lorg/chromium/content/browser/ContentViewCore;)V
    .locals 4

    .prologue
    .line 43
    iget-wide v0, p0, Lcom/google/android/apps/chrome/compositor/InstantPageContainer;->mNativeInstantPageContainerPtr:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 46
    :goto_0
    return-void

    .line 45
    :cond_0
    iget-wide v0, p0, Lcom/google/android/apps/chrome/compositor/InstantPageContainer;->mNativeInstantPageContainerPtr:J

    invoke-direct {p0, v0, v1, p1}, Lcom/google/android/apps/chrome/compositor/InstantPageContainer;->nativeRemoveChild(JLorg/chromium/content/browser/ContentViewCore;)V

    goto :goto_0
.end method
