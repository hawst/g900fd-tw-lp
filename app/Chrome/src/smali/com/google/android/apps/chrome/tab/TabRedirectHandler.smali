.class public Lcom/google/android/apps/chrome/tab/TabRedirectHandler;
.super Ljava/lang/Object;
.source "TabRedirectHandler.java"


# instance fields
.field private final mChromePackageName:Ljava/lang/String;

.field private final mIntentHistory:Ljava/util/HashMap;

.field private mIsOnEffectiveIntentRedirectChain:Z

.field private mLastIntentUpdatedTime:J

.field private mLastNewUrlLoadingTime:J

.field private final mPreviousResolvers:Ljava/util/HashSet;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tab/TabRedirectHandler;->mIntentHistory:Ljava/util/HashMap;

    .line 33
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tab/TabRedirectHandler;->mPreviousResolvers:Ljava/util/HashSet;

    .line 41
    iput-object p1, p0, Lcom/google/android/apps/chrome/tab/TabRedirectHandler;->mChromePackageName:Ljava/lang/String;

    .line 42
    return-void
.end method


# virtual methods
.method public applyNewUrlLoading(IJ)V
    .locals 4

    .prologue
    .line 88
    iget-wide v0, p0, Lcom/google/android/apps/chrome/tab/TabRedirectHandler;->mLastNewUrlLoadingTime:J

    .line 89
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/apps/chrome/tab/TabRedirectHandler;->mLastNewUrlLoadingTime:J

    .line 91
    iget-object v2, p0, Lcom/google/android/apps/chrome/tab/TabRedirectHandler;->mIntentHistory:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 112
    :cond_0
    :goto_0
    return-void

    .line 95
    :cond_1
    iget-wide v2, p0, Lcom/google/android/apps/chrome/tab/TabRedirectHandler;->mLastIntentUpdatedTime:J

    cmp-long v2, v2, p2

    if-gez v2, :cond_2

    .line 96
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/TabRedirectHandler;->clear()V

    goto :goto_0

    .line 100
    :cond_2
    iget-wide v2, p0, Lcom/google/android/apps/chrome/tab/TabRedirectHandler;->mLastIntentUpdatedTime:J

    cmp-long v0, v2, v0

    if-lez v0, :cond_4

    .line 103
    and-int/lit16 v0, p1, 0xff

    if-nez v0, :cond_3

    const/high16 v0, 0x8000000

    and-int/2addr v0, p1

    if-nez v0, :cond_0

    .line 106
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/TabRedirectHandler;->clear()V

    goto :goto_0

    .line 110
    :cond_4
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/tab/TabRedirectHandler;->mIsOnEffectiveIntentRedirectChain:Z

    goto :goto_0
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/TabRedirectHandler;->mIntentHistory:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 79
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/TabRedirectHandler;->mPreviousResolvers:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    .line 80
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/tab/TabRedirectHandler;->mIsOnEffectiveIntentRedirectChain:Z

    .line 81
    return-void
.end method

.method public hasNewResolver(Lcom/google/android/apps/chrome/UrlHandler$Environment;Landroid/content/Intent;)Z
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 125
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/TabRedirectHandler;->mIntentHistory:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 126
    if-eqz p2, :cond_0

    move v0, v2

    .line 143
    :goto_0
    return v0

    :cond_0
    move v0, v3

    .line 126
    goto :goto_0

    .line 127
    :cond_1
    if-nez p2, :cond_2

    move v0, v3

    .line 128
    goto :goto_0

    .line 131
    :cond_2
    invoke-interface {p1, p2}, Lcom/google/android/apps/chrome/UrlHandler$Environment;->queryIntentActivities(Landroid/content/Intent;)Ljava/util/List;

    move-result-object v4

    .line 132
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/TabRedirectHandler;->mIntentHistory:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_3
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 133
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_3

    .line 134
    iget-object v6, p0, Lcom/google/android/apps/chrome/tab/TabRedirectHandler;->mPreviousResolvers:Ljava/util/HashSet;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Intent;

    invoke-interface {p1, v1}, Lcom/google/android/apps/chrome/UrlHandler$Environment;->queryIntentActivities(Landroid/content/Intent;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v6, v1}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    .line 135
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map$Entry;->setValue(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 138
    :cond_4
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ComponentName;

    .line 139
    iget-object v4, p0, Lcom/google/android/apps/chrome/tab/TabRedirectHandler;->mPreviousResolvers:Ljava/util/HashSet;

    invoke-virtual {v4, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    move v0, v2

    .line 140
    goto :goto_0

    :cond_6
    move v0, v3

    .line 143
    goto :goto_0
.end method

.method public isOnEffectiveIntentRedirectChain()Z
    .locals 1

    .prologue
    .line 118
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tab/TabRedirectHandler;->mIsOnEffectiveIntentRedirectChain:Z

    return v0
.end method

.method public updateIntent(Landroid/content/Intent;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 53
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/chrome/tab/TabRedirectHandler;->mLastIntentUpdatedTime:J

    .line 54
    if-eqz p1, :cond_0

    const-string/jumbo v0, "android.intent.action.VIEW"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 55
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/TabRedirectHandler;->clear()V

    .line 72
    :goto_0
    return-void

    .line 58
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/TabRedirectHandler;->mIntentHistory:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    const/4 v1, 0x3

    if-ge v0, v1, :cond_2

    .line 61
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tab/TabRedirectHandler;->mIsOnEffectiveIntentRedirectChain:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/TabRedirectHandler;->mChromePackageName:Ljava/lang/String;

    const-string/jumbo v1, "com.android.browser.application_id"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 66
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tab/TabRedirectHandler;->clear()V

    .line 68
    :cond_3
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    .line 69
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 70
    iget-object v1, p0, Lcom/google/android/apps/chrome/tab/TabRedirectHandler;->mIntentHistory:Ljava/util/HashMap;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 71
    iput-boolean v3, p0, Lcom/google/android/apps/chrome/tab/TabRedirectHandler;->mIsOnEffectiveIntentRedirectChain:Z

    goto :goto_0
.end method
