.class Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$7;
.super Ljava/lang/Object;
.source "CompositorViewHolder.java"

# interfaces
.implements Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchManager$ContextualSearchContentViewDelegate;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;)V
    .locals 0

    .prologue
    .line 885
    iput-object p1, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$7;->this$0:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public releaseContextualSearchContentViewCore()V
    .locals 2

    .prologue
    .line 894
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$7;->this$0:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mContextualSearchContentViewCore:Lorg/chromium/content/browser/ContentViewCore;
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->access$1102(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;Lorg/chromium/content/browser/ContentViewCore;)Lorg/chromium/content/browser/ContentViewCore;

    .line 895
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$7;->this$0:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    # getter for: Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mTabVisible:Lcom/google/android/apps/chrome/tab/ChromeTab;
    invoke-static {v0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->access$100(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;)Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$7;->this$0:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    # getter for: Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mTabVisible:Lcom/google/android/apps/chrome/tab/ChromeTab;
    invoke-static {v0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->access$100(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;)Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->updateFullscreenEnabledState()V

    .line 896
    :cond_0
    return-void
.end method

.method public setContextualSearchContentViewCore(Lorg/chromium/content/browser/ContentViewCore;)V
    .locals 2

    .prologue
    .line 888
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$7;->this$0:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    # setter for: Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->mContextualSearchContentViewCore:Lorg/chromium/content/browser/ContentViewCore;
    invoke-static {v0, p1}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->access$1102(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;Lorg/chromium/content/browser/ContentViewCore;)Lorg/chromium/content/browser/ContentViewCore;

    .line 889
    iget-object v0, p0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder$7;->this$0:Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    const/4 v1, 0x0

    # invokes: Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->setupContentView(Lorg/chromium/content/browser/ContentViewCore;Z)V
    invoke-static {v0, p1, v1}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->access$300(Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;Lorg/chromium/content/browser/ContentViewCore;Z)V

    .line 890
    return-void
.end method
