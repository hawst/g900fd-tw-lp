.class public final Lcom/google/android/apps/chrome/snapshot/ChromeToMobileFeature;
.super Ljava/lang/Object;
.source "ChromeToMobileFeature.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    return-void
.end method

.method public static isEnabled(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 42
    invoke-static {p0}, Lcom/google/android/apps/chrome/snapshot/ChromeToMobileFeature;->updateCachedNewProfileManagement(Landroid/content/Context;)V

    .line 43
    invoke-static {p0}, Lcom/google/android/apps/chrome/snapshot/ChromeToMobileFeature;->useNewProfileManagement(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 44
    invoke-static {p0}, Lorg/chromium/sync/signin/ChromeSigninController;->get(Landroid/content/Context;)Lorg/chromium/sync/signin/ChromeSigninController;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/sync/signin/ChromeSigninController;->isSignedIn()Z

    move-result v0

    .line 46
    :goto_0
    return v0

    :cond_0
    invoke-static {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->legacyIsEnabled(Landroid/content/Context;)Z

    move-result v0

    goto :goto_0
.end method

.method public static setEnabled(Landroid/content/Context;Z)Z
    .locals 1

    .prologue
    .line 59
    invoke-static {p0}, Lcom/google/android/apps/chrome/snapshot/ChromeToMobileFeature;->updateCachedNewProfileManagement(Landroid/content/Context;)V

    .line 60
    invoke-static {p0}, Lcom/google/android/apps/chrome/snapshot/ChromeToMobileFeature;->useNewProfileManagement(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 61
    invoke-static {p0}, Lorg/chromium/sync/signin/ChromeSigninController;->get(Landroid/content/Context;)Lorg/chromium/sync/signin/ChromeSigninController;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/sync/signin/ChromeSigninController;->isSignedIn()Z

    move-result p1

    .line 64
    :goto_0
    return p1

    .line 63
    :cond_0
    invoke-static {p0, p1}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->legacySetEnabled(Landroid/content/Context;Z)V

    goto :goto_0
.end method

.method public static updateCachedNewProfileManagement(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 79
    invoke-static {}, Lorg/chromium/base/library_loader/LibraryLoader;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_1

    .line 88
    :cond_0
    :goto_0
    return-void

    .line 81
    :cond_1
    invoke-static {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->isNewProfileManagementEnabled(Landroid/content/Context;)Z

    move-result v0

    .line 82
    invoke-static {}, Lorg/chromium/chrome/browser/signin/SigninManager;->isNewProfileManagementEnabled()Z

    move-result v1

    .line 83
    if-eq v0, v1, :cond_0

    .line 86
    invoke-static {p0, v1}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->setNewProfileManagementEnabled(Landroid/content/Context;Z)V

    .line 87
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->setNeedsUpdating(Landroid/content/Context;Z)V

    goto :goto_0
.end method

.method private static useNewProfileManagement(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 98
    invoke-static {}, Lorg/chromium/base/library_loader/LibraryLoader;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 99
    invoke-static {}, Lorg/chromium/chrome/browser/signin/SigninManager;->isNewProfileManagementEnabled()Z

    move-result v0

    .line 102
    :goto_0
    return v0

    :cond_0
    invoke-static {p0}, Lcom/google/android/apps/chrome/snapshot/SnapshotSettings;->isNewProfileManagementEnabled(Landroid/content/Context;)Z

    move-result v0

    goto :goto_0
.end method
