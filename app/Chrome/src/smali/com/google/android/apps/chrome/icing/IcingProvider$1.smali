.class Lcom/google/android/apps/chrome/icing/IcingProvider$1;
.super Ljava/lang/Object;
.source "IcingProvider.java"

# interfaces
.implements Lorg/chromium/content/browser/BrowserStartupController$StartupCallback;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/icing/IcingProvider;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/icing/IcingProvider;)V
    .locals 0

    .prologue
    .line 176
    iput-object p1, p0, Lcom/google/android/apps/chrome/icing/IcingProvider$1;->this$0:Lcom/google/android/apps/chrome/icing/IcingProvider;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure()V
    .locals 0

    .prologue
    .line 186
    return-void
.end method

.method public onSuccess(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 180
    iget-object v0, p0, Lcom/google/android/apps/chrome/icing/IcingProvider$1;->this$0:Lcom/google/android/apps/chrome/icing/IcingProvider;

    # setter for: Lcom/google/android/apps/chrome/icing/IcingProvider;->mUsageReportingEnabled:Z
    invoke-static {v0, v2}, Lcom/google/android/apps/chrome/icing/IcingProvider;->access$002(Lcom/google/android/apps/chrome/icing/IcingProvider;Z)Z

    .line 181
    iget-object v0, p0, Lcom/google/android/apps/chrome/icing/IcingProvider$1;->this$0:Lcom/google/android/apps/chrome/icing/IcingProvider;

    # getter for: Lcom/google/android/apps/chrome/icing/IcingProvider;->mJniBridge:Lcom/google/android/apps/chrome/icing/SearchJniBridge;
    invoke-static {v0}, Lcom/google/android/apps/chrome/icing/IcingProvider;->access$200(Lcom/google/android/apps/chrome/icing/IcingProvider;)Lcom/google/android/apps/chrome/icing/SearchJniBridge;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/icing/IcingProvider$1;->this$0:Lcom/google/android/apps/chrome/icing/IcingProvider;

    # getter for: Lcom/google/android/apps/chrome/icing/IcingProvider;->mController:Lcom/google/android/apps/chrome/icing/IcingController;
    invoke-static {v1}, Lcom/google/android/apps/chrome/icing/IcingProvider;->access$100(Lcom/google/android/apps/chrome/icing/IcingProvider;)Lcom/google/android/apps/chrome/icing/IcingController;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/chrome/icing/SearchJniBridge;->init(Lcom/google/android/apps/chrome/icing/SearchJniBridge$DataChangeObserver;)Z

    .line 182
    iget-object v0, p0, Lcom/google/android/apps/chrome/icing/IcingProvider$1;->this$0:Lcom/google/android/apps/chrome/icing/IcingProvider;

    # getter for: Lcom/google/android/apps/chrome/icing/IcingProvider;->mNativeSideInitialized:Ljava/util/concurrent/atomic/AtomicBoolean;
    invoke-static {v0}, Lcom/google/android/apps/chrome/icing/IcingProvider;->access$300(Lcom/google/android/apps/chrome/icing/IcingProvider;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 183
    return-void
.end method
