.class Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$1;
.super Ljava/lang/Object;
.source "RecentTabsRowAdapter.java"

# interfaces
.implements Lorg/chromium/chrome/browser/favicon/FaviconHelper$FaviconImageCallback;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;

.field final synthetic val$url:Ljava/lang/String;

.field final synthetic val$viewHolder:Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$ViewHolder;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$ViewHolder;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 771
    iput-object p1, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$1;->this$0:Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;

    iput-object p2, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$1;->val$viewHolder:Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$ViewHolder;

    iput-object p3, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$1;->val$url:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFaviconAvailable(Landroid/graphics/Bitmap;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 774
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$1;->val$viewHolder:Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$ViewHolder;

    iget-object v0, v0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$ViewHolder;->imageCallback:Lorg/chromium/chrome/browser/favicon/FaviconHelper$FaviconImageCallback;

    if-eq p0, v0, :cond_0

    .line 780
    :goto_0
    return-void

    .line 775
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$1;->this$0:Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;

    # invokes: Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->faviconDrawable(Landroid/graphics/Bitmap;)Landroid/graphics/drawable/Drawable;
    invoke-static {v0, p1}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->access$700(Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;Landroid/graphics/Bitmap;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 776
    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$1;->this$0:Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;

    # getter for: Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->mDefaultFavicon:Landroid/graphics/drawable/Drawable;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->access$800(Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 777
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$1;->this$0:Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;

    # getter for: Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->mFaviconCache:Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$FaviconCache;
    invoke-static {v1}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->access$900(Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;)Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$FaviconCache;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$1;->val$url:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$FaviconCache;->putLocalFaviconImage(Ljava/lang/String;Landroid/graphics/drawable/Drawable;)V

    .line 778
    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$1;->val$viewHolder:Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$ViewHolder;

    iget-object v1, v1, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$ViewHolder;->textView:Landroid/widget/TextView;

    invoke-static {v1, v0, v3, v3, v3}, Lorg/chromium/base/ApiCompatibilityUtils;->setCompoundDrawablesRelativeWithIntrinsicBounds(Landroid/widget/TextView;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method
