.class Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$11;
.super Ljava/lang/Object;
.source "YouTubeMediaRouteController.java"

# interfaces
.implements Lcom/google/android/gms/common/api/k;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;

.field final synthetic val$connectingClient:Lcom/google/android/gms/common/api/i;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;Lcom/google/android/gms/common/api/i;)V
    .locals 0

    .prologue
    .line 747
    iput-object p1, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$11;->this$0:Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;

    iput-object p2, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$11;->val$connectingClient:Lcom/google/android/gms/common/api/i;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onConnected(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 751
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$11;->this$0:Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;

    # getter for: Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mDebug:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->access$1100(Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "YouTubeMediaRouteController"

    const-string/jumbo v1, "GoogleApiClient.onConnected()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 752
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$11;->this$0:Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;

    iget-object v1, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$11;->val$connectingClient:Lcom/google/android/gms/common/api/i;

    # setter for: Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mConnectedClient:Lcom/google/android/gms/common/api/i;
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->access$302(Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;Lcom/google/android/gms/common/api/i;)Lcom/google/android/gms/common/api/i;

    .line 754
    sget-object v0, Lcom/google/android/gms/cast/a;->c:Lcom/google/android/gms/cast/d;

    iget-object v1, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$11;->this$0:Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;

    # getter for: Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mConnectedClient:Lcom/google/android/gms/common/api/i;
    invoke-static {v1}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->access$300(Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;)Lcom/google/android/gms/common/api/i;

    move-result-object v1

    const-string/jumbo v2, "urn:x-cast:com.google.youtube.mdx"

    iget-object v3, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$11;->this$0:Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;

    # invokes: Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->createMessageReceivedCallback()Lcom/google/android/gms/cast/k;
    invoke-static {v3}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->access$1700(Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;)Lcom/google/android/gms/cast/k;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/cast/d;->a(Lcom/google/android/gms/common/api/i;Ljava/lang/String;Lcom/google/android/gms/cast/k;)V

    .line 758
    sget-object v0, Lcom/google/android/gms/cast/a;->c:Lcom/google/android/gms/cast/d;

    iget-object v1, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$11;->this$0:Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;

    # getter for: Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->mConnectedClient:Lcom/google/android/gms/common/api/i;
    invoke-static {v1}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->access$300(Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;)Lcom/google/android/gms/common/api/i;

    move-result-object v1

    const-string/jumbo v2, "233637DE"

    new-instance v3, Lcom/google/android/gms/cast/LaunchOptions;

    invoke-direct {v3}, Lcom/google/android/gms/cast/LaunchOptions;-><init>()V

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/cast/d;->a(Lcom/google/android/gms/common/api/i;Ljava/lang/String;Lcom/google/android/gms/cast/LaunchOptions;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$11;->this$0:Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;

    # invokes: Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->createLaunchApplicationCallback()Lcom/google/android/gms/common/api/o;
    invoke-static {v1}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->access$1800(Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;)Lcom/google/android/gms/common/api/o;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/l;->a(Lcom/google/android/gms/common/api/o;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 767
    :goto_0
    return-void

    .line 763
    :catch_0
    move-exception v0

    .line 764
    const-string/jumbo v1, "YouTubeMediaRouteController"

    const-string/jumbo v2, "Failed to launch application "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 765
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$11;->this$0:Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->release()V

    goto :goto_0
.end method

.method public onConnectionSuspended(I)V
    .locals 3

    .prologue
    .line 771
    const-string/jumbo v0, "YouTubeMediaRouteController"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "onConnectionSuspended: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 772
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController$11;->this$0:Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/videofling/YouTubeMediaRouteController;->release()V

    .line 773
    return-void
.end method
