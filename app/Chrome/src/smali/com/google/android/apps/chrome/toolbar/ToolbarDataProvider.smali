.class public interface abstract Lcom/google/android/apps/chrome/toolbar/ToolbarDataProvider;
.super Ljava/lang/Object;
.source "ToolbarDataProvider.java"


# virtual methods
.method public abstract getCorpusChipText()Ljava/lang/String;
.end method

.method public abstract getLoadProgress()I
.end method

.method public abstract getNewTabPageForCurrentTab()Lcom/google/android/apps/chrome/ntp/NewTabPage;
.end method

.method public abstract getPrimaryColor()I
.end method

.method public abstract getQueryExtractionParam()Ljava/lang/String;
.end method

.method public abstract getTab()Lorg/chromium/chrome/browser/Tab;
.end method

.method public abstract getText()Ljava/lang/String;
.end method

.method public abstract isIncognito()Z
.end method

.method public abstract isUsingBrandColor()Z
.end method
