.class Lcom/google/android/apps/chrome/document/DocumentActivity$9;
.super Ljava/lang/Object;
.source "DocumentActivity.java"

# interfaces
.implements Lorg/chromium/content/browser/ContentReadbackHandler$GetBitmapCallback;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/document/DocumentActivity;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/document/DocumentActivity;)V
    .locals 0

    .prologue
    .line 859
    iput-object p1, p0, Lcom/google/android/apps/chrome/document/DocumentActivity$9;->this$0:Lcom/google/android/apps/chrome/document/DocumentActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFinishGetBitmap(Landroid/graphics/Bitmap;)V
    .locals 3

    .prologue
    .line 862
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentActivity$9;->this$0:Lcom/google/android/apps/chrome/document/DocumentActivity;

    iget-object v1, p0, Lcom/google/android/apps/chrome/document/DocumentActivity$9;->this$0:Lcom/google/android/apps/chrome/document/DocumentActivity;

    iget-object v1, v1, Lcom/google/android/apps/chrome/document/DocumentActivity;->mDocumentTab:Lcom/google/android/apps/chrome/document/DocumentTab;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/document/DocumentTab;->getUrl()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/document/DocumentActivity$9;->this$0:Lcom/google/android/apps/chrome/document/DocumentActivity;

    iget-object v2, v2, Lcom/google/android/apps/chrome/document/DocumentActivity;->mDocumentTab:Lcom/google/android/apps/chrome/document/DocumentTab;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/document/DocumentTab;->isIncognito()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/android/apps/chrome/help/GoogleHelpUtils;->getHelpContextIdFromUrl(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p1}, Lcom/google/android/apps/chrome/help/GoogleHelpUtils;->show(Landroid/app/Activity;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    .line 867
    return-void
.end method
