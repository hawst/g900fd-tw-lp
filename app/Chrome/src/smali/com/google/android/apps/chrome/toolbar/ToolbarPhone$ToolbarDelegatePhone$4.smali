.class Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone$4;
.super Landroid/util/Property;
.source "ToolbarPhone.java"


# instance fields
.field private mRtlStateInvalid:Z

.field final synthetic this$1:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;

.field final synthetic val$containerView:Landroid/view/View;

.field final synthetic val$isContainerRtl:Z


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;Ljava/lang/Class;Ljava/lang/String;Landroid/view/View;Z)V
    .locals 0

    .prologue
    .line 1428
    iput-object p1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone$4;->this$1:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;

    iput-object p4, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone$4;->val$containerView:Landroid/view/View;

    iput-boolean p5, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone$4;->val$isContainerRtl:Z

    invoke-direct {p0, p2, p3}, Landroid/util/Property;-><init>(Ljava/lang/Class;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public get(Landroid/widget/TextView;)Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 1433
    invoke-virtual {p1}, Landroid/widget/TextView;->getScrollX()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1428
    check-cast p1, Landroid/widget/TextView;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone$4;->get(Landroid/widget/TextView;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public set(Landroid/widget/TextView;Ljava/lang/Integer;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1438
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone$4;->mRtlStateInvalid:Z

    if-eqz v0, :cond_0

    .line 1451
    :goto_0
    return-void

    .line 1439
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone$4;->val$containerView:Landroid/view/View;

    invoke-static {v0}, Lorg/chromium/base/ApiCompatibilityUtils;->isLayoutRtl(Landroid/view/View;)Z

    move-result v0

    .line 1440
    iget-boolean v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone$4;->val$isContainerRtl:Z

    if-eq v0, v1, :cond_2

    .line 1441
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone$4;->mRtlStateInvalid:Z

    .line 1442
    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone$4;->this$1:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;

    iget-object v1, v1, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone;->this$0:Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;

    # getter for: Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->mUrlBar:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;->access$3100(Lcom/google/android/apps/chrome/toolbar/ToolbarPhone;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 1443
    :cond_1
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    .line 1444
    if-eqz v0, :cond_2

    .line 1445
    invoke-virtual {p1}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/text/Layout;->getPrimaryHorizontal(I)F

    move-result v0

    float-to-int v0, v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 1446
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1}, Landroid/widget/TextView;->getWidth()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    .line 1450
    :cond_2
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setScrollX(I)V

    goto :goto_0
.end method

.method public bridge synthetic set(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1428
    check-cast p1, Landroid/widget/TextView;

    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/chrome/toolbar/ToolbarPhone$ToolbarDelegatePhone$4;->set(Landroid/widget/TextView;Ljava/lang/Integer;)V

    return-void
.end method
