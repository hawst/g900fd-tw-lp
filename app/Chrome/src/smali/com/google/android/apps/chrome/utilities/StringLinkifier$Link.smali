.class public final Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;
.super Ljava/lang/Object;
.source "StringLinkifier.java"

# interfaces
.implements Ljava/lang/Comparable;


# instance fields
.field public final endTag:Ljava/lang/String;

.field private mEndTagIndex:I

.field private mStartTagIndex:I

.field public final span:Ljava/lang/Object;

.field public final startTag:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput-object p1, p0, Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;->startTag:Ljava/lang/String;

    .line 45
    iput-object p2, p0, Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;->endTag:Ljava/lang/String;

    .line 46
    iput-object p3, p0, Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;->span:Ljava/lang/Object;

    .line 47
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;)I
    .locals 1

    .prologue
    .line 31
    iget v0, p0, Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;->mStartTagIndex:I

    return v0
.end method

.method static synthetic access$002(Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;I)I
    .locals 0

    .prologue
    .line 31
    iput p1, p0, Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;->mStartTagIndex:I

    return p1
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;)I
    .locals 1

    .prologue
    .line 31
    iget v0, p0, Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;->mEndTagIndex:I

    return v0
.end method

.method static synthetic access$102(Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;I)I
    .locals 0

    .prologue
    .line 31
    iput p1, p0, Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;->mEndTagIndex:I

    return p1
.end method


# virtual methods
.method public final compareTo(Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;)I
    .locals 2

    .prologue
    .line 51
    iget v0, p0, Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;->mStartTagIndex:I

    iget v1, p1, Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;->mStartTagIndex:I

    if-ge v0, v1, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;->mStartTagIndex:I

    iget v1, p1, Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;->mStartTagIndex:I

    if-ne v0, v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 31
    check-cast p1, Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;->compareTo(Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;)I

    move-result v0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 57
    instance-of v1, p1, Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;

    if-nez v1, :cond_1

    .line 60
    :cond_0
    :goto_0
    return v0

    :cond_1
    check-cast p1, Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;->compareTo(Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;)I

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 65
    const/4 v0, 0x0

    return v0
.end method
