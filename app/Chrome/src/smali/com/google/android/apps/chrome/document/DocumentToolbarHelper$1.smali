.class Lcom/google/android/apps/chrome/document/DocumentToolbarHelper$1;
.super Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;
.source "DocumentToolbarHelper.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 61
    const-class v0, Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/document/DocumentToolbarHelper$1;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;)V
    .locals 0

    .prologue
    .line 61
    iput-object p1, p0, Lcom/google/android/apps/chrome/document/DocumentToolbarHelper$1;->this$0:Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2

    .prologue
    .line 64
    sget-boolean v0, Lcom/google/android/apps/chrome/document/DocumentToolbarHelper$1;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget v0, p1, Landroid/os/Message;->what:I

    const/16 v1, 0x22

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 65
    :cond_0
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "shown"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    .line 66
    if-eqz v0, :cond_1

    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->recordActionBarShown()V

    .line 67
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/chrome/document/DocumentToolbarHelper$1;->this$0:Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;

    # getter for: Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;->mActionBarDelegate:Lcom/google/android/apps/chrome/ContextualMenuBar$ActionBarDelegate;
    invoke-static {v1}, Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;->access$000(Lcom/google/android/apps/chrome/document/DocumentToolbarHelper;)Lcom/google/android/apps/chrome/ContextualMenuBar$ActionBarDelegate;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/chrome/ContextualMenuBar$ActionBarDelegate;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    .line 68
    if-nez v0, :cond_2

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Landroid/app/ActionBar;->hide()V

    .line 69
    :cond_2
    return-void
.end method
