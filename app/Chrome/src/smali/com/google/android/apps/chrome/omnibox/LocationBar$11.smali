.class Lcom/google/android/apps/chrome/omnibox/LocationBar$11;
.super Ljava/lang/Object;
.source "LocationBar.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

.field final synthetic val$currentPageUrl:Ljava/lang/String;

.field final synthetic val$extraHeaders:Ljava/lang/String;

.field final synthetic val$incognito:Z

.field final synthetic val$nextUrl:Ljava/lang/String;

.field final synthetic val$transition:I


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/omnibox/LocationBar;Ljava/lang/String;Ljava/lang/String;ZILjava/lang/String;)V
    .locals 0

    .prologue
    .line 2244
    iput-object p1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$11;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    iput-object p2, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$11;->val$currentPageUrl:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$11;->val$nextUrl:Ljava/lang/String;

    iput-boolean p4, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$11;->val$incognito:Z

    iput p5, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$11;->val$transition:I

    iput-object p6, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$11;->val$extraHeaders:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 2247
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$11;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->access$3400(Lcom/google/android/apps/chrome/omnibox/LocationBar;Z)V

    .line 2248
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$11;->this$0:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    # getter for: Lcom/google/android/apps/chrome/omnibox/LocationBar;->mLoadUrlDelegate:Lcom/google/android/apps/chrome/omnibox/LocationBar$LoadUrlDelegate;
    invoke-static {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->access$3500(Lcom/google/android/apps/chrome/omnibox/LocationBar;)Lcom/google/android/apps/chrome/omnibox/LocationBar$LoadUrlDelegate;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$11;->val$currentPageUrl:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$11;->val$nextUrl:Ljava/lang/String;

    iget-boolean v3, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$11;->val$incognito:Z

    iget v4, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$11;->val$transition:I

    iget-object v5, p0, Lcom/google/android/apps/chrome/omnibox/LocationBar$11;->val$extraHeaders:Ljava/lang/String;

    invoke-interface/range {v0 .. v5}, Lcom/google/android/apps/chrome/omnibox/LocationBar$LoadUrlDelegate;->loadUrl(Ljava/lang/String;Ljava/lang/String;ZILjava/lang/String;)V

    .line 2250
    return-void
.end method
