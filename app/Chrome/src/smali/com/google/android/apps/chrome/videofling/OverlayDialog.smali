.class public Lcom/google/android/apps/chrome/videofling/OverlayDialog;
.super Landroid/app/Dialog;
.source "OverlayDialog.java"


# instance fields
.field private mOutsideTouchHandler:Landroid/view/View$OnTouchListener;


# virtual methods
.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    .line 84
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 85
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/OverlayDialog;->mOutsideTouchHandler:Landroid/view/View$OnTouchListener;

    const/4 v1, 0x0

    invoke-interface {v0, v1, p1}, Landroid/view/View$OnTouchListener;->onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z

    .line 87
    :cond_0
    invoke-super {p0, p1}, Landroid/app/Dialog;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method
