.class Lcom/google/android/apps/chrome/videofling/MediaRouteController$10;
.super Ljava/lang/Object;
.source "MediaRouteController.java"

# interfaces
.implements Lcom/google/android/apps/chrome/videofling/MediaRouteController$ResultBundleHandler;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/videofling/MediaRouteController;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/videofling/MediaRouteController;)V
    .locals 0

    .prologue
    .line 845
    iput-object p1, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController$10;->this$0:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 860
    return-void
.end method

.method public onResult(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 848
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController$10;->this$0:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    const-string/jumbo v1, "android.media.intent.extra.SESSION_ID"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mCurrentSessionId:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->access$202(Lcom/google/android/apps/chrome/videofling/MediaRouteController;Ljava/lang/String;)Ljava/lang/String;

    .line 849
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController$10;->this$0:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    # getter for: Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->access$300(Lcom/google/android/apps/chrome/videofling/MediaRouteController;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController$10;->this$0:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    # getter for: Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mCurrentSessionId:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->access$200(Lcom/google/android/apps/chrome/videofling/MediaRouteController;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/videofling/RemotePlaybackSettings;->setSessionId(Landroid/content/Context;Ljava/lang/String;)V

    .line 850
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController$10;->this$0:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    # getter for: Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mDebug:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->access$400(Lcom/google/android/apps/chrome/videofling/MediaRouteController;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "MediaRouteController"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Got a session id: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController$10;->this$0:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    # getter for: Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mCurrentSessionId:Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->access$200(Lcom/google/android/apps/chrome/videofling/MediaRouteController;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 851
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController$10;->this$0:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    # setter for: Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mIsPrepared:Z
    invoke-static {v0, v3}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->access$1302(Lcom/google/android/apps/chrome/videofling/MediaRouteController;Z)Z

    .line 852
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/MediaRouteController$10;->this$0:Lcom/google/android/apps/chrome/videofling/MediaRouteController;

    # setter for: Lcom/google/android/apps/chrome/videofling/MediaRouteController;->mReconnecting:Z
    invoke-static {v0, v3}, Lcom/google/android/apps/chrome/videofling/MediaRouteController;->access$102(Lcom/google/android/apps/chrome/videofling/MediaRouteController;Z)Z

    .line 853
    return-void
.end method
