.class public Lcom/google/android/apps/chrome/precache/DeviceState;
.super Ljava/lang/Object;
.source "DeviceState.java"


# static fields
.field private static sDeviceState:Lcom/google/android/apps/chrome/precache/DeviceState;


# instance fields
.field protected mNetworkInfoDelegateFactory:Lcom/google/android/apps/chrome/precache/NetworkInfoDelegateFactory;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/apps/chrome/precache/DeviceState;->sDeviceState:Lcom/google/android/apps/chrome/precache/DeviceState;

    return-void
.end method

.method protected constructor <init>()V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    new-instance v0, Lcom/google/android/apps/chrome/precache/NetworkInfoDelegateFactory;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/precache/NetworkInfoDelegateFactory;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/precache/DeviceState;->mNetworkInfoDelegateFactory:Lcom/google/android/apps/chrome/precache/NetworkInfoDelegateFactory;

    .line 27
    return-void
.end method

.method public static getInstance()Lcom/google/android/apps/chrome/precache/DeviceState;
    .locals 1

    .prologue
    .line 30
    sget-object v0, Lcom/google/android/apps/chrome/precache/DeviceState;->sDeviceState:Lcom/google/android/apps/chrome/precache/DeviceState;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/chrome/precache/DeviceState;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/precache/DeviceState;-><init>()V

    sput-object v0, Lcom/google/android/apps/chrome/precache/DeviceState;->sDeviceState:Lcom/google/android/apps/chrome/precache/DeviceState;

    .line 31
    :cond_0
    sget-object v0, Lcom/google/android/apps/chrome/precache/DeviceState;->sDeviceState:Lcom/google/android/apps/chrome/precache/DeviceState;

    return-object v0
.end method


# virtual methods
.method getStickyBatteryStatus(Landroid/content/Context;)I
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 45
    new-instance v1, Landroid/content/IntentFilter;

    const-string/jumbo v2, "android.intent.action.BATTERY_CHANGED"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 46
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object v1

    .line 48
    if-nez v1, :cond_0

    .line 51
    :goto_0
    return v0

    :cond_0
    const-string/jumbo v2, "status"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    goto :goto_0
.end method

.method public isInteractive(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 66
    const-string/jumbo v0, "power"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 67
    invoke-static {v0}, Lcom/google/android/apps/chrome/utilities/UnreleasedApiCompatibilityUtils;->isInteractive(Landroid/os/PowerManager;)Z

    move-result v0

    return v0
.end method

.method public isPowerConnected(Landroid/content/Context;)Z
    .locals 2

    .prologue
    .line 58
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/precache/DeviceState;->getStickyBatteryStatus(Landroid/content/Context;)I

    move-result v0

    .line 59
    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    const/4 v1, 0x5

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isWifiAvailable(Landroid/content/Context;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 72
    iget-object v1, p0, Lcom/google/android/apps/chrome/precache/DeviceState;->mNetworkInfoDelegateFactory:Lcom/google/android/apps/chrome/precache/NetworkInfoDelegateFactory;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/chrome/precache/NetworkInfoDelegateFactory;->getNetworkInfoDelegate(Landroid/content/Context;)Lcom/google/android/apps/chrome/precache/NetworkInfoDelegate;

    move-result-object v1

    .line 74
    invoke-virtual {v1}, Lcom/google/android/apps/chrome/precache/NetworkInfoDelegate;->isValid()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/precache/NetworkInfoDelegate;->getType()I

    move-result v2

    if-ne v2, v0, :cond_0

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/precache/NetworkInfoDelegate;->isAvailable()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/precache/NetworkInfoDelegate;->isConnected()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/precache/NetworkInfoDelegate;->isRoaming()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/precache/NetworkInfoDelegate;->isActiveNetworkMetered()Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method setNetworkInfoDelegateFactory(Lcom/google/android/apps/chrome/precache/NetworkInfoDelegateFactory;)V
    .locals 0

    .prologue
    .line 39
    iput-object p1, p0, Lcom/google/android/apps/chrome/precache/DeviceState;->mNetworkInfoDelegateFactory:Lcom/google/android/apps/chrome/precache/NetworkInfoDelegateFactory;

    .line 40
    return-void
.end method
