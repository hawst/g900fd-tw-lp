.class public interface abstract Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView$Listener;
.super Ljava/lang/Object;
.source "AccountFirstRunView.java"


# virtual methods
.method public abstract onAccountSelectionCanceled()V
.end method

.method public abstract onAccountSelectionConfirmed(Ljava/lang/String;)V
.end method

.method public abstract onFailedToSetForcedAccount(Ljava/lang/String;)V
.end method

.method public abstract onNewAccount()V
.end method

.method public abstract onSettingsButtonClicked(Ljava/lang/String;)V
.end method

.method public abstract onSigningInCompleted(Ljava/lang/String;)V
.end method
