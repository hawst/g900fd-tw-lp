.class public Lcom/google/android/apps/chrome/preferences/autofill/AutofillEnabler;
.super Lcom/google/android/apps/chrome/preferences/ChromeBaseSwitchEnabler;
.source "AutofillEnabler.java"


# instance fields
.field private final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/widget/Switch;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/preferences/ChromeBaseSwitchEnabler;-><init>(Landroid/content/Context;Landroid/widget/Switch;)V

    .line 25
    iput-object p1, p0, Lcom/google/android/apps/chrome/preferences/autofill/AutofillEnabler;->mContext:Landroid/content/Context;

    .line 26
    return-void
.end method


# virtual methods
.method protected isSwitchEnabled()Z
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/autofill/AutofillEnabler;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getPrefAutofillPreference()Z

    move-result v0

    return v0
.end method

.method public onValueChanged(Z)V
    .locals 2

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/autofill/AutofillEnabler;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->setAutofillEnabled(Ljava/lang/Boolean;)V

    .line 31
    return-void
.end method
