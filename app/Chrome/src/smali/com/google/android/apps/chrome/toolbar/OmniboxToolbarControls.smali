.class public Lcom/google/android/apps/chrome/toolbar/OmniboxToolbarControls;
.super Ljava/lang/Object;
.source "OmniboxToolbarControls.java"

# interfaces
.implements Lcom/google/android/apps/chrome/document/DocumentToolbarHelper$ToolbarControls;


# instance fields
.field private mControlContainer:Landroid/view/View;

.field private mToolbar:Lcom/google/android/apps/chrome/toolbar/Toolbar;

.field private mToolbarManager:Lcom/google/android/apps/chrome/toolbar/ToolbarManager;


# direct methods
.method public constructor <init>(Landroid/view/View;Lcom/google/android/apps/chrome/ChromeActivity;Lorg/chromium/chrome/browser/appmenu/AppMenuHandler;Lcom/google/android/apps/chrome/ContextualMenuBar;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    invoke-virtual {p2}, Lcom/google/android/apps/chrome/ChromeActivity;->getTabModelSelector()Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    move-result-object v1

    .line 39
    invoke-interface {v1}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->getCurrentTab()Lorg/chromium/chrome/browser/Tab;

    move-result-object v2

    .line 41
    sget v0, Lcom/google/android/apps/chrome/R$id;->control_container:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/toolbar/OmniboxToolbarControls;->mControlContainer:Landroid/view/View;

    .line 42
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/OmniboxToolbarControls;->mControlContainer:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setBackgroundResource(I)V

    .line 44
    sget v0, Lcom/google/android/apps/chrome/R$id;->toolbar:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/toolbar/Toolbar;

    iput-object v0, p0, Lcom/google/android/apps/chrome/toolbar/OmniboxToolbarControls;->mToolbar:Lcom/google/android/apps/chrome/toolbar/Toolbar;

    .line 46
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/OmniboxToolbarControls;->mToolbar:Lcom/google/android/apps/chrome/toolbar/Toolbar;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/toolbar/Toolbar;->getDelegate()Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;

    move-result-object v3

    .line 47
    invoke-virtual {p4}, Lcom/google/android/apps/chrome/ContextualMenuBar;->getCustomSelectionActionModeCallback()Lcom/google/android/apps/chrome/CustomSelectionActionModeCallback;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->setDefaultActionModeCallbackForTextEdit(Lcom/google/android/apps/chrome/CustomSelectionActionModeCallback;)V

    .line 49
    invoke-virtual {v3}, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->getLocationBar()Lcom/google/android/apps/chrome/omnibox/LocationBar;

    move-result-object v0

    new-instance v4, Lcom/google/android/apps/chrome/WindowDelegate;

    invoke-virtual {p2}, Lcom/google/android/apps/chrome/ChromeActivity;->getWindow()Landroid/view/Window;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/google/android/apps/chrome/WindowDelegate;-><init>(Landroid/view/Window;)V

    invoke-virtual {v0, v4}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->setWindowDelegate(Lcom/google/android/apps/chrome/WindowDelegate;)V

    .line 51
    invoke-virtual {v3}, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->getLocationBar()Lcom/google/android/apps/chrome/omnibox/LocationBar;

    move-result-object v0

    invoke-virtual {v2}, Lorg/chromium/chrome/browser/Tab;->getWindowAndroid()Lorg/chromium/ui/base/WindowAndroid;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->setWindowAndroid(Lorg/chromium/ui/base/WindowAndroid;)V

    .line 52
    invoke-virtual {v3}, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->getLocationBar()Lcom/google/android/apps/chrome/omnibox/LocationBar;

    move-result-object v0

    invoke-virtual {p4}, Lcom/google/android/apps/chrome/ContextualMenuBar;->getActionBarDelegate()Lcom/google/android/apps/chrome/ContextualMenuBar$ActionBarDelegate;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->createContextualMenuBar(Lcom/google/android/apps/chrome/ContextualMenuBar$ActionBarDelegate;)V

    .line 54
    invoke-virtual {v3}, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->getLocationBar()Lcom/google/android/apps/chrome/omnibox/LocationBar;

    move-result-object v0

    invoke-virtual {v0, v6}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->setIgnoreURLBarModification(Z)V

    .line 56
    new-instance v0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;

    iget-object v2, p0, Lcom/google/android/apps/chrome/toolbar/OmniboxToolbarControls;->mToolbar:Lcom/google/android/apps/chrome/toolbar/Toolbar;

    invoke-direct {v0, v2}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;-><init>(Lcom/google/android/apps/chrome/toolbar/Toolbar;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/toolbar/OmniboxToolbarControls;->mToolbarManager:Lcom/google/android/apps/chrome/toolbar/ToolbarManager;

    .line 57
    iget-object v2, p0, Lcom/google/android/apps/chrome/toolbar/OmniboxToolbarControls;->mToolbarManager:Lcom/google/android/apps/chrome/toolbar/ToolbarManager;

    invoke-virtual {p2}, Lcom/google/android/apps/chrome/ChromeActivity;->getFullscreenManager()Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;

    invoke-virtual {v2, v1, p3, v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->initializeWithNative(Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;Lorg/chromium/chrome/browser/appmenu/AppMenuHandler;Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;)V

    .line 59
    invoke-virtual {v3}, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->getLocationBar()Lcom/google/android/apps/chrome/omnibox/LocationBar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->updateVisualsForState()V

    .line 60
    invoke-virtual {v3}, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->getLocationBar()Lcom/google/android/apps/chrome/omnibox/LocationBar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->setUrlToPageUrl()V

    .line 61
    return-void
.end method


# virtual methods
.method public destroy()V
    .locals 0

    .prologue
    .line 84
    return-void
.end method

.method public focusUrlBar()V
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/OmniboxToolbarControls;->mToolbar:Lcom/google/android/apps/chrome/toolbar/Toolbar;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/toolbar/Toolbar;->getDelegate()Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;

    move-result-object v0

    if-nez v0, :cond_0

    .line 73
    :goto_0
    return-void

    .line 72
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/OmniboxToolbarControls;->mToolbar:Lcom/google/android/apps/chrome/toolbar/Toolbar;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/toolbar/Toolbar;->getDelegate()Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->getLocationBar()Lcom/google/android/apps/chrome/omnibox/LocationBar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->requestUrlFocus()V

    goto :goto_0
.end method

.method public getMenuAnchor()Landroid/view/View;
    .locals 2

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/OmniboxToolbarControls;->mToolbar:Lcom/google/android/apps/chrome/toolbar/Toolbar;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/toolbar/Toolbar;->getView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$id;->menu_button:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 88
    return-void
.end method

.method public setThemeColor(I)V
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/OmniboxToolbarControls;->mToolbarManager:Lcom/google/android/apps/chrome/toolbar/ToolbarManager;

    if-nez v0, :cond_0

    .line 80
    :goto_0
    return-void

    .line 79
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/OmniboxToolbarControls;->mToolbarManager:Lcom/google/android/apps/chrome/toolbar/ToolbarManager;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->updatePrimaryColor(I)V

    goto :goto_0
.end method
