.class Lcom/google/android/apps/chrome/feedback/PlayServicesFeedbackReporter;
.super Ljava/lang/Object;
.source "PlayServicesFeedbackReporter.java"

# interfaces
.implements Lcom/google/android/apps/chrome/feedback/FeedbackReporter;
.implements Lcom/google/android/gms/common/api/k;
.implements Lcom/google/android/gms/common/c;


# instance fields
.field private final mApplicationContext:Landroid/content/Context;

.field private mExtras:Landroid/os/Bundle;

.field private mGoogleApiClient:Lcom/google/android/gms/common/api/i;

.field private mScreenshot:Landroid/graphics/Bitmap;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/feedback/PlayServicesFeedbackReporter;->mApplicationContext:Landroid/content/Context;

    .line 37
    return-void
.end method

.method private cleanupData()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 79
    iput-object v0, p0, Lcom/google/android/apps/chrome/feedback/PlayServicesFeedbackReporter;->mScreenshot:Landroid/graphics/Bitmap;

    .line 80
    iput-object v0, p0, Lcom/google/android/apps/chrome/feedback/PlayServicesFeedbackReporter;->mExtras:Landroid/os/Bundle;

    .line 81
    return-void
.end method

.method private cleanupGoogleApiClient()V
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/android/apps/chrome/feedback/PlayServicesFeedbackReporter;->mGoogleApiClient:Lcom/google/android/gms/common/api/i;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/i;->b()V

    .line 85
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/feedback/PlayServicesFeedbackReporter;->mGoogleApiClient:Lcom/google/android/gms/common/api/i;

    .line 86
    return-void
.end method

.method private launchLegacyFeedbackReporterAndCleanup()V
    .locals 3

    .prologue
    .line 72
    invoke-direct {p0}, Lcom/google/android/apps/chrome/feedback/PlayServicesFeedbackReporter;->cleanupGoogleApiClient()V

    .line 73
    new-instance v0, Lcom/google/android/apps/chrome/feedback/LegacyFeedbackReporter;

    iget-object v1, p0, Lcom/google/android/apps/chrome/feedback/PlayServicesFeedbackReporter;->mApplicationContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/google/android/apps/chrome/feedback/LegacyFeedbackReporter;-><init>(Landroid/content/Context;)V

    .line 74
    iget-object v1, p0, Lcom/google/android/apps/chrome/feedback/PlayServicesFeedbackReporter;->mScreenshot:Landroid/graphics/Bitmap;

    iget-object v2, p0, Lcom/google/android/apps/chrome/feedback/PlayServicesFeedbackReporter;->mExtras:Landroid/os/Bundle;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/chrome/feedback/FeedbackReporter;->reportFeedback(Landroid/graphics/Bitmap;Landroid/os/Bundle;)V

    .line 75
    invoke-direct {p0}, Lcom/google/android/apps/chrome/feedback/PlayServicesFeedbackReporter;->cleanupData()V

    .line 76
    return-void
.end method


# virtual methods
.method public onConnected(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/apps/chrome/feedback/PlayServicesFeedbackReporter;->mGoogleApiClient:Lcom/google/android/gms/common/api/i;

    iget-object v1, p0, Lcom/google/android/apps/chrome/feedback/PlayServicesFeedbackReporter;->mScreenshot:Landroid/graphics/Bitmap;

    iget-object v2, p0, Lcom/google/android/apps/chrome/feedback/PlayServicesFeedbackReporter;->mExtras:Landroid/os/Bundle;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/feedback/a;->a(Lcom/google/android/gms/common/api/i;Landroid/graphics/Bitmap;Landroid/os/Bundle;)Lcom/google/android/gms/common/api/l;

    .line 42
    invoke-direct {p0}, Lcom/google/android/apps/chrome/feedback/PlayServicesFeedbackReporter;->cleanupData()V

    .line 43
    invoke-direct {p0}, Lcom/google/android/apps/chrome/feedback/PlayServicesFeedbackReporter;->cleanupGoogleApiClient()V

    .line 44
    return-void
.end method

.method public onConnectionFailed(Lcom/google/android/gms/common/ConnectionResult;)V
    .locals 2

    .prologue
    .line 54
    const-string/jumbo v0, "PlayServicesFeedbackReporter"

    const-string/jumbo v1, "Connection to Google API Client failed. Falling back to legacy mode."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 55
    invoke-direct {p0}, Lcom/google/android/apps/chrome/feedback/PlayServicesFeedbackReporter;->launchLegacyFeedbackReporterAndCleanup()V

    .line 56
    return-void
.end method

.method public onConnectionSuspended(I)V
    .locals 2

    .prologue
    .line 48
    const-string/jumbo v0, "PlayServicesFeedbackReporter"

    const-string/jumbo v1, "Connection to Google API Client suspended. Falling back to legacy mode."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 49
    invoke-direct {p0}, Lcom/google/android/apps/chrome/feedback/PlayServicesFeedbackReporter;->launchLegacyFeedbackReporterAndCleanup()V

    .line 50
    return-void
.end method

.method public reportFeedback(Landroid/graphics/Bitmap;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 60
    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    .line 61
    iput-object p1, p0, Lcom/google/android/apps/chrome/feedback/PlayServicesFeedbackReporter;->mScreenshot:Landroid/graphics/Bitmap;

    .line 62
    iput-object p2, p0, Lcom/google/android/apps/chrome/feedback/PlayServicesFeedbackReporter;->mExtras:Landroid/os/Bundle;

    .line 63
    new-instance v0, Lcom/google/android/gms/common/api/j;

    iget-object v1, p0, Lcom/google/android/apps/chrome/feedback/PlayServicesFeedbackReporter;->mApplicationContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/api/j;-><init>(Landroid/content/Context;)V

    .line 64
    sget-object v1, Lcom/google/android/gms/feedback/a;->a:Lcom/google/android/gms/common/api/a;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/j;->a(Lcom/google/android/gms/common/api/a;)Lcom/google/android/gms/common/api/j;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/gms/common/api/j;->a(Lcom/google/android/gms/common/api/k;)Lcom/google/android/gms/common/api/j;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/gms/common/api/j;->a(Lcom/google/android/gms/common/c;)Lcom/google/android/gms/common/api/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/j;->a()Lcom/google/android/gms/common/api/i;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/feedback/PlayServicesFeedbackReporter;->mGoogleApiClient:Lcom/google/android/gms/common/api/i;

    .line 68
    iget-object v0, p0, Lcom/google/android/apps/chrome/feedback/PlayServicesFeedbackReporter;->mGoogleApiClient:Lcom/google/android/gms/common/api/i;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/i;->a()V

    .line 69
    return-void
.end method
