.class Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog$FolderListEntry;
.super Ljava/lang/Object;
.source "EnhancedBookmarkFolderSelectDialog.java"


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final TYPE_DIVIDER:I = 0x2

.field public static final TYPE_NEW_FOLDER:I = 0x1

.field public static final TYPE_NONE:I = 0x0

.field public static final TYPE_NORMAL:I = 0x3


# instance fields
.field mDepth:I

.field mId:Lorg/chromium/components/bookmarks/BookmarkId;

.field mIsSelected:Z

.field mTitle:Ljava/lang/String;

.field mType:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 180
    const-class v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog$FolderListEntry;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/chromium/components/bookmarks/BookmarkId;ILjava/lang/String;ZI)V
    .locals 1

    .prologue
    .line 193
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 194
    sget-boolean v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog$FolderListEntry;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    const/4 v0, 0x2

    if-eq p5, v0, :cond_0

    const/4 v0, 0x1

    if-eq p5, v0, :cond_0

    if-eqz p5, :cond_0

    const/4 v0, 0x3

    if-eq p5, v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 196
    :cond_0
    iput p2, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog$FolderListEntry;->mDepth:I

    .line 197
    iput-object p1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog$FolderListEntry;->mId:Lorg/chromium/components/bookmarks/BookmarkId;

    .line 198
    iput-object p3, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog$FolderListEntry;->mTitle:Ljava/lang/String;

    .line 199
    iput-boolean p4, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog$FolderListEntry;->mIsSelected:Z

    .line 200
    iput p5, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog$FolderListEntry;->mType:I

    .line 201
    return-void
.end method
