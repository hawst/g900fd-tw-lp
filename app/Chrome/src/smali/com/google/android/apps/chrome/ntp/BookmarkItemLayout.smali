.class public Lcom/google/android/apps/chrome/ntp/BookmarkItemLayout;
.super Landroid/widget/LinearLayout;
.source "BookmarkItemLayout.java"


# instance fields
.field private mBookmarkItemView:Lcom/google/android/apps/chrome/ntp/BookmarkItemView;

.field private mLayoutParams:Landroid/view/ViewGroup$LayoutParams;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/chrome/ntp/BookmarkItemView;)V
    .locals 3

    .prologue
    .line 26
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 27
    iput-object p2, p0, Lcom/google/android/apps/chrome/ntp/BookmarkItemLayout;->mBookmarkItemView:Lcom/google/android/apps/chrome/ntp/BookmarkItemView;

    .line 28
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarkItemLayout;->mLayoutParams:Landroid/view/ViewGroup$LayoutParams;

    .line 30
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarkItemLayout;->mBookmarkItemView:Lcom/google/android/apps/chrome/ntp/BookmarkItemView;

    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/BookmarkItemLayout;->mLayoutParams:Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/ntp/BookmarkItemView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 31
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarkItemLayout;->mBookmarkItemView:Lcom/google/android/apps/chrome/ntp/BookmarkItemView;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ntp/BookmarkItemLayout;->addView(Landroid/view/View;)V

    .line 32
    const/16 v0, 0x30

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ntp/BookmarkItemLayout;->setGravity(I)V

    .line 33
    return-void
.end method


# virtual methods
.method public getBookmarkItemView()Lcom/google/android/apps/chrome/ntp/BookmarkItemView;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarkItemLayout;->mBookmarkItemView:Lcom/google/android/apps/chrome/ntp/BookmarkItemView;

    return-object v0
.end method

.method public setHeight(I)V
    .locals 1

    .prologue
    .line 47
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ntp/BookmarkItemLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarkItemLayout;->mLayoutParams:Landroid/view/ViewGroup$LayoutParams;

    .line 48
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarkItemLayout;->mLayoutParams:Landroid/view/ViewGroup$LayoutParams;

    iput p1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 49
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarkItemLayout;->mLayoutParams:Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ntp/BookmarkItemLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 50
    return-void
.end method
