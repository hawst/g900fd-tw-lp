.class Lcom/google/android/apps/chrome/preferences/HomepagePreferences$4;
.super Ljava/lang/Object;
.source "HomepagePreferences.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/preferences/HomepagePreferences;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/preferences/HomepagePreferences;)V
    .locals 0

    .prologue
    .line 86
    iput-object p1, p0, Lcom/google/android/apps/chrome/preferences/HomepagePreferences$4;->this$0:Lcom/google/android/apps/chrome/preferences/HomepagePreferences;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/HomepagePreferences$4;->this$0:Lcom/google/android/apps/chrome/preferences/HomepagePreferences;

    # getter for: Lcom/google/android/apps/chrome/preferences/HomepagePreferences;->mChromePreferenceManager:Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;
    invoke-static {v0}, Lcom/google/android/apps/chrome/preferences/HomepagePreferences;->access$300(Lcom/google/android/apps/chrome/preferences/HomepagePreferences;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->setPrefHomepagePartnerEnabledPreference(Z)V

    .line 90
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/HomepagePreferences$4;->this$0:Lcom/google/android/apps/chrome/preferences/HomepagePreferences;

    # invokes: Lcom/google/android/apps/chrome/preferences/HomepagePreferences;->updateUIState()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/preferences/HomepagePreferences;->access$000(Lcom/google/android/apps/chrome/preferences/HomepagePreferences;)V

    .line 91
    return-void
.end method
