.class Lcom/google/android/apps/chrome/ChromeTabbedActivity$InternalIntentDelegate;
.super Ljava/lang/Object;
.source "ChromeTabbedActivity.java"

# interfaces
.implements Lcom/google/android/apps/chrome/IntentHandler$IntentHandlerDelegate;


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/ChromeTabbedActivity;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 637
    const-class v0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/ChromeTabbedActivity$InternalIntentDelegate;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Lcom/google/android/apps/chrome/ChromeTabbedActivity;)V
    .locals 0

    .prologue
    .line 637
    iput-object p1, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity$InternalIntentDelegate;->this$0:Lcom/google/android/apps/chrome/ChromeTabbedActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/chrome/ChromeTabbedActivity;Lcom/google/android/apps/chrome/ChromeTabbedActivity$1;)V
    .locals 0

    .prologue
    .line 637
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/ChromeTabbedActivity$InternalIntentDelegate;-><init>(Lcom/google/android/apps/chrome/ChromeTabbedActivity;)V

    return-void
.end method


# virtual methods
.method public processUrlViewIntent(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/chrome/IntentHandler$TabOpenType;Ljava/lang/String;Ljava/lang/String;ILandroid/content/Intent;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v3, -0x1

    const/4 v5, 0x1

    .line 648
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity$InternalIntentDelegate;->this$0:Lcom/google/android/apps/chrome/ChromeTabbedActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getCurrentTabModel()Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v0

    .line 649
    sget-object v1, Lcom/google/android/apps/chrome/ChromeTabbedActivity$19;->$SwitchMap$com$google$android$apps$chrome$IntentHandler$TabOpenType:[I

    invoke-virtual {p3}, Lcom/google/android/apps/chrome/IntentHandler$TabOpenType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 720
    sget-boolean v0, Lcom/google/android/apps/chrome/ChromeTabbedActivity$InternalIntentDelegate;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Unknown TabOpenType: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 652
    :pswitch_0
    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->getCount()I

    move-result v1

    if-lez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity$InternalIntentDelegate;->this$0:Lcom/google/android/apps/chrome/ChromeTabbedActivity;

    # getter for: Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mUIInitialized:Z
    invoke-static {v1}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->access$300(Lcom/google/android/apps/chrome/ChromeTabbedActivity;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity$InternalIntentDelegate;->this$0:Lcom/google/android/apps/chrome/ChromeTabbedActivity;

    # invokes: Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getAndSetupOverviewLayout()Lcom/google/android/apps/chrome/OverviewBehavior;
    invoke-static {v1}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->access$400(Lcom/google/android/apps/chrome/ChromeTabbedActivity;)Lcom/google/android/apps/chrome/OverviewBehavior;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/chrome/OverviewBehavior;->overviewVisible()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 654
    iget-object v1, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity$InternalIntentDelegate;->this$0:Lcom/google/android/apps/chrome/ChromeTabbedActivity;

    # invokes: Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getAndSetupOverviewLayout()Lcom/google/android/apps/chrome/OverviewBehavior;
    invoke-static {v1}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->access$400(Lcom/google/android/apps/chrome/ChromeTabbedActivity;)Lcom/google/android/apps/chrome/OverviewBehavior;

    move-result-object v1

    invoke-interface {v1, v5}, Lcom/google/android/apps/chrome/OverviewBehavior;->hideOverview(Z)V

    .line 656
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity$InternalIntentDelegate;->this$0:Lcom/google/android/apps/chrome/ChromeTabbedActivity;

    # getter for: Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mTabModelSelectorImpl:Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;
    invoke-static {v1}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->access$500(Lcom/google/android/apps/chrome/ChromeTabbedActivity;)Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->tryToRestoreTabState(Ljava/lang/String;)Z

    .line 657
    invoke-static {v0, p1}, Lorg/chromium/chrome/browser/tabmodel/TabModelUtils;->getTabIndexByUrl(Lorg/chromium/chrome/browser/tabmodel/TabList;Ljava/lang/String;)I

    move-result v1

    .line 658
    invoke-interface {v0, v1}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->getTabAt(I)Lorg/chromium/chrome/browser/Tab;

    move-result-object v2

    .line 659
    if-eqz v2, :cond_3

    .line 660
    invoke-static {v0, v1}, Lorg/chromium/chrome/browser/tabmodel/TabModelUtils;->setIndex(Lorg/chromium/chrome/browser/tabmodel/TabModel;I)V

    .line 661
    invoke-virtual {v2}, Lorg/chromium/chrome/browser/Tab;->reload()V

    .line 662
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->tabClobbered()V

    .line 666
    :goto_0
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->receivedExternalIntent()V

    .line 723
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity$InternalIntentDelegate;->this$0:Lcom/google/android/apps/chrome/ChromeTabbedActivity;

    # getter for: Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mToolbar:Lcom/google/android/apps/chrome/toolbar/Toolbar;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->access$700(Lcom/google/android/apps/chrome/ChromeTabbedActivity;)Lcom/google/android/apps/chrome/toolbar/Toolbar;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/toolbar/Toolbar;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 725
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity$InternalIntentDelegate;->this$0:Lcom/google/android/apps/chrome/ChromeTabbedActivity;

    # getter for: Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mToolbar:Lcom/google/android/apps/chrome/toolbar/Toolbar;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->access$700(Lcom/google/android/apps/chrome/ChromeTabbedActivity;)Lcom/google/android/apps/chrome/toolbar/Toolbar;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/toolbar/Toolbar;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->clearFocus()V

    .line 727
    :cond_2
    return-void

    .line 664
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity$InternalIntentDelegate;->this$0:Lcom/google/android/apps/chrome/ChromeTabbedActivity;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p4

    move-object v4, p5

    move-object v6, p7

    # invokes: Lcom/google/android/apps/chrome/ChromeTabbedActivity;->launchIntent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLandroid/content/Intent;)V
    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->access$600(Lcom/google/android/apps/chrome/ChromeTabbedActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLandroid/content/Intent;)V

    goto :goto_0

    .line 669
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity$InternalIntentDelegate;->this$0:Lcom/google/android/apps/chrome/ChromeTabbedActivity;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p4

    move-object v4, p5

    move v5, v6

    move-object v6, p7

    # invokes: Lcom/google/android/apps/chrome/ChromeTabbedActivity;->launchIntent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLandroid/content/Intent;)V
    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->access$600(Lcom/google/android/apps/chrome/ChromeTabbedActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLandroid/content/Intent;)V

    .line 670
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->receivedExternalIntent()V

    goto :goto_1

    .line 673
    :pswitch_2
    invoke-static {v0, p6}, Lorg/chromium/chrome/browser/tabmodel/TabModelUtils;->getTabIndexById(Lorg/chromium/chrome/browser/tabmodel/TabList;I)I

    move-result v1

    .line 674
    if-ne v1, v3, :cond_6

    .line 675
    iget-object v1, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity$InternalIntentDelegate;->this$0:Lcom/google/android/apps/chrome/ChromeTabbedActivity;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getTabModelSelector()Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    move-result-object v1

    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->isIncognito()Z

    move-result v0

    if-nez v0, :cond_5

    :goto_2
    invoke-interface {v1, v5}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->getModel(Z)Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v0

    .line 677
    invoke-static {v0, p6}, Lorg/chromium/chrome/browser/tabmodel/TabModelUtils;->getTabIndexById(Lorg/chromium/chrome/browser/tabmodel/TabList;I)I

    move-result v1

    .line 678
    if-eq v1, v3, :cond_4

    .line 679
    iget-object v2, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity$InternalIntentDelegate;->this$0:Lcom/google/android/apps/chrome/ChromeTabbedActivity;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getTabModelSelector()Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    move-result-object v2

    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->isIncognito()Z

    move-result v3

    invoke-interface {v2, v3}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->selectModel(Z)V

    .line 680
    invoke-static {v0, v1}, Lorg/chromium/chrome/browser/tabmodel/TabModelUtils;->setIndex(Lorg/chromium/chrome/browser/tabmodel/TabModel;I)V

    .line 685
    :cond_4
    :goto_3
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->receivedExternalIntent()V

    goto :goto_1

    :cond_5
    move v5, v6

    .line 675
    goto :goto_2

    .line 683
    :cond_6
    invoke-static {v0, v1}, Lorg/chromium/chrome/browser/tabmodel/TabModelUtils;->setIndex(Lorg/chromium/chrome/browser/tabmodel/TabModel;I)V

    goto :goto_3

    .line 690
    :pswitch_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity$InternalIntentDelegate;->this$0:Lcom/google/android/apps/chrome/ChromeTabbedActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getCurrentTab()Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    .line 691
    if-eqz v0, :cond_7

    .line 692
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getTabRedirectHandler()Lcom/google/android/apps/chrome/tab/TabRedirectHandler;

    move-result-object v1

    invoke-virtual {v1, p7}, Lcom/google/android/apps/chrome/tab/TabRedirectHandler;->updateIntent(Landroid/content/Intent;)V

    .line 693
    new-instance v1, Lorg/chromium/content_public/browser/LoadUrlParams;

    const/high16 v2, 0x8000000

    invoke-direct {v1, p1, v2}, Lorg/chromium/content_public/browser/LoadUrlParams;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->loadUrl(Lorg/chromium/content_public/browser/LoadUrlParams;)I

    .line 696
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->tabClobbered()V

    goto/16 :goto_1

    .line 698
    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity$InternalIntentDelegate;->this$0:Lcom/google/android/apps/chrome/ChromeTabbedActivity;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p4

    move-object v4, p5

    move-object v6, p7

    # invokes: Lcom/google/android/apps/chrome/ChromeTabbedActivity;->launchIntent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLandroid/content/Intent;)V
    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->access$600(Lcom/google/android/apps/chrome/ChromeTabbedActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLandroid/content/Intent;)V

    goto/16 :goto_1

    .line 702
    :pswitch_4
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity$InternalIntentDelegate;->this$0:Lcom/google/android/apps/chrome/ChromeTabbedActivity;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p4

    move-object v4, p5

    move-object v6, p7

    # invokes: Lcom/google/android/apps/chrome/ChromeTabbedActivity;->launchIntent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLandroid/content/Intent;)V
    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->access$600(Lcom/google/android/apps/chrome/ChromeTabbedActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLandroid/content/Intent;)V

    .line 703
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->receivedExternalIntent()V

    goto/16 :goto_1

    .line 706
    :pswitch_5
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity$InternalIntentDelegate;->this$0:Lcom/google/android/apps/chrome/ChromeTabbedActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-static {p5, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 709
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity$InternalIntentDelegate;->this$0:Lcom/google/android/apps/chrome/ChromeTabbedActivity;

    invoke-virtual {v0, v5}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getTabCreator(Z)Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;

    move-result-object v0

    const-string/jumbo v1, "chrome-native://newtab/"

    sget-object v2, Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;->FROM_MENU_OR_OVERVIEW:Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;->launchUrl(Ljava/lang/String;Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;)Lcom/google/android/apps/chrome/tab/ChromeTab;

    goto/16 :goto_1

    .line 713
    :cond_8
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity$InternalIntentDelegate;->this$0:Lcom/google/android/apps/chrome/ChromeTabbedActivity;

    invoke-virtual {v0, v5}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getTabCreator(Z)Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;

    move-result-object v0

    const-string/jumbo v1, "chrome-native://newtab/"

    sget-object v2, Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;->FROM_EXTERNAL_APP:Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;->launchUrl(Ljava/lang/String;Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;)Lcom/google/android/apps/chrome/tab/ChromeTab;

    .line 716
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->receivedExternalIntent()V

    goto/16 :goto_1

    .line 649
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public processWebSearchIntent(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 731
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.action.WEB_SEARCH"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 732
    const-string/jumbo v1, "query"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 733
    iget-object v1, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity$InternalIntentDelegate;->this$0:Lcom/google/android/apps/chrome/ChromeTabbedActivity;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->startActivity(Landroid/content/Intent;)V

    .line 734
    return-void
.end method
