.class Lcom/google/android/apps/chrome/enhancedbookmark/ChromeEnhancedBookmarkBridge$2;
.super Ljava/lang/Object;
.source "ChromeEnhancedBookmarkBridge.java"

# interfaces
.implements Lcom/google/android/apps/chrome/enhancedbookmark/ChromeEnhancedBookmarkBridge$SalientImageCallback;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/enhancedbookmark/ChromeEnhancedBookmarkBridge;

.field final synthetic val$callback:Lcom/google/android/apps/chrome/enhancedbookmark/ChromeEnhancedBookmarkBridge$SalientImageCallback;

.field final synthetic val$url:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/enhancedbookmark/ChromeEnhancedBookmarkBridge;Ljava/lang/String;Lcom/google/android/apps/chrome/enhancedbookmark/ChromeEnhancedBookmarkBridge$SalientImageCallback;)V
    .locals 0

    .prologue
    .line 81
    iput-object p1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/ChromeEnhancedBookmarkBridge$2;->this$0:Lcom/google/android/apps/chrome/enhancedbookmark/ChromeEnhancedBookmarkBridge;

    iput-object p2, p0, Lcom/google/android/apps/chrome/enhancedbookmark/ChromeEnhancedBookmarkBridge$2;->val$url:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/apps/chrome/enhancedbookmark/ChromeEnhancedBookmarkBridge$2;->val$callback:Lcom/google/android/apps/chrome/enhancedbookmark/ChromeEnhancedBookmarkBridge$SalientImageCallback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSalientImageReady(Landroid/graphics/Bitmap;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 84
    if-eqz p1, :cond_0

    .line 85
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/ChromeEnhancedBookmarkBridge$2;->this$0:Lcom/google/android/apps/chrome/enhancedbookmark/ChromeEnhancedBookmarkBridge;

    # getter for: Lcom/google/android/apps/chrome/enhancedbookmark/ChromeEnhancedBookmarkBridge;->mSalientImageCache:Landroid/util/LruCache;
    invoke-static {v0}, Lcom/google/android/apps/chrome/enhancedbookmark/ChromeEnhancedBookmarkBridge;->access$000(Lcom/google/android/apps/chrome/enhancedbookmark/ChromeEnhancedBookmarkBridge;)Landroid/util/LruCache;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/ChromeEnhancedBookmarkBridge$2;->val$url:Ljava/lang/String;

    new-instance v2, Landroid/util/Pair;

    invoke-direct {v2, p2, p1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v0, v1, v2}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 87
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/ChromeEnhancedBookmarkBridge$2;->val$callback:Lcom/google/android/apps/chrome/enhancedbookmark/ChromeEnhancedBookmarkBridge$SalientImageCallback;

    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/chrome/enhancedbookmark/ChromeEnhancedBookmarkBridge$SalientImageCallback;->onSalientImageReady(Landroid/graphics/Bitmap;Ljava/lang/String;)V

    .line 88
    return-void
.end method
