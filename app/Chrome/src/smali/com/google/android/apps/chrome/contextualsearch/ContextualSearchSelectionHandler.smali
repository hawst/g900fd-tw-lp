.class public interface abstract Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionHandler;
.super Ljava/lang/Object;
.source "ContextualSearchSelectionHandler.java"


# virtual methods
.method public abstract handleInvalidTap()V
.end method

.method public abstract handleScroll()V
.end method

.method public abstract handleSelection(Ljava/lang/String;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchSelectionController$SelectionType;FF)V
.end method

.method public abstract handleSelectionModification(Ljava/lang/String;FF)V
.end method
