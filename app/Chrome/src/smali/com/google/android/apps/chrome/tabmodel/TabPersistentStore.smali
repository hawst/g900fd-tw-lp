.class public Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;
.super Ljava/lang/Object;
.source "TabPersistentStore.java"


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final SAVED_STATE_FILE:Ljava/lang/String; = "tab_state"

.field public static final SAVED_TAB_STATE_FILE_PREFIX:Ljava/lang/String; = "tab"

.field public static final SAVED_TAB_STATE_FILE_PREFIX_INCOGNITO:Ljava/lang/String; = "cryptonito"

.field private static sMigrationTask:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$FileMigrationTask;


# instance fields
.field private mCancelIncognitoTabLoads:Z

.field private mCancelNormalTabLoads:Z

.field private final mContext:Landroid/content/Context;

.field private mDestroyed:Z

.field private mIncognitoTabsRestored:Landroid/util/SparseIntArray;

.field private mLoadTabTask:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$LoadTabTask;

.field private mNormalTabsRestored:Landroid/util/SparseIntArray;

.field private mSaveListTask:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$SaveListTask;

.field private mSaveTabTask:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$SaveTabTask;

.field private final mSelectorIndex:I

.field private mTabContentManager:Lcom/google/android/apps/chrome/compositor/TabContentManager;

.field private final mTabCreatorManager:Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreatorManager;

.field private final mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

.field private final mTabsToRestore:Ljava/util/Deque;

.field private final mTabsToSave:Ljava/util/Deque;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45
    const-class v0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->$assertionsDisabled:Z

    .line 60
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->sMigrationTask:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$FileMigrationTask;

    return-void

    .line 45
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;ILandroid/content/Context;Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreatorManager;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 109
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 93
    iput-boolean v2, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mCancelNormalTabLoads:Z

    .line 94
    iput-boolean v2, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mCancelIncognitoTabLoads:Z

    .line 110
    iput-object p1, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    .line 111
    iput-object p3, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mContext:Landroid/content/Context;

    .line 112
    iput-object p4, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mTabCreatorManager:Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreatorManager;

    .line 113
    new-instance v0, Ljava/util/ArrayDeque;

    invoke-direct {v0}, Ljava/util/ArrayDeque;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mTabsToSave:Ljava/util/Deque;

    .line 114
    new-instance v0, Ljava/util/ArrayDeque;

    invoke-direct {v0}, Ljava/util/ArrayDeque;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mTabsToRestore:Ljava/util/Deque;

    .line 115
    iput p2, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mSelectorIndex:I

    .line 117
    sget-object v0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->sMigrationTask:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$FileMigrationTask;

    if-nez v0, :cond_0

    .line 118
    new-instance v0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$FileMigrationTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$FileMigrationTask;-><init>(Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$1;)V

    .line 119
    sput-object v0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->sMigrationTask:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$FileMigrationTask;

    sget-object v1, Landroid/os/AsyncTask;->SERIAL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$FileMigrationTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 121
    :cond_0
    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;)I
    .locals 1

    .prologue
    .line 45
    iget v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mSelectorIndex:I

    return v0
.end method

.method static synthetic access$1200(Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;I)Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->readTabState(I)Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1300(Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;)Z
    .locals 1

    .prologue
    .line 45
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mCancelIncognitoTabLoads:Z

    return v0
.end method

.method static synthetic access$1400(Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;)Z
    .locals 1

    .prologue
    .line 45
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mCancelNormalTabLoads:Z

    return v0
.end method

.method static synthetic access$1500(Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$TabRestoreDetails;Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;Z)V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->restoreTab(Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$TabRestoreDetails;Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;Z)V

    return-void
.end method

.method static synthetic access$1600(Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;)V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->loadNextTab()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;)Z
    .locals 1

    .prologue
    .line 45
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mDestroyed:Z

    return v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;IZ)Ljava/io/FileOutputStream;
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->openTabStateOutputStream(IZ)Ljava/io/FileOutputStream;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;IZ)V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->deleteTabStateFile(IZ)V

    return-void
.end method

.method static synthetic access$502(Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$SaveTabTask;)Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$SaveTabTask;
    .locals 0

    .prologue
    .line 45
    iput-object p1, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mSaveTabTask:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$SaveTabTask;

    return-object p1
.end method

.method static synthetic access$600(Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;)V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->saveNextTab()V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;)Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;[B)V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->saveListToFile([B)V

    return-void
.end method

.method static synthetic access$902(Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$SaveListTask;)Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$SaveListTask;
    .locals 0

    .prologue
    .line 45
    iput-object p1, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mSaveListTask:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$SaveListTask;

    return-object p1
.end method

.method private cleanupAllEncryptedPersistentData()V
    .locals 5

    .prologue
    .line 742
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mContext:Landroid/content/Context;

    iget v1, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mSelectorIndex:I

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->getStateFolder(Landroid/content/Context;I)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v1

    .line 743
    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 744
    const-string/jumbo v4, "cryptonito"

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 745
    invoke-direct {p0, v3}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->deleteFileAsync(Ljava/lang/String;)V

    .line 743
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 748
    :cond_1
    return-void
.end method

.method private cleanupPersistentData()V
    .locals 7

    .prologue
    .line 713
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mContext:Landroid/content/Context;

    iget v1, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mSelectorIndex:I

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->getStateFolder(Landroid/content/Context;I)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v2

    .line 714
    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 715
    invoke-static {v4}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->parseStateDataFromFilename(Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v5

    .line 716
    if-eqz v5, :cond_0

    .line 717
    iget-object v6, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    iget-object v0, v5, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-interface {v6, v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->getModel(Z)Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v6

    .line 718
    iget-object v0, v5, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v6, v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelUtils;->getTabById(Lorg/chromium/chrome/browser/tabmodel/TabList;I)Lorg/chromium/chrome/browser/Tab;

    move-result-object v0

    if-nez v0, :cond_0

    .line 719
    invoke-direct {p0, v4}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->deleteFileAsync(Ljava/lang/String;)V

    .line 714
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 723
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mTabContentManager:Lcom/google/android/apps/chrome/compositor/TabContentManager;

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/compositor/TabContentManager;->cleanupPersistentData(Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;)V

    .line 724
    return-void
.end method

.method private cleanupPersistentData(IZ)V
    .locals 1

    .prologue
    .line 456
    invoke-static {p1, p2}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->getTabStateFilename(IZ)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->deleteFileAsync(Ljava/lang/String;)V

    .line 459
    return-void
.end method

.method private cleanupPersistentDataAtAndAboveId(I)V
    .locals 5

    .prologue
    .line 727
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mContext:Landroid/content/Context;

    iget v1, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mSelectorIndex:I

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->getStateFolder(Landroid/content/Context;I)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v2

    .line 729
    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 730
    invoke-static {v4}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->parseStateDataFromFilename(Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v0

    .line 731
    if-eqz v0, :cond_0

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-lt v0, p1, :cond_0

    .line 732
    invoke-direct {p0, v4}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->deleteFileAsync(Ljava/lang/String;)V

    .line 729
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 736
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mTabContentManager:Lcom/google/android/apps/chrome/compositor/TabContentManager;

    if-eqz v0, :cond_2

    .line 737
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mTabContentManager:Lcom/google/android/apps/chrome/compositor/TabContentManager;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/compositor/TabContentManager;->cleanupPersistentDataAtAndAboveId(I)V

    .line 739
    :cond_2
    return-void
.end method

.method private deleteFileAsync(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 751
    new-instance v0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$2;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$2;-><init>(Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;Ljava/lang/String;)V

    sget-object v1, Landroid/os/AsyncTask;->SERIAL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$2;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 760
    return-void
.end method

.method private deleteTabStateFile(IZ)V
    .locals 3

    .prologue
    .line 868
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mContext:Landroid/content/Context;

    iget v1, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mSelectorIndex:I

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->getStateFolder(Landroid/content/Context;I)Ljava/io/File;

    move-result-object v0

    .line 869
    new-instance v1, Ljava/io/File;

    invoke-static {p1, p2}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->getTabStateFilename(IZ)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 870
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 871
    :cond_0
    return-void
.end method

.method public static getStateFolder(Landroid/content/Context;I)Ljava/io/File;
    .locals 3

    .prologue
    .line 130
    new-instance v0, Ljava/io/File;

    const-string/jumbo v1, "tabs"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 132
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 133
    return-object v0
.end method

.method public static getTabStateFilename(IZ)Ljava/lang/String;
    .locals 2

    .prologue
    .line 157
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    if-eqz p1, :cond_0

    const-string/jumbo v0, "cryptonito"

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string/jumbo v0, "tab"

    goto :goto_0
.end method

.method private getTabToRestoreById(I)Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$TabRestoreDetails;
    .locals 3

    .prologue
    .line 438
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mTabsToRestore:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$TabRestoreDetails;

    .line 439
    iget v2, v0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$TabRestoreDetails;->id:I

    if-ne v2, p1, :cond_0

    .line 443
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getTabToRestoreByUrl(Ljava/lang/String;)Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$TabRestoreDetails;
    .locals 3

    .prologue
    .line 429
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mTabsToRestore:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$TabRestoreDetails;

    .line 430
    iget-object v2, v0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$TabRestoreDetails;->url:Ljava/lang/String;

    invoke-static {v2, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 434
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private loadNextTab()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 697
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mDestroyed:Z

    if-eqz v0, :cond_0

    .line 710
    :goto_0
    return-void

    .line 699
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mTabsToRestore:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 700
    iput-object v1, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mNormalTabsRestored:Landroid/util/SparseIntArray;

    .line 701
    iput-object v1, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mIncognitoTabsRestored:Landroid/util/SparseIntArray;

    .line 702
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->cleanupPersistentData()V

    .line 703
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->onStateLoaded()V

    .line 704
    iput-object v1, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mLoadTabTask:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$LoadTabTask;

    goto :goto_0

    .line 706
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mTabsToRestore:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->removeFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$TabRestoreDetails;

    .line 707
    new-instance v1, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$LoadTabTask;

    invoke-direct {v1, p0, v0}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$LoadTabTask;-><init>(Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$TabRestoreDetails;)V

    iput-object v1, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mLoadTabTask:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$LoadTabTask;

    .line 708
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mLoadTabTask:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$LoadTabTask;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$LoadTabTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method private static logSaveException(Ljava/lang/Exception;)V
    .locals 2

    .prologue
    .line 146
    const-string/jumbo v0, "TabPersistentStore"

    const-string/jumbo v1, "Error while saving tabs state; will attempt to continue..."

    invoke-static {v0, v1, p0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 147
    return-void
.end method

.method private onStateLoaded()V
    .locals 2

    .prologue
    .line 692
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mContext:Landroid/content/Context;

    const/16 v1, 0x10

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastNotification(Landroid/content/Context;I)V

    .line 694
    return-void
.end method

.method private openTabStateOutputStream(IZ)Ljava/io/FileOutputStream;
    .locals 4

    .prologue
    .line 863
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mContext:Landroid/content/Context;

    iget v1, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mSelectorIndex:I

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->getStateFolder(Landroid/content/Context;I)Ljava/io/File;

    move-result-object v0

    .line 864
    new-instance v1, Ljava/io/FileOutputStream;

    new-instance v2, Ljava/io/File;

    invoke-static {p1, p2}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->getTabStateFilename(IZ)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v0, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-direct {v1, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    return-object v1
.end method

.method public static parseStateDataFromFilename(Ljava/lang/String;)Landroid/util/Pair;
    .locals 2

    .prologue
    .line 880
    :try_start_0
    const-string/jumbo v0, "cryptonito"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 881
    const/16 v0, 0xa

    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 883
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    .line 892
    :goto_0
    return-object v0

    .line 884
    :cond_0
    const-string/jumbo v0, "tab"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 885
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 887
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 892
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static readSavedStateFile(Ljava/io/File;Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$OnTabStateReadCallback;)I
    .locals 13

    .prologue
    .line 560
    const/4 v1, 0x0

    .line 565
    invoke-static {}, Landroid/os/StrictMode;->allowThreadDiskReads()Landroid/os/StrictMode$ThreadPolicy;

    move-result-object v9

    .line 567
    :try_start_0
    new-instance v0, Ljava/io/File;

    const-string/jumbo v2, "tab_state"

    invoke-direct {v0, p0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 568
    invoke-virtual {v0}, Ljava/io/File;->exists()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result v2

    if-nez v2, :cond_0

    .line 601
    const/4 v0, 0x0

    invoke-static {v0}, Lorg/chromium/chrome/browser/util/StreamUtil;->closeQuietly(Ljava/io/Closeable;)V

    .line 602
    invoke-static {v9}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    const/4 v0, 0x0

    :goto_0
    return v0

    .line 570
    :cond_0
    :try_start_1
    new-instance v6, Ljava/io/DataInputStream;

    new-instance v2, Ljava/io/BufferedInputStream;

    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v2, v3}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v6, v2}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 572
    const/4 v0, 0x0

    .line 573
    const/4 v1, 0x0

    .line 574
    :try_start_2
    invoke-virtual {v6}, Ljava/io/DataInputStream;->readInt()I

    move-result v2

    .line 575
    const/4 v3, 0x4

    if-eq v2, v3, :cond_9

    .line 576
    const/4 v1, 0x3

    if-ne v2, v1, :cond_2

    .line 578
    const/4 v1, 0x1

    move v8, v1

    .line 584
    :goto_1
    invoke-virtual {v6}, Ljava/io/DataInputStream;->readInt()I

    move-result v10

    .line 585
    invoke-virtual {v6}, Ljava/io/DataInputStream;->readInt()I

    move-result v11

    .line 586
    invoke-virtual {v6}, Ljava/io/DataInputStream;->readInt()I

    move-result v12

    .line 587
    if-ltz v10, :cond_1

    if-ge v11, v10, :cond_1

    if-lt v12, v10, :cond_3

    .line 588
    :cond_1
    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0}, Ljava/io/IOException;-><init>()V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 601
    :catchall_0
    move-exception v0

    move-object v1, v6

    :goto_2
    invoke-static {v1}, Lorg/chromium/chrome/browser/util/StreamUtil;->closeQuietly(Ljava/io/Closeable;)V

    .line 602
    invoke-static {v9}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    throw v0

    .line 580
    :cond_2
    invoke-static {v6}, Lorg/chromium/chrome/browser/util/StreamUtil;->closeQuietly(Ljava/io/Closeable;)V

    .line 602
    invoke-static {v9}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    const/4 v0, 0x0

    goto :goto_0

    .line 591
    :cond_3
    const/4 v1, 0x0

    :goto_3
    if-ge v1, v10, :cond_7

    .line 592
    :try_start_3
    invoke-virtual {v6}, Ljava/io/DataInputStream;->readInt()I

    move-result v2

    .line 593
    if-eqz v8, :cond_4

    const-string/jumbo v3, ""

    .line 594
    :goto_4
    if-lt v2, v0, :cond_8

    add-int/lit8 v0, v2, 0x1

    move v7, v0

    .line 596
    :goto_5
    if-ne v1, v12, :cond_5

    const/4 v4, 0x1

    :goto_6
    if-ne v1, v11, :cond_6

    const/4 v5, 0x1

    :goto_7
    move-object v0, p1

    invoke-interface/range {v0 .. v5}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$OnTabStateReadCallback;->onStateRead(IILjava/lang/String;ZZ)V

    .line 591
    add-int/lit8 v1, v1, 0x1

    move v0, v7

    goto :goto_3

    .line 593
    :cond_4
    invoke-virtual {v6}, Ljava/io/DataInputStream;->readUTF()Ljava/lang/String;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v3

    goto :goto_4

    .line 596
    :cond_5
    const/4 v4, 0x0

    goto :goto_6

    :cond_6
    const/4 v5, 0x0

    goto :goto_7

    .line 601
    :cond_7
    invoke-static {v6}, Lorg/chromium/chrome/browser/util/StreamUtil;->closeQuietly(Ljava/io/Closeable;)V

    .line 602
    invoke-static {v9}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    goto :goto_0

    .line 601
    :catchall_1
    move-exception v0

    goto :goto_2

    :cond_8
    move v7, v0

    goto :goto_5

    :cond_9
    move v8, v1

    goto :goto_1
.end method

.method private readTabState(I)Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v0, 0x0

    .line 837
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mContext:Landroid/content/Context;

    iget v4, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mSelectorIndex:I

    invoke-static {v1, v4}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->getStateFolder(Landroid/content/Context;I)Ljava/io/File;

    move-result-object v4

    .line 842
    :try_start_0
    new-instance v1, Ljava/io/File;

    const/4 v5, 0x0

    invoke-static {p1, v5}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->getTabStateFilename(IZ)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v1, v4, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 845
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v5

    if-nez v5, :cond_1

    .line 847
    new-instance v1, Ljava/io/File;

    const/4 v3, 0x1

    invoke-static {p1, v3}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->getTabStateFilename(IZ)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v4, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    move v3, v2

    move-object v2, v1

    .line 851
    :goto_0
    invoke-virtual {v2}, Ljava/io/File;->exists()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-nez v1, :cond_0

    .line 857
    invoke-static {v0}, Lorg/chromium/chrome/browser/util/StreamUtil;->closeQuietly(Ljava/io/Closeable;)V

    :goto_1
    return-object v0

    .line 854
    :cond_0
    :try_start_1
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 855
    :try_start_2
    invoke-static {v1, v3}, Lcom/google/android/apps/chrome/tab/ChromeTab;->readState(Ljava/io/FileInputStream;Z)Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v0

    .line 857
    invoke-static {v1}, Lorg/chromium/chrome/browser/util/StreamUtil;->closeQuietly(Ljava/io/Closeable;)V

    goto :goto_1

    :catchall_0
    move-exception v1

    move-object v6, v1

    move-object v1, v0

    move-object v0, v6

    :goto_2
    invoke-static {v1}, Lorg/chromium/chrome/browser/util/StreamUtil;->closeQuietly(Ljava/io/Closeable;)V

    throw v0

    :catchall_1
    move-exception v0

    goto :goto_2

    :cond_1
    move-object v2, v1

    goto :goto_0
.end method

.method private restoreTab(Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$TabRestoreDetails;Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;Z)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 340
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    iget-boolean v1, p2, Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;->isIncognito:Z

    invoke-interface {v0, v1}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->getModel(Z)Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v3

    .line 341
    iget-boolean v0, p2, Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;->isIncognito:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mIncognitoTabsRestored:Landroid/util/SparseIntArray;

    .line 344
    :goto_0
    invoke-virtual {v0}, Landroid/util/SparseIntArray;->size()I

    move-result v1

    if-lez v1, :cond_2

    iget v1, p1, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$TabRestoreDetails;->originalIndex:I

    invoke-virtual {v0}, Landroid/util/SparseIntArray;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v0, v4}, Landroid/util/SparseIntArray;->keyAt(I)I

    move-result v4

    if-le v1, v4, :cond_2

    .line 347
    invoke-virtual {v0}, Landroid/util/SparseIntArray;->size()I

    move-result v1

    .line 358
    :goto_1
    iget-object v2, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mTabCreatorManager:Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreatorManager;

    iget-boolean v4, p2, Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;->isIncognito:Z

    invoke-interface {v2, v4}, Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreatorManager;->getTabCreator(Z)Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;

    move-result-object v2

    iget v4, p1, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$TabRestoreDetails;->id:I

    invoke-virtual {v2, p2, v4, v1}, Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;->createFrozenTab(Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;II)V

    .line 360
    if-eqz p3, :cond_0

    .line 361
    iget v1, p1, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$TabRestoreDetails;->id:I

    invoke-static {v3, v1}, Lorg/chromium/chrome/browser/tabmodel/TabModelUtils;->getTabIndexById(Lorg/chromium/chrome/browser/tabmodel/TabList;I)I

    move-result v1

    invoke-static {v3, v1}, Lorg/chromium/chrome/browser/tabmodel/TabModelUtils;->setIndex(Lorg/chromium/chrome/browser/tabmodel/TabModel;I)V

    .line 363
    :cond_0
    iget v1, p1, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$TabRestoreDetails;->originalIndex:I

    iget v2, p1, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$TabRestoreDetails;->id:I

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 364
    return-void

    .line 341
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mNormalTabsRestored:Landroid/util/SparseIntArray;

    goto :goto_0

    :cond_2
    move v1, v2

    .line 350
    :goto_2
    invoke-virtual {v0}, Landroid/util/SparseIntArray;->size()I

    move-result v4

    if-ge v1, v4, :cond_5

    .line 351
    invoke-virtual {v0, v1}, Landroid/util/SparseIntArray;->keyAt(I)I

    move-result v4

    iget v5, p1, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$TabRestoreDetails;->originalIndex:I

    if-le v4, v5, :cond_4

    .line 352
    invoke-virtual {v0, v1}, Landroid/util/SparseIntArray;->valueAt(I)I

    move-result v1

    invoke-static {v3, v1}, Lorg/chromium/chrome/browser/tabmodel/TabModelUtils;->getTabById(Lorg/chromium/chrome/browser/tabmodel/TabList;I)Lorg/chromium/chrome/browser/Tab;

    move-result-object v1

    .line 353
    if-eqz v1, :cond_3

    invoke-interface {v3, v1}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->indexOf(Lorg/chromium/chrome/browser/Tab;)I

    move-result v1

    goto :goto_1

    :cond_3
    const/4 v1, -0x1

    goto :goto_1

    .line 350
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_5
    move v1, v2

    goto :goto_1
.end method

.method private restoreTab(Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$TabRestoreDetails;Z)Z
    .locals 6

    .prologue
    .line 319
    const/4 v0, 0x0

    .line 320
    invoke-static {}, Landroid/os/StrictMode;->allowThreadDiskReads()Landroid/os/StrictMode$ThreadPolicy;

    move-result-object v2

    .line 322
    :try_start_0
    iget v1, p1, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$TabRestoreDetails;->id:I

    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->readTabState(I)Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;

    move-result-object v1

    .line 324
    if-eqz v1, :cond_0

    .line 325
    invoke-direct {p0, p1, v1, p2}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->restoreTab(Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$TabRestoreDetails;Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 326
    const/4 v0, 0x1

    .line 333
    :cond_0
    invoke-static {v2}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    .line 335
    :goto_0
    return v0

    .line 328
    :catch_0
    move-exception v1

    .line 331
    :try_start_1
    const-string/jumbo v3, "TabPersistentStore"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "loadTabs exception: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 333
    invoke-static {v2}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {v2}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    throw v0
.end method

.method private declared-synchronized saveListToFile([B)V
    .locals 3

    .prologue
    .line 507
    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mContext:Landroid/content/Context;

    iget v2, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mSelectorIndex:I

    invoke-static {v1, v2}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->getStateFolder(Landroid/content/Context;I)Ljava/io/File;

    move-result-object v1

    const-string/jumbo v2, "tab_state"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    .line 509
    invoke-static {v0, p1}, Lorg/chromium/base/ImportantFileWriterAndroid;->writeFileAtomically(Ljava/lang/String;[B)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 510
    monitor-exit p0

    return-void

    .line 507
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private saveNextTab()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 607
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mSaveTabTask:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$SaveTabTask;

    if-eqz v0, :cond_0

    .line 616
    :goto_0
    return-void

    .line 608
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mTabsToSave:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 609
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mTabsToSave:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->removeFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/tab/ChromeTab;

    .line 610
    new-instance v1, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$SaveTabTask;

    invoke-direct {v1, p0, v0}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$SaveTabTask;-><init>(Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;Lcom/google/android/apps/chrome/tab/ChromeTab;)V

    iput-object v1, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mSaveTabTask:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$SaveTabTask;

    .line 611
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mSaveTabTask:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$SaveTabTask;

    sget-object v1, Landroid/os/AsyncTask;->SERIAL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$SaveTabTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    .line 613
    :cond_1
    new-instance v0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$SaveListTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$SaveListTask;-><init>(Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$1;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mSaveListTask:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$SaveListTask;

    .line 614
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mSaveListTask:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$SaveListTask;

    sget-object v1, Landroid/os/AsyncTask;->SERIAL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$SaveListTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method public static serializeTabLists(Lorg/chromium/chrome/browser/tabmodel/TabList;Lorg/chromium/chrome/browser/tabmodel/TabList;)[B
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 483
    invoke-static {}, Lorg/chromium/base/ThreadUtils;->assertOnUiThread()V

    .line 485
    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 486
    new-instance v3, Ljava/io/DataOutputStream;

    invoke-direct {v3, v2}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 487
    const/4 v0, 0x4

    invoke-virtual {v3, v0}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 488
    invoke-interface {p0}, Lorg/chromium/chrome/browser/tabmodel/TabList;->getCount()I

    move-result v0

    invoke-interface {p1}, Lorg/chromium/chrome/browser/tabmodel/TabList;->getCount()I

    move-result v4

    add-int/2addr v0, v4

    invoke-virtual {v3, v0}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 489
    invoke-interface {p0}, Lorg/chromium/chrome/browser/tabmodel/TabList;->index()I

    move-result v0

    invoke-virtual {v3, v0}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 490
    invoke-interface {p1}, Lorg/chromium/chrome/browser/tabmodel/TabList;->index()I

    move-result v0

    invoke-interface {p0}, Lorg/chromium/chrome/browser/tabmodel/TabList;->getCount()I

    move-result v4

    add-int/2addr v0, v4

    invoke-virtual {v3, v0}, Ljava/io/DataOutputStream;->writeInt(I)V

    move v0, v1

    .line 493
    :goto_0
    invoke-interface {p0}, Lorg/chromium/chrome/browser/tabmodel/TabList;->getCount()I

    move-result v4

    if-ge v0, v4, :cond_0

    .line 494
    invoke-interface {p0, v0}, Lorg/chromium/chrome/browser/tabmodel/TabList;->getTabAt(I)Lorg/chromium/chrome/browser/Tab;

    move-result-object v4

    invoke-virtual {v4}, Lorg/chromium/chrome/browser/Tab;->getId()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 495
    invoke-interface {p0, v0}, Lorg/chromium/chrome/browser/tabmodel/TabList;->getTabAt(I)Lorg/chromium/chrome/browser/Tab;

    move-result-object v4

    invoke-virtual {v4}, Lorg/chromium/chrome/browser/Tab;->getUrl()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    .line 493
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 497
    :cond_0
    :goto_1
    invoke-interface {p1}, Lorg/chromium/chrome/browser/tabmodel/TabList;->getCount()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 498
    invoke-interface {p1, v1}, Lorg/chromium/chrome/browser/tabmodel/TabList;->getTabAt(I)Lorg/chromium/chrome/browser/Tab;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/Tab;->getId()I

    move-result v0

    invoke-virtual {v3, v0}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 499
    invoke-interface {p1, v1}, Lorg/chromium/chrome/browser/tabmodel/TabList;->getTabAt(I)Lorg/chromium/chrome/browser/Tab;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/Tab;->getUrl()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    .line 497
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 501
    :cond_1
    invoke-virtual {v3}, Ljava/io/DataOutputStream;->close()V

    .line 502
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    return-object v0
.end method

.method public static serializeTabModelSelector(Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;)[B
    .locals 2

    .prologue
    .line 469
    const/4 v0, 0x1

    invoke-interface {p0, v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->getModel(Z)Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v0

    .line 470
    const/4 v1, 0x0

    invoke-interface {p0, v1}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->getModel(Z)Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v1

    .line 471
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->serializeTabLists(Lorg/chromium/chrome/browser/tabmodel/TabList;Lorg/chromium/chrome/browser/tabmodel/TabList;)[B

    move-result-object v0

    return-object v0
.end method

.method public static waitForMigrationToFinish()V
    .locals 2

    .prologue
    .line 141
    sget-boolean v0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->sMigrationTask:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$FileMigrationTask;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    const-string/jumbo v1, "The migration should be initialized by now."

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 142
    :cond_0
    sget-object v0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->sMigrationTask:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$FileMigrationTask;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$FileMigrationTask;->get()Ljava/lang/Object;

    .line 143
    return-void
.end method


# virtual methods
.method public addTabToSaveQueue(Lcom/google/android/apps/chrome/tab/ChromeTab;)V
    .locals 1

    .prologue
    .line 405
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mTabsToSave:Ljava/util/Deque;

    invoke-interface {v0, p1}, Ljava/util/Deque;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->shouldTabStateBePersisted()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mTabsToSave:Ljava/util/Deque;

    invoke-interface {v0, p1}, Ljava/util/Deque;->addLast(Ljava/lang/Object;)V

    .line 406
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->saveNextTab()V

    .line 407
    return-void
.end method

.method public cancelLoadingTabs(Z)V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 397
    if-eqz p1, :cond_0

    .line 398
    iput-boolean v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mCancelIncognitoTabLoads:Z

    .line 402
    :goto_0
    return-void

    .line 400
    :cond_0
    iput-boolean v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mCancelNormalTabLoads:Z

    goto :goto_0
.end method

.method public clearEncryptedState()V
    .locals 0

    .prologue
    .line 384
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->cleanupAllEncryptedPersistentData()V

    .line 385
    return-void
.end method

.method public clearState()V
    .locals 1

    .prologue
    .line 374
    const-string/jumbo v0, "tab_state"

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->deleteFileAsync(Ljava/lang/String;)V

    .line 375
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->cleanupPersistentData()V

    .line 376
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->onStateLoaded()V

    .line 377
    return-void
.end method

.method public destroy()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 447
    iput-boolean v2, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mDestroyed:Z

    .line 448
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mLoadTabTask:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$LoadTabTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mLoadTabTask:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$LoadTabTask;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$LoadTabTask;->cancel(Z)Z

    .line 449
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mTabsToSave:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->clear()V

    .line 450
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mTabsToRestore:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->clear()V

    .line 451
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mSaveTabTask:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$SaveTabTask;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mSaveTabTask:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$SaveTabTask;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$SaveTabTask;->cancel(Z)Z

    .line 452
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mSaveListTask:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$SaveListTask;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mSaveListTask:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$SaveListTask;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$SaveListTask;->cancel(Z)Z

    .line 453
    :cond_2
    return-void
.end method

.method public getRestoredTabCount()I
    .locals 1

    .prologue
    .line 370
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mTabsToRestore:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->size()I

    move-result v0

    return v0
.end method

.method public loadState()I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 235
    :try_start_0
    invoke-static {}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->waitForMigrationToFinish()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_2

    .line 242
    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mCancelNormalTabLoads:Z

    .line 243
    iput-boolean v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mCancelIncognitoTabLoads:Z

    .line 244
    new-instance v1, Landroid/util/SparseIntArray;

    invoke-direct {v1}, Landroid/util/SparseIntArray;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mNormalTabsRestored:Landroid/util/SparseIntArray;

    .line 245
    new-instance v1, Landroid/util/SparseIntArray;

    invoke-direct {v1}, Landroid/util/SparseIntArray;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mIncognitoTabsRestored:Landroid/util/SparseIntArray;

    .line 248
    :try_start_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->loadStateInternal()I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result v0

    .line 256
    :goto_1
    const-string/jumbo v1, "tab_state"

    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->deleteFileAsync(Ljava/lang/String;)V

    .line 261
    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->cleanupPersistentDataAtAndAboveId(I)V

    .line 263
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mTabsToRestore:Ljava/util/Deque;

    invoke-interface {v1}, Ljava/util/Deque;->size()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->tabCountAtStartup(I)V

    .line 264
    return v0

    .line 249
    :catch_0
    move-exception v1

    .line 252
    const-string/jumbo v2, "TabPersistentStore"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "loadState exception: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 240
    :catch_1
    move-exception v1

    goto :goto_0

    :catch_2
    move-exception v1

    goto :goto_0
.end method

.method public loadStateInternal()I
    .locals 9

    .prologue
    const/4 v0, 0x0

    .line 520
    sget-boolean v1, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->getModel(Z)Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v1

    invoke-interface {v1}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->getCount()I

    move-result v1

    if-eqz v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 521
    :cond_0
    sget-boolean v1, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->$assertionsDisabled:Z

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    invoke-interface {v1, v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->getModel(Z)Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v1

    invoke-interface {v1}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->getCount()I

    move-result v1

    if-eqz v1, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 523
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mContext:Landroid/content/Context;

    const-string/jumbo v2, "tabs"

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v3

    .line 524
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mContext:Landroid/content/Context;

    iget v2, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mSelectorIndex:I

    invoke-static {v1, v2}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->getStateFolder(Landroid/content/Context;I)Ljava/io/File;

    move-result-object v4

    .line 525
    array-length v5, v3

    move v2, v0

    move v1, v0

    :goto_0
    if-ge v2, v5, :cond_4

    aget-object v6, v3, v2

    .line 526
    sget-boolean v0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    invoke-virtual {v6}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 527
    :cond_2
    invoke-virtual {v6}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 528
    invoke-virtual {v6, v4}, Ljava/io/File;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 529
    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mTabsToRestore:Ljava/util/Deque;

    .line 530
    :goto_1
    iget-object v7, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    invoke-interface {v7}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->isIncognitoSelected()Z

    move-result v7

    .line 532
    new-instance v8, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$1;

    invoke-direct {v8, p0, v0, v7}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$1;-><init>(Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;Ljava/util/Deque;Z)V

    invoke-static {v6, v8}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->readSavedStateFile(Ljava/io/File;Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$OnTabStateReadCallback;)I

    move-result v0

    .line 552
    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 525
    :goto_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_0

    .line 529
    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    .line 555
    :cond_4
    return v1

    :cond_5
    move v0, v1

    goto :goto_2
.end method

.method public removeTabFromQueues(Lorg/chromium/chrome/browser/Tab;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 410
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mTabsToSave:Ljava/util/Deque;

    invoke-interface {v0, p1}, Ljava/util/Deque;->remove(Ljava/lang/Object;)Z

    .line 411
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mTabsToRestore:Ljava/util/Deque;

    invoke-virtual {p1}, Lorg/chromium/chrome/browser/Tab;->getId()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->getTabToRestoreById(I)Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$TabRestoreDetails;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Deque;->remove(Ljava/lang/Object;)Z

    .line 413
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mLoadTabTask:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$LoadTabTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mLoadTabTask:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$LoadTabTask;

    iget-object v0, v0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$LoadTabTask;->mTabToRestore:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$TabRestoreDetails;

    iget v0, v0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$TabRestoreDetails;->id:I

    invoke-virtual {p1}, Lorg/chromium/chrome/browser/Tab;->getId()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 414
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mLoadTabTask:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$LoadTabTask;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$LoadTabTask;->cancel(Z)Z

    .line 415
    iput-object v3, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mLoadTabTask:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$LoadTabTask;

    .line 416
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->loadNextTab()V

    .line 419
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mSaveTabTask:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$SaveTabTask;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mSaveTabTask:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$SaveTabTask;

    iget v0, v0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$SaveTabTask;->mId:I

    invoke-virtual {p1}, Lorg/chromium/chrome/browser/Tab;->getId()I

    move-result v1

    if-ne v0, v1, :cond_1

    .line 420
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mSaveTabTask:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$SaveTabTask;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$SaveTabTask;->cancel(Z)Z

    .line 421
    iput-object v3, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mSaveTabTask:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$SaveTabTask;

    .line 422
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->saveNextTab()V

    .line 425
    :cond_1
    invoke-virtual {p1}, Lorg/chromium/chrome/browser/Tab;->getId()I

    move-result v0

    invoke-virtual {p1}, Lorg/chromium/chrome/browser/Tab;->isIncognito()Z

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->cleanupPersistentData(IZ)V

    .line 426
    return-void
.end method

.method public restoreTabState(Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 297
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mLoadTabTask:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$LoadTabTask;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mLoadTabTask:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$LoadTabTask;

    iget-object v1, v1, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$LoadTabTask;->mTabToRestore:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$TabRestoreDetails;

    iget-object v1, v1, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$TabRestoreDetails;->url:Ljava/lang/String;

    invoke-static {v1, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 300
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mLoadTabTask:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$LoadTabTask;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$LoadTabTask;->cancel(Z)Z

    .line 301
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mLoadTabTask:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$LoadTabTask;

    iget-object v1, v1, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$LoadTabTask;->mTabToRestore:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$TabRestoreDetails;

    .line 302
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->loadNextTab()V

    .line 306
    :goto_0
    if-eqz v1, :cond_0

    .line 307
    iget-object v2, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mTabsToRestore:Ljava/util/Deque;

    invoke-interface {v2, v1}, Ljava/util/Deque;->remove(Ljava/lang/Object;)Z

    .line 308
    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->restoreTab(Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$TabRestoreDetails;Z)Z

    move-result v0

    .line 310
    :cond_0
    return v0

    .line 304
    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->getTabToRestoreByUrl(Ljava/lang/String;)Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$TabRestoreDetails;

    move-result-object v1

    goto :goto_0
.end method

.method public restoreTabs(Z)V
    .locals 2

    .prologue
    .line 276
    if-eqz p1, :cond_0

    .line 282
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mTabsToRestore:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mNormalTabsRestored:Landroid/util/SparseIntArray;

    invoke-virtual {v0}, Landroid/util/SparseIntArray;->size()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mIncognitoTabsRestored:Landroid/util/SparseIntArray;

    invoke-virtual {v0}, Landroid/util/SparseIntArray;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 283
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mTabsToRestore:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->removeFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$TabRestoreDetails;

    .line 284
    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->restoreTab(Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$TabRestoreDetails;Z)Z

    goto :goto_0

    .line 287
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->loadNextTab()V

    .line 288
    return-void
.end method

.method public saveState()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 171
    invoke-static {}, Landroid/os/StrictMode;->allowThreadDiskWrites()Landroid/os/StrictMode$ThreadPolicy;

    move-result-object v1

    .line 173
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    invoke-interface {v0, v6}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->getModel(Z)Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelUtils;->getCurrentTab(Lorg/chromium/chrome/browser/tabmodel/TabList;)Lorg/chromium/chrome/browser/Tab;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->fromTab(Lorg/chromium/chrome/browser/Tab;)Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    .line 175
    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mTabsToSave:Ljava/util/Deque;

    invoke-interface {v2, v0}, Ljava/util/Deque;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->shouldTabStateBePersisted()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 177
    iget-object v2, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mTabsToSave:Ljava/util/Deque;

    invoke-interface {v2, v0}, Ljava/util/Deque;->addLast(Ljava/lang/Object;)V

    .line 179
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    const/4 v2, 0x1

    invoke-interface {v0, v2}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->getModel(Z)Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelUtils;->getCurrentTab(Lorg/chromium/chrome/browser/tabmodel/TabList;)Lorg/chromium/chrome/browser/Tab;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->fromTab(Lorg/chromium/chrome/browser/Tab;)Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    .line 181
    if-eqz v0, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mTabsToSave:Ljava/util/Deque;

    invoke-interface {v2, v0}, Ljava/util/Deque;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->shouldTabStateBePersisted()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 183
    iget-object v2, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mTabsToSave:Ljava/util/Deque;

    invoke-interface {v2, v0}, Ljava/util/Deque;->addLast(Ljava/lang/Object;)V

    .line 186
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mSaveTabTask:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$SaveTabTask;

    if-eqz v0, :cond_3

    .line 190
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mSaveTabTask:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$SaveTabTask;

    invoke-virtual {v0, v6}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$SaveTabTask;->cancel(Z)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mSaveTabTask:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$SaveTabTask;

    iget-boolean v0, v0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$SaveTabTask;->mStateSaved:Z

    if-nez v0, :cond_2

    .line 192
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mSaveTabTask:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$SaveTabTask;

    iget-object v0, v0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$SaveTabTask;->mTab:Lcom/google/android/apps/chrome/tab/ChromeTab;

    .line 193
    iget-object v2, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mTabsToSave:Ljava/util/Deque;

    invoke-interface {v2, v0}, Ljava/util/Deque;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->shouldTabStateBePersisted()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 195
    iget-object v2, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mTabsToSave:Ljava/util/Deque;

    invoke-interface {v2, v0}, Ljava/util/Deque;->addLast(Ljava/lang/Object;)V

    .line 199
    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mSaveTabTask:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$SaveTabTask;

    .line 203
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mTabsToSave:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/tab/ChromeTab;

    .line 204
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getId()I

    move-result v3

    .line 205
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->isIncognito()Z

    move-result v4

    .line 207
    :try_start_0
    invoke-direct {p0, v3, v4}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->openTabStateOutputStream(IZ)Ljava/io/FileOutputStream;

    move-result-object v5

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getState()Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;

    move-result-object v0

    invoke-static {v5, v0, v4}, Lcom/google/android/apps/chrome/tab/ChromeTab;->saveState(Ljava/io/FileOutputStream;Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;Z)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 209
    :catch_0
    move-exception v0

    .line 210
    invoke-static {v0}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->logSaveException(Ljava/lang/Exception;)V

    goto :goto_0

    .line 212
    :catch_1
    move-exception v0

    const-string/jumbo v0, "TabPersistentStore"

    const-string/jumbo v5, "Out of memory error while attempting to save tab state.  Erasing."

    invoke-static {v0, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 213
    invoke-direct {p0, v3, v4}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->deleteTabStateFile(IZ)V

    goto :goto_0

    .line 216
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mTabsToSave:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->clear()V

    .line 218
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mSaveListTask:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$SaveListTask;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mSaveListTask:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$SaveListTask;

    invoke-virtual {v0, v6}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$SaveListTask;->cancel(Z)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mSaveListTask:Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$SaveListTask;

    iget-boolean v0, v0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$SaveListTask;->mStateSaved:Z

    if-nez v0, :cond_6

    .line 220
    :cond_5
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    invoke-static {v0}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->serializeTabModelSelector(Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;)[B

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->saveListToFile([B)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 225
    :cond_6
    :goto_1
    invoke-static {v1}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    .line 226
    return-void

    .line 221
    :catch_2
    move-exception v0

    .line 222
    invoke-static {v0}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->logSaveException(Ljava/lang/Exception;)V

    goto :goto_1
.end method

.method public setTabContentManager(Lcom/google/android/apps/chrome/compositor/TabContentManager;)V
    .locals 0

    .prologue
    .line 166
    iput-object p1, p0, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->mTabContentManager:Lcom/google/android/apps/chrome/compositor/TabContentManager;

    .line 167
    return-void
.end method
