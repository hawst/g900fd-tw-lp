.class public Lcom/google/android/apps/chrome/ChromeTabbedActivity;
.super Lcom/google/android/apps/chrome/CompositorChromeActivity;
.source "ChromeTabbedActivity.java"

# interfaces
.implements Landroid/view/accessibility/AccessibilityManager$AccessibilityStateChangeListener;
.implements Lcom/google/android/apps/chrome/ContextualMenuBar$ActionBarDelegate;
.implements Lcom/google/android/apps/chrome/sync/ui/SyncChooseAccountFragment$Listener;


# static fields
.field public static final INTENT_EXTRA_DISABLE_CRASH_DUMP_UPLOADING:Ljava/lang/String; = "disable_crash_dump_uploading"

.field public static final INTENT_EXTRA_TEST_RENDER_PROCESS_LIMIT:Ljava/lang/String; = "render_process_limit"

.field private static final NOTIFICATIONS:[I


# instance fields
.field private mAppMenuHandler:Lorg/chromium/chrome/browser/appmenu/AppMenuHandler;

.field private mChromeAppMenuPropertiesDelegate:Lcom/google/android/apps/chrome/ChromeAppMenuPropertiesDelegate;

.field private mConnectionChangeReceiver:Lcom/google/android/apps/chrome/preferences/ConnectionChangeReceiver;

.field private mContentContainer:Landroid/view/ViewGroup;

.field private mContextualMenuBar:Lcom/google/android/apps/chrome/ContextualMenuBar;

.field private mControlContainer:Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;

.field private mCreatedTabOnStartup:Z

.field private final mCurrentLocale:Ljava/util/Locale;

.field private mFindToolbarWrapper:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarWrapper;

.field private mFirstDrawListener:Landroid/view/ViewTreeObserver$OnPreDrawListener;

.field private mInflateInitialLayoutDurationMs:J

.field private mIntentWithEffect:Z

.field private mIsOnFirstRun:Z

.field private mMemoryUma:Lcom/google/android/apps/chrome/uma/MemoryUma;

.field private mMenuAnchor:Landroid/view/View;

.field private final mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

.field private mOverviewBehavior:Lcom/google/android/apps/chrome/OverviewBehavior;

.field private mShowTabStack:Z

.field private mShowingOldTabStrip:Z

.field private mTabModelSelectorImpl:Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;

.field private mToolbar:Lcom/google/android/apps/chrome/toolbar/Toolbar;

.field private mToolbarManager:Lcom/google/android/apps/chrome/toolbar/ToolbarManager;

.field private mUIInitialized:Z

.field private mUndoBarPopupController:Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupController;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 943
    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->NOTIFICATIONS:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x2
        0x5
        0x22
        0x48
        0x4a
    .end array-data
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 121
    invoke-direct {p0}, Lcom/google/android/apps/chrome/CompositorChromeActivity;-><init>()V

    .line 189
    iput-boolean v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mShowingOldTabStrip:Z

    .line 190
    iput-boolean v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mShowTabStack:Z

    .line 194
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mContextualMenuBar:Lcom/google/android/apps/chrome/ContextualMenuBar;

    .line 200
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mUIInitialized:Z

    .line 202
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mIsOnFirstRun:Z

    .line 207
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mCreatedTabOnStartup:Z

    .line 210
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mIntentWithEffect:Z

    .line 218
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mCurrentLocale:Ljava/util/Locale;

    .line 951
    new-instance v0, Lcom/google/android/apps/chrome/ChromeTabbedActivity$10;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity$10;-><init>(Lcom/google/android/apps/chrome/ChromeTabbedActivity;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    .line 1552
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/ChromeTabbedActivity;)V
    .locals 0

    .prologue
    .line 121
    invoke-direct {p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->toggleOverview()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/ChromeTabbedActivity;)Lcom/google/android/apps/chrome/ChromeAppMenuPropertiesDelegate;
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mChromeAppMenuPropertiesDelegate:Lcom/google/android/apps/chrome/ChromeAppMenuPropertiesDelegate;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/google/android/apps/chrome/ChromeTabbedActivity;)V
    .locals 0

    .prologue
    .line 121
    invoke-direct {p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->recordOmniboxInteractionStats()V

    return-void
.end method

.method static synthetic access$1100(Lcom/google/android/apps/chrome/ChromeTabbedActivity;)Lcom/google/android/apps/chrome/ContextualMenuBar;
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mContextualMenuBar:Lcom/google/android/apps/chrome/ContextualMenuBar;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/ChromeTabbedActivity;)V
    .locals 0

    .prologue
    .line 121
    invoke-direct {p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->createInitialTab()V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/apps/chrome/ChromeTabbedActivity;)Z
    .locals 1

    .prologue
    .line 121
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mUIInitialized:Z

    return v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/chrome/ChromeTabbedActivity;)Lcom/google/android/apps/chrome/OverviewBehavior;
    .locals 1

    .prologue
    .line 121
    invoke-direct {p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getAndSetupOverviewLayout()Lcom/google/android/apps/chrome/OverviewBehavior;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/chrome/ChromeTabbedActivity;)Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mTabModelSelectorImpl:Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/chrome/ChromeTabbedActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLandroid/content/Intent;)V
    .locals 0

    .prologue
    .line 121
    invoke-direct/range {p0 .. p6}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->launchIntent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLandroid/content/Intent;)V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/apps/chrome/ChromeTabbedActivity;)Lcom/google/android/apps/chrome/toolbar/Toolbar;
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mToolbar:Lcom/google/android/apps/chrome/toolbar/Toolbar;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/apps/chrome/ChromeTabbedActivity;)Landroid/view/ViewTreeObserver$OnPreDrawListener;
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mFirstDrawListener:Landroid/view/ViewTreeObserver$OnPreDrawListener;

    return-object v0
.end method

.method static synthetic access$802(Lcom/google/android/apps/chrome/ChromeTabbedActivity;Landroid/view/ViewTreeObserver$OnPreDrawListener;)Landroid/view/ViewTreeObserver$OnPreDrawListener;
    .locals 0

    .prologue
    .line 121
    iput-object p1, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mFirstDrawListener:Landroid/view/ViewTreeObserver$OnPreDrawListener;

    return-object p1
.end method

.method static synthetic access$900(Lcom/google/android/apps/chrome/ChromeTabbedActivity;)Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mControlContainer:Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;

    return-object v0
.end method

.method private createInitialTab()V
    .locals 4

    .prologue
    .line 593
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/partnercustomizations/HomepageManager;->getHomepageUri(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 594
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string/jumbo v0, "chrome-native://newtab/"

    .line 595
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getTabCreator(Z)Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;

    move-result-object v1

    new-instance v2, Lorg/chromium/content_public/browser/LoadUrlParams;

    invoke-direct {v2, v0}, Lorg/chromium/content_public/browser/LoadUrlParams;-><init>(Ljava/lang/String;)V

    sget-object v0, Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;->FROM_MENU_OR_OVERVIEW:Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v0, v3}, Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;->createNewTab(Lorg/chromium/content_public/browser/LoadUrlParams;Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;Lorg/chromium/chrome/browser/Tab;)Lcom/google/android/apps/chrome/tab/ChromeTab;

    .line 597
    return-void
.end method

.method private createSearchEngineListener()V
    .locals 2

    .prologue
    .line 510
    new-instance v0, Lcom/google/android/apps/chrome/ChromeTabbedActivity$UpdateSearchEngineFromListener;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity$UpdateSearchEngineFromListener;-><init>(Landroid/content/Context;)V

    .line 512
    invoke-static {}, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;->getInstance()Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;

    move-result-object v1

    invoke-virtual {v1, v0}, Lorg/chromium/chrome/browser/search_engines/TemplateUrlService;->registerLoadListener(Lorg/chromium/chrome/browser/search_engines/TemplateUrlService$LoadListener;)V

    .line 513
    return-void
.end method

.method private createTabModelSelectorImpl(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 928
    if-eqz p1, :cond_1

    const-string/jumbo v1, "is_incognito_selected"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_1

    move v1, v2

    .line 930
    :goto_0
    if-eqz p1, :cond_0

    const-string/jumbo v3, "window_index"

    invoke-virtual {p1, v3, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 931
    :cond_0
    invoke-static {}, Lcom/google/android/apps/chrome/tabmodel/TabWindowManager;->getInstance()Lcom/google/android/apps/chrome/tabmodel/TabWindowManager;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getWindowAndroid()Lorg/chromium/ui/base/WindowAndroid;

    move-result-object v4

    invoke-virtual {v3, p0, v4, v0}, Lcom/google/android/apps/chrome/tabmodel/TabWindowManager;->requestSelector(Lcom/google/android/apps/chrome/ChromeActivity;Lorg/chromium/ui/base/WindowAndroid;I)Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;

    iput-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mTabModelSelectorImpl:Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;

    .line 933
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mTabModelSelectorImpl:Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;

    if-nez v0, :cond_2

    .line 934
    sget v0, Lcom/google/android/apps/chrome/R$string;->unsupported_number_of_windows:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 936
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->finish()V

    .line 941
    :goto_1
    return-void

    :cond_1
    move v1, v0

    .line 928
    goto :goto_0

    .line 939
    :cond_2
    if-eqz v1, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mTabModelSelectorImpl:Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->selectModel(Z)V

    .line 940
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mTabModelSelectorImpl:Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->setTabModelSelector(Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;)V

    goto :goto_1
.end method

.method private getAndSetupOverviewLayout()Lcom/google/android/apps/chrome/OverviewBehavior;
    .locals 2

    .prologue
    .line 1531
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mOverviewBehavior:Lcom/google/android/apps/chrome/OverviewBehavior;

    if-eqz v0, :cond_0

    .line 1532
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mOverviewBehavior:Lcom/google/android/apps/chrome/OverviewBehavior;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/chrome/device/DeviceClassManager;->enableAnimations(Landroid/content/Context;)Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/chrome/OverviewBehavior;->setEnableAnimations(Z)V

    .line 1535
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mOverviewBehavior:Lcom/google/android/apps/chrome/OverviewBehavior;

    return-object v0
.end method

.method private getConnectionChangeReceiver()Lcom/google/android/apps/chrome/preferences/ConnectionChangeReceiver;
    .locals 1

    .prologue
    .line 1472
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mConnectionChangeReceiver:Lcom/google/android/apps/chrome/preferences/ConnectionChangeReceiver;

    if-nez v0, :cond_0

    .line 1473
    new-instance v0, Lcom/google/android/apps/chrome/preferences/ConnectionChangeReceiver;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/preferences/ConnectionChangeReceiver;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mConnectionChangeReceiver:Lcom/google/android/apps/chrome/preferences/ConnectionChangeReceiver;

    .line 1475
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mConnectionChangeReceiver:Lcom/google/android/apps/chrome/preferences/ConnectionChangeReceiver;

    return-object v0
.end method

.method private handleDebugIntent(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 363
    const-string/jumbo v0, "com.google.android.apps.chrome.ACTION_CLOSE_TABS"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 364
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getTabModelSelector()Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    move-result-object v0

    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->closeAllTabs()V

    .line 369
    :goto_0
    return-void

    .line 365
    :cond_0
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lorg/chromium/base/MemoryPressureListener;->handleDebugIntent(Landroid/app/Activity;Ljava/lang/String;)Z

    goto :goto_0
.end method

.method private hideMenus()V
    .locals 1

    .prologue
    .line 1456
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mAppMenuHandler:Lorg/chromium/chrome/browser/appmenu/AppMenuHandler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mAppMenuHandler:Lorg/chromium/chrome/browser/appmenu/AppMenuHandler;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/appmenu/AppMenuHandler;->hideAppMenu()V

    .line 1457
    :cond_0
    return-void
.end method

.method private hideSuggestions()V
    .locals 1

    .prologue
    .line 1466
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mToolbar:Lcom/google/android/apps/chrome/toolbar/Toolbar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mToolbar:Lcom/google/android/apps/chrome/toolbar/Toolbar;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/toolbar/Toolbar;->getDelegate()Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1467
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mToolbar:Lcom/google/android/apps/chrome/toolbar/Toolbar;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/toolbar/Toolbar;->getDelegate()Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->getLocationBar()Lcom/google/android/apps/chrome/omnibox/LocationBar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->hideSuggestions()V

    .line 1469
    :cond_0
    return-void
.end method

.method private initializeUI()V
    .locals 8

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 372
    invoke-static {}, Lorg/chromium/base/TraceEvent;->begin()V

    .line 374
    invoke-static {}, Lorg/chromium/base/CommandLine;->getInstance()Lorg/chromium/base/CommandLine;

    move-result-object v0

    .line 376
    new-instance v1, Lcom/google/android/apps/chrome/ChromeAppMenuPropertiesDelegate;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/ChromeAppMenuPropertiesDelegate;-><init>(Lcom/google/android/apps/chrome/ChromeActivity;)V

    iput-object v1, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mChromeAppMenuPropertiesDelegate:Lcom/google/android/apps/chrome/ChromeAppMenuPropertiesDelegate;

    .line 377
    new-instance v1, Lorg/chromium/chrome/browser/appmenu/AppMenuHandler;

    iget-object v2, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mChromeAppMenuPropertiesDelegate:Lcom/google/android/apps/chrome/ChromeAppMenuPropertiesDelegate;

    sget v3, Lcom/google/android/apps/chrome/R$menu;->main_menu:I

    invoke-direct {v1, p0, v2, v3}, Lorg/chromium/chrome/browser/appmenu/AppMenuHandler;-><init>(Landroid/app/Activity;Lorg/chromium/chrome/browser/appmenu/AppMenuPropertiesDelegate;I)V

    iput-object v1, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mAppMenuHandler:Lorg/chromium/chrome/browser/appmenu/AppMenuHandler;

    .line 380
    const-string/jumbo v1, "enable-instant-extended-api"

    invoke-virtual {v0, v1}, Lorg/chromium/base/CommandLine;->appendSwitch(Ljava/lang/String;)V

    .line 381
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mToolbar:Lcom/google/android/apps/chrome/toolbar/Toolbar;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/toolbar/Toolbar;->getDelegate()Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;

    move-result-object v2

    .line 382
    new-instance v0, Lcom/google/android/apps/chrome/ChromeTabbedActivity$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity$2;-><init>(Lcom/google/android/apps/chrome/ChromeTabbedActivity;)V

    invoke-virtual {v2, v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->setOnOverviewClickHandler(Landroid/view/View$OnClickListener;)V

    .line 388
    new-instance v0, Lcom/google/android/apps/chrome/ChromeTabbedActivity$3;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity$3;-><init>(Lcom/google/android/apps/chrome/ChromeTabbedActivity;)V

    invoke-virtual {v2, v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->setOnNewTabClickHandler(Landroid/view/View$OnClickListener;)V

    .line 396
    new-instance v0, Lcom/google/android/apps/chrome/ChromeTabbedActivity$4;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity$4;-><init>(Lcom/google/android/apps/chrome/ChromeTabbedActivity;)V

    invoke-virtual {v2, v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->setBookmarkClickHandler(Landroid/view/View$OnClickListener;)V

    .line 402
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mContextualMenuBar:Lcom/google/android/apps/chrome/ContextualMenuBar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ContextualMenuBar;->getCustomSelectionActionModeCallback()Lcom/google/android/apps/chrome/CustomSelectionActionModeCallback;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->setDefaultActionModeCallbackForTextEdit(Lcom/google/android/apps/chrome/CustomSelectionActionModeCallback;)V

    .line 404
    invoke-virtual {v2}, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->getLocationBar()Lcom/google/android/apps/chrome/omnibox/LocationBar;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->createContextualMenuBar(Lcom/google/android/apps/chrome/ContextualMenuBar$ActionBarDelegate;)V

    .line 405
    invoke-virtual {v2}, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->getLocationBar()Lcom/google/android/apps/chrome/omnibox/LocationBar;

    move-result-object v0

    invoke-virtual {v0, v7}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->setIgnoreURLBarModification(Z)V

    .line 408
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getCompositorViewHolder()Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    move-result-object v3

    .line 409
    invoke-static {p0}, Lorg/chromium/ui/base/DeviceFormFactor;->isTablet(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 410
    invoke-static {}, Lorg/chromium/base/CommandLine;->getInstance()Lorg/chromium/base/CommandLine;

    move-result-object v0

    const-string/jumbo v1, "disable-compositor-tab-strip"

    invoke-virtual {v0, v1}, Lorg/chromium/base/CommandLine;->hasSwitch(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    move v0, v6

    .line 412
    :goto_0
    invoke-static {}, Lorg/chromium/base/CommandLine;->getInstance()Lorg/chromium/base/CommandLine;

    move-result-object v1

    const-string/jumbo v4, "enable-tablet-tab-stack"

    invoke-virtual {v1, v4}, Lorg/chromium/base/CommandLine;->hasSwitch(Ljava/lang/String;)Z

    move-result v4

    .line 414
    new-instance v1, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet;

    invoke-direct {v1, v3, v3, v0, v4}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromeTablet;-><init>(Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;Lcom/google/android/apps/chrome/eventfilter/EventFilterHost;ZZ)V

    .line 421
    :goto_1
    invoke-virtual {v2}, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->getLocationBar()Lcom/google/android/apps/chrome/omnibox/LocationBar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->getUrlBar()Lcom/google/android/apps/chrome/omnibox/UrlBar;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mContentContainer:Landroid/view/ViewGroup;

    iget-object v4, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mControlContainer:Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;

    sget v5, Lcom/google/android/apps/chrome/R$dimen;->control_container_height:I

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->initializeCompositorContent(Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;Landroid/view/View;Landroid/view/ViewGroup;Lcom/google/android/apps/chrome/widget/ControlContainer;I)V

    .line 425
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getFullscreenManager()Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 426
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$dimen;->control_container_height:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    .line 427
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mContentContainer:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 433
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mTabModelSelectorImpl:Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mTabModelSelectorImpl:Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->getCurrentTab()Lorg/chromium/chrome/browser/Tab;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 434
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mTabModelSelectorImpl:Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->getCurrentTab()Lorg/chromium/chrome/browser/Tab;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->fromTab(Lorg/chromium/chrome/browser/Tab;)Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getFullscreenManager()Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->setFullscreenManager(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/FullscreenManager;)V

    .line 438
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mAppMenuHandler:Lorg/chromium/chrome/browser/appmenu/AppMenuHandler;

    new-instance v1, Lcom/google/android/apps/chrome/ChromeTabbedActivity$5;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity$5;-><init>(Lcom/google/android/apps/chrome/ChromeTabbedActivity;)V

    invoke-virtual {v0, v1}, Lorg/chromium/chrome/browser/appmenu/AppMenuHandler;->addObserver(Lorg/chromium/chrome/browser/appmenu/AppMenuObserver;)V

    .line 447
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mToolbarManager:Lcom/google/android/apps/chrome/toolbar/ToolbarManager;

    iget-object v1, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mTabModelSelectorImpl:Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;

    iget-object v2, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mAppMenuHandler:Lorg/chromium/chrome/browser/appmenu/AppMenuHandler;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getFullscreenManager()Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->initializeWithNative(Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;Lorg/chromium/chrome/browser/appmenu/AppMenuHandler;Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;)V

    .line 452
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mToolbarManager:Lcom/google/android/apps/chrome/toolbar/ToolbarManager;

    new-instance v1, Lcom/google/android/apps/chrome/ChromeTabbedActivity$6;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity$6;-><init>(Lcom/google/android/apps/chrome/ChromeTabbedActivity;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->setMenuDelegatePhone(Lcom/google/android/apps/chrome/toolbar/ToolbarManager$MenuDelegatePhone;)V

    .line 461
    sget v0, Lcom/google/android/apps/chrome/R$id;->menu_anchor_stub:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mMenuAnchor:Landroid/view/View;

    .line 463
    new-instance v0, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarWrapper;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getTabModelSelector()Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mContextualMenuBar:Lcom/google/android/apps/chrome/ContextualMenuBar;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/ContextualMenuBar;->getCustomSelectionActionModeCallback()Lcom/google/android/apps/chrome/CustomSelectionActionModeCallback;

    move-result-object v2

    sget v4, Lcom/google/android/apps/chrome/R$id;->find_toolbar_stub:I

    sget v5, Lcom/google/android/apps/chrome/R$id;->find_toolbar_tablet_stub:I

    move-object v3, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarWrapper;-><init>(Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;Landroid/view/ActionMode$Callback;Landroid/app/Activity;II)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mFindToolbarWrapper:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarWrapper;

    .line 466
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mFindToolbarWrapper:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarWrapper;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarWrapper;->initialize()V

    .line 469
    invoke-static {}, Lcom/google/android/apps/chrome/device/DeviceClassManager;->enableToolbarSwipe()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 470
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mControlContainer:Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getLayoutManager()Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->getTopSwipeHandler()Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->setSwipeHandler(Lcom/google/android/apps/chrome/eventfilter/EdgeSwipeHandler;)V

    .line 473
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->removeWindowBackground()V

    .line 475
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mShowingOldTabStrip:Z

    if-eqz v0, :cond_3

    .line 476
    sget v0, Lcom/google/android/apps/chrome/R$id;->tabstrip_stub:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 477
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 478
    sget v0, Lcom/google/android/apps/chrome/R$id;->tabstrip:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;

    .line 479
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getFullscreenManager()Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->setFullscreenManager(Lcom/google/android/apps/chrome/widgetcontroller/fullscreen/ChromeFullscreenManager;)V

    .line 481
    iget-boolean v1, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mShowTabStack:Z

    if-nez v1, :cond_8

    .line 482
    iput-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mOverviewBehavior:Lcom/google/android/apps/chrome/OverviewBehavior;

    .line 487
    :goto_2
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getTabModelSelector()Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getTabContentManager()Lcom/google/android/apps/chrome/compositor/TabContentManager;

    move-result-object v2

    invoke-virtual {v0, v1, p0, v2}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->setTabModelSelector(Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreatorManager;Lcom/google/android/apps/chrome/compositor/TabContentManager;)V

    .line 492
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mIsTablet:Z

    if-eqz v0, :cond_4

    .line 493
    new-instance v0, Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewWrapper;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getTabModelSelector()Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    move-result-object v1

    invoke-virtual {p0, v7}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getTabCreator(Z)Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;

    move-result-object v2

    sget v4, Lcom/google/android/apps/chrome/R$id;->empty_container_stub:I

    iget-object v5, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mAppMenuHandler:Lorg/chromium/chrome/browser/appmenu/AppMenuHandler;

    move-object v3, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewWrapper;-><init>(Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;Landroid/app/Activity;ILorg/chromium/chrome/browser/appmenu/AppMenuHandler;)V

    .line 496
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/emptybackground/EmptyBackgroundViewWrapper;->initialize()V

    .line 499
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mOverviewBehavior:Lcom/google/android/apps/chrome/OverviewBehavior;

    if-nez v0, :cond_5

    .line 500
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getLayoutManager()Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mOverviewBehavior:Lcom/google/android/apps/chrome/OverviewBehavior;

    .line 502
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mOverviewBehavior:Lcom/google/android/apps/chrome/OverviewBehavior;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getCompositorViewHolder()Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/chrome/OverviewBehavior;->setKeyboardHider(Lcom/google/android/apps/chrome/KeyboardHider;)V

    .line 503
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mOverviewBehavior:Lcom/google/android/apps/chrome/OverviewBehavior;

    invoke-interface {v0, v7}, Lcom/google/android/apps/chrome/OverviewBehavior;->hideOverview(Z)V

    .line 505
    iput-boolean v6, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mUIInitialized:Z

    .line 506
    invoke-static {}, Lorg/chromium/base/TraceEvent;->end()V

    .line 507
    return-void

    :cond_6
    move v0, v7

    .line 410
    goto/16 :goto_0

    .line 417
    :cond_7
    new-instance v1, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromePhone;

    invoke-direct {v1, v3, v3}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChromePhone;-><init>(Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerHost;Lcom/google/android/apps/chrome/eventfilter/EventFilterHost;)V

    goto/16 :goto_1

    .line 484
    :cond_8
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getCompositorViewHolder()Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/widget/tabstrip/TabStripManager;->setKeyboardHider(Lcom/google/android/apps/chrome/KeyboardHider;)V

    goto :goto_2
.end method

.method private isFullscreenVideoPlaying()Z
    .locals 1

    .prologue
    .line 1607
    invoke-static {}, Lorg/chromium/content/browser/ContentVideoView;->getContentVideoView()Lorg/chromium/content/browser/ContentVideoView;

    move-result-object v0

    .line 1608
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    if-ne v0, p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private launchFirstRunExperience()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 857
    invoke-static {p0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->setDelaySync(Z)V

    .line 858
    iget-boolean v2, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mIsOnFirstRun:Z

    if-eqz v2, :cond_1

    .line 859
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mTabModelSelectorImpl:Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->clearState()V

    .line 871
    :cond_0
    :goto_0
    return-void

    .line 863
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "android.intent.action.MAIN"

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    move v0, v1

    .line 865
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-static {p0, v2, v0}, Lcom/google/android/apps/chrome/firstrun/FirstRunFlowSequencer;->checkIfFirstRunIsNecessary(Landroid/app/Activity;Landroid/content/Intent;Z)Landroid/content/Intent;

    move-result-object v0

    .line 867
    if-eqz v0, :cond_0

    .line 869
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mIsOnFirstRun:Z

    .line 870
    const/16 v1, 0x65

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method private launchIntent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLandroid/content/Intent;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1291
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mUIInitialized:Z

    if-eqz v0, :cond_0

    .line 1292
    invoke-direct {p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getAndSetupOverviewLayout()Lcom/google/android/apps/chrome/OverviewBehavior;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/google/android/apps/chrome/OverviewBehavior;->hideOverview(Z)V

    .line 1293
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mToolbar:Lcom/google/android/apps/chrome/toolbar/Toolbar;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/toolbar/Toolbar;->getDelegate()Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->finishAnimations()V

    .line 1295
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-static {p4, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1300
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getCurrentTabCreator()Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;

    move-result-object v0

    sget-object v1, Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;->FROM_LINK:Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;

    invoke-virtual {v0, p1, v1, p6}, Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;->launchUrl(Ljava/lang/String;Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;Landroid/content/Intent;)Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    .line 1305
    :goto_0
    if-eqz p3, :cond_1

    .line 1306
    invoke-virtual {v0, p3}, Lcom/google/android/apps/chrome/tab/ChromeTab;->setSnapshotId(Ljava/lang/String;)V

    .line 1308
    :cond_1
    return-void

    .line 1302
    :cond_2
    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getTabCreator(Z)Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;

    move-result-object v0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p4

    move v4, p5

    move-object v5, p6

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/chrome/tabmodel/ChromeTabCreator;->launchUrlFromExternalApp(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLandroid/content/Intent;)Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    goto :goto_0
.end method

.method private preInitializeUI()V
    .locals 4

    .prologue
    .line 802
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x13

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 806
    sget v0, Lcom/google/android/apps/chrome/R$id;->content_container:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mContentContainer:Landroid/view/ViewGroup;

    .line 807
    sget v0, Lcom/google/android/apps/chrome/R$id;->control_container:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;

    iput-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mControlContainer:Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;

    .line 809
    new-instance v0, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupController;

    const v1, 0x1020002

    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mTabModelSelectorImpl:Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupController;-><init>(Landroid/view/View;Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mUndoBarPopupController:Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupController;

    .line 812
    new-instance v0, Lcom/google/android/apps/chrome/ContextualMenuBar;

    invoke-direct {v0, p0, p0}, Lcom/google/android/apps/chrome/ContextualMenuBar;-><init>(Landroid/content/Context;Lcom/google/android/apps/chrome/ContextualMenuBar$ActionBarDelegate;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mContextualMenuBar:Lcom/google/android/apps/chrome/ContextualMenuBar;

    .line 813
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mContextualMenuBar:Lcom/google/android/apps/chrome/ContextualMenuBar;

    new-instance v1, Lcom/google/android/apps/chrome/CustomSelectionActionModeCallback;

    invoke-direct {v1}, Lcom/google/android/apps/chrome/CustomSelectionActionModeCallback;-><init>()V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/ContextualMenuBar;->setCustomSelectionActionModeCallback(Lcom/google/android/apps/chrome/CustomSelectionActionModeCallback;)V

    .line 816
    sget v0, Lcom/google/android/apps/chrome/R$id;->toolbar:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/toolbar/Toolbar;

    iput-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mToolbar:Lcom/google/android/apps/chrome/toolbar/Toolbar;

    .line 817
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mToolbar:Lcom/google/android/apps/chrome/toolbar/Toolbar;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/toolbar/Toolbar;->getDelegate()Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;

    move-result-object v0

    .line 818
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getCompositorViewHolder()Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->getInvalidator()Lcom/google/android/apps/chrome/compositor/Invalidator;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->setPaintInvalidator(Lcom/google/android/apps/chrome/compositor/Invalidator;)V

    .line 819
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->getLocationBar()Lcom/google/android/apps/chrome/omnibox/LocationBar;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/chrome/WindowDelegate;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/android/apps/chrome/WindowDelegate;-><init>(Landroid/view/Window;)V

    invoke-virtual {v1, v2}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->setWindowDelegate(Lcom/google/android/apps/chrome/WindowDelegate;)V

    .line 820
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->getLocationBar()Lcom/google/android/apps/chrome/omnibox/LocationBar;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getWindowAndroid()Lorg/chromium/ui/base/WindowAndroid;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->setWindowAndroid(Lorg/chromium/ui/base/WindowAndroid;)V

    .line 823
    new-instance v0, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;

    iget-object v1, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mToolbar:Lcom/google/android/apps/chrome/toolbar/Toolbar;

    invoke-direct {v0, v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;-><init>(Lcom/google/android/apps/chrome/toolbar/Toolbar;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mToolbarManager:Lcom/google/android/apps/chrome/toolbar/ToolbarManager;

    .line 825
    new-instance v0, Lcom/google/android/apps/chrome/ChromeTabbedActivity$8;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity$8;-><init>(Lcom/google/android/apps/chrome/ChromeTabbedActivity;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mFirstDrawListener:Landroid/view/ViewTreeObserver$OnPreDrawListener;

    .line 840
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mControlContainer:Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mFirstDrawListener:Landroid/view/ViewTreeObserver$OnPreDrawListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 841
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v1, "accessibility"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    .line 843
    invoke-virtual {v0, p0}, Landroid/view/accessibility/AccessibilityManager;->addAccessibilityStateChangeListener(Landroid/view/accessibility/AccessibilityManager$AccessibilityStateChangeListener;)Z

    .line 844
    return-void
.end method

.method private recordOmniboxInteractionStats()V
    .locals 4

    .prologue
    .line 920
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mToolbar:Lcom/google/android/apps/chrome/toolbar/Toolbar;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/toolbar/Toolbar;->getDelegate()Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->getLocationBar()Lcom/google/android/apps/chrome/omnibox/LocationBar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->getFirstFocusTime()J

    move-result-wide v0

    .line 921
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-eqz v2, :cond_0

    .line 922
    iget-wide v2, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mOnCreateTimestampMs:J

    sub-long/2addr v0, v2

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->omniboxFirstFocusTime(J)V

    .line 924
    :cond_0
    return-void
.end method

.method private refreshSignIn()V
    .locals 2

    .prologue
    .line 234
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mIsOnFirstRun:Z

    if-eqz v0, :cond_0

    .line 249
    :goto_0
    return-void

    .line 236
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mIntentWithEffect:Z

    new-instance v1, Lcom/google/android/apps/chrome/ChromeTabbedActivity$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity$1;-><init>(Lcom/google/android/apps/chrome/ChromeTabbedActivity;)V

    invoke-static {p0, v0, v1}, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;->start(Landroid/app/Activity;ZLorg/chromium/chrome/browser/signin/SigninManager$SignInFlowObserver;)V

    goto :goto_0
.end method

.method private showMenu()Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1590
    iget-boolean v2, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mUIInitialized:Z

    if-eqz v2, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->isFullscreenVideoPlaying()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    .line 1603
    :goto_0
    return v0

    .line 1596
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 1598
    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    .line 1599
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getWindow()Landroid/view/Window;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 1600
    iget v3, v3, Landroid/graphics/Rect;->top:I

    .line 1601
    iget-object v4, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mMenuAnchor:Landroid/view/View;

    sub-int/2addr v2, v3

    int-to-float v2, v2

    invoke-virtual {v4, v2}, Landroid/view/View;->setY(F)V

    .line 1602
    iget-object v2, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mAppMenuHandler:Lorg/chromium/chrome/browser/appmenu/AppMenuHandler;

    iget-object v3, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mMenuAnchor:Landroid/view/View;

    invoke-virtual {v2, v3, v0, v1}, Lorg/chromium/chrome/browser/appmenu/AppMenuHandler;->showAppMenu(Landroid/view/View;ZZ)Z

    goto :goto_0
.end method

.method private toggleOverview()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x1

    .line 1339
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getTabModelSelector()Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    move-result-object v0

    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->getCurrentTab()Lorg/chromium/chrome/browser/Tab;

    move-result-object v0

    .line 1340
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/Tab;->getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    .line 1343
    :goto_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getAndSetupOverviewLayout()Lcom/google/android/apps/chrome/OverviewBehavior;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/chrome/OverviewBehavior;->overviewVisible()Z

    move-result v2

    if-nez v2, :cond_2

    .line 1344
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getCompositorViewHolder()Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/chrome/ChromeTabbedActivity$18;

    invoke-direct {v2, p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity$18;-><init>(Lcom/google/android/apps/chrome/ChromeTabbedActivity;)V

    invoke-virtual {v1, v2}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->hideKeyboard(Ljava/lang/Runnable;)V

    .line 1350
    if-eqz v0, :cond_0

    .line 1351
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lorg/chromium/content/browser/ContentViewCore;->setAccessibilityState(Z)V

    .line 1365
    :cond_0
    :goto_1
    return-void

    :cond_1
    move-object v0, v1

    .line 1340
    goto :goto_0

    .line 1353
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getCurrentTabModel()Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v0

    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->getCount()I

    move-result v0

    if-eqz v0, :cond_0

    .line 1355
    invoke-direct {p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getAndSetupOverviewLayout()Lcom/google/android/apps/chrome/OverviewBehavior;

    move-result-object v0

    invoke-interface {v0, v3}, Lcom/google/android/apps/chrome/OverviewBehavior;->hideOverview(Z)V

    .line 1358
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getTabModelSelector()Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    move-result-object v0

    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->getCurrentTab()Lorg/chromium/chrome/browser/Tab;

    move-result-object v0

    .line 1359
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/Tab;->getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v1

    .line 1361
    :cond_3
    if-eqz v1, :cond_0

    .line 1362
    invoke-virtual {v1, v3}, Lorg/chromium/content/browser/ContentViewCore;->setAccessibilityState(Z)V

    goto :goto_1
.end method


# virtual methods
.method public addOrEditBookmark(Lcom/google/android/apps/chrome/tab/ChromeTab;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 1316
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->isFrozen()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1336
    :cond_0
    :goto_0
    return-void

    .line 1320
    :cond_1
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/chrome/bookmark/ManageBookmarkActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1323
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getUserBookmarkId()J

    move-result-wide v2

    .line 1325
    const-wide/16 v4, -0x1

    cmp-long v1, v2, v4

    if-nez v1, :cond_3

    .line 1326
    const-string/jumbo v1, "folder"

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1327
    const-string/jumbo v1, "title"

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1328
    const-string/jumbo v1, "url"

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getUrl()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1334
    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mToolbar:Lcom/google/android/apps/chrome/toolbar/Toolbar;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mToolbar:Lcom/google/android/apps/chrome/toolbar/Toolbar;

    invoke-interface {v1}, Lcom/google/android/apps/chrome/toolbar/Toolbar;->getDelegate()Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->onBookmarkUiVisibilityChange(Z)V

    .line 1335
    :cond_2
    const/16 v1, 0x67

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 1330
    :cond_3
    const-string/jumbo v1, "folder"

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1331
    const-string/jumbo v1, "_id"

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    goto :goto_1
.end method

.method protected buildCompositorViewHolder()Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;
    .locals 1

    .prologue
    .line 793
    sget v0, Lcom/google/android/apps/chrome/R$id;->compositor_view_holder:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    return-object v0
.end method

.method protected createIntentHandlerDelegate()Lcom/google/android/apps/chrome/IntentHandler$IntentHandlerDelegate;
    .locals 2

    .prologue
    .line 1623
    new-instance v0, Lcom/google/android/apps/chrome/ChromeTabbedActivity$InternalIntentDelegate;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/chrome/ChromeTabbedActivity$InternalIntentDelegate;-><init>(Lcom/google/android/apps/chrome/ChromeTabbedActivity;Lcom/google/android/apps/chrome/ChromeTabbedActivity$1;)V

    return-object v0
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 1442
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mUIInitialized:Z

    invoke-static {p1, p0, v0}, Lcom/google/android/apps/chrome/KeyboardShortcuts;->dispatchKeyEvent(Landroid/view/KeyEvent;Lcom/google/android/apps/chrome/ChromeActivity;Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 1443
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/CompositorChromeActivity;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public finishNativeInitialization()V
    .locals 3

    .prologue
    .line 253
    invoke-static {}, Lorg/chromium/base/TraceEvent;->begin()V

    .line 255
    invoke-direct {p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->launchFirstRunExperience()V

    .line 257
    invoke-static {p0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v0

    .line 261
    iget-boolean v1, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mIntentWithEffect:Z

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getFirstRunFlowComplete()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 264
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getPromosSkippedOnFirstStart()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 267
    invoke-static {p0}, Lcom/google/android/apps/chrome/promoscreen/SigninPromoScreen;->launchSigninPromoIfNeeded(Landroid/app/Activity;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 268
    invoke-static {p0}, Lcom/google/android/apps/chrome/promoscreen/DataReductionPromoScreen;->launchDataReductionPromo(Landroid/app/Activity;)V

    .line 275
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->refreshSignIn()V

    .line 277
    sget-object v0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->NOTIFICATIONS:[I

    iget-object v1, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-static {p0, v0, v1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->registerForNotifications(Landroid/content/Context;[ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    .line 279
    invoke-static {}, Lorg/chromium/base/CommandLine;->getInstance()Lorg/chromium/base/CommandLine;

    move-result-object v0

    const-string/jumbo v1, "notification-center-logging"

    invoke-virtual {v0, v1}, Lorg/chromium/base/CommandLine;->hasSwitch(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 281
    invoke-static {}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->getInstance()Lcom/google/android/apps/chrome/ChromeNotificationCenter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->enableLogging()V

    .line 284
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->createSearchEngineListener()V

    .line 286
    invoke-direct {p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->initializeUI()V

    .line 289
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mTabModelSelectorImpl:Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->didChange()V

    .line 291
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x5

    const/4 v2, -0x2

    invoke-virtual {v0, v1, v2}, Landroid/view/Window;->setFeatureInt(II)V

    .line 295
    const/16 v0, 0x25

    invoke-static {v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastNotification(I)V

    .line 297
    invoke-super {p0}, Lcom/google/android/apps/chrome/CompositorChromeActivity;->finishNativeInitialization()V

    .line 299
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getCurrentTab()Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 300
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getCurrentTab()Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getWebContents()Lorg/chromium/content_public/browser/WebContents;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/apps/chrome/preferences/bandwidth/BandwidthReductionPreferences;->launchDataReductionSSLInfoBar(Landroid/content/Context;Lorg/chromium/content_public/browser/WebContents;)V

    .line 304
    :cond_2
    invoke-static {}, Lorg/chromium/base/TraceEvent;->end()V

    .line 305
    return-void

    .line 271
    :cond_3
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->setPromosSkippedOnFirstStart(Z)V

    goto :goto_0
.end method

.method public getAccessibilityOverviewLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;
    .locals 1

    .prologue
    .line 1519
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getLayoutManager()Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;->getAccessibilityOverviewLayout()Lcom/google/android/apps/chrome/tabs/layout/Layout;

    move-result-object v0

    return-object v0
.end method

.method public getAppMenuHandler()Lorg/chromium/chrome/browser/appmenu/AppMenuHandler;
    .locals 1

    .prologue
    .line 1573
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mAppMenuHandler:Lorg/chromium/chrome/browser/appmenu/AppMenuHandler;

    return-object v0
.end method

.method public getControlTopMargin()I
    .locals 1

    .prologue
    .line 1496
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mControlContainer:Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 1498
    iget v0, v0, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    return v0
.end method

.method public getLayoutManager()Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;
    .locals 1

    .prologue
    .line 1514
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getCompositorViewHolder()Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->getLayoutManager()Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerDocument;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/tabs/layout/LayoutManagerChrome;

    return-object v0
.end method

.method public getTabsView()Landroid/view/View;
    .locals 1

    .prologue
    .line 1509
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getCompositorViewHolder()Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    move-result-object v0

    return-object v0
.end method

.method public handleBackPressed()Z
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1195
    invoke-super {p0}, Lcom/google/android/apps/chrome/CompositorChromeActivity;->handleBackPressed()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1274
    :goto_0
    return v0

    .line 1197
    :cond_0
    iget-boolean v2, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mUIInitialized:Z

    if-nez v2, :cond_1

    move v0, v1

    goto :goto_0

    .line 1198
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getCurrentTab()Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v4

    .line 1200
    if-nez v4, :cond_3

    .line 1201
    iget-object v1, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mToolbarManager:Lcom/google/android/apps/chrome/toolbar/ToolbarManager;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->back()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1202
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->systemBackForNavigation()V

    .line 1206
    :goto_1
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->systemBack()V

    goto :goto_0

    .line 1204
    :cond_2
    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->moveTaskToBack(Z)Z

    goto :goto_1

    .line 1211
    :cond_3
    invoke-direct {p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getAndSetupOverviewLayout()Lcom/google/android/apps/chrome/OverviewBehavior;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/chrome/OverviewBehavior;->overviewVisible()Z

    move-result v2

    if-eqz v2, :cond_4

    iget-boolean v2, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mIsTablet:Z

    if-nez v2, :cond_4

    .line 1212
    invoke-direct {p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getAndSetupOverviewLayout()Lcom/google/android/apps/chrome/OverviewBehavior;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/android/apps/chrome/OverviewBehavior;->hideOverview(Z)V

    goto :goto_0

    .line 1217
    :cond_4
    invoke-direct {p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->isFullscreenVideoPlaying()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 1218
    invoke-static {}, Lorg/chromium/content/browser/ContentVideoView;->getContentVideoView()Lorg/chromium/content/browser/ContentVideoView;

    move-result-object v2

    invoke-virtual {v2, v1}, Lorg/chromium/content/browser/ContentVideoView;->exitFullscreen(Z)V

    goto :goto_0

    .line 1222
    :cond_5
    iget-object v2, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mToolbarManager:Lcom/google/android/apps/chrome/toolbar/ToolbarManager;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->back()Z

    move-result v2

    if-nez v2, :cond_d

    .line 1223
    invoke-virtual {v4}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getLaunchType()Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;

    move-result-object v2

    .line 1224
    invoke-virtual {v4}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getAppAssociatedWith()Ljava/lang/String;

    move-result-object v5

    .line 1225
    invoke-virtual {v4}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getParentId()I

    move-result v3

    .line 1226
    invoke-virtual {v4}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getUrl()Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v7, "https://support.google.com/chrome/"

    invoke-virtual {v6, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    .line 1231
    sget-object v7, Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;->FROM_MENU_OR_OVERVIEW:Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;

    if-ne v2, v7, :cond_6

    if-eqz v6, :cond_6

    .line 1232
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getCurrentTabModel()Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v1

    invoke-interface {v1, v4}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->closeTab(Lorg/chromium/chrome/browser/Tab;)Z

    goto :goto_0

    .line 1240
    :cond_6
    sget-object v6, Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;->FROM_LINK:Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;

    if-eq v2, v6, :cond_7

    sget-object v6, Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;->FROM_EXTERNAL_APP:Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;

    if-eq v2, v6, :cond_7

    sget-object v6, Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;->FROM_LONGPRESS_FOREGROUND:Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;

    if-eq v2, v6, :cond_7

    sget-object v6, Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;->FROM_LONGPRESS_BACKGROUND:Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;

    if-eq v2, v6, :cond_7

    sget-object v6, Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;->FROM_RESTORE:Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;

    if-ne v2, v6, :cond_a

    const/4 v6, -0x1

    if-eq v3, v6, :cond_a

    :cond_7
    move v3, v0

    .line 1250
    :goto_2
    if-eqz v3, :cond_8

    sget-object v6, Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;->FROM_EXTERNAL_APP:Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;

    if-ne v2, v6, :cond_b

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v5, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_b

    :cond_8
    move v2, v0

    .line 1253
    :goto_3
    if-eqz v2, :cond_c

    .line 1254
    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->moveTaskToBack(Z)Z

    .line 1255
    if-eqz v3, :cond_9

    .line 1259
    iget-object v1, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mHandler:Landroid/os/Handler;

    new-instance v2, Lcom/google/android/apps/chrome/ChromeTabbedActivity$17;

    invoke-direct {v2, p0, v4}, Lcom/google/android/apps/chrome/ChromeTabbedActivity$17;-><init>(Lcom/google/android/apps/chrome/ChromeTabbedActivity;Lcom/google/android/apps/chrome/tab/ChromeTab;)V

    const-wide/16 v4, 0x1f4

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1272
    :cond_9
    :goto_4
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->systemBack()V

    goto/16 :goto_0

    :cond_a
    move v3, v1

    .line 1240
    goto :goto_2

    :cond_b
    move v2, v1

    .line 1250
    goto :goto_3

    .line 1266
    :cond_c
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getCurrentTabModel()Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v2

    invoke-interface {v2, v4, v0, v1, v1}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->closeTab(Lorg/chromium/chrome/browser/Tab;ZZZ)Z

    goto :goto_4

    .line 1270
    :cond_d
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->systemBackForNavigation()V

    goto :goto_4
.end method

.method public hasDoneFirstDraw()Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1618
    iget-object v1, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mToolbar:Lcom/google/android/apps/chrome/toolbar/Toolbar;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mToolbar:Lcom/google/android/apps/chrome/toolbar/Toolbar;

    invoke-interface {v1}, Lcom/google/android/apps/chrome/toolbar/Toolbar;->getDelegate()Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->getFirstDrawTime()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public initializeCompositor()V
    .locals 2

    .prologue
    .line 222
    invoke-static {}, Lorg/chromium/base/TraceEvent;->begin()V

    .line 223
    invoke-super {p0}, Lcom/google/android/apps/chrome/CompositorChromeActivity;->initializeCompositor()V

    .line 225
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mTabModelSelectorImpl:Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getTabContentManager()Lcom/google/android/apps/chrome/compositor/TabContentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->onNativeLibraryReady(Lcom/google/android/apps/chrome/compositor/TabContentManager;)V

    .line 227
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mSavedInstanceState:Landroid/os/Bundle;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mSavedInstanceState:Landroid/os/Bundle;

    const-string/jumbo v1, "First run is running"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 228
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mSavedInstanceState:Landroid/os/Bundle;

    const-string/jumbo v1, "First run is running"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mIsOnFirstRun:Z

    .line 230
    :cond_0
    invoke-static {}, Lorg/chromium/base/TraceEvent;->end()V

    .line 231
    return-void
.end method

.method public initializeState()V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 528
    invoke-static {}, Lorg/chromium/base/TraceEvent;->begin()V

    .line 530
    invoke-super {p0}, Lcom/google/android/apps/chrome/CompositorChromeActivity;->initializeState()V

    .line 532
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 534
    invoke-static {}, Lorg/chromium/content/browser/crypto/CipherFactory;->getInstance()Lorg/chromium/content/browser/crypto/CipherFactory;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mSavedInstanceState:Landroid/os/Bundle;

    invoke-virtual {v3, v4}, Lorg/chromium/content/browser/crypto/CipherFactory;->restoreFromBundle(Landroid/os/Bundle;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 535
    iget-object v3, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mTabModelSelectorImpl:Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->clearEncryptedState()V

    .line 538
    :cond_0
    invoke-static {}, Lorg/chromium/base/CommandLine;->getInstance()Lorg/chromium/base/CommandLine;

    move-result-object v3

    const-string/jumbo v4, "no-restore-state"

    invoke-virtual {v3, v4}, Lorg/chromium/base/CommandLine;->hasSwitch(Ljava/lang/String;)Z

    move-result v3

    .line 540
    if-eqz v3, :cond_7

    .line 542
    iget-object v3, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mTabModelSelectorImpl:Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->clearState()V

    .line 550
    :cond_1
    :goto_0
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mIntentWithEffect:Z

    .line 551
    iget-boolean v3, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mIsOnFirstRun:Z

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mSavedInstanceState:Landroid/os/Bundle;

    if-nez v3, :cond_3

    :cond_2
    if-eqz v0, :cond_3

    iget-object v3, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mIntentHandler:Lcom/google/android/apps/chrome/IntentHandler;

    invoke-virtual {v3, p0, v0}, Lcom/google/android/apps/chrome/IntentHandler;->shouldIgnoreIntent(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 553
    iget-object v3, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mIntentHandler:Lcom/google/android/apps/chrome/IntentHandler;

    invoke-virtual {v3, v0}, Lcom/google/android/apps/chrome/IntentHandler;->onNewIntent(Landroid/content/Intent;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mIntentWithEffect:Z

    .line 556
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getCurrentTabModel()Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v0

    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->getCount()I

    move-result v0

    if-gtz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mTabModelSelectorImpl:Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->getRestoredTabCount()I

    move-result v0

    if-gtz v0, :cond_4

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mIntentWithEffect:Z

    if-eqz v0, :cond_8

    :cond_4
    move v0, v2

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mCreatedTabOnStartup:Z

    .line 564
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mIntentWithEffect:Z

    if-nez v0, :cond_9

    .line 565
    :goto_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mTabModelSelectorImpl:Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->restoreTabs(Z)V

    .line 571
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mCreatedTabOnStartup:Z

    if-eqz v0, :cond_5

    if-eqz v2, :cond_6

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getTabModelSelector()Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    move-result-object v0

    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->getTotalTabCount()I

    move-result v0

    if-nez v0, :cond_6

    .line 576
    :cond_5
    new-instance v0, Lcom/google/android/apps/chrome/ChromeTabbedActivity$7;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity$7;-><init>(Lcom/google/android/apps/chrome/ChromeTabbedActivity;)V

    const-wide/16 v2, 0x1f4

    invoke-static {v0, v2, v3}, Lcom/google/android/apps/chrome/partnercustomizations/PartnerBrowserCustomizations;->setOnInitializeAsyncFinished(Ljava/lang/Runnable;J)V

    .line 585
    :cond_6
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mIntentWithEffect:Z

    invoke-static {v0}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->coldStartup(Z)V

    .line 586
    invoke-static {}, Lorg/chromium/base/TraceEvent;->end()V

    .line 587
    return-void

    .line 543
    :cond_7
    iget-boolean v3, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mIsOnFirstRun:Z

    if-nez v3, :cond_1

    .line 547
    iget-object v3, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mTabModelSelectorImpl:Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;

    invoke-virtual {v3}, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->loadState()V

    goto :goto_0

    :cond_8
    move v0, v1

    .line 556
    goto :goto_1

    :cond_9
    move v2, v1

    .line 564
    goto :goto_2
.end method

.method public isInOverviewMode()Z
    .locals 1

    .prologue
    .line 1613
    invoke-direct {p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getAndSetupOverviewLayout()Lcom/google/android/apps/chrome/OverviewBehavior;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/OverviewBehavior;->overviewVisible()Z

    move-result v0

    return v0
.end method

.method public isOverlayVisible()Z
    .locals 1

    .prologue
    .line 1540
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getCompositorViewHolder()Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getCompositorViewHolder()Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->isTabInteractive()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public mayShowUpdateInfoBar()Z
    .locals 1

    .prologue
    .line 1545
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->isOverlayVisible()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onAccessibilityStateChanged(Z)V
    .locals 1

    .prologue
    .line 1413
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mToolbarManager:Lcom/google/android/apps/chrome/toolbar/ToolbarManager;

    if-eqz v0, :cond_0

    .line 1414
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mToolbarManager:Lcom/google/android/apps/chrome/toolbar/ToolbarManager;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/toolbar/ToolbarManager;->onAccessibilityStatusChanged(Z)V

    .line 1416
    :cond_0
    return-void
.end method

.method public onAccountSelected(Landroid/app/Activity;Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 875
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/sync/signin/AccountManagerHelper;->get(Landroid/content/Context;)Lorg/chromium/sync/signin/AccountManagerHelper;

    move-result-object v0

    invoke-virtual {v0, p2}, Lorg/chromium/sync/signin/AccountManagerHelper;->getAccountFromName(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v1

    .line 877
    if-nez v1, :cond_0

    .line 881
    :goto_0
    return-void

    .line 879
    :cond_0
    const/4 v3, 0x1

    const/4 v5, 0x0

    move-object v0, p0

    move v4, v2

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/chrome/firstrun/FirstRunUtil;->signInToSelectedAccount(Landroid/app/Activity;Landroid/accounts/Account;IIZLorg/chromium/chrome/browser/signin/SigninManager$SignInFlowObserver;)V

    goto :goto_0
.end method

.method public onActivityResultWithNative(IILandroid/content/Intent;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 601
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/chrome/CompositorChromeActivity;->onActivityResultWithNative(IILandroid/content/Intent;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 625
    :cond_0
    :goto_0
    return v0

    .line 603
    :cond_1
    const/16 v2, 0x65

    if-ne p1, v2, :cond_4

    .line 604
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mIsOnFirstRun:Z

    .line 605
    const/4 v2, -0x1

    if-ne p2, v2, :cond_2

    .line 606
    invoke-direct {p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->refreshSignIn()V

    goto :goto_0

    .line 608
    :cond_2
    if-eqz p3, :cond_3

    const-string/jumbo v2, "Close App"

    invoke-virtual {p3, v2, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 610
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getTabModelSelector()Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    move-result-object v1

    invoke-interface {v1}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->closeAllTabs()V

    .line 611
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->finish()V

    goto :goto_0

    .line 613
    :cond_3
    invoke-direct {p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->launchFirstRunExperience()V

    goto :goto_0

    .line 617
    :cond_4
    const/16 v2, 0x66

    if-ne p1, v2, :cond_5

    .line 618
    const/16 v1, 0x32

    invoke-static {p0, v1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastNotification(Landroid/content/Context;I)V

    goto :goto_0

    .line 621
    :cond_5
    const/16 v2, 0x67

    if-ne p1, v2, :cond_6

    .line 622
    iget-object v2, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mToolbar:Lcom/google/android/apps/chrome/toolbar/Toolbar;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mToolbar:Lcom/google/android/apps/chrome/toolbar/Toolbar;

    invoke-interface {v2}, Lcom/google/android/apps/chrome/toolbar/Toolbar;->getDelegate()Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->onBookmarkUiVisibilityChange(Z)V

    goto :goto_0

    :cond_6
    move v0, v1

    .line 625
    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .prologue
    .line 1009
    invoke-direct {p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->hideMenus()V

    .line 1010
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/CompositorChromeActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1011
    return-void
.end method

.method protected onDeferredStartup()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x7530

    .line 885
    invoke-static {}, Lorg/chromium/base/TraceEvent;->begin()V

    .line 886
    invoke-super {p0}, Lcom/google/android/apps/chrome/CompositorChromeActivity;->onDeferredStartup()V

    .line 889
    iget-wide v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mInflateInitialLayoutDurationMs:J

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->toolbarInflationTime(J)V

    .line 890
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mToolbar:Lcom/google/android/apps/chrome/toolbar/Toolbar;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/toolbar/Toolbar;->getDelegate()Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->getFirstDrawTime()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mOnCreateTimestampMs:J

    sub-long/2addr v0, v2

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->toolbarFirstDrawTime(J)V

    .line 893
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mOnCreateTimestampMs:J

    sub-long/2addr v0, v2

    .line 894
    cmp-long v2, v0, v4

    if-ltz v2, :cond_0

    .line 895
    invoke-direct {p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->recordOmniboxInteractionStats()V

    .line 905
    :goto_0
    const-string/jumbo v0, "activity"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 906
    invoke-virtual {v0}, Landroid/app/ActivityManager;->getMemoryClass()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->deviceMemoryClass(I)V

    .line 908
    new-instance v0, Lcom/google/android/apps/chrome/feedback/DomDistillerFeedbackLauncher;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/feedback/DomDistillerFeedbackLauncher;-><init>()V

    invoke-static {v0}, Lorg/chromium/chrome/browser/dom_distiller/DomDistillerFeedbackReporter;->setExternalFeedbackReporter(Lorg/chromium/chrome/browser/dom_distiller/ExternalFeedbackReporter;)V

    .line 911
    invoke-static {}, Lorg/chromium/chrome/browser/omnibox/AutocompleteController;->nativePrefetchZeroSuggestResults()V

    .line 913
    invoke-static {}, Lorg/chromium/base/TraceEvent;->end()V

    .line 914
    return-void

    .line 897
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mHandler:Landroid/os/Handler;

    new-instance v3, Lcom/google/android/apps/chrome/ChromeTabbedActivity$9;

    invoke-direct {v3, p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity$9;-><init>(Lcom/google/android/apps/chrome/ChromeTabbedActivity;)V

    sub-long v0, v4, v0

    invoke-virtual {v2, v3, v0, v1}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method public onDestroyInternal()V
    .locals 2

    .prologue
    .line 1387
    const/16 v0, 0x27

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastImmediateNotification(Landroid/content/Context;ILandroid/os/Bundle;)V

    .line 1389
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mTabModelSelectorImpl:Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mTabModelSelectorImpl:Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->destroy()V

    .line 1390
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mUndoBarPopupController:Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupController;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mUndoBarPopupController:Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupController;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupController;->destroy()V

    .line 1391
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    if-eqz v0, :cond_2

    .line 1392
    sget-object v0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->NOTIFICATIONS:[I

    iget-object v1, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mNotificationHandler:Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;

    invoke-static {p0, v0, v1}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->unregisterForNotifications(Landroid/content/Context;[ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    .line 1396
    :cond_2
    invoke-super {p0}, Lcom/google/android/apps/chrome/CompositorChromeActivity;->onDestroyInternal()V

    .line 1398
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mCurrentLocale:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1406
    const-string/jumbo v0, "ChromeTabbedActivity"

    const-string/jumbo v1, "Forcefully killing process..."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1407
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v0

    invoke-static {v0}, Landroid/os/Process;->killProcess(I)V

    .line 1409
    :cond_3
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1448
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mUIInitialized:Z

    if-nez v0, :cond_1

    .line 1451
    :cond_0
    :goto_0
    return v2

    .line 1449
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getAndSetupOverviewLayout()Lcom/google/android/apps/chrome/OverviewBehavior;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/OverviewBehavior;->overviewVisible()Z

    move-result v0

    if-nez v0, :cond_4

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mIsTablet:Z

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getCurrentTabModel()Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v0

    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->getCount()I

    move-result v0

    if-eqz v0, :cond_4

    :cond_2
    move v0, v1

    .line 1451
    :goto_1
    invoke-static {p2, p0, v0, v1}, Lcom/google/android/apps/chrome/KeyboardShortcuts;->onKeyDown(Landroid/view/KeyEvent;Lcom/google/android/apps/chrome/ChromeActivity;ZZ)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/chrome/CompositorChromeActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_3
    move v2, v1

    goto :goto_0

    :cond_4
    move v0, v2

    .line 1449
    goto :goto_1
.end method

.method public onLowMemory()V
    .locals 1

    .prologue
    .line 1420
    invoke-super {p0}, Lcom/google/android/apps/chrome/CompositorChromeActivity;->onLowMemory()V

    .line 1421
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mMemoryUma:Lcom/google/android/apps/chrome/uma/MemoryUma;

    if-eqz v0, :cond_0

    .line 1422
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mMemoryUma:Lcom/google/android/apps/chrome/uma/MemoryUma;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/uma/MemoryUma;->onLowMemory()V

    .line 1424
    :cond_0
    return-void
.end method

.method public onMenuOrKeyboardAction(I)Z
    .locals 7

    .prologue
    const/4 v3, 0x0

    const/4 v6, 0x1

    .line 1016
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getCurrentTab()Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v1

    .line 1017
    sget v0, Lcom/google/android/apps/chrome/R$id;->forward_menu_id:I

    if-ne p1, v0, :cond_2

    .line 1018
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->canGoForward()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1019
    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->goForward()V

    .line 1020
    invoke-static {v6}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->forward(Z)V

    :cond_0
    :goto_0
    move v3, v6

    .line 1190
    :cond_1
    return v3

    .line 1022
    :cond_2
    sget v0, Lcom/google/android/apps/chrome/R$id;->reload_menu_id:I

    if-ne p1, v0, :cond_4

    .line 1023
    if-eqz v1, :cond_0

    .line 1024
    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->isLoading()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1025
    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->stopLoading()V

    goto :goto_0

    .line 1027
    :cond_3
    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->reload()V

    .line 1028
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->reload()V

    goto :goto_0

    .line 1031
    :cond_4
    sget v0, Lcom/google/android/apps/chrome/R$id;->new_tab_menu_id:I

    if-ne p1, v0, :cond_5

    .line 1032
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getCompositorViewHolder()Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/chrome/ChromeTabbedActivity$11;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity$11;-><init>(Lcom/google/android/apps/chrome/ChromeTabbedActivity;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->hideKeyboard(Ljava/lang/Runnable;)V

    .line 1040
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->menuNewTab()V

    goto :goto_0

    .line 1041
    :cond_5
    sget v0, Lcom/google/android/apps/chrome/R$id;->new_incognito_tab_menu_id:I

    if-ne p1, v0, :cond_6

    .line 1042
    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->isIncognitoModeEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1045
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->menuNewIncognitoTab()V

    .line 1046
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getCompositorViewHolder()Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/chrome/ChromeTabbedActivity$12;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity$12;-><init>(Lcom/google/android/apps/chrome/ChromeTabbedActivity;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->hideKeyboard(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 1055
    :cond_6
    sget v0, Lcom/google/android/apps/chrome/R$id;->add_to_homescreen_id:I

    if-ne p1, v0, :cond_7

    .line 1056
    if-eqz v1, :cond_0

    .line 1057
    invoke-static {p0, v1}, Lcom/google/android/apps/chrome/webapps/WebappManager;->showDialog(Landroid/app/Activity;Lorg/chromium/chrome/browser/Tab;)V

    .line 1058
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->menuAddToHomescreen()V

    goto :goto_0

    .line 1060
    :cond_7
    sget v0, Lcom/google/android/apps/chrome/R$id;->all_bookmarks_menu_id:I

    if-ne p1, v0, :cond_8

    .line 1061
    if-eqz v1, :cond_0

    .line 1062
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getCompositorViewHolder()Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    move-result-object v0

    new-instance v2, Lcom/google/android/apps/chrome/ChromeTabbedActivity$13;

    invoke-direct {v2, p0, v1}, Lcom/google/android/apps/chrome/ChromeTabbedActivity$13;-><init>(Lcom/google/android/apps/chrome/ChromeTabbedActivity;Lcom/google/android/apps/chrome/tab/ChromeTab;)V

    invoke-virtual {v0, v2}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->hideKeyboard(Ljava/lang/Runnable;)V

    .line 1076
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->menuAllBookmarks()V

    goto :goto_0

    .line 1078
    :cond_8
    sget v0, Lcom/google/android/apps/chrome/R$id;->recent_tabs_menu_id:I

    if-ne p1, v0, :cond_9

    .line 1079
    if-eqz v1, :cond_0

    .line 1080
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getCompositorViewHolder()Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    move-result-object v0

    new-instance v2, Lcom/google/android/apps/chrome/ChromeTabbedActivity$14;

    invoke-direct {v2, p0, v1}, Lcom/google/android/apps/chrome/ChromeTabbedActivity$14;-><init>(Lcom/google/android/apps/chrome/ChromeTabbedActivity;Lcom/google/android/apps/chrome/tab/ChromeTab;)V

    invoke-virtual {v0, v2}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->hideKeyboard(Ljava/lang/Runnable;)V

    .line 1088
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->menuOpenTabs()V

    goto/16 :goto_0

    .line 1090
    :cond_9
    sget v0, Lcom/google/android/apps/chrome/R$id;->close_all_tabs_menu_id:I

    if-ne p1, v0, :cond_a

    .line 1092
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getTabModelSelector()Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    move-result-object v0

    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->closeAllTabs()V

    .line 1093
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->menuCloseAllTabs()V

    goto/16 :goto_0

    .line 1094
    :cond_a
    sget v0, Lcom/google/android/apps/chrome/R$id;->open_history_menu_id:I

    if-ne p1, v0, :cond_b

    .line 1095
    if-eqz v1, :cond_0

    .line 1096
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getCompositorViewHolder()Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    move-result-object v0

    new-instance v2, Lcom/google/android/apps/chrome/ChromeTabbedActivity$15;

    invoke-direct {v2, p0, v1}, Lcom/google/android/apps/chrome/ChromeTabbedActivity$15;-><init>(Lcom/google/android/apps/chrome/ChromeTabbedActivity;Lcom/google/android/apps/chrome/tab/ChromeTab;)V

    invoke-virtual {v0, v2}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->hideKeyboard(Ljava/lang/Runnable;)V

    .line 1102
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->menuHistory()V

    goto/16 :goto_0

    .line 1104
    :cond_b
    sget v0, Lcom/google/android/apps/chrome/R$id;->close_all_incognito_tabs_menu_id:I

    if-ne p1, v0, :cond_c

    .line 1106
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getTabModelSelector()Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    move-result-object v0

    invoke-interface {v0, v6}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->getModel(Z)Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v0

    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->closeAllTabs()V

    .line 1108
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->menuCloseAllTabs()V

    goto/16 :goto_0

    .line 1109
    :cond_c
    sget v0, Lcom/google/android/apps/chrome/R$id;->find_in_page_id:I

    if-ne p1, v0, :cond_d

    .line 1110
    const/16 v0, 0x2c

    invoke-static {p0, v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastImmediateNotification(Landroid/content/Context;I)V

    .line 1112
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->menuFindInPage()V

    goto/16 :goto_0

    .line 1113
    :cond_d
    sget v0, Lcom/google/android/apps/chrome/R$id;->bookmark_this_page_id:I

    if-ne p1, v0, :cond_e

    .line 1114
    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->addOrEditBookmark(Lcom/google/android/apps/chrome/tab/ChromeTab;)V

    .line 1115
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->menuAddToBookmarks()V

    goto/16 :goto_0

    .line 1116
    :cond_e
    sget v0, Lcom/google/android/apps/chrome/R$id;->share_menu_id:I

    if-eq p1, v0, :cond_f

    sget v0, Lcom/google/android/apps/chrome/R$id;->direct_share_menu_id:I

    if-ne p1, v0, :cond_11

    .line 1119
    :cond_f
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getWindowAndroid()Lorg/chromium/ui/base/WindowAndroid;

    move-result-object v2

    sget v0, Lcom/google/android/apps/chrome/R$id;->direct_share_menu_id:I

    if-ne p1, v0, :cond_10

    move v3, v6

    :cond_10
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getCurrentTabModel()Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v0

    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->isIncognito()Z

    move-result v4

    const/high16 v5, 0x80000

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->onShareMenuItemSelected(Lcom/google/android/apps/chrome/tab/ChromeTab;Lorg/chromium/ui/base/WindowAndroid;ZZI)V

    goto/16 :goto_0

    .line 1122
    :cond_11
    sget v0, Lcom/google/android/apps/chrome/R$id;->preferences_id:I

    if-ne p1, v0, :cond_12

    .line 1123
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1124
    const-class v1, Lcom/google/android/apps/chrome/preferences/Preferences;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    .line 1125
    const/high16 v1, 0x20020000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1127
    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->startActivity(Landroid/content/Intent;)V

    .line 1128
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->menuSettings()V

    goto/16 :goto_0

    .line 1129
    :cond_12
    sget v0, Lcom/google/android/apps/chrome/R$id;->help_id:I

    if-ne p1, v0, :cond_15

    .line 1132
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getContentReadbackHandler()Lorg/chromium/content/browser/ContentReadbackHandler;

    move-result-object v0

    .line 1133
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getWindowAndroid()Lorg/chromium/ui/base/WindowAndroid;

    move-result-object v2

    .line 1135
    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getCurrentTabModel()Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v3

    invoke-interface {v3}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->isIncognito()Z

    move-result v3

    invoke-static {v1, v3}, Lcom/google/android/apps/chrome/help/GoogleHelpUtils;->getHelpContextIdFromUrl(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    .line 1137
    new-instance v3, Lcom/google/android/apps/chrome/ChromeTabbedActivity$16;

    invoke-direct {v3, p0, p0, v1}, Lcom/google/android/apps/chrome/ChromeTabbedActivity$16;-><init>(Lcom/google/android/apps/chrome/ChromeTabbedActivity;Landroid/app/Activity;Ljava/lang/String;)V

    .line 1145
    if-eqz v0, :cond_13

    if-nez v2, :cond_14

    .line 1146
    :cond_13
    const/4 v0, 0x0

    invoke-interface {v3, v0}, Lorg/chromium/content/browser/ContentReadbackHandler$GetBitmapCallback;->onFinishGetBitmap(Landroid/graphics/Bitmap;)V

    goto/16 :goto_0

    .line 1148
    :cond_14
    invoke-virtual {v0, v2, v3}, Lorg/chromium/content/browser/ContentReadbackHandler;->getCompositorBitmapAsync(Lorg/chromium/ui/base/WindowAndroid;Lorg/chromium/content/browser/ContentReadbackHandler$GetBitmapCallback;)V

    goto/16 :goto_0

    .line 1150
    :cond_15
    sget v0, Lcom/google/android/apps/chrome/R$id;->print_id:I

    if-ne p1, v0, :cond_16

    .line 1151
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getChromeApplication()Lcom/google/android/apps/chrome/ChromeMobileApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeMobileApplication;->getPrintingController()Lorg/chromium/printing/PrintingController;

    move-result-object v0

    .line 1152
    if-eqz v0, :cond_0

    invoke-interface {v0}, Lorg/chromium/printing/PrintingController;->isBusy()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1153
    new-instance v2, Lorg/chromium/chrome/browser/printing/TabPrinter;

    invoke-direct {v2, v1}, Lorg/chromium/chrome/browser/printing/TabPrinter;-><init>(Lorg/chromium/chrome/browser/Tab;)V

    new-instance v1, Lorg/chromium/printing/PrintManagerDelegateImpl;

    invoke-direct {v1, p0}, Lorg/chromium/printing/PrintManagerDelegateImpl;-><init>(Landroid/content/Context;)V

    invoke-interface {v0, v2, v1}, Lorg/chromium/printing/PrintingController;->startPrint(Lorg/chromium/printing/Printable;Lorg/chromium/printing/PrintManagerDelegate;)V

    .line 1155
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->menuPrint()V

    goto/16 :goto_0

    .line 1157
    :cond_16
    sget v0, Lcom/google/android/apps/chrome/R$id;->request_desktop_site_id:I

    if-ne p1, v0, :cond_19

    .line 1158
    if-eqz v1, :cond_0

    .line 1159
    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->isNativePage()Z

    move-result v0

    if-nez v0, :cond_18

    move v0, v6

    .line 1160
    :goto_1
    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getUseDesktopUserAgent()Z

    move-result v2

    .line 1161
    if-nez v2, :cond_17

    move v3, v6

    :cond_17
    invoke-virtual {v1, v3, v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->setUseDesktopUserAgent(ZZ)V

    .line 1162
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->menuRequestDesktopSite()V

    goto/16 :goto_0

    :cond_18
    move v0, v3

    .line 1159
    goto :goto_1

    .line 1164
    :cond_19
    sget v0, Lcom/google/android/apps/chrome/R$id;->reader_mode_id:I

    if-ne p1, v0, :cond_1a

    .line 1165
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->getWebContents()Lorg/chromium/content_public/browser/WebContents;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1167
    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->getWebContents()Lorg/chromium/content_public/browser/WebContents;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/chrome/browser/dom_distiller/DomDistillerTabUtils;->distillCurrentPageAndView(Lorg/chromium/content_public/browser/WebContents;)V

    .line 1169
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->readerModeEntered()V

    goto/16 :goto_0

    .line 1171
    :cond_1a
    sget v0, Lcom/google/android/apps/chrome/R$id;->reader_mode_prefs_id:I

    if-ne p1, v0, :cond_1b

    .line 1172
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->getWebContents()Lorg/chromium/content_public/browser/WebContents;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1174
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->readerModePrefsEntered()V

    .line 1175
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 1176
    invoke-static {p0}, Lorg/chromium/chrome/browser/dom_distiller/DistilledPagePrefsView;->create(Landroid/content/Context;)Lorg/chromium/chrome/browser/dom_distiller/DistilledPagePrefsView;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 1177
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto/16 :goto_0

    .line 1179
    :cond_1b
    sget v0, Lcom/google/android/apps/chrome/R$id;->show_menu:I

    if-ne p1, v0, :cond_1c

    .line 1180
    invoke-direct {p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->showMenu()Z

    goto/16 :goto_0

    .line 1181
    :cond_1c
    sget v0, Lcom/google/android/apps/chrome/R$id;->focus_url_bar:I

    if-ne p1, v0, :cond_1

    .line 1182
    invoke-direct {p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getAndSetupOverviewLayout()Lcom/google/android/apps/chrome/OverviewBehavior;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/OverviewBehavior;->overviewVisible()Z

    move-result v0

    if-nez v0, :cond_1e

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mIsTablet:Z

    if-eqz v0, :cond_1d

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getCurrentTabModel()Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v0

    invoke-interface {v0}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->getCount()I

    move-result v0

    if-eqz v0, :cond_1e

    :cond_1d
    move v3, v6

    .line 1184
    :cond_1e
    if-eqz v3, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mToolbar:Lcom/google/android/apps/chrome/toolbar/Toolbar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mToolbar:Lcom/google/android/apps/chrome/toolbar/Toolbar;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/toolbar/Toolbar;->getDelegate()Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1185
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mToolbar:Lcom/google/android/apps/chrome/toolbar/Toolbar;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/toolbar/Toolbar;->getDelegate()Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->getLocationBar()Lcom/google/android/apps/chrome/omnibox/LocationBar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->requestUrlFocus()V

    goto/16 :goto_0
.end method

.method public onNewIntentWithNative(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 351
    :try_start_0
    invoke-static {}, Lorg/chromium/base/TraceEvent;->begin()V

    .line 353
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/CompositorChromeActivity;->onNewIntentWithNative(Landroid/content/Intent;)V

    .line 354
    invoke-static {}, Lorg/chromium/base/CommandLine;->getInstance()Lorg/chromium/base/CommandLine;

    move-result-object v0

    const-string/jumbo v1, "enable-test-intents"

    invoke-virtual {v0, v1}, Lorg/chromium/base/CommandLine;->hasSwitch(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 355
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->handleDebugIntent(Landroid/content/Intent;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 358
    :cond_0
    invoke-static {}, Lorg/chromium/base/TraceEvent;->end()V

    .line 359
    return-void

    .line 358
    :catchall_0
    move-exception v0

    invoke-static {}, Lorg/chromium/base/TraceEvent;->end()V

    throw v0
.end method

.method public onOrientationChange(I)V
    .locals 1

    .prologue
    .line 630
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/CompositorChromeActivity;->onOrientationChange(I)V

    .line 631
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mContextualMenuBar:Lcom/google/android/apps/chrome/ContextualMenuBar;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ContextualMenuBar;->showControlsOnOrientationChange()V

    .line 632
    return-void
.end method

.method public onPauseWithNative()V
    .locals 1

    .prologue
    .line 309
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mTabModelSelectorImpl:Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->commitAllTabClosures()V

    .line 310
    invoke-super {p0}, Lcom/google/android/apps/chrome/CompositorChromeActivity;->onPauseWithNative()V

    .line 311
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1369
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/CompositorChromeActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1370
    invoke-static {}, Lorg/chromium/content/browser/crypto/CipherFactory;->getInstance()Lorg/chromium/content/browser/crypto/CipherFactory;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/chromium/content/browser/crypto/CipherFactory;->saveToBundle(Landroid/os/Bundle;)V

    .line 1371
    const-string/jumbo v0, "is_incognito_selected"

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getCurrentTabModel()Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v1

    invoke-interface {v1}, Lorg/chromium/chrome/browser/tabmodel/TabModel;->isIncognito()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1372
    const-string/jumbo v0, "First run is running"

    iget-boolean v1, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mIsOnFirstRun:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1373
    const-string/jumbo v0, "window_index"

    invoke-static {}, Lcom/google/android/apps/chrome/tabmodel/TabWindowManager;->getInstance()Lcom/google/android/apps/chrome/tabmodel/TabWindowManager;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/google/android/apps/chrome/tabmodel/TabWindowManager;->getIndexForWindow(Landroid/app/Activity;)I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1375
    return-void
.end method

.method public onStartWithNative()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 330
    invoke-super {p0}, Lcom/google/android/apps/chrome/CompositorChromeActivity;->onStartWithNative()V

    .line 332
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getCurrentTab()Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getAndSetupOverviewLayout()Lcom/google/android/apps/chrome/OverviewBehavior;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/google/android/apps/chrome/OverviewBehavior;->showOverview(Z)V

    .line 334
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getConnectionChangeReceiver()Lcom/google/android/apps/chrome/preferences/ConnectionChangeReceiver;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/apps/chrome/preferences/ConnectionChangeReceiver;->registerReceiver(Landroid/content/Context;)V

    .line 336
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mSavedInstanceState:Landroid/os/Bundle;

    .line 339
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mToolbar:Lcom/google/android/apps/chrome/toolbar/Toolbar;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/toolbar/Toolbar;->getDelegate()Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarDelegate;->getLocationBar()Lcom/google/android/apps/chrome/omnibox/LocationBar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/omnibox/LocationBar;->updateMicButtonState()V

    .line 341
    invoke-static {}, Lcom/google/android/apps/chrome/WarmupManager;->getInstance()Lcom/google/android/apps/chrome/WarmupManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/WarmupManager;->clearWebContentsIfNecessary()V

    .line 343
    invoke-static {p0}, Lcom/google/android/apps/chrome/utilities/FeatureUtilitiesInternal;->isDocumentModeEligible(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 344
    invoke-static {v1}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->recordDocumentMode(Z)V

    .line 346
    :cond_1
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 1379
    invoke-super {p0}, Lcom/google/android/apps/chrome/CompositorChromeActivity;->onStop()V

    .line 1380
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mMemoryUma:Lcom/google/android/apps/chrome/uma/MemoryUma;

    if-eqz v0, :cond_0

    .line 1381
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mMemoryUma:Lcom/google/android/apps/chrome/uma/MemoryUma;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/uma/MemoryUma;->onStop()V

    .line 1383
    :cond_0
    return-void
.end method

.method public onStopWithNative()V
    .locals 1

    .prologue
    .line 315
    invoke-super {p0}, Lcom/google/android/apps/chrome/CompositorChromeActivity;->onStopWithNative()V

    .line 316
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mTabModelSelectorImpl:Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabmodel/TabModelSelectorImpl;->saveState()V

    .line 318
    :try_start_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getConnectionChangeReceiver()Lcom/google/android/apps/chrome/preferences/ConnectionChangeReceiver;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/apps/chrome/preferences/ConnectionChangeReceiver;->unregisterReceiver(Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 324
    :goto_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->hideMenus()V

    .line 325
    invoke-direct {p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->hideSuggestions()V

    .line 326
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public onTrimMemory(I)V
    .locals 1

    .prologue
    .line 1428
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/CompositorChromeActivity;->onTrimMemory(I)V

    .line 1431
    const/16 v0, 0xa

    if-lt p1, v0, :cond_0

    const/16 v0, 0x14

    if-lt p1, v0, :cond_1

    :cond_0
    const/16 v0, 0x3c

    if-lt p1, v0, :cond_2

    .line 1433
    :cond_1
    invoke-static {}, Lcom/google/android/apps/chrome/ntp/NativePageAssassin;->getInstance()Lcom/google/android/apps/chrome/ntp/NativePageAssassin;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ntp/NativePageAssassin;->freezeAllHiddenPages()V

    .line 1435
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mMemoryUma:Lcom/google/android/apps/chrome/uma/MemoryUma;

    if-eqz v0, :cond_3

    .line 1436
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mMemoryUma:Lcom/google/android/apps/chrome/uma/MemoryUma;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/uma/MemoryUma;->onTrimMemory(I)V

    .line 1438
    :cond_3
    return-void
.end method

.method public postInflationStartup()V
    .locals 2

    .prologue
    .line 772
    invoke-super {p0}, Lcom/google/android/apps/chrome/CompositorChromeActivity;->postInflationStartup()V

    .line 777
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mSavedInstanceState:Landroid/os/Bundle;

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->createTabModelSelectorImpl(Landroid/os/Bundle;)V

    .line 779
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 789
    :cond_0
    :goto_0
    return-void

    .line 784
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->preInitializeUI()V

    .line 786
    invoke-static {}, Lorg/chromium/base/CommandLine;->getInstance()Lorg/chromium/base/CommandLine;

    move-result-object v0

    const-string/jumbo v1, "take-surface"

    invoke-virtual {v0, v1}, Lorg/chromium/base/CommandLine;->hasSwitch(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 787
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getCompositorViewHolder()Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/compositor/CompositorViewHolder;->getSurfaceHolderCallback2()Landroid/view/SurfaceHolder$Callback2;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/Window;->takeSurface(Landroid/view/SurfaceHolder$Callback2;)V

    goto :goto_0
.end method

.method public preInflationStartup()V
    .locals 7

    .prologue
    const/4 v5, -0x1

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 739
    invoke-super {p0}, Lcom/google/android/apps/chrome/CompositorChromeActivity;->preInflationStartup()V

    .line 741
    invoke-static {}, Lorg/chromium/base/CommandLine;->getInstance()Lorg/chromium/base/CommandLine;

    move-result-object v3

    .line 742
    const-string/jumbo v0, "enable-test-intents"

    invoke-virtual {v3, v0}, Lorg/chromium/base/CommandLine;->hasSwitch(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v4, "render_process_limit"

    invoke-virtual {v0, v4}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 746
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v4, "render_process_limit"

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 748
    if-eq v0, v5, :cond_0

    .line 749
    new-array v4, v1, [Ljava/lang/String;

    .line 750
    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "--renderer-process-limit="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v2

    .line 752
    invoke-virtual {v3, v4}, Lorg/chromium/base/CommandLine;->appendSwitchesAndArguments([Ljava/lang/String;)V

    .line 756
    :cond_0
    const-string/jumbo v0, "enable-high-end-ui-undo"

    invoke-virtual {v3, v0}, Lorg/chromium/base/CommandLine;->appendSwitch(Ljava/lang/String;)V

    .line 758
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mIsTablet:Z

    if-eqz v0, :cond_3

    const-string/jumbo v0, "disable-compositor-tab-strip"

    invoke-virtual {v3, v0}, Lorg/chromium/base/CommandLine;->hasSwitch(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mShowingOldTabStrip:Z

    .line 760
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mIsTablet:Z

    if-eqz v0, :cond_1

    const-string/jumbo v0, "enable-tablet-tab-stack"

    invoke-virtual {v3, v0}, Lorg/chromium/base/CommandLine;->hasSwitch(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/device/DeviceClassManager;->isAccessibilityModeEnabled(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    move v2, v1

    :cond_2
    iput-boolean v2, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mShowTabStack:Z

    .line 765
    const-string/jumbo v0, "enable-begin-frame-scheduling"

    invoke-virtual {v3, v0}, Lorg/chromium/base/CommandLine;->appendSwitch(Ljava/lang/String;)V

    .line 767
    const/16 v0, 0xa

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->requestWindowFeature(I)Z

    .line 768
    return-void

    :cond_3
    move v0, v2

    .line 758
    goto :goto_0
.end method

.method public setActionBarBackgroundVisibility(Z)V
    .locals 2

    .prologue
    .line 1503
    if-eqz p1, :cond_0

    const/4 v0, 0x0

    .line 1504
    :goto_0
    sget v1, Lcom/google/android/apps/chrome/R$id;->action_bar_black_background:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1505
    return-void

    .line 1503
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method protected setContentView()V
    .locals 5

    .prologue
    .line 1628
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    .line 1629
    const-string/jumbo v0, "onCreate->setContentView(R.layout.main)"

    invoke-static {v0}, Lorg/chromium/base/TraceEvent;->begin(Ljava/lang/String;)V

    .line 1630
    invoke-static {}, Lcom/google/android/apps/chrome/WarmupManager;->getInstance()Lcom/google/android/apps/chrome/WarmupManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/WarmupManager;->hasBuiltViewHierarchy()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1631
    new-instance v1, Landroid/view/View;

    invoke-direct {v1, p0}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 1632
    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->setContentView(Landroid/view/View;)V

    .line 1633
    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 1634
    invoke-static {}, Lcom/google/android/apps/chrome/WarmupManager;->getInstance()Lcom/google/android/apps/chrome/WarmupManager;

    move-result-object v4

    invoke-virtual {v4, v0}, Lcom/google/android/apps/chrome/WarmupManager;->transferViewHierarchyTo(Landroid/view/ViewGroup;)V

    .line 1635
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 1641
    :goto_0
    invoke-super {p0}, Lcom/google/android/apps/chrome/CompositorChromeActivity;->setContentView()V

    .line 1642
    const-string/jumbo v0, "onCreate->setContentView(R.layout.main)"

    invoke-static {v0}, Lorg/chromium/base/TraceEvent;->end(Ljava/lang/String;)V

    .line 1643
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mInflateInitialLayoutDurationMs:J

    .line 1644
    return-void

    .line 1637
    :cond_0
    sget v0, Lcom/google/android/apps/chrome/R$layout;->main:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->setContentView(I)V

    goto :goto_0
.end method

.method public setControlTopMargin(I)V
    .locals 2

    .prologue
    .line 1485
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mControlContainer:Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 1487
    iput p1, v0, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 1488
    iget-object v1, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mControlContainer:Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/widget/ToolbarControlContainer;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1489
    return-void
.end method

.method public shouldShowAppMenu()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1580
    iget-boolean v1, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mUIInitialized:Z

    if-nez v1, :cond_1

    .line 1586
    :cond_0
    :goto_0
    return v0

    .line 1583
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mFindToolbarWrapper:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarWrapper;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mFindToolbarWrapper:Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarWrapper;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/widget/findinpage/FindToolbarWrapper;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-boolean v1, p0, Lcom/google/android/apps/chrome/ChromeTabbedActivity;->mIsTablet:Z

    if-eqz v1, :cond_0

    .line 1586
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method
