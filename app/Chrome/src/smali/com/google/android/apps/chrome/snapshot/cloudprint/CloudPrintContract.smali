.class public final Lcom/google/android/apps/chrome/snapshot/cloudprint/CloudPrintContract;
.super Ljava/lang/Object;
.source "CloudPrintContract.java"


# static fields
.field public static final ACCEPT_HTTP_HEADER:Ljava/lang/String; = "Accept"

.field static final APPLICATION_VERSION_TAG:Ljava/lang/String; = "__version__application"

.field static final APPLICATION_VERSION_TAG_REMOVAL_REGEXP:Ljava/lang/String; = "__version__application=.*"

.field static final APP_HEADER_NAME:Ljava/lang/String; = "X-CloudPrint-Proxy"

.field static final APP_HEADER_VALUE:Ljava/lang/String; = "ChromeMobile"

.field static final AUTH_HEADER_NAME:Ljava/lang/String; = "Authorization"

.field static final AUTH_HEADER_OAUTH2_VALUE_PREFIX:Ljava/lang/String; = "OAuth "

.field static final AUTH_HEADER_VALUE_PREFIX:Ljava/lang/String; = "GoogleLogin auth="

.field static final C2DM_REGISTRATION_ID_PARAM:Ljava/lang/String; = "__c2dm__reg_id"

.field static final C2DM_REGISTRATION_ID_REMOVAL_REGEXP:Ljava/lang/String; = "__c2dm__reg_id=.*"

.field static final CLIENTLOGIN_AUTH_TOKEN_TYPE:Ljava/lang/String; = "cloudprint"

.field static final CLIENT_QUERY_PARAM:Ljava/lang/String; = "client=chrome-to-mobile-android"

.field static final CLOUD_PRINT_BASE_URL:Ljava/lang/String; = "https://www.google.com/cloudprint/"

.field static final CONTROL_INTERFACE_NAME:Ljava/lang/String; = "control"

.field static final DELETEJOB_INTERFACE_NAME:Ljava/lang/String; = "deletejob"

.field static final DELETE_INTERFACE_NAME:Ljava/lang/String; = "delete"

.field static final FETCH_INTERFACE_NAME:Ljava/lang/String; = "fetch"

.field static final JOB_ID_PARAM_NAME:Ljava/lang/String; = "jobid"

.field static final LIST_INTERFACE_NAME:Ljava/lang/String; = "list"

.field public static final OAUTH2_SCOPE:Ljava/lang/String; = "https://www.googleapis.com/auth/cloudprint"

.field static final PRINTER_CAPABILITIES_HASH_PARAM_NAME:Ljava/lang/String; = "capsHash"

.field static final PRINTER_CAPABILITIES_PARAM_NAME:Ljava/lang/String; = "capabilities"

.field static final PRINTER_DEFAULT_VALUES_PARAM_NAME:Ljava/lang/String; = "default"

.field static final PRINTER_DESCRIPTION_PARAM_NAME:Ljava/lang/String; = "description"

.field static final PRINTER_ID_PARAM_NAME:Ljava/lang/String; = "printerid"

.field static final PRINTER_ID_RESPONSE_NAME:Ljava/lang/String; = "id"

.field static final PRINTER_INTERFACE_NAME:Ljava/lang/String; = "printer"

.field static final PRINTER_NAME_REGISTRATION_PARAM_NAME:Ljava/lang/String; = "printer"

.field static final PRINTER_NAME_RESPONSE_PARAM_NAME:Ljava/lang/String; = "name"

.field static final PRINTER_STATUS_PARAM_NAME:Ljava/lang/String; = "status"

.field static final PRINTER_STATUS_PARAM_VALUE_OK:Ljava/lang/String; = "0"

.field static final PRINTER_TYPE_PARAM_NAME:Ljava/lang/String; = "type"

.field static final PRINTER_TYPE_VALUE:Ljava/lang/String; = "ANDROID_CHROME_SNAPSHOT"

.field static final PROTOCOL_VERSION_TAG:Ljava/lang/String; = "__version__protocol"

.field static final PROTOCOL_VERSION_TAG_REMOVAL_REGEXP:Ljava/lang/String; = "__version__protocol=.*"

.field static final PROXY_ID_PREFIX:Ljava/lang/String; = "ChromeMobile_"

.field static final PROXY_PARAM_NAME:Ljava/lang/String; = "proxy"

.field static final REGISTER_INTERFACE_NAME:Ljava/lang/String; = "register"

.field static final REMOVE_TAG_PARAM:Ljava/lang/String; = "remove_tag"

.field static final RESPONSE_MESSAGE_PARAM_NAME:Ljava/lang/String; = "message"

.field static final RESPONSE_PRINTERS_PARAM_NAME:Ljava/lang/String; = "printers"

.field static final RESPONSE_PRINT_JOBS_PARAM_NAME:Ljava/lang/String; = "jobs"

.field static final RESPONSE_SUCCESS_PARAM_NAME:Ljava/lang/String; = "success"

.field static final TAGS_PARAM:Ljava/lang/String; = "tags"

.field static final TAG_PARAM:Ljava/lang/String; = "tag"

.field static final UPDATE_INTERFACE_NAME:Ljava/lang/String; = "update"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 89
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 90
    return-void
.end method

.method public static isChromeToMobileOAuth2Enabled()Z
    .locals 2

    .prologue
    .line 23
    invoke-static {}, Lorg/chromium/base/CommandLine;->getInstance()Lorg/chromium/base/CommandLine;

    move-result-object v0

    const-string/jumbo v1, "disable-chrome-to-mobile-oauth2"

    invoke-virtual {v0, v1}, Lorg/chromium/base/CommandLine;->hasSwitch(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
