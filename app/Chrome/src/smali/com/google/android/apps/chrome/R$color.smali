.class public final Lcom/google/android/apps/chrome/R$color;
.super Ljava/lang/Object;
.source "R.java"


# static fields
.field public static final abc_background_cache_hint_selector_material_dark:I = 0x7f0b00a4

.field public static final abc_background_cache_hint_selector_material_light:I = 0x7f0b00a5

.field public static final abc_input_method_navigation_guard:I = 0x7f0b001a

.field public static final abc_primary_text_disable_only_material_dark:I = 0x7f0b00a6

.field public static final abc_primary_text_disable_only_material_light:I = 0x7f0b00a7

.field public static final abc_primary_text_material_dark:I = 0x7f0b00a8

.field public static final abc_primary_text_material_light:I = 0x7f0b00a9

.field public static final abc_search_url_text:I = 0x7f0b00aa

.field public static final abc_search_url_text_normal:I = 0x7f0b0017

.field public static final abc_search_url_text_pressed:I = 0x7f0b0019

.field public static final abc_search_url_text_selected:I = 0x7f0b0018

.field public static final abc_secondary_text_material_dark:I = 0x7f0b00ab

.field public static final abc_secondary_text_material_light:I = 0x7f0b00ac

.field public static final accent_material_dark:I = 0x7f0b0026

.field public static final accent_material_light:I = 0x7f0b0025

.field public static final accessibility_close_undo_text:I = 0x7f0b005a

.field public static final accessibility_tab_switcher_list_item:I = 0x7f0b0059

.field public static final account_management_disabled_color:I = 0x7f0b0095

.field public static final active_control_color:I = 0x7f0b0062

.field public static final app_banner_card_highlight:I = 0x7f0b005d

.field public static final app_banner_install_button_fg:I = 0x7f0b005b

.field public static final app_banner_open_button_fg:I = 0x7f0b005c

.field public static final background_floating_material_dark:I = 0x7f0b001d

.field public static final background_floating_material_light:I = 0x7f0b001e

.field public static final background_material_dark:I = 0x7f0b001b

.field public static final background_material_light:I = 0x7f0b001c

.field public static final bg_tabstrip_normal:I = 0x7f0b00a3

.field public static final bright_foreground_disabled_material_dark:I = 0x7f0b002d

.field public static final bright_foreground_disabled_material_light:I = 0x7f0b002e

.field public static final bright_foreground_inverse_material_dark:I = 0x7f0b002f

.field public static final bright_foreground_inverse_material_light:I = 0x7f0b0030

.field public static final bright_foreground_material_dark:I = 0x7f0b002b

.field public static final bright_foreground_material_light:I = 0x7f0b002c

.field public static final button_material_dark:I = 0x7f0b0027

.field public static final button_material_light:I = 0x7f0b0028

.field public static final cast_media_controller_text:I = 0x7f0b0094

.field public static final color_picker_background_color:I = 0x7f0b0049

.field public static final color_picker_border_color:I = 0x7f0b0048

.field public static final common_action_bar_splitter:I = 0x7f0b0009

.field public static final common_signin_btn_dark_text_default:I = 0x7f0b0000

.field public static final common_signin_btn_dark_text_disabled:I = 0x7f0b0002

.field public static final common_signin_btn_dark_text_focused:I = 0x7f0b0003

.field public static final common_signin_btn_dark_text_pressed:I = 0x7f0b0001

.field public static final common_signin_btn_default_background:I = 0x7f0b0008

.field public static final common_signin_btn_light_text_default:I = 0x7f0b0004

.field public static final common_signin_btn_light_text_disabled:I = 0x7f0b0006

.field public static final common_signin_btn_light_text_focused:I = 0x7f0b0007

.field public static final common_signin_btn_light_text_pressed:I = 0x7f0b0005

.field public static final common_signin_btn_text_dark:I = 0x7f0b00ad

.field public static final common_signin_btn_text_light:I = 0x7f0b00ae

.field public static final data_reduction_percent_color:I = 0x7f0b0087

.field public static final default_primary_color:I = 0x7f0b0063

.field public static final dim_foreground_disabled_material_dark:I = 0x7f0b0033

.field public static final dim_foreground_disabled_material_light:I = 0x7f0b0034

.field public static final dim_foreground_material_dark:I = 0x7f0b0031

.field public static final dim_foreground_material_light:I = 0x7f0b0032

.field public static final disable_pref_activity:I = 0x7f0b0088

.field public static final distilled_page_prefs_selected:I = 0x7f0b0060

.field public static final distilled_page_prefs_unselected:I = 0x7f0b0061

.field public static final drawer_list_view_highlight:I = 0x7f0b0096

.field public static final dropdown_dark_divider_color:I = 0x7f0b004b

.field public static final dropdown_divider_color:I = 0x7f0b004a

.field public static final enhanced_bookmark_detail_section:I = 0x7f0b0098

.field public static final enhanced_bookmark_detail_text:I = 0x7f0b0097

.field public static final enhanced_bookmark_shadow_color:I = 0x7f0b0099

.field public static final find_in_page_failed_results_status_color:I = 0x7f0b007e

.field public static final find_in_page_query_color:I = 0x7f0b007a

.field public static final find_in_page_query_white_color:I = 0x7f0b007b

.field public static final find_in_page_results_status_color:I = 0x7f0b007c

.field public static final find_in_page_results_status_white_color:I = 0x7f0b007d

.field public static final find_result_bar_active_border_color:I = 0x7f0b0079

.field public static final find_result_bar_active_color:I = 0x7f0b0078

.field public static final find_result_bar_background_border_color:I = 0x7f0b0075

.field public static final find_result_bar_background_color:I = 0x7f0b0074

.field public static final find_result_bar_result_border_color:I = 0x7f0b0077

.field public static final find_result_bar_result_color:I = 0x7f0b0076

.field public static final fre_background_color:I = 0x7f0b007f

.field public static final fre_dark_text_color:I = 0x7f0b0084

.field public static final fre_light_text_color:I = 0x7f0b0085

.field public static final fre_negative_button_color:I = 0x7f0b0083

.field public static final fre_text_color:I = 0x7f0b0081

.field public static final fre_thin_line_color:I = 0x7f0b0080

.field public static final fre_title_color:I = 0x7f0b0082

.field public static final highlighted_text_material_dark:I = 0x7f0b0037

.field public static final highlighted_text_material_light:I = 0x7f0b0038

.field public static final hint_foreground_material_dark:I = 0x7f0b0035

.field public static final hint_foreground_material_light:I = 0x7f0b0036

.field public static final incognito_primary_color:I = 0x7f0b0064

.field public static final infobar_accent_blue:I = 0x7f0b004f

.field public static final infobar_accent_blue_pressed:I = 0x7f0b0050

.field public static final infobar_background:I = 0x7f0b004d

.field public static final infobar_background_separator:I = 0x7f0b004e

.field public static final infobar_tertiary_button_text:I = 0x7f0b0051

.field public static final infobar_text:I = 0x7f0b004c

.field public static final light_background_color:I = 0x7f0b0086

.field public static final link_text_material_dark:I = 0x7f0b0039

.field public static final link_text_material_light:I = 0x7f0b003a

.field public static final locationbar_default_text:I = 0x7f0b0065

.field public static final locationbar_domain_and_registry:I = 0x7f0b006b

.field public static final locationbar_light_default_text:I = 0x7f0b006e

.field public static final locationbar_light_domain_and_registry:I = 0x7f0b006f

.field public static final locationbar_light_hint_text:I = 0x7f0b006d

.field public static final locationbar_scheme_to_domain:I = 0x7f0b006a

.field public static final locationbar_start_scheme_ev_secure:I = 0x7f0b0068

.field public static final locationbar_start_scheme_secure:I = 0x7f0b0069

.field public static final locationbar_start_scheme_security_error:I = 0x7f0b0067

.field public static final locationbar_start_scheme_security_warning:I = 0x7f0b0066

.field public static final locationbar_trailing_url:I = 0x7f0b006c

.field public static final material_blue_grey_800:I = 0x7f0b0045

.field public static final material_blue_grey_900:I = 0x7f0b0046

.field public static final material_blue_grey_950:I = 0x7f0b0047

.field public static final material_deep_teal_200:I = 0x7f0b0043

.field public static final material_deep_teal_500:I = 0x7f0b0044

.field public static final new_ntp_bg:I = 0x7f0b0089

.field public static final ntp_bg:I = 0x7f0b008a

.field public static final ntp_bg_incognito:I = 0x7f0b008b

.field public static final ntp_list_header_subtext:I = 0x7f0b0090

.field public static final ntp_list_header_subtext_active:I = 0x7f0b0091

.field public static final ntp_list_header_text:I = 0x7f0b008f

.field public static final ntp_list_item_text:I = 0x7f0b008e

.field public static final ntp_most_visited_bg:I = 0x7f0b008d

.field public static final ntp_most_visited_text:I = 0x7f0b008c

.field public static final opt_out_text_color:I = 0x7f0b009a

.field public static final preference_header_text_color:I = 0x7f0b00af

.field public static final primary_dark_material_dark:I = 0x7f0b0021

.field public static final primary_dark_material_light:I = 0x7f0b0022

.field public static final primary_material_dark:I = 0x7f0b001f

.field public static final primary_material_light:I = 0x7f0b0020

.field public static final primary_text_default_material_dark:I = 0x7f0b003d

.field public static final primary_text_default_material_light:I = 0x7f0b003b

.field public static final primary_text_disabled_material_dark:I = 0x7f0b0041

.field public static final primary_text_disabled_material_light:I = 0x7f0b003f

.field public static final ripple_material_dark:I = 0x7f0b0023

.field public static final ripple_material_light:I = 0x7f0b0024

.field public static final secondary_text_default_material_dark:I = 0x7f0b003e

.field public static final secondary_text_default_material_light:I = 0x7f0b003c

.field public static final secondary_text_disabled_material_dark:I = 0x7f0b0042

.field public static final secondary_text_disabled_material_light:I = 0x7f0b0040

.field public static final switch_thumb_normal_material_dark:I = 0x7f0b0029

.field public static final switch_thumb_normal_material_light:I = 0x7f0b002a

.field public static final tab_back:I = 0x7f0b0057

.field public static final tab_back_incognito:I = 0x7f0b0058

.field public static final tab_strip_bottom_stroke:I = 0x7f0b00a1

.field public static final tab_strip_bottom_stroke_incognito:I = 0x7f0b00a2

.field public static final tab_strip_fade_mode_overlay:I = 0x7f0b00a0

.field public static final tab_switcher_background:I = 0x7f0b0052

.field public static final tab_title_bar_shadow:I = 0x7f0b0055

.field public static final tab_title_bar_shadow_incognito:I = 0x7f0b0056

.field public static final tab_title_bar_text:I = 0x7f0b0053

.field public static final tab_title_bar_text_incognito:I = 0x7f0b0054

.field public static final titlebar_dark_text:I = 0x7f0b009c

.field public static final titlebar_highlight_opaque:I = 0x7f0b009f

.field public static final titlebar_highlight_translucent:I = 0x7f0b009e

.field public static final titlebar_light_text:I = 0x7f0b009d

.field public static final titlebar_shadow_color:I = 0x7f0b009b

.field public static final toolbar_tab_count_color:I = 0x7f0b0070

.field public static final toolbar_tab_count_color_pressed:I = 0x7f0b0072

.field public static final toolbar_tab_count_color_white:I = 0x7f0b0071

.field public static final toolbar_tab_count_color_white_pressed:I = 0x7f0b0073

.field public static final undo_bar_textview_color:I = 0x7f0b00b0

.field public static final wallet_bright_foreground_disabled_holo_light:I = 0x7f0b000f

.field public static final wallet_bright_foreground_holo_dark:I = 0x7f0b000a

.field public static final wallet_bright_foreground_holo_light:I = 0x7f0b0010

.field public static final wallet_dim_foreground_disabled_holo_dark:I = 0x7f0b000c

.field public static final wallet_dim_foreground_holo_dark:I = 0x7f0b000b

.field public static final wallet_dim_foreground_inverse_disabled_holo_dark:I = 0x7f0b000e

.field public static final wallet_dim_foreground_inverse_holo_dark:I = 0x7f0b000d

.field public static final wallet_highlighted_text_holo_dark:I = 0x7f0b0014

.field public static final wallet_highlighted_text_holo_light:I = 0x7f0b0013

.field public static final wallet_hint_foreground_holo_dark:I = 0x7f0b0012

.field public static final wallet_hint_foreground_holo_light:I = 0x7f0b0011

.field public static final wallet_holo_blue_light:I = 0x7f0b0015

.field public static final wallet_link_text_light:I = 0x7f0b0016

.field public static final wallet_primary_text_holo_light:I = 0x7f0b00b1

.field public static final wallet_secondary_text_holo_dark:I = 0x7f0b00b2

.field public static final webapp_url_bar_bg:I = 0x7f0b0092

.field public static final webapp_url_bar_separator:I = 0x7f0b0093

.field public static final website_settings_popup_reset_cert_decisions_button:I = 0x7f0b005f

.field public static final website_settings_popup_text_link:I = 0x7f0b005e


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2226
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
