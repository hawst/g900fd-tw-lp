.class public interface abstract Lcom/google/android/apps/chrome/bookmark/AddEditBookmarkFragment$OnActionListener;
.super Ljava/lang/Object;
.source "AddEditBookmarkFragment.java"


# virtual methods
.method public abstract onCancel()V
.end method

.method public abstract onFolderCreated(JLjava/lang/String;)V
.end method

.method public abstract onNodeEdited(J)V
.end method

.method public abstract onRemove()V
.end method

.method public abstract setBackEnabled(Z)V
.end method

.method public abstract triggerFolderSelection()V
.end method
