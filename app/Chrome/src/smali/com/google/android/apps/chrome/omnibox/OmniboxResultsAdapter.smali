.class public Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter;
.super Landroid/widget/BaseAdapter;
.source "OmniboxResultsAdapter.java"


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mLocationBar:Lcom/google/android/apps/chrome/omnibox/LocationBar;

.field private mSuggestionDelegate:Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter$OmniboxSuggestionDelegate;

.field private final mSuggestionItems:Ljava/util/List;

.field private mUseDarkColors:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/chrome/omnibox/LocationBar;Ljava/util/List;)V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 27
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter;->mUseDarkColors:Z

    .line 31
    iput-object p1, p0, Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter;->mContext:Landroid/content/Context;

    .line 32
    iput-object p2, p0, Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter;->mLocationBar:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    .line 33
    iput-object p3, p0, Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter;->mSuggestionItems:Ljava/util/List;

    .line 34
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter;->mSuggestionItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter;->mSuggestionItems:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 52
    int-to-long v0, p1

    return-wide v0
.end method

.method public getSuggestionDelegate()Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter$OmniboxSuggestionDelegate;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter;->mSuggestionDelegate:Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter$OmniboxSuggestionDelegate;

    return-object v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 58
    if-eqz p2, :cond_0

    instance-of v0, p2, Lcom/google/android/apps/chrome/omnibox/SuggestionView;

    if-eqz v0, :cond_0

    .line 59
    check-cast p2, Lcom/google/android/apps/chrome/omnibox/SuggestionView;

    .line 63
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter;->mSuggestionItems:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter$OmniboxResultItem;

    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter;->mSuggestionDelegate:Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter$OmniboxSuggestionDelegate;

    iget-boolean v2, p0, Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter;->mUseDarkColors:Z

    invoke-virtual {p2, v0, v1, p1, v2}, Lcom/google/android/apps/chrome/omnibox/SuggestionView;->init(Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter$OmniboxResultItem;Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter$OmniboxSuggestionDelegate;IZ)V

    .line 65
    return-object p2

    .line 61
    :cond_0
    new-instance p2, Lcom/google/android/apps/chrome/omnibox/SuggestionView;

    iget-object v0, p0, Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter;->mLocationBar:Lcom/google/android/apps/chrome/omnibox/LocationBar;

    invoke-direct {p2, v0, v1}, Lcom/google/android/apps/chrome/omnibox/SuggestionView;-><init>(Landroid/content/Context;Lcom/google/android/apps/chrome/omnibox/LocationBar;)V

    goto :goto_0
.end method

.method public notifySuggestionsChanged()V
    .locals 0

    .prologue
    .line 37
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter;->notifyDataSetChanged()V

    .line 38
    return-void
.end method

.method public setSuggestionDelegate(Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter$OmniboxSuggestionDelegate;)V
    .locals 0

    .prologue
    .line 74
    iput-object p1, p0, Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter;->mSuggestionDelegate:Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter$OmniboxSuggestionDelegate;

    .line 75
    return-void
.end method

.method public setUseDarkColors(Z)V
    .locals 0

    .prologue
    .line 90
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/omnibox/OmniboxResultsAdapter;->mUseDarkColors:Z

    .line 91
    return-void
.end method
