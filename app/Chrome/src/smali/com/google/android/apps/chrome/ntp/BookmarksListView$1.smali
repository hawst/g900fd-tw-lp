.class Lcom/google/android/apps/chrome/ntp/BookmarksListView$1;
.super Ljava/lang/Object;
.source "BookmarksListView.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/ntp/BookmarksListView;


# virtual methods
.method public run()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 355
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksListView$1;->this$0:Lcom/google/android/apps/chrome/ntp/BookmarksListView;

    # getter for: Lcom/google/android/apps/chrome/ntp/BookmarksListView;->mIsScrolling:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/BookmarksListView;->access$000(Lcom/google/android/apps/chrome/ntp/BookmarksListView;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 388
    :cond_0
    :goto_0
    return-void

    .line 356
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksListView$1;->this$0:Lcom/google/android/apps/chrome/ntp/BookmarksListView;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ntp/BookmarksListView;->getFirstVisiblePosition()I

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksListView$1;->this$0:Lcom/google/android/apps/chrome/ntp/BookmarksListView;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/ntp/BookmarksListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksListView$1;->this$0:Lcom/google/android/apps/chrome/ntp/BookmarksListView;

    # getter for: Lcom/google/android/apps/chrome/ntp/BookmarksListView;->mTouchY:I
    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/BookmarksListView;->access$100(Lcom/google/android/apps/chrome/ntp/BookmarksListView;)I

    move-result v0

    iget-object v2, p0, Lcom/google/android/apps/chrome/ntp/BookmarksListView$1;->this$0:Lcom/google/android/apps/chrome/ntp/BookmarksListView;

    # getter for: Lcom/google/android/apps/chrome/ntp/BookmarksListView;->mLowerBound:I
    invoke-static {v2}, Lcom/google/android/apps/chrome/ntp/BookmarksListView;->access$200(Lcom/google/android/apps/chrome/ntp/BookmarksListView;)I

    move-result v2

    if-gt v0, v2, :cond_2

    .line 358
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksListView$1;->this$0:Lcom/google/android/apps/chrome/ntp/BookmarksListView;

    # invokes: Lcom/google/android/apps/chrome/ntp/BookmarksListView;->stopScrolling()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/BookmarksListView;->access$300(Lcom/google/android/apps/chrome/ntp/BookmarksListView;)V

    goto :goto_0

    .line 360
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksListView$1;->this$0:Lcom/google/android/apps/chrome/ntp/BookmarksListView;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ntp/BookmarksListView;->getLastVisiblePosition()I

    move-result v0

    iget-object v2, p0, Lcom/google/android/apps/chrome/ntp/BookmarksListView$1;->this$0:Lcom/google/android/apps/chrome/ntp/BookmarksListView;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/ntp/BookmarksListView;->getCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ne v0, v2, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksListView$1;->this$0:Lcom/google/android/apps/chrome/ntp/BookmarksListView;

    iget-object v2, p0, Lcom/google/android/apps/chrome/ntp/BookmarksListView$1;->this$0:Lcom/google/android/apps/chrome/ntp/BookmarksListView;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/ntp/BookmarksListView;->getChildCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v2}, Lcom/google/android/apps/chrome/ntp/BookmarksListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v0

    iget-object v2, p0, Lcom/google/android/apps/chrome/ntp/BookmarksListView$1;->this$0:Lcom/google/android/apps/chrome/ntp/BookmarksListView;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/ntp/BookmarksListView;->getHeight()I

    move-result v2

    if-ne v0, v2, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksListView$1;->this$0:Lcom/google/android/apps/chrome/ntp/BookmarksListView;

    # getter for: Lcom/google/android/apps/chrome/ntp/BookmarksListView;->mTouchY:I
    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/BookmarksListView;->access$100(Lcom/google/android/apps/chrome/ntp/BookmarksListView;)I

    move-result v0

    iget-object v2, p0, Lcom/google/android/apps/chrome/ntp/BookmarksListView$1;->this$0:Lcom/google/android/apps/chrome/ntp/BookmarksListView;

    # getter for: Lcom/google/android/apps/chrome/ntp/BookmarksListView;->mUpperBound:I
    invoke-static {v2}, Lcom/google/android/apps/chrome/ntp/BookmarksListView;->access$400(Lcom/google/android/apps/chrome/ntp/BookmarksListView;)I

    move-result v2

    if-lt v0, v2, :cond_3

    .line 363
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksListView$1;->this$0:Lcom/google/android/apps/chrome/ntp/BookmarksListView;

    # invokes: Lcom/google/android/apps/chrome/ntp/BookmarksListView;->stopScrolling()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/BookmarksListView;->access$300(Lcom/google/android/apps/chrome/ntp/BookmarksListView;)V

    goto :goto_0

    .line 366
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksListView$1;->this$0:Lcom/google/android/apps/chrome/ntp/BookmarksListView;

    # getter for: Lcom/google/android/apps/chrome/ntp/BookmarksListView;->mTouchY:I
    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/BookmarksListView;->access$100(Lcom/google/android/apps/chrome/ntp/BookmarksListView;)I

    move-result v0

    iget-object v2, p0, Lcom/google/android/apps/chrome/ntp/BookmarksListView$1;->this$0:Lcom/google/android/apps/chrome/ntp/BookmarksListView;

    # getter for: Lcom/google/android/apps/chrome/ntp/BookmarksListView;->mUpperBound:I
    invoke-static {v2}, Lcom/google/android/apps/chrome/ntp/BookmarksListView;->access$400(Lcom/google/android/apps/chrome/ntp/BookmarksListView;)I

    move-result v2

    if-ge v0, v2, :cond_4

    .line 368
    const/16 v0, -0xa

    .line 376
    :goto_1
    iget-object v2, p0, Lcom/google/android/apps/chrome/ntp/BookmarksListView$1;->this$0:Lcom/google/android/apps/chrome/ntp/BookmarksListView;

    # getter for: Lcom/google/android/apps/chrome/ntp/BookmarksListView;->mIsScrolling:Z
    invoke-static {v2}, Lcom/google/android/apps/chrome/ntp/BookmarksListView;->access$000(Lcom/google/android/apps/chrome/ntp/BookmarksListView;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 379
    iget-object v2, p0, Lcom/google/android/apps/chrome/ntp/BookmarksListView$1;->this$0:Lcom/google/android/apps/chrome/ntp/BookmarksListView;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/ntp/BookmarksListView;->getFirstVisiblePosition()I

    move-result v2

    .line 380
    iget-object v3, p0, Lcom/google/android/apps/chrome/ntp/BookmarksListView$1;->this$0:Lcom/google/android/apps/chrome/ntp/BookmarksListView;

    invoke-virtual {v3, v1}, Lcom/google/android/apps/chrome/ntp/BookmarksListView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 381
    if-nez v3, :cond_6

    .line 382
    :goto_2
    sub-int v0, v1, v0

    .line 383
    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/BookmarksListView$1;->this$0:Lcom/google/android/apps/chrome/ntp/BookmarksListView;

    invoke-virtual {v1, v2, v0}, Lcom/google/android/apps/chrome/ntp/BookmarksListView;->setSelectionFromTop(II)V

    .line 384
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksListView$1;->this$0:Lcom/google/android/apps/chrome/ntp/BookmarksListView;

    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/BookmarksListView$1;->this$0:Lcom/google/android/apps/chrome/ntp/BookmarksListView;

    # invokes: Lcom/google/android/apps/chrome/ntp/BookmarksListView;->getItemToExpand()I
    invoke-static {v1}, Lcom/google/android/apps/chrome/ntp/BookmarksListView;->access$500(Lcom/google/android/apps/chrome/ntp/BookmarksListView;)I

    move-result v1

    # invokes: Lcom/google/android/apps/chrome/ntp/BookmarksListView;->expandView(I)V
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/ntp/BookmarksListView;->access$600(Lcom/google/android/apps/chrome/ntp/BookmarksListView;I)V

    .line 385
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksListView$1;->this$0:Lcom/google/android/apps/chrome/ntp/BookmarksListView;

    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/BookmarksListView$1;->this$0:Lcom/google/android/apps/chrome/ntp/BookmarksListView;

    # getter for: Lcom/google/android/apps/chrome/ntp/BookmarksListView;->mTouchY:I
    invoke-static {v1}, Lcom/google/android/apps/chrome/ntp/BookmarksListView;->access$100(Lcom/google/android/apps/chrome/ntp/BookmarksListView;)I

    move-result v1

    # invokes: Lcom/google/android/apps/chrome/ntp/BookmarksListView;->translateView(I)V
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/ntp/BookmarksListView;->access$700(Lcom/google/android/apps/chrome/ntp/BookmarksListView;I)V

    .line 386
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksListView$1;->this$0:Lcom/google/android/apps/chrome/ntp/BookmarksListView;

    const-wide/16 v2, 0x1

    invoke-virtual {v0, p0, v2, v3}, Lcom/google/android/apps/chrome/ntp/BookmarksListView;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 370
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksListView$1;->this$0:Lcom/google/android/apps/chrome/ntp/BookmarksListView;

    # getter for: Lcom/google/android/apps/chrome/ntp/BookmarksListView;->mTouchY:I
    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/BookmarksListView;->access$100(Lcom/google/android/apps/chrome/ntp/BookmarksListView;)I

    move-result v0

    iget-object v2, p0, Lcom/google/android/apps/chrome/ntp/BookmarksListView$1;->this$0:Lcom/google/android/apps/chrome/ntp/BookmarksListView;

    # getter for: Lcom/google/android/apps/chrome/ntp/BookmarksListView;->mLowerBound:I
    invoke-static {v2}, Lcom/google/android/apps/chrome/ntp/BookmarksListView;->access$200(Lcom/google/android/apps/chrome/ntp/BookmarksListView;)I

    move-result v2

    if-le v0, v2, :cond_5

    .line 371
    const/16 v0, 0xa

    goto :goto_1

    .line 373
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/BookmarksListView$1;->this$0:Lcom/google/android/apps/chrome/ntp/BookmarksListView;

    # invokes: Lcom/google/android/apps/chrome/ntp/BookmarksListView;->stopScrolling()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/BookmarksListView;->access$300(Lcom/google/android/apps/chrome/ntp/BookmarksListView;)V

    goto/16 :goto_0

    .line 381
    :cond_6
    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    move-result v1

    goto :goto_2
.end method
