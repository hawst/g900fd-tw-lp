.class Lcom/google/android/apps/chrome/firstrun/AccountFirstRunFragment$1;
.super Ljava/lang/Object;
.source "AccountFirstRunFragment.java"

# interfaces
.implements Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView$Listener;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/firstrun/AccountFirstRunFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/firstrun/AccountFirstRunFragment;)V
    .locals 0

    .prologue
    .line 40
    iput-object p1, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunFragment$1;->this$0:Lcom/google/android/apps/chrome/firstrun/AccountFirstRunFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAccountSelectionCanceled()V
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunFragment$1;->this$0:Lcom/google/android/apps/chrome/firstrun/AccountFirstRunFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunFragment;->getPageDelegate()Lcom/google/android/apps/chrome/firstrun/FirstRunPageDelegate;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/firstrun/FirstRunPageDelegate;->refuseSignIn()V

    .line 49
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunFragment$1;->this$0:Lcom/google/android/apps/chrome/firstrun/AccountFirstRunFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunFragment;->advanceToNextPage()V

    .line 50
    return-void
.end method

.method public onAccountSelectionConfirmed(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunFragment$1;->this$0:Lcom/google/android/apps/chrome/firstrun/AccountFirstRunFragment;

    # getter for: Lcom/google/android/apps/chrome/firstrun/AccountFirstRunFragment;->mView:Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;
    invoke-static {v0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunFragment;->access$000(Lcom/google/android/apps/chrome/firstrun/AccountFirstRunFragment;)Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunView;->switchToSignedMode()V

    .line 44
    return-void
.end method

.method public onFailedToSetForcedAccount(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunFragment$1;->this$0:Lcom/google/android/apps/chrome/firstrun/AccountFirstRunFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunFragment;->getPageDelegate()Lcom/google/android/apps/chrome/firstrun/FirstRunPageDelegate;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/firstrun/FirstRunPageDelegate;->abortFirstRunExperience()V

    .line 75
    return-void
.end method

.method public onNewAccount()V
    .locals 2

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunFragment$1;->this$0:Lcom/google/android/apps/chrome/firstrun/AccountFirstRunFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunFragment;->getPageDelegate()Lcom/google/android/apps/chrome/firstrun/FirstRunPageDelegate;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunFragment$1;->this$0:Lcom/google/android/apps/chrome/firstrun/AccountFirstRunFragment;

    invoke-interface {v0, v1}, Lcom/google/android/apps/chrome/firstrun/FirstRunPageDelegate;->openAccountAdder(Landroid/app/Fragment;)V

    .line 55
    return-void
.end method

.method public onSettingsButtonClicked(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunFragment$1;->this$0:Lcom/google/android/apps/chrome/firstrun/AccountFirstRunFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunFragment;->getPageDelegate()Lcom/google/android/apps/chrome/firstrun/FirstRunPageDelegate;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/apps/chrome/firstrun/FirstRunPageDelegate;->acceptSignIn(Ljava/lang/String;)V

    .line 66
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunFragment$1;->this$0:Lcom/google/android/apps/chrome/firstrun/AccountFirstRunFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunFragment;->getPageDelegate()Lcom/google/android/apps/chrome/firstrun/FirstRunPageDelegate;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/chrome/firstrun/FirstRunPageDelegate;->askToOpenSyncSettings()V

    .line 67
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunFragment$1;->this$0:Lcom/google/android/apps/chrome/firstrun/AccountFirstRunFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunFragment;->advanceToNextPage()V

    .line 68
    return-void
.end method

.method public onSigningInCompleted(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunFragment$1;->this$0:Lcom/google/android/apps/chrome/firstrun/AccountFirstRunFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunFragment;->getPageDelegate()Lcom/google/android/apps/chrome/firstrun/FirstRunPageDelegate;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/apps/chrome/firstrun/FirstRunPageDelegate;->acceptSignIn(Ljava/lang/String;)V

    .line 60
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunFragment$1;->this$0:Lcom/google/android/apps/chrome/firstrun/AccountFirstRunFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/firstrun/AccountFirstRunFragment;->advanceToNextPage()V

    .line 61
    return-void
.end method
