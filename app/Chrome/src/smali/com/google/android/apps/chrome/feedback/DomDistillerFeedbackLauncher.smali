.class public Lcom/google/android/apps/chrome/feedback/DomDistillerFeedbackLauncher;
.super Ljava/lang/Object;
.source "DomDistillerFeedbackLauncher.java"

# interfaces
.implements Lorg/chromium/chrome/browser/dom_distiller/ExternalFeedbackReporter;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static createExtraBundle(Ljava/lang/String;Z)Landroid/os/Bundle;
    .locals 3

    .prologue
    .line 31
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 32
    if-eqz p0, :cond_0

    const-string/jumbo v0, "URL"

    invoke-virtual {v1, v0, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 33
    :cond_0
    if-eqz p1, :cond_1

    const-string/jumbo v0, "good"

    .line 34
    :goto_0
    const-string/jumbo v2, "Distillation quality"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 35
    return-object v1

    .line 33
    :cond_1
    const-string/jumbo v0, "bad"

    goto :goto_0
.end method


# virtual methods
.method public reportFeedback(Landroid/app/Activity;Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 25
    invoke-static {p1}, Lcom/google/android/apps/chrome/feedback/GoogleFeedback;->prepareWithActivity(Landroid/app/Activity;)V

    .line 26
    invoke-static {p2, p3}, Lcom/google/android/apps/chrome/feedback/DomDistillerFeedbackLauncher;->createExtraBundle(Ljava/lang/String;Z)Landroid/os/Bundle;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/google/android/apps/chrome/feedback/GoogleFeedback;->launchWithExtras(Landroid/content/Context;Landroid/os/Bundle;)V

    .line 27
    invoke-static {}, Lcom/google/android/apps/chrome/feedback/GoogleFeedback;->cleanup()V

    .line 28
    return-void
.end method
