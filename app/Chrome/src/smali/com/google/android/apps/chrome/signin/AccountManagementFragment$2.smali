.class Lcom/google/android/apps/chrome/signin/AccountManagementFragment$2;
.super Ljava/lang/Object;
.source "AccountManagementFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/signin/AccountManagementFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/signin/AccountManagementFragment;)V
    .locals 0

    .prologue
    .line 274
    iput-object p1, p0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment$2;->this$0:Lcom/google/android/apps/chrome/signin/AccountManagementFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 277
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment$2;->this$0:Lcom/google/android/apps/chrome/signin/AccountManagementFragment;

    # getter for: Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->mGaiaServiceType:I
    invoke-static {v1}, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->access$000(Lcom/google/android/apps/chrome/signin/AccountManagementFragment;)I

    move-result v1

    invoke-static {v0, v1}, Lorg/chromium/chrome/browser/signin/AccountManagementScreenHelper;->logEvent(II)V

    .line 281
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.action.VIEW"

    const-string/jumbo v2, "googlechrome://navigate?url=chrome-native://newtab/"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 283
    const-string/jumbo v1, "com.google.android.apps.chrome.EXTRA_OPEN_NEW_INCOGNITO_TAB"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 284
    iget-object v1, p0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment$2;->this$0:Lcom/google/android/apps/chrome/signin/AccountManagementFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 285
    iget-object v1, p0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment$2;->this$0:Lcom/google/android/apps/chrome/signin/AccountManagementFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const-class v2, Lcom/google/android/apps/chrome/document/ChromeLauncherActivity;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 288
    const/high16 v1, 0x34020000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 291
    iget-object v1, p0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment$2;->this$0:Lcom/google/android/apps/chrome/signin/AccountManagementFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/IntentHandler;->startActivityForTrustedIntent(Landroid/content/Intent;Landroid/content/Context;)V

    .line 293
    iget-object v0, p0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment$2;->this$0:Lcom/google/android/apps/chrome/signin/AccountManagementFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/signin/AccountManagementFragment$2;->this$0:Lcom/google/android/apps/chrome/signin/AccountManagementFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/signin/AccountManagementFragment;->closeEditor()V

    .line 294
    :cond_0
    return-void
.end method
