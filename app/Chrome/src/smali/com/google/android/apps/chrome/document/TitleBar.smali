.class public Lcom/google/android/apps/chrome/document/TitleBar;
.super Landroid/widget/FrameLayout;
.source "TitleBar.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final TEXTBOX_COLOR:I


# instance fields
.field private mAnimation:Landroid/animation/AnimatorSet;

.field private mAnimationHolder:Landroid/view/View;

.field private mAppMenuButtonHelper:Lorg/chromium/chrome/browser/appmenu/AppMenuButtonHelper;

.field private mAppMenuHandler:Lorg/chromium/chrome/browser/appmenu/AppMenuHandler;

.field private mBoxOpaque:Landroid/graphics/drawable/Drawable;

.field private mBoxTransparent:Landroid/graphics/drawable/Drawable;

.field private mCurrentColor:I

.field private mDelegate:Lcom/google/android/apps/chrome/document/TitleBar$DocumentDelegate;

.field private mDocumentTitle:Landroid/widget/TextView;

.field private mDomain:Ljava/lang/String;

.field private mHomeActivityMode:Z

.field private mIsInitialized:Z

.field private mListeners:Lorg/chromium/base/ObserverList;

.field private mMenuButton:Landroid/widget/ImageButton;

.field private mMenuDark:Landroid/graphics/drawable/Drawable;

.field private mMenuLight:Landroid/graphics/drawable/Drawable;

.field private mMicButton:Landroid/widget/ImageButton;

.field private mSearchBox:Landroid/view/View;

.field private mSsbStateListener:Lcom/google/android/apps/chrome/document/ssb/SsbStateListener;

.field private mTextColorDark:I

.field private mTextColorLight:I

.field private mTitleBoxBackground:Lcom/google/android/apps/chrome/document/MaterialTouchHighlightView;

.field private mTouchCoordinates:[I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 41
    const-class v0, Lcom/google/android/apps/chrome/document/TitleBar;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/document/TitleBar;->$assertionsDisabled:Z

    .line 77
    const/16 v0, 0xbf

    invoke-static {v0, v1, v1, v1}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    sput v0, Lcom/google/android/apps/chrome/document/TitleBar;->TEXTBOX_COLOR:I

    return-void

    :cond_0
    move v0, v1

    .line 41
    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 133
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 93
    new-instance v0, Lorg/chromium/base/ObserverList;

    invoke-direct {v0}, Lorg/chromium/base/ObserverList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/document/TitleBar;->mListeners:Lorg/chromium/base/ObserverList;

    .line 95
    new-instance v0, Lcom/google/android/apps/chrome/document/TitleBar$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/document/TitleBar$1;-><init>(Lcom/google/android/apps/chrome/document/TitleBar;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/document/TitleBar;->mSsbStateListener:Lcom/google/android/apps/chrome/document/ssb/SsbStateListener;

    .line 105
    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/android/apps/chrome/document/TitleBar;->mTouchCoordinates:[I

    .line 134
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/document/TitleBar;Z)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/document/TitleBar;->setMicState(Z)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/document/TitleBar;)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/google/android/apps/chrome/document/TitleBar;->broadcastTitleLongClicked()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/document/TitleBar;)Lorg/chromium/chrome/browser/appmenu/AppMenuButtonHelper;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/TitleBar;->mAppMenuButtonHelper:Lorg/chromium/chrome/browser/appmenu/AppMenuButtonHelper;

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/android/apps/chrome/document/TitleBar;Landroid/animation/AnimatorSet;)Landroid/animation/AnimatorSet;
    .locals 0

    .prologue
    .line 41
    iput-object p1, p0, Lcom/google/android/apps/chrome/document/TitleBar;->mAnimation:Landroid/animation/AnimatorSet;

    return-object p1
.end method

.method static synthetic access$400(Lcom/google/android/apps/chrome/document/TitleBar;)Landroid/view/View;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/TitleBar;->mSearchBox:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/chrome/document/TitleBar;)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/google/android/apps/chrome/document/TitleBar;->broadcastTitleClicked()V

    return-void
.end method

.method private broadcastMicClicked()V
    .locals 2

    .prologue
    .line 434
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/TitleBar;->mListeners:Lorg/chromium/base/ObserverList;

    invoke-virtual {v0}, Lorg/chromium/base/ObserverList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/document/TitleBar$TitleBarListener;

    .line 435
    invoke-interface {v0}, Lcom/google/android/apps/chrome/document/TitleBar$TitleBarListener;->onMicClicked()V

    goto :goto_0

    .line 437
    :cond_0
    return-void
.end method

.method private broadcastTitleClicked()V
    .locals 2

    .prologue
    .line 422
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/TitleBar;->mListeners:Lorg/chromium/base/ObserverList;

    invoke-virtual {v0}, Lorg/chromium/base/ObserverList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/document/TitleBar$TitleBarListener;

    .line 423
    invoke-interface {v0}, Lcom/google/android/apps/chrome/document/TitleBar$TitleBarListener;->onTitleClicked()V

    goto :goto_0

    .line 425
    :cond_0
    return-void
.end method

.method private broadcastTitleLongClicked()V
    .locals 2

    .prologue
    .line 428
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/TitleBar;->mListeners:Lorg/chromium/base/ObserverList;

    invoke-virtual {v0}, Lorg/chromium/base/ObserverList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/document/TitleBar$TitleBarListener;

    .line 429
    invoke-interface {v0}, Lcom/google/android/apps/chrome/document/TitleBar$TitleBarListener;->onTitleLongClicked()V

    goto :goto_0

    .line 431
    :cond_0
    return-void
.end method

.method private static getSecurityIconResource(I)I
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 397
    packed-switch p0, :pswitch_data_0

    .line 408
    :pswitch_0
    sget-boolean v1, Lcom/google/android/apps/chrome/document/TitleBar;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 401
    :pswitch_1
    sget v0, Lcom/google/android/apps/chrome/R$drawable;->doc_omnibox_https_warning:I

    .line 410
    :cond_0
    :goto_0
    :pswitch_2
    return v0

    .line 403
    :pswitch_3
    sget v0, Lcom/google/android/apps/chrome/R$drawable;->omnibox_https_invalid:I

    goto :goto_0

    .line 406
    :pswitch_4
    sget v0, Lcom/google/android/apps/chrome/R$drawable;->doc_omnibox_https_valid:I

    goto :goto_0

    .line 397
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_4
        :pswitch_4
        :pswitch_1
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method private setMicState(Z)V
    .locals 2

    .prologue
    .line 441
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/TitleBar;->mMicButton:Landroid/widget/ImageButton;

    if-nez v0, :cond_0

    .line 444
    :goto_0
    return-void

    .line 442
    :cond_0
    if-eqz p1, :cond_1

    sget v0, Lcom/google/android/apps/chrome/R$drawable;->mic_on:I

    .line 443
    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/chrome/document/TitleBar;->mMicButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setImageResource(I)V

    goto :goto_0

    .line 442
    :cond_1
    sget v0, Lcom/google/android/apps/chrome/R$drawable;->mic_off:I

    goto :goto_1
.end method


# virtual methods
.method public addListener(Lcom/google/android/apps/chrome/document/TitleBar$TitleBarListener;)V
    .locals 1

    .prologue
    .line 414
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/TitleBar;->mListeners:Lorg/chromium/base/ObserverList;

    invoke-virtual {v0, p1}, Lorg/chromium/base/ObserverList;->addObserver(Ljava/lang/Object;)Z

    .line 415
    return-void
.end method

.method public animateTransitionFromSearch()V
    .locals 10

    .prologue
    const/high16 v9, 0x3f800000    # 1.0f

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 282
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/TitleBar;->mAnimation:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/document/TitleBar;->mAnimation:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->cancel()V

    .line 283
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/document/TitleBar;->mAnimation:Landroid/animation/AnimatorSet;

    .line 285
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/TitleBar;->mSearchBox:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    .line 304
    :goto_0
    return-void

    .line 288
    :cond_1
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/document/TitleBar;->mAnimation:Landroid/animation/AnimatorSet;

    .line 289
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/TitleBar;->mAnimation:Landroid/animation/AnimatorSet;

    new-instance v1, Lcom/google/android/apps/chrome/document/TitleBar$4;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/document/TitleBar$4;-><init>(Lcom/google/android/apps/chrome/document/TitleBar;)V

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 296
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/TitleBar;->mAnimation:Landroid/animation/AnimatorSet;

    const/4 v1, 0x3

    new-array v1, v1, [Landroid/animation/Animator;

    iget-object v2, p0, Lcom/google/android/apps/chrome/document/TitleBar;->mSearchBox:Landroid/view/View;

    const-string/jumbo v3, "alpha"

    new-array v4, v8, [F

    iget-object v5, p0, Lcom/google/android/apps/chrome/document/TitleBar;->mSearchBox:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getAlpha()F

    move-result v5

    aput v5, v4, v6

    const/4 v5, 0x0

    aput v5, v4, v7

    invoke-static {v2, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    aput-object v2, v1, v6

    iget-object v2, p0, Lcom/google/android/apps/chrome/document/TitleBar;->mTitleBoxBackground:Lcom/google/android/apps/chrome/document/MaterialTouchHighlightView;

    const-string/jumbo v3, "alpha"

    new-array v4, v8, [F

    iget-object v5, p0, Lcom/google/android/apps/chrome/document/TitleBar;->mTitleBoxBackground:Lcom/google/android/apps/chrome/document/MaterialTouchHighlightView;

    invoke-virtual {v5}, Lcom/google/android/apps/chrome/document/MaterialTouchHighlightView;->getAlpha()F

    move-result v5

    aput v5, v4, v6

    aput v9, v4, v7

    invoke-static {v2, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    aput-object v2, v1, v7

    iget-object v2, p0, Lcom/google/android/apps/chrome/document/TitleBar;->mAnimationHolder:Landroid/view/View;

    const-string/jumbo v3, "alpha"

    new-array v4, v8, [F

    iget-object v5, p0, Lcom/google/android/apps/chrome/document/TitleBar;->mAnimationHolder:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getAlpha()F

    move-result v5

    aput v5, v4, v6

    aput v9, v4, v7

    invoke-static {v2, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    aput-object v2, v1, v8

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 302
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/TitleBar;->mAnimation:Landroid/animation/AnimatorSet;

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v2, v3}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 303
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/TitleBar;->mAnimation:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    goto :goto_0
.end method

.method animateTransitionToSearch()V
    .locals 12

    .prologue
    const-wide/16 v10, 0x64

    const/4 v9, 0x0

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 308
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/document/TitleBar;->mIsInitialized:Z

    if-nez v0, :cond_0

    .line 349
    :goto_0
    return-void

    .line 309
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/TitleBar;->mAnimation:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/document/TitleBar;->mAnimation:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->cancel()V

    .line 310
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/document/TitleBar;->mAnimation:Landroid/animation/AnimatorSet;

    .line 318
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/TitleBar;->mSearchBox:Landroid/view/View;

    const-string/jumbo v1, "alpha"

    new-array v2, v8, [F

    iget-object v3, p0, Lcom/google/android/apps/chrome/document/TitleBar;->mSearchBox:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getAlpha()F

    move-result v3

    aput v3, v2, v6

    const/high16 v3, 0x3f800000    # 1.0f

    aput v3, v2, v7

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 320
    invoke-virtual {v0, v10, v11}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 322
    iget-object v1, p0, Lcom/google/android/apps/chrome/document/TitleBar;->mTitleBoxBackground:Lcom/google/android/apps/chrome/document/MaterialTouchHighlightView;

    const-string/jumbo v2, "alpha"

    new-array v3, v8, [F

    iget-object v4, p0, Lcom/google/android/apps/chrome/document/TitleBar;->mTitleBoxBackground:Lcom/google/android/apps/chrome/document/MaterialTouchHighlightView;

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/document/MaterialTouchHighlightView;->getAlpha()F

    move-result v4

    aput v4, v3, v6

    aput v9, v3, v7

    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 324
    invoke-virtual {v1, v10, v11}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 326
    iget-object v2, p0, Lcom/google/android/apps/chrome/document/TitleBar;->mAnimationHolder:Landroid/view/View;

    const-string/jumbo v3, "alpha"

    new-array v4, v8, [F

    iget-object v5, p0, Lcom/google/android/apps/chrome/document/TitleBar;->mAnimationHolder:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getAlpha()F

    move-result v5

    aput v5, v4, v6

    aput v9, v4, v7

    invoke-static {v2, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    .line 328
    const-wide/16 v4, 0x42

    invoke-virtual {v2, v4, v5}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 330
    new-instance v3, Landroid/animation/AnimatorSet;

    invoke-direct {v3}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v3, p0, Lcom/google/android/apps/chrome/document/TitleBar;->mAnimation:Landroid/animation/AnimatorSet;

    .line 334
    iget-object v3, p0, Lcom/google/android/apps/chrome/document/TitleBar;->mAnimation:Landroid/animation/AnimatorSet;

    const/4 v4, 0x3

    new-array v4, v4, [Landroid/animation/Animator;

    aput-object v0, v4, v6

    aput-object v1, v4, v7

    aput-object v2, v4, v8

    invoke-virtual {v3, v4}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 336
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/TitleBar;->mAnimation:Landroid/animation/AnimatorSet;

    new-instance v1, Lcom/google/android/apps/chrome/document/TitleBar$5;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/document/TitleBar$5;-><init>(Lcom/google/android/apps/chrome/document/TitleBar;)V

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 348
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/TitleBar;->mAnimation:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    goto :goto_0
.end method

.method public getMenuAnchor()Landroid/view/View;
    .locals 1

    .prologue
    .line 253
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/TitleBar;->mMenuButton:Landroid/widget/ImageButton;

    return-object v0
.end method

.method public initialize(Lcom/google/android/apps/chrome/document/TitleBar$DocumentDelegate;Lorg/chromium/chrome/browser/appmenu/AppMenuHandler;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 230
    iput-object p1, p0, Lcom/google/android/apps/chrome/document/TitleBar;->mDelegate:Lcom/google/android/apps/chrome/document/TitleBar$DocumentDelegate;

    .line 232
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/TitleBar;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/apps/chrome/document/TitleBar;->mDelegate:Lcom/google/android/apps/chrome/document/TitleBar$DocumentDelegate;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/document/TitleBar$DocumentDelegate;->isIncognito()Z

    move-result v0

    if-eqz v0, :cond_0

    sget v0, Lcom/google/android/apps/chrome/R$color;->incognito_primary_color:I

    :goto_0
    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 234
    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/document/TitleBar;->updateTheme(I)V

    .line 236
    iput-object p2, p0, Lcom/google/android/apps/chrome/document/TitleBar;->mAppMenuHandler:Lorg/chromium/chrome/browser/appmenu/AppMenuHandler;

    .line 237
    new-instance v0, Lorg/chromium/chrome/browser/appmenu/AppMenuButtonHelper;

    iget-object v1, p0, Lcom/google/android/apps/chrome/document/TitleBar;->mMenuButton:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/google/android/apps/chrome/document/TitleBar;->mAppMenuHandler:Lorg/chromium/chrome/browser/appmenu/AppMenuHandler;

    invoke-direct {v0, v1, v2}, Lorg/chromium/chrome/browser/appmenu/AppMenuButtonHelper;-><init>(Landroid/view/View;Lorg/chromium/chrome/browser/appmenu/AppMenuHandler;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/document/TitleBar;->mAppMenuButtonHelper:Lorg/chromium/chrome/browser/appmenu/AppMenuButtonHelper;

    .line 238
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/TitleBar;->mMenuButton:Landroid/widget/ImageButton;

    new-instance v1, Lcom/google/android/apps/chrome/document/TitleBar$3;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/document/TitleBar$3;-><init>(Lcom/google/android/apps/chrome/document/TitleBar;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 245
    invoke-virtual {p0, p3}, Lcom/google/android/apps/chrome/document/TitleBar;->onUrlChanged(Ljava/lang/String;)V

    .line 246
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/document/TitleBar;->mIsInitialized:Z

    .line 247
    return-void

    .line 232
    :cond_0
    sget v0, Lcom/google/android/apps/chrome/R$color;->default_primary_color:I

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 271
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/document/TitleBar;->mIsInitialized:Z

    if-nez v0, :cond_1

    .line 278
    :cond_0
    :goto_0
    return-void

    .line 273
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/TitleBar;->mDocumentTitle:Landroid/widget/TextView;

    if-ne p1, v0, :cond_2

    .line 274
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/TitleBar;->mAnimation:Landroid/animation/AnimatorSet;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/TitleBar;->animateTransitionToSearch()V

    goto :goto_0

    .line 275
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/TitleBar;->mMicButton:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_0

    .line 276
    invoke-direct {p0}, Lcom/google/android/apps/chrome/document/TitleBar;->broadcastMicClicked()V

    goto :goto_0
.end method

.method public onFinishInflate()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 138
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 141
    sget v0, Lcom/google/android/apps/chrome/R$id;->animation_holder:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/document/TitleBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/document/TitleBar;->mAnimationHolder:Landroid/view/View;

    .line 143
    sget v0, Lcom/google/android/apps/chrome/R$id;->document_box_background:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/document/TitleBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/document/MaterialTouchHighlightView;

    iput-object v0, p0, Lcom/google/android/apps/chrome/document/TitleBar;->mTitleBoxBackground:Lcom/google/android/apps/chrome/document/MaterialTouchHighlightView;

    .line 145
    sget v0, Lcom/google/android/apps/chrome/R$id;->document_box_overlay:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/document/TitleBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/document/TitleBar;->mSearchBox:Landroid/view/View;

    .line 147
    sget v0, Lcom/google/android/apps/chrome/R$id;->menu:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/document/TitleBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/google/android/apps/chrome/document/TitleBar;->mMenuButton:Landroid/widget/ImageButton;

    .line 149
    sget v0, Lcom/google/android/apps/chrome/R$id;->mic:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/document/TitleBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/google/android/apps/chrome/document/TitleBar;->mMicButton:Landroid/widget/ImageButton;

    .line 150
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/TitleBar;->mMicButton:Landroid/widget/ImageButton;

    if-eqz v0, :cond_0

    iput-boolean v3, p0, Lcom/google/android/apps/chrome/document/TitleBar;->mHomeActivityMode:Z

    .line 151
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/document/TitleBar;->mHomeActivityMode:Z

    if-eqz v0, :cond_1

    .line 152
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/TitleBar;->mMicButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 153
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/TitleBar;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 155
    const-string/jumbo v1, "show_hotword_enabled"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 157
    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/document/TitleBar;->setMicState(Z)V

    .line 160
    :cond_1
    sget v0, Lcom/google/android/apps/chrome/R$id;->document_url:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/document/TitleBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/chrome/document/TitleBar;->mDocumentTitle:Landroid/widget/TextView;

    .line 161
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/TitleBar;->mDocumentTitle:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setClickable(Z)V

    .line 162
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/TitleBar;->mDocumentTitle:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 163
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/TitleBar;->mDocumentTitle:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 164
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/TitleBar;->mDocumentTitle:Landroid/widget/TextView;

    new-instance v1, Lcom/google/android/apps/chrome/document/TitleBar$2;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/document/TitleBar$2;-><init>(Lcom/google/android/apps/chrome/document/TitleBar;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 173
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/TitleBar;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 174
    sget v1, Lcom/google/android/apps/chrome/R$drawable;->doc_omnibox_transparent:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/chrome/document/TitleBar;->mBoxTransparent:Landroid/graphics/drawable/Drawable;

    .line 175
    sget v1, Lcom/google/android/apps/chrome/R$drawable;->doc_omnibox:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/chrome/document/TitleBar;->mBoxOpaque:Landroid/graphics/drawable/Drawable;

    .line 176
    sget v1, Lcom/google/android/apps/chrome/R$color;->titlebar_dark_text:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/chrome/document/TitleBar;->mTextColorDark:I

    .line 177
    sget v1, Lcom/google/android/apps/chrome/R$color;->titlebar_light_text:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/chrome/document/TitleBar;->mTextColorLight:I

    .line 178
    sget v1, Lcom/google/android/apps/chrome/R$drawable;->btn_menu:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/chrome/document/TitleBar;->mMenuDark:Landroid/graphics/drawable/Drawable;

    .line 179
    sget v1, Lcom/google/android/apps/chrome/R$drawable;->btn_menu_white:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/document/TitleBar;->mMenuLight:Landroid/graphics/drawable/Drawable;

    .line 182
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/TitleBar;->mTitleBoxBackground:Lcom/google/android/apps/chrome/document/MaterialTouchHighlightView;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/document/MaterialTouchHighlightView;->initializeBackgroundParameters()V

    .line 184
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/TitleBar;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$dimen;->document_titlebar_box_margin:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 185
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    .line 186
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/TitleBar;->mSearchBox:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    .line 187
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/TitleBar;->mSearchBox:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 188
    iget v3, v2, Landroid/graphics/Rect;->top:I

    sub-int v3, v1, v3

    iput v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 189
    iget v3, v2, Landroid/graphics/Rect;->left:I

    sub-int v3, v1, v3

    iput v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 190
    iget v3, v2, Landroid/graphics/Rect;->right:I

    sub-int v3, v1, v3

    iput v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    .line 191
    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    .line 192
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/TitleBar;->requestLayout()V

    .line 193
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 207
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/TitleBar;->mTouchCoordinates:[I

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/document/TitleBar;->mTitleBoxBackground:Lcom/google/android/apps/chrome/document/MaterialTouchHighlightView;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/document/MaterialTouchHighlightView;->getLeft()I

    move-result v2

    sub-int/2addr v1, v2

    aput v1, v0, v3

    .line 208
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/TitleBar;->mTouchCoordinates:[I

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    float-to-int v1, v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/document/TitleBar;->mTitleBoxBackground:Lcom/google/android/apps/chrome/document/MaterialTouchHighlightView;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/document/MaterialTouchHighlightView;->getTop()I

    move-result v2

    sub-int/2addr v1, v2

    aput v1, v0, v4

    .line 210
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/TitleBar;->mTitleBoxBackground:Lcom/google/android/apps/chrome/document/MaterialTouchHighlightView;

    iget-object v1, p0, Lcom/google/android/apps/chrome/document/TitleBar;->mTouchCoordinates:[I

    aget v1, v1, v3

    iget-object v2, p0, Lcom/google/android/apps/chrome/document/TitleBar;->mTouchCoordinates:[I

    aget v2, v2, v4

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/document/MaterialTouchHighlightView;->setTouchHighlightCoordinates(II)V

    .line 213
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    .line 214
    if-nez v0, :cond_1

    .line 215
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/TitleBar;->mTitleBoxBackground:Lcom/google/android/apps/chrome/document/MaterialTouchHighlightView;

    invoke-virtual {v0, v4}, Lcom/google/android/apps/chrome/document/MaterialTouchHighlightView;->setTouchHighlightVisible(Z)V

    .line 219
    :cond_0
    :goto_0
    return v3

    .line 216
    :cond_1
    if-eq v0, v4, :cond_2

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 217
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/TitleBar;->mTitleBoxBackground:Lcom/google/android/apps/chrome/document/MaterialTouchHighlightView;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/chrome/document/MaterialTouchHighlightView;->setTouchHighlightVisible(Z)V

    goto :goto_0
.end method

.method onSSLStateChanged(I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 356
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/TitleBar;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$color;->default_primary_color:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 357
    const/4 v1, 0x5

    if-ne p1, v1, :cond_0

    .line 358
    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/document/TitleBar;->updateTheme(I)V

    .line 360
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/TitleBar;->mDocumentTitle:Landroid/widget/TextView;

    invoke-static {p1}, Lcom/google/android/apps/chrome/document/TitleBar;->getSecurityIconResource(I)I

    move-result v1

    invoke-static {v0, v1, v2, v2, v2}, Lorg/chromium/base/ApiCompatibilityUtils;->setCompoundDrawablesRelativeWithIntrinsicBounds(Landroid/widget/TextView;IIII)V

    .line 362
    return-void
.end method

.method public onUrlChanged(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 261
    const-string/jumbo v0, "chrome-native://"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 262
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/TitleBar;->mDocumentTitle:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 267
    :goto_0
    return-void

    .line 264
    :cond_0
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/document/TitleBar;->mDomain:Ljava/lang/String;

    .line 265
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/TitleBar;->mDocumentTitle:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/chrome/document/TitleBar;->mDomain:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public onWindowFocusChanged(Z)V
    .locals 2

    .prologue
    .line 197
    if-eqz p1, :cond_0

    .line 198
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/TitleBar;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/document/ssb/ChromeSsbServiceCallback;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/document/ssb/ChromeSsbServiceCallback;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/document/TitleBar;->mSsbStateListener:Lcom/google/android/apps/chrome/document/ssb/SsbStateListener;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/document/ssb/ChromeSsbServiceCallback;->addListener(Lcom/google/android/apps/chrome/document/ssb/SsbStateListener;)V

    .line 202
    :goto_0
    return-void

    .line 200
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/TitleBar;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/document/ssb/ChromeSsbServiceCallback;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/document/ssb/ChromeSsbServiceCallback;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/document/TitleBar;->mSsbStateListener:Lcom/google/android/apps/chrome/document/ssb/SsbStateListener;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/document/ssb/ChromeSsbServiceCallback;->removeListener(Lcom/google/android/apps/chrome/document/ssb/SsbStateListener;)V

    goto :goto_0
.end method

.method updateTheme(I)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 369
    iget v0, p0, Lcom/google/android/apps/chrome/document/TitleBar;->mCurrentColor:I

    if-ne v0, p1, :cond_0

    .line 394
    :goto_0
    return-void

    .line 370
    :cond_0
    iput p1, p0, Lcom/google/android/apps/chrome/document/TitleBar;->mCurrentColor:I

    .line 377
    iget v0, p0, Lcom/google/android/apps/chrome/document/TitleBar;->mCurrentColor:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/document/TitleBar;->setBackgroundColor(I)V

    .line 379
    iget v0, p0, Lcom/google/android/apps/chrome/document/TitleBar;->mCurrentColor:I

    invoke-static {v0}, Lcom/google/android/apps/chrome/document/BrandColorUtils;->getLightnessForColor(I)F

    move-result v1

    .line 381
    invoke-static {v1}, Lcom/google/android/apps/chrome/document/BrandColorUtils;->shouldUseLightDrawablesForToolbar(F)Z

    move-result v2

    .line 383
    iget-object v3, p0, Lcom/google/android/apps/chrome/document/TitleBar;->mDocumentTitle:Landroid/widget/TextView;

    if-eqz v2, :cond_1

    iget v0, p0, Lcom/google/android/apps/chrome/document/TitleBar;->mTextColorLight:I

    :goto_1
    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 384
    iget-object v3, p0, Lcom/google/android/apps/chrome/document/TitleBar;->mMenuButton:Landroid/widget/ImageButton;

    if-eqz v2, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/document/TitleBar;->mMenuLight:Landroid/graphics/drawable/Drawable;

    :goto_2
    invoke-virtual {v3, v0}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 386
    invoke-static {v1}, Lcom/google/android/apps/chrome/document/BrandColorUtils;->shouldUseOpaqueTextboxBackground(F)Z

    move-result v1

    .line 388
    iget-object v2, p0, Lcom/google/android/apps/chrome/document/TitleBar;->mTitleBoxBackground:Lcom/google/android/apps/chrome/document/MaterialTouchHighlightView;

    if-eqz v1, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/chrome/document/TitleBar;->mBoxOpaque:Landroid/graphics/drawable/Drawable;

    :goto_3
    invoke-static {v2, v0}, Lorg/chromium/base/ApiCompatibilityUtils;->setBackgroundForView(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 390
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/TitleBar;->mTitleBoxBackground:Lcom/google/android/apps/chrome/document/MaterialTouchHighlightView;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/document/MaterialTouchHighlightView;->setTouchHighlightIsOpaque(Z)V

    .line 392
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/TitleBar;->mDocumentTitle:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/chrome/document/TitleBar;->mDelegate:Lcom/google/android/apps/chrome/document/TitleBar$DocumentDelegate;

    invoke-interface {v1}, Lcom/google/android/apps/chrome/document/TitleBar$DocumentDelegate;->getSecurityLevel()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/chrome/document/TitleBar;->getSecurityIconResource(I)I

    move-result v1

    invoke-static {v0, v1, v4, v4, v4}, Lorg/chromium/base/ApiCompatibilityUtils;->setCompoundDrawablesRelativeWithIntrinsicBounds(Landroid/widget/TextView;IIII)V

    goto :goto_0

    .line 383
    :cond_1
    iget v0, p0, Lcom/google/android/apps/chrome/document/TitleBar;->mTextColorDark:I

    goto :goto_1

    .line 384
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/TitleBar;->mMenuDark:Landroid/graphics/drawable/Drawable;

    goto :goto_2

    .line 388
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/TitleBar;->mBoxTransparent:Landroid/graphics/drawable/Drawable;

    goto :goto_3
.end method
