.class public Lcom/google/android/apps/chrome/toolbar/ToolbarProgressBar;
.super Lcom/google/android/apps/chrome/widget/SmoothProgressBar;
.source "ToolbarProgressBar.java"


# instance fields
.field private final mClearLoadProgressRunnable:Ljava/lang/Runnable;

.field private mDesiredVisibility:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 30
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/widget/SmoothProgressBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 33
    invoke-virtual {p0, v2}, Lcom/google/android/apps/chrome/toolbar/ToolbarProgressBar;->setVisibility(I)V

    .line 35
    new-instance v0, Lcom/google/android/apps/chrome/toolbar/ToolbarProgressBar$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarProgressBar$1;-><init>(Lcom/google/android/apps/chrome/toolbar/ToolbarProgressBar;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarProgressBar;->mClearLoadProgressRunnable:Ljava/lang/Runnable;

    .line 43
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarProgressBar;->getProgressDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 44
    instance-of v1, v0, Landroid/graphics/drawable/LayerDrawable;

    if-eqz v1, :cond_0

    .line 45
    check-cast v0, Landroid/graphics/drawable/LayerDrawable;

    const/high16 v1, 0x1020000

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/LayerDrawable;->findDrawableByLayerId(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 47
    if-eqz v0, :cond_0

    .line 48
    invoke-virtual {v0, v2, v2}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    .line 49
    invoke-virtual {v0, v2}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 52
    :cond_0
    return-void
.end method

.method private setVisibilityForProgress()V
    .locals 2

    .prologue
    .line 84
    iget v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarProgressBar;->mDesiredVisibility:I

    if-eqz v0, :cond_0

    .line 85
    iget v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarProgressBar;->mDesiredVisibility:I

    invoke-super {p0, v0}, Lcom/google/android/apps/chrome/widget/SmoothProgressBar;->setVisibility(I)V

    .line 91
    :goto_0
    return-void

    .line 89
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarProgressBar;->getProgress()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarProgressBar;->getSecondaryProgress()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 90
    if-nez v0, :cond_1

    const/4 v0, 0x4

    :goto_1
    invoke-super {p0, v0}, Lcom/google/android/apps/chrome/widget/SmoothProgressBar;->setVisibility(I)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public declared-synchronized setProgress(I)V
    .locals 1

    .prologue
    .line 56
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarProgressBar;->mClearLoadProgressRunnable:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/toolbar/ToolbarProgressBar;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 57
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/widget/SmoothProgressBar;->setProgress(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 58
    monitor-exit p0

    return-void

    .line 56
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected setProgressInternal(I)V
    .locals 4

    .prologue
    .line 68
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/widget/SmoothProgressBar;->setProgressInternal(I)V

    .line 70
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarProgressBar;->getMax()I

    move-result v0

    if-ne p1, v0, :cond_0

    .line 71
    iget-object v0, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarProgressBar;->mClearLoadProgressRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0xc8

    invoke-virtual {p0, v0, v2, v3}, Lcom/google/android/apps/chrome/toolbar/ToolbarProgressBar;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 74
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarProgressBar;->setVisibilityForProgress()V

    .line 75
    return-void
.end method

.method public declared-synchronized setSecondaryProgress(I)V
    .locals 1

    .prologue
    .line 62
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/widget/SmoothProgressBar;->setSecondaryProgress(I)V

    .line 63
    invoke-direct {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarProgressBar;->setVisibilityForProgress()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 64
    monitor-exit p0

    return-void

    .line 62
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setVisibility(I)V
    .locals 0

    .prologue
    .line 79
    iput p1, p0, Lcom/google/android/apps/chrome/toolbar/ToolbarProgressBar;->mDesiredVisibility:I

    .line 80
    invoke-direct {p0}, Lcom/google/android/apps/chrome/toolbar/ToolbarProgressBar;->setVisibilityForProgress()V

    .line 81
    return-void
.end method
