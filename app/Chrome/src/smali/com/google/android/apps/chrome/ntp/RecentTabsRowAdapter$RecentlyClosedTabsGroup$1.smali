.class Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$RecentlyClosedTabsGroup$1;
.super Ljava/lang/Object;
.source "RecentTabsRowAdapter.java"

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field final synthetic this$1:Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$RecentlyClosedTabsGroup;

.field final synthetic val$recentlyClosedTab:Lorg/chromium/chrome/browser/RecentlyClosedBridge$RecentlyClosedTab;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 535
    const-class v0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$RecentlyClosedTabsGroup$1;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$RecentlyClosedTabsGroup;Lorg/chromium/chrome/browser/RecentlyClosedBridge$RecentlyClosedTab;)V
    .locals 0

    .prologue
    .line 535
    iput-object p1, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$RecentlyClosedTabsGroup$1;->this$1:Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$RecentlyClosedTabsGroup;

    iput-object p2, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$RecentlyClosedTabsGroup$1;->val$recentlyClosedTab:Lorg/chromium/chrome/browser/RecentlyClosedBridge$RecentlyClosedTab;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    .line 538
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 547
    sget-boolean v0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$RecentlyClosedTabsGroup$1;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 540
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$RecentlyClosedTabsGroup$1;->this$1:Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$RecentlyClosedTabsGroup;

    iget-object v0, v0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$RecentlyClosedTabsGroup;->this$0:Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;

    # getter for: Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->mRecentTabsManager:Lcom/google/android/apps/chrome/ntp/RecentTabsManager;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->access$300(Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;)Lcom/google/android/apps/chrome/ntp/RecentTabsManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->clearRecentlyClosedTabs()V

    .line 549
    :cond_0
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 543
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$RecentlyClosedTabsGroup$1;->this$1:Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$RecentlyClosedTabsGroup;

    iget-object v0, v0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$RecentlyClosedTabsGroup;->this$0:Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;

    # getter for: Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->mRecentTabsManager:Lcom/google/android/apps/chrome/ntp/RecentTabsManager;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;->access$300(Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter;)Lcom/google/android/apps/chrome/ntp/RecentTabsManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/RecentTabsRowAdapter$RecentlyClosedTabsGroup$1;->val$recentlyClosedTab:Lorg/chromium/chrome/browser/RecentlyClosedBridge$RecentlyClosedTab;

    const/4 v2, 0x5

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/chrome/ntp/RecentTabsManager;->openRecentlyClosedTab(Lorg/chromium/chrome/browser/RecentlyClosedBridge$RecentlyClosedTab;I)V

    goto :goto_0

    .line 538
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
