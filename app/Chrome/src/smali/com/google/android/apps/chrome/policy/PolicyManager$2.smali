.class Lcom/google/android/apps/chrome/policy/PolicyManager$2;
.super Ljava/lang/Object;
.source "PolicyManager.java"

# interfaces
.implements Lorg/chromium/chrome/browser/autofill/AutofillLogger$Logger;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/policy/PolicyManager;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/policy/PolicyManager;)V
    .locals 0

    .prologue
    .line 130
    iput-object p1, p0, Lcom/google/android/apps/chrome/policy/PolicyManager$2;->this$0:Lcom/google/android/apps/chrome/policy/PolicyManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public didFillField(Lorg/chromium/chrome/browser/autofill/AutofillLogger$LogEntry;)V
    .locals 5

    .prologue
    .line 133
    iget-object v0, p0, Lcom/google/android/apps/chrome/policy/PolicyManager$2;->this$0:Lcom/google/android/apps/chrome/policy/PolicyManager;

    # getter for: Lcom/google/android/apps/chrome/policy/PolicyManager;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/google/android/apps/chrome/policy/PolicyManager;->access$100(Lcom/google/android/apps/chrome/policy/PolicyManager;)Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$AuditEvent;->AUTOFILL_SELECTED:Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$AuditEvent;

    const-string/jumbo v2, "Chrome_module"

    invoke-virtual {p1}, Lorg/chromium/chrome/browser/autofill/AutofillLogger$LogEntry;->getAutofilledValue()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lorg/chromium/chrome/browser/autofill/AutofillLogger$LogEntry;->getProfileFullName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger;->logIfNecessary(Landroid/content/Context;Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$AuditEvent;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    return-void
.end method
