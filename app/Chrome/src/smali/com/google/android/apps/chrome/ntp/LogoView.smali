.class public Lcom/google/android/apps/chrome/ntp/LogoView;
.super Landroid/view/View;
.source "LogoView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static sDefaultLogo:Ljava/lang/ref/WeakReference;


# instance fields
.field private mAnimation:Landroid/animation/ObjectAnimator;

.field private mLogo:Landroid/graphics/Bitmap;

.field private mLogoIsDefault:Z

.field private mLogoMatrix:Landroid/graphics/Matrix;

.field private mManager:Lcom/google/android/apps/chrome/ntp/NewTabPageView$NewTabPageManager;

.field private mNewLogo:Landroid/graphics/Bitmap;

.field private mNewLogoIsDefault:Z

.field private mNewLogoMatrix:Landroid/graphics/Matrix;

.field private mPaint:Landroid/graphics/Paint;

.field private mTransitionAmount:F

.field private final mTransitionProperty:Landroid/util/Property;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 79
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 57
    new-instance v0, Lcom/google/android/apps/chrome/ntp/LogoView$1;

    const-class v1, Ljava/lang/Float;

    const-string/jumbo v2, ""

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/apps/chrome/ntp/LogoView$1;-><init>(Lcom/google/android/apps/chrome/ntp/LogoView;Ljava/lang/Class;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/ntp/LogoView;->mTransitionProperty:Landroid/util/Property;

    .line 81
    invoke-direct {p0}, Lcom/google/android/apps/chrome/ntp/LogoView;->getDefaultLogo()Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/ntp/LogoView;->mLogo:Landroid/graphics/Bitmap;

    .line 82
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/ntp/LogoView;->mLogoMatrix:Landroid/graphics/Matrix;

    .line 83
    iput-boolean v3, p0, Lcom/google/android/apps/chrome/ntp/LogoView;->mLogoIsDefault:Z

    .line 85
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/ntp/LogoView;->mPaint:Landroid/graphics/Paint;

    .line 86
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/LogoView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 87
    invoke-virtual {p0, p0}, Lcom/google/android/apps/chrome/ntp/LogoView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 88
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/ntp/LogoView;)F
    .locals 1

    .prologue
    .line 31
    iget v0, p0, Lcom/google/android/apps/chrome/ntp/LogoView;->mTransitionAmount:F

    return v0
.end method

.method static synthetic access$002(Lcom/google/android/apps/chrome/ntp/LogoView;F)F
    .locals 0

    .prologue
    .line 31
    iput p1, p0, Lcom/google/android/apps/chrome/ntp/LogoView;->mTransitionAmount:F

    return p1
.end method

.method static synthetic access$102(Lcom/google/android/apps/chrome/ntp/LogoView;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 0

    .prologue
    .line 31
    iput-object p1, p0, Lcom/google/android/apps/chrome/ntp/LogoView;->mLogo:Landroid/graphics/Bitmap;

    return-object p1
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/ntp/LogoView;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/LogoView;->mNewLogo:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic access$202(Lcom/google/android/apps/chrome/ntp/LogoView;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 0

    .prologue
    .line 31
    iput-object p1, p0, Lcom/google/android/apps/chrome/ntp/LogoView;->mNewLogo:Landroid/graphics/Bitmap;

    return-object p1
.end method

.method static synthetic access$302(Lcom/google/android/apps/chrome/ntp/LogoView;Landroid/graphics/Matrix;)Landroid/graphics/Matrix;
    .locals 0

    .prologue
    .line 31
    iput-object p1, p0, Lcom/google/android/apps/chrome/ntp/LogoView;->mLogoMatrix:Landroid/graphics/Matrix;

    return-object p1
.end method

.method static synthetic access$400(Lcom/google/android/apps/chrome/ntp/LogoView;)Landroid/graphics/Matrix;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/LogoView;->mNewLogoMatrix:Landroid/graphics/Matrix;

    return-object v0
.end method

.method static synthetic access$402(Lcom/google/android/apps/chrome/ntp/LogoView;Landroid/graphics/Matrix;)Landroid/graphics/Matrix;
    .locals 0

    .prologue
    .line 31
    iput-object p1, p0, Lcom/google/android/apps/chrome/ntp/LogoView;->mNewLogoMatrix:Landroid/graphics/Matrix;

    return-object p1
.end method

.method static synthetic access$502(Lcom/google/android/apps/chrome/ntp/LogoView;Z)Z
    .locals 0

    .prologue
    .line 31
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/ntp/LogoView;->mLogoIsDefault:Z

    return p1
.end method

.method static synthetic access$600(Lcom/google/android/apps/chrome/ntp/LogoView;)Z
    .locals 1

    .prologue
    .line 31
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/ntp/LogoView;->mNewLogoIsDefault:Z

    return v0
.end method

.method static synthetic access$702(Lcom/google/android/apps/chrome/ntp/LogoView;Landroid/animation/ObjectAnimator;)Landroid/animation/ObjectAnimator;
    .locals 0

    .prologue
    .line 31
    iput-object p1, p0, Lcom/google/android/apps/chrome/ntp/LogoView;->mAnimation:Landroid/animation/ObjectAnimator;

    return-object p1
.end method

.method private getDefaultLogo()Landroid/graphics/Bitmap;
    .locals 2

    .prologue
    .line 194
    sget-object v0, Lcom/google/android/apps/chrome/ntp/LogoView;->sDefaultLogo:Ljava/lang/ref/WeakReference;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 195
    :goto_0
    if-nez v0, :cond_0

    .line 196
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ntp/LogoView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$drawable;->google_logo:I

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 197
    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    sput-object v1, Lcom/google/android/apps/chrome/ntp/LogoView;->sDefaultLogo:Ljava/lang/ref/WeakReference;

    .line 199
    :cond_0
    return-object v0

    .line 194
    :cond_1
    sget-object v0, Lcom/google/android/apps/chrome/ntp/LogoView;->sDefaultLogo:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    goto :goto_0
.end method

.method private isTransitioning()Z
    .locals 2

    .prologue
    .line 164
    iget v0, p0, Lcom/google/android/apps/chrome/ntp/LogoView;->mTransitionAmount:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setMatrix(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Z)V
    .locals 8

    .prologue
    const/high16 v7, 0x3f000000    # 0.5f

    .line 175
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ntp/LogoView;->getWidth()I

    move-result v1

    .line 176
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/ntp/LogoView;->getHeight()I

    move-result v2

    .line 177
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    .line 178
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    .line 180
    int-to-float v0, v1

    int-to-float v5, v3

    div-float/2addr v0, v5

    int-to-float v5, v2

    int-to-float v6, v4

    div-float/2addr v5, v6

    invoke-static {v0, v5}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 181
    if-eqz p3, :cond_0

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-static {v5, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 183
    :cond_0
    int-to-float v1, v1

    int-to-float v3, v3

    mul-float/2addr v3, v0

    sub-float/2addr v1, v3

    mul-float/2addr v1, v7

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    .line 184
    int-to-float v2, v2

    int-to-float v3, v4

    mul-float/2addr v3, v0

    sub-float/2addr v2, v3

    mul-float/2addr v2, v7

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    .line 186
    invoke-virtual {p2, v0, v0}, Landroid/graphics/Matrix;->setScale(FF)V

    .line 187
    int-to-float v0, v1

    int-to-float v1, v2

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 188
    return-void
.end method

.method private updateLogo(Landroid/graphics/Bitmap;Ljava/lang/String;Z)V
    .locals 4

    .prologue
    .line 121
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/LogoView;->mAnimation:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/LogoView;->mAnimation:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->end()V

    .line 123
    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/chrome/ntp/LogoView;->mNewLogo:Landroid/graphics/Bitmap;

    .line 124
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/ntp/LogoView;->mNewLogoMatrix:Landroid/graphics/Matrix;

    .line 125
    iput-boolean p3, p0, Lcom/google/android/apps/chrome/ntp/LogoView;->mNewLogoIsDefault:Z

    .line 126
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/LogoView;->mNewLogo:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/LogoView;->mNewLogoMatrix:Landroid/graphics/Matrix;

    iget-boolean v2, p0, Lcom/google/android/apps/chrome/ntp/LogoView;->mNewLogoIsDefault:Z

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/apps/chrome/ntp/LogoView;->setMatrix(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Z)V

    .line 128
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/LogoView;->mTransitionProperty:Landroid/util/Property;

    const/4 v1, 0x2

    new-array v1, v1, [F

    fill-array-data v1, :array_0

    invoke-static {p0, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/ntp/LogoView;->mAnimation:Landroid/animation/ObjectAnimator;

    .line 129
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/LogoView;->mAnimation:Landroid/animation/ObjectAnimator;

    const-wide/16 v2, 0x190

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 130
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/LogoView;->mAnimation:Landroid/animation/ObjectAnimator;

    new-instance v1, Lcom/google/android/apps/chrome/ntp/LogoView$2;

    invoke-direct {v1, p0, p2}, Lcom/google/android/apps/chrome/ntp/LogoView$2;-><init>(Lcom/google/android/apps/chrome/ntp/LogoView;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 157
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/LogoView;->mAnimation:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 158
    return-void

    .line 128
    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method


# virtual methods
.method public endAnimation()V
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/LogoView;->mAnimation:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_0

    .line 102
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/LogoView;->mAnimation:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->end()V

    .line 103
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/ntp/LogoView;->mAnimation:Landroid/animation/ObjectAnimator;

    .line 105
    :cond_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 231
    if-ne p1, p0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/LogoView;->mManager:Lcom/google/android/apps/chrome/ntp/NewTabPageView$NewTabPageManager;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/ntp/LogoView;->isTransitioning()Z

    move-result v0

    if-nez v0, :cond_0

    .line 232
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/LogoView;->mManager:Lcom/google/android/apps/chrome/ntp/NewTabPageView$NewTabPageManager;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/ntp/NewTabPageView$NewTabPageManager;->openLogoLink()V

    .line 234
    :cond_0
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 5

    .prologue
    const/high16 v4, 0x43ff0000    # 510.0f

    const/high16 v3, 0x3f000000    # 0.5f

    const/4 v2, 0x0

    .line 204
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/LogoView;->mLogo:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/chrome/ntp/LogoView;->mTransitionAmount:F

    cmpg-float v0, v0, v3

    if-gez v0, :cond_0

    .line 205
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/LogoView;->mPaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/google/android/apps/chrome/ntp/LogoView;->mTransitionAmount:F

    sub-float v1, v3, v1

    mul-float/2addr v1, v4

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 206
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 207
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/LogoView;->mLogoMatrix:Landroid/graphics/Matrix;

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->concat(Landroid/graphics/Matrix;)V

    .line 208
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/LogoView;->mLogo:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/LogoView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v2, v2, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 209
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 212
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/LogoView;->mNewLogo:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/google/android/apps/chrome/ntp/LogoView;->mTransitionAmount:F

    cmpl-float v0, v0, v3

    if-lez v0, :cond_1

    .line 213
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/LogoView;->mPaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/google/android/apps/chrome/ntp/LogoView;->mTransitionAmount:F

    sub-float/2addr v1, v3

    mul-float/2addr v1, v4

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 214
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 215
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/LogoView;->mNewLogoMatrix:Landroid/graphics/Matrix;

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->concat(Landroid/graphics/Matrix;)V

    .line 216
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/LogoView;->mNewLogo:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/LogoView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v2, v2, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 217
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 219
    :cond_1
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 3

    .prologue
    .line 223
    if-ne p1, p3, :cond_0

    if-eq p2, p4, :cond_2

    .line 224
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/LogoView;->mLogo:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/LogoView;->mLogo:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/LogoView;->mLogoMatrix:Landroid/graphics/Matrix;

    iget-boolean v2, p0, Lcom/google/android/apps/chrome/ntp/LogoView;->mLogoIsDefault:Z

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/apps/chrome/ntp/LogoView;->setMatrix(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Z)V

    .line 225
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/LogoView;->mNewLogo:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/LogoView;->mNewLogo:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/LogoView;->mNewLogoMatrix:Landroid/graphics/Matrix;

    iget-boolean v2, p0, Lcom/google/android/apps/chrome/ntp/LogoView;->mNewLogoIsDefault:Z

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/apps/chrome/ntp/LogoView;->setMatrix(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Z)V

    .line 227
    :cond_2
    return-void
.end method

.method public setMananger(Lcom/google/android/apps/chrome/ntp/NewTabPageView$NewTabPageManager;)V
    .locals 0

    .prologue
    .line 94
    iput-object p1, p0, Lcom/google/android/apps/chrome/ntp/LogoView;->mManager:Lcom/google/android/apps/chrome/ntp/NewTabPageView$NewTabPageManager;

    .line 95
    return-void
.end method

.method public updateLogo(Lorg/chromium/chrome/browser/LogoBridge$Logo;)V
    .locals 3

    .prologue
    .line 113
    if-nez p1, :cond_0

    .line 114
    invoke-direct {p0}, Lcom/google/android/apps/chrome/ntp/LogoView;->getDefaultLogo()Landroid/graphics/Bitmap;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/apps/chrome/ntp/LogoView;->updateLogo(Landroid/graphics/Bitmap;Ljava/lang/String;Z)V

    .line 118
    :goto_0
    return-void

    .line 116
    :cond_0
    iget-object v0, p1, Lorg/chromium/chrome/browser/LogoBridge$Logo;->image:Landroid/graphics/Bitmap;

    iget-object v1, p1, Lorg/chromium/chrome/browser/LogoBridge$Logo;->altText:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/apps/chrome/ntp/LogoView;->updateLogo(Landroid/graphics/Bitmap;Ljava/lang/String;Z)V

    goto :goto_0
.end method
