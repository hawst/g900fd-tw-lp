.class public Lcom/google/android/apps/chrome/sync/AutoSigninSyncDelay;
.super Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;
.source "AutoSigninSyncDelay.java"


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final NOTIFICATIONS:[I


# instance fields
.field private final mPreferenceManager:Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

.field private final mProfileSyncService:Lorg/chromium/chrome/browser/sync/ProfileSyncService;

.field private mTabObserver:Lorg/chromium/chrome/browser/TabObserver;

.field private mWelcomeTab:Lorg/chromium/chrome/browser/Tab;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 25
    const-class v0, Lcom/google/android/apps/chrome/sync/AutoSigninSyncDelay;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/sync/AutoSigninSyncDelay;->$assertionsDisabled:Z

    .line 48
    new-array v0, v1, [I

    const/4 v1, 0x3

    aput v1, v0, v2

    sput-object v0, Lcom/google/android/apps/chrome/sync/AutoSigninSyncDelay;->NOTIFICATIONS:[I

    return-void

    :cond_0
    move v0, v2

    .line 25
    goto :goto_0
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/sync/AutoSigninSyncDelay;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/AutoSigninSyncDelay;->startSyncAndUnregisterListeners()V

    return-void
.end method

.method private startSyncAndUnregisterListeners()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 88
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/AutoSigninSyncDelay;->mPreferenceManager:Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->setDelaySync(Z)V

    .line 89
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/AutoSigninSyncDelay;->mProfileSyncService:Lorg/chromium/chrome/browser/sync/ProfileSyncService;

    invoke-virtual {v0, v1}, Lorg/chromium/chrome/browser/sync/ProfileSyncService;->setSetupInProgress(Z)V

    .line 91
    sget-object v0, Lcom/google/android/apps/chrome/sync/AutoSigninSyncDelay;->NOTIFICATIONS:[I

    invoke-static {v0, p0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->unregisterForNotifications([ILcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;)V

    .line 92
    iget-object v0, p0, Lcom/google/android/apps/chrome/sync/AutoSigninSyncDelay;->mWelcomeTab:Lorg/chromium/chrome/browser/Tab;

    iget-object v1, p0, Lcom/google/android/apps/chrome/sync/AutoSigninSyncDelay;->mTabObserver:Lorg/chromium/chrome/browser/TabObserver;

    invoke-virtual {v0, v1}, Lorg/chromium/chrome/browser/Tab;->removeObserver(Lorg/chromium/chrome/browser/TabObserver;)V

    .line 93
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3

    .prologue
    .line 75
    sget-boolean v0, Lcom/google/android/apps/chrome/sync/AutoSigninSyncDelay;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 77
    :cond_0
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "incognito"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 85
    :cond_1
    :goto_0
    return-void

    .line 81
    :cond_2
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "tabId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string/jumbo v2, "lastId"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    if-eq v0, v1, :cond_1

    .line 84
    invoke-direct {p0}, Lcom/google/android/apps/chrome/sync/AutoSigninSyncDelay;->startSyncAndUnregisterListeners()V

    goto :goto_0
.end method
