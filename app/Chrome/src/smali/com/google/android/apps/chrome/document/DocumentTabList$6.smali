.class Lcom/google/android/apps/chrome/document/DocumentTabList$6;
.super Landroid/os/AsyncTask;
.source "DocumentTabList.java"


# instance fields
.field private final mStatesToWrite:Landroid/util/SparseArray;

.field final synthetic this$0:Lcom/google/android/apps/chrome/document/DocumentTabList;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/document/DocumentTabList;)V
    .locals 1

    .prologue
    .line 893
    iput-object p1, p0, Lcom/google/android/apps/chrome/document/DocumentTabList$6;->this$0:Lcom/google/android/apps/chrome/document/DocumentTabList;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 894
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentTabList$6;->mStatesToWrite:Landroid/util/SparseArray;

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 893
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/document/DocumentTabList$6;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 4

    .prologue
    .line 907
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentTabList$6;->mStatesToWrite:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 908
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentTabList$6;->mStatesToWrite:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v2

    .line 909
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentTabList$6;->mStatesToWrite:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;

    .line 910
    iget-object v3, p0, Lcom/google/android/apps/chrome/document/DocumentTabList$6;->this$0:Lcom/google/android/apps/chrome/document/DocumentTabList;

    # invokes: Lcom/google/android/apps/chrome/document/DocumentTabList;->saveTabState(ILcom/google/android/apps/chrome/tab/ChromeTab$TabState;)V
    invoke-static {v3, v2, v0}, Lcom/google/android/apps/chrome/document/DocumentTabList;->access$1400(Lcom/google/android/apps/chrome/document/DocumentTabList;ILcom/google/android/apps/chrome/tab/ChromeTab$TabState;)V

    .line 907
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 912
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 893
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/document/DocumentTabList$6;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 917
    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentTabList$6;->mStatesToWrite:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 918
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentTabList$6;->mStatesToWrite:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v0

    .line 919
    iget-object v3, p0, Lcom/google/android/apps/chrome/document/DocumentTabList$6;->this$0:Lcom/google/android/apps/chrome/document/DocumentTabList;

    # getter for: Lcom/google/android/apps/chrome/document/DocumentTabList;->mEntryMap:Landroid/util/SparseArray;
    invoke-static {v3}, Lcom/google/android/apps/chrome/document/DocumentTabList;->access$400(Lcom/google/android/apps/chrome/document/DocumentTabList;)Landroid/util/SparseArray;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/document/DocumentTabList$Entry;

    .line 920
    if-eqz v0, :cond_0

    .line 921
    iput-boolean v2, v0, Lcom/google/android/apps/chrome/document/DocumentTabList$Entry;->isDirty:Z

    .line 917
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 923
    :cond_1
    return-void
.end method

.method protected onPreExecute()V
    .locals 4

    .prologue
    .line 898
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentTabList$6;->this$0:Lcom/google/android/apps/chrome/document/DocumentTabList;

    # getter for: Lcom/google/android/apps/chrome/document/DocumentTabList;->mEntryMap:Landroid/util/SparseArray;
    invoke-static {v0}, Lcom/google/android/apps/chrome/document/DocumentTabList;->access$400(Lcom/google/android/apps/chrome/document/DocumentTabList;)Landroid/util/SparseArray;

    move-result-object v0

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 899
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentTabList$6;->this$0:Lcom/google/android/apps/chrome/document/DocumentTabList;

    # getter for: Lcom/google/android/apps/chrome/document/DocumentTabList;->mEntryMap:Landroid/util/SparseArray;
    invoke-static {v0}, Lcom/google/android/apps/chrome/document/DocumentTabList;->access$400(Lcom/google/android/apps/chrome/document/DocumentTabList;)Landroid/util/SparseArray;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/document/DocumentTabList$Entry;

    .line 900
    iget-boolean v2, v0, Lcom/google/android/apps/chrome/document/DocumentTabList$Entry;->isDirty:Z

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/google/android/apps/chrome/document/DocumentTabList$Entry;->tabState:Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;

    if-eqz v2, :cond_0

    .line 901
    iget-object v2, p0, Lcom/google/android/apps/chrome/document/DocumentTabList$6;->mStatesToWrite:Landroid/util/SparseArray;

    iget v3, v0, Lcom/google/android/apps/chrome/document/DocumentTabList$Entry;->tabId:I

    iget-object v0, v0, Lcom/google/android/apps/chrome/document/DocumentTabList$Entry;->tabState:Lcom/google/android/apps/chrome/tab/ChromeTab$TabState;

    invoke-virtual {v2, v3, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 898
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 903
    :cond_1
    return-void
.end method
