.class public Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment;
.super Landroid/app/Fragment;
.source "AsyncTaskFragment.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field protected mCurrentTask:Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment$FragmentAsyncTask;

.field private mDialogMessage:Ljava/lang/String;

.field private final mDialogStaysEnough:Ljava/lang/Runnable;

.field private final mHandler:Landroid/os/Handler;

.field private mHasDialogStayedEnough:Z

.field private mProgressDialog:Landroid/app/ProgressDialog;

.field private mShouldShowDialog:Z

.field private final mShowProgressDialog:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const-class v0, Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 38
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment;->mHandler:Landroid/os/Handler;

    .line 45
    new-instance v0, Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment$1;-><init>(Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment;->mShowProgressDialog:Ljava/lang/Runnable;

    .line 53
    new-instance v0, Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment$2;-><init>(Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment;->mDialogStaysEnough:Ljava/lang/Runnable;

    .line 100
    return-void
.end method

.method static synthetic access$002(Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment;Z)Z
    .locals 0

    .prologue
    .line 24
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment;->mShouldShowDialog:Z

    return p1
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment;->showDialog()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment;)Z
    .locals 1

    .prologue
    .line 24
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment;->mHasDialogStayedEnough:Z

    return v0
.end method

.method static synthetic access$202(Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment;Z)Z
    .locals 0

    .prologue
    .line 24
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment;->mHasDialogStayedEnough:Z

    return p1
.end method

.method static synthetic access$300(Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment;->mProgressDialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment;->taskFinished()V

    return-void
.end method

.method private hideDialog()V
    .locals 1

    .prologue
    .line 220
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment;->mProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    .line 221
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 222
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 224
    :cond_0
    return-void
.end method

.method private showDialog()V
    .locals 5

    .prologue
    .line 214
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment;->isDetached()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment;->isHidden()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment;->mShouldShowDialog:Z

    if-nez v0, :cond_1

    .line 217
    :cond_0
    :goto_0
    return-void

    .line 215
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment;->mDialogMessage:Ljava/lang/String;

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-static {v0, v1, v2, v3, v4}, Landroid/app/ProgressDialog;->show(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZZ)Landroid/app/ProgressDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 216
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment;->mDialogStaysEnough:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method private taskFinished()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 227
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment;->mShowProgressDialog:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 228
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment;->mDialogStaysEnough:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 229
    invoke-direct {p0}, Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment;->hideDialog()V

    .line 230
    iput-boolean v2, p0, Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment;->mShouldShowDialog:Z

    .line 231
    iput-boolean v2, p0, Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment;->mHasDialogStayedEnough:Z

    .line 232
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment;->mCurrentTask:Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment$FragmentAsyncTask;

    .line 233
    return-void
.end method

.method private updateTaskDependentUI()V
    .locals 1

    .prologue
    .line 210
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment;->mCurrentTask:Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment$FragmentAsyncTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment;->mCurrentTask:Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment$FragmentAsyncTask;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment$FragmentAsyncTask;->updateDependentUI()V

    .line 211
    :cond_0
    return-void
.end method


# virtual methods
.method public cancelFragmentAsyncTask()V
    .locals 2

    .prologue
    .line 91
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment;->mCurrentTask:Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment$FragmentAsyncTask;

    if-eqz v0, :cond_0

    .line 92
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment;->mCurrentTask:Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment$FragmentAsyncTask;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment$FragmentAsyncTask;->cancel(Z)Z

    .line 93
    invoke-direct {p0}, Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment;->taskFinished()V

    .line 95
    :cond_0
    return-void
.end method

.method public isFragmentAsyncTaskRunning()Z
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment;->mCurrentTask:Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment$FragmentAsyncTask;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 189
    invoke-super {p0, p1}, Landroid/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 190
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment;->isFragmentAsyncTaskRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment;->updateTaskDependentUI()V

    .line 191
    :cond_0
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 165
    invoke-super {p0, p1}, Landroid/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 166
    invoke-direct {p0}, Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment;->showDialog()V

    .line 167
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 177
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 178
    const/4 v0, 0x1

    invoke-super {p0, v0}, Landroid/app/Fragment;->setRetainInstance(Z)V

    .line 179
    return-void
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 183
    invoke-super {p0}, Landroid/app/Fragment;->onDestroy()V

    .line 184
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment;->cancelFragmentAsyncTask()V

    .line 185
    return-void
.end method

.method public onDetach()V
    .locals 0

    .prologue
    .line 171
    invoke-super {p0}, Landroid/app/Fragment;->onDetach()V

    .line 172
    invoke-direct {p0}, Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment;->hideDialog()V

    .line 173
    return-void
.end method

.method public onHiddenChanged(Z)V
    .locals 0

    .prologue
    .line 195
    invoke-super {p0, p1}, Landroid/app/Fragment;->onHiddenChanged(Z)V

    .line 196
    if-eqz p1, :cond_0

    .line 197
    invoke-direct {p0}, Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment;->hideDialog()V

    .line 201
    :goto_0
    return-void

    .line 199
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment;->showDialog()V

    goto :goto_0
.end method

.method public runFragmentAsyncTask(Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment$FragmentAsyncTask;Ljava/lang/String;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 75
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment;->isFragmentAsyncTaskRunning()Z

    move-result v1

    if-nez v1, :cond_0

    if-nez p1, :cond_1

    .line 83
    :cond_0
    :goto_0
    return v0

    .line 77
    :cond_1
    iput-object p1, p0, Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment;->mCurrentTask:Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment$FragmentAsyncTask;

    .line 78
    iput-object p2, p0, Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment;->mDialogMessage:Ljava/lang/String;

    .line 79
    iput-boolean v0, p0, Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment;->mShouldShowDialog:Z

    .line 80
    iput-boolean v0, p0, Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment;->mHasDialogStayedEnough:Z

    .line 81
    iget-object v1, p0, Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment;->mShowProgressDialog:Ljava/lang/Runnable;

    const-wide/16 v4, 0x12c

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 82
    new-array v0, v0, [Ljava/lang/Void;

    invoke-virtual {p1, v0}, Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment$FragmentAsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 83
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setRetainInstance(Z)V
    .locals 1

    .prologue
    .line 206
    sget-boolean v0, Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 207
    :cond_0
    return-void
.end method
