.class public Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment;
.super Lcom/google/android/apps/chrome/preferences/ChromeBaseDialogFragment;
.source "ClearBrowsingDataDialogFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Landroid/content/DialogInterface$OnMultiChoiceClickListener;
.implements Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences$OnClearBrowsingDataListener;


# static fields
.field public static final FRAGMENT_TAG:Ljava/lang/String; = "ClearBrowsingDataDialogFragment"


# instance fields
.field private mDialog:Landroid/app/AlertDialog;

.field private mOptions:[Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment$DialogOption;

.field private mProgressDialog:Landroid/app/ProgressDialog;

.field private mSelectedOptions:Ljava/util/EnumSet;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBaseDialogFragment;-><init>()V

    .line 43
    return-void
.end method

.method private updateButtonState()V
    .locals 2

    .prologue
    .line 130
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment;->mDialog:Landroid/app/AlertDialog;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v1

    .line 131
    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment;->mSelectedOptions:Ljava/util/EnumSet;

    invoke-virtual {v0}, Ljava/util/EnumSet;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 132
    :cond_0
    return-void

    .line 131
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected final clearBrowsingData(Ljava/util/EnumSet;)V
    .locals 7

    .prologue
    .line 74
    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment$DialogOption;->CLEAR_HISTORY:Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment$DialogOption;

    invoke-virtual {p1, v1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    sget-object v1, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment$DialogOption;->CLEAR_CACHE:Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment$DialogOption;

    invoke-virtual {p1, v1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v3

    sget-object v1, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment$DialogOption;->CLEAR_COOKIES_AND_SITE_DATA:Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment$DialogOption;

    invoke-virtual {p1, v1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v4

    sget-object v1, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment$DialogOption;->CLEAR_PASSWORDS:Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment$DialogOption;

    invoke-virtual {p1, v1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v5

    sget-object v1, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment$DialogOption;->CLEAR_FORM_DATA:Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment$DialogOption;

    invoke-virtual {p1, v1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v6

    move-object v1, p0

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->clearBrowsingData(Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences$OnClearBrowsingDataListener;ZZZZZ)V

    .line 80
    return-void
.end method

.method protected dismissProgressDialog()V
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment;->mProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 84
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 86
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 87
    return-void
.end method

.method protected getDefaultDialogOptionsSelections()Ljava/util/EnumSet;
    .locals 3

    .prologue
    .line 107
    sget-object v0, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment$DialogOption;->CLEAR_HISTORY:Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment$DialogOption;

    sget-object v1, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment$DialogOption;->CLEAR_CACHE:Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment$DialogOption;

    sget-object v2, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment$DialogOption;->CLEAR_COOKIES_AND_SITE_DATA:Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment$DialogOption;

    invoke-static {v0, v1, v2}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    return-object v0
.end method

.method protected getDialogOptions()[Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment$DialogOption;
    .locals 3

    .prologue
    .line 94
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment$DialogOption;

    const/4 v1, 0x0

    sget-object v2, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment$DialogOption;->CLEAR_HISTORY:Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment$DialogOption;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment$DialogOption;->CLEAR_CACHE:Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment$DialogOption;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment$DialogOption;->CLEAR_COOKIES_AND_SITE_DATA:Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment$DialogOption;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment$DialogOption;->CLEAR_PASSWORDS:Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment$DialogOption;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment$DialogOption;->CLEAR_FORM_DATA:Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment$DialogOption;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 191
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/preferences/ChromeBaseDialogFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 193
    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment;->updateButtonState()V

    .line 194
    return-void
.end method

.method public onBrowsingDataCleared()V
    .locals 0

    .prologue
    .line 115
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment;->dismissProgressDialog()V

    .line 116
    return-void
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 1

    .prologue
    .line 120
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 121
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment;->dismissProgressDialog()V

    .line 122
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment;->mSelectedOptions:Ljava/util/EnumSet;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment;->onOptionSelected(Ljava/util/EnumSet;)V

    .line 124
    :cond_0
    return-void
.end method

.method public onClick(Landroid/content/DialogInterface;IZ)V
    .locals 2

    .prologue
    .line 136
    if-eqz p3, :cond_0

    .line 137
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment;->mSelectedOptions:Ljava/util/EnumSet;

    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment;->mOptions:[Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment$DialogOption;

    aget-object v1, v1, p2

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 141
    :goto_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment;->updateButtonState()V

    .line 142
    return-void

    .line 139
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment;->mSelectedOptions:Ljava/util/EnumSet;

    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment;->mOptions:[Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment$DialogOption;

    aget-object v1, v1, p2

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 146
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment;->getDialogOptions()[Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment$DialogOption;

    move-result-object v0

    .line 147
    array-length v2, v0

    invoke-static {v0, v2}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment$DialogOption;

    iput-object v0, p0, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment;->mOptions:[Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment$DialogOption;

    .line 148
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment;->getDefaultDialogOptionsSelections()Ljava/util/EnumSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment;->mSelectedOptions:Ljava/util/EnumSet;

    .line 150
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment;->mOptions:[Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment$DialogOption;

    array-length v0, v0

    new-array v2, v0, [Ljava/lang/String;

    .line 151
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment;->mOptions:[Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment$DialogOption;

    array-length v0, v0

    new-array v3, v0, [Z

    .line 152
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    move v0, v1

    .line 153
    :goto_0
    iget-object v5, p0, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment;->mOptions:[Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment$DialogOption;

    array-length v5, v5

    if-ge v0, v5, :cond_0

    .line 154
    iget-object v5, p0, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment;->mOptions:[Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment$DialogOption;

    aget-object v5, v5, v0

    invoke-virtual {v5}, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment$DialogOption;->getResourceId()I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v0

    .line 155
    iget-object v5, p0, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment;->mSelectedOptions:Ljava/util/EnumSet;

    iget-object v6, p0, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment;->mOptions:[Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment$DialogOption;

    aget-object v6, v6, v0

    invoke-virtual {v5, v6}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v5

    aput-boolean v5, v3, v0

    .line 153
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 158
    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-direct {v0, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v4, Lcom/google/android/apps/chrome/R$string;->clear_browsing_data_title:I

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v4, Lcom/google/android/apps/chrome/R$string;->clear_data_delete:I

    invoke-virtual {v0, v4, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v4, Lcom/google/android/apps/chrome/R$string;->cancel:I

    invoke-virtual {v0, v4, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v2, v3, p0}, Landroid/app/AlertDialog$Builder;->setMultiChoiceItems([Ljava/lang/CharSequence;[ZLandroid/content/DialogInterface$OnMultiChoiceClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    .line 164
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/sync/signin/ChromeSigninController;->get(Landroid/content/Context;)Lorg/chromium/sync/signin/ChromeSigninController;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/sync/signin/ChromeSigninController;->isSignedIn()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 165
    sget v0, Lcom/google/android/apps/chrome/R$string;->clear_cookies_no_sign_out_summary:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 166
    const/4 v3, 0x1

    new-array v3, v3, [Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;

    new-instance v4, Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;

    const-string/jumbo v5, "<link>"

    const-string/jumbo v6, "</link>"

    new-instance v7, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment$1;

    invoke-direct {v7, p0}, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment$1;-><init>(Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment;)V

    invoke-direct {v4, v5, v6, v7}, Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    aput-object v4, v3, v1

    invoke-static {v0, v3}, Lcom/google/android/apps/chrome/utilities/StringLinkifier;->linkify(Ljava/lang/String;[Lcom/google/android/apps/chrome/utilities/StringLinkifier$Link;)Landroid/text/SpannableString;

    move-result-object v1

    .line 177
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    sget v3, Lcom/google/android/apps/chrome/R$layout;->single_line_bottom_text_dialog:I

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 179
    sget v0, Lcom/google/android/apps/chrome/R$id;->summary:I

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 180
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 181
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 182
    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 185
    :cond_1
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment;->mDialog:Landroid/app/AlertDialog;

    .line 186
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment;->mDialog:Landroid/app/AlertDialog;

    return-object v0
.end method

.method protected onOptionSelected(Ljava/util/EnumSet;)V
    .locals 0

    .prologue
    .line 202
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment;->showProgressDialog()V

    .line 203
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment;->clearBrowsingData(Ljava/util/EnumSet;)V

    .line 204
    return-void
.end method

.method protected final showProgressDialog()V
    .locals 5

    .prologue
    .line 207
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    sget v2, Lcom/google/android/apps/chrome/R$string;->clear_browsing_data_progress_title:I

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    sget v3, Lcom/google/android/apps/chrome/R$string;->clear_browsing_data_progress_message:I

    invoke-virtual {v2, v3}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-static {v0, v1, v2, v3, v4}, Landroid/app/ProgressDialog;->show(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZZ)Landroid/app/ProgressDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/preferences/ClearBrowsingDataDialogFragment;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 211
    return-void
.end method
