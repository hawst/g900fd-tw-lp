.class public Lcom/google/android/apps/chrome/preferences/autofill/AutofillCreditCardEditor;
.super Lcom/google/android/apps/chrome/preferences/ChromeBaseFragment;
.source "AutofillCreditCardEditor.java"

# interfaces
.implements Landroid/text/TextWatcher;
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# instance fields
.field private mExpirationMonth:Landroid/widget/Spinner;

.field private mExpirationYear:Landroid/widget/Spinner;

.field private mGUID:Ljava/lang/String;

.field private mInitialExpirationMonthPos:I

.field private mInitialExpirationYearPos:I

.field private mNameText:Landroid/widget/EditText;

.field private mNumberText:Landroid/widget/EditText;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBaseFragment;-><init>()V

    .line 248
    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/preferences/autofill/AutofillCreditCardEditor;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/autofill/AutofillCreditCardEditor;->deleteCreditCard()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/preferences/autofill/AutofillCreditCardEditor;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/autofill/AutofillCreditCardEditor;->saveCreditCard()V

    return-void
.end method

.method private addCardDataToEditFields()V
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 128
    invoke-static {}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager;->getInstance()Lorg/chromium/chrome/browser/autofill/PersonalDataManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/autofill/AutofillCreditCardEditor;->mGUID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager;->getCreditCard(Ljava/lang/String;)Lorg/chromium/chrome/browser/autofill/PersonalDataManager$CreditCard;

    move-result-object v5

    .line 129
    if-nez v5, :cond_0

    .line 130
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/autofill/AutofillCreditCardEditor;->mNameText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 170
    :goto_0
    return-void

    .line 134
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/autofill/AutofillCreditCardEditor;->mNameText:Landroid/widget/EditText;

    invoke-virtual {v5}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager$CreditCard;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 135
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/autofill/AutofillCreditCardEditor;->mNumberText:Landroid/widget/EditText;

    invoke-virtual {v5}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager$CreditCard;->getNumber()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 137
    iput v4, p0, Lcom/google/android/apps/chrome/preferences/autofill/AutofillCreditCardEditor;->mInitialExpirationMonthPos:I

    .line 139
    invoke-virtual {v5}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager$CreditCard;->getMonth()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    .line 140
    invoke-virtual {v5}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager$CreditCard;->getMonth()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    move v1, v0

    :goto_1
    move v3, v4

    .line 142
    :goto_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/autofill/AutofillCreditCardEditor;->mExpirationMonth:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    invoke-interface {v0}, Landroid/widget/SpinnerAdapter;->getCount()I

    move-result v0

    if-ge v3, v0, :cond_1

    .line 143
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/autofill/AutofillCreditCardEditor;->mExpirationMonth:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/widget/SpinnerAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 144
    if-ne v1, v0, :cond_3

    .line 145
    iput v3, p0, Lcom/google/android/apps/chrome/preferences/autofill/AutofillCreditCardEditor;->mInitialExpirationMonthPos:I

    .line 149
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/autofill/AutofillCreditCardEditor;->mExpirationMonth:Landroid/widget/Spinner;

    iget v1, p0, Lcom/google/android/apps/chrome/preferences/autofill/AutofillCreditCardEditor;->mInitialExpirationMonthPos:I

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 151
    iput v4, p0, Lcom/google/android/apps/chrome/preferences/autofill/AutofillCreditCardEditor;->mInitialExpirationYearPos:I

    move v0, v4

    .line 153
    :goto_3
    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/autofill/AutofillCreditCardEditor;->mExpirationYear:Landroid/widget/Spinner;

    invoke-virtual {v1}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v1

    invoke-interface {v1}, Landroid/widget/SpinnerAdapter;->getCount()I

    move-result v1

    if-ge v0, v1, :cond_5

    .line 154
    invoke-virtual {v5}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager$CreditCard;->getYear()Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/apps/chrome/preferences/autofill/AutofillCreditCardEditor;->mExpirationYear:Landroid/widget/Spinner;

    invoke-virtual {v3}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v3

    invoke-interface {v3, v0}, Landroid/widget/SpinnerAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 155
    iput v0, p0, Lcom/google/android/apps/chrome/preferences/autofill/AutofillCreditCardEditor;->mInitialExpirationYearPos:I

    .line 162
    :goto_4
    if-nez v2, :cond_2

    invoke-virtual {v5}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager$CreditCard;->getYear()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 164
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/autofill/AutofillCreditCardEditor;->mExpirationYear:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    check-cast v0, Landroid/widget/ArrayAdapter;

    .line 166
    invoke-virtual {v5}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager$CreditCard;->getYear()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v4}, Landroid/widget/ArrayAdapter;->insert(Ljava/lang/Object;I)V

    .line 167
    iput v4, p0, Lcom/google/android/apps/chrome/preferences/autofill/AutofillCreditCardEditor;->mInitialExpirationYearPos:I

    .line 169
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/autofill/AutofillCreditCardEditor;->mExpirationYear:Landroid/widget/Spinner;

    iget v1, p0, Lcom/google/android/apps/chrome/preferences/autofill/AutofillCreditCardEditor;->mInitialExpirationYearPos:I

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    goto/16 :goto_0

    .line 142
    :cond_3
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_2

    .line 153
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_5
    move v2, v4

    goto :goto_4

    :cond_6
    move v1, v2

    goto :goto_1
.end method

.method private deleteCreditCard()V
    .locals 2

    .prologue
    .line 190
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/autofill/AutofillCreditCardEditor;->mGUID:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 191
    invoke-static {}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager;->getInstance()Lorg/chromium/chrome/browser/autofill/PersonalDataManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/autofill/AutofillCreditCardEditor;->mGUID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager;->deleteCreditCard(Ljava/lang/String;)V

    .line 193
    :cond_0
    return-void
.end method

.method private enableSaveButton()V
    .locals 2

    .prologue
    .line 235
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/autofill/AutofillCreditCardEditor;->getView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$id;->autofill_credit_card_save:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 236
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 237
    return-void
.end method

.method private hookupSaveCancelDeleteButtons(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 196
    sget v0, Lcom/google/android/apps/chrome/R$id;->autofill_credit_card_delete:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 197
    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/autofill/AutofillCreditCardEditor;->mGUID:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/autofill/AutofillCreditCardEditor;->mGUID:Ljava/lang/String;

    const-string/jumbo v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_1

    .line 199
    :cond_0
    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 209
    :goto_0
    sget v0, Lcom/google/android/apps/chrome/R$id;->autofill_credit_card_cancel:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 210
    new-instance v1, Lcom/google/android/apps/chrome/preferences/autofill/AutofillCreditCardEditor$2;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/preferences/autofill/AutofillCreditCardEditor$2;-><init>(Lcom/google/android/apps/chrome/preferences/autofill/AutofillCreditCardEditor;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 216
    sget v0, Lcom/google/android/apps/chrome/R$id;->autofill_credit_card_save:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 217
    new-instance v1, Lcom/google/android/apps/chrome/preferences/autofill/AutofillCreditCardEditor$3;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/preferences/autofill/AutofillCreditCardEditor$3;-><init>(Lcom/google/android/apps/chrome/preferences/autofill/AutofillCreditCardEditor;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 225
    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 228
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/autofill/AutofillCreditCardEditor;->mNameText:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 229
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/autofill/AutofillCreditCardEditor;->mNumberText:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 230
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/autofill/AutofillCreditCardEditor;->mExpirationMonth:Landroid/widget/Spinner;

    invoke-virtual {v0, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 231
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/autofill/AutofillCreditCardEditor;->mExpirationYear:Landroid/widget/Spinner;

    invoke-virtual {v0, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 232
    return-void

    .line 201
    :cond_1
    new-instance v1, Lcom/google/android/apps/chrome/preferences/autofill/AutofillCreditCardEditor$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/preferences/autofill/AutofillCreditCardEditor$1;-><init>(Lcom/google/android/apps/chrome/preferences/autofill/AutofillCreditCardEditor;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method private saveCreditCard()V
    .locals 8

    .prologue
    .line 176
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/autofill/AutofillCreditCardEditor;->mNumberText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "\\s+"

    const-string/jumbo v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 177
    new-instance v0, Lorg/chromium/chrome/browser/autofill/PersonalDataManager$CreditCard;

    iget-object v1, p0, Lcom/google/android/apps/chrome/preferences/autofill/AutofillCreditCardEditor;->mGUID:Ljava/lang/String;

    const-string/jumbo v2, "Chrome settings"

    iget-object v3, p0, Lcom/google/android/apps/chrome/preferences/autofill/AutofillCreditCardEditor;->mNameText:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    iget-object v5, p0, Lcom/google/android/apps/chrome/preferences/autofill/AutofillCreditCardEditor;->mExpirationMonth:Landroid/widget/Spinner;

    invoke-virtual {v5}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/apps/chrome/preferences/autofill/AutofillCreditCardEditor;->mExpirationYear:Landroid/widget/Spinner;

    invoke-virtual {v5}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    move-object v5, v4

    invoke-direct/range {v0 .. v7}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager$CreditCard;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 186
    invoke-static {}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager;->getInstance()Lorg/chromium/chrome/browser/autofill/PersonalDataManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager;->setCreditCard(Lorg/chromium/chrome/browser/autofill/PersonalDataManager$CreditCard;)Ljava/lang/String;

    .line 187
    return-void
.end method


# virtual methods
.method addSpinnerAdapters()V
    .locals 9

    .prologue
    const v8, 0x1090009

    const v7, 0x1090008

    const/4 v1, 0x1

    .line 109
    new-instance v2, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/autofill/AutofillCreditCardEditor;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-direct {v2, v0, v7}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    move v0, v1

    .line 111
    :goto_0
    const/16 v3, 0xc

    if-gt v0, v3, :cond_0

    .line 112
    const-string/jumbo v3, "%02d"

    new-array v4, v1, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 111
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 114
    :cond_0
    invoke-virtual {v2, v8}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 115
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/autofill/AutofillCreditCardEditor;->mExpirationMonth:Landroid/widget/Spinner;

    invoke-virtual {v0, v2}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 117
    new-instance v2, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/autofill/AutofillCreditCardEditor;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-direct {v2, v0, v7}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 119
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v1

    move v0, v1

    .line 120
    :goto_1
    add-int/lit8 v3, v1, 0xa

    if-ge v0, v3, :cond_1

    .line 121
    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 120
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 123
    :cond_1
    invoke-virtual {v2, v8}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 124
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/autofill/AutofillCreditCardEditor;->mExpirationYear:Landroid/widget/Spinner;

    invoke-virtual {v0, v2}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 125
    return-void
.end method

.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 98
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 101
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 48
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/preferences/ChromeBaseFragment;->onCreate(Landroid/os/Bundle;)V

    .line 49
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    .prologue
    .line 54
    invoke-super {p0, p3}, Lcom/google/android/apps/chrome/preferences/ChromeBaseFragment;->onCreate(Landroid/os/Bundle;)V

    .line 55
    sget v0, Lcom/google/android/apps/chrome/R$layout;->autofill_credit_card_editor:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 56
    sget v0, Lcom/google/android/apps/chrome/R$string;->autofill_create_or_edit_credit_card:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 57
    sget v0, Lcom/google/android/apps/chrome/R$id;->autofill_credit_card_editor_name_edit:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/google/android/apps/chrome/preferences/autofill/AutofillCreditCardEditor;->mNameText:Landroid/widget/EditText;

    .line 59
    sget v0, Lcom/google/android/apps/chrome/R$id;->autofill_credit_card_editor_number_edit:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/google/android/apps/chrome/preferences/autofill/AutofillCreditCardEditor;->mNumberText:Landroid/widget/EditText;

    .line 63
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/autofill/AutofillCreditCardEditor;->mNumberText:Landroid/widget/EditText;

    new-instance v2, Lcom/google/android/apps/chrome/preferences/autofill/AutofillCreditCardEditor$CreditCardNumberFormattingTextWatcher;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Lcom/google/android/apps/chrome/preferences/autofill/AutofillCreditCardEditor$CreditCardNumberFormattingTextWatcher;-><init>(Lcom/google/android/apps/chrome/preferences/autofill/AutofillCreditCardEditor$1;)V

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 65
    sget v0, Lcom/google/android/apps/chrome/R$id;->autofill_credit_card_editor_month_spinner:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/google/android/apps/chrome/preferences/autofill/AutofillCreditCardEditor;->mExpirationMonth:Landroid/widget/Spinner;

    .line 67
    sget v0, Lcom/google/android/apps/chrome/R$id;->autofill_credit_card_editor_year_spinner:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/google/android/apps/chrome/preferences/autofill/AutofillCreditCardEditor;->mExpirationYear:Landroid/widget/Spinner;

    .line 72
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/autofill/AutofillCreditCardEditor;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 73
    if-eqz v0, :cond_0

    .line 74
    const-string/jumbo v2, "guid"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/preferences/autofill/AutofillCreditCardEditor;->mGUID:Ljava/lang/String;

    .line 76
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/autofill/AutofillCreditCardEditor;->mGUID:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 77
    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/google/android/apps/chrome/preferences/autofill/AutofillCreditCardEditor;->mGUID:Ljava/lang/String;

    .line 80
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/autofill/AutofillCreditCardEditor;->addSpinnerAdapters()V

    .line 81
    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/autofill/AutofillCreditCardEditor;->addCardDataToEditFields()V

    .line 82
    invoke-direct {p0, v1}, Lcom/google/android/apps/chrome/preferences/autofill/AutofillCreditCardEditor;->hookupSaveCancelDeleteButtons(Landroid/view/View;)V

    .line 83
    return-object v1
.end method

.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/autofill/AutofillCreditCardEditor;->mExpirationYear:Landroid/widget/Spinner;

    if-ne p1, v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/chrome/preferences/autofill/AutofillCreditCardEditor;->mInitialExpirationYearPos:I

    if-ne p3, v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/autofill/AutofillCreditCardEditor;->mExpirationMonth:Landroid/widget/Spinner;

    if-ne p1, v0, :cond_2

    iget v0, p0, Lcom/google/android/apps/chrome/preferences/autofill/AutofillCreditCardEditor;->mInitialExpirationMonthPos:I

    if-eq p3, v0, :cond_2

    .line 90
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/autofill/AutofillCreditCardEditor;->enableSaveButton()V

    .line 92
    :cond_2
    return-void
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0

    .prologue
    .line 95
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 105
    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/autofill/AutofillCreditCardEditor;->enableSaveButton()V

    .line 106
    return-void
.end method
