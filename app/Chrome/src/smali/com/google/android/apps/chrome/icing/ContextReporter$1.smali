.class Lcom/google/android/apps/chrome/icing/ContextReporter$1;
.super Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;
.source "ContextReporter.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/icing/ContextReporter;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 61
    const-class v0, Lcom/google/android/apps/chrome/icing/ContextReporter;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/icing/ContextReporter$1;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lcom/google/android/apps/chrome/icing/ContextReporter;)V
    .locals 0

    .prologue
    .line 61
    iput-object p1, p0, Lcom/google/android/apps/chrome/icing/ContextReporter$1;->this$0:Lcom/google/android/apps/chrome/icing/ContextReporter;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, -0x1

    .line 64
    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    .line 97
    sget-boolean v0, Lcom/google/android/apps/chrome/icing/ContextReporter$1;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 67
    :sswitch_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/icing/ContextReporter$1;->this$0:Lcom/google/android/apps/chrome/icing/ContextReporter;

    # getter for: Lcom/google/android/apps/chrome/icing/ContextReporter;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;
    invoke-static {v0}, Lcom/google/android/apps/chrome/icing/ContextReporter;->access$000(Lcom/google/android/apps/chrome/icing/ContextReporter;)Lcom/google/android/apps/chrome/ChromeActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeActivity;->getCurrentTab()Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v0

    .line 68
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/Tab;->isIncognito()Z

    move-result v1

    if-nez v1, :cond_0

    .line 69
    iget-object v1, p0, Lcom/google/android/apps/chrome/icing/ContextReporter$1;->this$0:Lcom/google/android/apps/chrome/icing/ContextReporter;

    # invokes: Lcom/google/android/apps/chrome/icing/ContextReporter;->report(Lorg/chromium/chrome/browser/Tab;)V
    invoke-static {v1, v0}, Lcom/google/android/apps/chrome/icing/ContextReporter;->access$100(Lcom/google/android/apps/chrome/icing/ContextReporter;Lorg/chromium/chrome/browser/Tab;)V

    .line 70
    iget-object v0, p0, Lcom/google/android/apps/chrome/icing/ContextReporter$1;->this$0:Lcom/google/android/apps/chrome/icing/ContextReporter;

    # setter for: Lcom/google/android/apps/chrome/icing/ContextReporter;->mLastContextWasTitleChange:Z
    invoke-static {v0, v3}, Lcom/google/android/apps/chrome/icing/ContextReporter;->access$202(Lcom/google/android/apps/chrome/icing/ContextReporter;Z)Z

    .line 99
    :cond_0
    :goto_0
    return-void

    .line 76
    :sswitch_1
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "tabId"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 77
    iget-object v1, p0, Lcom/google/android/apps/chrome/icing/ContextReporter$1;->this$0:Lcom/google/android/apps/chrome/icing/ContextReporter;

    # getter for: Lcom/google/android/apps/chrome/icing/ContextReporter;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;
    invoke-static {v1}, Lcom/google/android/apps/chrome/icing/ContextReporter;->access$000(Lcom/google/android/apps/chrome/icing/ContextReporter;)Lcom/google/android/apps/chrome/ChromeActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/ChromeActivity;->getCurrentTab()Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v1

    .line 78
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lorg/chromium/chrome/browser/Tab;->isIncognito()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v1}, Lorg/chromium/chrome/browser/Tab;->getId()I

    move-result v2

    if-ne v0, v2, :cond_0

    .line 80
    iget-object v0, p0, Lcom/google/android/apps/chrome/icing/ContextReporter$1;->this$0:Lcom/google/android/apps/chrome/icing/ContextReporter;

    # invokes: Lcom/google/android/apps/chrome/icing/ContextReporter;->report(Lorg/chromium/chrome/browser/Tab;)V
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/icing/ContextReporter;->access$100(Lcom/google/android/apps/chrome/icing/ContextReporter;Lorg/chromium/chrome/browser/Tab;)V

    .line 81
    iget-object v0, p0, Lcom/google/android/apps/chrome/icing/ContextReporter$1;->this$0:Lcom/google/android/apps/chrome/icing/ContextReporter;

    # setter for: Lcom/google/android/apps/chrome/icing/ContextReporter;->mLastContextWasTitleChange:Z
    invoke-static {v0, v3}, Lcom/google/android/apps/chrome/icing/ContextReporter;->access$202(Lcom/google/android/apps/chrome/icing/ContextReporter;Z)Z

    goto :goto_0

    .line 87
    :sswitch_2
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "tabId"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 88
    iget-object v1, p0, Lcom/google/android/apps/chrome/icing/ContextReporter$1;->this$0:Lcom/google/android/apps/chrome/icing/ContextReporter;

    # getter for: Lcom/google/android/apps/chrome/icing/ContextReporter;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;
    invoke-static {v1}, Lcom/google/android/apps/chrome/icing/ContextReporter;->access$000(Lcom/google/android/apps/chrome/icing/ContextReporter;)Lcom/google/android/apps/chrome/ChromeActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/ChromeActivity;->getCurrentTab()Lcom/google/android/apps/chrome/tab/ChromeTab;

    move-result-object v1

    .line 89
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lorg/chromium/chrome/browser/Tab;->isIncognito()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v1}, Lorg/chromium/chrome/browser/Tab;->getId()I

    move-result v2

    if-ne v0, v2, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/icing/ContextReporter$1;->this$0:Lcom/google/android/apps/chrome/icing/ContextReporter;

    # getter for: Lcom/google/android/apps/chrome/icing/ContextReporter;->mLastContextWasTitleChange:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/icing/ContextReporter;->access$200(Lcom/google/android/apps/chrome/icing/ContextReporter;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 91
    iget-object v0, p0, Lcom/google/android/apps/chrome/icing/ContextReporter$1;->this$0:Lcom/google/android/apps/chrome/icing/ContextReporter;

    # invokes: Lcom/google/android/apps/chrome/icing/ContextReporter;->report(Lorg/chromium/chrome/browser/Tab;)V
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/icing/ContextReporter;->access$100(Lcom/google/android/apps/chrome/icing/ContextReporter;Lorg/chromium/chrome/browser/Tab;)V

    .line 92
    iget-object v0, p0, Lcom/google/android/apps/chrome/icing/ContextReporter$1;->this$0:Lcom/google/android/apps/chrome/icing/ContextReporter;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/apps/chrome/icing/ContextReporter;->mLastContextWasTitleChange:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/icing/ContextReporter;->access$202(Lcom/google/android/apps/chrome/icing/ContextReporter;Z)Z

    goto :goto_0

    .line 64
    nop

    :sswitch_data_0
    .sparse-switch
        0x3 -> :sswitch_0
        0xa -> :sswitch_2
        0x19 -> :sswitch_1
    .end sparse-switch
.end method
