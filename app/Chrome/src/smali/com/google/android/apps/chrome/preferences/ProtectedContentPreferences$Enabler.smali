.class Lcom/google/android/apps/chrome/preferences/ProtectedContentPreferences$Enabler;
.super Lcom/google/android/apps/chrome/preferences/ChromeBaseSwitchEnabler;
.source "ProtectedContentPreferences.java"


# instance fields
.field private final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/widget/Switch;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/preferences/ChromeBaseSwitchEnabler;-><init>(Landroid/content/Context;Landroid/widget/Switch;)V

    .line 35
    iput-object p1, p0, Lcom/google/android/apps/chrome/preferences/ProtectedContentPreferences$Enabler;->mContext:Landroid/content/Context;

    .line 36
    return-void
.end method


# virtual methods
.method protected isSwitchEnabled()Z
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ProtectedContentPreferences$Enabler;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getPrefEnableProtectedContent()Z

    move-result v0

    return v0
.end method

.method public onValueChanged(Z)V
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/ProtectedContentPreferences$Enabler;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->setProtectedContent(Z)V

    .line 41
    return-void
.end method
