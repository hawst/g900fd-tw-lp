.class public Lcom/google/android/apps/chrome/services/GoogleServicesStates$GoogleServicesStatesBuilder;
.super Ljava/lang/Object;
.source "GoogleServicesStates.java"


# instance fields
.field private mTempAutoLogin:Ljava/lang/Boolean;

.field private mTempChromeToMobile:Ljava/lang/Boolean;

.field private mTempMasterSyncState:Ljava/lang/Boolean;

.field private mTempSync:Ljava/lang/Boolean;

.field private mTempWantedSyncState:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public autoLogin(Z)Lcom/google/android/apps/chrome/services/GoogleServicesStates$GoogleServicesStatesBuilder;
    .locals 1

    .prologue
    .line 111
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/services/GoogleServicesStates$GoogleServicesStatesBuilder;->mTempAutoLogin:Ljava/lang/Boolean;

    .line 112
    return-object p0
.end method

.method public build()Lcom/google/android/apps/chrome/services/GoogleServicesStates;
    .locals 7

    .prologue
    .line 116
    new-instance v0, Lcom/google/android/apps/chrome/services/GoogleServicesStates;

    iget-object v1, p0, Lcom/google/android/apps/chrome/services/GoogleServicesStates$GoogleServicesStatesBuilder;->mTempSync:Ljava/lang/Boolean;

    iget-object v2, p0, Lcom/google/android/apps/chrome/services/GoogleServicesStates$GoogleServicesStatesBuilder;->mTempWantedSyncState:Ljava/lang/Boolean;

    iget-object v3, p0, Lcom/google/android/apps/chrome/services/GoogleServicesStates$GoogleServicesStatesBuilder;->mTempMasterSyncState:Ljava/lang/Boolean;

    iget-object v4, p0, Lcom/google/android/apps/chrome/services/GoogleServicesStates$GoogleServicesStatesBuilder;->mTempChromeToMobile:Ljava/lang/Boolean;

    iget-object v5, p0, Lcom/google/android/apps/chrome/services/GoogleServicesStates$GoogleServicesStatesBuilder;->mTempAutoLogin:Ljava/lang/Boolean;

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/chrome/services/GoogleServicesStates;-><init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Lcom/google/android/apps/chrome/services/GoogleServicesStates$1;)V

    return-object v0
.end method

.method public chromeToMobile(Z)Lcom/google/android/apps/chrome/services/GoogleServicesStates$GoogleServicesStatesBuilder;
    .locals 1

    .prologue
    .line 106
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/services/GoogleServicesStates$GoogleServicesStatesBuilder;->mTempChromeToMobile:Ljava/lang/Boolean;

    .line 107
    return-object p0
.end method

.method public masterSyncState(Z)Lcom/google/android/apps/chrome/services/GoogleServicesStates$GoogleServicesStatesBuilder;
    .locals 1

    .prologue
    .line 101
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/services/GoogleServicesStates$GoogleServicesStatesBuilder;->mTempMasterSyncState:Ljava/lang/Boolean;

    .line 102
    return-object p0
.end method

.method public sync(Z)Lcom/google/android/apps/chrome/services/GoogleServicesStates$GoogleServicesStatesBuilder;
    .locals 1

    .prologue
    .line 91
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/services/GoogleServicesStates$GoogleServicesStatesBuilder;->mTempSync:Ljava/lang/Boolean;

    .line 92
    return-object p0
.end method

.method public wantedSyncState(Z)Lcom/google/android/apps/chrome/services/GoogleServicesStates$GoogleServicesStatesBuilder;
    .locals 1

    .prologue
    .line 96
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/services/GoogleServicesStates$GoogleServicesStatesBuilder;->mTempWantedSyncState:Ljava/lang/Boolean;

    .line 97
    return-object p0
.end method
