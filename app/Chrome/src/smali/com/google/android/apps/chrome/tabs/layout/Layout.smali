.class public abstract Lcom/google/android/apps/chrome/tabs/layout/Layout;
.super Ljava/lang/Object;
.source "Layout.java"

# interfaces
.implements Lcom/google/android/apps/chrome/compositor/TabContentManager$ThumbnailChangeListener;


# static fields
.field public static final NEED_TITLE:Z = true

.field public static final NO_CLOSE_BUTTON:Z = false

.field public static final NO_TITLE:Z = false

.field public static final SHOW_CLOSE_BUTTON:Z = true

.field public static final UNSTALLED_ANIMATION_DURATION_MS:J = 0x1f4L


# instance fields
.field private mContext:Landroid/content/Context;

.field private mCurrentOrientation:I

.field private mEventFilter:Lcom/google/android/apps/chrome/eventfilter/EventFilter;

.field private mHeight:F

.field private mHeightMinusTopControls:F

.field private mIsHiding:Z

.field private mLayoutAnimations:Lcom/google/android/apps/chrome/ChromeAnimation;

.field private mLayoutHelperManager:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;

.field protected mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

.field private mNextTabId:I

.field protected final mRenderHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;

.field private final mStacker:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripStacker;

.field protected mTabContentManager:Lcom/google/android/apps/chrome/compositor/TabContentManager;

.field protected mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

.field private final mUpdateHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;

.field private mWidth:F


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;Lcom/google/android/apps/chrome/eventfilter/EventFilter;Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripStacker;)V
    .locals 2

    .prologue
    const/high16 v1, -0x40800000    # -1.0f

    .line 160
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 147
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mNextTabId:I

    .line 161
    iput-object p1, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mContext:Landroid/content/Context;

    .line 162
    iput-object p2, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mUpdateHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;

    .line 163
    iput-object p3, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mRenderHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;

    .line 164
    iput-object p4, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mEventFilter:Lcom/google/android/apps/chrome/eventfilter/EventFilter;

    .line 165
    iput-object p5, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mStacker:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripStacker;

    .line 168
    iput v1, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mWidth:F

    .line 169
    iput v1, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mHeight:F

    .line 170
    iput v1, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mHeightMinusTopControls:F

    .line 172
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mCurrentOrientation:I

    .line 173
    return-void
.end method

.method public static broadcastAnimationFinished(Landroid/content/Context;Ljava/lang/Class;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 878
    const/16 v0, 0x29

    invoke-static {p0, p1, p2, v0}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->broadcastAnimationStateChanged(Landroid/content/Context;Ljava/lang/Class;Ljava/lang/String;I)V

    .line 880
    return-void
.end method

.method private static broadcastAnimationStateChanged(Landroid/content/Context;Ljava/lang/Class;Ljava/lang/String;I)V
    .locals 3

    .prologue
    .line 884
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 885
    if-eqz p1, :cond_0

    .line 886
    const-string/jumbo v1, "source"

    invoke-virtual {p1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 888
    :cond_0
    if-eqz p2, :cond_1

    .line 889
    const-string/jumbo v1, "animation"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 891
    :cond_1
    invoke-static {p0, p3, v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastImmediateNotification(Landroid/content/Context;ILandroid/os/Bundle;)V

    .line 892
    return-void
.end method


# virtual methods
.method protected addToAnimation(Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJ)V
    .locals 11

    .prologue
    .line 939
    const/4 v10, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    move-wide/from16 v6, p5

    move-wide/from16 v8, p7

    invoke-virtual/range {v1 .. v10}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->addToAnimation(Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJZ)V

    .line 940
    return-void
.end method

.method protected addToAnimation(Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJZ)V
    .locals 13

    .prologue
    .line 948
    invoke-static {}, Lcom/google/android/apps/chrome/ChromeAnimation;->getDecelerateInterpolator()Landroid/view/animation/DecelerateInterpolator;

    move-result-object v11

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move/from16 v4, p3

    move/from16 v5, p4

    move-wide/from16 v6, p5

    move-wide/from16 v8, p7

    move/from16 v10, p9

    invoke-virtual/range {v1 .. v11}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->addToAnimation(Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJZLandroid/view/animation/Interpolator;)V

    .line 950
    return-void
.end method

.method protected addToAnimation(Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJZLandroid/view/animation/Interpolator;)V
    .locals 1

    .prologue
    .line 969
    invoke-static/range {p1 .. p10}, Lcom/google/android/apps/chrome/ChromeAnimation$AnimatableAnimation;->createAnimation(Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;FFJJZLandroid/view/animation/Interpolator;)Lcom/google/android/apps/chrome/ChromeAnimation$Animation;

    move-result-object v0

    .line 971
    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->addToAnimation(Lcom/google/android/apps/chrome/ChromeAnimation$Animation;)V

    .line 972
    return-void
.end method

.method protected addToAnimation(Lcom/google/android/apps/chrome/ChromeAnimation$Animation;)V
    .locals 1

    .prologue
    .line 979
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mLayoutAnimations:Lcom/google/android/apps/chrome/ChromeAnimation;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mLayoutAnimations:Lcom/google/android/apps/chrome/ChromeAnimation;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeAnimation;->finished()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 980
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->onAnimationStarted()V

    .line 981
    new-instance v0, Lcom/google/android/apps/chrome/ChromeAnimation;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/ChromeAnimation;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mLayoutAnimations:Lcom/google/android/apps/chrome/ChromeAnimation;

    .line 982
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mLayoutAnimations:Lcom/google/android/apps/chrome/ChromeAnimation;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeAnimation;->start()V

    .line 984
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/ChromeAnimation$Animation;->start()V

    .line 985
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mLayoutAnimations:Lcom/google/android/apps/chrome/ChromeAnimation;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/ChromeAnimation;->add(Lcom/google/android/apps/chrome/ChromeAnimation$Animation;)V

    .line 986
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->requestUpdate()V

    .line 987
    return-void
.end method

.method protected animationIsRunning()Z
    .locals 1

    .prologue
    .line 993
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mLayoutAnimations:Lcom/google/android/apps/chrome/ChromeAnimation;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mLayoutAnimations:Lcom/google/android/apps/chrome/ChromeAnimation;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeAnimation;->finished()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public attachViews(Landroid/view/ViewGroup;)V
    .locals 0

    .prologue
    .line 461
    return-void
.end method

.method protected cancelAnimation(Lcom/google/android/apps/chrome/ChromeAnimation$Animatable;Ljava/lang/Enum;)V
    .locals 1

    .prologue
    .line 1002
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mLayoutAnimations:Lcom/google/android/apps/chrome/ChromeAnimation;

    if-eqz v0, :cond_0

    .line 1003
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mLayoutAnimations:Lcom/google/android/apps/chrome/ChromeAnimation;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/chrome/ChromeAnimation;->cancel(Ljava/lang/Object;Ljava/lang/Enum;)V

    .line 1005
    :cond_0
    return-void
.end method

.method public click(JFF)V
    .locals 0

    .prologue
    .line 670
    return-void
.end method

.method public contextChanged(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 332
    iput-object p1, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mContext:Landroid/content/Context;

    .line 333
    invoke-static {p1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->resetDimensionConstants(Landroid/content/Context;)V

    .line 334
    return-void
.end method

.method public createLayoutTab(IZZZ)Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;
    .locals 7

    .prologue
    const/high16 v5, -0x40800000    # -1.0f

    .line 258
    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v6, v5

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->createLayoutTab(IZZZFF)Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v0

    return-object v0
.end method

.method public createLayoutTab(IZZZFF)Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;
    .locals 7

    .prologue
    .line 275
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mUpdateHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    invoke-interface/range {v0 .. v6}, Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;->createLayoutTab(IZZZFF)Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    move-result-object v0

    .line 277
    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->initLayoutTabFromHost(Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;)Z

    .line 278
    return-object v0
.end method

.method public detachViews()V
    .locals 0

    .prologue
    .line 466
    return-void
.end method

.method public doneHiding()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 421
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mIsHiding:Z

    .line 422
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mNextTabId:I

    if-eq v0, v2, :cond_1

    .line 423
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mNextTabId:I

    invoke-interface {v0, v1}, Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;->getModelForTabId(I)Lorg/chromium/chrome/browser/tabmodel/TabModel;

    move-result-object v0

    .line 424
    if-eqz v0, :cond_0

    .line 425
    iget v1, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mNextTabId:I

    invoke-static {v0, v1}, Lorg/chromium/chrome/browser/tabmodel/TabModelUtils;->getTabIndexById(Lorg/chromium/chrome/browser/tabmodel/TabList;I)I

    move-result v1

    invoke-static {v0, v1}, Lorg/chromium/chrome/browser/tabmodel/TabModelUtils;->setIndex(Lorg/chromium/chrome/browser/tabmodel/TabModel;I)V

    .line 427
    :cond_0
    iput v2, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mNextTabId:I

    .line 429
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mUpdateHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;->doneHiding()V

    .line 430
    return-void
.end method

.method public drag(JFFFF)V
    .locals 0

    .prologue
    .line 634
    return-void
.end method

.method public fling(JFFFF)V
    .locals 0

    .prologue
    .line 645
    return-void
.end method

.method protected forceAnimationToFinish()V
    .locals 1

    .prologue
    .line 521
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mLayoutAnimations:Lcom/google/android/apps/chrome/ChromeAnimation;

    if-eqz v0, :cond_0

    .line 522
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mLayoutAnimations:Lcom/google/android/apps/chrome/ChromeAnimation;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeAnimation;->updateAndFinish()V

    .line 523
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mLayoutAnimations:Lcom/google/android/apps/chrome/ChromeAnimation;

    .line 524
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->onAnimationFinished()V

    .line 526
    :cond_0
    return-void
.end method

.method public getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 186
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method public getContextualSearchPanelY()F
    .locals 1

    .prologue
    .line 597
    const/4 v0, 0x0

    return v0
.end method

.method public getEventFilter()Lcom/google/android/apps/chrome/eventfilter/EventFilter;
    .locals 1

    .prologue
    .line 1082
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mEventFilter:Lcom/google/android/apps/chrome/eventfilter/EventFilter;

    return-object v0
.end method

.method public getHeight()F
    .locals 1

    .prologue
    .line 539
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mHeight:F

    return v0
.end method

.method public getHeightMinusTopControls()F
    .locals 1

    .prologue
    .line 546
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mHeightMinusTopControls:F

    return v0
.end method

.method protected getLayoutTab(I)Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;
    .locals 2

    .prologue
    .line 1018
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    if-eqz v0, :cond_1

    .line 1019
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 1020
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getId()I

    move-result v1

    if-ne v1, p1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    aget-object v0, v1, v0

    .line 1023
    :goto_1
    return-object v0

    .line 1019
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1023
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public getLayoutTabsDrawnCount()I
    .locals 1

    .prologue
    .line 1066
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mRenderHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;->getLayoutTabsDrawnCount()I

    move-result v0

    return v0
.end method

.method public getLayoutTabsToRender()[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;
    .locals 1

    .prologue
    .line 1011
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mLayoutTabs:[Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    return-object v0
.end method

.method public getModelSelectorButton()Lcom/google/android/apps/chrome/tabs/CompositorButton;
    .locals 1

    .prologue
    .line 215
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mLayoutHelperManager:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mLayoutHelperManager:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->getModelSelectorButton()Lcom/google/android/apps/chrome/tabs/CompositorButton;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getNewTabButton()Lcom/google/android/apps/chrome/tabs/CompositorButton;
    .locals 1

    .prologue
    .line 207
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mLayoutHelperManager:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mLayoutHelperManager:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->getNewTabButton()Lcom/google/android/apps/chrome/tabs/CompositorButton;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getOrientation()I
    .locals 1

    .prologue
    .line 555
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mCurrentOrientation:I

    return v0
.end method

.method public getSizingFlags()I
    .locals 1

    .prologue
    .line 380
    const/16 v0, 0x110

    return v0
.end method

.method public getStripBorderOpacity()F
    .locals 1

    .prologue
    .line 245
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mLayoutHelperManager:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mLayoutHelperManager:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->getBorderOpacity()F

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getStripBrightness()F
    .locals 1

    .prologue
    .line 223
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mLayoutHelperManager:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mLayoutHelperManager:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->getStripBrightness()F

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_0
.end method

.method public getStripHeight()F
    .locals 1

    .prologue
    .line 238
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mLayoutHelperManager:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mLayoutHelperManager:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->getHeight()F

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getStripLayoutTabsToRender()[Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;
    .locals 1

    .prologue
    .line 199
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mLayoutHelperManager:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mLayoutHelperManager:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->getStripLayoutTabsToRender()[Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutTab;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getStripWidth()F
    .locals 1

    .prologue
    .line 231
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mLayoutHelperManager:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mLayoutHelperManager:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->getWidth()F

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getTopControlsOffset(F)F
    .locals 1

    .prologue
    .line 606
    const/high16 v0, 0x7fc00000    # NaNf

    return v0
.end method

.method public getWidth()F
    .locals 1

    .prologue
    .line 532
    iget v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mWidth:F

    return v0
.end method

.method public handlesCloseAll()Z
    .locals 1

    .prologue
    .line 1044
    const/4 v0, 0x0

    return v0
.end method

.method public handlesTabClosing()Z
    .locals 1

    .prologue
    .line 1030
    const/4 v0, 0x0

    return v0
.end method

.method public handlesTabCreating()Z
    .locals 1

    .prologue
    .line 1037
    const/4 v0, 0x0

    return v0
.end method

.method protected initLayoutTabFromHost(Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;)Z
    .locals 2

    .prologue
    .line 619
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->isInitFromHostNeeded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 620
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mUpdateHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getId()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;->initLayoutTabFromHost(I)V

    .line 621
    const/4 v0, 0x1

    .line 623
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isActive()Z
    .locals 1

    .prologue
    .line 193
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mUpdateHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;

    invoke-interface {v0, p0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;->isActiveLayout(Lcom/google/android/apps/chrome/tabs/layout/Layout;)Z

    move-result v0

    return v0
.end method

.method public isContextualSearchLayoutShowing()Z
    .locals 1

    .prologue
    .line 577
    const/4 v0, 0x0

    return v0
.end method

.method public isHiding()Z
    .locals 1

    .prologue
    .line 406
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mIsHiding:Z

    return v0
.end method

.method public isTabInteractive()Z
    .locals 1

    .prologue
    .line 1058
    const/4 v0, 0x0

    return v0
.end method

.method protected notifySizeChanged(FFI)V
    .locals 0

    .prologue
    .line 360
    return-void
.end method

.method protected onAnimationFinished()V
    .locals 3

    .prologue
    .line 929
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mLayoutAnimations:Lcom/google/android/apps/chrome/ChromeAnimation;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->broadcastAnimationFinished(Landroid/content/Context;Ljava/lang/Class;Ljava/lang/String;)V

    .line 930
    :cond_0
    return-void
.end method

.method protected onAnimationStarted()V
    .locals 0

    .prologue
    .line 923
    return-void
.end method

.method public onBackPressed()Z
    .locals 1

    .prologue
    .line 704
    const/4 v0, 0x0

    return v0
.end method

.method public onDown(JFF)V
    .locals 0

    .prologue
    .line 654
    return-void
.end method

.method public onLongPress(JFF)V
    .locals 0

    .prologue
    .line 662
    return-void
.end method

.method public onPinch(JFFFFZ)V
    .locals 0

    .prologue
    .line 687
    return-void
.end method

.method public onTabClosed(JIIZ)V
    .locals 1

    .prologue
    .line 738
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mLayoutHelperManager:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mLayoutHelperManager:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;

    invoke-virtual {v0, p1, p2, p5, p3}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->tabClosed(JZI)V

    .line 739
    :cond_0
    return-void
.end method

.method public onTabClosing(JI)V
    .locals 0

    .prologue
    .line 727
    return-void
.end method

.method public onTabClosureCancelled(JIZ)V
    .locals 1

    .prologue
    .line 851
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mLayoutHelperManager:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;

    if-eqz v0, :cond_0

    .line 852
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mLayoutHelperManager:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;

    invoke-virtual {v0, p1, p2, p4, p3}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->tabClosureCancelled(JZI)V

    .line 854
    :cond_0
    return-void
.end method

.method public onTabClosureCommitted(JIZ)V
    .locals 0

    .prologue
    .line 862
    return-void
.end method

.method public onTabClosurePending(JIZ)V
    .locals 0

    .prologue
    .line 842
    return-void
.end method

.method public onTabCreated(JIIIZZFF)V
    .locals 9

    .prologue
    .line 772
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mLayoutHelperManager:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;

    if-eqz v0, :cond_0

    .line 773
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mLayoutHelperManager:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;

    if-nez p7, :cond_1

    const/4 v7, 0x1

    :goto_0
    move-wide v2, p1

    move v4, p6

    move v5, p3

    move v6, p5

    invoke-virtual/range {v1 .. v7}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->tabCreated(JZIIZ)V

    .line 775
    :cond_0
    return-void

    .line 773
    :cond_1
    const/4 v7, 0x0

    goto :goto_0
.end method

.method public onTabCreating(I)V
    .locals 0

    .prologue
    .line 755
    return-void
.end method

.method public onTabLoadFinished(IZ)V
    .locals 1

    .prologue
    .line 833
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mLayoutHelperManager:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mLayoutHelperManager:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->tabLoadFinished(IZ)V

    .line 834
    :cond_0
    return-void
.end method

.method public onTabLoadStarted(IZ)V
    .locals 1

    .prologue
    .line 824
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mLayoutHelperManager:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mLayoutHelperManager:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->tabLoadStarted(IZ)V

    .line 825
    :cond_0
    return-void
.end method

.method public onTabModelSwitched(Z)V
    .locals 1

    .prologue
    .line 783
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mLayoutHelperManager:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mLayoutHelperManager:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->tabModelSwitched(Z)V

    .line 784
    :cond_0
    return-void
.end method

.method public onTabMoved(JIIIZ)V
    .locals 9

    .prologue
    .line 795
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mLayoutHelperManager:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;

    if-eqz v0, :cond_0

    .line 796
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mLayoutHelperManager:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;

    move-wide v2, p1

    move v4, p6

    move v5, p3

    move v6, p4

    move v7, p5

    invoke-virtual/range {v1 .. v7}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->tabMoved(JZIII)V

    .line 798
    :cond_0
    return-void
.end method

.method public onTabPageLoadFinished(IZ)V
    .locals 1

    .prologue
    .line 815
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mLayoutHelperManager:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mLayoutHelperManager:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->tabPageLoadFinished(IZ)V

    .line 816
    :cond_0
    return-void
.end method

.method public onTabPageLoadStarted(IZ)V
    .locals 1

    .prologue
    .line 806
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mLayoutHelperManager:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mLayoutHelperManager:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->tabPageLoadStarted(IZ)V

    .line 807
    :cond_0
    return-void
.end method

.method public onTabSelected(JIIZ)V
    .locals 7

    .prologue
    .line 715
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mLayoutHelperManager:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;

    if-eqz v0, :cond_0

    .line 716
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mLayoutHelperManager:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;

    move-wide v2, p1

    move v4, p5

    move v5, p3

    move v6, p4

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->tabSelected(JZII)V

    .line 718
    :cond_0
    return-void
.end method

.method public onTabSelecting(JI)V
    .locals 3

    .prologue
    .line 438
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 439
    const-string/jumbo v1, "tabId"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 440
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mContext:Landroid/content/Context;

    const/16 v2, 0x15

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastImmediateNotification(Landroid/content/Context;ILandroid/os/Bundle;)V

    .line 442
    invoke-virtual {p0, p3}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->startHiding(I)V

    .line 443
    return-void
.end method

.method public onTabsAllClosing(JZ)V
    .locals 0

    .prologue
    .line 748
    return-void
.end method

.method public onThumbnailChange(I)V
    .locals 0

    .prologue
    .line 866
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->requestUpdate()V

    .line 867
    return-void
.end method

.method public onUpOrCancel(J)V
    .locals 0

    .prologue
    .line 676
    return-void
.end method

.method public final onUpdate(JJ)Z
    .locals 1

    .prologue
    .line 296
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->onUpdateAnimation(JZ)Z

    move-result v0

    .line 297
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->updateLayout(JJ)V

    .line 298
    return v0
.end method

.method protected onUpdateAnimation(JZ)Z
    .locals 3

    .prologue
    .line 901
    const/4 v0, 0x1

    .line 902
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mLayoutAnimations:Lcom/google/android/apps/chrome/ChromeAnimation;

    if-eqz v1, :cond_2

    .line 903
    if-eqz p3, :cond_3

    .line 904
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mLayoutAnimations:Lcom/google/android/apps/chrome/ChromeAnimation;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/ChromeAnimation;->finished()Z

    move-result v0

    .line 905
    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mLayoutAnimations:Lcom/google/android/apps/chrome/ChromeAnimation;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/ChromeAnimation;->updateAndFinish()V

    .line 910
    :goto_0
    if-nez v0, :cond_0

    if-eqz p3, :cond_1

    .line 911
    :cond_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mLayoutAnimations:Lcom/google/android/apps/chrome/ChromeAnimation;

    .line 912
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->onAnimationFinished()V

    .line 914
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->requestUpdate()V

    .line 916
    :cond_2
    return v0

    .line 907
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mLayoutAnimations:Lcom/google/android/apps/chrome/ChromeAnimation;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/chrome/ChromeAnimation;->update(J)Z

    move-result v0

    goto :goto_0
.end method

.method public releaseTabLayout(Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;)V
    .locals 2

    .prologue
    .line 286
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mUpdateHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;

    invoke-virtual {p1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;->getId()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;->releaseTabLayout(I)V

    .line 287
    return-void
.end method

.method public requestRender()V
    .locals 1

    .prologue
    .line 315
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mRenderHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutRenderHost;->requestRender()V

    .line 316
    return-void
.end method

.method public requestUpdate()V
    .locals 1

    .prologue
    .line 323
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mUpdateHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;->requestUpdate()V

    .line 324
    return-void
.end method

.method public setEventFilter(Lcom/google/android/apps/chrome/eventfilter/EventFilter;)V
    .locals 0

    .prologue
    .line 1075
    iput-object p1, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mEventFilter:Lcom/google/android/apps/chrome/eventfilter/EventFilter;

    .line 1076
    return-void
.end method

.method public setStripLayoutHelperManager(Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;)V
    .locals 0

    .prologue
    .line 179
    iput-object p1, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mLayoutHelperManager:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;

    .line 180
    return-void
.end method

.method public setTabModelSelector(Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;Lcom/google/android/apps/chrome/compositor/TabContentManager;)V
    .locals 1

    .prologue
    .line 370
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mTabContentManager:Lcom/google/android/apps/chrome/compositor/TabContentManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mTabContentManager:Lcom/google/android/apps/chrome/compositor/TabContentManager;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/chrome/compositor/TabContentManager;->removeThumbnailChangeListener(Lcom/google/android/apps/chrome/compositor/TabContentManager$ThumbnailChangeListener;)V

    .line 371
    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    .line 372
    iput-object p2, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mTabContentManager:Lcom/google/android/apps/chrome/compositor/TabContentManager;

    .line 373
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mTabContentManager:Lcom/google/android/apps/chrome/compositor/TabContentManager;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mTabContentManager:Lcom/google/android/apps/chrome/compositor/TabContentManager;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/chrome/compositor/TabContentManager;->addThumbnailChangeListener(Lcom/google/android/apps/chrome/compositor/TabContentManager$ThumbnailChangeListener;)V

    .line 374
    :cond_1
    return-void
.end method

.method public shouldDisplayContentOverlay()Z
    .locals 1

    .prologue
    .line 1051
    const/4 v0, 0x0

    return v0
.end method

.method public shouldShowContextualSearchView()Z
    .locals 1

    .prologue
    .line 587
    const/4 v0, 0x0

    return v0
.end method

.method public show(JZ)V
    .locals 2

    .prologue
    .line 451
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mIsHiding:Z

    .line 452
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mNextTabId:I

    .line 454
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mLayoutHelperManager:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mLayoutHelperManager:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;

    iget-object v1, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mStacker:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripStacker;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->setTabStacker(Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripStacker;)V

    .line 455
    :cond_0
    return-void
.end method

.method public final sizeChanged(FFFI)V
    .locals 0

    .prologue
    .line 346
    iput p1, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mWidth:F

    .line 347
    iput p2, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mHeight:F

    .line 348
    iput p3, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mHeightMinusTopControls:F

    .line 349
    iput p4, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mCurrentOrientation:I

    .line 350
    invoke-virtual {p0, p1, p2, p4}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->notifySizeChanged(FFI)V

    .line 351
    return-void
.end method

.method public startHiding(I)V
    .locals 1

    .prologue
    .line 396
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/tabs/layout/Layout;->getLayoutTab(I)Lcom/google/android/apps/chrome/tabs/layout/LayoutTab;

    .line 397
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mUpdateHost:Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;

    invoke-interface {v0, p1}, Lcom/google/android/apps/chrome/tabs/layout/LayoutUpdateHost;->startHiding(I)V

    .line 398
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mIsHiding:Z

    .line 399
    iput p1, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mNextTabId:I

    .line 400
    return-void
.end method

.method public swipeFinished(J)V
    .locals 0

    .prologue
    .line 495
    return-void
.end method

.method public swipeFlingOccurred(JFFFFFF)V
    .locals 0

    .prologue
    .line 515
    return-void
.end method

.method public swipeStarted(JLcom/google/android/apps/chrome/eventfilter/EdgeSwipeEventFilter$ScrollDirection;FF)V
    .locals 0

    .prologue
    .line 475
    return-void
.end method

.method public swipeUpdated(JFFFFFF)V
    .locals 0

    .prologue
    .line 488
    return-void
.end method

.method public unstallImmediately()V
    .locals 0

    .prologue
    .line 692
    return-void
.end method

.method public unstallImmediately(I)V
    .locals 0

    .prologue
    .line 698
    return-void
.end method

.method protected updateCacheVisibleIds(Ljava/util/List;)V
    .locals 1

    .prologue
    .line 388
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mTabContentManager:Lcom/google/android/apps/chrome/compositor/TabContentManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mTabContentManager:Lcom/google/android/apps/chrome/compositor/TabContentManager;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/compositor/TabContentManager;->updateVisibleIds(Ljava/util/List;)V

    .line 389
    :cond_0
    return-void
.end method

.method protected updateLayout(JJ)V
    .locals 1

    .prologue
    .line 307
    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mLayoutHelperManager:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/tabs/layout/Layout;->mLayoutHelperManager:Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/android/apps/chrome/tabs/layout/tablet/helper/StripLayoutHelperManager;->updateLayout(JJ)Z

    .line 308
    :cond_0
    return-void
.end method
