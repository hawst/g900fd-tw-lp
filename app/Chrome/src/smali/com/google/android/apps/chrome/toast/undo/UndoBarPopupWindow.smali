.class public Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupWindow;
.super Landroid/widget/PopupWindow;
.source "UndoBarPopupWindow.java"


# instance fields
.field private final mAnimationDuration:I

.field private final mMessageView:Lcom/google/android/apps/chrome/toast/undo/TemplatePreservingTextView;

.field private final mUndoButtonView:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;Landroid/view/View$OnClickListener;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 33
    invoke-direct {p0}, Landroid/widget/PopupWindow;-><init>()V

    .line 34
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$layout;->undo_bar:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 35
    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupWindow;->setContentView(Landroid/view/View;)V

    .line 36
    sget v0, Lcom/google/android/apps/chrome/R$id;->undobar_message:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/toast/undo/TemplatePreservingTextView;

    iput-object v0, p0, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupWindow;->mMessageView:Lcom/google/android/apps/chrome/toast/undo/TemplatePreservingTextView;

    .line 37
    iget-object v0, p0, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupWindow;->mMessageView:Lcom/google/android/apps/chrome/toast/undo/TemplatePreservingTextView;

    invoke-virtual {v0, p3}, Lcom/google/android/apps/chrome/toast/undo/TemplatePreservingTextView;->setTemplate(Ljava/lang/String;)V

    .line 38
    sget v0, Lcom/google/android/apps/chrome/R$id;->undobar_button:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupWindow;->mUndoButtonView:Landroid/view/View;

    .line 39
    invoke-virtual {v1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v2, 0x10e0000

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupWindow;->mAnimationDuration:I

    .line 41
    iget-object v0, p0, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupWindow;->mUndoButtonView:Landroid/view/View;

    invoke-virtual {v0, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 44
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/ui/base/DeviceFormFactor;->isTablet(Landroid/content/Context;)Z

    move-result v0

    .line 45
    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Lcom/google/android/apps/chrome/R$dimen;->undo_bar_tablet_width:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupWindow;->setWidth(I)V

    .line 49
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Lcom/google/android/apps/chrome/R$dimen;->undo_bar_height:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupWindow;->setHeight(I)V

    .line 51
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupWindow;->getHeight()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {v1, v0}, Landroid/view/View;->setTranslationY(F)V

    .line 52
    invoke-virtual {v1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupWindow;->mAnimationDuration:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    .line 53
    return-void

    .line 45
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v0

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupWindow;)V
    .locals 0

    .prologue
    .line 21
    invoke-super {p0}, Landroid/widget/PopupWindow;->dismiss()V

    return-void
.end method


# virtual methods
.method public dismiss()V
    .locals 4

    .prologue
    .line 60
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupWindow;->getContentView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupWindow;->mAnimationDuration:I

    div-int/lit8 v1, v1, 0x2

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupWindow$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupWindow$1;-><init>(Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupWindow;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 67
    return-void
.end method

.method public setUndoText(Ljava/lang/String;Z)V
    .locals 4

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupWindow;->mMessageView:Lcom/google/android/apps/chrome/toast/undo/TemplatePreservingTextView;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/toast/undo/TemplatePreservingTextView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 84
    if-eqz p2, :cond_0

    .line 85
    iget-object v0, p0, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupWindow;->mMessageView:Lcom/google/android/apps/chrome/toast/undo/TemplatePreservingTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/toast/undo/TemplatePreservingTextView;->setAlpha(F)V

    .line 86
    iget-object v0, p0, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupWindow;->mMessageView:Lcom/google/android/apps/chrome/toast/undo/TemplatePreservingTextView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/toast/undo/TemplatePreservingTextView;->setText(Ljava/lang/CharSequence;)V

    .line 87
    iget-object v0, p0, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupWindow;->mMessageView:Lcom/google/android/apps/chrome/toast/undo/TemplatePreservingTextView;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/toast/undo/TemplatePreservingTextView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupWindow;->mAnimationDuration:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 91
    :goto_0
    return-void

    .line 89
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/toast/undo/UndoBarPopupWindow;->mMessageView:Lcom/google/android/apps/chrome/toast/undo/TemplatePreservingTextView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/toast/undo/TemplatePreservingTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
