.class final enum Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter$GestureOrientation;
.super Ljava/lang/Enum;
.source "ContextualSearchEventFilter.java"


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter$GestureOrientation;

.field public static final enum HORIZONTAL:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter$GestureOrientation;

.field public static final enum UNDETERMINED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter$GestureOrientation;

.field public static final enum VERTICAL:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter$GestureOrientation;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 41
    new-instance v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter$GestureOrientation;

    const-string/jumbo v1, "UNDETERMINED"

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter$GestureOrientation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter$GestureOrientation;->UNDETERMINED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter$GestureOrientation;

    .line 42
    new-instance v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter$GestureOrientation;

    const-string/jumbo v1, "HORIZONTAL"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter$GestureOrientation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter$GestureOrientation;->HORIZONTAL:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter$GestureOrientation;

    .line 43
    new-instance v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter$GestureOrientation;

    const-string/jumbo v1, "VERTICAL"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter$GestureOrientation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter$GestureOrientation;->VERTICAL:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter$GestureOrientation;

    .line 40
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter$GestureOrientation;

    sget-object v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter$GestureOrientation;->UNDETERMINED:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter$GestureOrientation;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter$GestureOrientation;->HORIZONTAL:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter$GestureOrientation;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter$GestureOrientation;->VERTICAL:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter$GestureOrientation;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter$GestureOrientation;->$VALUES:[Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter$GestureOrientation;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter$GestureOrientation;
    .locals 1

    .prologue
    .line 40
    const-class v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter$GestureOrientation;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter$GestureOrientation;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter$GestureOrientation;
    .locals 1

    .prologue
    .line 40
    sget-object v0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter$GestureOrientation;->$VALUES:[Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter$GestureOrientation;

    invoke-virtual {v0}, [Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter$GestureOrientation;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchEventFilter$GestureOrientation;

    return-object v0
.end method
