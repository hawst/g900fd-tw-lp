.class Lcom/google/android/apps/chrome/document/DocumentTab$DocumentContextMenuItemDelegate;
.super Lorg/chromium/chrome/browser/Tab$TabChromeContextMenuItemDelegate;
.source "DocumentTab.java"


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/document/DocumentTab;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/chrome/document/DocumentTab;)V
    .locals 0

    .prologue
    .line 249
    iput-object p1, p0, Lcom/google/android/apps/chrome/document/DocumentTab$DocumentContextMenuItemDelegate;->this$0:Lcom/google/android/apps/chrome/document/DocumentTab;

    invoke-direct {p0, p1}, Lorg/chromium/chrome/browser/Tab$TabChromeContextMenuItemDelegate;-><init>(Lorg/chromium/chrome/browser/Tab;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/chrome/document/DocumentTab;Lcom/google/android/apps/chrome/document/DocumentTab$1;)V
    .locals 0

    .prologue
    .line 249
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/document/DocumentTab$DocumentContextMenuItemDelegate;-><init>(Lcom/google/android/apps/chrome/document/DocumentTab;)V

    return-void
.end method


# virtual methods
.method public canLoadOriginalImage()Z
    .locals 1

    .prologue
    .line 257
    const/4 v0, 0x0

    return v0
.end method

.method public isIncognitoSupported()Z
    .locals 1

    .prologue
    .line 252
    invoke-static {}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->getInstance()Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromeNativePreferences;->isIncognitoModeEnabled()Z

    move-result v0

    return v0
.end method

.method public onOpenImageInNewTab(Ljava/lang/String;Lorg/chromium/content_public/common/Referrer;)V
    .locals 5

    .prologue
    .line 290
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentTab$DocumentContextMenuItemDelegate;->this$0:Lcom/google/android/apps/chrome/document/DocumentTab;

    # invokes: Lcom/google/android/apps/chrome/document/DocumentTab;->isSpdyProxyEnabledForUrl(Ljava/lang/String;)Z
    invoke-static {v0, p1}, Lcom/google/android/apps/chrome/document/DocumentTab;->access$400(Lcom/google/android/apps/chrome/document/DocumentTab;Ljava/lang/String;)Z

    move-result v0

    .line 292
    new-instance v1, Lorg/chromium/content_public/browser/LoadUrlParams;

    invoke-direct {v1, p1}, Lorg/chromium/content_public/browser/LoadUrlParams;-><init>(Ljava/lang/String;)V

    .line 293
    if-eqz v0, :cond_0

    const-string/jumbo v0, "X-PSA-Client-Options: v=1,m=1\nCache-Control: no-cache"

    :goto_0
    invoke-virtual {v1, v0}, Lorg/chromium/content_public/browser/LoadUrlParams;->setVerbatimHeaders(Ljava/lang/String;)V

    .line 294
    invoke-virtual {v1, p2}, Lorg/chromium/content_public/browser/LoadUrlParams;->setReferrer(Lorg/chromium/content_public/common/Referrer;)V

    .line 295
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentTab$DocumentContextMenuItemDelegate;->this$0:Lcom/google/android/apps/chrome/document/DocumentTab;

    # getter for: Lcom/google/android/apps/chrome/document/DocumentTab;->mActivity:Lcom/google/android/apps/chrome/document/DocumentActivity;
    invoke-static {v0}, Lcom/google/android/apps/chrome/document/DocumentTab;->access$500(Lcom/google/android/apps/chrome/document/DocumentTab;)Lcom/google/android/apps/chrome/document/DocumentActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/document/DocumentActivity;->getTabModelSelector()Lcom/google/android/apps/chrome/tabmodel/SingleTabModelSelector;

    move-result-object v0

    sget-object v2, Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;->FROM_LONGPRESS_BACKGROUND:Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;

    iget-object v3, p0, Lcom/google/android/apps/chrome/document/DocumentTab$DocumentContextMenuItemDelegate;->this$0:Lcom/google/android/apps/chrome/document/DocumentTab;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentTab$DocumentContextMenuItemDelegate;->isIncognito()Z

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/apps/chrome/tabmodel/SingleTabModelSelector;->openNewTab(Lorg/chromium/content_public/browser/LoadUrlParams;Lorg/chromium/chrome/browser/tabmodel/TabModel$TabLaunchType;Lorg/chromium/chrome/browser/Tab;Z)Lorg/chromium/chrome/browser/Tab;

    .line 297
    return-void

    .line 293
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onOpenImageUrl(Ljava/lang/String;Lorg/chromium/content_public/common/Referrer;)V
    .locals 2

    .prologue
    .line 301
    new-instance v0, Lorg/chromium/content_public/browser/LoadUrlParams;

    invoke-direct {v0, p1}, Lorg/chromium/content_public/browser/LoadUrlParams;-><init>(Ljava/lang/String;)V

    .line 302
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lorg/chromium/content_public/browser/LoadUrlParams;->setTransitionType(I)V

    .line 303
    invoke-virtual {v0, p2}, Lorg/chromium/content_public/browser/LoadUrlParams;->setReferrer(Lorg/chromium/content_public/common/Referrer;)V

    .line 304
    iget-object v1, p0, Lcom/google/android/apps/chrome/document/DocumentTab$DocumentContextMenuItemDelegate;->this$0:Lcom/google/android/apps/chrome/document/DocumentTab;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/document/DocumentTab;->loadUrl(Lorg/chromium/content_public/browser/LoadUrlParams;)I

    .line 305
    return-void
.end method

.method public onOpenInNewIncognitoTab(Ljava/lang/String;)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 281
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentTab$DocumentContextMenuItemDelegate;->this$0:Lcom/google/android/apps/chrome/document/DocumentTab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/document/DocumentTab;->getWindowAndroid()Lorg/chromium/ui/base/WindowAndroid;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/ui/base/WindowAndroid;->getActivity()Ljava/lang/ref/WeakReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    const/4 v1, 0x1

    const/16 v4, 0xc9

    const/4 v5, 0x6

    const/4 v7, 0x0

    move-object v3, p1

    move v6, v2

    invoke-static/range {v0 .. v7}, Lcom/google/android/apps/chrome/document/ChromeLauncherActivity;->launchInstance(Landroid/app/Activity;ZILjava/lang/String;IIZLcom/google/android/apps/chrome/document/DocumentActivityDatabase$PendingDocumentData;)V

    .line 286
    return-void
.end method

.method public onOpenInNewTab(Ljava/lang/String;Lorg/chromium/content_public/common/Referrer;)V
    .locals 8

    .prologue
    .line 270
    new-instance v7, Lcom/google/android/apps/chrome/document/DocumentActivityDatabase$PendingDocumentData;

    invoke-direct {v7}, Lcom/google/android/apps/chrome/document/DocumentActivityDatabase$PendingDocumentData;-><init>()V

    .line 271
    iput-object p2, v7, Lcom/google/android/apps/chrome/document/DocumentActivityDatabase$PendingDocumentData;->referrer:Lorg/chromium/content_public/common/Referrer;

    .line 272
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentTab$DocumentContextMenuItemDelegate;->this$0:Lcom/google/android/apps/chrome/document/DocumentTab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/document/DocumentTab;->getWindowAndroid()Lorg/chromium/ui/base/WindowAndroid;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/ui/base/WindowAndroid;->getActivity()Ljava/lang/ref/WeakReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/document/DocumentTab$DocumentContextMenuItemDelegate;->isIncognito()Z

    move-result v1

    const/4 v2, 0x1

    const/16 v4, 0xc9

    const/4 v5, 0x6

    const/4 v6, 0x0

    move-object v3, p1

    invoke-static/range {v0 .. v7}, Lcom/google/android/apps/chrome/document/ChromeLauncherActivity;->launchInstance(Landroid/app/Activity;ZILjava/lang/String;IIZLcom/google/android/apps/chrome/document/DocumentActivityDatabase$PendingDocumentData;)V

    .line 277
    return-void
.end method

.method public onSearchByImageInNewTab()V
    .locals 1

    .prologue
    .line 309
    invoke-static {}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->recordContextMenuSearchByImage()V

    .line 310
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentTab$DocumentContextMenuItemDelegate;->this$0:Lcom/google/android/apps/chrome/document/DocumentTab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/document/DocumentTab;->triggerSearchByImage()V

    .line 311
    return-void
.end method

.method public startDownload(Ljava/lang/String;Z)Z
    .locals 1

    .prologue
    .line 262
    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentTab$DocumentContextMenuItemDelegate;->this$0:Lcom/google/android/apps/chrome/document/DocumentTab;

    # invokes: Lcom/google/android/apps/chrome/document/DocumentTab;->shouldInterceptContextMenuDownload(Ljava/lang/String;)Z
    invoke-static {v0, p1}, Lcom/google/android/apps/chrome/document/DocumentTab;->access$300(Lcom/google/android/apps/chrome/document/DocumentTab;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 263
    const/4 v0, 0x0

    .line 265
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method
