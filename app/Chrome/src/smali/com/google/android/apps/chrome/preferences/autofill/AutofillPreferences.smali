.class public Lcom/google/android/apps/chrome/preferences/autofill/AutofillPreferences;
.super Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;
.source "AutofillPreferences.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;
.implements Lorg/chromium/chrome/browser/autofill/PersonalDataManager$PersonalDataManagerObserver;


# static fields
.field public static final AUTOFILL_GUID:Ljava/lang/String; = "guid"

.field public static final SETTINGS_ORIGIN:Ljava/lang/String; = "Chrome settings"


# instance fields
.field private mActionBarSwitch:Landroid/widget/Switch;

.field private mAutofillEnabler:Lcom/google/android/apps/chrome/preferences/WidgetEnabler;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;-><init>()V

    return-void
.end method

.method private rebuildCreditCardList()V
    .locals 6

    .prologue
    .line 89
    const-string/jumbo v0, "autofill_credit_cards"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/autofill/AutofillPreferences;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceGroup;

    .line 90
    invoke-virtual {v0}, Landroid/preference/PreferenceGroup;->removeAll()V

    .line 91
    invoke-static {}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager;->getInstance()Lorg/chromium/chrome/browser/autofill/PersonalDataManager;

    move-result-object v1

    invoke-virtual {v1}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager;->getCreditCards()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/chromium/chrome/browser/autofill/PersonalDataManager$CreditCard;

    .line 93
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/autofill/AutofillPreferences;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/autofill/AutofillPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/preference/PreferenceManager;->createPreferenceScreen(Landroid/content/Context;)Landroid/preference/PreferenceScreen;

    move-result-object v3

    .line 94
    invoke-virtual {v1}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager$CreditCard;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/preference/PreferenceScreen;->setTitle(Ljava/lang/CharSequence;)V

    .line 95
    invoke-virtual {v1}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager$CreditCard;->getObfuscatedNumber()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/preference/PreferenceScreen;->setSummary(Ljava/lang/CharSequence;)V

    .line 96
    const-class v4, Lcom/google/android/apps/chrome/preferences/autofill/AutofillCreditCardEditor;

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/preference/PreferenceScreen;->setFragment(Ljava/lang/String;)V

    .line 97
    invoke-virtual {v3}, Landroid/preference/PreferenceScreen;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    .line 98
    const-string/jumbo v5, "guid"

    invoke-virtual {v1}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager$CreditCard;->getGUID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v5, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    invoke-virtual {v0, v3}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    goto :goto_0

    .line 101
    :cond_0
    return-void
.end method

.method private rebuildLists()V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/autofill/AutofillPreferences;->rebuildProfileList()V

    .line 60
    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/autofill/AutofillPreferences;->rebuildCreditCardList()V

    .line 61
    return-void
.end method

.method private rebuildProfileList()V
    .locals 7

    .prologue
    const/16 v6, 0x28

    .line 66
    const-string/jumbo v0, "autofill_profiles"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/autofill/AutofillPreferences;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceGroup;

    .line 67
    invoke-virtual {v0}, Landroid/preference/PreferenceGroup;->removeAll()V

    .line 68
    invoke-static {}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager;->getInstance()Lorg/chromium/chrome/browser/autofill/PersonalDataManager;

    move-result-object v1

    invoke-virtual {v1}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager;->getProfiles()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/chromium/chrome/browser/autofill/PersonalDataManager$AutofillProfile;

    .line 70
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/autofill/AutofillPreferences;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/autofill/AutofillPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/preference/PreferenceManager;->createPreferenceScreen(Landroid/content/Context;)Landroid/preference/PreferenceScreen;

    move-result-object v4

    .line 71
    invoke-virtual {v1}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager$AutofillProfile;->getFullName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Landroid/preference/PreferenceScreen;->setTitle(Ljava/lang/CharSequence;)V

    .line 74
    invoke-virtual {v1}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager$AutofillProfile;->getLabel()Ljava/lang/String;

    move-result-object v2

    .line 75
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v5

    if-le v5, v6, :cond_0

    .line 76
    const/4 v5, 0x0

    invoke-virtual {v2, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 77
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v5, "..."

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 79
    :cond_0
    invoke-virtual {v4, v2}, Landroid/preference/PreferenceScreen;->setSummary(Ljava/lang/CharSequence;)V

    .line 81
    const-class v2, Lcom/google/android/apps/chrome/preferences/autofill/AutofillProfileEditor;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Landroid/preference/PreferenceScreen;->setFragment(Ljava/lang/String;)V

    .line 82
    invoke-virtual {v4}, Landroid/preference/PreferenceScreen;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    .line 83
    const-string/jumbo v5, "guid"

    invoke-virtual {v1}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager$AutofillProfile;->getGUID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v5, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    invoke-virtual {v0, v4}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    goto :goto_0

    .line 86
    :cond_1
    return-void
.end method


# virtual methods
.method public onAttach(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 128
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;->onAttach(Landroid/app/Activity;)V

    .line 129
    invoke-static {}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager;->getInstance()Lorg/chromium/chrome/browser/autofill/PersonalDataManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager;->registerDataObserver(Lorg/chromium/chrome/browser/autofill/PersonalDataManager$PersonalDataManagerObserver;)V

    .line 130
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 41
    invoke-super {p0, p1}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    .line 42
    sget v0, Lcom/google/android/apps/chrome/R$xml;->autofill_preferences:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/autofill/AutofillPreferences;->addPreferencesFromResource(I)V

    .line 43
    new-instance v0, Landroid/widget/Switch;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/autofill/AutofillPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/Switch;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/preferences/autofill/AutofillPreferences;->mActionBarSwitch:Landroid/widget/Switch;

    .line 44
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/autofill/AutofillPreferences;->mActionBarSwitch:Landroid/widget/Switch;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/autofill/AutofillPreferences;->addSwitchToActionBar(Landroid/widget/Switch;)V

    .line 45
    new-instance v0, Lcom/google/android/apps/chrome/preferences/autofill/AutofillEnabler;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/preferences/autofill/AutofillPreferences;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/preferences/autofill/AutofillPreferences;->mActionBarSwitch:Landroid/widget/Switch;

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/chrome/preferences/autofill/AutofillEnabler;-><init>(Landroid/content/Context;Landroid/widget/Switch;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/preferences/autofill/AutofillPreferences;->mAutofillEnabler:Lcom/google/android/apps/chrome/preferences/WidgetEnabler;

    .line 46
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/autofill/AutofillPreferences;->mAutofillEnabler:Lcom/google/android/apps/chrome/preferences/WidgetEnabler;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/preferences/WidgetEnabler;->attach()V

    .line 47
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 120
    invoke-super {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;->onDestroy()V

    .line 121
    invoke-static {}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager;->getInstance()Lorg/chromium/chrome/browser/autofill/PersonalDataManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lorg/chromium/chrome/browser/autofill/PersonalDataManager;->unregisterDataObserver(Lorg/chromium/chrome/browser/autofill/PersonalDataManager$PersonalDataManagerObserver;)V

    .line 122
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/autofill/AutofillPreferences;->mAutofillEnabler:Lcom/google/android/apps/chrome/preferences/WidgetEnabler;

    invoke-interface {v0}, Lcom/google/android/apps/chrome/preferences/WidgetEnabler;->destroy()V

    .line 123
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/autofill/AutofillPreferences;->mActionBarSwitch:Landroid/widget/Switch;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/Switch;->setVisibility(I)V

    .line 124
    return-void
.end method

.method public onPersonalDataChanged()V
    .locals 0

    .prologue
    .line 115
    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/autofill/AutofillPreferences;->rebuildLists()V

    .line 116
    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/autofill/AutofillPreferences;->rebuildLists()V

    .line 52
    const/4 v0, 0x1

    return v0
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 105
    invoke-super {p0}, Lcom/google/android/apps/chrome/preferences/ChromeBasePreferenceFragment;->onResume()V

    .line 110
    invoke-direct {p0}, Lcom/google/android/apps/chrome/preferences/autofill/AutofillPreferences;->rebuildLists()V

    .line 111
    return-void
.end method
