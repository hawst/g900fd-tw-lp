.class public Lcom/google/android/apps/chrome/document/DocumentTab$DocumentTabChromeWebContentsDelegateAndroidImpl;
.super Lcom/google/android/apps/chrome/tab/ChromeTab$TabChromeWebContentsDelegateAndroidImpl;
.source "DocumentTab.java"


# instance fields
.field private final mContentsToUrlMapping:Ljava/util/Map;

.field final synthetic this$0:Lcom/google/android/apps/chrome/document/DocumentTab;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/chrome/document/DocumentTab;)V
    .locals 1

    .prologue
    .line 322
    iput-object p1, p0, Lcom/google/android/apps/chrome/document/DocumentTab$DocumentTabChromeWebContentsDelegateAndroidImpl;->this$0:Lcom/google/android/apps/chrome/document/DocumentTab;

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/tab/ChromeTab$TabChromeWebContentsDelegateAndroidImpl;-><init>(Lcom/google/android/apps/chrome/tab/ChromeTab;)V

    .line 324
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentTab$DocumentTabChromeWebContentsDelegateAndroidImpl;->mContentsToUrlMapping:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method public addNewContents(JJILandroid/graphics/Rect;Z)Z
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v6, 0x0

    .line 338
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentTab$DocumentTabChromeWebContentsDelegateAndroidImpl;->this$0:Lcom/google/android/apps/chrome/document/DocumentTab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/document/DocumentTab;->isClosing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 350
    :goto_0
    return v6

    .line 339
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentTab$DocumentTabChromeWebContentsDelegateAndroidImpl;->mContentsToUrlMapping:Ljava/util/Map;

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Ljava/lang/String;

    .line 340
    new-instance v7, Lcom/google/android/apps/chrome/document/DocumentActivityDatabase$PendingDocumentData;

    invoke-direct {v7}, Lcom/google/android/apps/chrome/document/DocumentActivityDatabase$PendingDocumentData;-><init>()V

    .line 341
    iput-wide p3, v7, Lcom/google/android/apps/chrome/document/DocumentActivityDatabase$PendingDocumentData;->nativeWebContents:J

    .line 342
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentTab$DocumentTabChromeWebContentsDelegateAndroidImpl;->this$0:Lcom/google/android/apps/chrome/document/DocumentTab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/document/DocumentTab;->getWindowAndroid()Lorg/chromium/ui/base/WindowAndroid;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/ui/base/WindowAndroid;->getActivity()Ljava/lang/ref/WeakReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/android/apps/chrome/document/DocumentTab$DocumentTabChromeWebContentsDelegateAndroidImpl;->this$0:Lcom/google/android/apps/chrome/document/DocumentTab;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/document/DocumentTab;->isIncognito()Z

    move-result v1

    if-eqz v3, :cond_1

    :goto_1
    const/16 v4, 0xc8

    const/4 v5, 0x6

    invoke-static/range {v0 .. v7}, Lcom/google/android/apps/chrome/document/ChromeLauncherActivity;->launchInstance(Landroid/app/Activity;ZILjava/lang/String;IIZLcom/google/android/apps/chrome/document/DocumentActivityDatabase$PendingDocumentData;)V

    move v6, v2

    .line 350
    goto :goto_0

    .line 342
    :cond_1
    const-string/jumbo v3, ""

    goto :goto_1
.end method

.method public webContentsCreated(JJLjava/lang/String;Ljava/lang/String;J)V
    .locals 3

    .prologue
    .line 329
    invoke-super/range {p0 .. p8}, Lcom/google/android/apps/chrome/tab/ChromeTab$TabChromeWebContentsDelegateAndroidImpl;->webContentsCreated(JJLjava/lang/String;Ljava/lang/String;J)V

    .line 331
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentTab$DocumentTabChromeWebContentsDelegateAndroidImpl;->mContentsToUrlMapping:Ljava/util/Map;

    invoke-static {p7, p8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1, p6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 332
    invoke-static {}, Lcom/google/android/apps/chrome/document/DocumentWebContentsDelegate;->getInstance()Lcom/google/android/apps/chrome/document/DocumentWebContentsDelegate;

    move-result-object v0

    invoke-virtual {v0, p7, p8}, Lcom/google/android/apps/chrome/document/DocumentWebContentsDelegate;->attachDelegate(J)V

    .line 333
    return-void
.end method
