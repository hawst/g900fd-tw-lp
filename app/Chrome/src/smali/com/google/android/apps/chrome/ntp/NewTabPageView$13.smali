.class Lcom/google/android/apps/chrome/ntp/NewTabPageView$13;
.super Ljava/lang/Object;
.source "NewTabPageView.java"

# interfaces
.implements Lorg/chromium/chrome/browser/favicon/FaviconHelper$FaviconImageCallback;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/ntp/NewTabPageView;

.field final synthetic val$isInitialLoad:Z

.field final synthetic val$item:Lcom/google/android/apps/chrome/ntp/MostVisitedItem;

.field final synthetic val$url:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/ntp/NewTabPageView;Ljava/lang/String;Lcom/google/android/apps/chrome/ntp/MostVisitedItem;Z)V
    .locals 0

    .prologue
    .line 691
    iput-object p1, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView$13;->this$0:Lcom/google/android/apps/chrome/ntp/NewTabPageView;

    iput-object p2, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView$13;->val$url:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView$13;->val$item:Lcom/google/android/apps/chrome/ntp/MostVisitedItem;

    iput-boolean p4, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView$13;->val$isInitialLoad:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFaviconAvailable(Landroid/graphics/Bitmap;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 694
    if-nez p1, :cond_0

    .line 695
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView$13;->this$0:Lcom/google/android/apps/chrome/ntp/NewTabPageView;

    # getter for: Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mFaviconGenerator:Lcom/google/android/apps/chrome/widget/TabIconGenerator;
    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->access$1100(Lcom/google/android/apps/chrome/ntp/NewTabPageView;)Lcom/google/android/apps/chrome/widget/TabIconGenerator;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView$13;->val$url:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/widget/TabIconGenerator;->generateIconForTab(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object p1

    .line 697
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView$13;->val$item:Lcom/google/android/apps/chrome/ntp/MostVisitedItem;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/chrome/ntp/MostVisitedItem;->setFavicon(Landroid/graphics/Bitmap;)V

    .line 698
    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView$13;->this$0:Lcom/google/android/apps/chrome/ntp/NewTabPageView;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/apps/chrome/ntp/NewTabPageView;->mSnapshotMostVisitedChanged:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->access$902(Lcom/google/android/apps/chrome/ntp/NewTabPageView;Z)Z

    .line 699
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView$13;->val$isInitialLoad:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/ntp/NewTabPageView$13;->this$0:Lcom/google/android/apps/chrome/ntp/NewTabPageView;

    # invokes: Lcom/google/android/apps/chrome/ntp/NewTabPageView;->loadTaskCompleted()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/ntp/NewTabPageView;->access$1000(Lcom/google/android/apps/chrome/ntp/NewTabPageView;)V

    .line 700
    :cond_1
    return-void
.end method
