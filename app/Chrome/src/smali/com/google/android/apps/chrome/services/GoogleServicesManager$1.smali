.class Lcom/google/android/apps/chrome/services/GoogleServicesManager$1;
.super Landroid/os/AsyncTask;
.source "GoogleServicesManager.java"


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/services/GoogleServicesManager;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/services/GoogleServicesManager;)V
    .locals 0

    .prologue
    .line 263
    iput-object p1, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager$1;->this$0:Lcom/google/android/apps/chrome/services/GoogleServicesManager;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 263
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/services/GoogleServicesManager$1;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 2

    .prologue
    .line 266
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager$1;->this$0:Lcom/google/android/apps/chrome/services/GoogleServicesManager;

    iget-object v0, v0, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->mContext:Landroid/content/Context;

    new-instance v1, Lcom/google/android/apps/chrome/minutemaid/ChainedAccountsChangedReceiver$SystemAccountChangeEventChecker;

    invoke-direct {v1}, Lcom/google/android/apps/chrome/minutemaid/ChainedAccountsChangedReceiver$SystemAccountChangeEventChecker;-><init>()V

    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->updateAccountRenameData(Landroid/content/Context;Lcom/google/android/apps/chrome/services/GoogleServicesManager$AccountChangeEventChecker;)V

    .line 267
    const/4 v0, 0x0

    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 263
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/services/GoogleServicesManager$1;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 272
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager$1;->this$0:Lcom/google/android/apps/chrome/services/GoogleServicesManager;

    iget-object v0, v0, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->getNewSignedInAccountName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 273
    if-nez v0, :cond_0

    .line 274
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager$1;->this$0:Lcom/google/android/apps/chrome/services/GoogleServicesManager;

    invoke-virtual {v0, v1, v1}, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->signOut(Landroid/app/Activity;Ljava/lang/Runnable;)V

    .line 278
    :goto_0
    return-void

    .line 276
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/GoogleServicesManager$1;->this$0:Lcom/google/android/apps/chrome/services/GoogleServicesManager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/services/GoogleServicesManager;->validateAccountSettings(Z)V

    goto :goto_0
.end method
