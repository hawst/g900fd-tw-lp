.class final Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$5;
.super Landroid/os/AsyncTask;
.source "KnoxAuditLogger.java"


# instance fields
.field final synthetic val$componentName:Ljava/lang/String;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$event:Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$AuditEvent;

.field final synthetic val$message:Ljava/lang/String;

.field final synthetic val$url:Ljava/lang/String;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$AuditEvent;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 130
    iput-object p1, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$5;->val$context:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$5;->val$event:Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$AuditEvent;

    iput-object p3, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$5;->val$componentName:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$5;->val$url:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$5;->val$message:Ljava/lang/String;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected final bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 130
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$5;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected final varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 12

    .prologue
    .line 133
    iget-object v0, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$5;->val$context:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$5;->val$event:Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$AuditEvent;

    invoke-virtual {v1}, Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$AuditEvent;->name()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-object v4, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$5;->val$event:Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$AuditEvent;

    invoke-virtual {v4}, Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$AuditEvent;->getSeverity()I

    move-result v4

    iget-object v5, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$5;->val$event:Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$AuditEvent;

    invoke-virtual {v5}, Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$AuditEvent;->getGroup()I

    move-result v5

    iget-object v6, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$5;->val$event:Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$AuditEvent;

    invoke-virtual {v6}, Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$AuditEvent;->getOutcome()Z

    move-result v6

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v7

    iget-object v8, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$5;->val$componentName:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v10, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$5;->val$event:Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$AuditEvent;

    invoke-virtual {v10}, Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$AuditEvent;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, " "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$5;->val$url:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, " "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$5;->val$event:Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$AuditEvent;

    invoke-virtual {v10}, Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$AuditEvent;->getOutcomeAsString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v9, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$5;->val$message:Ljava/lang/String;

    invoke-virtual {v9}, Ljava/lang/String;->isEmpty()Z

    move-result v9

    if-eqz v9, :cond_0

    const-string/jumbo v9, ""

    :goto_0
    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static/range {v0 .. v9}, Lcom/google/android/apps/chrome/knoxsettings/KnoxSettingsProvider;->saveToAuditLog(Landroid/content/Context;Ljava/lang/String;JIIZILjava/lang/String;Ljava/lang/String;)V

    .line 140
    const/4 v0, 0x0

    return-object v0

    .line 133
    :cond_0
    new-instance v9, Ljava/lang/StringBuilder;

    const-string/jumbo v11, " "

    invoke-direct {v9, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v11, p0, Lcom/google/android/apps/chrome/knoxsettings/KnoxAuditLogger$5;->val$message:Ljava/lang/String;

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    goto :goto_0
.end method
