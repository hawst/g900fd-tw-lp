.class Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter$1;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source "GestureEventFilter.java"


# instance fields
.field private mOnScrollBeginX:F

.field private mOnScrollBeginY:F

.field final synthetic this$0:Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;)V
    .locals 0

    .prologue
    .line 91
    iput-object p1, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter$1;->this$0:Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    .line 145
    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter$1;->this$0:Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->mSeenFirstScrollEvent:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->access$202(Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;Z)Z

    .line 146
    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter$1;->this$0:Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;

    # getter for: Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->mHandler:Lcom/google/android/apps/chrome/eventfilter/GestureHandler;
    invoke-static {v0}, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->access$400(Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;)Lcom/google/android/apps/chrome/eventfilter/GestureHandler;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter$1;->this$0:Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;

    # getter for: Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->mSingleInput:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->access$500(Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 147
    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter$1;->this$0:Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;

    # getter for: Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->mHandler:Lcom/google/android/apps/chrome/eventfilter/GestureHandler;
    invoke-static {v0}, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->access$400(Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;)Lcom/google/android/apps/chrome/eventfilter/GestureHandler;

    move-result-object v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter$1;->this$0:Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;

    iget v2, v2, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->mPxToDp:F

    mul-float/2addr v1, v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter$1;->this$0:Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;

    iget v3, v3, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->mPxToDp:F

    mul-float/2addr v2, v3

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/chrome/eventfilter/GestureHandler;->onDown(FF)V

    .line 149
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 5

    .prologue
    .line 136
    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter$1;->this$0:Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;

    # getter for: Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->mHandler:Lcom/google/android/apps/chrome/eventfilter/GestureHandler;
    invoke-static {v0}, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->access$400(Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;)Lcom/google/android/apps/chrome/eventfilter/GestureHandler;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter$1;->this$0:Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;

    # getter for: Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->mSingleInput:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->access$500(Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 137
    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter$1;->this$0:Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;

    # getter for: Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->mHandler:Lcom/google/android/apps/chrome/eventfilter/GestureHandler;
    invoke-static {v0}, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->access$400(Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;)Lcom/google/android/apps/chrome/eventfilter/GestureHandler;

    move-result-object v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter$1;->this$0:Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;

    iget v2, v2, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->mPxToDp:F

    mul-float/2addr v1, v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter$1;->this$0:Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;

    iget v3, v3, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->mPxToDp:F

    mul-float/2addr v2, v3

    iget-object v3, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter$1;->this$0:Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;

    iget v3, v3, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->mPxToDp:F

    mul-float/2addr v3, p3

    iget-object v4, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter$1;->this$0:Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;

    iget v4, v4, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->mPxToDp:F

    mul-float/2addr v4, p4

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/google/android/apps/chrome/eventfilter/GestureHandler;->fling(FFFF)V

    .line 140
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter$1;->this$0:Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;

    # invokes: Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->longPress(Landroid/view/MotionEvent;)V
    invoke-static {v0, p1}, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->access$100(Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;Landroid/view/MotionEvent;)V

    .line 155
    return-void
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    .line 99
    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter$1;->this$0:Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;

    # getter for: Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->mSeenFirstScrollEvent:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->access$200(Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 102
    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter$1;->this$0:Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;

    # setter for: Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->mSeenFirstScrollEvent:Z
    invoke-static {v0, v8}, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->access$202(Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;Z)Z

    .line 103
    mul-float v0, p3, p3

    mul-float v1, p4, p4

    add-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v0, v0

    .line 105
    cmpl-float v1, v0, v2

    if-lez v1, :cond_0

    .line 106
    iget-object v1, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter$1;->this$0:Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;

    # getter for: Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->mScaledTouchSlop:I
    invoke-static {v1}, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->access$300(Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;)I

    move-result v1

    int-to-float v1, v1

    sub-float v1, v0, v1

    invoke-static {v2, v1}, Ljava/lang/Math;->max(FF)F

    move-result v1

    div-float v0, v1, v0

    .line 107
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    sub-float v2, v3, v0

    mul-float/2addr v2, p3

    add-float/2addr v1, v2

    iput v1, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter$1;->mOnScrollBeginX:F

    .line 108
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    sub-float v2, v3, v0

    mul-float/2addr v2, p4

    add-float/2addr v1, v2

    iput v1, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter$1;->mOnScrollBeginY:F

    .line 109
    mul-float/2addr p3, v0

    .line 110
    mul-float/2addr p4, v0

    .line 113
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter$1;->this$0:Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;

    # getter for: Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->mHandler:Lcom/google/android/apps/chrome/eventfilter/GestureHandler;
    invoke-static {v0}, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->access$400(Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;)Lcom/google/android/apps/chrome/eventfilter/GestureHandler;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter$1;->this$0:Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;

    # getter for: Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->mSingleInput:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->access$500(Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 116
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iget v1, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter$1;->mOnScrollBeginX:F

    sub-float v5, v0, v1

    .line 117
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iget v1, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter$1;->mOnScrollBeginY:F

    sub-float v6, v0, v1

    .line 118
    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter$1;->this$0:Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;

    # getter for: Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->mHandler:Lcom/google/android/apps/chrome/eventfilter/GestureHandler;
    invoke-static {v0}, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->access$400(Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;)Lcom/google/android/apps/chrome/eventfilter/GestureHandler;

    move-result-object v0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter$1;->this$0:Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;

    iget v2, v2, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->mPxToDp:F

    mul-float/2addr v1, v2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter$1;->this$0:Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;

    iget v3, v3, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->mPxToDp:F

    mul-float/2addr v2, v3

    neg-float v3, p3

    iget-object v4, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter$1;->this$0:Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;

    iget v4, v4, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->mPxToDp:F

    mul-float/2addr v3, v4

    neg-float v4, p4

    iget-object v7, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter$1;->this$0:Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;

    iget v7, v7, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->mPxToDp:F

    mul-float/2addr v4, v7

    iget-object v7, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter$1;->this$0:Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;

    iget v7, v7, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->mPxToDp:F

    mul-float/2addr v5, v7

    iget-object v7, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter$1;->this$0:Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;

    iget v7, v7, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->mPxToDp:F

    mul-float/2addr v6, v7

    invoke-interface/range {v0 .. v6}, Lcom/google/android/apps/chrome/eventfilter/GestureHandler;->drag(FFFFFF)V

    .line 122
    :cond_1
    return v8
.end method

.method public onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    .line 127
    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter$1;->this$0:Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;

    # getter for: Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->mHandler:Lcom/google/android/apps/chrome/eventfilter/GestureHandler;
    invoke-static {v0}, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->access$400(Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;)Lcom/google/android/apps/chrome/eventfilter/GestureHandler;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter$1;->this$0:Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;

    # getter for: Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->mSingleInput:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->access$500(Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 128
    iget-object v0, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter$1;->this$0:Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;

    # getter for: Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->mHandler:Lcom/google/android/apps/chrome/eventfilter/GestureHandler;
    invoke-static {v0}, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->access$400(Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;)Lcom/google/android/apps/chrome/eventfilter/GestureHandler;

    move-result-object v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter$1;->this$0:Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;

    iget v2, v2, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->mPxToDp:F

    mul-float/2addr v1, v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter$1;->this$0:Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;

    iget v3, v3, Lcom/google/android/apps/chrome/eventfilter/GestureEventFilter;->mPxToDp:F

    mul-float/2addr v2, v3

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/chrome/eventfilter/GestureHandler;->click(FF)V

    .line 130
    :cond_0
    const/4 v0, 0x1

    return v0
.end method
