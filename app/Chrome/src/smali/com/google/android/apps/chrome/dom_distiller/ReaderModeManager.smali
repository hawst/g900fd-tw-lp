.class public Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;
.super Lorg/chromium/chrome/browser/EmptyTabObserver;
.source "ReaderModeManager.java"


# static fields
.field public static final NOT_POSSIBLE:I = 0x1

.field public static final POSSIBLE:I = 0x0

.field public static final READER_MODE_STATUS_IDENTIFIER:Ljava/lang/String; = "readerModeStatus"

.field public static final STARTED:I = 0x2

.field private static final sIsReadableJs:Ljava/lang/String;


# instance fields
.field private final mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

.field private mIsUmaRecorded:Z

.field private mReaderModePageUrl:Ljava/lang/String;

.field private mReaderModeStatus:I

.field private final mTab:Lcom/google/android/apps/chrome/tab/ChromeTab;

.field private mWebContentsObserver:Lorg/chromium/content/browser/WebContentsObserverAndroid;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 50
    invoke-static {}, Lorg/chromium/components/dom_distiller/core/DomDistillerUrlUtils;->getIsDistillableJs()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;->sIsReadableJs:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/chrome/tab/ChromeTab;Lcom/google/android/apps/chrome/ChromeActivity;)V
    .locals 1

    .prologue
    .line 73
    invoke-direct {p0}, Lorg/chromium/chrome/browser/EmptyTabObserver;-><init>()V

    .line 60
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;->mReaderModeStatus:I

    .line 74
    iput-object p2, p0, Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    .line 75
    iput-object p1, p0, Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;->mTab:Lcom/google/android/apps/chrome/tab/ChromeTab;

    .line 76
    iget-object v0, p0, Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;->mTab:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->addObserver(Lorg/chromium/chrome/browser/TabObserver;)V

    .line 77
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;)Lcom/google/android/apps/chrome/tab/ChromeTab;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;->mTab:Lcom/google/android/apps/chrome/tab/ChromeTab;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;Z)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;->updateStatusBasedOnReaderModeCriteria(Z)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;)I
    .locals 1

    .prologue
    .line 28
    iget v0, p0, Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;->mReaderModeStatus:I

    return v0
.end method

.method static synthetic access$202(Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;I)I
    .locals 0

    .prologue
    .line 28
    iput p1, p0, Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;->mReaderModeStatus:I

    return p1
.end method

.method static synthetic access$300(Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;->sendReaderModeStatusChangedNotification()V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;->mReaderModePageUrl:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$402(Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 28
    iput-object p1, p0, Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;->mReaderModePageUrl:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$500(Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;)Z
    .locals 1

    .prologue
    .line 28
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;->mIsUmaRecorded:Z

    return v0
.end method

.method static synthetic access$502(Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;Z)Z
    .locals 0

    .prologue
    .line 28
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;->mIsUmaRecorded:Z

    return p1
.end method

.method private createWebContentsObserverAndroid(Lorg/chromium/content_public/browser/WebContents;)Lorg/chromium/content/browser/WebContentsObserverAndroid;
    .locals 1

    .prologue
    .line 97
    new-instance v0, Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager$1;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager$1;-><init>(Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;Lorg/chromium/content_public/browser/WebContents;)V

    return-object v0
.end method

.method private sendReaderModeStatusChangedNotification()V
    .locals 3

    .prologue
    .line 163
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 164
    const-string/jumbo v1, "readerModeStatus"

    iget v2, p0, Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;->mReaderModeStatus:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 165
    const-string/jumbo v1, "tabId"

    iget-object v2, p0, Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;->mTab:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getId()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 166
    iget-object v1, p0, Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;->mActivity:Lcom/google/android/apps/chrome/ChromeActivity;

    const/16 v2, 0x4f

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter;->broadcastNotification(Landroid/content/Context;ILandroid/os/Bundle;)V

    .line 168
    return-void
.end method

.method private updateStatusBasedOnReaderModeCriteria(Z)V
    .locals 3

    .prologue
    .line 141
    iget-object v0, p0, Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;->mTab:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    if-nez v0, :cond_0

    .line 160
    :goto_0
    return-void

    .line 143
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;->mTab:Lcom/google/android/apps/chrome/tab/ChromeTab;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/tab/ChromeTab;->getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;->sIsReadableJs:Ljava/lang/String;

    new-instance v2, Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager$2;

    invoke-direct {v2, p0, p1}, Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager$2;-><init>(Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;Z)V

    invoke-virtual {v0, v1, v2}, Lorg/chromium/content/browser/ContentViewCore;->evaluateJavaScript(Ljava/lang/String;Lorg/chromium/content_public/browser/JavaScriptCallback;)V

    goto :goto_0
.end method


# virtual methods
.method public getReaderModeStatus()I
    .locals 1

    .prologue
    .line 174
    iget v0, p0, Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;->mReaderModeStatus:I

    return v0
.end method

.method public onContentChanged(Lorg/chromium/chrome/browser/Tab;)V
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;->mWebContentsObserver:Lorg/chromium/content/browser/WebContentsObserverAndroid;

    if-eqz v0, :cond_0

    .line 87
    iget-object v0, p0, Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;->mWebContentsObserver:Lorg/chromium/content/browser/WebContentsObserverAndroid;

    invoke-virtual {v0}, Lorg/chromium/content/browser/WebContentsObserverAndroid;->detachFromWebContents()V

    .line 88
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;->mWebContentsObserver:Lorg/chromium/content/browser/WebContentsObserverAndroid;

    .line 90
    :cond_0
    invoke-virtual {p1}, Lorg/chromium/chrome/browser/Tab;->getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lorg/chromium/chrome/browser/Tab;->getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->getWebContents()Lorg/chromium/content_public/browser/WebContents;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 91
    invoke-virtual {p1}, Lorg/chromium/chrome/browser/Tab;->getContentViewCore()Lorg/chromium/content/browser/ContentViewCore;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/content/browser/ContentViewCore;->getWebContents()Lorg/chromium/content_public/browser/WebContents;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;->createWebContentsObserverAndroid(Lorg/chromium/content_public/browser/WebContents;)Lorg/chromium/content/browser/WebContentsObserverAndroid;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;->mWebContentsObserver:Lorg/chromium/content/browser/WebContentsObserverAndroid;

    .line 94
    :cond_1
    return-void
.end method

.method public onDestroyed(Lorg/chromium/chrome/browser/Tab;)V
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;->mWebContentsObserver:Lorg/chromium/content/browser/WebContentsObserverAndroid;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/dom_distiller/ReaderModeManager;->mWebContentsObserver:Lorg/chromium/content/browser/WebContentsObserverAndroid;

    invoke-virtual {v0}, Lorg/chromium/content/browser/WebContentsObserverAndroid;->detachFromWebContents()V

    .line 82
    :cond_0
    return-void
.end method
