.class public Lcom/google/android/apps/chrome/document/DocumentMigrationHelper;
.super Ljava/lang/Object;
.source "DocumentMigrationHelper.java"


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final FINALIZE_MODE_FINISH_ACTIVITY:I = 0x1

.field public static final FINALIZE_MODE_NO_ACTION:I = 0x0

.field public static final FINALIZE_MODE_RESTART_APP:I = 0x2

.field private static final ICON_TYPES:[I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 59
    const-class v0, Lcom/google/android/apps/chrome/document/DocumentMigrationHelper;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/document/DocumentMigrationHelper;->$assertionsDisabled:Z

    .line 62
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/apps/chrome/document/DocumentMigrationHelper;->ICON_TYPES:[I

    return-void

    .line 59
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 62
    :array_0
    .array-data 4
        0x1
        0x6
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    return-void
.end method

.method static synthetic access$000(Landroid/app/Activity;Lcom/google/android/apps/chrome/document/DocumentMigrationHelper$MigrationTabList;I)V
    .locals 0

    .prologue
    .line 60
    invoke-static {p0, p1, p2}, Lcom/google/android/apps/chrome/document/DocumentMigrationHelper;->addAppTasksFromFiles(Landroid/app/Activity;Lcom/google/android/apps/chrome/document/DocumentMigrationHelper$MigrationTabList;I)V

    return-void
.end method

.method static synthetic access$100(Landroid/app/Activity;ILcom/google/android/apps/chrome/tab/ChromeTab$TabState;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)V
    .locals 0

    .prologue
    .line 60
    invoke-static/range {p0 .. p6}, Lcom/google/android/apps/chrome/document/DocumentMigrationHelper;->addAppTask(Landroid/app/Activity;ILcom/google/android/apps/chrome/tab/ChromeTab$TabState;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)V

    return-void
.end method

.method static synthetic access$200(Landroid/app/Activity;I)V
    .locals 0

    .prologue
    .line 60
    invoke-static {p0, p1}, Lcom/google/android/apps/chrome/document/DocumentMigrationHelper;->finalizeMigration(Landroid/app/Activity;I)V

    return-void
.end method

.method private static addAppTask(Landroid/app/Activity;ILcom/google/android/apps/chrome/tab/ChromeTab$TabState;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x0

    .line 387
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/chrome/IntentHandler;->getTabIdFromIntent(Landroid/content/Intent;)I

    move-result v0

    if-ne p1, v0, :cond_0

    .line 413
    :goto_0
    return-void

    .line 389
    :cond_0
    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v0, "android.intent.action.VIEW"

    invoke-static {p1, p3}, Lcom/google/android/apps/chrome/document/DocumentActivityDatabase;->createDocumentDataString(ILjava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 391
    invoke-static {v8}, Lcom/google/android/apps/chrome/document/ChromeLauncherActivity;->getDocumentClassName(Z)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, p0, v0}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    .line 392
    const-string/jumbo v0, "com.android.chrome.preserve_task"

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 393
    const-string/jumbo v0, "activity"

    invoke-virtual {p0, v0}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 396
    invoke-virtual {v0}, Landroid/app/ActivityManager;->getAppTaskThumbnailSize()Landroid/util/Size;

    move-result-object v2

    invoke-virtual {v2}, Landroid/util/Size;->getWidth()I

    move-result v2

    invoke-virtual {v0}, Landroid/app/ActivityManager;->getAppTaskThumbnailSize()Landroid/util/Size;

    move-result-object v3

    invoke-virtual {v3}, Landroid/util/Size;->getHeight()I

    move-result v3

    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v2, v3, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 398
    new-instance v3, Landroid/graphics/Canvas;

    invoke-direct {v3, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 399
    if-nez p6, :cond_1

    .line 400
    const/4 v4, -0x1

    invoke-virtual {v3, v4}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 408
    :goto_1
    new-instance v3, Landroid/app/ActivityManager$TaskDescription;

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/google/android/apps/chrome/R$color;->default_primary_color:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-direct {v3, p4, p5, v4}, Landroid/app/ActivityManager$TaskDescription;-><init>(Ljava/lang/String;Landroid/graphics/Bitmap;I)V

    .line 410
    invoke-virtual {v0, p0, v1, v3, v2}, Landroid/app/ActivityManager;->addAppTask(Landroid/app/Activity;Landroid/content/Intent;Landroid/app/ActivityManager$TaskDescription;Landroid/graphics/Bitmap;)I

    .line 411
    new-instance v0, Lcom/google/android/apps/chrome/document/DocumentTabList$Entry;

    invoke-direct {v0, p1, p2}, Lcom/google/android/apps/chrome/document/DocumentTabList$Entry;-><init>(ILcom/google/android/apps/chrome/tab/ChromeTab$TabState;)V

    .line 412
    invoke-static {v8}, Lcom/google/android/apps/chrome/document/DocumentActivityDatabase;->getTabList(Z)Lcom/google/android/apps/chrome/document/DocumentTabList;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/document/DocumentTabList;->addEntryForMigration(Lcom/google/android/apps/chrome/document/DocumentTabList$Entry;)V

    goto :goto_0

    .line 402
    :cond_1
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {p6}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    int-to-float v5, v5

    div-float/2addr v4, v5

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    int-to-float v5, v5

    invoke-virtual {p6}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    int-to-float v6, v6

    div-float/2addr v5, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->max(FF)F

    move-result v4

    .line 405
    invoke-virtual {v3, v4, v4}, Landroid/graphics/Canvas;->scale(FF)V

    .line 406
    const/4 v4, 0x0

    invoke-virtual {v3, p6, v7, v7, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_1
.end method

.method private static addAppTasksFromFiles(Landroid/app/Activity;Lcom/google/android/apps/chrome/document/DocumentMigrationHelper$MigrationTabList;I)V
    .locals 15

    .prologue
    .line 326
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/chrome/document/DocumentMigrationHelper$MigrationTabList;->getCount()I

    move-result v1

    if-nez v1, :cond_1

    .line 327
    move/from16 v0, p2

    invoke-static {p0, v0}, Lcom/google/android/apps/chrome/document/DocumentMigrationHelper;->finalizeMigration(Landroid/app/Activity;I)V

    .line 383
    :cond_0
    return-void

    .line 330
    :cond_1
    new-instance v9, Lcom/google/android/apps/chrome/compositor/TabContentManager;

    new-instance v1, Lcom/google/android/apps/chrome/document/DocumentMigrationHelper$4;

    invoke-direct {v1}, Lcom/google/android/apps/chrome/document/DocumentMigrationHelper$4;-><init>()V

    invoke-direct {v9, p0, v1}, Lcom/google/android/apps/chrome/compositor/TabContentManager;-><init>(Landroid/content/Context;Lcom/google/android/apps/chrome/compositor/ContentOffsetProvider;)V

    .line 337
    new-instance v11, Lorg/chromium/chrome/browser/favicon/FaviconHelper;

    invoke-direct {v11}, Lorg/chromium/chrome/browser/favicon/FaviconHelper;-><init>()V

    .line 338
    const/4 v1, 0x0

    move v10, v1

    :goto_0
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/chrome/document/DocumentMigrationHelper$MigrationTabList;->getCount()I

    move-result v1

    if-ge v10, v1, :cond_0

    .line 339
    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Lcom/google/android/apps/chrome/document/DocumentMigrationHelper$MigrationTabList;->getTabIdAtIndex(I)I

    move-result v4

    .line 340
    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lcom/google/android/apps/chrome/document/DocumentMigrationHelper$MigrationTabList;->getCurrentUrlForDocument(I)Ljava/lang/String;

    move-result-object v2

    .line 341
    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lcom/google/android/apps/chrome/document/DocumentMigrationHelper$MigrationTabList;->getTitleForDocument(I)Ljava/lang/String;

    move-result-object v6

    .line 342
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/chrome/document/DocumentMigrationHelper$MigrationTabList;->getCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ne v10, v1, :cond_4

    const/4 v7, 0x1

    .line 345
    :goto_1
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string/jumbo v2, "chrome-native://newtab/"

    .line 346
    :cond_2
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x0

    invoke-static {v2, v1}, Lcom/google/android/apps/chrome/tab/NativePageFactory;->isNativePageUrl(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_3

    .line 348
    const/4 v1, 0x0

    invoke-static {v2, v1}, Lorg/chromium/chrome/browser/UrlUtilities;->getDomainAndRegistry(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v6

    .line 353
    :cond_3
    invoke-static {}, Lorg/chromium/chrome/browser/profiles/Profile;->getLastUsedProfile()Lorg/chromium/chrome/browser/profiles/Profile;

    move-result-object v1

    invoke-virtual {v1}, Lorg/chromium/chrome/browser/profiles/Profile;->getOriginalProfile()Lorg/chromium/chrome/browser/profiles/Profile;

    move-result-object v12

    sget-object v13, Lcom/google/android/apps/chrome/document/DocumentMigrationHelper;->ICON_TYPES:[I

    const/16 v14, 0x20

    new-instance v1, Lcom/google/android/apps/chrome/document/DocumentMigrationHelper$5;

    move-object v3, p0

    move-object/from16 v5, p1

    move/from16 v8, p2

    invoke-direct/range {v1 .. v9}, Lcom/google/android/apps/chrome/document/DocumentMigrationHelper$5;-><init>(Ljava/lang/String;Landroid/app/Activity;ILcom/google/android/apps/chrome/document/DocumentMigrationHelper$MigrationTabList;Ljava/lang/String;ZILcom/google/android/apps/chrome/compositor/TabContentManager;)V

    move-object v3, v11

    move-object v4, v12

    move-object v5, v2

    move-object v6, v13

    move v7, v14

    move-object v8, v1

    invoke-virtual/range {v3 .. v8}, Lorg/chromium/chrome/browser/favicon/FaviconHelper;->getLargestRawFaviconForUrl(Lorg/chromium/chrome/browser/profiles/Profile;Ljava/lang/String;[IILorg/chromium/chrome/browser/favicon/FaviconHelper$FaviconImageCallback;)V

    .line 338
    add-int/lit8 v1, v10, 0x1

    move v10, v1

    goto :goto_0

    .line 342
    :cond_4
    const/4 v7, 0x0

    goto :goto_1
.end method

.method private static createDelegateWithTabsToMigrate(Landroid/app/Activity;)Lcom/google/android/apps/chrome/document/DocumentTabList$Delegate;
    .locals 11

    .prologue
    const/4 v2, 0x0

    .line 241
    invoke-static {}, Lcom/google/android/apps/chrome/document/DocumentTabList;->getStateDirectory()Ljava/io/File;

    move-result-object v4

    .line 242
    invoke-virtual {v4}, Ljava/io/File;->mkdir()Z

    .line 244
    new-instance v5, Landroid/util/SparseArray;

    invoke-direct {v5}, Landroid/util/SparseArray;-><init>()V

    .line 246
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 247
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    .line 250
    invoke-static {p0, v2}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->getStateFolder(Landroid/content/Context;I)Ljava/io/File;

    move-result-object v0

    move v1, v2

    .line 251
    :goto_0
    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v3

    if-eqz v3, :cond_3

    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v3

    array-length v3, v3

    if-eqz v3, :cond_3

    .line 252
    invoke-static {p0, v1}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->getStateFolder(Landroid/content/Context;I)Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v7

    .line 255
    :try_start_0
    new-instance v3, Lcom/google/android/apps/chrome/document/DocumentMigrationHelper$2;

    invoke-direct {v3, v6}, Lcom/google/android/apps/chrome/document/DocumentMigrationHelper$2;-><init>(Landroid/content/SharedPreferences$Editor;)V

    invoke-static {v0, v3}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->readSavedStateFile(Ljava/io/File;Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore$OnTabStateReadCallback;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    move v3, v2

    .line 273
    :goto_2
    array-length v0, v7

    if-ge v3, v0, :cond_2

    .line 275
    aget-object v0, v7, v3

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v8

    .line 276
    invoke-static {v8}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->parseStateDataFromFilename(Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v9

    .line 278
    if-eqz v9, :cond_0

    .line 280
    iget-object v0, v9, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 281
    aget-object v0, v7, v3

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 273
    :cond_0
    :goto_3
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_2

    .line 270
    :catch_0
    move-exception v0

    const-string/jumbo v0, "DocumentMigrationHelper"

    const-string/jumbo v3, "IO Exception while trying to get the last used tab id"

    invoke-static {v0, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 283
    :cond_1
    aget-object v0, v7, v3

    new-instance v10, Ljava/io/File;

    invoke-direct {v10, v4, v8}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v0, v10}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    .line 284
    iget-object v0, v9, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v8

    new-instance v10, Lcom/google/android/apps/chrome/document/DocumentTabList$Entry;

    iget-object v0, v9, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const-string/jumbo v9, "chrome-native://newtab/"

    invoke-direct {v10, v0, v9}, Lcom/google/android/apps/chrome/document/DocumentTabList$Entry;-><init>(ILjava/lang/String;)V

    invoke-virtual {v5, v8, v10}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_3

    .line 288
    :cond_2
    add-int/lit8 v1, v1, 0x1

    .line 289
    invoke-static {p0, v1}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->getStateFolder(Landroid/content/Context;I)Ljava/io/File;

    move-result-object v0

    goto :goto_0

    .line 292
    :cond_3
    new-instance v0, Lcom/google/android/apps/chrome/document/DocumentMigrationHelper$3;

    invoke-direct {v0, v5}, Lcom/google/android/apps/chrome/document/DocumentMigrationHelper$3;-><init>(Landroid/util/SparseArray;)V

    return-object v0
.end method

.method private static finalizeMigration(Landroid/app/Activity;I)V
    .locals 1

    .prologue
    .line 226
    packed-switch p1, :pswitch_data_0

    .line 236
    sget-boolean v0, Lcom/google/android/apps/chrome/document/DocumentMigrationHelper;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 230
    :pswitch_0
    invoke-virtual {p0}, Landroid/app/Activity;->finishAndRemoveTask()V

    .line 238
    :cond_0
    :goto_0
    :pswitch_1
    return-void

    .line 233
    :pswitch_2
    const/4 v0, 0x1

    invoke-static {v0}, Lorg/chromium/chrome/browser/ApplicationLifetime;->terminate(Z)V

    goto :goto_0

    .line 226
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public static migrateTabsFromClassicToDocument(Landroid/app/Activity;I)V
    .locals 2

    .prologue
    .line 180
    invoke-static {p0}, Lcom/google/android/apps/chrome/document/DocumentMigrationHelper;->createDelegateWithTabsToMigrate(Landroid/app/Activity;)Lcom/google/android/apps/chrome/document/DocumentTabList$Delegate;

    move-result-object v0

    .line 181
    new-instance v1, Lcom/google/android/apps/chrome/document/DocumentMigrationHelper$MigrationTabList;

    invoke-direct {v1, v0}, Lcom/google/android/apps/chrome/document/DocumentMigrationHelper$MigrationTabList;-><init>(Lcom/google/android/apps/chrome/document/DocumentTabList$Delegate;)V

    .line 183
    new-instance v0, Lcom/google/android/apps/chrome/document/DocumentMigrationHelper$1;

    invoke-direct {v0, v1, p0, v1, p1}, Lcom/google/android/apps/chrome/document/DocumentMigrationHelper$1;-><init>(Lcom/google/android/apps/chrome/document/DocumentTabList;Landroid/app/Activity;Lcom/google/android/apps/chrome/document/DocumentMigrationHelper$MigrationTabList;I)V

    .line 200
    invoke-virtual {v0}, Lcom/google/android/apps/chrome/document/DocumentTabList$InitializationObserver;->runWhenReady()V

    .line 201
    return-void
.end method

.method public static migrateTabsFromDocumentToClassic(Landroid/app/Activity;I)V
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 112
    invoke-static {}, Lorg/chromium/base/ApplicationStatus;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    .line 115
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 117
    invoke-static {v1}, Lcom/google/android/apps/chrome/document/DocumentActivityDatabase;->getTabList(Z)Lcom/google/android/apps/chrome/document/DocumentTabList;

    move-result-object v5

    .line 118
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/google/android/apps/chrome/document/DocumentActivityDatabase;->getTabList(Z)Lcom/google/android/apps/chrome/document/DocumentTabList;

    move-result-object v6

    move v0, v1

    .line 119
    :goto_0
    invoke-virtual {v6}, Lcom/google/android/apps/chrome/document/DocumentTabList;->getCount()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 120
    invoke-virtual {v6, v0}, Lcom/google/android/apps/chrome/document/DocumentTabList;->getTabIdAtIndex(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 119
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    move v0, v1

    .line 122
    :goto_1
    invoke-virtual {v5}, Lcom/google/android/apps/chrome/document/DocumentTabList;->getCount()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 123
    invoke-virtual {v5, v0}, Lcom/google/android/apps/chrome/document/DocumentTabList;->getTabAt(I)Lorg/chromium/chrome/browser/Tab;

    move-result-object v2

    if-nez v2, :cond_1

    .line 124
    invoke-virtual {v5, v0}, Lcom/google/android/apps/chrome/document/DocumentTabList;->getTabIdAtIndex(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 122
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 127
    :cond_2
    const-string/jumbo v0, "activity"

    invoke-virtual {v3, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 129
    invoke-virtual {v0}, Landroid/app/ActivityManager;->getAppTasks()Ljava/util/List;

    move-result-object v7

    move v2, v1

    .line 130
    :goto_2
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_4

    .line 131
    invoke-interface {v7, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager$AppTask;

    invoke-virtual {v0}, Landroid/app/ActivityManager$AppTask;->getTaskInfo()Landroid/app/ActivityManager$RecentTaskInfo;

    move-result-object v0

    iget-object v0, v0, Landroid/app/ActivityManager$RecentTaskInfo;->baseIntent:Landroid/content/Intent;

    .line 132
    invoke-static {v0}, Lcom/google/android/apps/chrome/IntentHandler;->getTabIdFromIntent(Landroid/content/Intent;)I

    move-result v0

    .line 133
    const/4 v8, -0x1

    if-eq v0, v8, :cond_3

    .line 134
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v7, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager$AppTask;

    invoke-virtual {v0}, Landroid/app/ActivityManager$AppTask;->finishAndRemoveTask()V

    .line 130
    :cond_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 137
    :cond_4
    invoke-static {v3, v1}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->getStateFolder(Landroid/content/Context;I)Ljava/io/File;

    move-result-object v2

    .line 138
    new-instance v0, Ljava/io/File;

    const-string/jumbo v3, "tab_state"

    invoke-direct {v0, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    .line 142
    invoke-static {}, Lcom/google/android/apps/chrome/document/DocumentTabList;->getStateDirectory()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v7

    .line 144
    :goto_3
    :try_start_0
    array-length v0, v7

    if-ge v1, v0, :cond_8

    .line 145
    aget-object v0, v7, v1

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v8

    .line 146
    invoke-static {v8}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->parseStateDataFromFilename(Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v0

    .line 148
    if-eqz v0, :cond_5

    .line 149
    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 152
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 153
    aget-object v0, v7, v1

    new-instance v9, Ljava/io/File;

    invoke-direct {v9, v2, v8}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v0, v9}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    .line 144
    :cond_5
    :goto_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 155
    :cond_6
    aget-object v0, v7, v1

    invoke-virtual {v0}, Ljava/io/File;->delete()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_4

    .line 168
    :catch_0
    move-exception v0

    const-string/jumbo v0, "DocumentMigrationHelper"

    const-string/jumbo v1, "IO exception during tab migration, tab state might not restore correctly"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 170
    :cond_7
    :goto_5
    invoke-static {p0, p1}, Lcom/google/android/apps/chrome/document/DocumentMigrationHelper;->finalizeMigration(Landroid/app/Activity;I)V

    .line 171
    return-void

    .line 159
    :cond_8
    :try_start_1
    invoke-virtual {v5}, Lcom/google/android/apps/chrome/document/DocumentTabList;->updateRecentlyClosed()V

    .line 160
    invoke-virtual {v6}, Lcom/google/android/apps/chrome/document/DocumentTabList;->updateRecentlyClosed()V

    .line 162
    invoke-virtual {v5}, Lcom/google/android/apps/chrome/document/DocumentTabList;->getCount()I

    move-result v0

    if-eqz v0, :cond_7

    .line 164
    invoke-static {v6, v5}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->serializeTabLists(Lorg/chromium/chrome/browser/tabmodel/TabList;Lorg/chromium/chrome/browser/tabmodel/TabList;)[B

    move-result-object v0

    .line 165
    invoke-static {v3, v0}, Lorg/chromium/base/ImportantFileWriterAndroid;->writeFileAtomically(Ljava/lang/String;[B)Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_5
.end method

.method public static migrateTabsToDocumentForUpgrade(Landroid/app/Activity;I)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 213
    invoke-static {p0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->setAttemptedMigrationOnUpgrade()V

    .line 214
    invoke-static {p0, v1}, Lcom/google/android/apps/chrome/tabmodel/TabPersistentStore;->getStateFolder(Landroid/content/Context;I)Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    .line 215
    if-eqz v2, :cond_0

    array-length v3, v2

    if-eqz v3, :cond_0

    array-length v3, v2

    if-ne v3, v0, :cond_1

    aget-object v2, v2, v1

    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "tab_state"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    .line 222
    :goto_0
    return v0

    .line 221
    :cond_1
    invoke-static {p0, p1}, Lcom/google/android/apps/chrome/document/DocumentMigrationHelper;->migrateTabsFromClassicToDocument(Landroid/app/Activity;I)V

    goto :goto_0
.end method
