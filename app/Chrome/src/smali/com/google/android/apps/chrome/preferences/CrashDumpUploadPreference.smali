.class public Lcom/google/android/apps/chrome/preferences/CrashDumpUploadPreference;
.super Lcom/google/android/apps/chrome/preferences/ChromeBaseListPreference;
.source "CrashDumpUploadPreference.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/chrome/preferences/ChromeBaseListPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 23
    iput-object p1, p0, Lcom/google/android/apps/chrome/preferences/CrashDumpUploadPreference;->mContext:Landroid/content/Context;

    .line 24
    invoke-virtual {p0, p0}, Lcom/google/android/apps/chrome/preferences/CrashDumpUploadPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 25
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/CrashDumpUploadPreference;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getPrefCrashDumpUploadPreference()Ljava/lang/String;

    move-result-object v0

    .line 27
    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/CrashDumpUploadPreference;->getSummaryText(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/CrashDumpUploadPreference;->setSummary(I)V

    .line 28
    return-void
.end method


# virtual methods
.method public getSummaryText(Ljava/lang/String;)I
    .locals 2

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/CrashDumpUploadPreference;->mContext:Landroid/content/Context;

    sget v1, Lcom/google/android/apps/chrome/R$string;->crash_dump_always_upload_value:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 44
    sget v0, Lcom/google/android/apps/chrome/R$string;->crash_dump_always_upload:I

    .line 49
    :goto_0
    return v0

    .line 45
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/preferences/CrashDumpUploadPreference;->mContext:Landroid/content/Context;

    sget v1, Lcom/google/android/apps/chrome/R$string;->crash_dump_only_with_wifi_value:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 47
    sget v0, Lcom/google/android/apps/chrome/R$string;->crash_dump_only_with_wifi:I

    goto :goto_0

    .line 49
    :cond_1
    sget v0, Lcom/google/android/apps/chrome/R$string;->crash_dump_never_upload:I

    goto :goto_0
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 32
    check-cast p2, Ljava/lang/String;

    invoke-virtual {p0, p2}, Lcom/google/android/apps/chrome/preferences/CrashDumpUploadPreference;->getSummaryText(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/preferences/CrashDumpUploadPreference;->setSummary(I)V

    .line 33
    const/4 v0, 0x1

    return v0
.end method
