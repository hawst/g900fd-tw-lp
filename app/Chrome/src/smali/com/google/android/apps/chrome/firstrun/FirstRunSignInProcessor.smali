.class public Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;
.super Ljava/lang/Object;
.source "FirstRunSignInProcessor.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final mActivity:Landroid/app/Activity;

.field private mHasChildAccount:Z

.field private mIsAndroidEduDevice:Z

.field private final mObserver:Lorg/chromium/chrome/browser/signin/SigninManager$SignInFlowObserver;

.field private final mPrefManager:Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

.field private final mShowSignInNotification:Z

.field private final mSignInManager:Lorg/chromium/chrome/browser/signin/SigninManager;

.field private mSignInType:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 49
    const-class v0, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Landroid/app/Activity;ZLorg/chromium/chrome/browser/signin/SigninManager$SignInFlowObserver;)V
    .locals 2

    .prologue
    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    iput-object p1, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;->mActivity:Landroid/app/Activity;

    .line 79
    invoke-static {p1}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;->mPrefManager:Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    .line 80
    invoke-static {p1}, Lorg/chromium/chrome/browser/signin/SigninManager;->get(Landroid/content/Context;)Lorg/chromium/chrome/browser/signin/SigninManager;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;->mSignInManager:Lorg/chromium/chrome/browser/signin/SigninManager;

    .line 81
    iput-object p3, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;->mObserver:Lorg/chromium/chrome/browser/signin/SigninManager$SignInFlowObserver;

    .line 82
    iput-boolean p2, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;->mShowSignInNotification:Z

    .line 84
    new-instance v0, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor$1;-><init>(Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;)V

    iget-object v1, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor$1;->start(Landroid/app/Activity;)V

    .line 125
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;)Z
    .locals 1

    .prologue
    .line 49
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;->mIsAndroidEduDevice:Z

    return v0
.end method

.method static synthetic access$002(Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;Z)Z
    .locals 0

    .prologue
    .line 49
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;->mIsAndroidEduDevice:Z

    return p1
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;)Z
    .locals 1

    .prologue
    .line 49
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;->mHasChildAccount:Z

    return v0
.end method

.method static synthetic access$1000(Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;->openSyncSettings(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$102(Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;Z)Z
    .locals 0

    .prologue
    .line 49
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;->mHasChildAccount:Z

    return p1
.end method

.method static synthetic access$200(Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;)Lorg/chromium/chrome/browser/signin/SigninManager;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;->mSignInManager:Lorg/chromium/chrome/browser/signin/SigninManager;

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;I)I
    .locals 0

    .prologue
    .line 49
    iput p1, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;->mSignInType:I

    return p1
.end method

.method static synthetic access$400(Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;)Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;->mPrefManager:Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;->mActivity:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;)Lorg/chromium/chrome/browser/signin/SigninManager$SignInFlowObserver;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;->mObserver:Lorg/chromium/chrome/browser/signin/SigninManager$SignInFlowObserver;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;->requestToFireIntentAndFinish()V

    return-void
.end method

.method static synthetic access$800(Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;->completeFreSignInRequest()V

    return-void
.end method

.method static synthetic access$900(Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;->processAutomaticSignIn()V

    return-void
.end method

.method private completeFreSignInRequest()V
    .locals 6

    .prologue
    const/4 v3, 0x1

    .line 153
    sget-boolean v0, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;->mPrefManager:Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getFirstRunFlowSignInComplete()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 155
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;->mPrefManager:Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getFirstRunFlowSignInAccountName()Ljava/lang/String;

    move-result-object v0

    .line 156
    iget-object v1, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;->mActivity:Landroid/app/Activity;

    invoke-static {v1}, Lorg/chromium/chrome/browser/util/FeatureUtilities;->canAllowSync(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lorg/chromium/chrome/browser/signin/SigninManager;->get(Landroid/content/Context;)Lorg/chromium/chrome/browser/signin/SigninManager;

    move-result-object v1

    invoke-virtual {v1}, Lorg/chromium/chrome/browser/signin/SigninManager;->isSignInAllowed()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 159
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;->mPrefManager:Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->setFirstRunFlowSignInComplete(Z)V

    .line 160
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;->mObserver:Lorg/chromium/chrome/browser/signin/SigninManager$SignInFlowObserver;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;->mObserver:Lorg/chromium/chrome/browser/signin/SigninManager$SignInFlowObserver;

    invoke-interface {v0}, Lorg/chromium/chrome/browser/signin/SigninManager$SignInFlowObserver;->onSigninComplete()V

    .line 201
    :cond_2
    :goto_0
    return-void

    .line 164
    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;->mActivity:Landroid/app/Activity;

    invoke-static {v1}, Lorg/chromium/sync/signin/AccountManagerHelper;->get(Landroid/content/Context;)Lorg/chromium/sync/signin/AccountManagerHelper;

    move-result-object v1

    invoke-virtual {v1, v0}, Lorg/chromium/sync/signin/AccountManagerHelper;->getAccountFromName(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v1

    .line 166
    if-nez v1, :cond_4

    .line 168
    invoke-direct {p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;->requestToFireIntentAndFinish()V

    goto :goto_0

    .line 172
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;->mActivity:Landroid/app/Activity;

    iget v2, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;->mSignInType:I

    iget-boolean v4, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;->mShowSignInNotification:Z

    new-instance v5, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor$2;

    invoke-direct {v5, p0}, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor$2;-><init>(Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;)V

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/chrome/firstrun/FirstRunUtil;->signInToSelectedAccount(Landroid/app/Activity;Landroid/accounts/Account;IIZLorg/chromium/chrome/browser/signin/SigninManager$SignInFlowObserver;)V

    goto :goto_0
.end method

.method private openSyncSettings(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 208
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 209
    iget-object v1, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/google/android/apps/chrome/preferences/Preferences;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    .line 210
    const/high16 v1, 0x20020000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 212
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 213
    const-string/jumbo v1, ":android:show_fragment"

    const-class v2, Lcom/google/android/apps/chrome/sync/ui/SyncCustomizationFragment;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 215
    const-string/jumbo v1, ":android:show_fragment_title"

    sget v2, Lcom/google/android/apps/chrome/R$string;->sign_in_sync:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 216
    const-string/jumbo v1, "display_home_as_up"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 217
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 218
    const-string/jumbo v2, "account"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 219
    const-string/jumbo v2, ":android:show_fragment_args"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 221
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 222
    return-void
.end method

.method private processAutomaticSignIn()V
    .locals 6

    .prologue
    const/4 v3, 0x1

    .line 133
    sget-boolean v0, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;->mPrefManager:Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->getFirstRunFlowSignInComplete()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 134
    :cond_0
    sget-boolean v0, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;->mHasChildAccount:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;->mIsAndroidEduDevice:Z

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 135
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;->mIsAndroidEduDevice:Z

    if-nez v0, :cond_3

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;->mHasChildAccount:Z

    if-nez v0, :cond_3

    .line 146
    :cond_2
    :goto_0
    return-void

    .line 137
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;->mActivity:Landroid/app/Activity;

    invoke-static {v0}, Lorg/chromium/sync/signin/AccountManagerHelper;->get(Landroid/content/Context;)Lorg/chromium/sync/signin/AccountManagerHelper;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/sync/signin/AccountManagerHelper;->getGoogleAccounts()[Landroid/accounts/Account;

    move-result-object v1

    .line 139
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;->mActivity:Landroid/app/Activity;

    invoke-static {v0}, Lorg/chromium/chrome/browser/util/FeatureUtilities;->canAllowSync(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/chrome/browser/signin/SigninManager;->get(Landroid/content/Context;)Lorg/chromium/chrome/browser/signin/SigninManager;

    move-result-object v0

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/signin/SigninManager;->isSignInAllowed()Z

    move-result v0

    if-eqz v0, :cond_2

    array-length v0, v1

    if-ne v0, v3, :cond_2

    .line 143
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;->mActivity:Landroid/app/Activity;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    iget v2, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;->mSignInType:I

    iget-boolean v4, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;->mShowSignInNotification:Z

    iget-object v5, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;->mObserver:Lorg/chromium/chrome/browser/signin/SigninManager$SignInFlowObserver;

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/chrome/firstrun/FirstRunUtil;->signInToSelectedAccount(Landroid/app/Activity;Landroid/accounts/Account;IIZLorg/chromium/chrome/browser/signin/SigninManager$SignInFlowObserver;)V

    goto :goto_0
.end method

.method private requestToFireIntentAndFinish()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 228
    const-string/jumbo v0, "FirstRunSignInProcessor"

    const-string/jumbo v1, "Attempt to pass-through without completed FRE"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 229
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;->mObserver:Lorg/chromium/chrome/browser/signin/SigninManager$SignInFlowObserver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;->mObserver:Lorg/chromium/chrome/browser/signin/SigninManager$SignInFlowObserver;

    invoke-interface {v0}, Lorg/chromium/chrome/browser/signin/SigninManager$SignInFlowObserver;->onSigninCancelled()V

    .line 232
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;->mPrefManager:Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->setFirstRunFlowComplete(Z)V

    .line 233
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;->mPrefManager:Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->setFirstRunFlowSignInComplete(Z)V

    .line 234
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;->mPrefManager:Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->setFirstRunFlowSignInAccountName(Ljava/lang/String;)V

    .line 235
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;->mPrefManager:Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/chrome/preferences/ChromePreferenceManager;->setFirstRunFlowSignInSetupSync(Z)V

    .line 236
    iget-object v0, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;->mActivity:Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;->mActivity:Landroid/app/Activity;

    iget-object v2, p0, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;->mActivity:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/chrome/firstrun/FirstRunFlowSequencer;->createGenericFirstRunIntent(Landroid/app/Activity;Landroid/content/Intent;Z)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 238
    return-void
.end method

.method public static start(Landroid/app/Activity;ZLorg/chromium/chrome/browser/signin/SigninManager$SignInFlowObserver;)V
    .locals 1

    .prologue
    .line 73
    new-instance v0, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/apps/chrome/firstrun/FirstRunSignInProcessor;-><init>(Landroid/app/Activity;ZLorg/chromium/chrome/browser/signin/SigninManager$SignInFlowObserver;)V

    .line 74
    return-void
.end method
