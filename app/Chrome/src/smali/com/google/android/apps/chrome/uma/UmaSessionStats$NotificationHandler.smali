.class Lcom/google/android/apps/chrome/uma/UmaSessionStats$NotificationHandler;
.super Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;
.source "UmaSessionStats.java"


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/uma/UmaSessionStats;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/chrome/uma/UmaSessionStats;)V
    .locals 0

    .prologue
    .line 36
    iput-object p1, p0, Lcom/google/android/apps/chrome/uma/UmaSessionStats$NotificationHandler;->this$0:Lcom/google/android/apps/chrome/uma/UmaSessionStats;

    invoke-direct {p0}, Lcom/google/android/apps/chrome/ChromeNotificationCenter$ChromeNotificationHandler;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/chrome/uma/UmaSessionStats;Lcom/google/android/apps/chrome/uma/UmaSessionStats$1;)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/uma/UmaSessionStats$NotificationHandler;-><init>(Lcom/google/android/apps/chrome/uma/UmaSessionStats;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/apps/chrome/uma/UmaSessionStats$NotificationHandler;->this$0:Lcom/google/android/apps/chrome/uma/UmaSessionStats;

    # getter for: Lcom/google/android/apps/chrome/uma/UmaSessionStats;->mTabModelSelector:Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;
    invoke-static {v0}, Lcom/google/android/apps/chrome/uma/UmaSessionStats;->access$000(Lcom/google/android/apps/chrome/uma/UmaSessionStats;)Lorg/chromium/chrome/browser/tabmodel/TabModelSelector;

    move-result-object v0

    if-nez v0, :cond_0

    .line 41
    const-string/jumbo v0, "UmaSessionStats"

    const-string/jumbo v1, "Ignoring notification: mTabModelSelector is null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 48
    :goto_0
    return-void

    .line 45
    :cond_0
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 47
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/chrome/uma/UmaSessionStats$NotificationHandler;->this$0:Lcom/google/android/apps/chrome/uma/UmaSessionStats;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string/jumbo v2, "tabId"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    # invokes: Lcom/google/android/apps/chrome/uma/UmaSessionStats;->recordPageLoadStats(I)V
    invoke-static {v0, v1}, Lcom/google/android/apps/chrome/uma/UmaSessionStats;->access$100(Lcom/google/android/apps/chrome/uma/UmaSessionStats;I)V

    goto :goto_0

    .line 45
    nop

    :pswitch_data_0
    .packed-switch 0x9
        :pswitch_0
    .end packed-switch
.end method
