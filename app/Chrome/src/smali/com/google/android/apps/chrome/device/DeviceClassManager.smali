.class public Lcom/google/android/apps/chrome/device/DeviceClassManager;
.super Ljava/lang/Object;
.source "DeviceClassManager.java"


# static fields
.field private static sInstance:Lcom/google/android/apps/chrome/device/DeviceClassManager;


# instance fields
.field private mEnableAccessibilityLayout:Z

.field private mEnableAnimations:Z

.field private final mEnableFullscreen:Z

.field private mEnableInstantSearchClicks:Z

.field private mEnableLayerDecorationCache:Z

.field private mEnablePrerendering:Z

.field private mEnableSnapshots:Z

.field private mEnableToolbarSwipe:Z

.field private mEnableUndo:Z


# direct methods
.method private constructor <init>()V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    invoke-static {}, Lcom/google/android/apps/chrome/device/DeviceClassManager;->isLowEndDevice()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 50
    iput-boolean v2, p0, Lcom/google/android/apps/chrome/device/DeviceClassManager;->mEnableSnapshots:Z

    .line 51
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/device/DeviceClassManager;->mEnableLayerDecorationCache:Z

    .line 52
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/device/DeviceClassManager;->mEnableAccessibilityLayout:Z

    .line 53
    iput-boolean v2, p0, Lcom/google/android/apps/chrome/device/DeviceClassManager;->mEnableAnimations:Z

    .line 54
    iput-boolean v2, p0, Lcom/google/android/apps/chrome/device/DeviceClassManager;->mEnablePrerendering:Z

    .line 55
    iput-boolean v2, p0, Lcom/google/android/apps/chrome/device/DeviceClassManager;->mEnableToolbarSwipe:Z

    .line 56
    iput-boolean v2, p0, Lcom/google/android/apps/chrome/device/DeviceClassManager;->mEnableInstantSearchClicks:Z

    .line 67
    :goto_0
    invoke-static {}, Lorg/chromium/base/ApplicationStatus;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lorg/chromium/ui/base/DeviceFormFactor;->isTablet(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 68
    iput-boolean v2, p0, Lcom/google/android/apps/chrome/device/DeviceClassManager;->mEnableAccessibilityLayout:Z

    .line 72
    :cond_0
    invoke-static {}, Lorg/chromium/base/CommandLine;->getInstance()Lorg/chromium/base/CommandLine;

    move-result-object v3

    .line 73
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/device/DeviceClassManager;->mEnableAccessibilityLayout:Z

    const-string/jumbo v4, "enable-accessibility-tab-switcher"

    invoke-virtual {v3, v4}, Lorg/chromium/base/CommandLine;->hasSwitch(Ljava/lang/String;)Z

    move-result v4

    or-int/2addr v0, v4

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/device/DeviceClassManager;->mEnableAccessibilityLayout:Z

    .line 75
    const-string/jumbo v0, "disable-fullscreen"

    invoke-virtual {v3, v0}, Lorg/chromium/base/CommandLine;->hasSwitch(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/apps/chrome/device/DeviceClassManager;->mEnableFullscreen:Z

    .line 77
    const-string/jumbo v0, "enable-high-end-ui-undo"

    invoke-virtual {v3, v0}, Lorg/chromium/base/CommandLine;->hasSwitch(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/device/DeviceClassManager;->mEnableUndo:Z

    .line 80
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/device/DeviceClassManager;->mEnableAccessibilityLayout:Z

    if-eqz v0, :cond_1

    .line 81
    iput-boolean v2, p0, Lcom/google/android/apps/chrome/device/DeviceClassManager;->mEnableAnimations:Z

    .line 84
    :cond_1
    invoke-static {}, Lcom/google/android/apps/chrome/device/DeviceClassManager;->isLowEndDevice()Z

    move-result v0

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/apps/chrome/device/DeviceClassManager;->mEnableAccessibilityLayout:Z

    if-eqz v0, :cond_3

    .line 85
    :cond_2
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/device/DeviceClassManager;->mEnableUndo:Z

    .line 87
    :cond_3
    return-void

    .line 58
    :cond_4
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/device/DeviceClassManager;->mEnableSnapshots:Z

    .line 59
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/device/DeviceClassManager;->mEnableLayerDecorationCache:Z

    .line 60
    iput-boolean v2, p0, Lcom/google/android/apps/chrome/device/DeviceClassManager;->mEnableAccessibilityLayout:Z

    .line 61
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/device/DeviceClassManager;->mEnableAnimations:Z

    .line 62
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/device/DeviceClassManager;->mEnablePrerendering:Z

    .line 63
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/device/DeviceClassManager;->mEnableToolbarSwipe:Z

    .line 64
    iput-boolean v1, p0, Lcom/google/android/apps/chrome/device/DeviceClassManager;->mEnableInstantSearchClicks:Z

    goto :goto_0

    :cond_5
    move v0, v2

    .line 75
    goto :goto_1
.end method

.method public static enableAccessibilityLayout()Z
    .locals 1

    .prologue
    .line 107
    invoke-static {}, Lcom/google/android/apps/chrome/device/DeviceClassManager;->getInstance()Lcom/google/android/apps/chrome/device/DeviceClassManager;

    move-result-object v0

    iget-boolean v0, v0, Lcom/google/android/apps/chrome/device/DeviceClassManager;->mEnableAccessibilityLayout:Z

    return v0
.end method

.method public static enableAnimations(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 122
    invoke-static {}, Lcom/google/android/apps/chrome/device/DeviceClassManager;->getInstance()Lcom/google/android/apps/chrome/device/DeviceClassManager;

    move-result-object v0

    iget-boolean v0, v0, Lcom/google/android/apps/chrome/device/DeviceClassManager;->mEnableAnimations:Z

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/google/android/apps/chrome/device/DeviceClassManager;->isAccessibilityModeEnabled(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static enableFullscreen()Z
    .locals 1

    .prologue
    .line 114
    invoke-static {}, Lcom/google/android/apps/chrome/device/DeviceClassManager;->getInstance()Lcom/google/android/apps/chrome/device/DeviceClassManager;

    move-result-object v0

    iget-boolean v0, v0, Lcom/google/android/apps/chrome/device/DeviceClassManager;->mEnableFullscreen:Z

    return v0
.end method

.method public static enableInstantSearchClicks()Z
    .locals 1

    .prologue
    .line 143
    invoke-static {}, Lcom/google/android/apps/chrome/device/DeviceClassManager;->getInstance()Lcom/google/android/apps/chrome/device/DeviceClassManager;

    move-result-object v0

    iget-boolean v0, v0, Lcom/google/android/apps/chrome/device/DeviceClassManager;->mEnableInstantSearchClicks:Z

    return v0
.end method

.method public static enableLayerDecorationCache()Z
    .locals 1

    .prologue
    .line 100
    invoke-static {}, Lcom/google/android/apps/chrome/device/DeviceClassManager;->getInstance()Lcom/google/android/apps/chrome/device/DeviceClassManager;

    move-result-object v0

    iget-boolean v0, v0, Lcom/google/android/apps/chrome/device/DeviceClassManager;->mEnableLayerDecorationCache:Z

    return v0
.end method

.method public static enablePrerendering()Z
    .locals 1

    .prologue
    .line 129
    invoke-static {}, Lcom/google/android/apps/chrome/device/DeviceClassManager;->getInstance()Lcom/google/android/apps/chrome/device/DeviceClassManager;

    move-result-object v0

    iget-boolean v0, v0, Lcom/google/android/apps/chrome/device/DeviceClassManager;->mEnablePrerendering:Z

    return v0
.end method

.method public static enableToolbarSwipe()Z
    .locals 1

    .prologue
    .line 136
    invoke-static {}, Lcom/google/android/apps/chrome/device/DeviceClassManager;->getInstance()Lcom/google/android/apps/chrome/device/DeviceClassManager;

    move-result-object v0

    iget-boolean v0, v0, Lcom/google/android/apps/chrome/device/DeviceClassManager;->mEnableToolbarSwipe:Z

    return v0
.end method

.method public static enableUndo(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 150
    invoke-static {}, Lcom/google/android/apps/chrome/device/DeviceClassManager;->getInstance()Lcom/google/android/apps/chrome/device/DeviceClassManager;

    move-result-object v0

    iget-boolean v0, v0, Lcom/google/android/apps/chrome/device/DeviceClassManager;->mEnableUndo:Z

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/google/android/apps/chrome/device/DeviceClassManager;->isAccessibilityModeEnabled(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static getInstance()Lcom/google/android/apps/chrome/device/DeviceClassManager;
    .locals 1

    .prologue
    .line 37
    sget-object v0, Lcom/google/android/apps/chrome/device/DeviceClassManager;->sInstance:Lcom/google/android/apps/chrome/device/DeviceClassManager;

    if-nez v0, :cond_0

    .line 38
    new-instance v0, Lcom/google/android/apps/chrome/device/DeviceClassManager;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/device/DeviceClassManager;-><init>()V

    sput-object v0, Lcom/google/android/apps/chrome/device/DeviceClassManager;->sInstance:Lcom/google/android/apps/chrome/device/DeviceClassManager;

    .line 40
    :cond_0
    sget-object v0, Lcom/google/android/apps/chrome/device/DeviceClassManager;->sInstance:Lcom/google/android/apps/chrome/device/DeviceClassManager;

    return-object v0
.end method

.method public static isAccessibilityModeEnabled(Landroid/content/Context;)Z
    .locals 2

    .prologue
    .line 161
    const-string/jumbo v0, "DeviceClassManager::isAccessibilityModeEnabled"

    invoke-static {v0}, Lorg/chromium/base/TraceEvent;->begin(Ljava/lang/String;)V

    .line 162
    const-string/jumbo v0, "accessibility"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    .line 164
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isTouchExplorationEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 166
    :goto_0
    const-string/jumbo v1, "DeviceClassManager::isAccessibilityModeEnabled"

    invoke-static {v1}, Lorg/chromium/base/TraceEvent;->end(Ljava/lang/String;)V

    .line 167
    return v0

    .line 164
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isLowEndDevice()Z
    .locals 1

    .prologue
    .line 157
    invoke-static {}, Lorg/chromium/base/SysUtils;->isLowEndDevice()Z

    move-result v0

    return v0
.end method
