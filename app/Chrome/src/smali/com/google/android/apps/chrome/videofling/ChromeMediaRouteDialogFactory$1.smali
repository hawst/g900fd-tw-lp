.class Lcom/google/android/apps/chrome/videofling/ChromeMediaRouteDialogFactory$1;
.super Landroid/support/v7/app/A;
.source "ChromeMediaRouteDialogFactory.java"


# instance fields
.field final mVisibilitySaver:Lcom/google/android/apps/chrome/videofling/ChromeMediaRouteDialogFactory$SystemVisibilitySaver;

.field final synthetic this$0:Lcom/google/android/apps/chrome/videofling/ChromeMediaRouteDialogFactory;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/videofling/ChromeMediaRouteDialogFactory;)V
    .locals 2

    .prologue
    .line 68
    iput-object p1, p0, Lcom/google/android/apps/chrome/videofling/ChromeMediaRouteDialogFactory$1;->this$0:Lcom/google/android/apps/chrome/videofling/ChromeMediaRouteDialogFactory;

    invoke-direct {p0}, Landroid/support/v7/app/A;-><init>()V

    .line 69
    new-instance v0, Lcom/google/android/apps/chrome/videofling/ChromeMediaRouteDialogFactory$SystemVisibilitySaver;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/apps/chrome/videofling/ChromeMediaRouteDialogFactory$SystemVisibilitySaver;-><init>(Lcom/google/android/apps/chrome/videofling/ChromeMediaRouteDialogFactory$1;)V

    iput-object v0, p0, Lcom/google/android/apps/chrome/videofling/ChromeMediaRouteDialogFactory$1;->mVisibilitySaver:Lcom/google/android/apps/chrome/videofling/ChromeMediaRouteDialogFactory$SystemVisibilitySaver;

    return-void
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    .prologue
    .line 73
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    .line 74
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/ChromeMediaRouteDialogFactory$1;->mVisibilitySaver:Lcom/google/android/apps/chrome/videofling/ChromeMediaRouteDialogFactory$SystemVisibilitySaver;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/ChromeMediaRouteDialogFactory$1;->getActivity()Landroid/support/v4/app/k;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/videofling/ChromeMediaRouteDialogFactory$SystemVisibilitySaver;->saveSystemVisibility(Landroid/app/Activity;)V

    .line 76
    :cond_0
    new-instance v0, Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/ChromeMediaRouteDialogFactory$1;->getActivity()Landroid/support/v4/app/k;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/chrome/videofling/ChromeMediaRouteDialogFactory$1;->this$0:Lcom/google/android/apps/chrome/videofling/ChromeMediaRouteDialogFactory;

    # getter for: Lcom/google/android/apps/chrome/videofling/ChromeMediaRouteDialogFactory;->mDisconnectListener:Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog$DisconnectListener;
    invoke-static {v2}, Lcom/google/android/apps/chrome/videofling/ChromeMediaRouteDialogFactory;->access$100(Lcom/google/android/apps/chrome/videofling/ChromeMediaRouteDialogFactory;)Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog$DisconnectListener;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog;-><init>(Landroid/content/Context;Lcom/google/android/apps/chrome/third_party/ChromeMediaRouteControllerDialog$DisconnectListener;)V

    return-object v0
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 81
    invoke-super {p0}, Landroid/support/v7/app/A;->onStop()V

    .line 82
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    .line 83
    iget-object v0, p0, Lcom/google/android/apps/chrome/videofling/ChromeMediaRouteDialogFactory$1;->mVisibilitySaver:Lcom/google/android/apps/chrome/videofling/ChromeMediaRouteDialogFactory$SystemVisibilitySaver;

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/videofling/ChromeMediaRouteDialogFactory$1;->getActivity()Landroid/support/v4/app/k;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/videofling/ChromeMediaRouteDialogFactory$SystemVisibilitySaver;->restoreSystemVisibility(Landroid/app/Activity;)V

    .line 85
    :cond_0
    return-void
.end method
