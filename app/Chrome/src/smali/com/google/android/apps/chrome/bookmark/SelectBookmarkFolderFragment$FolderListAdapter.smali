.class Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment$FolderListAdapter;
.super Landroid/widget/ArrayAdapter;
.source "SelectBookmarkFolderFragment.java"


# instance fields
.field private final mDefaultPaddingLeft:I

.field private final mPaddingLeftInc:I

.field final synthetic this$0:Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment;Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 255
    iput-object p1, p0, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment$FolderListAdapter;->this$0:Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment;

    .line 256
    sget v0, Lcom/google/android/apps/chrome/R$layout;->select_bookmark_folder_item:I

    invoke-direct {p0, p2, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 258
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 259
    sget v1, Lcom/google/android/apps/chrome/R$dimen;->select_bookmark_folder_item_left:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment$FolderListAdapter;->mDefaultPaddingLeft:I

    .line 261
    sget v1, Lcom/google/android/apps/chrome/R$dimen;->select_bookmark_folder_item_inc_left:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment$FolderListAdapter;->mPaddingLeftInc:I

    .line 263
    return-void
.end method


# virtual methods
.method public getItemId(I)J
    .locals 2

    .prologue
    .line 292
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment$FolderListAdapter;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment$FolderListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment$FolderListEntry;

    iget-object v0, v0, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment$FolderListEntry;->mFolder:Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/ChromeBrowserProvider$BookmarkNode;->id()J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 267
    invoke-super {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 268
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment$FolderListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment$FolderListEntry;

    .line 272
    iget v3, p0, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment$FolderListAdapter;->mDefaultPaddingLeft:I

    iget v4, v1, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment$FolderListEntry;->mDepth:I

    iget-object v5, p0, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment$FolderListAdapter;->this$0:Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment;

    # getter for: Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment;->mMaximumFolderIndentDepth:I
    invoke-static {v5}, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment;->access$000(Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment;)I

    move-result v5

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v4

    iget v5, p0, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment$FolderListAdapter;->mPaddingLeftInc:I

    mul-int/2addr v4, v5

    add-int/2addr v3, v4

    .line 274
    invoke-static {}, Lorg/chromium/ui/base/LocalizationUtils;->isLayoutRtl()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 275
    invoke-virtual {v0, v2, v2, v3, v2}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 279
    :goto_0
    const/4 v3, 0x0

    iget-boolean v4, v1, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment$FolderListEntry;->mIsSelectedFolder:Z

    if-eqz v4, :cond_0

    const/4 v2, 0x1

    :cond_0
    invoke-virtual {v0, v3, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 280
    sget v2, Lcom/google/android/apps/chrome/R$drawable;->ntp_toolbar_button_background:I

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 281
    new-instance v2, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment$FolderListAdapter$1;

    invoke-direct {v2, p0, v1}, Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment$FolderListAdapter$1;-><init>(Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment$FolderListAdapter;Lcom/google/android/apps/chrome/bookmark/SelectBookmarkFolderFragment$FolderListEntry;)V

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 287
    return-object v0

    .line 277
    :cond_1
    invoke-virtual {v0, v3, v2, v2, v2}, Landroid/widget/TextView;->setPadding(IIII)V

    goto :goto_0
.end method

.method public hasStableIds()Z
    .locals 1

    .prologue
    .line 297
    const/4 v0, 0x1

    return v0
.end method
