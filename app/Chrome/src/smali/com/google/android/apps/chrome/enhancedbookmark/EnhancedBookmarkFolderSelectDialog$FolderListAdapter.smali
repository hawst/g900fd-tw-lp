.class Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog$FolderListAdapter;
.super Landroid/widget/BaseAdapter;
.source "EnhancedBookmarkFolderSelectDialog.java"


# instance fields
.field private final mBasePadding:I

.field mContext:Landroid/content/Context;

.field mEntryList:Ljava/util/List;

.field private final mIconWidth:I

.field private final mPaddingIncrement:I


# direct methods
.method public constructor <init>(Ljava/util/List;Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 216
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 217
    iput-object p2, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog$FolderListAdapter;->mContext:Landroid/content/Context;

    .line 218
    iput-object p1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog$FolderListAdapter;->mEntryList:Ljava/util/List;

    .line 219
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog$FolderListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$dimen;->enhanced_bookmark_folder_item_left:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog$FolderListAdapter;->mBasePadding:I

    .line 221
    iget v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog$FolderListAdapter;->mBasePadding:I

    shl-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog$FolderListAdapter;->mPaddingIncrement:I

    .line 222
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog$FolderListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$drawable;->eb_add_folder:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog$FolderListAdapter;->mIconWidth:I

    .line 224
    return-void
.end method

.method private setUpIcons(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog$FolderListEntry;Landroid/widget/TextView;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 285
    sget v0, Lcom/google/android/apps/chrome/R$drawable;->eb_navigation_item_folder:I

    .line 287
    iget v2, p1, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog$FolderListEntry;->mType:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    .line 288
    sget v0, Lcom/google/android/apps/chrome/R$drawable;->eb_add_folder:I

    .line 294
    :cond_0
    :goto_0
    iget-boolean v2, p1, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog$FolderListEntry;->mIsSelected:Z

    if-eqz v2, :cond_2

    sget v2, Lcom/google/android/apps/chrome/R$drawable;->eb_check_blue:I

    .line 295
    :goto_1
    invoke-static {p2, v0, v1, v2, v1}, Lorg/chromium/base/ApiCompatibilityUtils;->setCompoundDrawablesRelativeWithIntrinsicBounds(Landroid/widget/TextView;IIII)V

    .line 297
    return-void

    .line 289
    :cond_1
    iget v2, p1, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog$FolderListEntry;->mType:I

    if-nez v2, :cond_0

    move v0, v1

    .line 291
    goto :goto_0

    :cond_2
    move v2, v1

    .line 294
    goto :goto_1
.end method

.method private setUpPadding(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog$FolderListEntry;Landroid/widget/TextView;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 305
    iget v0, p1, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog$FolderListEntry;->mType:I

    if-nez v0, :cond_0

    .line 307
    iget v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog$FolderListAdapter;->mIconWidth:I

    invoke-virtual {p2}, Landroid/widget/TextView;->getCompoundDrawablePadding()I

    move-result v1

    add-int/2addr v0, v1

    iget v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog$FolderListAdapter;->mBasePadding:I

    add-int/2addr v0, v1

    .line 313
    :goto_0
    iget v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog$FolderListAdapter;->mBasePadding:I

    invoke-static {p2, v0, v3, v1, v3}, Lorg/chromium/base/ApiCompatibilityUtils;->setPaddingRelative(Landroid/view/View;IIII)V

    .line 315
    return-void

    .line 310
    :cond_0
    iget v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog$FolderListAdapter;->mBasePadding:I

    iget v1, p1, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog$FolderListEntry;->mDepth:I

    const/4 v2, 0x5

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    iget v2, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog$FolderListAdapter;->mPaddingIncrement:I

    mul-int/2addr v1, v2

    add-int/2addr v0, v1

    goto :goto_0
.end method


# virtual methods
.method public areAllItemsEnabled()Z
    .locals 1

    .prologue
    .line 319
    const/4 v0, 0x0

    return v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 228
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog$FolderListAdapter;->mEntryList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog$FolderListEntry;
    .locals 1

    .prologue
    .line 233
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog$FolderListAdapter;->mEntryList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog$FolderListEntry;

    return-object v0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 204
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog$FolderListAdapter;->getItem(I)Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog$FolderListEntry;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 238
    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 1

    .prologue
    .line 251
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog$FolderListAdapter;->getItem(I)Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog$FolderListEntry;

    move-result-object v0

    .line 252
    iget v0, v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog$FolderListEntry;->mType:I

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 257
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog$FolderListAdapter;->getItem(I)Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog$FolderListEntry;

    move-result-object v1

    .line 258
    if-eqz p2, :cond_0

    iget v0, v1, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog$FolderListEntry;->mType:I

    const/4 v2, 0x3

    if-eq v0, v2, :cond_0

    .line 276
    :goto_0
    return-object p2

    .line 261
    :cond_0
    if-nez p2, :cond_2

    .line 262
    iget v0, v1, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog$FolderListEntry;->mType:I

    const/4 v2, 0x2

    if-ne v0, v2, :cond_1

    .line 263
    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/google/android/apps/chrome/R$layout;->eb_drawer_item_divider:I

    invoke-virtual {v0, v1, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    goto :goto_0

    .line 266
    :cond_1
    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v2, Lcom/google/android/apps/chrome/R$layout;->eb_folder_select_item:I

    invoke-virtual {v0, v2, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 270
    :goto_1
    check-cast v0, Landroid/widget/TextView;

    .line 271
    iget-object v2, v1, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog$FolderListEntry;->mTitle:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 273
    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog$FolderListAdapter;->setUpIcons(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog$FolderListEntry;Landroid/widget/TextView;)V

    .line 274
    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog$FolderListAdapter;->setUpPadding(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog$FolderListEntry;Landroid/widget/TextView;)V

    move-object p2, v0

    .line 276
    goto :goto_0

    :cond_2
    move-object v0, p2

    goto :goto_1
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 246
    const/4 v0, 0x4

    return v0
.end method

.method public isEnabled(I)Z
    .locals 2

    .prologue
    .line 327
    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog$FolderListAdapter;->getItem(I)Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog$FolderListEntry;

    move-result-object v0

    iget v0, v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog$FolderListEntry;->mType:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
