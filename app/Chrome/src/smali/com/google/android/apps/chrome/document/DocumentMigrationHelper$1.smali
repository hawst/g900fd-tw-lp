.class final Lcom/google/android/apps/chrome/document/DocumentMigrationHelper$1;
.super Lcom/google/android/apps/chrome/document/DocumentTabList$InitializationObserver;
.source "DocumentMigrationHelper.java"


# instance fields
.field final synthetic val$activity:Landroid/app/Activity;

.field final synthetic val$finalizeMode:I

.field final synthetic val$normalTabList:Lcom/google/android/apps/chrome/document/DocumentMigrationHelper$MigrationTabList;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/document/DocumentTabList;Landroid/app/Activity;Lcom/google/android/apps/chrome/document/DocumentMigrationHelper$MigrationTabList;I)V
    .locals 0

    .prologue
    .line 183
    iput-object p2, p0, Lcom/google/android/apps/chrome/document/DocumentMigrationHelper$1;->val$activity:Landroid/app/Activity;

    iput-object p3, p0, Lcom/google/android/apps/chrome/document/DocumentMigrationHelper$1;->val$normalTabList:Lcom/google/android/apps/chrome/document/DocumentMigrationHelper$MigrationTabList;

    iput p4, p0, Lcom/google/android/apps/chrome/document/DocumentMigrationHelper$1;->val$finalizeMode:I

    invoke-direct {p0, p1}, Lcom/google/android/apps/chrome/document/DocumentTabList$InitializationObserver;-><init>(Lcom/google/android/apps/chrome/document/DocumentTabList;)V

    return-void
.end method


# virtual methods
.method public final isCanceled()Z
    .locals 1

    .prologue
    .line 196
    const/4 v0, 0x0

    return v0
.end method

.method public final isSatisfied()Z
    .locals 2

    .prologue
    .line 191
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentMigrationHelper$1;->val$normalTabList:Lcom/google/android/apps/chrome/document/DocumentMigrationHelper$MigrationTabList;

    invoke-virtual {v0}, Lcom/google/android/apps/chrome/document/DocumentMigrationHelper$MigrationTabList;->getCurrentState()I

    move-result v0

    const/4 v1, 0x6

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final runImmediately()V
    .locals 3

    .prologue
    .line 186
    iget-object v0, p0, Lcom/google/android/apps/chrome/document/DocumentMigrationHelper$1;->val$activity:Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/android/apps/chrome/document/DocumentMigrationHelper$1;->val$normalTabList:Lcom/google/android/apps/chrome/document/DocumentMigrationHelper$MigrationTabList;

    iget v2, p0, Lcom/google/android/apps/chrome/document/DocumentMigrationHelper$1;->val$finalizeMode:I

    # invokes: Lcom/google/android/apps/chrome/document/DocumentMigrationHelper;->addAppTasksFromFiles(Landroid/app/Activity;Lcom/google/android/apps/chrome/document/DocumentMigrationHelper$MigrationTabList;I)V
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/chrome/document/DocumentMigrationHelper;->access$000(Landroid/app/Activity;Lcom/google/android/apps/chrome/document/DocumentMigrationHelper$MigrationTabList;I)V

    .line 187
    return-void
.end method
