.class Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper$4;
.super Lorg/chromium/components/web_contents_delegate_android/WebContentsDelegateAndroid;
.source "BackgroundContentViewHelper.java"


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;)V
    .locals 0

    .prologue
    .line 250
    iput-object p1, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper$4;->this$0:Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;

    invoke-direct {p0}, Lorg/chromium/components/web_contents_delegate_android/WebContentsDelegateAndroid;-><init>()V

    return-void
.end method


# virtual methods
.method public onLoadProgressChanged(I)V
    .locals 1

    .prologue
    .line 253
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper$4;->this$0:Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;

    # setter for: Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mLoadProgress:I
    invoke-static {v0, p1}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->access$102(Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;I)I

    .line 254
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper$4;->this$0:Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;

    # getter for: Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->mDelegate:Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper$BackgroundContentViewDelegate;
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->access$200(Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;)Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper$BackgroundContentViewDelegate;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper$BackgroundContentViewDelegate;->onLoadProgressChanged(I)V

    .line 255
    iget-object v0, p0, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper$4;->this$0:Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;

    # invokes: Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->trySwappingBackgroundView()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;->access$300(Lcom/google/android/apps/chrome/tab/BackgroundContentViewHelper;)V

    .line 256
    return-void
.end method
