.class public abstract Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment$FragmentAsyncTask;
.super Landroid/os/AsyncTask;
.source "AsyncTaskFragment.java"


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment;)V
    .locals 0

    .prologue
    .line 100
    iput-object p1, p0, Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment$FragmentAsyncTask;->this$0:Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method private cleanUp()V
    .locals 1

    .prologue
    .line 153
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment$FragmentAsyncTask;->setDependentUIEnabled(Z)V

    .line 154
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment$FragmentAsyncTask;->this$0:Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment;

    # invokes: Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment;->taskFinished()V
    invoke-static {v0}, Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment;->access$400(Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment;)V

    .line 155
    return-void
.end method

.method private finishTask()V
    .locals 0

    .prologue
    .line 148
    invoke-direct {p0}, Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment$FragmentAsyncTask;->cleanUp()V

    .line 149
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment$FragmentAsyncTask;->onTaskFinished()V

    .line 150
    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 100
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment$FragmentAsyncTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 1

    .prologue
    .line 132
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment$FragmentAsyncTask;->runBackgroundTask()V

    .line 133
    const/4 v0, 0x0

    return-object v0
.end method

.method protected bridge synthetic onCancelled(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 100
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment$FragmentAsyncTask;->onCancelled(Ljava/lang/Void;)V

    return-void
.end method

.method protected onCancelled(Ljava/lang/Void;)V
    .locals 0

    .prologue
    .line 144
    invoke-direct {p0}, Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment$FragmentAsyncTask;->cleanUp()V

    .line 145
    return-void
.end method

.method onDialogStayedEnough()V
    .locals 2

    .prologue
    .line 159
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment$FragmentAsyncTask;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->FINISHED:Landroid/os/AsyncTask$Status;

    if-ne v0, v1, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment$FragmentAsyncTask;->finishTask()V

    .line 160
    :cond_0
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 100
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment$FragmentAsyncTask;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment$FragmentAsyncTask;->this$0:Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment;

    # getter for: Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment;->access$300(Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment$FragmentAsyncTask;->this$0:Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment;

    # getter for: Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment;->mHasDialogStayedEnough:Z
    invoke-static {v0}, Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment;->access$200(Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment$FragmentAsyncTask;->finishTask()V

    .line 140
    :cond_1
    return-void
.end method

.method protected onPreExecute()V
    .locals 1

    .prologue
    .line 127
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment$FragmentAsyncTask;->setDependentUIEnabled(Z)V

    .line 128
    return-void
.end method

.method protected abstract onTaskFinished()V
.end method

.method protected abstract runBackgroundTask()V
.end method

.method protected abstract setDependentUIEnabled(Z)V
.end method

.method public updateDependentUI()V
    .locals 2

    .prologue
    .line 122
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment$FragmentAsyncTask;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/apps/chrome/bookmark/AsyncTaskFragment$FragmentAsyncTask;->setDependentUIEnabled(Z)V

    .line 123
    return-void

    .line 122
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
