.class public Lcom/google/android/apps/chrome/document/DocumentUma;
.super Ljava/lang/Object;
.source "DocumentUma.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static recordClickAction(I)V
    .locals 0

    .prologue
    .line 23
    invoke-static {p0}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->recordDocumentActivityClickAction(I)V

    .line 24
    return-void
.end method

.method static recordStartedBy(I)V
    .locals 0

    .prologue
    .line 30
    invoke-static {p0}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->recordDocumentActivityStartedBy(I)V

    .line 31
    return-void
.end method

.method static recordStartedBy(Ljava/lang/String;Landroid/content/Intent;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 39
    if-nez p1, :cond_0

    .line 40
    invoke-static {v0}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->recordDocumentActivityStartedBy(I)V

    .line 79
    :goto_0
    return-void

    .line 45
    :cond_0
    invoke-static {p0, p1}, Lcom/google/android/apps/chrome/IntentHandler;->determineExternalIntentSource(Ljava/lang/String;Landroid/content/Intent;)Lcom/google/android/apps/chrome/IntentHandler$ExternalAppId;

    move-result-object v1

    .line 48
    const-string/jumbo v2, "com.android.chrome.started_by"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 49
    const-string/jumbo v1, "com.android.chrome.started_by"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 74
    :cond_1
    :goto_1
    if-nez v0, :cond_2

    .line 75
    const-string/jumbo v1, "DocumentUma"

    const-string/jumbo v2, "Unknown source detected"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 78
    :cond_2
    invoke-static {v0}, Lcom/google/android/apps/chrome/uma/UmaRecordAction;->recordDocumentActivityStartedBy(I)V

    goto :goto_0

    .line 51
    :cond_3
    const-string/jumbo v2, "REUSE_URL_MATCHING_TAB_ELSE_NEW_TAB"

    invoke-virtual {p1, v2, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 54
    const/4 v0, 0x1

    goto :goto_1

    .line 55
    :cond_4
    const-string/jumbo v2, "com.android.chrome.append_task"

    invoke-virtual {p1, v2, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 56
    const/16 v0, 0x12c

    goto :goto_1

    .line 57
    :cond_5
    const-string/jumbo v2, "com.android.chrome.preserve_task"

    invoke-virtual {p1, v2, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 59
    const/16 v0, 0x12d

    goto :goto_1

    .line 60
    :cond_6
    sget-object v2, Lcom/google/android/apps/chrome/IntentHandler$ExternalAppId;->GMAIL:Lcom/google/android/apps/chrome/IntentHandler$ExternalAppId;

    if-ne v1, v2, :cond_7

    .line 61
    const/16 v0, 0x190

    goto :goto_1

    .line 62
    :cond_7
    sget-object v2, Lcom/google/android/apps/chrome/IntentHandler$ExternalAppId;->FACEBOOK:Lcom/google/android/apps/chrome/IntentHandler$ExternalAppId;

    if-ne v1, v2, :cond_8

    .line 63
    const/16 v0, 0x191

    goto :goto_1

    .line 64
    :cond_8
    sget-object v2, Lcom/google/android/apps/chrome/IntentHandler$ExternalAppId;->PLUS:Lcom/google/android/apps/chrome/IntentHandler$ExternalAppId;

    if-ne v1, v2, :cond_9

    .line 65
    const/16 v0, 0x192

    goto :goto_1

    .line 66
    :cond_9
    sget-object v2, Lcom/google/android/apps/chrome/IntentHandler$ExternalAppId;->TWITTER:Lcom/google/android/apps/chrome/IntentHandler$ExternalAppId;

    if-ne v1, v2, :cond_a

    .line 67
    const/16 v0, 0x193

    goto :goto_1

    .line 68
    :cond_a
    sget-object v2, Lcom/google/android/apps/chrome/IntentHandler$ExternalAppId;->CHROME:Lcom/google/android/apps/chrome/IntentHandler$ExternalAppId;

    if-ne v1, v2, :cond_b

    .line 69
    const/16 v0, 0x194

    goto :goto_1

    .line 70
    :cond_b
    sget-object v2, Lcom/google/android/apps/chrome/IntentHandler$ExternalAppId;->OTHER:Lcom/google/android/apps/chrome/IntentHandler$ExternalAppId;

    if-ne v1, v2, :cond_1

    .line 71
    const/16 v0, 0x195

    goto :goto_1
.end method
