.class Lcom/google/android/apps/chrome/ChromeActivity$3;
.super Ljava/lang/Object;
.source "ChromeActivity.java"

# interfaces
.implements Lcom/google/android/apps/chrome/IntentHandler$IntentHandlerDelegate;


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/chrome/ChromeActivity;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/ChromeActivity;)V
    .locals 0

    .prologue
    .line 569
    iput-object p1, p0, Lcom/google/android/apps/chrome/ChromeActivity$3;->this$0:Lcom/google/android/apps/chrome/ChromeActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public processUrlViewIntent(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/chrome/IntentHandler$TabOpenType;Ljava/lang/String;Ljava/lang/String;ILandroid/content/Intent;)V
    .locals 0

    .prologue
    .line 581
    return-void
.end method

.method public processWebSearchIntent(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 572
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.action.WEB_SEARCH"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 573
    const-string/jumbo v1, "query"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 574
    iget-object v1, p0, Lcom/google/android/apps/chrome/ChromeActivity$3;->this$0:Lcom/google/android/apps/chrome/ChromeActivity;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/chrome/ChromeActivity;->startActivity(Landroid/content/Intent;)V

    .line 575
    return-void
.end method
