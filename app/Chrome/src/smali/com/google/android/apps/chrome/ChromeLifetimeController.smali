.class Lcom/google/android/apps/chrome/ChromeLifetimeController;
.super Ljava/lang/Object;
.source "ChromeLifetimeController.java"

# interfaces
.implements Lorg/chromium/base/ApplicationStatus$ActivityStateListener;
.implements Lorg/chromium/chrome/browser/ApplicationLifetime$Observer;


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mRemainingActivitiesCount:I

.field private mRestartChromeOnDestroy:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const-class v0, Lcom/google/android/apps/chrome/ChromeLifetimeController;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/chrome/ChromeLifetimeController;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/chrome/ChromeLifetimeController;->mRemainingActivitiesCount:I

    .line 43
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/chrome/ChromeLifetimeController;->mContext:Landroid/content/Context;

    .line 44
    invoke-static {p0}, Lorg/chromium/chrome/browser/ApplicationLifetime;->addObserver(Lorg/chromium/chrome/browser/ApplicationLifetime$Observer;)V

    .line 45
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/ChromeLifetimeController;)Z
    .locals 1

    .prologue
    .line 29
    iget-boolean v0, p0, Lcom/google/android/apps/chrome/ChromeLifetimeController;->mRestartChromeOnDestroy:Z

    return v0
.end method

.method static synthetic access$002(Lcom/google/android/apps/chrome/ChromeLifetimeController;Z)Z
    .locals 0

    .prologue
    .line 29
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/ChromeLifetimeController;->mRestartChromeOnDestroy:Z

    return p1
.end method

.method static synthetic access$100(Lcom/google/android/apps/chrome/ChromeLifetimeController;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/apps/chrome/ChromeLifetimeController;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$200(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 29
    invoke-static {p0}, Lcom/google/android/apps/chrome/ChromeLifetimeController;->scheduleRestart(Landroid/content/Context;)V

    return-void
.end method

.method private destroyProcess()V
    .locals 2

    .prologue
    .line 81
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 82
    new-instance v1, Lcom/google/android/apps/chrome/ChromeLifetimeController$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/chrome/ChromeLifetimeController$1;-><init>(Lcom/google/android/apps/chrome/ChromeLifetimeController;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 93
    return-void
.end method

.method private static scheduleRestart(Landroid/content/Context;)V
    .locals 8

    .prologue
    .line 96
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 97
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 98
    const-string/jumbo v1, "android.intent.action.MAIN"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 99
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 100
    const/4 v1, 0x0

    const/high16 v2, 0x40000000    # 2.0f

    invoke-static {p0, v1, v0, v2}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 102
    if-eqz v1, :cond_0

    .line 103
    const-string/jumbo v0, "alarm"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 104
    const/4 v2, 0x1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    add-long/2addr v4, v6

    invoke-virtual {v0, v2, v4, v5, v1}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 106
    :cond_0
    return-void
.end method


# virtual methods
.method public onActivityStateChange(Landroid/app/Activity;I)V
    .locals 1

    .prologue
    .line 70
    sget-boolean v0, Lcom/google/android/apps/chrome/ChromeLifetimeController;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/chrome/ChromeLifetimeController;->mRemainingActivitiesCount:I

    if-gtz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 71
    :cond_0
    const/4 v0, 0x6

    if-ne p2, v0, :cond_1

    .line 72
    iget v0, p0, Lcom/google/android/apps/chrome/ChromeLifetimeController;->mRemainingActivitiesCount:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/apps/chrome/ChromeLifetimeController;->mRemainingActivitiesCount:I

    .line 73
    iget v0, p0, Lcom/google/android/apps/chrome/ChromeLifetimeController;->mRemainingActivitiesCount:I

    if-nez v0, :cond_1

    .line 74
    invoke-direct {p0}, Lcom/google/android/apps/chrome/ChromeLifetimeController;->destroyProcess()V

    .line 78
    :cond_1
    return-void
.end method

.method public onTerminate(Z)V
    .locals 3

    .prologue
    .line 49
    iput-boolean p1, p0, Lcom/google/android/apps/chrome/ChromeLifetimeController;->mRestartChromeOnDestroy:Z

    .line 52
    iget v0, p0, Lcom/google/android/apps/chrome/ChromeLifetimeController;->mRemainingActivitiesCount:I

    if-lez v0, :cond_1

    .line 53
    const-string/jumbo v0, "ChromeLifetimeController"

    const-string/jumbo v1, "onTerminate called twice"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 65
    :cond_0
    return-void

    .line 57
    :cond_1
    invoke-static {}, Lorg/chromium/base/ApplicationStatus;->getRunningActivities()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 58
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 59
    if-eqz v0, :cond_2

    .line 60
    invoke-static {p0, v0}, Lorg/chromium/base/ApplicationStatus;->registerStateListenerForActivity(Lorg/chromium/base/ApplicationStatus$ActivityStateListener;Landroid/app/Activity;)V

    .line 61
    iget v2, p0, Lcom/google/android/apps/chrome/ChromeLifetimeController;->mRemainingActivitiesCount:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/android/apps/chrome/ChromeLifetimeController;->mRemainingActivitiesCount:I

    .line 62
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0
.end method
