.class Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;
.super Ljava/lang/Object;
.source "ContextualSearchUma.java"


# instance fields
.field final mHashCode:I

.field final mReason:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

.field final mState:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;


# direct methods
.method constructor <init>(Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;)V
    .locals 2

    .prologue
    .line 135
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 136
    iput-object p1, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;->mState:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    .line 137
    iput-object p2, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;->mReason:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    .line 138
    invoke-virtual {p1}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p2}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;->mHashCode:I

    .line 139
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 142
    instance-of v2, p1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;

    if-nez v2, :cond_1

    .line 149
    :cond_0
    :goto_0
    return v0

    .line 145
    :cond_1
    if-ne p1, p0, :cond_2

    move v0, v1

    .line 146
    goto :goto_0

    .line 148
    :cond_2
    check-cast p1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;

    .line 149
    iget-object v2, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;->mState:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    iget-object v3, p1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;->mState:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$PanelState;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;->mReason:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    iget-object v3, p1, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;->mReason:Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchState$StateChangeReason;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 153
    iget v0, p0, Lcom/google/android/apps/chrome/contextualsearch/ContextualSearchUma$StateChangeKey;->mHashCode:I

    return v0
.end method
