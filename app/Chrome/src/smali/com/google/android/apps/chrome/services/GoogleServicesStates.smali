.class public Lcom/google/android/apps/chrome/services/GoogleServicesStates;
.super Ljava/lang/Object;
.source "GoogleServicesStates.java"


# instance fields
.field private final mAutoLogin:Ljava/lang/Boolean;

.field private final mChromeToMobile:Ljava/lang/Boolean;

.field private final mMasterSyncState:Ljava/lang/Boolean;

.field private final mSync:Ljava/lang/Boolean;

.field private final mWantedSyncState:Ljava/lang/Boolean;


# direct methods
.method private constructor <init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/google/android/apps/chrome/services/GoogleServicesStates;->mSync:Ljava/lang/Boolean;

    .line 23
    iput-object p2, p0, Lcom/google/android/apps/chrome/services/GoogleServicesStates;->mWantedSyncState:Ljava/lang/Boolean;

    .line 24
    iput-object p3, p0, Lcom/google/android/apps/chrome/services/GoogleServicesStates;->mMasterSyncState:Ljava/lang/Boolean;

    .line 25
    iput-object p4, p0, Lcom/google/android/apps/chrome/services/GoogleServicesStates;->mChromeToMobile:Ljava/lang/Boolean;

    .line 26
    iput-object p5, p0, Lcom/google/android/apps/chrome/services/GoogleServicesStates;->mAutoLogin:Ljava/lang/Boolean;

    .line 27
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Lcom/google/android/apps/chrome/services/GoogleServicesStates$1;)V
    .locals 0

    .prologue
    .line 18
    invoke-direct/range {p0 .. p5}, Lcom/google/android/apps/chrome/services/GoogleServicesStates;-><init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;)V

    return-void
.end method

.method public static create()Lcom/google/android/apps/chrome/services/GoogleServicesStates$GoogleServicesStatesBuilder;
    .locals 1

    .prologue
    .line 76
    new-instance v0, Lcom/google/android/apps/chrome/services/GoogleServicesStates$GoogleServicesStatesBuilder;

    invoke-direct {v0}, Lcom/google/android/apps/chrome/services/GoogleServicesStates$GoogleServicesStatesBuilder;-><init>()V

    return-object v0
.end method


# virtual methods
.method public hasAutoLoginSet()Z
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/GoogleServicesStates;->mAutoLogin:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasChromeToMobile()Z
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/GoogleServicesStates;->mChromeToMobile:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasMasterSyncState()Z
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/GoogleServicesStates;->mMasterSyncState:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSync()Z
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/GoogleServicesStates;->mSync:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasWantedSyncState()Z
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/GoogleServicesStates;->mWantedSyncState:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isAutoLoginEnabled()Z
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/GoogleServicesStates;->mAutoLogin:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public isChromeToMobileEnabled()Z
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/GoogleServicesStates;->mChromeToMobile:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public isMasterSyncStateEnabled()Z
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/GoogleServicesStates;->mMasterSyncState:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public isSyncEnabled()Z
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/GoogleServicesStates;->mSync:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public isWantedSyncStateEnabled()Z
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/apps/chrome/services/GoogleServicesStates;->mWantedSyncState:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method
