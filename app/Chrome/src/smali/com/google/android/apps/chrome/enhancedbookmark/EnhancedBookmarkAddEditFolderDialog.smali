.class public Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkAddEditFolderDialog;
.super Landroid/app/DialogFragment;
.source "EnhancedBookmarkAddEditFolderDialog.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog$FolderSelectedListener;


# instance fields
.field private mBackButton:Landroid/widget/ImageButton;

.field private mBookmarkToMove:Lorg/chromium/components/bookmarks/BookmarkId;

.field private mDeleteButton:Landroid/widget/Button;

.field private mFolderId:Lorg/chromium/components/bookmarks/BookmarkId;

.field private mFolderListDialog:Landroid/app/DialogFragment;

.field private mFolderTitle:Landroid/widget/EditText;

.field private mIsAddMode:Z

.field private mIsFolderMoved:Z

.field private mModel:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;

.field private mParentId:Lorg/chromium/components/bookmarks/BookmarkId;

.field private mParentTextView:Landroid/widget/TextView;

.field private mSaveButton:Landroid/widget/ImageButton;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;)V
    .locals 1

    .prologue
    .line 91
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 36
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkAddEditFolderDialog;->mIsFolderMoved:Z

    .line 92
    iput-object p1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkAddEditFolderDialog;->mModel:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;

    .line 93
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkAddEditFolderDialog;)Lorg/chromium/components/bookmarks/BookmarkId;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkAddEditFolderDialog;->mFolderId:Lorg/chromium/components/bookmarks/BookmarkId;

    return-object v0
.end method

.method public static createAddFolderDialog(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;Lorg/chromium/components/bookmarks/BookmarkId;Landroid/app/DialogFragment;)Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkAddEditFolderDialog;
    .locals 2

    .prologue
    .line 82
    new-instance v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkAddEditFolderDialog;

    invoke-direct {v0, p0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkAddEditFolderDialog;-><init>(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;)V

    .line 84
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkAddEditFolderDialog;->mIsAddMode:Z

    .line 85
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;->getMobileFolderId()Lorg/chromium/components/bookmarks/BookmarkId;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkAddEditFolderDialog;->mParentId:Lorg/chromium/components/bookmarks/BookmarkId;

    .line 86
    iput-object p1, v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkAddEditFolderDialog;->mBookmarkToMove:Lorg/chromium/components/bookmarks/BookmarkId;

    .line 87
    iput-object p2, v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkAddEditFolderDialog;->mFolderListDialog:Landroid/app/DialogFragment;

    .line 88
    return-object v0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 158
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkAddEditFolderDialog;->mParentTextView:Landroid/widget/TextView;

    if-ne p1, v0, :cond_1

    .line 159
    new-instance v0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog;

    iget-object v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkAddEditFolderDialog;->mModel:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;

    invoke-direct {v0, v1, p0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog;-><init>(Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog$FolderSelectedListener;)V

    invoke-virtual {p0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkAddEditFolderDialog;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkFolderSelectDialog;->show(Landroid/app/FragmentManager;)V

    .line 176
    :cond_0
    :goto_0
    return-void

    .line 161
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkAddEditFolderDialog;->mSaveButton:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_4

    .line 162
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkAddEditFolderDialog;->mFolderTitle:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 163
    iget-boolean v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkAddEditFolderDialog;->mIsAddMode:Z

    if-eqz v1, :cond_2

    .line 164
    iget-object v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkAddEditFolderDialog;->mModel:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;

    iget-object v2, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkAddEditFolderDialog;->mParentId:Lorg/chromium/components/bookmarks/BookmarkId;

    invoke-virtual {v1, v2, v4, v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;->addFolder(Lorg/chromium/components/bookmarks/BookmarkId;ILjava/lang/String;)Lorg/chromium/components/bookmarks/BookmarkId;

    move-result-object v0

    .line 165
    iget-object v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkAddEditFolderDialog;->mModel:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;

    iget-object v2, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkAddEditFolderDialog;->mBookmarkToMove:Lorg/chromium/components/bookmarks/BookmarkId;

    invoke-virtual {v1, v2, v0, v4}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;->moveBookmark(Lorg/chromium/components/bookmarks/BookmarkId;Lorg/chromium/components/bookmarks/BookmarkId;I)V

    .line 166
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkAddEditFolderDialog;->mFolderListDialog:Landroid/app/DialogFragment;

    invoke-virtual {v0}, Landroid/app/DialogFragment;->dismiss()V

    .line 172
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkAddEditFolderDialog;->dismiss()V

    goto :goto_0

    .line 169
    :cond_2
    iget-boolean v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkAddEditFolderDialog;->mIsFolderMoved:Z

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkAddEditFolderDialog;->mModel:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;

    iget-object v2, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkAddEditFolderDialog;->mFolderId:Lorg/chromium/components/bookmarks/BookmarkId;

    iget-object v3, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkAddEditFolderDialog;->mParentId:Lorg/chromium/components/bookmarks/BookmarkId;

    invoke-virtual {v1, v2, v3, v4}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;->moveBookmark(Lorg/chromium/components/bookmarks/BookmarkId;Lorg/chromium/components/bookmarks/BookmarkId;I)V

    .line 170
    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkAddEditFolderDialog;->mModel:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;

    iget-object v2, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkAddEditFolderDialog;->mFolderId:Lorg/chromium/components/bookmarks/BookmarkId;

    invoke-virtual {v1, v2, v0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;->setBookmarkTitle(Lorg/chromium/components/bookmarks/BookmarkId;Ljava/lang/String;)V

    goto :goto_1

    .line 173
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkAddEditFolderDialog;->mBackButton:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_0

    .line 174
    invoke-virtual {p0}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkAddEditFolderDialog;->dismiss()V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 151
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 152
    const/4 v0, 0x2

    sget v1, Lcom/google/android/apps/chrome/R$style;->EnhancedBookmarkDialog:I

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkAddEditFolderDialog;->setStyle(II)V

    .line 153
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    .prologue
    .line 116
    sget v0, Lcom/google/android/apps/chrome/R$layout;->eb_add_edit_folder_dialog:I

    invoke-virtual {p1, v0, p2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 117
    sget v0, Lcom/google/android/apps/chrome/R$id;->dialog_title:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 119
    sget v1, Lcom/google/android/apps/chrome/R$id;->parent_folder:I

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkAddEditFolderDialog;->mParentTextView:Landroid/widget/TextView;

    .line 120
    sget v1, Lcom/google/android/apps/chrome/R$id;->folder_title:I

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkAddEditFolderDialog;->mFolderTitle:Landroid/widget/EditText;

    .line 121
    sget v1, Lcom/google/android/apps/chrome/R$id;->delete:I

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkAddEditFolderDialog;->mDeleteButton:Landroid/widget/Button;

    .line 122
    sget v1, Lcom/google/android/apps/chrome/R$id;->back:I

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    iput-object v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkAddEditFolderDialog;->mBackButton:Landroid/widget/ImageButton;

    .line 123
    sget v1, Lcom/google/android/apps/chrome/R$id;->save:I

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    iput-object v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkAddEditFolderDialog;->mSaveButton:Landroid/widget/ImageButton;

    .line 125
    iget-object v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkAddEditFolderDialog;->mBackButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 126
    iget-object v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkAddEditFolderDialog;->mSaveButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 127
    iget-object v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkAddEditFolderDialog;->mParentTextView:Landroid/widget/TextView;

    invoke-virtual {v1, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 128
    iget-object v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkAddEditFolderDialog;->mDeleteButton:Landroid/widget/Button;

    invoke-virtual {v1, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 130
    iget-boolean v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkAddEditFolderDialog;->mIsAddMode:Z

    if-eqz v1, :cond_0

    .line 131
    iget-object v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkAddEditFolderDialog;->mDeleteButton:Landroid/widget/Button;

    const/16 v3, 0x8

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 132
    sget v1, Lcom/google/android/apps/chrome/R$string;->add_folder:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 133
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkAddEditFolderDialog;->mParentTextView:Landroid/widget/TextView;

    sget v1, Lcom/google/android/apps/chrome/R$string;->enhanced_bookmark_none_folder:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkAddEditFolderDialog;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 142
    :goto_0
    return-object v2

    .line 136
    :cond_0
    sget v1, Lcom/google/android/apps/chrome/R$string;->edit_folder:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 137
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkAddEditFolderDialog;->mModel:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;

    iget-object v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkAddEditFolderDialog;->mFolderId:Lorg/chromium/components/bookmarks/BookmarkId;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;->getBookmarkById(Lorg/chromium/components/bookmarks/BookmarkId;)Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;

    move-result-object v0

    .line 138
    invoke-virtual {v0}, Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;->getParentId()Lorg/chromium/components/bookmarks/BookmarkId;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkAddEditFolderDialog;->mParentId:Lorg/chromium/components/bookmarks/BookmarkId;

    .line 139
    iget-object v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkAddEditFolderDialog;->mFolderTitle:Landroid/widget/EditText;

    invoke-virtual {v0}, Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;->getTitle()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 140
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkAddEditFolderDialog;->mParentTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkAddEditFolderDialog;->mModel:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;

    iget-object v3, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkAddEditFolderDialog;->mParentId:Lorg/chromium/components/bookmarks/BookmarkId;

    invoke-virtual {v1, v3}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;->getBookmarkById(Lorg/chromium/components/bookmarks/BookmarkId;)Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;

    move-result-object v1

    invoke-virtual {v1}, Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public onFolderSelected(Lorg/chromium/components/bookmarks/BookmarkId;)V
    .locals 3

    .prologue
    .line 184
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkAddEditFolderDialog;->mParentId:Lorg/chromium/components/bookmarks/BookmarkId;

    invoke-virtual {p1, v0}, Lorg/chromium/components/bookmarks/BookmarkId;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 190
    :goto_0
    return-void

    .line 185
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkAddEditFolderDialog;->mIsFolderMoved:Z

    .line 186
    iput-object p1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkAddEditFolderDialog;->mParentId:Lorg/chromium/components/bookmarks/BookmarkId;

    .line 189
    iget-object v0, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkAddEditFolderDialog;->mParentTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkAddEditFolderDialog;->mModel:Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;

    iget-object v2, p0, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarkAddEditFolderDialog;->mParentId:Lorg/chromium/components/bookmarks/BookmarkId;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/chrome/enhancedbookmark/EnhancedBookmarksModel;->getBookmarkById(Lorg/chromium/components/bookmarks/BookmarkId;)Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;

    move-result-object v1

    invoke-virtual {v1}, Lorg/chromium/chrome/browser/BookmarksBridge$BookmarkItem;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public show(Landroid/app/FragmentManager;)V
    .locals 1

    .prologue
    .line 179
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-super {p0, p1, v0}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 180
    return-void
.end method
